export const addTable = [
  { action: 'retain', data: 3 },
  {
    action: 'retain',
    attributes: { opType: 'slides' },
    data: {
      slide: {
        s0000000: [
          { action: 'retain', data: 1 },
          {
            action: 'retain',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                { action: 'retain', data: 1 },
                {
                  action: 'retain',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000065: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'graphicFrame' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: {
                                  object: {
                                    nvGraphicFramePr: {
                                      cNvPr: {
                                        id: 0,
                                        name: '',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                      },
                                    },
                                    xfrm: {
                                      offX: 56.44444444444444,
                                      offY: 38.1,
                                      extX: 226.13055555555556,
                                      extY: 31.642306315104165,
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  type: 'table',
                                  opType: 'graphic',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'attrs' },
                                      data: {
                                        object: {
                                          Pr: {
                                            TableW: { W: 0, Type: 0 },
                                            TableLayout: 0,
                                          },
                                          Look: {
                                            FirstCol: false,
                                            FirstRow: true,
                                            LastCol: false,
                                            LastRow: false,
                                            BandHor: true,
                                            BandVer: false,
                                          },
                                          styleName:
                                            'Middle Style 2 - accent 1',
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'grid' },
                                      data: {
                                        object: {
                                          grid: [
                                            75.25925925925927,
                                            75.25925925925927,
                                            75.25925925925927,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'rows' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { type: 'row' },
                                            data: {
                                              delta: [
                                                {
                                                  action: 'insert',
                                                  attributes: {
                                                    opType: 'attrs',
                                                  },
                                                  data: {
                                                    object: {
                                                      Pr: {
                                                        Height: {
                                                          Value: 6.596324327256946,
                                                          HRule: 0,
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                                {
                                                  action: 'insert',
                                                  attributes: {
                                                    opType: 'cells',
                                                  },
                                                  data: {
                                                    delta: [
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                        },
                                                        data: {
                                                          delta: [
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType: 'attrs',
                                                              },
                                                              data: {
                                                                object: {
                                                                  gridSpan: 2,
                                                                  rowSpan: 2,
                                                                  vMerge: true,
                                                                  hMerge: true,
                                                                  Pr: {
                                                                    GridSpan: 2,
                                                                    TableCellW: {
                                                                      W: 150.51851851851853,
                                                                      Type: 1,
                                                                    },
                                                                    VMerge: 2,
                                                                  },
                                                                },
                                                              },
                                                            },
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType:
                                                                  'txBody',
                                                              },
                                                              data: {
                                                                delta: [
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'bodyPr',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'listStyle',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'docContent',
                                                                    },
                                                                    data: {
                                                                      delta: [
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          data:
                                                                            'abc',
                                                                        },
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            paraEnd: true,
                                                                          },
                                                                          data: 1,
                                                                        },
                                                                      ],
                                                                    },
                                                                  },
                                                                ],
                                                              },
                                                            },
                                                          ],
                                                        },
                                                      },
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                          isEmpty: true,
                                                          vMerge: true,
                                                          hMerge: true,
                                                        },
                                                        data: 1,
                                                      },
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                        },
                                                        data: {
                                                          delta: [
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType: 'attrs',
                                                              },
                                                              data: {
                                                                object: {
                                                                  Pr: {
                                                                    TableCellW: {
                                                                      W: 75.25925925925927,
                                                                      Type: 1,
                                                                    },
                                                                  },
                                                                },
                                                              },
                                                            },
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType:
                                                                  'txBody',
                                                              },
                                                              data: {
                                                                delta: [
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'bodyPr',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'listStyle',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'docContent',
                                                                    },
                                                                    data: {
                                                                      delta: [
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            paraEnd: true,
                                                                          },
                                                                          data: 1,
                                                                        },
                                                                      ],
                                                                    },
                                                                  },
                                                                ],
                                                              },
                                                            },
                                                          ],
                                                        },
                                                      },
                                                    ],
                                                  },
                                                },
                                              ],
                                            },
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { type: 'row' },
                                            data: {
                                              delta: [
                                                {
                                                  action: 'insert',
                                                  attributes: {
                                                    opType: 'attrs',
                                                  },
                                                  data: {
                                                    object: {
                                                      Pr: {
                                                        Height: {
                                                          Value: 6.949102105034722,
                                                          HRule: 0,
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                                {
                                                  action: 'insert',
                                                  attributes: {
                                                    opType: 'cells',
                                                  },
                                                  data: {
                                                    delta: [
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                        },
                                                        data: {
                                                          delta: [
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType: 'attrs',
                                                              },
                                                              data: {
                                                                object: {
                                                                  gridSpan: 2,
                                                                  vMerge: true,
                                                                  hMerge: true,
                                                                  Pr: {
                                                                    GridSpan: 2,
                                                                    TableCellW: {
                                                                      W: 150.51851851851853,
                                                                      Type: 1,
                                                                    },
                                                                    VMerge: 2,
                                                                  },
                                                                },
                                                              },
                                                            },
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType:
                                                                  'txBody',
                                                              },
                                                              data: {
                                                                delta: [
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'bodyPr',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'listStyle',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'docContent',
                                                                    },
                                                                    data: {
                                                                      delta: [
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            paraEnd: true,
                                                                          },
                                                                          data: 1,
                                                                        },
                                                                      ],
                                                                    },
                                                                  },
                                                                ],
                                                              },
                                                            },
                                                          ],
                                                        },
                                                      },
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                          isEmpty: true,
                                                          vMerge: true,
                                                          hMerge: true,
                                                        },
                                                        data: 1,
                                                      },
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                        },
                                                        data: {
                                                          delta: [
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType: 'attrs',
                                                              },
                                                              data: {
                                                                object: {
                                                                  Pr: {
                                                                    TableCellW: {
                                                                      W: 75.25925925925927,
                                                                      Type: 1,
                                                                    },
                                                                  },
                                                                },
                                                              },
                                                            },
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType:
                                                                  'txBody',
                                                              },
                                                              data: {
                                                                delta: [
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'bodyPr',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'listStyle',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'docContent',
                                                                    },
                                                                    data: {
                                                                      delta: [
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            paraEnd: true,
                                                                          },
                                                                          data: 1,
                                                                        },
                                                                      ],
                                                                    },
                                                                  },
                                                                ],
                                                              },
                                                            },
                                                          ],
                                                        },
                                                      },
                                                    ],
                                                  },
                                                },
                                              ],
                                            },
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { type: 'row' },
                                            data: {
                                              delta: [
                                                {
                                                  action: 'insert',
                                                  attributes: {
                                                    opType: 'attrs',
                                                  },
                                                  data: {
                                                    object: {
                                                      Pr: {
                                                        Height: {
                                                          Value: 6.949102105034722,
                                                          HRule: 0,
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                                {
                                                  action: 'insert',
                                                  attributes: {
                                                    opType: 'cells',
                                                  },
                                                  data: {
                                                    delta: [
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                        },
                                                        data: {
                                                          delta: [
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType: 'attrs',
                                                              },
                                                              data: {
                                                                object: {
                                                                  Pr: {
                                                                    TableCellW: {
                                                                      W: 75.25925925925927,
                                                                      Type: 1,
                                                                    },
                                                                  },
                                                                },
                                                              },
                                                            },
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType:
                                                                  'txBody',
                                                              },
                                                              data: {
                                                                delta: [
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'bodyPr',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'listStyle',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'docContent',
                                                                    },
                                                                    data: {
                                                                      delta: [
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            paraEnd: true,
                                                                          },
                                                                          data: 1,
                                                                        },
                                                                      ],
                                                                    },
                                                                  },
                                                                ],
                                                              },
                                                            },
                                                          ],
                                                        },
                                                      },
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                        },
                                                        data: {
                                                          delta: [
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType: 'attrs',
                                                              },
                                                              data: {
                                                                object: {
                                                                  Pr: {
                                                                    TableCellW: {
                                                                      W: 75.25925925925927,
                                                                      Type: 1,
                                                                    },
                                                                  },
                                                                },
                                                              },
                                                            },
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType:
                                                                  'txBody',
                                                              },
                                                              data: {
                                                                delta: [
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'bodyPr',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'listStyle',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'docContent',
                                                                    },
                                                                    data: {
                                                                      delta: [
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            paraEnd: true,
                                                                          },
                                                                          data: 1,
                                                                        },
                                                                      ],
                                                                    },
                                                                  },
                                                                ],
                                                              },
                                                            },
                                                          ],
                                                        },
                                                      },
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                        },
                                                        data: {
                                                          delta: [
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType: 'attrs',
                                                              },
                                                              data: {
                                                                object: {
                                                                  Pr: {
                                                                    TableCellW: {
                                                                      W: 75.25925925925927,
                                                                      Type: 1,
                                                                    },
                                                                  },
                                                                },
                                                              },
                                                            },
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType:
                                                                  'txBody',
                                                              },
                                                              data: {
                                                                delta: [
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'bodyPr',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'listStyle',
                                                                    },
                                                                    data: 1,
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'docContent',
                                                                    },
                                                                    data: {
                                                                      delta: [
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            paraEnd: true,
                                                                          },
                                                                          data: 1,
                                                                        },
                                                                      ],
                                                                    },
                                                                  },
                                                                ],
                                                              },
                                                            },
                                                          ],
                                                        },
                                                      },
                                                    ],
                                                  },
                                                },
                                              ],
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
]
