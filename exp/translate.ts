export const translate = {
  'Slide title': '单击此处添加标题',
  'Slide subtitle': '单击此处添加副标题',
  'Click to add first slide': '单击此处添加第一张幻灯片',
  'Slide text': '幻灯片文本',
  'Your text here': '请在这里添加文字',
  'Click to add notes': '单击此处添加备注',
  Header: '页眉',
  Footer: '页脚',
  Chart: '图表',
  'Clip Art': '剪贴画',
  Diagram: '图示',
  'Date and time': '时间日期',
  Media: '媒体',
  Picture: '图片',
  Image: '图片',
  'Slide number': '幻灯片编号',
  Table: '表格',
  Loading: '加载中',
  'The end of slide preview. Click to exit.':
    '已到最后一张幻灯片，单击后退出。',
}
