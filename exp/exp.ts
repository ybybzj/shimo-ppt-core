import { Nullable } from '../liber/pervasive'

class CachedArray<T> {
  private array: T[]
  private endIndex: number = 0
  private maxCapacity: number
  get length() {
    return this.endIndex
  }
  constructor(maxCapacity?: number) {
    this.maxCapacity = maxCapacity ?? 1000
    this.array = []
  }

  reset() {
    if (this.array.length > this.maxCapacity) {
      this.array.length = this.maxCapacity
    }
    this.endIndex = 0
  }

  dispose() {
    this.array.length = 0
    this.endIndex = 0
  }

  at(index: number) {
    if (index < 0 || index >= this.endIndex) return undefined
    return this.array[index]
  }

  push(item: T) {
    this.array[this.endIndex] = item
    this.endIndex += 1
  }
  shift() {
    const result = this.array.shift()
    if (result) {
      this.endIndex -= 1
    }
    return result
  }
  unshift(item: T) {
    this.array.unshift(item)
    this.endIndex += 1
  }
  pop() {
    let result: undefined | T
    if (this.endIndex > 0) {
      result = this.array[this.endIndex - 1]
      this.endIndex -= 1
    }
    return result
  }

  insert(index: number, ...items: T[]) {
    index = index < 0 ? 0 : index > this.endIndex ? this.endIndex : index

    this.array.splice(index, 0, ...items)
    this.endIndex += items.length
  }

  remove(index: number, count: number) {
    index = index < 0 ? 0 : index > this.endIndex ? this.endIndex : index
    const result = this.array.splice(index, count)
    this.endIndex -= result.length
    return result
  }

  forEach(cb: (item: T, index: number) => void) {
    for (let i = 0; i < this.endIndex; i++) {
      cb(this.array[i], i)
    }
  }
  findIndex(pred: (item: T, index: number) => boolean): number {
    let found: number = -1
    for (let i = 0; i < this.endIndex; i++) {
      const item = this.array[i]
      if (pred(item, i) === true) {
        found = i
        break
      }
    }
    return found
  }
  find(pred: (item: T, index: number) => boolean): Nullable<T> {
    const i = this.findIndex(pred)
    return i > -1 ? this.array[i] : undefined
  }
}

function newArray() {
  const arr: number[] = []
  for (let i = 0; i < 1000; i++) {
    arr[i] = i
  }
  arr.length = 0
}

const arr = new CachedArray()
function reuseArray() {
  for (let i = 0; i < 1000; i++) {
    arr.push(i)
  }
  arr.reset()
}

function run(which: 1 | 2) {
  const fn = which === 1 ? newArray : reuseArray
  for (let i = 0; i < 100000; i++) {
    fn()
  }
}
const body = document.getElementsByTagName('body')[0]
const btn1 = document.createElement('button')
btn1.id = 'btn1'
btn1.textContent = 'newArray'

const btn2 = document.createElement('button')
btn2.id = 'btn2'
btn2.textContent = 'reuseArray'

body.appendChild(btn1)
body.appendChild(btn2)

btn1.addEventListener(
  'click',
  () => {
    run(1)
    console.log('run(1) finished')
  },
  false
)
btn2.addEventListener(
  'click',
  () => {
    run(2)
    console.log('run(2) finished')
  },
  false
)

window['run'] = run
