import { Delta, IOperatorPlainObject } from './modoc'
import { IOType as IO } from '../src'

type DeltaOrOps = Delta | IOperatorPlainObject[]

export const ioAdapter: IO.IOAdapter<Delta> = {
  fromOperations(ops?: IO.Operation[]): Delta {
    return new Delta(ops || ([] as any))
  },

  stringify(delta: DeltaOrOps): string {
    return (Delta.isDelta(delta) ? delta : new Delta(delta)).stringify()
  },
}
