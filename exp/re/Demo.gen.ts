/* TypeScript file generated from Demo.res by genType. */

/* eslint-disable */
/* tslint:disable */

import * as DemoJS from './Demo.res.js';

import type {Canvas_t as WebAPIDom_Canvas_t} from '../../re/WebApi/WebAPIDom.gen';

import type {Image_t as WebAPIDom_Image_t} from '../../re/WebApi/WebAPIDom.gen';

export const draw: (canvas:WebAPIDom_Canvas_t) => void = DemoJS.draw as any;

export const drawImage: (canvas:WebAPIDom_Canvas_t, img:WebAPIDom_Image_t) => void = DemoJS.drawImage as any;
