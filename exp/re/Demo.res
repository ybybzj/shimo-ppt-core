open RescriptCore
@scope("Math") @val external pi: float = "PI"
%%private(let {setWidth, setHeight} = module(WebAPIDom.Canvas))
%%private(
  let {getContext2d, beginPath, arc, setStrokeStyle, stroke, drawImageToDest} = module(
    WebAPICanvas2d
  )
)

@gentype
let draw = canvas => {
  canvas->setWidth(800.0)
  canvas->setHeight(600.0)

  let cxt = canvas->getContext2d

  cxt->beginPath
  // cxt->moveTo(~x=400.0, ~y=300.0)
  cxt->arc(~x=400.0, ~y=300.0, ~r=40.0, ~startAngle=1.0 *. pi, ~endAngle=2.0 *. pi, ~anticw=false)
  cxt->setStrokeStyle(#String("#ff00ff"))
  cxt->stroke
}

@gentype
let drawImage = (canvas, img: WebAPIDom.Image.t) => {
  canvas->setWidth(800.0)
  canvas->setHeight(600.0)

  let cxt = canvas->getContext2d

  let imgW = img->WebAPIDom.Image.width
  let imgH = img->WebAPIDom.Image.height
  Console.log2(imgW, imgH)
  cxt->drawImageToDest(img, ~dx=50.0, ~dy=50.0, ~dw=imgW, ~dh=imgH, ())
}
