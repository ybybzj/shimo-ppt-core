export const modocStr = [
  {
    action: 'insert',
    attributes: { opType: 'presentation' },
    data: {
      delta: [
        {
          action: 'insert',
          attributes: { opType: 'Pres' },
          data: {
            object: {
              pres: {
                defaultTextStyle: [
                  {
                    marL: 0,
                    algn: 'l',
                    defTabSz: 914400,
                    rtl: false,
                    eaLnBrk: true,
                    latinLnBrk: false,
                    hangingPunct: true,
                    defaultRunPr: {
                      fontSize: 1800,
                      kern: 1200,
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: { mods: [], color: { type: 3, val: 'tx1' } },
                          },
                        },
                      },
                      latin: { typeface: '+mn-lt' },
                      ea: { typeface: '+mn-ea' },
                      cs: { typeface: '+mn-cs' },
                    },
                  },
                  {
                    marL: 457200,
                    algn: 'l',
                    defTabSz: 914400,
                    rtl: false,
                    eaLnBrk: true,
                    latinLnBrk: false,
                    hangingPunct: true,
                    defaultRunPr: {
                      fontSize: 1800,
                      kern: 1200,
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: { mods: [], color: { type: 3, val: 'tx1' } },
                          },
                        },
                      },
                      latin: { typeface: '+mn-lt' },
                      ea: { typeface: '+mn-ea' },
                      cs: { typeface: '+mn-cs' },
                    },
                  },
                  {
                    marL: 914400,
                    algn: 'l',
                    defTabSz: 914400,
                    rtl: false,
                    eaLnBrk: true,
                    latinLnBrk: false,
                    hangingPunct: true,
                    defaultRunPr: {
                      fontSize: 1800,
                      kern: 1200,
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: { mods: [], color: { type: 3, val: 'tx1' } },
                          },
                        },
                      },
                      latin: { typeface: '+mn-lt' },
                      ea: { typeface: '+mn-ea' },
                      cs: { typeface: '+mn-cs' },
                    },
                  },
                  {
                    marL: 1371600,
                    algn: 'l',
                    defTabSz: 914400,
                    rtl: false,
                    eaLnBrk: true,
                    latinLnBrk: false,
                    hangingPunct: true,
                    defaultRunPr: {
                      fontSize: 1800,
                      kern: 1200,
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: { mods: [], color: { type: 3, val: 'tx1' } },
                          },
                        },
                      },
                      latin: { typeface: '+mn-lt' },
                      ea: { typeface: '+mn-ea' },
                      cs: { typeface: '+mn-cs' },
                    },
                  },
                  {
                    marL: 1828800,
                    algn: 'l',
                    defTabSz: 914400,
                    rtl: false,
                    eaLnBrk: true,
                    latinLnBrk: false,
                    hangingPunct: true,
                    defaultRunPr: {
                      fontSize: 1800,
                      kern: 1200,
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: { mods: [], color: { type: 3, val: 'tx1' } },
                          },
                        },
                      },
                      latin: { typeface: '+mn-lt' },
                      ea: { typeface: '+mn-ea' },
                      cs: { typeface: '+mn-cs' },
                    },
                  },
                  {
                    marL: 2286000,
                    algn: 'l',
                    defTabSz: 914400,
                    rtl: false,
                    eaLnBrk: true,
                    latinLnBrk: false,
                    hangingPunct: true,
                    defaultRunPr: {
                      fontSize: 1800,
                      kern: 1200,
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: { mods: [], color: { type: 3, val: 'tx1' } },
                          },
                        },
                      },
                      latin: { typeface: '+mn-lt' },
                      ea: { typeface: '+mn-ea' },
                      cs: { typeface: '+mn-cs' },
                    },
                  },
                  {
                    marL: 2743200,
                    algn: 'l',
                    defTabSz: 914400,
                    rtl: false,
                    eaLnBrk: true,
                    latinLnBrk: false,
                    hangingPunct: true,
                    defaultRunPr: {
                      fontSize: 1800,
                      kern: 1200,
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: { mods: [], color: { type: 3, val: 'tx1' } },
                          },
                        },
                      },
                      latin: { typeface: '+mn-lt' },
                      ea: { typeface: '+mn-ea' },
                      cs: { typeface: '+mn-cs' },
                    },
                  },
                  {
                    marL: 3200400,
                    algn: 'l',
                    defTabSz: 914400,
                    rtl: false,
                    eaLnBrk: true,
                    latinLnBrk: false,
                    hangingPunct: true,
                    defaultRunPr: {
                      fontSize: 1800,
                      kern: 1200,
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: { mods: [], color: { type: 3, val: 'tx1' } },
                          },
                        },
                      },
                      latin: { typeface: '+mn-lt' },
                      ea: { typeface: '+mn-ea' },
                      cs: { typeface: '+mn-cs' },
                    },
                  },
                  {
                    marL: 3657600,
                    algn: 'l',
                    defTabSz: 914400,
                    rtl: false,
                    eaLnBrk: true,
                    latinLnBrk: false,
                    hangingPunct: true,
                    defaultRunPr: {
                      fontSize: 1800,
                      kern: 1200,
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: { mods: [], color: { type: 3, val: 'tx1' } },
                          },
                        },
                      },
                      latin: { typeface: '+mn-lt' },
                      ea: { typeface: '+mn-ea' },
                      cs: { typeface: '+mn-cs' },
                    },
                  },
                  { defaultRunPr: { lang: 'zh-CN' } },
                ],
                attrSaveSubsetFonts: true,
                sldSz: { cx: 12192000, cy: 6858000 },
                notesSz: { cx: 6858000, cy: 9144000 },
              },
            },
          },
        },
        {
          action: 'insert',
          attributes: { opType: 'PresentationPr' },
          data: { object: { showPr: {} } },
        },
        {
          action: 'insert',
          attributes: { opType: 'TableStyle' },
          data: {
            object: {
              defaultTableStyleId: '{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}',
              styles: {},
            },
          },
        },
        {
          action: 'insert',
          attributes: { opType: 'Slides' },
          data: { object: { s0000000: { order: 0 } } },
        },
        {
          action: 'insert',
          attributes: { opType: 'SlideMaster' },
          data: { object: { sm000000: { order: 0 } } },
        },
        {
          action: 'insert',
          attributes: { opType: 'Notes' },
          data: { object: {} },
        },
        {
          action: 'insert',
          attributes: { opType: 'NotesMaster' },
          data: { object: { noteMasterRefs: [] } },
        },
      ],
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'layouts' },
    data: {
      slide: {
        l0000000: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'title',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '标题幻灯片' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000049: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { type: 'ctrTitle' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 1524000,
                                        offY: 1122363,
                                        extX: 9144000,
                                        extY: 2387600,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: { anchor: 'b', extra: {} },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 6000 },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000050: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '副标题 2' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 1, type: 'subTitle' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 1524000,
                                        offY: 3602038,
                                        extX: 9144000,
                                        extY: 1655762,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 2400 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1800 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '单击以编辑母版副标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000051: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2022/1/19',
                                              type: 'datetimeFigureOut',
                                              id: '{833FEBC9-CFB8-4265-9EDD-A53298066C8C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000052: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000053: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id: '{6E921371-DFA3-4571-9877-A95A4C003856}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000001: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'obj',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '标题和内容' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000001: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000002: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '内容占位符 2' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { idx: 1 } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000003: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2022/1/19',
                                              type: 'datetimeFigureOut',
                                              id: '{833FEBC9-CFB8-4265-9EDD-A53298066C8C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000004: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000005: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id: '{6E921371-DFA3-4571-9877-A95A4C003856}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000002: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'secHead',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '节标题' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000041: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 831850,
                                        offY: 1709738,
                                        extX: 10515600,
                                        extY: 2852737,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: { anchor: 'b', extra: {} },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: { fontSize: 6000 },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000042: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '文本占位符 2' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { idx: 1, type: 'body' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 831850,
                                        offY: 4589463,
                                        extX: 10515600,
                                        extY: 1500187,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2400,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2000,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1800,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000043: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2022/1/19',
                                              type: 'datetimeFigureOut',
                                              id: '{833FEBC9-CFB8-4265-9EDD-A53298066C8C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000044: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000045: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id: '{6E921371-DFA3-4571-9877-A95A4C003856}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000003: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'twoObj',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '两栏内容' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000022: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000023: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '内容占位符 2' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { idx: 1, sz: 'half' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 1825625,
                                        extX: 5181600,
                                        extY: 4351338,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000024: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '内容占位符 3' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { idx: 2, sz: 'half' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 6172200,
                                        offY: 1825625,
                                        extX: 5181600,
                                        extY: 4351338,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000025: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '日期占位符 4' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2022/1/19',
                                              type: 'datetimeFigureOut',
                                              id: '{833FEBC9-CFB8-4265-9EDD-A53298066C8C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000026: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 6, name: '页脚占位符 5' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000027: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: '灯片编号占位符 6',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id: '{6E921371-DFA3-4571-9877-A95A4C003856}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000004: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'twoTxTwoObj',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '比较' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000033: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 365125,
                                        extX: 10515600,
                                        extY: 1325563,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000034: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '文本占位符 2' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { idx: 1, type: 'body' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 1681163,
                                        extX: 5157787,
                                        extY: 823912,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: { anchor: 'b', extra: {} },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2400,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2000,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1800,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000035: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '内容占位符 3' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { idx: 2, sz: 'half' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 2505075,
                                        extX: 5157787,
                                        extY: 3684588,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000036: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '文本占位符 4' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 3,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 6172200,
                                        offY: 1681163,
                                        extX: 5183188,
                                        extY: 823912,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: { anchor: 'b', extra: {} },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2400,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2000,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1800,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000037: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 6, name: '内容占位符 5' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { idx: 4, sz: 'quarter' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 6172200,
                                        offY: 2505075,
                                        extX: 5183188,
                                        extY: 3684588,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000038: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 7, name: '日期占位符 6' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2022/1/19',
                                              type: 'datetimeFigureOut',
                                              id: '{833FEBC9-CFB8-4265-9EDD-A53298066C8C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000039: [
                        {
                          action: 'insert',
                          attributes: { order: 6, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 8, name: '页脚占位符 7' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000040: [
                        {
                          action: 'insert',
                          attributes: { order: 7, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 9,
                                        name: '灯片编号占位符 8',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id: '{6E921371-DFA3-4571-9877-A95A4C003856}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000005: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'titleOnly',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '仅标题' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000018: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000019: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '日期占位符 2' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2022/1/19',
                                              type: 'datetimeFigureOut',
                                              id: '{833FEBC9-CFB8-4265-9EDD-A53298066C8C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000020: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '页脚占位符 3' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000021: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: '灯片编号占位符 4',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id: '{6E921371-DFA3-4571-9877-A95A4C003856}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000006: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'blank',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '空白' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000046: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '日期占位符 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2022/1/19',
                                              type: 'datetimeFigureOut',
                                              id: '{833FEBC9-CFB8-4265-9EDD-A53298066C8C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000047: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '页脚占位符 2' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000048: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: '灯片编号占位符 3',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id: '{6E921371-DFA3-4571-9877-A95A4C003856}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000007: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'objTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '内容与标题' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000006: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 457200,
                                        extX: 3932237,
                                        extY: 1600200,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: { anchor: 'b', extra: {} },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: { fontSize: 3200 },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000007: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '内容占位符 2' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { idx: 1 } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 5183188,
                                        offY: 987425,
                                        extX: 6172200,
                                        extY: 4873625,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: { fontSize: 3200 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2800 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2400 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000008: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '文本占位符 3' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 2,
                                          sz: 'half',
                                          type: 'body',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 2057400,
                                        extX: 3932237,
                                        extY: 3811588,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1400 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1200 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000009: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '日期占位符 4' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2022/1/19',
                                              type: 'datetimeFigureOut',
                                              id: '{833FEBC9-CFB8-4265-9EDD-A53298066C8C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000010: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 6, name: '页脚占位符 5' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000011: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: '灯片编号占位符 6',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id: '{6E921371-DFA3-4571-9877-A95A4C003856}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000008: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'picTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '图片与标题' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000012: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 457200,
                                        extX: 3932237,
                                        extY: 1600200,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: { anchor: 'b', extra: {} },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: { fontSize: 3200 },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000013: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '图片占位符 2' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { idx: 1, type: 'pic' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 5183188,
                                        offY: 987425,
                                        extX: 6172200,
                                        extY: 4873625,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 3200 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2800 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2400 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000014: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '文本占位符 3' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 2,
                                          sz: 'half',
                                          type: 'body',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 2057400,
                                        extX: 3932237,
                                        extY: 3811588,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1400 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1200 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000015: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '日期占位符 4' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2022/1/19',
                                              type: 'datetimeFigureOut',
                                              id: '{833FEBC9-CFB8-4265-9EDD-A53298066C8C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000016: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 6, name: '页脚占位符 5' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000017: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: '灯片编号占位符 6',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id: '{6E921371-DFA3-4571-9877-A95A4C003856}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000009: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'vertTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '标题和竖排文字' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000054: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000055: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 3,
                                        name: '竖排文字占位符 2',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 1,
                                          orient: 'vert',
                                          type: 'body',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: { vert: 'eaVert', extra: {} },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000056: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2022/1/19',
                                              type: 'datetimeFigureOut',
                                              id: '{833FEBC9-CFB8-4265-9EDD-A53298066C8C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000057: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000058: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id: '{6E921371-DFA3-4571-9877-A95A4C003856}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000010: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'vertTitleAndTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '竖排标题与文本' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000028: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '竖排标题 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { orient: 'vert', type: 'title' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 8724900,
                                        offY: 365125,
                                        extX: 2628900,
                                        extY: 5811838,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: { vert: 'eaVert', extra: {} },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000029: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 3,
                                        name: '竖排文字占位符 2',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 1,
                                          orient: 'vert',
                                          type: 'body',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 365125,
                                        extX: 7734300,
                                        extY: 5811838,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: { vert: 'eaVert', extra: {} },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000030: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2022/1/19',
                                              type: 'datetimeFigureOut',
                                              id: '{833FEBC9-CFB8-4265-9EDD-A53298066C8C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000031: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000032: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { extra: {} } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { extra: {} } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id: '{6E921371-DFA3-4571-9877-A95A4C003856}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'slideMasters' },
    data: {
      slide: {
        sm000000: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                clrMap: {
                  bg1: 'lt1',
                  tx1: 'dk1',
                  bg2: 'lt2',
                  tx2: 'dk2',
                  accent1: 'accent1',
                  accent2: 'accent2',
                  accent3: 'accent3',
                  accent4: 'accent4',
                  accent5: 'accent5',
                  accent6: 'accent6',
                  hlink: 'hlink',
                  folHlink: 'folHlink',
                },
                txStyles: {
                  titleStyle: [
                    {
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 0, type: 'percent' },
                      defaultRunPr: {
                        fontSize: 4400,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mj-lt' },
                        ea: { typeface: '+mj-ea' },
                        cs: { typeface: '+mj-cs' },
                      },
                      bullet: { bulletType: { type: 'none' } },
                    },
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                  ],
                  bodyStyle: [
                    {
                      marL: 228600,
                      indent: -228600,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 1000, type: 'point' },
                      defaultRunPr: {
                        fontSize: 2800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 685800,
                      indent: -228600,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 2400,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 1143000,
                      indent: -228600,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 2000,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 1600200,
                      indent: -228600,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 2057400,
                      indent: -228600,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 2514600,
                      indent: -228600,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 2971800,
                      indent: -228600,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 3429000,
                      indent: -228600,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 3886200,
                      indent: -228600,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    null,
                  ],
                  otherStyle: [
                    {
                      marL: 0,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 457200,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 914400,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 1371600,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 1828800,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 2286000,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 2743200,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 3200400,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 3657600,
                      algn: 'l',
                      defTabSz: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    { defaultRunPr: { lang: 'zh-CN' } },
                  ],
                },
                themeRef: 'th000000',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'layoutRefs' },
            data: {
              object: {
                layoutRefs: [
                  'l0000000',
                  'l0000001',
                  'l0000002',
                  'l0000003',
                  'l0000004',
                  'l0000005',
                  'l0000006',
                  'l0000007',
                  'l0000008',
                  'l0000009',
                  'l0000010',
                ],
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: {
                    object: {
                      bg: {
                        bgRef: {
                          idx: 1001,
                          color: { mods: [], color: { type: 3, val: 'bg1' } },
                        },
                      },
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000059: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题占位符 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      geometry: { preset: 'rect', avLst: [] },
                                      xfrm: {
                                        offX: 838200,
                                        offY: 365125,
                                        extX: 10515600,
                                        extY: 1325563,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                          textFit: { type: 'normAutofit' },
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000060: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '文本占位符 2' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { idx: 1, type: 'body' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      geometry: { preset: 'rect', avLst: [] },
                                      xfrm: {
                                        offX: 838200,
                                        offY: 1825625,
                                        extX: 10515600,
                                        extY: 4351338,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                          textFit: { type: 'normAutofit' },
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              smtClean: false,
                                            },
                                            data: '第五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000061: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 2, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      geometry: { preset: 'rect', avLst: [] },
                                      xfrm: {
                                        offX: 838200,
                                        offY: 6356350,
                                        extX: 2743200,
                                        extY: 365125,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'l',
                                              defaultRunPr: {
                                                fontSize: 1200,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2022/1/19',
                                              type: 'datetimeFigureOut',
                                              id: '{833FEBC9-CFB8-4265-9EDD-A53298066C8C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000062: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 3,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      geometry: { preset: 'rect', avLst: [] },
                                      xfrm: {
                                        offX: 4038600,
                                        offY: 6356350,
                                        extX: 4114800,
                                        extY: 365125,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: {
                                                fontSize: 1200,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000063: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 4,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      geometry: { preset: 'rect', avLst: [] },
                                      xfrm: {
                                        offX: 8610600,
                                        offY: 6356350,
                                        extX: 2743200,
                                        extY: 365125,
                                      },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'r',
                                              defaultRunPr: {
                                                fontSize: 1200,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id: '{6E921371-DFA3-4571-9877-A95A4C003856}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'slides' },
    data: {
      slide: {
        s0000000: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                clrMap: { type: 'useMaster' },
                layoutRef: 'l0000000',
                notesRef: null,
                commentsRef: null,
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: {} },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000064: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '文本框 3' },
                                      nvUniSpPr: {},
                                      nvPr: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      geometry: { preset: 'rect', avLst: [] },
                                      xfrm: {
                                        offX: 3096666,
                                        offY: 1490703,
                                        extX: 2512679,
                                        extY: 369332,
                                      },
                                      fill: { fill: { type: 2 } },
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          rtlCol: false,
                                          wrap: 'square',
                                          textFit: { type: 'spAutoFit' },
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'ar-SA',
                                              altLang: 'zh-CN',
                                              dirty: false,
                                              latin:
                                                '{"typeface":"Bahij TheSansArabic Plain","panose":"02040503050201020203","pitchFamily":18,"charset":-78}',
                                              cs: '{"typeface":"Bahij TheSansArabic Plain","panose":"02040503050201020203","pitchFamily":18,"charset":-78}',
                                            },
                                            data: 'خاصية طي الرسائل الطويله ',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { paraEnd: true },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'themes' },
    data: {
      slide: {
        th000000: [
          {
            action: 'insert',
            data: {
              object: {
                name: 'Office 主题​​',
                themeElements: {
                  clrScheme: {
                    name: 'Office',
                    lt1: {
                      mods: [],
                      color: { type: 4, val: 'window', lastClr: 'FFFFFF' },
                    },
                    lt2: { mods: [], color: { type: 1, val: 'E7E6E6' } },
                    dk1: {
                      mods: [],
                      color: { type: 4, val: 'windowText', lastClr: '000000' },
                    },
                    dk2: { mods: [], color: { type: 1, val: '44546A' } },
                    hlink: { mods: [], color: { type: 1, val: '0563C1' } },
                    folHlink: { mods: [], color: { type: 1, val: '954F72' } },
                    accent1: { mods: [], color: { type: 1, val: '5B9BD5' } },
                    accent2: { mods: [], color: { type: 1, val: 'ED7D31' } },
                    accent3: { mods: [], color: { type: 1, val: 'A5A5A5' } },
                    accent4: { mods: [], color: { type: 1, val: 'FFC000' } },
                    accent5: { mods: [], color: { type: 1, val: '4472C4' } },
                    accent6: { mods: [], color: { type: 1, val: '70AD47' } },
                  },
                  fontScheme: {
                    name: 'Office',
                    minorFont: {
                      latin: {
                        typeface: '等线',
                        panose: '020F0502020204030204',
                      },
                      ea: { typeface: '' },
                      cs: { typeface: '' },
                      extra: {
                        supplementalFontLst: [
                          { script: 'Jpan', typeface: '游ゴシック' },
                          { script: 'Hang', typeface: '맑은 고딕' },
                          { script: 'Hans', typeface: '等线' },
                          { script: 'Hant', typeface: '新細明體' },
                          { script: 'Arab', typeface: 'Arial' },
                          { script: 'Hebr', typeface: 'Arial' },
                          { script: 'Thai', typeface: 'Cordia New' },
                          { script: 'Ethi', typeface: 'Nyala' },
                          { script: 'Beng', typeface: 'Vrinda' },
                          { script: 'Gujr', typeface: 'Shruti' },
                          { script: 'Khmr', typeface: 'DaunPenh' },
                          { script: 'Knda', typeface: 'Tunga' },
                          { script: 'Guru', typeface: 'Raavi' },
                          { script: 'Cans', typeface: 'Euphemia' },
                          { script: 'Cher', typeface: 'Plantagenet Cherokee' },
                          { script: 'Yiii', typeface: 'Microsoft Yi Baiti' },
                          { script: 'Tibt', typeface: 'Microsoft Himalaya' },
                          { script: 'Thaa', typeface: 'MV Boli' },
                          { script: 'Deva', typeface: 'Mangal' },
                          { script: 'Telu', typeface: 'Gautami' },
                          { script: 'Taml', typeface: 'Latha' },
                          { script: 'Syrc', typeface: 'Estrangelo Edessa' },
                          { script: 'Orya', typeface: 'Kalinga' },
                          { script: 'Mlym', typeface: 'Kartika' },
                          { script: 'Laoo', typeface: 'DokChampa' },
                          { script: 'Sinh', typeface: 'Iskoola Pota' },
                          { script: 'Mong', typeface: 'Mongolian Baiti' },
                          { script: 'Viet', typeface: 'Arial' },
                          { script: 'Uigh', typeface: 'Microsoft Uighur' },
                          { script: 'Geor', typeface: 'Sylfaen' },
                        ],
                      },
                    },
                    majorFont: {
                      latin: {
                        typeface: '等线 Light',
                        panose: '020F0302020204030204',
                      },
                      ea: { typeface: '' },
                      cs: { typeface: '' },
                      extra: {
                        supplementalFontLst: [
                          { script: 'Jpan', typeface: '游ゴシック Light' },
                          { script: 'Hang', typeface: '맑은 고딕' },
                          { script: 'Hans', typeface: '等线 Light' },
                          { script: 'Hant', typeface: '新細明體' },
                          { script: 'Arab', typeface: 'Times New Roman' },
                          { script: 'Hebr', typeface: 'Times New Roman' },
                          { script: 'Thai', typeface: 'Angsana New' },
                          { script: 'Ethi', typeface: 'Nyala' },
                          { script: 'Beng', typeface: 'Vrinda' },
                          { script: 'Gujr', typeface: 'Shruti' },
                          { script: 'Khmr', typeface: 'MoolBoran' },
                          { script: 'Knda', typeface: 'Tunga' },
                          { script: 'Guru', typeface: 'Raavi' },
                          { script: 'Cans', typeface: 'Euphemia' },
                          { script: 'Cher', typeface: 'Plantagenet Cherokee' },
                          { script: 'Yiii', typeface: 'Microsoft Yi Baiti' },
                          { script: 'Tibt', typeface: 'Microsoft Himalaya' },
                          { script: 'Thaa', typeface: 'MV Boli' },
                          { script: 'Deva', typeface: 'Mangal' },
                          { script: 'Telu', typeface: 'Gautami' },
                          { script: 'Taml', typeface: 'Latha' },
                          { script: 'Syrc', typeface: 'Estrangelo Edessa' },
                          { script: 'Orya', typeface: 'Kalinga' },
                          { script: 'Mlym', typeface: 'Kartika' },
                          { script: 'Laoo', typeface: 'DokChampa' },
                          { script: 'Sinh', typeface: 'Iskoola Pota' },
                          { script: 'Mong', typeface: 'Mongolian Baiti' },
                          { script: 'Viet', typeface: 'Times New Roman' },
                          { script: 'Uigh', typeface: 'Microsoft Uighur' },
                          { script: 'Geor', typeface: 'Sylfaen' },
                        ],
                      },
                    },
                  },
                  fmtScheme: {
                    name: 'Office',
                    fillStyleLst: [
                      {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              mods: [],
                              color: { type: 3, val: 'phClr' },
                            },
                          },
                        },
                      },
                      {
                        fill: {
                          type: 4,
                          data: {
                            colors: [
                              {
                                pos: 0,
                                color: {
                                  mods: [
                                    { name: 'lumMod', val: 110000 },
                                    { name: 'satMod', val: 105000 },
                                    { name: 'tint', val: 67000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                              {
                                pos: 50000,
                                color: {
                                  mods: [
                                    { name: 'lumMod', val: 105000 },
                                    { name: 'satMod', val: 103000 },
                                    { name: 'tint', val: 73000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                              {
                                pos: 100000,
                                color: {
                                  mods: [
                                    { name: 'lumMod', val: 105000 },
                                    { name: 'satMod', val: 109000 },
                                    { name: 'tint', val: 81000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                            ],
                            lin: { angle: 5400000, scale: false },
                          },
                        },
                      },
                      {
                        fill: {
                          type: 4,
                          data: {
                            colors: [
                              {
                                pos: 0,
                                color: {
                                  mods: [
                                    { name: 'satMod', val: 103000 },
                                    { name: 'lumMod', val: 102000 },
                                    { name: 'tint', val: 94000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                              {
                                pos: 50000,
                                color: {
                                  mods: [
                                    { name: 'satMod', val: 110000 },
                                    { name: 'lumMod', val: 100000 },
                                    { name: 'shade', val: 100000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                              {
                                pos: 100000,
                                color: {
                                  mods: [
                                    { name: 'lumMod', val: 99000 },
                                    { name: 'satMod', val: 120000 },
                                    { name: 'shade', val: 78000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                            ],
                            lin: { angle: 5400000, scale: false },
                          },
                        },
                      },
                    ],
                    lnStyleLst: [
                      {
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'phClr' },
                              },
                            },
                          },
                        },
                        prstDash: 'solid',
                        join: { type: 'miter', limit: 800000 },
                        w: 6350,
                        cmpd: 'sng',
                        cap: 'flat',
                        algn: 'ctr',
                      },
                      {
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'phClr' },
                              },
                            },
                          },
                        },
                        prstDash: 'solid',
                        join: { type: 'miter', limit: 800000 },
                        w: 12700,
                        cmpd: 'sng',
                        cap: 'flat',
                        algn: 'ctr',
                      },
                      {
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'phClr' },
                              },
                            },
                          },
                        },
                        prstDash: 'solid',
                        join: { type: 'miter', limit: 800000 },
                        w: 19050,
                        cmpd: 'sng',
                        cap: 'flat',
                        algn: 'ctr',
                      },
                    ],
                    bgFillStyleLst: [
                      {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              mods: [],
                              color: { type: 3, val: 'phClr' },
                            },
                          },
                        },
                      },
                      {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              mods: [
                                { name: 'tint', val: 95000 },
                                { name: 'satMod', val: 170000 },
                              ],
                              color: { type: 3, val: 'phClr' },
                            },
                          },
                        },
                      },
                      {
                        fill: {
                          type: 4,
                          data: {
                            colors: [
                              {
                                pos: 0,
                                color: {
                                  mods: [
                                    { name: 'tint', val: 93000 },
                                    { name: 'satMod', val: 150000 },
                                    { name: 'shade', val: 98000 },
                                    { name: 'lumMod', val: 102000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                              {
                                pos: 50000,
                                color: {
                                  mods: [
                                    { name: 'tint', val: 98000 },
                                    { name: 'satMod', val: 130000 },
                                    { name: 'shade', val: 90000 },
                                    { name: 'lumMod', val: 103000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                              {
                                pos: 100000,
                                color: {
                                  mods: [
                                    { name: 'shade', val: 63000 },
                                    { name: 'satMod', val: 120000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                            ],
                            lin: { angle: 5400000, scale: false },
                          },
                        },
                      },
                    ],
                  },
                },
                objectDefaults: {},
                extraClrSchemeLst: [],
              },
            },
          },
        ],
      },
    },
  },
  { action: 'insert', attributes: { opType: 'notesMasters' }, data: 1 },
  { action: 'insert', attributes: { opType: 'notes' }, data: 1 },
  { action: 'insert', attributes: { opType: 'comments' }, data: 1 },
]
