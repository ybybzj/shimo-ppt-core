import { modocStr as Text } from './text'
import { modocStr as FillRect } from './fillRect'
import { modocStr as Table } from './table'
import { modocStr as Blank } from './blank'
import { modocStr as BlankStr } from './blankStr'
import { modocStr as CustGeom } from './custGeom'
import { modocStr as Field } from './fld'
import { modocStr as BigData } from './bigData'
import { modocStr as Example } from './example'
import { modocStr as Debug } from './debug'
import { modocStr as bigText } from './bigText'
import { modocStr as arabicText } from './arabicText'
import { modocStr as bigTable } from './bigTable'
export {
  Blank,
  BlankStr,
  Text,
  FillRect,
  Table,
  CustGeom,
  Field,
  BigData,
  Example,
  Debug,
  bigText,
  arabicText,
  bigTable,
}
