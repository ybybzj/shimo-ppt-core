export const modocStr = [
  {
    action: 'insert',
    attributes: { opType: 'presentation' },
    data: {
      delta: [
        {
          action: 'insert',
          attributes: { opType: 'Pres' },
          data: {
            object: {
              pres: {
                attrAutoCompressPictures: false,
                attrSaveSubsetFonts: true,
                sldSz: { cx: 12192000, cy: 6858000 },
                notesSz: { cx: 6858000, cy: 9144000 },
              },
            },
          },
        },
        {
          action: 'insert',
          attributes: { opType: 'PresentationPr' },
          data: { object: { showPr: {} } },
        },
        {
          action: 'insert',
          attributes: { opType: 'TableStyle' },
          data: {
            object: {
              defaultTableStyleId: '{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}',
              styles: {
                '{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}': {
                  id: '{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}',
                  name: '中度样式 2 - 强调 1',
                  tblBgFill: {},
                  tblBgFillRef: {},
                  tableWholeTable: {
                    txStyle: {
                      latin: {},
                      ea: {},
                      cs: {},
                      fontRef: {
                        idx: 'minor',
                        color: { mods: [], color: { type: 2, val: 'black' } },
                      },
                      color: { mods: [], color: { type: 3, val: 'dk1' } },
                    },
                    cellStyle: {
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              mods: [{ name: 'tint', val: 20000 }],
                              color: { type: 3, val: 'accent1' },
                            },
                          },
                        },
                      },
                      fillRef: {},
                      borders: {
                        left: {
                          ln: {
                            fill: {
                              fill: {
                                type: 3,
                                data: {
                                  color: {
                                    mods: [],
                                    color: { type: 3, val: 'lt1' },
                                  },
                                },
                              },
                            },
                            w: 12700,
                            cmpd: 'sng',
                          },
                        },
                        right: {
                          ln: {
                            fill: {
                              fill: {
                                type: 3,
                                data: {
                                  color: {
                                    mods: [],
                                    color: { type: 3, val: 'lt1' },
                                  },
                                },
                              },
                            },
                            w: 12700,
                            cmpd: 'sng',
                          },
                        },
                        top: {
                          ln: {
                            fill: {
                              fill: {
                                type: 3,
                                data: {
                                  color: {
                                    mods: [],
                                    color: { type: 3, val: 'lt1' },
                                  },
                                },
                              },
                            },
                            w: 12700,
                            cmpd: 'sng',
                          },
                        },
                        bottom: {
                          ln: {
                            fill: {
                              fill: {
                                type: 3,
                                data: {
                                  color: {
                                    mods: [],
                                    color: { type: 3, val: 'lt1' },
                                  },
                                },
                              },
                            },
                            w: 12700,
                            cmpd: 'sng',
                          },
                        },
                        insideH: {
                          ln: {
                            fill: {
                              fill: {
                                type: 3,
                                data: {
                                  color: {
                                    mods: [],
                                    color: { type: 3, val: 'lt1' },
                                  },
                                },
                              },
                            },
                            w: 12700,
                            cmpd: 'sng',
                          },
                        },
                        insideV: {
                          ln: {
                            fill: {
                              fill: {
                                type: 3,
                                data: {
                                  color: {
                                    mods: [],
                                    color: { type: 3, val: 'lt1' },
                                  },
                                },
                              },
                            },
                            w: 12700,
                            cmpd: 'sng',
                          },
                        },
                      },
                    },
                  },
                  tableBand1Horz: {
                    cellStyle: {
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              mods: [{ name: 'tint', val: 40000 }],
                              color: { type: 3, val: 'accent1' },
                            },
                          },
                        },
                      },
                      fillRef: {},
                      borders: {},
                    },
                  },
                  tableBand2Horz: {
                    cellStyle: { fill: {}, fillRef: {}, borders: {} },
                  },
                  tableBand1Vert: {
                    cellStyle: {
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              mods: [{ name: 'tint', val: 40000 }],
                              color: { type: 3, val: 'accent1' },
                            },
                          },
                        },
                      },
                      fillRef: {},
                      borders: {},
                    },
                  },
                  tableBand2Vert: {
                    cellStyle: { fill: {}, fillRef: {}, borders: {} },
                  },
                  tableLastCol: {
                    txStyle: {
                      latin: {},
                      ea: {},
                      cs: {},
                      fontRef: {
                        idx: 'minor',
                        color: { mods: [], color: { type: 2, val: 'black' } },
                      },
                      bold: 'on',
                      color: { mods: [], color: { type: 3, val: 'lt1' } },
                    },
                    cellStyle: {
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              mods: [],
                              color: { type: 3, val: 'accent1' },
                            },
                          },
                        },
                      },
                      fillRef: {},
                      borders: {},
                    },
                  },
                  tableFirstCol: {
                    txStyle: {
                      latin: {},
                      ea: {},
                      cs: {},
                      fontRef: {
                        idx: 'minor',
                        color: { mods: [], color: { type: 2, val: 'black' } },
                      },
                      bold: 'on',
                      color: { mods: [], color: { type: 3, val: 'lt1' } },
                    },
                    cellStyle: {
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              mods: [],
                              color: { type: 3, val: 'accent1' },
                            },
                          },
                        },
                      },
                      fillRef: {},
                      borders: {},
                    },
                  },
                  tableLastRow: {
                    txStyle: {
                      latin: {},
                      ea: {},
                      cs: {},
                      fontRef: {
                        idx: 'minor',
                        color: { mods: [], color: { type: 2, val: 'black' } },
                      },
                      bold: 'on',
                      color: { mods: [], color: { type: 3, val: 'lt1' } },
                    },
                    cellStyle: {
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              mods: [],
                              color: { type: 3, val: 'accent1' },
                            },
                          },
                        },
                      },
                      fillRef: {},
                      borders: {
                        top: {
                          ln: {
                            fill: {
                              fill: {
                                type: 3,
                                data: {
                                  color: {
                                    mods: [],
                                    color: { type: 3, val: 'lt1' },
                                  },
                                },
                              },
                            },
                            w: 38100,
                            cmpd: 'sng',
                          },
                        },
                      },
                    },
                  },
                  tableFirstRow: {
                    txStyle: {
                      latin: {},
                      ea: {},
                      cs: {},
                      fontRef: {
                        idx: 'minor',
                        color: { mods: [], color: { type: 2, val: 'black' } },
                      },
                      bold: 'on',
                      color: { mods: [], color: { type: 3, val: 'lt1' } },
                    },
                    cellStyle: {
                      fill: {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              mods: [],
                              color: { type: 3, val: 'accent1' },
                            },
                          },
                        },
                      },
                      fillRef: {},
                      borders: {
                        bottom: {
                          ln: {
                            fill: {
                              fill: {
                                type: 3,
                                data: {
                                  color: {
                                    mods: [],
                                    color: { type: 3, val: 'lt1' },
                                  },
                                },
                              },
                            },
                            w: 38100,
                            cmpd: 'sng',
                          },
                        },
                      },
                    },
                  },
                },
              },
            },
          },
        },
        {
          action: 'insert',
          attributes: { opType: 'Slides' },
          data: { object: { s0000011: { order: 0 } } },
        },
        {
          action: 'insert',
          attributes: { opType: 'SlideMaster' },
          data: { object: { slideMasterRefs: ['sm000000'] } },
        },
        {
          action: 'insert',
          attributes: { opType: 'Notes' },
          data: { object: {} },
        },
        {
          action: 'insert',
          attributes: { opType: 'NotesMaster' },
          data: { object: { noteMasterRefs: ['nm000000'] } },
        },
      ],
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'layouts' },
    data: {
      slide: {
        l0000000: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'title',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '标题幻灯片' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000032: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'ctrTitle' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 1524000,
                                        offY: 1122363,
                                        extX: 9144000,
                                        extY: 2387600,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 6000 },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { paraEnd: true },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000033: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '副标题 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 1, type: 'subTitle' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 1524000,
                                        offY: 3602038,
                                        extX: 9144000,
                                        extY: 1655762,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 2400 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1800 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版副标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { paraEnd: true },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000034: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2020/6/8',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{9E5E444B-C64D-A44D-8F09-F2F2985754D3}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000035: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000036: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '幻灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id:
                                                '{D73F8E2F-5A3B-B24C-AACE-6D5FB0099ADB}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000001: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'obj',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '标题和内容' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000000: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { paraEnd: true },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000001: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '内容占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 1 } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000002: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2020/6/8',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{9E5E444B-C64D-A44D-8F09-F2F2985754D3}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000003: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000004: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '幻灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id:
                                                '{D73F8E2F-5A3B-B24C-AACE-6D5FB0099ADB}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000002: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'secHead',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '节标题' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000021: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 831850,
                                        offY: 1709738,
                                        extX: 10515600,
                                        extY: 2852737,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: { fontSize: 6000 },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { paraEnd: true },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000022: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '文本占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 1, type: 'body' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 831850,
                                        offY: 4589463,
                                        extX: 10515600,
                                        extY: 1500187,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2400,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2000,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1800,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000023: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2020/6/8',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{9E5E444B-C64D-A44D-8F09-F2F2985754D3}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000024: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000025: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '幻灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id:
                                                '{D73F8E2F-5A3B-B24C-AACE-6D5FB0099ADB}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000003: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'twoObj',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '两项内容' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000026: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { paraEnd: true },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000027: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '内容占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 1, sz: 'half' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 1825625,
                                        extX: 5181600,
                                        extY: 4351338,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000028: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '内容占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 2, sz: 'half' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 6172200,
                                        offY: 1825625,
                                        extX: 5181600,
                                        extY: 4351338,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000029: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '日期占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2020/6/8',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{9E5E444B-C64D-A44D-8F09-F2F2985754D3}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000030: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 6, name: '页脚占位符 5' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000031: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: '幻灯片编号占位符 6',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id:
                                                '{D73F8E2F-5A3B-B24C-AACE-6D5FB0099ADB}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000004: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'twoTxTwoObj',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '比较' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000013: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 365125,
                                        extX: 10515600,
                                        extY: 1325563,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { paraEnd: true },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000014: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '文本占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 1, type: 'body' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 1681163,
                                        extX: 5157787,
                                        extY: 823912,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2400,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2000,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1800,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000015: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '内容占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 2, sz: 'half' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 2505075,
                                        extX: 5157787,
                                        extY: 3684588,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000016: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '文本占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 3,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 6172200,
                                        offY: 1681163,
                                        extX: 5183188,
                                        extY: 823912,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2400,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2000,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1800,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                bold: true,
                                              },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000017: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 6, name: '内容占位符 5' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 4, sz: 'quarter' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 6172200,
                                        offY: 2505075,
                                        extX: 5183188,
                                        extY: 3684588,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000018: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 7, name: '日期占位符 6' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2020/6/8',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{9E5E444B-C64D-A44D-8F09-F2F2985754D3}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000019: [
                        {
                          action: 'insert',
                          attributes: { order: 6, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 8, name: '页脚占位符 7' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000020: [
                        {
                          action: 'insert',
                          attributes: { order: 7, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 9,
                                        name: '幻灯片编号占位符 8',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id:
                                                '{D73F8E2F-5A3B-B24C-AACE-6D5FB0099ADB}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000005: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'titleOnly',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '仅标题' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000037: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { paraEnd: true },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000038: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '日期占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2020/6/8',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{9E5E444B-C64D-A44D-8F09-F2F2985754D3}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000039: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '页脚占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000040: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: '幻灯片编号占位符 4',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id:
                                                '{D73F8E2F-5A3B-B24C-AACE-6D5FB0099ADB}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000006: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'blank',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '空白' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000010: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '日期占位符 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2020/6/8',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{9E5E444B-C64D-A44D-8F09-F2F2985754D3}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000011: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '页脚占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000012: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: '幻灯片编号占位符 3',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id:
                                                '{D73F8E2F-5A3B-B24C-AACE-6D5FB0099ADB}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000007: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'objTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '内容与标题' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000041: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 457200,
                                        extX: 3932237,
                                        extY: 1600200,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: { fontSize: 3200 },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { paraEnd: true },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000042: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '内容占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 1 } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 5183188,
                                        offY: 987425,
                                        extX: 6172200,
                                        extY: 4873625,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: { fontSize: 3200 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2800 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2400 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000043: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '文本占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 2,
                                          sz: 'half',
                                          type: 'body',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 2057400,
                                        extX: 3932237,
                                        extY: 3811588,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1400 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1200 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000044: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '日期占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2020/6/8',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{9E5E444B-C64D-A44D-8F09-F2F2985754D3}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000045: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 6, name: '页脚占位符 5' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000046: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: '幻灯片编号占位符 6',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id:
                                                '{D73F8E2F-5A3B-B24C-AACE-6D5FB0099ADB}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000008: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'picTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '图片与标题' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000052: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 457200,
                                        extX: 3932237,
                                        extY: 1600200,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: { fontSize: 3200 },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { paraEnd: true },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000053: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '图片占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 1, type: 'pic' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 5183188,
                                        offY: 987425,
                                        extX: 6172200,
                                        extY: 4873625,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 3200 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2800 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2400 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000054: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '文本占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 2,
                                          sz: 'half',
                                          type: 'body',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 2057400,
                                        extX: 3932237,
                                        extY: 3811588,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1400 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1200 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000055: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '日期占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2020/6/8',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{9E5E444B-C64D-A44D-8F09-F2F2985754D3}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000056: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 6, name: '页脚占位符 5' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000057: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: '幻灯片编号占位符 6',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id:
                                                '{D73F8E2F-5A3B-B24C-AACE-6D5FB0099ADB}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000009: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'vertTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '标题和竖排文本' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000047: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { paraEnd: true },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000048: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 3,
                                        name: '竖排文本占位符 2',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 1,
                                          orient: 'vert',
                                          type: 'body',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { vert: 'eaVert' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000049: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2020/6/8',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{9E5E444B-C64D-A44D-8F09-F2F2985754D3}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000050: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000051: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '幻灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id:
                                                '{D73F8E2F-5A3B-B24C-AACE-6D5FB0099ADB}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000010: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'vertTitleAndTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '竖排标题和文本' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000005: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '竖排标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { orient: 'vert', type: 'title' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 8724900,
                                        offY: 365125,
                                        extX: 2628900,
                                        extY: 5811838,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { vert: 'eaVert' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { paraEnd: true },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000006: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 3,
                                        name: '竖排文本占位符 2',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 1,
                                          orient: 'vert',
                                          type: 'body',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 365125,
                                        extX: 7734300,
                                        extY: 5811838,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { vert: 'eaVert' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000007: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2020/6/8',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{9E5E444B-C64D-A44D-8F09-F2F2985754D3}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000008: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000009: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '幻灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id:
                                                '{D73F8E2F-5A3B-B24C-AACE-6D5FB0099ADB}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'slideMasters' },
    data: {
      slide: {
        sm000000: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                clrMap: {
                  bg1: 'lt1',
                  tx1: 'dk1',
                  bg2: 'lt2',
                  tx2: 'dk2',
                  accent1: 'accent1',
                  accent2: 'accent2',
                  accent3: 'accent3',
                  accent4: 'accent4',
                  accent5: 'accent5',
                  accent6: 'accent6',
                  hlink: 'hlink',
                  folHlink: 'folHlink',
                },
                txStyles: {
                  titleStyle: [
                    {
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 0, type: 'percent' },
                      defaultRunPr: {
                        fontSize: 4400,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: { bulletType: { type: 'none' } },
                    },
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                  ],
                  bodyStyle: [
                    {
                      marL: 228600,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 1000, type: 'point' },
                      defaultRunPr: {
                        fontSize: 2800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 685800,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 2400,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 1143000,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 2000,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 1600200,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 2057400,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 2514600,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 2971800,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 3429000,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 3886200,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    null,
                  ],
                  otherStyle: [
                    {
                      marL: 0,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 457200,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 914400,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 1371600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 1828800,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 2286000,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 2743200,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 3200400,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 3657600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    { defaultRunPr: { lang: 'zh-CN' } },
                  ],
                },
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'layoutRefs' },
            data: {
              object: {
                layoutRefs: [
                  'l0000007',
                  'l0000002',
                  'l0000006',
                  'l0000001',
                  'l0000000',
                  'l0000005',
                  'l0000010',
                  'l0000004',
                  'l0000009',
                  'l0000003',
                  'l0000008',
                ],
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: {
                    object: {
                      bg: {
                        bgRef: {
                          idx: 1001,
                          color: { mods: [], color: { type: 3, val: 'bg1' } },
                        },
                      },
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000058: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题占位符 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 365125,
                                        extX: 10515600,
                                        extY: 1325563,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { paraEnd: true },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000059: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '文本占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 1, type: 'body' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 1825625,
                                        extX: 10515600,
                                        extY: 4351338,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000060: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 2, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 6356350,
                                        extX: 2743200,
                                        extY: 365125,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'l',
                                              defaultRunPr: {
                                                fontSize: 1200,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '2020/6/8',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{9E5E444B-C64D-A44D-8F09-F2F2985754D3}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000061: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 3,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 4038600,
                                        offY: 6356350,
                                        extX: 4114800,
                                        extY: 365125,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: {
                                                fontSize: 1200,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000062: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '幻灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 4,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 8610600,
                                        offY: 6356350,
                                        extX: 2743200,
                                        extY: 365125,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'r',
                                              defaultRunPr: {
                                                fontSize: 1200,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              text: '‹#›',
                                              type: 'slidenum',
                                              id:
                                                '{D73F8E2F-5A3B-B24C-AACE-6D5FB0099ADB}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'slides' },
    data: {
      slide: {
        s0000011: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: { clrMap: { type: 'useMaster' }, layoutRef: 'l0000001' },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: {} },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000080: [
                        {
                          action: 'insert',
                          attributes: { spType: 'graphicFrame', order: 0 },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: {
                                  object: {
                                    nvGraphicFramePr: {
                                      nvSpPr: {
                                        cNvPr: { id: 2, name: '表格 1' },
                                        nvUniSpPr: { locks: 1 },
                                        nvPr: {},
                                      },
                                    },
                                    xfrm: {
                                      offX: 1863558,
                                      offY: 1128740,
                                      extX: 8127999,
                                      extY: 741680,
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  type: 'table',
                                  opType: 'graphic',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'attrs' },
                                      data: {
                                        object: {
                                          style: {
                                            id:
                                              '{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}',
                                          },
                                          look: {
                                            firstRow: true,
                                            bandRow: true,
                                          },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'grid' },
                                      data: {
                                        object: {
                                          grid: [2709333, 2709333, 2709333],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'rows' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { type: 'row' },
                                            data: {
                                              delta: [
                                                {
                                                  action: 'insert',
                                                  attributes: {
                                                    opType: 'attrs',
                                                  },
                                                  data: {
                                                    object: { height: 370840 },
                                                  },
                                                },
                                                {
                                                  action: 'insert',
                                                  attributes: {
                                                    opType: 'cells',
                                                  },
                                                  data: {
                                                    delta: [
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                        },
                                                        data: {
                                                          delta: [
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType: 'attrs',
                                                              },
                                                              data: {
                                                                object: {
                                                                  pr: {},
                                                                },
                                                              },
                                                            },
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType:
                                                                  'txBody',
                                                              },
                                                              data: {
                                                                delta: [
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'bodyPr',
                                                                    },
                                                                    data: {
                                                                      object: {},
                                                                    },
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'listStyle',
                                                                    },
                                                                    data: {
                                                                      object: {
                                                                        listStyle: [
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                        ],
                                                                      },
                                                                    },
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'docContent',
                                                                    },
                                                                    data: {
                                                                      delta: [
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            lang:
                                                                              'en-US',
                                                                            altLang:
                                                                              'zh-CN',
                                                                            dirty: false,
                                                                          },
                                                                          data:
                                                                            'name',
                                                                        },
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            rPr:
                                                                              '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                                                            paraEnd: true,
                                                                          },
                                                                          data: 1,
                                                                        },
                                                                      ],
                                                                    },
                                                                  },
                                                                ],
                                                              },
                                                            },
                                                          ],
                                                        },
                                                      },
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                        },
                                                        data: {
                                                          delta: [
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType: 'attrs',
                                                              },
                                                              data: {
                                                                object: {
                                                                  pr: {},
                                                                },
                                                              },
                                                            },
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType:
                                                                  'txBody',
                                                              },
                                                              data: {
                                                                delta: [
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'bodyPr',
                                                                    },
                                                                    data: {
                                                                      object: {},
                                                                    },
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'listStyle',
                                                                    },
                                                                    data: {
                                                                      object: {
                                                                        listStyle: [
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                        ],
                                                                      },
                                                                    },
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'docContent',
                                                                    },
                                                                    data: {
                                                                      delta: [
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            lang:
                                                                              'en-US',
                                                                            altLang:
                                                                              'zh-CN',
                                                                            dirty: false,
                                                                            err: true,
                                                                          },
                                                                          data:
                                                                            'desc',
                                                                        },
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            rPr:
                                                                              '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                                                            paraEnd: true,
                                                                          },
                                                                          data: 1,
                                                                        },
                                                                      ],
                                                                    },
                                                                  },
                                                                ],
                                                              },
                                                            },
                                                          ],
                                                        },
                                                      },
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                        },
                                                        data: {
                                                          delta: [
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType: 'attrs',
                                                              },
                                                              data: {
                                                                object: {
                                                                  pr: {},
                                                                },
                                                              },
                                                            },
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType:
                                                                  'txBody',
                                                              },
                                                              data: {
                                                                delta: [
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'bodyPr',
                                                                    },
                                                                    data: {
                                                                      object: {},
                                                                    },
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'listStyle',
                                                                    },
                                                                    data: {
                                                                      object: {
                                                                        listStyle: [
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                        ],
                                                                      },
                                                                    },
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'docContent',
                                                                    },
                                                                    data: {
                                                                      delta: [
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            lang:
                                                                              'en-US',
                                                                            altLang:
                                                                              'zh-CN',
                                                                            dirty: false,
                                                                          },
                                                                          data:
                                                                            'info',
                                                                        },
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            rPr:
                                                                              '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                                                            paraEnd: true,
                                                                          },
                                                                          data: 1,
                                                                        },
                                                                      ],
                                                                    },
                                                                  },
                                                                ],
                                                              },
                                                            },
                                                          ],
                                                        },
                                                      },
                                                    ],
                                                  },
                                                },
                                              ],
                                            },
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { type: 'row' },
                                            data: {
                                              delta: [
                                                {
                                                  action: 'insert',
                                                  attributes: {
                                                    opType: 'attrs',
                                                  },
                                                  data: {
                                                    object: { height: 370840 },
                                                  },
                                                },
                                                {
                                                  action: 'insert',
                                                  attributes: {
                                                    opType: 'cells',
                                                  },
                                                  data: {
                                                    delta: [
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                        },
                                                        data: {
                                                          delta: [
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType: 'attrs',
                                                              },
                                                              data: {
                                                                object: {
                                                                  pr: {},
                                                                },
                                                              },
                                                            },
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType:
                                                                  'txBody',
                                                              },
                                                              data: {
                                                                delta: [
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'bodyPr',
                                                                    },
                                                                    data: {
                                                                      object: {},
                                                                    },
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'listStyle',
                                                                    },
                                                                    data: {
                                                                      object: {
                                                                        listStyle: [
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                        ],
                                                                      },
                                                                    },
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'docContent',
                                                                    },
                                                                    data: {
                                                                      delta: [
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            lang:
                                                                              'en-US',
                                                                            altLang:
                                                                              'zh-CN',
                                                                            dirty: false,
                                                                            err: true,
                                                                          },
                                                                          data:
                                                                            'pedro',
                                                                        },
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            rPr:
                                                                              '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                                                            paraEnd: true,
                                                                          },
                                                                          data: 1,
                                                                        },
                                                                      ],
                                                                    },
                                                                  },
                                                                ],
                                                              },
                                                            },
                                                          ],
                                                        },
                                                      },
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                        },
                                                        data: {
                                                          delta: [
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType: 'attrs',
                                                              },
                                                              data: {
                                                                object: {
                                                                  pr: {},
                                                                },
                                                              },
                                                            },
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType:
                                                                  'txBody',
                                                              },
                                                              data: {
                                                                delta: [
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'bodyPr',
                                                                    },
                                                                    data: {
                                                                      object: {},
                                                                    },
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'listStyle',
                                                                    },
                                                                    data: {
                                                                      object: {
                                                                        listStyle: [
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                        ],
                                                                      },
                                                                    },
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'docContent',
                                                                    },
                                                                    data: {
                                                                      delta: [
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            lang:
                                                                              'en-US',
                                                                            altLang:
                                                                              'zh-CN',
                                                                            dirty: false,
                                                                          },
                                                                          data:
                                                                            'man',
                                                                        },
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            rPr:
                                                                              '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                                                            paraEnd: true,
                                                                          },
                                                                          data: 1,
                                                                        },
                                                                      ],
                                                                    },
                                                                  },
                                                                ],
                                                              },
                                                            },
                                                          ],
                                                        },
                                                      },
                                                      {
                                                        action: 'insert',
                                                        attributes: {
                                                          type: 'cell',
                                                        },
                                                        data: {
                                                          delta: [
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType: 'attrs',
                                                              },
                                                              data: {
                                                                object: {
                                                                  pr: {},
                                                                },
                                                              },
                                                            },
                                                            {
                                                              action: 'insert',
                                                              attributes: {
                                                                opType:
                                                                  'txBody',
                                                              },
                                                              data: {
                                                                delta: [
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'bodyPr',
                                                                    },
                                                                    data: {
                                                                      object: {},
                                                                    },
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'listStyle',
                                                                    },
                                                                    data: {
                                                                      object: {
                                                                        listStyle: [
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                          null,
                                                                        ],
                                                                      },
                                                                    },
                                                                  },
                                                                  {
                                                                    action:
                                                                      'insert',
                                                                    attributes: {
                                                                      opType:
                                                                        'docContent',
                                                                    },
                                                                    data: {
                                                                      delta: [
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            lang:
                                                                              'en-US',
                                                                            altLang:
                                                                              'zh-CN',
                                                                            dirty: false,
                                                                            err: true,
                                                                          },
                                                                          data:
                                                                            'guid',
                                                                        },
                                                                        {
                                                                          action:
                                                                            'insert',
                                                                          attributes: {
                                                                            rPr:
                                                                              '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                                                            paraEnd: true,
                                                                          },
                                                                          data: 1,
                                                                        },
                                                                      ],
                                                                    },
                                                                  },
                                                                ],
                                                              },
                                                            },
                                                          ],
                                                        },
                                                      },
                                                    ],
                                                  },
                                                },
                                              ],
                                            },
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
]
