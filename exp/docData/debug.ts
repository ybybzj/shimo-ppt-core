export const modocStr = [
  {
    action: 'insert',
    attributes: { opType: 'presentation' },
    data: {
      delta: [
        {
          action: 'insert',
          attributes: { opType: 'Pres' },
          data: {
            object: {
              pres: {
                attrSaveSubsetFonts: true,
                sldSz: { cx: 12192000, cy: 6858000 },
                notesSz: { cx: 6858000, cy: 12192000 },
              },
            },
          },
        },
        {
          action: 'insert',
          attributes: { opType: 'PresentationPr' },
          data: { object: { showPr: { present: false } } },
        },
        {
          action: 'insert',
          attributes: { opType: 'TableStyle' },
          data: { object: {} },
        },
        {
          action: 'insert',
          attributes: { opType: 'Slides' },
          data: { object: { s0000003: { order: 0 } } },
        },
        {
          action: 'insert',
          attributes: { opType: 'SlideMaster' },
          data: { object: { slideMasterRefs: ['sm000000'] } },
        },
        {
          action: 'insert',
          attributes: { opType: 'Notes' },
          data: { object: { noteRefs: [] } },
        },
        {
          action: 'insert',
          attributes: { opType: 'NotesMaster' },
          data: { object: { noteMasterRefs: [] } },
        },
      ],
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'layouts' },
    data: {
      slide: {
        l0000000: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                matchingName: '',
                preserve: true,
                showMasterPhAnim: false,
                userDrawn: true,
                type: 'title',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Title Slide' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000036: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: 'Title 1',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          type: 'ctrTitle',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 1524000,
                                        offY: 1122363,
                                        extX: 9144000,
                                        extY: 2387600,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 6000 },
                                            },
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000037: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: 'Subtitle 2',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '1',
                                          type: 'subTitle',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 1524000,
                                        offY: 3602038,
                                        extX: 9144000,
                                        extY: 1655762,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2400 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1800 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master subtitle style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000038: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: 'Date Placeholder 3',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '10',
                                          sz: 'half',
                                          type: 'dt',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000039: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: 'Footer Placeholder 4',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '11',
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000040: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 8,
                                        name: 'Slide Number Placeholder 5',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '12',
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000001: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                matchingName: '',
                preserve: true,
                showMasterPhAnim: false,
                userDrawn: true,
                type: 'obj',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Title and Content' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000031: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: 'Title 1',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000032: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: 'Content Placeholder 2',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '1',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000033: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: 'Date Placeholder 3',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '10',
                                          sz: 'half',
                                          type: 'dt',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000034: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: 'Footer Placeholder 4',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '11',
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000035: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 8,
                                        name: 'Slide Number Placeholder 5',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '12',
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000002: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                matchingName: '',
                preserve: true,
                showMasterPhAnim: false,
                userDrawn: true,
                type: 'secHead',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Section Header' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000021: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: 'Title 1',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 831850,
                                        offY: 1709738,
                                        extX: 10515600,
                                        extY: 2852737,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: { fontSize: 6000 },
                                            },
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000022: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: 'Text Placeholder 2',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '1',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 831850,
                                        offY: 4589463,
                                        extX: 10515600,
                                        extY: 1500187,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2400,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [
                                                          { name: 'tint' },
                                                        ],
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 2000,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [
                                                          { name: 'tint' },
                                                        ],
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1800,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [
                                                          { name: 'tint' },
                                                        ],
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [
                                                          { name: 'tint' },
                                                        ],
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [
                                                          { name: 'tint' },
                                                        ],
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [
                                                          { name: 'tint' },
                                                        ],
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [
                                                          { name: 'tint' },
                                                        ],
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [
                                                          { name: 'tint' },
                                                        ],
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: {
                                                fontSize: 1600,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [
                                                          { name: 'tint' },
                                                        ],
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000023: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: 'Date Placeholder 3',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '10',
                                          sz: 'half',
                                          type: 'dt',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000024: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: 'Footer Placeholder 4',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '11',
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000025: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 8,
                                        name: 'Slide Number Placeholder 5',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '12',
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000003: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                matchingName: '',
                preserve: true,
                showMasterPhAnim: false,
                userDrawn: true,
                type: 'twoObj',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Two Content' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000052: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: 'Title 1',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000053: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: 'Content Placeholder 2',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '1',
                                          sz: 'half',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 838200,
                                        offY: 1825625,
                                        extX: 5181600,
                                        extY: 4351338,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000054: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: 'Content Placeholder 3',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '2',
                                          sz: 'half',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 6172200,
                                        offY: 1825625,
                                        extX: 5181600,
                                        extY: 4351338,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000055: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: 'Date Placeholder 4',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '10',
                                          sz: 'half',
                                          type: 'dt',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000056: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 8,
                                        name: 'Footer Placeholder 5',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '11',
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000057: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 9,
                                        name: 'Slide Number Placeholder 6',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '12',
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000004: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                matchingName: '',
                preserve: true,
                showMasterPhAnim: false,
                userDrawn: true,
                type: 'twoTxTwoObj',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Comparison' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000010: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: 'Title 1',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 839788,
                                        offY: 365125,
                                        extX: 10515600,
                                        extY: 1325563,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000011: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: 'Text Placeholder 2',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '1',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 839788,
                                        offY: 1681163,
                                        extX: 5157787,
                                        extY: 823912,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 2400,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 2000,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1800,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1600,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1600,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1600,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1600,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1600,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1600,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000012: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: 'Content Placeholder 3',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '2',
                                          sz: 'half',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 839788,
                                        offY: 2505074,
                                        extX: 5157787,
                                        extY: 3684588,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000013: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: 'Text Placeholder 4',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '3',
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 6172200,
                                        offY: 1681163,
                                        extX: 5183188,
                                        extY: 823912,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 2400,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 2000,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1800,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1600,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1600,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1600,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1600,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1600,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: {
                                                bold: true,
                                                fontSize: 1600,
                                              },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000014: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 8,
                                        name: 'Content Placeholder 5',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '4',
                                          sz: 'quarter',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 6172200,
                                        offY: 2505074,
                                        extX: 5183188,
                                        extY: 3684588,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000015: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 9,
                                        name: 'Date Placeholder 6',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '10',
                                          sz: 'half',
                                          type: 'dt',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000016: [
                        {
                          action: 'insert',
                          attributes: { order: 6, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 10,
                                        name: 'Footer Placeholder 7',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '11',
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000017: [
                        {
                          action: 'insert',
                          attributes: { order: 7, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 11,
                                        name: 'Slide Number Placeholder 8',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '12',
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000005: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                matchingName: '',
                preserve: true,
                showMasterPhAnim: false,
                userDrawn: true,
                type: 'titleOnly',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Title Only' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000006: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: 'Title 1',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000007: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: 'Date Placeholder 2',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '10',
                                          sz: 'half',
                                          type: 'dt',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000008: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: 'Footer Placeholder 3',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '11',
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000009: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: 'Slide Number Placeholder 4',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '12',
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000006: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                matchingName: '',
                preserve: true,
                showMasterPhAnim: false,
                userDrawn: true,
                type: 'blank',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Blank' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000018: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: 'Date Placeholder 1',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '10',
                                          sz: 'half',
                                          type: 'dt',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000019: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: 'Footer Placeholder 2',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '11',
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000020: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: 'Slide Number Placeholder 3',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '12',
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000007: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                matchingName: '',
                preserve: true,
                showMasterPhAnim: false,
                userDrawn: true,
                type: 'objTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Content with Caption' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000041: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: 'Title 1',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 839788,
                                        offY: 457200,
                                        extX: 3932237,
                                        extY: 1600200,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: { fontSize: 3200 },
                                            },
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000042: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: 'Content Placeholder 2',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '1',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 5183188,
                                        offY: 987425,
                                        extX: 6172200,
                                        extY: 4873625,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: { fontSize: 3200 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2800 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2400 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            {
                                              defaultRunPr: { fontSize: 2000 },
                                            },
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000043: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: 'Text Placeholder 3',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '2',
                                          sz: 'half',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 839788,
                                        offY: 2057400,
                                        extX: 3932237,
                                        extY: 3811588,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1400 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1200 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000044: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: 'Date Placeholder 4',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '10',
                                          sz: 'half',
                                          type: 'dt',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000045: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 8,
                                        name: 'Footer Placeholder 5',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '11',
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000046: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 9,
                                        name: 'Slide Number Placeholder 6',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '12',
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000008: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                matchingName: '',
                preserve: true,
                showMasterPhAnim: false,
                userDrawn: true,
                type: 'picTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Picture with Caption' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000000: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: 'Title 1',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 839788,
                                        offY: 457200,
                                        extX: 3932237,
                                        extY: 1600200,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: { fontSize: 3200 },
                                            },
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000001: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: 'Picture Placeholder 2',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '1',
                                          type: 'pic',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noChangeAspect', 'noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 5183188,
                                        offY: 987425,
                                        extX: 6172200,
                                        extY: 4873625,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 't' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 3200 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2800 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2400 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Click icon to add picture',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000002: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: 'Text Placeholder 3',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '2',
                                          sz: 'half',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 839788,
                                        offY: 2057400,
                                        extX: 3932237,
                                        extY: 3811588,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1400 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1200 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              defaultRunPr: { fontSize: 1000 },
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                  char: '•',
                                                  autoNumType: 'alphaLcParenR',
                                                  startAt: 1,
                                                },
                                              },
                                            },
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000003: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: 'Date Placeholder 4',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '10',
                                          sz: 'half',
                                          type: 'dt',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000004: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 8,
                                        name: 'Footer Placeholder 5',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '11',
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000005: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 9,
                                        name: 'Slide Number Placeholder 6',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '12',
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000009: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                matchingName: '',
                preserve: true,
                showMasterPhAnim: false,
                userDrawn: true,
                type: 'vertTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Title and Vertical Text' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000047: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: 'Title 1',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000048: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: 'Vertical Text Placeholder 2',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '1',
                                          orient: 'vert',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { vert: 'eaVert' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000049: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: 'Date Placeholder 3',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '10',
                                          sz: 'half',
                                          type: 'dt',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000050: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: 'Footer Placeholder 4',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '11',
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000051: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 8,
                                        name: 'Slide Number Placeholder 5',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '12',
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000010: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                matchingName: '',
                preserve: true,
                showMasterPhAnim: false,
                userDrawn: true,
                type: 'vertTitleAndTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Vertical Title and Text' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000026: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: 'Vertical Title 1',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          orient: 'vert',
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 8724900,
                                        offY: 365125,
                                        extX: 2628900,
                                        extY: 5811838,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { vert: 'eaVert' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000027: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: 'Vertical Text Placeholder 2',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '1',
                                          orient: 'vert',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 838200,
                                        offY: 365125,
                                        extX: 7734300,
                                        extY: 5811838,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { vert: 'eaVert' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000028: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: 'Date Placeholder 3',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '10',
                                          sz: 'half',
                                          type: 'dt',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000029: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: 'Footer Placeholder 4',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '11',
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000030: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 8,
                                        name: 'Slide Number Placeholder 5',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '12',
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: { bwMode: 'auto' } } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'slideMasters' },
    data: {
      slide: {
        sm000000: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: false,
                clrMap: {
                  accent1: 'accent1',
                  accent2: 'accent2',
                  accent3: 'accent3',
                  accent4: 'accent4',
                  accent5: 'accent5',
                  accent6: 'accent6',
                  bg1: 'lt1',
                  bg2: 'lt2',
                  folHlink: 'folHlink',
                  hlink: 'hlink',
                  tx1: 'dk1',
                  tx2: 'dk2',
                },
                txStyles: {
                  bodyStyle: [
                    {
                      algn: 'l',
                      marL: 228600,
                      indent: -228600,
                      lineSpacing: { type: 'percent', value: 90000 },
                      spaceBefore: { type: 'point', value: 1000 },
                      defaultRunPr: {
                        fontSize: 2800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                        bulletType: {
                          type: 'character',
                          char: '•',
                          autoNumType: 'alphaLcParenR',
                          startAt: 1,
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 685800,
                      indent: -228600,
                      lineSpacing: { type: 'percent', value: 90000 },
                      spaceBefore: { type: 'point', value: 500 },
                      defaultRunPr: {
                        fontSize: 2400,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                        bulletType: {
                          type: 'character',
                          char: '•',
                          autoNumType: 'alphaLcParenR',
                          startAt: 1,
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 1143000,
                      indent: -228600,
                      lineSpacing: { type: 'percent', value: 90000 },
                      spaceBefore: { type: 'point', value: 500 },
                      defaultRunPr: {
                        fontSize: 2000,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                        bulletType: {
                          type: 'character',
                          char: '•',
                          autoNumType: 'alphaLcParenR',
                          startAt: 1,
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 1600200,
                      indent: -228600,
                      lineSpacing: { type: 'percent', value: 90000 },
                      spaceBefore: { type: 'point', value: 500 },
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                        bulletType: {
                          type: 'character',
                          char: '•',
                          autoNumType: 'alphaLcParenR',
                          startAt: 1,
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 2057400,
                      indent: -228600,
                      lineSpacing: { type: 'percent', value: 90000 },
                      spaceBefore: { type: 'point', value: 500 },
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                        bulletType: {
                          type: 'character',
                          char: '•',
                          autoNumType: 'alphaLcParenR',
                          startAt: 1,
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 2514600,
                      indent: -228600,
                      lineSpacing: { type: 'percent', value: 90000 },
                      spaceBefore: { type: 'point', value: 500 },
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                        bulletType: {
                          type: 'character',
                          char: '•',
                          autoNumType: 'alphaLcParenR',
                          startAt: 1,
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 2971800,
                      indent: -228600,
                      lineSpacing: { type: 'percent', value: 90000 },
                      spaceBefore: { type: 'point', value: 500 },
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                        bulletType: {
                          type: 'character',
                          char: '•',
                          autoNumType: 'alphaLcParenR',
                          startAt: 1,
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 3429000,
                      indent: -228600,
                      lineSpacing: { type: 'percent', value: 90000 },
                      spaceBefore: { type: 'point', value: 500 },
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                        bulletType: {
                          type: 'character',
                          char: '•',
                          autoNumType: 'alphaLcParenR',
                          startAt: 1,
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 3886200,
                      indent: -228600,
                      lineSpacing: { type: 'percent', value: 90000 },
                      spaceBefore: { type: 'point', value: 500 },
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                        bulletType: {
                          type: 'character',
                          char: '•',
                          autoNumType: 'alphaLcParenR',
                          startAt: 1,
                        },
                      },
                    },
                    '#nil#',
                  ],
                  titleStyle: [
                    {
                      algn: 'l',
                      lineSpacing: { type: 'percent', value: 90000 },
                      spaceBefore: { type: 'point', value: 0 },
                      defaultRunPr: {
                        fontSize: 4400,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: {
                          type: 'none',
                          char: '•',
                          autoNumType: 'alphaLcParenR',
                          startAt: 1,
                        },
                      },
                    },
                    '#nil#',
                    '#nil#',
                    '#nil#',
                    '#nil#',
                    '#nil#',
                    '#nil#',
                    '#nil#',
                    '#nil#',
                    '#nil#',
                  ],
                  otherStyle: [
                    {
                      algn: 'l',
                      marL: 0,
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 457200,
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 914400,
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 1371600,
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 1828800,
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 2286000,
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 2743200,
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 3200400,
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      algn: 'l',
                      marL: 3657600,
                      defaultRunPr: {
                        fontSize: 1800,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'tx1' },
                                mods: [],
                              },
                            },
                          },
                        },
                      },
                    },
                    { defaultRunPr: { lang: 'en-US' } },
                  ],
                },
                themeRef: 'th000000',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'layoutRefs' },
            data: {
              object: {
                layoutRefs: [
                  'l0000000',
                  'l0000001',
                  'l0000002',
                  'l0000003',
                  'l0000004',
                  'l0000005',
                  'l0000006',
                  'l0000007',
                  'l0000008',
                  'l0000009',
                  'l0000010',
                ],
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: {
                    object: {
                      name: '',
                      bg: {
                        bgRef: {
                          idx: 1001,
                          color: { color: { type: 3, val: 'bg1' }, mods: [] },
                        },
                      },
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000058: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: 'Title Placeholder 1',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 838200,
                                        offY: 365125,
                                        extX: 10515600,
                                        extY: 1325563,
                                      },
                                      geometry: { preset: 'rect', avLst: {} },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          tIns: 45720,
                                          rtlCol: false,
                                          vert: 'horz',
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000059: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: 'Text Placeholder 2',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '1',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 838200,
                                        offY: 1825625,
                                        extX: 10515600,
                                        extY: 4351338,
                                      },
                                      geometry: { preset: 'rect', avLst: {} },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          tIns: 45720,
                                          rtlCol: false,
                                          vert: 'horz',
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: { lang: 'en-US' },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000060: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: 'Date Placeholder 3',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '2',
                                          sz: 'half',
                                          type: 'dt',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 838200,
                                        offY: 6356350,
                                        extX: 2743200,
                                        extY: 365125,
                                      },
                                      geometry: { preset: 'rect', avLst: {} },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          tIns: 45720,
                                          rtlCol: false,
                                          vert: 'horz',
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'l',
                                              defaultRunPr: {
                                                fontSize: 1200,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [
                                                          { name: 'tint' },
                                                        ],
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000061: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: 'Footer Placeholder 4',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '3',
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 4038600,
                                        offY: 6356350,
                                        extX: 4114800,
                                        extY: 365125,
                                      },
                                      geometry: { preset: 'rect', avLst: {} },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          tIns: 45720,
                                          rtlCol: false,
                                          vert: 'horz',
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: {
                                                fontSize: 1200,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [
                                                          { name: 'tint' },
                                                        ],
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000062: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 8,
                                        name: 'Slide Number Placeholder 5',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                        ph: {
                                          hasCustomPrompt: false,
                                          idx: '4',
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 8610600,
                                        offY: 6356350,
                                        extX: 2743200,
                                        extY: 365125,
                                      },
                                      geometry: { preset: 'rect', avLst: {} },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          tIns: 45720,
                                          rtlCol: false,
                                          vert: 'horz',
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'r',
                                              defaultRunPr: {
                                                fontSize: 1200,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [
                                                          { name: 'tint' },
                                                        ],
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                            '#nil#',
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'slides' },
    data: {
      slide: {
        s0000003: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                show: true,
                showMasterPhAnim: false,
                transition: { advClick: true },
                layoutRef: 'l0000006',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000063: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 1,
                                        name: '',
                                        isHidden: false,
                                      },
                                      nvPr: {
                                        isPhoto: false,
                                        userDrawn: false,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      xfrm: {
                                        offX: 3590238,
                                        offY: 922261,
                                        extX: 1920191,
                                        extY: 354343,
                                        flipH: false,
                                        flipV: false,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                      fill: { fill: { type: 2 } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 't',
                                          anchorCtr: false,
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          tIns: 45720,
                                          forceAA: false,
                                          fromWordArt: false,
                                          numCol: 1,
                                          rtlCol: false,
                                          upright: false,
                                          vert: 'horz',
                                          horzOverflow: 'overflow',
                                          vertOverflow: 'overflow',
                                          wrap: 'square',
                                          textFit: { type: 'spAutoFit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: { object: { listStyle: [] } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              fontSize: 4800,
                                              latin:
                                                '{"typeface":"KaiTi_GB2312"}',
                                              ea: '{"typeface":"KaiTi_GB2312"}',
                                              cs: '{"typeface":"KaiTi_GB2312"}',
                                            },
                                            data: '我',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"cs":{"typeface":"KaiTi_GB2312"},"ea":{"typeface":"KaiTi_GB2312"},"fontSize":4800,"latin":{"typeface":"KaiTi_GB2312"}}',
                                              textPr:
                                                '{"cs":{"typeface":"KaiTi_GB2312"},"ea":{"typeface":"KaiTi_GB2312"},"fontSize":4800,"latin":{"typeface":"KaiTi_GB2312"}}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              fontSize: 4800,
                                              latin:
                                                '{"typeface":"FZXiaoBiaoSongGBK"}',
                                              ea:
                                                '{"typeface":"FZXiaoBiaoSongGBK"}',
                                              cs:
                                                '{"typeface":"FZXiaoBiaoSongGBK"}',
                                            },
                                            data: '你',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"cs":{"typeface":"FZXiaoBiaoSongGBK"},"ea":{"typeface":"FZXiaoBiaoSongGBK"},"fontSize":4800,"latin":{"typeface":"FZXiaoBiaoSongGBK"}}',
                                              textPr:
                                                '{"cs":{"typeface":"FZXiaoBiaoSongGBK"},"ea":{"typeface":"FZXiaoBiaoSongGBK"},"fontSize":4800,"latin":{"typeface":"FZXiaoBiaoSongGBK"}}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'themes' },
    data: {
      slide: {
        th000000: [
          {
            action: 'insert',
            data: {
              object: {
                name: 'SM_Theme_D7EA2X',
                themeElements: {
                  clrScheme: {
                    accent1: { color: { type: 1, val: 'E69760' } },
                    accent2: { color: { type: 1, val: 'D27168' } },
                    accent3: { color: { type: 1, val: '799DDE' } },
                    accent4: { color: { type: 1, val: 'EEC869' } },
                    accent5: { color: { type: 1, val: '92C0C7' } },
                    accent6: { color: { type: 1, val: 'C8C687' } },
                    dk1: { color: { type: 1, val: '2B3033' } },
                    dk2: { color: { type: 1, val: '83878C' } },
                    folHlink: { color: { type: 1, val: '7A66CC' } },
                    hlink: { color: { type: 1, val: '5BA0E7' } },
                    lt1: { color: { type: 1, val: 'FFFFFF' } },
                    lt2: { color: { type: 1, val: 'ECEDED' } },
                    name: '',
                  },
                  fontScheme: {
                    name: '',
                    majorFont: { latin: { typeface: 'Arial' } },
                    minorFont: { latin: { typeface: 'Arial' } },
                  },
                  fmtScheme: {
                    name: '',
                    fillStyleLst: [
                      {
                        fill: {
                          type: 3,
                          data: { color: { color: { type: 3, val: 'phClr' } } },
                        },
                      },
                      {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              color: { color: { type: 1, val: '000000' } },
                            },
                          },
                        },
                      },
                      {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              color: { color: { type: 1, val: '000000' } },
                            },
                          },
                        },
                      },
                    ],
                    lnStyleLst: [
                      {
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                color: { type: 3, val: 'phClr' },
                                mods: [
                                  { name: 'shade', val: 95000 },
                                  { name: 'satMod', val: 105000 },
                                ],
                              },
                            },
                          },
                        },
                        w: 9525,
                      },
                      {
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: { color: { type: 3, val: 'phClr' } },
                            },
                          },
                        },
                        w: 25400,
                      },
                      {
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: { color: { type: 3, val: 'phClr' } },
                            },
                          },
                        },
                        w: 38100,
                      },
                    ],
                    bgFillStyleLst: [
                      {
                        fill: {
                          type: 3,
                          data: { color: { color: { type: 3, val: 'phClr' } } },
                        },
                      },
                      {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              color: { color: { type: 1, val: '000000' } },
                            },
                          },
                        },
                      },
                      {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              color: { color: { type: 1, val: '000000' } },
                            },
                          },
                        },
                      },
                    ],
                  },
                },
              },
            },
          },
        ],
      },
    },
  },
]
