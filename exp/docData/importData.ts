export const modocStr = [
  {
    action: 'insert',
    attributes: { opType: 'presentation' },
    data: {
      delta: [
        {
          action: 'insert',
          attributes: { opType: 'Pres' },
          data: {
            object: {
              pres: {
                sldSz: { cx: 12192000, cy: 6858000 },
                notesSz: { cx: 7103745, cy: 10234295 },
              },
            },
          },
        },
        {
          action: 'insert',
          attributes: { opType: 'PresentationPr' },
          data: { object: { showPr: {} } },
        },
        {
          action: 'insert',
          attributes: { opType: 'TableStyle' },
          data: { object: {} },
        },
        {
          action: 'insert',
          attributes: { opType: 'Slides' },
          data: { object: { s0000000: { order: 0 } } },
        },
        {
          action: 'insert',
          attributes: { opType: 'SlideMaster' },
          data: { object: { slideMasterRefs: ['sm000000'] } },
        },
        {
          action: 'insert',
          attributes: { opType: 'Notes' },
          data: { object: {} },
        },
        {
          action: 'insert',
          attributes: { opType: 'NotesMaster' },
          data: { object: { noteMasterRefs: ['nm000000'] } },
        },
      ],
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'layouts' },
    data: {
      slide: {
        l0000000: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'title',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Title Slide' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000030: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'ctrTitle' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 1524000,
                                        offY: 1322962,
                                        extX: 9144000,
                                        extY: 2187001,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'b',
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            algn: 'ctr',
                                            lineSpacing: {
                                              value: 130000,
                                              type: 'percent',
                                            },
                                            defaultRunPr: { fontSize: 6000 },
                                          },
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000031: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{760FBDFE-C587-4B4C-A407-44438C67B59E}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000032: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000033: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'slidenum',
                                              id:
                                                '{49AE70B2-8BF9-45C0-BB95-33D1B9D3A854}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000034: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '副标题 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 1, type: 'subTitle' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 1524000,
                                        offY: 3602038,
                                        extX: 9144000,
                                        extY: 1655762,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            marL: 0,
                                            indent: 0,
                                            algn: 'ctr',
                                            defaultRunPr: {
                                              fontSize: 1800,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 457200,
                                            indent: 0,
                                            algn: 'ctr',
                                            defaultRunPr: { fontSize: 2000 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 914400,
                                            indent: 0,
                                            algn: 'ctr',
                                            defaultRunPr: { fontSize: 1800 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 1371600,
                                            indent: 0,
                                            algn: 'ctr',
                                            defaultRunPr: { fontSize: 1600 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 1828800,
                                            indent: 0,
                                            algn: 'ctr',
                                            defaultRunPr: { fontSize: 1600 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 2286000,
                                            indent: 0,
                                            algn: 'ctr',
                                            defaultRunPr: { fontSize: 1600 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 2743200,
                                            indent: 0,
                                            algn: 'ctr',
                                            defaultRunPr: { fontSize: 1600 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 3200400,
                                            indent: 0,
                                            algn: 'ctr',
                                            defaultRunPr: { fontSize: 1600 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 3657600,
                                            indent: 0,
                                            algn: 'ctr',
                                            defaultRunPr: { fontSize: 1600 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master subtitle style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000001: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'obj',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Title and Content' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000035: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 647700,
                                        offY: 258445,
                                        extX: 10515600,
                                        extY: 1325563,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          anchorCtr: false,
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            defaultRunPr: {
                                              fontSize: 2400,
                                              bold: true,
                                            },
                                          },
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000036: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '内容占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 1 } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 647700,
                                        offY: 1825625,
                                        extX: 10515600,
                                        extY: 4351338,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            defaultRunPr: {
                                              fontSize: 2000,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          {
                                            defaultRunPr: {
                                              fontSize: 1800,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          {
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          {
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          {
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000037: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{760FBDFE-C587-4B4C-A407-44438C67B59E}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000038: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000039: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'slidenum',
                                              id:
                                                '{49AE70B2-8BF9-45C0-BB95-33D1B9D3A854}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000002: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'secHead',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Section Header' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000043: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 831850,
                                        offY: 3750945,
                                        extX: 9848088,
                                        extY: 811530,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'b',
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          { defaultRunPr: { fontSize: 4000 } },
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000044: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '文本占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 1, type: 'body' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 831850,
                                        offY: 4610028,
                                        extX: 7321550,
                                        extY: 647555,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            marL: 0,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1800,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 457200,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 2000,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'tint',
                                                          value: 75000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 914400,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1800,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'tint',
                                                          value: 75000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 1371600,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'tint',
                                                          value: 75000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 1828800,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'tint',
                                                          value: 75000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 2286000,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'tint',
                                                          value: 75000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 2743200,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'tint',
                                                          value: 75000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 3200400,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'tint',
                                                          value: 75000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 3657600,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'tint',
                                                          value: 75000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000045: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{760FBDFE-C587-4B4C-A407-44438C67B59E}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000046: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000047: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'slidenum',
                                              id:
                                                '{49AE70B2-8BF9-45C0-BB95-33D1B9D3A854}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000003: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'twoObj',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Two Content' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000000: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 647700,
                                        offY: 258445,
                                        extX: 10515600,
                                        extY: 1325563,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            defaultRunPr: {
                                              fontSize: 2400,
                                              bold: true,
                                              italic: false,
                                            },
                                          },
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000001: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '内容占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 1, sz: 'half' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 647700,
                                        offY: 1825625,
                                        extX: 5181600,
                                        extY: 4351338,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            lineSpacing: {
                                              value: 150000,
                                              type: 'percent',
                                            },
                                            defaultRunPr: {
                                              fontSize: 2000,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          {
                                            lineSpacing: {
                                              value: 150000,
                                              type: 'percent',
                                            },
                                            defaultRunPr: {
                                              fontSize: 1800,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          {
                                            lineSpacing: {
                                              value: 150000,
                                              type: 'percent',
                                            },
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          {
                                            lineSpacing: {
                                              value: 150000,
                                              type: 'percent',
                                            },
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          {
                                            lineSpacing: {
                                              value: 150000,
                                              type: 'percent',
                                            },
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000002: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '内容占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 2, sz: 'half' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 5981700,
                                        offY: 1825625,
                                        extX: 5181600,
                                        extY: 4351338,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            lineSpacing: {
                                              value: 150000,
                                              type: 'percent',
                                            },
                                            defaultRunPr: {
                                              fontSize: 2000,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          {
                                            lineSpacing: {
                                              value: 150000,
                                              type: 'percent',
                                            },
                                            defaultRunPr: {
                                              fontSize: 1800,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          {
                                            lineSpacing: {
                                              value: 150000,
                                              type: 'percent',
                                            },
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          {
                                            lineSpacing: {
                                              value: 150000,
                                              type: 'percent',
                                            },
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          {
                                            lineSpacing: {
                                              value: 150000,
                                              type: 'percent',
                                            },
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'lumMod',
                                                          value: 75000,
                                                        },
                                                        {
                                                          name: 'lumOff',
                                                          value: 25000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000003: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '日期占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{760FBDFE-C587-4B4C-A407-44438C67B59E}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000004: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 6, name: '页脚占位符 5' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000005: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: '灯片编号占位符 6',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'slidenum',
                                              id:
                                                '{49AE70B2-8BF9-45C0-BB95-33D1B9D3A854}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000004: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'twoTxTwoObj',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Comparison' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000022: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 365125,
                                        extX: 10515600,
                                        extY: 1325563,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000023: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '文本占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 1, type: 'body' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 1744961,
                                        extX: 5157787,
                                        extY: 823912,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            marL: 0,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 2400,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 457200,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 2000,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 914400,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1800,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 1371600,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 1828800,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 2286000,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 2743200,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 3200400,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 3657600,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000024: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '内容占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 2, sz: 'half' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 839788,
                                        offY: 2615609,
                                        extX: 5157787,
                                        extY: 3574054,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000025: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '文本占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 3,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 6172200,
                                        offY: 1744961,
                                        extX: 5183188,
                                        extY: 823912,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            marL: 0,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 2400,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 457200,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 2000,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 914400,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1800,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 1371600,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 1828800,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 2286000,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 2743200,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 3200400,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 3657600,
                                            indent: 0,
                                            defaultRunPr: {
                                              fontSize: 1600,
                                              bold: true,
                                            },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000026: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 6, name: '内容占位符 5' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 4, sz: 'quarter' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 6172200,
                                        offY: 2615609,
                                        extX: 5183188,
                                        extY: 3574054,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000027: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 7, name: '日期占位符 6' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{760FBDFE-C587-4B4C-A407-44438C67B59E}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000028: [
                        {
                          action: 'insert',
                          attributes: { order: 6, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 8, name: '页脚占位符 7' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000029: [
                        {
                          action: 'insert',
                          attributes: { order: 7, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 9,
                                        name: '灯片编号占位符 8',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'slidenum',
                                              id:
                                                '{49AE70B2-8BF9-45C0-BB95-33D1B9D3A854}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000005: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'titleOnly',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Title Only' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000006: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 2766219,
                                        extX: 10515600,
                                        extY: 1325563,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            algn: 'ctr',
                                            defaultRunPr: {
                                              fontSize: 4800,
                                              bold: false,
                                            },
                                          },
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000007: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '日期占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{760FBDFE-C587-4B4C-A407-44438C67B59E}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000008: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '页脚占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000009: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: '灯片编号占位符 4',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'slidenum',
                                              id:
                                                '{49AE70B2-8BF9-45C0-BB95-33D1B9D3A854}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000006: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'blank',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Blank' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000040: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '日期占位符 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{760FBDFE-C587-4B4C-A407-44438C67B59E}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000041: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '页脚占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000042: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: '灯片编号占位符 3',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'slidenum',
                                              id:
                                                '{49AE70B2-8BF9-45C0-BB95-33D1B9D3A854}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000007: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'picTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Picture with Caption' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000010: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 646747,
                                        offY: 127000,
                                        extX: 4165200,
                                        extY: 1600200,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          anchorCtr: false,
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            defaultRunPr: {
                                              fontSize: 2400,
                                              bold: true,
                                            },
                                          },
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000011: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '图片占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 1, type: 'pic' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 5184000,
                                        offY: 766354,
                                        extX: 5817375,
                                        extY: 5094446,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            marL: 0,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 3200 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 457200,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 2800 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 914400,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 2400 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 1371600,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 2000 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 1828800,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 2000 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 2286000,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 2000 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 2743200,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 2000 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 3200400,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 2000 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 3657600,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 2000 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000012: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '文本占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 2,
                                          sz: 'half',
                                          type: 'body',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 651827,
                                        offY: 2057400,
                                        extX: 4165200,
                                        extY: 3811588,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            marL: 0,
                                            indent: 0,
                                            lineSpacing: {
                                              value: 150000,
                                              type: 'percent',
                                            },
                                            defaultRunPr: { fontSize: 1600 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 457200,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 1400 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 914400,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 1200 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 1371600,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 1000 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 1828800,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 1000 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 2286000,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 1000 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 2743200,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 1000 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 3200400,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 1000 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          {
                                            marL: 3657600,
                                            indent: 0,
                                            defaultRunPr: { fontSize: 1000 },
                                            bullet: {
                                              bulletType: { type: 'none' },
                                            },
                                          },
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000013: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '日期占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{9EFD9D74-47D9-4702-A33C-335B63B48DBF}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000014: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 6, name: '页脚占位符 5' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000015: [
                        {
                          action: 'insert',
                          attributes: { order: 5, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 7,
                                        name: '灯片编号占位符 6',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'slidenum',
                                              id:
                                                '{FABC47A4-756D-490B-A52F-7D9E2C9FC05F}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000008: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'vertTitleAndTx',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Vertical Title and Text' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000017: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '竖排标题 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { orient: 'vert', type: 'title' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 9824484,
                                        offY: 365125,
                                        extX: 1529316,
                                        extY: 5811838,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          vert: 'eaVert',
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          { defaultRunPr: { fontSize: 3600 } },
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000018: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 3,
                                        name: '竖排文字占位符 2',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 1,
                                          orient: 'vert',
                                          type: 'body',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 365125,
                                        extX: 8879958,
                                        extY: 5811838,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { vert: 'eaVert' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000019: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{760FBDFE-C587-4B4C-A407-44438C67B59E}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000020: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000021: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'slidenum',
                                              id:
                                                '{49AE70B2-8BF9-45C0-BB95-33D1B9D3A854}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000009: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                userDrawn: true,
                clrMap: { type: 'useMaster' },
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: 'Content' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000048: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '日期占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{760FBDFE-C587-4B4C-A407-44438C67B59E}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000049: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '页脚占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000050: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: '灯片编号占位符 4',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'slidenum',
                                              id:
                                                '{49AE70B2-8BF9-45C0-BB95-33D1B9D3A854}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000051: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 7, name: '内容占位符 6' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 13, sz: 'quarter' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 551543,
                                        extX: 10515600,
                                        extY: 5558971,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'slideMasters' },
    data: {
      slide: {
        sm000000: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                clrMap: {
                  bg1: 'lt1',
                  tx1: 'dk1',
                  bg2: 'lt2',
                  tx2: 'dk2',
                  accent1: 'accent1',
                  accent2: 'accent2',
                  accent3: 'accent3',
                  accent4: 'accent4',
                  accent5: 'accent5',
                  accent6: 'accent6',
                  hlink: 'hlink',
                  folHlink: 'folHlink',
                },
                txStyles: {
                  titleStyle: [
                    {
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 0, type: 'percent' },
                      defaultRunPr: {
                        fontSize: 4000,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: { bulletType: { type: 'none' } },
                    },
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                  ],
                  bodyStyle: [
                    {
                      marL: 228600,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 1000, type: 'point' },
                      defaultRunPr: {
                        fontSize: 2400,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 685800,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 2000,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 1143000,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 1600200,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 2057400,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 2514600,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 2971800,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 3429000,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 3886200,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                      bullet: {
                        bulletType: { type: 'character' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    null,
                  ],
                  otherStyle: [
                    {
                      marL: 0,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 457200,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 914400,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 1371600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 1828800,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 2286000,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 2743200,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 3200400,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    {
                      marL: 3657600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                      },
                    },
                    { defaultRunPr: { lang: 'zh-CN' } },
                  ],
                },
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'layoutRefs' },
            data: {
              object: {
                layoutRefs: [
                  'l0000008',
                  'l0000007',
                  'l0000006',
                  'l0000005',
                  'l0000004',
                  'l0000003',
                  'l0000002',
                  'l0000001',
                  'l0000009',
                  'l0000000',
                ],
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: {
                    object: {
                      bg: {
                        bgPr: {
                          fill: {
                            fill: {
                              type: 3,
                              data: {
                                color: {
                                  mods: [],
                                  color: { type: 3, val: 'bg1' },
                                },
                              },
                            },
                          },
                        },
                      },
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000052: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题占位符 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 365125,
                                        extX: 10515600,
                                        extY: 1325563,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master title style',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000053: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '文本占位符 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { idx: 1, type: 'body' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 1825625,
                                        extX: 10515600,
                                        extY: 4351338,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data:
                                              'Click to edit Master text styles',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Second level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Third level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fourth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                              dirty: false,
                                            },
                                            data: 'Fifth level',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","dirty":false}',
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000054: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 2, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 6356350,
                                        extX: 2743200,
                                        extY: 365125,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            algn: 'l',
                                            defaultRunPr: {
                                              fontSize: 1200,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'tint',
                                                          value: 75000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{760FBDFE-C587-4B4C-A407-44438C67B59E}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000055: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 3,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 4038600,
                                        offY: 6356350,
                                        extX: 4114800,
                                        extY: 365125,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            algn: 'ctr',
                                            defaultRunPr: {
                                              fontSize: 1200,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'tint',
                                                          value: 75000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000056: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: {
                                          idx: 4,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 8610600,
                                        offY: 6356350,
                                        extX: 2743200,
                                        extY: 365125,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          vert: 'horz',
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: [
                                          {
                                            algn: 'r',
                                            defaultRunPr: {
                                              fontSize: 1200,
                                              fill: {
                                                fill: {
                                                  type: 3,
                                                  data: {
                                                    color: {
                                                      mods: [
                                                        {
                                                          name: 'tint',
                                                          value: 75000,
                                                        },
                                                      ],
                                                      color: {
                                                        type: 3,
                                                        val: 'tx1',
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                          },
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                          null,
                                        ],
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'slidenum',
                                              id:
                                                '{49AE70B2-8BF9-45C0-BB95-33D1B9D3A854}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'slides' },
    data: {
      slide: {
        s0000000: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: { clrMap: { type: 'useMaster' }, layoutRef: 'l0000000' },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: {} },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000057: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: 'Title 1' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: { ph: { type: 'ctrTitle' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              bold: true,
                                              lang: '',
                                              altLang: 'en-US',
                                              fill:
                                                '{"fill":{"type":3,"data":{"color":{"mods":[{"name":"lumMod","value":60000},{"name":"lumOff","value":40000}],"color":{"type":3,"val":"accent1"}}}}}',
                                            },
                                            data: 'a',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              bold: true,
                                              lang: '',
                                              altLang: 'en-US',
                                            },
                                            data: 'as',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              bold: true,
                                              lang: '',
                                              altLang: 'en-US',
                                              italic: true,
                                            },
                                            data: 'd',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              lang: '',
                                              altLang: 'en-US',
                                              underline: 'sng',
                                            },
                                            data: 'c',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              underline: 'sng',
                                              lang: 'zh-CN',
                                              altLang: '',
                                              fontSize: 4400,
                                            },
                                            data: '自行车',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              underline: 'sng',
                                              lang: 'zh-CN',
                                              altLang: '',
                                              fontSize: 4400,
                                              break: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              underline: 'sng',
                                              lang: 'zh-CN',
                                              altLang: '',
                                              fontSize: 4400,
                                            },
                                            data: '是',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"marL":0,"indent":0}',
                                              rPr:
                                                '{"lang":"zh-CN","altLang":"","fontSize":4400,"underline":"sng"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000058: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: 'Subtitle 2' },
                                      nvUniSpPr: { locks: 1 },
                                      nvPr: {
                                        ph: { idx: 1, type: 'subTitle' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr: '{"lang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
]
