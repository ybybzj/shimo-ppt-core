export const modocStr = [
  {
    action: 'insert',
    attributes: {
      opType: 'presentation',
    },
    data: {
      delta: [
        {
          action: 'insert',
          attributes: {
            opType: 'Pres',
          },
          data: {
            object: {
              pres: {
                attrAutoCompressPictures: false,
                attrSaveSubsetFonts: true,
                defaultTextStyle: [
                  {
                    algn: 'l',
                    defTabSz: 914400,
                    defaultRunPr: {
                      cs: {
                        typeface: '+mn-cs',
                      },
                      ea: {
                        typeface: '+mn-ea',
                      },
                      fill: {
                        fill: {
                          data: {
                            color: {
                              color: {
                                type: 3,
                                val: 'tx1',
                              },
                              mods: [],
                            },
                          },
                          type: 3,
                        },
                      },
                      fontSize: 1800,
                      kern: 1200,
                      latin: {
                        typeface: '+mn-lt',
                      },
                    },
                    eaLnBrk: true,
                    hangingPunct: true,
                    latinLnBrk: false,
                    marL: 0,
                    rtl: false,
                  },
                  {
                    algn: 'l',
                    defTabSz: 914400,
                    defaultRunPr: {
                      cs: {
                        typeface: '+mn-cs',
                      },
                      ea: {
                        typeface: '+mn-ea',
                      },
                      fill: {
                        fill: {
                          data: {
                            color: {
                              color: {
                                type: 3,
                                val: 'tx1',
                              },
                              mods: [],
                            },
                          },
                          type: 3,
                        },
                      },
                      fontSize: 1800,
                      kern: 1200,
                      latin: {
                        typeface: '+mn-lt',
                      },
                    },
                    eaLnBrk: true,
                    hangingPunct: true,
                    latinLnBrk: false,
                    marL: 457200,
                    rtl: false,
                  },
                  {
                    algn: 'l',
                    defTabSz: 914400,
                    defaultRunPr: {
                      cs: {
                        typeface: '+mn-cs',
                      },
                      ea: {
                        typeface: '+mn-ea',
                      },
                      fill: {
                        fill: {
                          data: {
                            color: {
                              color: {
                                type: 3,
                                val: 'tx1',
                              },
                              mods: [],
                            },
                          },
                          type: 3,
                        },
                      },
                      fontSize: 1800,
                      kern: 1200,
                      latin: {
                        typeface: '+mn-lt',
                      },
                    },
                    eaLnBrk: true,
                    hangingPunct: true,
                    latinLnBrk: false,
                    marL: 914400,
                    rtl: false,
                  },
                  {
                    algn: 'l',
                    defTabSz: 914400,
                    defaultRunPr: {
                      cs: {
                        typeface: '+mn-cs',
                      },
                      ea: {
                        typeface: '+mn-ea',
                      },
                      fill: {
                        fill: {
                          data: {
                            color: {
                              color: {
                                type: 3,
                                val: 'tx1',
                              },
                              mods: [],
                            },
                          },
                          type: 3,
                        },
                      },
                      fontSize: 1800,
                      kern: 1200,
                      latin: {
                        typeface: '+mn-lt',
                      },
                    },
                    eaLnBrk: true,
                    hangingPunct: true,
                    latinLnBrk: false,
                    marL: 1371600,
                    rtl: false,
                  },
                  {
                    algn: 'l',
                    defTabSz: 914400,
                    defaultRunPr: {
                      cs: {
                        typeface: '+mn-cs',
                      },
                      ea: {
                        typeface: '+mn-ea',
                      },
                      fill: {
                        fill: {
                          data: {
                            color: {
                              color: {
                                type: 3,
                                val: 'tx1',
                              },
                              mods: [],
                            },
                          },
                          type: 3,
                        },
                      },
                      fontSize: 1800,
                      kern: 1200,
                      latin: {
                        typeface: '+mn-lt',
                      },
                    },
                    eaLnBrk: true,
                    hangingPunct: true,
                    latinLnBrk: false,
                    marL: 1828800,
                    rtl: false,
                  },
                  {
                    algn: 'l',
                    defTabSz: 914400,
                    defaultRunPr: {
                      cs: {
                        typeface: '+mn-cs',
                      },
                      ea: {
                        typeface: '+mn-ea',
                      },
                      fill: {
                        fill: {
                          data: {
                            color: {
                              color: {
                                type: 3,
                                val: 'tx1',
                              },
                              mods: [],
                            },
                          },
                          type: 3,
                        },
                      },
                      fontSize: 1800,
                      kern: 1200,
                      latin: {
                        typeface: '+mn-lt',
                      },
                    },
                    eaLnBrk: true,
                    hangingPunct: true,
                    latinLnBrk: false,
                    marL: 2286000,
                    rtl: false,
                  },
                  {
                    algn: 'l',
                    defTabSz: 914400,
                    defaultRunPr: {
                      cs: {
                        typeface: '+mn-cs',
                      },
                      ea: {
                        typeface: '+mn-ea',
                      },
                      fill: {
                        fill: {
                          data: {
                            color: {
                              color: {
                                type: 3,
                                val: 'tx1',
                              },
                              mods: [],
                            },
                          },
                          type: 3,
                        },
                      },
                      fontSize: 1800,
                      kern: 1200,
                      latin: {
                        typeface: '+mn-lt',
                      },
                    },
                    eaLnBrk: true,
                    hangingPunct: true,
                    latinLnBrk: false,
                    marL: 2743200,
                    rtl: false,
                  },
                  {
                    algn: 'l',
                    defTabSz: 914400,
                    defaultRunPr: {
                      cs: {
                        typeface: '+mn-cs',
                      },
                      ea: {
                        typeface: '+mn-ea',
                      },
                      fill: {
                        fill: {
                          data: {
                            color: {
                              color: {
                                type: 3,
                                val: 'tx1',
                              },
                              mods: [],
                            },
                          },
                          type: 3,
                        },
                      },
                      fontSize: 1800,
                      kern: 1200,
                      latin: {
                        typeface: '+mn-lt',
                      },
                    },
                    eaLnBrk: true,
                    hangingPunct: true,
                    latinLnBrk: false,
                    marL: 3200400,
                    rtl: false,
                  },
                  {
                    algn: 'l',
                    defTabSz: 914400,
                    defaultRunPr: {
                      cs: {
                        typeface: '+mn-cs',
                      },
                      ea: {
                        typeface: '+mn-ea',
                      },
                      fill: {
                        fill: {
                          data: {
                            color: {
                              color: {
                                type: 3,
                                val: 'tx1',
                              },
                              mods: [],
                            },
                          },
                          type: 3,
                        },
                      },
                      fontSize: 1800,
                      kern: 1200,
                      latin: {
                        typeface: '+mn-lt',
                      },
                    },
                    eaLnBrk: true,
                    hangingPunct: true,
                    latinLnBrk: false,
                    marL: 3657600,
                    rtl: false,
                  },
                  {
                    defaultRunPr: {
                      lang: 'en-CN',
                    },
                  },
                ],
                notesSz: {
                  cx: 6858000,
                  cy: 9144000,
                },
                sldSz: {
                  cx: 12192000,
                  cy: 6858000,
                },
              },
            },
          },
        },
        {
          action: 'insert',
          attributes: {
            opType: 'PresentationPr',
          },
          data: {
            object: {
              showPr: {},
            },
          },
        },
        {
          action: 'insert',
          attributes: {
            opType: 'TableStyle',
          },
          data: {
            object: {
              defaultTableStyleId: '{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}',
              styles: {},
            },
          },
        },
        {
          action: 'insert',
          attributes: {
            opType: 'Slides',
          },
          data: {
            object: {
              s0000000: {
                order: 0,
              },
            },
          },
        },
        {
          action: 'insert',
          attributes: {
            opType: 'SlideMaster',
          },
          data: {
            object: {
              sm000000: {
                order: 0,
              },
            },
          },
        },
        {
          action: 'insert',
          attributes: {
            opType: 'Notes',
          },
          data: {
            object: {},
          },
        },
        {
          action: 'insert',
          attributes: {
            opType: 'NotesMaster',
          },
          data: {
            object: {
              noteMasterRefs: [],
            },
          },
        },
      ],
    },
  },
  {
    action: 'insert',
    attributes: {
      opType: 'layouts',
    },
    data: {
      slide: {
        l0000000: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                type: 'title',
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      name: 'Title Slide',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000024: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 19,
                                        name: '标题 18',
                                      },
                                      nvPr: {
                                        ph: {
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 8041080,
                                        extY: 2077249,
                                        offX: 2066747,
                                        offY: 1351751,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          anchor: 'b',
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              bullet: {
                                                bulletColor: {
                                                  type: 'followText',
                                                },
                                                bulletSize: {
                                                  type: 'followText',
                                                },
                                                bulletType: {
                                                  type: 'none',
                                                },
                                                bulletTypeface: {
                                                  type: 'followText',
                                                },
                                              },
                                              defTabSz: 914400,
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                extra: {
                                                  effects: {
                                                    data: [],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                                fontSize: 3600,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                spc: 0,
                                              },
                                              indent: 0,
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 110000,
                                              },
                                              marL: 0,
                                              marR: 0,
                                              rtl: false,
                                              spaceAfter: {
                                                type: 'point',
                                                value: 0,
                                              },
                                              spaceBefore: {
                                                type: 'point',
                                                value: 0,
                                              },
                                              tabs: [],
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000025: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 1,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: '文本占位符 3',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 15,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 8040688,
                                        extY: 1792288,
                                        offX: 2075656,
                                        offY: 3533775,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                          textFit: {
                                            type: 'noAutofit',
                                          },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                },
                                              },
                                              defaultRunPr: {
                                                fontSize: 1800,
                                                spc: 0,
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: {
                                                fontSize: 1800,
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: {
                                                fontSize: 1800,
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: {
                                                fontSize: 1800,
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: {
                                                fontSize: 1800,
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000001: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                type: 'obj',
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      name: 'Title and Content',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000003: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 9,
                                        name: '标题 18',
                                      },
                                      nvPr: {
                                        ph: {
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 9687271,
                                        extY: 831465,
                                        offX: 1252365,
                                        offY: 442800,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'l',
                                              bullet: {
                                                bulletColor: {
                                                  type: 'followText',
                                                },
                                                bulletSize: {
                                                  type: 'followText',
                                                },
                                                bulletType: {
                                                  type: 'none',
                                                },
                                                bulletTypeface: {
                                                  type: 'followText',
                                                },
                                              },
                                              defTabSz: 914400,
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                extra: {
                                                  effects: {
                                                    data: [],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                                fontSize: 3200,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                spc: 0,
                                              },
                                              indent: 0,
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 110000,
                                              },
                                              marL: 0,
                                              marR: 0,
                                              rtl: false,
                                              spaceAfter: {
                                                type: 'point',
                                                value: 0,
                                              },
                                              spaceBefore: {
                                                type: 'point',
                                                value: 0,
                                              },
                                              tabs: [],
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000004: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 1,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 11,
                                        name: '文本占位符 10',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 9686925,
                                        extY: 4848513,
                                        offX: 1252711,
                                        offY: 1436400,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              bullet: {
                                                bulletType: {
                                                  char: 'n',
                                                  type: 'character',
                                                },
                                                bulletTypeface: {
                                                  type: 'font',
                                                  typeface: 'Wingdings',
                                                },
                                              },
                                              defaultRunPr: {
                                                spc: 0,
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000002: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                type: 'secHead',
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      bg: {
                        bgPr: {
                          fill: {
                            fill: {
                              data: {
                                color: {
                                  color: {
                                    type: 3,
                                    val: 'accent1',
                                  },
                                  mods: [],
                                },
                              },
                              type: 3,
                            },
                          },
                        },
                      },
                      name: 'Section Header',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000001: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 2,
                                        name: '标题 1',
                                      },
                                      nvPr: {
                                        ph: {
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 8031892,
                                        extY: 2199536,
                                        offX: 1130120,
                                        offY: 1296628,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          anchor: 'b',
                                          extra: {},
                                          textFit: {
                                            type: 'normAutofit',
                                          },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: {
                                                bold: false,
                                                cap: 'none',
                                                cs: {
                                                  typeface: '+mn-cs',
                                                },
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                extra: {
                                                  effects: {
                                                    data: [],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'bg1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                                fontSize: 3600,
                                                italic: false,
                                                lang: 'zh-CN',
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                spc: 0,
                                              },
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 100000,
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000002: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 1,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: '文本占位符 3',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 14,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 8031163,
                                        extY: 1831975,
                                        offX: 1130849,
                                        offY: 3527574,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                },
                                              },
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'bg1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                                spc: 0,
                                              },
                                              indent: 0,
                                              marL: 0,
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'bg1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'bg1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'bg1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'bg1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'bg1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'bg1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'bg1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'bg1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000003: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                type: 'twoObj',
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      name: 'Two Content',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000005: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 9,
                                        name: '标题 18',
                                      },
                                      nvPr: {
                                        ph: {
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 9687271,
                                        extY: 831465,
                                        offX: 1252364,
                                        offY: 442186,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'l',
                                              bullet: {
                                                bulletColor: {
                                                  type: 'followText',
                                                },
                                                bulletSize: {
                                                  type: 'followText',
                                                },
                                                bulletType: {
                                                  type: 'none',
                                                },
                                                bulletTypeface: {
                                                  type: 'followText',
                                                },
                                              },
                                              defTabSz: 914400,
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                extra: {
                                                  effects: {
                                                    data: [],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                                fontSize: 3200,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                spc: 0,
                                              },
                                              indent: 0,
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 110000,
                                              },
                                              marL: 0,
                                              marR: 0,
                                              rtl: false,
                                              spaceAfter: {
                                                type: 'point',
                                                value: 0,
                                              },
                                              spaceBefore: {
                                                type: 'point',
                                                value: 0,
                                              },
                                              tabs: [],
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000006: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 1,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 11,
                                        name: '文本占位符 10',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 18,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 4711700,
                                        extY: 4921250,
                                        offX: 1252364,
                                        offY: 1436832,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: {
                                                spc: 0,
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000007: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 2,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 12,
                                        name: '文本占位符 10',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 19,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 4711700,
                                        extY: 4921250,
                                        offX: 6227938,
                                        offY: 1436832,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: {
                                                spc: 0,
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000004: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                type: 'twoTxTwoObj',
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      name: 'Comparison',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000021: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 9,
                                        name: '标题 18',
                                      },
                                      nvPr: {
                                        ph: {
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 9687271,
                                        extY: 2077249,
                                        offX: 1252280,
                                        offY: 442800,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          anchor: 'b',
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              bullet: {
                                                bulletColor: {
                                                  type: 'followText',
                                                },
                                                bulletSize: {
                                                  type: 'followText',
                                                },
                                                bulletType: {
                                                  type: 'none',
                                                },
                                                bulletTypeface: {
                                                  type: 'followText',
                                                },
                                              },
                                              defTabSz: 914400,
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                extra: {
                                                  effects: {
                                                    data: [],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                                fontSize: 3200,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                spc: 0,
                                              },
                                              indent: 0,
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 110000,
                                              },
                                              marL: 0,
                                              marR: 0,
                                              rtl: false,
                                              spaceAfter: {
                                                type: 'point',
                                                value: 0,
                                              },
                                              spaceBefore: {
                                                type: 'point',
                                                value: 0,
                                              },
                                              tabs: [],
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000022: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 1,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 8,
                                        name: '文本占位符 10',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 18,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 4711700,
                                        extY: 3753427,
                                        offX: 1252364,
                                        offY: 2660073,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: {
                                                spc: 0,
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000023: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 2,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 11,
                                        name: '文本占位符 10',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 19,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 4711700,
                                        extY: 3753427,
                                        offX: 6227938,
                                        offY: 2660073,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: {
                                                spc: 0,
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000005: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                type: 'titleOnly',
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      name: 'Title Only',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000008: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 9,
                                        name: '标题 18',
                                      },
                                      nvPr: {
                                        ph: {
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 9687271,
                                        extY: 831465,
                                        offX: 1252365,
                                        offY: 442800,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'l',
                                              bullet: {
                                                bulletColor: {
                                                  type: 'followText',
                                                },
                                                bulletSize: {
                                                  type: 'followText',
                                                },
                                                bulletType: {
                                                  type: 'none',
                                                },
                                                bulletTypeface: {
                                                  type: 'followText',
                                                },
                                              },
                                              defTabSz: 914400,
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                extra: {
                                                  effects: {
                                                    data: [],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                                fontSize: 3200,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                spc: 0,
                                              },
                                              indent: 0,
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 110000,
                                              },
                                              marL: 0,
                                              marR: 0,
                                              rtl: false,
                                              spaceAfter: {
                                                type: 'point',
                                                value: 0,
                                              },
                                              spaceBefore: {
                                                type: 'point',
                                                value: 0,
                                              },
                                              tabs: [],
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000006: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                type: 'blank',
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      bg: {
                        bgPr: {
                          fill: {
                            fill: {
                              data: {
                                color: {
                                  color: {
                                    type: 3,
                                    val: 'bg1',
                                  },
                                  mods: [],
                                },
                              },
                              type: 3,
                            },
                          },
                        },
                      },
                      name: 'Blank',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {},
                  },
                },
              ],
            },
          },
        ],
        l0000007: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                type: 'objTx',
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      name: 'Content with Caption',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000009: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 9,
                                        name: '图片占位符 8',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 14,
                                          sz: 'quarter',
                                          type: 'pic',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {
                                        effects: {
                                          data: [
                                            {
                                              algn: 'ctr',
                                              blurRad: 365269,
                                              color: {
                                                color: {
                                                  type: 1,
                                                  val: '000000',
                                                },
                                                mods: [
                                                  {
                                                    name: 'alpha',
                                                    val: 16000,
                                                  },
                                                ],
                                              },
                                              dir: 5400000,
                                              dist: 19050,
                                              name: 'outerShdw',
                                              rotWithShape: false,
                                              sx: 98688,
                                              sy: 98688,
                                            },
                                          ],
                                          type: 'effectLst',
                                        },
                                      },
                                      fill: {
                                        fill: {
                                          type: 2,
                                        },
                                      },
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      ln: {
                                        cap: 'sq',
                                        fill: {
                                          fill: {
                                            data: {
                                              color: {
                                                color: {
                                                  type: 3,
                                                  val: 'bg1',
                                                },
                                                mods: [],
                                              },
                                            },
                                            type: 3,
                                          },
                                        },
                                        join: {
                                          limit: 800000,
                                          type: 'miter',
                                        },
                                        w: 76200,
                                      },
                                      xfrm: {
                                        extX: 4884737,
                                        extY: 6005513,
                                        offX: 6911492,
                                        offY: 356895,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {
                                    effectRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 3,
                                    },
                                    fillRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 3,
                                    },
                                    fontRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'lt1',
                                        },
                                        mods: [],
                                      },
                                      idx: 'minor',
                                    },
                                    lnRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 0,
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                },
                                              },
                                              defaultRunPr: {
                                                extra: {
                                                  effects: {
                                                    data: [
                                                      {
                                                        algn: 'tl',
                                                        blurRad: 50800,
                                                        color: {
                                                          color: {
                                                            type: 2,
                                                            val: 'black',
                                                          },
                                                          mods: [
                                                            {
                                                              name: 'alpha',
                                                              val: 40000,
                                                            },
                                                          ],
                                                        },
                                                        dir: 2700000,
                                                        dist: 38100,
                                                        name: 'outerShdw',
                                                        rotWithShape: false,
                                                      },
                                                    ],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    type: 2,
                                                  },
                                                },
                                                spc: 0,
                                              },
                                              indent: 0,
                                              marL: 0,
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000010: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 1,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 2,
                                        name: '标题 1',
                                      },
                                      nvPr: {
                                        ph: {
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 6029843,
                                        extY: 2167258,
                                        offX: 395771,
                                        offY: 356895,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          anchor: 'b',
                                          extra: {},
                                          textFit: {
                                            type: 'normAutofit',
                                          },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: {
                                                bold: false,
                                                cap: 'none',
                                                cs: {
                                                  typeface: '+mn-cs',
                                                },
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                extra: {
                                                  effects: {
                                                    data: [],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                                fontSize: 3600,
                                                italic: false,
                                                lang: 'zh-CN',
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                spc: 0,
                                              },
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 100000,
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000011: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 2,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: '文本占位符 3',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 15,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 6030912,
                                        extY: 3710776,
                                        offX: 395236,
                                        offY: 2683824,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                },
                                              },
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'accent3',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                                spc: 0,
                                              },
                                              indent: 0,
                                              marL: 0,
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'accent3',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'accent3',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'accent3',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'accent3',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000008: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                type: 'picTx',
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      name: 'Picture with Caption',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000012: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 9,
                                        name: '图片占位符 8',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 14,
                                          sz: 'quarter',
                                          type: 'pic',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {
                                        effects: {
                                          data: [
                                            {
                                              algn: 'ctr',
                                              blurRad: 365269,
                                              color: {
                                                color: {
                                                  type: 1,
                                                  val: '000000',
                                                },
                                                mods: [
                                                  {
                                                    name: 'alpha',
                                                    val: 16000,
                                                  },
                                                ],
                                              },
                                              dir: 5400000,
                                              dist: 19050,
                                              name: 'outerShdw',
                                              rotWithShape: false,
                                              sx: 98688,
                                              sy: 98688,
                                            },
                                          ],
                                          type: 'effectLst',
                                        },
                                      },
                                      fill: {
                                        fill: {
                                          type: 2,
                                        },
                                      },
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      ln: {
                                        cap: 'sq',
                                        fill: {
                                          fill: {
                                            data: {
                                              color: {
                                                color: {
                                                  type: 3,
                                                  val: 'bg1',
                                                },
                                                mods: [],
                                              },
                                            },
                                            type: 3,
                                          },
                                        },
                                        join: {
                                          limit: 800000,
                                          type: 'miter',
                                        },
                                        w: 76200,
                                      },
                                      xfrm: {
                                        extX: 4884737,
                                        extY: 6005513,
                                        offX: 6911492,
                                        offY: 356895,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {
                                    effectRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 3,
                                    },
                                    fillRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 3,
                                    },
                                    fontRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'lt1',
                                        },
                                        mods: [],
                                      },
                                      idx: 'minor',
                                    },
                                    lnRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 0,
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                },
                                              },
                                              defaultRunPr: {
                                                extra: {
                                                  effects: {
                                                    data: [
                                                      {
                                                        algn: 'tl',
                                                        blurRad: 50800,
                                                        color: {
                                                          color: {
                                                            type: 2,
                                                            val: 'black',
                                                          },
                                                          mods: [
                                                            {
                                                              name: 'alpha',
                                                              val: 40000,
                                                            },
                                                          ],
                                                        },
                                                        dir: 2700000,
                                                        dist: 38100,
                                                        name: 'outerShdw',
                                                        rotWithShape: false,
                                                      },
                                                    ],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    type: 2,
                                                  },
                                                },
                                                spc: 0,
                                              },
                                              indent: 0,
                                              marL: 0,
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000013: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 1,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 2,
                                        name: '标题 1',
                                      },
                                      nvPr: {
                                        ph: {
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 6029843,
                                        extY: 2167258,
                                        offX: 395771,
                                        offY: 3101071,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          anchor: 'b',
                                          extra: {},
                                          textFit: {
                                            type: 'normAutofit',
                                          },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: {
                                                bold: false,
                                                cap: 'none',
                                                cs: {
                                                  typeface: '+mn-cs',
                                                },
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                extra: {
                                                  effects: {
                                                    data: [],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                                fontSize: 3600,
                                                italic: false,
                                                lang: 'zh-CN',
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN Medium',
                                                },
                                                spc: 0,
                                              },
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 100000,
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000014: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 2,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: '文本占位符 3',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 15,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 6030912,
                                        extY: 1013491,
                                        offX: 395236,
                                        offY: 5381108,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                },
                                              },
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'accent3',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                                spc: 0,
                                              },
                                              indent: 0,
                                              marL: 0,
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'accent3',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'accent3',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'accent3',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'accent3',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000009: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      name: 'Content Only',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000026: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 3,
                                        name: '文本占位符 2',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 14,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 6029325,
                                        extY: 4356100,
                                        offX: 3081337,
                                        offY: 1250950,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          extra: {},
                                          textFit: {
                                            type: 'normAutofit',
                                          },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                                fontSize: 2400,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                                fontSize: 2400,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                                fontSize: 2400,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                                fontSize: 2400,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                                fontSize: 2400,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                                fontSize: 2400,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                              },
                                            },
                                            {
                                              bullet: {
                                                bulletType: {
                                                  char: 'n',
                                                  type: 'character',
                                                },
                                                bulletTypeface: {
                                                  type: 'font',
                                                  typeface: 'Wingdings',
                                                },
                                              },
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                                fontSize: 2400,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                              },
                                            },
                                            {
                                              bullet: {
                                                bulletType: {
                                                  char: 'n',
                                                  type: 'character',
                                                },
                                                bulletTypeface: {
                                                  type: 'font',
                                                  typeface: 'Wingdings',
                                                },
                                              },
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                                fontSize: 2400,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                              },
                                            },
                                            {
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                                fontSize: 2400,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN',
                                                },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000010: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      name: 'Salient Facts',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000015: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: '标题 4',
                                      },
                                      nvPr: {
                                        ph: {
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 9667042,
                                        extY: 781235,
                                        offX: 1262477,
                                        offY: 3968318,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          anchor: 't',
                                          extra: {},
                                          textFit: {
                                            type: 'noAutofit',
                                          },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: {
                                                bold: false,
                                                ea: {
                                                  typeface: '+mj-ea',
                                                },
                                                fill: {
                                                  fill: {
                                                    data: {
                                                      color: {
                                                        color: {
                                                          type: 3,
                                                          val: 'accent2',
                                                        },
                                                        mods: [],
                                                      },
                                                    },
                                                    type: 3,
                                                  },
                                                },
                                                fontSize: 2400,
                                                latin: {
                                                  typeface: '+mj-ea',
                                                },
                                                spc: 0,
                                              },
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 100000,
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000016: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 1,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '文本占位符 5',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 9667875,
                                        extY: 2182812,
                                        offX: 1262477,
                                        offY: 1775933,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                },
                                              },
                                              defaultRunPr: {
                                                bold: true,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN Heavy',
                                                },
                                                fontSize: 13000,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN Heavy',
                                                },
                                                spc: 0,
                                              },
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 100000,
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: {
                                                bold: true,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN Heavy',
                                                },
                                                fontSize: 13000,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN Heavy',
                                                },
                                              },
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 100000,
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: {
                                                bold: true,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN Heavy',
                                                },
                                                fontSize: 13000,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN Heavy',
                                                },
                                              },
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 100000,
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: {
                                                bold: true,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN Heavy',
                                                },
                                                fontSize: 13000,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN Heavy',
                                                },
                                              },
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 100000,
                                              },
                                            },
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: {
                                                bold: true,
                                                ea: {
                                                  typeface:
                                                    'Source Han Sans CN Heavy',
                                                },
                                                fontSize: 13000,
                                                italic: false,
                                                latin: {
                                                  typeface:
                                                    'Source Han Sans CN Heavy',
                                                },
                                              },
                                              lineSpacing: {
                                                type: 'percent',
                                                value: 100000,
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                              rPr: '{"dirty":false,"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000011: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                type: 'objAndTwoObj',
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      name: 'Multi-Picture Slide',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000017: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 3,
                                        name: '图片占位符 8',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 14,
                                          sz: 'quarter',
                                          type: 'pic',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {
                                        effects: {
                                          data: [
                                            {
                                              algn: 'ctr',
                                              blurRad: 365269,
                                              color: {
                                                color: {
                                                  type: 1,
                                                  val: '000000',
                                                },
                                                mods: [
                                                  {
                                                    name: 'alpha',
                                                    val: 16000,
                                                  },
                                                ],
                                              },
                                              dir: 5400000,
                                              dist: 19050,
                                              name: 'outerShdw',
                                              rotWithShape: false,
                                              sx: 98688,
                                              sy: 98688,
                                            },
                                          ],
                                          type: 'effectLst',
                                        },
                                      },
                                      fill: {
                                        fill: {
                                          type: 2,
                                        },
                                      },
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      ln: {
                                        cap: 'sq',
                                        fill: {
                                          fill: {
                                            data: {
                                              color: {
                                                color: {
                                                  type: 3,
                                                  val: 'bg1',
                                                },
                                                mods: [],
                                              },
                                            },
                                            type: 3,
                                          },
                                        },
                                        join: {
                                          limit: 800000,
                                          type: 'miter',
                                        },
                                        w: 76200,
                                      },
                                      xfrm: {
                                        extX: 5412054,
                                        extY: 2883939,
                                        offX: 6384176,
                                        offY: 374650,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {
                                    effectRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 3,
                                    },
                                    fillRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 3,
                                    },
                                    fontRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'lt1',
                                        },
                                        mods: [],
                                      },
                                      idx: 'minor',
                                    },
                                    lnRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 0,
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                },
                                              },
                                              defaultRunPr: {
                                                extra: {
                                                  effects: {
                                                    data: [
                                                      {
                                                        algn: 'tl',
                                                        blurRad: 50800,
                                                        color: {
                                                          color: {
                                                            type: 2,
                                                            val: 'black',
                                                          },
                                                          mods: [
                                                            {
                                                              name: 'alpha',
                                                              val: 40000,
                                                            },
                                                          ],
                                                        },
                                                        dir: 2700000,
                                                        dist: 38100,
                                                        name: 'outerShdw',
                                                        rotWithShape: false,
                                                      },
                                                    ],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    type: 2,
                                                  },
                                                },
                                              },
                                              indent: 0,
                                              marL: 0,
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000018: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 1,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 4,
                                        name: '图片占位符 8',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 15,
                                          sz: 'quarter',
                                          type: 'pic',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {
                                        effects: {
                                          data: [
                                            {
                                              algn: 'ctr',
                                              blurRad: 365269,
                                              color: {
                                                color: {
                                                  type: 1,
                                                  val: '000000',
                                                },
                                                mods: [
                                                  {
                                                    name: 'alpha',
                                                    val: 16000,
                                                  },
                                                ],
                                              },
                                              dir: 5400000,
                                              dist: 19050,
                                              name: 'outerShdw',
                                              rotWithShape: false,
                                              sx: 98688,
                                              sy: 98688,
                                            },
                                          ],
                                          type: 'effectLst',
                                        },
                                      },
                                      fill: {
                                        fill: {
                                          type: 2,
                                        },
                                      },
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      ln: {
                                        cap: 'sq',
                                        fill: {
                                          fill: {
                                            data: {
                                              color: {
                                                color: {
                                                  type: 3,
                                                  val: 'bg1',
                                                },
                                                mods: [],
                                              },
                                            },
                                            type: 3,
                                          },
                                        },
                                        join: {
                                          limit: 800000,
                                          type: 'miter',
                                        },
                                        w: 76200,
                                      },
                                      xfrm: {
                                        extX: 5718312,
                                        extY: 6005513,
                                        offX: 377688,
                                        offY: 374650,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {
                                    effectRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 3,
                                    },
                                    fillRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 3,
                                    },
                                    fontRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'lt1',
                                        },
                                        mods: [],
                                      },
                                      idx: 'minor',
                                    },
                                    lnRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 0,
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                },
                                              },
                                              defaultRunPr: {
                                                extra: {
                                                  effects: {
                                                    data: [
                                                      {
                                                        algn: 'tl',
                                                        blurRad: 50800,
                                                        color: {
                                                          color: {
                                                            type: 2,
                                                            val: 'black',
                                                          },
                                                          mods: [
                                                            {
                                                              name: 'alpha',
                                                              val: 40000,
                                                            },
                                                          ],
                                                        },
                                                        dir: 2700000,
                                                        dist: 38100,
                                                        name: 'outerShdw',
                                                        rotWithShape: false,
                                                      },
                                                    ],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    type: 2,
                                                  },
                                                },
                                              },
                                              indent: 0,
                                              marL: 0,
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000019: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 2,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 5,
                                        name: '图片占位符 8',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 16,
                                          sz: 'quarter',
                                          type: 'pic',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {
                                        effects: {
                                          data: [
                                            {
                                              algn: 'ctr',
                                              blurRad: 365269,
                                              color: {
                                                color: {
                                                  type: 1,
                                                  val: '000000',
                                                },
                                                mods: [
                                                  {
                                                    name: 'alpha',
                                                    val: 16000,
                                                  },
                                                ],
                                              },
                                              dir: 5400000,
                                              dist: 19050,
                                              name: 'outerShdw',
                                              rotWithShape: false,
                                              sx: 98688,
                                              sy: 98688,
                                            },
                                          ],
                                          type: 'effectLst',
                                        },
                                      },
                                      fill: {
                                        fill: {
                                          type: 2,
                                        },
                                      },
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      ln: {
                                        cap: 'sq',
                                        fill: {
                                          fill: {
                                            data: {
                                              color: {
                                                color: {
                                                  type: 3,
                                                  val: 'bg1',
                                                },
                                                mods: [],
                                              },
                                            },
                                            type: 3,
                                          },
                                        },
                                        join: {
                                          limit: 800000,
                                          type: 'miter',
                                        },
                                        w: 76200,
                                      },
                                      xfrm: {
                                        extX: 5412054,
                                        extY: 2883939,
                                        offX: 6384176,
                                        offY: 3533486,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {
                                    effectRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 3,
                                    },
                                    fillRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 3,
                                    },
                                    fontRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'lt1',
                                        },
                                        mods: [],
                                      },
                                      idx: 'minor',
                                    },
                                    lnRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 0,
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              bullet: {
                                                bulletType: {
                                                  type: 'none',
                                                },
                                              },
                                              defaultRunPr: {
                                                extra: {
                                                  effects: {
                                                    data: [
                                                      {
                                                        algn: 'tl',
                                                        blurRad: 50800,
                                                        color: {
                                                          color: {
                                                            type: 2,
                                                            val: 'black',
                                                          },
                                                          mods: [
                                                            {
                                                              name: 'alpha',
                                                              val: 40000,
                                                            },
                                                          ],
                                                        },
                                                        dir: 2700000,
                                                        dist: 38100,
                                                        name: 'outerShdw',
                                                        rotWithShape: false,
                                                      },
                                                    ],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    type: 2,
                                                  },
                                                },
                                              },
                                              indent: 0,
                                              marL: 0,
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000012: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                type: 'objOnly',
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      name: 'Big Picture',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000020: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 10,
                                        name: 'Content Placeholder 9',
                                      },
                                      nvPr: {
                                        ph: {
                                          hasCustomPrompt: true,
                                          idx: 16,
                                          sz: 'quarter',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      extra: {
                                        effects: {
                                          data: [
                                            {
                                              algn: 'ctr',
                                              blurRad: 365269,
                                              color: {
                                                color: {
                                                  type: 1,
                                                  val: '000000',
                                                },
                                                mods: [
                                                  {
                                                    name: 'alpha',
                                                    val: 16000,
                                                  },
                                                ],
                                              },
                                              dir: 5400000,
                                              dist: 19050,
                                              name: 'outerShdw',
                                              rotWithShape: false,
                                              sx: 98688,
                                              sy: 98688,
                                            },
                                          ],
                                          type: 'effectLst',
                                        },
                                      },
                                      fill: {
                                        fill: {
                                          type: 2,
                                        },
                                      },
                                      ln: {
                                        cap: 'sq',
                                        fill: {
                                          fill: {
                                            data: {
                                              color: {
                                                color: {
                                                  type: 3,
                                                  val: 'bg1',
                                                },
                                                mods: [],
                                              },
                                            },
                                            type: 3,
                                          },
                                        },
                                        join: {
                                          limit: 800000,
                                          type: 'miter',
                                        },
                                        w: 76200,
                                      },
                                      xfrm: {
                                        extX: 11509512,
                                        extY: 6005513,
                                        offX: 341244,
                                        offY: 426243,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {
                                    effectRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 3,
                                    },
                                    fillRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 3,
                                    },
                                    fontRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'lt1',
                                        },
                                        mods: [],
                                      },
                                      idx: 'minor',
                                    },
                                    lnRef: {
                                      color: {
                                        color: {
                                          type: 3,
                                          val: 'accent5',
                                        },
                                        mods: [],
                                      },
                                      idx: 0,
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          bIns: 45720,
                                          extra: {},
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          textFit: {
                                            type: 'normAutofit',
                                          },
                                          vert: 'horz',
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              defaultRunPr: {
                                                dirty: false,
                                                ea: {
                                                  typeface: '+mn-ea',
                                                },
                                                extra: {
                                                  effects: {
                                                    data: [
                                                      {
                                                        algn: 'tl',
                                                        blurRad: 50800,
                                                        color: {
                                                          color: {
                                                            type: 2,
                                                            val: 'black',
                                                          },
                                                          mods: [
                                                            {
                                                              name: 'alpha',
                                                              val: 40000,
                                                            },
                                                          ],
                                                        },
                                                        dir: 2700000,
                                                        dist: 38100,
                                                        name: 'outerShdw',
                                                        rotWithShape: false,
                                                      },
                                                    ],
                                                    type: 'effectLst',
                                                  },
                                                },
                                                fill: {
                                                  fill: {
                                                    type: 2,
                                                  },
                                                },
                                                lang: 'en-CN',
                                                latin: {
                                                  typeface: '+mn-lt',
                                                },
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"bullet":{"bulletType":{"type":"none"}},"indent":0,"lvl":0,"marL":0}',
                                              paraEnd: true,
                                              rPr: '{}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
        l0000013: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                preserve: true,
                showMasterPhAnim: false,
                type: 'blank',
                userDrawn: true,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      name: 'Blank Slide',
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {},
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: {
      opType: 'slideMasters',
    },
    data: {
      slide: {
        sm000000: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  accent1: 'accent1',
                  accent2: 'accent2',
                  accent3: 'accent3',
                  accent4: 'accent4',
                  accent5: 'accent5',
                  accent6: 'accent6',
                  bg1: 'lt1',
                  bg2: 'lt2',
                  folHlink: 'folHlink',
                  hlink: 'hlink',
                  tx1: 'dk1',
                  tx2: 'dk2',
                },
                hf: {
                  dt: false,
                  ftr: false,
                  hdr: false,
                  sldNum: false,
                },
                preserve: true,
                themeRef: 'th000000',
                txStyles: {
                  bodyStyle: [
                    {
                      algn: 'l',
                      bullet: {
                        bulletType: {
                          char: 'n',
                          type: 'character',
                        },
                        bulletTypeface: {
                          type: 'font',
                          typeface: 'Wingdings',
                        },
                      },
                      defTabSz: 914400,
                      defaultRunPr: {
                        bold: false,
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'accent2',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 2000,
                        italic: false,
                        latin: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        spc: 0,
                      },
                      indent: -342900,
                      lineSpacing: {
                        type: 'percent',
                        value: 135000,
                      },
                      marL: 342900,
                      rtl: false,
                      spaceBefore: {
                        type: 'point',
                        value: 1000,
                      },
                    },
                    {
                      algn: 'l',
                      bullet: {
                        bulletType: {
                          char: 'n',
                          type: 'character',
                        },
                        bulletTypeface: {
                          type: 'font',
                          typeface: 'Wingdings',
                        },
                      },
                      defTabSz: 914400,
                      defaultRunPr: {
                        bold: false,
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'accent2',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 2000,
                        italic: false,
                        latin: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        spc: 0,
                      },
                      indent: -342900,
                      lineSpacing: {
                        type: 'percent',
                        value: 135000,
                      },
                      marL: 800100,
                      rtl: false,
                      spaceBefore: {
                        type: 'point',
                        value: 500,
                      },
                    },
                    {
                      algn: 'l',
                      bullet: {
                        bulletType: {
                          char: 'n',
                          type: 'character',
                        },
                        bulletTypeface: {
                          type: 'font',
                          typeface: 'Wingdings',
                        },
                      },
                      defTabSz: 914400,
                      defaultRunPr: {
                        bold: false,
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'accent2',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 2000,
                        italic: false,
                        lang: 'en-US',
                        latin: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        spc: 0,
                      },
                      indent: -342900,
                      lineSpacing: {
                        type: 'percent',
                        value: 135000,
                      },
                      marL: 1257300,
                      rtl: false,
                      spaceBefore: {
                        type: 'point',
                        value: 500,
                      },
                    },
                    {
                      algn: 'l',
                      bullet: {
                        bulletType: {
                          char: 'n',
                          type: 'character',
                        },
                        bulletTypeface: {
                          type: 'font',
                          typeface: 'Wingdings',
                        },
                      },
                      defTabSz: 914400,
                      defaultRunPr: {
                        bold: false,
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'accent2',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 2000,
                        italic: false,
                        lang: 'en-US',
                        latin: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        spc: 0,
                      },
                      indent: -342900,
                      lineSpacing: {
                        type: 'percent',
                        value: 135000,
                      },
                      marL: 1714500,
                      rtl: false,
                      spaceBefore: {
                        type: 'point',
                        value: 500,
                      },
                    },
                    {
                      algn: 'l',
                      bullet: {
                        bulletType: {
                          char: 'n',
                          type: 'character',
                        },
                        bulletTypeface: {
                          type: 'font',
                          typeface: 'Wingdings',
                        },
                      },
                      defTabSz: 914400,
                      defaultRunPr: {
                        bold: false,
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'accent2',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 2000,
                        italic: false,
                        lang: 'en-US',
                        latin: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        spc: 0,
                      },
                      indent: -342900,
                      lineSpacing: {
                        type: 'percent',
                        value: 135000,
                      },
                      marL: 2171700,
                      rtl: false,
                      spaceBefore: {
                        type: 'point',
                        value: 500,
                      },
                    },
                    {
                      algn: 'l',
                      bullet: {
                        bulletType: {
                          char: 'n',
                          type: 'character',
                        },
                        bulletTypeface: {
                          type: 'font',
                          typeface: 'Wingdings',
                        },
                      },
                      defTabSz: 914400,
                      defaultRunPr: {
                        bold: false,
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'accent2',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 2000,
                        italic: false,
                        lang: 'en-US',
                        latin: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        spc: 0,
                      },
                      indent: -228600,
                      lineSpacing: {
                        type: 'percent',
                        value: 135000,
                      },
                      marL: 2514600,
                      rtl: false,
                      spaceBefore: {
                        type: 'point',
                        value: 500,
                      },
                    },
                    {
                      algn: 'l',
                      bullet: {
                        bulletType: {
                          type: 'none',
                        },
                        bulletTypeface: {
                          type: 'font',
                          typeface: 'Wingdings',
                        },
                      },
                      defTabSz: 914400,
                      defaultRunPr: {
                        bold: false,
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'accent2',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 2000,
                        italic: false,
                        lang: 'en-US',
                        latin: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        spc: 0,
                      },
                      indent: 0,
                      lineSpacing: {
                        type: 'percent',
                        value: 135000,
                      },
                      marL: 2743200,
                      rtl: false,
                      spaceBefore: {
                        type: 'point',
                        value: 500,
                      },
                    },
                    {
                      algn: 'l',
                      bullet: {
                        bulletType: {
                          char: 'n',
                          type: 'character',
                        },
                        bulletTypeface: {
                          type: 'font',
                          typeface: 'Wingdings',
                        },
                      },
                      defTabSz: 914400,
                      defaultRunPr: {
                        bold: false,
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'accent2',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 2000,
                        italic: false,
                        lang: 'en-US',
                        latin: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        spc: 300,
                      },
                      indent: -228600,
                      lineSpacing: {
                        type: 'percent',
                        value: 135000,
                      },
                      marL: 3429000,
                      rtl: false,
                      spaceBefore: {
                        type: 'point',
                        value: 500,
                      },
                    },
                    {
                      algn: 'l',
                      bullet: {
                        bulletType: {
                          char: 'n',
                          type: 'character',
                        },
                        bulletTypeface: {
                          type: 'font',
                          typeface: 'Wingdings',
                        },
                      },
                      defTabSz: 914400,
                      defaultRunPr: {
                        bold: false,
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'accent2',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 2000,
                        italic: false,
                        lang: 'zh-CN',
                        latin: {
                          typeface: 'Source Han Sans CN Light',
                        },
                        spc: 300,
                      },
                      indent: -228600,
                      lineSpacing: {
                        type: 'percent',
                        value: 135000,
                      },
                      marL: 3886200,
                      rtl: false,
                      spaceBefore: {
                        type: 'point',
                        value: 500,
                      },
                    },
                    null,
                  ],
                  otherStyle: [
                    {
                      algn: 'l',
                      defTabSz: 914400,
                      defaultRunPr: {
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: '+mn-ea',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'tx1',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 1800,
                        latin: {
                          typeface: '+mn-lt',
                        },
                      },
                      marL: 0,
                      rtl: false,
                    },
                    {
                      algn: 'l',
                      defTabSz: 914400,
                      defaultRunPr: {
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: '+mn-ea',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'tx1',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 1800,
                        latin: {
                          typeface: '+mn-lt',
                        },
                      },
                      marL: 457200,
                      rtl: false,
                    },
                    {
                      algn: 'l',
                      defTabSz: 914400,
                      defaultRunPr: {
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: '+mn-ea',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'tx1',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 1800,
                        latin: {
                          typeface: '+mn-lt',
                        },
                      },
                      marL: 914400,
                      rtl: false,
                    },
                    {
                      algn: 'l',
                      defTabSz: 914400,
                      defaultRunPr: {
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: '+mn-ea',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'tx1',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 1800,
                        latin: {
                          typeface: '+mn-lt',
                        },
                      },
                      marL: 1371600,
                      rtl: false,
                    },
                    {
                      algn: 'l',
                      defTabSz: 914400,
                      defaultRunPr: {
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: '+mn-ea',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'tx1',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 1800,
                        latin: {
                          typeface: '+mn-lt',
                        },
                      },
                      marL: 1828800,
                      rtl: false,
                    },
                    {
                      algn: 'l',
                      defTabSz: 914400,
                      defaultRunPr: {
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: '+mn-ea',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'tx1',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 1800,
                        latin: {
                          typeface: '+mn-lt',
                        },
                      },
                      marL: 2286000,
                      rtl: false,
                    },
                    {
                      algn: 'l',
                      defTabSz: 914400,
                      defaultRunPr: {
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: '+mn-ea',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'tx1',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 1800,
                        latin: {
                          typeface: '+mn-lt',
                        },
                      },
                      marL: 2743200,
                      rtl: false,
                    },
                    {
                      algn: 'l',
                      defTabSz: 914400,
                      defaultRunPr: {
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: '+mn-ea',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'tx1',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 1800,
                        latin: {
                          typeface: '+mn-lt',
                        },
                      },
                      marL: 3200400,
                      rtl: false,
                    },
                    {
                      algn: 'l',
                      defTabSz: 914400,
                      defaultRunPr: {
                        cs: {
                          typeface: '+mn-cs',
                        },
                        ea: {
                          typeface: '+mn-ea',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'tx1',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 1800,
                        latin: {
                          typeface: '+mn-lt',
                        },
                      },
                      marL: 3657600,
                      rtl: false,
                    },
                    {
                      defaultRunPr: {
                        lang: 'zh-CN',
                      },
                    },
                  ],
                  titleStyle: [
                    {
                      algn: 'l',
                      bullet: {
                        bulletType: {
                          type: 'none',
                        },
                      },
                      defTabSz: 914400,
                      defaultRunPr: {
                        cs: {
                          typeface: '+mj-cs',
                        },
                        ea: {
                          typeface: '+mj-ea',
                        },
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'tx1',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        fontSize: 3200,
                        latin: {
                          typeface: '+mj-lt',
                        },
                        spc: 0,
                      },
                      lineSpacing: {
                        type: 'percent',
                        value: 90000,
                      },
                      rtl: false,
                      spaceBefore: {
                        type: 'point',
                        value: 0,
                      },
                    },
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                  ],
                },
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'layoutRefs',
            },
            data: {
              object: {
                layoutRefs: [
                  'l0000000',
                  'l0000001',
                  'l0000002',
                  'l0000003',
                  'l0000004',
                  'l0000005',
                  'l0000006',
                  'l0000007',
                  'l0000008',
                  'l0000009',
                  'l0000010',
                  'l0000011',
                  'l0000012',
                  'l0000013',
                ],
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {
                      bg: {
                        bgPr: {
                          fill: {
                            fill: {
                              data: {
                                color: {
                                  color: {
                                    type: 1,
                                    val: 'F7F7F7',
                                  },
                                  mods: [],
                                },
                              },
                              type: 3,
                            },
                          },
                        },
                      },
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000027: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 2,
                                        name: '标题占位符 1',
                                      },
                                      nvPr: {
                                        ph: {
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 10515600,
                                        extY: 1325563,
                                        offX: 838200,
                                        offY: 365125,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          extra: {},
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          textFit: {
                                            type: 'normAutofit',
                                          },
                                          vert: 'horz',
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"zh-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000028: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 1,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 3,
                                        name: '文本占位符 2',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 1,
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      bwMode: 'auto',
                                      extra: {},
                                      geometry: {
                                        avLst: [],
                                        preset: 'rect',
                                      },
                                      xfrm: {
                                        extX: 10515600,
                                        extY: 4351338,
                                        offX: 838200,
                                        offY: 1825625,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          bIns: 45720,
                                          extra: {},
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          textFit: {
                                            type: 'normAutofit',
                                          },
                                          vert: 'horz',
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              paraEnd: true,
                                              rPr: '{"lang":"en-US"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: {
      opType: 'slides',
    },
    data: {
      slide: {
        s0000000: [
          {
            action: 'insert',
            attributes: {
              opType: 'attrs',
            },
            data: {
              object: {
                clrMap: {
                  type: 'useMaster',
                },
                commentsRef: null,
                layoutRef: 'l0000000',
                notesRef: null,
              },
            },
          },
          {
            action: 'insert',
            attributes: {
              opType: 'csld',
            },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: {
                    opType: 'attrs',
                  },
                  data: {
                    object: {},
                  },
                },
                {
                  action: 'insert',
                  attributes: {
                    opType: 'spTree',
                  },
                  data: {
                    slide: {
                      sp000029: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 0,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 2,
                                        name: 'Title 1',
                                      },
                                      nvPr: {
                                        ph: {
                                          type: 'title',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      extra: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"en-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000030: [
                        {
                          action: 'insert',
                          attributes: {
                            order: 1,
                            spType: 'sp',
                          },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'attrs',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'nvSpPr',
                                },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 3,
                                        name: 'Text Placeholder 2',
                                      },
                                      nvPr: {
                                        ph: {
                                          idx: 15,
                                          sz: 'quarter',
                                          type: 'body',
                                        },
                                      },
                                      nvUniSpPr: {
                                        locks: ['noGrp'],
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'spPr',
                                },
                                data: {
                                  object: {
                                    spPr: {
                                      extra: {},
                                      xfrm: {
                                        extX: 8041080,
                                        extY: 1792288,
                                        offX: 2066748,
                                        offY: 3533775,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'style',
                                },
                                data: {
                                  object: {},
                                },
                              },
                              {
                                action: 'insert',
                                attributes: {
                                  opType: 'txBody',
                                },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'bodyPr',
                                      },
                                      data: {
                                        object: {
                                          extra: {},
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'listStyle',
                                      },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: {
                                        opType: 'docContent',
                                      },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              paraEnd: true,
                                              rPr: '{"lang":"en-CN"}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: {
      opType: 'themes',
    },
    data: {
      slide: {
        th000000: [
          {
            action: 'insert',
            data: {
              object: {
                extraClrSchemeLst: [],
                name: 'Default Layout',
                objectDefaults: {
                  spDef: {
                    bodyPr: {
                      anchor: 'ctr',
                      extra: {},
                      rtlCol: false,
                      textFit: {
                        type: 'noAutofit',
                      },
                    },
                    lstStyle: [
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      {
                        algn: 'ctr',
                        defaultRunPr: {
                          ea: {
                            typeface: 'Source Han Sans CN',
                          },
                          fill: {
                            fill: {
                              data: {
                                color: {
                                  color: {
                                    type: 3,
                                    val: 'bg2',
                                  },
                                  mods: [],
                                },
                              },
                              type: 3,
                            },
                          },
                          fontSize: 2000,
                          latin: {
                            typeface: 'Source Han Sans CN',
                          },
                        },
                        lineSpacing: {
                          type: 'percent',
                          value: 125000,
                        },
                      },
                    ],
                    spPr: {
                      bwMode: 'auto',
                      extra: {},
                      fill: {
                        fill: {
                          data: {
                            color: {
                              color: {
                                type: 3,
                                val: 'accent2',
                              },
                              mods: [],
                            },
                          },
                          type: 3,
                        },
                      },
                      ln: {
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'accent1',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                      },
                    },
                  },
                  txDef: {
                    bodyPr: {
                      extra: {},
                      rtlCol: false,
                      textFit: {
                        type: 'spAutoFit',
                      },
                      wrap: 'square',
                    },
                    lstStyle: [
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      null,
                      {
                        algn: 'l',
                        defaultRunPr: {
                          ea: {
                            typeface: 'Source Han Sans CN Light',
                          },
                          fill: {
                            fill: {
                              data: {
                                color: {
                                  color: {
                                    type: 3,
                                    val: 'accent4',
                                  },
                                  mods: [
                                    {
                                      name: 'lumMod',
                                      val: 50000,
                                    },
                                  ],
                                },
                              },
                              type: 3,
                            },
                          },
                          fontSize: 2000,
                          latin: {
                            typeface: 'Source Han Sans CN Light',
                          },
                        },
                        lineSpacing: {
                          type: 'percent',
                          value: 125000,
                        },
                      },
                    ],
                    spPr: {
                      bwMode: 'auto',
                      extra: {},
                    },
                  },
                },
                themeElements: {
                  clrScheme: {
                    accent1: {
                      color: {
                        type: 1,
                        val: '2A3033',
                      },
                      mods: [],
                    },
                    accent2: {
                      color: {
                        type: 1,
                        val: '3F464B',
                      },
                      mods: [],
                    },
                    accent3: {
                      color: {
                        type: 1,
                        val: '797E81',
                      },
                      mods: [],
                    },
                    accent4: {
                      color: {
                        type: 1,
                        val: '9FA2A5',
                      },
                      mods: [],
                    },
                    accent5: {
                      color: {
                        type: 1,
                        val: 'C3C6C7',
                      },
                      mods: [],
                    },
                    accent6: {
                      color: {
                        type: 1,
                        val: 'D8DADB',
                      },
                      mods: [],
                    },
                    dk1: {
                      color: {
                        type: 1,
                        val: '2A3033',
                      },
                      mods: [],
                    },
                    dk2: {
                      color: {
                        type: 1,
                        val: '343B3E',
                      },
                      mods: [],
                    },
                    folHlink: {
                      color: {
                        type: 1,
                        val: 'B7DCF9',
                      },
                      mods: [],
                    },
                    hlink: {
                      color: {
                        type: 1,
                        val: '3DA1ED',
                      },
                      mods: [],
                    },
                    lt1: {
                      color: {
                        type: 1,
                        val: 'FAFBFA',
                      },
                      mods: [],
                    },
                    lt2: {
                      color: {
                        type: 1,
                        val: 'FAFBFA',
                      },
                      mods: [],
                    },
                    name: '自定义 1',
                  },
                  fmtScheme: {
                    bgFillStyleLst: [
                      {
                        fill: {
                          data: {
                            color: {
                              color: {
                                type: 3,
                                val: 'phClr',
                              },
                              mods: [],
                            },
                          },
                          type: 3,
                        },
                      },
                      {
                        fill: {
                          data: {
                            color: {
                              color: {
                                type: 3,
                                val: 'phClr',
                              },
                              mods: [
                                {
                                  name: 'tint',
                                  val: 95000,
                                },
                                {
                                  name: 'satMod',
                                  val: 170000,
                                },
                              ],
                            },
                          },
                          type: 3,
                        },
                      },
                      {
                        fill: {
                          data: {
                            colors: [
                              {
                                color: {
                                  color: {
                                    type: 3,
                                    val: 'phClr',
                                  },
                                  mods: [
                                    {
                                      name: 'tint',
                                      val: 93000,
                                    },
                                    {
                                      name: 'satMod',
                                      val: 150000,
                                    },
                                    {
                                      name: 'shade',
                                      val: 98000,
                                    },
                                    {
                                      name: 'lumMod',
                                      val: 102000,
                                    },
                                  ],
                                },
                                pos: 0,
                              },
                              {
                                color: {
                                  color: {
                                    type: 3,
                                    val: 'phClr',
                                  },
                                  mods: [
                                    {
                                      name: 'tint',
                                      val: 98000,
                                    },
                                    {
                                      name: 'satMod',
                                      val: 130000,
                                    },
                                    {
                                      name: 'shade',
                                      val: 90000,
                                    },
                                    {
                                      name: 'lumMod',
                                      val: 103000,
                                    },
                                  ],
                                },
                                pos: 50000,
                              },
                              {
                                color: {
                                  color: {
                                    type: 3,
                                    val: 'phClr',
                                  },
                                  mods: [
                                    {
                                      name: 'shade',
                                      val: 63000,
                                    },
                                    {
                                      name: 'satMod',
                                      val: 120000,
                                    },
                                  ],
                                },
                                pos: 100000,
                              },
                            ],
                            lin: {
                              angle: 5400000,
                              scale: false,
                            },
                          },
                          type: 4,
                        },
                      },
                    ],
                    fillStyleLst: [
                      {
                        fill: {
                          data: {
                            color: {
                              color: {
                                type: 3,
                                val: 'phClr',
                              },
                              mods: [],
                            },
                          },
                          type: 3,
                        },
                      },
                      {
                        fill: {
                          data: {
                            colors: [
                              {
                                color: {
                                  color: {
                                    type: 3,
                                    val: 'phClr',
                                  },
                                  mods: [
                                    {
                                      name: 'lumMod',
                                      val: 110000,
                                    },
                                    {
                                      name: 'satMod',
                                      val: 105000,
                                    },
                                    {
                                      name: 'tint',
                                      val: 67000,
                                    },
                                  ],
                                },
                                pos: 0,
                              },
                              {
                                color: {
                                  color: {
                                    type: 3,
                                    val: 'phClr',
                                  },
                                  mods: [
                                    {
                                      name: 'lumMod',
                                      val: 105000,
                                    },
                                    {
                                      name: 'satMod',
                                      val: 103000,
                                    },
                                    {
                                      name: 'tint',
                                      val: 73000,
                                    },
                                  ],
                                },
                                pos: 50000,
                              },
                              {
                                color: {
                                  color: {
                                    type: 3,
                                    val: 'phClr',
                                  },
                                  mods: [
                                    {
                                      name: 'lumMod',
                                      val: 105000,
                                    },
                                    {
                                      name: 'satMod',
                                      val: 109000,
                                    },
                                    {
                                      name: 'tint',
                                      val: 81000,
                                    },
                                  ],
                                },
                                pos: 100000,
                              },
                            ],
                            lin: {
                              angle: 5400000,
                              scale: false,
                            },
                          },
                          type: 4,
                        },
                      },
                      {
                        fill: {
                          data: {
                            colors: [
                              {
                                color: {
                                  color: {
                                    type: 3,
                                    val: 'phClr',
                                  },
                                  mods: [
                                    {
                                      name: 'satMod',
                                      val: 103000,
                                    },
                                    {
                                      name: 'lumMod',
                                      val: 102000,
                                    },
                                    {
                                      name: 'tint',
                                      val: 94000,
                                    },
                                  ],
                                },
                                pos: 0,
                              },
                              {
                                color: {
                                  color: {
                                    type: 3,
                                    val: 'phClr',
                                  },
                                  mods: [
                                    {
                                      name: 'satMod',
                                      val: 110000,
                                    },
                                    {
                                      name: 'lumMod',
                                      val: 100000,
                                    },
                                    {
                                      name: 'shade',
                                      val: 100000,
                                    },
                                  ],
                                },
                                pos: 50000,
                              },
                              {
                                color: {
                                  color: {
                                    type: 3,
                                    val: 'phClr',
                                  },
                                  mods: [
                                    {
                                      name: 'lumMod',
                                      val: 99000,
                                    },
                                    {
                                      name: 'satMod',
                                      val: 120000,
                                    },
                                    {
                                      name: 'shade',
                                      val: 78000,
                                    },
                                  ],
                                },
                                pos: 100000,
                              },
                            ],
                            lin: {
                              angle: 5400000,
                              scale: false,
                            },
                          },
                          type: 4,
                        },
                      },
                    ],
                    lnStyleLst: [
                      {
                        algn: 'ctr',
                        cap: 'flat',
                        cmpd: 'sng',
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'phClr',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        join: {
                          limit: 800000,
                          type: 'miter',
                        },
                        prstDash: 'solid',
                        w: 6350,
                      },
                      {
                        algn: 'ctr',
                        cap: 'flat',
                        cmpd: 'sng',
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'phClr',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        join: {
                          limit: 800000,
                          type: 'miter',
                        },
                        prstDash: 'solid',
                        w: 12700,
                      },
                      {
                        algn: 'ctr',
                        cap: 'flat',
                        cmpd: 'sng',
                        fill: {
                          fill: {
                            data: {
                              color: {
                                color: {
                                  type: 3,
                                  val: 'phClr',
                                },
                                mods: [],
                              },
                            },
                            type: 3,
                          },
                        },
                        join: {
                          limit: 800000,
                          type: 'miter',
                        },
                        prstDash: 'solid',
                        w: 19050,
                      },
                    ],
                    name: 'Office',
                  },
                  fontScheme: {
                    majorFont: {
                      cs: {
                        typeface: '',
                      },
                      ea: {
                        typeface: '思源黑体 CN Medium',
                      },
                      extra: {
                        supplementalFontLst: [],
                      },
                      latin: {
                        typeface: '思源黑体 CN Medium',
                      },
                    },
                    minorFont: {
                      cs: {
                        typeface: '',
                      },
                      ea: {
                        typeface: '思源黑体 CN',
                      },
                      extra: {
                        supplementalFontLst: [],
                      },
                      latin: {
                        typeface: '思源黑体 CN',
                      },
                    },
                    name: '思源黑体medium',
                  },
                },
              },
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: {
      opType: 'notesMasters',
    },
    data: 1,
  },
  {
    action: 'insert',
    attributes: {
      opType: 'notes',
    },
    data: 1,
  },
  {
    action: 'insert',
    attributes: {
      opType: 'comments',
    },
    data: 1,
  },
]
