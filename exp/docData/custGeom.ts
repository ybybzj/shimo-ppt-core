export const modocStr = [
  {
    action: 'insert',
    attributes: { opType: 'presentation' },
    data: {
      delta: [
        {
          action: 'insert',
          attributes: { opType: 'Pres' },
          data: {
            object: {
              pres: {
                attrAutoCompressPictures: false,
                sldSz: { cx: 12192000, cy: 6858000 },
                notesSz: { cx: 6858000, cy: 9144000 },
              },
            },
          },
        },
        {
          action: 'insert',
          attributes: { opType: 'PresentationPr' },
          data: { object: { showPr: {} } },
        },
        {
          action: 'insert',
          attributes: { opType: 'TableStyle' },
          data: {
            object: {
              defaultTableStyleId: '{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}',
              styles: {},
            },
          },
        },
        {
          action: 'insert',
          attributes: { opType: 'Slides' },
          data: { object: { s0000000: { order: 0 } } },
        },
        {
          action: 'insert',
          attributes: { opType: 'SlideMaster' },
          data: { object: { slideMasterRefs: ['sm000000'] } },
        },
        {
          action: 'insert',
          attributes: { opType: 'Notes' },
          data: { object: {} },
        },
        {
          action: 'insert',
          attributes: { opType: 'NotesMaster' },
          data: { object: { noteMasterRefs: [] } },
        },
      ],
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'layouts' },
    data: {
      slide: {
        l0000000: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                preserve: true,
                clrMap: { type: 'useMaster' },
                type: 'title',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: { name: '标题幻灯片' } },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000000: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { type: 'ctrTitle' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 1524000,
                                        offY: 1122363,
                                        extX: 9144000,
                                        extY: 2387600,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: { anchor: 'b' } },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 6000 },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000001: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '副标题 2' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 1, type: 'subTitle' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 1524000,
                                        offY: 3602038,
                                        extX: 9144000,
                                        extY: 1655762,
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              marL: 0,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 2400 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 457200,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 2000 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 914400,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1800 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1371600,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 1828800,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2286000,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 2743200,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3200400,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            {
                                              marL: 3657600,
                                              indent: 0,
                                              algn: 'ctr',
                                              defaultRunPr: { fontSize: 1600 },
                                              bullet: {
                                                bulletType: { type: 'none' },
                                              },
                                            },
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版副标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000002: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 10, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{A8B3D44F-58D5-174A-981C-E5721219F8BE}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000003: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 11,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000004: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 12,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: { object: { spPr: {} } },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: { object: {} },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'slidenum',
                                              id:
                                                '{C7E503E5-A455-8A49-B273-08A5E9DAAD2C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'slideMasters' },
    data: {
      slide: {
        sm000000: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: {
                clrMap: {
                  bg1: 'lt1',
                  tx1: 'dk1',
                  bg2: 'lt2',
                  tx2: 'dk2',
                  accent1: 'accent1',
                  accent2: 'accent2',
                  accent3: 'accent3',
                  accent4: 'accent4',
                  accent5: 'accent5',
                  accent6: 'accent6',
                  hlink: 'hlink',
                  folHlink: 'folHlink',
                },
                txStyles: {
                  titleStyle: [
                    {
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 0, type: 'percent' },
                      defaultRunPr: {
                        fontSize: 4400,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mj-lt' },
                        ea: { typeface: '+mj-ea' },
                        cs: { typeface: '+mj-cs' },
                      },
                      bullet: { bulletType: { type: 'none' } },
                    },
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                  ],
                  bodyStyle: [
                    {
                      marL: 228600,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 1000, type: 'point' },
                      defaultRunPr: {
                        fontSize: 2800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 685800,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 2400,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 1143000,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 2000,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 1600200,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 2057400,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 2514600,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 2971800,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 3429000,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    {
                      marL: 3886200,
                      indent: -228600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      lineSpacing: { value: 90000, type: 'percent' },
                      spaceBefore: { value: 500, type: 'point' },
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                      bullet: {
                        bulletType: { type: 'character', char: '•' },
                        bulletTypeface: { type: 'font', typeface: 'Arial' },
                      },
                    },
                    null,
                  ],
                  otherStyle: [
                    {
                      marL: 0,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 457200,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 914400,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 1371600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 1828800,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 2286000,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 2743200,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 3200400,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    {
                      marL: 3657600,
                      algn: 'l',
                      defaultTab: 914400,
                      rtl: false,
                      eaLnBrk: true,
                      latinLnBrk: false,
                      hangingPunct: true,
                      defaultRunPr: {
                        fontSize: 1800,
                        kern: 1200,
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'tx1' },
                              },
                            },
                          },
                        },
                        latin: { typeface: '+mn-lt' },
                        ea: { typeface: '+mn-ea' },
                        cs: { typeface: '+mn-cs' },
                      },
                    },
                    { defaultRunPr: { lang: 'zh-CN' } },
                  ],
                },
                themeRef: 'th000000',
              },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'layoutRefs' },
            data: { object: { layoutRefs: ['l0000000'] } },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: {
                    object: {
                      bg: {
                        bgRef: {
                          idx: 1001,
                          color: { mods: [], color: { type: 3, val: 'bg1' } },
                        },
                      },
                    },
                  },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000005: [
                        {
                          action: 'insert',
                          attributes: { order: 0, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 2, name: '标题占位符 1' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { type: 'title' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 365125,
                                        extX: 10515600,
                                        extY: 1325563,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版标题样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000006: [
                        {
                          action: 'insert',
                          attributes: { order: 1, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 3, name: '文本占位符 2' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: { ph: { idx: 1, type: 'body' } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 1825625,
                                        extX: 10515600,
                                        extY: 4351338,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                          textFit: { type: 'normAutofit' },
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '单击此处编辑母版文本样式',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"lvl":0}',
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '二级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                              pPr: '{"lvl":1}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '三级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                              pPr: '{"lvl":2}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '四级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                              pPr: '{"lvl":3}',
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              kumimoji: true,
                                              lang: 'zh-CN',
                                              altLang: 'en-US',
                                            },
                                            data: '五级',
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                              pPr: '{"lvl":4}',
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000007: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 4, name: '日期占位符 3' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: { idx: 2, sz: 'half', type: 'dt' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 838200,
                                        offY: 6356350,
                                        extX: 2743200,
                                        extY: 365125,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'l',
                                              defaultRunPr: {
                                                fontSize: 1200,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'datetimeFigureOut',
                                              id:
                                                '{A8B3D44F-58D5-174A-981C-E5721219F8BE}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000008: [
                        {
                          action: 'insert',
                          attributes: { order: 3, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 5, name: '页脚占位符 4' },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 3,
                                          sz: 'quarter',
                                          type: 'ftr',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 4038600,
                                        offY: 6356350,
                                        extX: 4114800,
                                        extY: 365125,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'ctr',
                                              defaultRunPr: {
                                                fontSize: 1200,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                      sp000009: [
                        {
                          action: 'insert',
                          attributes: { order: 4, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: {
                                        id: 6,
                                        name: '灯片编号占位符 5',
                                      },
                                      nvUniSpPr: { locks: ['noGrp'] },
                                      nvPr: {
                                        ph: {
                                          idx: 4,
                                          sz: 'quarter',
                                          type: 'sldNum',
                                        },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 8610600,
                                        offY: 6356350,
                                        extX: 2743200,
                                        extY: 365125,
                                      },
                                      geometry: { preset: 'rect', avLst: [] },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          bIns: 45720,
                                          lIns: 91440,
                                          rIns: 91440,
                                          rtlCol: false,
                                          tIns: 45720,
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            {
                                              algn: 'r',
                                              defaultRunPr: {
                                                fontSize: 1200,
                                                fill: {
                                                  fill: {
                                                    type: 3,
                                                    data: {
                                                      color: {
                                                        mods: [
                                                          {
                                                            name: 'tint',
                                                            val: 75000,
                                                          },
                                                        ],
                                                        color: {
                                                          type: 3,
                                                          val: 'tx1',
                                                        },
                                                      },
                                                    },
                                                  },
                                                },
                                              },
                                            },
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US","smtClean":false}',
                                              type: 'slidenum',
                                              id:
                                                '{C7E503E5-A455-8A49-B273-08A5E9DAAD2C}',
                                              paraField: true,
                                            },
                                            data: 1,
                                          },
                                          {
                                            action: 'insert',
                                            attributes: {
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'slides' },
    data: {
      slide: {
        s0000000: [
          {
            action: 'insert',
            attributes: { opType: 'attrs' },
            data: {
              object: { clrMap: { type: 'useMaster' }, layoutRef: 'l0000000' },
            },
          },
          {
            action: 'insert',
            attributes: { opType: 'csld' },
            data: {
              delta: [
                {
                  action: 'insert',
                  attributes: { opType: 'attrs' },
                  data: { object: {} },
                },
                {
                  action: 'insert',
                  attributes: { opType: 'spTree' },
                  data: {
                    slide: {
                      sp000010: [
                        {
                          action: 'insert',
                          attributes: { order: 2, spType: 'sp' },
                          data: {
                            delta: [
                              {
                                action: 'insert',
                                attributes: { opType: 'attrs' },
                                data: { object: {} },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'nvSpPr' },
                                data: {
                                  object: {
                                    nvSpPr: {
                                      cNvPr: { id: 12, name: '任意形状 11' },
                                      nvUniSpPr: {},
                                      nvPr: {},
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'spPr' },
                                data: {
                                  object: {
                                    spPr: {
                                      xfrm: {
                                        offX: 2544896,
                                        offY: 336341,
                                        extX: 3062690,
                                        extY: 1547833,
                                      },
                                      geometry: {
                                        ahLst: [],
                                        cxnLst: [
                                          {
                                            x: 'connsiteX0',
                                            y: 'connsiteY0',
                                            ang: 0,
                                          },
                                          {
                                            x: 'connsiteX1',
                                            y: 'connsiteY1',
                                            ang: 0,
                                          },
                                          {
                                            x: 'connsiteX2',
                                            y: 'connsiteY2',
                                            ang: 0,
                                          },
                                          {
                                            x: 'connsiteX3',
                                            y: 'connsiteY3',
                                            ang: 0,
                                          },
                                        ],
                                        rect: {
                                          l: 'l',
                                          t: 't',
                                          r: 'r',
                                          b: 'b',
                                        },
                                        pathLst: [
                                          {
                                            w: 3062690,
                                            h: 1547833,
                                            cmds: [
                                              {
                                                type: 'moveTo',
                                                x: 0,
                                                y: 181452,
                                              },
                                              {
                                                type: 'cubicBezTo',
                                                pts: [
                                                  { x: 512284, y: 872760 },
                                                  { x: 1024569, y: 1564068 },
                                                  { x: 1410159, y: 1547543 },
                                                ],
                                              },
                                              {
                                                type: 'cubicBezTo',
                                                pts: [
                                                  { x: 1795750, y: 1531018 },
                                                  { x: 2038121, y: 287948 },
                                                  { x: 2313543, y: 82300 },
                                                ],
                                              },
                                              {
                                                type: 'cubicBezTo',
                                                pts: [
                                                  { x: 2588965, y: -123348 },
                                                  { x: 2825827, y: 95153 },
                                                  { x: 3062690, y: 313654 },
                                                ],
                                              },
                                            ],
                                          },
                                        ],
                                        avLst: [],
                                        gdLst: [
                                          {
                                            name: 'connsiteX0',
                                            formula: '*/ 0 w 3062690',
                                          },
                                          {
                                            name: 'connsiteY0',
                                            formula: '*/ 181452 h 1547833',
                                          },
                                          {
                                            name: 'connsiteX1',
                                            formula: '*/ 1410159 w 3062690',
                                          },
                                          {
                                            name: 'connsiteY1',
                                            formula: '*/ 1547543 h 1547833',
                                          },
                                          {
                                            name: 'connsiteX2',
                                            formula: '*/ 2313543 w 3062690',
                                          },
                                          {
                                            name: 'connsiteY2',
                                            formula: '*/ 82300 h 1547833',
                                          },
                                          {
                                            name: 'connsiteX3',
                                            formula: '*/ 3062690 w 3062690',
                                          },
                                          {
                                            name: 'connsiteY3',
                                            formula: '*/ 313654 h 1547833',
                                          },
                                        ],
                                      },
                                      fill: { fill: { type: 2 } },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'style' },
                                data: {
                                  object: {
                                    lnRef: {
                                      idx: 2,
                                      color: {
                                        mods: [{ name: 'shade', val: 50000 }],
                                        color: { type: 3, val: 'accent1' },
                                      },
                                    },
                                    fillRef: {
                                      idx: 1,
                                      color: {
                                        mods: [],
                                        color: { type: 3, val: 'accent1' },
                                      },
                                    },
                                    effectRef: {
                                      idx: 0,
                                      color: {
                                        mods: [],
                                        color: { type: 3, val: 'accent1' },
                                      },
                                    },
                                    fontRef: {
                                      idx: 'minor',
                                      color: {
                                        mods: [],
                                        color: { type: 3, val: 'lt1' },
                                      },
                                    },
                                  },
                                },
                              },
                              {
                                action: 'insert',
                                attributes: { opType: 'txBody' },
                                data: {
                                  delta: [
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'bodyPr' },
                                      data: {
                                        object: {
                                          anchor: 'ctr',
                                          rtlCol: false,
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'listStyle' },
                                      data: {
                                        object: {
                                          listStyle: [
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                            null,
                                          ],
                                        },
                                      },
                                    },
                                    {
                                      action: 'insert',
                                      attributes: { opType: 'docContent' },
                                      data: {
                                        delta: [
                                          {
                                            action: 'insert',
                                            attributes: {
                                              pPr: '{"algn":"ctr"}',
                                              rPr:
                                                '{"kumimoji":true,"lang":"zh-CN","altLang":"en-US"}',
                                              paraEnd: true,
                                            },
                                            data: 1,
                                          },
                                        ],
                                      },
                                    },
                                  ],
                                },
                              },
                            ],
                          },
                        },
                      ],
                    },
                  },
                },
              ],
            },
          },
        ],
      },
    },
  },
  {
    action: 'insert',
    attributes: { opType: 'themes' },
    data: {
      slide: {
        th000000: [
          {
            action: 'insert',
            data: {
              object: {
                name: 'Office 主题​​',
                themeElements: {
                  clrScheme: {
                    name: 'Office',
                    lt1: {
                      mods: [],
                      color: { type: 4, val: 'window', lastClr: 'FFFFFF' },
                    },
                    lt2: { mods: [], color: { type: 1, val: 'E7E6E6' } },
                    dk1: {
                      mods: [],
                      color: { type: 4, val: 'windowText', lastClr: '000000' },
                    },
                    dk2: { mods: [], color: { type: 1, val: '44546A' } },
                    hlink: { mods: [], color: { type: 1, val: '0563C1' } },
                    folHlink: { mods: [], color: { type: 1, val: '954F72' } },
                    accent1: { mods: [], color: { type: 1, val: '4472C4' } },
                    accent2: { mods: [], color: { type: 1, val: 'ED7D31' } },
                    accent3: { mods: [], color: { type: 1, val: 'A5A5A5' } },
                    accent4: { mods: [], color: { type: 1, val: 'FFC000' } },
                    accent5: { mods: [], color: { type: 1, val: '5B9BD5' } },
                    accent6: { mods: [], color: { type: 1, val: '70AD47' } },
                  },
                  fontScheme: {
                    name: 'Office',
                    minorFont: {
                      latin: { typeface: '等线' },
                      ea: { typeface: '' },
                      cs: { typeface: '' },
                    },
                    majorFont: {
                      latin: { typeface: '等线 Light' },
                      ea: { typeface: '' },
                      cs: { typeface: '' },
                    },
                  },
                  fmtScheme: {
                    name: 'Office',
                    fillStyleLst: [
                      {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              mods: [],
                              color: { type: 3, val: 'phClr' },
                            },
                          },
                        },
                      },
                      {
                        fill: {
                          type: 4,
                          data: {
                            colors: [
                              {
                                pos: 0,
                                color: {
                                  mods: [
                                    { name: 'lumMod', val: 110000 },
                                    { name: 'satMod', val: 105000 },
                                    { name: 'tint', val: 67000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                              {
                                pos: 50000,
                                color: {
                                  mods: [
                                    { name: 'lumMod', val: 105000 },
                                    { name: 'satMod', val: 103000 },
                                    { name: 'tint', val: 73000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                              {
                                pos: 100000,
                                color: {
                                  mods: [
                                    { name: 'lumMod', val: 105000 },
                                    { name: 'satMod', val: 109000 },
                                    { name: 'tint', val: 81000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                            ],
                            lin: { angle: 5400000, scale: false },
                          },
                        },
                      },
                      {
                        fill: {
                          type: 4,
                          data: {
                            colors: [
                              {
                                pos: 0,
                                color: {
                                  mods: [
                                    { name: 'satMod', val: 103000 },
                                    { name: 'lumMod', val: 102000 },
                                    { name: 'tint', val: 94000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                              {
                                pos: 50000,
                                color: {
                                  mods: [
                                    { name: 'satMod', val: 110000 },
                                    { name: 'lumMod', val: 100000 },
                                    { name: 'shade', val: 100000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                              {
                                pos: 100000,
                                color: {
                                  mods: [
                                    { name: 'lumMod', val: 99000 },
                                    { name: 'satMod', val: 120000 },
                                    { name: 'shade', val: 78000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                            ],
                            lin: { angle: 5400000, scale: false },
                          },
                        },
                      },
                    ],
                    lnStyleLst: [
                      {
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'phClr' },
                              },
                            },
                          },
                        },
                        prstDash: 'solid',
                        join: { type: 'miter', limit: 800000 },
                        w: 6350,
                        cmpd: 'sng',
                        cap: 'flat',
                        algn: 'ctr',
                      },
                      {
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'phClr' },
                              },
                            },
                          },
                        },
                        prstDash: 'solid',
                        join: { type: 'miter', limit: 800000 },
                        w: 12700,
                        cmpd: 'sng',
                        cap: 'flat',
                        algn: 'ctr',
                      },
                      {
                        fill: {
                          fill: {
                            type: 3,
                            data: {
                              color: {
                                mods: [],
                                color: { type: 3, val: 'phClr' },
                              },
                            },
                          },
                        },
                        prstDash: 'solid',
                        join: { type: 'miter', limit: 800000 },
                        w: 19050,
                        cmpd: 'sng',
                        cap: 'flat',
                        algn: 'ctr',
                      },
                    ],
                    bgFillStyleLst: [
                      {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              mods: [],
                              color: { type: 3, val: 'phClr' },
                            },
                          },
                        },
                      },
                      {
                        fill: {
                          type: 3,
                          data: {
                            color: {
                              mods: [
                                { name: 'tint', val: 95000 },
                                { name: 'satMod', val: 170000 },
                              ],
                              color: { type: 3, val: 'phClr' },
                            },
                          },
                        },
                      },
                      {
                        fill: {
                          type: 4,
                          data: {
                            colors: [
                              {
                                pos: 0,
                                color: {
                                  mods: [
                                    { name: 'tint', val: 93000 },
                                    { name: 'satMod', val: 150000 },
                                    { name: 'shade', val: 98000 },
                                    { name: 'lumMod', val: 102000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                              {
                                pos: 50000,
                                color: {
                                  mods: [
                                    { name: 'tint', val: 98000 },
                                    { name: 'satMod', val: 130000 },
                                    { name: 'shade', val: 90000 },
                                    { name: 'lumMod', val: 103000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                              {
                                pos: 100000,
                                color: {
                                  mods: [
                                    { name: 'shade', val: 63000 },
                                    { name: 'satMod', val: 120000 },
                                  ],
                                  color: { type: 3, val: 'phClr' },
                                },
                              },
                            ],
                            lin: { angle: 5400000, scale: false },
                          },
                        },
                      },
                    ],
                  },
                },
              },
            },
          },
        ],
      },
    },
  },
]
