import { Delta } from './modoc'
declare const window: any
window.Delta = Delta

function insert(data, attributes = {}) {
  return {
    action: 'insert',
    data,
    attributes,
  }
}

function retain(data, attributes = {}) {
  return {
    action: 'retain',
    data,
    attributes,
  }
}

function remove(data, attributes = {}) {
  return {
    action: 'remove',
    data,
    attributes,
  }
}

window.insert = insert
window.retain = retain
window.remove = remove
