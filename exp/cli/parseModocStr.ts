import {

  printModoc,
} from '../../src/re/export/modoc'
import { modocStr } from '../docData/bigData'

import { Delta } from '../modoc'


console.log(
  printModoc(new Delta(modocStr as any))
)
