import { IStringObject } from '../../type'

/**
 * initialToObjectKey 只能新增，不能修改已有数据，否则会造成不兼容
 */
export const initialToObjectKey = {
  A: 'attachment',
  C: 'cell',
  I: 'image',
  D: 'delta',
  T: 'table',
  P: 'presentation',
  S: 'slide',
  M: 'mention',
  N: 'inline-break',
  V: 'divide',
  U: 'upload-placeholder-attachment',
  G: 'gallery',
  B: 'gallery-block',
  Z: 'mindmap',
  O: 'object',
  F: 'form',
} as IStringObject

export const objectKeyToInitial = Object.keys(initialToObjectKey).reduce(
  (ret, key) => {
    ret[initialToObjectKey[key]] = key
    return ret
  },
  {}
) as IStringObject
