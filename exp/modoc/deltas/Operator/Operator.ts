import { Pool } from '../../Pool/Pool'
import { IStringObject, OperatorAction, OperatorData } from '../../type'
import { IDataHandler } from '../dataHandlers/IDataHandler'
import { PackedOperatorData } from './type'
import { invert, merge, transform } from '../utils/objectOT'
import { objectKeyToInitial } from './objectKeys'

export interface IPooledOperator {
  action: OperatorAction
  data: OperatorData
  packedAttributes?: string
  pool?: Pool
}

const EMPTY_DELTA_STR = '0:'
export class Operator {
  private static dataHandlers: Record<string, IDataHandler> = {}
  /**
   * 获取没有解析 object 类型的属性。本质是直接调用 Operator#getUnresolvedAttributes()，
   * 不过为了兼容 <1.0.0 的 modoc 实例，会 fallback 到 Operator#getAttributes() 方法。
   *
   * @private
   * @static
   * @param {Operator} op
   * @returns {IAttributeObject}
   * @memberof Operator
   */
  readonly action: OperatorAction
  readonly data: PackedOperatorData
  readonly dataType: string
  private pool?: Pool
  private packedAttributes?: string
  private packedData?: string
  static isOperator(arg: any): arg is Operator {
    return typeof arg.getAttributes === 'function'
  }
  static registerDataHandler(dataKey: string, handler: IDataHandler) {
    Operator.dataHandlers[dataKey] = handler
  }

  /**
   * 获取没有解析 object 类型的属性。本质是直接调用 Operator#getUnresolvedAttributes()，
   * 不过为了兼容 <1.0.0 的 modoc 实例，会 fallback 到 Operator#getAttributes() 方法。
   *
   * @private
   * @static
   * @param {Operator} op
   * @returns {IAttributeObject}
   * @memberof Operator
   */
  private static getUnresolvedAttributes(op) {
    if (typeof op.getUnresolvedAttributes === 'function') {
      return op.getUnresolvedAttributes()
    }
    // < 1.0.0 的 Operator#getAttributes() 返回的类型是 IAttributeObject
    return op.getAttributes()
  }
  /**
    * 获取 Operator 的长度。
    * Operator 的长度规则为：
    * 1. 对于字符串类型的 data，长度为字符串长度；
    * 2. 对于数字类型的 data，长度为数字本身；
    * 3. 对于其他类型的 data，长度为 1。
    *
    * @readonly
    * @type {number}
    * @memberof IOperator
    */
  get length() {
    if (typeof this.data === 'string') {
      return this.data.length
    }
    if (typeof this.data === 'number') {
      return this.data
    }
    return 1
  }
  constructor(op: IPooledOperator, packedData?: string) {
    this.action = op.action
    const data = op.data
    if (data === null || data === undefined) {
      throw new Error('"data" is required for an Operator')
    }
    if (typeof data === 'object') {
      const objectKey = Object.keys(data)[0]
      const dataValue = data[objectKey]
      if (typeof dataValue === 'string') {
        this.data = data as IStringObject
        this.dataType = objectKey
      } else {
        const handler = Operator.dataHandlers[objectKey]
        if (!handler) {
          throw new Error('Unsupported nested data type "' + objectKey + '"')
        }
        const packedValue = handler.pack(dataValue, this.action === 'insert')
        if (objectKey === 'delta' && packedValue === EMPTY_DELTA_STR) {
          this.data = 1
          this.dataType = 'number'
        } else {
          this.data = { [objectKey]: packedValue }
          this.dataType = objectKey
        }
      }
    } else {
      this.data = data
      this.dataType = typeof data
    }
    this.packedAttributes = op.packedAttributes
    this.pool = op.pool
    this.packedData = packedData
  }

  isEmpty() {
    return (
      this.action === 'retain' &&
      (this.packedAttributes === '' || this.packedAttributes === undefined) &&
      typeof this.data === 'number'
    )
  }
  getParsedData() {
    const handler = Operator.dataHandlers[this.dataType]
    if (handler === undefined) {
      throw new Error('Unknown handler for ' + this.dataType)
    }
    return handler.parse(this.getCustomData())
  }
  hasAttributes() {
    return this.packedAttributes !== undefined && this.packedAttributes !== ''
  }
  getAttributes() {
    return this.pool !== undefined && this.hasAttributes()
      ? this.pool.unpackAttributes(this.packedAttributes!)
      : {}
  }
  stringify() {
    if (this.packedData === undefined) {
      const data = this.data
      let str: undefined | string = void 0
      switch (this.action) {
        case 'insert':
          str = '!'
          break
        case 'retain':
          str = '@'
          break
        default:
          str = '#'
      }
      if (typeof data === 'number') {
        str += data.toString(36) + '#'
      } else if (typeof data === 'string') {
        str += data.length.toString(36) + '!' + data
      } else {
        const dataType = this.dataType
        if (dataType === undefined) {
          throw new Error('ASSERT: dataType === undefined')
        }
        const initial = objectKeyToInitial[dataType]
        if (!initial) {
          throw new Error('Unsupport object data type: "' + dataType + '"')
        }
        const combine = '' + initial + data[dataType]
        str += combine.length.toString(36) + '@' + combine
      }
      this.packedData = str
    }
    const packedResult =
      this.packedAttributes === undefined
        ? this.packedData
        : this.packedData + this.packedAttributes
    return packedResult.length.toString(36) + packedResult
  }
  merge(op: Operator): Operator | null {
    // First, the action should be same
    if (this.action !== op.action) {
      return null
    }
    // Section, both data are string or number
    const dataType = typeof this.data
    if (dataType === 'object' || dataType !== typeof op.data) {
      return null
    }
    const hasAttributes = this.hasAttributes()
    if (hasAttributes !== op.hasAttributes()) {
      return null
    }
    if (hasAttributes) {
      if (this.pool === op.pool) {
        if (this.packedAttributes !== op.packedAttributes) {
          return null
        }
      } else if (
        !isIAttributeObjectsEqual(
          this.getUnresolvedAttributes(),
          Operator.getUnresolvedAttributes(op)
        )
      ) {
        return null
      }
    }
    return new Operator({
      action: this.action,
      data: (this.data as number) + (op.data as number),
      pool: this.pool,
      packedAttributes: this.packedAttributes,
    })
  }
  getCustomData() {
    switch (typeof this.data) {
      case 'string':
      case 'number':
        throw new Error('Cannot get custom data from a simple operator')
      default:
        return this.data[this.dataType]
    }
  }
  compose(other: Operator, isDocument: boolean): Operator {
    if (this.action === 'remove') {
      throw new Error('Removing operator cannot compose other operators')
    }
    if (other.action !== 'retain') {
      throw new Error('Only retaining operator can be composed')
    }
    let data
    let otherHasCellValue = false
    let packedAttributes
    if (other.hasAttributes()) {
      if (this.pool === undefined) {
        throw new Error('this.pool is undefined when composing')
      }
      const otherAttributes = Operator.getUnresolvedAttributes(other)
      otherHasCellValue = otherAttributes.hasOwnProperty('cellValue')
      const attributes = merge(
        this.getUnresolvedAttributes(),
        otherAttributes,
        !isDocument
      )
      packedAttributes = this.pool.packAttributes(attributes)
    } else {
      packedAttributes = this.packedAttributes
    }
    if (otherHasCellValue) {
      /*
              {action: insert, data: 2}.compose({action: retain, data: 2, attributes: {cellValue: 'abc'}})
              => {action: insert, data: 2, attributes: {cellValue: 'abc'}}

              {action: insert, data: {delta: xxx}}.compose({action: retain, data: 1, attributes: {cellValue: 'abc'}})
              => {action: insert, data: 1, attributes: {cellValue: 'abc'}}
            */
      data = other.dataType === 'number' ? other.data : 1
    } else if (this.dataType === 'number') {
      data = other.data
    } else if (other.dataType === 'number') {
      data = this.data
    } else {
      if (this.dataType === undefined) {
        throw new Error(
          'Missing "dataType" when composing ' +
          this.data +
          ' with ' +
          other.data
        )
      }
      if (this.dataType !== other.dataType) {
        throw new Error(
          'Expect ' +
          other.dataType +
          ' to equal ' +
          this.dataType +
          ' when composing ' +
          this.data +
          ' with ' +
          other.data
        )
      }
      const handler = Operator.dataHandlers[this.dataType]
      if (handler === undefined) {
        throw new Error('Unknown handler for ' + this.dataType)
      }
      const ret = handler.compose(
        this.getCustomData(),
        other.getCustomData(),
        isDocument
      )
      data =
        this.dataType === 'delta' && ret === EMPTY_DELTA_STR
          ? 1
          : { [this.dataType]: ret }
    }
    return new Operator({
      action: this.action,
      data: data,
      pool: this.pool,
      packedAttributes: packedAttributes,
    })
  }
  transform(other: Operator, priority?: boolean): Operator {
    if (priority === void 0) {
      priority = false
    }
    if (this.action !== 'retain') {
      throw new Error('Only retaining operator can transform other operators')
    }
    if (other.action !== 'retain') {
      throw new Error('Only retaining operator can be transformed')
    }
    let data
    if (this.dataType === 'number' || other.dataType === 'number') {
      data = other.data
    } else {
      if (this.dataType === undefined) {
        throw new Error(
          'Missing "dataType" when transforming ' +
          this.data +
          ' with ' +
          other.data
        )
      }
      if (this.dataType !== other.dataType) {
        throw new Error(
          'Expect ' +
          other.dataType +
          ' to equal ' +
          this.dataType +
          ' when transforming ' +
          this.data +
          ' with ' +
          other.data
        )
      }
      const handler = Operator.dataHandlers[this.dataType]
      if (handler === undefined) {
        throw new Error('Unknown handler for ' + this.dataType)
      }
      const ret = handler.transform(
        this.getCustomData(),
        other.getCustomData(),
        priority
      )
      data =
        this.dataType === 'delta' && ret === EMPTY_DELTA_STR
          ? 1
          : { [this.dataType]: ret }
    }
    let packedAttributes
    if (other.hasAttributes()) {
      const attributes = transform(
        this.getUnresolvedAttributes(),
        Operator.getUnresolvedAttributes(other),
        priority
      )
      if (other.pool === undefined) {
        throw new Error('ASSERT: other.pool === undefined when transforming')
      }
      packedAttributes = other.pool.packAttributes(attributes)
    } else {
      packedAttributes = ''
    }
    return new Operator({
      action: this.action,
      data: data,
      pool: other.pool,
      packedAttributes: packedAttributes,
    })
  }
  invert(other: Operator, overrideData?: OperatorData): Operator {
    if (this.action !== 'insert') {
      throw new Error(
        'invert should only apply to the "insert" action (' +
        this.dataType +
        ')'
      )
    }
    if (other.action !== 'retain') {
      throw new Error('Only retaining operator can be inverted')
    }
    let data
    let otherHasCellValue = false
    let packedAttributes
    if (other.hasAttributes()) {
      const otherAttributes = Operator.getUnresolvedAttributes(other)
      otherHasCellValue = otherAttributes.hasOwnProperty('cellValue')
      const attributes = invert(this.getUnresolvedAttributes(), otherAttributes)
      if (other.pool === undefined) {
        throw new Error('ASSERT: other.pool === undefined when inverting')
      }
      packedAttributes = other.pool.packAttributes(attributes)
    } else {
      packedAttributes = ''
    }
    if (otherHasCellValue) {
      data = this.data
    } else if (overrideData !== undefined) {
      data = overrideData
    } else if (typeof other.data === 'number') {
      data = other.data
    } else if (typeof this.data === 'number' && other.dataType !== 'delta') {
      data = this.data
    } else {
      if (this.dataType === undefined) {
        throw new Error(
          'Missing "dataType" when inverting ' +
          this.data +
          ' with ' +
          other.data
        )
      }
      if (this.dataType === 'number' && other.dataType === 'delta') {
        const handler = Operator.dataHandlers.delta
        if (handler === undefined) {
          throw new Error('Unknown handler for "delta"')
        }
        data = { delta: handler.invert(EMPTY_DELTA_STR, other.getCustomData()) }
      } else {
        if (this.dataType !== other.dataType) {
          throw new Error(
            'Expect ' +
            other.dataType +
            ' to equal ' +
            this.dataType +
            ' when inverting ' +
            this.data +
            ' with ' +
            other.data
          )
        }
        const handler = Operator.dataHandlers[this.dataType]
        if (handler === undefined) {
          throw new Error('Unknown handler for ' + this.dataType)
        }
        const ret = handler.invert(this.getCustomData(), other.getCustomData())
        data =
          this.dataType === 'delta' && ret === EMPTY_DELTA_STR
            ? 1
            : { [this.dataType]: ret }
      }
    }
    return new Operator({
      data: data,
      action: 'retain',
      pool: other.pool,
      packedAttributes: packedAttributes,
    })
  }
  toJSON() {
    const plain: Record<string, any> = { action: this.action }
    if (this.hasAttributes()) {
      plain.attributes = this.getAttributes()
    }
    if (this.dataType === 'number' || this.dataType === 'string') {
      plain.data = this.data
    } else {
      const handler = Operator.dataHandlers[this.dataType]
      const value = this.getCustomData()
      plain.data = {
        [this.dataType]: handler ? handler.toJSON(value) : value,
      }
    }
    return plain
  }

  explain(prefix?: string): string {
    if (prefix === void 0) {
      prefix = ''
    }
    const len = this.stringify().length
    let ret =
      prefix +
      '[' +
      this.dataType +
      '(' +
      this.action +
      ')] #' +
      (len - String(len).length) +
      ':\n'
    if (this.dataType === 'number' || this.dataType === 'string') {
      ret += prefix + this.data
    } else {
      const handler = Operator.dataHandlers[this.dataType]
      const value = this.getCustomData()
      ret += handler ? handler.explain(prefix + '  ', value) : value
    }
    return (
      ret + '\n' + prefix + 'PackedAttributes:' + this.packedAttributes + '\n'
    )
  }
  private getUnresolvedAttributes() {
    return this.pool !== undefined && this.hasAttributes()
      ? this.pool.unpackAttributes(this.packedAttributes!, false)
      : {}
  }
}
function isIAttributeObjectsEqual(a: object, b: object) {
  if (a === b) {
    return true
  }
  const keys = Object.keys(a)
  const length = keys.length
  if (length !== Object.keys(b).length) {
    return false
  }
  for (let i = 0; i < length; i++) {
    if (a[keys[i]] !== b[keys[i]]) {
      return false
    }
  }
  return true
}
