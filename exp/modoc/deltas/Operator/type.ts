import { Operator } from './Operator'
import { IStringObject } from '../../type'
export type IOperatorConstructor = typeof Operator
export type PackedOperatorData = string | number | IStringObject;
export interface IDataHandlerRegister {
    registerDataHandler: typeof Operator.registerDataHandler
}