import { Delta } from './Delta/Delta'
import { DeltaIter } from './Delta/DeltaIter'
export type IDeltaIter = DeltaIter
export type IOperator = Operator
export { Delta }
import { Operator } from './Operator/Operator'
import { registerDataHandlers } from './dataHandlers/registerDataHandlers'
registerDataHandlers(Operator, Delta, Operator)
