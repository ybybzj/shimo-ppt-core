import { Delta } from '../Delta/Delta'
import { AbstractDataHandler } from './AbstractDataHandler'
import { PossibleDeltaInput, possibleDeltaInputToString } from './helpers'

export class DeltaListDataHandler extends AbstractDataHandler {
  compose(a: string, b: string, isDocument: boolean) {
    const aItems = this.parse(a).slice(0)
    const bItems = this.parse(b).slice(0)
    const len = Math.max(aItems.length, bItems.length)
    const ret = new Array(len)
    for (let i = 0; i < len; i++) {
      const aItem = aItems[i]
      const bItem = bItems[i]
      ret[i] = aItem
        ? bItem
          ? aItem.compose(bItem, isDocument)
          : aItem
        : bItem
    }
    return this.pack(ret)
  }
  transform(a: string, b: string, priority: boolean) {
    const aItems = this.parse(a).slice(0)
    const bItems = this.parse(b).slice(0)
    const len = Math.max(aItems.length, bItems.length)
    const ret = new Array(len)
    for (let i = 0; i < len; i++) {
      const aItem = aItems[i]
      const bItem = bItems[i]
      ret[i] = aItem
        ? bItem
          ? aItem.transform(bItem, priority)
          : undefined
        : bItem
    }
    return this.pack(ret)
  }
  invert(a: string, b: string) {
    const aItems = this.parse(a).slice(0)
    const bItems = this.parse(b).slice(0)
    const len = Math.max(aItems.length, bItems.length)
    const ret = new Array(len)
    for (let i = 0; i < len; i++) {
      const aItem = aItems[i]
      const bItem = bItems[i]
      ret[i] = aItem ? (bItem ? aItem.invert(bItem) : undefined) : bItem
    }
    return this.pack(ret)
  }
  parse(value: string) {
    let cursor = 0
    const ret: Array<Delta | undefined> = []
    while (cursor < value.length) {
      const index = value.indexOf('$', cursor)
      if (index === -1) {
        break
      }
      const length_1 = parseInt(value.substring(cursor, index), 36)
      const endIndex = index + 1 + length_1
      const delta =
        length_1 === 0
          ? undefined
          : new this.deltaConstructor(value.substring(index + 1, endIndex))
      cursor = endIndex
      ret.push(delta)
    }
    return ret
  }
  pack(items: PossibleDeltaInput[]) {
    let ret = ''
    for (let i = 0, items_l = items.length; i < items_l; i++) {
      const item = items[i]
      const packed = possibleDeltaInputToString(this.deltaConstructor, item)
      ret += packed.length.toString(36) + '$' + packed
    }
    return ret
  }
  toJSON(value: string) {
    const items = this.parse(value)
    return items.map(function (item) {
      return item ? item.toJSON() : undefined
    })
  }
  explain(prefix: string, value: string) {
    let ret = prefix + 'List:\n'
    const items = this.parse(value)
    for (let i = 0; i < items.length; i++) {
      const item = items[i]
      ret += prefix + ('Item #' + i + ':\n')
      ret += item ? item.explain(prefix) : '[EMPTY]'
    }
    return ret
  }
}
