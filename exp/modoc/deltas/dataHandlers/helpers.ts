import { IOperatorPlainObject } from '../../type'
import { Delta } from '../Delta/Delta'
import { IDeltaConstructor } from '../Delta/type'


export type PossibleDeltaInput =
  | undefined
  | null
  | Delta
  | IOperatorPlainObject[]
export function possibleDeltaInputToString(
  deltaConstructor: IDeltaConstructor,
  value?: PossibleDeltaInput
) {
  if (value === undefined || value === null) {
    return ''
  }
  if (Array.isArray(value)) {
    if (value.length === 0) {
      return ''
    }
    const delta = new deltaConstructor(value)
    return delta.isEmpty() ? '' : delta.stringify()
  }
  return value.isEmpty() ? '' : value.stringify()
}

