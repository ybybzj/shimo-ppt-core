export interface IDataHandler {
    compose(me: string, other: string, isDocument: boolean): string;
    transform(me: string, other: string, priority: boolean): string;
    invert(me: string, other: string): string;
    parse(value: string): object;
    pack(value: object, isDocument: boolean): string;
    toJSON(value: string): any;
    explain(prefix: string, value: string): string;
}
