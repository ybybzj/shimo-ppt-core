import { Delta } from '../Delta/Delta'
import { AbstractDataHandler } from './AbstractDataHandler'

export class DeltaDataHandler extends AbstractDataHandler {
  compose(me: string, other: string, isDocument: boolean) {
    return this.parse(me).compose(this.parse(other), isDocument).stringify()
  }
  transform(me: string, other: string, priority: boolean) {
    return this.parse(me).transform(this.parse(other), priority).stringify()
  }
  invert(me: string, other: string) {
    return this.parse(me).invert(this.parse(other)).stringify()
  }
  parse(value: string) {
    return new this.deltaConstructor(value)
  }
  pack(value: object) {
    if (Array.isArray(value)) {
      return new this.deltaConstructor(value).stringify()
    }
    if (Delta.isDelta(value)) {
      return value.stringify()
    }
    throw new Error('Unsupported delta value for packing ' + value)
  }
  toJSON(value: string) {
    return this.parse(value).toJSON()
  }
  explain(prefix: string, value: string) {
    return this.parse(value).explain(prefix)
  }
}
