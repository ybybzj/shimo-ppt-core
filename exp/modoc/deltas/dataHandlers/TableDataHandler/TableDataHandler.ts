import { OperatorList } from "../../../OperatorList"
import { Pool } from "../../../Pool/Pool"
import { Delta } from "../../Delta/Delta"
import { Operator } from "../../Operator/Operator"
import { AbstractDataHandler } from "../AbstractDataHandler"
import { PossibleDeltaInput, possibleDeltaInputToString } from "../helpers"
import { reindexInnerDeltas, invertInnerDeltas, DeltaToIndexModifiers, reindexDelta } from "./helpers"

export interface ITableObject {
    rows?: PossibleDeltaInput
    cols?: PossibleDeltaInput
}

export class TableDataHandler extends AbstractDataHandler {
    pack({ rows, cols }: ITableObject) {
        const rowDelta = possibleDeltaInputToString(this.deltaConstructor, rows)
        const columnDelta = possibleDeltaInputToString(this.deltaConstructor, cols)
        return rowDelta.length.toString(36) + '$' + rowDelta + columnDelta
    }
    toJSON(value: string): object {
        const _a = this.parse(value), rows = _a[0], cols = _a[1]
        return {
            rows: rows === null ? null : rows.toJSON(),
            cols: cols === null ? null : cols.toJSON()
        }
    }
    explain(prefix: string, value: string) {
        let ret = prefix + 'Rows:\n'
        const _a = this.parse(value), rows = _a[0], cols = _a[1]
        ret += rows === null ? '[EMPTY]' : rows.explain(prefix)
        ret += prefix + 'Cols:\n'
        ret += cols === null ? '[EMPTY]' : cols.explain(prefix)
        return ret
    };
    parse(value: string): [Delta | null, Delta | null] {
        const index = value.indexOf('$')
        const rowLength = parseInt(value.substring(0, index), 36)
        const rowEndIndex = index + 1 + rowLength
        const rowDelta = rowLength === 0
            ? null
            : new this.deltaConstructor(value.substring(index + 1, rowEndIndex))
        const colDelta = value.length === rowEndIndex
            ? null
            : new this.deltaConstructor(value.substring(rowEndIndex))
        return [rowDelta, colDelta]
    };
    compose(a: string, b: string, isDocument: boolean): string {
        const _a = this.parse(a), aRows = _a[0], aCols = _a[1]
        const _b = this.parse(b), bRows = _b[0], bCols = _b[1]
        const rows = bRows === null
            ? aRows
            : (aRows === null ? bRows : aRows.compose(bRows, isDocument))
        const sourceCols = reindexInnerDeltas(aCols, bRows)
        const cols = bCols === null
            ? sourceCols
            : (sourceCols === null ? bCols : sourceCols.compose(bCols, isDocument))
        return this.pack({ rows: rows, cols: cols })
    };
    transform(a: string, b: string, priority: boolean): string {
        const _a = this.parse(a), aRows = _a[0], aCols = _a[1]
        const _b = this.parse(b), bRows = _b[0], bCols = _b[1]
        const rows = aRows === null || bRows === null
            ? bRows
            : aRows.transform(bRows, priority)
        const reversalRows = aRows === null || bRows === null
            ? aRows
            : bRows.transform(aRows, !priority)
        const sourceCols = reindexInnerDeltas(aCols, rows)
        const targetCols = reindexInnerDeltas(bCols, reversalRows)
        const cols = sourceCols === null || targetCols === null
            ? targetCols
            : sourceCols.transform(targetCols, priority)
        return this.pack({ rows: rows, cols: cols })
    };
    invert(a: string, b: string) {
        const _this = this
        const _a = this.parse(a), aRows = _a[0], aCols = _a[1]
        const _b = this.parse(b), bRows = _b[0], bCols = _b[1]
        const rows = bRows && (aRows === null ? new this.deltaConstructor([]) : aRows).invert(bRows)
        let cols = invertInnerDeltas(aCols, bRows)
        if (bCols !== null) {
            const sourceCols = aCols === null ? new Delta([]) : aCols
            if (cols) {
                const colModifiers = DeltaToIndexModifiers(bCols)
                cols = reindexDelta(cols, colModifiers)
            }
            const targetCols = reindexInnerDeltas(bCols, rows)
            if (targetCols === null) {
                throw new Error('ASSERT: targetCols === null')
            }
            const invertedCols = sourceCols.invert(targetCols, function (thisOp, otherOp) {
                if (otherOp.dataType !== 'delta') {
                    return thisOp.invert(otherOp)
                }
                const pool = new Pool()
                const opl = new OperatorList([])
                const thisIter = thisOp.dataType === 'number'
                    ? null
                    : new _this.deltaConstructor(thisOp.getCustomData()).getIter({ targetPool: pool })
                const otherIter = new _this.deltaConstructor(otherOp.getCustomData()).getIter({ targetPool: pool })
                while (otherIter.hasNext()) {
                    let thisInnerOp: Operator
                    let otherInnerOp: Operator
                    if (thisIter !== null && thisIter.hasNext()) {
                        const length_1 = Math.min(thisIter.peekLength(), otherIter.peekLength())
                        thisInnerOp = thisIter.next(length_1, 'insert')
                        otherInnerOp = otherIter.next(length_1)
                    }
                    else {
                        const length_2 = otherIter.peekLength()
                        thisInnerOp = new _this.operatorConstructor({ action: 'insert', data: length_2 })
                        otherInnerOp = otherIter.next()
                    }
                    opl.push(thisInnerOp.invert(otherInnerOp))
                }
                return thisOp.invert(otherOp, {
                    delta: new _this.deltaConstructor(opl.stringify(true), pool).stringify()
                })
            })
            cols = cols ? cols.compose(invertedCols) : invertedCols
        }
        return this.pack({ rows: rows, cols: cols })
    };
}
