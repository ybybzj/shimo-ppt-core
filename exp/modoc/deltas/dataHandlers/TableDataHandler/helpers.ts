import { IOperatorPlainObject } from "../../../type"
import { OperatorList } from "../../../OperatorList"
import { Delta } from "../../Delta/Delta"
import { Operator } from "../../Operator/Operator"

export type IndexModifier = [string, number]
export function DeltaToIndexModifiers(delta: Delta): IndexModifier[] {
    const ret: IndexModifier[] = []
    let index = -1
    const iter = delta.getIter()
    while (iter.hasNext()) {
        const _a = iter.next(), action = _a.action, length_1 = _a.length
        if (index !== -1 && ret[index][0] === action) {
            ret[index][1] += length_1
        }
        else {
            ret[++index] = [action, length_1]
        }
    }
    return ret
}

export function reindexDelta(delta: Delta, indexModifiers: IndexModifier[]): Delta {
    const pool = delta.getPoolCopy()
    const opl = new OperatorList([])
    const iter = delta.getIter({ sourcePool: pool, targetPool: pool })
    const overridedIndexModifiers = new Array(indexModifiers.length)
    for (let i = 0; i < indexModifiers.length; i++) {
        if (!iter.hasNext()) {
            break
        }
        const _a = overridedIndexModifiers[i] || indexModifiers[i], operation = _a[0], length_2 = _a[1]
        switch (operation) {
            case 'insert':
                opl.push(new Operator({
                    action: 'retain',
                    data: length_2,
                    packedAttributes: '',
                    pool: pool
                }))
                break
            case 'retain': {
                const nextLength = iter.peekLength()
                if (nextLength >= length_2) {
                    opl.push(iter.next(length_2))
                }
                else {
                    opl.push(iter.next())
                    overridedIndexModifiers[i] = [
                        'retain',
                        length_2 - nextLength
                    ]
                    i -= 1
                }
                break
            }
            case 'remove': {
                const nextLength = iter.peekLength()
                if (nextLength >= length_2) {
                    iter.skip(length_2)
                }
                else {
                    iter.skip()
                    overridedIndexModifiers[i] = [
                        'remove',
                        length_2 - nextLength
                    ]
                    i -= 1
                }
                break
            }
        }
    }
    while (iter.hasNext()) {
        opl.push(iter.next())
    }
    return new Delta(opl.stringify(true), pool)
}

export function reindexInnerDeltas(delta: Delta | null, updater: Delta | null): Delta | null {
    if (delta === null || updater === null) {
        return delta
    }
    const modifier = DeltaToIndexModifiers(updater)
    if (modifier.length === 0) {
        return delta
    }
    return delta.map((op): IOperatorPlainObject<object> => {
        const plain: IOperatorPlainObject<object> = { action: op.action, data: op.data }
        if (plain.action !== 'remove' && op.hasAttributes()) {
            plain.attributes = op.getAttributes()
        }
        if (op.dataType === 'delta') {
            const updatedDelta = reindexDelta(new Delta(op.getCustomData()), modifier)
            plain.data = updatedDelta.isEmpty() ? 1 : { delta: updatedDelta.stringify() }
        }
        else {
            plain.data = op.data
        }
        return plain
    })
}

function invertDelta(delta, indexModifiers) {
    const pool = delta.getPoolCopy()
    const opl = new OperatorList([])
    const iter = delta.getIter({ sourcePool: pool, targetPool: pool })
    const overridedIndexModifiers = new Array(indexModifiers.length)
    for (let i = 0; i < indexModifiers.length; i++) {
        if (!iter.hasNext()) {
            break
        }
        const _a = overridedIndexModifiers[i] || indexModifiers[i], operationType = _a[0], operationLength = _a[1]
        switch (operationType) {
            case 'retain': {
                const nextLength = iter.peekLength()
                const advanceLength = Math.min(operationLength, nextLength)
                opl.push(new Operator({
                    action: 'retain',
                    data: advanceLength,
                    packedAttributes: '',
                    pool: pool
                }))
                iter.skip(advanceLength)
                if (nextLength < operationLength) {
                    overridedIndexModifiers[i] = [
                        'retain',
                        operationLength - nextLength
                    ]
                    i -= 1
                }
                break
            }
            case 'remove': {
                const nextLength = iter.peekLength()
                if (nextLength >= operationLength) {
                    opl.push(iter.next(operationLength))
                }
                else {
                    opl.push(iter.next())
                    overridedIndexModifiers[i] = [
                        'remove',
                        operationLength - nextLength
                    ]
                    i -= 1
                }
                break
            }
        }
    }
    let length = 0
    while (iter.hasNext()) {
        length += iter.peekLength()
        iter.skip()
    }
    if (length > 0) {
        opl.push(new Operator({
            action: 'retain',
            data: length,
            packedAttributes: '',
            pool: pool
        }))
    }
    return new Delta(opl.stringify(true), pool)
}
export function invertInnerDeltas(delta: Delta | null, updater: Delta | null): Delta | null {
    if (delta === null || updater === null) {
        return null
    }
    const modifiers = DeltaToIndexModifiers(updater)
    let containsRemove = false
    for (let _i = 0, modifiers_1 = modifiers; _i < modifiers_1.length; _i++) {
        const modifier = modifiers_1[_i]
        if (modifier[0] === 'remove') {
            containsRemove = true
            break
        }
    }
    if (!containsRemove) {
        return null
    }
    return delta.map((op): IOperatorPlainObject<object> => {
        const plain: IOperatorPlainObject<object> = { action: 'retain', data: op.data }
        if (op.hasAttributes()) {
            plain.attributes = op.getAttributes()
        }
        if (op.dataType === 'delta') {
            const updatedDelta = invertDelta(new Delta(op.getCustomData()), modifiers)
            plain.data = updatedDelta.isEmpty() ? 1 : { delta: updatedDelta.stringify() }
        }
        else {
            plain.data = op.data
        }
        return plain
    })
}

