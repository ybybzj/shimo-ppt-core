import { invert, merge, transform } from '../utils/objectOT'
import { AbstractDataHandler } from './AbstractDataHandler'

export class ObjectDataHandler extends AbstractDataHandler {
  compose(me: string, other: string, isDocument: boolean) {
    return this.pack(
      merge(this.parse(me), this.parse(other), !isDocument),
      isDocument
    )
  }
  transform(me: string, other: string, priority: boolean) {
    return this.pack(
      transform(this.parse(me), this.parse(other), priority),
      false
    )
  }
  invert(me: string, other: string) {
    return this.pack(invert(this.parse(me), this.parse(other)), false)
  }
  parse(value: string) {
    return JSON.parse(value)
  }
  pack(value: object, isDocument: boolean) {
    // Ignore properties that have value of `null`
    if (!isDocument) {
      return JSON.stringify(value)
    }
    return JSON.stringify(value, function (_, data) {
      return data === null ? undefined : data
    })
  }
  toJSON(value: string) {
    return this.parse(value)
  }
  explain(prefix: string, value: string) {
    return JSON.stringify(this.parse(value), null, prefix)
  }
}
