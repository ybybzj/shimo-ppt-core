import { Delta } from '../Delta/Delta'
import { AbstractDataHandler } from './AbstractDataHandler'

export interface IDeltaMapObject {
  [id: string]: Delta
}

export class DeltaMapDataHandler extends AbstractDataHandler {
  compose(a: string, b: string, isDocument: boolean) {
    const parsedA = this.parse(a)
    const parsedB = this.parse(b)
    const bKeys = Object.keys(parsedB)
    for (let _i = 0, bKeys_1 = bKeys; _i < bKeys_1.length; _i++) {
      const key = bKeys_1[_i]
      const valueA = parsedA[key]
      const valueB = parsedB[key]
      parsedA[key] =
        valueA === undefined ? valueB : valueA.compose(valueB, isDocument)
    }
    return this.pack(parsedA)
  }
  transform(a: string, b: string, priority: boolean) {
    const parsedA = this.parse(a)
    const parsedB = this.parse(b)
    const bKeys = Object.keys(parsedB)
    for (let _i = 0, bKeys_2 = bKeys; _i < bKeys_2.length; _i++) {
      const key = bKeys_2[_i]
      const valueA = parsedA[key]
      if (valueA !== undefined) {
        const valueB = parsedB[key]
        parsedB[key] = valueA.transform(valueB, priority)
      }
    }
    return this.pack(parsedB)
  }
  invert(a: string, b: string) {
    const parsedA = this.parse(a)
    const parsedB = this.parse(b)
    const bKeys = Object.keys(parsedB)
    for (let _i = 0, bKeys_3 = bKeys; _i < bKeys_3.length; _i++) {
      const key = bKeys_3[_i]
      const valueA = parsedA[key] || new this.deltaConstructor([])
      parsedB[key] = valueA.invert(parsedB[key])
    }
    return this.pack(parsedB)
  }
  /**
   * Parse a slide packed string
   *
   * xxxxxxxxN#XXXXX
   * x: element id (length should be 8)
   * N: Delta length
   * #: literally a "#" symbol
   * X: the Delta content
   */
  parse(value: string) {
    const parsed = {}
    let cursor = 0
    while (cursor < value.length) {
      const elementId = value.substring(cursor, cursor + 8)
      const poundSignPosition = value.indexOf('#', cursor + 8)
      const deltaLength = parseInt(
        value.substring(cursor + 8, poundSignPosition),
        36
      )
      const delta = new this.deltaConstructor(
        value.substring(
          poundSignPosition + 1,
          poundSignPosition + 1 + deltaLength
        )
      )
      parsed[elementId] = delta
      cursor = poundSignPosition + 1 + deltaLength
    }
    return parsed
  }
  pack(value: IDeltaMapObject) {
    let ret = ''
    const sortedElementIds = Object.keys(value).sort()
    for (
      let _i = 0, sortedElementIds_1 = sortedElementIds;
      _i < sortedElementIds_1.length;
      _i++
    ) {
      const elementId = sortedElementIds_1[_i]
      if (elementId.length !== 8) {
        throw new Error('element id length must equal to 8, got ' + elementId)
      }
      const element = value[elementId]
      const delta = Array.isArray(element)
        ? new this.deltaConstructor(element)
        : element
      const iter = delta.getIter()
      // TODO: use Delta#isEmpty() instead
      if (!iter.hasNext()) {
        continue
      }
      const deltaString = delta.stringify()
      ret +=
        '' + elementId + deltaString.length.toString(36) + '#' + deltaString
    }
    return ret
  }
  toJSON(value: string) {
    const ret = {}
    const parsed = this.parse(value)
    const keys = Object.keys(parsed)
    for (let _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
      const key = keys_1[_i]
      const element = parsed[key]
      ret[key] = element.toJSON()
    }
    return ret
  }
  explain(prefix: string, value: string) {
    let ret = prefix + 'Elements:\n'
    const parsed = this.parse(value)
    const keys = Object.keys(parsed)
    for (let _i = 0, keys_2 = keys; _i < keys_2.length; _i++) {
      const key = keys_2[_i]
      const element = parsed[key]
      ret += element.explain(prefix)
    }
    return ret
  }
}
