
import { IDeltaConstructor } from '../Delta/type'

import { IDataHandlerRegister, IOperatorConstructor } from '../Operator/type'
import { IDataHandlerConstructor } from './AbstractDataHandler'
import { DeltaDataHandler } from './DeltaDataHandler'
import { DeltaListDataHandler } from './DeltaListDataHandler'
import { DeltaMapDataHandler } from './DeltaMapDataHandler'
import { ObjectDataHandler } from './ObjectDataHandler'
import { TableDataHandler } from './TableDataHandler/TableDataHandler'


export function registerDataHandlers(
  register: IDataHandlerRegister,
  deltaCtor: IDeltaConstructor,
  operatorConstructor: IOperatorConstructor
) {
  const dhc = function dataHandlerConstructor(t: IDataHandlerConstructor) {
    return new t(deltaCtor, operatorConstructor)
  }

  register.registerDataHandler('delta', dhc(DeltaDataHandler))
  register.registerDataHandler('slide', dhc(DeltaMapDataHandler))
  register.registerDataHandler('mindmap', dhc(DeltaMapDataHandler))
  register.registerDataHandler('object', dhc(ObjectDataHandler))
  register.registerDataHandler('form', dhc(DeltaListDataHandler))
  register.registerDataHandler('table', dhc(TableDataHandler))
}
