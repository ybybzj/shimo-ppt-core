import { IDeltaConstructor } from '../Delta/type'
import { IOperatorConstructor } from '../Operator/type'
import { IDataHandler } from './IDataHandler'

export abstract class AbstractDataHandler {
  operatorConstructor: IOperatorConstructor
  deltaConstructor: IDeltaConstructor
  constructor(
    deltaConstructor: IDeltaConstructor,
    operatorConstructor: IOperatorConstructor
  ) {
    this.deltaConstructor = deltaConstructor
    this.operatorConstructor = operatorConstructor
  }
  compose(_me: string, _other: string, _isDocument: boolean): string {
    throw new Error('#compose() is not implemented')
  }
  transform(_me: string, _other: string, _priority: boolean) {
    throw new Error('#transform() is not implemented')
  }
  invert(_me: string, _other: string) {
    throw new Error('#invert() is not implemented')
  }
  parse(_value: string) {
    throw new Error('#parse() is not implemented')
  }
  pack(_value: object, _isDocument: boolean) {
    throw new Error('#pack() is not implemented')
  }
  toJSON(_value: string): any {
    throw new Error('#toJSON() is not implemented')
  }
  explain(prefix: string, value: string): string {
    return '' + prefix + value
  }
}

export interface IDataHandlerConstructor {
  new(deltaConstructor: IDeltaConstructor, operatorConstructor: IOperatorConstructor): IDataHandler
}
