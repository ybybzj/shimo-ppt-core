import { OperatorList } from '../../OperatorList'
import { Pool } from '../../Pool/Pool'
import { IOperatorPlainObject, TYPE_PROPERTY } from '../../type'
import { DeltaOrOps } from './type'
import { Operator } from '../Operator/Operator'
import { DeltaIter } from './DeltaIter'

export class Delta {
  private ops: string
  private pool: Pool
  static isDelta(input: any): input is Delta {
    return (
      typeof input === 'object' &&
      input !== null &&
      input[TYPE_PROPERTY] === 'Delta'
    )
  }
  /* 获取 Delta 的长度。方法是遍历 Delta 的每一个 Operator，进行累加。
  *
  * @readonly
  * @type {number}
  * @memberof IDelta
  */
  get length() {
    const iter = this.getIter()
    let length = 0
    while (iter.hasNext()) {
      length += iter.peekLength()
      iter.skip()
    }
    return length
  }

  constructor(ops: IOperatorPlainObject[], pool?: Pool)
  constructor(opstr: string, pool: Pool)
  constructor(data: string)
  constructor(data, pool?) {
    this[TYPE_PROPERTY] = 'Delta'
    if (Array.isArray(data)) {
      const opl = new OperatorList()
      if (!Pool.isPool(pool) || data.length === 0) {
        pool = new Pool()
      }
      for (let _i = 0, data_1 = data; _i < data_1.length; _i++) {
        const op = data_1[_i]
        // Ignore attributes for removing operators
        const packedAttributes = pool.packAttributes(op.attributes)
        opl.push(
          new Operator({
            action: op.action,
            data: op.data,
            pool: pool,
            packedAttributes: packedAttributes,
          })
        )
      }
      this.ops = opl.stringify(true)
      this.pool = pool
    } else if (pool === undefined) {
      const lenIndex = data.indexOf(':')
      if (lenIndex <= 0) {
        throw new Error('Invalid delta: ' + data)
      }
      const len = parseInt(data.substring(0, lenIndex), 36)
      if (isNaN(len)) {
        throw new Error('Invalid delta: ' + data)
      }
      const opsEndIndex = lenIndex + 1 + len
      this.ops = data.substring(lenIndex + 1, opsEndIndex)
      this.pool =
        len === 0 || data.length === opsEndIndex
          ? new Pool()
          : new Pool(data.substring(lenIndex + len + 1))
    } else {
      this.ops = data
      this.pool = data === '' ? new Pool() : pool
    }
  }
  /**
   * 判断当前 Delta 是否为空，即其中没有 Operators
   *
   * @returns {boolean}
   * @memberof IDelta
   */
  isEmpty() {
    return this.ops.length === 0
  }
  /**
   * 获取迭代器来迭代 Operator
   *
   * @example
   * ```
   * const iter = delta.getIter()
   * while (iter.hasNext()) {
   *   const op = iter.next()
   *   // ...
   * }
   * ```
   * @param {{targetPool?: Pool, sourcePool?: Pool}} [{targetPool, sourcePool}={}]
   * @returns {IDeltaIter}
   * @memberof IDelta
   */
  getIter(opt?: { targetPool?: Pool; sourcePool?: Pool }): DeltaIter {
    const { targetPool, sourcePool } = opt ?? {}
    return new DeltaIter(
      this.ops,
      sourcePool || this.pool,
      targetPool || this.pool,
      Operator
    )
  }
  /**
   * 获得当前 Delta 对应的 Pool 的深拷贝副本。
   *
   * @returns {Pool}
   * @memberof IDelta
   */
  getPoolCopy() {
    return this.pool.clone()
  }

  map(processor: (op: Operator) => IOperatorPlainObject): Delta {
    const ops: IOperatorPlainObject[] = []
    const iter = this.getIter()
    while (iter.hasNext()) {
      ops.push(processor(iter.next()))
    }
    return new Delta(ops)
  }
  /**
     * 和另外的 Delta 或者 OperatorPlainObject 数组进行 compose 操作。
     * 推荐使用 OperatorPlainObject 数组来省去在两个 pool 之间移动属性
     * 的操作。
     *
     * @param {(Delta | Array<OperatorPlainObject>)} other
     * @param {boolean} [isDocument] 当前 this 是否为 Document。该参数主要为了解决内嵌 Operation
     * 的 action 不一致的问题（目前只有 columns）。
     * @returns {Delta}
     * @memberof IDelta
     */
  compose(other: DeltaOrOps, isDocument?: boolean): Delta {
    const pool = this.getPoolCopy()
    const opl = new OperatorList([])
    const thisIter = this.getIter({ sourcePool: pool, targetPool: pool })
    // Passing an array of operators is recommanded
    // to avoid pool repacking.
    if (Array.isArray(other)) {
      other = new Delta(other as IOperatorPlainObject[], pool)
    }
    const otherIter = other.getIter({ targetPool: pool })
    while (true) {
      const thisHasNext = thisIter.hasNext()
      const otherHasNext = otherIter.hasNext()
      if (!thisHasNext && !otherHasNext) {
        break
      }
      if (!otherHasNext) {
        return new Delta(thisIter.remainingStrs(opl), pool)
      }
      if (!thisHasNext && otherIter.isPoolCompatible()) {
        return new Delta(otherIter.remainingStrs(opl), pool)
      }
      if (otherIter.peekAction() === 'insert') {
        // * <- insert
        opl.push(otherIter.next())
      } else if (thisIter.peekAction() === 'remove') {
        // remove <- *
        opl.push(thisIter.next())
      } else {
        const length_1 = Math.min(thisIter.peekLength(), otherIter.peekLength())
        const thisOp = thisHasNext ? thisIter.next(length_1) : null
        const otherOp = otherIter.next(length_1)
        if (otherOp.action === 'retain') {
          // insert/retain <- retain
          opl.push(
            thisOp
              ? thisOp.compose(
                otherOp,
                isDocument === undefined
                  ? thisOp.action === 'insert'
                  : isDocument
              )
              : otherOp
          )
        } else if (!thisOp || thisOp.action === 'retain') {
          // retain <- remove
          opl.push(otherOp)
        }
        // insert <- remove cancels out
      }
    }
    return new Delta(opl.stringify(true), pool)
  }

  /**
   * 对另一个 Delta 执行 transform 操作
   *
   * @param {Delta} other 另一个 Delta
   * @param {boolean} [priority=false] this 对应的操作是否优先
   * @returns {Delta}
   * @memberof IDelta
   */
  transform(other: DeltaOrOps, priority?: boolean): Delta {
    if (priority === void 0) {
      priority = false
    }
    if (Array.isArray(other)) {
      other = new Delta(other)
    }
    const pool = other.getPoolCopy()
    const opl = new OperatorList()
    const thisIter = this.getIter()
    const otherIter = other.getIter({ sourcePool: pool, targetPool: pool })
    while (otherIter.hasNext()) {
      if (!thisIter.hasNext()) {
        return new Delta(otherIter.remainingStrs(opl), pool)
      }
      if (
        thisIter.peekAction() === 'insert' &&
        (priority || otherIter.peekAction() !== 'insert')
      ) {
        opl.push(
          new Operator({
            action: 'retain',
            data: thisIter.peekLength(),
          })
        )
        thisIter.skip()
      } else if (otherIter.peekAction() === 'insert') {
        opl.push(otherIter.next())
      } else {
        const length_2 = Math.min(thisIter.peekLength(), otherIter.peekLength())
        const thisOp = thisIter.next(length_2)
        const otherOp = otherIter.next(length_2)
        if (thisOp.action === 'remove') {
          if (
            thisOp.hasAttributes() ||
            (otherOp.action === 'remove' && otherOp.hasAttributes())
          ) {
            // Transforming removing operations with attributes
            // causes inconsistent result, so we throws explicitly here.
            throw new Error(
              'Cannot transform removing operations with attributes'
            )
          }
          // `this` remove either makes `other` remove redundant or removes `other` retain
          continue
        }
        if (otherOp.action === 'remove') {
          if (otherOp.hasAttributes()) {
            throw new Error(
              'Cannot transform removing operations with attributes'
            )
          }
          opl.push(otherOp)
        } else {
          // `retain`.transform(`retain`)
          opl.push(thisOp.transform(otherOp, priority))
        }
      }
    }
    return new Delta(opl.stringify(true), pool)
  }

  /**
   * Invert a change
   *
   * Given C = A.compose(B) and D = A.invert(B)
   * We have C.compose(D) = A
   *
   * @param {Delta} other the composing delta
   * @return {Delta} the composed delta
   */
  invert(
    other: DeltaOrOps,
    processor?: (thisOp: Operator, otherOp: Operator) => Operator
  ): Delta {
    const pool = new Pool()
    const opl = new OperatorList()
    const thisIter = this.getIter({ targetPool: pool })
    const otherIter = (
      Array.isArray(other) ? new Delta(other, new Pool()) : other
    ).getIter({ targetPool: pool })
    while (otherIter.hasNext()) {
      if (otherIter.peekAction() === 'insert') {
        opl.push(
          new Operator({
            action: 'remove',
            data: otherIter.peekLength(),
          })
        )
        otherIter.skip()
      } else {
        if (!thisIter.hasNext()) {
          throw new Error('Delta should be smaller than document')
        }
        if (thisIter.peekAction() !== 'insert') {
          throw new Error('Only document can invert deltas')
        }
        const length_3 = Math.min(thisIter.peekLength(), otherIter.peekLength())
        const otherOp = otherIter.next(length_3)
        const thisOp = thisIter.next(length_3)
        if (otherOp.action === 'remove') {
          opl.push(thisOp)
        } else {
          // insert + retain
          opl.push(
            processor ? processor(thisOp, otherOp) : thisOp.invert(otherOp)
          )
        }
      }
    }
    return new Delta(opl.stringify(true), pool)
  }
  /**
   * 转换成字符串
   *
   * @returns {string}
   * @memberof IDelta
   */
  stringify(): string {
    return this.isEmpty()
      ? '0:'
      : this.ops.length.toString(36) + ':' + this.ops + this.pool.stringify()
  }
  /**
   * 转成 JSON 对象，用于便于阅读和编码。
   *
   * 注意此方法会递归转换所有的数据，所以当需要一次性读取 Delta 的所有内容时
   * 调用此方法不会产生额外开销，但是只需要 Delta 中某一个部分的信息时，就会
   * 额外浪费很多解析。
   *
   * toJSON() 返回的内容可以重新转成 Delta，即下面表达式保证为 true:
   * deepEqual(delta.toJSON(), new Delta(delta.toJSON()).toJSON())
   *
   * 另外也可以很方便地通过 JSON.stringify(delta) 将 Delta 转成 JSON 字符串，
   * 尽管绝大部分时候应该使用 Delta#stringify() 来进行更高效的序列化。
   *
   * @returns {Array<any>}
   * @memberof Delta
   */
  toJSON(): any[] {
    const json: any[] = []
    const iter = this.getIter()
    while (iter.hasNext()) {
      json.push(iter.next().toJSON())
    }
    return json
  }
  explain(prefix?: string): string {
    if (prefix === void 0) {
      prefix = ''
    }
    let ret = prefix + 'Delta #' + this.ops.length + ':\n'
    const iter = this.getIter()
    while (iter.hasNext()) {
      ret += iter.next().explain(prefix + '  ')
    }
    return '' + ret + prefix + 'Pool: ' + this.pool.stringify() + '\n'
  }
  /**
   * 校验 Table 的合法性。目前主要是保证工作表不重名
   *
   * @returns {Delta} 返回自身，便于链式调用
   * @memberof Delta
   */
  validateTable(): Delta {
    const tableNames = {}
    const iter = this.getIter()
    while (iter.hasNext()) {
      const operator = iter.next()
      if (operator.dataType === 'table') {
        const name_1 = operator.getAttributes().name
        if (typeof name_1 !== 'string') {
          continue
        }
        const key = '$$' + name_1 + '$$'
        if (tableNames.hasOwnProperty(key)) {
          throw new Error('Duplicated table name found: ' + name_1)
        }
        tableNames[key] = true
      }
    }
    return this
  }
}
