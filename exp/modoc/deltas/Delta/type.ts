
import { IOperatorPlainObject } from '../../type';
import { Delta } from './Delta'
export type IDeltaConstructor = typeof Delta

export type DeltaOrOps = Delta | IOperatorPlainObject[];