import { PushResult, OperatorList } from '../../OperatorList'
import { Pool } from '../../Pool/Pool'
import { OperatorAction } from '../../type'
import { IOperatorConstructor } from '../Operator/type'
import { initialToObjectKey } from '../Operator/objectKeys'
import { Operator } from '../Operator/Operator'

export interface INext {
  action: OperatorAction
  dataIndex: number
  actualLength: number
  dataLength: number
  opIndex: number
  opLength: number
  dataType: string
}

export class DeltaIter {
  private ops: string
  private pool: Pool
  private targetPool: Pool
  private operatorConstructor: IOperatorConstructor
  private cursor: number
  private nextData?: INext
  private offset: number
  private isCompatible?: boolean
  constructor(
    ops: string,
    pool: Pool,
    targetPool: Pool,
    operatorConstructor: IOperatorConstructor
  ) {
    this.ops = ops
    this.pool = pool
    this.targetPool = targetPool
    this.operatorConstructor = operatorConstructor
    this.cursor = 0
    this.offset = 0
  }
  isPoolCompatible() {
    if (this.isCompatible === undefined) {
      this.isCompatible = this.pool.isSubsetOf(this.targetPool)
    }
    return this.isCompatible
  }
  hasNext() {
    return this.cursor < this.ops.length
  }
  remainingStrs(opl: OperatorList): string {
    while (this.hasNext()) {
      const op = this.next()
      const pushResult = opl.push(op)
      if (pushResult !== PushResult.REORDERED) {
        const cursor = this.cursor
        this.cursor = this.ops.length
        this.nextData = undefined
        return opl.stringify() + this.ops.substring(cursor)
      }
    }
    return opl.stringify(true)
  }
  skip(expectedLength?: number): [number, number, INext] {
    if (expectedLength === void 0) {
      expectedLength = Infinity
    }
    const next = this.peekNext()
    if (next === null) {
      throw new Error('No more operators')
    }
    const offset = this.offset
    let length
    if (expectedLength + offset >= next.dataLength) {
      length = next.dataLength - offset
      // Advance an operator
      this.nextData = undefined
      this.cursor = next.opIndex + next.opLength
      this.offset = 0
    } else {
      length = expectedLength
      this.offset += length
    }
    return [length, offset, next]
  }
  next(expectedLength?: number, overrideAction?: OperatorAction): Operator {
    if (expectedLength === void 0) {
      expectedLength = Infinity
    }
    const _a = this.skip(expectedLength),
      length = _a[0],
      offset = _a[1],
      next = _a[2]
    let data
    switch (next.dataType) {
      case 'number':
        data = length
        break
      case 'string':
        data = this.ops.substring(
          next.dataIndex + 1 + offset,
          next.dataIndex + 1 + offset + length
        )
        break
      default:
        // ASSERT: length === 1 && offset === 0
        const initial = this.ops[next.dataIndex + 1 + offset]
        const value = this.ops.substring(
          next.dataIndex + 1 + 1,
          next.dataIndex + 1 + next.actualLength
        )
        data = {
          [initialToObjectKey[initial]]: value,
        }
        break
    }
    let packedAttributes = this.ops.substring(
      next.dataIndex + 1 + next.actualLength,
      next.opIndex + next.opLength
    )
    const pool = this.targetPool
    const needRepackAttributes =
      packedAttributes !== '' && !this.isPoolCompatible()
    let packedData
    if (offset === 0 && length === next.dataLength && !needRepackAttributes) {
      packedData = this.ops.substring(
        next.opIndex,
        next.dataIndex + 1 + next.actualLength
      )
    }
    if (needRepackAttributes) {
      packedAttributes = pool.packAttributes(
        this.pool.unpackAttributes(packedAttributes)
      )
    }
    return new this.operatorConstructor(
      {
        action: overrideAction || next.action,
        data: data,
        packedAttributes: packedAttributes,
        pool: pool,
      },
      packedData
    )
  }
  peekAction(): string {
    const next = this.peekNext()
    return next === null ? 'retain' : next.action
  }
  peekLength(): number {
    const next = this.peekNext()
    return next === null ? Infinity : next.dataLength - this.offset
  }
  private peekNext() {
    if (!this.hasNext()) {
      return null
    }
    if (this.nextData === undefined) {
      let i = this.cursor + 1
      const opsLength = this.ops.length
      let action
      while (i < opsLength) {
        switch (this.ops[i]) {
          case '!':
            action = 'insert'
            break
          case '@':
            action = 'retain'
            break
          case '#':
            action = 'remove'
            break
        }
        if (action !== undefined) {
          break
        }
        i += 1
      }
      const opIndex = i
      const opLength = parseInt(this.ops.substring(this.cursor, opIndex), 36)
      i = opIndex + 1

      let dataType
      while (i < opsLength) {
        switch (this.ops[i]) {
          case '!':
            dataType = 'string'
            break
          case '#':
            dataType = 'number'
            break
          case '@':
            dataType = 'object'
            break
        }
        if (dataType !== undefined) {
          break
        }
        i += 1
      }
      const dataIndex = i
      const length_1 = parseInt(this.ops.substring(opIndex + 1, dataIndex), 36)
      const actualLength = dataType === 'number' ? 0 : length_1
      const dataLength = dataType === 'object' ? 1 : length_1
      this.nextData = {
        action: action,
        dataIndex: dataIndex,
        dataLength: dataLength,
        opIndex: opIndex,
        opLength: opLength,
        dataType: dataType,
        actualLength: actualLength,
      }
    }
    return this.nextData
  }
}
