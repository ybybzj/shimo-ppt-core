import { IOperator } from './deltas'

export enum PushResult {
  PUSHED = 0,
  MERGED = 1,
  REORDERED = 2,
}

/**
 * Operator 容器。类似一个数组，但是可以自动合并相邻的 Operator。
 *
 * @export
 * @class OperatorList
 */

export class OperatorList {
  private ops: IOperator[]
  private stringified: boolean
  constructor(ops?: IOperator[]) {
    if (ops === void 0) {
      ops = []
    }
    this.stringified = false
    this.ops = ops
  }

  push(newOp: IOperator): PushResult {
    if (this.stringified) {
      throw new Error('Cannot push new operators to a stringified operatorList')
    }
    let index = this.ops.length
    if (index === 0) {
      this.ops.push(newOp)
      return PushResult.PUSHED
    }
    let result = PushResult.PUSHED
    let lastOp = this.ops[index - 1]
    // Since it does not matter if we insert before or after deleting at the same index,
    // always prefer to insert first
    if (lastOp.action === 'remove' && newOp.action === 'insert') {
      index -= 1
      lastOp = this.ops[index - 1]
      result = PushResult.REORDERED
      if (lastOp === undefined) {
        this.ops.unshift(newOp)
        return result
      }
    }
    const mergedOp = lastOp.merge(newOp)
    if (mergedOp) {
      this.ops[index - 1] = mergedOp
      result = PushResult.MERGED
    } else if (index === this.ops.length) {
      this.ops.push(newOp)
    } else {
      this.ops.splice(index, 0, newOp)
    }
    return result
  }
  stringify(chop?: boolean): string {
    if (chop === void 0) {
      chop = false
    }
    this.stringified = true
    const length = this.ops.length
    let str = ''
    for (let i = 0; i < length; i++) {
      const op = this.ops[i]
      if (!chop || i < length - 1 || !op.isEmpty()) {
        str += op.stringify()
      }
    }
    return str
  }
}
