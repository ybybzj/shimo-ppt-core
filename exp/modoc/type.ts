export const TYPE_PROPERTY = '__MODOC_NAME__'
export type AttributeKey = string
export type AttributeValue = string | number | boolean | null
export interface IStringObject<T = string> {
  [key: string]: T
}
export interface IAttributeObject {
  [attributeKey: string]: AttributeValue
}
export interface IExternalAttributeObject {
  [attributeKey: string]: AttributeValue | object
}
export type OperatorData<T extends object = object> = string | number | T
export type OperatorAction = 'insert' | 'retain' | 'remove'
export interface IOperatorPlainObject<T extends object = object> {
  action: OperatorAction
  data: OperatorData<T>
  attributes?: IExternalAttributeObject
}
