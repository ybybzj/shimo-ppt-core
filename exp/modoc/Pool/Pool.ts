import {
  AttributeKey,
  AttributeValue,
  IAttributeObject,
  IExternalAttributeObject,
  TYPE_PROPERTY,
} from '../type'
import {
  combinedAttributeWithValue,
  getGulByCombinedKeyValue,
  getKeyValueByGul,
  parseCombinedAttributeValue,
} from './attributePresets'
import { keyPathToObject, objectToKeyPaths } from './utils'
export interface IAttributeMap {
  [attribute: string]: string
}
export interface IParsedPoolData {
  array: string[]
  packedData?: string
}
export interface IPoolConfig {
  enableCache?: boolean
  enableNullIgnore?: boolean
  enableRawString?: boolean
}

let isCacheEnabled: boolean = false
let isNullIgnored: boolean = false
let isRawStringEnabled: boolean = false

export class Pool {
  private map?: Record<string, string>
  private array: string[]
  private cache: Record<string, [string, AttributeValue]>
  private packedData?: string
  constructor(data?) {
    if (data === void 0) {
      data = { array: [], packedData: '' }
    }
    this[TYPE_PROPERTY] = 'Pool'
    this.cache = {}
    if (typeof data === 'string') {
      this.array = JSON.parse(data)
      this.packedData = data
    } else {
      this.array = data.array
      this.packedData = data.packedData
    }
  }

  static isPool(input): input is Pool {
    return (
      typeof input === 'object' &&
      input !== null &&
      input[TYPE_PROPERTY] === 'Pool'
    )
  }
  static setIPoolConfig(config?: IPoolConfig) {
    if (config) {
      isCacheEnabled = !!config.enableCache
      isNullIgnored = !!config.enableNullIgnore
      isRawStringEnabled = !!config.enableRawString
    }
  }

  isSubsetOf(pool: Pool) {
    if (this === pool) {
      return true
    }
    const length = this.array.length
    if (length === 0) {
      return true
    }
    if (length > pool.array.length) {
      return false
    }
    for (let i = 0; i < length; i++) {
      if (this.array[i] !== pool.array[i]) {
        return false
      }
    }
    return true
  }
  getMap() {
    if (!this.map) {
      this.map = {}
      for (let i = 0; i < this.array.length; i++) {
        this.map[this.array[i]] = String(i)
      }
    }
    return this.map
  }
  packAttributes(attributes?: object) {
    if (!attributes) {
      return ''
    }
    const keys = Object.keys(attributes)
    const length = keys.length
    if (length === 0) {
      return ''
    }
    const pooledAttrs: string[] = []
    const presetAttrs: string[] = []
    for (let i = 0; i < length; i++) {
      const key = keys[i]
      const value = attributes[key]
      if (typeof value === 'object' && value !== null) {
        const pairs = objectToKeyPaths(value, '.' + key)
        for (let _i = 0, pairs_1 = pairs; _i < pairs_1.length; _i++) {
          const pair = pairs_1[_i]
          const pattr = this.getAddress(pair[0], pair[1])
          if (pattr[0] === ' ') {
            pooledAttrs.push(pattr)
          } else {
            presetAttrs.push(pattr)
          }
        }
      } else {
        const pattr = this.getAddress(key, value)
        if (pattr[0] === ' ') {
          pooledAttrs.push(pattr)
        } else {
          presetAttrs.push(pattr)
        }
      }
    }
    return presetAttrs.sort().join('') + pooledAttrs.sort().join('')
  }
  getAddress(key: AttributeKey, value: AttributeValue): string {
    const combinedAttribute = combinedAttributeWithValue(key, value)
    const presetIndex = getGulByCombinedKeyValue(combinedAttribute)
    if (typeof presetIndex === 'string') {
      return presetIndex
    }
    const map = this.getMap()
    if (!map.hasOwnProperty(combinedAttribute)) {
      this.packedData = undefined
      const index = this.array.length
      map[combinedAttribute] = String(index)
      this.array.push(combinedAttribute)
    }
    return ' ' + map[combinedAttribute]
  }
  unpackAttributes(
    packedAttributes: string,
    resolveObject: false
  ): IAttributeObject
  unpackAttributes(
    packedAttributes: string,
    resolveObject?: boolean
  ): IExternalAttributeObject
  unpackAttributes(packedAttributes: string, resolveObject) {
    if (resolveObject === void 0) {
      resolveObject = true
    }
    const ret: IAttributeObject | IExternalAttributeObject = {}
    let lastIndex = 0
    while (
      lastIndex < packedAttributes.length &&
      packedAttributes[lastIndex] !== ' '
    ) {
      const _a = getKeyValueByGul(packedAttributes[lastIndex]),
        attributeKey = _a[0],
        attributeValue = _a[1]
      if (resolveObject && attributeKey[0] === '.') {
        keyPathToObject(ret, attributeKey, attributeValue)
      } else {
        ret[attributeKey] = attributeValue
      }
      lastIndex += 1
    }
    if (lastIndex < packedAttributes.length - 1) {
      const addresses = packedAttributes.substring(lastIndex + 1).split(' ')
      lastIndex++
      for (let _i = 0, addresses_1 = addresses; _i < addresses_1.length; _i++) {
        const address = addresses_1[_i]
        const _b = isCacheEnabled
            ? this.cache[address] ||
              (this.cache[address] = parseCombinedAttributeValue(
                this.array[address]
              ))
            : parseCombinedAttributeValue(this.array[address]),
          attributeKey = _b[0],
          attributeValue = _b[1]
        if (resolveObject && attributeKey[0] === '.') {
          keyPathToObject(ret, attributeKey, attributeValue)
        } else if (!isNullIgnored || attributeValue !== null) {
          ret[attributeKey] = attributeValue
          if (
            isRawStringEnabled &&
            !ret.rawString &&
            (attributeKey === 'cellValue' || attributeKey === 'formula')
          ) {
            ret.rawString =
              packedAttributes.substring(0, lastIndex) +
              packedAttributes.substring(lastIndex + address.length + 1)
          }
        }
        lastIndex += address.length + 1
      }
    }
    if (isRawStringEnabled && !ret.rawString) {
      ret.rawString = packedAttributes
    }
    return ret
  }
  stringify() {
    if (typeof this.packedData !== 'string') {
      this.packedData = this.array.length ? JSON.stringify(this.array) : ''
    }
    return this.packedData
  }
  clone() {
    return new Pool({
      array: this.array.slice(),
      packedData: this.packedData,
    })
  }
}
