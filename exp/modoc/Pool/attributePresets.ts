import { AttributeKey, AttributeValue } from "../type"

export const keyToGul = {
  color: '0',
  background: '1',
  'font-size': '2',
  width: '3',
  height: '4',
  rowspan: '5',
  colspan: '6',
  align: '7',
  bold: '8',
  italic: '9',
  underline: 'a',
  vertical: 'b',
  strike: 'c',
  wrap: 'd',
  fixedRowsTop: 'e',
  fixedColumnsLeft: 'f',
  link: 'g',
  format: 'h',
  formula: 'i',
  filter: 'j',
  'border-top': 'k',
  'border-right': 'l',
  'border-bottom': 'm',
  'border-left': 'n',
  line: 'o',
  guid: 'p',
  author: 'q',
  size: 'r',
  list: 's',
  layout: 't',
  margin: 'u',
  font: 'v',
  header: 'w',
  indent: 'x',
  comment: 'y',
  'comment-block': 'z',
  'code-block': 'A',
  name: 'B',
  order: 'C',
  autoFit: 'D',
  userID: 'E',
  nextIndex: 'F',
  id: 'G',
  colVisible: 'H',
  rowVisible: 'I',
  autoFormatter: 'J',
  copyIndex: 'K',
  manualAlign: 'L',
  lock: 'M',
  left: 'N',
  top: 'O',
} as const

const gulIndexToKey = [
  'color',
  'background',
  'font-size',
  'width',
  'height',
  'rowspan',
  'colspan',
  'align',
  'bold',
  'italic',
  'underline',
  'vertical',
  'strike',
  'wrap',
  'fixedRowsTop',
  'fixedColumnsLeft',
  'link',
  'format',
  'formula',
  'filter',
  'border-top',
  'border-right',
  'border-bottom',
  'border-left',
  'line',
  'guid',
  'author',
  'size',
  'list',
  'layout',
  'margin',
  'font',
  'header',
  'indent',
  'comment',
  'comment-block',
  'code-block',
  'name',
  'order',
  'autoFit',
  'userID',
  'nextIndex',
  'id',
  'colVisible',
  'rowVisible',
  'autoFormatter',
  'copyIndex',
  'manualAlign',
  'lock',
  'left',
  'top',
  'cellValue',
] as const

export function getAttributeKeyByGul(gul: string): AttributeKey {
  const code = gul.charCodeAt(0)
  let key
  if (code >= 48 && code <= 57) {
    key = gulIndexToKey[code - 48]
  } else if (code >= 65 && code <= 90) {
    key = gulIndexToKey[code - 29]
  } else if (code >= 97 && code <= 122) {
    key = gulIndexToKey[code - 87]
  } else {
    throw new Error('Unknown gul "' + gul + '"')
  }
  return key
}

export function getGulByAttributeKey(attribute: AttributeKey): string {
  const replacement = keyToGul[attribute]
  if (replacement) {
    return replacement
  }

  const attributeLength = attribute.length.toString(36)
  const length = attributeLength.length
  if (length > 2) {
    throw new Error('Key length of attribute should <= 2: ' + attributeLength)
  }
  const symbol = length === 1 ? '$' : '%'
  return '' + symbol + attributeLength + attribute
}

export function combinedAttributeWithValue(attribute: string, value: AttributeValue): string {
  const replacement = getGulByAttributeKey(attribute)
  if (typeof value === 'string') {
    return replacement + ':' + value
  }
  if (typeof value === 'number') {
    return replacement + '*' + value.toString()
  }
  if (typeof value === 'boolean') {
    return replacement + (value ? '1' : '0')
  }
  return replacement
}
export const keyValueToGul = {
  81: '!',
  '2*12': '"',
  '2*11': '#',
  '2*10': '$',
  '2*9': '%',
  '2*8': '&',
  '6*2': "'",
  '6*3': '(',
  '6*4': ')',
  '5*2': '*',
  '5*3': '+',
  '5*4': ',',
  '0:#ff0000': '-',
  '0:#ffffff': '.',
  'b:bottom': '/',
  'h:normal': '0',
  1: '1',
  '1:#ffffff': '2',
  '1:#ff0000': '3',
  '1:#ffff00': '4',
  '1:#00ffff': '5',
  '7:left': '6',
  '7:center': '7',
  '7:right': '8',
  '1*4': '9',
  'd:text-wrap': ':',
  'd:text-no-wrap': ';',
  'h:YYYY/MM/DD': '<',
  'l:["",1]': '=',
  'n:["",1]': '>',
  'k:["",1]': '?',
  'm:["",1]': '@',
  'l:["#2b2b2b",1]': 'A',
  'n:["#2b2b2b",1]': 'B',
  'k:["#2b2b2b",1]': 'C',
  'm:["#2b2b2b",1]': 'D',
} as const

const keyValues = [
  ['bold', true],
  ['font-size', 12],
  ['font-size', 11],
  ['font-size', 10],
  ['font-size', 9],
  ['font-size', 8],
  ['colspan', 2],
  ['colspan', 3],
  ['colspan', 4],
  ['rowspan', 2],
  ['rowspan', 3],
  ['rowspan', 4],
  ['color', '#ff0000'],
  ['color', '#ffffff'],
  ['vertical', 'bottom'],
  ['format', 'normal'],
  ['background', null],
  ['background', '#ffffff'],
  ['background', '#ff0000'],
  ['background', '#ffff00'],
  ['background', '#00ffff'],
  ['align', 'left'],
  ['align', 'center'],
  ['align', 'right'],
  ['background', 4],
  ['wrap', 'text-wrap'],
  ['wrap', 'text-no-wrap'],
  ['format', 'YYYY/MM/DD'],
  ['border-right', '["",1]'],
  ['border-left', '["",1]'],
  ['border-top', '["",1]'],
  ['border-bottom', '["",1]'],
  ['border-right', '["#2b2b2b",1]'],
  ['border-left', '["#2b2b2b",1]'],
  ['border-top', '["#2b2b2b",1]'],
  ['border-bottom', '["#2b2b2b",1]'],
] as const

export function getGulByCombinedKeyValue(name: string): string | null {
  return keyValueToGul.hasOwnProperty(name)
    ? keyValueToGul[name]
    : null
}
export function getKeyValueByGul(gul: string): [AttributeKey, AttributeValue] {
  const index = gul.charCodeAt(0) - 33
  return keyValues[index] as [AttributeKey, AttributeValue]
}

export function parseCombinedAttributeValue(combined: string): [AttributeKey, AttributeValue] {
  let attribute
  if (combined[0] === '$') {
    const attributeLength = parseInt(combined[1], 36)
    attribute = combined.substring(2, 2 + attributeLength)
    combined = combined.substring(1 + attributeLength)
  } else if (combined[0] === '%') {
    const attributeLength = parseInt(combined[1] + combined[2], 36)
    attribute = combined.substring(3, 3 + attributeLength)
    combined = combined.substring(2 + attributeLength)
  } else {
    attribute = getAttributeKeyByGul(combined[0])
  }
  if (combined.length === 1) {
    return [attribute, null]
  }
  const flag = combined[1]
  if (flag === '1') {
    return [attribute, true]
  }
  if (flag === '0') {
    return [attribute, false]
  }
  const value = combined.substring(2)
  return [attribute, flag === ':' ? value : Number(value)]
}
