import { AttributeValue } from '../type'

export type KeyPathsResult = Array<[string, AttributeValue]>
export function objectToKeyPaths(
  object: object,
  prefix: string,
  ret?: KeyPathsResult
) {
  if (ret === void 0) {
    ret = []
  }
  const keys = Object.keys(object)
  for (let _i = 0, keys_1 = keys; _i < keys_1.length; _i++) {
    const key = keys_1[_i]
    const item = object[key]
    const nestedKey = prefix + '.' + key
    if (typeof item === 'object' && item !== null) {
      objectToKeyPaths(item, nestedKey, ret)
    } else {
      ret.push([nestedKey, item])
    }
  }
  return ret
}

export function keyPathToObject(obj: object, keyPath: string, value: any) {
  const path = keyPath.split('.')
  const length = path.length
  for (let i = 1; i < length - 1; i++) {
    const key = path[i]
    const currentValue = obj[key]
    obj =
      typeof currentValue === 'object' && currentValue !== null
        ? currentValue
        : (obj[key] = {})
  }
  const lastPath = path[length - 1]
  if (
    value === null &&
    // 避免跳过不存在属性 (obj[key] === undefined) 需要赋值为 null 的情况
    obj[lastPath] !== undefined &&
    obj[lastPath] !== null
  ) {
    return
  }
  obj[lastPath] = value
}
