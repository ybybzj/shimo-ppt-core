import { SMCore } from '../src'
import { Delta } from './modoc'
import './modocHelper'
import './matrixHelper'
import * as DocData from './docData'
import { ioAdapter } from './modocAdapters'
import { translate } from './translate'

const {
  createEditor,
  registerCallback,
  EventNames,
  loadPPT,
  resize,
  setViewMode,
} = SMCore
;(window as any).core = SMCore
const editor = createEditor(
  {
    'id-view': 'editor_sdk',
    fonts: {} as any, // font function is not working
    translate,
  },
  ioAdapter
)

registerCallback(EventNames.Evt_onScriptError, (msg) => {
  console.error(msg)
})

registerCallback(EventNames.Evt_onDocumentContentReady, () => {
  setViewMode(editor, false)
})
loadPPT(editor, new Delta(DocData.bigTable as any))

addEventListener('resize', () => {
  resize(editor)
})
