/* eslint-disable */
;(() => {
  const proto = CanvasRenderingContext2D.prototype
  const desForGetCanvas = Object.getOwnPropertyDescriptor(proto, 'canvas')
  const oldGetCanvas = desForGetCanvas['get']
  const ctxBag = new WeakMap()
  let id = 0
  let lastCtx = null
  const groupLableStyle =
    'background-color: darkblue; color: white; font-style: italic; border: 2px solid hotpink; font-size: 1em;'
  const contentStyle = 'color: yellow'
  const getStyle = 'color: white'
  const setStyle = 'color: lightgreen'
  const callStyle = 'color: lightblue'

  function logCtx(ctx) {
    if (!ctxBag.has(ctx)) {
      ctxBag.set(ctx, id++)
    }
    const cid = ctxBag.get(ctx)

    if (ctx !== lastCtx) {
      if (lastCtx != null) {
        console.log('')
        console.groupCollapsed('%ccanvas', contentStyle)
        console.log(getWAndH(lastCtx))
        console.log(getBase64(lastCtx))
        console.groupEnd('canvas')
        console.groupEnd(`[${ctxBag.get(lastCtx)}]`)
        console.log('')
      }
      lastCtx = ctx
      console.groupCollapsed(`%c[${cid}]`, groupLableStyle)
      console.groupCollapsed('%ccanvas', contentStyle)
      console.log(getWAndH(ctx))
      console.log(getBase64(ctx))
      console.groupEnd('canvas')
      console.log('')
    }
  }
  function getWAndH(ctx) {
    const canvas = oldGetCanvas.apply(ctx, [])
    return `${canvas.width}X${canvas.height}`
  }

  function getBase64(ctx) {
    const canvas = oldGetCanvas.apply(ctx, [])
    return canvas.toDataURL()
  }

  Object.keys(proto).forEach((key) => {
    const des = Object.getOwnPropertyDescriptor(proto, key)
    const _isSetter = isSetter(des)
    const _isGetter = isGetter(des)
    const _isMethod = isMethod(des)
    switch (true) {
      case _isGetter || _isSetter: {
        const updaters = {}
        if (_isGetter) {
          const oldGetter = des['get']
          updaters['get'] = function () {
            const v = oldGetter.apply(this, arguments)
            logCtx(this)
            console.log(`%cget ${key}: ${v}`, getStyle)
            return v
          }
        }
        if (_isSetter) {
          const oldSetter = des['set']
          updaters['set'] = function () {
            const args = []
            for (let i = 0, l = arguments.length; i < l; i++) {
              args.push(arguments[i])
            }
            logCtx(this)
            console.log(`%cset ${key} with: [${args}], %O`, setStyle, arguments)
            return oldSetter.apply(this, arguments)
          }
        }
        Object.defineProperty(proto, key, {
          ...des,
          ...updaters,
        })
        break
      }
      case _isMethod: {
        // console.log(`"${key}" is a method`)
        const oldMethod = proto[key]
        Object.defineProperty(proto, key, {
          ...des,
          value: function () {
            if (['getImageData'].indexOf(key) === -1) {
              const args = []
              for (let i = 0, l = arguments.length; i < l; i++) {
                const arg = arguments[i]
                if (arg.getContext) {
                  const ctx = arg.getContext('2d')
                  const ctx_id = ctxBag.get(ctx)
                  args.push(`(${ctx_id})`)
                } else {
                  args.push(arg)
                }
              }
              logCtx(this)
              console.log(
                `%ccall ${key} ${args.length > 0 ? `with: [${args}], %O` : ''}`,
                callStyle,
                arguments
              )
            }
            return oldMethod.apply(this, arguments)
          },
        })
      }
    }
  })

  const canvasProto = HTMLCanvasElement.prototype
  Object.keys(canvasProto).forEach((key) => {
    if (key === 'width' || key === 'height') {
      const des = Object.getOwnPropertyDescriptor(canvasProto, key)
      const _isSetter = isSetter(des)
      const updaters = {}
      if (_isSetter) {
        const oldSetter = des['set']
        updaters['set'] = function () {
          const args = []
          for (let i = 0, l = arguments.length; i < l; i++) {
            args.push(arguments[i])
          }
          console.log('%ccanvas: %O', 'color: darkorange', this)
          console.log(
            `%cset ${key} with: [${args}], %O`,
            'color: darkorange',
            arguments
          )
          return oldSetter.apply(this, arguments)
        }
      }
      Object.defineProperty(canvasProto, key, {
        ...des,
        ...updaters,
      })
    }
  })

  function isGetter(des) {
    return typeof des['get'] === 'function'
  }

  function isSetter(des) {
    return typeof des['set'] === 'function'
  }

  function isMethod(des) {
    return typeof des['value'] === 'function'
  }
})()
