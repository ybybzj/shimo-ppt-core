export interface FontMeta {
  fontFamily: string
  /** 别名，解决从MSOffice导入的字体名不规范问题 */
  alias?: string[]
  url: string
  textMetrics: {
    ascender: number
    descender: number
    unitsPerEm: number
    lineGap: number
  }
  thumbnail?: string
  fullName?: string
  fontName?: {
    win?: string
    mac?: string
    linux?: string
    ios?: string
    android?: string
  }
  isSymbolFont?: boolean
}

/**
 * 字体引擎和信息与专业文档同步
 *
 * - 字体列表：https://github.com/ShimoFour/shark/blob/develop/packages/core-editor/src/fonts.ts
 * - 字体预览图：https://github.com/ShimoFour/shark/tree/develop/packages/core-view/src/assets/images/font
 */
const fonts: { [key: string]: FontMeta } = {
  // lineGap 实际参数为 0，但计算后跟 word 有差别，根据 hh 的 magic number 反推 lineGap 约为 378
  MicrosoftYahei: {
    fontFamily: 'Microsoft Yahei',
    fullName: '微软雅黑',
    textMetrics: {
      unitsPerEm: 2048,
      ascender: 2167,
      descender: -536,
      lineGap: 378,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/b52cc006349033c1b7f924f242248a5d.woff',
  },
  // Songti SC 实际参数为： { unitsPerEm: 1000, ascender: 1060, descender: -340, lineGap: 0 }
  // 但为了计算行高等保持跟 word 一致，使用 word 中的 SimSun 参数
  // 这个和下面的 SimSun 是同一个，应该只需要一个字体即可
  // SongtiSC: {
  //   fontFamily: 'Songti SC',
  //   fullName: a18n('宋体'),
  //   textMetrics: {
  //     unitsPerEm: 256,
  //     ascender: 220,
  //     descender: -36,
  //     lineGap: 36,
  //   },
  //   url: SongtiSC,
  // },
  SimSun: {
    fontFamily: 'SimSun',
    fullName: '宋体',
    textMetrics: {
      unitsPerEm: 256,
      ascender: 220,
      descender: -36,
      lineGap: 36,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/c8199714524553fd9f837124cc97edca.woff',
  },
  SimHei: {
    fontFamily: 'SimHei',
    fullName: '黑体',
    textMetrics: {
      unitsPerEm: 256,
      ascender: 220,
      descender: -36,
      lineGap: 36,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/a8f1d4ba143163586525547247275a1d.woff',
  },
  FangSong_GB2312: {
    fontFamily: 'FangSong_GB2312',
    fullName: '仿宋_GB2312',
    textMetrics: {
      unitsPerEm: 256,
      ascender: 220,
      descender: -36,
      lineGap: 36,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/285d783dae8d082415badbee391e6f58.woff',
  },
  FangSong: {
    fontFamily: 'FangSong',
    fullName: '仿宋',
    textMetrics: {
      unitsPerEm: 256,
      ascender: 220,
      descender: -36,
      lineGap: 36,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/0be615d20de22056451f8326ea543b24.woff',
  },
  KaiTi_GB2312: {
    fontFamily: 'KaiTi_GB2312',
    fullName: '楷体_GB2312',
    textMetrics: {
      unitsPerEm: 256,
      ascender: 220,
      descender: -36,
      lineGap: 36,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/507606d2b34f19e429c5b5aeb854ae4b.woff',
  },
  KaiTi: {
    fontFamily: 'KaiTi',
    fullName: '楷体',
    textMetrics: {
      unitsPerEm: 256,
      ascender: 220,
      descender: -36,
      lineGap: 36,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/422f6025a86fcb9c766d927ee6967cfa.woff',
  },
  FZXiaoBiaoSong_B05S: {
    fontFamily: 'FZXiaoBiaoSong-B05S',
    fullName: '方正小标宋简体',
    textMetrics: {
      unitsPerEm: 256,
      ascender: 231,
      descender: -74,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/28421fc6459bfd371151b195bc3bddc9.woff',
  },
  FZXiaoBiaoSongGBK: {
    fontFamily: 'FZXiaoBiaoSongGBK',
    fullName: '方正小标宋_GBK',
    textMetrics: {
      unitsPerEm: 256,
      ascender: 231,
      descender: -74,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/f5a09efe3b0113836a1535cede56bdec.woff',
  },
  FZFangSongGBK: {
    fontFamily: 'FZFangSongGBK',
    fullName: '方正仿宋_GBK',
    textMetrics: {
      unitsPerEm: 256,
      ascender: 226,
      descender: -62,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/dd78a03002be6709a1dbf3f4bf8cb0a1.woff',
  },
  FZHeiTiGBK: {
    fontFamily: 'FZHeiTiGBK',
    fullName: '方正黑体_GBK',
    textMetrics: {
      unitsPerEm: 256,
      ascender: 230,
      descender: -58,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/e2cdf076287d329f3c5bd218f2d9cf0e.woff',
  },
  FZDaHei_B02S: {
    fontFamily: 'FZDaHei-B02S',
    fullName: '方正大黑简体',
    textMetrics: {
      unitsPerEm: 256,
      ascender: 229,
      descender: -62,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/078ea83d344a94d7e994ccdff54500f7.woff',
  },
  FZKaiTiGBK: {
    fontFamily: 'FZKaiTiGBK',
    fullName: '方正楷体_GBK',
    textMetrics: {
      unitsPerEm: 256,
      ascender: 221,
      descender: -57,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/64012a50e0b25efe950e16bf83f164a8.woff',
  },
  STLiti: {
    fontFamily: 'STLiti',
    fullName: '华文隶书',
    textMetrics: {
      unitsPerEm: 1000,
      ascender: 800,
      descender: -259,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/61affead7e371b5f62990eca801af587.woff',
  },
  LiSu: {
    fontFamily: 'LiSu',
    fullName: '隶书',
    textMetrics: {
      unitsPerEm: 256,
      ascender: 220,
      descender: -36,
      lineGap: 36,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/b6d6fda5e5cdaaa6cc9c6ec771dfa760.woff',
  },
  STKaiti: {
    fontFamily: 'STKaiti',
    fullName: '华文楷体',
    textMetrics: {
      unitsPerEm: 1000,
      ascender: 986,
      descender: -315,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/23a69de2b90d20c477092c9429767024.woff',
  },
  STZhongSong: {
    fontFamily: 'STZhongSong',
    fullName: '华文中宋',
    textMetrics: {
      unitsPerEm: 1000,
      ascender: 1007,
      descender: -318,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/2fe95a09cf2e71ff170b6480a31d81b2.woff',
  },
  Arial: {
    fontFamily: 'Arial',
    textMetrics: {
      unitsPerEm: 2048,
      ascender: 1854,
      descender: -434,
      lineGap: 67,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/a0283dc3ff28ba602496ba08582aaf1a.woff',
  },
  Calibri: {
    fontFamily: 'Calibri',
    textMetrics: {
      unitsPerEm: 2048,
      ascender: 1536,
      descender: -512,
      lineGap: 452,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/6671925e340e4164083c31d7496ec456.woff',
  },
  CourierNew: {
    fontFamily: 'Courier New',
    textMetrics: {
      unitsPerEm: 2048,
      ascender: 1705,
      descender: -615,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/e6ac1ca4943d48f0e18a69fd85234f20.woff',
  },
  SourceCodePro: {
    fontFamily: 'Source Code Pro',
    textMetrics: {
      unitsPerEm: 1000,
      ascender: 984,
      descender: -273,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/061b09795a8b060f1df6a5873ade2b5e.woff',
  },
  TimesNewRoman: {
    fontFamily: 'Times New Roman',
    textMetrics: {
      unitsPerEm: 2048,
      ascender: 1825,
      descender: -443,
      lineGap: 87,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/f23a25daf5267cfbe66eba013fc4a4a7.woff',
  },
  Verdana: {
    fontFamily: 'Verdana',
    textMetrics: {
      unitsPerEm: 2048,
      ascender: 2059,
      descender: -430,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/47d48ab88d53f78859b8440c7e6c792b.woff',
  },
  Wingdings: {
    fontFamily: 'Wingdings',
    textMetrics: {
      unitsPerEm: 2048,
      ascender: 1841,
      descender: -432,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/6bc06fda4398d5c89493bc937a090d93.woff',
    isSymbolFont: true,
  },
  SymbolFont: {
    fontFamily: 'Symbol',
    textMetrics: {
      unitsPerEm: 2048,
      ascender: 1436,
      descender: -612,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/518fe663582115c08edcab5635ad73da.woff',
    isSymbolFont: true,
  },
  NotoSansJPRegular: {
    fontFamily: 'Noto Sans JP Regular',
    fullName: 'Noto Sans JP',
    textMetrics: {
      unitsPerEm: 1000,
      ascender: 1160,
      descender: -320,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/0c2a4c994ca926395b73548a5152101d.otf',
  },
  NotoSerifJP: {
    fontFamily: 'Noto Serif JP',
    fullName: 'Noto Serif JP',
    textMetrics: {
      unitsPerEm: 1000,
      ascender: 1151,
      descender: -286,
      lineGap: 0,
    },
    url:
      'https://assets.smcdn.cn/docx/assets/fonts/0e381bfaf5093058518dcf4c282f60f6.otf',
  },
}

export const getFonts = () => {
  return fonts
}

export const getFontByName = (name: string): FontMeta | undefined => {
  if (!name) {
    return undefined
  }
  for (const [key, font] of Object.entries(fonts)) {
    if (
      name === key ||
      name === font.fontFamily ||
      name === font.fontName ||
      name === font.fullName ||
      font.alias?.includes(name)
    ) {
      return font
    }
  }

  return undefined
}
