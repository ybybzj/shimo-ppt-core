#!/usr/bin/env node

const parseArgs = require('yargs-parser')
const cp = require('child_process')

const run = (command, print = false) => {
  if (print) {
    console.info('[run]', command)
  }
  return cp.execSync(command, { encoding: 'utf-8' })
}

const git = {
  getCurrentBranch: () => {
    const branch = process.env['CI_COMMIT_BRANCH']
    return branch ?? run(`git rev-parse --abbrev-ref HEAD`).trim()
  },
  getLastCommitTimestamp: () => {
    return new Date(run(`git log -1 --format=%cd `).trim()).getTime()
  },
  isWorkspaceClean: () => {
    return !run(`git status --porcelain`).trim()
  },
}

const ci = {
  isPullRequest: () => {
    return Boolean(process.env.DRONE_PULL_REQUEST)
  },
  isPushTag: () => {
    return Boolean(process.env.DRONE_TAG)
  },
}

const npm = {
  publishedVersions: (() => {
    const versions = run(`npm dist-tag ls`)
      .trim()
      .split('\n')
      .map((line) => {
        const [tag, version] = line.split(': ')
        return { tag, version }
      })
    // console.info('existed:')
    // console.info(versions)
    return versions
  })(),

  getPublishedVersion(tag) {
    const versions = npm.publishedVersions
    const ver = versions.find((v) => v.tag === tag)
    return ver ? ver.version : '1.0.0'
  },

  parse(version) {
    const [major, minor, patch, ...rest] = version.split(/[.-]/g)
    const prereleaseVersion = rest.pop()
    const prereleaseName = rest.join('-')

    return {
      major: Number(major),
      minor: Number(minor),
      patch: Number(patch),
      prereleaseName,
      prereleaseVersion: Number(prereleaseVersion),
    }
  },

  // part: major/minor/patch/prepreleaseVersion
  bump(version, part = 'patch') {
    const v = npm.parse(version)

    switch (part) {
      case 'major':
        v.major++
        v.minor = 0
        v.patch = 0
        v.prereleaseVersion = 0
        break
      case 'minor':
        v.minor++
        v.patch = 0
        v.prereleaseVersion = 0
        break
      case 'patch':
        v.patch++
        v.prereleaseVersion = 0
        break
      case 'prereleaseVersion':
        v.prereleaseVersion = (v.prereleaseVersion || 0) + 1
        break

      default:
        throw new TypeError('illegal part: ' + part)
    }

    return (
      [v.major, v.minor, v.patch].join('.') +
      (v.prereleaseName && v.prereleaseVersion
        ? `-${v.prereleaseName}.${v.prereleaseVersion}`
        : '')
    )
  },

  compare(versionA, versionB) {
    const a = npm.parse(versionA)
    const b = npm.parse(versionB)
    if (a.major !== b.major) {
      return a.major - b.major
    }
    if (a.minor !== b.minor) {
      return a.minor - b.minor
    }
    if (a.patch !== b.patch) {
      return a.patch - b.patch
    }
    if (a.prereleaseName && a.prereleaseName === b.prereleaseName) {
      return a.prereleaseVersion - b.prereleaseVersion
    }
    return 0
  },
}

const showHelp = () => {
  console.info(
    `
根据当前分支，指定或生成合适的 npm 版本号，发布

示例:
  yarn run bumpversion --version=patch
参数：
  --version=    major|minor|patch。要递增版本号的哪个部分, 默认会按当前分支确定版本
                该选项只能在release分支上使用
  --run         如果不加这个参数，为 dryrun
  --force       不检查是否有未提交的文件
说明：
  假设当前release版本1.2.3，对不同分支会生成以下版本号:
    branch=master       version=1.2.4-beta.x   npmTag=master
    branch=release      version=1.2.4          npmTag=release
    branch=feature-xyz  version=1.2.4-xyz.x    npmTag=xyz
`.trim()
  )
}

const quit = (error) => {
  if (error) {
    console.warn(`[error]`, error)
    console.info(`see help with '--help'`)
    process.exit(-1)
    return -1
  } else {
    process.exit(0)
    return 0
  }
}

const getNextVersion = (part) => {
  let ret = {} // version, npmTag

  const branch = git.getCurrentBranch()
  console.info('current branch:', branch)

  const npmTag = branch.replace(/^feature-/, '')
  console.info('current npmTag:', npmTag)
  const publishedVersion = npm.getPublishedVersion(npmTag)
  console.info('current version:', publishedVersion)

  const releaseVersion = npm.getPublishedVersion('release')
  console.info('release version:', releaseVersion)
  if (npmTag === 'release') {
    ret.version = npm.bump(releaseVersion, part || 'patch')
  } else {
    const nextReleaseVersion = npm.bump(releaseVersion, 'patch')
    const prereleaseName =
      branch === 'master' || npmTag === 'HEAD' ? 'beta' : npmTag

    if (
      !publishedVersion ||
      npm.compare(publishedVersion, nextReleaseVersion) < 0
    ) {
      ret.version = nextReleaseVersion + `-${prereleaseName}.1`
    } else {
      const parsed = npm.parse(publishedVersion)
      if (prereleaseName !== parsed.prereleaseName) {
        ret.version = npm.bump(
          nextReleaseVersion + `-${prereleaseName}.${parsed.prereleaseVersion}`,
          'prereleaseVersion'
        )
      } else {
        ret.version = npm.bump(publishedVersion, 'prereleaseVersion')
      }
    }
  }

  ret.npmTag = npmTag
  return ret
}

const main = async () => {
  const {
    version,
    force,
    run: justRun,
    help,
  } = parseArgs(process.argv.slice(2), {
    boolean: ['run', 'force'],
  })

  if (help) {
    showHelp()
    return quit()
  }

  if (ci.isPullRequest()) {
    console.warn('pull request, skipping publish')
    return quit()
  }
  if (ci.isPushTag()) {
    console.warn('push tag, skipping publish')
    return quit()
  }

  if (!force && !git.isWorkspaceClean()) {
    return quit(`you have uncommitted files (use --force to ignore)`)
  }

  const branch = git.getCurrentBranch()
  if (version && branch !== 'release') {
    return quit(`you can only specify "version" on 'release' branch`)
  }
  if (version && !['major', 'minor', 'patch'].includes(version)) {
    return quit(`unknown version: must be one of major, minor, patch`)
  }

  const next = getNextVersion(version)
  console.info('next: ', next)

  const commands = [
    // prepare
    version && `yarn && yarn test && yarn build:release`,

    // npm
    `npm --version`,
    `npm version ${next.version} --no-git-tag-version --force --allow-same-version`,
    `npm publish --registry=http://registry.npm.shimo.run ${
      next.npmTag ? `--tag=${next.npmTag}` : ''
    }`,

    // git
    // version && `git reset --hard`, // reset packge.json#version
    version && `yarn conventional-changelog -p angular -i CHANGELOG.md -s -r 0`,
    version &&
      `git add . && git commit -m 'v${next.version}  [CI SKIP]' || echo 'nothing to commit'`, // https://docs.drone.io/pipeline/skipping/
    version && `git push origin ${branch} --force`,
    version && `git tag v${next.version}`,
    version && `git push origin v${next.version}`,
  ].filter(Boolean)

  if (justRun) {
    for (const cmd of commands) {
      run(cmd, true)
    }
  } else {
    console.info(`copy & run:`)
    console.info(`---------------------------`)
    console.info(commands.join('\n'))
    console.info(`---------------------------`)
  }
}

main().catch((e) => {
  console.error(e)
  process.exit(-1)
})
