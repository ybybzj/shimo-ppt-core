#!/usr/bin/env node

const cp = require('child_process')

const run = (command, print = false) => {
  if (print) {
    console.info('[run]', command)
  }
  return cp.execSync(command, { encoding: 'utf-8' })
}

const getCurrentBranch = () => {
  const branch = process.env['CI_COMMIT_BRANCH']
  return branch ?? run(`git rev-parse --abbrev-ref HEAD`).trim()
}

const npm = {
  publishedVersions: (() => {
    const versions = run(`npm dist-tag ls`)
      .trim()
      .split('\n')
      .map((line) => {
        const [tag, version] = line.split(': ')
        return { tag, version }
      })
    // console.info('existed:')
    // console.info(versions)
    return versions
  })(),

  getPublishedVersion(tag) {
    const versions = npm.publishedVersions
    const ver = versions.find((v) => v.tag === tag)
    return ver ? ver.version : '1.0.0'
  },
}
const branch = getCurrentBranch()

const npmTag = branch.replace(/^feature-/, '')
const publishedVersion = npm.getPublishedVersion(npmTag)
console.log(publishedVersion)
