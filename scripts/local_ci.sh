set -xe

export CI=true

# ensure we are at latest commit
BRANCH=$(git symbolic-ref --short HEAD)
git branch --set-upstream-to=origin/${BRANCH} ${BRANCH}
git push || git pull --rebase

# build
yarn
yarn lint
yarn test
export NODE_ENV=${BRANCH} && yarn build && echo 'build success'
yarn run bumpversion --force --run

# reset package.json change
git checkout -- package.json

# push commit
git push

echo "success"
