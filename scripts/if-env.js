#!/usr/bin/env node

var parse = require('querystring').parse
var conditions = process.argv.slice(2)
var query = conditions.join('&')
var expected = parse(query)

Object.keys(expected).forEach((env) => {
  const envVal = process.env[env]
  console.log(`process.env[${env}]:`, envVal)
  const expectedVals = expected[env].split(',').map((s) => s.trim())

  console.log(`expected vals for ${env}:`, expectedVals)
  if (expectedVals.indexOf(envVal) <= -1) {
    console.log('Run Else Case...')
    process.exit(1)
  }
})
console.log('Run If Case...')
process.exit(0)
