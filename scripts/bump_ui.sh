set -e

cd ../lizard-service-presentation-sdk
git checkout develop
git pull --rebase
yarn add @shimo/shimo-ppt-core@master
git add .
if [[ -z $1 ]]; then
  git commit -m "chore: core@master "
else
  git commit -m "chore: core@master - $1"
fi
git push