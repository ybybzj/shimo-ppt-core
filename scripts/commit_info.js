#!/usr/bin/env node

const cp = require('child_process')

const run = (command, print = false) => {
  if (print) {
    console.info('[run]', command)
  }
  return cp.execSync(command, { encoding: 'utf-8' })
}

const git = {
  getCurrentBranch: () => {
    return run(`git rev-parse --abbrev-ref HEAD`).trim()
  },
  getLastCommitDateTime: () => {
    return run(`git log -1 --format=%cd `).trim()
  },
  getShortCommit: () => {
    return run(`git rev-parse --short HEAD`).trim()
  },
}

console.log(JSON.stringify({
  branch: git.getCurrentBranch(),
  commit: git.getShortCommit(),
  time: git.getLastCommitDateTime(),
}))
