/* TypeScript file generated from ParagraphContentType.res by genType. */

/* eslint-disable */
/* tslint:disable */

import * as ParagraphContentTypeJS from './ParagraphContentType.res.js';

import type {Paragraph_itemWithContent as Core_Paragraph_itemWithContent} from '../../../src/re/Core.gen';

import type {Paragraph_item as Core_Paragraph_item} from '../../../src/re/Core.gen';

import type {Paragraph_runElement as Core_Paragraph_runElement} from '../../../src/re/Core.gen';

export const toRunElement: (runElement:Core_Paragraph_runElement) => Core_Paragraph_item = ParagraphContentTypeJS.toRunElement as any;

export const toItemWithContent: (itemWithContent:Core_Paragraph_itemWithContent) => Core_Paragraph_item = ParagraphContentTypeJS.toItemWithContent as any;
