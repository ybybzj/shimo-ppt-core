// Generated by ReScript, PLEASE EDIT WITH CARE

import * as Belt_Range from "rescript/lib/es6/belt_Range.js";
import * as Caml_option from "rescript/lib/es6/caml_option.js";
import * as Core$ShimoPptCore from "../Core.res.js";
import * as Utils$ShimoPptCore from "../../../re/Utils.res.js";

function _getIndexAndItemAtLevel(itemWithContent, posArray, level, defaultEdge) {
  var contentLength = Core$ShimoPptCore.Paragraph.lengthOfContent(itemWithContent);
  var pos = posArray[level];
  var index_level = pos !== undefined ? pos : (
      defaultEdge === "ContentEnd" ? contentLength - 1 | 0 : 0
    );
  var _level_index = index_level;
  while(true) {
    var level_index = _level_index;
    if (level_index < 0 || level_index > contentLength) {
      return ;
    }
    var itemAtLevel = Core$ShimoPptCore.Paragraph.itemOfContentAt(itemWithContent, level_index);
    if (itemAtLevel !== undefined) {
      return [
              level_index,
              itemAtLevel
            ];
    }
    var next_index;
    next_index = defaultEdge === "ContentEnd" ? level_index - 1 | 0 : level_index + 1 | 0;
    _level_index = next_index;
    continue ;
  };
}

function _matchPosition(_itemWithContent, posArray, _level, defaultEdge, _matchResult) {
  while(true) {
    var matchResult = _matchResult;
    var level = _level;
    var itemWithContent = _itemWithContent;
    if (level >= 3) {
      return ;
    }
    var levelIndexAndItem = _getIndexAndItemAtLevel(itemWithContent, posArray, level, defaultEdge);
    if (levelIndexAndItem === undefined) {
      return ;
    }
    var item = levelIndexAndItem[1];
    var levelIndex = levelIndexAndItem[0];
    if (item.TAG === "RunElement") {
      if (level === 2 || level === 1) {
        matchResult.push([
              levelIndex,
              0
            ]);
        return matchResult;
      } else {
        return ;
      }
    }
    var itemWithContent$1 = item._0;
    if (!(level === 0 || level === 1)) {
      return ;
    }
    _matchResult = Utils$ShimoPptCore.Arr.push(matchResult, [
          levelIndex,
          Core$ShimoPptCore.Paragraph.lengthOfContent(itemWithContent$1)
        ]);
    _level = level + 1 | 0;
    _itemWithContent = itemWithContent$1;
    continue ;
  };
}

function _fromArrayPos(paragraph, posArray, defaultEdge) {
  var arr = posArray.length !== 0 ? posArray : [0];
  var levels = _matchPosition(paragraph, arr, 0, defaultEdge, []);
  if (levels === undefined) {
    return ;
  }
  var len = levels.length;
  if (len !== 2) {
    if (len !== 3) {
      return ;
    }
    var l1 = levels[0];
    var l2 = levels[1];
    var l3 = levels[2];
    return {
            TAG: "HyperlinkRunItemPos",
            _0: [
              l1,
              l2,
              l3[0]
            ]
          };
  }
  var l1$1 = levels[0];
  var l2$1 = levels[1];
  return {
          TAG: "RunItemPos",
          _0: [
            l1$1,
            l2$1[0]
          ]
        };
}

function fromPos(paragraph, pos) {
  var posArray = Core$ShimoPptCore.Paragraph.arrayFromPos(pos);
  return _fromArrayPos(paragraph, posArray, "ContentStart");
}

function fromPosArray(paragraph, arr) {
  return _fromArrayPos(paragraph, arr, "ContentStart");
}

function endIndicesFromPosArray(paragraph, arr) {
  return _fromArrayPos(paragraph, arr, "ContentEnd");
}

function endIndicesFromPos(paragraph, pos) {
  var posArray = Core$ShimoPptCore.Paragraph.arrayFromPos(pos);
  return _fromArrayPos(paragraph, posArray, "ContentEnd");
}

function toPosArray(t) {
  if (t.TAG === "RunItemPos") {
    var match = t._0;
    return [
            match[0][0],
            match[1]
          ];
  }
  var match$1 = t._0;
  return [
          match$1[0][0],
          match$1[1][0],
          match$1[2]
        ];
}

function _updateElementLevelIndex(t, runElementIndex) {
  if (t.TAG === "RunItemPos") {
    return {
            TAG: "RunItemPos",
            _0: [
              t._0[0],
              runElementIndex
            ]
          };
  }
  var match = t._0;
  return {
          TAG: "HyperlinkRunItemPos",
          _0: [
            match[0],
            match[1],
            runElementIndex
          ]
        };
}

function updateElementLevelIndexFromPos(paragraph, pos, runElementIndex) {
  var t = fromPos(paragraph, pos);
  if (t !== undefined) {
    return _updateElementLevelIndex(t, runElementIndex);
  }
  
}

function compare(left, right) {
  if (left.TAG === "RunItemPos") {
    var match = left._0;
    var l1 = match[0];
    if (right.TAG !== "RunItemPos") {
      return l1[0] - right._0[0][0] | 0;
    }
    var match$1 = right._0;
    var c = l1[0] - match$1[0][0] | 0;
    if (c !== 0) {
      return c;
    } else {
      return match[1] - match$1[1] | 0;
    }
  }
  var match$2 = left._0;
  var l1$1 = match$2[0];
  if (right.TAG === "RunItemPos") {
    return l1$1[0] - right._0[0][0] | 0;
  }
  var match$3 = right._0;
  var c$1 = l1$1[0] - match$3[0][0] | 0;
  if (c$1 !== 0) {
    return c$1;
  }
  var c$2 = match$2[1][0] - match$3[1][0] | 0;
  if (c$2 !== 0) {
    return c$2;
  } else {
    return match$2[2] - match$3[2] | 0;
  }
}

function decrease(paragraph, t) {
  var a = toPosArray(t);
  var len = a.length;
  var adjustArrPos;
  if (len !== 2) {
    if (len !== 3) {
      adjustArrPos = a;
    } else {
      var l1 = a[0];
      var l2 = a[1];
      var l3 = a[2];
      var newL3 = l3 - 1 | 0;
      if (newL3 >= 0) {
        adjustArrPos = [
          l1,
          l2,
          newL3
        ];
      } else {
        var newL2 = l2 - 1 | 0;
        if (newL2 >= 0) {
          adjustArrPos = [
            l1,
            newL2
          ];
        } else {
          var newL1 = l1 - 1 | 0;
          adjustArrPos = newL1 < 0 ? undefined : [newL1];
        }
      }
    }
  } else {
    var l1$1 = a[0];
    var l2$1 = a[1];
    var newL2$1 = l2$1 - 1 | 0;
    if (newL2$1 >= 0) {
      adjustArrPos = [
        l1$1,
        newL2$1
      ];
    } else {
      var newL1$1 = l1$1 - 1 | 0;
      adjustArrPos = newL1$1 < 0 ? undefined : [newL1$1];
    }
  }
  if (adjustArrPos === undefined) {
    return ;
  }
  var t$1 = _fromArrayPos(paragraph, adjustArrPos, "ContentEnd");
  if (t$1 !== undefined) {
    return t$1;
  }
  
}

function increase(paragraph, t) {
  var l1Count = Core$ShimoPptCore.Paragraph.lengthOfContent(paragraph);
  var adjustArrPos;
  if (t.TAG === "RunItemPos") {
    var match = t._0;
    var l1 = match[0];
    var newL2 = match[1] + 1 | 0;
    if (newL2 < l1[1]) {
      adjustArrPos = [
        l1[0],
        newL2
      ];
    } else {
      var newL1 = l1[0] + 1 | 0;
      adjustArrPos = newL1 < l1Count ? [newL1] : undefined;
    }
  } else {
    var match$1 = t._0;
    var l2 = match$1[1];
    var l1$1 = match$1[0];
    var newL3 = match$1[2] + 1 | 0;
    if (newL3 < l2[1]) {
      adjustArrPos = [
        l1$1[0],
        l2[0],
        newL3
      ];
    } else {
      var newL2$1 = l2[0] + 1 | 0;
      if (newL2$1 < l1$1[1]) {
        adjustArrPos = [
          l1$1[0],
          newL2$1
        ];
      } else {
        var newL1$1 = l1$1[0] + 1 | 0;
        adjustArrPos = newL1$1 < l1Count ? [newL1$1] : undefined;
      }
    }
  }
  if (adjustArrPos === undefined) {
    return ;
  }
  var t$1 = _fromArrayPos(paragraph, adjustArrPos, "ContentStart");
  if (t$1 !== undefined) {
    return t$1;
  }
  
}

function _runElementAt(itemWithContent, index) {
  var opt = Core$ShimoPptCore.Paragraph.itemOfContentAt(itemWithContent, index);
  if (opt !== undefined) {
    var item = Caml_option.valFromOption(opt);
    if (item.TAG === "RunElement") {
      return Caml_option.some(item._0);
    } else {
      return ;
    }
  }
  
}

function getRunElement(paragraph, t) {
  var continueGet = function (itemWithContent, index, nextLevelIndex, getter) {
    var opt = Core$ShimoPptCore.Paragraph.itemOfContentAt(itemWithContent, index);
    if (opt !== undefined) {
      var item = Caml_option.valFromOption(opt);
      if (item.TAG === "RunElement") {
        return ;
      } else {
        return getter(item._0, nextLevelIndex);
      }
    }
    
  };
  if (t.TAG === "RunItemPos") {
    var match = t._0;
    return continueGet(paragraph, match[0][0], match[1], _runElementAt);
  }
  var match$1 = t._0;
  var l3 = match$1[2];
  return continueGet(paragraph, match$1[0][0], match$1[1][0], (function (itemWithContent, index) {
                return continueGet(itemWithContent, index, l3, _runElementAt);
              }));
}

function _iterateAt(paragraph, start, end, visitor) {
  var continueVisit = function (itemWithContent, index, parentIndice, childrenStartIndex, childrenEndIndex, iterator) {
    var o = Core$ShimoPptCore.Paragraph.itemOfContentAt(itemWithContent, index);
    if (o !== undefined) {
      var item = Caml_option.valFromOption(o);
      if (item.TAG === "RunElement") {
        return ;
      }
      var itemWithContent$1 = item._0;
      var lenOfContent = Core$ShimoPptCore.Paragraph.lengthOfContent(itemWithContent$1);
      var childParentIndice = Utils$ShimoPptCore.Arr.copyPush(parentIndice, [
            index,
            lenOfContent
          ]);
      var start = childrenStartIndex < 0 ? 0 : childrenStartIndex;
      var end = childrenEndIndex < 0 ? lenOfContent - 1 | 0 : childrenEndIndex;
      return Belt_Range.forEach(start, end, (function (childIndex) {
                    iterator(itemWithContent$1, childParentIndice, childIndex);
                  }));
    }
    
  };
  var _visitAt = function (itemWithContent, parentIndice, index) {
    var o = _runElementAt(itemWithContent, index);
    if (o !== undefined) {
      var runElement = Caml_option.valFromOption(o);
      var len = parentIndice.length;
      var t;
      if (len >= 3) {
        t = undefined;
      } else {
        switch (len) {
          case 0 :
              t = undefined;
              break;
          case 1 :
              var l1 = parentIndice[0];
              t = {
                TAG: "RunItemPos",
                _0: [
                  l1,
                  index
                ]
              };
              break;
          case 2 :
              var l1$1 = parentIndice[0];
              var l2 = parentIndice[1];
              t = {
                TAG: "HyperlinkRunItemPos",
                _0: [
                  l1$1,
                  l2,
                  index
                ]
              };
              break;
          
        }
      }
      if (t !== undefined) {
        return visitor(runElement, t);
      } else {
        return ;
      }
    }
    
  };
  if (start.TAG === "RunItemPos") {
    var match = start._0;
    if (end.TAG === "RunItemPos") {
      return continueVisit(paragraph, match[0][0], [], match[1], end._0[1], _visitAt);
    } else {
      return ;
    }
  }
  var match$1 = start._0;
  var l3_start = match$1[2];
  var l2_start = match$1[1];
  if (end.TAG === "RunItemPos") {
    return ;
  }
  var match$2 = end._0;
  var l3_end = match$2[2];
  var l2_end = match$2[1];
  continueVisit(paragraph, match$1[0][0], [], l2_start[0], l2_end[0], (function (itemWithContent, parentIndice, index) {
          var match = index === l2_start[0] ? [
              l3_start,
              -1
            ] : (
              index === l2_end[0] ? [
                  -1,
                  l3_end
                ] : [
                  -1,
                  -1
                ]
            );
          continueVisit(itemWithContent, index, parentIndice, match[0], match[1], _visitAt);
        }));
}

function _getLevel1Index(t) {
  return t._0[0][0];
}

function iterateBetween(paragraph, start, end, visitor) {
  var match = Utils$ShimoPptCore.Compare.fromInt(compare(start, end));
  switch (match) {
    case "Equal" :
        var o = getRunElement(paragraph, start);
        if (o !== undefined) {
          var runElement = Caml_option.valFromOption(o);
          return visitor(runElement, start);
        } else {
          return ;
        }
    case "Less" :
        var startL1Index = _getLevel1Index(start);
        var endL1Index = _getLevel1Index(end);
        if (startL1Index === endL1Index) {
          return _iterateAt(paragraph, start, end, visitor);
        }
        if (startL1Index >= endL1Index) {
          return ;
        }
        var startEndEdge = _fromArrayPos(paragraph, [startL1Index], "ContentEnd");
        var endStartEdge = _fromArrayPos(paragraph, [endL1Index], "ContentStart");
        if (startEndEdge !== undefined) {
          _iterateAt(paragraph, start, startEndEdge, visitor);
        }
        Belt_Range.forEach(startL1Index + 1 | 0, endL1Index - 1 | 0, (function (l1) {
                var itemAtLevel = Core$ShimoPptCore.Paragraph.itemOfContentAt(paragraph, l1);
                if (itemAtLevel === undefined) {
                  return ;
                }
                var ss = _fromArrayPos(paragraph, [l1], "ContentStart");
                var ee = _fromArrayPos(paragraph, [l1], "ContentEnd");
                if (ss !== undefined && ee !== undefined) {
                  return _iterateAt(paragraph, ss, ee, visitor);
                }
                
              }));
        if (endStartEdge !== undefined) {
          return _iterateAt(paragraph, endStartEdge, end, visitor);
        } else {
          return ;
        }
    case "Greater" :
        return ;
    
  }
}

export {
  fromPos ,
  endIndicesFromPos ,
  fromPosArray ,
  endIndicesFromPosArray ,
  updateElementLevelIndexFromPos ,
  compare ,
  decrease ,
  increase ,
  toPosArray ,
  getRunElement ,
  iterateBetween ,
}
/* Core-ShimoPptCore Not a pure module */
