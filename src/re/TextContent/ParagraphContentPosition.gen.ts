/* TypeScript file generated from ParagraphContentPosition.resi by genType. */

/* eslint-disable */
/* tslint:disable */

import * as ParagraphContentPositionJS from './ParagraphContentPosition.res.js';

import type {Paragraph_itemWithContent as Core_Paragraph_itemWithContent} from '../../../src/re/Core.gen';

import type {Paragraph_paragraphContentPos as Core_Paragraph_paragraphContentPos} from '../../../src/re/Core.gen';

import type {Paragraph_runElement as Core_Paragraph_runElement} from '../../../src/re/Core.gen';

export abstract class t { protected opaque!: any }; /* simulate opaque types */
export type RunElementIndices = t;

export type visitor = (_1:Core_Paragraph_runElement, _2:t) => void;

export const fromPos: (_1:Core_Paragraph_itemWithContent, _2:Core_Paragraph_paragraphContentPos) => (undefined | t) = ParagraphContentPositionJS.fromPos as any;

export const endIndicesFromPos: (_1:Core_Paragraph_itemWithContent, _2:Core_Paragraph_paragraphContentPos) => (undefined | t) = ParagraphContentPositionJS.endIndicesFromPos as any;

export const fromPosArray: (_1:Core_Paragraph_itemWithContent, _2:number[]) => (undefined | t) = ParagraphContentPositionJS.fromPosArray as any;

export const endIndicesFromPosArray: (_1:Core_Paragraph_itemWithContent, _2:number[]) => (undefined | t) = ParagraphContentPositionJS.endIndicesFromPosArray as any;

export const updateElementLevelIndexFromPos: (_1:Core_Paragraph_itemWithContent, _2:Core_Paragraph_paragraphContentPos, _3:number) => (undefined | t) = ParagraphContentPositionJS.updateElementLevelIndexFromPos as any;

export const compare: (_1:t, _2:t) => number = ParagraphContentPositionJS.compare as any;

export const decrease: (_1:Core_Paragraph_itemWithContent, _2:t) => (undefined | t) = ParagraphContentPositionJS.decrease as any;

export const increase: (_1:Core_Paragraph_itemWithContent, _2:t) => (undefined | t) = ParagraphContentPositionJS.increase as any;

export const toPosArray: (_1:t) => number[] = ParagraphContentPositionJS.toPosArray as any;

export const getRunElement: (_1:Core_Paragraph_itemWithContent, _2:t) => (undefined | Core_Paragraph_runElement) = ParagraphContentPositionJS.getRunElement as any;

export const iterateBetween: (_1:Core_Paragraph_itemWithContent, start:t, end:t, visitor:visitor) => void = ParagraphContentPositionJS.iterateBetween as any;
