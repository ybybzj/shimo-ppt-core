open RescriptCore
%%private(
  @inline
  let opt_do = (o, @inline f) =>
    switch o {
    | Some(v) => f(v)
    | None => ignore()
    }
  @inline
  let opt_flatMap = (opt, f) =>
    switch opt {
    | Some(x) => f(x)
    | None => None
    }
)

type itemWithContentIndex = {
  @as("0") index: int,
  @as("1") contentLength: int,
}
type t =
  | RunItemPos((itemWithContentIndex, int))
  | HyperlinkRunItemPos((itemWithContentIndex, itemWithContentIndex, int))

type contentEdge = ContentEnd | ContentStart

let _getIndexAndItemAtLevel = (itemWithContent, posArray, level, ~defaultEdge: contentEdge) => {
  open Core.Paragraph
  let contentLength = itemWithContent->lengthOfContent
  let index_level = switch posArray[level] {
  | Some(pos) => pos
  | None =>
    switch defaultEdge {
    | ContentStart => 0
    | ContentEnd => contentLength - 1
    }
  }

  let rec _getIndexAndItem = (itemWithContent, level_index, contentLength, edge) => {
    if level_index < 0 || level_index > contentLength {
      None
    } else {
      let itemAtLevel = itemOfContentAt(itemWithContent, level_index)

      switch itemAtLevel {
      | Some(item) => Some((level_index, item))
      | None => {
          let next_index = switch edge {
          | ContentStart => level_index + 1
          | ContentEnd => level_index - 1
          }

          _getIndexAndItem(itemWithContent, next_index, contentLength, edge)
        }
      }
    }
  }

  _getIndexAndItem(itemWithContent, index_level, contentLength, defaultEdge)
}

let rec _matchPosition = (
  itemWithContent,
  posArray,
  level,
  ~defaultEdge: contentEdge,
  ~matchResult: array<itemWithContentIndex>,
) => {
  open Core.Paragraph
  if level >= 3 {
    None
  } else {
    let levelIndexAndItem = _getIndexAndItemAtLevel(itemWithContent, posArray, level, ~defaultEdge)

    switch levelIndexAndItem {
    | Some((levelIndex, item)) =>
      switch item {
      | ItemWithContent(itemWithContent) =>
        switch level {
        | 0
        | 1 =>
          _matchPosition(
            itemWithContent,
            posArray,
            level + 1,
            ~defaultEdge,
            ~matchResult=matchResult->Utils.Arr.push({
              index: levelIndex,
              contentLength: itemWithContent->lengthOfContent,
            }),
          )
        | _ => None
        }
      | RunElement(_) =>
        switch level {
        | 1
        | 2 => {
            matchResult->Array.push({
              index: levelIndex,
              contentLength: 0,
            })
            Some(matchResult)
          }
        | _ => None
        }
      }
    | None => None
    }
  }
}

let _fromArrayPos = (paragraph, posArray, ~defaultEdge: contentEdge) => {
  let arr = switch posArray {
  | [] => [0]
  | a => a
  }

  switch _matchPosition(paragraph, arr, 0, ~defaultEdge, ~matchResult=[]) {
  | Some(levels) =>
    switch levels {
    | [l1, l2] => Some(RunItemPos((l1, l2.index)))
    | [l1, l2, l3] => Some(HyperlinkRunItemPos((l1, l2, l3.index)))
    | _ => None
    }
  | None => None
  }
}

let fromPos = (paragraph, pos) => {
  let posArray = Core.Paragraph.arrayFromPos(pos)

  _fromArrayPos(paragraph, posArray, ~defaultEdge=ContentStart)
}

let fromPosArray = (paragraph, arr) => _fromArrayPos(paragraph, arr, ~defaultEdge=ContentStart)
let endIndicesFromPosArray = (paragraph, arr) =>
  _fromArrayPos(paragraph, arr, ~defaultEdge=ContentEnd)

let endIndicesFromPos = (paragraph, pos) => {
  let posArray = Core.Paragraph.arrayFromPos(pos)

  _fromArrayPos(paragraph, posArray, ~defaultEdge=ContentEnd)
}

let toPosArray = t => {
  switch t {
  | RunItemPos((l1, l2)) => [l1.index, l2]
  | HyperlinkRunItemPos((l1, l2, l3)) => [l1.index, l2.index, l3]
  }
}

let _updateElementLevelIndex = (t, runElementIndex) => {
  switch t {
  | RunItemPos((level1, _)) => RunItemPos((level1, runElementIndex))
  | HyperlinkRunItemPos((level1, level2, _)) =>
    HyperlinkRunItemPos((level1, level2, runElementIndex))
  }
}

let updateElementLevelIndexFromPos = (paragraph, pos, runElementIndex) => {
  let t = fromPos(paragraph, pos)
  switch t {
  | Some(t) => Some(t->_updateElementLevelIndex(runElementIndex))
  | None => None
  }
}

/* --- */
let compare = (left, right) => {
  switch (left, right) {
  | (RunItemPos((l, _)), HyperlinkRunItemPos((r, _, _)))
  | (HyperlinkRunItemPos((l, _, _)), RunItemPos((r, _))) =>
    l.index - r.index
  | (RunItemPos((l1, l2)), RunItemPos((r1, r2))) =>
    switch l1.index - r1.index {
    | 0 => l2 - r2
    | c => c
    }
  | (HyperlinkRunItemPos((l1, l2, l3)), HyperlinkRunItemPos((r1, r2, r3))) =>
    switch l1.index - r1.index {
    | 0 =>
      switch l2.index - r2.index {
      | 0 => l3 - r3
      | c => c
      }
    | c => c
    }
  }
}

let decrease = (paragraph, t) => {
  let adjustArrPos = switch t->toPosArray {
  | [l1, l2] => {
      let newL2 = l2 - 1
      if newL2 >= 0 {
        Some([l1, newL2])
      } else {
        let newL1 = l1 - 1
        newL1 < 0 ? None : Some([newL1])
      }
    }
  | [l1, l2, l3] => {
      let newL3 = l3 - 1
      if newL3 >= 0 {
        Some([l1, l2, newL3])
      } else {
        let newL2 = l2 - 1
        if newL2 >= 0 {
          Some([l1, newL2])
        } else {
          let newL1 = l1 - 1
          newL1 < 0 ? None : Some([newL1])
        }
      }
    }
  | a => Some(a)
  }

  switch adjustArrPos {
  | None => None
  | Some(adjustArrPos) =>
    switch _fromArrayPos(paragraph, adjustArrPos, ~defaultEdge=ContentEnd) {
    | Some(t) => Some(t)
    | None => None
    }
  }
}

let increase = (paragraph, t) => {
  let l1Count = paragraph->Core.Paragraph.lengthOfContent
  let adjustArrPos = switch t {
  | RunItemPos((l1, l2)) => {
      let newL2 = l2 + 1
      if newL2 < l1.contentLength {
        Some([l1.index, newL2])
      } else {
        let newL1 = l1.index + 1
        newL1 < l1Count ? Some([newL1]) : None
      }
    }
  | HyperlinkRunItemPos((l1, l2, l3)) => {
      let newL3 = l3 + 1
      if newL3 < l2.contentLength {
        Some([l1.index, l2.index, newL3])
      } else {
        let newL2 = l2.index + 1
        if newL2 < l1.contentLength {
          Some([l1.index, newL2])
        } else {
          let newL1 = l1.index + 1
          newL1 < l1Count ? Some([newL1]) : None
        }
      }
    }
  }

  switch adjustArrPos {
  | None => None
  | Some(adjustArrPos) =>
    switch _fromArrayPos(paragraph, adjustArrPos, ~defaultEdge=ContentStart) {
    | Some(t) => Some(t)
    | None => None
    }
  }
}

let _runElementAt = (itemWithContent, ~index) => {
  itemWithContent
  ->Core.Paragraph.itemOfContentAt(index)
  ->opt_flatMap(item =>
    switch item {
    | RunElement(e) => Some(e)
    | _ => None
    }
  )
}

let getRunElement = (paragraph, t) => {
  open Core.Paragraph
  let continueGet = (
    itemWithContent,
    ~index,
    ~nextLevelIndex,
    ~getter: (itemWithContent, ~index: int) => option<runElement>,
  ) => {
    itemWithContent
    ->itemOfContentAt(index)
    ->opt_flatMap(item =>
      switch item {
      | RunElement(_) => None
      | ItemWithContent(itemWithContent) => getter(itemWithContent, ~index=nextLevelIndex)
      }
    )
  }

  switch t {
  | RunItemPos((l1, l2)) =>
    paragraph->continueGet(~index=l1.index, ~nextLevelIndex=l2, ~getter=_runElementAt)
  | HyperlinkRunItemPos((l1, l2, l3)) =>
    paragraph->continueGet(~index=l1.index, ~nextLevelIndex=l2.index, ~getter=(
      itemWithContent,
      ~index,
    ) => continueGet(itemWithContent, ~index, ~getter=_runElementAt, ~nextLevelIndex=l3))
  }
}

type visitor = (Core.Paragraph.runElement, t) => unit

let _iterateAt = (paragraph, ~start: t, ~end: t, ~visitor: visitor) => {
  open Core.Paragraph
  let continueVisit = (
    itemWithContent,
    ~index,
    ~parentIndice: array<itemWithContentIndex>,
    ~childrenStartIndex,
    ~childrenEndIndex,
    ~iterator: (itemWithContent, ~parentIndice: array<itemWithContentIndex>, ~index: int) => unit,
  ) => {
    itemWithContent
    ->itemOfContentAt(index)
    ->opt_do(item => {
      switch item {
      | RunElement(_) => ()
      | ItemWithContent(itemWithContent) => {
          let lenOfContent = itemWithContent->lengthOfContent
          let childParentIndice =
            parentIndice->Utils.Arr.copyPush({index, contentLength: lenOfContent})
          let start = childrenStartIndex < 0 ? 0 : childrenStartIndex
          let end = childrenEndIndex < 0 ? lenOfContent - 1 : childrenEndIndex
          Utils.Range.forEach(start, end, childIndex => {
            iterator(itemWithContent, ~parentIndice=childParentIndice, ~index=childIndex)
          })
        }
      }
    })
  }

  let _visitAt = (itemWithContent, ~parentIndice, ~index) => {
    _runElementAt(itemWithContent, ~index)->opt_do(runElement => {
      let t = switch parentIndice {
      | [l1] => Some(RunItemPos((l1, index)))
      | [l1, l2] => Some(HyperlinkRunItemPos((l1, l2, index)))
      | _ => None
      }
      switch t {
      | Some(t) => visitor(runElement, t)
      | _ => ()
      }
    })
  }

  switch (start, end) {
  | (RunItemPos(_), HyperlinkRunItemPos(_))
  | (HyperlinkRunItemPos(_), RunItemPos(_)) => ()
  | (RunItemPos((l1, l2_start)), RunItemPos((_, l2_end))) =>
    paragraph->continueVisit(
      ~index=l1.index,
      ~parentIndice=[],
      ~childrenStartIndex=l2_start,
      ~childrenEndIndex=l2_end,
      ~iterator=_visitAt,
    )
  | (HyperlinkRunItemPos((l1, l2_start, l3_start)), HyperlinkRunItemPos((_, l2_end, l3_end))) =>
    paragraph->continueVisit(
      ~index=l1.index,
      ~parentIndice=[],
      ~childrenStartIndex=l2_start.index,
      ~childrenEndIndex=l2_end.index,
      ~iterator=(itemWithContent, ~parentIndice, ~index) => {
        let (child_start, child_end) = switch index {
        | n if n === l2_start.index => (l3_start, -1)
        | n if n === l2_end.index => (-1, l3_end)
        | _n => (-1, -1)
        }
        continueVisit(
          itemWithContent,
          ~index,
          ~parentIndice,
          ~childrenStartIndex=child_start,
          ~childrenEndIndex=child_end,
          ~iterator=_visitAt,
        )
      },
    )
  }
}

let _getLevel1Index = t =>
  switch t {
  | RunItemPos((l1, _)) => l1.index
  | HyperlinkRunItemPos((l1, _, _)) => l1.index
  }

let iterateBetween = (paragraph, ~start: t, ~end: t, ~visitor: visitor): unit => {
  switch compare(start, end)->Utils.Compare.fromInt {
  | Utils.Compare.Equal =>
    getRunElement(paragraph, start)->opt_do(runElement => visitor(runElement, start))
  | Utils.Compare.Less => {
      let startL1Index = start->_getLevel1Index
      let endL1Index = end->_getLevel1Index
      switch (startL1Index, endL1Index) {
      | (s, e) if s === e => _iterateAt(paragraph, ~start, ~end, ~visitor)
      | (s, e) if s < e => {
          let startEndEdge = paragraph->_fromArrayPos([s], ~defaultEdge=ContentEnd)
          let endStartEdge = paragraph->_fromArrayPos([e], ~defaultEdge=ContentStart)
          switch startEndEdge {
          | Some(t) => _iterateAt(paragraph, ~start, ~end=t, ~visitor)
          | None => ()
          }

          Utils.Range.forEach(s + 1, e - 1, l1 => {
            let itemAtLevel = Core.Paragraph.itemOfContentAt(paragraph, l1)
            switch itemAtLevel {
            | Some(_) => {
                let ss = paragraph->_fromArrayPos([l1], ~defaultEdge=ContentStart)
                let ee = paragraph->_fromArrayPos([l1], ~defaultEdge=ContentEnd)
                switch (ss, ee) {
                | (Some(s), Some(e)) => _iterateAt(paragraph, ~start=s, ~end=e, ~visitor)
                | _ => ()
                }
              }
            | _ => ()
            }
          })
          switch endStartEdge {
          | Some(t) => _iterateAt(paragraph, ~start=t, ~end, ~visitor)
          | None => ()
          }
        }
      | _ => ()
      }
    }
  | Utils.Compare.Greater => ()
  }
}
