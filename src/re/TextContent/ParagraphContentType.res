open Core.Paragraph
@gentype
let toRunElement = runElement => RunElement(runElement)
@gentype
let toItemWithContent = itemWithContent => ItemWithContent(itemWithContent)
