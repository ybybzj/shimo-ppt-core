/* TypeScript file generated from Core.res by genType. */

/* eslint-disable */
/* tslint:disable */

import type {ItemWithContent as $$Paragraph_itemWithContent} from './core/textContent';

import type {Matrix2D as $$matrix} from '../core/graphic/Matrix';

import type {ParagraphElementPosition as $$Paragraph_paragraphContentPos} from '../core/Paragraph/common';

import type {RunContentElement as $$Paragraph_runElement} from '../core/Paragraph/RunElement/type';

import type {SlideElement as $$slideElement} from '../core/SlideElement/type';

export type matrix = $$matrix;

export type slideElement = $$slideElement;

export type Paragraph_runElement = $$Paragraph_runElement;

export type Paragraph_itemWithContent = $$Paragraph_itemWithContent;

export abstract class Paragraph_item { protected opaque!: any }; /* simulate opaque types */
export type ParagraphContnetItem = Paragraph_item;

export type Paragraph_paragraphContentPos = $$Paragraph_paragraphContentPos;
