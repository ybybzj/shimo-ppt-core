/**
 * custom implement modoc string parser utils
 */
import { Dict } from '../../../liber/pervasive'
import {
  AttributeObject,
  Operation,
  OperationAction,
  OperationData,
} from '../../io/operation'
import { checkIsInDebug } from '../../lib/debug/log'
import { ModocDelta } from '../../types/globals/io'
import {
  stringToSlice,
  sliceToString,
  ModocAction as ModocActionRe,
  slice,
  getOpIter,
  OpIter_hasNext,
  OpIter_next,
  getMapIter,
  MapIter_hasNext,
  MapIter_next,
  ModocData as ModocDataRe,
} from '../io/Modoc.gen'
import { resetAttrParseCache } from '../io/ModocAttr'

// for debug
import { invert } from '../io/ModocModel.gen'
;(window as any)['invertDelta'] = invert

export {
  compose as composeDelta,
  invert as invertDelta,
  delta_isEmpty as isDeltaEmpty,
} from '../io/ModocModel.gen'
export { getMapIter, MapIter_hasNext, MapIter_next } from '../io/Modoc.gen'

export { slice as StringSlice, stringToSlice, sliceToString }
export interface PackedOpDataDelta {
  type: 'delta'
  data: slice
}
export interface PackedOpDataMap {
  type: 'map'
  data: slice
}

export interface PackedOpDataJson {
  type: 'json'
  data: Dict
}

export interface PackedOpDataString {
  type: 'string'
  data: string
}

export interface PackedOpDataNumber {
  type: 'number'
  data: number
}

export type PackedOpData =
  | PackedOpDataNumber
  | PackedOpDataString
  | PackedOpDataDelta
  | PackedOpDataMap
  | PackedOpDataJson

export interface OpExtra {
  action: OperationAction
  attributes: AttributeObject
}

export type PackedOpDelta = PackedOpDataDelta & OpExtra
export type PackedOpMap = PackedOpDataMap & OpExtra
export type PackedOpJson = PackedOpDataJson & OpExtra
export type PackedOpString = PackedOpDataString & OpExtra
export type PackedOpNumber = PackedOpDataNumber & OpExtra
export type PackedOperation =
  | PackedOpNumber
  | PackedOpString
  | PackedOpDelta
  | PackedOpMap
  | PackedOpJson

export type OperationFromPackedOp<Packed extends PackedOperation> =
  Packed extends PackedOpDelta
    ? { data: { delta: Operation[] } } & OpExtra
    : Packed extends PackedOpMap
    ? { data: { slide: { [k in string]: Operation[] } } } & OpExtra
    : Packed extends PackedOpJson
    ? { data: { object: Dict } } & OpExtra
    : Packed extends PackedOpString
    ? { data: string } & OpExtra
    : Packed extends PackedOpNumber
    ? { data: number } & OpExtra
    : never

export type MapOperation = {
  data: { slide: { [k in string]: Operation[] } }
} & OpExtra

export type DeltaOperation = {
  data: { delta: Operation[] }
} & OpExtra

export type DeltaIter = {
  hasNext: () => boolean
  next: () => PackedOperation
}

export function getOpIterFromSlice(deltaStr: slice): DeltaIter {
  const iter = getOpIter(deltaStr)
  return {
    hasNext() {
      return OpIter_hasNext(iter)
    },
    next() {
      return toPackedOperation(OpIter_next(iter))
    },
  }
}

export function reduceFromSlice<T>(
  ds: slice,
  reducer: (ret: T, op: PackedOperation) => T,
  init: T
): T {
  const iter = getOpIterFromSlice(ds)
  let ret = init
  while (iter.hasNext()) {
    ret = reducer(ret, iter.next())
  }
  return ret
}

function toPackedOperation(op: ModocActionRe): PackedOperation {
  const action: OperationAction = op.action
  const attributes = op.attr
  let data: PackedOperation['data'] = op.data.data
  switch (op.data.type) {
    case 'json':
      data = JSON.parse(sliceToString(op.data.data))
      break
    case 'string':
      data = sliceToString(op.data.data)
      break
  }
  return {
    type: op.data.type,
    action,
    attributes,
    data,
  } as PackedOperation
}

export function getLengthOfPackedOp(op: PackedOperation) {
  switch (op.type) {
    case 'number':
      return op.data
    case 'string':
      return op.data.length
    case 'map':
    case 'delta':
    case 'json':
      return 1
  }
}

export function getLengthOfDeltaSlice(s: slice) {
  const iter = getOpIterFromSlice(s)
  let len: number = 0
  while (iter.hasNext()) {
    len += getLengthOfPackedOp(iter.next())
  }
  return len
}

// to operations
function opDataFromReOpData(data: ModocDataRe): OperationData {
  switch (data.type) {
    case 'delta':
      return {
        delta: deltaSliceToOperations(data.data),
      }
    case 'map':
      const res: Dict = {}
      const iter = getMapIter(data.data)
      while (MapIter_hasNext(iter)) {
        const { id, delta } = MapIter_next(iter)
        res[id] = deltaSliceToOperations(delta)
      }

      return { slide: res }
    case 'json':
      return {
        object: JSON.parse(sliceToString(data.data)),
      }
    case 'string':
      return sliceToString(data.data)
    case 'number':
      return data.data
  }
}

function opFromReAction(op: ModocActionRe): Operation {
  const action: OperationAction = op.action
  const attributes = op.attr
  const data = opDataFromReOpData(op.data)
  return {
    action,
    data,
    attributes,
  }
}

export function deltaSliceToOperations(
  delta: slice,
  fromIndex = 0
): Operation[] {
  const ops: Operation[] = []
  const iter = getOpIter(delta)
  let i = 0
  while (OpIter_hasNext(iter)) {
    const next = OpIter_next(iter)

    if (i >= fromIndex) {
      ops.push(opFromReAction(next))
    }

    i += 1
  }
  return ops
}

export function ModocDeltaToOperations(delta: ModocDelta): Operation[] {
  if (Array.isArray(delta)) return delta
  const s = delta.stringify()
  const slice = stringToSlice(s)
  const ops = deltaSliceToOperations(slice)

  resetAttrParseCache()
  return ops
}

export function opFromPackedOperation<P extends PackedOperation>(
  op: P
): OperationFromPackedOp<P> {
  return {
    action: op.action,
    data: opDataFromPackedOpData({
      data: op.data,
      type: op.type,
    } as PackedOpData),
    attributes: op.attributes,
  } as OperationFromPackedOp<P>
}

function opDataFromPackedOpData(data: PackedOpData): OperationData {
  switch (data.type) {
    case 'delta':
      return {
        delta: deltaSliceToOperations(data.data),
      }
    case 'map':
      const res: Dict = {}
      const iter = getMapIter(data.data)
      while (MapIter_hasNext(iter)) {
        const { id, delta } = MapIter_next(iter)
        res[id] = deltaSliceToOperations(delta)
      }

      return { slide: res }
    case 'json':
      return {
        object: data.data,
      }
    case 'string':
    case 'number':
      return data.data
  }
}
// helpers
export function printModoc(delta: ModocDelta, msg?: string) {
  if (!checkIsInDebug()) return
  if (msg) {
    console.log(msg)
  }

  if (Array.isArray(delta)) {
    console.log(delta)
    return
  }

  const s = delta.stringify()
  const slice = stringToSlice(s)

  console.time('printModoc')
  const ops = deltaSliceToOperations(slice)
  // const ops = parseDelta(slice).map(opToJSON)

  resetAttrParseCache()
  console.timeEnd('printModoc')
  console.log(ops)
}

// ;(window as any)['compose'] = composeDelta
