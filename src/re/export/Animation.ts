export {
  makeAnimation,
  makeAnimationWithDuration,
  startAnimation as startAnimationRe,
  stopAnimation as stopAnimationRe,
  isAnimationPlaying as isAnimationPlayingRe,
  animation as animationRe,
} from '../Animation.gen'
