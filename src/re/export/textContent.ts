import { Nullable } from '../../../liber/pervasive'
import { ParagraphElementPosition } from '../../core/Paragraph/common'
import { ParaHyperlink } from '../../core/Paragraph/ParaContent/ParaHyperlink'
import { ParaRun } from '../../core/Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../../core/Paragraph/Paragraph'
import { RunContentElement } from '../../core/Paragraph/RunElement/type'
import { TextPr } from '../../core/textAttributes/TextPr'
import { InstanceType } from '../../core/instanceTypes'
import {
  RunElementIndices,
  toPosArray as runElementIndicesToArray,
  updateElementLevelIndexFromPos,
  endIndicesFromPos as runElementEndIndicesFromPos,
  getRunElement as getRunElementByIndices,
  decrease as decreaseRunElementIndices,
  endIndicesFromPosArray,
} from '../TextContent/ParagraphContentPosition.gen'

export {
  RunElementIndices,
  visitor as RunElementVisitor,
  updateElementLevelIndexFromPos,
  fromPos as runElementIndicesFromPos,
  endIndicesFromPos as runElementEndIndicesFromPos,
  iterateBetween as eachRunElementBetween,
  compare as compareIndices,
  getRunElement as getRunElementByIndices,
  decrease as decreaseRunElementIndices,
  increase as increaseRunElementIndices,
} from '../TextContent/ParagraphContentPosition.gen'

export function getRunElementIndicesByElement(
  paragraph: Paragraph,
  elem: ParaHyperlink | ParaRun,
  runElementPos?: number
): Nullable<RunElementIndices> {
  const pos = paragraph.getContentPositionByElement(elem)
  if (pos == null) {
    return undefined
  }
  return updateElementLevelIndexFromPos(paragraph, pos, runElementPos ?? 0)
}

export interface RunAndHyperlinkInfo {
  run: ParaRun
  isInLink: boolean
  isVisited?: boolean
}

export function getRunElementIndexFromIndices(
  indices: RunElementIndices
): number {
  const posArr = runElementIndicesToArray(indices)
  const arrOfLen = posArr.length
  return arrOfLen > 0 ? posArr[arrOfLen - 1] : 0
}

export function updateContentPosByIndices(
  pos: ParagraphElementPosition,
  indices: RunElementIndices
) {
  const posArr = runElementIndicesToArray(indices)
  const arrOfLen = posArr.length
  for (let i = 0; i < arrOfLen; i++) {
    pos.updatePosByLevel(posArr[i], i)
  }
}
export function contentPosFromIndices(indices: RunElementIndices) {
  const pos = new ParagraphElementPosition()
  updateContentPosByIndices(pos, indices)
  return pos
}

export function getRunAndHyperlinkInfo(
  paragraph: Paragraph,
  runIndices: RunElementIndices
): Nullable<RunAndHyperlinkInfo> {
  const posArr = runElementIndicesToArray(runIndices)
  if (posArr.length === 2) {
    const run = paragraph.children.at(posArr[0])
    return run && run.instanceType === InstanceType.ParaRun
      ? {
          run,
          isInLink: false,
        }
      : null
  } else if (posArr.length === 3) {
    const hyperlink = paragraph.children.at(posArr[0])
    if (hyperlink && hyperlink.instanceType === InstanceType.ParaHyperlink) {
      const run = hyperlink.children.at(posArr[1])
      return run && run.instanceType === InstanceType.ParaRun
        ? {
            run,
            isInLink: true,
            isVisited: hyperlink.visited,
          }
        : null
    }
  }
  return null
}

export function getLastRunElementAtLine(
  para: Paragraph,
  lineIndex: number
): Nullable<RunContentElement> {
  const renderingState = para.renderingState
  const segment = renderingState.getSegment(lineIndex)
  if (segment == null) return undefined

  const elementPosition = new ParagraphElementPosition()

  elementPosition.updatePosByLevel(segment.endIndex, 0)

  let endIndices = runElementEndIndicesFromPos(para, elementPosition)
  if (endIndices == null) return undefined
  const ret = getRunElementByIndices(para, endIndices)
  if (ret && ret.instanceType === InstanceType.ParaEnd) {
    endIndices = decreaseRunElementIndices(para, endIndices)

    return endIndices != null
      ? getRunElementByIndices(para, endIndices)
      : undefined
  }
  return ret
}

export function getFirstRunElementInfo(
  para: Paragraph
): Nullable<{ runItem: RunContentElement; pr?: TextPr }> {
  const elmPosArr = [0]

  const indices = endIndicesFromPosArray(para, elmPosArr)
  if (indices == null) return undefined
  const runItem = getRunElementByIndices(para, indices)
  if (runItem) {
    const runInfo = getRunAndHyperlinkInfo(para, indices)
    const pr = runInfo?.run.getCalcedPr(false)
    return {
      runItem,
      pr,
    }
  }
  return undefined
}
