export {
  genAnimQueue as makeShapeAnimationQueue,
  nodeType as ShapeAnimationNodeType,
  node_t as ShapeAnimationNode,
  nodeUpdater as ShapeAnimationNodeUpdater,
} from '../ShapeAnimation/ShapeAnimationNode.gen'
export {
  makePlayer as makeShapeAnimationPlayer,
  ShapeAnimationPlayer,
  ShapeAnimationPlayerCursor,
  StopPlayStatus,
  OnEndHandler,
  isPlaying as isPlayerPlaying,
  isAutoPlay as isPlayerAutoPlay,
  stopAutoPlay as stopPlayerAutopPlay,
  next as playNext,
  reset as resetPlay,
  prev as playPrev,
  stop as stopPlay,
  setCursor as setPlayerCursor,
  resetCursor as resetPlayerCursor,
  resetCursorAtEnd as resetPlayerCursorAtEnd,
  isAtBeginnig as isPlayerCursorAtBeginning,
  isAtEnd as isPlayerCursorAtEnd,
  drawStartFrame,
  drawEndFrame,
  drawFrame,
} from '../ShapeAnimation/ShapeAnimationPlayer.gen'

export {
  AnimationUpdateInfo_t as ShapeAnimationUpdateInfo,
  animQueue as ShapeAnimationQueue,
  AnimationUpdateInfo_transformInfo as ShapeAnimationTransformInfo,
  AnimationUpdateResult_t as AnimationUpdateResult,
  animationUpdater as ShapeAnimationUpdater,
  animationDrawer as ShapeAnimationDrawer,
  AnimationUpdateResult_noAnimation,
  AnimationUpdateResult_notStart,
  AnimationUpdateResult_update,
  AnimationUpdateResult_toUpdateInfo,
} from '../ShapeAnimation/ShapeAnimationType.gen'

import {
  ShapeAnimationPlayer,
  encodeCursor,
  getCursor,
} from '../ShapeAnimation/ShapeAnimationPlayer.gen'

export function getPlayerCursor(player: ShapeAnimationPlayer): string {
  return encodeCursor(getCursor(player))
}
