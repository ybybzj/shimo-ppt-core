import {
  changeShapeSize,
  type changeInfo as ShapeSizeUpdateInfo,
  type slideSize as SlideSize,
  type slideSizeChangeType as SlideSizeChangeInfo,
  compareSlideSizes as compareSlideSizesRe,
  getDestSlideSize,
  compareSlideSizesByDefault as compareSlideSizesByDefaultRe,
} from '../Presentation/ChangeSize.gen'

export function getDestSlideSizeInfo(
  changeInfo: SlideSizeChangeInfo
): SlideSize {
  return getDestSlideSize(changeInfo)
}

export function getScaleUpdateInfoByChangeInfo(
  changeInfo: SlideSizeChangeInfo
): ShapeSizeUpdateInfo {
  return changeShapeSize(changeInfo)
}

/** 内部 apply 默认 Scale/ScaleDown */
export function getDefaultScaleUpdateInfo(args: {
  from: SlideSize
  to: SlideSize
}): ShapeSizeUpdateInfo {
  const defaultInfo = compareSlideSizesByDefaultRe(args.from, args.to)
  return getScaleUpdateInfoByChangeInfo(defaultInfo)
}

export { SlideSizeChangeInfo, ShapeSizeUpdateInfo }

export type SlideSizeComparisonInfo =
  | {
      type: 'ScaleNoScale'
      options: {
        readonly ['scale']: SlideSizeChangeInfo
        readonly ['noScale']: SlideSizeChangeInfo
      }
    }
  | {
      type: 'ScaleUpDown'
      options: {
        readonly ['scaleUp']: SlideSizeChangeInfo
        readonly ['scaleDown']: SlideSizeChangeInfo
      }
    }

export function compareSlideSizes(args: {
  from: SlideSize
  to: SlideSize
}): SlideSizeComparisonInfo {
  const info = compareSlideSizesRe(args.from, args.to)
  if (info.TAG === 'ScaleNoScale') {
    return {
      ['type']: 'ScaleNoScale',
      ['options']: {
        ['scale']: info.scale,
        ['noScale']: info.noScale,
      },
    }
  } else {
    return {
      ['type']: 'ScaleUpDown',
      ['options']: {
        ['scaleUp']: info.scaleUp,
        ['scaleDown']: info.scaleDown,
      },
    }
  }
}
