/* TypeScript file generated from Animation.resi by genType. */

/* eslint-disable */
/* tslint:disable */

import * as AnimationJS from './Animation.res.js';

export type frameCallbackWithDuration = (_1:number) => void;

export type frameCallback = () => void;

export type animationEndCallback = () => void;

export abstract class t { protected opaque!: any }; /* simulate opaque types */
export type animation = t;

export const makeAnimation: (_1:frameCallback) => t = AnimationJS.makeAnimation as any;

export const makeAnimationWithDuration: (_1:frameCallbackWithDuration, endCallback:(undefined | animationEndCallback), duration:number) => t = AnimationJS.makeAnimationWithDuration as any;

export const stopAnimation: (_1:t) => void = AnimationJS.stopAnimation as any;

export const startAnimation: (_1:t) => void = AnimationJS.startAnimation as any;

export const isAnimationPlaying: (_1:t) => boolean = AnimationJS.isAnimationPlaying as any;
