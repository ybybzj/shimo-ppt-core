import { Matrix2D, MatrixUtils } from '../../core/graphic/Matrix'
import { getShapeBounds } from '../../core/utilities/shape/draw'
import { SlideElement } from '../../core/SlideElement/type'
import {
  AnimationUpdateInfo_t as UpdateInfo,
  AnimationUpdateInfo_transformInfo as TransformInfo,
} from '../ShapeAnimation/ShapeAnimationType.gen'

export function toTransformInfo(
  t: UpdateInfo,
  sp: SlideElement
): TransformInfo {
  const spTr = new Matrix2D()
  const bounds = getShapeBounds(sp)
  const orgX = sp.x
  const orgY = sp.y
  const { x, y, rot, w, h } = t

  const curX = orgX + x
  const curY = orgY + y
  const hc = bounds.w * 0.5
  const vc = bounds.h * 0.5

  MatrixUtils.translateMatrix(spTr, -hc, -vc)
  const rad = (-rot * Math.PI) / 180
  MatrixUtils.rotateMatrix(spTr, rad)
  MatrixUtils.scaleMatrix(spTr, w, h)

  MatrixUtils.translateMatrix(spTr, curX + hc, curY + vc)
  sp.updateAnimationDrawInfo({ action: 'Draw', transform: spTr, rot })
  return {
    transform: spTr,
    alpha: t.alpha,
    visibility: t.visibility,
  }
}
