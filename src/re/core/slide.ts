import { toInt } from '../../common/utils'
import { SlideElement } from '../../core/SlideElement/type'

export function hashSlideElement(sp: SlideElement): number {
  const id = sp.getId()
  return toInt(id)
}
