import { Nullable } from '../../../liber/pervasive'
import { ParagraphElementPosition } from '../../core/Paragraph/common'
import { ParaHyperlink } from '../../core/Paragraph/ParaContent/ParaHyperlink'
import { ParaRun } from '../../core/Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../../core/Paragraph/Paragraph'
import { InstanceType } from '../../core/instanceTypes'
import { ParagraphContnetItem } from '../Core.gen'
import {
  toItemWithContent,
  toRunElement,
} from '../TextContent/ParagraphContentType.gen'

export type ItemWithContent = Paragraph | ParaHyperlink | ParaRun

export function getContentLength(itemWithContent: ItemWithContent): number {
  return itemWithContent.children.length
}

function isEmptyItemWithContent(itemWithContent: ItemWithContent) {
  if (itemWithContent == null) {
    return true
  }
  return itemWithContent.children.length < 1
}

export function getContentItemAt(
  itemWithContent: ItemWithContent,
  index: number
): Nullable<ParagraphContnetItem> {
  if (isEmptyItemWithContent(itemWithContent)) {
    return undefined
  }
  const item = itemWithContent.children.at(index)
  if (item == null) {
    return undefined
  }
  if (
    item.instanceType === InstanceType.ParaHyperlink ||
    item.instanceType === InstanceType.ParaRun
  ) {
    return isEmptyItemWithContent(item) ? undefined : toItemWithContent(item)
  } else {
    switch (item.instanceType) {
      case InstanceType.ParaNewLine:
      case InstanceType.ParaEnd:
      case InstanceType.ParaText:
      case InstanceType.ParaSpace:
      case InstanceType.ParaTab:
        return toRunElement(item)
    }
  }
  return undefined
}

export function arrayFromPos(pos: ParagraphElementPosition): number[] {
  const len = pos.positonIndices.length
  let lvl = pos.level
  lvl = lvl > len ? len : lvl
  lvl = lvl > 3 ? 3 : lvl
  lvl = lvl < 0 ? 0 : lvl
  return pos.positonIndices.slice(0, lvl)
}
