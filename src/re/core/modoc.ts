const gulIndexToKey = [
  'color',
  'background',
  'font-size',
  'width',
  'height',
  'rowspan',
  'colspan',
  'align',
  'bold',
  'italic',
  'underline',
  'vertical',
  'strike',
  'wrap',
  'fixedRowsTop',
  'fixedColumnsLeft',
  'link',
  'format',
  'formula',
  'filter',
  'border-top',
  'border-right',
  'border-bottom',
  'border-left',
  'line',
  'guid',
  'author',
  'size',
  'list',
  'layout',
  'margin',
  'font',
  'header',
  'indent',
  'comment',
  'comment-block',
  'code-block',
  'name',
  'order',
  'autoFit',
  'userID',
  'nextIndex',
  'id',
  'colVisible',
  'rowVisible',
  'autoFormatter',
  'copyIndex',
  'manualAlign',
  'lock',
  'left',
  'top',
  'cellValue',
] as const

export function getAttributeKeyByGul(gul: string) {
  const code = gul.charCodeAt(0)
  let key: typeof gulIndexToKey[number]
  if (code >= 48 && code <= 57) {
    key = gulIndexToKey[code - 48]
  } else if (code >= 65 && code <= 90) {
    key = gulIndexToKey[code - 29]
  } else if (code >= 97 && code <= 122) {
    key = gulIndexToKey[code - 87]
  } else {
    throw new Error('Unknown gul "' + gul + '"')
  }
  return key
}
