open RescriptCore
open StringParser

module StrSlice = Slice.StringSlice

type slice = StrSlice.t
@tag("type")
type data =
  | @as("delta") Delta({data: slice})
  | @as("json") Object({data: slice})
  | @as("map") Slides({data: slice})
  | @as("number") Number({data: int})
  | @as("string") String({data: slice})
@unboxed type attr = Attr(ModocAttr.attributes)

@tag("action")
type action =
  | @as("insert") Insert({data: data, attr: attr})
  | @as("retain") Retain({data: data, attr: attr})
  | @as("remove") Remove({data: data, attr: attr})

open ModocUtils
let opMark = "!@#"

let p_op = C.takeUtil(opMark)->C.map(((n_str, mark)) =>
  switch n_str->Int.fromString(~radix=36) {
  | Some(len) => Some(String.length(n_str), len, mark)
  | _ => None
  }
)

let p_delta = C.takeUtil(":")->C.map(((n_str, _)) =>
  switch n_str->Int.fromString(~radix=36) {
  | Some(len) => Some(String.length(n_str), len)
  | _ => None
  }
)

let dataInitial = "DSO"->one_of_string

let dataMarks = C.either(
  C.product(C.item("@"), dataInitial),
  C.product("!#"->one_of_string, C.return("")),
)

type data_parse_res =
  | Obj({len: int, initial: string})
  | Str(int)
  | Num(int)

let p_data = C.product(dit36, dataMarks)->C.map(res =>
  switch res {
  | (Some(offset, len), ("@", dataInitial)) => Some(offset + 1, Obj({len, initial: dataInitial}))
  // length of string  doesn't include mark "!"
  | (Some(offset, len), ("!", _)) => Some(offset, Str(len + 1))
  | (Some(offset, v), ("#", _)) => Some(offset, Num(v))
  | _ => None
  }
)

let p_slideId = C.take(8)
let p_lenOfSlide = C.takeUtil("#")->C.map(((n_str, _)) =>
  switch n_str->Int.fromString(~radix=36) {
  | Some(len) => Some(String.length(n_str) + 1, len)
  | _ => None
  }
)

let p_slides = C.product(p_slideId, p_lenOfSlide)->C.map(((sid, len)) =>
  switch len {
  | Some((offset, v)) => (sid, offset + 8, v)->Some
  | _ => None
  }
)

let ep_slides = p_slides

let parseData = (s, pool) =>
  switch s->(eval(p_data, _)) {
  | Some(res, _) =>
    switch res {
    | Some(offset, Obj({len, initial})) =>
      let data = StrSlice.slice(s, ~start=offset + 1, ~end=offset + len)
      let rest = StrSlice.sliceToEnd(s, ~start=offset + len)
      let mdata = switch initial {
      | "D" => Delta({data: data})
      | "S" => Slides({data: data})
      | "O" => Object({data: data})
      | i =>
        failwith(
          `[parseData] invalid object initial ${i} in
      "${StrSlice.to_source(s)}"`,
        )
      }
      (mdata, Attr(ModocAttr.decodeAttrs(pool, rest)))
    | Some(offset, Str(len)) =>
      let data = StrSlice.slice(s, ~start=offset + 1, ~end=offset + len)
      let rest = StrSlice.sliceToEnd(s, ~start=offset + len)
      (String({data: data}), Attr(ModocAttr.decodeAttrs(pool, rest)))
    | Some(offset, Num(v)) =>
      let rest = StrSlice.sliceToEnd(s, ~start=offset + 1)
      (Number({data: v}), Attr(ModocAttr.decodeAttrs(pool, rest)))
    | None => failwith(`[parseData] parse failed for "${StrSlice.to_source(s)}"`)
    }

  | _ => failwith(`[parseData] parse failed for "${StrSlice.to_source(s)}"`)
  }

let parseAction = (s, pool) =>
  switch s->(eval(p_op, _)) {
  | Some(Some(dit_len, len, mark), _) =>
    s
    ->StrSlice.slice(_, ~start=dit_len + 1, ~end=len + dit_len)
    ->parseData(pool)
    ->(
      ((mdata, attr)) => {
        let rest_s = StrSlice.sliceToEnd(s, ~start=len + dit_len)
        let action = switch mark {
        | "!" => Insert({data: mdata, attr})
        | "@" => Retain({data: mdata, attr})
        | "#" => Remove({data: mdata, attr})
        | i =>
          failwith(
            `[parseOp] invalid action mark "${i}" in
          "${StrSlice.to_source(s)}"`,
          )
        }
        (action, rest_s)
      }
    )

  | _ => failwith(`[parseAction]parse failed for ${s->(StrSlice.to_source(_))}`)
  }

module OpIter = {
  type t = {
    mutable slice: slice,
    pool: array<string>,
  }

  let make = (s, pool) => {slice: s, pool}

  let hasNext = t => StrSlice.length(t.slice) > 0

  let next = t => {
    let (action, rest) = parseAction(t.slice, t.pool)
    t.slice = rest
    action
  }
}

let getOpIter = s =>
  switch s->(eval(p_delta, _)) {
  | Some(Some(dit_len, len), _) =>
    let ds = s->(StrSlice.slice(_, ~start=dit_len + 1, ~end=len + dit_len + 1))
    let pool = s->StrSlice.sliceToEnd(_, ~start=len + dit_len + 1)->ModocAttr.decodePool
    OpIter.make(ds, pool)
  | _ => failwith(`invalid delta string => ${StrSlice.to_source(s)}`)
  }

let parseSlides = s =>
  switch s->(eval(ep_slides, _)) {
  | Some(Some(sid, offset, len), _) =>
    let ds = s->(StrSlice.slice(_, ~start=offset, ~end=offset + len))
    let rest = s->(StrSlice.sliceToEnd(_, ~start=offset + len))
    (sid, ds, rest)
  | _ => failwith(`[parseSlides] parse slides data failed for "${s->(StrSlice.to_source(_))}"`)
  }

module MapIter = {
  type t = {mutable slice: slice}

  type item = {id: string, delta: slice}

  let make = s => {slice: s}

  let hasNext = t => StrSlice.length(t.slice) > 0

  let next = t => {
    let (sid, ds, rest) = parseSlides(t.slice)
    t.slice = rest
    {
      id: sid,
      delta: ds,
    }
  }
}

let getMapIter = s => MapIter.make(s)

let stringToSlice = s => StrSlice.makeToEnd(s, ~start=0)
let sliceToString = s => StrSlice.to_source(s)
