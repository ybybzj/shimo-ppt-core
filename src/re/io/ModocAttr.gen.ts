/* TypeScript file generated from ModocAttr.resi by genType. */

/* eslint-disable */
/* tslint:disable */

import type {AttributeValue as $$value} from '../../io/operation';

export type value = $$value;

export type attributes = {[id: string]: value};
