open RescriptCore
type attr = ModocAttr.attributes

type action = [#insert | #remove | #retain]

type operationDataType = [
  | #string
  | #number
  | #delta
  | #json
  | #map
]

type operationData

@module("../../io/operation")
external getOperationDataType: operationData => operationDataType = "getOperationDataType"

type operation = {
  action: action,
  data: operationData,
  attributes: option<attr>,
}

type delta = array<operation>

let delta_isEmpty = d => d->Array.length <= 0

type deltaOp = {delta: delta}

type jsonOp = {object: JSON.t}

type mapOp = {slide: Dict.t<delta>}

external _asString: 'a => string = "%identity"
external _asNumber: 'a => int = "%identity"
external _asDeltaOp: 'a => deltaOp = "%identity"
external _asJsonOp: 'a => jsonOp = "%identity"
external _asMapOp: 'a => mapOp = "%identity"

external deltaOpToOp: deltaOp => operationData = "%identity"
external stringToOp: string => operationData = "%identity"
external numberToOp: int => operationData = "%identity"
external jsonOpToOp: jsonOp => operationData = "%identity"
external mapOpToOp: mapOp => operationData = "%identity"

type classifiedOp = String(string) | Number(int) | Delta(deltaOp) | Json(jsonOp) | Map(mapOp)

let classify = opData => {
  switch opData->getOperationDataType {
  | #string => String(_asString(opData))
  | #number => Number(_asNumber(opData))
  | #delta => Delta(_asDeltaOp(opData))
  | #json => Json(_asJsonOp(opData))
  | #map => Map(_asMapOp(opData))
  }
}

let toOperationData = op =>
  switch op {
  | String(s) => stringToOp(s)
  | Number(n) => numberToOp(n)
  | Delta(d) => deltaOpToOp(d)
  | Json(j) => jsonOpToOp(j)
  | Map(m) => mapOpToOp(m)
  }
%%private(
  @inline
  let opt_do = (o, @inline f) =>
    switch o {
    | Some(v) => f(v)
    | None => ()
    }
  @inline
  let opt_map = (o, f) =>
    switch o {
    | Some(v) => Some(f(v))
    | None => None
    }

  @inline
  let opt_getWithDefault = (o, default) =>
    switch o {
    | Some(x) => x
    | None => default
    }
)

module ArrSeq = {
  type t<'a> = {
    mutable index: int,
    arr: array<'a>,
    mutable replaceItem: option<'a>,
  }
  let make = arr => {
    index: 0,
    arr,
    replaceItem: None,
  }

  let getIndex = t => t.index

  let length = t => t.arr->Array.length
  let hasNext = t => {
    t.index < length(t)
  }

  let peekNext = t => {
    switch t.replaceItem {
    | Some(_) => t.replaceItem
    | None => t.arr[t.index]
    }
  }

  let skipOne = t => {
    t.replaceItem = None
    let index = t.index
    t.index = index + 1
  }

  let next = t => {
    let ret = peekNext(t)
    skipOne(t)
    ret
  }

  let replaceNext = (t, a) => {
    if hasNext(t) {
      t.replaceItem = Some(a)
    }
  }

  let replaceInPlaceNext = (t, a) => {
    if hasNext(t) {
      Array.splice(t.arr, ~start=t.index, ~remove=1, ~insert=[a])
      t.replaceItem = None
    }
  }

  let removeNext = t => {
    if hasNext(t) {
      let item = t.arr->Array.getUnsafe(t.index)
      Array.splice(t.arr, ~start=t.index, ~remove=1, ~insert=[])
      Some(item)
    } else {
      None
    }
  }

  let appendTo = (t, arr) => {
    if hasNext(t) {
      Utils.Arr.appendArr(arr, ~start=t.index, t.arr)
    }
  }
}
let lengthOfOperation = op =>
  switch op.data->getOperationDataType {
  | #delta | #json | #map => 1
  | #string => _asString(op.data)->String.length
  | #number => _asNumber(op.data)
  }

// let op_isEmpty = op =>
//   switch (op.action, op.data->getOperationDataType) {
//   | (#retain, #number) if ModocAttr.isEmpty(op.attributes) => true
//   | _ => false
//   }

let op_merge = (
  {action: self_action, data: self_data, attributes: self_attr},
  {action, data, attributes: attr},
) => {
  @inline
  let merge_data = (self, data) => {
    switch (self->getOperationDataType, data->getOperationDataType) {
    | (#number, #number) => (_asNumber(self) + _asNumber(data))->numberToOp->Some
    | (#string, #string) => (_asString(self) ++ _asString(data))->stringToOp->Some
    | _ => None
    }
  }

  if !ModocAttr.isEqual(self_attr, attr) {
    None
  } else {
    switch (self_action, action) {
    | (#insert, #insert) =>
      merge_data(self_data, data)->opt_map(data => {
        action: #insert,
        data,
        attributes: self_attr,
      })
    | (#retain, #retain) =>
      merge_data(self_data, data)->opt_map(data => {
        action: #retain,
        data,
        attributes: self_attr,
      })
    | (#remove, #remove) =>
      merge_data(self_data, data)->opt_map(data => {
        action: #remove,
        data,
        attributes: self_attr,
      })
    | _ => None
    }
  }
}

@module("./ModocModel")
external cloneObj: option<attr> => attr = "cloneObj"
let op_expectLength = (op, ~len: int): (operation, option<operation> /* chopped op */) => {
  @inline
  let data_expectLength = (data, len) =>
    switch data->getOperationDataType {
    | #string =>
      let s = _asString(data)
      (
        s->String.substring(~start=0, ~end=len)->stringToOp,
        s->String.substringToEnd(~start=len)->stringToOp->Some,
      )
    | #number =>
      let n = _asNumber(data)
      (len->numberToOp, (n - len)->numberToOp->Some)
    | _ => (data, None)
    }

  @inline
  let updateData = d => {
    ...op,
    data: d,
  }
  let op_len = lengthOfOperation(op)
  if op_len <= len {
    (op, None)
  } else {
    let (data1, data2) = data_expectLength(op.data, len)

    (updateData(data1), data2->opt_map(updateData))
  }
}

module Delta = {
  type pushResult = Pushed | Merged | Reordered

  let delta_push = (delta: delta, ~end=?, op) => {
    let len = Array.length(delta)
    let lastIndex = switch end {
    | Some(end) if end < len => end
    | _ => len - 1
    }

    let rec _push = (delta, op, index, result) => {
      switch delta->Array.get(index) {
      | None =>
        Array.unshift(delta, op)
        result->opt_getWithDefault(Pushed)
      | Some(self) =>
        switch (self.action, op.action) {
        | (#remove, #insert) => _push(delta, op, index - 1, Some(Reordered))
        | _ =>
          switch op_merge(self, op) {
          | Some(newOp) =>
            delta->Array.set(index, newOp)
            Merged
          | None =>
            delta->Array.splice(~start=index + 1, ~remove=0, ~insert=[op])
            result->opt_getWithDefault(Pushed)
          }
        }
      }
    }
    _push(delta, op, lastIndex, None)
  }

  let delta_append = (self, deltaSeq) => {
    if !ArrSeq.hasNext(deltaSeq) {
      ()
    } else {
      let rec _append = (self, deltaSeq) => {
        switch deltaSeq->ArrSeq.next {
        | Some(op) =>
          switch delta_push(self, op) {
          | Reordered => _append(self, deltaSeq)
          | _ => ArrSeq.appendTo(deltaSeq, self)
          }
        | None => ()
        }
      }
      _append(self, deltaSeq)
    }
  }

  let delta_normalize = (delta, ~start) => {
    let len = delta->Array.length
    if start < 0 || start >= len || len <= 1 {
      ()
    } else {
      let rec _normalize = (delta, start) => {
        switch delta->Array.get(start) {
        | Some(op) =>
          Array.splice(delta, ~start, ~remove=1, ~insert=[])
          switch delta_push(delta, ~end=start - 1, op) {
          | Reordered => _normalize(delta, start + 1)
          | _ => ()
          }
        | None => ()
        }
      }
      _normalize(delta, start)
    }
  }
}
/*
 *  OT(Operational transformation) functions:
 *  - compose
 *  - transform
 *  - invert
 */

// compose
%%private(
  @module("./ModocModel")
  external mergeAttrs: (attr, option<attr>, bool) => attr = "mergeObj"
  @module("./ModocModel")
  external mergeJson: (JSON.t, JSON.t, bool) => JSON.t = "mergeObj"
)
let rec op_compose = (self, op, isDoc) => {
  let canCompose = switch (self.action, op.action) {
  | (#retain, #retain)
  | (#insert, #retain) => true
  | _ => false
  }
  if !canCompose {
    Error.panic("[compose operation]invalid actions")
  }

  let attr = mergeAttrs(self.attributes->cloneObj, op.attributes, !isDoc)

  let data = data_compose(self.data, op.data, isDoc)
  {
    action: self.action,
    data,
    attributes: Some(attr),
  }
}
and data_compose = (self, data, isDoc) => {
  switch (self->classify, data->classify) {
  | (Number(_), data) => data
  | (self, Number(_)) => self

  | (Json({object: self}), Json({object: j})) =>
    Json({
      object: mergeJson(self, j, !isDoc),
    })

  | (Delta({delta: self}), Delta({delta})) =>
    switch delta_compose(self, delta, Some(isDoc)) {
    | Some(delta) => Delta({delta: delta})
    | None => Number(1)
    }

  | (Map({slide: a}), Map({slide: b})) =>
    b
    ->Dict.keysToArray
    ->Array.forEach(keyB => {
      let valueB = b->Utils.Dict.unsafeGet(keyB)
      switch a->Dict.get(keyB) {
      | None => Dict.set(a, keyB, valueB)
      | Some(valueA) =>
        switch delta_compose(valueA, valueB, Some(isDoc)) {
        | Some(delta) => Dict.set(a, keyB, delta)
        | None => Dict.delete(a, keyB)
        }
      }
    })
    Map({slide: a})
  | _ => Error.panic("[compose data]invalid operation data to be composed")
  }->toOperationData
}

and delta_compose = (self, delta, isDoc: option<bool>) => {
  let selfSeq = ArrSeq.make(self)
  let deltaSeq = ArrSeq.make(delta)

  @inline
  let _pushToSelf = op =>
    switch Delta.delta_push(self, ~end=selfSeq->ArrSeq.getIndex - 1, op) {
    | Merged => ()
    | _ => selfSeq->ArrSeq.skipOne
    }

  @inline
  let _nextByLen = (peekedOp, len, seq, replaceInPlace) => {
    let (expectedOp, leftOp) = op_expectLength(peekedOp, ~len)
    switch leftOp {
    | Some(op) => replaceInPlace ? ArrSeq.replaceInPlaceNext(seq, op) : ArrSeq.replaceNext(seq, op)
    | None => replaceInPlace ? ArrSeq.removeNext(seq)->ignore : ArrSeq.skipOne(seq)
    }
    expectedOp
  }

  @inline
  let _opsWithSameLen = (selfOp, deltaOp, selfSeq, deltaSeq) => {
    switch (selfOp->lengthOfOperation, deltaOp->lengthOfOperation) {
    | (sl, l) if sl == l => (
        selfSeq->ArrSeq.removeNext->Option.getUnsafe,
        deltaSeq->ArrSeq.next->Option.getUnsafe,
      )
    | (sl, l) if sl > l => (
        _nextByLen(selfOp, l, selfSeq, true),
        deltaSeq->ArrSeq.next->Option.getUnsafe,
      )
    | (sl, _l) /* sl < l */ => (
        selfSeq->ArrSeq.removeNext->Option.getUnsafe,
        _nextByLen(deltaOp, sl, deltaSeq, false),
      )
    }
  }
  let rec _compose = (selfSeq, deltaSeq, isDoc) => {
    switch (selfSeq->ArrSeq.peekNext, deltaSeq->ArrSeq.peekNext) {
    | (None, None) => ()
    | (Some(_), None) => Delta.delta_normalize(self, ~start=selfSeq->ArrSeq.getIndex)
    | (None, Some(_)) => Delta.delta_append(self, deltaSeq)
    | (Some(selfOp), Some(op)) =>
      switch (selfOp.action, op.action) {
      | (_, #insert) => deltaSeq->ArrSeq.next
      | (#remove, _) => selfSeq->ArrSeq.removeNext
      | _ =>
        let (selfOp, op) = _opsWithSameLen(selfOp, op, selfSeq, deltaSeq)
        switch (selfOp.action, op.action) {
        | (_ /* insert/retain */, #retain) =>
          let _isDoc = isDoc->opt_getWithDefault(selfOp.action == #insert)
          op_compose(selfOp, op, _isDoc)->Some

        | (#retain, _ /* remove */) => op->Some
        | _ /* insert <- remove */ => None
        }
      }->opt_do(_pushToSelf)
      _compose(selfSeq, deltaSeq, isDoc)
    }
  }

  _compose(selfSeq, deltaSeq, isDoc)
  self->delta_isEmpty ? None : Some(self)
}

let compose = (target, delta) => {
  delta_compose(target, delta, None)->ignore
  target
}

// invert
%%private(
  @module("./ModocModel")
  external invertAttrs: (attr, option<attr>) => attr = "invertObj"
  @module("./ModocModel")
  external invertJson: (JSON.t, JSON.t) => JSON.t = "invertObj"
)

let empty_delta: delta = []

let rec op_invert = (self, op) => {
  let canCompose = switch (self.action, op.action) {
  | (#insert, #retain) => true
  | _ => false
  }
  if !canCompose {
    Error.panic("[invert operation]invalid actions")
  }

  let attr = invertAttrs(self.attributes->cloneObj, op.attributes)

  let data = data_invert(self.data, op.data)
  {
    action: #retain,
    data,
    attributes: Some(attr),
  }
}
and data_invert = (self, data) => {
  switch (self->classify, data->classify) {
  | (_, Number(_) as data) => data
  | (Number(_) as self, Json(_))
  | (Number(_) as self, Map(_))
  | (Number(_) as self, String(_)) => self
  | (Number(_), Delta({delta})) =>
    switch delta_invert(empty_delta, delta) {
    | Some(delta) => Delta({delta: delta})
    | None => Number(1)
    }

  | (Json({object: self}), Json({object: j})) =>
    Json({
      object: invertJson(self, j),
    })

  | (Delta({delta: self}), Delta({delta})) =>
    switch delta_invert(self, delta) {
    | Some(delta) => Delta({delta: delta})
    | None => Number(1)
    }

  | (Map({slide: a}), Map({slide: b})) =>
    let map = Dict.make()
    b
    ->Dict.keysToArray
    ->Array.forEach(keyB => {
      let valueB = b->Utils.Dict.unsafeGet(keyB)
      let valueA = a->Dict.get(keyB)->Option.getOr(empty_delta)
      switch delta_invert(valueA, valueB) {
      | Some(delta) => Dict.set(map, keyB, delta)
      | _ => ()
      }
    })
    Map({slide: map})
  | _ => Error.panic("[invert data]invalid operation data to be invert")
  }->toOperationData
}

and delta_invert = (self, delta) => {
  let selfSeq = ArrSeq.make(self)
  let deltaSeq = ArrSeq.make(delta)
  let opl: delta = []

  @inline
  let _pushToOpl = op => Delta.delta_push(opl, op)->ignore

  @inline
  let _nextByLen = (peekedOp, len, seq) => {
    let (expectedOp, leftOp) = op_expectLength(peekedOp, ~len)
    switch leftOp {
    | Some(op) => ArrSeq.replaceNext(seq, op)
    | None => ArrSeq.skipOne(seq)
    }
    expectedOp
  }

  @inline
  let _opsWithSameLen = (selfOp, deltaOp, selfSeq, deltaSeq) => {
    switch (selfOp->lengthOfOperation, deltaOp->lengthOfOperation) {
    | (sl, l) if sl == l => (
        selfSeq->ArrSeq.next->Option.getUnsafe,
        deltaSeq->ArrSeq.next->Option.getUnsafe,
      )
    | (sl, l) if sl > l => (_nextByLen(selfOp, l, selfSeq), deltaSeq->ArrSeq.next->Option.getUnsafe)
    | (sl, _l) /* sl < l */ => (
        selfSeq->ArrSeq.next->Option.getUnsafe,
        _nextByLen(deltaOp, sl, deltaSeq),
      )
    }
  }
  let rec _invert = (selfSeq, deltaSeq) => {
    switch (selfSeq->ArrSeq.peekNext, deltaSeq->ArrSeq.peekNext) {
    | (_, None) => ()
    | (_, Some(op)) if op.action == #insert =>
      {action: #remove, data: numberToOp(op->lengthOfOperation), attributes: None}->_pushToOpl
      deltaSeq->ArrSeq.skipOne
      _invert(selfSeq, deltaSeq)
    | (peekSelf, Some(op)) =>
      switch peekSelf {
      | None => Error.panic("[invert data]Delta should be smaller than document")
      | Some(selfOp) =>
        switch (selfOp.action, op.action) {
        | (#insert, _) =>
          let (_selfOp, _op) = _opsWithSameLen(selfOp, op, selfSeq, deltaSeq)
          switch (_selfOp.action, _op.action) {
          | (_, #remove) => _selfOp
          | (_, _ /* #insert|#retain */) => op_invert(_selfOp, _op)
          }
        | _ => Error.panic("[invert data]Only document can invert deltas")
        }->_pushToOpl
        _invert(selfSeq, deltaSeq)
      }
    }
  }

  _invert(selfSeq, deltaSeq)
  opl->delta_isEmpty ? None : Some(opl)
}

let invert = (target, delta) => {
  delta_invert(target, delta)->Option.getOr([])
}
