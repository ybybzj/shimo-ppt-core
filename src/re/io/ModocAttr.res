open RescriptCore
open StringParser
open ModocUtils
// open Utils
module StrSlice = Slice.StringSlice

@module("./ModocAttr")
external poolToArray: string => array<string> = "decodePool"
let decodePool = pool => {
  let poolStr = StrSlice.to_source(pool)
  poolStr->poolToArray
}

%%private(
  @inline
  let opt_map = (o, @inline f) =>
    switch o {
    | Some(x) => f(x)->Some
    | None => None
    }
  @inline
  let opt_flatMap = (opt, f) =>
    switch opt {
    | Some(x) => f(x)
    | None => None
    }
)

type value
external attr_s: string => value = "%identity"
external attr_f: float => value = "%identity"
external attr_b: bool => value = "%identity"
let attr_nil: value = Obj.magic(null)

type t = Attr(string, value) | String(string)
type attributes = Dict.t<value>

let isEmpty = attrs => Dict.keysToArray(attrs)->Array.length <= 0

@module("./ModocAttr") @val
external isEqual: (option<attributes>, option<attributes>) => bool = "isDictEqual"

let p_n = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"->one_of_string

type p_attr_result = Offset(int, int) | Letter(string)

let p_attr = C.either(
  C.either(
    U.right(
      C.item("$"),
      p_n->C.map(n => {
        let v = n->Int.fromString(~radix=36)->Option.getExn
        Offset(2, v)
      }),
    ),
    U.right(
      C.item("%"),
      C.product(p_n, p_n)->C.map(((n1, n2)) => {
        let v = (n1 ++ n2)->Int.fromString(~radix=36)->Option.getExn
        Offset(3, v)
      }),
    ),
  ),
  C.take(1)->C.map(l => Letter(l)),
)

let parseAttrStr = s => {
  @inline
  let vfrom = rest =>
    switch rest->String.get(0) {
    | Some(l) =>
      switch l {
      | "0" => attr_b(false)->Some
      | "1" => attr_b(true)->Some
      | ":" => attr_s(rest->String.substringToEnd(~start=1))->Some
      | _ => rest->String.substringToEnd(~start=1)->Float.fromString->opt_map(f => attr_f(f))
      }
    | None => attr_nil->Some
    }
  switch s->StrSlice.makeToEnd(_, ~start=0)->(eval(p_attr, _)) {
  | Some(res, _) =>
    switch res {
    | Offset(offset, attrLen) =>
      let attrKey = s->String.substring(~start=offset, ~end=offset + attrLen)
      let rest = s->String.substringToEnd(~start=offset + attrLen)
      rest->vfrom->opt_map(v => (attrKey, v))

    | Letter(c) =>
      let attrKey = Core.Modoc.getAttrKeyByChar(c)
      let rest = s->String.substringToEnd(~start=1)
      rest->vfrom->opt_map(v => (attrKey, v))
    }
  | None => None
  }
}

let keyValues = [
  Attr("bold", attr_b(true)),
  Attr(`font-size`, attr_f(12.)),
  Attr(`font-size`, attr_f(11.)),
  Attr(`font-size`, attr_f(10.)),
  Attr(`font-size`, attr_f(9.)),
  Attr(`font-size`, attr_f(8.)),
  Attr(`colspan`, attr_f(2.)),
  Attr(`colspan`, attr_f(3.)),
  Attr(`colspan`, attr_f(4.)),
  Attr(`rowspan`, attr_f(2.)),
  Attr(`rowspan`, attr_f(3.)),
  Attr(`rowspan`, attr_f(4.)),
  Attr(`color`, attr_s(`#ff0000`)),
  Attr(`color`, attr_s(`#ffffff`)),
  Attr(`vertical`, attr_s(`bottom`)),
  Attr(`format`, attr_s(`normal`)),
  Attr(`background`, attr_nil),
  Attr(`background`, attr_s(`#ffffff`)),
  Attr(`background`, attr_s(`#ff0000`)),
  Attr(`background`, attr_s(`#ffff00`)),
  Attr(`background`, attr_s(`#00ffff`)),
  Attr(`align`, attr_s(`left`)),
  Attr(`align`, attr_s(`center`)),
  Attr(`align`, attr_s(`right`)),
  Attr(`background`, attr_f(4.)),
  Attr(`wrap`, attr_s(`text-wrap`)),
  Attr(`wrap`, attr_s(`text-no-wrap`)),
  Attr(`format`, attr_s(`YYYY/MM/DD`)),
  Attr(`border-right`, attr_s(`["",1]`)),
  Attr(`border-left`, attr_s(`["",1]`)),
  Attr(`border-top`, attr_s(`["",1]`)),
  Attr(`border-bottom`, attr_s(`["",1]`)),
  Attr(`border-right`, attr_s(`["#2b2b2b",1]`)),
  Attr(`border-left`, attr_s(`["#2b2b2b",1]`)),
  Attr(`border-top`, attr_s(`["#2b2b2b",1]`)),
  Attr(`border-bottom`, attr_s(`["#2b2b2b",1]`)),
]

@inline
let opt_do = (o, @inline f) =>
  switch o {
  | Some(v) => f(v)
  | None => ignore()
  }

%%private(
  @module("./ModocAttr")
  external getValueFromCache: string => option<t> = "getValuesFromCache"
  @module("./ModocAttr")
  external setValueToCache: (string, t) => unit = "addValueToCache"
)

let decodeAttrs = (pool, attrIdxStr) => {
  let indices =
    attrIdxStr->StrSlice.sliceToEnd(_, ~start=0)->StrSlice.to_source(_)->String.split(" ")

  let arr = Js.Dict.empty()
  let i = ref(0)

  @inline
  let addToDict = (o, t) => {
    switch t {
    | Attr(key, v) => Js.Dict.set(o, key, v)
    | String(s) =>
      let key = `$${i.contents->Int.toString}`
      i := i.contents + 1
      Js.Dict.set(o, key, attr_s(s))
    }
  }

  @inline
  let _getValue = attrStr => {
    switch attrStr->getValueFromCache {
    | Some(v) => v
    | None =>
      let t = switch attrStr->parseAttrStr {
      | Some(key, v) => Attr(key, v)
      | None => String(attrStr)
      }
      setValueToCache(attrStr, t)
      t
    }
  }

  @inline
  let reduceAttrs = (s, pool, arr) => {
    switch s->Int.fromString(~radix=10)->opt_flatMap(v => pool[v]) {
    | Some(attrStr) =>
      let t = _getValue(attrStr)
      addToDict(arr, t)
    | None =>
      for i in 0 to s->String.length - 1 {
        let idx = s->String.charCodeAt(i)->Float.toInt - 33
        keyValues[idx]->opt_do(t => arr->addToDict(t))
      }
    }
  }

  for i in 0 to indices->Array.length - 1 {
    reduceAttrs(indices[i]->Option.getUnsafe, pool, arr)
  }

  arr
}
