/* TypeScript file generated from Modoc.resi by genType. */

/* eslint-disable */
/* tslint:disable */

import * as ModocJS from './Modoc.res.js';

import type {attributes as ModocAttr_attributes} from './ModocAttr.gen';

export abstract class slice { protected opaque!: any }; /* simulate opaque types */

export type data = 
    { type: "delta"; readonly data: slice }
  | { type: "json"; readonly data: slice }
  | { type: "map"; readonly data: slice }
  | { type: "number"; readonly data: number }
  | { type: "string"; readonly data: slice };
export type ModocData = data;

export type attr = ModocAttr_attributes;
export type ModocAttr = attr;

export type action = 
    { action: "insert"; readonly data: data; readonly attr: attr }
  | { action: "retain"; readonly data: data; readonly attr: attr }
  | { action: "remove"; readonly data: data; readonly attr: attr };
export type ModocAction = action;

export abstract class OpIter_t { protected opaque!: any }; /* simulate opaque types */

export abstract class MapIter_t { protected opaque!: any }; /* simulate opaque types */

export type MapIter_item = { readonly id: string; readonly delta: slice };

export const stringToSlice: (_1:string) => slice = ModocJS.stringToSlice as any;

export const sliceToString: (_1:slice) => string = ModocJS.sliceToString as any;

export const OpIter_hasNext: (_1:OpIter_t) => boolean = ModocJS.OpIter.hasNext as any;

export const OpIter_next: (_1:OpIter_t) => action = ModocJS.OpIter.next as any;

export const getOpIter: (_1:slice) => OpIter_t = ModocJS.getOpIter as any;

export const MapIter_hasNext: (_1:MapIter_t) => boolean = ModocJS.MapIter.hasNext as any;

export const MapIter_next: (_1:MapIter_t) => MapIter_item = ModocJS.MapIter.next as any;

export const getMapIter: (_1:slice) => MapIter_t = ModocJS.getMapIter as any;

export const MapIter: { next: (_1:MapIter_t) => MapIter_item; hasNext: (_1:MapIter_t) => boolean } = ModocJS.MapIter as any;

export const OpIter: { next: (_1:OpIter_t) => action; hasNext: (_1:OpIter_t) => boolean } = ModocJS.OpIter as any;
