open RescriptCore
open StringParser

let one_of_string = s => s->C.one_of

let to_int = (s, ~radix) =>
  switch s->Int.fromString(~radix) {
  | Some(v) => (String.length(s), v)->Some
  | None => None
  }

let dit36 =
  "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ"
  ->C.takeWhen
  ->C.map(l => l->to_int(~radix=36))

let dit10 = "0123456789"->C.takeWhen->C.map(l => l->to_int(~radix=10))
