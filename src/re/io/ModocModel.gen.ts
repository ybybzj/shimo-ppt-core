/* TypeScript file generated from ModocModel.resi by genType. */

/* eslint-disable */
/* tslint:disable */

import * as ModocModelJS from './ModocModel.res.js';

import type {OperationAction as $$action} from '../../io/operation';

import type {OperationDataType as $$operationDataType} from '../../io/operation';

import type {OperationData as $$operationData} from '../../io/operation';

import type {Operation as $$operation} from '../../io/operation';

export type action = $$action;

export type operationDataType = $$operationDataType;

export type operationData = $$operationData;

export type operation = $$operation;

export type delta = operation[];

export const lengthOfOperation: (_1:operation) => number = ModocModelJS.lengthOfOperation as any;

export const compose: (_1:delta, _2:delta) => delta = ModocModelJS.compose as any;

export const invert: (_1:delta, _2:delta) => delta = ModocModelJS.invert as any;

export const delta_isEmpty: (_1:delta) => boolean = ModocModelJS.delta_isEmpty as any;
