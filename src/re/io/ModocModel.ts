export function cloneObj<T extends object>(o: T): T {
  return o == null ? ({} as T) : { ...o }
}
export function mergeObj(
  a: object = {},
  b: object = {},
  keepNull: boolean
): object {
  const akeys = Object.keys(a)
  if (keepNull && akeys.length === 0) {
    return b
  }
  const bkeys = Object.keys(b)
  for (let _i = 0, bkeys_1 = bkeys; _i < bkeys_1.length; _i++) {
    const bkey = bkeys_1[_i]
    a[bkey] = b[bkey]
    if (!keepNull && a[bkey] === null) {
      delete a[bkey]
    }
  }
  return a
}

export function transformObj<T extends object>(
  a: T,
  b: T,
  priority: boolean
): T {
  if (priority) {
    return b
  }
  const bkeys = Object.keys(b)
  if (bkeys.length === 0) {
    return b
  }
  const akeys = Object.keys(a)
  if (akeys.length === 0) {
    return b
  }
  if (akeys.length > bkeys.length) {
    for (let _i = 0, bkeys_2 = bkeys; _i < bkeys_2.length; _i++) {
      const bkey = bkeys_2[_i]
      if (a.hasOwnProperty(bkey)) {
        delete b[bkey]
      }
    }
  } else {
    for (let _a = 0, akeys_1 = akeys; _a < akeys_1.length; _a++) {
      const akey = akeys_1[_a]
      delete b[akey]
    }
  }
  return b
}

export function invertObj<T extends object>(a: T, b: T): T {
  b = cloneObj(b)
  const bkeys = Object.keys(b)
  if (bkeys.length === 0) {
    return b
  }
  const akeys = Object.keys(a)
  if (akeys.length === 0) {
    for (let _i = 0, bkeys_3 = bkeys; _i < bkeys_3.length; _i++) {
      const bkey = bkeys_3[_i]
      b[bkey] = null
    }
  } else {
    for (let _a = 0, bkeys_4 = bkeys; _a < bkeys_4.length; _a++) {
      const bkey = bkeys_4[_a]
      const avalue = a[bkey]
      if (avalue === undefined) {
        b[bkey] = null
      } else {
        b[bkey] = avalue
      }
    }
  }
  return b
}
