const decodedPool = new Map<string, string[]>()

abstract class ModocAttrValue {
  protected opaque!: any
}

const parsedValuesMap = new Map<string, ModocAttrValue>()

export function getValuesFromCache(s: string): undefined | ModocAttrValue {
  return parsedValuesMap.get(s)
}

export function addValueToCache(s: string, v: ModocAttrValue) {
  parsedValuesMap.set(s, v)
}

export function decodePool(pool: string): string[] {
  if (decodedPool.has(pool)) {
    return decodedPool.get(pool)!
  } else {
    let res: string[]
    try {
      res = JSON.parse(pool)
    } catch (_) {
      res = []
    }
    decodedPool.set(pool, res)

    return res
  }
}

export function isDictEqual(a: object, b: object) {
  if (a === b || (a == null && b == null)) {
    return true
  }
  if (a == null || b == null) return false

  const keys = Object.keys(a)
  const length = keys.length
  if (length !== Object.keys(b).length) {
    return false
  }
  for (let i = 0; i < length; i++) {
    if (a[keys[i]] !== b[keys[i]]) {
      return false
    }
  }
  return true
}

export function resetAttrParseCache() {
  decodedPool.clear()
  parsedValuesMap.clear()
}
