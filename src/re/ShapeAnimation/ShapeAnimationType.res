module AnimationUpdateInfo = {
  type t = {
    x: float,
    y: float,
    rot: float, //degree
    w: float, //scale
    h: float,
    alpha: float,
    visibility: bool,
  }
  let initValue = {
    x: 0.0,
    y: 0.0,
    rot: 0.0, //degree
    w: 1.0, //scale
    h: 1.0,
    alpha: 1.0,
    visibility: true,
  }
  type transformInfo = {
    transform: Core.matrix,
    alpha: float,
    visibility: bool,
  }

  @module("../core/animation")
  external toTransformInfo: (t, Core.slideElement) => transformInfo = "toTransformInfo"
}

module AnimationUpdateResult = {
  type t = NoAnimation | NotStart | Update(AnimationUpdateInfo.t)
  @unboxed
  type updateInfo =
    | NoDraw
    | NoUpdate
    | UpdateDraw(AnimationUpdateInfo.transformInfo)
  let noAnimation = () => NoAnimation
  let notStart = () => NotStart
  let update = info => Update(info)

  let toUpdateInfo = (t, sp) => {
    let info = switch t {
    | NotStart /* is impossible */ => NoDraw
    | NoAnimation => NoUpdate
    | Update(info) => UpdateDraw(AnimationUpdateInfo.toTransformInfo(info, sp))
    }
    info
  }
}

// (sp, percentage) => result
type animationUpdater = (
  Core.slideElement,
  float,
  option<AnimationUpdateInfo.t>,
) => AnimationUpdateResult.t

// (updater, percentage) => unit
type animationDrawer = (animationUpdater, float) => unit

type animationItem = {
  updater: animationUpdater,
  duration: float,
  getInitUpdate: (Core.slideElement, option<AnimationUpdateInfo.t>) => AnimationUpdateResult.t,
}

type animQueue = array<animationItem>

module SpCache = Utils.MakeHashMap({
  type t = Core.slideElement
  let hash = Core.hashSlideElement
  let eq = (t1, t2) => hash(t1) === hash(t2)
})
