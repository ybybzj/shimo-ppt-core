open RescriptCore

%%private(
  @inline
  let opt_do = (o, @inline f) =>
    switch o {
    | Some(v) => f(v)
    | None => ignore()
    }

  @inline
  let opt_flatMap = (opt, f) =>
    switch opt {
    | Some(x) => f(x)
    | None => None
    }

  @inline
  let opt_getWithDefault = (o, default) =>
    switch o {
    | Some(x) => x
    | None => default
    }
)
open Utils

type cursor = {
  mutable index: int,
  mutable percentage: float,
}

let encodeCursor = cursor => {
  let percentageChar = switch cursor.percentage {
  | n if n <= 0.0 => "b"
  | n if n >= 1.0 => "e"
  | _ => "m"
  }
  `${cursor.index->Int.toString}@${percentageChar}`
}

let decodeCursor = input => {
  let parts = input->String.trim->String.split("@")

  switch parts {
  | [indexPart, percentagePart] =>
    let index = indexPart->Int.fromString
    let percentage = switch percentagePart {
    | "b" => Some(0.0)
    | "m" => Some(0.5)
    | "e" => Some(1.0)
    | _ => None
    }
    switch (index, percentage) {
    | (Some(index), Some(percentage)) => Some({index, percentage})
    | _ => None
    }
  | _ => None
  }
}

module FrameUpdatesCache = {
  module IntMap = Utils.MutableIntMap
  type infoMap = IntMap.t<ShapeAnimationType.AnimationUpdateInfo.t>
  type t = ShapeAnimationType.SpCache.t<infoMap>
  let makeInfoMap = IntMap.make
  let make = ShapeAnimationType.SpCache.make
  let clear = t => {
    t->(ShapeAnimationType.SpCache.each(_, infoMap => infoMap->IntMap.clear))
    ShapeAnimationType.SpCache.clear(t)
  }

  let get = (t, ~sp, ~index) => {
    t->ShapeAnimationType.SpCache.get(_, sp)->opt_flatMap(infoMap => infoMap->IntMap.get(index))
  }

  let update = (t, ~sp, ~index, ~info) => {
    let map = ShapeAnimationType.SpCache.get(t, sp)->opt_getWithDefault(makeInfoMap())
    map->IntMap.set(index, info)
    t->(ShapeAnimationType.SpCache.update(_, sp, map))
  }
}

type stopPlayStatus = StopAtBeginning | StopAtEnd
type onEndHandler = stopPlayStatus => unit

type t = {
  queue: ShapeAnimationType.animQueue,
  cursor: cursor,
  mutable currentAnim: option<Animation.t>,
  onEnd: option<onEndHandler>,
  frameCache: FrameUpdatesCache.t,
  autoPlay: unit => bool,
  mutable stopAutoPlay: bool,
}

let isPlaying = player => player.currentAnim->Option.isSome

let isAutoPlay = player => player.autoPlay() && player.stopAutoPlay == false
// let printPlayerState = (player, msg) => {
//   // Js.log(msg)
//   // Js.log(player)
// }

let stopAutoPlay = (player, autoPlay) => player.stopAutoPlay = autoPlay

let getCursor = player => player.cursor

let updateCursor = (player, ~index, ~percentage) => {
  let len = Array.length(player.queue)
  player.cursor.index = index < 0 ? 0 : index > len - 1 ? len - 1 : index
  player.cursor.percentage = percentage < 0.0 ? 0.0 : percentage > 1.0 ? 1.0 : percentage
}

let resetCursor = player => updateCursor(player, ~index=0, ~percentage=0.0)

let resetCursorAtEnd = player => {
  let len = Array.length(player.queue)
  updateCursor(player, ~index=len - 1, ~percentage=1.0)
}

let setCursor = (player, cursor) =>
  decodeCursor(cursor)->opt_do(({index, percentage}) => {
    updateCursor(player, ~index, ~percentage)
  })

let makePlayer = (queue: ShapeAnimationType.animQueue, ~onEnd, ~autoPlay) => {
  let cursor = {
    index: 0,
    percentage: 0.0,
  }

  let frameCache = FrameUpdatesCache.make()

  {
    cursor,
    queue,
    currentAnim: None,
    onEnd,
    frameCache,
    autoPlay,
    stopAutoPlay: false,
  }
}

let onEndCallback = (player, stopStatus) => {
  // printPlayerState(player, "onEndCallback")
  // player->updateCursor(~index=0, ~percentage=0.0)
  player.currentAnim = None
  // FrameUpdatesCache.clear(. player.frameCache)

  player.onEnd->opt_do(onEnd => onEnd(stopStatus))
}

let rec getPrevUpdateInfoByIndex = (player, ~sp, ~index, ~prevUpdateInfo) => {
  open ShapeAnimationType.AnimationUpdateResult
  let curItem = player.queue->Array.get(index)

  let toResult = info =>
    switch info {
    | None => NoAnimation
    | Some(info) => Update(info)
    }

  switch curItem {
  | None => toResult(prevUpdateInfo)
  | Some(curItem) =>
    switch FrameUpdatesCache.get(player.frameCache, ~sp, ~index) {
    | Some(info) => Update(info)
    | None =>
      let _prevUpdateInfo = switch index {
      | n if n <= 0 => prevUpdateInfo
      | _ =>
        switch getPrevUpdateInfoByIndex(player, ~sp, ~index=index - 1, ~prevUpdateInfo) {
        | Update(info) => Some(info)
        | _ => prevUpdateInfo
        }
      }
      switch curItem.updater(sp, 1.0, _prevUpdateInfo) {
      | Update(info) =>
        FrameUpdatesCache.update(player.frameCache, ~sp, ~index, ~info)
        Update(info)
      | _ => toResult(_prevUpdateInfo)
      }
    }
  }
}

let getUpdaterByIndex = (player, index) => (sp, percentage, prevUpdateInfo) => {
  open ShapeAnimationType.AnimationUpdateResult
  let curItem = player.queue->Array.get(index)

  switch curItem {
  | None => NoAnimation
  | Some(curItem) =>
    let prevUpdateInfoResult = getPrevUpdateInfoByIndex(
      player,
      ~sp,
      ~index=index - 1,
      ~prevUpdateInfo,
    )

    let prevInfo = switch prevUpdateInfoResult {
    | Update(info) => Some(info)
    | _ => prevUpdateInfo
    }

    switch curItem.updater(sp, percentage, prevInfo) {
    | NoAnimation => NoAnimation
    | Update(info) => Update(info)
    | NotStart =>
      let mapper = (animItem: ShapeAnimationType.animationItem) =>
        switch animItem.getInitUpdate(sp, prevInfo) {
        | NoAnimation
        | NotStart =>
          None
        | Update(info) => Some(Update(info))
        }

      player.queue
      ->Arr.findMapU(~index, ~direction=Arr.Forward, ~map=mapper)
      ->opt_getWithDefault(prevUpdateInfoResult)
    }
  }
}

let stopPlayAnimation = player => {
  player.currentAnim->opt_do(anim => Animation.stopAnimation(anim))
  player.currentAnim = None
}

let isAtEnd = player => {
  player.cursor.index === Array.length(player.queue) - 1 && player.cursor.percentage >= 1.0
}

let isAtBeginnig = player => {
  player.cursor.index <= 0 && player.cursor.percentage <= 0.0
}

let drawFrame = (player, updater, drawer, percentage, ~isRecordFrame) => {
  let wrappedUpdater = (sp, percentage, prevUpdateInfo) => {
    let info = updater(sp, percentage, prevUpdateInfo)

    if isRecordFrame {
      switch info {
      | ShapeAnimationType.AnimationUpdateResult.Update(info) =>
        FrameUpdatesCache.update(player.frameCache, ~sp, ~index=player.cursor.index, ~info)
      | _ => ()
      }
    }
    info
  }

  drawer(wrappedUpdater, percentage)
  player->updateCursor(~index=player.cursor.index, ~percentage)
}

let afterStop = player => {
  // printPlayerState(player, "afterStop")
  player.currentAnim = None
}

let playFrame = (player, drawer) => {
  let updater = getUpdaterByIndex(player, player.cursor.index)
  player->drawFrame(
    updater,
    drawer,
    player.cursor.percentage,
    ~isRecordFrame=player.cursor.percentage >= 1.0,
  )
  player->afterStop
}

let rec next = (player, drawer) => {
  player->stopPlayAnimation
  if player->isAtEnd {
    onEndCallback(player, StopAtEnd)
  } else {
    // printPlayerState(player, "next")
    switch player.cursor {
    | {index, percentage} if percentage <= 0.0 =>
      player->updateCursor(~index, ~percentage=0.0)
      player->startPlayAnimation(drawer)

    | {index, percentage} if percentage > 0.0 && percentage < 1.0 =>
      player->updateCursor(~index, ~percentage=1.0)
      player->playFrame(drawer)
      if player->isAutoPlay {
        next(player, drawer)
      }
    | {index} =>
      player->updateCursor(~index=index + 1, ~percentage=0.0)
      player->startPlayAnimation(drawer)
    }
  }
}

and startPlayAnimation = (player, drawer) => {
  player.queue
  ->Array.get(player.cursor.index)
  ->opt_do(animItem => {
    let updater = getUpdaterByIndex(player, player.cursor.index)
    let anim = Animation.makeAnimationWithDuration(
      percentage => player->drawFrame(updater, drawer, percentage, ~isRecordFrame=false),
      ~endCallback=Some(
        () => {
          // in case that the animation is nerver played at 100% percentage
          player->updateCursor(~index=player.cursor.index, ~percentage=1.0)
          player->drawFrame(updater, drawer, player.cursor.percentage, ~isRecordFrame=true)

          afterStop(player)
          if player->isAutoPlay {
            next(player, drawer)
          }
        },
      ),
      ~duration=animItem.duration,
    )

    player.currentAnim = Some(anim)
    Animation.startAnimation(anim)
  })
}

let prev = (player, drawer) => {
  player->stopPlayAnimation

  if player->isAtBeginnig {
    onEndCallback(player, StopAtBeginning)
  } else {
    switch player.cursor {
    | {index, percentage} if percentage <= 0.0 =>
      player->updateCursor(~index=index - 1, ~percentage=0.0)
      player->playFrame(drawer)
    | {index} =>
      player->updateCursor(~index, ~percentage=0.0)
      player->playFrame(drawer)
    }
  }
}

let stop = (player, drawer) => {
  if isPlaying(player) {
    player->stopPlayAnimation
    player->updateCursor(~index=player.cursor.index, ~percentage=1.0)
    player->playFrame(drawer)
  }
}

let reset = player => {
  player->stopPlayAnimation
  player->updateCursor(~index=0, ~percentage=0.0)
  FrameUpdatesCache.clear(player.frameCache)
}

let drawStartFrame = (queue, drawer) => {
  let player = makePlayer(queue, ~onEnd=None, ~autoPlay=() => false)
  let updater = (sp, percentage, prevUpdateInfo) => {
    open ShapeAnimationType.AnimationUpdateResult
    let curItem = player.queue->Array.get(0)
    switch curItem {
    | None => NoAnimation
    | Some(curItem) =>
      switch curItem.updater(sp, percentage, prevUpdateInfo) {
      | NoAnimation => NoAnimation
      | Update(info) => Update(info)
      | NotStart =>
        let mapper = (animItem: ShapeAnimationType.animationItem) =>
          switch animItem.getInitUpdate(sp, prevUpdateInfo) {
          | NoAnimation => None
          | NotStart => None
          | Update(info) => Some(Update(info))
          }

        player.queue
        ->Arr.findMapU(~index=1, ~direction=Arr.Forward, ~map=mapper)
        ->opt_getWithDefault(NoAnimation)
      }
    }
  }
  player->drawFrame(updater, drawer, 0.0, ~isRecordFrame=false)
  reset(player)
}

let drawEndFrame = (queue, drawer) => {
  // open ShapeAnimationType.AnimationUpdateResult
  let player = makePlayer(queue, ~onEnd=None, ~autoPlay=() => false)

  let updater = getUpdaterByIndex(player, Array.length(player.queue) - 1)

  player->drawFrame(updater, drawer, 1.0, ~isRecordFrame=false)
  reset(player)
}

let drawFrame = (t, drawer) => playFrame(t, drawer)
