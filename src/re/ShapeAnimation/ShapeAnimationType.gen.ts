/* TypeScript file generated from ShapeAnimationType.resi by genType. */

/* eslint-disable */
/* tslint:disable */

import * as ShapeAnimationTypeJS from './ShapeAnimationType.res.js';

import type {matrix as Core_matrix} from '../../../src/re/Core.gen';

import type {slideElement as Core_slideElement} from '../../../src/re/Core.gen';

export type AnimationUpdateInfo_t = {
  readonly x: number; 
  readonly y: number; 
  readonly rot: number; 
  readonly w: number; 
  readonly h: number; 
  readonly alpha: number; 
  readonly visibility: boolean
};

export type AnimationUpdateInfo_transformInfo = {
  readonly transform: Core_matrix; 
  readonly alpha: number; 
  readonly visibility: boolean
};

export abstract class AnimationUpdateResult_t { protected opaque!: any }; /* simulate opaque types */

export type AnimationUpdateResult_updateInfo = 
    "NoDraw"
  | "NoUpdate"
  | AnimationUpdateInfo_transformInfo;

export type animationUpdater = (_1:Core_slideElement, _2:number, _3:(undefined | AnimationUpdateInfo_t)) => AnimationUpdateResult_t;

export type animationDrawer = (_1:animationUpdater, _2:number) => void;

export abstract class animationItem { protected opaque!: any }; /* simulate opaque types */

export type animQueue = animationItem[];

export const AnimationUpdateResult_noAnimation: () => AnimationUpdateResult_t = ShapeAnimationTypeJS.AnimationUpdateResult.noAnimation as any;

export const AnimationUpdateResult_notStart: () => AnimationUpdateResult_t = ShapeAnimationTypeJS.AnimationUpdateResult.notStart as any;

export const AnimationUpdateResult_update: (_1:AnimationUpdateInfo_t) => AnimationUpdateResult_t = ShapeAnimationTypeJS.AnimationUpdateResult.update as any;

export const AnimationUpdateResult_toUpdateInfo: (_1:AnimationUpdateResult_t, _2:Core_slideElement) => AnimationUpdateResult_updateInfo = ShapeAnimationTypeJS.AnimationUpdateResult.toUpdateInfo as any;

export const AnimationUpdateResult: {
  update: (_1:AnimationUpdateInfo_t) => AnimationUpdateResult_t; 
  toUpdateInfo: (_1:AnimationUpdateResult_t, _2:Core_slideElement) => AnimationUpdateResult_updateInfo; 
  notStart: () => AnimationUpdateResult_t; 
  noAnimation: () => AnimationUpdateResult_t
} = ShapeAnimationTypeJS.AnimationUpdateResult as any;
