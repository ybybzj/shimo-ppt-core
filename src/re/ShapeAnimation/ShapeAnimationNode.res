open RescriptCore

%%private(
  @inline
  let opt_do = (o, @inline f) =>
    switch o {
    | Some(v) => f(v)
    | None => ignore()
    }
  @inline
  let opt_mapWithDefault = (o, default, @inline f) =>
    switch o {
    | Some(x) => f(x)
    | None => default
    }
  @inline
  let opt_getWithDefault = (o, default) =>
    switch o {
    | Some(x) => x
    | None => default
    }
)

type nodeType = WithEffect | ClickEffect | AfterEffect

type nodeUpdater = (
  ~sp: Core.slideElement,
  ~percentage: float,
  ~prevUpdateInfo: ShapeAnimationType.AnimationUpdateInfo.t,
) => ShapeAnimationType.AnimationUpdateInfo.t

type node_t = {
  sp: Core.slideElement,
  nodeType: nodeType,
  duration: float,
  delay: float,
  category: int,
  updater: nodeUpdater,
}

let isClickNode = n =>
  switch n.nodeType {
  | WithEffect | AfterEffect => false
  | ClickEffect => true
  }

let isAfterEffectNode = n =>
  switch n.nodeType {
  | WithEffect | ClickEffect => false
  | AfterEffect => true
  }

module ShapeSet = Utils.MakeHashSet({
  type t = Core.slideElement
  let hash = Core.hashSlideElement
  let eq = (t1, t2) => hash(t1) === hash(t2)
})

let normalizePercentage = percentage => {
  switch percentage {
  | percentage if percentage < 0.0 => 0.0
  | percentage if percentage > 1.0 => 1.0
  | percentage => percentage
  }
}
/* ------------after effects---------------- */
type nodeGroup = {
  shapes: ShapeSet.t,
  duration: float,
  delay: float,
  updater: nodeUpdater,
}

let mergeGroup = (first, second) => {
  let resultShapes = ShapeSet.merge(first.shapes, second.shapes)
  let isInFirst = sp => first.shapes->(ShapeSet.has(_, sp))
  let isInSecond = sp => second.shapes->(ShapeSet.has(_, sp))
  let firstDelay = first.delay
  let secondDelay = second.delay

  let firstTotal = firstDelay +. first.duration
  let secondTotal = secondDelay +. second.duration
  let resultTotal = firstTotal +. secondTotal

  let firstUpdater = first.updater
  let secondUpdater = second.updater

  let resultUpdater = (~sp, ~percentage, ~prevUpdateInfo) => {
    let passedTime = resultTotal *. percentage

    switch passedTime {
    | passedTime if passedTime >= 0.0 && passedTime <= firstDelay =>
      if isInFirst(sp) {
        firstUpdater(~sp, ~percentage=0.0, ~prevUpdateInfo)
      } else {
        secondUpdater(~sp, ~percentage=0.0, ~prevUpdateInfo)
      }
    | passedTime if passedTime > firstDelay && passedTime <= firstTotal =>
      if isInFirst(sp) {
        let firstPercentage = (passedTime -. firstDelay) /. first.duration
        firstUpdater(~sp, ~percentage=firstPercentage, ~prevUpdateInfo)
      } else {
        secondUpdater(~sp, ~percentage=0.0, ~prevUpdateInfo)
      }

    | passedTime if passedTime > firstTotal && passedTime <= firstTotal +. secondDelay =>
      if isInSecond(sp) {
        let prevInfo = isInFirst(sp)
          ? firstUpdater(~sp, ~percentage=1.0, ~prevUpdateInfo)
          : prevUpdateInfo
        secondUpdater(~sp, ~percentage=0.0, ~prevUpdateInfo=prevInfo)
      } else {
        firstUpdater(~sp, ~percentage=1.0, ~prevUpdateInfo)
      }

    | passedTime if passedTime > firstTotal +. secondDelay && passedTime <= resultTotal =>
      if isInSecond(sp) {
        let prevInfo = isInFirst(sp)
          ? firstUpdater(~sp, ~percentage=1.0, ~prevUpdateInfo)
          : prevUpdateInfo
        let secondPercentage = (passedTime -. firstTotal -. secondDelay) /. second.duration

        secondUpdater(~sp, ~percentage=secondPercentage, ~prevUpdateInfo=prevInfo)
      } else {
        firstUpdater(~sp, ~percentage=1.0, ~prevUpdateInfo)
      }
    | _ =>
      if isInSecond(sp) {
        let prevInfo = isInFirst(sp)
          ? firstUpdater(~sp, ~percentage=1.0, ~prevUpdateInfo)
          : prevUpdateInfo
        secondUpdater(~sp, ~percentage=1.0, ~prevUpdateInfo=prevInfo)
      } else {
        firstUpdater(~sp, ~percentage=1.0, ~prevUpdateInfo)
      }
    }
  }

  {
    shapes: resultShapes,
    delay: 0.0,
    duration: resultTotal,
    updater: resultUpdater,
  }
}

/* ------with effects-------- */

module CacheByCategory: {
  type t
  let make: unit => t
  let add: (t, int, node_t) => unit
  let updateInfoFromCache: (
    t,
    Core.slideElement,
    float,
    ShapeAnimationType.AnimationUpdateInfo.t,
  ) => ShapeAnimationType.AnimationUpdateInfo.t
} = {
  type item = ShapeAnimationType.SpCache.t<array<node_t>>
  let makeItem = node => {
    let spCache = ShapeAnimationType.SpCache.make()
    ShapeAnimationType.SpCache.update(spCache, node.sp, [node])
    spCache
  }
  let updateItem = (spCache: item, node) => {
    let value =
      ShapeAnimationType.SpCache.get(spCache, node.sp)->opt_mapWithDefault([node], arr =>
        Utils.Arr.push(arr, node)
      )
    ShapeAnimationType.SpCache.update(spCache, node.sp, value)
    spCache
  }

  module IntMap = Utils.MutableIntMap
  type t = IntMap.t<item>
  let make = () => IntMap.make()
  let add = (t, key, node: node_t) => {
    t->IntMap.update(key, spCache => {
      spCache->opt_mapWithDefault(Some(makeItem(node)), spCache => Some(updateItem(spCache, node)))
    })
  }

  let rec updateInfoByNodes = (nodes: array<node_t>, sp, passedTime, initUpdateInfo) => {
    let updateInfo = ref(initUpdateInfo)
    let found: ref<option<(node_t, float)>> = ref(None)
    let node_total_time = ref(0.0)
    nodes->Utils.Arr.traverseUtil(~index=0, ~direction=Utils.Arr.Forward, ~util=node => {
      switch node.delay >= passedTime {
      | true =>
        switch found.contents {
        | None =>
          updateInfo := node.updater(~sp, ~percentage=0.0, ~prevUpdateInfo=updateInfo.contents)
        | Some((node, percentage)) =>
          let prevUpdateInfo = updateInfoByNodes(nodes, sp, node.delay, updateInfo.contents)
          updateInfo := node.updater(~sp, ~percentage, ~prevUpdateInfo)
        }
        found := None
        true

      | false =>
        let percentage = (passedTime -. node.delay) /. node.duration

        let totalTime = node.delay +. node.duration
        let needUpdateNodeTotalTime = node_total_time.contents <= totalTime
        if needUpdateNodeTotalTime {
          node_total_time := totalTime
        }
        switch percentage {
        | percentage if percentage >= 0.0 && percentage <= 1.0 => found := Some((node, percentage))
        | _ =>
          if needUpdateNodeTotalTime {
            found := Some((node, normalizePercentage(percentage)))
          }
        }
        false
      }
    })
    found.contents->opt_do(((node, percentage)) => {
      let prevUpdateInfo = updateInfoByNodes(nodes, sp, node.delay, updateInfo.contents)
      updateInfo := node.updater(~sp, ~percentage, ~prevUpdateInfo)
    })
    updateInfo.contents
  }

  let updateInfoByCategory = (item: item, sp, passedTime, initUpdateInfo) => {
    let nodes = ShapeAnimationType.SpCache.get(item, sp)
    switch nodes {
    | None => initUpdateInfo
    | Some(nodes) => updateInfoByNodes(nodes, sp, passedTime, initUpdateInfo)
    }
  }

  let updateInfoFromCache = (
    t: t,
    sp: Core.slideElement,
    passedTime: float,
    initUpdateInfo: ShapeAnimationType.AnimationUpdateInfo.t,
  ) => {
    t->IntMap.reduce(initUpdateInfo, (resultInfo, _key, spCache) => {
      updateInfoByCategory(spCache, sp, passedTime, resultInfo)
    })
  }
}

let mergeNodes = (first: node_t, otherNodes: array<node_t>) => {
  if Array.length(otherNodes) <= 0 {
    let shapes = ShapeSet.make()
    shapes->(ShapeSet.add(_, first.sp))

    {
      shapes,
      duration: first.duration,
      delay: first.delay,
      updater: first.updater,
    }
  } else {
    let nodes = otherNodes->Utils.Arr.unshift(first)
    nodes->Array.sort((n1, n2) =>
      n1.delay < n2.delay
        ? Ordering.less
        : n1.delay === n2.delay
        ? Ordering.equal
        : Ordering.greater
    )

    let shapes = ShapeSet.make()
    let cacheByCategory = CacheByCategory.make()
    let totalTime = ref(0.0)
    nodes->Array.forEach(node => {
      shapes->(ShapeSet.add(_, node.sp))
      cacheByCategory->CacheByCategory.add(node.category, node)
      let timeSpan = node.delay +. node.duration
      if timeSpan > totalTime.contents {
        totalTime := timeSpan
      }
    })
    let resultDelay = (nodes->Array.getUnsafe(0)).delay

    let resultDuration = totalTime.contents -. resultDelay

    let resultUpdater = (~sp, ~percentage, ~prevUpdateInfo) => {
      let passedTime = resultDuration *. percentage +. resultDelay
      cacheByCategory->CacheByCategory.updateInfoFromCache(sp, passedTime, prevUpdateInfo)
    }
    {
      shapes,
      duration: totalTime.contents -. resultDelay,
      delay: resultDelay,
      updater: resultUpdater,
    }
  }
}

/* ------------generate ShapeAnimationType.animQueue--------------- */

let animationItemFromNodeGroup = (
  nodeGroup,
  animatedShapes: ShapeSet.t,
): ShapeAnimationType.animationItem => {
  let totalTimeSpan = nodeGroup.duration +. nodeGroup.delay
  let animUpdater = (sp, percentage, prevUpdateInfo) => {
    open ShapeAnimationType
    let passedTime = totalTimeSpan *. percentage
    if ShapeSet.has(animatedShapes, sp) {
      if ShapeSet.has(nodeGroup.shapes, sp) && passedTime >= nodeGroup.delay {
        let info = nodeGroup.updater(
          ~sp,
          ~percentage=(passedTime -. nodeGroup.delay) /. nodeGroup.duration,
          ~prevUpdateInfo=prevUpdateInfo->opt_getWithDefault(
            ShapeAnimationType.AnimationUpdateInfo.initValue,
          ),
        )
        AnimationUpdateResult.Update(info)
      } else {
        AnimationUpdateResult.NotStart
      }
    } else {
      AnimationUpdateResult.NoAnimation
    }
  }

  let getInitUpdate = (sp, prevUpdateInfo) => {
    open ShapeAnimationType
    if ShapeSet.has(animatedShapes, sp) {
      if ShapeSet.has(nodeGroup.shapes, sp) {
        let info = nodeGroup.updater(
          ~sp,
          ~percentage=0.0,
          ~prevUpdateInfo=prevUpdateInfo->opt_getWithDefault(
            ShapeAnimationType.AnimationUpdateInfo.initValue,
          ),
        )
        AnimationUpdateResult.Update(info)
      } else {
        AnimationUpdateResult.NotStart
      }
    } else {
      AnimationUpdateResult.NoAnimation
    }
  }
  {
    updater: animUpdater,
    duration: totalTimeSpan,
    getInitUpdate,
  }
}

let handleClickGroup = (first, nodes, animatedShapes) => {
  let nodeGroup =
    Utils.Arr.unshift(nodes, first)
    ->Utils.Arr.mapAt(
      ~at=node => isAfterEffectNode(node),
      ~mapper=(first, otherNodes) => {
        mergeNodes(first, otherNodes)
      },
    )
    ->Utils.Arr.reduceSelf(~reducer=(first, second) => mergeGroup(first, second))

  animationItemFromNodeGroup(nodeGroup->Option.getUnsafe, animatedShapes)
}

let genAnimQueue = (first: node_t, nodes: array<node_t>) => {
  Array.unshift(nodes, first)

  let animatedShapes = nodes->Array.reduce(ShapeSet.make(), (set, node) => {
    ShapeSet.add(set, node.sp)
    set
  })

  nodes->Utils.Arr.mapAt(
    ~at=node => isClickNode(node),
    ~mapper=(first, nodes) => {
      handleClickGroup(first, nodes, animatedShapes)
    },
  )
}
