/* TypeScript file generated from ShapeAnimationNode.resi by genType. */

/* eslint-disable */
/* tslint:disable */

import * as ShapeAnimationNodeJS from './ShapeAnimationNode.res.js';

import type {AnimationUpdateInfo_t as ShapeAnimationType_AnimationUpdateInfo_t} from './ShapeAnimationType.gen';

import type {animQueue as ShapeAnimationType_animQueue} from './ShapeAnimationType.gen';

import type {slideElement as Core_slideElement} from '../../../src/re/Core.gen';

export type nodeType = "WithEffect" | "ClickEffect" | "AfterEffect";

export type nodeUpdater = (sp:Core_slideElement, percentage:number, prevUpdateInfo:ShapeAnimationType_AnimationUpdateInfo_t) => ShapeAnimationType_AnimationUpdateInfo_t;

export type node_t = {
  readonly sp: Core_slideElement; 
  readonly nodeType: nodeType; 
  readonly duration: number; 
  readonly delay: number; 
  readonly category: number; 
  readonly updater: nodeUpdater
};

export const genAnimQueue: (_1:node_t, _2:node_t[]) => ShapeAnimationType_animQueue = ShapeAnimationNodeJS.genAnimQueue as any;
