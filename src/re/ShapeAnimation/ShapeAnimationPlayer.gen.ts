/* TypeScript file generated from ShapeAnimationPlayer.resi by genType. */

/* eslint-disable */
/* tslint:disable */

import * as ShapeAnimationPlayerJS from './ShapeAnimationPlayer.res.js';

import type {animQueue as ShapeAnimationType_animQueue} from './ShapeAnimationType.gen';

import type {animationDrawer as ShapeAnimationType_animationDrawer} from './ShapeAnimationType.gen';

export abstract class t { protected opaque!: any }; /* simulate opaque types */
export type ShapeAnimationPlayer = t;

export abstract class cursor { protected opaque!: any }; /* simulate opaque types */
export type ShapeAnimationPlayerCursor = cursor;

export type stopPlayStatus = "StopAtBeginning" | "StopAtEnd";
export type StopPlayStatus = stopPlayStatus;

export type onEndHandler = (_1:stopPlayStatus) => void;
export type OnEndHandler = onEndHandler;

export const encodeCursor: (_1:cursor) => string = ShapeAnimationPlayerJS.encodeCursor as any;

export const decodeCursor: (_1:string) => (undefined | cursor) = ShapeAnimationPlayerJS.decodeCursor as any;

export const makePlayer: (_1:ShapeAnimationType_animQueue, onEnd:(undefined | (((_1:stopPlayStatus) => void))), autoPlay:(() => boolean)) => t = ShapeAnimationPlayerJS.makePlayer as any;

export const drawFrame: (_1:t, _2:ShapeAnimationType_animationDrawer) => void = ShapeAnimationPlayerJS.drawFrame as any;

export const isPlaying: (_1:t) => boolean = ShapeAnimationPlayerJS.isPlaying as any;

export const isAtEnd: (_1:t) => boolean = ShapeAnimationPlayerJS.isAtEnd as any;

export const isAtBeginnig: (_1:t) => boolean = ShapeAnimationPlayerJS.isAtBeginnig as any;

export const isAutoPlay: (_1:t) => boolean = ShapeAnimationPlayerJS.isAutoPlay as any;

export const stopAutoPlay: (_1:t, _2:boolean) => void = ShapeAnimationPlayerJS.stopAutoPlay as any;

export const setCursor: (_1:t, _2:string) => void = ShapeAnimationPlayerJS.setCursor as any;

export const getCursor: (_1:t) => cursor = ShapeAnimationPlayerJS.getCursor as any;

export const resetCursor: (_1:t) => void = ShapeAnimationPlayerJS.resetCursor as any;

export const resetCursorAtEnd: (_1:t) => void = ShapeAnimationPlayerJS.resetCursorAtEnd as any;

export const next: (_1:t, _2:ShapeAnimationType_animationDrawer) => void = ShapeAnimationPlayerJS.next as any;

export const prev: (_1:t, _2:ShapeAnimationType_animationDrawer) => void = ShapeAnimationPlayerJS.prev as any;

export const stop: (_1:t, _2:ShapeAnimationType_animationDrawer) => void = ShapeAnimationPlayerJS.stop as any;

export const reset: (_1:t) => void = ShapeAnimationPlayerJS.reset as any;

export const drawStartFrame: (_1:ShapeAnimationType_animQueue, _2:ShapeAnimationType_animationDrawer) => void = ShapeAnimationPlayerJS.drawStartFrame as any;

export const drawEndFrame: (_1:ShapeAnimationType_animQueue, _2:ShapeAnimationType_animationDrawer) => void = ShapeAnimationPlayerJS.drawEndFrame as any;
