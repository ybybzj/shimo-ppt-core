open RescriptCore

let {requestAnimationFrameU, cancelAnimationFrameU} = module(WebAPIRequestAnimationFrame)
let {now} = module(Utils)

type frameCallbackWithDuration = float => unit
type frameCallback = unit => unit
type animationEndCallback = unit => unit
type animStatus = IsPlaying | IsNotPlaying
type animInfo =
  | Anim({frameCallback: frameCallback})
  | AnimWithDur({
      frameCallback: frameCallbackWithDuration,
      duration: float,
      endCallback: option<animationEndCallback>,
    })
type t = {
  mutable status: animStatus,
  info: animInfo,
}
// schedule animationFrame globally
type animationFrameInfo = {timeoutId: int, curAnimation: ref<t>}

let lastAnimationFrame: ref<option<animationFrameInfo>> = ref(None)

let hasAnimationScheduled = lastAnimationFrame =>
  switch lastAnimationFrame.contents {
  | Some(_) => true
  | None => false
  }

let makeAnimation = callback => {
  status: IsNotPlaying,
  info: Anim({frameCallback: callback}),
}
let makeAnimationWithDuration = (
  callback,
  ~endCallback: option<animationEndCallback>,
  ~duration,
) => {
  status: IsNotPlaying,
  info: AnimWithDur({
    duration,
    frameCallback: callback,
    endCallback,
  }),
}

let stopAnimation = anim => {
  if anim.status == IsPlaying {
    anim.status = IsNotPlaying
    if hasAnimationScheduled(lastAnimationFrame) {
      switch lastAnimationFrame.contents {
      | Some({timeoutId, curAnimation}) =>
        cancelAnimationFrameU(timeoutId)
        curAnimation.contents.status = IsNotPlaying
      | _ => ()
      }
      lastAnimationFrame := None
    }
  }
}

let startAnimation = (anim: t) => {
  if !hasAnimationScheduled(lastAnimationFrame) {
    let startTime = now()
    let lastFrameTime = ref(startTime)
    let stepNext = frame => {
      if anim.status == IsPlaying {
        let timeoutId = requestAnimationFrameU(frame)
        lastAnimationFrame :=
          Some({
            timeoutId,
            curAnimation: ref(anim),
          })
      }
    }

    let rec frame = _ => {
      let curTime = now()
      let elapsedTime = curTime - startTime
      let elapsedBetweenFrames = curTime - lastFrameTime.contents

      if elapsedBetweenFrames >= 16 {
        switch anim.info {
        | Anim({frameCallback}) => frameCallback()
        | AnimWithDur({frameCallback, duration, endCallback}) =>
          let percentOfDuration = elapsedTime->Int.toFloat /. duration
          if percentOfDuration >= 1.0 {
            stopAnimation(anim)
            switch endCallback {
            | Some(cb) => cb()
            | None => ()
            }
          } else {
            frameCallback(percentOfDuration)
          }
        }
        lastFrameTime := now()
      }
      stepNext(frame)
    }
    switch anim.info {
    | Anim({frameCallback}) => frameCallback()
    | AnimWithDur({frameCallback}) => frameCallback(0.0)
    }
    anim.status = IsPlaying
    stepNext(frame)
  }
}

let isAnimationPlaying = anim => anim.status == IsPlaying

let playAnimationFrameAt = (anim, percentage) => {
  switch anim.info {
  | Anim({frameCallback}) => frameCallback()
  | AnimWithDur({frameCallback}) => frameCallback(percentage)
  }
}
