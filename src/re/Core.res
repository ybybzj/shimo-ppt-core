@gentype.import(("../core/graphic/Matrix", "Matrix2D"))
type matrix

@gentype.import(("../core/SlideElement/type", "SlideElement"))
type slideElement = {"transform": matrix}

@module("./core/slide")
external hashSlideElement: slideElement => int = "hashSlideElement"

module Paragraph = {
  @gentype.import(("../core/Paragraph/RunElement/type", "RunContentElement"))
  type runElement

  @gentype.import(("./core/textContent", "ItemWithContent"))
  type itemWithContent

  @module("./core/textContent")
  external lengthOfContent: itemWithContent => int = "getContentLength"

  @gentype.as("ParagraphContnetItem") @gentype.opaque
  type item = RunElement(runElement) | ItemWithContent(itemWithContent)

  @module("./core/textContent") @return(nullable)
  external itemOfContentAt: (itemWithContent, int) => option<item> = "getContentItemAt"

  @gentype.import(("../core/Paragraph/common", "ParagraphElementPosition"))
  type paragraphContentPos

  @module("./core/textContent")
  external arrayFromPos: paragraphContentPos => array<int> = "arrayFromPos"
}

module Modoc = {
  @module("./core/modoc")
  external getAttrKeyByChar: string => string = "getAttributeKeyByGul"
}
