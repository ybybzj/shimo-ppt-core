/* TypeScript file generated from ChangeSize.res by genType. */

/* eslint-disable */
/* tslint:disable */

import * as ChangeSizeJS from './ChangeSize.res.js';

export type slideSize = { readonly width: number; readonly height: number };

export type changeInfo = {
  readonly scale: number; 
  readonly deltaX: number; 
  readonly deltaY: number
};

export type srcSlideSize = slideSize;

export type destSlideSize = slideSize;

export abstract class slideSizeChangeType { protected opaque!: any }; /* simulate opaque types */

export type slideSizeComparisonType = 
    { TAG: "ScaleNoScale"; readonly scale: slideSizeChangeType; readonly noScale: slideSizeChangeType }
  | { TAG: "ScaleUpDown"; readonly scaleUp: slideSizeChangeType; readonly scaleDown: slideSizeChangeType };

export const compareSlideSizes: (src:srcSlideSize, dest:destSlideSize) => slideSizeComparisonType = ChangeSizeJS.compareSlideSizes as any;

export const compareSlideSizesByDefault: (src:srcSlideSize, dest:destSlideSize) => slideSizeChangeType = ChangeSizeJS.compareSlideSizesByDefault as any;

export const changeShapeSize: (sizeChangeInfo:slideSizeChangeType) => changeInfo = ChangeSizeJS.changeShapeSize as any;

export const getDestSlideSize: (sizeChangeInfo:slideSizeChangeType) => slideSize = ChangeSizeJS.getDestSlideSize as any;
