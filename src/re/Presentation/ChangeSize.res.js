// Generated by ReScript, PLEASE EDIT WITH CARE


function compareSlideSizes(src, dest) {
  var destH = dest.height;
  var destW = dest.width;
  var srcH = src.height;
  var srcW = src.width;
  if (srcW >= destW && srcH >= destH) {
    return {
            TAG: "ScaleNoScale",
            scale: {
              TAG: "CoveredBySrc",
              _0: src,
              _1: dest,
              _2: "Scale"
            },
            noScale: {
              TAG: "CoveredBySrc",
              _0: src,
              _1: dest,
              _2: "NoScale"
            }
          };
  } else if (srcW <= destW && srcH <= destH) {
    return {
            TAG: "ScaleNoScale",
            scale: {
              TAG: "CoveredByDest",
              _0: dest,
              _1: src,
              _2: "Scale"
            },
            noScale: {
              TAG: "CoveredByDest",
              _0: dest,
              _1: src,
              _2: "NoScale"
            }
          };
  } else {
    return {
            TAG: "ScaleUpDown",
            scaleUp: {
              TAG: "Intersectant",
              _0: src,
              _1: dest,
              _2: "ScaleUp"
            },
            scaleDown: {
              TAG: "Intersectant",
              _0: src,
              _1: dest,
              _2: "ScaleDown"
            }
          };
  }
}

function compareSlideSizesByDefault(src, dest) {
  var destH = dest.height;
  var destW = dest.width;
  var srcH = src.height;
  var srcW = src.width;
  if (srcW >= destW && srcH >= destH) {
    return {
            TAG: "CoveredBySrc",
            _0: src,
            _1: dest,
            _2: "Scale"
          };
  } else if (srcW <= destW && srcH <= destH) {
    return {
            TAG: "CoveredByDest",
            _0: dest,
            _1: src,
            _2: "NoScale"
          };
  } else {
    return {
            TAG: "Intersectant",
            _0: src,
            _1: dest,
            _2: "ScaleDown"
          };
  }
}

function getChangeInfo(compareInfo) {
  var match;
  switch (compareInfo.TAG) {
    case "CoveredBySrc" :
        var match$1 = compareInfo._1;
        var destH = match$1.height;
        var destW = match$1.width;
        var match$2 = compareInfo._0;
        var srcH = match$2.height;
        var srcW = match$2.width;
        match = compareInfo._2 === "Scale" ? [
            srcW === destW ? destH / srcH : (
                srcH === destH ? destW / srcW : Math.min(destW / srcW, destH / srcH)
              ),
            srcW,
            destW,
            srcH,
            destH
          ] : [
            1.0,
            srcW,
            destW,
            srcH,
            destH
          ];
        break;
    case "CoveredByDest" :
        var match$3 = compareInfo._1;
        var srcH$1 = match$3.height;
        var srcW$1 = match$3.width;
        var match$4 = compareInfo._0;
        var destH$1 = match$4.height;
        var destW$1 = match$4.width;
        match = compareInfo._2 === "Scale" ? [
            1.0,
            srcW$1,
            destW$1,
            srcH$1,
            destH$1
          ] : [
            srcW$1 === destW$1 ? destH$1 / srcH$1 : (
                srcH$1 === destH$1 ? destW$1 / srcW$1 : Math.min(destW$1 / srcW$1, destH$1 / srcH$1)
              ),
            srcW$1,
            destW$1,
            srcH$1,
            destH$1
          ];
        break;
    case "Intersectant" :
        var match$5 = compareInfo._1;
        var destH$2 = match$5.height;
        var destW$2 = match$5.width;
        var match$6 = compareInfo._0;
        var srcH$2 = match$6.height;
        var srcW$2 = match$6.width;
        match = compareInfo._2 === "ScaleDown" ? [
            srcW$2 > destW$2 && srcH$2 < destH$2 ? destW$2 / srcW$2 : destH$2 / srcH$2,
            srcW$2,
            destW$2,
            srcH$2,
            destH$2
          ] : [
            srcW$2 > destW$2 && srcH$2 < destH$2 ? destH$2 / srcH$2 : destW$2 / srcW$2,
            srcW$2,
            destW$2,
            srcH$2,
            destH$2
          ];
        break;
    
  }
  var scale = match[0];
  var deltaX = (match[2] - match[1] * scale) / 2;
  var deltaY = (match[4] - match[3] * scale) / 2;
  return {
          scale: scale,
          deltaX: deltaX,
          deltaY: deltaY
        };
}

function changeShapeSize(sizeChangeInfo) {
  return getChangeInfo(sizeChangeInfo);
}

function getDestSlideSize(sizeChangeInfo) {
  switch (sizeChangeInfo.TAG) {
    case "CoveredByDest" :
        var match = sizeChangeInfo._0;
        return {
                width: match.width,
                height: match.height
              };
    case "CoveredBySrc" :
    case "Intersectant" :
        break;
    
  }
  var match$1 = sizeChangeInfo._1;
  return {
          width: match$1.width,
          height: match$1.height
        };
}

export {
  compareSlideSizes ,
  compareSlideSizesByDefault ,
  getChangeInfo ,
  changeShapeSize ,
  getDestSlideSize ,
}
/* No side effect */
