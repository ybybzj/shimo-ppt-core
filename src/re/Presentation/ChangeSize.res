open RescriptCore

type slideSize = {
  width: float,
  height: float,
}

type changeInfo = {
  scale: float,
  deltaX: float,
  deltaY: float,
}
@unboxed
type srcSlideSize = SrcSlideSize(slideSize)
@unboxed
type destSlideSize = DestSlideSize(slideSize)

type intersecScaleType = ScaleDown | ScaleUp
type coveredScaleType = Scale | NoScale

@gentype.opaque
type slideSizeChangeType =
  | CoveredBySrc(srcSlideSize, destSlideSize, coveredScaleType)
  | CoveredByDest(destSlideSize, srcSlideSize, coveredScaleType)
  | Intersectant(srcSlideSize, destSlideSize, intersecScaleType)

type slideSizeComparisonType =
  | ScaleNoScale({scale: slideSizeChangeType, noScale: slideSizeChangeType})
  | ScaleUpDown({scaleUp: slideSizeChangeType, scaleDown: slideSizeChangeType})

@gentype
let compareSlideSizes = (src, dest) => {
  switch (src, dest) {
  | (SrcSlideSize({width: srcW, height: srcH}), DestSlideSize({width: destW, height: destH}))
    if srcW >= destW && srcH >= destH =>
    ScaleNoScale({
      scale: CoveredBySrc(src, dest, Scale),
      noScale: CoveredBySrc(src, dest, NoScale),
    })
  | (SrcSlideSize({width: srcW, height: srcH}), DestSlideSize({width: destW, height: destH}))
    if srcW <= destW && srcH <= destH =>
    ScaleNoScale({
      scale: CoveredByDest(dest, src, Scale),
      noScale: CoveredByDest(dest, src, NoScale),
    })
  | _ =>
    ScaleUpDown({
      scaleUp: Intersectant(src, dest, ScaleUp),
      scaleDown: Intersectant(src, dest, ScaleDown),
    })
  }
}

@gentype
let compareSlideSizesByDefault = (src, dest) => {
  switch (src, dest) {
  | (SrcSlideSize({width: srcW, height: srcH}), DestSlideSize({width: destW, height: destH}))
    if srcW >= destW && srcH >= destH =>
    CoveredBySrc(src, dest, Scale)
  | (SrcSlideSize({width: srcW, height: srcH}), DestSlideSize({width: destW, height: destH}))
    if srcW <= destW && srcH <= destH =>
    CoveredByDest(dest, src, NoScale)
  | _ => Intersectant(src, dest, ScaleDown)
  }
}

let getChangeInfo = compareInfo => {
  let (scale, srcW, destW, srcH, destH) = switch compareInfo {
  | CoveredBySrc(
      SrcSlideSize({width: srcW, height: srcH}),
      DestSlideSize({width: destW, height: destH}),
      Scale,
    ) => (
      if srcW === destW {
        destH /. srcH
      } else if srcH === destH {
        destW /. srcW
      } else {
        Math.min(destW /. srcW, destH /. srcH)
      },
      srcW,
      destW,
      srcH,
      destH,
    )
  | CoveredBySrc(
      SrcSlideSize({width: srcW, height: srcH}),
      DestSlideSize({width: destW, height: destH}),
      NoScale,
    ) => (1.0, srcW, destW, srcH, destH)
  | CoveredByDest(
      DestSlideSize({width: destW, height: destH}),
      SrcSlideSize({width: srcW, height: srcH}),
      Scale,
    ) => (1.0, srcW, destW, srcH, destH)

  | CoveredByDest(
      DestSlideSize({width: destW, height: destH}),
      SrcSlideSize({width: srcW, height: srcH}),
      NoScale,
    ) => (
      if srcW === destW {
        destH /. srcH
      } else if srcH === destH {
        destW /. srcW
      } else {
        Math.min(destW /. srcW, destH /. srcH)
      },
      srcW,
      destW,
      srcH,
      destH,
    )
  | Intersectant(
      SrcSlideSize({width: srcW, height: srcH}),
      DestSlideSize({width: destW, height: destH}),
      ScaleUp,
    ) => (
      if srcW > destW && srcH < destH {
        destH /. srcH
      } else {
        destW /. srcW
      },
      srcW,
      destW,
      srcH,
      destH,
    )

  | Intersectant(
      SrcSlideSize({width: srcW, height: srcH}),
      DestSlideSize({width: destW, height: destH}),
      ScaleDown,
    ) => (
      if srcW > destW && srcH < destH {
        destW /. srcW
      } else {
        destH /. srcH
      },
      srcW,
      destW,
      srcH,
      destH,
    )
  }

  let deltaX = (destW -. srcW *. scale) /. 2.
  let deltaY = (destH -. srcH *. scale) /. 2.
  {
    scale,
    deltaX,
    deltaY,
  }
}

@gentype
let changeShapeSize = sizeChangeInfo => {
  sizeChangeInfo->getChangeInfo
}

@gentype
let getDestSlideSize = sizeChangeInfo => {
  switch sizeChangeInfo {
  | CoveredBySrc(_, DestSlideSize({width, height}), _) => {width, height}
  | CoveredByDest(DestSlideSize({width, height}), _, _) => {width, height}
  | Intersectant(_, DestSlideSize({width, height}), _) => {width, height}
  }
}
