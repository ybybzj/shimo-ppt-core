import { Nullable } from '../../liber/pervasive'
import { EditorSettings } from '../exports/type'

export function checkErrorImage(imgSrc: Nullable<string>): boolean {
  return (
    !imgSrc ||
    (imgSrc.startsWith('error://') && !KnownErrorImageUrls.includes(imgSrc))
  )
}

const ErrorMediaUrls = [
  'error://space_not_enough',
  'error://upload_failed',
  'error://error:media_binding_failed',
  'error://extension_not_supported',
]

// 需要获取封面图的错误
const ErrorPosterFrameUrls = [
  'error://video_poster_frame',
  'error://upload_failed',
]

export const ImportErrorImageUrls = ['error://converting']

export const KnownErrorImageUrls = [
  ...ImportErrorImageUrls,
  ...ErrorMediaUrls,
  ...ErrorPosterFrameUrls,
]

export const isErrorPosterFrameUrl = (url: string) => {
  return ErrorPosterFrameUrls.includes(url)
}

export const isErrorMediaUrl = (url?: string) => {
  return url && ErrorMediaUrls.includes(url)
}

export const isCustomAssetsUrl = (url?: string) => {
  return url != null && url.startsWith(EditorSettings.customAssetProtocol)
}

export const ErrorInfoToImageUrlMap = {}

export const ImageUrlToErrorInfoMap = {}
