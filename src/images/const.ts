export const MAX_IMAGE_DATA_SIZE = 8 * 1048576 // 10 megabytes
export const MIN_CACHE_IMAGE_SIZE = 1024 * 1024 // pixels
