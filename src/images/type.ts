export interface FetchActinMessage {
  action: 'fetch'
  src: string
  imageSrcOrigin: string
}

export interface CompressActionMessage {
  action: 'compress'
  src: string
  needCompress: boolean
  isDebug: boolean
  imgData: Blob
  sizeHint?: number
}

export interface CompressedResultMessage {
  action: 'compresse'
  src: string
  success: true
  isCompressed: boolean
  scale: number
  data: ImageBitmap
}

export interface FetchResultMessage {
  action: 'fetch'
  src: string
  success: true
  data: Blob
}

export interface ActionFailedMessage {
  action: 'fetch' | 'compress'
  src: string
  success: false
  error: Error
}
