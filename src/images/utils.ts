import { Nullable, valuesOfDict } from '../../liber/pervasive'
import { EditorKit } from '../editor/global'
import { loadSVGfromUrl } from '../common/dom/svg'
import { isGifImage } from '../common/utils'
import { checkIsInDebug, logger } from '../lib/debug/log'
import { EditorSettings } from '../core/common/EditorSettings'
import { isSupportOffscreenCanvas } from '../common/dom/OffscreenCanvas'
import { isSupportImageBitmap } from '../common/dom/featureDetect'
import { LRUMap } from '../common/LRUMap'
import {
  checkErrorImage,
  isErrorMediaUrl,
  isErrorPosterFrameUrl,
} from './errorImageSrc'
import { addSearchParams } from '../lib/utils/querystring'
import { MAX_IMAGE_DATA_SIZE, MIN_CACHE_IMAGE_SIZE } from './const'
import { CompressActionMessage, FetchActinMessage } from './type'
import { IEditorKit } from '../editor/type'
import { ScaleOfPPTXSizes } from '../core/common/const/unit'

export const StatusOfLoadableImage = {
  Init: -1,
  Loading: 0,
  Complete: 1,
  Error: 2,
} as const

type ImageLoadHandler = (img: LoadableImage, e?) => void

const bitmapCacheSize =
  (EditorSettings.imageBitmapCacheCount < 0
    ? 500
    : EditorSettings.imageBitmapCacheCount) >> 0

const gImageBitmapCache: LRUMap<string, ImageCacheData> = new LRUMap(
  bitmapCacheSize,
  function releasImageBitmap(bitmap) {
    if (isSupportImageBitmap && bitmap instanceof ImageBitmap) {
      bitmap.close()
    }
  }
)
const gLoadImageBitmapPromises: Map<string, Promise<void>> = new Map()

export type ImageCacheData = HTMLCanvasElement | ImageBitmap

export const gLoadImageExtraUrlParamsMap: Map<
  string,
  { type: 'chart' | 'image'; extX: number; extY: number }
> = new Map()

const BitMapStatus = {
  Dirty: 1,
  Loaded: 2,
  Not_need: 3,
} as const

export class LoadableImage {
  src: string
  get Image(): Nullable<HTMLImageElement | ImageCacheData> {
    this.reloadBitmap()
    return this.bitMap ?? this.imageElm
  }
  private get bitMap(): undefined | ImageCacheData {
    return gImageBitmapCache.get(this.src)
  }
  private set bitMap(bitmap: undefined | ImageCacheData) {
    if (bitmap) {
      gImageBitmapCache.set(this.src, bitmap)
    } else {
      gImageBitmapCache.delete(this.src)
    }
    this._bitMapStatus = BitMapStatus.Loaded
  }
  private _bitMapStatus: valuesOfDict<typeof BitMapStatus> =
    BitMapStatus.Not_need
  private imageElm?: HTMLImageElement
  private imageBlob?: Blob
  private _sizeHint?: number
  private get sizeHint(): undefined | number {
    return this._sizeHint
  }
  private fetchImageFn?: (url: string) => Promise<Blob | undefined>
  Status: valuesOfDict<typeof StatusOfLoadableImage> =
    StatusOfLoadableImage.Init
  isSVG?: boolean
  isGif?: boolean
  isBase64?: boolean
  isErrorImg?: boolean
  encryptSrc?: string
  private onload!: () => void
  private onError?: (e: any) => void
  scale: number = 1
  private editor: IEditorKit

  constructor(editor: IEditorKit, src: string, encryptSrc?: string) {
    this.editor = editor
    this.src = src
    this.encryptSrc = encryptSrc
    this.isGif = isGifImage(src)
    this.isSVG = isSVGUrl(src)
    this.isBase64 = typeof src === 'string' && src.startsWith('data:')
    this.isErrorImg =
      checkErrorImage(src) ||
      isErrorPosterFrameUrl(src) ||
      !!isErrorMediaUrl(src)
  }

  private isUrlSrc() {
    return this.src != null && !this.isBase64 && !this.isSVG
  }
  private resetImage() {
    this.bitMap = undefined
    this.imageElm = undefined
    this.imageBlob = undefined
  }

  private prepareHandlers(
    onload: ImageLoadHandler,
    onerror?: ImageLoadHandler
  ) {
    this.onload = () => {
      this.Status = StatusOfLoadableImage.Complete
      onload(this)
    }
    this.onError = onerror
      ? (ev) => {
          this.Status = StatusOfLoadableImage.Error
          onerror(this, ev)
          this.resetImage()
        }
      : undefined
  }

  reloadBitmap() {
    if (
      this._bitMapStatus === BitMapStatus.Not_need ||
      (this._bitMapStatus === BitMapStatus.Loaded && this.bitMap != null)
    ) {
      return
    }

    if (
      bitmapCacheSize <= 0 ||
      this.Status !== StatusOfLoadableImage.Complete ||
      this.imageBlob == null ||
      this.imageElm == null ||
      this.isBase64 ||
      this.isErrorImg ||
      this.isSVG
    ) {
      return
    }

    const width = this.imageElm.width
    const height = this.imageElm.height

    if (width * height <= MIN_CACHE_IMAGE_SIZE) {
      return
    }

    const sizeOfImg = width * height
    const canCacheToBitmap = isSupportImageBitmap && isSupportOffscreenCanvas
    const sizeLimit = this.sizeHint
      ? (MAX_IMAGE_DATA_SIZE * this.sizeHint) >> 0
      : MAX_IMAGE_DATA_SIZE
    const needCompress = sizeOfImg * 4 > sizeLimit
    if (canCacheToBitmap === false && needCompress === false) {
      return
    }

    if (gLoadImageBitmapPromises.has(this.src)) {
      return
    }

    const loadP = canCacheToBitmap
      ? compressImageData(this.src, this.imageBlob, this.sizeHint).then(
          ([bitmap, scale]) => {
            this.bitMap = bitmap
            this.scale = scale
            gLoadImageBitmapPromises.delete(this.src)
          }
        )
      : Promise.resolve().then(() => {
          const res = _compressImage(this.imageElm!, this.sizeHint)
          if (res) {
            this.bitMap = res.bitmap
            this.scale = res.scale
          }
          gLoadImageBitmapPromises.delete(this.src)
        })
    gLoadImageBitmapPromises.set(this.src, loadP)
  }

  reload(onload: ImageLoadHandler, onerror?: ImageLoadHandler) {
    this.Status = StatusOfLoadableImage.Init
    this.resetImage()
    this.load(onload, onerror)
  }

  load(
    onload: ImageLoadHandler,
    onerror?: ImageLoadHandler,
    fetchImageFn?: (url: string) => Promise<Blob | undefined>
  ) {
    if (fetchImageFn != null) {
      this.fetchImageFn = fetchImageFn
    }
    this.prepareHandlers(onload, onerror)
    this._loadSrc()
  }
  updateSizeHint(extX: number, extY: number) {
    const presentation = this.editor.presentation
    const slideWidth = presentation.width
    const slideHeight = presentation.height
    const imgSpWidth = extX
    const imgSpHeight = extY
    this._sizeHint = (imgSpWidth * imgSpHeight) / (slideWidth * slideHeight)
    this._sizeHint = this._sizeHint < 0.3 ? 0.3 : this._sizeHint
    //reset bitMap
    if (this._bitMapStatus !== BitMapStatus.Not_need) {
      this._bitMapStatus = BitMapStatus.Dirty
    }
  }
  private _loadSrc() {
    let src = this.src
    if (this.isUrlSrc()) {
      const params = gLoadImageExtraUrlParamsMap.get(this.src)
      if (params?.type === 'chart') {
        src = addSearchParams(src, {
          ['extX']: params.extX,
          ['extY']: params.extY,
        })
      } else if (
        screen.width > 0 &&
        screen.height > 0 &&
        params?.type === 'image'
      ) {
        this.updateSizeHint(
          params.extX / ScaleOfPPTXSizes,
          params.extY / ScaleOfPPTXSizes
        )
        //only load once
        gLoadImageExtraUrlParamsMap.delete(this.src)
      }
    }
    // 绘制时采用高保真压缩图, 后续可考虑分场景使用缩略图/原图
    if (EditorKit.transformImageSrcFunc) {
      try {
        src = EditorKit.transformImageSrcFunc(src, 'compressed')
      } catch {
        src = src
      }
      this._load(src)
    } else {
      this._load(src)
    }
  }

  private _load(src: string) {
    if (this.Status === StatusOfLoadableImage.Loading) return
    if (this.isErrorImg) {
      this.onload()
      return
    }
    this.Status = StatusOfLoadableImage.Loading

    if (this.isSVG) {
      const img = new Image()
      img.onload = this.onload
      if (this.onError) {
        img.onerror = this.onError
      }
      this.imageElm = img
      return loadSVGfromUrl(src, img)
    }
    const fetchPromise =
      EditorSettings.isSupportFetch && this.isBase64 !== true
        ? fetchImageData(src, this.fetchImageFn)
            .then(async (imgBlob) => {
              if (imgBlob == null) {
                throw new Error('fetchImage failed, blob is null')
              }

              this.imageBlob = imgBlob
              const url = URL.createObjectURL(imgBlob)
              const img = await _loadImage(url)
              this.imageElm = img
              URL.revokeObjectURL(url)

              const { width, height } = img
              const sizeOfImg = width * height

              if (bitmapCacheSize > 0 && sizeOfImg > MIN_CACHE_IMAGE_SIZE) {
                let bitmap: ImageCacheData | undefined
                if (isSupportImageBitmap && isSupportOffscreenCanvas) {
                  const [_bitmap, scale] = await compressImageData(
                    src,
                    imgBlob,
                    this.sizeHint
                  )
                  bitmap = _bitmap
                  this.scale = scale
                }

                if (EditorSettings.compressLargeImage && bitmap == null) {
                  const _compressResult = _compressImage(img, this.sizeHint)
                  if (_compressResult) {
                    this.scale = _compressResult.scale
                    bitmap = _compressResult.bitmap
                  }
                }
                this.bitMap = bitmap
              }
            })
            .catch(async (e) => {
              logger.warn(`fetchImage failed, fallback to use image.src.`, e)
              return _loadImage(src).then((img) => (this.imageElm = img))
            })
        : _loadImage(src).then((img) => (this.imageElm = img))

    fetchPromise
      .then(() => {
        this.Status = StatusOfLoadableImage.Complete
        this.onload()
      })
      .catch((e) => this.onError && this.onError(e))
  }
}

function _compressImage(img: HTMLImageElement, sizeHint?: number) {
  const width = img.width
  const height = img.height
  const sizeOfBitmap = 4 * width * height
  const sizeLimit = sizeHint
    ? (MAX_IMAGE_DATA_SIZE * sizeHint) >> 0
    : MAX_IMAGE_DATA_SIZE

  if (sizeOfBitmap <= sizeLimit) {
    return undefined
  }

  const scale = Math.sqrt(sizeLimit / sizeOfBitmap)

  const w = (width * scale + 0.5) >> 0
  const h = (height * scale + 0.5) >> 0
  const canvas = document.createElement('canvas')
  canvas.width = w
  canvas.height = h
  const ctx = canvas.getContext('2d')!
  ctx.drawImage(img, 0, 0, w, h)
  return { bitmap: canvas, scale }
}

async function _loadImage(src: string) {
  const img = new Image()
  return new Promise<HTMLImageElement>((resolve, reject) => {
    img.onload = () => resolve(img)
    img.onerror = reject
    img.src = src
    img.crossOrigin = 'anonymous'
  })
}

const a = document.createElement('a')
export function isSVGUrl(url: string): boolean {
  a.href = url
  const path = a.pathname.split('.').pop()
  return !!path?.startsWith('svg')
}

const isDebug = checkIsInDebug()

type PromiseHandlers<T, E> = {
  resolver: (res: T) => void
  rejecter: (e: E) => void
}
const pHandlersForFetch = new Map<string, PromiseHandlers<Blob, any>>()
const pHandlersForCompress = new Map<
  string,
  PromiseHandlers<[ImageBitmap, number], any>
>()

import WorkerSrc from 'inline-worker:../workers/fetchAndCompressImage'
const worker = new Worker(WorkerSrc)

worker.onmessage = async (e) => {
  const res = e['data']
  if (res == null || res['action'] == null || res['src'] == null) {
    return
  }
  const key = res['src']

  switch (res['action']) {
    case 'fetch': {
      if (pHandlersForFetch.has(key) === false) return

      const { resolver, rejecter } = pHandlersForFetch.get(key)!
      if (res['success']) {
        resolver(res['data'])
      } else {
        rejecter(res['error'])
      }
      pHandlersForFetch.delete(key)
      break
    }
    case 'compress': {
      if (pHandlersForCompress.has(key) === false) return
      const { resolver, rejecter } = pHandlersForCompress.get(key)!
      if (res['success']) {
        resolver([res['data'], res['scale']])
      } else {
        rejecter(res['error'])
      }
      pHandlersForCompress.delete(key)
      break
    }
  }
}

async function fetchImageData(
  src: string,
  fetchImageFn?: (url: string) => Promise<Blob | undefined>
) {
  if (EditorSettings.useWorkerForImageLoading && fetchImageFn != null) {
    return fetchImageFn(src)
  } else if (EditorSettings.useWorkerForImageLoading) {
    const p = new Promise<Blob>((resolver, rejecter) => {
      pHandlersForFetch.set(src, { resolver, rejecter })
    })
    const msg: FetchActinMessage = {
      ['src']: src,
      ['action']: 'fetch',
      ['imageSrcOrigin']: EditorSettings.imageSrcOrigin,
    }
    worker.postMessage(msg)
    return p
  }
}

async function compressImageData(
  src: string,
  imgData: Blob,
  sizeHint?: number
) {
  const p = new Promise<[ImageBitmap, number]>((resolver, rejecter) => {
    pHandlersForCompress.set(src, { resolver, rejecter })
  })
  const msg: CompressActionMessage = {
    ['src']: src,
    ['imgData']: imgData,
    ['action']: 'compress',
    ['isDebug']: isDebug,
    ['needCompress']:
      EditorSettings.compressLargeImage && isSupportOffscreenCanvas,
  }

  if (sizeHint != null) {
    msg['sizeHint'] = sizeHint
  }

  if (imgData instanceof ImageBitmap) {
    worker.postMessage(msg, [imgData])
  } else {
    worker.postMessage(msg)
  }
  return p
}
