import { Dict, Nullable } from '../../liber/pervasive'
import { IEditorKit, UploadUrlResult } from '../editor/type'
import { invalidAllMastersThumbnails } from '../ui/editorUI/utils/utils'
import { refreshPresentation } from '../editor/utils/editor'
import { StatusOfLoadableImage, LoadableImage } from './utils'
import { ImageShape } from '../core/SlideElement/Image'
import { BlipFill } from '../core/SlideElement/attrs/fill/blipFill'

export class ImageLoadManager {
  imagesMap: Dict<LoadableImage> = {}
  private base64Map: Map<string, ImageShape> = new Map<string, ImageShape>()
  editor!: IEditorKit
  private loadingPromises: Map<string, Promise<LoadableImage>> = new Map()
  constructor() {}

  init(editor: IEditorKit) {
    this.editor = editor
  }

  getImage(src: Nullable<string>) {
    if (!src) {
      return null
    }
    return this.imagesMap[src]
  }

  collectBase64ImageShape(dataurl: string, sp: ImageShape) {
    this.base64Map.set(dataurl, sp)
  }

  async loadBase64(): Promise<{ url: string } | void> {
    const entries: [string, ImageShape][] = []
    this.base64Map.forEach((sp, key) => entries.push([key, sp]))
    if (entries.length <= 0) {
      return
    }

    const imagesToLoad: UploadUrlResult[] = []
    for (const [dataurl, sp] of entries) {
      try {
        const urls = await this.editor.uploadImages([dataurl])
        if (urls && urls.length > 0) {
          const url = urls[0].url
          if (url) {
            if (sp.blipFill) {
              sp.blipFill.setImageSrcId(url)
            } else {
              sp.blipFill = new BlipFill(url)
              sp.blipFill.setStretch(true)
            }
            imagesToLoad.push({ url })
          }
        }
      } catch (_) {}
    }
    this.base64Map.clear()
    if (imagesToLoad.length > 0) {
      await this.loadImages(imagesToLoad, () => {
        refreshPresentation(this.editor)
        invalidAllMastersThumbnails(this.editor.editorUI, {
          images: imagesToLoad.map((r) => r.url),
        })
      })
    }
  }

  async loadImages(
    urls: Array<UploadUrlResult>,
    cb?: (image: LoadableImage) => any
  ): Promise<LoadableImage[]> {
    if (urls.length === 0) {
      return []
    }
    const tasks = urls.map(({ url, encryptUrl }) => {
      const image = this.getImage(url)
      let loadImgP = this.loadingPromises.get(url)
      if (image) {
        switch (image.Status) {
          case StatusOfLoadableImage.Complete: {
            return Promise.resolve(image)
          }
          case StatusOfLoadableImage.Loading: {
            if (loadImgP) {
              return loadImgP
            }
            break
          }
          case StatusOfLoadableImage.Init:
          case StatusOfLoadableImage.Error: {
            loadImgP = new Promise((resolve) => {
              image.reload(
                () => {
                  if (cb) {
                    cb(image)
                  }
                  this.loadingPromises.delete(url)
                  resolve(image)
                },
                () => {
                  if (cb) {
                    cb(image)
                  }

                  this.loadingPromises.delete(url)
                  resolve(image)
                }
              )
            })
            break
          }
        }
      }

      if (loadImgP == null) {
        loadImgP = new Promise((resolve: (image: LoadableImage) => any) => {
          const image = new LoadableImage(this.editor, url, encryptUrl)
          this.imagesMap[image.src] = image
          image.load(
            () => {
              if (cb) {
                cb(image)
              }
              this.loadingPromises.delete(url)
              resolve(image)
            },
            () => {
              if (cb) {
                cb(image)
              }

              this.loadingPromises.delete(url)
              resolve(image)
            },
            this.editor.fetchImage
          )
        })
      }
      this.loadingPromises.set(url, loadImgP)
      return loadImgP
    })

    return Promise.all(tasks)
  }

  reloadErrorImages() {
    const errorImageUrls = this.getErrorImageUrls()

    const urls = errorImageUrls.map((errUrl) => {
      return { url: errUrl }
    })
    refreshPresentation(this.editor)
    this.loadImages(urls, () => {
      refreshPresentation(this.editor)
    }).finally(() => {
      invalidAllMastersThumbnails(this.editor.editorUI, {
        images: urls.map((o) => o.url),
      })
    })
  }

  private getErrorImageUrls() {
    const errorImageUrls: string[] = []
    for (const url in this.imagesMap) {
      if (this.checkErrorImageUrl(url)) {
        errorImageUrls.push(url)
      }
    }
    return errorImageUrls
  }

  checkErrorImageUrl(url): boolean {
    const { Status } = this.imagesMap[url] || {}
    if (Status === StatusOfLoadableImage.Error) {
      return true
    }
    return false
  }

  addAliasForExistingImage(url: string, targetUrl: string) {
    const image = this.getImage(targetUrl)
    if (image) {
      this.imagesMap[url] = image
      delete this.imagesMap[targetUrl]
    }
  }
}
