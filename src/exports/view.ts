import { Editor } from './type'
import { IEditorKit } from '../editor/type'
import { CollaboratorStatus } from '../ui/editorUI/components/thumbnailManager'
import { Dict } from '../../liber/pervasive'
import { isNumber } from '../common/utils'

export function canZoom(editor: Editor) {
  return (editor as unknown as IEditorKit).editorUI.canZoom()
}

export function zoomIn(editor: Editor) {
  ;(editor as unknown as IEditorKit).zoomIn()
}

export function zoomOut(editor: Editor) {
  ;(editor as unknown as IEditorKit).zoomOut()
}

export function zoomFitToPage(editor: Editor) {
  ;(editor as unknown as IEditorKit).zoomFitToPage()
}

export function zoomFitToWidth(editor: Editor) {
  ;(editor as unknown as IEditorKit).zoomFitToWidth()
}

export function zoom(editor: Editor, percent: number) {
  ;(editor as unknown as IEditorKit).zoom(percent)
}

export function getZoomValue(editor: Editor): number {
  return (editor as unknown as IEditorKit).getZoomValue()
}

export function goToPage(editor: Editor, slideIndex: number) {
  ;(editor as unknown as IEditorKit).goToSlide(slideIndex)
}

export function goToPageByRefId(editor: Editor, refId: string) {
  ;(editor as unknown as IEditorKit).goToSlideByRefId(refId)
}

/**
 * 总页数
 * @export
 * @param {Editor} editor
 */
export function getAmountOfSlides(editor: Editor): number {
  return (editor as unknown as IEditorKit).getAmountOfSlides()
}

/**
 * 获取激活幻灯片位置
 * @export
 * @param {Editor} editor
 */
export function getCurrentPage(editor: Editor): number {
  return (editor as unknown as IEditorKit).getCurrentSlideIndex()
}

export function updateCollaboratorStatus(
  editor: Editor,
  status: CollaboratorStatus
) {
  ;(
    editor as unknown as IEditorKit
  ).editorUI.thumbnailsManager.updateCollaboratorStatus(status)
}

/** 协作 ui core 接口数据类型 */
export type CollaborationStatus = {
  userId: number
  userName: string
  info: CollaborationInfo
  color: string
  time: number
}[]

/** 协作 ui 对外事件数据类型 */
export interface CollaborationInfo {
  slideRef: string
  shapeInfos: CollaborationShapeInfo[]
}

export interface CollaborationShapeInfo {
  spRef: string
  tableInfo?: { rowIndex: number; cellIndex: number }
}

/** DrawingController 内部使用的数据类型, key 为 spRef */
export type CollaborationShapeState = Dict<{
  users: CollborationUserInfo[]
}>

export interface CollborationUserInfo {
  id: number
  name: string
  lastActiveTime: number
  color: string
  tableInfo?: { rowIndex: number; cellIndex: number }
}

export function updateCollaborationUI(
  editor: Editor,
  status: CollaborationStatus
) {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  const drawingCoStatus: Dict<CollaborationShapeState> = {}
  for (const state of status) {
    const userId = state['userId']
    const userName = state['userName'] ?? userId
    const color = state['color']
    const time = state['time']
    const collaborationInfo = state['info']
    if (!userId || !collaborationInfo) continue
    const slideRef = collaborationInfo['slideRef']
    const shapeInfos = collaborationInfo['shapeInfos']

    for (const shapeInfo of shapeInfos) {
      const spRef = shapeInfo['spRef']
      const tableInfo = shapeInfo['tableInfo']
      const lastActiveTime = isNumber(time) ? time : Date.now()
      if (!drawingCoStatus[slideRef]) {
        drawingCoStatus[slideRef] = {
          [spRef]: {
            users: [
              { id: userId, name: userName, color, lastActiveTime, tableInfo },
            ],
          },
        }
      } else if (!drawingCoStatus[slideRef][spRef]) {
        drawingCoStatus[slideRef][spRef] = {
          users: [
            { id: userId, name: userName, color, lastActiveTime, tableInfo },
          ],
        }
      } else {
        const spCoStatus = drawingCoStatus[slideRef][spRef]
        spCoStatus.users.push({
          id: userId,
          name: userName,
          color,
          lastActiveTime,
          tableInfo,
        })
      }
    }
  }
  editorUI.renderingController.updateCollaborationStatus(drawingCoStatus)
  editorUI.editorHandler.updateEditLayer()
  editorUI.triggerRender(true)
}
