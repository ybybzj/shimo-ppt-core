import { Editor } from './type'
import { IEditorKit } from '../editor/type'
import { replaceText } from '../core/search/replaceText'

type SearchParams = {
  text: string
  isNext: boolean
  isCaseSensitive?: boolean
  noJump?: boolean
}

type ReplaceParams = {
  text: string
  replaceWith: string
  isReplaceAll: boolean
  isCaseSensitive?: boolean
}

export function find(editor: Editor, searchParams: SearchParams) {
  const text = searchParams['text']
  const isNext = searchParams['isNext']
  const isCaseSensitive = searchParams['isCaseSensitive']
  const noJump = searchParams['noJump']
  return (editor as unknown as IEditorKit).findText(
    text,
    isNext,
    isCaseSensitive,
    noJump
  )
}

/**
 * - 若 isReplaceAll === true, 则直接全量替换
 * - 否则, 会先 find 至要替换的文本, 再次调用则进行替换操作
 */
export function replace(editor: Editor, replaceParams: ReplaceParams) {
  const text = replaceParams['text']
  const replaceWith = replaceParams['replaceWith']
  const isReplaceAll = replaceParams['isReplaceAll']
  const isCaseSensitive = replaceParams['isCaseSensitive']
  const presentation = (editor as unknown as IEditorKit).presentation

  const result = replaceText(
    presentation,
    text,
    replaceWith,
    isReplaceAll,
    isCaseSensitive
  )
  // 对于全量替换暂时先这样处理, 后续应该仅更新对应的 slideThumbnail
  if (isReplaceAll && result && result.replaceNum > 0) {
    ;(editor as unknown as IEditorKit).editorUI.thumbnailsManager.updateAll()
  }
  return result
}

export function clearSearchResult(editor: Editor) {
  ;(editor as unknown as IEditorKit).clearSearchResult()
}
