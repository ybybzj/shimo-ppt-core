import { createEditorKit } from '../editor/editorKit'
import { Editor, EditorApiOptions, GuidelineUsability } from './type'
import { EditorUtil as EditorGlobal } from '../globals/editor'
import { IEditorKit as EditorKit, IEditorKit } from '../editor/type'
import {
  EditorSettings,
  EditorSettingsKeys,
  setEditorSettings,
} from '../core/common/EditorSettings'
import { refreshPresentation } from '../editor/utils/editor'
import { idGenerator } from '../globals/IdGenerator'
import { invalidAllMastersThumbnails } from '../ui/editorUI/utils/utils'
import { BrowserInfo } from '../common/browserInfo'
import { IOAdapter } from '../io/adapter'
// import { CachedArray, PoolAllocator } from '../common/allocator'

export function createEditor(
  config: EditorApiOptions,
  ioAdapter: IOAdapter<any>
): Editor {
  return createEditorKit(config, ioAdapter) as unknown as Editor
}

export function setSourceFileId(editor: Editor, id?: string) {
  const editorKit = editor as unknown as EditorKit
  editorKit.sourceFileId = id
}

export function loadPPT(editor: Editor, data: any) {
  ;(editor as unknown as EditorKit).loadData(data)
}

export function reloadPPT(editor: Editor, data: any) {
  EditorGlobal.entityRegistry.reset()
  EditorGlobal.ChangesStack.clear()
  idGenerator.clear()
  // PoolAllocator.disposeAll()
  // CachedArray.dispose()
  const editorKit = editor as unknown as EditorKit
  editorKit.editorUI.reset()
  editorKit.loadData(data)
}

export function setViewMode(editor: Editor, isViewMode: boolean) {
  console.log('setViewMode', isViewMode)
  ;(editor as unknown as EditorKit).setViewMode(isViewMode)
}

export function isLoadPPTCompleted(editor: Editor): boolean {
  return (editor as unknown as EditorKit).presentation.isLoadComplete === true
}

/**
 *  可视区域的自适应调整
 * @export
 * @param {Editor} editor
 */
export function resize(editor: Editor) {
  ;(editor as unknown as EditorKit).Resize()
}

export function getMainContainerElement(editor: Editor) {
  return (editor as unknown as EditorKit).editorUI.mainContainer.element
}

/** 权限控制 */
export function setEditorFeatureSetting(
  editor: Editor,
  settings: Partial<EditorSettings>
) {
  const editorKit = editor as unknown as IEditorKit
  const editorUI = editorKit.editorUI
  // 混淆, 不能直接匹配 key
  //

  for (const key of Object.keys(settings)) {
    const value = settings[key]
    switch (key) {
      case 'isRTL': {
        setEditorSettings(key, value)
        // Todo: 后续支持各组件内单独设置 RTL
        editorUI.thumbnailsManager.setRTL(value)
        editorUI.calcLayoutDirection()
        break
      }
      case 'isShowThumbnail': {
        if (EditorSettings.isEnableThumbnail || value === false) {
          setEditorSettings(key, value)
          editorUI.setThumbnailsVisibility(EditorSettings.isShowThumbnail)
        }
        break
      }
      case 'isEnableThumbnail': {
        setEditorSettings(key, value)
        if (value === false && EditorSettings.isShowThumbnail) {
          setEditorFeatureSetting(editor, { ['isShowThumbnail']: false })
        }
        break
      }
      case 'isUseHarfBuzz': {
        setEditorSettings(key, value)
        refreshPresentation(editorKit, true)
        invalidAllMastersThumbnails(editorKit.editorUI)
        break
      }
      case 'isNotesEnabled': {
        if (EditorSettings.isNotesFeatureEnabled || value === false) {
          editorUI.setNotesEnable(value)

          setEditorSettings(key, value)
        }
        break
      }
      case 'isNotesFeatureEnabled': {
        setEditorSettings(key, value)
        if (value === false && EditorSettings.isNotesEnabled) {
          setEditorFeatureSetting(editor, { ['isNotesEnabled']: false })
        }
        break
      }
      default: {
        setEditorSettings(key as EditorSettingsKeys, value)
      }
    }
  }

  editorUI.triggerRender()
}

export function removeEventListeners(editor: Editor) {
  ;(editor as unknown as EditorKit).editorUI.removeEvents()
}

export function setViewModeCopyable(editor: Editor, copyable = false) {
  ;(editor as unknown as IEditorKit).setViewModeCopyable(copyable)
}

export function checkSlideLoaded(
  editor: Editor,
  refId: string,
  isWait = false
) {
  return (
    editor as unknown as IEditorKit
  ).resourcesLoadManager.checkSlideLoaded(refId, isWait)
}

export function getGuidelineUsability(): GuidelineUsability {
  const ret: GuidelineUsability = {}
  ret['isMeasureGuidelineEnabled'] = EditorSettings.isMeasureGuidelineEnabled
  ret['isAlignGuidelineEnabled'] = EditorSettings.isAlignGuidelineEnabled
  return ret
}

export function setDeviceHasExternalKeyboard(
  editor: Editor,
  hasExternalKeyBoard: boolean
) {
  BrowserInfo.hasExternalKeyBoard = hasExternalKeyBoard
  ;(editor as unknown as EditorKit).editorUI.checkMobileInput()
}

export function focusEditor(editor: Editor) {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  editorUI.focusForKeyHandling()
}
