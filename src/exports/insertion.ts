import { Dict } from '../../liber/pervasive'
import { IEditorKit } from '../editor/type'
import { setRefContent } from '../io/insertion/refContent'
import {
  SpPackedData,
  insertSlideElementFromPackedOp,
  packedOpFromSpData,
  peviewImageFromPackedOp,
  slideElementFromPackedOp,
  spPackedDataFromTypeAndData,
} from '../io/insertion/shapes'
import { Operation } from '../io/operation'
import { strHash } from '../lib/utils/string'
import { getPPTData } from './collab'
import { Editor } from './type'

export { packedOpFromSpData } from '../io/insertion/shapes'

export function spDataFromSp(
  editor: Editor,
  spRid: string,
  checkAssets = false
): undefined | SpPackedData | null {
  const modocJson = getPPTData(editor) as Operation[] | undefined
  if (!modocJson || !modocJson[3]) return
  const slidesData = modocJson[3]

  const _editor: IEditorKit = editor as unknown as IEditorKit
  const ioAdapter = _editor.ioAdapter

  const findSpData = (
    doc: Dict<any>,
    targetKey: string
  ): undefined | SpPackedData | null => {
    for (const key of Object.keys(doc)) {
      const target = doc[key]
      if (key === targetKey && Array.isArray(target)) {
        if (target[0]?.['data']['delta'] != null) {
          const _target = target[0]
          const spType = _target['attributes']['spType']
          const deltaJson = _target['data']['delta']
          const data = ioAdapter.stringify(ioAdapter.fromOperations(deltaJson))

          const spPackedData = spPackedDataFromTypeAndData(spType, data)
          if (checkAssets && spPackedData != null) {
            const op = packedOpFromSpData(spPackedData)
            if (op) {
              const info = slideElementFromPackedOp(op, _editor, false)
              const assets = info?.asserts
              if (assets && assets.length > 0) {
                let deltaJsonStr = JSON.stringify(deltaJson)
                for (const url of assets) {
                  const reg = new RegExp(url, 'g')
                  deltaJsonStr = deltaJsonStr.replace(reg, strHash(url))
                }

                const _data = ioAdapter.stringify(
                  ioAdapter.fromOperations(JSON.parse(deltaJsonStr))
                )
                return spPackedDataFromTypeAndData(spType, _data)
              }
            }
          }

          return spPackedData
        }
        return null
      } else {
        if (Object(target) === target) {
          const ret = findSpData(target, targetKey)
          if (ret !== undefined) return ret
        }
      }
    }
  }
  return findSpData(slidesData, spRid)
}

export function previewImageFromSpData(
  editor: Editor,
  spData: SpPackedData,
  ratio = 1
) {
  const _editor: IEditorKit = editor as unknown as IEditorKit

  const op = packedOpFromSpData(spData)
  if (op == null) return undefined

  return peviewImageFromPackedOp(op, _editor, ratio)
}

export function insertSlideElementFromSpData(
  editor: Editor,
  spData: SpPackedData,
  keepFormatting = false,
  assertUrlsMappings?: Record<string, string>
) {
  const _editor: IEditorKit = editor as unknown as IEditorKit

  const op = packedOpFromSpData(spData)
  if (op == null) return undefined

  return insertSlideElementFromPackedOp(
    op,
    _editor,
    keepFormatting,
    assertUrlsMappings
  )
}

export function setRefContentForSpInsertion(_editor: Editor, content: string) {
  setRefContent(content)
}
;(window as any).insertSpData = insertSlideElementFromSpData
