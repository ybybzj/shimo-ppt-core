import { Editor } from './type'
import { IEditorKit } from '../editor/type'

export function setFont(editor: Editor, name: string) {
  ;(editor as unknown as IEditorKit).setFont(name)
}

export function setFontSize(editor: Editor, size: number) {
  ;(editor as unknown as IEditorKit).setFontSize(size)
}

export function setBold(editor: Editor, value: boolean) {
  ;(editor as unknown as IEditorKit).setBold(value)
}

export function setItalic(editor: Editor, value: boolean) {
  ;(editor as unknown as IEditorKit).setItalic(value)
}

export function setUnderline(editor: Editor, value: boolean) {
  ;(editor as unknown as IEditorKit).setUnderline(value)
}

export function setStrikeout(editor: Editor, value: boolean) {
  ;(editor as unknown as IEditorKit).setStrikeout(value)
}

export function setFontSizeIn(editor: Editor): void {
  ;(editor as unknown as IEditorKit).setFontSizeIn()
}

export function setFontSizeOut(editor: Editor): void {
  ;(editor as unknown as IEditorKit).setFontSizeOut()
}

export function setTextColor(editor: Editor, color: string): void {
  ;(editor as unknown as IEditorKit).setTextColor(color)
}

export function setHighlight(
  editor: Editor,
  isHighlight: boolean,
  color?: string
): void {
  ;(editor as unknown as IEditorKit).setHighlight(isHighlight, color)
}
