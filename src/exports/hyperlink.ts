import { Editor } from './type'
import { IEditorKit } from '../editor/type'
import { HyperlinkProps } from '../editor/proto'

export function canInsertHyperlink(editor: Editor): boolean {
  return (editor as unknown as IEditorKit).canInsertHyperlink()
}

/**
 * 插入超链接
 */
export function insertHyperlink(editor: Editor, props: HyperlinkProps): void {
  ;(editor as unknown as IEditorKit).insertHyperlink(props)
}

/**
 * 修改超链接
 */
export function changeHyperlink(editor: Editor, props: HyperlinkProps): void {
  ;(editor as unknown as IEditorKit).changeHyperlink(props)
}

/**
 * 移除超链接
 * @export
 * @param {Editor} editor
 */
export function removeHyperlink(editor: Editor): void {
  ;(editor as unknown as IEditorKit).removeHyperlink()
}
