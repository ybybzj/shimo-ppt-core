import { Editor } from './type'
import { IEditorKit } from '../editor/type'
import { EditorSettings } from '../core/common/EditorSettings'
import {
  GCommentIconsRetinaSourceRTL,
  GCommentIconsRetinaSource,
  CommentStatusType,
} from '../core/common/const/drawing'
import {
  hoverCommentById,
  onHideComments,
  selectCommentById,
  showComments as _showComments,
} from '../ui/features/comment/utils'
import {
  InputCommentData,
  ServerInputCommentData,
} from '../core/utilities/comments'
import { Evt_onCommentChange } from '../editor/events/EventNames'

/** 关闭功能开关会同时隐藏评论 */
export function setCommentPermission(
  editor: Editor,
  permission: 'none' | 'view' | 'insert' | 'delete'
) {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  switch (permission) {
    case 'none':
      EditorSettings.isSupportComment = false
      EditorSettings.isShowComment = false
      EditorSettings.canAddComment = false
      onHideComments(editorUI.editorHandler)
      break
    case 'view':
      EditorSettings.isSupportComment = true
      EditorSettings.canAddComment = false
      break
    case 'insert':
      EditorSettings.isSupportComment = true
      EditorSettings.canAddComment = true
      break
    case 'delete':
      // 暂未细分
      EditorSettings.isSupportComment = true
      EditorSettings.canAddComment = true
      break
  }
}

export function addComment(
  editor: Editor,
  data: InputCommentData
): string | undefined {
  return (editor as unknown as IEditorKit).addComment(data)
}

export function editCommentContent(
  editor: Editor,
  commentUid: string,
  content: string
) {
  if (!EditorSettings.isSupportComment) return
  const editorKit = editor as unknown as IEditorKit
  const presentation = editorKit.presentation
  presentation.updateCommentContent(commentUid, content)
  editorKit.emitEvent(Evt_onCommentChange)
}

interface MobilePos {
  slideIndex: number
  percentX: number // [0, 1]
  percentY: number
}

export function addCommentAtPagePercentagePosition(
  editor: Editor,
  pos: MobilePos,
  data: ServerInputCommentData
): string | undefined {
  if (!EditorSettings.isSupportComment) return
  const editorKit = editor as unknown as IEditorKit

  const slideIndex = pos['slideIndex']
  const percentX = pos['percentX']
  const percentY = pos['percentY']
  const { editorUI, presentation } = editorKit
  if (presentation.currentSlideIndex < 0) return
  if (presentation.currentSlideIndex !== slideIndex) {
    editorUI.goToSlide(slideIndex)
  }

  const cmtXY = {
    x: percentX * presentation.width,
    y: percentY * presentation.height,
  }
  const comment = presentation.addComment(data, cmtXY, slideIndex)
  if (comment) {
    editorUI.triggerRender(true)
    editorKit.emitAddComment()
    return comment.getUid()
  }
}

export function canAddComment(editor: Editor): boolean {
  return (editor as unknown as IEditorKit).presentation.canAddComment()
}

export function getSlideComments(editor: Editor, slideIndex?: number) {
  if (!EditorSettings.isSupportComment) return {}
  return (editor as unknown as IEditorKit).presentation.getSlideComments(
    slideIndex
  )
}

export function getCommmentIcon(_editor: Editor) {
  const source = EditorSettings.isRTL
    ? GCommentIconsRetinaSourceRTL
    : GCommentIconsRetinaSource
  return {
    default: source[CommentStatusType.Default].src.src,
    hover: source[CommentStatusType.Hover].src.src,
    select: source[CommentStatusType.Select].src.src,
  }
}

export function hideComments(editor: Editor): void {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  onHideComments(editorUI.editorHandler)
}

export function hoverComment(editor: Editor, id?: string): void {
  if (!EditorSettings.isSupportComment && !EditorSettings.isShowComment) {
    return
  }
  const editorUI = (editor as unknown as IEditorKit).editorUI
  hoverCommentById(editorUI.editorHandler, id)
}

export function removeComment(editor: Editor, id: string) {
  if (!EditorSettings.isSupportComment) return
  const editorUI = (editor as unknown as IEditorKit).editorUI
  const { presentation } = editorUI
  const slideIdxForRemovedComments = presentation.removeComment(id)
  if (slideIdxForRemovedComments > -1) {
    editorUI.editorKit.emitRemoveComment()
    if (presentation.currentSlideIndex !== slideIdxForRemovedComments) {
      editorUI.goToSlide(slideIdxForRemovedComments)
    }
  }
}

export function selectComment(editor: Editor, id?: string): void {
  if (!EditorSettings.isSupportComment && !EditorSettings.isShowComment) {
    return
  }
  const editorUI = (editor as unknown as IEditorKit).editorUI
  selectCommentById(editorUI.editorHandler, id)
}

export function showComments(editor: Editor): void {
  if (!EditorSettings.isSupportComment) return
  const editorUI = (editor as unknown as IEditorKit).editorUI
  _showComments(editorUI.editorHandler)
}

export function startAddComment(editor: Editor) {
  ;(editor as unknown as IEditorKit).startAddComment()
}

export function endAddComment(editor: Editor) {
  ;(editor as unknown as IEditorKit).endAddComment()
}

/** 对于维护在外部的状态(如 shouldDisplayComment ), 若发生变化, 可以通过此方法更新 comment 视图 */
export function updateCommentView(editor: Editor, _data?) {
  ;(editor as unknown as IEditorKit).editorUI.triggerRender(true)
}
