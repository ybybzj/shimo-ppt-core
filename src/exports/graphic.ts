import { Editor, GradientStop, MediaInfo, GradientDirections } from './type'
import { IEditorKit, UploadUrlResult } from '../editor/type'
import {
  FillKIND,
  FillGradPathType,
  FillGradType,
  MediaType,
  ObjectsAlignType,
  ObjectsAlignTypeValues,
} from '../core/common/const/attrs'
import { ImageShape } from '../core/SlideElement/Image'
import { EditorUtil as EditorInstance } from '../globals/editor'
import { InstanceType } from '../core/instanceTypes'
import {
  cropByGeometry,
  cropByOption,
  cropByRatio,
  cropBySize,
  endCrop,
  getCropImage,
  resetSrcRect,
  startCrop,
} from '../ui/features/crop/utils'
import { EditActionFlag } from '../core/EditActionFlag'
import { AddedShapeInfo } from '../editor/proto'
import { TextVerticalType } from '../core/SlideElement/const'
import { Offset, Size } from '../core/common/types'
import {
  ImageSubTypes,
  PresetLineDashStyleValues,
} from '../io/dataType/spAttrs'
import { formatGsLst, formatLinearAngle } from '../core/graphic/utils'
import { startEditAction } from '../core/calculation/informChanges/startAction'
import { isNumber } from '../common/utils'
import {
  alignByDirection,
  distributeHorizental,
  distributeVertical,
} from '../ui/features/move/utils'
import { startAddShape } from '../ui/features/addShape/utils'
import { ShapeTypes } from '../ui/features/addShape/const'
import { FillGrad } from '../core/properties/props'
import {
  ShapeFillOptions,
  SlideElementOptions,
} from '../core/properties/options'

export const _ShapeTypes = ShapeTypes

type BasicShapesTypes =
  (typeof ShapeTypes.BasicShapes)[keyof typeof ShapeTypes.BasicShapes]
type FiguredArrowsTypes =
  (typeof ShapeTypes.FiguredArrows)[keyof typeof ShapeTypes.FiguredArrows]
type MathTypes = (typeof ShapeTypes.Math)[keyof typeof ShapeTypes.Math]
type ChartsTypes = (typeof ShapeTypes.Charts)[keyof typeof ShapeTypes.Charts]
type StarsAndRibbonsTypes =
  (typeof ShapeTypes.StarsAndRibbons)[keyof typeof ShapeTypes.StarsAndRibbons]
type CalloutsTypes =
  (typeof ShapeTypes.Callouts)[keyof typeof ShapeTypes.Callouts]
type ButtonsTypes = (typeof ShapeTypes.Buttons)[keyof typeof ShapeTypes.Buttons]
type RectanglesTypes =
  (typeof ShapeTypes.Rectangles)[keyof typeof ShapeTypes.Rectangles]
type LinesTypes = (typeof ShapeTypes.Lines)[keyof typeof ShapeTypes.Lines]
export type PresetShapeTypes =
  | BasicShapesTypes
  | FiguredArrowsTypes
  | MathTypes
  | ChartsTypes
  | StarsAndRibbonsTypes
  | CalloutsTypes
  | ButtonsTypes
  | RectanglesTypes
  | LinesTypes

/**
 * 插入形状
 * @export
 * @param {Editor} editor
 * @param {string} preset   // 'ellipse' 'smileyFace' and so on
 * @param {boolean} isApply
 */
export function addShape(
  editor: Editor,
  preset: PresetShapeTypes,
  isApply: boolean,
  options?: {
    vertType?: number
  }
) {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  const handler = editorUI.editorHandler

  startAddShape(handler, preset, isApply, options)
  if (isApply) {
    editorUI.mouseCursor.lock('crosshair')
  }
}

/**
 * 变换形状类型
 * @export
 * @param {string} shapetype   // 'ellipse' 'smileyFace' and so on
 */
export function changeShapeType(editor: Editor, shapetype: PresetShapeTypes) {
  ;(editor as unknown as IEditorKit).modifyShapeType(shapetype)
}

/**
 * 插入文本框
 * @export
 * @param {Editor} editor
 * @param {boolean} isApply
 */
export function addTextBox(editor: Editor, isApply: boolean, isVert?: boolean) {
  const options = isVert
    ? {
        vertType: TextVerticalType.eaVert,
      }
    : undefined
  addShape(editor, 'textRect', isApply, options)
}

export function addAdvancedTextBox(
  editor: Editor,
  options: { offset?: Offset; size?: Size } = {}
): AddedShapeInfo {
  return (editor as unknown as IEditorKit).addTextBox(options)
}

/**
 * 插入矩形
 * @export
 * @param {Editor} editor
 * @param {boolean} isApply
 */
export function addRect(editor: Editor, isApply: boolean) {
  addShape(editor, 'rect', isApply)
}

/**
 * 插入直线
 * @export
 * @param {Editor} editor
 * @param {boolean} isApply
 */
export function addLine(editor: Editor, isApply: boolean) {
  addShape(editor, 'line', isApply)
}

/**
 * shape中的边框、线条、颜色
 * @export
 * @param {Editor} editor
 * @param {number} dashStyle   边框线的类型   0---11  6是实心线
 * @param {number} borderWidth  边框线的宽度  毫米mm
 * @param {string} borderColor  边框线的颜色
 *       eg： setShapeborderProp(4.0583, 6, 'FF0000')
 */
export function setShapeborderProp(
  editor: Editor,
  borderWidth: number,
  dashStyle: PresetLineDashStyleValues,
  borderColor: string
) {
  ;(editor as unknown as IEditorKit).setShapeborderProp(
    borderWidth,
    dashStyle,
    borderColor
  )
}

const AlignShapeType = {
  ALIGN_LEFT: 0,
  ALIGN_RIGHT: 1,
  ALIGN_TOP: 2,
  ALIGN_BOTTOM: 3,
  ALIGN_CENTER: 4,
  ALIGN_MIDDLE: 5,
}

/**
 * 设置形状对齐
 * @export
 * @param {Editor} editor
 * @param {number} type left: 0 right: 1 top: 2 bottom: 3 center: 4 middle: 5
 * @param {number} alignType selected: 0 slide: 1
 */
export function setShapesAlign(
  editor: Editor,
  type: number,
  alignType?: ObjectsAlignTypeValues
) {
  const editorKit = editor as unknown as IEditorKit
  if (!editorKit.canEdit()) return
  const formatedType = formatAlignType(editorKit, alignType)
  const handler = editorKit.editorUI.editorHandler
  switch (type) {
    case AlignShapeType.ALIGN_LEFT: {
      alignByDirection(handler, formatedType, { horizental: 'left' })
      break
    }
    case AlignShapeType.ALIGN_RIGHT: {
      alignByDirection(handler, formatedType, { horizental: 'right' })
      break
    }
    case AlignShapeType.ALIGN_TOP: {
      alignByDirection(handler, formatedType, { vertical: 'top' })
      break
    }
    case AlignShapeType.ALIGN_BOTTOM: {
      alignByDirection(handler, formatedType, { vertical: 'bottom' })
      break
    }
    case AlignShapeType.ALIGN_CENTER: {
      alignByDirection(handler, formatedType, { horizental: 'center' })
      break
    }
    case AlignShapeType.ALIGN_MIDDLE: {
      alignByDirection(handler, formatedType, { vertical: 'center' })
      break
    }
    default:
      break
  }
}

function formatAlignType(
  editorKit: IEditorKit,
  alignType?: ObjectsAlignTypeValues
) {
  let alignTypeFixed =
    alignType != null
      ? alignType
      : editorKit.getSelectedSlideElementsCount() < 2
      ? ObjectsAlignType.Slide
      : ObjectsAlignType.Selected
  if (!isNumber(alignTypeFixed)) {
    alignTypeFixed = ObjectsAlignType.Slide
  }
  return alignTypeFixed
}

/**
 * 形状水平分布
 * @export
 * @param {Editor} editor
 */
export function distributeHorizontally(
  editor: Editor,
  alignType?: ObjectsAlignTypeValues
) {
  const editorKit = editor as unknown as IEditorKit
  if (!editorKit.canEdit()) return
  const formatedType = formatAlignType(editorKit, alignType)
  const handler = editorKit.editorUI.editorHandler
  distributeHorizental(handler, formatedType)
}

/**
 * 形状垂直分布
 * @export
 * @param {Editor} editor
 */
export function distributeVertically(
  editor: Editor,
  alignType?: ObjectsAlignTypeValues
) {
  const editorKit = editor as unknown as IEditorKit
  if (!editorKit.canEdit()) return
  const formatedType = formatAlignType(editorKit, alignType)
  const handler = editorKit.editorUI.editorHandler
  distributeVertical(handler, formatedType)
}

// shape arrange
export function bringToFront(editor: Editor) {
  ;(editor as unknown as IEditorKit).bringToFront()
}

export function bringForward(editor: Editor) {
  ;(editor as unknown as IEditorKit).bringForward()
}

export function sendToBack(editor: Editor) {
  ;(editor as unknown as IEditorKit).bringToBack()
}

export function sendBackward(editor: Editor) {
  ;(editor as unknown as IEditorKit).bringBackward()
}

// shape group
export function groupShapes(editor: Editor) {
  ;(editor as unknown as IEditorKit).groupShapes()
}

export function unGroupShapes(editor: Editor) {
  ;(editor as unknown as IEditorKit).unGroupShapes()
}

export function canGroup(editor: Editor): boolean {
  return (editor as unknown as IEditorKit).canGroup()
}

export function canUnGroup(editor: Editor): boolean {
  return (editor as unknown as IEditorKit).canUnGroup()
}

// shape rotate
export function rotate(editor: Editor, angle: number) {
  ;(editor as unknown as IEditorKit).rotate(angle)
}

// shape flip
export function flipHorizontal(editor: Editor) {
  ;(editor as unknown as IEditorKit).flipHorizontal()
}

export function flipVertical(editor: Editor) {
  ;(editor as unknown as IEditorKit).flipVertical()
}

// shape stroke
export function setShapeStroke(
  editor: Editor,
  size: number,
  color: string,
  type: number
) {
  ;(editor as unknown as IEditorKit).setShapeStroke(
    size,
    color,
    type as PresetLineDashStyleValues
  )
}

export function setShapeStrokeSize(editor: Editor, size: number) {
  ;(editor as unknown as IEditorKit).setShapeStrokeSize(size)
}

export function setShapeStrokeColor(
  editor: Editor,
  color: string,
  transparency?: number
) {
  ;(editor as unknown as IEditorKit).setShapeStrokeColor(color, transparency)
}

export function setShapeStrokeDash(
  editor: Editor,
  type: PresetLineDashStyleValues
) {
  ;(editor as unknown as IEditorKit).setShapeStrokeDash(type)
}

// shape fill
export function setShapeFillNoFill(editor: Editor) {
  ;(editor as unknown as IEditorKit).setShapeFillNoFill()
}

export function setShapeFillTransparency(editor: Editor, transparency: number) {
  ;(editor as unknown as IEditorKit).setShapeFillTransparency(transparency)
}

export function setShapeFillSolid(
  editor: Editor,
  color: string,
  transparency?: number
) {
  ;(editor as unknown as IEditorKit).setShapeFillSolid(color, transparency)
}

/** @param gradientStops 至少两个节点, pos: [0 - 100]*/
export function setShapeFillLinearGradient(
  editor: Editor,
  angle: number,
  gradientStops: GradientStop[]
) {
  if (gradientStops.length < 2) return
  const gradFill = new FillGrad()
  gradFill.setGsLst(formatGsLst(gradientStops))
  gradFill.setLinearAngle(formatLinearAngle(angle))
  gradFill.setLinearScale(true)
  gradFill.setGradType(FillGradType.LINEAR)
  const fill: ShapeFillOptions = {
    type: FillKIND.GRAD,
    fill: gradFill,
  }
  const shapeProp: SlideElementOptions = {
    fill,
  }
  ;(editor as unknown as IEditorKit).applyPropsForSp(shapeProp)
}

export function setShapeFillRadialGradient(
  editor: Editor,
  direction: GradientDirections,
  gradientStops: GradientStop[]
) {
  if (gradientStops.length < 2) return
  const gradFill = new FillGrad()
  gradFill.setGsLst(formatGsLst(gradientStops))
  gradFill.setGradType(FillGradType.PATH)
  gradFill.setPathType(FillGradPathType.Circle)
  gradFill.setPathDirection(direction ?? GradientDirections.FromCenter)
  const fill: ShapeFillOptions = {
    type: FillKIND.GRAD,
    fill: gradFill,
  }
  ;(editor as unknown as IEditorKit).applyPropsForSp({ fill })
}

export function addVideo(
  editor: Editor,
  coverImageUrl: string,
  videoUrl: UploadUrlResult,
  options?: {
    slideRefId?: string
    offset?: Offset
    size?: Size
    name?: string
  }
) {
  return (editor as unknown as IEditorKit).addImages([{ url: coverImageUrl }], {
    ...options,
    mediaInfo: {
      type: MediaType.video,
      media: videoUrl.url,
      encryptUrl: videoUrl.encryptUrl,
    },
  })
}

/** 考虑带上 name? */
export function addAudio(
  editor: Editor,
  coverImageUrl: string,
  audioUrl: UploadUrlResult,
  options?: {
    slideRefId?: string
    offset?: Offset
    size?: Size
    name?: string
  }
) {
  return (editor as unknown as IEditorKit).addImages([{ url: coverImageUrl }], {
    ...options,
    mediaInfo: {
      type: MediaType.audio,
      media: audioUrl.url,
      encryptUrl: audioUrl.encryptUrl,
    },
  })
}

/** 以 OleObject 形式上传附件 */
export function addAttachment(
  editor: Editor,
  coverImageUrl: string,
  fileInfo: UploadUrlResult & {
    /** suffix */
    fileType?: string
    name?: string
  },
  options?: {
    slideRefId?: string
    offset?: Offset
    size?: Size
  }
) {
  const url = fileInfo['url']
  const encryptUrl = fileInfo['encryptUrl']
  const fileType = fileInfo['fileType']
  const name = fileInfo['name']

  return (editor as unknown as IEditorKit).addOleObject(
    [{ url: coverImageUrl }],
    {
      slideRefId: options?.['slideRefId'],
      offset: options?.['offset'],
      size: options?.['size']
        ? {
            width: options['size']['width'],
            height: options['size']['height'],
          }
        : undefined,
      fileInfo: { url, encryptUrl, fileType, name },
    }
  )
}

/** UI 暂未使用该接口 */
export function getSelectedCharts(editor: Editor) {
  return (editor as unknown as IEditorKit).getSelectedCharts()
}

export function getSelectedImagesInfo(editor: Editor) {
  return (editor as unknown as IEditorKit).getSelectedImagesInfo()
}

export function addChartImage(
  editor: Editor,
  url: string,
  link: string,
  slideRefId?: string,
  size?: {
    width: number
    height: number
  }
) {
  if (url) {
    ;(editor as unknown as IEditorKit).addImages([{ url }], {
      link,
      slideRefId,
      size,
      subType: ImageSubTypes.Chart,
    })
  }
}

export function addAdvancedImage(
  editor: Editor,
  url: UploadUrlResult,
  options?: {
    slideRefId?: string
    offset?: Offset
    size?: Size
    link?: string
    subType?: string
  }
) {
  return (editor as unknown as IEditorKit).addImages([url], options)
}

export function updateImage(
  editor: Editor,
  id: string,
  options: {
    url?: UploadUrlResult
    link?: string
    subType?: string
  }
) {
  ;(editor as unknown as IEditorKit).updateImage(id, options)
}

export function addImages(
  editor: Editor,
  urls: Array<UploadUrlResult>,
  slideRefId?: string,
  size?: {
    width: number
    height: number
  }
) {
  if (urls && urls.length) {
    return (editor as unknown as IEditorKit).addImages(urls, {
      slideRefId,
      size,
    })
  }
}

export function setImageTransparency(editor: Editor, val: number) {
  ;(editor as unknown as IEditorKit).setBlipFillTransparency(val)
}

export function getSelectedSlideElementsCount(editor: Editor) {
  return (editor as unknown as IEditorKit).getSelectedSlideElementsCount()
}

export function getMediaByRefId(
  editor: Editor,
  slideRefId: string,
  shapeRefId: string
): MediaInfo {
  return (editor as unknown as IEditorKit).getMediaByRefId(
    slideRefId,
    shapeRefId
  )
}

export function startImageCrop(editor: Editor) {
  const editorKit = editor as unknown as IEditorKit
  const handler = editorKit.editorUI.editorHandler
  startCrop(handler)
}

export function endImageCrop(editor: Editor) {
  const editorKit = editor as unknown as IEditorKit
  const handler = editorKit.editorUI.editorHandler
  endCrop(handler)
}

export function cropImageByShapeType(
  editor: Editor,
  shpaeType: PresetShapeTypes,
  ids?: Array<string>
) {
  const editorKit = editor as unknown as IEditorKit
  const handler = editorKit.editorUI.editorHandler
  let imgs: ImageShape[] = []

  if (ids && ids.length) {
    for (const id of ids) {
      const img = EditorInstance.entityRegistry.getEntityById<ImageShape>(id)
      if (img?.instanceType === InstanceType.ImageShape) {
        imgs.push(img)
      }
    }
  } else {
    const selectObjects = handler.getSelectedSlideElements()
    imgs = selectObjects.filter(
      (sp) => sp.instanceType === InstanceType.ImageShape
    ) as ImageShape[]
  }
  if (imgs.length) {
    startEditAction(EditActionFlag.action_Presentation_Crop)
    for (const img of imgs) {
      cropByGeometry(img, shpaeType)
    }
    editorKit.editorUI.triggerRender()
  }
}

export function cropImageByRatio(editor: Editor, ratio: number) {
  const editorKit = editor as unknown as IEditorKit
  const handler = editorKit.editorUI.editorHandler
  startEditAction(EditActionFlag.action_Presentation_Crop)
  cropByRatio(handler, ratio)
}

export function cropImageBySize(
  editor: Editor,
  size: { width: number; height: number }
) {
  const editorKit = editor as unknown as IEditorKit
  const handler = editorKit.editorUI.editorHandler
  startEditAction(EditActionFlag.action_Presentation_Crop)

  cropBySize(handler, { w: size.width, h: size.height })
}

export function canCrop(editor: Editor) {
  const editorKit = editor as unknown as IEditorKit
  const handler = editorKit.editorUI.editorHandler
  return !!getCropImage(handler)
}

export function cropImageFit(editor: Editor) {
  const editorKit = editor as unknown as IEditorKit
  const handler = editorKit.editorUI.editorHandler
  startEditAction(EditActionFlag.action_Presentation_Crop)
  cropByOption(handler, 'fit')
}

export function cropImageFill(editor: Editor) {
  const editorKit = editor as unknown as IEditorKit
  const handler = editorKit.editorUI.editorHandler
  startEditAction(EditActionFlag.action_Presentation_Crop)
  cropByOption(handler, 'fill')
}

export function resetImageCrop(editor: Editor) {
  const editorKit = editor as unknown as IEditorKit
  const handler = editorKit.editorUI.editorHandler
  startEditAction(EditActionFlag.action_Presentation_Crop)
  resetSrcRect(handler)
}
