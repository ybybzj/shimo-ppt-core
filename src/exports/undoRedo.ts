import { Editor } from './type'
import { IEditorKit } from '../editor/type'

export function canUndo(editor: Editor): boolean {
  return (editor as unknown as IEditorKit).canUndo()
}
export function canRedo(editor: Editor): boolean {
  return (editor as unknown as IEditorKit).canRedo()
}
export function undo(editor: Editor) {
  ;(editor as unknown as IEditorKit).undo()
}
export function redo(editor: Editor) {
  ;(editor as unknown as IEditorKit).redo()
}

export function resetUndoRedo(editor: Editor) {
  ;(editor as unknown as IEditorKit).resetUndoRedo()
}
