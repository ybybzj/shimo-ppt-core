import { IEditorKit } from '../editor/type'
import { Editor } from './type'

export function isFocusOnNotes(editor: Editor): boolean {
  return (editor as unknown as IEditorKit).presentation.isNotesFocused
}

export function isFocusOnTableCell(editor: Editor): boolean {
  return (editor as unknown as IEditorKit).isFocusOnTableCell()
}

export function isFocusOnText(editor: Editor): boolean {
  return (editor as unknown as IEditorKit).isFocusOnText()
}

export function isLoopShow(editor: Editor): boolean {
  const presentation = (editor as unknown as IEditorKit).presentation
  return presentation.isLoopShow()
}

export function setLoopShow(editor: Editor, isLoop: boolean) {
  ;(editor as unknown as IEditorKit).setLoopShow(isLoop)
}

export function isUseTiming(editor: Editor): boolean {
  const presentation = (editor as unknown as IEditorKit).presentation
  return presentation.isUseTiming()
}

export function setUseTiming(editor: Editor, isUseTiming: boolean) {
  const presentation = (editor as unknown as IEditorKit).presentation
  presentation.setUseTiming(isUseTiming)
}

export function isEmpty(editor: Editor): boolean {
  const presentation = (editor as unknown as IEditorKit).presentation
  return presentation.isEmpty()
}

export function getLayoutRefs(editor) {
  const presentation = (editor as unknown as IEditorKit).presentation
  return presentation.getLayoutRefs()
}