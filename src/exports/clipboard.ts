import { gClipboard } from '../copyPaste/gClipboard'
import { EditorSettings } from '../core/common/EditorSettings'
import { IEditorKit } from '../editor/type'
import { Editor } from './type'

export function canCopy(editor: Editor): boolean {
  const editorKit = editor as unknown as IEditorKit
  if (editorKit.getViewMode() && !EditorSettings.isViewModeCopyable) {
    return false
  }
  return !!editorKit.presentation.canCopyCut()
}

export function canCut(editor: Editor): boolean {
  const editorKit = editor as unknown as IEditorKit
  if (editorKit.getViewMode()) {
    return false
  }
  return !!editorKit.presentation.canCopyCut()
}

export function canPaste(editor: Editor): boolean {
  const editorKit = editor as unknown as IEditorKit
  if (!editorKit.editorUI || !editorKit.presentation) {
    return false
  }
  return true
}

export function copy(_editor: Editor) {
  return gClipboard.handleApiCopy()
}

export function cut(_editor: Editor) {
  return gClipboard.handleApiCut()
}

export function paste(editor: Editor) {
  const editorKit = editor as unknown as IEditorKit
  if (!editorKit.presentation) return false
  if (gClipboard.checkIsProcessing()) return false
  return gClipboard.handleApiPaste()
}

export function specialPaste(editor: Editor, type: number, isRevert?: boolean) {
  ;(editor as unknown as IEditorKit).specialPaste(type, isRevert)
}

export function querySpecialPasteOptions(editor: Editor) {
  return (editor as unknown as IEditorKit).querySpecialPasteOptions()
}



export function copySelectedData(editor: Editor, data: DataTransfer) {
  const editorKit = editor as unknown as IEditorKit
  if (!editorKit.presentation) return false
  if (gClipboard.checkIsProcessing()) return false
  gClipboard.updataDragData(data)
}


