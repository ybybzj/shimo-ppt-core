import { Editor } from './type'
import { IEditorKit } from '../editor/type'
import { CellBordersOptions } from '../editor/proto'

/**
 * 插入表格
 * @export
 * @param {Editor} editor
 * @param {number} col
 * @param {number} row
 */
export function addTable(editor: Editor, col: number, row: number): void {
  ;(editor as unknown as IEditorKit).addTable(col, row)
}

export function removeTable(editor: Editor): void {
  ;(editor as unknown as IEditorKit).removeTable()
}

export function addRowAbove(editor: Editor, count: number): void {
  ;(editor as unknown as IEditorKit).addRowAbove(count)
}

export function addRowBelow(editor: Editor, count: number): void {
  ;(editor as unknown as IEditorKit).addRowBelow(count)
}
export function addColumnLeft(editor: Editor, count: number): void {
  ;(editor as unknown as IEditorKit).addColumnLeft(count)
}
export function addColumnRight(editor: Editor, count: number): void {
  ;(editor as unknown as IEditorKit).addColumnRight(count)
}
export function removeRow(editor: Editor): void {
  ;(editor as unknown as IEditorKit).removeRow()
}

export function removeColumn(editor: Editor): void {
  ;(editor as unknown as IEditorKit).removeColumn()
}

export function canMergeTableCells(editor: Editor): boolean {
  return (editor as unknown as IEditorKit).canMergeTableCells()
}

export function canSplitTableCells(editor: Editor): boolean {
  return (editor as unknown as IEditorKit).canSplitTableCells()
}

export function mergeCells(editor: Editor): void {
  ;(editor as unknown as IEditorKit).mergeCells()
}

export function splitCell(editor: Editor, cols: number, rows: number): void {
  ;(editor as unknown as IEditorKit).splitCell(cols, rows)
}

export function setCellsBackground(
  editor: Editor,
  color: string,
  transparent?: number
) {
  ;(editor as unknown as IEditorKit).setCellsBackground(color, transparent)
}

export function clearCellsBackground(editor: Editor) {
  ;(editor as unknown as IEditorKit).clearCellsBackground()
}

/**
 * type：各种方向边框组合, 比如设置左边和右边 'lt', 设置所有边框 'ltbrhv'
 * 边框方向 左：l 右：r 上：t 下：b 垂直：v 水平：h
 */
export function setCellBorders(
  editor: Editor,
  type: string,
  option: CellBordersOptions
) {
  ;(editor as unknown as IEditorKit).setCellBorders(type, option)
}

export function setTableDirection(editor: Editor, isRTL = false) {
  ;(editor as unknown as IEditorKit).setTableDirection(isRTL)
}
