import { AnimationSetting, Editor, ImageLikeTypes } from './type'
import { IEditorKit } from '../editor/type'
import {
  ShowState,
  ShowStartOption,
  ShowManager,
} from '../ui/transitions/ShowManager'
import { ShapeAnimationNodeType } from '../re/export/ShapeAnimation'
import { TimeNode, writeTimeNodeFromSetting } from '../core/Slide/Animation'
import {
  Evt_onAnimationStart,
  Evt_onMediaPlay,
  Evt_viewAttachment,
  Evt_viewSourceImage,
} from '../editor/events/EventNames'
import { getPointerEventPos } from '../common/dom/events'
import { cancelEmitMenu, isInShowMode } from '../ui/editorUI/utils/utils'
import { isAudioImage } from '../core/utilities/shape/image'
import { genAudioAction } from '../ui/transitions/AudioManager'
import { EditorUtil } from '../globals/editor'
import { StatusOfLoadableImage } from '../images/utils'
import { getMediaFromShape } from '../core/utilities/shape/getters'
import { BrowserInfo } from '../common/browserInfo'
import { getSpPosInShow } from '../ui/transitions/utils'

export function isInPPTShowMode(editor: Editor): boolean{
  const editorUI = (editor as unknown as IEditorKit).editorUI
  return isInShowMode(editorUI)
}
/**
 * 开始演示
 * @export
 * @param {Editor} editor
 * @param {string} containerDomId // 容器div的id
 * @param {number} startSlideIndex // 演示幻灯片起始页
 */
export function startPPTShow(
  editor: Editor,
  containerDomId: string,
  startSlideIndex: number,
  options?: ShowStartOption
) {
  ;(editor as unknown as IEditorKit).startPPTShow(
    containerDomId,
    startSlideIndex,
    options
  )
}

export function endPPTShow(editor: Editor) {
  ;(editor as unknown as IEditorKit).endPPTShow()
}

export function enableEventsForPPTShow(
  editor: Editor,
  enabled: boolean,
  keyCode?: number
) {
  ;(editor as unknown as IEditorKit).enableEventsInShowMode(enabled, keyCode)
}

export function playPPTShow(editor: Editor) {
  ;(editor as unknown as IEditorKit).playPPTShow()
}

export function pausePPTShow(editor: Editor) {
  ;(editor as unknown as IEditorKit).pausePPTShow()
}

export function getCurSlideIndexInPPTShow(editor: Editor) {
  return (editor as unknown as IEditorKit).getCurSlideIndexInPPTShow()
}

export function goToNextSlideInPPTShow(
  editor: Editor,
  isStopTransition?: boolean
) {
  ;(editor as unknown as IEditorKit).goToNextSlideInPPTShow(isStopTransition)
}
export function goToPrevSlideInPPTShow(
  editor: Editor,
  isStopTransition?: boolean
) {
  ;(editor as unknown as IEditorKit).goToPrevSlideInPPTShow(isStopTransition)
}

// Deprecated， Todo delete
export function goToSlideInPPTShow(editor: Editor, slideIndex: number) {
  const editorKit = editor as unknown as IEditorKit
  editorKit.editorUI.showManager.goToSlide(slideIndex)
}

/** 供远程演示客户端同步播放进度用, 同步后的 action 由 ui 触发 */
export function syncAnimationStateForPPTShow(editor: Editor, param: ShowState) {
  const editorKit = editor as unknown as IEditorKit
  editorKit.editorUI.showManager.syncAnimStateForShow(param)
}

export function setUseEndSlideInPPTShow(editor: Editor, val: boolean) {
  ;(editor as unknown as IEditorKit).setUseEndSlideInPPTShow(val)
}

export function setTextOfEndingSlideForPPTShow(editor: Editor, val: string) {
  ;(editor as unknown as IEditorKit).setTextOfEndingSlideForPPTShow(val)
}

export function setEndPPTShowOnlyByEsc(editor: Editor, val: boolean) {
  ;(editor as unknown as IEditorKit).setEndPPTShowOnlyByEsc(val)
}

export function startPreviewAnimation(
  editor: Editor,
  animationSettings: AnimationSetting[]
) {
  const editorKit = editor as unknown as IEditorKit
  const timeNodes: TimeNode[] = []
  for (const setting of animationSettings) {
    const timeNode = writeTimeNodeFromSetting(setting)
    if (timeNode) timeNodes.push(timeNode)
  }
  if (timeNodes.length) {
    editorKit.editorUI.animationPreviewer.start(timeNodes)
    editorKit.emitEvent(Evt_onAnimationStart)
  }
}

export function endPreviewAnimation(editor: Editor) {
  const editorKit = editor as unknown as IEditorKit
  if (editorKit.editorUI.animationPreviewer.isPlaying()) {
    editorKit.editorUI.animationPreviewer.end('StopAtEnd')
  }
}

export function groupAnimItemByEffect<T>(
  animArr: T[],
  getEffectCallback: (item: T) => ShapeAnimationNodeType
): T[][] {
  const animGroup: T[][] = []
  let animList: T[] = []
  for (const item of animArr) {
    if (getEffectCallback(item) !== 'ClickEffect') {
      animList.push(item)
    } else {
      if (!animGroup.length) {
        if (animList.length) {
          animGroup.push(animList)
        }
        animList = [item]
      } else {
        animGroup.push(animList)
        animList = [item]
      }
    }
  }
  if (animList.length) animGroup.push(animList)
  return animGroup
}

export function isPPTShowInPlayMode(editor: Editor): boolean {
  return (editor as unknown as IEditorKit).editorUI.showManager.isInPlayMode()
}

export function isPPTShowForceAutoPlay(editor: Editor): boolean {
  return (
    editor as unknown as IEditorKit
  ).editorUI.showManager.isForceAutoPlay()
}

export function toggleForceAutoPlayForPPTShow(editor: Editor): void {
  ;(editor as unknown as IEditorKit).editorUI.showManager.toggleForceAutoPlay()
}

export function handleDoubleTapForPPTShow(editor: Editor, e: PointerEvent) {
  const XY = getPointerEventPos(e)
  if (XY == null) return
  const editorKit = editor as unknown as IEditorKit
  const [touchX, touchY] = XY
  const editorUI = editorKit.editorUI
  const pos = getMousePosInPPTShow(editorUI.showManager, touchX, touchY)
  const { x, y } = pos
  const info = editorKit.getHittestInfoByMousePosition(x, y, false)
  const imgInfo = info?.['image']
  if (imgInfo) {
    switch (imgInfo['type']) {
      case ImageLikeTypes.attachment:
        editorUI.editorKit.emitEvent(Evt_viewAttachment, imgInfo)
        break
      case ImageLikeTypes.audio:
        const curSlide = editorUI.editorHandler.getCurrentSlide()
        const sp = curSlide
          .getSpTree()
          .find((sp) => sp.refId === imgInfo['refId'])
        if (sp && isAudioImage(sp)) {
          const audioAction = genAudioAction(curSlide, sp, 'toggle', true)
          if (audioAction) {
            const pos = getSpPosInShow(editorUI.showManager, curSlide, sp)
            editorKit.emitAudioAction(audioAction, pos)
            cancelEmitMenu()
          }
        }
        break
      case ImageLikeTypes.video: {
        const curSlide = editorUI.editorHandler.getCurrentSlide()
        const sp = curSlide
          .getSpTree()
          .find((sp) => sp.refId === imgInfo['refId'])
        if (sp) {
          const media = getMediaFromShape(sp)
          editorKit.emitEvent(Evt_onMediaPlay, media)
        }
        break
      }
      case ImageLikeTypes.image:
      default:
        const status =
          EditorUtil.imageLoader.imagesMap[imgInfo['imgInfo']['url']]?.Status
        if (status !== StatusOfLoadableImage.Error) {
          editorUI.editorKit.emitEvent(Evt_viewSourceImage, imgInfo)
        }
        break
    }
    return
  }
}

function getMousePosInPPTShow(
  showManager: ShowManager,
  px: number,
  py: number
): { x: number; y: number } {
  const transition = showManager.transitionManager
  const { left, top } = showManager.canvas!.getBoundingClientRect()
  let { x, y, w, h } = transition.rect
  x = x / BrowserInfo.PixelRatio + left
  y = y / BrowserInfo.PixelRatio + top
  w /= BrowserInfo.PixelRatio
  h /= BrowserInfo.PixelRatio
  const mmW = showManager.presentation.width
  const mmH = showManager.presentation.height

  let _x = px - x
  let _y = py - y

  _x = (_x * mmW) / w
  _y = (_y * mmH) / h

  return { x: _x, y: _y }
}
