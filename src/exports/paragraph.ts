import { Editor } from './type'
import { IEditorKit } from '../editor/type'
import { AlignKindValue } from '../core/common/const/attrs'

export function setTextDirection(editor: Editor, isRTL = false) {
  ;(editor as unknown as IEditorKit).setTextDirection(isRTL)
}

/**
 * 设置对齐
 * @export
 * @param {Editor} editor
 * @param {number} value
 * Right: 0 Left: 1 Center: 2 Justify: 3 Dist: 4
 */
export function setAlign(editor: Editor, align: AlignKindValue): void {
  ;(editor as unknown as IEditorKit).setAlign(align)
}

/**
 * 设置垂直对齐
 * @export
 * @param {Editor} editor
 * @param {number} value
 * VAlign Bottom: 0 Center: 1 Distributed: 2 Justified: 3 Top: 4
 */
export function setVerticalAlign(editor: Editor, align: number): void {
  ;(editor as unknown as IEditorKit).setVerticalAlign(align)
}

/**
 * 增加缩进
 * @export
 * @param {Editor} editor
 */
export function increaseIndent(editor: Editor): void {
  ;(editor as unknown as IEditorKit).increaseIndent()
}

/**
 * 减少缩进
 * @export
 * @param {Editor} editor
 */
export function decreaseIndent(editor: Editor): void {
  ;(editor as unknown as IEditorKit).decreaseIndent()
}

export function setLineSpacing(
  editor: Editor,
  type: number,
  value: number
): void {
  ;(editor as unknown as IEditorKit).setPrLineSpacing(type, value)
}

export function setLineSpacingBeforeAfter(
  editor: Editor,
  type: number,
  value: number
): void {
  ;(editor as unknown as IEditorKit).setLineSpacingBeforeAfter(type, value)
}

/**
 * 设置列表
 * @export
 * @param {Editor} editor
 * @param {number} type // list: 0 numbering: 1
 * @param {number} subType
 *  Bulleted List Type = 0
      none         - SubType = -1
      black dot - SubType = 1
      circle         - SubType = 2
      square      - SubType = 3
      picture     - SubType = -1
      4 diamonds      - SubType = 4
      arrow  - SubType = 5
      check        - SubType = 6
      diamond        - SubType = 7
      dash        - SubType = 7

    Type numbered list = 1
      none - SubType = -1
      1.  - SubType = 1
      1)  - SubType = 2
      I.  - SubType = 3
      A.  - SubType = 4
      a)  - SubType = 5
      a.  - SubType = 6
      i.  - SubType = 7

    Layered Type List = 2
      none            - SubType = -1
      1)a)i)        - SubType = 1
      1.1.1         - SubType = 2
      marked - SubType = 3
 */
export function setTextListType(
  editor: Editor,
  type: number,
  subType: number
): void {
  ;(editor as unknown as IEditorKit).setNumberingType(type, subType)
}

export function addText(
  editor: Editor,
  text: string,
  options?: {
    font?: string
  }
): void {
  ;(editor as unknown as IEditorKit).addText(text, options)
}
