import { gClipboard } from '../copyPaste/gClipboard'
import { IEditorKit } from '../editor/type'
import { Editor } from './type'

export function startDragOut(editor: Editor) {
  ;(editor as unknown as IEditorKit).editorUI.editorHandler.startDragOut()
}

/** 可以考虑 option 键触发 */
export function endDragOut(editor: Editor) {
  ;(editor as unknown as IEditorKit).editorUI.editorHandler.endDragOut()
}

export function pasteDragData(editor: Editor, data: DataTransfer) {
  const editorKit = editor as unknown as IEditorKit
  if (!editorKit.presentation) return false
  if (gClipboard.checkIsProcessing()) return false
  return gClipboard.handleApiPasteDragData(data)
}

export function isDragging(editor: Editor) {
  return (editor as unknown as IEditorKit).editorUI.editorHandler.isDragging()
}
