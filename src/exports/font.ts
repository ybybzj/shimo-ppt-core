import { EditorUtil } from '../globals/editor'
import { FontLoadEvents } from '../../src/fonts/FontLoader'
import { EditorKit } from '../editor/global'
import { refreshPresentation } from '../editor/utils/editor'
import { invalidAllMastersThumbnails } from '../ui/editorUI/utils/utils'

export interface FontInfo {
  isLoading: boolean
  isLocal: boolean
}

export function getFontInfo(font: string): FontInfo | undefined {
  const result = EditorUtil.FontKit.getFontFile(font)
  return result
    ? {
        ['isLoading']: result.isLoading,
        ['isLocal']: result.isLocal,
      }
    : undefined
}

export function loadFont(font: string) {
  return EditorUtil.FontLoader.loadFont(font).then(() => {
    refreshPresentation(EditorKit, true)
    invalidAllMastersThumbnails(EditorKit.editorUI)
  })
}

export function onFontLoadEvents(evtName: FontLoadEvents, cb: any) {
  EditorUtil.FontLoader.on(evtName, cb)
}

export function offFontLoadEvents(evtName: FontLoadEvents, cb?: any) {
  EditorUtil.FontLoader.off(evtName, cb)
}

export function setDefaultFontFamily(font: string) {
  EditorUtil.FontKit.setDefaultFontFamily(font)
}

export { FontLoadEvents }
