import { IEditorKit, UploadUrlResult } from '../editor/type'
import {
  FillKIND,
  FillGradPathType,
  FillGradType,
} from '../core/common/const/attrs'

import {
  SlideTransition,
  TransitionOptions,
  TransitionTypes,
} from '../core/Slide/SlideTransition'
import {
  AnimationSetting,
  Editor,
  GradientStop,
  LayoutId,
  GradientDirections,
  SlidePreviewImageInfo,
  SlideTransitionSettings,
  SlideTransitionProps,
  AudioSetting,
} from './type'
import { ApplyType, ThemeInfo } from '../editor/proto/theme'
import {
  SlideSizeChangeInfo,
  SlideSizeComparisonInfo,
} from '../re/export/Slide'
import {
  updatePresentationSize,
  getResizeOptionsBySize,
} from '../core/utilities/presentation/size'
import {
  EmphasisAnimationTypes,
  EntranceAnimationTypes,
  ExitAnimationTypes,
  CustomAnimTypes,
} from '../io/dataType/animation'
import { assignVal } from '../io/utils'
import {
  getShapeContentText,
  getShapeName,
  getShapeType,
} from '../core/utilities/shape/getters'
import { InstanceType } from '../core/instanceTypes'
import { EditorUtil as EditorInstance } from '../globals/editor'
import { SlideElement } from '../core/SlideElement/type'
import { PresetShapeTypes } from './graphic'
import { Nullable, Optional } from '../../liber/pervasive'
import {
  AudioNode,
  canAddAnimation,
  TimeNode,
  timeNodesToAnimationSettings,
  writeAudioNodeFromSetting,
  writeTimeNodeFromSetting,
} from '../core/Slide/Animation'
import { EditActionFlag } from '../core/EditActionFlag'
import { isChartImage } from '../core/utilities/shape/asserts'
import { Slide } from '../core/Slide/Slide'
import { isSlideHasAnimation } from '../core/utilities/slide'
import { isNumber } from '../common/utils'
import { isAudioImage, isVideoImage } from '../core/utilities/shape/image'
import { translator } from '../globals/translator'
import { formatGsLst, formatLinearAngle } from '../core/graphic/utils'
import { startEditAction } from '../core/calculation/informChanges/startAction'
import { getTransitionProps } from '../editor/interfaceState/common'
import { FillGrad } from '../core/properties/props'

export function isSlideAvailable(editor: Editor, slideRefId: string): boolean {
  return !!(editor as unknown as IEditorKit).getSlideByRefId(slideRefId)
}

/**
 *
 * 添加幻灯片
 * @export
 * @param {Editor} editor
 * @param {number} layoutIndex
 */
export function addSlide(editor: Editor, layoutId?: LayoutId) {
  ;(editor as unknown as IEditorKit).addSlide(layoutId)
}

/**
 * 删除幻灯片
 * @export
 * @param {Editor} editor
 */
export function deleteSlide(editor: Editor) {
  const editorKit = editor as unknown as IEditorKit
  const { presentation } = editorKit.editorUI
  const selectedSlideIndices = presentation.getSelectedSlideIndices()

  if (!editorKit.isSupportEmptySlides) {
    if (selectedSlideIndices.length === presentation.slides.length) {
      selectedSlideIndices.splice(0, 1)
    }
  }

  if (selectedSlideIndices.length !== 0) {
    editorKit.editorUI.editorHandler.deleteSlides(selectedSlideIndices)
  }
}

/**
 * 复制幻灯片
 * @export
 * @param {Editor} editor
 */
export function duplicateSlide(editor: Editor) {
  ;(editor as unknown as IEditorKit).duplicateSlide()
}

/**
 * 隐藏幻灯片
 * @export
 * @param {Editor} editor
 */
export function hideSlide(editor: Editor, isHide: boolean) {
  ;(editor as unknown as IEditorKit).hideSlides(isHide)
}

export function isSlideVisible(editor: Editor, index: number): boolean {
  return (editor as unknown as IEditorKit).isSlideVisible(index)
}

export function getCurrentSlideIndex(editor: Editor): number {
  return (editor as unknown as IEditorKit).getCurrentSlideIndex()
}

/**
 * 更改幻灯片布局
 * @export
 * @param {Editor} editor
 * @param {number} layoutIndex
 */
export function changeLayout(editor: Editor, layoutId: LayoutId) {
  ;(editor as unknown as IEditorKit).changeLayout(layoutId)
}

/**
 * 全选
 * @export
 * @param {Editor} editor
 */
export function selectAllSlides(editor: Editor) {
  ;(editor as unknown as IEditorKit).selectAllSlides()
}

/**
 * 设置幻灯片大小
 * @param {Editor} editor
 * @param {SlideSizeComparisonInfo} scaleInfo // 通过 getScaleAbleType 得到的选项值
 */
export function updatePresentationSizeByInfo(
  editor: Editor,
  scaleInfo: SlideSizeChangeInfo
) {
  updatePresentationSize(
    (editor as unknown as IEditorKit).presentation,
    scaleInfo
  )
}

/** 根据宽高计算可选的缩放规则信息 */
export function getResizeOptions(
  editor: Editor,
  width: number,
  height: number
): SlideSizeComparisonInfo {
  return getResizeOptionsBySize(
    (editor as unknown as IEditorKit).presentation,
    width,
    height
  )
}

export function changePresentationSize(
  editor: Editor,
  slideSize: { width: number; height: number },
  scaleType: 'maximize' | 'fit'
) {
  const { width, height } = slideSize
  const info = getResizeOptionsBySize(
    (editor as unknown as IEditorKit).presentation,
    width,
    height
  )
  const presentation = (editor as unknown as IEditorKit).presentation
  switch (info.type) {
    case 'ScaleNoScale':
      updatePresentationSize(
        presentation,
        scaleType === 'maximize'
          ? info.options['noScale']
          : info.options['scale']
      )
      break
    case 'ScaleUpDown':
      updatePresentationSize(
        presentation,
        scaleType === 'maximize'
          ? info.options['scaleUp']
          : info.options['scaleDown']
      )
      break
  }
}

export type SlideMastersInfo = Array<{
  masterId: string
  theme: string
  image: string
  isSelected: boolean
  layouts: Array<{
    index: number
    layoutId: LayoutId
    name: string
    image: string
    isSelected: boolean
  }>
}>

/**
 * 获取当前幻灯片所有的master和layout
 * @export
 * @param {Editor} editor
 */
export function getSlideMasters(editor: Editor): SlideMastersInfo {
  return adaptSlideMastersResult(
    (editor as unknown as IEditorKit).getSlideMasters()
  )
}

function adaptSlideMastersResult(result: SlideMastersInfo): SlideMastersInfo {
  return result.map((slideMaster) => {
    return {
      ['masterId']: slideMaster.masterId,
      ['theme']: translator.getValue(slideMaster.theme),
      ['image']: slideMaster.image,
      ['isSelected']: slideMaster.isSelected,
      ['layouts']: slideMaster.layouts.map((layout) => ({
        ['index']: layout.index,
        ['layoutId']: layout.layoutId,
        ['name']: translator.getValue(layout.name),
        ['image']: layout.image,
        ['isSelected']: layout.isSelected,
      })),
    }
  })
}

/**
 * 获取指定幻灯片的masterId和layoutId
 * @export
 * @param {Editor} editor
 * @param {number} index
 * @returns {{
 *   masterId: string,
 *   layoutId: string
 * }}
 */
export function getSlideLayout(
  editor: Editor,
  index: number
): {
  masterId?: string
  layoutId?: string
} {
  return adaptSlideLayoutInfo(
    (editor as unknown as IEditorKit).getSlideLayout(index)
  )
}

function adaptSlideLayoutInfo(info: { masterId?: string; layoutId?: string }): {
  masterId?: string
  layoutId?: string
} {
  return {
    ['masterId']: info.masterId,
    ['layoutId']: info.layoutId,
  }
}

/** refId */
export function getSlideId(editor: Editor, index: number) {
  return (editor as unknown as IEditorKit).getSlideId(index)
}

// ------背景编辑 -------
/**
 * 设置无填充背景
 * @export
 * @param {Editor} editor
 */
export function setSlideFillNoFill(editor: Editor) {
  ;(editor as unknown as IEditorKit).setSlideFillNoFill()
}

/**
 * 设置颜色填充背景
 * @export
 * @param {Editor} editor
 * @param {string} color   //e.g.  "E7E6E6"  "70AD47"
 */
export function setSlideFillSolid(editor: Editor, color: string) {
  ;(editor as unknown as IEditorKit).setSlideFillSolid(color)
}

/**
 * 图案填充样式
 * @export
 * @param {Editor} editor
 * @param {number} patternFillType  (0 --- 53)
 * @param {string} fgColor  eg: 4472C4（一种蓝色）
 * @param {number} bgColor  eg: FFC000（一种黄色）
 */
export function setSlideFillPattern(
  editor: Editor,
  patternFillType: number,
  fgColor?: string,
  bgColor?: string
) {
  ;(editor as unknown as IEditorKit).setSlideFillPattern(
    patternFillType,
    fgColor,
    bgColor
  )
}

/**
 * 图案填充前景色
 * @export
 * @param {Editor} editor
 * @param {string} color
 */
export function setSlideFillPatternForegroundColor(
  editor: Editor,
  color: string
) {
  ;(editor as unknown as IEditorKit).setSlideFillPatternForegroundColor(color)
}

/**
 * 图案填充背景色
 * @export
 * @param {Editor} editor
 * @param {string} color
 */
export function setSlideFillPatternBackgroundColor(
  editor: Editor,
  color: string
) {
  ;(editor as unknown as IEditorKit).setSlideFillPatternBackgroundColor(color)
}

export function setSlideFillLinearGradient(
  editor: Editor,
  angle: number,
  gradientStops: GradientStop[]
) {
  if (gradientStops.length < 2) return
  const gradFill = new FillGrad()
  gradFill.setGsLst(formatGsLst(gradientStops))
  gradFill.setLinearAngle(formatLinearAngle(angle))
  gradFill.setLinearScale(true)
  gradFill.setGradType(FillGradType.LINEAR)
  const fill = {
    type: FillKIND.GRAD,
    fill: gradFill,
  }
  ;(editor as unknown as IEditorKit).applySlideOptions({ background: fill })
}

export function setSlideFillRadialGradient(
  editor: Editor,
  direction: GradientDirections,
  gradientStops: GradientStop[]
) {
  if (gradientStops.length < 2) return
  const gradFill = new FillGrad()
  gradFill.setGsLst(formatGsLst(gradientStops))
  gradFill.setGradType(FillGradType.PATH)
  gradFill.setPathType(FillGradPathType.Circle)
  gradFill.setPathDirection(direction ?? GradientDirections.FromCenter)
  const fill = {
    type: FillKIND.GRAD,
    fill: gradFill,
  }
  ;(editor as unknown as IEditorKit).applySlideOptions({ background: fill })
}

/**
 * 纹理
 * @export
 * @param {Editor} editor
 * @param {number} textureId    0---10
 * @param {number} blipType     STRETCH --- 1,   TILE --- 2,
 */
export function setSlideFillBlipTexture(
  editor: Editor,
  textureId: number,
  blipType: number
) {
  ;(editor as unknown as IEditorKit).setSlideFillBlipTexture(
    textureId,
    blipType
  )
}

/**
 * 图片
 * @export
 * @param {Editor} editor
 * @param {number} url
 * @param {number} blipType     STRETCH --- 1,   TILE --- 2,
 */
export function setSlideFillBlipImage(
  editor: Editor,
  url: string,
  blipType: number
) {
  ;(editor as unknown as IEditorKit).setSlideFillBlipImage(url, blipType)
}

/**
 * 填充类型
 * @export
 * @param {Editor} editor
 * @param {number} blipType  STRETCH --- 1,   TILE --- 2,
 */
export function setSlideFillBlipType(editor: Editor, blipType: number) {
  ;(editor as unknown as IEditorKit).setSlideFillBlipType(blipType)
}

/**
 * 将背景应用到全部
 * @export
 * @param {Editor} editor
 */
export function applySlideFillToAll(editor: Editor) {
  ;(editor as unknown as IEditorKit).applySlideFillToAll()
}

/**
 * 设置slide的切页动画
 */

// --- 配置项 --->

export const SlideTransitionOptions = {
  ['None']: {
    ['type']: TransitionTypes.None,
  },
  ['Fade']: {
    ['type']: TransitionTypes.Fade,
    ['options']: {
      ['Smoothly']: TransitionOptions.FadeSmoothly,
      ['ThroughBlack']: TransitionOptions.FadeThroughBlack,
    },
  },
  ['Push']: {
    ['type']: TransitionTypes.Push,
    ['options']: {
      ['Right']: TransitionOptions.DirectionRight,
      ['Left']: TransitionOptions.DirectionLeft,
      ['Top']: TransitionOptions.DirectionTop,
      ['Bottom']: TransitionOptions.DirectionBottom,
    },
  },
  ['Wipe']: {
    ['type']: TransitionTypes.Wipe,
    ['options']: {
      ['Right']: TransitionOptions.DirectionRight,
      ['Left']: TransitionOptions.DirectionLeft,
      ['Top']: TransitionOptions.DirectionTop,
      ['Bottom']: TransitionOptions.DirectionBottom,
      ['TopRight']: TransitionOptions.DirectionTopRight,
      ['BottomRight']: TransitionOptions.DirectionBottomRight,
      ['TopLeft']: TransitionOptions.DirectionTopLeft,
      ['BottomLeft']: TransitionOptions.DirectionBottomLeft,
    },
  },
  ['Split']: {
    ['type']: TransitionTypes.Split,
    ['options']: {
      ['VerticalOut']: TransitionOptions.SplitVerticalOut,
      ['VerticalIn']: TransitionOptions.SplitVerticalIn,
      ['HorizontalOut']: TransitionOptions.SplitHorizontalOut,
      ['HorizontalIn']: TransitionOptions.SplitHorizontalIn,
    },
  },
  ['Uncover']: {
    ['type']: TransitionTypes.Uncover,
    ['options']: {
      ['Right']: TransitionOptions.DirectionRight,
      ['Left']: TransitionOptions.DirectionLeft,
      ['Top']: TransitionOptions.DirectionTop,
      ['Bottom']: TransitionOptions.DirectionBottom,
      ['TopRight']: TransitionOptions.DirectionTopRight,
      ['BottomRight']: TransitionOptions.DirectionBottomRight,
      ['TopLeft']: TransitionOptions.DirectionTopLeft,
      ['BottomLeft']: TransitionOptions.DirectionBottomLeft,
    },
  },
  ['Cover']: {
    ['type']: TransitionTypes.Cover,
    ['options']: {
      ['Right']: TransitionOptions.DirectionRight,
      ['Left']: TransitionOptions.DirectionLeft,
      ['Top']: TransitionOptions.DirectionTop,
      ['Bottom']: TransitionOptions.DirectionBottom,
      ['TopRight']: TransitionOptions.DirectionTopRight,
      ['BottomRight']: TransitionOptions.DirectionBottomRight,
      ['TopLeft']: TransitionOptions.DirectionTopLeft,
      ['BottomLeft']: TransitionOptions.DirectionBottomLeft,
    },
  },
  ['Clock']: {
    ['type']: TransitionTypes.Clock,
    ['options']: {
      ['Clockwise']: TransitionOptions.ClockClockwise,
      ['Counterclockwise']: TransitionOptions.ClockCounterclockwise,
      ['Wedge']: TransitionOptions.ClockWedge,
    },
  },
  ['Zoom']: {
    ['type']: TransitionTypes.Zoom,
    ['options']: {
      ['ZoomIn']: TransitionOptions.ZoomIn,
      ['ZoomOut']: TransitionOptions.ZoomOut,
      ['ZoomAndRotate']: TransitionOptions.ZoomAndRotate,
    },
  },
} as const

// <--- 配置项 ---
export function applyTransition(
  editor: Editor,
  settings: SlideTransitionSettings
) {
  // 混淆转换
  const transitionProps: SlideTransitionProps = {
    type: settings['TransitionType'] as any,
    option: settings['TransitionOption'] as any,
    duration: settings['TransitionDuration'],
    advanceOnMouseClick: settings['SlideAdvanceOnMouseClick'],
    advanceAfter: settings['SlideAdvanceAfter'],
    advanceDuration: settings['SlideAdvanceDuration'],
  }

  const transition = new SlideTransition()
  transition.applyProps(transitionProps)
  ;(editor as unknown as IEditorKit).ApplySlideTiming(transition)
}

export function applyCurrentTrasitionToAll(editor: Editor) {
  ;(editor as unknown as IEditorKit).SlideTimingApplyToAll()
}

/** 提供给 ui 的选项， 基于 xml 数据组装过 */
export const SlideAnimationOptions = [
  {
    ['typeName']: 'Appear',
    ['type']: EntranceAnimationTypes['appear']['id'],
    ['animClass']: 'Entrance',
  },
  {
    ['typeName']: 'FadeIn',
    ['type']: EntranceAnimationTypes['fadeIn']['id'],
    ['animClass']: 'Entrance',
  },
  {
    ['typeName']: 'FlyIn',
    ['type']: EntranceAnimationTypes['flyIn']['id'],
    ['animClass']: 'Entrance',
    ['params']: {
      ['type']: 'options',
      ['options']: [
        {
          ['subtype']: EntranceAnimationTypes['flyIn']['subTypes'].fromBottom,
          ['subtypeName']: 'FromBottom',
        },
        {
          ['subtype']: EntranceAnimationTypes['flyIn']['subTypes'].fromTop,
          ['subtypeName']: 'FromTop',
        },
        {
          ['subtype']: EntranceAnimationTypes['flyIn']['subTypes'].fromLeft,
          ['subtypeName']: 'FromLeft',
        },
        {
          ['subtype']: EntranceAnimationTypes['flyIn']['subTypes'].fromRight,
          ['subtypeName']: 'FromRight',
        },
        {
          ['subtype']:
            EntranceAnimationTypes['flyIn']['subTypes'].fromBottomLeft,
          ['subtypeName']: 'FromBottomLeft',
        },
        {
          ['subtype']:
            EntranceAnimationTypes['flyIn']['subTypes'].fromBottomRight,
          ['subtypeName']: 'FromBottomRight',
        },
        {
          ['subtype']: EntranceAnimationTypes['flyIn']['subTypes'].fromTopLeft,
          ['subtypeName']: 'FromTopLeft',
        },
        {
          ['subtype']: EntranceAnimationTypes['flyIn']['subTypes'].fromTopRight,
          ['subtypeName']: 'FromTopRight',
        },
      ],
    },
  },
  {
    ['typeName']: 'FloatIn',
    ['type']: CustomAnimTypes['Entrance']['FloatIn'], // 占位符, 对 ui 唯一即可
    ['animClass']: 'Entrance',
    ['params']: {
      ['type']: 'options',
      ['options']: [
        {
          ['subtype']: EntranceAnimationTypes['floatIn-Up']['id'],
          ['subtypeName']: 'FloatUp',
        },
        {
          ['subtype']: EntranceAnimationTypes['floatIn-Down']['id'],
          ['subtypeName']: 'FloatDown',
        },
      ],
    },
  },
  {
    ['typeName']: 'Disappear',
    ['type']: ExitAnimationTypes['disappear']['id'],
    ['animClass']: 'Exit',
  },
  {
    ['typeName']: 'FadeOut',
    ['type']: ExitAnimationTypes['fadeOut']['id'],
    ['animClass']: 'Exit',
  },
  {
    ['typeName']: 'FlyOut',
    ['type']: ExitAnimationTypes['flyOut']['id'],
    ['animClass']: 'Exit',
    ['params']: {
      ['type']: 'options',
      ['options']: [
        {
          ['subtype']: ExitAnimationTypes['flyOut']['subTypes']['toBottom'],
          ['subtypeName']: 'ToBottom',
        },
        {
          ['subtype']: ExitAnimationTypes['flyOut']['subTypes']['toTop'],
          ['subtypeName']: 'ToTop',
        },
        {
          ['subtype']: ExitAnimationTypes['flyOut']['subTypes']['toLeft'],
          ['subtypeName']: 'ToLeft',
        },
        {
          ['subtype']: ExitAnimationTypes['flyOut']['subTypes']['toRight'],
          ['subtypeName']: 'ToRight',
        },
        {
          ['subtype']: ExitAnimationTypes['flyOut']['subTypes']['toBottomLeft'],
          ['subtypeName']: 'ToBottomLeft',
        },
        {
          ['subtype']:
            ExitAnimationTypes['flyOut']['subTypes']['toBottomRight'],
          ['subtypeName']: 'ToBottomRight',
        },
        {
          ['subtype']: ExitAnimationTypes['flyOut']['subTypes']['toTopLeft'],
          ['subtypeName']: 'ToTopLeft',
        },
        {
          ['subtype']: ExitAnimationTypes['flyOut']['subTypes']['toTopRight'],
          ['subtypeName']: 'ToTopRight',
        },
      ],
    },
  },
  {
    ['typeName']: 'FloatOut',
    ['type']: CustomAnimTypes['Exit']['FloatOut'], // 占位符, 对 ui 唯一即可
    ['animClass']: 'Exit',
    ['params']: {
      ['type']: 'options',
      ['options']: [
        {
          ['subtype']: ExitAnimationTypes['floatOut-Up']['id'],
          ['subtypeName']: 'FloatUp',
        },
        {
          ['subtype']: ExitAnimationTypes['floatOut-Down']['id'],
          ['subtypeName']: 'FloatDown',
        },
      ],
    },
  },
  {
    ['typeName']: 'Spin',
    ['type']: EmphasisAnimationTypes['spin']['id'],
    ['animClass']: 'Emphasis',
    ['params']: {
      ['type']: 'spin',
      ['default']: 360,
    },
  },
  {
    ['typeName']: 'GrowShrink',
    ['type']: EmphasisAnimationTypes['growShrink']['id'],
    ['animClass']: 'Emphasis',
    ['params']: {
      ['type']: 'scale',
      ['default']: {
        ['scaleX']: 1.5,
        ['scaleY']: 1.5,
      },
    },
  },
  {
    ['typeName']: 'Blink',
    ['type']: EmphasisAnimationTypes['blink']['id'],
    ['animClass']: 'Emphasis',
  },
] as const

/** 多选 shape 设置 animation, 第一个之后的节点均为 AfterEffect */
export function applySlideAnimation(
  editor: Editor,
  animationSettings: AnimationSetting[]
) {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  const presentation = editorUI.presentation
  const curSlide = presentation.slides[presentation.currentSlideIndex]

  const settings: AnimationSetting[] = []
  for (const animSetting of animationSettings) {
    settings.push(animSetting)
  }

  const timeNodes: TimeNode[] = []
  for (const setting of settings) {
    const timeNode = writeTimeNodeFromSetting(setting)
    if (timeNode) timeNodes.push(timeNode)
  }
  startEditAction(EditActionFlag.action_Presentation_ApplyAnimation)
  curSlide.animation.updateTimeNodes(timeNodes)
}

export function getSlideAnimation(editor: Editor, index?: number) {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  const { presentation, editorHandler: handler } = editorUI
  let slide: Slide
  if (isNumber(index)) {
    slide = presentation.slides[index] ?? handler.getCurrentSlide()
  } else {
    slide = handler.getCurrentSlide()
  }
  const timeNodes = slide.animation.timeNodes
  return timeNodesToAnimationSettings(slide, timeNodes)
}

/** 需要全量更新 */
export function applySlideAudios(
  editor: Editor,
  audioSettings: AudioSetting[]
) {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  const presentation = editorUI.presentation
  const curSlide = presentation.slides[presentation.currentSlideIndex]

  const audioNodes: AudioNode[] = []
  for (const setting of audioSettings) {
    const audioNode = writeAudioNodeFromSetting(setting)
    if (audioNode) {
      const index = audioNodes.findIndex(
        (node) => node.spRefId === audioNode.spRefId
      )
      if (index > -1) {
        audioNodes[index] = audioNode
      } else {
        audioNodes.push(audioNode)
      }
    }
  }
  startEditAction(EditActionFlag.action_Presentation_ApplyAnimation)
  curSlide.animation.updateAudioNodes(audioNodes)
}

export function hasAnimation(editor: Editor, index: number): boolean {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  const presentation = editorUI.presentation
  const slide = presentation.slides[index]
  if (!slide) return false
  return isSlideHasAnimation(slide)
}

/**
 * 获取指定slide的预览图片Base64数据
 */
export function getSlidePreviews(
  editor: Editor,
  previewInfos: SlidePreviewImageInfo[]
): { [k: number]: { imgBase64: string; notesImgBase64?: string } } {
  const slidesPreviewer = (editor as unknown as IEditorKit).slidesPreviewer

  const infos = previewInfos.map((info) => ({
    slideNum: info['slideNum'],
    width: info['width'],
    height: info['height'],
    notesWidth: info['notesWidth'],
    notesHeight: info['notesHeight'],
  }))
  const result = slidesPreviewer ? slidesPreviewer.getSlidePreviews(infos) : {}
  return Object.keys(result).reduce((res, slideIndex) => {
    res[slideIndex] = {
      ['imgBase64']: result[slideIndex].imgBase64,
      ['notesImgBase64']: result[slideIndex].notesImgBase64,
    }
    return res
  }, {})
}

export async function getSlidePreviewsAsync(
  editor: Editor,
  previewInfos: SlidePreviewImageInfo[]
): Promise<{ [k: number]: { imageData: Blob } }> {
  const slidesPreviewer = (editor as unknown as IEditorKit).slidesPreviewer
  const infos = previewInfos.map((info) => ({
    slideNum: info['slideNum'],
    width: info['width'],
    height: info['height'],
    type: info['type'],
    quality: info['quality'],
  }))

  const result = slidesPreviewer
    ? await slidesPreviewer?.getSlidePreviewsAsync(infos)
    : {}
  return Object.keys(result).reduce((res, slideIndex) => {
    res[slideIndex] = {
      ['imageData']: result[slideIndex].imageData,
    }
    return res
  }, {})
}

export function getSlideTransition(editor: Editor, slideIndex: number) {
  const slide = (editor as unknown as IEditorKit).presentation.slides[
    slideIndex
  ]
  if (slide && slide.timing) {
    return getTransitionProps(slide.timing.clone())
  }
}

/** 获取指定 index (默认当前页) 的所有 element 信息 */
export function getSlideElements(
  editor: Editor,
  index?: number
): Array<SlideElementInfo> {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  const {
    presentation: { slides, currentSlideIndex },
  } = editorUI
  const curSlide = slides[currentSlideIndex]
  const slide = index ? (slides[index] ? slides[index] : curSlide) : curSlide
  const slideElementsInfo: Array<SlideElementInfo> = []
  for (const sp of slide.cSld.spTree) {
    const id = sp.getId()
    slideElementsInfo.push(getElementInfoById(editor, id)!)
  }
  return slideElementsInfo
}

// debug only, 直接获取对象引用
export function getSlideElementById(editor: Editor, id): Nullable<Object> {
  const sp = EditorInstance.entityRegistry.getEntityById(
    id
  ) as Nullable<SlideElement>
  return sp
}

/**
 * 预览slide的切页动画
 */

export function playSlideTransition(editor: Editor) {
  ;(editor as unknown as IEditorKit).PlaySlideTransition()
}

export function stopPlayingSlideTrasition(editor: Editor) {
  ;(editor as unknown as IEditorKit).StopSlideTransitionPlay()
}

// notes
export function getSlideNotesContent(
  editor: Editor,
  slideIndex: number
): string {
  return (editor as unknown as IEditorKit).getSlideNotesContent(slideIndex)
}

export function setSlideNotesContent(
  editor: Editor,
  slideIndex: number,
  text: string
) {
  ;(editor as unknown as IEditorKit).setSlideNotesContent(slideIndex, text)
}

export function getBuiltInThemes(editor: Editor) {
  return (editor as unknown as IEditorKit).getBuiltInThemes()
}

export function changeBuiltInTheme(
  editor: Editor,
  refId: string,
  type?: ApplyType
) {
  ;(editor as unknown as IEditorKit).changeBuiltInTheme(refId, type)
}

export function applyOfficalTheme(editor: Editor, themeInfo: ThemeInfo) {
  ;(editor as unknown as IEditorKit).applyOfficalTheme(themeInfo)
}

type ShapeInfo = {
  type: 'Shape'
  name: string
  shapeType: PresetShapeTypes | 'custom'
  textContent: string
  canAddAnimation: boolean
}
type ImageInfo = {
  type: 'Image'
  name: string
  imageUrl: Nullable<string>
  encryptImageUrl?: string
  /** 若 url 不存在则说明上传失败或者为 link 型 ole */
  oleInfo?: Optional<UploadUrlResult>
  subType?: 'Chart' | 'Video' | 'Audio' | 'Ole'
}
type TableInfo = { type: 'Table'; name: string }
type GroupInfo = {
  type: 'Group'
  name: string
  childSpInfos: SlideElementInfo[]
}

type SlideElementInfo = { id: string } & (
  | ShapeInfo
  | ImageInfo
  | TableInfo
  | GroupInfo
)

/**
 * @param gTableId
 * */
export function getElementInfoById(
  editor: Editor,
  id: string
): Nullable<SlideElementInfo> {
  const sp = EditorInstance.entityRegistry.getEntityById(id) as
    | SlideElement
    | undefined
  if (!sp) return
  const name = getShapeName(sp)

  switch (sp.instanceType) {
    case InstanceType.Shape:
      return {
        ['id']: id,
        ['type']: 'Shape',
        ['name']: name,
        ['shapeType']: getShapeType(sp),
        ['textContent']: getShapeContentText(sp).trim(),
        ['canAddAnimation']: !canAddAnimation(sp),
      }
    case InstanceType.ImageShape:
      const isChart = isChartImage(sp)
      const isVideo = isVideoImage(sp)
      const isAudio = isAudioImage(sp)
      const { url, encryptUrl } = sp.getImageUrl()
      const imageInfo: SlideElementInfo = {
        ['id']: id,
        ['type']: 'Image',
        ['name']: name,
        ['imageUrl']: url,
        ['encryptImageUrl']: encryptUrl,
      }
      if (sp.isOleObject()) {
        imageInfo['oleInfo'] = {
          ['url']: sp.uri,
          ['encryptUrl']: sp.encryptUrl,
        }
        assignVal(imageInfo, 'subType', 'Ole')
      } else {
        const subType = isChart
          ? 'Chart'
          : isVideo
          ? 'Video'
          : isAudio
          ? 'Audio'
          : undefined
        assignVal(imageInfo, 'subType', subType)
      }
      return imageInfo
    case InstanceType.GraphicFrame:
      return { ['id']: id, ['type']: 'Table', ['name']: name }
    case InstanceType.GroupShape:
      const childSpInfos: SlideElementInfo[] = []
      for (const childSp of sp.spTree) {
        childSpInfos.push(getElementInfoById(editor, childSp.getId())!)
      }
      return { ['id']: id, ['type']: 'Group', ['name']: name, ['childSpInfos']: childSpInfos }
  }
}

export function getSlideImageLikes(editor: Editor, slideIndex: number) {
  return (editor as unknown as IEditorKit).getSlideImageLikes(slideIndex)
}

/** 获取选中的所有附件信息 */
export function getSelectedImageLikes(editor: Editor) {
  return (editor as unknown as IEditorKit).getSelectedImageLikes()
}

export function getHittestInfoByMousePosition(
  editor: Editor,
  x: number,
  y: number
) {
  return (editor as unknown as IEditorKit).getHittestInfoByMousePosition(x, y)
}

// percentage: 0 - 1
export function getHittestInfoByPagePercentagePosition(
  editor: Editor,
  percentageX: number,
  percentageY: number,
  slideIndex: number
) {
  return (
    editor as unknown as IEditorKit
  ).getHittestInfoByPagePercentagePosition(percentageX, percentageY, slideIndex)
}
