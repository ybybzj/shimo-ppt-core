import { isNumber } from '../common/utils'
import { IEditorKit } from '../editor/type'
import {
  prepareComposedCanvasByIndex,
  ToImageLayoutSettings,
} from '../ui/features/print/longImage'
import {
  getPrintSlideIndexes,
  getPrintSlideIndexesAtPage,
  paintPrintPageBySettings,
  PrintCanvasCache,
  PrintLayoutSettings,
} from '../ui/features/print/print'
import { checkSlideLoaded } from './editor'
import { Editor } from './type'

export function getPrintPageCountBySettings(
  editor: Editor,
  settings: PrintLayoutSettings
) {
  const editorKit = editor as unknown as IEditorKit
  const printSettings = formatSettings(settings)
  const { isPrintHidden, rangeType } = printSettings
  const slideIndexes = getPrintSlideIndexes(editorKit, isPrintHidden, rangeType)
  if (!slideIndexes || slideIndexes.length < 1) {
    return 0
  }
  const slidePerPage =
    printSettings.layout.type === 'handouts'
      ? printSettings.layout.slidesPerPage
      : 1
  return Math.ceil(slideIndexes.length / slidePerPage)
}

// 传入 waitSourceLoaded 则表示确定等待/不等待加载，否则包含询问是否加载完成的动作
export async function getPrintPageBySettings(
  editor: Editor,
  pageIndex: number,
  settings: PrintLayoutSettings,
  waitSourceLoaded?: boolean
): Promise<PrintCanvasCache | 'loading'> {
  const editorKit = editor as unknown as IEditorKit
  const slideIndexes = getPrintSlideIndexesAtPage(
    editorKit,
    pageIndex,
    settings
  )
  const slides = editorKit.presentation.slides
  const printSettings = formatSettings(settings)

  if (waitSourceLoaded === false) {
    return Promise.resolve(
      paintPrintPageBySettings(editorKit, pageIndex, printSettings)
    )
  } else {
    return Promise.all(
      slideIndexes.map((index) => {
        const slide = slides[index]
        return checkSlideLoaded(editor, slide.refId!, waitSourceLoaded)
      })
    ).then((result) => {
      if (waitSourceLoaded !== true && result.includes(false)) {
        return 'loading'
      }
      return paintPrintPageBySettings(editorKit, pageIndex, printSettings)
    })
  }
}

/** 导出长图(blob) */
export async function getLongImageData(
  editor: Editor,
  indexes: number[],
  settings?: ToImageLayoutSettings
): Promise<Blob | number> {
  const editorKit = editor as unknown as IEditorKit
  return new Promise((reslove) => {
    const layoutSettings = {
      margin: settings ? settings['margin'] : undefined,
      dpi: settings ? settings['dpi'] : undefined,
    }
    const result = prepareComposedCanvasByIndex(
      editorKit,
      indexes,
      layoutSettings
    )
    if (isNumber(result)) {
      reslove(result)
    }
    let canvas = result as HTMLCanvasElement | undefined
    canvas!.toBlob((blob) => {
      reslove(blob!)
      canvas!.remove()
      canvas = undefined
    })
  })
}

function formatSettings(settings: PrintLayoutSettings): PrintLayoutSettings {
  return {
    layout: {
      type: settings['layout']['type'],
      slidesPerPage: settings['layout']['slidesPerPage'],
      needPlaceHolder: settings['layout']['needPlaceHolder'],
    },
    pageSize: {
      mmWidth: settings['pageSize']['mmWidth'],
      mmHeight: settings['pageSize']['mmHeight'],
    },
    direction: settings['direction'],
    needBorder: settings['needBorder'],
    isPrintHidden: settings['isPrintHidden'],
    scaleByPage: settings['scaleByPage'],
    rangeType: settings['rangeType'],
    dpi: settings['dpi'],
  }
}
