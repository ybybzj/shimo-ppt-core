import { IEditorKit } from '../editor/type'
import { SelectedElementType } from '../core/common/const/attrs'
import {
  Editor,
  SelectedObject,
  SelectedObjectType,
  SlideElementPos,
} from './type'
import { getSelectedText as getSelectedTextForPresentation } from '../core/utilities/presentation/getters'
import { isLineShape } from '../core/utilities/shape/asserts'
import { EditControlType } from '../core/common/const/ui'
import { getSelectedElementsInfo } from '../editor/interfaceState/syncInterfaceState'
import { EditorUtil as EditorInstance } from '../globals/editor'
import { Nullable } from '../../liber/pervasive'
import { SlideElement } from '../core/SlideElement/type'
import { getBase64ImgForSlideElement } from '../core/render/toImage'

export function getSelectedText(editor: Editor): string | null {
  const presentation = (editor as unknown as IEditorKit).presentation
  return getSelectedTextForPresentation(presentation, true, undefined)
}

/**
 * 获取选中对象的类型和属性
 * @export
 * @param {Editor} editor
 * TODO 后续转为 Dict
 */
export function getSelectedElements(
  editor: Editor,
  update?: boolean
): SelectedObject[] {
  const editorKit = editor as unknown as IEditorKit
  const presentation = editorKit.presentation
  const { width, height } = presentation

  const selectedObjectInfos: SelectedObject[] = []
  selectedObjectInfos.push({
    ['type']: SelectedElementType.Presentation,
    ['value']: {
      ['size']: { ['width']: width, ['height']: height },
    },
  })

  const elementInfos = getSelectedElementsInfo(editorKit, update)
  for (const info of elementInfos) {
    selectedObjectInfos.push({
      ['type']: info.type as
        | SelectedObjectType.paragraph
        | SelectedObjectType.table
        | SelectedObjectType.image
        | SelectedObjectType.hyperlink
        | SelectedObjectType.shape
        | SelectedObjectType.slide
        | SelectedObjectType.presentation,
      ['value']: info.value,
      // @ts-ignore
      ['ids']: info.ids,
    })
  }
  return selectedObjectInfos
}

/** id 若为空则刷新 */
export function hoverElementById(editor: Editor, id?: string) {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  const { renderingController: drawingController, editorHandler: handler } =
    editorUI
  if (!id) {
    handler.updateEditLayer()
    return
  }
  const sp = EditorInstance.entityRegistry.getEntityById(
    id
  ) as Nullable<SlideElement>
  if (!sp) return
  const { extX, extY } = sp
  const matrix = sp.getTransform()
  drawingController.drawControl(
    EditControlType.HoveringOutline,
    matrix,
    0,
    0,
    extX,
    extY,
    isLineShape(sp),
    false
  )
}

export function getSelectedElementsNumberByTypes(editor: Editor) {
  return (editor as unknown as IEditorKit).getSelectedElementsNumberByTypes()
}

export function getElementPosById(
  editor: Editor,
  id: string
): SlideElementPos | undefined {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  const handler = editorUI.editorHandler
  if (handler) {
    const posInfo = handler.getSpPosById(id)
    if (posInfo) {
      return {
        ['width']: posInfo.width,
        ['height']: posInfo.height,
        ['absX']: posInfo.absX,
        ['absY']: posInfo.absY,
      }
    }
  }
}

export function selectElementByIds(editor: Editor, ids: string[]) {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  const handler = editorUI.editorHandler
  if (handler) {
    handler.selectObjectByIds(ids, true)
  }
}

/** debug only, 根据 refId 跨页面选中 sp, 方便定位异常数据 */
export function selectElementByRefId(editor: Editor, refId: string) {
  const editorUI = (editor as unknown as IEditorKit).editorUI
  const handler = editorUI.editorHandler
  if (handler) {
    handler.selectElementByRefId(refId)
  }
}

export function getSelectedElementsBase64Img(editor: Editor) {
  const handler = (editor as unknown as IEditorKit).editorUI.editorHandler
  const selectedSp = handler.getSelectedSlideElements()[0]
  if (!selectedSp) return
  return getBase64ImgForSlideElement(selectedSp)
}
