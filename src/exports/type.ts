import { Nullable, Opaque, Optional, valuesOfDict } from '../../liber/pervasive'
import { CmContentData as _CmContentData } from '../io/dataType/comment'
import { AnimClass } from '../io/dataType/animation'
import { SlidePreviewImageInfo as _SlidePreviewImageInfo } from '../ui/SlidePreviewImages'
import { ShapeAnimationNodeType } from '../re/export/ShapeAnimation'
import { DashStyle } from '../core/common/const/attrs'
import { RGBClr } from '../core/color/type'
import { PresetShapeTypes } from './graphic'
import { ImageLikeInfo as _ImageLikeInfo } from '../editor/proto'
import { UploadUrlResult } from '../editor/type'

export type Color = RGBClr

export type { EditorKitOptions as EditorApiOptions } from '../editor/type'

export type Editor = Opaque<'Editor', object>

export type LayoutId = Opaque<'LayoutId', string>

export enum SelectedObjectType {
  paragraph = 0,
  table = 1,
  image = 2,
  header = 3,
  hyperlink = 4,
  shape = 6,
  slide = 7,
  chart = 8,
  math = 9,
  presentation = 10,
}

/** 同步 editor/proto/slide 中 */
export enum ImageLikeTypes {
  image,
  video,
  audio,
  chart,
  attachment,
}

export type SelectedObject =
  | {
      type: typeof SelectedObjectType.presentation
      value: PresentationProps
    }
  | {
      type: typeof SelectedObjectType.slide
      value: SlideProps
    }
  | {
      type: typeof SelectedObjectType.paragraph
      value: ParagraphProps
    }
  | {
      type: typeof SelectedObjectType.shape
      value: ShapeProps
      ids: string[]
    }
  | {
      type: typeof SelectedObjectType.table
      value: TableProps
      ids: string[]
    }
  | {
      type: typeof SelectedObjectType.image
      value: ImageProps
      ids: string[]
    }
  | {
      type: typeof SelectedObjectType.hyperlink
      value: HyperlinkProps
    }

export type PresentationProps = {
  size: { width: number; height: number }
}

export type SlideProps = {
  background: ShapeFill | null
  transition: SlideTransition
  animation: AnimationSetting[]
  audio: AudioSetting[]
}

export type SlideTransition = {
  transitionType?: number
  transitionOption?: number
  transitionDuration?: number
  slideAdvanceOnMouseClick?: boolean
  slideAdvanceAfter?: boolean
  slideAdvanceDuration?: number
  showLoop?: boolean
}

export type ParagraphProps = {
  spacing: Nullable<{
    after: Nullable<number>
    before: Nullable<number>
    line: Nullable<number>
    lineRule: Nullable<number>
  }>
  rtl: boolean
}

export type ShapeProps = {
  type?: PresetShapeTypes | null
  stroke: ShapeStroke
  fill: ShapeFill
  verticalTextAlign?: Nullable<number>
  canFill: boolean
  isEmptyPlaceholder: boolean
  isFromImage: boolean
}

export type TableProps = {
  cellsBackground?: {
    color: RGBClr
    transparent: number
    visible: boolean
  }
  borders: {
    [key: string]: {
      color: Nullable<RGBClr>
      transparent: Nullable<number>
      size: Nullable<number>
      visible: boolean
    } | null
  }
  rtl?: boolean
}

export type ImageProps = {
  id?: string
  subTypes?: ImageSubTypes[]
  url: string
  encryptUrl?: string
  linkUrl?: string
  transparent: number
  media?: {
    type?: MediaType
    url?: string
    encryptUrl?: string
  }
  attachment?: Optional<UploadUrlResult>
}

export type HyperlinkProps = {
  text?: Nullable<string>
  value?: Nullable<string>
  toolTip?: Nullable<string>
}

export enum gradType {
  linear = 0,
  circle = 1,
  rect = 2,
  shape = 3,
}

export type ShapeFill =
  | {
      type: FillType.noFill
      fill: null
    }
  | {
      type: FillType.solid
      fill: FillSolid
      transparent?: number
    }
  | {
      type: FillType.blip
      fill: FillBlip
      transparent?: number
    }
  | {
      type: FillType.gradient
      gradType: gradType
      angle?: number
      direction?: GradientDirections
      gsLst: Array<{ color: RGBClr; pos: number }>
    }
  | null

export enum FillType {
  none = 0,
  blip = 1,
  noFill = 2,
  solid = 3,
  gradient = 4,
  pattern = 5,
  grp = 6,
}

export type FillBlip = {
  type: BlipFillStyle
  url: string | null
}

export enum BlipFillStyle {
  stretch = 1,
  tile = 2,
}

export type FillSolid = {
  color: RGBClr
}

export type ShapeStroke = {
  type: number
  width: number
  prstDash: valuesOfDict<typeof DashStyle>
  color: RGBClr | null
  transparent?: number
} | null

export enum DashType {
  dash = 0,
  dashDot = 1,
  dot = 2,
  lgDash = 3,
  lgDashDot = 4,
  lgDashDotDot = 5,
  solid = 6,
  sysDash = 7,
  sysDashDot = 8,
  sysDashDotDot = 9,
  sysDot = 10,
}

export type SlidePreviewImageInfo = _SlidePreviewImageInfo

// Todo, rename with ui
export interface SlideTransitionSettings {
  TransitionType?: number
  TransitionOption?: number
  TransitionDuration?: number
  SlideAdvanceOnMouseClick?: boolean
  SlideAdvanceAfter?: boolean
  SlideAdvanceDuration?: number
}

export interface SlideTransitionProps {
  type?: (typeof TransitionTypes)[keyof typeof TransitionTypes]
  option?: -1 | (typeof TransitionOptions)[keyof typeof TransitionOptions]
  duration?: number
  advanceOnMouseClick?: boolean
  advanceAfter?: boolean
  advanceDuration?: number
}

// TODO, declear shapeTypes

export interface AnimationSetting {
  /** entityId, not refId */
  ShapeId: string
  AnimClass: AnimClass
  AnimType: number
  AnimSubType?: number
  AnimParam?: { rot: number } | { sx: number; sy: number }
  AnimDelay: number
  AnimDuration: number
  AnimTriggerType: ShapeAnimationNodeType
  AnimRepeatCount?: number
}

export enum AlignType {
  Selected = 0,
  Slide = 1,
}

export enum Errors {
  SplitCellExceedMaxRows = 1,
  SplitCellExceedMaxCols = 2,
  SplitCellErrorRows = 3,
  ErrorChange = 4,
  PasteReachLimit = 5,
}

export type CmContentData = _CmContentData

export enum MediaType {
  video = 7,
  audio = 8,
}

export type ImageLikeInfo = _ImageLikeInfo

export enum ImageSubTypes {
  image = 'image',
  ole = 'ole',
  chart = 'chart',
  media = 'media',
  custom = 'custom',
}

export type MediaInfo =
  | {
      type?: MediaType
      media?: string
    }
  | undefined

export interface AudioSetting {
  /** entityId, not refId */
  shapeId: string
  acrossSlideNum: number
  triggerType: 'click' | 'auto'
  hideWhenShow: boolean
  loopUntilStopped: boolean
  /** 后续可能设置播放指定区间, 因此 duration 应该还是在设置动画的时候去获取, 默认1 */
  duration?: number
  volume?: number
}

export type AudioAction = {
  action: 'toggle' | 'start' | 'end' | 'dbClick'
  url: string
  setting?: AudioSetting
  pos?: SlideElementPos
  hidePlayer?: boolean
}

export interface SlideElementPos {
  absX: number
  absY: number
  width: number
  height: number
}

export enum SpecialPasteTypes {
  sourceFormatting = 1,
  destinationTheme = 2,
  picture = 3,
  keepTextOnly = 4,
}

export enum ContextmenuSpecialPasteStatusType {
  succeed = 0,
  unsupported = 1,
  denied = 2,
  empty = 3,
}

export interface GradientStop {
  color: string
  pos: number
  transparent?: number
}

export enum GradientDirections {
  /** fillRect: {t: 100, l: 100}, tileRect: {b: -100, r: -100}*/
  FromBottomRightCorner = 1,
  /** fillRect: {t: 100, r: 100}, tileRect: {b: -100, l: -100} */
  FromBottomLeftCorner = 2,
  /** fillRect: {l: 50, t: 50, r: 50, b: 50} */
  FromCenter = 3,
  /** fillRect: {l: 100, b: 100}, tileRect: {t: -100, r: -100} */
  FromTopRightCorner = 4,
  /** fillRect: {r: 100, b: 100}, tileRect: {t: -100, l: -100} */
  FromTopLeftCorner = 5,
}

export type TrackErrorType =
  | 'CORE_TRACK'
  | 'INVERT_ERROR'
  | 'COPY_PASTE_ERROR'
  | 'LOCKING_CHANGE'
  | 'NO_ACTION_CHANGE'
  | 'ERROR_BETWEEN_CHANGE'
  | 'MISSING_DOM'

export type TrackPerfType =
  | 'IO_LOAD'
  | 'IO_SEND'
  | 'IO_APPLY'
  | 'CALC_STATUS'
  | 'RENDER_SLIDE'

export interface GuidelineUsability {
  isMeasureGuidelineEnabled?: boolean
  isAlignGuidelineEnabled?: boolean
}
import { EditorSettings } from '../core/common/EditorSettings'
import {
  TransitionOptions,
  TransitionTypes,
} from '../core/Slide/SlideTransition'
export { EditorSettings }

export type { SpPackedData } from '../io/insertion/shapes'
