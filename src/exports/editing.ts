import { Editor } from './type'
import { IEditorKit } from '../editor/type'
import { EditorSettings } from '../core/common/EditorSettings'
import { StatusOfFormatPainterValues } from '../ui/editorUI/const'

export function remove(editor: Editor) {
  ;(editor as unknown as IEditorKit).remove()
}

export function selectAll(editor: Editor, isSelectByFocus?: boolean) {
  ;(editor as unknown as IEditorKit).selectAll(isSelectByFocus)
}

export function selectText(editor: Editor) {
  const handler = (editor as unknown as IEditorKit).editorUI.editorHandler
  handler.selectText()
}

/**
 * 格式刷
 * @export
 * @param {Editor} editor
 * @param {number} value
 * off: 0 on: 1 multiple: 2
 */

export type { StatusOfFormatPainterValues }
export function setPaintFormat(
  editor: Editor,
  value: StatusOfFormatPainterValues
) {
  ;(editor as unknown as IEditorKit).setPaintFormat(value)
}

/**
 * 粘贴格式
 */
export function pastePaintFormat(
  editor: Editor
) {
  ;(editor as unknown as IEditorKit).pastePaintFormat()
}

/**
 * 是否可以使用格式刷提取格式
 */
export function canSetPaintFormat(editor: Editor): boolean {
  const presentation = (editor as unknown as IEditorKit).presentation
  const selectionState = presentation.selectionState
  if (selectionState.selectedTextSp != null) return true

  if (EditorSettings.enableSpFormatPainting === false) return false
  if (selectionState.selectedSpForFormatting != null) return true
  if (selectionState.selectedTable != null) return true
  return false
}

/**
 * 清除样式
 * @export
 * @param {Editor} editor
 */
export function clearFormating(editor: Editor) {
  ;(editor as unknown as IEditorKit).presentation.clearFormattingForParagraph(
    false,
    true
  )
}
