import { Editor } from './type'
import { IEditorKit } from '../editor/type'

export function applyChanges(editor: Editor, delta: any) {
  ;(editor as unknown as IEditorKit).collaborationManager.applyChange({ delta })
}

export function canHandleChanges(editor: Editor): boolean {
  return (
    editor as unknown as IEditorKit
  ).collaborationManager.canHandleChanges()
}

export function getPPTData(editor: Editor): any {
  return (
    editor as unknown as IEditorKit
  ).collaborationManager.currentPptIOData()
}
