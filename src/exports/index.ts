import * as SMCoreType from './type'

import { SMCore } from './quoted_exports'
import { EditorUtil } from '../globals/editor'
import { printModoc } from '../re/export/modoc'

dev: {
  const _editorExports = window['EditorCoreApi']
  if (!_editorExports) {
    console.log()
    window['EditorCoreApi'] = SMCore
    window['EditorUtil'] = EditorUtil
    window['printModoc'] = printModoc
  }
}

export * from './quoted_exports'
export { SMCoreType }
