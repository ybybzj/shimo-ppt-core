import { ZhCNLangID } from '../lib/locale'
import { FontHint, FontClass, FontClassValues } from './const'

const numOfChunks = 65536
const chunks = 4

const hintEAPos = numOfChunks
const hintZHPos = 2 * numOfChunks
const hintEACSPos = 3 * numOfChunks

const fontClassData = (
  typeof Uint8Array !== 'undefined'
    ? (() => {
        const data = new Uint8Array(numOfChunks * chunks)
        let i, j

        // ********************** 1st table *********************** //
        j = 0
        for (i = 0x0000; i <= 0x007f; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0x00a0; i <= 0x04ff; i++) {
          data[i + j] = FontClass.HAnsi
        }
        for (i = 0x0590; i <= 0x07bf; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0x1100; i <= 0x11ff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x1e00; i <= 0x1eff; i++) {
          data[i + j] = FontClass.HAnsi
        }
        for (i = 0x1f00; i <= 0x27bf; i++) {
          data[i + j] = FontClass.HAnsi
        }
        for (i = 0x2e80; i <= 0x319f; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x3200; i <= 0x4d8f; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x4e00; i <= 0x9faf; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xa000; i <= 0xa4cf; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xac00; i <= 0xd7af; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xd800; i <= 0xdfff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xe000; i <= 0xf8ff; i++) {
          data[i + j] = FontClass.HAnsi
        }
        for (i = 0xf900; i <= 0xfaff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xfb00; i <= 0xfb1c; i++) {
          data[i + j] = FontClass.HAnsi
        }
        for (i = 0xfb1d; i <= 0xfdff; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0xfe30; i <= 0xfe6f; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xfe70; i <= 0xfefe; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0xff00; i <= 0xffef; i++) {
          data[i + j] = FontClass.EastAsia
        }

        // ********************** 2nd table *********************** //
        j = hintEAPos
        for (i = 0x0000; i <= 0x007f; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0x00a0; i <= 0x04ff; i++) {
          data[i + j] = FontClass.HAnsi
        }
        data[0xa1 + j] = FontClass.EastAsia
        data[0xa4 + j] = FontClass.EastAsia
        data[0xa7 + j] = FontClass.EastAsia
        data[0xa8 + j] = FontClass.EastAsia
        data[0xaa + j] = FontClass.EastAsia
        data[0xad + j] = FontClass.EastAsia
        data[0xaf + j] = FontClass.EastAsia
        data[0xb0 + j] = FontClass.EastAsia
        data[0xb1 + j] = FontClass.EastAsia
        data[0xb2 + j] = FontClass.EastAsia
        data[0xb3 + j] = FontClass.EastAsia
        data[0xb4 + j] = FontClass.EastAsia
        data[0xb6 + j] = FontClass.EastAsia
        data[0xb7 + j] = FontClass.EastAsia
        data[0xb8 + j] = FontClass.EastAsia
        data[0xb9 + j] = FontClass.EastAsia
        data[0xba + j] = FontClass.EastAsia
        data[0xbc + j] = FontClass.EastAsia
        data[0xbd + j] = FontClass.EastAsia
        data[0xbe + j] = FontClass.EastAsia
        data[0xbf + j] = FontClass.EastAsia
        data[0xd7 + j] = FontClass.EastAsia
        data[0xf7 + j] = FontClass.EastAsia

        for (i = 0x02b0; i <= 0x04ff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x0590; i <= 0x07bf; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0x1100; i <= 0x11ff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x1e00; i <= 0x1eff; i++) {
          data[i + j] = FontClass.HAnsi
        }
        for (i = 0x1f00; i <= 0x1fff; i++) {
          data[i + j] = FontClass.HAnsi
        }
        for (i = 0x2000; i <= 0x27bf; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x2e80; i <= 0x319f; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x3200; i <= 0x4d8f; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x4e00; i <= 0x9faf; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xa000; i <= 0xa4cf; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xac00; i <= 0xd7af; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xd800; i <= 0xdfff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xe000; i <= 0xf8ff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xf900; i <= 0xfaff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xfb00; i <= 0xfb1c; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xfb1d; i <= 0xfdff; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0xfe30; i <= 0xfe6f; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xfe70; i <= 0xfefe; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0xff00; i <= 0xffef; i++) {
          data[i + j] = FontClass.EastAsia
        }

        // ********************** 3rd table *********************** //
        j = hintZHPos
        for (i = 0x0000; i <= 0x007f; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0x00a0; i <= 0x00ff; i++) {
          data[i + j] = FontClass.HAnsi
        }
        data[0xa1 + j] = FontClass.EastAsia
        data[0xa4 + j] = FontClass.EastAsia
        data[0xa7 + j] = FontClass.EastAsia
        data[0xa8 + j] = FontClass.EastAsia
        data[0xaa + j] = FontClass.EastAsia
        data[0xad + j] = FontClass.EastAsia
        data[0xaf + j] = FontClass.EastAsia
        data[0xb0 + j] = FontClass.EastAsia
        data[0xb1 + j] = FontClass.EastAsia
        data[0xb2 + j] = FontClass.EastAsia
        data[0xb3 + j] = FontClass.EastAsia
        data[0xb4 + j] = FontClass.EastAsia
        data[0xb6 + j] = FontClass.EastAsia
        data[0xb7 + j] = FontClass.EastAsia
        data[0xb8 + j] = FontClass.EastAsia
        data[0xb9 + j] = FontClass.EastAsia
        data[0xba + j] = FontClass.EastAsia
        data[0xbc + j] = FontClass.EastAsia
        data[0xbd + j] = FontClass.EastAsia
        data[0xbe + j] = FontClass.EastAsia
        data[0xbf + j] = FontClass.EastAsia
        data[0xd7 + j] = FontClass.EastAsia
        data[0xf7 + j] = FontClass.EastAsia

        data[0xe0 + j] = FontClass.EastAsia
        data[0xe1 + j] = FontClass.EastAsia
        data[0xe8 + j] = FontClass.EastAsia
        data[0xe9 + j] = FontClass.EastAsia
        data[0xea + j] = FontClass.EastAsia
        data[0xec + j] = FontClass.EastAsia
        data[0xed + j] = FontClass.EastAsia
        data[0xf2 + j] = FontClass.EastAsia
        data[0xf3 + j] = FontClass.EastAsia
        data[0xf9 + j] = FontClass.EastAsia
        data[0xfa + j] = FontClass.EastAsia
        data[0xfc + j] = FontClass.EastAsia

        for (i = 0x0100; i <= 0x02af; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x02b0; i <= 0x04ff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x0590; i <= 0x07bf; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0x1100; i <= 0x11ff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x1e00; i <= 0x1eff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x1f00; i <= 0x1fff; i++) {
          data[i + j] = FontClass.HAnsi
        }
        for (i = 0x2000; i <= 0x27bf; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x2e80; i <= 0x319f; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x3200; i <= 0x4d8f; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x4e00; i <= 0x9faf; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xa000; i <= 0xa4cf; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xac00; i <= 0xd7af; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xd800; i <= 0xdfff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xe000; i <= 0xf8ff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xf900; i <= 0xfaff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xfb00; i <= 0xfb1c; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xfb1d; i <= 0xfdff; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0xfe30; i <= 0xfe6f; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xfe70; i <= 0xfefe; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0xff00; i <= 0xffef; i++) {
          data[i + j] = FontClass.EastAsia
        }

        // ********************** 4rd table *********************** //
        j = hintEACSPos
        for (i = 0x0000; i <= 0x007f; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0x00a0; i <= 0x00ff; i++) {
          data[i + j] = FontClass.HAnsi
        }
        data[0xa1 + j] = FontClass.EastAsia
        data[0xa4 + j] = FontClass.EastAsia
        data[0xa7 + j] = FontClass.EastAsia
        data[0xa8 + j] = FontClass.EastAsia
        data[0xaa + j] = FontClass.EastAsia
        data[0xad + j] = FontClass.EastAsia
        data[0xaf + j] = FontClass.EastAsia
        data[0xb0 + j] = FontClass.EastAsia
        data[0xb1 + j] = FontClass.EastAsia
        data[0xb2 + j] = FontClass.EastAsia
        data[0xb3 + j] = FontClass.EastAsia
        data[0xb4 + j] = FontClass.EastAsia
        data[0xb6 + j] = FontClass.EastAsia
        data[0xb7 + j] = FontClass.EastAsia
        data[0xb8 + j] = FontClass.EastAsia
        data[0xb9 + j] = FontClass.EastAsia
        data[0xba + j] = FontClass.EastAsia
        data[0xbc + j] = FontClass.EastAsia
        data[0xbd + j] = FontClass.EastAsia
        data[0xbe + j] = FontClass.EastAsia
        data[0xbf + j] = FontClass.EastAsia
        data[0xd7 + j] = FontClass.EastAsia
        data[0xf7 + j] = FontClass.EastAsia

        for (i = 0x0100; i <= 0x02af; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x02b0; i <= 0x04ff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x0590; i <= 0x07bf; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0x1100; i <= 0x11ff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x1e00; i <= 0x1eff; i++) {
          data[i + j] = FontClass.HAnsi
        }
        for (i = 0x1f00; i <= 0x1fff; i++) {
          data[i + j] = FontClass.HAnsi
        }
        for (i = 0x2000; i <= 0x27bf; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x2e80; i <= 0x319f; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x3200; i <= 0x4d8f; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0x4e00; i <= 0x9faf; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xa000; i <= 0xa4cf; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xac00; i <= 0xd7af; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xd800; i <= 0xdfff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xe000; i <= 0xf8ff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xf900; i <= 0xfaff; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xfb00; i <= 0xfb1c; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xfb1d; i <= 0xfdff; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0xfe30; i <= 0xfe6f; i++) {
          data[i + j] = FontClass.EastAsia
        }
        for (i = 0xfe70; i <= 0xfefe; i++) {
          data[i + j] = FontClass.ASCII
        }
        for (i = 0xff00; i <= 0xffef; i++) {
          data[i + j] = FontClass.EastAsia
        }

        return data
      })()
    : []
) as FontClassValues[]

export function getFontClass(
  valueOfUnicode,
  valueOfHint,
  valueOfEastAsia,
  isCS
) {
  let fc: FontClassValues = FontClass.ASCII
  if (valueOfUnicode > 0xffff) {
    if (
      (valueOfUnicode >= 0x20000 && valueOfUnicode <= 0x2a6df) ||
      (valueOfUnicode >= 0x2f800 && valueOfUnicode <= 0x2fa1f)
    ) {
      fc = FontClass.EastAsia
    } else if (valueOfUnicode >= 0x1d400 && valueOfUnicode <= 0x1d7ff) {
      fc = FontClass.ASCII
    } else {
      fc = FontClass.HAnsi
    }
  } else if (valueOfHint !== FontHint.EastAsia) {
    fc = fontClassData[valueOfUnicode]
  } else {
    if (valueOfEastAsia === ZhCNLangID) {
      fc = fontClassData[hintZHPos + valueOfUnicode]
    } else {
      fc = fontClassData[hintEAPos + valueOfUnicode]
    }

    if (fc === FontClass.EastAsia) return fc
  }

  if (isCS) return FontClass.CS

  return fc ?? FontClass.ASCII
}
