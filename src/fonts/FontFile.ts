import { fontDetector } from './fontDetector'
import { Font, FontDownloadInfo, FontMetaInfo, FontNameInfo } from './type'

export class FontFile {
  public fullName: string
  public meta?: FontMetaInfo
  public name: FontNameInfo
  public download: FontDownloadInfo
  public isSystem = false
  public isLoading = false
  public isLocal = false

  constructor(fontInfo: Font) {
    this.isSystem = fontInfo['isSystem'] ?? false
    const nameInfo = fontInfo['name']
    this.name = {
      preferredFamily: nameInfo['preferredFamily'],
      preferredSubfamily: nameInfo['preferredSubfamily'],
      fontFamily: nameInfo['fontFamily'],
      fontSubfamily: nameInfo['fontSubfamily'],
      fullName: nameInfo['fullName'],
      postScriptName: nameInfo['postScriptName'],
    }

    const downloadInfo = fontInfo['download']
    this.download = {}
    this.download.url = downloadInfo['url']
    this.download.noDownload = downloadInfo['noDownload']
    this.download.blobUrl = downloadInfo['blobUrl']
    this.download.hidden = downloadInfo['hidden']

    const metaInfo = fontInfo['meta']
    if (metaInfo) {
      const head = metaInfo['head']
      const hhea = metaInfo['hhea']
      const vhea = metaInfo['vhea']
      const os2 = metaInfo['os2']
      this.meta = {
        head: { unitsPerEm: head['unitsPerEm'] },
        hhea: {
          ascender: hhea['ascender'],
          descender: hhea['descender'],
          lineGap: hhea['lineGap'],
        },
      }
      if (vhea) {
        this.meta.vhea = {
          ascender: vhea['ascender'],
          descender: vhea['descender'],
          lineGap: vhea['lineGap'],
        }
      }
      if (os2) {
        this.meta.os2 = {
          usWeightClass: os2['usWeightClass'],
          usWidthClass: os2['usWidthClass'],
        }
      }
    }

    this.fullName = this.getFullName()
    this.isLocal = this.isSystem
  }

  checkLocal() {
    // 由于该字体为备用字体，并不会随 apply load, 暂时先这样处理
    if (this.fullName === 'ShimoUnorderedList') {
      this.isLocal = true
    }
    if (!this.isLocal) {
      this.isLocal = fontDetector.detect(
        this.fullName,
        this.getPostScriptName()
      )
    }
  }

  get isUsable() {
    const isMetaValid = !!(this.meta && this.meta.head && this.meta.hhea)
    return isMetaValid && (this.isSystem || (!this.isSystem && this.isLocal))
  }

  getNameKeys() {
    const { fullName, postScriptName } = this.name
    const nameKeys = [
      ...Object.values(fullName),
      ...Object.values(postScriptName),
    ]

    // fix: 特殊字体名字导出时与配置不符，需添加识别修正
    if (fullName['zh'] === '思源黑体 CN') {
      nameKeys.unshift('思源黑体')
    }

    const { preferredFamily, preferredSubfamily, fontFamily, fontSubfamily } =
      this.name
    const familyNames = preferredFamily ?? fontFamily
    const subFamilyNames = preferredSubfamily ?? fontSubfamily
    const subFamily = getFontNameFromObject(subFamilyNames)

    if (subFamily?.toLocaleLowerCase() === 'regular') {
      nameKeys.push(...Object.values(familyNames))
    }
    return nameKeys
  }

  getFullName() {
    return getFontNameFromObject(this.name.fullName)
  }

  getPostScriptName() {
    return getFontNameFromObject(this.name.postScriptName)
  }

  getFamilyName() {
    const { preferredFamily, preferredSubfamily, fontFamily, fontSubfamily } =
      this.name
    const family = preferredFamily ?? fontFamily
    const subFamily = preferredSubfamily ?? fontSubfamily
    return {
      family: getFontNameFromObject(family),
      subFamily: getFontNameFromObject(subFamily),
    }
  }
}

function getFontNameFromObject(fontMap: Record<string, string>) {
  const fontName: string = fontMap['en']

  return fontName ?? Object.values(fontMap)[0]
}
