export interface FontNameInfo {
  preferredFamily?: Record<string, string>
  preferredSubfamily?: Record<string, string>
  fontFamily: Record<string, string>
  fontSubfamily: Record<string, string>
  fullName: Record<string, string>
  postScriptName: Record<string, string>
}

export interface FontMetaInfo {
  head: { unitsPerEm: number }
  hhea: { ascender: number; descender: number; lineGap: number }
  vhea?: { ascender: number; descender: number; lineGap: number }
  os2?: {
    usWeightClass?: number
    usWidthClass?: number
  }
}
export interface FontDownloadInfo {
  url?: string
  noDownload?: boolean
  blobUrl?: string
  hidden?: boolean
}

export interface Font {
  /** 取 fullname.en */
  id: string
  /** 是系统已安装的字体 */
  isSystem?: boolean
  name: FontNameInfo
  meta?: FontMetaInfo
  download: FontDownloadInfo
}

export interface FontProvider {
  /** 获取字体列表，根据当前环境排序 */
  getFontList: () => Font[]
  getFont: (nameLike: string) => Font | undefined
  /** 调用外部接口查询字体，匹配更准确 */
  searchFont: (nameLike: string) => Promise<Font | undefined>
  /** 加载字体字形信息(耗时长) */
  loadFontMeta: (nameLike: string) => Promise<Font | undefined>
}
