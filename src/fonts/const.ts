import { valuesOfDict } from '../../liber/pervasive'
import { Font } from './type'

export const LocalStorageFontCacheKey = 'shimoPresentationLoadedFonts'

export const ArialFallBackFont = {
  id: 'Arial',
  meta: {
    head: { unitsPerEm: 2048 },
    hhea: { ascender: 1854, descender: -434, lineGap: 67 },
    os2: { usWeightClass: 400, usWidthClass: 5 },
  },
  name: {
    fontFamily: { en: 'Arial' },
    fontSubfamily: {
      en: 'Regular',
    },
    fullName: { en: 'Arial' },
    postScriptName: { en: 'ArialMT' },
  },
  download: {},
} as Font

export const FontClass = {
  ASCII: 0,
  EastAsia: 1,
  CS: 2,
  HAnsi: 3,
} as const
export type FontClassValues = valuesOfDict<typeof FontClass>

export const FontHint = { Default: 0, CS: 1, EastAsia: 2 } as const
export type FontHintValues = valuesOfDict<typeof FontHint>
