import { fontDetector } from './fontDetector'
import { FontFile } from './FontFile'
import { FontKit } from './FontKit'
import { encryptFontFamily, decryptFontId } from './utils'
import { EventEmitter, makeEventEmitter } from '../../liber/emitter'
import { FuncMany } from '../../liber/fn/types'
import { loadHBFont } from './HBFontCache'
import { EditorUtil } from '../globals/editor'
import { Nullable } from '../../liber/pervasive'
import { LocalStorageFontCacheKey } from './const'

interface FontLoaderOpts {
  fontManager: FontKit
}

export enum FontLoadEvents {
  LoadStart = 'LoadStart',
  LoadProgress = 'LoadProgress',
  LoadFailed = 'LoadFailed',
  LoadSuccess = 'LoadSuccess',
  LoadComplete = 'LoadComplete',
}

export class FontLoader {
  private fontManager: FontKit
  private failedLoadFontIds: string[] = []
  private emitter: EventEmitter
  constructor({ fontManager }: FontLoaderOpts) {
    this.emitter = makeEventEmitter()
    this.fontManager = fontManager
  }

  emit(evt: string, ...args: any[]) {
    this.emitter.emit(evt, ...args)
  }
  on(evt: string, cb: FuncMany<any>) {
    this.emitter.on(evt, cb)
  }

  off(evt: string, cb?: FuncMany<any>) {
    if (cb) {
      this.emitter.off(evt, cb)
    } else {
      this.emitter.off(evt)
    }
  }

  async loadFont(font: string | FontFile): Promise<Nullable<FontFile>> {
    const { fontManager } = this
    const fontFile =
      typeof font === 'string' ? fontManager.getFontFile(font) : font
    if (!fontFile) {
      return
    }
    const { isLoading, isUsable, fullName } = fontFile
    if (isLoading || isUsable) {
      return fontFile
    }

    if (fontFile.isSystem && !fontFile.isUsable) {
      let font: FontFile | undefined = fontFile
      try {
        font = await this.loadSystemFont(fullName)
      } catch (e) {
        this.emit(FontLoadEvents.LoadFailed, fullName, e)
      }
      return font
    }

    fontFile.isLoading = true
    try {
      await this.loadFromUrl(fontFile)
      await this.checkReady(fullName, fontFile.getPostScriptName())
      fontFile.isLocal = true
      this.emit(FontLoadEvents.LoadSuccess, fullName)
      this.addFontToStorage(fullName)
    } catch (e) {
      this.emit(FontLoadEvents.LoadFailed, fullName, e)
      this.failedLoadFontIds.push(encryptFontFamily(fullName))
    }
    fontFile.isLoading = false

    this.emit(FontLoadEvents.LoadComplete, fullName)

    return fontManager.getFontFile(fullName)
  }

  async loadSystemFont(name: string) {
    if (this.fontManager.fontProvider.loadFontMeta) {
      const font = await this.fontManager.fontProvider.loadFontMeta(name)
      if (font) {
        this.fontManager.addFont(font)
      }
    }
    return this.fontManager.getFontFile(name)
  }

  async loadFonts(
    fonts: string[] = [],
    limit: number,
    cb?: (isFinished: boolean) => void
  ): Promise<boolean> {
    const fontsToLoad: FontFile[] = []
    const { fontManager } = this
    const fontNames = new Set(fonts.map((f) => f.toLowerCase()))
    const fontsNeedForHB: FontFile[] = []
    fontNames.forEach((fontName) => {
      const fontInfo = fontManager.getFontFile(fontName)
      if (fontInfo) {
        const canLoad =
          fontInfo.isSystem ||
          (!fontInfo.isSystem && !fontInfo.download.noDownload)
        if (!fontInfo.isUsable && canLoad) {
          fontsToLoad.push(fontInfo)
        } else if (fontInfo.isUsable && fontInfo.download.blobUrl) {
          fontsNeedForHB.push(fontInfo)
        }
      } else {
        fontManager.searchFont(fontName)
      }
    })
    const hb = EditorUtil.FontKit.hb
    const isHBAvailable = !!hb
    if (!isHBAvailable) {
      fontsNeedForHB.length = 0
    }

    const loadFont = () => {
      if (fontsToLoad.length > 0) {
        const font = fontsToLoad.shift()!
        return this.loadFont(font)
          .then(() => {
            if (cb) {
              cb(!fontsToLoad || fontsToLoad.length === 0)
            }
            return loadFont()
          })
          .catch(loadFont)
      } else if (fontsNeedForHB.length > 0 && isHBAvailable) {
        const font = fontsNeedForHB.shift()!
        const fontName = font.getFullName()
        const fontUrl = font.download.blobUrl!
        return loadHBFont(hb, fontName, fontUrl).then(loadFont).catch(loadFont)
      }
    }

    const needLoadCount = fontsToLoad.length + fontsNeedForHB.length
    const hasUninstallFonts = needLoadCount > 0
    const promiseList = Array(Math.min(limit, needLoadCount))
      .fill(Promise.resolve())
      .map((promise) => promise.then(loadFont))

    try {
      await Promise.all(promiseList)
    } catch (_e) {}
    return hasUninstallFonts
  }

  // 下载 localStorage 中记录的缓存字体
  public loadStorageFonts() {
    const storageFonts = localStorage.getItem(LocalStorageFontCacheKey)
    const illegalFontIds: string[] = []
    if (storageFonts && storageFonts.split && storageFonts !== '') {
      const storageFontIds = storageFonts.split(',')
      const load = () => {
        if (!storageFontIds.length) {
          return
        }
        const fontId = storageFontIds.shift()!
        const targetFont = decryptFontId(fontId)
        const fontInfo = this.fontManager.getFontFile(targetFont)
        if (fontInfo) {
          // 从 localStorage 中解析出的字体名称合法
          return this.loadFont(fontInfo).then(load).catch(load)
        } else {
          // 非法 fontId
          illegalFontIds.push(fontId)
          return Promise.resolve().then(load).catch(load)
        }
      }

      new Promise((res) => setTimeout(res, 1, 'resolve'))
        .then(load)
        .finally(() => {
          this._checkFontsStorage(illegalFontIds)
        })
    }
  }

  private _checkFontsStorage(illegalFontIds: string[] = []) {
    // 清除 localStorage 中的非法字体数据和下载失败字体数据
    const needClearFontIds = illegalFontIds.concat(this.failedLoadFontIds)
    if (needClearFontIds.length === 0) {
      return
    }
    const storageFonts = localStorage.getItem(LocalStorageFontCacheKey)
    let curStorageFontIdStr = ''
    if (
      storageFonts &&
      typeof storageFonts.split === 'function' &&
      storageFonts !== ''
    ) {
      curStorageFontIdStr = storageFonts
      needClearFontIds.map((id) => {
        curStorageFontIdStr = curStorageFontIdStr.replace(id, '')
      })
    }
    const curStorageFontIds = curStorageFontIdStr
      .split(',')
      .filter((item) => item !== '')
    localStorage.setItem(LocalStorageFontCacheKey, curStorageFontIds.join(','))
  }

  private loadFromUrl(fontInfo: FontFile) {
    const url = fontInfo.download.url ?? ''
    return new Promise((resolve, reject) => {
      if (!url) {
        reject()
      }
      const request = new XMLHttpRequest()
      request.open('get', url, true)
      request.responseType = 'arraybuffer'
      this.emit(FontLoadEvents.LoadStart, fontInfo.fullName, request)
      request.onprogress = (e) =>
        this.emit(FontLoadEvents.LoadProgress, fontInfo.fullName, e)
      request.onerror = (_e) => {
        return reject(new Error('Font could not be loaded'))
      }
      request.onload = (_e) => {
        if (request.response) {
          return resolve(request.response)
        } else {
          return reject(
            new Error('Font could not be loaded: ' + request.statusText)
          )
        }
      }
      request.onabort = (_e) => {
        fontInfo.isLoading = false
      }
      request.send()
    })
  }

  private checkReady(
    fontFamily: string,
    postScriptName: string,
    timeout = 3000
  ) {
    let checkReadyInterval: any
    let checkReadyTimeout: any
    const checkPromise = new Promise((resolve) => {
      checkReadyInterval = setInterval(() => {
        const detected = fontDetector.detect(fontFamily, postScriptName)
        if (detected) {
          clearInterval(checkReadyInterval)
          clearTimeout(checkReadyTimeout)
          resolve(fontFamily)
        }
      }, 20)
    })

    const timeoutPromise = new Promise((_, reject) => {
      const timeoutError = new Error(`Font ${fontFamily} is not loaded`)
      checkReadyTimeout = setTimeout(() => {
        clearInterval(checkReadyInterval)
        reject(timeoutError)
      }, timeout)
    })

    return Promise.race([checkPromise, timeoutPromise])
  }

  private addFontToStorage(fullName: string) {
    const fontId = encryptFontFamily(fullName)
    let newStorageFontIds: string[] = []
    const localLoadedFonts = localStorage.getItem(LocalStorageFontCacheKey)
    if (localLoadedFonts && localLoadedFonts.split && localLoadedFonts !== '') {
      newStorageFontIds = localLoadedFonts.split(',')
    }
    if (newStorageFontIds.includes(fontId)) {
      return
    }
    newStorageFontIds.push(fontId)
    localStorage.setItem(LocalStorageFontCacheKey, newStorageFontIds.join(','))
  }
}
