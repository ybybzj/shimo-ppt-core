/* eslint-disable */
interface IFontDetector {
  detect(fullName: string, postScriptName: string): boolean
}

const measureFont = (() => {
  const canvas = document.createElement('canvas')
  const context = canvas.getContext('2d')
  const testSize = '200px'
  return (font: string) => {
    if (!context) return 0

    context.font = `${testSize} ${font}`
    return context.measureText('A').width
  }
})()

const defaultRefFontDataUrl = `data:application/font-woff;base64,d09GRgABAAAAAAM0AAsAAAAABCQAAQAAAAAAAAAAAAAAAAAAAAAAAAAAABVPUy8yAAABcAAAADwAAABgNVxaOmNtYXAAAAG0AAAAKQAAADQADACUZ2FzcAAAAywAAAAIAAAACP//AANnbHlmAAAB6AAAACQAAAAwK9MKuGhlYWQAAAEIAAAAMwAAADYLo1XbaGhlYQAAATwAAAAcAAAAJP2RAdRobXR4AAABrAAAAAgAAAAIAMn9LGxvY2EAAAHgAAAABgAAAAYAGAAMbWF4cAAAAVgAAAAXAAAAIAAEAAVuYW1lAAACDAAAAQsAAAHwJKxCKHBvc3QAAAMYAAAAEwAAACD/KgCWeNpjYGRgYABi+QdqV+P5bb4ycHMwgMD1GraPEJqz/d80BoZ/PxgygFw2kFoGBgA++AvVAHjaY2BkYGBgBMOUf9MYc/79YACJIAMmAF7xBGN42mNgZGBgYGJgYQDRDFASCQAAAQEACgB42mNgZkhhnMDAysDAOovVmIGBURpCM19kSGMSYmBgAklhBR4+CgoMDgyOQMgIhhlgYUYoCaEZAIdLBiEAZP6WAGT+lnjaY2BgYGJgYGAGYhEgyQimWRgUgDQLEIL4jv//Q8j/B8B8BgBSlwadAAAAAAAADAAYAAB42mNg/DeNgeHfD4YMBmYGBkVTY9F/05IyMhgYcIgDAGnPDrV42oWQzU7CUBCFvwISZcEz3LjSBBoQ/yIrYzTEGEmIYY9QiglS01YMOx/DV+AtPXeophtjmumdOffMmXMH2GdOlaB2ACwUuzwQvi7yCk3d7PIqh794rcTZI+WryOslvMFn0OCJDW9EmjRhqtOxVRwJTXhXpxOa8CrOhJXQY0JhJ3Tocmn5NUt9jhEvxHKTk1kV6YyksNZ/ZnUsxaV0Uh4ZKm65YmycTL2J9J1UQ2l3/sT73JvKxlz0aJXc9Lkzds6NeiNNylWn1u37z0wjFP9UcSH8XFmbZ03JtYmFTu99Xqg4PqSR2Q5+9PxbnBx4Zyu9yP070+ultkPHoNhRmwchsaqpOLbhb0h9RfYAeNpjYGYAg//qDNMYsAAAKDQBwAAAAAAB//8AAg==`

const detectFont = (function FontDetector() {
  const baseFonts = ['monospace', 'sans-serif', 'serif'] as const
  let __DetectWidth__ = measureFont(baseFonts.join(' ,'))

  const refFontName = '__DEFAULT_FONT__'

  const style = document.createElement('style')
  style.setAttribute('type', 'text/css')
  style.textContent = `@font-face{font-family:'${refFontName}';src:url('${defaultRefFontDataUrl}');}`
  document.documentElement.appendChild(style)
  let handleId: null | number = null
  const refFontStr = [refFontName, ...baseFonts].join(' ,')

  let isRefFontReady = false

  ;(function initailizeDefaultFont() {
    const width = measureFont(refFontStr)
    if (width != __DetectWidth__) {
      __DetectWidth__ = width
      isRefFontReady = true

      if (handleId != null) {
        cancelAnimationFrame(handleId)
        handleId = null
      }
    } else {
      handleId = requestAnimationFrame(initailizeDefaultFont)
    }
  })()

  return function detect(font: string) {
    if(!isRefFontReady) return false
    const fontStr = `"${font}", ${refFontStr}`
    const width = measureFont(fontStr)
    return width !== __DetectWidth__
  }
})()

class MordernFontDetector {
  detect(fullName: string, postScriptName: string): boolean {
    return detectFont(fullName) || detectFont(postScriptName)
  }
}

export const fontDetector: IFontDetector = new MordernFontDetector()
