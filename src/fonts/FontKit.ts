import { FontFile } from './FontFile'
import { ArialFallBackFont, FontClass, FontClassValues } from './const'
import {
  FontsProps,
  RFonts,
  RFontsOptions,
} from '../core/textAttributes/RFonts'
import { Matrix2D, IMatrix2D, MatrixUtils } from '../core/graphic/Matrix'
import { Factor_pt_to_mm } from '../core/common/const/unit'
import {
  generateDefaultFontFamily,
  setDefaultFontFamilyName,
} from '../core/common/font'
import { BrowserInfo } from '../common/browserInfo'
import { EditorKit } from '../editor/global'
import { Dict, Nullable } from '../../liber/pervasive'
import { EditorSettings } from '../core/common/EditorSettings'
import { ZoomValue } from '../core/common/zoomValue'
import { HarfBuzzNative, loadHarfBuzz } from '../lib/utils/hb/HarfBuzzNative'
// import { getGlyphInfo } from '../lib/utils/hb/hb'
import { getHBFont, loadHBFont } from './HBFontCache'
import {
  CTextPrData,
  fromTextPrFontSize,
  TextPrFont,
  toTextPrFontSize,
} from '../io/dataType/style'
import { assignVal } from '../io/utils'
import { TextPr } from '../core/textAttributes/TextPr'
import { InstanceType } from '../core/instanceTypes'
import { isCJKChar } from './utils'
import { Font, FontNameInfo, FontProvider } from './type'
import { refreshPresentation } from '../editor/utils/editor'

export interface FontKitTextPr {
  fontSize?: Nullable<number>
  bold?: Nullable<boolean>
  italic?: Nullable<boolean>
  fontFamily?: Nullable<{ name: string; index: number }>
  rFonts?: Nullable<RFontsOptions>
}

export function fontkitTextPrToJSON(pr: FontKitTextPr): Partial<CTextPrData> {
  const data: Partial<CTextPrData> = {}

  assignVal(data, 'bold', pr.bold)
  assignVal(data, 'italic', pr.italic)

  if (pr.fontSize != null) {
    assignVal(data, 'fontSize', toTextPrFontSize(pr.fontSize))
  }

  if (pr.rFonts != null) {
    if (pr.rFonts.ascii) {
      assignVal(data, 'latin', {
        ['typeface']: pr.rFonts.ascii.name,
      } as TextPrFont)
    }
    if (pr.rFonts.eastAsia) {
      assignVal(data, 'ea', {
        ['typeface']: pr.rFonts.eastAsia.name,
      } as TextPrFont)
    }
    if (pr.rFonts.cs) {
      assignVal(data, 'cs', {
        ['typeface']: pr.rFonts.cs.name,
      } as TextPrFont)
    }
  }

  if (pr.fontFamily != null) {
    assignVal(data, 'fontFamily', {
      ['Name']: pr.fontFamily.name,
      ['Index']: pr.fontFamily.index,
    })
  }
  return data
}

export function fontkitTextPrFromJSON(
  textPr: FontKitTextPr,
  data: Partial<CTextPrData>
) {
  if ((textPr as TextPr).instanceType === InstanceType.TextPr) {
    const _textPr = textPr as TextPr
    _textPr.reset()
    _textPr.fromJSON(data)
  } else {
    textPr.fontSize = undefined
    textPr.bold = undefined
    textPr.italic = undefined
    textPr.fontFamily = undefined
    textPr.rFonts = undefined
    if (null != data['fontFamily']) {
      textPr.fontFamily = {
        name: data['fontFamily']['Name'],
        index: data['fontFamily']['Index'],
      }
    }
    if (data['bold'] != null) {
      textPr.bold = data['bold']
    }

    if (data['italic'] != null) {
      textPr.italic = data['italic']
    }

    if (null != data['fontSize']) {
      textPr.fontSize = fromTextPrFontSize(data['fontSize'])
    }

    if (null != data['latin'] || null != data['ea'] || null != data['cs']) {
      textPr.rFonts = new RFonts()

      if (null != data['latin']) {
        textPr.rFonts.ascii = {
          name: data['latin']['typeface'] || '',
          index: -1,
        }
        textPr.rFonts.hAnsi = { name: textPr.rFonts.ascii!.name, index: -1 }
      }

      if (null != data['ea']) {
        textPr.rFonts.eastAsia = {
          name: data['ea']['typeface'] || '',
          index: -1,
        }
      }

      if (null != data['cs']) {
        textPr.rFonts.cs = { name: data['cs']['typeface'] || '', index: -1 }
      }
    }
  }
}

type TextPrConstructorFn = new () => FontKitTextPr
type RFontsFn = new () => RFonts

interface FontManagerOpts {
  TextPrConstructorFn: TextPrConstructorFn
  RFontsFn: RFontsFn
}

export interface FontSimpleInfo {
  fontFamily: FontsProps
  fontSize: number
  italic: boolean
  bold: boolean
}

const _fallbackFont = new FontFile(ArialFallBackFont)

export class FontKit {
  public fontProvider!: FontProvider
  private fonts: Dict<FontFile> = {}
  public fontNameList: FontNameInfo[] = []

  public textPr!: FontKitTextPr
  public fontClass: FontClassValues = FontClass.ASCII
  public fontSizeFactor = 1
  private TextPrConstructorFn: TextPrConstructorFn
  private RFontsFn: RFontsFn

  //drawing related
  private transform: Matrix2D = new Matrix2D()
  private ignoreZoom: boolean

  //text measurement
  private measureCache = new Map<string, [number, number]>()
  private measurerCxt: CanvasRenderingContext2D

  private _isDrawEaVert = false

  private _hb_native: Nullable<HarfBuzzNative>

  constructor(opts: FontManagerOpts) {
    this.TextPrConstructorFn = opts.TextPrConstructorFn
    this.RFontsFn = opts.RFontsFn

    // drawing
    this.ignoreZoom = false

    // measurer
    const canvas = document.createElement('canvas')
    this.measurerCxt = canvas.getContext('2d')!
  }

  get isEaVertText() {
    return !!EditorSettings.isSupportEavert && this._isDrawEaVert
  }

  set isEaVertText(val: boolean) {
    this._isDrawEaVert = val
  }

  public init(fontProvider: FontProvider) {
    this.fontProvider = {
      getFontList: fontProvider['getFontList'],
      getFont: fontProvider['getFont'],
      searchFont: fontProvider['searchFont'],
      loadFontMeta: fontProvider['loadFontMeta'],
    }

    const fontsInfo = this.fontProvider.getFontList
      ? this.fontProvider.getFontList()
      : []

    let fontFaceText = ''
    fontsInfo.forEach((fontInfo) => {
      const font = this.addFont(fontInfo)
      if (!font.isLocal && !font.download.noDownload && font.download.url) {
        fontFaceText += `
          @font-face{
            font-family: '${font.fullName}';
            src: url('${font.download.url}');
          }
        `
        const postScriptName = font.getPostScriptName()
        if (postScriptName !== font.fullName) {
          fontFaceText += `
          @font-face{
            font-family: '${postScriptName}';
            src: url('${font.download.url}');
          }
        `
        }
      }
    })

    if (fontFaceText !== '') {
      const newStyle = document.createElement('style')
      newStyle.appendChild(document.createTextNode(fontFaceText))
      document.head.appendChild(newStyle)
    }
  }

  public useHB() {
    if (
      this._hb_native instanceof HarfBuzzNative ||
      EditorSettings.isUseHarfBuzz === false
    ) {
      return Promise.resolve()
    }
    return loadHarfBuzz().then((hb) => {
      this._hb_native = hb
      // if (hb) {
      //   ;(window as any)['getGlyphInfo'] = (text, fontName) =>
      //     getGlyphInfo(hb, text, fontName)
      // }
    })
  }
  public get hb() {
    return this._hb_native
  }

  public addFont(fontInfo: Font): FontFile {
    const fontFile = new FontFile(fontInfo)
    fontFile.checkLocal()
    const fontNames = fontFile.getNameKeys()
    fontNames.forEach((fontName) => {
      if (fontName) {
        const lowerCaseName = fontName.toLowerCase()
        this.fonts[lowerCaseName] = fontFile
      }
    })

    return fontFile
  }

  public getFontFile(name: string = ''): FontFile | undefined {
    const font = this.fonts[name.toLowerCase()]
    if (font) {
      return font
    }
  }

  async searchFont(name: string) {
    try {
      if (this.fontProvider.searchFont) {
        const fontInfo = await this.fontProvider.searchFont(name)
        if (fontInfo) {
          this.addFont(fontInfo)
          refreshPresentation(EditorKit, true)
          EditorKit.invalidAllThumbnailsOfMasters()
        }
      }
    } catch (_e) {}
  }

  public setFontClass(fc: FontClassValues, fontSizeFactor = 1) {
    this.fontClass = fc
    this.fontSizeFactor = fontSizeFactor
  }

  public getTextPr() {
    return this.textPr
  }

  public setTextPr(textPr: FontKitTextPr) {
    this.textPr = textPr
  }

  public setTextPrByData(data: Partial<CTextPrData>) {
    if (this.textPr == null) {
      this.textPr = TextPr.new()
    }
    fontkitTextPrFromJSON(this.textPr, data)
  }
  public setFont(font: FontSimpleInfo) {
    const { TextPrConstructorFn, RFontsFn } = this
    const textPr = new TextPrConstructorFn()
    if (font.fontSize) textPr.fontSize = font.fontSize
    if (font.italic) textPr.italic = font.italic
    if (font.bold) textPr.bold = font.bold
    if (font.fontFamily) {
      const rFonts = new RFontsFn()
      rFonts.setAll(font.fontFamily.name, font.fontFamily.index)
      textPr.rFonts = rFonts
    }
    this.textPr = textPr
  }

  public getHBFont(
    textPr: FontKitTextPr = this.textPr,
    fc: FontClassValues = this.fontClass
  ) {
    const { font } = this.getFont(textPr, fc)
    return font ? getHBFont(font.getFullName()) : undefined
  }

  public getFont(
    textPr: FontKitTextPr = this.textPr,
    fc: FontClassValues = this.fontClass
  ): { font: Required<FontFile>; isFallback: boolean } {
    let font = this._getFontFile(textPr, fc)
    const isFallback = font?.isUsable !== true
    if (isFallback) {
      font = this.getDefaultFont()
    }
    return { font: font as Required<FontFile>, isFallback }
  }

  private _getFontFile(textPr: FontKitTextPr, fc: FontClassValues) {
    let fontObj
    if (textPr.rFonts) {
      switch (fc) {
        case FontClass.ASCII:
          fontObj = textPr.rFonts.ascii
          break
        case FontClass.EastAsia:
          fontObj = textPr.rFonts.eastAsia
          break
        case FontClass.CS:
          fontObj = textPr.rFonts.cs
          break
        case FontClass.HAnsi:
          fontObj = textPr.rFonts.hAnsi
          break
      }
    }
    const name: string = fontObj?.name ?? textPr.fontFamily?.name
    return this.getFontFile(name)
  }
  public ensureHBFont(
    textPr: FontKitTextPr = this.textPr,
    fc: FontClassValues = FontClass.ASCII,
    cb?: () => void
  ) {
    if (this.hb == null) {
      return
    }
    const font = this._getFontFile(textPr, fc)
    if (font) {
      const fontName = font.getFullName()
      const fontUrl = font.download.blobUrl
      if (fontUrl != null) {
        loadHBFont(this.hb, fontName, fontUrl, cb)
      }
    }
  }

  public getDefaultFont() {
    const defaultFontName = generateDefaultFontFamily().name
    const font = this.getFontFile(defaultFontName)
    if (font?.isUsable) {
      return font
    }
    return _fallbackFont
  }

  public setDefaultFontFamily(fontFamilyName: string) {
    setDefaultFontFamilyName(fontFamilyName)
  }
  // drawing
  public setTransform(matrix: Matrix2D = new Matrix2D()) {
    this.transform.reset(matrix)
  }

  public resetTransform() {
    this.transform.reset()
  }

  public drawString(
    text: string,
    ctx: CanvasRenderingContext2D,
    _x: number,
    _y: number,
    drawOptions: { isStrokeText: boolean; isFillText: boolean },
    zoom?: number
  ): { x: number; y: number } {
    const textPr: FontKitTextPr = this.textPr
    const fc = this.fontClass
    const fontSizeFactor: number = this.fontSizeFactor
    const { font } = this.getFont(textPr, fc)

    const zoomValue = zoom != null ? zoom : this.getZoomValue()
    const fontSize = calcFontSize(textPr.fontSize!, fontSizeFactor)
    const m = this.extractMatrix(_x, _y)
    const { x, y } = m
    let { r } = m

    let italic = textPr.italic!

    const oldTextBaseline = ctx.textBaseline
    // 旋转文本
    if (this.isEaVertText) {
      const isCJK = isCJKChar(text)
      const vhea = font.meta.vhea
      if (!vhea && !isCJK) {
        ctx.textBaseline = 'alphabetic'
      } else {
        ctx.textBaseline = 'middle'
      }
      if (isCJK) {
        r += (-90 * Math.PI) / 180
        if (italic) {
          italic = false
          ctx.translate(x, y)
          ctx.transform(1, 0.25, 0, 1, 0, 0)
          ctx.translate(-x, -y)
        }
      }
    }

    if (r !== 0) {
      ctx.translate(x, y)
      ctx.rotate(r)
      ctx.translate(-x, -y)
    }
    const ctxScaleValue = zoomValue / 100
    ctx.scale(ctxScaleValue, ctxScaleValue)
    // only actually set ctx.font when font style is different from last time
    setCtxFont(
      ctx,
      fontSize * BrowserInfo.PixelRatio,
      textPr.bold!,
      italic,
      font
    )

    const zx = (x / ctxScaleValue) >> 0
    const zy = (y / ctxScaleValue) >> 0

    if (BrowserInfo.isMobile) {
      // 手机浏览器绘制文本边框线有异常，故后绘制文字填充覆盖异常边框渲染的部分
      if (drawOptions.isStrokeText === true) {
        ctx.strokeText(text, zx, zy)
      }
      if (drawOptions.isFillText === true) {
        ctx.fillText(text, zx, zy)
      }
    } else {
      if (drawOptions.isFillText === true) {
        ctx.fillText(text, zx, zy)
      }
      if (drawOptions.isStrokeText === true) {
        ctx.strokeText(text, zx, zy)
      }
    }

    ctx.textBaseline = oldTextBaseline
    return { x, y }
  }

  public executeIgnoreZoom(procedure: () => void) {
    this.executeWithDefaults(procedure, {
      ignoreZoom: true,
      ignoreFontSize: false,
    })
  }

  public executeWithDefaults(
    procedure: () => void,
    opts: { ignoreZoom?: boolean; ignoreFontSize?: boolean } = {
      ignoreZoom: true,
      ignoreFontSize: true,
    }
  ) {
    let preIgnoreZoom, preFontSizeFactor
    if (opts.ignoreZoom === true) {
      preIgnoreZoom = this.ignoreZoom
      this.ignoreZoom = true
    }
    if (opts.ignoreFontSize === true) {
      preFontSizeFactor = this.fontSizeFactor
      this.fontSizeFactor = 1
    }

    if (typeof procedure === 'function') {
      procedure()
    }

    if (opts.ignoreZoom === true) {
      this.ignoreZoom = preIgnoreZoom
    }
    if (opts.ignoreFontSize === true) {
      this.fontSizeFactor = preFontSizeFactor
    }
  }

  private getZoomValue(): number {
    if (this.ignoreZoom === true) {
      return 100
    }

    let zoomValue = ZoomValue.value

    const isInShowMod = !!EditorKit.editorUI.showManager.isInShowMode
    if (isInShowMod) {
      zoomValue = EditorKit.editorUI.showManager.zoomValue() ?? ZoomValue.value
    }
    return zoomValue
  }

  private extractMatrix(x: number, y: number) {
    const { tx, ty, sx, sy, shx, shy } = this.transform

    let rad = Math.atan2(shy, sx)
    if (rad < 0) rad += Math.PI * 2

    let factor = this.transform.factorForInvert()
    if (0.001 > factor && factor >= 0) {
      factor = 0.001
    } else if (-0.001 < factor && factor < 0) {
      factor = -0.001
    }

    const trX = (tx * sy - shx * ty) / factor
    const trY = (ty * sx - shy * tx) / factor

    const matrix = makeXYTransformMatrixFrom(rad, trX, trY)

    const _x = (MatrixUtils.XFromPoint(matrix, x, y) + 0.5) >> 0
    const _y = (MatrixUtils.YFromPoint(matrix, x, y) + 0.5) >> 0

    return {
      x: _x,
      y: _y,
      r: rad,
    }
  }

  //measurer
  public measure(
    char: string,
    textPr: FontKitTextPr = this.textPr,
    fontClass: FontClassValues = this.fontClass,
    fontSizeFactor: number = this.fontSizeFactor
  ) {
    const { font } = this.getFont(textPr, fontClass)
    const widthIn72px = _measureIn72px(
      this.measurerCxt,
      char,
      font,
      !!textPr.bold,
      !!textPr.italic
    )
    const widthInMM = _textWidthInMM(
      widthIn72px,
      textPr.fontSize!,
      fontSizeFactor
    )

    return widthInMM
  }

  /**
   * 计算 charCode 对应字符的宽度
   * 这里返回的是根据字符绝对宽度计算出的毫米值
   *
   * @param {number} charCode
   * @param {FontKitTextPr} textPr
   * @param {number} fontClass
   * @param {number} fontFactor
   * @returns number
   * @memberof FontManager
   */
  public MeasureCode(
    charCode: number | string,
    textPr: FontKitTextPr = this.textPr,
    fontClass: FontClassValues = this.fontClass,
    fontSizeFactor: number = this.fontSizeFactor
  ) {
    const char =
      typeof charCode === 'number' ? String.fromCodePoint(charCode) : charCode
    const { font, isFallback } = this.getFont(textPr, fontClass)
    let cacheKey = `${char}/${font.fullName}`

    const notCJK = !isCJKChar(charCode)
    if (notCJK) {
      if (textPr.italic) {
        cacheKey += 'i'
      }
      if (textPr.bold) {
        cacheKey += 'b'
      }
    }

    let measuredWidth
    let advanceHeight
    if (this.measureCache.has(cacheKey)) {
      const [width, height] = this.measureCache.get(cacheKey)!
      measuredWidth = width
      advanceHeight = height
    } else {
      // 缓存只记录 canvas 计算结果
      measuredWidth = _measureIn72px(
        this.measurerCxt,
        char,
        font,
        notCJK ? !!textPr.bold : false,
        notCJK ? !!textPr.italic : false
      )

      const { ascender, descender, lineGap } = font.meta.hhea

      const lineHeight =
        Math.abs(ascender) + Math.abs(descender) + Math.abs(lineGap)
      const lineHeightFactor = (1.2 * 72) / lineHeight
      advanceHeight =
        Math.abs(ascender * lineHeightFactor) +
        Math.abs(descender * lineHeightFactor)
      if (font.isUsable) {
        this.measureCache.set(cacheKey, [measuredWidth, advanceHeight])
      }
    }

    const widthInMM = _textWidthInMM(
      measuredWidth,
      textPr.fontSize!,
      fontSizeFactor
    )
    return { width: widthInMM, isFallback }
  }

  public AdjustMeasure(
    advanceWidth: number,
    unitsPerEm: number,
    textPr: FontKitTextPr = this.textPr,
    fontSizeFactor: number = this.fontSizeFactor
  ) {
    return (
      (advanceWidth * textPr.fontSize! * fontSizeFactor * 25.4) /
      (unitsPerEm * 72)
    )
  }
  public getMetrics(textPr: FontKitTextPr = this.textPr) {
    return getFontMetrics(this, textPr, this.fontClass)
  }
}

// function toHex(r: number, g: number, b: number) {
//   return ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1)
// }

function calcFontSize(fontSize: number, fontSizeFactor: number) {
  return (fontSize * fontSizeFactor) / 0.75
}

function makeXYTransformMatrixFrom(
  rad: number,
  tx: number,
  ty: number
): IMatrix2D {
  const m = {
    sx: 1,
    shy: 0,
    shx: 0,
    sy: 1,
    tx: 0,
    ty: 0,
  }
  // rotate
  const cw = Math.cos(-rad)
  const sw = Math.sin(-rad)

  const a = m.sx * cw - m.shx * sw
  const b = m.shy * cw - m.sy * sw
  const c = m.shx * cw + m.sx * sw
  const d = m.sy * cw + m.shy * sw

  m.sx = a
  m.shy = b
  m.shx = c
  m.sy = d

  //translate
  const e = tx * m.sx + ty * m.shx + m.tx
  const f = tx * m.shy + ty * m.sy + m.ty

  m.tx = e
  m.ty = f

  return m
}

export function getFontMetrics(
  fontKit: FontKit,
  textPr: FontKitTextPr,
  fc: FontClassValues,
  fontSizeFactor?: number
) {
  const { font } = fontKit.getFont(textPr, fc)
  const { hhea, vhea } = font.meta
  fontSizeFactor = fontSizeFactor ?? fontKit.fontSizeFactor
  const fontSize: number = textPr.fontSize! * fontSizeFactor

  const { ascender, descender, lineGap } =
    fontKit.isEaVertText && vhea ? vhea : hhea

  const lineHeight =
    Math.abs(ascender) + Math.abs(descender) + Math.abs(lineGap)
  const lineHeightFactor = (1.2 * fontSize) / lineHeight

  const ascentMM = ascender * lineHeightFactor * Factor_pt_to_mm

  const descentMM = descender * lineHeightFactor * Factor_pt_to_mm
  const lineGapMM = lineGap * lineHeightFactor * Factor_pt_to_mm

  // const textHeight =
  //   Math.abs(ascentMM) + Math.abs(descentMM) + Math.abs(lineGapMM)
  const textHeight = 1.2 * fontSize * Factor_pt_to_mm
  const vertTextYOffset = Math.abs(descentMM) + lineGapMM / 2

  return {
    vertTextYOffset,
    ascender: ascentMM,
    descender: descentMM,
    textHeight,
    lineGap: lineGapMM,
  }
}

export function getTextHeight(
  fontKit: FontKit,
  textPr: FontKitTextPr,
  fc: FontClassValues
) {
  return getFontMetrics(fontKit, textPr, fc).textHeight
}

// helpers
function _measureIn72px(
  context: CanvasRenderingContext2D,
  text: string,
  font: FontFile,
  bold: boolean,
  italic: boolean
) {
  setMeasureCtxFont(context, 72, bold, italic, font)

  const { width } = context.measureText(text)
  return width
}

function isValidFontName(fontName: string): boolean {
  // IOS 15 使用此字体导致 webview 崩溃
  return fontName !== '-apple-system'
}

function getFontString(
  fontSize: number,
  bold: boolean,
  italic: boolean,
  font: FontFile
): string {
  let fontName = font.fullName
  const { family, subFamily } = font.getFamilyName()
  const postScriptName = font.getPostScriptName()
  const styles = subFamily.toLocaleLowerCase().split(' ')
  if (styles.includes('bold')) {
    fontName = family
    bold = true
  }
  if (styles.includes('italic')) {
    fontName = family
    italic = true
  }
  const names = [font.fullName, postScriptName, fontName, family]
  const nameString = names
    .reduce((ret, name) => {
      if (name && isValidFontName(name)) {
        ret += `"${name}", `
      }
      return ret
    }, '')
    .slice(0, -2)
  let fontString = `${fontSize}px ${nameString}`
  if (italic) fontString = `italic ${fontString}`
  if (bold) fontString = `bold ${fontString}`
  return fontString
}

function _textWidthInMM(
  widthIn72px: number,
  fontSize: number,
  fontSizeFactor: number
) {
  const calcedSize = calcFontSize(fontSize, fontSizeFactor)
  const width = (widthIn72px * calcedSize) / 72

  return (width * 25.4) / 96
}

function makeSetCtxFontStyleFn() {
  let preStyleStr: string | undefined

  return (
    ctx: CanvasRenderingContext2D,
    fontSize: number,
    bold: boolean,
    italic: boolean,
    font: FontFile
  ) => {
    const curFontStyle = ctx.font
    if (curFontStyle !== preStyleStr) {
      preStyleStr = getFontString(fontSize, bold, italic, font)
      ctx.font = preStyleStr
    }
  }
}

const setCtxFont = makeSetCtxFontStyleFn()
const setMeasureCtxFont = makeSetCtxFontStyleFn()
