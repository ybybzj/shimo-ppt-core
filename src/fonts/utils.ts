// 生成字体 id 的编码参数
const xor = 234
const hex = 25

export function encryptFontFamily(str: string) {
  const chars: string[] = []
  for (let i = 0; i < str.length; i++) {
    let charCode = str.charCodeAt(i)
    charCode = (charCode * 1) ^ xor
    chars.push(charCode.toString(hex))
  }

  const splitStr = String.fromCodePoint(hex + 97)
  const result = chars.join(splitStr)
  return result
}

export const decryptFontId = (str: string) => {
  let strCharList: string[] = []
  const chars: string[] = []
  const splitStr = String.fromCodePoint(hex + 97)
  strCharList = str.split(splitStr)

  for (let i = 0; i < strCharList.length; i++) {
    let charCode = parseInt(strCharList[i], hex)
    charCode = (charCode * 1) ^ xor
    const strChar = String.fromCodePoint(charCode)
    chars.push(strChar)
  }
  const result = chars.join('')
  return result
}

// http://www.rikai.com/library/kanjitables/kanji_codes.unicode.shtml
const jp_Hiragana_Katakana = /[\u3040-\u309f\u30a0-\u30ff]/

// https://stackoverflow.com/questions/40099273/how-to-use-regular-expression-to-validate-chinese-input
const cjk = /[\u3000-\u303F\u3400-\u4DBF\u4E00-\u9FFF]/
const cnPunctuation =
  /[\u3002|\uff1f|\uff01|\uff0c|\u3001|\uff1b|\uff1a|\u201c|\u201d|\u2018|\u2019|\uff08|\uff09|\u300a|\u300b|\u3008|\u3009|\u3010|\u3011|\u300e|\u300f|\u300c|\u300d|\ufe43|\ufe44|\u3014|\u3015|\u2026|\u2014|\uff5e|\ufe4f|\uffe5]/

export function isCJKChar(charCode: string | number) {
  const char =
    typeof charCode === 'number' ? String.fromCodePoint(charCode) : charCode
  return (
    cjk.test(char) ||
    cnPunctuation.test(char) ||
    jp_Hiragana_Katakana.test(char)
  )
}
