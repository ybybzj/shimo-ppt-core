import { Dict, Nullable } from '../../liber/pervasive'
import { EditorSettings } from '../core/common/EditorSettings'
import { fetch_ } from '../lib/utils/fetch'
import {
  HarfBuzzBlob,
  HarfBuzzFace,
  HarfBuzzFont,
} from '../lib/utils/hb/HarfBuzz'
import { HarfBuzzNative } from '../lib/utils/hb/HarfBuzzNative'

const HB_FONT_LOADING = 1
const HB_FONT_LOADED = 2
const HB_FONT_LOAD_FAILED = 0
const HB_FONT_NOT_LOAD = -1

type CacheItem =
  | { status: typeof HB_FONT_NOT_LOAD }
  | { status: typeof HB_FONT_LOADING; promise: Promise<void> }
  | { status: typeof HB_FONT_LOAD_FAILED }
  | {
      status: typeof HB_FONT_LOADED
      font: HarfBuzzFont
    }

let hb_fonts_cache: Dict<CacheItem> = Object.create(null)

export function loadHBFont(
  hb: HarfBuzzNative,
  fontName: string,
  fontUrl: string,
  callback?: () => void
): Promise<void> {
  if (EditorSettings.isUseHarfBuzz === false) {
    return Promise.resolve()
  }

  const font = hb_fonts_cache[fontName] ?? { status: HB_FONT_NOT_LOAD }

  switch (font.status) {
    case HB_FONT_LOADED:
    case HB_FONT_LOAD_FAILED: {
      return Promise.resolve()
    }
    case HB_FONT_LOADING: {
      return font.promise
    }
    case HB_FONT_NOT_LOAD: {
      const promise = fetch_(fontUrl)
        .then((response) => {
          if (response.ok) {
            return response.arrayBuffer()
          } else {
            hb_fonts_cache[fontName] = { status: HB_FONT_LOAD_FAILED }
            console.warn(`fetch font [${fontName}] failed!`, fontUrl)
            return undefined
          }
        })
        .then((blob) => {
          if (blob != null) {
            const blobArrBuffer = new Uint8Array(blob)
            const fontBlob = new HarfBuzzBlob(hb, blobArrBuffer)
            const face = new HarfBuzzFace(hb, fontBlob, 0)
            const font = new HarfBuzzFont(hb, face)

            hb_fonts_cache[fontName] = {
              status: HB_FONT_LOADED,
              font,
            }
            face.destroy()
            fontBlob.destroy()
            if (callback) {
              callback()
              // console.log(`loadHbFont callback invoked!`)
            }
          }
        })
      hb_fonts_cache[fontName] = {
        status: HB_FONT_LOADING,
        promise,
      }
      return promise
    }
  }
}

export function getHBFont(fontName: string): Nullable<HarfBuzzFont> {
  const fontItem = hb_fonts_cache[fontName]
  return fontItem?.status === HB_FONT_LOADED ? fontItem.font : undefined
}

export function clearHBFontCache() {
  Object.keys(hb_fonts_cache).forEach((k) => {
    const font = hb_fonts_cache[k]
    if (font?.status === HB_FONT_LOADED) {
      font.font.destroy()
    }
  })
  hb_fonts_cache = Object.create(null)
}
