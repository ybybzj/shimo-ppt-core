import { EntityRegistry } from '../core/EntityRegistry'
import { EditChangesStack } from '../core/changes/EditChangesStack'

import { ImageLoadManager } from '../images/ImageLoadManager'
import { TextInputManager } from '../ui/textInputManager'
import { FontKit } from '../fonts/FontKit'
import { FontLoader } from '../fonts/FontLoader'
import { PPTPosition } from '../collaboration/PPTPosition'

let _pptPosition
let _entityRegistry
let _changesStack
let _fontKit
let _fontLoader
let _imageLoader
let _inputManager

export const EditorUtil: {
  pptPosition: PPTPosition
  entityRegistry: EntityRegistry
  ChangesStack: EditChangesStack
  FontKit: FontKit
  FontLoader: FontLoader

  imageLoader: ImageLoadManager
  inputManager: TextInputManager
} = {
  get pptPosition(): PPTPosition {
    return _pptPosition
  },

  get entityRegistry(): EntityRegistry {
    return _entityRegistry
  },

  get ChangesStack(): EditChangesStack {
    return _changesStack
  },

  get FontKit() {
    return _fontKit
  },

  get FontLoader() {
    return _fontLoader
  },

  get imageLoader() {
    return _imageLoader
  },

  get inputManager() {
    return _inputManager
  },

  set inputManager(arg) {
    _inputManager = arg
  },
}

export function initEditorGlobals({
  pptPosition,
  entityRegistry,
  changesStack,
  fontKit,
  fontLoader,
  imageLoader,
}: {
  pptPosition: PPTPosition
  entityRegistry: EntityRegistry
  changesStack: EditChangesStack
  fontKit: FontKit
  fontLoader: FontLoader
  imageLoader: ImageLoadManager
}) {
  _pptPosition = pptPosition
  _entityRegistry = entityRegistry
  _changesStack = changesStack
  _fontKit = fontKit
  _fontLoader = fontLoader
  _imageLoader = imageLoader
}
