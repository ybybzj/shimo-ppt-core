import { randomStr } from '../common/guid'
// import { Dict } from '../../liber/pervasive'
// import { toInt } from '../common/utils'
export const RefIdPrefixes = {
  Layout: 'l',
  Notes: 'n',
  NotesMaster: 'nm',
  Slide: 's',
  SlideMaster: 'sm',
  Shape: 'sp',
  Theme: 'th',
  Comment: 'cm',
} as const

// function fillForLength(s: string, n: number, length: number): string {
//   s = s.trim()
//   if (s.length > length) {
//     throw new Error(`[fillForLength]input string is too long`)
//   }
//   let ns = Math.abs(toInt(n)) + ''
//   const toFillLen = length - ns.length - s.length
//   if (toFillLen < 0) {
//     throw new Error(`[fillForLength]inputs are too long`)
//   }
//
//   for (let i = 0; i < toFillLen; i++) {
//     ns = '0' + ns
//   }
//
//   return `${s}${ns}`
// }
//
//
// function makeID(prefix: string, n: number): string {
//   return fillForLength(prefix, n, REFID_LENGTH)
// }
//
// const REFID_Reg = /^([a-zA-Z]+)(\d+)$/ //xxdddd
//
// function splitRefId(rId: string): { prefix: string; n: number } {
//   rId = rId.trim()
//   if (rId.length !== REFID_LENGTH) {
//     throw new Error(`[splitRefId]the length of refId should be ${REFID_LENGTH}`)
//   }
//   const match = rId.match(REFID_Reg)
//   if (!match) {
//     throw new Error(`[splitRefId]invalid refId format`)
//   }
//
//   return {
//     prefix: match[1],
//     n: Number(match[2]),
//   }
// }

const REFID_LENGTH = 8
function _make(prefixLen: number) {
  return randomStr(REFID_LENGTH - prefixLen)
}

function makeRefId(prefix: string): string {
  const prefixLen = prefix.length
  let rest = _make(prefixLen)
  while (rest.indexOf(prefix) === 0) {
    rest = _make(prefixLen)
  }

  return prefix + rest
}

class RefIdCounter {
  newIDBy(prefix: string): string {
    const result = makeRefId(prefix)
    return result
  }
}

export const gRefIdCounter = new RefIdCounter()
