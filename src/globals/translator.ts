import { Dict } from '../../liber/pervasive'

/** @constructor */
class Translator {
  translateResults: Dict<string>
  constructor() {
    this.translateResults = {}
  }

  init(map) {
    this.translateResults = map || {}
  }

  getValue(key) {
    const val = this.translateResults[key]

    return typeof val === 'string' && val.length > 0 ? val : key
  }
}

export const translator = new Translator()
