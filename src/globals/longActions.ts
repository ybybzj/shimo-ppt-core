import { Dict } from '../../liber/pervasive'
import { EditActionFlag } from '../core/EditActionFlag'

const actions: Dict<1> = Object.create(null)

export const longActions = {
  start(actionFlag: EditActionFlag) {
    if (actions[actionFlag] == null) {
      actions[actionFlag] = 1
    }
  },
  end(actionFlag: EditActionFlag) {
    if (actions[actionFlag] != null) {
      delete actions[actionFlag]
    }
  },

  isEmpty() {
    return Object.keys(actions).length <= 0
  },

  get() {
    return Object.keys(actions)
  },
}
