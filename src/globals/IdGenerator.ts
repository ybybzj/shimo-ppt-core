class IdGenerator {
  valueOfIdCounterLoad: number
  constructor() {
    this.valueOfIdCounterLoad = 0
  }

  newId() {
    this.valueOfIdCounterLoad++
    return '' + this.valueOfIdCounterLoad
  }

  clear() {
    this.valueOfIdCounterLoad = 0
  }
}
export const idGenerator = new IdGenerator()
