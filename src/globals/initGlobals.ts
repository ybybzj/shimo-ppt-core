import { initEditorGlobals } from './editor'
import { gEntityRegistry } from '../core/EntityRegistry'
import { EditChangesStack } from '../core/changes/EditChangesStack'

import { ImageLoadManager } from '../images/ImageLoadManager'
import { FontKit } from '../fonts/FontKit'
import { FontLoader } from '../fonts/FontLoader'
import { TextPr } from '../core/textAttributes/TextPr'
import { RFonts } from '../core/textAttributes/RFonts'
import { PPTPosition } from '../collaboration/PPTPosition'
import { calculateRenderingState } from '../core/calculation/calculate/presentation'

export function initGlobals() {
  const fontKit = new FontKit({
    TextPrConstructorFn: TextPr,
    RFontsFn: RFonts,
  })
  const editorGlobals = {
    pptPosition: new PPTPosition(),
    entityRegistry: gEntityRegistry,
    changesStack: new EditChangesStack(calculateRenderingState),
    fontKit: fontKit,
    fontLoader: new FontLoader({ fontManager: fontKit }),
    imageLoader: new ImageLoadManager(),
  }
  initEditorGlobals(editorGlobals)
}
