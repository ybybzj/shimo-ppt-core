import {
  getPointerEventPos,
  POINTER_BUTTON,
  preventDefault,
  stopEvent,
  TouchEPS,
} from '../../../common/dom/events'
import {
  MultiTouchEvtHandler,
  MULTI_TOUCH_EVT_TYPE,
} from '../../../common/dom/multiTouchEvent'
import { FocusTarget } from '../../../core/common/const/drawing'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { isAudioImage } from '../../../core/utilities/shape/image'
import {
  Evt_onReadOnlyModeFocusEmpty,
  Evt_viewAttachment,
  Evt_viewSourceImage,
} from '../../../editor/events/EventNames'
import { ImageLikeTypes } from '../../../editor/proto'
import { EditorUtil } from '../../../globals/editor'
import { StatusOfLoadableImage } from '../../../images/utils'
import { InstanceType } from '../../../core/instanceTypes'
import { genAudioAction } from '../../transitions/AudioManager'
import { EditorUI } from '../EditorUI'
import { handleMouseDown, handleMouseUp } from './pointers'
import { cancelEmitMenu, convertPixToInnerMMPos } from './utils'

let timerId: any
let touchStartEvent: PointerEvent | undefined

const multiTouchHandler = new MultiTouchEvtHandler()

export type EditorTouchAction = 'tap' | 'press' | 'longPress'
/** 识别本次操作基于何种按压模式(例如 tap + move = swipe, press + move = drag) */
let touchStartX = 0
let touchStartY = 0
let touchMoveX = 0
let touchMoveY = 0
/** mousedown 延迟执行的标记 */
let needHandleTouchDown = false

const PRESS_DURATION = 200 // ms
const LONG_PRESS_DURATION = 600 // ms

const resetTouchMonitor = () => {
  if (timerId != null) {
    clearTimeout(timerId)
    timerId = null
  }
}

const resetTouchMode = (editorUI: EditorUI) => {
  editorUI.editorHandler.touchAction = 'tap'
}

/** 根据静止按压的时间依次调整本次操作为滑动/拖拽 */
const monitorTouchMode = (
  editorUI: EditorUI,
  cb: (editorUI: EditorUI, mode: EditorTouchAction) => void
) => {
  const next = (mode: EditorTouchAction) => cb(editorUI, mode)
  resetTouchMonitor()
  timerId = setTimeout(() => {
    next('press')
    timerId = setTimeout(() => {
      next('longPress')
      timerId = null
    }, LONG_PRESS_DURATION)
  }, PRESS_DURATION)
}

function handleTouchDown(editorUI: EditorUI) {
  const pos = convertPixToInnerMMPos(editorUI, touchStartX, touchStartY)
  checkTriggerViewImage(editorUI, pos)
  const { renderingController, pointerEvtInfo: pointerEvt } = editorUI
  const ret = renderingController.checkPlaceHolder(pos, 'mouseDown')
  if (ret) {
    stopEvent(touchStartEvent)
    return
  }
  const handler = editorUI.editorHandler

  // 双击退出多选态(Todo: 单元格以外)
  if (pointerEvt.clicks > 1 && handler.isMultiSelectState) {
    handler.isMultiSelectState = false
    handler.resetSelectionState()
  }

  handleMouseDown(editorUI, pointerEvt, pos.x, pos.y)
  checkTriggerSelectScroll(editorUI)
  needHandleTouchDown = false
}

function resetTouchDownHandler() {
  needHandleTouchDown = false
  touchStartEvent = undefined
}

function saveTouchDownHandler(e: PointerEvent) {
  needHandleTouchDown = true
  touchStartEvent = e
}

function checkNeedHandleTouchDown(editorUI: EditorUI) {
  if (needHandleTouchDown) {
    handleTouchDown(editorUI)
    resetTouchDownHandler()
  }
}

export function handleEditorTouchStart(editorUI: EditorUI, e: PointerEvent) {
  if (false === editorUI.editorKit.isEditorUIInitComplete) return
  editorUI.checkMobileInput() // 只要能触发 touch 则应该都执行焦点检查
  stopDemostration(editorUI)
  preventDefault({ e })
  if (EditorUtil.inputManager && EditorUtil.inputManager.checkAndClearIME()) {
    return
  }

  if (editorUI.presentation.isNotesFocused !== true) {
    const handled = multiTouchHandler.onDown(e)
    if (handled === true) {
      resetTouchMode(editorUI)
      resetTouchMonitor()
      return
    }
  }

  const { renderingController, presentation } = editorUI
  presentation.setFocusTargetType(FocusTarget.EditorView)
  if (editorUI.showManager.isInShowMode) return false

  const pointerEvt = editorUI.pointerEvtInfo
  pointerEvt.onDown(e, true)

  editorUI.isLockMouse = true
  if (renderingController.isEmptySlides && presentation.canEdit()) {
    presentation.addNextSlide()
    return
  }
  if (presentation.currentSlideIndex === -1) {
    return
  }

  // 排除所有一次性操作后再开始计时
  resetTouchMode(editorUI)
  monitorTouchMode(editorUI, handleTouchModeChange)
  ;[touchStartX, touchStartY] = [pointerEvt.x, pointerEvt.y]
  ;[touchMoveX, touchMoveY] = [pointerEvt.x, pointerEvt.y]

  saveTouchDownHandler(e)
  presentation.setFocusTargetType(FocusTarget.EditorView)
}

export function handleEditorTouchMove(editorUI: EditorUI, e: PointerEvent) {
  if (false === editorUI.editorKit.isEditorUIInitComplete) return

  // 优先响应多指
  if (editorUI.presentation.isNotesFocused !== true) {
    const handleInfo = multiTouchHandler.onMove(e)
    if (handleInfo.handled === true) {
      const { evtInfo } = handleInfo
      switch (evtInfo.type) {
        case MULTI_TOUCH_EVT_TYPE.PinchZoomIn: {
          editorUI.zoomIn(true)
          break
        }
        case MULTI_TOUCH_EVT_TYPE.PinchZoomOut: {
          editorUI.zoomOut(true)
          break
        }
        case MULTI_TOUCH_EVT_TYPE.Panning: {
          break
        }
        case MULTI_TOUCH_EVT_TYPE.None: {
          break
        }
      }
      return
    }
  }

  preventDefault({ e })

  const { pointerEvtInfo: pointerEvt } = editorUI
  const pos = getPointerEventPos(e)
  if (!pos) return
  const [x, y] = pos
  if (timerId) {
    const [dx, dy] = [Math.abs(touchStartX - x), Math.abs(touchStartY - y)]
    if (hasMoved(dx, dy, TouchEPS)) {
      resetTouchMonitor()

      cancelEmitMenu()
    } else {
      return
    }
  }

  const handler = editorUI.editorHandler
  const touchMode = handler.touchAction ?? 'tap'

  // Swipe
  if (touchMode === 'tap') {
    // 这里为了计算 delta scroll 需要保存上次的位置判断每次增量sceollBy/goNextPage
    pointerEvt.onMove(e)
    const [deltaX, deltaY] = [touchMoveX - x, touchMoveY - y]
    editorUI.scrollByDelta(deltaX, deltaY, false)
    ;[touchMoveX, touchMoveY] = [x, y]
    resetTouchDownHandler()
    return
  }

  // 原 mousemove
  /* touchMode === 'press' || touchMode === 'longPress' */
  checkNeedHandleTouchDown(editorUI)
  pointerEvt.onMove(e)
  if (editorUI.showManager.isInShowMode) return false
  editorUI._handlePointerMove()
}

export function handleEditorTouchEnd(editorUI: EditorUI, e: PointerEvent) {
  if (false === editorUI.editorKit.isEditorUIInitComplete) return

  if (editorUI.presentation.isNotesFocused !== true) {
    if (multiTouchHandler.onUp(e)) {
      return
    }
  }

  const { pointerEvtInfo: pointerEvt, renderingController } = editorUI
  if (!pointerEvt.isLocked) return

  if (editorUI.showManager.isInShowMode) {
    preventDefault({ e })
    return false
  }

  resetTouchMonitor()
  pointerEvt.onUp(e)

  if (
    renderingController.isEmptySlides ||
    editorUI.presentation.currentSlideIndex === -1
  ) {
    return
  }

  checkNeedHandleTouchDown(editorUI)
  const pos = convertPixToInnerMMPos(editorUI, pointerEvt.x, pointerEvt.y)
  editorUI.mouseCursor.unLock()
  editorUI.isLockMouse = false
  checkClearSelectScroll(editorUI)

  if (renderingController.checkPlaceHolder(pos, 'mouseUp') === true) {
    const handler = editorUI.editorHandler
    handler.changeToState(InstanceType.UIInitState, handler)
    return
  }
  handleMouseUp(editorUI, pointerEvt, pos.x, pos.y)
  checkNeedReloadErrImages(editorUI, pos)
  editorUI.checkMobileInput()
  editorUI.renderingController.clearAdornment()
  resetTouchMode(editorUI)
}

function handleTouchModeChange(editorUI: EditorUI, mode: EditorTouchAction) {
  editorUI.editorHandler.touchAction = mode
  // switch states case
}

export function hasMoved(dx: number, dy: number, eps: number) {
  return Math.abs(dx) > eps || Math.abs(dy) > eps
}

export function stopDemostration(editorUI: EditorUI) {
  if (editorUI.transitionManager.isPlaying()) {
    editorUI.transitionManager.end()
  }

  if (editorUI.animationPreviewer.isPlaying()) {
    editorUI.animationPreviewer.end('StopAtEnd')
  }
}

/** 只读模式时单机图片则触发查看大图/附件/音频 */
export function checkTriggerViewImage(
  editorUI: EditorUI,
  pos?: { x: number; y: number }
) {
  if (!EditorSettings.isViewMode) return
  const { pointerEvtInfo: pointerEvt, editorKit } = editorUI
  const { x: pixX, y: pixY, button } = pointerEvt
  if (POINTER_BUTTON.Left === button) {
    const { x, y } = pos ?? convertPixToInnerMMPos(editorUI, pixX, pixY)
    const info = editorKit.getHittestInfoByMousePosition(x, y, false)
    const imgInfo = info?.['image']
    if (imgInfo) {
      switch (imgInfo['type']) {
        case ImageLikeTypes.attachment:
          editorUI.editorKit.emitEvent(Evt_viewAttachment, imgInfo)
          break
        case ImageLikeTypes.audio:
          const curSlide = editorUI.editorHandler.getCurrentSlide()
          const sp = curSlide
            .getSpTree()
            .find((sp) => sp.refId === imgInfo['refId'])
          if (sp && isAudioImage(sp)) {
            const audioAction = genAudioAction(curSlide, sp, 'toggle', true)
            if (audioAction) {
              editorKit.emitAudioAction(audioAction)
              cancelEmitMenu()
            }
          }
          break
        case ImageLikeTypes.image:
        default:
          const status =
            EditorUtil.imageLoader.imagesMap[imgInfo['imgInfo']['url']]?.Status
          if (status !== StatusOfLoadableImage.Error) {
            editorUI.editorKit.emitEvent(Evt_viewSourceImage, imgInfo)
          }
          break
      }
      return
    }
  }
  // Todo: 只读模式目前没有选中态机制, 只能通过这种方式通知外部状态变化
  editorUI.editorKit.emitEvent(Evt_onReadOnlyModeFocusEmpty)
}

/** 光标按下后移动的过程中，应该 scroll 跟随 */
export function checkTriggerSelectScroll(editorUI: EditorUI) {
  const handler = editorUI.editorHandler
  if (
    handler.curState.instanceType !== InstanceType.UIInitState &&
    -1 === editorUI.selectScrollTimerId
  ) {
    editorUI.selectScrollTimerId = setInterval(
      editorUI.onRectSelectcroll,
      20
    ) as unknown as number
  }
}

export function checkClearSelectScroll(editorUI: EditorUI) {
  if (-1 !== editorUI.selectScrollTimerId) {
    clearInterval(editorUI.selectScrollTimerId)
    editorUI.selectScrollTimerId = -1
  }
}

export function checkNeedReloadErrImages(
  editorUI: EditorUI,
  pos: { x: number; y: number }
) {
  if (editorUI.editorHandler.checkIsHitOnErrorImage(pos.x, pos.y)) {
    EditorUtil.imageLoader.reloadErrorImages()
  }
}
