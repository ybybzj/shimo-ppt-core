import {
  getPointerEventPos,
  stopEvent,
  TouchEPS,
} from '../../../common/dom/events'
import { FocusTarget } from '../../../core/common/const/drawing'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { InstanceType } from '../../../core/instanceTypes'
import { NotesComponent } from '../components/notes'
import { EditorUI } from '../EditorUI'
import { hasMoved } from './touches'
import {
  handleMouseDownForNotes,
  handleMouseMoveForNotes,
  handleMouseUpForNotes,
} from './pointers'

// notes 与画布区域共用 pointerInfo(lock, cursor)
// touchMode 不共用，极端情况可能状态异常
// 但总体 notes 区域与 editor 逻辑差异较大, 因此拆分处理, 也方便后续管理,

let timerId: any
let touchStartEvent: PointerEvent | undefined

export type EditorTouchMode = 'tap' | 'press' | 'longPress'
/** 识别本次操作基于何种按压模式(例如 tap + move = swipe, press + move = drag) */
let touchStartX = 0
let touchStartY = 0
// let touchMoveX = 0
let touchMoveY = 0
/** mousedown 延迟执行的标记 */
let needHandleTouchDown = false

const PRESS_DURATION = 200 // ms
const LONG_PRESS_DURATION = 600 // ms

const resetTouchMonitor = () => {
  if (timerId != null) {
    clearTimeout(timerId)
    timerId = null
  }
}

const resetTouchMode = (editorUI: EditorUI) => {
  editorUI.editorHandler.touchAction = 'tap'
}

/** 根据静止按压的时间依次调整本次操作为滑动/拖拽 */
const monitorTouchMode = (
  editorUI: EditorUI,
  cb: (editorUI: EditorUI, mode: EditorTouchMode) => void
) => {
  const next = (mode: EditorTouchMode) => cb(editorUI, mode)
  resetTouchMonitor()
  timerId = setTimeout(() => {
    next('press')
    timerId = setTimeout(() => {
      next('longPress')
      timerId = null
    }, LONG_PRESS_DURATION)
  }, PRESS_DURATION)
}

function handleTouchDown(editorUI: EditorUI) {
  const { renderingController, notesView: notes } = editorUI
  const pos = notes.convertPixToInnerMMPos(touchStartX, touchStartY)
  checkNotesTriggerSelectScroll(notes)
  const ret = renderingController.checkPlaceHolder(pos, 'mouseDown')
  if (ret) {
    stopEvent(touchStartEvent)
    return
  }
  const handler = editorUI.editorHandler
  if (handler.isMultiSelectState) {
    handler.isMultiSelectState = false
    handler.resetSelectionState()
  }

  handleMouseDownForNotes(editorUI, notes.pointerEvtInfo, pos.x, pos.y)
  needHandleTouchDown = false
}

function resetTouchDownHandler() {
  needHandleTouchDown = false
  touchStartEvent = undefined
}

function saveTocuhDownHandler(e: PointerEvent) {
  needHandleTouchDown = true
  touchStartEvent = e
}

function checkNeedHandleTouchDown(editorUI: EditorUI) {
  if (needHandleTouchDown) {
    handleTouchDown(editorUI)
    resetTouchDownHandler()
  }
}

export function handleNotesTouchStart(notes: NotesComponent, e: PointerEvent) {
  const { editorUI, pointerEvtInfo: pointerEvt } = notes
  if (-1 === editorUI.renderingController.curSlideIndex) return
  editorUI.checkMobileInput() // notes 并非始终 foucus 到 input, 也可能是滚动
  stopEvent(e)
  editorUI.presentation.setFocusTargetType(FocusTarget.EditorView)
  const { presentation, renderingController } = editorUI
  if (!EditorSettings.isNotesEnabled) return

  pointerEvt.onDown(e, true)

  editorUI.isLockMouse = true
  if (renderingController.isEmptySlides && presentation.canEdit()) {
    presentation.addNextSlide()
    return
  }
  if (presentation.currentSlideIndex === -1) {
    return
  }

  // 排除所有一次性操作后再开始计时
  resetTouchMode(editorUI)
  monitorTouchMode(editorUI, handleTouchModeChange)
  ;[touchStartX, touchStartY] = [pointerEvt.x, pointerEvt.y]
  touchMoveY = pointerEvt.y

  saveTocuhDownHandler(e)
  presentation.setFocusTargetType(FocusTarget.EditorView)
}

export function handleNotesTouchMove(notes: NotesComponent, e?: PointerEvent) {
  const { editorUI, pointerEvtInfo: pointerEvt } = notes
  if (
    -1 === editorUI.renderingController.curSlideIndex ||
    !EditorSettings.isNotesEnabled
  ) {
    return
  }

  if (!e) {
    const pos = notes.convertPixToInnerMMPos(pointerEvt.x, pointerEvt.y)
    handleMouseMoveForNotes(editorUI, pointerEvt, pos.x, pos.y)
    return
  }

  const pagePos = getPointerEventPos(e)
  if (!pagePos) return
  const [x, y] = pagePos
  if (timerId) {
    const [dx, dy] = [Math.abs(touchStartX - x), Math.abs(touchStartY - y)]
    if (hasMoved(dx, dy, TouchEPS)) {
      resetTouchMonitor()
    } else {
      return
    }
  }

  const handler = editorUI.editorHandler
  const touchMode = handler.touchAction ?? 'tap'

  // Swipe
  if (touchMode === 'tap') {
    pointerEvt.onMove(e)
    editorUI.notesView.scrollByDelta(touchMoveY - y)
    touchMoveY = y
    resetTouchDownHandler()
    return
  }

  checkNeedHandleTouchDown(editorUI)
  pointerEvt.onMove(e)
  const pos = notes.convertPixToInnerMMPos(pointerEvt.x, pointerEvt.y)
  handleMouseMoveForNotes(editorUI, pointerEvt, pos.x, pos.y)
}

export function handleNotesTouchEnd(notes: NotesComponent, e: PointerEvent) {
  const { editorUI, pointerEvtInfo: pointerEvt } = notes
  if (-1 === editorUI.renderingController.curSlideIndex) return

  const { renderingController } = editorUI
  checkNotesClearSelectScroll(notes)
  if (!pointerEvt.isLocked || !EditorSettings.isNotesEnabled) return

  resetTouchMonitor()
  pointerEvt.onUp(e)

  checkNeedHandleTouchDown(editorUI)
  const pos = notes.convertPixToInnerMMPos(pointerEvt.x, pointerEvt.y)
  editorUI.mouseCursor.unLock()
  editorUI.isLockMouse = false

  if (renderingController.checkPlaceHolder(pos, 'mouseUp') === true) {
    const handler = editorUI.editorHandler
    handler.changeToState(InstanceType.UIInitState, handler)
    return
  }
  handleMouseUpForNotes(editorUI, pointerEvt, pos.x, pos.y)
  editorUI.isLockMouse = false
  editorUI.checkMobileInput()
  editorUI.renderingController.clearAdornment()
  resetTouchMode(editorUI)
}

function handleTouchModeChange(editorUI: EditorUI, mode: EditorTouchMode) {
  editorUI.editorHandler.touchAction = mode
}

/** 光标按下后移动的过程中，应该 scroll 跟随 */
export function checkNotesTriggerSelectScroll(notes: NotesComponent) {
  const { editorUI } = notes
  const handler = editorUI.editorHandler
  if (
    handler.curState.instanceType !== InstanceType.UIInitState &&
    -1 === editorUI.selectScrollTimerId
  ) {
    notes.selectScrollTimerId = setInterval(
      notes.onSelectScroll,
      20
    ) as unknown as number
  }
}

export function checkNotesClearSelectScroll(notes: NotesComponent) {
  if (-1 !== notes.selectScrollTimerId) {
    clearInterval(notes.selectScrollTimerId)
    notes.selectScrollTimerId = -1
  }
}
