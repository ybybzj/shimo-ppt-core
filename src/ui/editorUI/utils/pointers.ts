import { HoveredElementType } from '../../../core/common/const/attrs'
import { PointerEventInfo, POINTER_BUTTON } from '../../../common/dom/events'
import { startEditAction } from '../../../core/calculation/informChanges/startAction'
import { calculateForRendering } from '../../../core/calculation/calculate/calculateForRendering'
import { searchManager } from '../../../core/search/SearchManager'
import { Shape } from '../../../core/SlideElement/Shape'
import { isLinePreset } from '../../../core/utilities/shape/asserts'
import {
  testTextDocOfShape,
  getTextDocumentOfSp,
  getGeomPreset,
} from '../../../core/utilities/shape/getters'
import { Evt_onContextMenu } from '../../../editor/events/EventNames'
import { EditorKit } from '../../../editor/global'
import { syncInterfaceState } from '../../../editor/interfaceState/syncInterfaceState'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { EditorUtil } from '../../../globals/editor'
import { InstanceType } from '../../../core/instanceTypes'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { endAddComment } from '../../features/comment/utils'
import {
  getCurStateHoverInfo,
  isCurAddingComment,
  isCurAddingShape,
} from '../../states/utils'
import {
  KeyEvent_NoPrevent,
  KeyEvent_PreventAll,
  StatusOfFormatPainter,
} from '../const'
import { EditorUI } from '../EditorUI'
import { handleUpdateUICursor, syncSelectionState } from './updates'
import { getContextMenuData } from './utils'

export function handleMouseDown(
  editorUI: EditorUI,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  const presentation = editorUI.presentation

  const oldFocusOnNotes = presentation.isNotesFocused
  presentation.isNotesFocused = false

  if (searchManager.searchElementsCount > 0) {
    searchManager.resetCurrentSearchId()
  }

  e.ctrlKey = e.ctrlKey
  e.shiftKey = e.shiftKey
  const handler = editorUI.editorHandler
  if (oldFocusOnNotes !== presentation.isNotesFocused) {
    handler.resetSelectionState()
  }
  const handled = handleUIStateOnMouseDown(handler, e, x, y)

  if (!handled) {
    if (EditorKit.paintFormatState !== StatusOfFormatPainter.OFF) {
      // 取消格式刷
      EditorKit.emitPaintFormat(StatusOfFormatPainter.OFF)
      handleUpdateUICursor(handler, x, y)
    }
    syncSelectionState(editorUI)
  } else if (
    editorUI.editorKit.paintFormatState === StatusOfFormatPainter.MULTIPLE
  ) {
    editorUI.presentation.allowPasteFormat = true
  }

  syncInterfaceState(editorUI.editorKit)

  if (oldFocusOnNotes) {
    presentation.calcForEmptyNotes()
  }
  if (handled) return KeyEvent_PreventAll
  return KeyEvent_NoPrevent
}

export function handleMouseUp(
  editorUI: EditorUI,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  const presentation = editorUI.presentation
  e.ctrlKey = e.ctrlKey
  e.shiftKey = e.shiftKey
  const slideIndex = presentation.currentSlideIndex
  const handler = editorUI.editorHandler
  handleUIStateOnMouseUp(handler, e, x, y)
  if (slideIndex !== presentation.currentSlideIndex) {
    syncSelectionState(editorUI)
  }
  if (e.button === POINTER_BUTTON.Right && !handler.isInShowMod()) {
    const menuData = getContextMenuData(editorUI, { mmX: x, mmY: y })
    editorUI.editorKit.emitEvent(Evt_onContextMenu, menuData)
  }
  syncInterfaceState(editorUI.editorKit)

  if (handler.isInShowMod()) {
    const result = getCurStateHoverInfo(handler, x, y, e)
    if (result) return KeyEvent_PreventAll
  }
  if (editorUI.editorKit.paintFormatState === StatusOfFormatPainter.ON) {
    editorUI.editorKit.emitPaintFormat(StatusOfFormatPainter.OFF)
    handleUpdateUICursor(handler, x, y)
  }

  return KeyEvent_NoPrevent
}

export function handleMouseMove(
  editorUI: EditorUI,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  const presentation = editorUI.presentation
  e.ctrlKey = e.ctrlKey
  e.shiftKey = e.shiftKey

  const handler = editorUI.editorHandler

  const orgFocus = presentation.isNotesFocused
  presentation.isNotesFocused = false
  handleUIStateOnMouseMove(handler, e, x, y)
  presentation.isNotesFocused = orgFocus

  if (handler.isInShowMod()) {
    const result = getCurStateHoverInfo(handler, x, y, e)
    if (result) return KeyEvent_PreventAll
  }
  return KeyEvent_NoPrevent
}

export function handleMouseDownForNotes(
  editorUI: EditorUI,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  const presentation = editorUI.presentation
  const isSlideFocused = !presentation.isNotesFocused
  presentation.isNotesFocused = true
  const slide = presentation.slides[presentation.currentSlideIndex]
  const handler = editorUI.editorHandler
  if (EditorKit.paintFormatState !== StatusOfFormatPainter.OFF) {
    // 取消格式刷
    EditorKit.emitPaintFormat(StatusOfFormatPainter.OFF)
  }

  if (isCurAddingShape(handler)) {
    handler.changeToState(InstanceType.UIInitState, handler)
    handler.updateEditLayer()
  }
  if (isCurAddingComment(handler)) {
    endAddComment(handler)
    editorUI.triggerRender()
  }

  handleUpdateUICursor(handler, x, y)
  if (slide) {
    if (isSlideFocused) {
      let needCalc = false
      if (testTextDocOfShape(presentation.getCurrentTextTarget(false))) {
        needCalc = true
      }
      if (slide.slideComments) {
        const arrOfComments =
          slide.slideComments && slide.slideComments.comments
        if (Array.isArray(arrOfComments)) {
          for (let i = 0; i < arrOfComments.length; ++i) {
            if (arrOfComments[i].selected) {
              needCalc = true
              arrOfComments[i].selected = false
              break
            }
          }
        }
      }
      handler.reset(false)
      if (needCalc) {
        editorUI.onCalculateSlide(presentation.currentSlideIndex)
      }
    }
    if (slide.notes) {
      e.ctrlKey = e.ctrlKey
      e.shiftKey = e.shiftKey
      const handled = handleUIStateOnMouseDown(handler, e, x, y)
      if (isSlideFocused) {
        presentation.calcForEmptyNotes()
      }
      if (!handled) {
        syncSelectionState(editorUI)
      }
      syncInterfaceState(editorUI.editorKit)
    }
  }
}

export function handleMouseUpForNotes(
  editorUI: EditorUI,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  const presentation = editorUI.presentation
  if (!presentation.isNotesFocused) {
    return
  }
  const slide = presentation.slides[presentation.currentSlideIndex]
  const handler = editorUI.editorHandler
  if (slide?.notes) {
    e.ctrlKey = e.ctrlKey
    e.shiftKey = e.shiftKey
    handleUIStateOnMouseUp(handler, e, x, y)
    if (e.button === POINTER_BUTTON.Right) {
      const menuData = getContextMenuData(editorUI, { mmX: x, mmY: y })
      menuData.isSlideSelected = false
      editorUI.editorKit.emitEvent(Evt_onContextMenu, menuData)
    }
    syncInterfaceState(editorUI.editorKit)
  }
}

export function handleMouseMoveForNotes(
  editorUI: EditorUI,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  const presentation = editorUI.presentation
  const slide = presentation.slides[presentation.currentSlideIndex]
  const handler = editorUI.editorHandler
  if (slide && handler) {
    if (slide.notes) {
      e.ctrlKey = e.ctrlKey
      e.shiftKey = e.shiftKey

      const orgFocus = presentation.isNotesFocused
      presentation.isNotesFocused = true
      handleUIStateOnMouseMove(handler, e, x, y)
      presentation.isNotesFocused = orgFocus
    }
  }
}

/** 演示模式 onMouseDown 时要用到命中信息(仅 initState) */
export function handleUIStateOnMouseDown(
  handler: IEditorHandler,
  e: PointerEventInfo,
  x,
  y
) {
  const ret = handler.curState.onStart(e, x, y)
  if (e.clicks < 2) {
    handler.updateEditLayer()
    handler.checkSelectionState()
  }
  return ret
}

/** changeState 后可视情况调用来响应新 state 的操作/状态/光标 */
export function handleUIStateOnMouseMove(
  handler: IEditorHandler,
  e: PointerEventInfo,
  x,
  y
) {
  const ret = handleUpdateUICursor(handler, x, y, e)
  handler.curState.onMove(e, x, y)

  if (handler.curState.instanceType === InstanceType.UIInitState) {
    const preHoveredObject = handler.hoveredObject
    let needRedrawSlide = false
    // TODO: 偶发情况下即使 ret 存在也可能取到 null
    handler.hoveredObject = ret
      ? EditorUtil.entityRegistry.getEntityById(ret.objectId)
      : null
    if (preHoveredObject && (!ret || !handler.hoveredObject)) {
      handler.editorUI.editorKit.emitOnHoveredChange()
      if (preHoveredObject.instanceType === InstanceType.Comment) {
        needRedrawSlide = true
        handler.editorUI.editorKit.emitHoveredCommentChangeCallback()
      }
    }
    if (ret && handler.hoveredObject) {
      const hoveredObject = handler.hoveredObject
      if (
        !preHoveredObject ||
        (preHoveredObject && preHoveredObject.getId() !== ret.objectId)
      ) {
        const hoveredInfo = handler.assembleHoverObjectInfo(e.x, e.y)!
        // Commemnt
        if (hoveredObject.instanceType === InstanceType.Comment) {
          hoveredInfo.type = HoveredElementType.Comment
          needRedrawSlide = true
          handler.editorUI.editorKit.emitHoveredCommentChangeCallback(
            hoveredObject.toJSON()
          )
        } else if (preHoveredObject?.instanceType === InstanceType.Comment) {
          needRedrawSlide = true
          handler.editorUI.editorKit.emitHoveredCommentChangeCallback()
        }
        // OleImage
        if (hoveredObject.instanceType === InstanceType.ImageShape) {
          if (hoveredObject.isOleObject()) {
            hoveredInfo.type = HoveredElementType.OleImage
          }
        }

        handler.editorUI.editorKit.emitOnHoveredChange(hoveredInfo)
      }
    }
    if (needRedrawSlide) {
      handler.editorUI.triggerRender(true)
    }
  }
}

export function handleUIStateOnMouseUp(handler: IEditorHandler, e, x, y) {
  handler.curState.onEnd(e, x, y)
}

export function handleDoubleClickEmptyShape(
  handler: IEditorHandler,
  shape: Shape
) {
  startEditAction(EditActionFlag.action_EditOnPresentation)
  if (!getTextDocumentOfSp(shape) && !isLinePreset(getGeomPreset(shape))) {
    shape.createTxBody()
    const textDoc = getTextDocumentOfSp(shape)!
    textDoc.selectSelf(0)
    handler.checkSelectionState()
  }
  calculateForRendering()
}
