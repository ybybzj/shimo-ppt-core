import { EditorUI } from '../EditorUI'
import {
  Factor_in_to_mm,
  Factor_mm_to_pix,
  Factor_pix_to_mm,
} from '../../../core/common/const/unit'
import { SlideMaster } from '../../../core/Slide/SlideMaster'
import { Matrix2D } from '../../../core/graphic/Matrix'
import { FocusTarget } from '../../../core/common/const/drawing'
import { BrowserInfo } from '../../../common/browserInfo'
import { InstanceType } from '../../../core/instanceTypes'
import { endCrop } from '../../features/crop/utils'
import { UIOperationStates } from '../../states/type'
import { ZoomValue } from '../../../core/common/zoomValue'
import { SlideElement } from '../../../core/SlideElement/type'
import {
  ContextMenuData,
  ContextMenuTypes,
} from '../../../core/properties/props'
import { calculateForSlideMaster } from '../../../core/calculation/calculate/slideMaster'
import { calculateForSlideLayout } from '../../../core/calculation/calculate/slideLayout'
import { getCursorCoordsInTableOrTextDocument } from '../../../core/utilities/cursor/utils'
import { debounce } from '../../../common/utils'
import { DoubleClickDuration } from '../../../common/dom/events'
import { Max_ZoomValue } from '../../../core/common/const/zoom'
import {
  Evt_onContextMenu,
  Evt_onUpdateTheme,
  Evt_toggleContextMenu,
} from '../../../editor/events/EventNames'
import { FitMode } from '../const'
import { EditorSettings } from '../../../exports/type'

// coords converters
export function convertInnerMMToPixPos(
  editorUI: EditorUI,
  xInMM: number,
  yInMM: number,
  ignoreRetina = false,
  delta = 0
): { x: number; y: number } {
  const curSlideRect = editorUI.renderingController.curSlideRect
  const factor = (ZoomValue.value * Factor_mm_to_pix) / 100
  let xInPx = (curSlideRect.left + xInMM * factor + delta) >> 0
  let yInPx = (curSlideRect.top + yInMM * factor + delta) >> 0
  if (ignoreRetina === true) {
    xInPx /= BrowserInfo.PixelRatio
    yInPx /= BrowserInfo.PixelRatio
  }
  return { x: xInPx, y: yInPx }
}

/** 将全局坐标转换为以 slideRect 左上角为原点的坐标系的相对坐标, 后续 handler 处理采用的均为此坐标 */
export function convertPixToInnerMMPos(
  editorUI: EditorUI,
  xInPx: number,
  yInPx: number
): { x: number; y: number } {
  const { renderingController, x: editorOffsetX, y: editorOffsetY } = editorUI
  const { mainContainer, mainView } = editorUI

  const mainContainerOffsetX = mainContainer.PositionInPx.L
  const mainViewOffsetX = mainView.PositionInPx.L
  const innerOffX = (mainContainerOffsetX + mainViewOffsetX) * Factor_mm_to_pix
  const innerPixX = xInPx - editorOffsetX - innerOffX

  const mainContainerOffsetY = mainContainer.PositionInPx.T
  const mainViewOffsetY = mainView.PositionInPx.T
  const innerOffY = (mainContainerOffsetY + mainViewOffsetY) * Factor_mm_to_pix
  const innerPixY = yInPx - editorOffsetY - innerOffY

  const factor = (100 * Factor_pix_to_mm) / ZoomValue.value
  const pos = { x: 0, y: 0, slideIndex: renderingController.curSlideIndex }

  if (renderingController.curSlideIndex !== -1) {
    const curSlideRect = renderingController.curSlideRect
    const mmX = (innerPixX - curSlideRect.left) * factor
    const mmY = (innerPixY - curSlideRect.top) * factor
    pos.x = mmX
    pos.y = mmY
  }

  return pos
}

export function convertPctToInnerMMPos(
  editorUI: EditorUI,
  pctX: number,
  pctY: number
): { x: number; y: number } {
  const { curSlideIndex, curSlideRect } = editorUI.renderingController
  const pos = { x: 0, y: 0 }

  if (curSlideIndex !== -1) {
    const factor = (100 * Factor_pix_to_mm) / ZoomValue.value
    const { left, right, top, bottom } = curSlideRect
    const width = right - left
    const height = bottom - top
    const mmX = width * pctX * factor
    const mmY = height * pctY * factor

    pos.x = mmX
    pos.y = mmY
  }

  return pos
}

export function convertGlobalInnerMMToPixPos(
  editorUI: EditorUI,
  xInMM: number,
  yInMM: number,
  transform?: Matrix2D
): { x: number; y: number } {
  const { renderingController, mainContainer, mainView, notesView } = editorUI

  let x = xInMM
  let y = yInMM
  if (transform) {
    x = transform.XFromPoint(xInMM, yInMM)
    y = transform.YFromPoint(xInMM, yInMM)
  }

  const mainContainerL = mainContainer.PositionInPx.L
  const mainContainerT = mainContainer.PositionInPx.T
  const factor = Factor_mm_to_pix

  if (!editorUI.presentation.isNotesFocused) {
    const { L: mainViewL, T: mainViewT } = mainView.PositionInPx
    const { left, top } = renderingController.curSlideRect
    const zoomFactor = (ZoomValue.value * factor) / 100

    const xInPx =
      (left + x * zoomFactor + (mainContainerL + mainViewL) * factor) >> 0
    const yInPx =
      (top + y * zoomFactor + (mainContainerT + mainViewT) * factor) >> 0

    return { x: xInPx, y: yInPx }
  } else {
    const notesViewAbsPos = notesView.control.PositionInPx
    const { L: notesViewL, T: notesViewT } = notesViewAbsPos
    const xInPx =
      (x * factor + 10 + (mainContainerL + notesViewL) * factor) >> 0
    const yInPx = (y * factor + (mainContainerT + notesViewT) * factor) >> 0

    return { x: xInPx, y: yInPx }
  }
}

export function isInShowMode(editorUI: EditorUI): boolean {
  return !!editorUI?.showManager?.isInShowMode
}

export const generateAllMastersThumbnails = debounce((editorUI: EditorUI) => {
  let slideMasters = editorUI.presentation.slideMasters

  if (slideMasters.length > EditorSettings.preloadCountForSlideMaster) {
    slideMasters = slideMasters.slice(
      0,
      EditorSettings.preloadCountForSlideMaster
    )
  }
  slideMasters.forEach((master) => generateMasterThumbnails(editorUI, master))
}, 1000)

export function generateMasterThumbnails(
  editorUI: EditorUI,
  master: SlideMaster
) {
  const layouts = master.layouts
  const masterDrawer = editorUI.masterDrawer
  const layoutDrawer = editorUI.layoutDrawer

  calculateForSlideMaster(master)
  master.imgBase64 = masterDrawer.getThumbnail(master)

  layouts.forEach((layout) => {
    calculateForSlideLayout(layout)
    layout.imgBase64 = layoutDrawer.getThumbnail(layout)
    layout.thumbPixWidth = layoutDrawer.widthInPx
    layout.thumbPixHeight = layoutDrawer.heightInPx
  })
}

const triggerUpdateTheme = debounce((editorUI: EditorUI) => {
  editorUI.editorKit.emitEvent(Evt_onUpdateTheme)
}, 300)
export function invalidAllMastersThumbnails(
  editorUI: EditorUI,
  resources?: {
    images?: string[]
    fonts?: string[]
  }
) {
  const presentation = editorUI.presentation
  presentation.slideMasters.forEach((master) =>
    invalidMasterThumbnails(editorUI, master, resources)
  )

  triggerUpdateTheme(editorUI)
}

export function invalidMasterThumbnails(
  editorUI: EditorUI,
  master: SlideMaster,
  resources?: {
    images?: string[]
    fonts?: string[]
  }
) {
  if (resources == null) {
    _invalidMasterThumbnails(master)
    return
  }
  const resourceMgr = editorUI.editorKit.resourcesLoadManager
  const { images, fonts } = resources
  if (images && images.length > 0) {
    for (const imgRes of images) {
      if (
        resourceMgr.getResourceOfSlideMaster(
          master,
          'image',
          (src) => src === imgRes,
          false
        ).length > 0
      ) {
        _invalidMasterThumbnails(master)
        return
      }
    }
  }
  if (fonts && fonts.length > 0) {
    for (const fontRes of fonts) {
      if (
        resourceMgr.getResourceOfSlideMaster(
          master,
          'font',
          (src) => src === fontRes,
          false
        ).length > 0
      ) {
        _invalidMasterThumbnails(master)
        return
      }
    }
  }
}

function _invalidMasterThumbnails(master: SlideMaster) {
  const layouts = master.layouts
  master.imgBase64 = ''
  layouts.forEach((layout) => {
    layout.imgBase64 = ''
    layout.thumbPixWidth = 0
    layout.thumbPixHeight = 0
  })
}

export function onElementFocusChange(
  editorUI: EditorUI,
  type: FocusTarget,
  oldState: UIOperationStates
) {
  const handler = editorUI.editorHandler
  if (
    oldState.instanceType === InstanceType.CropState &&
    type !== FocusTarget.EditorView
  ) {
    endCrop(handler)
  }
}

export function executeWithCenterKeep(editorUI: EditorUI, cb: () => void) {
  const { xInMM, yInMM } = editorUI.scrolls.getCurCenterPos()
  cb()
  editorUI.scrollToPosition(xInMM, yInMM, 0, 0)
}

export function getContextMenuData(
  editorUI: EditorUI,
  pos?: { mmX: number; mmY: number },
  type?: (typeof ContextMenuTypes)[keyof typeof ContextMenuTypes]
): ContextMenuData {
  if (!pos) {
    pos = getDefaultContextMenuPos(editorUI)
  }
  const { mmX, mmY } = pos
  const pixPos = convertGlobalInnerMMToPixPos(editorUI, mmX, mmY)
  return {
    type: type ?? ContextMenuTypes.Main,
    absX: pixPos.x,
    absY: pixPos.y,
  }
}

function getDefaultContextMenuPos(editorUI: EditorUI) {
  const { editorHandler: handler, presentation } = editorUI
  let i, sps: SlideElement[] | undefined, dX, dY
  const selectionState = handler.selectionState

  if (selectionState && selectionState.hasSelection) {
    const selectTextSp = handler.getTargetTextSp()
    const selectedGroup = handler.selectionState?.groupSelection?.group
    if (selectTextSp) {
      const textTransform = selectTextSp.textTransform
      const targetObjectOrTable = presentation.getCurrentTextTarget(true)
      if (selectTextSp && targetObjectOrTable && textTransform) {
        const pos = getCursorCoordsInTableOrTextDocument(targetObjectOrTable)
        return {
          mmX: textTransform.XFromPoint(pos.x, pos.y),
          mmY: textTransform.YFromPoint(pos.x, pos.y),
        }
      }
      return { mmX: 0, mmY: 0 }
    } else if (selectedGroup) {
      sps = selectedGroup.slideElementsInGroup
      for (i = sps.length - 1; i > -1; --i) {
        const sp = sps[i]
        if (sps[i].selected) {
          dX = sp.transform.XFromPoint(sp.extX / 2, sp.extY / 2) - sp.extX / 2
          dY = sp.transform.YFromPoint(sp.extX / 2, sp.extY / 2) - sp.extY / 2
          return { mmX: dX, mmY: dY }
        }
      }
    } else {
      sps = handler.getCurrentSpTree()
      if (!sps) return { mmX: 0, mmY: 0 }
      for (i = sps.length - 1; i > -1; --i) {
        const sp = sps[i]
        if (sp.selected) {
          dX = sp.transform.XFromPoint(sp.extX / 2, sp.extY / 2) - sp.extX / 2
          dY = sp.transform.YFromPoint(sp.extX / 2, sp.extY / 2) - sp.extY / 2
          return { mmX: dX, mmY: dY }
        }
      }
    }
  }

  return { mmX: 0, mmY: 0 }
}

// 展开/更新 contextmenu
function emitEditorContextMenu(
  editorUI: EditorUI,
  pos?: { mmX: number; mmY: number },
  type?: (typeof ContextMenuTypes)[keyof typeof ContextMenuTypes]
) {
  const menuData = getContextMenuData(editorUI, pos, type)
  editorUI.editorKit.emitEvent(Evt_onContextMenu, menuData)
}

let canEmitMenu = true

export function cancelEmitMenu() {
  canEmitMenu = false
}

// 延迟(检测非双击时)展开/更新 contextmenu
export const touchEmitEditorContextMenu = debounce(
  (
    editorUI: EditorUI,
    pos?: { mmX: number; mmY: number },
    type?: (typeof ContextMenuTypes)[keyof typeof ContextMenuTypes]
  ) => {
    if (editorUI.pointerEvtInfo.clicks < 2 && canEmitMenu) {
      if (pos) {
        pos.mmX += 0.7 * Factor_in_to_mm // touch finger 宽度在 0.6 - 0.8 inch, 避免触屏遮挡误触
      }
      emitEditorContextMenu(editorUI, pos, type)
    }
    canEmitMenu = true
  },
  DoubleClickDuration
)

// 延迟(检测非双击时) 展开/关闭 contextmenu
export const touchToggleContextMenu = debounce(
  (
    editorUI: EditorUI,
    pos?: { mmX: number; mmY: number },
    type?: (typeof ContextMenuTypes)[keyof typeof ContextMenuTypes]
  ) => {
    if (editorUI.pointerEvtInfo.clicks < 2 && canEmitMenu) {
      if (pos) {
        pos.mmX += 0.7 * Factor_in_to_mm
      }
      toggleContextMenu(editorUI, pos, type)
    }
    canEmitMenu = true
  },
  DoubleClickDuration
)

function toggleContextMenu(
  editorUI: EditorUI,
  pos?: { mmX: number; mmY: number },
  type?: (typeof ContextMenuTypes)[keyof typeof ContextMenuTypes]
) {
  const menuData = getContextMenuData(editorUI, pos, type)
  editorUI.editorKit.emitEvent(Evt_toggleContextMenu, menuData)
}

/** 适应/放大至最大  */
export function toggleZoomMode(editorUI: EditorUI) {
  if (editorUI.zoomType !== FitMode.Window) {
    editorUI.zoomFitPage()
  } else {
    ZoomValue.value = Max_ZoomValue
    executeWithCenterKeep(editorUI, () => {
      editorUI.exeZoom(FitMode.Custom)
    })
  }
}

export function getScrollPageEPS(editorUI: EditorUI) {
  return (editorUI.isInTouchMode() ? 10 : 5) * BrowserInfo.PixelRatio
}
