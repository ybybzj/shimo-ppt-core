import {
  AlignKind,
  VertAlignType,
  AlignKindValue,
} from '../../../core/common/const/attrs'
import { KeyboardEventInfo } from '../../../common/dom/events'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { CurrentFocusElementsInfo } from '../../../core/Slide/CurrentFocusElementsInfo'
import { ParaNewLine } from '../../../core/Paragraph/RunElement/ParaNewLine'
import { ParaSpace } from '../../../core/Paragraph/RunElement/ParaSpace'
import { ParaTab } from '../../../core/Paragraph/RunElement/ParaTab'
import { ParaText } from '../../../core/Paragraph/RunElement/ParaText'
import {
  BulletTypeValue,
  PlaceholderType,
} from '../../../core/SlideElement/const'
import { Presentation } from '../../../core/Slide/Presentation'
import {
  isPlaceholder,
  isPlaceholderWithEmptyContent,
  isLinePreset,
} from '../../../core/utilities/shape/asserts'
import {
  getPlaceholderType,
  getTextDocumentOfSp,
  getMatchedSlideElementForPh,
  getGeomPreset,
} from '../../../core/utilities/shape/getters'
import {
  setFontSizeIn,
  setFontSizeOut,
  setStrikeout,
} from '../../../core/utilities/presentation/textPr'
import { EditorKit } from '../../../editor/global'
import { syncInterfaceState } from '../../../editor/interfaceState/syncInterfaceState'
import { EditorUtil } from '../../../globals/editor'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { InstanceType, isInstanceTypeOf } from '../../../core/instanceTypes'
import { EditorUI } from '../EditorUI'
import { getContextMenuData } from './utils'
import { endCrop } from '../../features/crop/utils'
import { handleUpdateUICursor, syncSelectionState } from './updates'
import { FocusTarget } from '../../../core/common/const/drawing'
import {
  Evt_onContextMenu,
  Evt_onToggleCommentShow,
} from '../../../editor/events/EventNames'
import { isCurAddingComment, isCurAddingShape } from '../../states/utils'
import { TextPrOptions } from '../../../core/textAttributes/TextPr'
import { calculateForRendering } from '../../../core/calculation/calculate/calculateForRendering'
import { startEditAction } from '../../../core/calculation/informChanges/startAction'
import { moveCursorByCell } from '../../../core/utilities/cursor/table'
import {
  KeyEvent_NoPrevent,
  KeyEvent_PreventAll,
  KeyEvent_PreventPress,
  StatusOfFormatPainter,
} from '../const'
import {
  endAddComment,
  showComments,
  startAddComment,
} from '../../features/comment/utils'
import { TableSelectionType } from '../../../core/common/const/table'
import { Table } from '../../../core/Table/Table'
import {
  moveCursorToStartPosInTextDocument,
  moveCursorToStartPosInTable,
} from '../../../core/utilities/cursor/moveToEdgePos'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { isCursorAtBeginInParagraph } from '../../../core/Paragraph/utils/cursor'
import {
  getCurrentParagraph,
  getCurrentFocusElementsInfo,
} from '../../../core/utilities/tableOrTextDocContent/getter'
import { BrowserInfo } from '../../../common/browserInfo'

export const keyCodeMap = {
  backSpace: 8,
  tab: 9,
  enter: 13,
  altOrOption: 18,
  esc: 27,
  space: 32,
  pageUp: 33,
  pageDown: 34,
  end: 35,
  home: 36,
  arrowLeft: 37,
  arrowUp: 38,
  arrowRight: 39,
  arrowDown: 40,
  insert: 45,
  delete: 46,
  digit1: 49,
  digit2: 50,
  digit3: 51,
  digit4: 52,
  digit5: 53,
  digit8: 56,
  keyA: 65,
  keyB: 66,
  keyC: 67,
  keyD: 68,
  keyE: 69,
  keyF: 70,
  keyG: 71,
  keyH: 72,
  keyI: 73,
  keyJ: 74,
  keyK: 75,
  keyL: 76,
  keyM: 77,
  keyN: 78,
  keyO: 79,
  keyP: 80,
  keyQ: 81,
  keyR: 82,
  keyS: 83,
  keyT: 84,
  keyU: 85,
  keyV: 86,
  keyW: 87,
  keyX: 88,
  keyY: 89,
  keyZ: 90,
  f10: 121,
  numLock: 144,
  scrollLock: 145,
  equalOrPlus: 187,
  comma: 188, // ,
  minus: 189, // -
  period: 190, // .
  bracketLeft: 219, // [
  brckslash: 200, // \
  bracketRight: 221, // ]
} as const

export function handleKeyDown(
  editorUI: EditorUI,
  e: KeyboardEventInfo
): number {
  let isUpdateSelection = true
  let result = KeyEvent_NoPrevent
  const { editorHandler: handler, presentation } = editorUI
  const hasSelection = presentation.selectionState.hasSelection
  const { keyCode, altKey, ctrlKey, shiftKey, altGr } = e
  const canEdit = presentation.canEdit()
  if (handler.curState.instanceType === InstanceType.DragOutState) {
    handler.endDragOut()
  }

  switch (keyCode) {
    case keyCodeMap.backSpace:
    case keyCodeMap.delete: {
      if (!shiftKey) {
        result = KeyEvent_PreventAll
        if (canEdit) {
          const dir = keyCode === keyCodeMap.backSpace ? -1 : 1
          handler.remove(dir)
        }
      }
      break
    }
    case keyCodeMap.tab: {
      result = KeyEvent_PreventAll
      if (!canEdit || !handler) break
      const textTarget = presentation.getCurrentTextTarget(true)
      if (textTarget) {
        if (textTarget.instanceType === InstanceType.Table) {
          const isNext = true === shiftKey ? false : true
          moveCursorByCell(textTarget, isNext)
        } else {
          const focusInfo = new CurrentFocusElementsInfo()
          const { children: paragraphs, selection, cursorPosition } = textTarget
          const paraIndex = cursorPosition.childIndex
          getCurrentFocusElementsInfo(textTarget, focusInfo)
          const curParagraph = paragraphs.at(paraIndex)
          if (curParagraph) {
            const bullet = curParagraph.calcedPrs.pr?.paraPr?.bullet
            const isValidBullet =
              bullet?.isBullet() &&
              bullet.bulletType!.type !== BulletTypeValue.NONE
            if (
              focusInfo.isMultiSelection() ||
              curParagraph.isSelectFromStart() ||
              (isValidBullet &&
                selection.startIndex === selection.endIndex &&
                isCursorAtBeginInParagraph(curParagraph))
            ) {
              if (presentation.canChangeParagraphIndentLevel(!shiftKey)) {
                presentation.changeIndentLevel(!shiftKey)
              }
            } else {
              startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
              presentation.addToParagraph(ParaTab.new())
            }
          }
        }
      } else {
        handler.selectNextElement(!shiftKey ? 1 : -1)
      }
      syncInterfaceState(editorUI.editorKit)
      break
    }
    case keyCodeMap.enter: {
      result = KeyEvent_PreventAll
      if (ctrlKey || !handler) break
      const textTarget = presentation.getCurrentTextTarget()
      if (textTarget && hasSelection) {
        if (shiftKey) {
          if (hasSelection) {
            startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)

            const focusInfo = new CurrentFocusElementsInfo()
            getCurrentFocusElementsInfo(textTarget, focusInfo)
            presentation.addToParagraph(ParaNewLine.new())
          }
        } else {
          if (hasSelection) {
            const arrOfSelectedElements =
              presentation.selectionState.selectedSlideElements
            if (
              arrOfSelectedElements.length === 1 &&
              isPlaceholder(arrOfSelectedElements[0]) &&
              (getPlaceholderType(arrOfSelectedElements[0]) ===
                PlaceholderType.ctrTitle ||
                getPlaceholderType(arrOfSelectedElements[0]) ===
                  PlaceholderType.title)
            ) {
              startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
              const focusInfo = new CurrentFocusElementsInfo()
              getCurrentFocusElementsInfo(textTarget, focusInfo)
              presentation.addToParagraph(ParaNewLine.new())
            } else {
              const focusInfo = new CurrentFocusElementsInfo()
              getCurrentFocusElementsInfo(textTarget, focusInfo)
              handler.addNewParagraph()
            }
          }
        }
      } else {
        const valueOfRet = handleEnter(handler)
        if (valueOfRet & 2) {
          if (presentation.slides[presentation.currentSlideIndex]) {
            editorUI.onCalculateSlide(presentation.currentSlideIndex)
          }
        }
        if (valueOfRet & 1) {
          syncSelectionState(editorUI)
          syncInterfaceState(editorUI.editorKit)
        }
      }
      break
    }
    case keyCodeMap.altOrOption: {
      if (handler.curState.instanceType === InstanceType.UIInitState && !(ctrlKey || shiftKey)) {
        // 按住 alt/option 的过程中可拖拽, 松开结束
        handler.startDragOut()
      }
      return result
    }
    case keyCodeMap.esc: {
      result = KeyEvent_PreventAll
      if (EditorKit.paintFormatState !== StatusOfFormatPainter.OFF) {
        // 取消格式刷
        EditorKit.emitPaintFormat(StatusOfFormatPainter.OFF)
      }

      if (handler && !presentation.isNotesFocused) {
        const curState = handler.curState
        if (isCurAddingShape(handler)) {
          editorUI.editorKit.emitEndAddShape()
          handler.changeToState(InstanceType.UIInitState, handler)
          handler.updateEditLayer()
          handleUpdateUICursor(handler)
          return result
        }
        if (isCurAddingComment(handler)) {
          endAddComment(handler)
          editorUI.triggerRender(true)
          return result
        }
        if (isInstanceTypeOf(curState, InstanceType.CropState)) {
          endCrop(handler)
          return result
        }
        const selectTextSp = handler.getTargetTextSp()

        let needCalc = false
        if (selectTextSp && isPlaceholderWithEmptyContent(selectTextSp)) {
          needCalc = true
        }

        const selectionState = handler.selectionState
        const selectedTextSp = selectionState?.selectedTextSp
        const groupSelection = selectionState?.selectedGroup
        if (shiftKey || (!groupSelection && !selectedTextSp)) {
          selectionState.reset()
        } else {
          if (selectedTextSp) {
            selectionState.setTextSelection(null)
          } else if (groupSelection) {
            selectionState.resetGroupSelection()
          }
        }
        if (needCalc) {
          editorUI.onCalculateSlide(presentation.currentSlideIndex)
        }
        syncSelectionState(editorUI)
        syncInterfaceState(editorUI.editorKit)
      }
      break
    }
    case keyCodeMap.space: {
      result = KeyEvent_PreventAll
      if (!canEdit) break
      if (true === shiftKey && true === ctrlKey) {
        editorUI.textCursor.show()
        if (hasSelection) {
          startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
          presentation.addToParagraph(ParaText.new(0x00a0))
        }
      } else if (true === ctrlKey) {
        presentation.clearFormattingForParagraph(false, true)
      } else {
        if (hasSelection) {
          startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
          presentation.addToParagraph(ParaSpace.new())
        }
      }
      break
    }
    case keyCodeMap.pageUp: {
      result = KeyEvent_PreventAll
      if (altKey || presentation.currentSlideIndex < 1) break
      editorUI.goToSlide(presentation.currentSlideIndex - 1)
      break
    }
    case keyCodeMap.pageDown: {
      result = KeyEvent_PreventAll
      if (altKey) break
      if (presentation.currentSlideIndex + 1 < presentation.slides.length) {
        editorUI.goToSlide(presentation.currentSlideIndex + 1)
      }
      break
    }
    case keyCodeMap.end: {
      result = KeyEvent_PreventAll
      if (presentation.getCurrentTextTarget()) {
        if (true === ctrlKey) {
          handler.moveCursorToEndPosition()
        } else {
          handler.moveCursorToEndOfLine(true === shiftKey)
        }
      } else {
        if (!shiftKey) {
          if (
            presentation.currentSlideIndex !==
            presentation.slides.length - 1
          ) {
            editorUI.goToSlide(presentation.slides.length - 1)
          }
        } else {
          if (presentation.slides.length > 0) {
            editorUI.thumbnailsManager.shiftSelect(false, true)
          }
        }
      }
      break
    }
    case keyCodeMap.home: {
      result = KeyEvent_PreventAll
      if (presentation.getCurrentTextTarget()) {
        if (true === ctrlKey) {
          handler.moveCursorToStartPosition()
        } else {
          handler.moveCursorToStartOfLine(true === shiftKey)
        }
      } else {
        if (!shiftKey) {
          if (
            presentation.currentSlideIndex !==
            presentation.slides.length - 1
          ) {
            editorUI.goToSlide(presentation.slides.length - 1)
          }
        } else {
          if (presentation.slides.length > 0) {
            editorUI.thumbnailsManager.shiftSelect(false, true)
          }
        }
      }
      break
    }
    case keyCodeMap.arrowLeft: {
      result = KeyEvent_PreventAll
      if (!hasSelection) {
        if (presentation.currentSlideIndex > 0) {
          editorUI.goToSlide(presentation.currentSlideIndex - 1)
        }
      } else {
        if (BrowserInfo.inWindows) {
          handler.moveCursorLeft(true === shiftKey, true === ctrlKey)
        } else {
          if (true === ctrlKey) {
            handler.moveCursorToStartOfLine(true === shiftKey)
          } else {
            handler.moveCursorLeft(true === shiftKey, true === altKey)
          }
        }
      }
      break
    }
    case keyCodeMap.arrowUp: {
      result = KeyEvent_PreventAll
      if (ctrlKey) {
        if (shiftKey) {
          editorUI.editorKit.bringToFront()
        } else {
          editorUI.editorKit.bringForward()
        }
      } else {
        if (!hasSelection) {
          if (presentation.currentSlideIndex > 0) {
            editorUI.goToSlide(presentation.currentSlideIndex - 1)
          }
        } else {
          handler.moveCursorUp(true === shiftKey)
        }
      }
      break
    }
    case keyCodeMap.arrowRight: {
      result = KeyEvent_PreventAll
      if (!hasSelection) {
        if (presentation.currentSlideIndex < presentation.slides.length - 1) {
          editorUI.goToSlide(presentation.currentSlideIndex + 1)
        }
      } else {
        if (BrowserInfo.inWindows) {
          handler.moveCursorRight(true === shiftKey, true === ctrlKey)
        } else {
          if (true === ctrlKey) {
            handler.moveCursorToEndOfLine(true === shiftKey)
          } else {
            handler.moveCursorRight(true === shiftKey, true === altKey)
          }
        }
      }
      break
    }
    case keyCodeMap.arrowDown: {
      result = KeyEvent_PreventAll
      if (ctrlKey) {
        if (shiftKey) {
          editorUI.editorKit.bringToBack()
        } else {
          editorUI.editorKit.bringBackward()
        }
      } else {
        if (!hasSelection) {
          if (presentation.currentSlideIndex < presentation.slides.length - 1) {
            editorUI.goToSlide(presentation.currentSlideIndex + 1)
          }
        } else {
          handler.moveCursorDown(true === shiftKey)
        }
      }
      break
    }
    case keyCodeMap.digit1:
    case keyCodeMap.digit2:
    case keyCodeMap.digit3: {
      if (altKey && !altGr) result = KeyEvent_PreventAll
      break
    }
    case keyCodeMap.digit5: {
      if (true !== ctrlKey) break
      const textPr = presentation.getCalcedTextPr()
      if (null != textPr && canEdit) {
        result = KeyEvent_PreventAll
        handleKeyDownSetTextPr(presentation, {
          strikeout: textPr.strikeout === true ? false : true,
        })
      }
      break
    }
    case keyCodeMap.keyA: {
      if (ctrlKey) {
        result = KeyEvent_PreventAll
        presentation.selectAll()
      }
      break
    }
    case keyCodeMap.keyB: {
      if (true !== ctrlKey || shiftKey || altKey) break
      const textPr = presentation.getCalcedTextPr()
      if (null != textPr && canEdit) {
        result = KeyEvent_PreventAll
        handleKeyDownSetTextPr(presentation, {
          bold: textPr.bold === true ? false : true,
        })
      }
      break
    }
    case keyCodeMap.keyD: {
      if (true !== ctrlKey) break
      result = KeyEvent_PreventAll
      if (!canEdit) break
      if (hasSelection) {
        startEditAction(EditActionFlag.action_Document_PasteByHotKey)
        handler.copySelectedElements()
        calculateForRendering()
        syncInterfaceState(editorUI.editorKit)
      }
      break
    }
    case keyCodeMap.keyE: {
      if (true !== ctrlKey || shiftKey) break
      result = KeyEvent_PreventAll
      if (!canEdit) break
      if (true !== altKey) {
        handleKeyDownSetAlign(presentation, AlignKind.Center)
      } else {
        startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
        presentation.addToParagraph(ParaText.new('€'.charCodeAt(0)))
      }
      break
    }
    case keyCodeMap.keyG: {
      if (true !== ctrlKey) break
      result = KeyEvent_PreventAll
      if (!canEdit) break
      if (true === shiftKey) {
        presentation.unGroupShapes()
      } else {
        presentation.groupShapes()
      }
      break
    }
    case keyCodeMap.keyI: {
      if (true !== ctrlKey || shiftKey || altKey) break
      const textPr = presentation.getCalcedTextPr()
      if (null != textPr) {
        result = KeyEvent_PreventAll
        if (canEdit) {
          handleKeyDownSetTextPr(presentation, {
            italic: textPr.italic === true ? false : true,
          })
        }
      }
      break
    }
    case keyCodeMap.keyJ: {
      if (true !== ctrlKey) break
      result = KeyEvent_PreventAll
      handleKeyDownSetAlign(presentation, AlignKind.Justify)
      break
    }
    case keyCodeMap.keyL: {
      if (true !== ctrlKey || shiftKey) break
      result = KeyEvent_PreventAll
      if (canEdit) {
        handleKeyDownSetAlign(presentation, AlignKind.Left)
      }
      break
    }
    case keyCodeMap.keyM: {
      if (ctrlKey) {
        result = KeyEvent_PreventAll
        if (shiftKey) {
          if (
            !EditorSettings.isSupportComment ||
            !EditorSettings.canAddComment
          ) {
            return result
          }
          const isAddingComment = isCurAddingComment(handler)
          if (!isAddingComment) {
            startAddComment(handler)
          } else {
            endAddComment(handler)
          }
          if (!EditorSettings.isShowComment) {
            showComments(handler)
            editorUI.editorKit.emitEvent(Evt_onToggleCommentShow)
          }
          editorUI.triggerRender(true)
        }
      }
      break
    }
    case keyCodeMap.keyR: {
      if (true !== ctrlKey) break
      result = KeyEvent_PreventAll
      if (canEdit) handleKeyDownSetAlign(presentation, AlignKind.Right)
      break
    }
    case keyCodeMap.keyS: {
      if (true !== ctrlKey || true !== shiftKey) break
      result = KeyEvent_PreventAll
      const textPr = presentation.getCalcedTextPr()
      setStrikeout(presentation, !textPr?.strikeout)
      break
    }
    case keyCodeMap.keyU: {
      if (true !== ctrlKey || shiftKey || altKey) break
      const textPr = presentation.getCalcedTextPr()
      if (null != textPr) {
        result = KeyEvent_PreventAll
        if (canEdit) {
          handleKeyDownSetTextPr(presentation, {
            underline: textPr.underline === true ? false : true,
          })
        }
      }
      break
    }
    case keyCodeMap.keyY: {
      if (true !== ctrlKey) break
      result = KeyEvent_PreventAll
      if (canEdit) {
        editorUI.editorKit.collaborationManager.redo()
      }
      break
    }
    case keyCodeMap.keyZ: {
      if (true !== ctrlKey) break
      result = KeyEvent_PreventAll
      if (canEdit) {
        if (true === shiftKey) {
          editorUI.editorKit.redo()
        } else {
          editorUI.editorKit.undo()
        }
      }
      break
    }
    case 57351:
    case keyCodeMap.f10: {
      if (keyCode === keyCodeMap.f10 && true !== shiftKey) break
      result = KeyEvent_PreventAll
      isUpdateSelection = false
      if (presentation.getFocusType() === FocusTarget.EditorView) {
        const menuData = getContextMenuData(editorUI)
        editorUI.editorKit.emitEvent(Evt_onContextMenu, menuData)
      }
      break
    }
    case keyCodeMap.numLock:
    case keyCodeMap.scrollLock: {
      result = KeyEvent_PreventAll
      break
    }
    case keyCodeMap.equalOrPlus: {
      // Todo, 会同步触发 zoom 导致抖动, 确认是否需要修复
      if (true !== ctrlKey) break
      const textPr = presentation.getCalcedTextPr()
      if (null != textPr) {
        result = KeyEvent_PreventAll
        if (canEdit) {
          const prOptions: TextPrOptions =
            true === shiftKey
              ? {
                  vertAlign:
                    textPr.vertAlign === VertAlignType.SuperScript
                      ? VertAlignType.Baseline
                      : VertAlignType.SuperScript,
                }
              : {
                  vertAlign:
                    textPr.vertAlign === VertAlignType.SubScript
                      ? VertAlignType.Baseline
                      : VertAlignType.SubScript,
                }
          handleKeyDownSetTextPr(presentation, prOptions)
        }
      }
      break
    }
    case keyCodeMap.comma: {
      if (true !== ctrlKey) break
      result = KeyEvent_PreventAll
      if (shiftKey === true) {
        presentation.changeFontSize(false)
      } else {
        const textPr = presentation.getCalcedTextPr()
        if (null != textPr && canEdit) {
          handleKeyDownSetTextPr(presentation, {
            vertAlign:
              textPr.vertAlign === VertAlignType.SuperScript
                ? VertAlignType.Baseline
                : VertAlignType.SuperScript,
          })
        }
      }
      break
    }
    case keyCodeMap.minus: {
      if (true !== ctrlKey || true !== shiftKey) break
      result = KeyEvent_PreventAll
      if (canEdit) {
        editorUI.textCursor.show()

        startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)

        const paraText = ParaText.new(0x2013)
        paraText.spaceAfter = false
        presentation.addToParagraph(paraText)
      }
      break
    }
    case keyCodeMap.period: {
      if (true !== ctrlKey) break
      result = KeyEvent_PreventAll
      if (true === shiftKey) {
        presentation.changeFontSize(true)
      } else {
        const textPr = presentation.getCalcedTextPr()
        if (null != textPr && canEdit) {
          handleKeyDownSetTextPr(presentation, {
            vertAlign:
              textPr.vertAlign === VertAlignType.SubScript
                ? VertAlignType.Baseline
                : VertAlignType.SubScript,
          })
        }
      }
      break
    }
    case keyCodeMap.bracketLeft: {
      if (true !== ctrlKey) break
      result = KeyEvent_PreventAll
      if (canEdit) {
        setFontSizeOut(presentation)
        syncInterfaceState(editorUI.editorKit)
      }
      break
    }
    case keyCodeMap.bracketRight: {
      if (true !== ctrlKey) break
      result = KeyEvent_PreventAll
      if (canEdit) {
        setFontSizeIn(presentation)
        syncInterfaceState(editorUI.editorKit)
      }
      break
    }
    default:
      break
  }

  if (result & KeyEvent_PreventPress && true === isUpdateSelection) {
    syncSelectionState(editorUI)
  }

  return result
}

export function handleKeyPress(editorUI: EditorUI, e: KeyboardEventInfo) {
  const presentation = editorUI.presentation
  if (!presentation.canEdit()) return false

  const slide = presentation.slides[presentation.currentSlideIndex]
  if (!slide) {
    return
  }

  if (presentation.isNotesFocused && !slide.shapeOfNotes) {
    return
  }
  let code
  if (null != e.which) code = e.which
  else if (e.keyCode) code = e.keyCode
  else code = 0 //special char

  let result = false
  if (code > 0x20) {
    const hasSelection = presentation.selectionState.hasSelection
    if (!presentation.isNotesFocused && !hasSelection) {
      const title = getMatchedSlideElementForPh(
        slide,
        PlaceholderType.title,
        null
      )
      if (title) {
        const textDoc = getTextDocumentOfSp(title)!
        if (textDoc.isEmpty()) {
          textDoc.selectSelf(0)
        } else {
          return
        }
      } else {
        return
      }
    }

    startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)

    const target1 = presentation.getCurrentTextTarget()
    presentation.addToParagraph(ParaText.new(code), false, true)
    const target2 = presentation.getCurrentTextTarget()
    if (!target1 && target2) {
      syncInterfaceState(editorUI.editorKit)
    }

    result = true
  }

  if (true === result) {
    syncSelectionState(editorUI)
  }

  return result
}

export function handleKeyUp(editorUI: EditorUI, e: KeyboardEventInfo) {
  const { editorHandler: handler } = editorUI
  switch (e.keyCode) {
    case keyCodeMap.altOrOption:
      handler.endDragOut()
      break
    default:
      break
  }
}

export function handleBeginCompositeInput(editorUI: EditorUI) {
  const { presentation, editorHandler: handler } = editorUI
  const slide = presentation.slides[presentation.currentSlideIndex]
  const hasSelection = presentation.selectionState.hasSelection
  if (!presentation.isNotesFocused && slide && !hasSelection) {
    const title = getMatchedSlideElementForPh(
      slide,
      PlaceholderType.title,
      null
    )
    if (title) {
      const textDoc = getTextDocumentOfSp(title)!
      if (textDoc.isEmpty()) {
        textDoc.selectSelf(0)
      } else {
        return
      }
    } else {
      return
    }
  }

  startEditAction(EditActionFlag.action_Document_CompositeInput)
  if (handler) {
    handler.createTextDocument()
  }

  const textDoc = presentation.getCurrentTextTarget()
  if (!textDoc) {
    EditorUtil.ChangesStack.removeLastAction()
    return false
  }

  editorUI.textCursor.show()

  const para = getCurrentParagraph(textDoc)
  if (!para) {
    EditorUtil.ChangesStack.removeLastAction()
    return false
  }
  if (true === textDoc.hasTextSelection()) {
    textDoc.remove(1, false, true)
  }
  const run = para.elementAtElementPos(
    para.currentElementPosition(false, false)
  )
  if (!run || run.instanceType !== InstanceType.ParaRun) {
    EditorUtil.ChangesStack.removeLastAction()
    return false
  }

  presentation.compositeInputInfo = {
    run,
    index: run.state.elementIndex,
    length: 0,
  }

  run.setCompositeInputInfo(presentation.compositeInputInfo)

  return true
}

function handleKeyDownSetAlign(
  presentation: Presentation,
  alignType: AlignKindValue
) {
  const paraPr = presentation.getCalcedParaPr()
  if (null != paraPr && paraPr.jcAlign !== alignType) {
    presentation.setAlignForParagraph(alignType)
  }
}

function handleKeyDownSetTextPr(
  presentation: Presentation,
  prOptions: TextPrOptions
) {
  startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
  presentation.applyTextPrToParagraph(prOptions)
}

export function handleEnter(handler: IEditorHandler) {
  const selectionState = handler.selectionState
  const selectedObjets = selectionState?.selectedElements || []

  let valueOfRet = 0
  if (selectedObjets.length === 1) {
    const selectedObject = selectedObjets[0]
    switch (selectedObject.instanceType) {
      case InstanceType.Shape: {
        let textDoc = getTextDocumentOfSp(selectedObject)
        if (!textDoc) {
          if (!isLinePreset(getGeomPreset(selectedObject))) {
            startEditAction(EditActionFlag.action_EditOnPresentation)
            selectedObject.createTxBody()
            textDoc = getTextDocumentOfSp(selectedObject)
            if (textDoc) {
              moveCursorToStartPosInTextDocument(textDoc)
              selectionState?.setTextSelection(selectedObject)
            }
            valueOfRet |= 1
            valueOfRet |= 2
            calculateForRendering()
          }
        } else {
          if (textDoc.isEmpty()) {
            moveCursorToStartPosInTextDocument(textDoc)
          } else {
            textDoc.selectAll()
          }
          valueOfRet |= 1
          if (isPlaceholderWithEmptyContent(selectedObject)) {
            valueOfRet |= 2
          }
          selectionState?.setTextSelection(selectedObject)
        }
        break
      }

      case InstanceType.GraphicFrame: {
        const table: Table = selectedObject.graphicObject!
        if (selectionState?.selectedTextSp === selectedObject) {
          const tableSelection = table.selection
          if (tableSelection.type === TableSelectionType.CELL) {
            startEditAction(EditActionFlag.action_EditOnPresentation)
            table.remove(-1)
            table.removeSelection()
            valueOfRet |= 1
            valueOfRet |= 2
            calculateForRendering()
          }
        } else {
          moveCursorToStartPosInTable(table, false)
          const curCell = table.curCell
          if (curCell && curCell.textDoc) {
            if (!curCell.textDoc.isEmpty()) {
              curCell.textDoc.selectAll()
              table.selection.isUse = true
              table.selection.type = TableSelectionType.TEXT
              table.selection.selectionStartInfo.cellPos = {
                row: curCell.parent.index,
                cell: curCell.index,
              }
              table.selection.selectionEndInfo.cellPos = {
                row: curCell.parent.index,
                cell: curCell.index,
              }
              table.selection.currentRowIndex = curCell.parent.index
            }
            selectionState?.setTextSelection(selectedObject)
            valueOfRet |= 1
          }
        }
        break
      }
    }
  }
  return valueOfRet
}
