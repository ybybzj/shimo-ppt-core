import { Nullable } from '../../../../liber/pervasive'
import { PointerEventInfo } from '../../../common/dom/events'
import { isNumber } from '../../../common/utils'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { HoverInfo } from '../../states/type'
import { getCurStateHoverInfo } from '../../states/utils'
import { EditorUI } from '../EditorUI'

export function syncSelectionState(editorUI: EditorUI) {
  const { presentation, editorHandler } = editorUI
  if (presentation.lockInterfaceEvents) {
    return
  }
  if (editorHandler) {
    editorHandler.checkSelectionState()
  }
}

/** 默认 default */
export function handleUpdateUICursor(
  handler: IEditorHandler,
  x?: number,
  y?: number,
  mouseEvent?: PointerEventInfo
): Nullable<HoverInfo> {
  const cursorComponent = handler.editorUI.mouseCursor
  if (!isNumber(x) || !isNumber(y)) {
    cursorComponent.setCursorType('default')
  }
  const hoverInfo = getCurStateHoverInfo(handler, x!, y!, mouseEvent)
  if (hoverInfo) {
    if (!hoverInfo.updated) {
      cursorComponent.setCursorType(hoverInfo.cursorType, hoverInfo.mouseData)
      hoverInfo.updated = true
    }
  } else {
    cursorComponent.setCursorType('default')
  }
  return hoverInfo
}
