import { Factor_mm_to_pix } from '../../../core/common/const/unit'
import { EditorSettings } from '../../../core/common/EditorSettings'
import {
  ContextMenuData,
  ContextMenuTypes,
} from '../../../core/properties/props'
import { Evt_hideContextMenu } from '../../../editor/events/EventNames'
import { ThumbnailsManager } from '../components/thumbnailManager'
import type { EditorTouchAction } from './touches'

// Todo: Menu 部分情况下会闪烁需要排查, 非稳定复现
let timerId: any

/** 识别本次操作基于何种按压模式(例如 tap + move = swipe, press + move = drag) */
let touchMode: EditorTouchAction = 'tap'

const PRESS_DURATION = 200 // ms
const LONG_PRESS_DURATION = 600 // ms
const EPS = 2 // px
// let touchMoveX = 0
let touchMoveY = 0

export const DRAG_MIN_DELTA = 10
export enum AnimateScrollType {
  None = -1,
  Top = 0,
  Bottom = 1,
}

const resetTouchMonitor = () => {
  clearTimeout(timerId)
}

const resetTouchMode = () => {
  touchMode = 'tap'
}

/** 根据静止按压的时间依次调整本次操作为滑动/拖拽 */
const monitorTouchMode = (
  thumbnailManager: ThumbnailsManager,
  cb: (thumbnailManager: ThumbnailsManager, mode: EditorTouchAction) => void
) => {
  const next = (mode: EditorTouchAction) => cb(thumbnailManager, mode)
  resetTouchMonitor()
  timerId = setTimeout(() => {
    next('press')
    timerId = setTimeout(() => {
      next('longPress')
    }, LONG_PRESS_DURATION)
  }, PRESS_DURATION)
}

export function handleThumbnailTouchStart(
  thumbnailManager: ThumbnailsManager,
  e: PointerEvent
) {
  monitorTouchMode(thumbnailManager, handleTouchModeChange)

  const { thumbnails, pointerEvtInfo } = thumbnailManager
  const control = thumbnails.element
  if (pointerEvtInfo.isLocked === true && pointerEvtInfo.target !== control) {
    return false
  }
  pointerEvtInfo.onDown(e, true)
  thumbnailManager.onFocus()
  const { x: mouseX, y: mouseY } = pointerEvtInfo
  const pos = thumbnailManager.getMMPosAndSlidePosByMousePos(mouseX, mouseY)
  const posIndex = pos.slideIndex

  if (-1 < posIndex && thumbnailManager.slideThumbnails.length > 1) {
    thumbnailManager.dragStartIndex = posIndex
  }
  thumbnailManager.dragStartX = mouseX
  thumbnailManager.dragStartY = mouseY
  touchMoveY = mouseY
  return false
}

export function handleThumbnailTouchMove(
  thumbnailManager: ThumbnailsManager,
  e: PointerEvent
) {
  const { pointerEvtInfo, dragStartX, dragStartY } = thumbnailManager
  pointerEvtInfo.onMove(e)
  const { x, y } = pointerEvtInfo
  const [dx, dy] = [Math.abs(dragStartX - x), Math.abs(dragStartY - y)]

  if (hasMoved(dx, dy, EPS)) {
    resetTouchMonitor()
  }

  if (touchMode === 'tap') {
    const delta = adjustTouchScrollDelta(touchMoveY - y)
    emitThumbnailScroll(thumbnailManager, delta)
    touchMoveY = y
    return
  }

  if (
    touchMode === 'press' &&
    !EditorSettings.isViewMode &&
    hasMoved(dx, dy, DRAG_MIN_DELTA)
  ) {
    // 多选态下, 如果拖拽的对象未选中, 则退出多选, 选中该张并拖拽
    if (thumbnailManager.isMultiSelectState) {
      const selectedIndices = thumbnailManager.getSelectedSlideIndices()
      if (!selectedIndices.includes(thumbnailManager.dragStartIndex)) {
        thumbnailManager.isMultiSelectState = false
        thumbnailManager.selectSlides(thumbnailManager.dragStartIndex)
      }
    }
    emitThumbnailDragMove(thumbnailManager, x, y)
  }
}

export function handleThumbnailTouchEnd(
  thumbnailManager: ThumbnailsManager,
  e: PointerEvent
) {
  const currentTouchMode = touchMode
  resetTouchMonitor()
  resetTouchMode() // 如果 mouseUp 中需要用到 touchMode 则要在每个 return 处 reset

  const { editorUI, pointerEvtInfo, dragStartX, dragStartY } = thumbnailManager
  pointerEvtInfo.onUp(e)
  thumbnailManager.checkNeedAnimateScrollsByType(AnimateScrollType.None)
  const { x, y } = pointerEvtInfo
  const [dx, dy] = [Math.abs(dragStartX - x), Math.abs(dragStartY - y)]

  const selectedIndices = thumbnailManager.getSelectedSlideIndices() // 注意这里直接返回了原始引用
  const isMultiSelect = thumbnailManager.isMultiSelectState

  // click/longPress
  if (!hasMoved(dx, dy, EPS) && !thumbnailManager.isDraging) {
    const { slideIndex } = thumbnailManager.getMMPosAndSlidePosByMousePos(x, y)
    // 非多选态
    if (!isMultiSelect && -1 < slideIndex) {
      // 未选中则选中
      if (!selectedIndices.includes(slideIndex)) {
        thumbnailManager.selectSlides(slideIndex)
        editorUI.goToSlide(slideIndex, undefined, true)
        editorUI.editorKit.emitEvent(Evt_hideContextMenu)
      }
      // 已选中则触发右键菜单
      else {
        emitThumbnailContextMenu(thumbnailManager, { x, y })
      }
      thumbnailManager.syncSelectedSlideIndexes()
      thumbnailManager.render()
      return false
    }

    // 初次进入多选态, 本次 pointerUp 不重复响应选中逻辑
    if (isMultiSelect === 'pending') {
      thumbnailManager.isMultiSelectState = true
    }

    // 多选态
    if (isMultiSelect === true && -1 < slideIndex) {
      // 多选态再次长按触发菜单
      if (currentTouchMode === 'longPress') {
        emitThumbnailContextMenu(thumbnailManager, { x, y })
        return
      }
      const targetThumbnail = thumbnailManager.slideThumbnails[slideIndex]
      if (!targetThumbnail.selected) {
        thumbnailManager.selectSlides([...selectedIndices, slideIndex])
      } else {
        // 取消选中至最后一张时, 退出多选
        targetThumbnail.selected = false
        if (selectedIndices.length <= 2) {
          thumbnailManager.isMultiSelectState = false
        }
        thumbnailManager.syncSelectedSlideIndexes()
      }
      thumbnailManager.render()
    }
    return
  }

  // drag
  if (-1 < thumbnailManager.dragEndIndex) {
    // 非多选态，支持对未选中单张的 slide 拖拽; 多选态，仅支持对选中的 slides 操作1
    const newSelectedSlidesPosArr = thumbnailManager.presentation.reorderSlides(
      thumbnailManager.dragEndIndex,
      !isMultiSelect ? [thumbnailManager.dragStartIndex] : selectedIndices
    )

    if (newSelectedSlidesPosArr && newSelectedSlidesPosArr.length) {
      thumbnailManager.selectAll(newSelectedSlidesPosArr)
      thumbnailManager.clearCacheForce()
      thumbnailManager.updateEditLayer()
    }
  }
  resetThumbnailUIState(thumbnailManager)
}

function handleTouchModeChange(
  thumbnailManager: ThumbnailsManager,
  mode: EditorTouchAction
) {
  touchMode = mode

  if (touchMode === 'press') {
    return false
  }
  if (touchMode === 'longPress') {
    const pointerEvt = thumbnailManager.pointerEvtInfo
    const { x, y } = pointerEvt
    const pos = thumbnailManager.getMMPosAndSlidePosByMousePos(x, y)
    const targetThumbnail = thumbnailManager.slideThumbnails[pos.slideIndex]

    if (targetThumbnail?.selected === false) {
      // 进入多选态并选中
      thumbnailManager.isMultiSelectState = 'pending'
      const selectedIndices = thumbnailManager.getSelectedSlideIndices()
      thumbnailManager.selectSlides([...selectedIndices, pos.slideIndex])

      // 同步呼出针对已选 slides 的右键菜单
      if (-1 < pos.slideIndex) {
        emitThumbnailContextMenu(thumbnailManager, { x, y })
        return false
      }
    }
  }
}

export function emitThumbnailContextMenu(
  thumbnailManager: ThumbnailsManager,
  pixPos?: { x: number; y: number }
) {
  const { editorUI, pointerEvtInfo, thumbnails } = thumbnailManager
  const { x, y } = pixPos ?? pointerEvtInfo
  const menuData: ContextMenuData = {
    type: ContextMenuTypes.Thumbnails,
    absX:
      x -
      ((thumbnails.PositionInPx.L * Factor_mm_to_pix) >> 0) -
      editorUI.x +
      10,
    absY:
      y - ((thumbnails.PositionInPx.T * Factor_mm_to_pix) >> 0) - editorUI.y,
  }
  thumbnailManager.editorUI.editorKit.emitToggleContextMenu(menuData)
}

export function emitThumbnailScroll(
  thumbnailManager: ThumbnailsManager,
  dy: number
) {
  if (!thumbnailManager.scrollBar) return
  thumbnailManager.scrollBar.scrollBy(0, dy)
}

export function emitThumbnailDragMove(
  thumbnailManager: ThumbnailsManager,
  x: number,
  y: number
) {
  thumbnailManager.isDraging = true
  thumbnailManager.dragEndIndex = thumbnailManager.getSlideIndexMousePos(x, y)
  thumbnailManager.updateEditLayer()

  // 跟随拖拽滚动
  if (thumbnailManager.isScrollVisible) {
    const innerY = y - thumbnailManager.editorUI.y
    const absRect = thumbnailManager.thumbnails.PositionInPx
    const maxY = (absRect.B - absRect.T) * Factor_mm_to_pix

    let scrollType = AnimateScrollType.None
    if (innerY < 30) {
      scrollType = AnimateScrollType.Top
    } else if (innerY >= maxY - 30 /* && _Y < _YMax*/) {
      scrollType = AnimateScrollType.Bottom
    }

    thumbnailManager.checkNeedAnimateScrollsByType(scrollType)
  }
}

export function resetThumbnailUIState(thumbnailManager: ThumbnailsManager) {
  thumbnailManager.isDraging = false
  thumbnailManager.dragStartIndex = -1
  thumbnailManager.dragStartX = -1
  thumbnailManager.dragStartY = -1
  thumbnailManager.dragEndIndex = -1
}

function hasMoved(dx: number, dy: number, eps: number) {
  return Math.abs(dx) > eps || Math.abs(dy) > eps
}

function adjustTouchScrollDelta(dy: number) {
  // const absDy = Math.abs(dy ?? 0)
  // if (absDy < 20) {
  //   return (dy * 0.2) >> 0
  // } else if (20 <= absDy && absDy < 40) {
  //   return (dy * 0.3) >> 0
  // } else if (40 <= absDy && absDy < 80) {
  //   return (dy * 0.45) >> 0
  // } else if (80 <= absDy && absDy < 120) {
  //   return (dy * 0.6) >> 0
  // } else if (120 <= absDy && absDy < 200) {
  //   return (dy * 0.75) >> 0
  // } else if (200 <= absDy && absDy < 300) {
  //   return dy >> 0
  // } else if (300 <= dy) {
  //   return (dy * 1.2) >> 0
  // }
  return dy
}
