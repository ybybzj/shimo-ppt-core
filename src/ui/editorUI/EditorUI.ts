import { RenderController } from '../rendering/RenderController'
import { SlideMaster } from '../../core/Slide/SlideMaster'
import { ShowManager } from '../transitions/ShowManager'
import { TransitionManager } from '../transitions/TransitionManager'
import { UIControlContainer } from './Controls'
import { EditorSkin, FocusTarget } from '../../core/common/const/drawing'
import {
  stopEvent,
  GlobalEvents,
  POINTER_BUTTON,
  PointerEventType,
  PointerEventArg,
  listenPointerEvents,
  PointerEventInfo,
  preventDefault,
} from '../../common/dom/events'
import { BrowserInfo, updatePixelRatio } from '../../common/browserInfo'
import { getOffset } from '../../common/dom/utils'
import { assignRef, deref, Nullable, ref, Ref } from '../../../liber/pervasive'
import { debounce } from '../../common/utils'
import { initInputManager } from '../textInputManager'
import {
  Factor_mm_to_pix,
  Factor_pix_to_mm,
  THRESHOLD_DELTA,
} from '../../core/common/const/unit'
import { Evt_onUpdateLayout } from '../../editor/events/EventNames'
import { IEditorKit } from '../../editor/type'
import { InstanceType } from '../../core/instanceTypes'
import { Dom } from '../../common/dom/vdom'
import { NotesComponent } from './components/notes'
import { SlideRenderer } from '../rendering/Drawer/SlideRenderer'
import { EditorKit } from '../../editor/global'
import { SlideDrawerCONST } from '../rendering/Drawer/const'
import { ZoomValue } from '../../core/common/zoomValue'
import { MasterPreviewsDrawer } from '../rendering/Drawer/MasterPreviewsDrawer'
import { LayoutPreviewsDrawer } from '../rendering/Drawer/LayoutPreviewsDrawer'
import { DomWheelEvt } from '../../common/dom/wheelEvent'
import { EditorSettings } from '../../core/common/EditorSettings'
import { splitterStates, SCROLLBAR_WIDTH_PX } from './splitterStates'
import { ScrollComponent } from './components/scrolls'
import { ThumbnailsManager } from './components/thumbnailManager'
import { Splitters } from './components/splitter'
import {
  SlideMainContainerCtrl_t,
  MainContainer,
  MainContainerCtrl_t,
} from './controls/mainContainer'
import { RightPanelContainerCtrl } from './controls/RightPanel'

import {
  MainViewCtrl_t,
  SlideMainCtrl_t,
  SlideEditLayerCtrl_t,
  SlideAnimationLayerCtrl_t,
} from './controls/EditorView'
import { TextCursorComponent } from './components/cursors/textCursor'
import {
  convertPixToInnerMMPos,
  convertInnerMMToPixPos,
  executeWithCenterKeep,
  getScrollPageEPS,
} from './utils/utils'
import { InterfaceUITextureDrawer } from './InterfaceUITextureDrawer'
import {
  EditorHandler,
  IEditorHandler,
} from '../rendering/editorHandler/EditorHandler'
import { EditorUtil } from '../../globals/editor'
import { RenderingSlideInfo } from '../rendering/type'
import { AnimationPreviewer } from '../transitions/AnimationPreviewer'
import { requestAnimationFrame } from '../../../re/shim/RequestAnimationFrame.shim'
import { getZoomInValue, getZoomOutValue } from '../../core/common/const/zoom'
import { handleKeyDown, handleKeyPress, handleKeyUp, keyCodeMap } from './utils/keys'
import {
  handleMouseDown,
  handleMouseMove,
  handleMouseUp,
} from './utils/pointers'
import { syncSelectionState } from './utils/updates'
import { syncInterfaceState } from '../../editor/interfaceState/syncInterfaceState'
import { zoomValueForFitToSlide } from '../../core/utilities/zoom'
import { ScreenTipManager } from './components/screenTip'
import { CSS_CLASS, DOM_ID, DOM_STYLE } from './controls/const'
import { getAndUpdateCursorPositionForPresentation } from '../../core/utilities/cursor/cursorPosition'
import {
  KeyEvent_PreventPress,
  KeyEvent_DefaultPrevent,
  SCROLL_DEBOUNCE_LIMIT,
  FitModeValue,
  FitMode,
} from './const'
import { gSpecialPasteUtil } from '../../copyPaste/gSpecialPasteUtil'
import { calculateRenderingState } from '../../core/calculation/calculate/presentation'
import { GuidelinesManager } from '../features/guideLines/guideLines'
import { PointerCursorComponent } from './components/cursors/pointerCursor'
import {
  stopDemostration,
  handleEditorTouchEnd,
  handleEditorTouchMove,
  handleEditorTouchStart,
  checkTriggerViewImage,
  checkTriggerSelectScroll,
  checkClearSelectScroll,
  checkNeedReloadErrImages,
} from './utils/touches'

export class EditorUI {
  readonly instanceType = InstanceType.EditorUI
  editorKit: IEditorKit
  elemName: string
  editorElement: HTMLDivElement
  /** EditorSDK X 轴起点到 Document 文档最左方的距离(目前为 0) */
  x: number = 0
  /** EditorSDK Y 轴起点到 Document 文档最上方的距离(lizardOne + ui工具栏的高度) */
  y: number = 0
  width: number = 10
  height: number = 10
  zoomType?: FitModeValue = FitMode.Window
  slideMaster: null | SlideMaster = null
  // DomController
  editorControl!: UIControlContainer<string, HTMLElement, {}, []>
  panelRight!: RightPanelContainerCtrl
  mainView!: MainViewCtrl_t
  /** 控制权托管到 offscreen canvas, 取宽高元素尽量用计算结果, 避免直接取 dom 的 width/height */
  editorView!: SlideMainCtrl_t
  editLayer!: SlideEditLayerCtrl_t
  animationLayer!: SlideAnimationLayerCtrl_t
  mainContainer!: MainContainerCtrl_t
  mainContent!: SlideMainContainerCtrl_t
  // componentsManager
  transitionManager: TransitionManager
  splitters!: Splitters
  notesView!: NotesComponent
  thumbnailsManager!: ThumbnailsManager
  textCursor!: TextCursorComponent
  mouseCursor!: PointerCursorComponent
  scrolls!: ScrollComponent
  showManager: ShowManager
  renderingController: RenderController
  editorHandler: IEditorHandler
  // drawer
  slideRenderer: SlideRenderer
  layoutDrawer: LayoutPreviewsDrawer
  masterDrawer: MasterPreviewsDrawer
  uiTextureDrawer: InterfaceUITextureDrawer
  animationPreviewer: AnimationPreviewer
  screenTipManager!: ScreenTipManager
  guidelinesManager: GuidelinesManager
  // state
  needRerender: boolean = false
  isFocus: boolean = true
  /** 光标按下还未弹起的过程, 用于添加快捷键 */
  isLockMouse: boolean = false
  isUseKeyPress: boolean = true
  isKeydownWithoutPress!: boolean
  isAfterSlideSwitching = false
  paintTimerId: number = -1
  selectScrollTimerId: number = -1
  pointerEvtInfo: PointerEventInfo = new PointerEventInfo()

  private rePainting = false

  private _unlistenEvent: undefined | (() => void)
  get presentation() {
    return this.editorKit.presentation
  }

  get editorRenderingCanvas(): Nullable<HTMLCanvasElement> {
    return this.editorView.element
  }

  constructor(editorKit: IEditorKit) {
    this.editorKit = editorKit
    this.elemName = editorKit.targetElementId ?? ''
    this.editorElement = document.getElementById(
      this.elemName
    ) as HTMLDivElement

    this.renderingController = new RenderController(this)
    this.uiTextureDrawer = new InterfaceUITextureDrawer(this)
    this.layoutDrawer = new LayoutPreviewsDrawer()
    this.masterDrawer = new MasterPreviewsDrawer()
    this.slideRenderer = new SlideRenderer(this)

    this.showManager = new ShowManager(this)
    this.transitionManager = new TransitionManager(this)
    this.animationPreviewer = new AnimationPreviewer(this)
    this.editorHandler = new EditorHandler(this)
    this.guidelinesManager = new GuidelinesManager(this)

    this.init()
  }

  reset() {
    this.slideMaster = null
    if (this.presentation) {
      this.presentation.reset()
    }
  }

  initComponents() {
    this.thumbnailsManager = new ThumbnailsManager(this)
    this.splitters = new Splitters(this)
    this.textCursor = new TextCursorComponent(this)
    this.mouseCursor = new PointerCursorComponent(this)
    this.scrolls = new ScrollComponent(this)
  }

  init() {
    const elem = document.getElementById(this.elemName) as HTMLElement
    if (!elem) {
      // setTimeout(() => {
      //   this.init()
      // }, 2000)
      EditorKit.trackError('MISSING_DOM', new Error('Empty Editor Dom'), {
        ['elemName']: this.elemName,
      })
      console.error(`'Empty Editor Dom`, this.elemName)
      return
    }
    this.initComponents()
    initCSS(this.elemName)
    elem.style.backgroundColor = EditorSkin.Background_Light

    const thumbnailContainerId = DOM_ID.ThumbnailContainer.Id
    const mainContainerId = DOM_ID.MainContainer.Id

    // body ----
    this.editorControl = new UIControlContainer<
      string,
      HTMLElement,
      {},
      [typeof thumbnailContainerId, typeof mainContainerId]
    >(this.elemName, elem, [
      Dom.placeholder(thumbnailContainerId),
      Dom.placeholder(mainContainerId),
      Dom.div(`#${DOM_ID.Tip}`, { style: DOM_STYLE.Tip }),
      Dom.div(`#${DOM_ID.SplitterBox}`, { style: DOM_STYLE.SplitterBox }),
    ])
    this.screenTipManager = new ScreenTipManager(this)
    splitterStates.check()

    const mainContainer = MainContainer(this)

    this.editorControl.add(this.thumbnailsManager.control)
    this.editorControl.add(mainContainer)

    this.renderingController.onEditorUIInit()
    this._unlistenEvent = this.initEvents()
  }

  handlePointerEvent<T extends PointerEventType>(et: T, e: PointerEventArg<T>) {
    if (et === 'pointerdown') {
      this.pointerEvtInfo.lockPointerType(e as PointerEvent)
    }
    const isTouchMode = this.pointerEvtInfo.isTouchMode()
    switch (et) {
      case 'pointerdown': {
        this.textCursor.setCheckScroll(true)
        if (isTouchMode) {
          handleEditorTouchStart(this, e as PointerEvent)
        } else {
          this.onPointerDown(e as PointerEvent)
        }
        break
      }
      case 'pointermove': {
        if (isTouchMode) {
          handleEditorTouchMove(this, e as PointerEvent)
        } else {
          this.onPointerMove(e as PointerEvent)
        }
        break
      }
      case 'pointerup': {
        if (isTouchMode) {
          handleEditorTouchEnd(this, e as PointerEvent)
        } else {
          this.onPointerUp(e as PointerEvent)
        }
        this.pointerEvtInfo.unLockPointerType()
        break
      }
      case 'wheel': {
        this.onMouseWheel(e as DomWheelEvt)
      }
    }
  }

  isInTouchMode() {
    return this.pointerEvtInfo.isTouchMode()
  }

  initEvents() {
    if (window.addEventListener) {
      window.addEventListener('beforeunload', (_e) => {
        EditorKit.endPPTShow()
      })
    }

    this.editorControl.element.oncontextmenu = (e) => {
      stopEvent(e)
      return false
    }

    const pointerEvts: PointerEventType[] = [
      'pointerdown',
      'pointermove',
      'pointerup',
    ]
    const unlistenEditorView = listenPointerEvents(
      this.editorView.element,
      pointerEvts,
      this
    )

    const unlistenEditorLayer = listenPointerEvents(
      this.editLayer.element,
      pointerEvts,
      this
    )

    const unlistenMainContent = listenPointerEvents(
      this.mainContent.element,
      ['wheel'],
      this
    )

    const unlistenEditorControl = listenPointerEvents(
      this.editorControl.element,
      ['wheel'],
      (et, evt) => {
        if (et === 'wheel') {
          const { eventObj: e } = evt as DomWheelEvt
          e.preventDefault()
        }
      }
    )

    if (!EditorSettings.isMobile) {
      this.splitters.initEvents()
    }
    // this.initMobileEvents()

    this.thumbnailsManager.initEvents()
    this.notesView.initEvents()
    return () => {
      unlistenEditorLayer()
      unlistenEditorView()
      unlistenMainContent()
      unlistenEditorControl()
    }
  }

  removeEvents() {
    this.editorControl.element.oncontextmenu = null
    if (this._unlistenEvent) {
      this._unlistenEvent()
      this._unlistenEvent = undefined
    }

    this.scrolls.removeEvents()
    this.thumbnailsManager.removeEvents()
    this.notesView.removeEvents()
  }

  private calcFitWidthZoomValue() {
    let value = 100
    if (!this.presentation) return value

    const w = this.editorView.getPxWidth()

    let zoom = 100
    const slideWidth = this.presentation.width * Factor_mm_to_pix
    if (0 !== slideWidth) {
      zoom = (100 * (w - 2 * SlideDrawerCONST.BORDER)) / slideWidth
      if (zoom < 5) zoom = 5
    }
    value = zoom >> 0
    return value
  }
  private calcFitPageZoomValue() {
    return zoomValueForFitToSlide(this.presentation, {
      width: () => this.editorView.getWidth(),
      height: () => this.editorView.getHeight(),
    })
  }

  canZoom() {
    return this.editorHandler.canZoom()
  }

  zoomFitWidth() {
    this.zoomType = FitMode.Width
    if (!this.presentation) return
    if (!this.canZoom()) return

    const newZoom = this.calcFitWidthZoomValue()
    if (newZoom !== ZoomValue.value) {
      ZoomValue.value = newZoom
      this.exeZoom(FitMode.Width)
      return true
    } else {
      this.editorKit.emitZoomChange(ZoomValue.value, FitMode.Width)
    }
    return false
  }

  zoomFitPage() {
    this.zoomType = FitMode.Window
    if (!this.presentation) return
    if (!this.canZoom()) return

    const newZoom = this.calcFitPageZoomValue()
    if (newZoom !== ZoomValue.value) {
      ZoomValue.value = newZoom
      this.exeZoom(FitMode.Window)
      return true
    } else {
      this.editorKit.emitZoomChange(ZoomValue.value, FitMode.Window)
    }
    return false
  }

  zoomOut(byWheel = false) {
    if (false === this.editorKit.isEditorUIInitComplete) return
    if (!this.canZoom()) return

    const oldZoom = ZoomValue.value
    const zoom = getZoomOutValue(oldZoom, byWheel)

    if (ZoomValue.value <= zoom) return

    ZoomValue.value = zoom
    executeWithCenterKeep(this, () => {
      this.rePainting = true
      this.exeZoom(FitMode.Custom, zoom / oldZoom)
    })
  }
  zoomIn(byWheel = false) {
    if (false === this.editorKit.isEditorUIInitComplete) return
    if (!this.canZoom()) return

    const oldZoom = ZoomValue.value
    const zoom = getZoomInValue(oldZoom, byWheel)
    if (ZoomValue.value >= zoom) return

    ZoomValue.value = zoom
    executeWithCenterKeep(this, () => {
      this.rePainting = true
      this.exeZoom(FitMode.Custom, zoom / oldZoom)
    })
  }

  exeZoom(type?: FitModeValue, scaleVal?: number) {
    if (false === this.editorKit.isEditorUIInitComplete) return
    if (!this.canZoom()) return

    this.textCursor.setCheckScroll(false)
    this.zoomType = type
    this.updateDocumentSize()

    let curSlideIndex = this.renderingController.curSlideIndex
    if (this.showManager.isInShowMode) {
      curSlideIndex = this.showManager.currentSlideIndex
    }

    this.slideRenderer.updateRenderInfo(curSlideIndex, {
      keepCache: true,
      scaleVal,
    })
    this.updateDocumentSize()
    this.rerenderOnScrollOrZoom()
    this.editorKit.emitZoomChange(ZoomValue.value, type)

    if (EditorSettings.isNotesEnabled && this.notesView) {
      this.notesView.calcOnResize()
      if (this.presentation.isNotesFocused) {
        getAndUpdateCursorPositionForPresentation(this.presentation)
      }
    }
  }

  rerenderOnScrollOrZoom() {
    this.prepareRender()
    this._debounceRender()
  }

  private _debounceRender = debounce(() => {
    const curSlideIndex = this.renderingController.curSlideIndex
    this.slideRenderer.updateRenderInfo(curSlideIndex, {
      keepCache: true,
      scaleVal: 1,
      drawFromCache: false,
    })
    this.prepareRender()
    this.rePainting = false
  }, 200)

  toPrevSlide() {
    if (false === this.editorKit.isEditorUIInitComplete) return
    const editorUI = this

    if (this.showManager.isInShowMode) {
      if (0 < this.showManager.currentSlideIndex) {
        editorUI.goToSlide(this.showManager.currentSlideIndex - 1)
      } else {
        editorUI.goToSlide(0)
      }
    } else {
      if (0 < editorUI.renderingController.curSlideIndex) {
        editorUI.goToSlide(editorUI.renderingController.curSlideIndex - 1)
      } else {
        editorUI.goToSlide(0)
      }
    }
  }

  toNextSlide() {
    if (false === this.editorKit.isEditorUIInitComplete) return

    const editorUI = this
    const countOfSlides = editorUI.renderingController.countOfSlides

    if (this.showManager.isInShowMode) {
      if (countOfSlides - 1 > this.showManager.currentSlideIndex) {
        editorUI.goToSlide(this.showManager.currentSlideIndex + 1)
      } else if (countOfSlides > 0) {
        editorUI.goToSlide(countOfSlides - 1)
      }
    } else {
      if (countOfSlides - 1 > editorUI.renderingController.curSlideIndex) {
        editorUI.goToSlide(editorUI.renderingController.curSlideIndex + 1)
      } else if (countOfSlides > 0) {
        editorUI.goToSlide(countOfSlides - 1)
      }
    }
  }

  /** 左键拖拽期间跟随滚动, 会同步触发一次 mouseMove */
  onRectSelectcroll = () => {
    if (false === this.editorKit.isEditorUIInitComplete) return

    const editorUI = this
    const { mainContainer, mainContent, scrolls } = editorUI
    const minY = mainContent.PositionInPx.T * Factor_mm_to_pix + editorUI.y
    const maxY = mainContent.PositionInPx.B * Factor_mm_to_pix + editorUI.y
    const pointerEvt = this.pointerEvtInfo

    let scrollDeltaY = 0
    if (pointerEvt.y < minY) {
      let delta = 30
      if (20 > minY - pointerEvt.y) delta = 10
      scrollDeltaY = -delta
    } else if (pointerEvt.y > maxY) {
      let delta = 30
      if (20 > pointerEvt.y - maxY) delta = 10
      scrollDeltaY = delta
    }

    let scrollDeltaX = 0
    if (scrolls.canScrollHor) {
      const minX = mainContainer.PositionInPx.L * Factor_mm_to_pix + editorUI.x

      const maxX =
        mainContainer.PositionInPx.R * Factor_mm_to_pix +
        editorUI.x -
        SCROLLBAR_WIDTH_PX

      if (pointerEvt.x < minX) {
        let delta = 30
        if (20 > minX - pointerEvt.x) delta = 10
        scrollDeltaX = -delta
      } else if (pointerEvt.x > maxX) {
        let delta = 30
        if (20 > pointerEvt.x - maxX) delta = 10
        scrollDeltaX = delta
      }
    }

    if (0 !== scrollDeltaY) {
      scrolls.scrollByDeltaY(scrollDeltaY)
    }
    if (0 !== scrollDeltaX) {
      scrolls.scrollByDeltaX(scrollDeltaX)
    }

    if (scrollDeltaX !== 0 || scrollDeltaY !== 0) editorUI._handlePointerMove()
  }

  onPointerDown(e: PointerEvent) {
    if (false === this.editorKit.isEditorUIInitComplete) return

    const editorUI = this

    if (!BrowserInfo.isMobile) {
      // 点击获取焦点
      editorUI.focusInput()
    }
    stopDemostration(editorUI)
    preventDefault({ e })

    if (EditorUtil.inputManager && EditorUtil.inputManager.checkAndClearIME()) {
      return
    }
    const { renderingController, presentation } = editorUI
    presentation.setFocusTargetType(FocusTarget.EditorView)
    if (editorUI.showManager.isInShowMode) return false
    const pointerEvt = editorUI.pointerEvtInfo
    this.editorHandler.touchAction = undefined
    pointerEvt.onDown(e, true)

    if (
      POINTER_BUTTON.Left === pointerEvt.button ||
      null == pointerEvt.button
    ) {
      pointerEvt.button = POINTER_BUTTON.Left
      editorUI.isLockMouse = true

      if (renderingController.isEmptySlides && presentation.canEdit()) {
        presentation.addNextSlide()
        return
      }
    }
    // 正常应该不会, 通过日志观察到存在此种情况后续仍需排查
    if (this.presentation.currentSlideIndex === -1) {
      return
    }

    if (
      POINTER_BUTTON.Left === pointerEvt.button ||
      null == pointerEvt.button ||
      POINTER_BUTTON.Right === pointerEvt.button
    ) {
      const pos = convertPixToInnerMMPos(editorUI, pointerEvt.x, pointerEvt.y)

      const ret = renderingController.checkPlaceHolder(pos, 'mouseDown')
      if (ret) {
        stopEvent(e)
        return
      }
      handleMouseDown(editorUI, pointerEvt, pos.x, pos.y)
    }

    checkTriggerSelectScroll(editorUI)
    this.presentation.setFocusTargetType(FocusTarget.EditorView)
  }

  onPointerMove(e: PointerEvent) {
    if (false === this.editorKit.isEditorUIInitComplete) return
    preventDefault({ e })

    this.pointerEvtInfo.onMove(e)

    if (this.showManager.isInShowMode) return false
    this._handlePointerMove()
  }

  _handlePointerMove() {
    if (this.renderingController.isEmptySlides) return
    const pos = convertPixToInnerMMPos(
      this,
      this.pointerEvtInfo.x,
      this.pointerEvtInfo.y
    )
    const curSlideIndex = this.presentation.currentSlideIndex
    if (curSlideIndex === -1) return
    const needRepaint = this.renderingController.checkHoverEffect(pos)
    if (needRepaint === true) {
      return
    }
    handleMouseMove(this, this.pointerEvtInfo, pos.x, pos.y)
  }

  onPointerUp(e: PointerEvent) {
    const editorUI = this
    if (false === editorUI.editorKit.isEditorUIInitComplete) return

    const { pointerEvtInfo: pointerEvt, renderingController } = editorUI
    if (!pointerEvt.isLocked) return

    if (editorUI.showManager.isInShowMode) {
      preventDefault({ e })
      return false
    }

    pointerEvt.onUp(e)

    if (
      renderingController.isEmptySlides ||
      editorUI.presentation.currentSlideIndex === -1
    ) {
      return
    }

    const pos = convertPixToInnerMMPos(editorUI, pointerEvt.x, pointerEvt.y)
    editorUI.mouseCursor.unLock()
    editorUI.isLockMouse = false
    checkClearSelectScroll(editorUI)

    if (
      POINTER_BUTTON.Left === pointerEvt.button ||
      null == pointerEvt.button ||
      POINTER_BUTTON.Right === pointerEvt.button
    ) {
      checkTriggerViewImage(editorUI, pos)
    }

    if (renderingController.checkPlaceHolder(pos, 'mouseUp') === true) {
      const handler = editorUI.editorHandler
      handler.changeToState(InstanceType.UIInitState, handler)
      return
    }
    handleMouseUp(editorUI, pointerEvt, pos.x, pos.y)
    checkNeedReloadErrImages(editorUI, pos)
    editorUI.checkMobileInput()
  }

  onMouseWheel({ eventObj: e, origin }: DomWheelEvt) {
    if (false === this.editorKit.isEditorUIInitComplete) return

    if (this.showManager.isInShowMode) {
      e.preventDefault()

      return false
    }

    this.textCursor.setCheckScroll(false)
    let deltaX = 0
    let deltaY = 0

    // New school multidimensional scroll (touchpads) deltas
    deltaY = e.deltaY

    let ctrl = false
    if (origin.metaKey !== undefined) ctrl = origin.ctrlKey || origin.metaKey
    else ctrl = origin.ctrlKey

    if (true === ctrl) {
      e.preventDefault()
      origin.stopPropagation()

      if (deltaY > 0) {
        this.zoomOut(true)
      } else {
        this.zoomIn(true)
      }

      return false
    }

    if (this.scrolls.canScrollHor) {
      deltaX = e.deltaX
    }

    deltaX >>= 0
    deltaY >>= 0

    this.scrollByDelta(deltaX, deltaY)
    e.preventDefault()

    return false
  }

  canScroll() {
    return this.editorHandler.canScroll()
  }

  /** 默认同时只响应一个方向的滚动, 优先响应 x 轴方向 */
  scrollByDelta(deltaX: number, deltaY: number, lockDirection = true) {
    if (!this.canScroll()) return
    const scrolls = this.scrolls
    if (0 !== deltaX && !this.rePainting && this.scrolls.canScrollHor) {
      this.scrolls.updateCurCenterPos()
      scrolls.hScrollBar!.scrollBy(deltaX, 0)
      if (lockDirection && Math.abs(deltaX) < 10) {
        return
      }
    }
    if (0 !== deltaY && !this.rePainting) {
      if (this.isAfterSlideSwitching) {
        this._updateSlideSwitch(false)
      } else {
        if (scrolls.canScrollVByDeltaY(deltaY)) {
          this.scrolls.updateCurCenterPos()
          scrolls.vScrollBar!.scrollBy(0, deltaY)
        } else if (Math.abs(deltaY) > getScrollPageEPS(this)) {
          this._scrollGoToSlide(deltaY)
        }
      }
    }
  }

  private _updateSlideSwitch = debounce((b: boolean) => {
    this.isAfterSlideSwitching = b
  }, SCROLL_DEBOUNCE_LIMIT)

  private _scrollGoToSlide(delta: number) {
    const { currentSlideIndex: preSlideIndex, slides } = this.presentation
    if (delta < 0 && 0 < preSlideIndex) {
      this.toPrevSlide()
    } else if (delta > 0 && preSlideIndex + 1 < slides.length) {
      this.toNextSlide()
    }
    const curSlideIndex = this.presentation.currentSlideIndex
    if (preSlideIndex !== curSlideIndex) {
      this.thumbnailsManager.updateEditLayer()
      // Todo, 跳页后滚动条连续优化待确定
      this.scrolls.onGoToSlide(curSlideIndex)
    }
    // 阻止切换页时引起下一页滚动
    this.isAfterSlideSwitching = true
  }

  onKeyDown = (e: KeyboardEvent) => {
    const editorUI = this
    if (false === editorUI.editorKit.isEditorUIInitComplete) {
      GlobalEvents.keyboard.updateModifyKeys(e)
      e.preventDefault()
      return
    }

    if (editorUI.isFocus === false) return

    if (editorUI.isLockMouse === true) {
      GlobalEvents.keyboard.updateModifyKeys(e)
      e.preventDefault()
      return
    }

    if (
      editorUI.editorKit.skipShortcut != null &&
      editorUI.editorKit.skipShortcut(e)
    ) {
      return
    }

    if (this.showManager.isInShowMode) {
      editorUI.showManager.onKeyDown(e)
      return
    }

    if (editorUI.presentation.getFocusType() === FocusTarget.Thumbnails) {
      if (0 === splitterStates.thumbnailSplitterPos) {
        e.preventDefault()
        return false
      }

      const ret = editorUI.thumbnailsManager.onKeyDown(e)
      if (false === ret) return false
      if (undefined === ret) return
    }

    if (editorUI.transitionManager.isPlaying()) {
      editorUI.transitionManager.end()
    }

    this.textCursor.setCheckScroll(true)
    GlobalEvents.keyboard.onKeyEvent(e)

    editorUI.isKeydownWithoutPress = true

    const keyDownResult = handleKeyDown(this, GlobalEvents.keyboard)
    editorUI.isUseKeyPress =
      (keyDownResult & KeyEvent_PreventPress) !== 0 ? false : true

    if ((keyDownResult & KeyEvent_DefaultPrevent) !== 0) {
      e.preventDefault()
    }
  }

  onKeyPress = (e: KeyboardEvent) => {
    if (this.presentation.getFocusType() === FocusTarget.Thumbnails) {
      return
    }

    const editorUI = this
    if (
      false === editorUI.editorKit.isEditorUIInitComplete ||
      editorUI.isFocus === false ||
      editorUI.isLockMouse === true
    ) {
      return
    }

    if (BrowserInfo.isOpera && !editorUI.isKeydownWithoutPress) {
      editorUI.onKeyDown(e)
    }

    if (
      editorUI.editorKit.skipShortcut != null &&
      editorUI.editorKit.skipShortcut(e)
    ) {
      return
    }

    editorUI.isKeydownWithoutPress = false

    if (this.showManager.isInShowMode) return
    if (false === editorUI.isUseKeyPress) return
    if (null == editorUI.presentation) return

    GlobalEvents.keyboard.onKeyEvent(e)

    const returnVal = handleKeyPress(editorUI, GlobalEvents.keyboard)
    if (true === returnVal) {
      e.preventDefault()
    }
    return
  }

  onKeyUp = (_e: KeyboardEvent) => {
    handleKeyUp(this, GlobalEvents.keyboard)
    if (BrowserInfo.inWindows && GlobalEvents.keyboard.keyCode === keyCodeMap.altOrOption) {
      _e.preventDefault()
    }
  }

  onResize(isForce?, isDirty = false) {
    updatePixelRatio()

    const isSizeChanged = ref(false)
    syncEditorElementSize(this, isSizeChanged)
    if (!deref(isSizeChanged) && false === isForce) {
      this.showManager.resize()
      return
    }

    this.notesView.onResize()
    this.resize(isDirty)
  }

  resize(isDirty = false) {
    const widthInMM = this.width * Factor_pix_to_mm
    const heightInMM = this.height * Factor_pix_to_mm
    this.editorControl.resize(widthInMM, heightInMM, this, isDirty)

    if (EditorUtil.inputManager) {
      EditorUtil.inputManager.onResize(this.mainContainer.name)
    }

    this.showManager.resize()

    const canZoom = this.scrolls.canZoom()

    // 由于触屏模式下无法判断是否唤起了虚拟键盘, 因此只能根据尺寸判断如果太小则需要放大聚焦到光标处
    // 部分环境下浏览器会自动放大聚焦，此时不再辅助聚焦
    // const handler = this.editorHandler
    // const curDpr = devicePixelRatio
    // if (
    //   BrowserInfo.isTablet &&
    //   BrowserInfo.originPixelRatio === curDpr &&
    //   this.isInTouchMode() &&
    //   handler.textEditingState != null
    // ) {
    //   // Tablet 判断基于物理尺寸但实际 zoom 需要参照可用尺寸
    //   const availWidth = window.innerWidth
    //   const availHeight = window.innerHeight
    //   const smallerSideSize = Math.min(availWidth, availHeight)

    //   if (smallerSideSize < TabletMiniMumSize) {
    //     // 暂时不考虑竖屏
    //     const ratio = smallerSideSize / BrowserInfo.deviceSmallerSide
    //     ZoomValue.save()
    //     const targetZoom = Math.round(ZoomValue.Value / ratio)
    //     ZoomValue.Value = Math.min(targetZoom, Max_ZoomValue)
    //     this.exeZoom(FitMode.Custom)
    //     handler.scrollElementIntoView(handler.textEditingState.sp)
    //     return
    //   }
    // }
    // // 触屏模式收起虚拟键盘, 优先还原缩放
    // if (ZoomValue.checkRestore()) {
    //   executeWithCenterKeep(this, () => {
    //     this.exeZoom(FitMode.Custom)
    //   })
    //   return
    // }

    if (FitMode.Width === this.zoomType && canZoom) {
      if (true === this.zoomFitWidth()) {
        this.checkRenderSync()
        return
      }
    }
    if (FitMode.Window === this.zoomType && canZoom) {
      if (true === this.zoomFitPage()) {
        this.checkRenderSync()
        return
      }
    }

    this.thumbnailsManager.needUpdate = true
    this.updateDocumentSize()

    if (this.presentation) {
      this.slideRenderer.updateRenderInfo(
        this.renderingController.curSlideIndex
      )
    }
    this.prepareRender()
    this.checkRenderSync(true)

    if (EditorSettings.isNotesEnabled && this.notesView) {
      this.notesView.calcOnResize()
    }
  }

  getSlideRenderingInfo(): RenderingSlideInfo {
    return {
      renderingRect: this.renderingController.curSlideRect,
      widthInMM: this.presentation.width,
      heightInMM: this.presentation.height,
    }
  }

  private calcRenderInfo() {
    if (false === this.editorKit.isEditorUIInitComplete) {
      this.scrolls.updateSettingsForScroll()
      return
    }

    const canvas = this.editorView.element
    if (null == canvas) return

    this.scrolls.calcSlideRenderInfo(this.renderingController.curSlideRect)
  }

  prepareRender() {
    this.calcRenderInfo()
    this.needRerender = true
    this.renderingController.onUpdateEditLayer()
  }

  private updateDocumentSize() {
    if (false === this.editorKit.isEditorUIInitComplete) {
      this.scrolls.updateSettingsForScroll()
      return
    }

    if (FitMode.Width === this.zoomType) {
      if (true === this.zoomFitWidth()) return
    }
    if (FitMode.Window === this.zoomType) {
      if (true === this.zoomFitPage()) return
    }

    this.scrolls.onUpdateDocumentSize()
    this.thumbnailsManager.onUpdateDocumentSize()
  }

  initUIControl() {
    this.thumbnailsManager.onControlInit()
    this.updateDocumentSize()
    this.startRender()
    this.editorKit.emitThumbnailsShow()

    if (!EditorSettings.isMobile) {
      initInputManager(this.editorKit, this.mainContainer.name)
    }
  }

  private startRender() {
    if (-1 === this.paintTimerId) this.checkRender()
  }

  private checkRender = () => {
    if (this.needRerender) {
      this.needRerender = false
      this.renderSlide()
    } else {
      this.thumbnailsManager.checkNeedUpdate()
    }

    this.renderingController.checkRender()
    this.notesView.checkRender()

    this.paintTimerId = requestAnimationFrame(
      this.checkRender
    ) as unknown as number
  }

  private checkRenderSync(isCheckTh?) {
    if (this.needRerender) {
      this.needRerender = false
      this.renderSlide()
      this.renderingController.rerenderEditLayer()
      if (isCheckTh !== undefined) {
        this.thumbnailsManager.checkNeedUpdate()
      }
      gSpecialPasteUtil.updateUI()
    } else {
      this.thumbnailsManager.checkNeedUpdate()
    }
    if (EditorSettings.isNotesEnabled && this.notesView) {
      this.editorKit.emitChangeNotesHeight(
        this.notesView.notes.element.style.height
      )
    }
  }

  private renderSlide() {
    if (false === this.editorKit.isEditorUIInitComplete) return

    const canvas = this.editorRenderingCanvas
    if (canvas == null) return

    // const width = context.canvas.width
    // const height = context.canvas.height
    // const fillStyle = context.fillStyle
    // context.clearRect(0, 0, width, height)
    // context.fillStyle = EditorSkin.Background_Light
    // context.fillRect(0, 0, width, height)
    // context.fillStyle = fillStyle
    if (canvas.width > 0 && canvas.height > 0) {
      this.slideRenderer.drawSlide(
        canvas,
        this.renderingController.curSlideIndex
      )
    }
  }

  updateLayouts(isForce?: boolean) {
    const { presentation, layoutDrawer, renderingController } = this
    if (
      -1 === this.renderingController.curSlideIndex &&
      0 === presentation.slideMasters.length
    ) {
      return
    }

    const master =
      presentation.slides[renderingController.curSlideIndex]?.layout?.master ??
      presentation.slideMasters[0]

    if (
      master &&
      (this.slideMaster !== master ||
        Math.abs(layoutDrawer.widthInMM - presentation.width) >
          THRESHOLD_DELTA ||
        Math.abs(layoutDrawer.heightInMM - presentation.height) >
          THRESHOLD_DELTA ||
        isForce === true)
    ) {
      this.slideMaster = master as SlideMaster

      const needRerender =
        Math.abs(layoutDrawer.widthInMM - presentation.width) >
          THRESHOLD_DELTA ||
        Math.abs(layoutDrawer.heightInMM - presentation.height) >
          THRESHOLD_DELTA

      master.layouts.map((layout) => {
        if ('' === layout.imgBase64 || needRerender) {
          layoutDrawer.widthInMM = presentation.width
          layoutDrawer.heightInMM = presentation.height
          layout.imgBase64 = layoutDrawer.getThumbnail(layout)
          layout.thumbPixWidth = layoutDrawer.widthInPx
          layout.thumbPixHeight = layoutDrawer.heightInPx
        }
      })
      this.editorKit.emitEvent(Evt_onUpdateLayout)
    }
  }

  private _afterGoToSlide(
    slideIndex: number,
    orgIsEmptySlides,
    isUpdate,
    isCalculateNote,
    fromThumbnailClick: boolean,
    isForce: boolean,
    ignoreScroll?: boolean
  ) {
    const textDocument = this.renderingController
    if (isForce && !isCalculateNote) {
      const curSlideIndex = this.presentation.currentSlideIndex
      if (curSlideIndex >= 0) {
        this.notesView.calculateState(
          curSlideIndex,
          this.presentation.slides[curSlideIndex].notesWidth,
          this.presentation.slides[curSlideIndex].getNotesShapeHeight()
        )
      }
    }

    this.updateLayouts()
    this.slideRenderer.updateRenderInfo(textDocument.curSlideIndex)
    syncInterfaceState(this.editorKit)

    this.updateDocumentSize()
    if (fromThumbnailClick !== true) {
      this.thumbnailsManager.selectSlides(slideIndex, ignoreScroll)
    }
    this.calcRenderInfo()

    if (ignoreScroll !== true) {
      this.scrolls.onGoToSlide(slideIndex)
    }

    if (EditorSettings.isViewMode === false && null != this.presentation) {
      getAndUpdateCursorPositionForPresentation(this.presentation)

      this.editorKit.emitCurrentSlide(textDocument.curSlideIndex)
    } else {
      this.editorKit.emitCurrentSlide(textDocument.curSlideIndex)
    }

    syncSelectionState(this)
    this.presentation.slideSelectManager.unlockFocusOnMain()

    if (
      this.renderingController.isEmptySlides !== orgIsEmptySlides ||
      isUpdate ||
      isForce === true
    ) {
      calculateRenderingState(this.presentation)
      this.prepareRender()
    }
  }

  goToSlide(
    slideIndex: number,
    isForce = false,
    fromThumbnailClick = false,
    ignoreScroll?: boolean
  ) {
    const renderingController = this.renderingController
    renderingController.clearAdornment()
    if (renderingController.countOfSlides <= 0) {
      slideIndex = -1
    }

    if (this.showManager.isInShowMode) {
      return this.editorKit.goToSlideInPPTShow(slideIndex)
    }

    if (!this.scrolls.vScrollBar) {
      return
    }

    const orgIsEmptySlides = this.renderingController.isEmptySlides

    this.renderingController.isEmptySlides = false
    if (-1 === slideIndex) {
      this.renderingController.isEmptySlides = true

      if (EditorSettings.isNotesEnabled && this.notesView) {
        this.notesView.calculateState(-1, 0, 0)
      }
    }

    if (this.transitionManager.isPlaying()) {
      this.transitionManager.end()
    }

    if (
      slideIndex !== -1 &&
      (slideIndex < 0 || slideIndex >= renderingController.countOfSlides)
    ) {
      return
    }

    this.presentation.slideSelectManager.lockFocusOnMain()
    this.scrolls.startVScroll = false

    const isUpdate = renderingController.curSlideIndex !== slideIndex
    const isCalculateNote = this.presentation.setCurSlide(slideIndex, isForce)

    this._afterGoToSlide(
      slideIndex,
      orgIsEmptySlides,
      isUpdate,
      isCalculateNote,
      fromThumbnailClick,
      isForce,
      ignoreScroll
    )
  }

  onCalculateSlide(index) {
    this.thumbnailsManager.updateSlideThumbnailDirty(index)

    const currentSlide = this.renderingController.curSlideIndex

    if (index === currentSlide) {
      this.triggerRender()
    }
  }

  private _debounceTriggerRender = debounce(
    (editorUI: EditorUI, keepCache: boolean) => {
      editorUI.presentation.slideSelectManager.lockFocusOnMain()

      const options =
        keepCache === true
          ? { keepCache: true, scaleVal: 1, drawFromCache: false }
          : undefined

      editorUI.slideRenderer.updateRenderInfo(
        editorUI.renderingController.curSlideIndex,
        options
      )
      editorUI.updateDocumentSize()

      editorUI.prepareRender()
      editorUI.presentation.slideSelectManager.unlockFocusOnMain()
    },
    16
  )

  triggerRender(keepCache = false) {
    this._debounceTriggerRender(this, keepCache)
  }

  calculatePreviews() {
    this.thumbnailsManager.calculateState()
    this.editorKit.slidesPreviewer?.calculateState()
  }

  setThumbnailsVisibility(isShow: boolean) {
    if (isShow) {
      splitterStates.resetThumbnailSplitterPos()
      this.splitters.onResize()
    } else {
      splitterStates.zeroThumbnailSplitterPos(() => {
        this.splitters.onResize()
      })
    }
  }

  setNotesEnable(isEnabled) {
    if (isEnabled === EditorSettings.isNotesEnabled) return
    EditorSettings.isNotesEnabled = isEnabled
    EditorSettings.isRenderingNotes = isEnabled

    this.editorHandler.resetSelectionState()
    splitterStates.resetNoteSplitterPos()
    this.splitters.onResize()
  }

  getNotesWidth() {
    if (!EditorSettings.isNotesEnabled) return 0

    if (!this.notesView) return 0

    return this.notesView.getPixWidth()
  }

  calculateStateForNotes(slideIndex, width, height) {
    if (!EditorSettings.isNotesEnabled || !this.notesView) return
    this.notesView.calculateState(slideIndex, width, height)
  }

  scrollToPosition(x: number, y: number, w: number, h: number) {
    if (this.presentation.slides.length <= 0) return

    const pos = convertInnerMMToPixPos(this, x, y, false, -0.5)
    const { scrollX, scrollY } = this.scrolls.state

    const scrollXMax = this.scrolls.state.scrollYmax //
    const scrollYMax = this.scrolls.state.scrollYMax

    const pixEditorWidth = this.editorView.getPxWidth()
    const pixEditorHeight = this.editorView.getPxHeight()

    const width = (w * (ZoomValue.value * Factor_mm_to_pix)) / 100
    let deltaX = (pos.x + width / 2 - pixEditorWidth / 2) >> 0
    if (deltaX + scrollX < 0) deltaX = -scrollX
    if (deltaX + scrollX > scrollXMax) deltaX = scrollXMax - scrollX
    this.scrolls.scrollByDeltaX(deltaX)

    const height = (h * (ZoomValue.value * Factor_mm_to_pix)) / 100
    let deltaY = (pos.y + height / 2 - pixEditorHeight / 2) >> 0
    if (deltaY + scrollY < 0) deltaY = -scrollY
    if (deltaY + scrollY > scrollYMax) deltaY = scrollYMax - scrollY
    this.scrolls.scrollByDeltaY(deltaY)
    this.scrolls.updateCurCenterPos()
    this.prepareRender()
  }

  setKeyEventsFocus(value: boolean) {
    if (this.isFocus !== value) {
      this.isFocus = value
    }
  }

  focusInput() {
    this.setKeyEventsFocus(true)
    EditorUtil.inputManager?.focusInpustElement()
  }

  focusEditor() {
    this.setKeyEventsFocus(true)
    EditorUtil.inputManager.focusEditorElement()
  }

  blurInput() {
    EditorUtil.inputManager.blurInpustElement()
  }

  /** 适配触屏设备的焦点管理
   * 聚焦到文本框才 focus 到 inputArea(否则会频繁弹起键盘)
   * 否则 focus 到 editorElement 以捕获 keyEvent */
  checkMobileInput() {
    if (!BrowserInfo.isTablet && !this.isInTouchMode()) return
    if (
      BrowserInfo.hasExternalKeyBoard ||
      this.editorHandler.textEditingState != null
    ) {
      this.focusInput()
    } else {
      this.blurInput()
      this.focusEditor()
    }
  }

  focusForKeyHandling() {
    if (
      !this.isInTouchMode() ||
      BrowserInfo.hasExternalKeyBoard ||
      this.editorHandler.textEditingState != null
    ) {
      this.focusInput()
    } else {
      this.blurInput()
      this.focusEditor()
    }
  }

  checkDemoFocus() {
    if (!BrowserInfo.isTablet && !this.isInTouchMode()) {
      this.focusInput()
      return
    }
    this.checkMobileInput()
  }

  calcLayoutDirection() {
    splitterStates.resetThumbnailSplitterPos()
    this.onResize(undefined, true)
  }
}

// helpers
function initCSS(elementId: string) {
  const head = document.getElementsByTagName('head')[0]

  const blockStyle = document.createElement('style')
  blockStyle.type = 'text/css'
  blockStyle.innerHTML = `#${elementId} .${CSS_CLASS.Block} { position:absolute;padding:0;margin:0; direction: ltr }`
  head.appendChild(blockStyle)
}

function syncEditorElementSize(
  editorUI: EditorUI,
  isSizeChanged?: Ref<boolean>
) {
  const el = editorUI.editorElement
  if (!el) return
  const off = getOffset(el)!

  if (off) {
    editorUI.x = off.left
    editorUI.y = off.top
  }

  if (
    editorUI.width !== el.offsetWidth ||
    editorUI.height !== el.offsetHeight
  ) {
    editorUI.width = el.offsetWidth
    editorUI.height = el.offsetHeight
    if (isSizeChanged != null) {
      assignRef(isSizeChanged, true)
    }
  } else if (isSizeChanged != null) {
    assignRef(isSizeChanged, false)
  }
}
