import { Dict } from '../../../liber/pervasive'
import { enlargeByRetinaRatio } from '../../common/browserInfo'
import { updateWidtAndHeightForCanvasWithMirror } from '../../common/dom/OffscreenCanvas'
import { replaceNodeWith } from '../../common/dom/utils'
import { Dom, fillElement, toElement } from '../../common/dom/vdom'
import { VNode } from '../../common/dom/vdom/vnode'
import { Factor_mm_to_pix } from '../../core/common/const/unit'
import {
  ANCHOR_BOTTOM,
  ANCHOR_LEFT,
  ANCHOR_RIGHT,
  ANCHOR_TOP,
  DOM_ID,
} from './controls/const'
import { getCtrlParamsById } from './controls/rtl'
import { EditorUI } from './EditorUI'

/** [l,t,r,b,isAbsL,isAbsT,isAbsR,isAbsB,absW,absh] */
export type BoundsParams = [
  number,
  number,
  number,
  number,
  boolean,
  boolean,
  boolean,
  boolean,
  number,
  number,
]
export class ControlBounds {
  L: number
  T: number
  R: number
  B: number
  isPxL: boolean
  isPxT: boolean
  isPxR: boolean
  isPxB: boolean
  witdh: number
  height: number
  constructor() {
    this.L = 0 // left border
    this.T = 0 // upper bound
    this.R = 0 // right border (if the isAbsR flag is set, then this distance is to the right, not R)
    this.B = 0 // upper bound (if isAbsB flag is on, then this distance is to the bottom, not B)

    this.isPxL = false
    this.isPxT = false
    this.isPxR = false
    this.isPxB = false

    this.witdh = -1
    this.height = -1
  }
  setParams(
    l: number,
    t: number,
    r: number,
    b: number,
    absoluteL: boolean,
    absoluteT: boolean,
    absoluteR: boolean,
    absoluteB: boolean,
    absoluteW: number,
    absoluteH: number
  ) {
    this.L = l
    this.T = t
    this.R = r
    this.B = b

    this.isPxL = absoluteL
    this.isPxT = absoluteT
    this.isPxR = absoluteR
    this.isPxB = absoluteB

    this.witdh = absoluteW
    this.height = absoluteH
  }
}

type Elements<E extends HTMLElement = HTMLElement> = Record<string, E>
export class UIControl<
  Name extends string,
  Elm extends HTMLElement,
  Elms extends Elements = Elements,
> {
  bounds: ControlBounds
  anchor: number
  name: string
  parent: UIControlContainer<
    string,
    HTMLElement,
    Dict<HTMLElement>,
    string[]
  > | null
  element: Elm
  PositionInPx: { L: number; T: number; R: number; B: number }
  elements: Elms

  constructor(name: Name, node: VNode<Elm>) {
    this.anchor = ANCHOR_LEFT | ANCHOR_TOP
    this.bounds = new ControlBounds()
    this.PositionInPx = { L: 0, T: 0, R: 0, B: 0 }

    this.name = name
    const elements: Record<string, HTMLElement> = {}

    this.element = toElement(node, elements) as Elm

    this.elements = elements as Elms
    this.parent = null
  }
  resize(width: number, height: number, editorUI: EditorUI, isDirty = false) {
    if (null == this.parent || null == this.element) return

    if (isDirty === true) {
      const params = getCtrlParamsById(this.name, editorUI)
      if (params) {
        const [anchor, bounds] = params
        this.anchor = anchor
        this.bounds.setParams(...bounds)
      }
    }

    resizeControlBySize(this, width, height)
    const { L, T, R, B } = this.PositionInPx
    let w = ((R - L) * Factor_mm_to_pix + 0.5) >> 0
    let h = ((B - T) * Factor_mm_to_pix + 0.5) >> 0
    if (needHandlerRetina(this.element)) {
      ;[w, h] = [enlargeByRetinaRatio(w), enlargeByRetinaRatio(h)]
    }

    if (this.element.nodeName === 'CANVAS') {
      updateWidtAndHeightForCanvasWithMirror(
        this.element as unknown as HTMLCanvasElement,
        w,
        h
      )
    } else {
      ;(this.element as any).width = w
      ;(this.element as any).height = h
    }
  }

  /** css 像素 */
  getPxWidth() {
    return (
      ((this.PositionInPx.R - this.PositionInPx.L) * Factor_mm_to_pix + 0.5) >>
      0
    )
  }
  /** css 像素 */
  getPxHeight() {
    return (
      ((this.PositionInPx.B - this.PositionInPx.T) * Factor_mm_to_pix + 0.5) >>
      0
    )
  }

  /** 对于 canvas 元素, 返回其宽高; 对于其他元素返回 css 尺寸 */
  getWidth() {
    if (this.element.nodeName === 'CANVAS' && needHandlerRetina(this.element)) {
      return enlargeByRetinaRatio(this.getPxWidth())
    }
    return this.getPxWidth()
  }

  /** 对于 canvas 元素, 返回其宽高; 对于其他元素返回 css 尺寸 */
  getHeight() {
    if (this.element.nodeName === 'CANVAS' && needHandlerRetina(this.element)) {
      return enlargeByRetinaRatio(this.getPxHeight())
    }
    return this.getPxHeight()
  }
}

export class UIControlContainer<
  Name extends string,
  Elm extends HTMLElement,
  Elms extends Elements,
  Placeholders extends string[],
> {
  bounds: ControlBounds
  anchor: number
  name: string
  parent: UIControlContainer<string, HTMLElement, Elements, string[]> | null
  element: Elm
  elements: Elms
  PositionInPx: { L: number; T: number; R: number; B: number }
  Controls: Array<
    | UIControl<Placeholders[number], HTMLElement, Elements>
    | UIControlContainer<Placeholders[number], HTMLElement, Elements, string[]>
  >
  placeholders: Record<Placeholders[number], HTMLElement>
  constructor(name: Name, node: VNode<Elm>)
  constructor(name: Name, elm: Elm, childrenNodes: VNode[])
  constructor(name: Name, ...args: any[]) {
    this.bounds = new ControlBounds()
    this.anchor = ANCHOR_LEFT | ANCHOR_TOP
    this.PositionInPx = { L: 0, T: 0, R: 0, B: 0 }
    this.Controls = []

    this.name = name
    const elements: Record<string, HTMLElement> = {}
    const placeholders = {}

    if (args.length === 1) {
      const node = args[0]
      this.element = toElement(node, elements, placeholders)
    } else {
      const elm = args[0]
      const childrenNodes = args[1]
      this.element = fillElement(elm, childrenNodes, elements, placeholders)
    }

    this.elements = elements as Elms

    this.placeholders = placeholders as Record<
      Placeholders[number],
      HTMLElement
    >
    this.parent = null
  }
  add(
    ctrl:
      | UIControl<Placeholders[number], HTMLElement, Elements>
      | UIControlContainer<
          Placeholders[number],
          HTMLElement,
          Elements,
          string[]
        >
  ): boolean {
    const placeholder = this.placeholders[ctrl.name]
    const ctrlElm = ctrl.element
    if (placeholder && ctrlElm) {
      replaceNodeWith(placeholder, ctrlElm)
      delete this.placeholders[ctrl.name]
      ctrl.parent = this
      this.Controls[this.Controls.length] = ctrl
      return true
    }
    return false
  }

  removeByName<Name extends Placeholders[number]>(
    name: Name
  ):
    | undefined
    | UIControl<Name, HTMLElement, Elements>
    | UIControlContainer<Name, HTMLElement, Elements, string[]> {
    let index = -1
    let ctrl
    for (let i = 0, l = this.Controls.length; i < l; i++) {
      const child = this.Controls[i]
      if (child.name === name) {
        index = i
        break
      }
    }
    if (index > -1) {
      ctrl = this.Controls[index]
      const placeholder = toElement(Dom.placeholder(name))
      this.placeholders[name] = placeholder
      replaceNodeWith(ctrl.element, placeholder)
      this.Controls.splice(index, 1)
    }
    return ctrl
  }

  resize(width, height, editorUI: EditorUI, isDirty = false) {
    if (isDirty === true) {
      const params = getCtrlParamsById(this.name, editorUI)
      if (params) {
        const [anchor, bounds] = params
        this.anchor = anchor
        this.bounds.setParams(...bounds)
      }
    }
    if (null == this.parent) {
      this.PositionInPx.L = 0
      this.PositionInPx.T = 0
      this.PositionInPx.R = width
      this.PositionInPx.B = height

      if (null != this.element) {
        const l = this.Controls.length
        for (let i = 0; i < l; i++) {
          this.Controls[i].resize(width, height, editorUI, isDirty)
        }
      }
      return
    }

    resizeControlBySize(this, width, height)
    const { L, T, R, B } = this.PositionInPx

    const l = this.Controls.length
    for (let i = 0; i < l; i++) {
      this.Controls[i].resize(R - L, B - T, editorUI, isDirty)
    }
  }

  /** css 像素 */
  getPxWidth() {
    return (
      ((this.PositionInPx.R - this.PositionInPx.L) * Factor_mm_to_pix + 0.5) >>
      0
    )
  }

  /** css 像素 */
  getPxHeight() {
    return (
      ((this.PositionInPx.B - this.PositionInPx.T) * Factor_mm_to_pix + 0.5) >>
      0
    )
  }
}

/** 根据新的 widthInMM, heightInMM 和当前的 anchor, bounds 计算绝对定位 */
function resizeControlBySize(
  target: UIControl<any, any, any> | UIControlContainer<any, any, any, any>,
  width: number,
  height: number
) {
  let [x, y, r, b] = [0, 0, 0, 0]

  const anchorHor = target.anchor & 0x05
  const anchorVer = target.anchor & 0x0a

  if (ANCHOR_LEFT === anchorHor) {
    if (target.bounds.isPxL) x = target.bounds.L
    else x = (target.bounds.L * width) / 1000

    if (-1 !== target.bounds.witdh) r = x + target.bounds.witdh
    else {
      if (target.bounds.isPxR) r = width - target.bounds.R
      else r = (target.bounds.R * width) / 1000
    }
  } else if (ANCHOR_RIGHT === anchorHor) {
    if (target.bounds.isPxR) r = width - target.bounds.R
    else r = (target.bounds.R * width) / 1000

    if (-1 !== target.bounds.witdh) x = r - target.bounds.witdh
    else {
      if (target.bounds.isPxL) x = target.bounds.L
      else x = (target.bounds.L * width) / 1000
    }
  } else if ((ANCHOR_LEFT | ANCHOR_RIGHT) === anchorHor) {
    if (target.bounds.isPxL) x = target.bounds.L
    else x = (target.bounds.L * width) / 1000

    if (target.bounds.isPxR) r = width - target.bounds.R
    else r = (target.bounds.R * width) / 1000
  } else {
    x = target.bounds.L
    r = target.bounds.R
  }

  if (ANCHOR_TOP === anchorVer) {
    if (target.bounds.isPxT) y = target.bounds.T
    else y = (target.bounds.T * height) / 1000

    if (-1 !== target.bounds.height) b = y + target.bounds.height
    else {
      if (target.bounds.isPxB) b = height - target.bounds.B
      else b = (target.bounds.B * height) / 1000
    }
  } else if (ANCHOR_BOTTOM === anchorVer) {
    if (target.bounds.isPxB) b = height - target.bounds.B
    else b = (target.bounds.B * height) / 1000

    if (-1 !== target.bounds.height) y = b - target.bounds.height
    else {
      if (target.bounds.isPxT) y = target.bounds.T
      else y = (target.bounds.T * height) / 1000
    }
  } else if ((ANCHOR_TOP | ANCHOR_BOTTOM) === anchorVer) {
    if (target.bounds.isPxT) y = target.bounds.T
    else y = (target.bounds.T * height) / 1000

    if (target.bounds.isPxB) b = height - target.bounds.B
    else b = (target.bounds.B * height) / 1000
  } else {
    y = target.bounds.T
    b = target.bounds.B
  }

  if (r < x) r = x
  if (b < y) b = y

  target.PositionInPx.L = x
  target.PositionInPx.T = y
  target.PositionInPx.R = r
  target.PositionInPx.B = b

  target.element.style.left = ((x * Factor_mm_to_pix + 0.5) >> 0) + 'px'
  target.element.style.top = ((y * Factor_mm_to_pix + 0.5) >> 0) + 'px'
  target.element.style.width = (((r - x) * Factor_mm_to_pix + 0.5) >> 0) + 'px'
  target.element.style.height = (((b - y) * Factor_mm_to_pix + 0.5) >> 0) + 'px'
}

function needHandlerRetina(htmlElement) {
  if (
    htmlElement &&
    (htmlElement.id === DOM_ID.MainContainer.Main.MainView.Slide ||
      htmlElement.id === DOM_ID.MainContainer.Main.MainView.AnimationLayer ||
      htmlElement.id === DOM_ID.MainContainer.Main.MainView.EditLayer ||
      htmlElement.id === DOM_ID.ThumbnailContainer.Thumbnail ||
      htmlElement.id === DOM_ID.ThumbnailContainer.BackGround ||
      htmlElement.id === DOM_ID.MainContainer.NotesContainer.Notes ||
      htmlElement.id === DOM_ID.MainContainer.NotesContainer.EditLayer)
  ) {
    return true
  }
  return false
}

export type NodeElem<N> = N extends VNode<infer E> ? E : never

export function createUIControlContainer<
  Name extends string,
  Elm extends HTMLElement,
  Elms extends Elements,
  Placeholders extends string[],
>(
  name: Name,
  vnode: VNode<Elm>,
  anchor: number,
  bounds: BoundsParams
): UIControlContainer<Name, Elm, Elms, Placeholders> {
  const control = new UIControlContainer<Name, Elm, Elms, Placeholders>(
    name,
    vnode
  )
  control.bounds.setParams(...bounds)
  control.anchor = anchor
  return control
}

export function createUIControl<
  Name extends string,
  Elm extends HTMLElement,
  Elms extends Elements,
>(
  name: Name,
  vnode: VNode<Elm>,
  anchor: number,
  bounds: BoundsParams
): UIControl<Name, Elm, Elms> {
  const control = new UIControl<Name, Elm, Elms>(name, vnode)
  control.bounds.setParams(...bounds)
  control.anchor = anchor
  return control
}

// eslint-disable-next-line @typescript-eslint/naming-convention
function CC<
  Elms extends Record<string, HTMLElement>,
  Placeholders extends string[],
  Settings extends { name: string; node: VNode<HTMLElement> },
>(
  settings: Settings
): (
  anchor: number,
  bounds: BoundsParams
) => UIControlContainer<
  Settings['name'],
  NodeElem<Settings['node']>,
  Elms,
  Placeholders
> {
  return (anchor: number, bounds: BoundsParams) =>
    createUIControlContainer<
      Settings['name'],
      NodeElem<Settings['node']>,
      Elms,
      Placeholders
    >(
      settings.name,
      settings.node as VNode<NodeElem<Settings['node']>>,
      anchor,
      bounds
    )
}

// eslint-disable-next-line @typescript-eslint/naming-convention
function C<
  Elms extends Record<string, HTMLElement>,
  Settings extends { name: string; node: VNode<HTMLElement> },
>(
  settings: Settings
): (
  anchor: number,
  bounds: BoundsParams
) => UIControl<Settings['name'], NodeElem<Settings['node']>, Elms> {
  return (anchor: number, bounds: BoundsParams) =>
    createUIControl<Settings['name'], NodeElem<Settings['node']>, Elms>(
      settings.name,
      settings.node as VNode<NodeElem<Settings['node']>>,
      anchor,
      bounds
    )
}
// eslint-disable-next-line @typescript-eslint/naming-convention
export const ControlCreaterMakers = { C, CC } as const
