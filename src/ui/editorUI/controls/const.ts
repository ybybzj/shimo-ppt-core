import { EditorSkin } from '../../../core/common/const/drawing'

export const ANCHOR_LEFT = 1 // 0001
export const ANCHOR_TOP = 2 // 0010
export const ANCHOR_RIGHT = 4 // 0100
export const ANCHOR_BOTTOM = 8 // 1000

export const ContainerBorderWidth = 1
export const SplitterBoxWidth = 18 // px
export const SplitterBoxHeight = 48 // px

export const DOM_ID = {
  ThumbnailContainer: {
    Id: `sm-qvpnk`,
    Splitter: `sm-sdvef`,
    BackGround: `sm-mgxsh`,
    Thumbnail: `sm-ngvzn`,
    Scroll: {
      Id: `sm-bhwle`,
      Container: {
        Id: `sm-cvvnx`,
      },
    },
  },
  MainContainer: {
    Id: `sm-qjwcc`,
    Main: {
      Id: `sm-hjrda`,
      MainView: {
        Id: `sm-udxkt`,
        Slide: `sm-lzrcc`,
        EditLayer: `sm-mjonn`,
        AnimationLayer: 'sm-owpbz',
      },
      RightContainer: {
        Id: `sm-eihrb`,
        VScrollContainer: {
          Id: `sm-zxxln`,
          Scroll: `sm-gyczt`,
        },
      },
      HScrollPanel: {
        Id: `sm-rhtxw`,
        HScrollContainer: {
          Id: `sm-ypflf`,
          Scroll: `sm-aaddb`,
        },
      },
    },
    NotesContainer: {
      Id: `sm-nqqol`,
      Notes: `sm-dhtdo`,
      EditLayer: `sm-awoou`,
      ScrollContainer: {
        Id: `sm-pqsqz`,
        Scroll: `sm-sarwk`,
      },
    },
  },
  Tip: `sm-rpkiz`,
  Splitter: `sm-lhoqy`,
  SplitterBox: `sm-lwbzu`,
  TextInputWrap: {
    Id: `sm-flqzm`,
    InputContainer: {
      Id: `sm-obfvn`,
      InputArea: `sm-ymskb`, // TextArea
    },
  },
} as const

export const DOM_STYLE = {
  ThumbnailContainer: {
    Style: { ['background-color']: EditorSkin.Background_Thumbnails },
    Splitter: {
      ['pointer-events']: 'none',
      ['background-color']: EditorSkin.Background_Thumbnails,
    },
    BackGround: {
      ['touch-action']: 'none',
      ['user-select']: 'none',
      ['-webkit-user-select']: 'none',
      ['z-index']: '1',
    },
    Thumbnail: {
      ['touch-action']: 'none',
      ['user-select']: 'none',
      ['-webkit-user-select']: 'none',
      ['z-index']: '2',
    },
    Scroll: {
      Style: {
        ['left']: '0',
        ['top']: '0',
        ['width']: '1px',
        ['overflow']: 'hidden',
        ['position']: 'absolute',
      },
      Container: {
        Style: {
          ['left']: '0',
          ['top']: '0',
          ['width']: '1px',
          ['height']: '6000px',
        },
      },
    },
  },
  MainContainer: {
    Style: {
      ['touch-action']: 'none',
      ['user-select']: 'none',
      ['-webkit-user-select']: 'none',
      ['overflow']: 'hidden',
    },
    Main: {
      Style: {
        ['z-index']: '5',
        ['touch-action']: 'none',
        ['user-select']: 'none',
        ['-webkit-user-select']: 'none',
        ['background-color']: EditorSkin.Background_Light,
        ['overflow']: 'hidden',
      },
      MainView: {
        Style: { ['overflow']: 'hidden' },
        Slide: {
          ['touch-action']: 'none',
          ['user-select']: 'none',
          ['-webkit-user-select']: 'none',
          ['background-color']: EditorSkin.Background_Light,
          ['z-index']: '6',
        },
        AnimationLayer: {
          ['visibility']: 'hidden',
          ['touch-action']: 'none',
          ['user-select']: 'none',
          ['-webkit-user-select']: 'none',
          ['z-index']: '6',
        },
        EditLayer: {
          ['touch-action']: 'none',
          ['user-select']: 'none',
          ['-webkit-user-select']: 'none',
          ['z-index']: '7',
        },
        Cursor: {
          ['touch-action']: 'none',
          ['user-select']: 'none',
          ['-webkit-user-select']: 'none',
          ['width']: '1px',
          ['height']: '1px',
          ['display']: 'none',
          ['left']: '0px',
          ['top']: '0px',
          ['z-index']: '9',
          ['will-change']: 'transform',
          ['transform']: 'translateZ(0)',
          ['transform-origin']: 'top left',
        },
      },
      RightContainer: {
        Style: {
          ['margin-right']: '1px',
          ['background-color']: EditorSkin.Background_Light,
          ['z-index']: '0',
        },
        VScrollContainer: {
          Style: {
            ['left']: '0',
            ['top']: '0',
            ['width']: '14px',
            ['overflow']: 'hidden',
            ['position']: 'absolute',
          },
          Scroll: {
            ['left']: '0',
            ['top']: '0',
            ['width']: '1px',
            ['height']: '6000px',
          },
        },
      },
    },
    NotesContainer: {
      Style: {
        ['touch-action']: 'none',
        ['user-select']: 'none',
        ['-webkit-user-select']: 'none',
        ['overflow']: 'hidden',
        ['background-color']: '#ffffff',
      },
      Notes: {
        ['touch-action']: 'none',
        ['user-select']: 'none',
        ['-webkit-user-select']: 'none',
        ['background-color']: '#ffffff',
        ['z-index']: '6',
      },
      EditLayer: {
        ['touch-action']: 'none',
        ['user-select']: 'none',
        ['-webkit-user-select']: 'none',
        ['z-index']: '7',
      },
      ScrollContainer: {
        Style: {
          ['left']: '0',
          ['top']: '0',
          ['width']: '16px',
          ['overflow']: 'hidden',
          ['position']: 'absolute',
        },
        Scroll: {
          ['left']: '0',
          ['top']: '0',
          ['width']: '1px',
          ['height']: '1px',
        },
      },
    },
  },
  Tip: {
    ['visibility']: 'hidden',
    ['color']: '#fff',
    ['fontSize']: '12px',
    ['border']:
      '0 3px 5px rgb(0 0 0 / 10%), 0 6px 10px rgb(0 0 0 / 6%), 0 1px 16px rgb(0 0 0 / 6%',
    ['borderRadius']: '2px',
    ['padding']: '8px 10px',
    ['background']: 'rgba(65,70,75,.9)',
    ['position']: 'absolute',
    ['maxWidth']: '330px',
    ['z-index']: '1111',
    ['pointerEvents']: 'none',
  },
  Splitter: {
    ['position']: 'absolute',
    ['background-color']: 'rgb(151, 151, 151)',
    ['overflow']: 'hidden',
    ['z-index']: '1000',
  },
  SplitterBox: {
    ['display']: 'none',
    ['width']: `${SplitterBoxWidth}px`,
    ['max-width']: `${SplitterBoxWidth}px`,
    ['height']: `${SplitterBoxHeight}px`,
    ['max-height']: `${SplitterBoxHeight}px`,
    ['position']: 'absolute',
    ['background-color']: EditorSkin.Background_Thumbnails,
    ['backgroundImage']:
      'url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAAYAAAAICAYAAADaxo44AAAACXBIWXMAAAsTAAALEwEAmpwYAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAABbSURBVHgBbY7BDYBACATvTCgFvtqCnVwHlqSdWINfXpTgnwdyiUcicT8bhmx2S0lCxNZ9SnB32z6PFy6quva7ZigidySqK3cFIKLDzOaRig5mbh68AOAsfxpzH2kKHypHs/lXAAAAAElFTkSuQmCC)',
    ['background-repeat']: 'no-repeat',
    ['background-position']: 'center',
    ['border-radius']: '4px',
    ['border']: '1px solid rgba(0, 0, 0, 0.12)',
    ['box-shadow']: '0px 4px 20px rgba(0, 0, 0, 0.04)',
    ['cursor']: 'pointer',
    ['z-index']: '7', // 与 editLayer 一致
  },
  TextInputWrap: {
    Style: {},
    InputContainer: {
      Style: {
        ['background']: 'transparent',
        ['border']: 'none',
        ['z-index']: '10',
        ['width']: '0px',
        ['height']: '0px',
        ['overflow']: 'hidden',
        ['box-sizing']: 'content-box',
        ['will-change']: 'left, top',
        ['transform']: 'translateZ(0)',
      },
      InputArea: {
        ['border']: '1px solid green',
      }, // TextArea
    },
  },
} as const

export const CSS_CLASS = {
  Block: `sc-blwss`,
} as const
