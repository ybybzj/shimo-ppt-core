import { EditorUI } from '../EditorUI'
import { Dom } from '../../../common/dom/vdom'
import {
  UIControl,
  UIControlContainer,
  ControlCreaterMakers,
} from '../Controls'
import { CSS_CLASS, DOM_ID, DOM_STYLE } from './const'
import { getCtrlParamsById } from './rtl'

const { C, CC } = ControlCreaterMakers

const PanelCtrlSettings = {
  name: DOM_ID.MainContainer.Main.RightContainer.Id,
  node: Dom.div(
    `#${DOM_ID.MainContainer.Main.RightContainer.Id}.${CSS_CLASS.Block}`,
    { style: DOM_STYLE.MainContainer.Main.RightContainer.Style },
    [
      Dom.placeholder(
        DOM_ID.MainContainer.Main.RightContainer.VScrollContainer.Id
      ),
    ]
  ),
} as const

const PanelCtrl = CC<
  {
    [DOM_ID.MainContainer.Main.RightContainer.Id]: HTMLDivElement
  },
  [typeof DOM_ID.MainContainer.Main.RightContainer.VScrollContainer.Id],
  typeof PanelCtrlSettings
>(PanelCtrlSettings)

export const VScrollCtrlSettings = {
  name: DOM_ID.MainContainer.Main.RightContainer.VScrollContainer.Id,
  node: Dom.div(
    `#${DOM_ID.MainContainer.Main.RightContainer.VScrollContainer.Id}`,
    {
      style: DOM_STYLE.MainContainer.Main.RightContainer.VScrollContainer.Style,
    },
    [
      Dom.div(
        `#${DOM_ID.MainContainer.Main.RightContainer.VScrollContainer.Scroll}.${CSS_CLASS.Block}`,
        {
          style:
            DOM_STYLE.MainContainer.Main.RightContainer.VScrollContainer.Scroll,
        }
      ),
    ]
  ),
} as const

export const VScrollCtrl = C<
  {
    [DOM_ID.MainContainer.Main.RightContainer.VScrollContainer
      .Id]: HTMLDivElement
    [DOM_ID.MainContainer.Main.RightContainer.VScrollContainer
      .Scroll]: HTMLDivElement
  },
  typeof VScrollCtrlSettings
>(VScrollCtrlSettings)

export type RightPanelContainerCtrl = UIControlContainer<
  typeof DOM_ID.MainContainer.Main.RightContainer.Id,
  HTMLDivElement,
  { [DOM_ID.MainContainer.Main.RightContainer.Id]: HTMLDivElement },
  [typeof DOM_ID.MainContainer.Main.RightContainer.VScrollContainer.Id]
>

export type RightPanelVScrollCtrl = UIControl<
  typeof DOM_ID.MainContainer.Main.RightContainer.VScrollContainer.Id,
  HTMLDivElement,
  {
    [DOM_ID.MainContainer.Main.RightContainer.VScrollContainer
      .Id]: HTMLDivElement
    [DOM_ID.MainContainer.Main.RightContainer.VScrollContainer
      .Scroll]: HTMLDivElement
  }
>

/** 依赖 ScrollComponent, 随 mainContainer 初始化 */
export function RightPanel(editorUI: EditorUI): RightPanelContainerCtrl {
  const panel = PanelCtrl(
    ...getCtrlParamsById(PanelCtrlSettings.name, editorUI)!
  )

  const vscroll = editorUI.scrolls.vScrollControl
  panel.add(vscroll)

  editorUI.panelRight = panel
  return panel
}
