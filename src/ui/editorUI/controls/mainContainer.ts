import { EditorUI } from '../EditorUI'
import { Dom } from '../../../common/dom/vdom'
import { EditorSkin } from '../../../core/common/const/drawing'
import { ControlCreaterMakers } from '../Controls'
import { NotesComponent } from '../components/notes'
import { EditorView } from './EditorView'
import { RightPanel } from './RightPanel'
import { CSS_CLASS, DOM_ID, DOM_STYLE, ContainerBorderWidth } from './const'
import { getCtrlParamsById } from './rtl'

const { CC } = ControlCreaterMakers

const MainContainerCtrlSettings = {
  name: DOM_ID.MainContainer.Id,
  node: Dom.div(
    `#${DOM_ID.MainContainer.Id}.${CSS_CLASS.Block}`,
    {
      style: DOM_STYLE.MainContainer.Style,
      attrs: { ['UNSELECTABLE']: 'on' },
    },
    [
      Dom.placeholder(DOM_ID.MainContainer.Main.Id),
      Dom.placeholder(DOM_ID.MainContainer.NotesContainer.Id),
    ]
  ),
} as const

/** @param bounds [l,t,r,b,isAbsL,isAbsT,isAbsR,isAbsB,absW,absh */
const MainContainerCtrl = CC<
  { [DOM_ID.MainContainer.Id]: HTMLDivElement },
  [
    typeof DOM_ID.MainContainer.Main.Id,
    typeof DOM_ID.MainContainer.NotesContainer.Id,
  ],
  typeof MainContainerCtrlSettings
>(MainContainerCtrlSettings)

export type MainContainerCtrl_t = ReturnType<typeof MainContainerCtrl>

const SlideMainContainerCtrlSetting = {
  name: DOM_ID.MainContainer.Main.Id,
  node: Dom.div(
    `#${DOM_ID.MainContainer.Main.Id}.${CSS_CLASS.Block}`,
    {
      style: DOM_STYLE.MainContainer.Main.Style,
    },
    [
      Dom.placeholder(DOM_ID.MainContainer.Main.MainView.Id),
      Dom.placeholder(DOM_ID.MainContainer.Main.RightContainer.Id),
      Dom.placeholder(DOM_ID.MainContainer.Main.HScrollPanel.Id),
    ]
  ),
} as const

const SlideMainContainerCtrl = CC<
  {
    [DOM_ID.MainContainer.Main.Id]: HTMLDivElement
  },
  [
    typeof DOM_ID.MainContainer.Main.MainView.Id,
    typeof DOM_ID.MainContainer.Main.RightContainer.Id,
    typeof DOM_ID.MainContainer.Main.HScrollPanel.Id,
  ],
  typeof SlideMainContainerCtrlSetting
>(SlideMainContainerCtrlSetting)

export type SlideMainContainerCtrl_t = ReturnType<typeof SlideMainContainerCtrl>

export function MainContainer(editorUI: EditorUI): MainContainerCtrl_t {
  const isRTL = editorUI.thumbnailsManager.isRTL
  editorUI.mainContainer = MainContainerCtrl(
    ...getCtrlParamsById(MainContainerCtrlSettings.name, editorUI)!
  )
  // Todo, isRTL 依赖集中管理
  editorUI.mainContainer.element.style[
    `border-${isRTL ? 'right' : 'left'}`
  ] = `${ContainerBorderWidth}px solid ${EditorSkin.Color_Splitter}`

  editorUI.mainContent = SlideMainContainerCtrl(
    ...getCtrlParamsById(SlideMainContainerCtrlSetting.name, editorUI)!
  )

  const rightPanel = RightPanel(editorUI)
  editorUI.mainContent.add(rightPanel)

  // initComponents 中已初始化
  const hScrollControl = editorUI.scrolls.hScrollControl
  editorUI.mainContent.add(hScrollControl)

  const mainView = EditorView(editorUI)
  editorUI.mainContent.add(mainView)

  editorUI.notesView = new NotesComponent(editorUI)
  editorUI.mainContainer.add(editorUI.mainContent)
  editorUI.mainContainer.add(editorUI.notesView.control)
  return editorUI.mainContainer
}
