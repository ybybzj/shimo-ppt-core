/* eslint-disable no-redeclare */
import { EditorUI } from '../EditorUI'
import { Dom } from '../../../common/dom/vdom'
import { ControlCreaterMakers } from '../Controls'
import { CSS_CLASS, DOM_ID, DOM_STYLE } from './const'
import { getCtrlParamsById } from './rtl'

const { C, CC } = ControlCreaterMakers

const MainViewCtrlSettings = {
  name: DOM_ID.MainContainer.Main.MainView.Id,
  node: Dom.div(
    `#${DOM_ID.MainContainer.Main.MainView.Id}.${CSS_CLASS.Block}`,
    { style: DOM_STYLE.MainContainer.Main.MainView.Style },
    [
      Dom.placeholder(DOM_ID.MainContainer.Main.MainView.Slide),
      Dom.placeholder(DOM_ID.MainContainer.Main.MainView.EditLayer),
      Dom.placeholder(DOM_ID.MainContainer.Main.MainView.AnimationLayer),
    ]
  ),
} as const

const MainViewCtrl = CC<
  {
    [DOM_ID.MainContainer.Main.MainView.Id]: HTMLDivElement
  },
  [
    typeof DOM_ID.MainContainer.Main.MainView.Slide,
    typeof DOM_ID.MainContainer.Main.MainView.EditLayer,
    typeof DOM_ID.MainContainer.Main.MainView.AnimationLayer
  ],
  typeof MainViewCtrlSettings
>(MainViewCtrlSettings)

export type MainViewCtrl_t = ReturnType<typeof MainViewCtrl>

const SlideMainCtrlSettings = {
  name: DOM_ID.MainContainer.Main.MainView.Slide,
  node: Dom.canvas(
    `#${DOM_ID.MainContainer.Main.MainView.Slide}.${CSS_CLASS.Block}`,
    { style: DOM_STYLE.MainContainer.Main.MainView.Slide }
  ),
} as const

const SlideMainCtrl = C<
  { [DOM_ID.MainContainer.Main.MainView.Slide]: HTMLCanvasElement },
  typeof SlideMainCtrlSettings
>(SlideMainCtrlSettings)

export type SlideMainCtrl_t = ReturnType<typeof SlideMainCtrl>

const SlideEditLayerCtrlSettings = {
  name: DOM_ID.MainContainer.Main.MainView.EditLayer,
  node: Dom.canvas(
    `#${DOM_ID.MainContainer.Main.MainView.EditLayer}.${CSS_CLASS.Block}`,
    { style: DOM_STYLE.MainContainer.Main.MainView.EditLayer }
  ),
} as const

const SlideEditLayerCtrl = C<
  { [DOM_ID.MainContainer.Main.MainView.EditLayer]: HTMLCanvasElement },
  typeof SlideEditLayerCtrlSettings
>(SlideEditLayerCtrlSettings)

export type SlideEditLayerCtrl_t = ReturnType<typeof SlideEditLayerCtrl>

const SlideAnimationLayerCtrlSettings = {
  name: DOM_ID.MainContainer.Main.MainView.AnimationLayer,
  node: Dom.canvas(
    `#${DOM_ID.MainContainer.Main.MainView.AnimationLayer}.${CSS_CLASS.Block}`,
    { style: DOM_STYLE.MainContainer.Main.MainView.AnimationLayer }
  ),
} as const

const SlideAnimationLayerCtrl = C<
  { [DOM_ID.MainContainer.Main.MainView.AnimationLayer]: HTMLCanvasElement },
  typeof SlideAnimationLayerCtrlSettings
>(SlideAnimationLayerCtrlSettings)

export type SlideAnimationLayerCtrl_t = ReturnType<
  typeof SlideAnimationLayerCtrl
>

export function EditorView(editorUI: EditorUI): MainViewCtrl_t {
  editorUI.mainView = MainViewCtrl(
    ...getCtrlParamsById(MainViewCtrlSettings.name, editorUI)!
  )
  editorUI.editorView = SlideMainCtrl(
    ...getCtrlParamsById(SlideMainCtrlSettings.name, editorUI)!
  )
  editorUI.editLayer = SlideEditLayerCtrl(
    ...getCtrlParamsById(SlideEditLayerCtrlSettings.name, editorUI)!
  )
  editorUI.animationLayer = SlideAnimationLayerCtrl(
    ...getCtrlParamsById(SlideEditLayerCtrlSettings.name, editorUI)!
  )

  editorUI.mainView.add(editorUI.editorView)
  editorUI.mainView.add(editorUI.animationLayer)
  editorUI.mainView.add(editorUI.editLayer)

  return editorUI.mainView
}
