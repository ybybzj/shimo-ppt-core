import { Factor_pix_to_mm } from '../../../core/common/const/unit'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { SCROLLBAR_WIDTH_MM } from '../const'
import { BoundsParams } from '../Controls'
import { EditorUI } from '../EditorUI'
import { splitterStates } from '../splitterStates'
import {
  DOM_ID,
  ContainerBorderWidth,
  ANCHOR_BOTTOM,
  ANCHOR_LEFT,
  ANCHOR_RIGHT,
  ANCHOR_TOP,
} from './const'

type PosParamsMaker = (isRTL?: boolean) => [number, BoundsParams]

/** 尽量将各组件依赖的本地 RTL 开关集中到此处管理 */
export function getCtrlParamsById(
  id: string,
  editorUI: EditorUI
): [number, BoundsParams] | undefined {
  let isRTL = EditorSettings.isRTL
  switch (id) {
    case DOM_ID.ThumbnailContainer.Id: {
      if (editorUI.thumbnailsManager) {
        isRTL = editorUI.thumbnailsManager.isRTL
      }
      return thumbnailContainerParams(isRTL)
    }
    case DOM_ID.ThumbnailContainer.BackGround: {
      if (editorUI.thumbnailsManager) {
        isRTL = editorUI.thumbnailsManager.isRTL
      }
      return thumbnailBackgroundParams(isRTL)
    }

    case DOM_ID.ThumbnailContainer.Scroll.Id: {
      if (editorUI.thumbnailsManager) {
        isRTL = editorUI.thumbnailsManager.isRTL
      }
      return thumbnailScrollerParams(isRTL)
    }

    case DOM_ID.ThumbnailContainer.Thumbnail: {
      if (editorUI.thumbnailsManager) {
        isRTL = editorUI.thumbnailsManager.isRTL
      }
      return thumbnailParams(isRTL)
    }

    case DOM_ID.MainContainer.Id: {
      return mainContainerParams(isRTL)
    }
    case DOM_ID.MainContainer.Main.Id: {
      return slideMainContainerParams(isRTL)
    }
    case DOM_ID.MainContainer.Main.MainView.Id: {
      return slideMainViewParams(isRTL)
    }
    case DOM_ID.MainContainer.Main.MainView.Slide:
    case DOM_ID.MainContainer.Main.MainView.AnimationLayer: {
      return slideMainCtrlParams(isRTL)
    }
    case DOM_ID.MainContainer.Main.MainView.EditLayer: {
      return slideEditLayerCtrlParams(isRTL)
    }
    case DOM_ID.MainContainer.Main.RightContainer.Id: {
      return rightPanelContainerParams(isRTL)
    }
    case DOM_ID.MainContainer.Main.RightContainer.VScrollContainer.Id: {
      return rightPanelVScrollContainerParams(isRTL)
    }
    case DOM_ID.MainContainer.Main.HScrollPanel.Id: {
      return hScrollPanelContainerParams(isRTL)
    }
    case DOM_ID.MainContainer.NotesContainer.Id: {
      return notesContainerParams(isRTL)
    }
    case DOM_ID.MainContainer.NotesContainer.Notes: {
      return notesCtrlParams(isRTL)
    }
    case DOM_ID.MainContainer.NotesContainer.EditLayer: {
      return notesEditLayerCtrlParams(isRTL)
    }
    case DOM_ID.MainContainer.NotesContainer.ScrollContainer.Id: {
      return notesScrollerContainerCtrlParams(isRTL)
    }
  }
}

export const thumbnailContainerParams: PosParamsMaker = (isRTL = false) => {
  const horizentalAnchor = isRTL ? ANCHOR_RIGHT : ANCHOR_LEFT
  const anchor = horizentalAnchor | ANCHOR_TOP | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    0,
    0,
    isRTL ? 0 : splitterStates.thumbnailSplitterPos,
    1000,
    isRTL ? false : true,
    false,
    isRTL ? true : false,
    false,
    splitterStates.thumbnailSplitterPos,
    -1,
  ]
  return [anchor, boundsParams]
}

export const thumbnailBackgroundParams: PosParamsMaker = (isRTL = false) => {
  const horizentalAnchor = isRTL ? ANCHOR_LEFT : ANCHOR_RIGHT
  const anchor = horizentalAnchor | ANCHOR_TOP | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    isRTL ? SCROLLBAR_WIDTH_MM : 0,
    0,
    isRTL ? 0 : SCROLLBAR_WIDTH_MM,
    1000,
    true,
    false,
    true,
    false,
    -1,
    -1,
  ]
  return [anchor, boundsParams]
}

export const thumbnailScrollerParams: PosParamsMaker = (isRTL = false) => {
  const horizentalAnchor = isRTL ? ANCHOR_LEFT : ANCHOR_RIGHT
  const anchor = horizentalAnchor | ANCHOR_TOP | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    0,
    0,
    1000,
    1000,
    false,
    false,
    false,
    false,
    SCROLLBAR_WIDTH_MM,
    -1,
  ]
  return [anchor, boundsParams]
}

export const thumbnailParams: PosParamsMaker = (isRTL = false) => {
  const horizentalAnchor = isRTL ? ANCHOR_LEFT : ANCHOR_RIGHT
  const anchor = horizentalAnchor | ANCHOR_TOP | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    isRTL ? SCROLLBAR_WIDTH_MM : 0,
    0,
    isRTL ? 0 : SCROLLBAR_WIDTH_MM,
    1000,
    true,
    false,
    true,
    false,
    -1,
    -1,
  ]
  return [anchor, boundsParams]
}

export const mainContainerParams: PosParamsMaker = (isRTL = false) => {
  const horizentalAnchor = isRTL ? ANCHOR_RIGHT : ANCHOR_LEFT
  const anchor = horizentalAnchor | ANCHOR_TOP
  const boundsParams: BoundsParams = [
    isRTL ? 0 : splitterStates.thumbnailSplitterPos,
    0,
    isRTL ? splitterStates.thumbnailSplitterPos : 0,
    1000,
    true,
    false,
    true,
    false,
    -1,
    -1,
  ]
  return [anchor, boundsParams]
}

export const slideMainContainerParams: PosParamsMaker = (_isRTL = false) => {
  const anchor = ANCHOR_LEFT | ANCHOR_TOP | ANCHOR_RIGHT | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    0,
    0,
    ContainerBorderWidth * Factor_pix_to_mm,
    splitterStates.notesSplitterPos,
    true,
    false,
    true,
    true,
    -1,
    -1,
  ]
  return [anchor, boundsParams]
}

export const slideLeftContainerParams: PosParamsMaker = (isRTL = false) => {
  const horizentalAnchor = isRTL ? ANCHOR_RIGHT : ANCHOR_LEFT
  const anchor = horizentalAnchor | ANCHOR_TOP | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    0,
    0,
    1000,
    1000,
    false,
    false,
    false,
    false,
    5,
    -1,
  ]
  return [anchor, boundsParams]
}

export const slideMainViewParams: PosParamsMaker = (isRTL = false) => {
  const scrollWidth = EditorSettings.isMobile ? 0 : SCROLLBAR_WIDTH_MM
  const horizentalAnchor = isRTL ? ANCHOR_RIGHT : ANCHOR_LEFT | ANCHOR_RIGHT
  const anchor = horizentalAnchor | ANCHOR_TOP | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    isRTL ? scrollWidth : 0,
    0,
    isRTL ? 0 : scrollWidth,
    scrollWidth,
    true,
    true,
    true,
    true,
    -1,
    -1,
  ]
  return [anchor, boundsParams]
}

export const slideMainCtrlParams: PosParamsMaker = (_isRTL = false) => {
  const anchor = ANCHOR_LEFT | ANCHOR_TOP | ANCHOR_RIGHT | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    0,
    0,
    1000,
    1000,
    false,
    false,
    false,
    false,
    -1,
    -1,
  ]
  return [anchor, boundsParams]
}

export const slideEditLayerCtrlParams: PosParamsMaker = (_isRTL = false) => {
  const anchor = ANCHOR_LEFT | ANCHOR_TOP | ANCHOR_RIGHT | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    0,
    0,
    1000,
    1000,
    false,
    false,
    false,
    false,
    -1,
    -1,
  ]
  return [anchor, boundsParams]
}

export const rightPanelContainerParams: PosParamsMaker = (isRTL = false) => {
  const horizentalAnchor = isRTL ? ANCHOR_LEFT : ANCHOR_RIGHT
  const anchor = horizentalAnchor | ANCHOR_TOP | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    0,
    0,
    1000,
    SCROLLBAR_WIDTH_MM,
    false,
    false,
    false,
    true,
    SCROLLBAR_WIDTH_MM,
    -1,
  ]
  return [anchor, boundsParams]
}

export const rightPanelVScrollContainerParams: PosParamsMaker = (
  isRTL = false
) => {
  const horizentalAnchor = isRTL ? ANCHOR_LEFT : ANCHOR_LEFT | ANCHOR_RIGHT
  const anchor = horizentalAnchor | ANCHOR_TOP | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    0,
    0,
    1000,
    2 * SCROLLBAR_WIDTH_MM,
    isRTL ? true : false,
    true,
    false,
    true,
    -1,
    -1,
  ]
  return [anchor, boundsParams]
}

export const hScrollPanelContainerParams: PosParamsMaker = (isRTL = false) => {
  const anchor = ANCHOR_LEFT | ANCHOR_RIGHT | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    isRTL ? SCROLLBAR_WIDTH_MM : 0,
    0,
    isRTL ? 0 : SCROLLBAR_WIDTH_MM,
    1000,
    true,
    false,
    true,
    false,
    -1,
    SCROLLBAR_WIDTH_MM,
  ]
  return [anchor, boundsParams]
}

export const notesContainerParams: PosParamsMaker = (_isRTL = false) => {
  const anchor = ANCHOR_LEFT | ANCHOR_RIGHT | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    0,
    0,
    ContainerBorderWidth * Factor_pix_to_mm,
    1000,
    true,
    true,
    true,
    false,
    -1,
    splitterStates.notesSplitterPos,
  ]
  return [anchor, boundsParams]
}

export const notesCtrlParams: PosParamsMaker = (isRTL = false) => {
  const horizentalAnchor = isRTL ? ANCHOR_RIGHT : ANCHOR_LEFT | ANCHOR_RIGHT
  const anchor = horizentalAnchor | ANCHOR_TOP | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    isRTL ? SCROLLBAR_WIDTH_MM : 0,
    0,
    isRTL ? 0 : SCROLLBAR_WIDTH_MM,
    1000,
    true,
    false,
    true,
    false,
    -1,
    -1,
  ]
  return [anchor, boundsParams]
}

export const notesEditLayerCtrlParams: PosParamsMaker = (isRTL = false) => {
  const horizentalAnchor = isRTL ? ANCHOR_RIGHT : ANCHOR_LEFT | ANCHOR_RIGHT
  const anchor = horizentalAnchor | ANCHOR_TOP | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    isRTL ? SCROLLBAR_WIDTH_MM : 0,
    0,
    isRTL ? 0 : SCROLLBAR_WIDTH_MM,
    1000,
    true,
    false,
    true,
    false,
    -1,
    -1,
  ]
  return [anchor, boundsParams]
}

export const notesScrollerContainerCtrlParams: PosParamsMaker = (
  isRTL = false
) => {
  const horizentalAnchor = isRTL ? ANCHOR_LEFT : ANCHOR_RIGHT
  const anchor = horizentalAnchor | ANCHOR_TOP | ANCHOR_BOTTOM
  const boundsParams: BoundsParams = [
    0,
    0,
    1000,
    1000,
    false,
    false,
    false,
    false,
    SCROLLBAR_WIDTH_MM,
    -1,
  ]
  return [anchor, boundsParams]
}
