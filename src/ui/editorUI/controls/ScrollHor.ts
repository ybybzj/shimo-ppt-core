/* eslint-disable no-redeclare */
import { Dom } from '../../../common/dom/vdom'
import { ControlCreaterMakers } from '../Controls'
import { CSS_CLASS, DOM_ID } from './const'

const { C } = ControlCreaterMakers

export const ScrollHorCtrlSettings = {
  name: DOM_ID.MainContainer.Main.HScrollPanel.Id,
  node: Dom.div(
    `#${DOM_ID.MainContainer.Main.HScrollPanel.Id}.${CSS_CLASS.Block}`,
    {
      style: {
        ['margin-bottom']: '1px',
        ['background-color']: '#f1f1f1',
      },
    },
    [
      Dom.div(
        `#${DOM_ID.MainContainer.Main.HScrollPanel.HScrollContainer.Id}`,
        {
          style: {
            ['left']: '0',
            ['top']: '0',
            ['height']: '10px',
            ['overflow']: 'hidden',
            ['position']: 'absolute',
            ['width']: '100%',
          },
        },
        [
          Dom.div(
            `#${DOM_ID.MainContainer.Main.HScrollPanel.HScrollContainer.Scroll}.${CSS_CLASS.Block}`,
            {
              style: {
                ['left']: '0',
                ['top']: '0',
                ['width']: '6000px',
                ['height']: '1px',
              },
            }
          ),
        ]
      ),
    ]
  ),
} as const

/** Todo: move to scrollComponent */
export const ScrollHorCtrl = C<
  {
    [DOM_ID.MainContainer.Main.HScrollPanel.Id]: HTMLDivElement
    [DOM_ID.MainContainer.Main.HScrollPanel.HScrollContainer.Id]: HTMLDivElement
    [DOM_ID.MainContainer.Main.HScrollPanel.HScrollContainer
      .Scroll]: HTMLDivElement
  },
  typeof ScrollHorCtrlSettings
>(ScrollHorCtrlSettings)

export type ScrollHorCtrl = ReturnType<typeof ScrollHorCtrl>
