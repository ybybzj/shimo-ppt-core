import { BrowserInfo } from '../../common/browserInfo'
import {
  listenPointerEvents,
  PointerEventArg,
  PointerEventType,
} from '../../common/dom/events'
import { DomWheelEvt } from '../../common/dom/wheelEvent'
import { EditorSkin } from '../../core/common/const/drawing'
import { hookManager, Hooks } from '../../core/hooks'

export class ScrollBarSettings {
  screenWidth = -1
  screenHeight = -1
  extraHeight = 0
  contentHeight!: number
  contentWidth!: number
  minHeight = 34
  maxHeight = 9999
  minWidth = 34
  maxWidth = 9999
  initDelay = 300
  afterDelay = 70
  parentPercent: number
  color: string
  bgColor: string
  vscrollDistPerTime = 10
  hscrollDistPerTime = 10
  radius = 4
  lockShow = false

  constructor() {
    this.parentPercent = 1 / 8
    this.color = 'rgba(65, 70, 75, 0.2)'
    this.bgColor = EditorSkin.Background_Scroll
  }
}

export class ScrollBar {
  settings: ScrollBarSettings
  mouseDown = false
  mouseDownOnScroller = false
  moveble = false
  lock = false
  scrollTimer: any
  mouseStartPos: { x: number; y: number }
  mouseEndPos: { x: number; y: number }
  minYForDrag = 0
  maxYForDrag = 0
  curYOfVScroll = 0
  curXOfHScroll = 0
  vHeight = 0
  hWidth = 0
  heightOfPane = 0
  widthOfPane = 0
  maxScrollY = 0
  maxScrollX = 0
  scrollFactor = 0
  scroller: { x: number; y: number; h: number; w: number }
  canvas!: HTMLCanvasElement
  context!: CanvasRenderingContext2D
  eventListeners: any[] = []
  canvasWidth = 1
  canvasHeight = 1
  isVScroll!: boolean
  isHScroll!: boolean
  maxXForDrag!: number
  minXForDrag!: number
  style: any
  onMouseLock: any
  onUnLockMouse: any
  private _unlisten: undefined | (() => void)
  constructor(container: HTMLDivElement, settings: ScrollBarSettings) {
    this.settings = settings
    this.scrollTimer = null
    this.mouseStartPos = { x: 0, y: 0 }
    this.mouseEndPos = { x: 0, y: 0 }
    this.scroller = { x: 0, y: 1, h: 0, w: 0 }
    this._init(container)
  }
  _init(container) {
    if (!container) return false

    if (container.getElementsByTagName('canvas').length === 0) {
      this.canvas = container.appendChild(document.createElement('CANVAS'))
    } else {
      this.canvas = container.children[1]
    }

    this.canvas.style.width = '100%'
    this.canvas.style.height = '100%'
    this.canvas.style.position = 'absolute'
    this.canvas.style.top = '0px'
    this.canvas.style['touchAction'] = 'none'
    this.canvas.style.cursor = 'default'
    this.canvas.style.userSelect = 'none'
    if (BrowserInfo.webkit) {
      this.canvas.style.webkitUserSelect = 'none'
    }

    this.context = this.canvas.getContext('2d')!
    this.context.setTransform(
      BrowserInfo.PixelRatio,
      0,
      0,
      BrowserInfo.PixelRatio,
      0,
      0
    )

    this.setDimension(container.clientHeight, container.clientWidth)
    this.maxScrollY =
      container.firstElementChild!.clientHeight - this.settings.screenHeight > 0
        ? container.firstElementChild!.clientHeight - this.settings.screenHeight
        : 0
    this.maxScrollX =
      container.firstElementChild!.clientWidth - this.settings.screenWidth > 0
        ? container.firstElementChild!.clientWidth - this.settings.screenWidth
        : 0

    this.isVScroll =
      container.firstElementChild!.clientHeight /
        Math.max(this.canvasHeight, 1) >
      1
    this.isHScroll =
      container.firstElementChild!.clientWidth / Math.max(this.canvasWidth, 1) >
      1

    this.setScrollerByDirection()

    this.heightOfPane = this.canvasHeight
    this.widthOfPane = this.canvasWidth

    this.calculateState()
    this.initEvents()

    this.context.fillStyle = this.settings.bgColor
    this.context.fillRect(0, 0, this.canvasWidth, this.canvasHeight)

    this.draw()

    return true
  }

  handlePointerEvent<T extends PointerEventType>(et: T, e: PointerEventArg<T>) {
    switch (et) {
      case 'pointerdown': {
        this.onMouseDown(e as PointerEvent)
        break
      }
      case 'pointermove': {
        this.onMouseMove(e as PointerEvent)
        break
      }
      case 'pointerup': {
        this.onMouseUp(e as PointerEvent)
        break
      }
      case 'wheel': {
        this.onMouseWheel(e as DomWheelEvt)
        break
      }
    }
  }

  initEvents() {
    this._unlisten = listenPointerEvents(
      this.canvas,
      ['pointerdown', 'pointerup', 'pointermove', 'wheel'],
      this
    )
  }

  removeEvents() {
    if (this._unlisten) {
      this._unlisten()
    }
  }

  convertClientPosToInnerPos(evt) {
    let elem: HTMLElement = this.canvas
    let [top, left] = [0, 0]
    while (elem && elem.tagName !== 'BODY') {
      top += elem.offsetTop
      left += elem.offsetLeft
      elem = elem.offsetParent as HTMLElement
    }
    const mouseX = (evt.clientX >> 0) - left + window.pageXOffset
    const mouseY = (evt.clientY >> 0) - top + window.pageYOffset

    return { x: mouseX, y: mouseY }
  }

  calculateState(startpos?) {
    if (this.isVScroll) {
      this.vHeight = this.canvasHeight
      this.scroller.y = 1

      const percentH = (this.maxScrollY + this.heightOfPane) / this.heightOfPane
      this.scroller.h = Math.ceil((1 / percentH) * this.vHeight)

      if (this.scroller.h < this.settings.minHeight) {
        this.scroller.h = this.settings.minHeight
      } else if (this.scroller.h > this.settings.maxHeight) {
        this.scroller.h = this.settings.maxHeight
      }
      this.scrollFactor =
        this.maxScrollY / Math.max(1, this.heightOfPane - this.scroller.h)
      if (startpos) {
        this.scroller.y = startpos / this.scrollFactor
      }
      this.maxYForDrag = this.canvasHeight - this.scroller.h + 1
      this.minYForDrag = 0
    }

    if (this.isHScroll) {
      this.hWidth = this.canvasWidth
      this.scroller.x = 1

      const percentW = (this.maxScrollX + this.widthOfPane) / this.widthOfPane
      this.scroller.w = Math.ceil((1 / percentW) * this.hWidth)

      if (this.scroller.w < this.settings.minWidth) {
        this.scroller.w = this.settings.minWidth
      } else if (this.scroller.w > this.settings.maxWidth) {
        this.scroller.w = this.settings.maxWidth
      }
      this.scrollFactor =
        this.maxScrollX / Math.max(1, this.widthOfPane - this.scroller.w)
      if (typeof startpos !== 'undefined') {
        this.scroller.x = startpos / this.scrollFactor
      }
      this.maxXForDrag = this.canvasWidth - this.scroller.w
      this.minXForDrag = 0
    }
  }

  resetBySettings(settings: ScrollBarSettings, direction?: 'h' | 'v') {
    const { screenWidth, screenHeight } = settings
    if (direction === 'v') {
      let canvasHeight = settings.screenHeight
      if (null != canvasHeight && settings.extraHeight) {
        canvasHeight += settings.extraHeight
      }

      if (
        canvasHeight === this.canvasHeight &&
        null != settings.contentHeight
      ) {
        const maxScrollY =
          settings.contentHeight - screenHeight > 0
            ? settings.contentHeight - screenHeight
            : 0
        if (maxScrollY === this.maxScrollY) return
      }
    }
    if (direction === 'h') {
      if (screenWidth === this.canvasWidth && null != settings.contentWidth) {
        const maxScrollX =
          settings.contentWidth - screenWidth > 0
            ? settings.contentWidth - screenWidth
            : 0
        if (maxScrollX === this.maxScrollX) return
      }
    }

    const parentNodeWidth = getDomWidth(this.canvas.parentNode)
    const parentNodeHeight = getDomHeight(this.canvas.parentNode)
    const widthOfFirstChild = getDomWidth(
      this.canvas.parentNode?.firstElementChild
    )
    const heightOfFirstChild = getDomHeight(
      this.canvas.parentNode?.firstElementChild
    )

    this.setDimension(parentNodeHeight, parentNodeWidth)
    this.maxScrollY =
      heightOfFirstChild - screenHeight > 0
        ? heightOfFirstChild - screenHeight
        : 0
    this.maxScrollX =
      widthOfFirstChild - screenWidth > 0 ? widthOfFirstChild - screenWidth : 0

    this.isVScroll =
      heightOfFirstChild / Math.max(this.canvasHeight, 1) > 1 ||
      this.isVScroll ||
      direction === 'v'
    this.isHScroll =
      widthOfFirstChild / Math.max(this.canvasWidth, 1) > 1 ||
      this.isHScroll ||
      direction === 'h'
    this.setScrollerByDirection()

    this.heightOfPane = this.canvasHeight
    this.widthOfPane = this.canvasWidth
    this.calculateState()
    if (this.isVScroll && !this.settings.lockShow) {
      if (this.curYOfVScroll > this.maxScrollY) {
        this.curYOfVScroll = this.maxScrollY
      }

      this.scrollToY(this.curYOfVScroll)
      if (this.maxScrollY === 0) {
        this.canvas.style.display = 'none'
      } else {
        this.canvas.style.display = ''
      }
    } else if (this.isHScroll) {
      if (this.curXOfHScroll > this.maxScrollX) {
        this.curXOfHScroll = this.maxScrollX
      }

      this.scrollToX(this.curXOfHScroll)
      if (this.maxScrollX === 0 && !this.settings.lockShow) {
        this.canvas.style.display = 'none'
      } else {
        this.canvas.style.display = ''
      }
    }

    this.draw()
  }

  scrollV(evt, pos, isForce?) {
    if (!this.isVScroll) {
      return
    }

    if (this.curYOfVScroll !== pos || isForce === true) {
      this.curYOfVScroll = pos
      evt.scrollDest = evt.scrollY = this.curYOfVScroll
      evt.maxScrollY = this.maxScrollY
      this.draw()
      this.triggerEvents('onscrollV', evt)
    }
    hookManager.invoke(Hooks.EditorUI.OnUpdateScrolls)
  }

  onUpdateScrollV(that, yPos) {
    if (!this.isVScroll) return null

    const events = that.eventListeners['onupdateVScroll']
    if (events) {
      if (events.length !== 1) return null

      return events[0].handler.apply(that, [yPos])
    }
    return null
  }

  onUpdateScrollVByDeltaY(that, delta) {
    if (!this.isVScroll) return null

    const events = that.eventListeners['onupdateVScrollByDelta']
    if (events) {
      if (events.length !== 1) return null

      return events[0].handler.apply(that, [delta])
    }
    return null
  }

  scrollH(evt, pos, isForce?: boolean) {
    if (!this.isHScroll) {
      return
    }
    if (this.curXOfHScroll !== pos || isForce === true) {
      this.curXOfHScroll = pos
      evt.scrollDest = evt.scrollX = this.curXOfHScroll
      evt.maxScrollX = this.maxScrollX

      this.draw()
      this.triggerEvents('onscrollH', evt)
    }
    hookManager.invoke(Hooks.EditorUI.OnUpdateScrolls)
  }

  scrollByDeltaY(deltaY: number, isForce?) {
    if (!this.isVScroll) {
      return
    }

    const result = this.onUpdateScrollVByDeltaY(this, deltaY)
    if (result != null && result.isChange === true) deltaY = result.pos

    let destY = this.curYOfVScroll + deltaY,
      vend = false

    if (destY < 0) {
      destY = 0
    } else if (destY > this.maxScrollY) {
      vend = true
      destY = this.maxScrollY
    }

    this.scroller.y = destY / Math.max(1, this.scrollFactor)
    if (this.scroller.y < this.minYForDrag) {
      this.scroller.y = this.minYForDrag + 1
    } else if (this.scroller.y > this.maxYForDrag) {
      this.scroller.y = this.maxYForDrag
    }

    if (this.scroller.y + this.scroller.h > this.canvasHeight) {
      this.scroller.y -= Math.abs(
        this.canvasHeight - this.scroller.y - this.scroller.h
      )
    }

    this.scroller.y = Math.round(this.scroller.y)

    if (vend) {
      this.moveble = true
    }
    this.scrollV({}, destY, isForce)
    if (vend) {
      this.moveble = false
    }
  }

  scrollToY(destY) {
    if (!this.isVScroll) {
      return
    }

    this.scroller.y = destY / Math.max(1, this.scrollFactor)
    if (this.scroller.y < this.minYForDrag) {
      this.scroller.y = this.minYForDrag + 1
    } else if (this.scroller.y > this.maxYForDrag) {
      this.scroller.y = this.maxYForDrag
    }

    if (this.scroller.y + this.scroller.h > this.canvasHeight) {
      this.scroller.y -= Math.abs(
        this.canvasHeight - this.scroller.y - this.scroller.h
      )
    }

    this.scroller.y = Math.round(this.scroller.y)

    this.scrollV({}, destY)
  }

  scrollByDeltaX(deltaX: number) {
    if (!this.isHScroll) {
      return
    }
    let destX = this.curXOfHScroll + deltaX,
      hend = false

    if (destX < 0) {
      destX = 0
    } else if (destX > this.maxScrollX) {
      hend = true
      destX = this.maxScrollX
    }

    this.scroller.x = destX / Math.max(1, this.scrollFactor)
    if (this.scroller.x < this.minXForDrag) {
      this.scroller.x = this.minXForDrag + 1
    } else if (this.scroller.x > this.maxXForDrag) {
      this.scroller.x = this.maxXForDrag
    }

    if (this.scroller.x + this.scroller.w > this.canvasWidth) {
      this.scroller.x -= Math.abs(
        this.canvasWidth - this.scroller.x - this.scroller.w
      )
    }

    this.scroller.x = Math.round(this.scroller.x)

    if (hend) {
      this.moveble = true
    }
    this.scrollH({}, destX)
    if (hend) {
      this.moveble = true
    }
  }

  scrollToX(destX, isForce?: boolean) {
    if (!this.isHScroll) {
      return
    }

    this.scroller.x = destX / Math.max(1, this.scrollFactor)
    if (this.scroller.x < this.minXForDrag) {
      this.scroller.x = this.minXForDrag + 1
    } else if (this.scroller.x > this.maxXForDrag) {
      this.scroller.x = this.maxXForDrag
    }

    if (this.scroller.x + this.scroller.w > this.canvasWidth) {
      this.scroller.x -= Math.abs(
        this.canvasWidth - this.scroller.x - this.scroller.w
      )
    }

    this.scroller.x = Math.round(this.scroller.x)

    this.scrollH({}, destX, isForce)
  }

  scrollTo(destX, destY) {
    this.scrollToX(destX)
    this.scrollToY(destY)
  }

  scrollBy(deltaX, deltaY) {
    this.scrollByDeltaX(deltaX)
    this.scrollByDeltaY(deltaY)
  }

  roundRect(x, y, width, height, radius) {
    if (typeof radius === 'undefined') {
      radius = 1
    }
    this.context.beginPath()
    this.context.moveTo(x + radius, y)
    this.context.lineTo(x + width - radius, y)
    this.context.quadraticCurveTo(x + width, y, x + width, y + radius)
    this.context.lineTo(x + width, y + height - radius)
    this.context.quadraticCurveTo(
      x + width,
      y + height,
      x + width - radius,
      y + height
    )
    this.context.lineTo(x + radius, y + height)
    this.context.quadraticCurveTo(x, y + height, x, y + height - radius)
    this.context.lineTo(x, y + radius)
    this.context.quadraticCurveTo(x, y, x + radius, y)
    this.context.closePath()
  }

  draw() {
    const drawFunc = () => {
      this.context.beginPath()

      if (this.isVScroll) {
        const y = 0
        const h = this.canvasHeight - (y << 1)

        if (h > 0) {
          this.context.rect(0, y, this.canvasWidth, h)
        }
      } else if (this.isHScroll) {
        const x = 0
        const w = this.canvasWidth - (x << 1)

        if (w > 0) {
          this.context.rect(x, 0, w, this.canvasHeight)
        }
      }

      this.context.fillStyle = this.settings.bgColor

      this.context.fill()
      this.context.beginPath()

      if (this.isVScroll && this.maxScrollY !== 0) {
        let y = this.scroller.y >> 0
        if (y < 0) {
          y = 0
        }
        let b = Math.round(this.scroller.y + this.scroller.h)
        if (b > this.canvasHeight - 1) {
          b = this.canvasHeight - 1
        }

        if (b > y) {
          this.roundRect(
            this.scroller.x - 0.5,
            y + 0.5,
            this.scroller.w - 1,
            this.scroller.h - 1,
            this.settings.radius
          )
        }
      } else if (this.isHScroll && this.maxScrollX !== 0) {
        let x = this.scroller.x >> 0
        if (x < 0) {
          x = 0
        }
        let r = (this.scroller.x + this.scroller.w) >> 0
        if (r > this.canvasWidth - 2) {
          r = this.canvasWidth - 1
        }

        if (r > x) {
          this.roundRect(
            x + 0.5,
            this.scroller.y - 0.5,
            this.scroller.w - 1,
            this.scroller.h - 1,
            this.settings.radius
          )
        }
      }
    }

    drawFunc()

    this.context.fillStyle = this.settings.color
    this.context.fill()
  }

  setDimension(h, w) {
    if (w === this.canvasWidth && h === this.canvasHeight) return

    this.canvasWidth = w
    this.canvasHeight = h

    this.canvas.height = h * BrowserInfo.PixelRatio
    this.canvas.width = w * BrowserInfo.PixelRatio
    this.context.setTransform(
      BrowserInfo.PixelRatio,
      0,
      0,
      BrowserInfo.PixelRatio,
      0,
      0
    )
  }

  setScrollerByDirection() {
    if (this.isVScroll) {
      this.scroller.x = 1 //0;
      this.scroller.w = this.canvasWidth - 1
    } else if (this.isHScroll) {
      this.scroller.y = 1 //0;
      this.scroller.h = this.canvasHeight - 1
    }
  }

  isMousePosOnScroller(mousePos: { x; y }) {
    if (
      mousePos.x >= this.scroller.x &&
      mousePos.x <= this.scroller.x + this.scroller.w &&
      mousePos.y >= this.scroller.y &&
      mousePos.y <= this.scroller.y + this.scroller.h
    ) {
      return true
    }
    return false
  }

  getMaxScrollY() {
    return this.maxScrollY
  }

  getMaxScrollX() {
    return this.maxScrollX
  }

  onMouseMove(e: PointerEvent) {
    if (this.style) this.style.cursor = 'default'

    const evt = e || window.event

    if (evt.preventDefault) evt.preventDefault()
    else evt.returnValue = false

    const mousePos = this.convertClientPosToInnerPos(evt)
    this.mouseEndPos.x = mousePos.x
    this.mouseEndPos.y = mousePos.y

    if (this.mouseDown && this.mouseDownOnScroller) {
      this.moveble = true
    } else {
      this.moveble = false
    }

    if (this.isVScroll) {
      if (this.moveble && this.mouseDownOnScroller) {
        let deltaY = this.mouseEndPos.y - this.mouseStartPos.y
        if (this.mouseEndPos.y === this.mouseStartPos.y) {
          return
        } else if (this.mouseEndPos.y < 0) {
          this.mouseEndPos.y = 0
          deltaY = 0
          this.scroller.y = 0
        } else if (this.mouseEndPos.y > this.canvasHeight - 0) {
          this.mouseEndPos.y = this.canvasHeight
          deltaY = 0
          this.scroller.y = this.canvasHeight - this.scroller.h
        } else {
          if (
            (deltaY > 0 &&
              this.scroller.y + deltaY + this.scroller.h <=
                this.canvasHeight) ||
            (deltaY < 0 && this.scroller.y + deltaY >= 0)
          ) {
            this.scroller.y += deltaY
          }
        }

        let destY = this.scroller.y * this.scrollFactor
        const result = this.onUpdateScrollV(this, destY)
        if (result != null && result.isChange === true) {
          destY = result.pos
        }

        this.scrollV(evt, destY)
        this.moveble = false
        this.mouseStartPos.x = this.mouseEndPos.x
        this.mouseStartPos.y = this.mouseEndPos.y
      }
    } else if (this.isHScroll) {
      if (this.moveble && this.mouseDownOnScroller) {
        let deltaX = this.mouseEndPos.x - this.mouseStartPos.x
        if (this.mouseEndPos.x === this.mouseStartPos.x) return
        else if (this.mouseEndPos.x < 0) {
          this.mouseEndPos.x = 0
          deltaX = 0
          this.scroller.x = 0
        } else if (this.mouseEndPos.x > this.canvasWidth - 0) {
          this.mouseEndPos.x = this.canvasWidth
          deltaX = 0
          this.scroller.x = this.canvasWidth - this.scroller.w
        } else {
          if (
            (deltaX > 0 &&
              this.scroller.x + deltaX + this.scroller.w <= this.canvasWidth) ||
            (deltaX < 0 && this.scroller.x + deltaX >= 0)
          ) {
            this.scroller.x += deltaX
          }
        }
        const destX = this.scroller.x * this.scrollFactor

        this.scrollH(evt, destX)
        this.moveble = false

        this.mouseStartPos.x = this.mouseEndPos.x
        this.mouseStartPos.y = this.mouseEndPos.y
      }
    }
  }

  onMouseUp(e) {
    const evt = e || window.event

    this.triggerEvents('onmouseup', evt)

    if (evt.preventDefault) evt.preventDefault()
    else evt.returnValue = false

    this.scrollTimer && clearTimeout(this.scrollTimer)
    this.scrollTimer = null
    if (this.mouseDownOnScroller) {
      this.mouseDown = false
      this.mouseDownOnScroller = false
      this.draw()
    }

    if (this.onMouseLock && this.onUnLockMouse) {
      this.onUnLockMouse(evt)
    }
  }

  onMouseDown(e: PointerEvent) {
    const evt = e || window.event

    const mousePos = this.convertClientPosToInnerPos(evt)
    this.mouseDown = true

    if (this.isMousePosOnScroller(mousePos)) {
      this.mouseDownOnScroller = true

      if (this.onMouseLock) {
        this.onMouseLock(evt)
      }
      this.mouseStartPos.x = mousePos.x
      this.mouseStartPos.y = mousePos.y
      this.draw()
    } else {
      if (this.isVScroll) {
        let isFirst = true

        const direction = mousePos.y - this.scroller.y - this.scroller.h / 2
        const step = this.heightOfPane * this.settings.parentPercent
        const doScroll = () => {
          this.lock = true
          if (direction > 0) {
            if (this.scroller.y + this.scroller.h / 2 + step < mousePos.y) {
              this.scrollByDeltaY(step * this.scrollFactor)
            } else {
              const _step = Math.abs(
                this.scroller.y + this.scroller.h / 2 - mousePos.y
              )
              this.scrollByDeltaY(_step * this.scrollFactor)
              clickEnd()
              return
            }
          } else if (direction < 0) {
            if (this.scroller.y + this.scroller.h / 2 - step > mousePos.y) {
              this.scrollByDeltaY(-step * this.scrollFactor)
            } else {
              const _step = Math.abs(
                this.scroller.y + this.scroller.h / 2 - mousePos.y
              )
              this.scrollByDeltaY(-_step * this.scrollFactor)
              clickEnd()
              return
            }
          }
          this.scrollTimer = setTimeout(
            doScroll,
            isFirst ? this.settings.initDelay : this.settings.afterDelay
          )
          isFirst = false
        }
        const clickEnd = () => {
          this.scrollTimer && clearTimeout(this.scrollTimer)
          this.scrollTimer = null
          this.unbind('mouseup.main')
          this.lock = false
        }

        if (this.onMouseLock) {
          this.onMouseLock(evt)
        }

        doScroll()
        this.bind('mouseup.main', clickEnd)
      }
      if (this.isHScroll) {
        const direction = mousePos.x - this.scroller.x - this.scroller.w / 2
        const step = this.widthOfPane * this.settings.parentPercent
        let isFirst = true
        const doScroll = () => {
          this.lock = true
          if (direction > 0) {
            if (this.scroller.x + this.scroller.w / 2 + step < mousePos.x) {
              this.scrollByDeltaX(step * this.scrollFactor)
            } else {
              const step = Math.abs(
                this.scroller.x + this.scroller.w / 2 - mousePos.x
              )
              this.scrollByDeltaX(step * this.scrollFactor)
              clickEnd()
              return
            }
          } else if (direction < 0) {
            if (this.scroller.x + this.scroller.w / 2 - step > mousePos.x) {
              this.scrollByDeltaX(-step * this.scrollFactor)
            } else {
              const step = Math.abs(
                this.scroller.x + this.scroller.w / 2 - mousePos.x
              )
              this.scrollByDeltaX(-step * this.scrollFactor)
              clickEnd()
              return
            }
          }
          this.scrollTimer = setTimeout(
            doScroll,
            isFirst ? this.settings.initDelay : this.settings.afterDelay
          )
          isFirst = false
        }
        const clickEnd = () => {
          this.scrollTimer && clearTimeout(this.scrollTimer)
          this.scrollTimer = null
          this.unbind('mouseup.main')
          this.lock = false
        }

        if (this.onMouseLock) {
          this.onMouseLock(evt)
        }

        doScroll()
        this.bind('mouseup.main', clickEnd)
      }
    }
  }

  onMouseWheel({ eventObj: evt }: DomWheelEvt) {
    if (this.isHScroll) return
    const delta =
      evt.deltaY > 0
        ? this.settings.vscrollDistPerTime
        : -this.settings.vscrollDistPerTime
    this.scroller.y += delta
    if (this.scroller.y < 0) {
      this.scroller.y = 0
    } else if (this.scroller.y + this.scroller.h > this.canvasHeight) {
      this.scroller.y = this.canvasHeight - this.scroller.h
    }
    this.scrollByDeltaY(delta)
  }

  bind(type, handler) {
    const types = type.split(' ')
    for (let n = 0; n < types.length; n++) {
      const type = types[n]
      const event = type.indexOf('touch') === -1 ? 'on' + type : type
      const parts = event.split('.')
      const baseEvent = parts[0]
      const name = parts.length > 1 ? parts[1] : ''

      if (!this.eventListeners[baseEvent]) {
        this.eventListeners[baseEvent] = []
      }

      this.eventListeners[baseEvent].push({
        name: name,
        handler: handler,
      })
    }
  }

  unbind(type) {
    const types = type.split(' ')

    for (let n = 0; n < types.length; n++) {
      const type = types[n]
      const event = type.indexOf('touch') === -1 ? 'on' + type : type
      const parts = event.split('.')
      const baseEvent = parts[0]

      if (this.eventListeners[baseEvent] && parts.length > 1) {
        const name = parts[1]

        for (let i = 0; i < this.eventListeners[baseEvent].length; i++) {
          if (this.eventListeners[baseEvent][i].name === name) {
            this.eventListeners[baseEvent].splice(i, 1)
            if (this.eventListeners[baseEvent].length === 0) {
              this.eventListeners[baseEvent] = undefined
            }
            break
          }
        }
      } else {
        this.eventListeners[baseEvent] = undefined
      }
    }
  }

  triggerEvents(eventType, evt) {
    const elem = this.eventListeners
    if (elem[eventType]) {
      const events = elem[eventType]
      for (let i = 0; i < events.length; i++) {
        events[i].handler.apply(this, [evt])
      }
    }
  }
}

function getDomWidth(elem) {
  const w = elem.clientWidth
  if (0 !== w) return w

  const sw = '' + elem.style.width
  if (-1 < sw.indexOf('%')) return 0

  const width = parseInt(sw)
  if (!isNaN(width) && 0 < width) return width

  return 0
}

function getDomHeight(elem) {
  const h = elem.clientHeight
  if (0 !== h) return h

  const sh = '' + elem.style.height
  if (-1 < sh.indexOf('%')) return 0

  const height = parseInt(sh)
  if (!isNaN(height) && 0 < height) return height

  return 0
}
