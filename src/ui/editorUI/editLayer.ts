import { BrowserInfo } from '../../common/browserInfo'
import { setImageSmoothingForCanvasCtx } from '../../common/dom/utils'
import { UIControl } from './Controls'
import { EditorUI } from './EditorUI'

/** 控制圆点的大小(已经修改为方形控点, 大小不变, 暂时还是按照圆 hittest) */
export const DEFAULT_DASHED_LINE_COLOR = '#000000'
const DEFAULT_DASHED_LINE_DISTANCE = 2
const ARROW_SIZE = 8

export class EditLayer {
  control!: UIControl<string, HTMLCanvasElement>
  canvas: HTMLCanvasElement
  context: CanvasRenderingContext2D
  minX: number
  minY: number
  maxX: number
  maxY: number
  editorUI: EditorUI
  resetAll: boolean

  constructor(
    control: UIControl<string, HTMLCanvasElement>,
    editorUI: EditorUI
  ) {
    this.control = control

    this.canvas = this.control.element
    this.context = this.canvas.getContext('2d')!
    setImageSmoothingForCanvasCtx(this.context, false)

    this.editorUI = editorUI

    this.minX = 0xffff
    this.minY = 0xffff
    this.maxX = -0xffff
    this.maxY = -0xffff

    this.resetAll = false
    this.clear()
  }

  clear() {
    this.resetTransform()

    this.context.beginPath()
    // if (this.maxX !== -0xffff && this.maxY !== -0xffff) {
    //   if (this.resetAll === true) {
    this.context.clearRect(
      0,
      0,
      (this.control.element as HTMLCanvasElement).width,
      (this.control.element as HTMLCanvasElement).height
    )
    this.resetAll = false
    //   } else {
    //     const _eps = 5
    //     this.context.clearRect(
    //       this.minX - _eps,
    //       this.minY - _eps,
    //       this.maxX - this.minX + 2 * _eps,
    //       this.maxY - this.minY + 2 * _eps
    //     )
    //   }
    // }
    this.minX = 0xffff
    this.minY = 0xffff
    this.maxX = -0xffff
    this.maxY = -0xffff
  }

  resetTransform() {
    this.context.setTransform(
      BrowserInfo.PixelRatio,
      0,
      0,
      BrowserInfo.PixelRatio,
      0,
      0
    )
  }

  drawVerticalLine(position, color?: string) {
    if (this.minX > position) this.minX = position
    if (this.maxX < position) this.maxX = position

    const oldAlpha = this.context.globalAlpha
    this.context.globalAlpha = 1

    this.minY = 0
    this.maxY = this.control.element.height

    this.context.lineWidth = 1

    const x = ((position + 0.5) >> 0) + 0.5
    let y = 0

    this.context.strokeStyle = color ?? DEFAULT_DASHED_LINE_COLOR
    this.context.beginPath()

    const dist = DEFAULT_DASHED_LINE_DISTANCE

    while (y < this.maxY) {
      this.context.moveTo(x, y)
      y += dist
      this.context.lineTo(x, y)
      y += dist
    }

    this.context.stroke()
    this.context.beginPath()

    this.context.globalAlpha = oldAlpha
  }

  vertLineFromPointToPoint(
    x: number,
    y: number,
    y1: number,
    offset = 0,
    color?: string,
    withArrow?: boolean
  ) {
    if (this.minX > x) this.minX = x
    if (this.maxX < x) this.maxX = x
    if (y1 < y) {
      const temp = y
      y = y1
      y1 = temp
    }
    const ctx = this.context
    const oldAlpha = ctx.globalAlpha
    ctx.globalAlpha = 1

    this.minY = y - offset
    this.maxY = y1 + offset

    ctx.lineWidth = 1

    ctx.strokeStyle = color ?? DEFAULT_DASHED_LINE_COLOR
    ctx.beginPath()
    const posX = ((x + 0.5) >> 0) + 0.5
    let posY = this.minY
    const interval = DEFAULT_DASHED_LINE_DISTANCE
    while (posY < this.maxY) {
      ctx.moveTo(posX, posY)
      posY += interval
      ctx.lineTo(posX, posY)
      posY += interval
    }
    ctx.stroke()
    ctx.beginPath()

    if (withArrow) {
      ctx.fillStyle = color ?? DEFAULT_DASHED_LINE_COLOR
      const height = ARROW_SIZE * Math.cos(Math.PI / 6)
      ctx.beginPath()
      ctx.moveTo(posX, this.minY)
      ctx.lineTo(posX - ARROW_SIZE / 2, this.minY + height)
      ctx.lineTo(posX + ARROW_SIZE / 2, this.minY + height)
      ctx.fill()
      ctx.beginPath()
      ctx.moveTo(posX, this.maxY)
      ctx.lineTo(posX - ARROW_SIZE / 2, this.maxY - height)
      ctx.lineTo(posX + ARROW_SIZE / 2, this.maxY - height)
      ctx.fill()
    }

    ctx.globalAlpha = oldAlpha
  }

  drawHorizontalLine(positionY: number, color?: string) {
    if (this.minY > positionY) this.minY = positionY
    if (this.maxY < positionY) this.maxY = positionY

    const oldAlpha = this.context.globalAlpha
    this.context.globalAlpha = 1

    this.minX = 0
    this.maxX = this.control.element.width

    this.context.lineWidth = 1

    const y = ((positionY + 0.5) >> 0) + 0.5
    let x = 0

    this.context.strokeStyle = color ?? DEFAULT_DASHED_LINE_COLOR
    this.context.beginPath()

    const dist = DEFAULT_DASHED_LINE_DISTANCE

    while (x < this.maxX) {
      this.context.moveTo(x, y)
      x += dist
      this.context.lineTo(x, y)
      x += dist
    }

    this.context.stroke()
    this.context.beginPath()

    this.context.globalAlpha = oldAlpha
  }

  horLineFromPointToPoint(
    y: number,
    x: number,
    x1: number,
    offset = 0,
    color?: string,
    withArrow?: boolean
  ) {
    if (this.minY > y) this.minY = y
    if (this.maxY < y) this.maxY = y

    if (x1 < x) {
      const temp = x
      x = x1
      x1 = temp
    }

    const ctx = this.context

    const oldAlpha = ctx.globalAlpha
    ctx.globalAlpha = 1

    this.minX = x - offset
    this.maxX = x1 + offset

    ctx.lineWidth = 1

    const posY = ((y + 0.5) >> 0) + 0.5
    let posX = this.minX

    ctx.strokeStyle = color ?? DEFAULT_DASHED_LINE_COLOR
    ctx.beginPath()

    const interval = DEFAULT_DASHED_LINE_DISTANCE
    while (posX < this.maxX) {
      ctx.moveTo(posX, posY)
      posX += interval
      ctx.lineTo(posX, posY)
      posX += interval
    }

    ctx.stroke()
    ctx.beginPath()

    if (withArrow) {
      ctx.fillStyle = color ?? DEFAULT_DASHED_LINE_COLOR
      const height = ARROW_SIZE * Math.cos(Math.PI / 6)
      ctx.beginPath()
      ctx.moveTo(this.minX, posY)
      ctx.lineTo(this.minX + height, posY - ARROW_SIZE / 2)
      ctx.lineTo(this.minX + height, posY + ARROW_SIZE / 2)
      ctx.fill()
      ctx.beginPath()
      ctx.moveTo(this.maxX, posY)
      ctx.lineTo(this.maxX - height, posY - ARROW_SIZE / 2)
      ctx.lineTo(this.maxX - height, posY + ARROW_SIZE / 2)
      ctx.fill()
    }

    this.context.globalAlpha = oldAlpha
  }

  update(minX: number, minY: number, maxX?: number, maxY?: number) {
    if (minX < this.minX) this.minX = minX
    if (minY < this.minY) this.minY = minY
    maxX = maxX ?? minX
    maxY = maxY ?? minY
    if (maxX > this.maxX) this.maxX = maxX
    if (maxY > this.maxY) this.maxY = maxY
  }

  addRectByCircle(x, y, r) {
    const _x = x - ((r / 2) >> 0)
    const _y = y - ((r / 2) >> 0)
    this.update(_x, _y, _x + r, _y + r)

    this.context.moveTo(_x, _y)
    this.context.rect(_x, _y, r, r)
  }

  addRectByEllipse(x, y, r, ex1, ey1, ex2, ey2) {
    const _r = r / 2

    const x1 = x + _r * (ex2 - ex1)
    const y1 = y + _r * (ey2 - ey1)

    const x2 = x + _r * (ex2 + ex1)
    const y2 = y + _r * (ey2 + ey1)

    const x3 = x + _r * (-ex2 + ex1)
    const y3 = y + _r * (-ey2 + ey1)

    const x4 = x + _r * (-ex2 - ex1)
    const y4 = y + _r * (-ey2 - ey1)

    this.update(x1, y1)
    this.update(x2, y2)
    this.update(x3, y3)
    this.update(x4, y4)

    const ctx = this.context
    ctx.moveTo(x1, y1)
    ctx.lineTo(x2, y2)
    ctx.lineTo(x3, y3)
    ctx.lineTo(x4, y4)
    ctx.closePath()
  }

  addRect(x: number, y: number, w: number, h: number) {
    this.updateByRect(x, y, w, h)
    const ctx = this.context
    ctx.moveTo(x, y)
    ctx.lineTo(x + w, y)
    ctx.lineTo(x + w, y + h)
    ctx.lineTo(x, y + h)
    ctx.closePath()
  }

  updateByRect(x: number, y: number, w: number, h: number) {
    this.update(x, y, x + w, y + h)
  }

  addEllipse(x, y, r) {
    this.update(x - r, y - r, x + r, y + r)

    this.context.moveTo(x + r, y)
    this.context.arc(x, y, r, 0, Math.PI * 2, false)
  }
}
