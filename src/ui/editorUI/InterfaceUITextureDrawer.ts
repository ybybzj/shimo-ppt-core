import { EditorUI } from './EditorUI'
import { toInt, checkImageSrc } from '../../common/utils'
import { StatusOfLoadableImage } from '../../images/utils'

export class InterfaceUITextureDrawer {
  editorUI: EditorUI
  uiDrawerShapeFillTextureParentId: string = ''
  uiDrawerShapeFillTexture: null | HTMLCanvasElement = null
  uiDrawerShapeFillTextureCtx: null | CanvasRenderingContext2D = null
  lastShapeUrl: string = ''

  uiDrawerSlideFillTextureParentId: string = ''
  uiDrawerSlideFillTexture: null | HTMLCanvasElement = null
  uiDrawerSlideFillTextureCtx: null | CanvasRenderingContext2D = null
  lastSlideUrl: string = ''

  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI
  }

  drawShapeImageTextureFill(url, lastUrl?) {
    if (arguments.length > 1) {
      this.lastShapeUrl = lastUrl
    }
    if (this.uiDrawerShapeFillTexture == null) {
      this.initShapeUIDrawer(this.uiDrawerShapeFillTextureParentId)
    }

    if (
      this.uiDrawerShapeFillTexture == null ||
      this.uiDrawerShapeFillTextureCtx == null ||
      url === this.lastShapeUrl
    ) {
      return
    }

    this.lastShapeUrl = url
    const width = this.uiDrawerShapeFillTexture.width
    const height = this.uiDrawerShapeFillTexture.height
    const ctx = this.uiDrawerShapeFillTextureCtx
    ctx.clearRect(0, 0, width, height)

    if (null == this.lastShapeUrl) return

    const img =
      this.editorUI.editorKit.imageLoader.imagesMap[
        checkImageSrc(this.lastShapeUrl)
      ]
    if (
      img != null &&
      img.Image != null &&
      img.Status === StatusOfLoadableImage.Complete
    ) {
      let [x, y] = [0, 0]
      let w = Math.max(img.Image.width, 1)
      let h = Math.max(img.Image.height, 1)

      const aspect1 = width / height
      const aspect2 = w / h

      w = width
      h = height
      if (aspect1 >= aspect2) {
        w = aspect2 * height
        x = (width - w) / 2
      } else {
        h = w / aspect2
        y = (height - h) / 2
      }

      ctx.drawImage(img.Image, x, y, w, h)
    } else {
      ctx.lineWidth = 1

      ctx.beginPath()
      ctx.moveTo(0, 0)
      ctx.lineTo(width, height)
      ctx.moveTo(width, 0)
      ctx.lineTo(0, height)
      ctx.strokeStyle = '#FF0000'
      ctx.stroke()

      ctx.beginPath()
      ctx.moveTo(0, 0)
      ctx.lineTo(width, 0)
      ctx.lineTo(width, height)
      ctx.lineTo(0, height)
      ctx.closePath()

      ctx.strokeStyle = '#000000'
      ctx.stroke()
      ctx.beginPath()
    }
  }

  drawSlideImageTextureFill(url) {
    if (this.uiDrawerSlideFillTexture == null) {
      this.initSlideUIDrawer(this.uiDrawerSlideFillTextureParentId)
    }

    if (
      this.uiDrawerSlideFillTexture == null ||
      this.uiDrawerSlideFillTextureCtx == null ||
      url === this.lastSlideUrl
    ) {
      return
    }

    this.lastSlideUrl = url
    const width = this.uiDrawerSlideFillTexture.width
    const height = this.uiDrawerSlideFillTexture.height
    const ctx = this.uiDrawerSlideFillTextureCtx

    ctx.clearRect(0, 0, width, height)

    if (null == this.lastSlideUrl) return

    const img =
      this.editorUI.editorKit.imageLoader.imagesMap[
        checkImageSrc(this.lastSlideUrl)
      ]
    if (
      img != null &&
      img.Image != null &&
      img.Status === StatusOfLoadableImage.Complete
    ) {
      let [x, y] = [0, 0]
      let w = Math.max(img.Image.width, 1)
      let h = Math.max(img.Image.height, 1)

      const aspect1 = width / height
      const aspect2 = w / h

      w = width
      h = height
      if (aspect1 >= aspect2) {
        w = aspect2 * height
        x = (width - w) / 2
      } else {
        h = w / aspect2
        y = (height - h) / 2
      }

      ctx.drawImage(img.Image, x, y, w, h)
    } else {
      ctx.lineWidth = 1

      ctx.beginPath()
      ctx.moveTo(0, 0)
      ctx.lineTo(width, height)
      ctx.moveTo(width, 0)
      ctx.lineTo(0, height)
      ctx.strokeStyle = '#FF0000'
      ctx.stroke()

      ctx.beginPath()
      ctx.moveTo(0, 0)
      ctx.lineTo(width, 0)
      ctx.lineTo(width, height)
      ctx.lineTo(0, height)
      ctx.closePath()

      ctx.strokeStyle = '#000000'
      ctx.stroke()
      ctx.beginPath()
    }
  }

  initShapeUIDrawer(divId: string) {
    if (null != this.uiDrawerShapeFillTexture) {
      const div = document.getElementById(
        this.uiDrawerShapeFillTextureParentId
      )!
      if (div) div.removeChild(this.uiDrawerShapeFillTexture)

      this.uiDrawerShapeFillTexture = null
      this.uiDrawerShapeFillTextureCtx = null
    }

    this.uiDrawerShapeFillTextureParentId = divId
    const div = document.getElementById(this.uiDrawerShapeFillTextureParentId)
    if (!div) return

    this.uiDrawerShapeFillTexture = document.createElement('canvas')
    this.uiDrawerShapeFillTexture.width = toInt(div.style.width)
    this.uiDrawerShapeFillTexture.height = toInt(div.style.height)

    this.lastShapeUrl = ''
    this.uiDrawerShapeFillTextureCtx =
      this.uiDrawerShapeFillTexture.getContext('2d')

    div.appendChild(this.uiDrawerShapeFillTexture)
  }

  initSlideUIDrawer(divId) {
    if (null != this.uiDrawerSlideFillTexture) {
      const div = document.getElementById(
        this.uiDrawerSlideFillTextureParentId
      )!
      if (div) div.removeChild(this.uiDrawerSlideFillTexture)

      this.uiDrawerSlideFillTexture = null
      this.uiDrawerSlideFillTextureCtx = null
    }

    this.uiDrawerSlideFillTextureParentId = divId
    const div = document.getElementById(this.uiDrawerSlideFillTextureParentId)
    if (!div) return

    this.uiDrawerSlideFillTexture = document.createElement('canvas')
    this.uiDrawerSlideFillTexture.width = toInt(div.style.width)
    this.uiDrawerSlideFillTexture.height = toInt(div.style.height)

    this.lastSlideUrl = ''
    this.uiDrawerSlideFillTextureCtx =
      this.uiDrawerSlideFillTexture.getContext('2d')

    div.appendChild(this.uiDrawerSlideFillTexture)
  }
}
