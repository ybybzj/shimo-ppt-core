import { EditorSettings } from '../../core/common/EditorSettings'

export const splitterStates: {
  /** LTR 下为 thumnail 的 mmX 坐标 */
  thumbnailSplitterPos: number
  oldThumbnailSplitterPos: number
  thumbnailSplitterPosMin: number
  thumbnailSplitterPosMax: number
  notesSplitterPos: number
  notesSplitterPosMin: number
  notesSplitterPosMax: number
  check(): void
  updateThumbnailSplitterPos(pos: number): void
  resetThumbnailSplitterPos(): void
  resetNoteSplitterPos(): void
  zeroThumbnailSplitterPos(cb: () => void): void
  updateNotesSplitterPos(pos: number): void
} = {
  thumbnailSplitterPos: getDefaultThumbnailSplitterPos(),
  thumbnailSplitterPosMin: 50,
  thumbnailSplitterPosMax: 100,
  notesSplitterPos: getDefaultNoteSplitterPos(),
  notesSplitterPosMin: 10,
  notesSplitterPosMax: 100,
  oldThumbnailSplitterPos: 0,

  check() {
    splitterStates.oldThumbnailSplitterPos = splitterStates.thumbnailSplitterPos
    if (splitterStates.notesSplitterPos < 1) {
      splitterStates.notesSplitterPos = 1
    }
  },

  resetThumbnailSplitterPos() {
    splitterStates.thumbnailSplitterPos = getDefaultThumbnailSplitterPos()
    splitterStates.check()
  },

  resetNoteSplitterPos() {
    splitterStates.notesSplitterPos = getDefaultNoteSplitterPos()
    splitterStates.check()
  },

  zeroThumbnailSplitterPos(cb: () => void) {
    const old = splitterStates.oldThumbnailSplitterPos
    splitterStates.updateThumbnailSplitterPos(0)
    if (typeof cb === 'function') {
      cb()
    }
    splitterStates.oldThumbnailSplitterPos = old
  },

  updateThumbnailSplitterPos(pos: number) {
    splitterStates.thumbnailSplitterPos = pos
    splitterStates.check()
  },

  updateNotesSplitterPos(pos: number) {
    splitterStates.notesSplitterPos = pos
    splitterStates.check()
  },
}

function getDefaultNoteSplitterPos() {
  if (!EditorSettings.isNotesEnabled) return 0
  return 11
}

function getDefaultThumbnailSplitterPos() {
  if (!EditorSettings.isShowThumbnail) return 0
  return 52
}

export const SCROLLBAR_WIDTH_PX = 10
