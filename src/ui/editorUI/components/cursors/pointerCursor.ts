import { EditorSettings } from '../../../../core/common/EditorSettings'
import {
  MouseMoveData,
  MouseMoveDataTypes,
} from '../../../../core/properties/MouseMoveData'
import { translator } from '../../../../globals/translator'
import { isPlayingAnimation } from '../../../transitions/utils'
import { EditorUI } from '../../EditorUI'
import { StatusOfFormatPainter } from '../../const'
import { SCROLLBAR_WIDTH_PX } from '../../splitterStates'
import { cursorExtType, getCursorStyle } from '../../uiCursor'

export class PointerCursorComponent {
  editorUI: EditorUI
  lockedCursorType: string = ''

  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI
  }

  /** 有关 cursorType 的控制可以统一在此处管理 */
  setCursorType(cursorType: string, moveData?: MouseMoveData) {
    const editorUI = this.editorUI
    if (
      isPlayingAnimation(editorUI) ||
      (editorUI.editorKit.getViewMode() && cursorType === 'col-resize')
    ) {
      cursorType = cursorExtType.Default
    }
    // handler rtl cursor
    if (EditorSettings.isRTL && cursorType === cursorExtType.CommentPainter) {
      cursorType = cursorExtType.CommentPainterRTL
    }
    let elem: HTMLElement = editorUI.mainContent.element
    if (editorUI.showManager.isInShowMode) {
      elem = editorUI.showManager.canvas!
    }
    const notesElem = editorUI.mainContainer.Controls[1]?.element
    if ('' === this.lockedCursorType) {
      if (editorUI.editorKit.paintFormatState !== StatusOfFormatPainter.OFF) {
        const fmtCopySrc = editorUI.presentation.getFormatSrc()
        if (
          fmtCopySrc === 'text' &&
          (cursorExtType.Default === cursorType ||
            cursorType === cursorExtType.Text ||
            cursorType === cursorExtType.VerticalText)
        ) {
          elem.style.cursor = getCursorStyle(cursorExtType.FormatPainter)
        } else if (fmtCopySrc === 'sp') {
          elem.style.cursor = getCursorStyle(cursorExtType.SpFormatPainter)
        }
      } else {
        elem.style.cursor = getCursorStyle(cursorType)
        if (notesElem) {
          notesElem.style.cursor = getCursorStyle(cursorType)
        }
      }
    } else {
      elem.style.cursor = getCursorStyle(this.lockedCursorType)
    }

    if (null == moveData) {
      moveData = new MouseMoveData()
    }

    // 此处统一处理所有 scrrenTip 效果
    if (moveData.type === MouseMoveDataTypes.Rotate) {
      // Todo: 集中管理 RTL 开关
      const isRTL = editorUI.thumbnailsManager.isRTL
      let absX = isRTL ? moveData.absX - editorUI.width : moveData.absX
      let absY = moveData.absY + 20
      const offset = SCROLLBAR_WIDTH_PX
      if (absY > editorUI.height - offset) {
        absY = editorUI.height - offset
      }

      const rotateInfo = moveData?.rotateInfo
      if (rotateInfo && rotateInfo.state === 'Hover') {
        editorUI.screenTipManager.debounceTogglePaint((dom: HTMLDivElement) => {
          dom.innerText = translator.getValue('按住 shift 键，以 15° 增量旋转')
          const tipWidth = dom.offsetWidth
          // LTR 矫正过右, RTL 矫正过左
          if (!isRTL && editorUI.width - absX < tipWidth + offset) {
            absX = editorUI.width - tipWidth - offset
          } else if (isRTL && editorUI.width + absX < tipWidth + offset) {
            absX = tipWidth + offset - editorUI.width
          }
          dom.style.transform = `translate(${absX}px, ${absY}px)`
        })
      }
      if (rotateInfo && rotateInfo.state === 'Track') {
        editorUI.screenTipManager.toggleScreenTipDisplay({
          isShow: true,
          callback: (dom: HTMLDivElement) => {
            if (!isRTL && editorUI.width - absX < 80 + offset) {
              absX = editorUI.width - 80 - offset
            } else if (isRTL && editorUI.width + absX < 80 + offset) {
              absX = 80 + offset - editorUI.width
            }
            const angle = rotateInfo.angle
            dom.innerHTML = `<svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg"> <path d="M0.857178 0.857147V11.1429H11.1429" stroke="white" stroke-width="0.857143"></path> <path d="M6.00005 10.7143C6.00005 8.11066 3.88939 6 1.28577 6" stroke="white" stroke-width="0.857143"></path> <rect width="1.71429" height="1.71429" fill="white"></rect> <rect y="10.2857" width="1.71429" height="1.71429" fill="white"></rect> <rect x="10.2858" y="10.2857" width="1.71429" height="1.71429" fill="white"></rect> </svg> ${angle}º`
            dom.style.willChange = 'transform'
            dom.style.transform = `translate(${absX}px, ${absY}px)`
          },
        })
      }
    } else {
      editorUI.screenTipManager.toggleScreenTipDisplay({ isShow: false })
    }
    editorUI.editorKit.emitMouseMove(moveData)
  }

  lock(cursotType?) {
    this.lockedCursorType = cursotType
    this.editorUI.mainContent.element.style.cursor = getCursorStyle(
      this.lockedCursorType
    )
  }

  unLock() {
    this.lockedCursorType = ''
  }

  onEndAddShape() {
    if (this.lockedCursorType === 'crosshair') {
      this.unLock()
    }
  }
}
