import { EditorUI } from '../../EditorUI'
import { BrowserInfo } from '../../../../common/browserInfo'
import { ZoomValue } from '../../../../core/common/zoomValue'
import { getAndUpdateCursorPositionForPresentation } from '../../../../core/utilities/cursor/cursorPosition'
import { Matrix2D, MatrixUtils } from '../../../../core/graphic/Matrix'
import { Factor_mm_to_pix } from '../../../../core/common/const/unit'
import { isPlayingAnimation } from '../../../transitions/utils'
import { convertInnerMMToPixPos } from '../../utils/utils'
import { EditorUtil } from '../../../../globals/editor'
import {
  cursorBlinkSwitch,
  drawTextCursor,
  hitTestTextCursor,
} from '../../../rendering/Drawer/textCursorDrawer'
import { Nullable } from '../../../../../liber/pervasive'
import { drawSelectionRect } from '../../../rendering/Drawer/selectionRectDrawer'
import { ColorRGBA } from '../../../../core/color/type'

interface CursorDrawingInfo {
  targetX: number
  targetY: number
  hitX: number
  hitY: number
  size: number
}

function _isEmptyCursorDrawingInfo(cDrawInfo: CursorDrawingInfo) {
  return (
    cDrawInfo.targetX === -1 ||
    cDrawInfo.targetY === -1 ||
    cDrawInfo.hitX === -1 ||
    cDrawInfo.hitY === -1 ||
    cDrawInfo.size <= 0
  )
}

function _clearCursorDrawingInfo(cDrawInfo: CursorDrawingInfo) {
  cDrawInfo.targetX = -1
  cDrawInfo.targetY = -1
  cDrawInfo.hitX = -1
  cDrawInfo.hitY = -1
  cDrawInfo.size = 0
}

export class TextCursorComponent {
  editorUI: EditorUI
  public left: number = 0
  public top: number = 0
  public height: number = 0
  size: number = 1
  private ascent: number = 0
  targetX: number = -1
  targetY: number = -1
  matrix: null | Matrix2D = null
  private cursorColor!: ColorRGBA
  private isUpdatePosition?: boolean
  private isShow = false
  private isRepaintOnUpdate: boolean = false
  private isCheckScroll: boolean = true
  private selectionCursors: {
    start: CursorDrawingInfo
    end: CursorDrawingInfo
  } = {
    start: {
      targetX: -1,
      targetY: -1,
      hitX: -1,
      hitY: -1,
      size: 0,
    },
    end: {
      targetX: -1,
      targetY: -1,
      hitX: -1,
      hitY: -1,
      size: 0,
    },
  }

  get presentation() {
    return this.editorUI.editorKit.presentation
  }

  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI
    this.setColor(0, 0, 0)
  }

  setColor(r: number, g: number, b: number) {
    this.cursorColor = {
      r: r,
      g: g,
      b: b,
      a: 255,
    }
  }

  setCheckScroll(b: boolean) {
    this.isCheckScroll = b
  }

  checkRender() {
    const presentation = this.presentation
    if (presentation) {
      if (true === this.isUpdatePosition) {
        cursorBlinkSwitch.setInit(true)
        this.isRepaintOnUpdate = true
        // this function may invoke this.onUpdate method
        getAndUpdateCursorPositionForPresentation(this.editorUI.presentation)
        this.isRepaintOnUpdate = false
        this.render()
      } else {
        if (this.editorUI.isInTouchMode() && this.editorUI.needRerender) {
          this.render(true)
        } else {
          cursorBlinkSwitch.switchState()
          this.render(true)
        }
      }
      this.isUpdatePosition = false
    }
  }

  onUpdateSelectionStart({
    targetX,
    targetY,
    hitX: left,
    hitY: top,
    size,
  }: CursorDrawingInfo) {
    this.selectionCursors.start.targetX = targetX
    this.selectionCursors.start.targetY = targetY
    this.selectionCursors.start.hitX = left
    this.selectionCursors.start.hitY = top
    this.selectionCursors.start.size = size
  }

  onUpdateSelectionEnd({
    targetX,
    targetY,
    hitX: left,
    hitY: top,
    size,
  }: CursorDrawingInfo) {
    this.selectionCursors.end.targetX = targetX
    this.selectionCursors.end.targetY = targetY
    this.selectionCursors.end.hitX = left
    this.selectionCursors.end.hitY = top
    this.selectionCursors.end.size = size
  }

  onClearSelection() {
    _clearCursorDrawingInfo(this.selectionCursors.start)
    _clearCursorDrawingInfo(this.selectionCursors.end)
  }

  onUpdate(x: number, y: number, slideIndex: number) {
    const editorUI = this.editorUI
    const renderingController = editorUI.renderingController
    if (
      slideIndex !== renderingController.curSlideIndex &&
      !editorUI.showManager.isInShowMode
    ) {
      editorUI.goToSlide(slideIndex)
    }
    if (this.isRepaintOnUpdate === false) {
      this.isUpdatePosition = true
      return
    }

    this.updatePositionAndCheckScroll(x, y)
  }
  onRenderTextCursor(
    x: number,
    y: number,
    size: number,
    colorStyle: ColorRGBA
  ) {
    return drawTextCursor(this.editorUI, x, y, size, colorStyle, this.matrix)
  }

  private render(onlyRedraw = false) {
    const renderingController = this.editorUI.renderingController
    if (
      this.isShow === false ||
      renderingController.countOfSlides === 0 ||
      isPlayingAnimation(this.editorUI)
    ) {
      return
    }

    const { left, top, height } = this.onRenderTextCursor(
      this.targetX,
      this.targetY,
      this.size,
      this.cursorColor
    )

    if (onlyRedraw === true) {
      return
    }

    this.left = left
    this.top = top
    this.height = height

    // update input element position
    const isSlideFocused = !this.presentation.isNotesFocused

    EditorUtil.inputManager.notesAreaOffY = 0
    const notesView = this.editorUI.notesView
    if (!isSlideFocused) {
      EditorUtil.inputManager.notesAreaOffY =
        (notesView.control.PositionInPx.T * Factor_mm_to_pix) >> 0
    }
    this.updateWithInputContext()
  }

  private updatePositionAndCheckScroll(x: number, y: number) {
    // update position
    this.targetX = x
    this.targetY = y

    if (this.isCheckScroll === false) return
    // check scroll
    const editorUI = this.editorUI
    const isFocusOnNotes = editorUI.presentation.isNotesFocused
    const zoomValue = isFocusOnNotes ? 100 : ZoomValue.value

    /// detect need scrolling
    const size = Number((this.size * zoomValue * Factor_mm_to_pix) / 100)
    let _x = x
    let _y = y
    if (this.matrix) {
      _x = this.matrix.XFromPoint(x, y)
      _y = this.matrix.YFromPoint(x, y)
    }

    if (!isFocusOnNotes) {
      // focus ton slide
      const pos = convertInnerMMToPixPos(editorUI, _x, _y)

      const ww = editorUI.editorView.element.width / BrowserInfo.PixelRatio
      const hh = editorUI.editorView.element.height / BrowserInfo.PixelRatio

      const boundX = 0
      const boundY = 0
      const boundRight = ww - 2
      const boundBottom = hh - size

      const scrollState = editorUI.scrolls.state
      let valueOfScrollHor = 0
      if (pos.x < boundX) {
        valueOfScrollHor = (scrollState.scrollX + pos.x - boundX) >> 0
      }
      if (pos.x > boundRight) {
        valueOfScrollHor = (scrollState.scrollX + pos.x - boundRight) >> 0
      }

      let valueOfValueScrollVer = 0
      if (pos.y < boundY) {
        valueOfValueScrollVer = (scrollState.scrollY + pos.y - boundY) >> 0
      }
      if (pos.y > boundBottom) {
        valueOfValueScrollVer = (scrollState.scrollY + pos.y - boundBottom) >> 0
      }

      /// check scroll
      let isNeedScroll = false
      if (0 !== valueOfScrollHor && editorUI.scrolls.hScrollBar) {
        isNeedScroll = true

        if (valueOfScrollHor > scrollState.scrollYmax) {
          valueOfScrollHor = scrollState.scrollYmax
        }
        if (0 > valueOfScrollHor) valueOfScrollHor = 0

        if (editorUI.selectScrollTimerId === -1) {
          editorUI.scrolls.hScrollBar.scrollToX(valueOfScrollHor)
        }
      }
      if (0 !== valueOfValueScrollVer) {
        isNeedScroll = true

        if (valueOfValueScrollVer > scrollState.slideMaxScroll) {
          valueOfValueScrollVer = scrollState.slideMaxScroll - 1
        }
        if (scrollState.slideMinScroll > valueOfValueScrollVer) {
          valueOfValueScrollVer = scrollState.slideMinScroll
        }

        if (editorUI.selectScrollTimerId === -1) {
          editorUI.scrolls.vScrollBar!.scrollToY(valueOfValueScrollVer)
        }
      }

      if (true === isNeedScroll) {
        editorUI.prepareRender()
        return
      }
    } else if (editorUI.notesView) {
      let yPos = _y * Factor_mm_to_pix - editorUI.notesView.scroll
      const hh =
        editorUI.notesView.notes.element.height / BrowserInfo.PixelRatio

      const boundY = 0
      const targetAscent = (this.ascent * Factor_mm_to_pix) >> 0

      let boundBottom = hh - (size - targetAscent)
      if (boundBottom < 0) boundBottom = hh

      yPos += targetAscent

      let valueOfScrollVer = 0
      if (yPos < boundY) {
        valueOfScrollVer = (editorUI.notesView.scroll + yPos - boundY) >> 0
      }
      if (yPos > boundBottom) {
        valueOfScrollVer = (editorUI.notesView.scroll + yPos - boundBottom) >> 0
      }

      /// check scroll
      if (0 !== valueOfScrollVer) {
        editorUI.notesView.scrollBar!.scrollToY(valueOfScrollVer)
        editorUI.prepareRender()
        return
      }
    }
  }

  setSize(size: number, ascent: number) {
    this.size = size
    this.ascent = ascent ?? 0
  }

  updateWithInputContext() {
    if (EditorUtil.inputManager) {
      EditorUtil.inputManager.move(this.left, this.top)
    }
  }

  updateTransform(matrix) {
    this.matrix = matrix
  }

  getCursorXY(): Nullable<[number /*X*/, number /*Y*/]> {
    if (this.targetX === -1 || this.targetY === -1) return undefined
    return [this.targetX, this.targetY]
  }

  private multiplyTransform(matrix) {
    if (!this.matrix) {
      this.matrix = matrix
    } else if (matrix) {
      MatrixUtils.preMultiplyMatrixes(this.matrix, matrix)
    }
  }

  onMultiplyTargetTransform(callback: () => void, transform?: Matrix2D) {
    let oldMatrix: any
    if (null != transform) {
      if (this.matrix) {
        oldMatrix = this.matrix
        this.matrix = this.matrix.clone()
      }
      this.multiplyTransform(transform.clone())
    }

    callback()

    if (null != transform) {
      this.matrix = oldMatrix
    }
  }

  onSelectionDraw(
    slideIndex: number,
    x: number,
    y: number,
    width: number,
    height: number,
    matrix?: Nullable<Matrix2D>
  ) {
    drawSelectionRect(
      this.editorUI,
      slideIndex,
      x,
      y,
      width,
      height,
      matrix ?? this.matrix
    )
  }

  show() {
    this.isShow = true
  }

  hide() {
    this.isShow = false
  }

  isVisible() {
    return this.isShow
  }

  hitTestXYForCursor(
    x: number,
    y: number
  ): { hit: false } | { hit: true; mmX: number; mmY: number } {
    if (this.targetX === -1 || this.targetY === -1) return { hit: false }

    return hitTestTextCursor(
      x,
      y,
      this.targetX,
      this.targetY,
      this.size,
      this.editorUI
    )
  }

  hitTestXYForSelectionEdge(
    x: number,
    y: number
  ):
    | { hit: false }
    | {
        hit: true
        otherEdgeXY: { x: number; y: number }
        XY: { x: number; y: number }
        isHitHandle: boolean
      } {
    if (this.editorUI.renderingController.getInTextSelection() === false) {
      return { hit: false }
    }

    const { start: startEdge, end: endEdge } = this.selectionCursors
    if (
      _isEmptyCursorDrawingInfo(startEdge) ||
      _isEmptyCursorDrawingInfo(endEdge)
    ) {
      return { hit: false }
    }

    let hitInfo = hitTestTextCursor(
      x,
      y,
      startEdge.targetX,
      startEdge.targetY,
      startEdge.size,
      this.editorUI
    )

    if (hitInfo.hit) {
      return {
        hit: true,
        XY: {
          x: hitInfo.mmX,
          y: hitInfo.mmY,
        },
        otherEdgeXY: {
          x: endEdge.hitX,
          y: endEdge.hitY,
        },
        isHitHandle: hitInfo.isHitHandle,
      }
    }

    hitInfo = hitTestTextCursor(
      x,
      y,
      endEdge.targetX,
      endEdge.targetY,
      endEdge.size,
      this.editorUI
    )

    if (hitInfo.hit) {
      return {
        hit: true,
        XY: {
          x: hitInfo.mmX,
          y: hitInfo.mmY,
        },
        otherEdgeXY: {
          x: startEdge.hitX,
          y: startEdge.hitY,
        },
        isHitHandle: hitInfo.isHitHandle,
      }
    }

    return { hit: false }
  }
}
