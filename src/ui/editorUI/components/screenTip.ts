import { DOM_ID } from '../controls/const'
import { EditorUI } from '../EditorUI'

interface DisplayParams {
  isShow: boolean
  callback?: (div: HTMLDivElement) => void
}

export class ScreenTipManager {
  editorUI: EditorUI
  timerId?: any
  visibility = false

  get screenTipElement() {
    return this.editorUI.editorControl.elements[DOM_ID.Tip]
  }

  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI
  }

  /** Todo: CSS 实现 */
  debounceTogglePaint = (
    callback?: (dom: HTMLDivElement) => void,
    delay = 300
  ) => {
    const next = () => this.toggleScreenTipDisplay({ isShow: true, callback })
    clearTimeout(this.timerId)
    this.timerId = setTimeout(next, delay)
  }

  toggleScreenTipDisplay(params: DisplayParams) {
    const screenTipElement = this.screenTipElement
    const { isShow, callback } = params

    if (isShow) {
      if (callback) callback(screenTipElement)
      if (!this.visibility) {
        this.visibility = true
        screenTipElement.style.visibility = 'visible'
      }
    }
    if (!isShow) {
      if (this.visibility) {
        this.visibility = false
        screenTipElement.style.visibility = 'hidden'
      }
      if (callback) callback(screenTipElement)
      if (this.timerId) {
        clearTimeout(this.timerId)
      }
    }
  }

  hide() {
    if (this.visibility) {
      this.visibility = false
      this.screenTipElement.style.visibility = 'hidden'
    }
  }
}
