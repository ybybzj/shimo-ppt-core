import { EditorUI } from '../EditorUI'
import { shrinkByRetinaRatio } from '../../../common/browserInfo'
import { PointerEventInfo } from '../../../common/dom/events'
import { SyncSemaphoreLock } from '../../../common/SyncSemaphoreLock'
import { toInt } from '../../../common/utils'
import { EditorSkin } from '../../../core/common/const/drawing'
import {
  RightPanelVScrollCtrl,
  VScrollCtrl,
  VScrollCtrlSettings,
} from '../controls/RightPanel'
import { ScrollHorCtrl, ScrollHorCtrlSettings } from '../controls/ScrollHor'
import { ScrollBar, ScrollBarSettings } from '../scroll'
import { calculateDimensionInfo, EditorDemensionInfo } from '../../helpers'
import { ZoomValue } from '../../../core/common/zoomValue'
import { DOM_ID } from '../controls/const'
import { getCtrlParamsById } from '../controls/rtl'
import { convertPixToInnerMMPos, getScrollPageEPS } from '../utils/utils'
import { Factor_mm_to_pix } from '../../../core/common/const/unit'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { SCROLL_DEBOUNCE_LIMIT } from '../const'

interface SlideRect {
  left: number
  right: number
  top: number
  bottom: number
}

export class ScrollComponent {
  editorUI: EditorUI
  canScrollHor = false
  hScrollControl!: ScrollHorCtrl
  vScrollControl!: RightPanelVScrollCtrl
  hScrollBar: ScrollBar | null = null
  vScrollBar: ScrollBar | null = null
  startVScroll = false
  scrollLocker: SyncSemaphoreLock = new SyncSemaphoreLock()
  state: ScrollState
  private isRePaintOnScroll: boolean = true
  private isGoToSlideMAXPos: boolean = false
  private zoomFreeSlideIndex: number = -1
  private timerId?
  private scrolledFlag?: 'top' | 'bottom' = 'top'
  private centerPosition?: { xInMM: number; yInMM: number } // zoom 后 scroller 应恢复的屏幕中心位置
  private pointer: PointerEventInfo = new PointerEventInfo()

  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI
    this.state = new ScrollState(this)
    this.init()
  }
  init() {
    this.vScrollControl = VScrollCtrl(
      ...getCtrlParamsById(VScrollCtrlSettings.name, this.editorUI)!
    )
    this.hScrollControl = ScrollHorCtrl(
      ...getCtrlParamsById(ScrollHorCtrlSettings.name, this.editorUI)!
    )
  }

  removeEvents() {
    if (this.hScrollBar) {
      this.hScrollBar.removeEvents()
    }
    if (this.vScrollBar) {
      this.vScrollBar.removeEvents()
    }
  }

  updateSettingsForScroll() {
    let settings = genSettingsFromScrolls(this)
    settings.lockShow = true

    if (this.canScrollHor) {
      if (this.hScrollBar) {
        this.hScrollBar.resetBySettings(settings, 'h')
      } else {
        const container =
          this.hScrollControl.elements[
            DOM_ID.MainContainer.Main.HScrollPanel.HScrollContainer.Id
          ]
        this.hScrollBar = new ScrollBar(container, settings)
        this.hScrollBar.bind('scrollH', (evt) => {
          this.onScrollH(this, evt.scrollDest, evt.maxScrollX)
        })

        this.hScrollBar.onMouseLock = (evt) => {
          this.pointer.onDown(evt, true, true)
        }
        this.hScrollBar.onUnLockMouse = (evt) => {
          this.pointer.onUp(evt)
        }
      }
    }

    settings = genSettingsFromScrolls(this)
    if (this.vScrollBar) {
      this.vScrollBar.resetBySettings(settings, 'v') //unbind("scrollV")
    } else {
      this.vScrollBar = new ScrollBar(
        this.vScrollControl.elements[
          DOM_ID.MainContainer.Main.RightContainer.VScrollContainer.Id
        ],
        settings
      )

      this.vScrollBar.onMouseLock = (evt) => {
        this.pointer.onDown(evt, true, true)
      }
      this.vScrollBar.onUnLockMouse = (evt) => {
        this.pointer.onUp(evt)
      }

      this.vScrollBar.bind('scrollV', (evt) => {
        this.onScrollV(this, evt.scrollDest, evt.maxScrollY)
      })
      this.vScrollBar.bind('mouseup', (evt) => {
        this.onSelectScrollVEnd(this, evt)
      })
      this.vScrollBar.bind('updateVScroll', (yPos) => {
        return this.correctSpeedVScroll(yPos)
      })
      this.vScrollBar.bind('updateVScrollByDelta', (delta) => {
        return this.correctVScrollByDeltaY(delta)
      })
    }

    this.state.onUpdateScrolls()
  }

  checkHorizontalScroll() {
    const editorUI = this.editorUI
    this.canScrollHor = this.canScrollH()

    const horizontalScroll =
      this.hScrollControl.elements[
        DOM_ID.MainContainer.Main.HScrollPanel.HScrollContainer.Scroll
      ]
    horizontalScroll.style.width = this.state.contentWidth + 'px'

    if (this.canScrollHor) {
      if (EditorSettings.isMobile) {
        editorUI.panelRight.bounds.B = 0
        editorUI.mainView.bounds.B = 0
      } else {
        this.hScrollControl.element.style.display = 'block'
      }
    } else {
      this.hScrollControl.element.style.display = 'none'
    }
  }

  canScrollH() {
    const width = this.state.contentWidth
    const editorUI = this.editorUI
    const w = editorUI.editorView.getPxWidth()
    return width <= w ? false : true
  }

  debounceSetScrolledFlag = (flag: 'bottom' | 'top') => {
    this.scrolledFlag = undefined
    if (this.timerId) clearTimeout(this.timerId)
    this.timerId = setTimeout(() => {
      this.scrolledFlag = flag
    }, SCROLL_DEBOUNCE_LIMIT)
  }

  canScrollVByDeltaY(deltaY: number) {
    const docHeight = this.state.contentHeight
    const editorUI = this.editorUI
    const viewportHeight = editorUI.editorView.getPxHeight()

    if (!this.vScrollBar || viewportHeight >= docHeight) return false
    const curScrollY = this.vScrollBar.curYOfVScroll
    const EPS = getScrollPageEPS(editorUI)

    if (deltaY < 0) {
      if (curScrollY > 0) {
        this.scrolledFlag = undefined
        return true
      }
      if (curScrollY === 0 && deltaY < -EPS) {
        if (this.scrolledFlag === 'top') {
          this.scrolledFlag = undefined
          return false
        } else {
          this.debounceSetScrolledFlag('top')
          return true
        }
      } else {
        return true
      }
    } else {
      const curHeight = viewportHeight + curScrollY
      if (docHeight > curHeight) return true
      if (docHeight === curHeight && deltaY > EPS) {
        if (this.scrolledFlag === 'bottom') {
          this.scrolledFlag = undefined
          return false
        } else {
          this.debounceSetScrolledFlag('bottom')
          return true
        }
      } else {
        return false
      }
    }
  }

  updateVScrollHeight(height: number) {
    const vscroll =
      this.vScrollControl.elements[
        DOM_ID.MainContainer.Main.RightContainer.VScrollContainer.Scroll
      ]
    vscroll.style.height = `${height}px`
  }

  correctSpeedVScroll(scrollPos) {
    if (this.scrollLocker.isLocked()) return
    this.startVScroll = true
    const res = { isChange: false, pos: scrollPos }
    return res
  }

  onUpdateDocumentSize() {
    this.state.onUpdateDocumentSize()
    this.scrollLocker.lock()

    this.checkHorizontalScroll()
    this.updateVScrollHeight(this.state.contentHeight)
    this.updateSettingsForScroll()

    this.scrollLocker.unlock()
  }

  canZoom() {
    return this.state.isValid()
  }

  onSlideZoom(slideIndex: number) {
    const scrollYRatio = this.state.getScrollYRatio()
    this.isRePaintOnScroll = false
    this.zoomFreeSlideIndex = slideIndex
    const posY = toInt(scrollYRatio * this.vScrollBar!.getMaxScrollY())
    this.vScrollBar!.scrollToY(posY)

    this.zoomFreeSlideIndex = -1
    this.isRePaintOnScroll = true
  }

  onGoToSlide(slideIndex: number) {
    this.zoomFreeSlideIndex = slideIndex
    this.resetCurCenterPos()
    const state = this.state

    if (this.isGoToSlideMAXPos) {
      if (state.slideMaxScroll > state.scrollYMax) {
        state.slideMaxScroll = state.scrollYMax
      }
      this.vScrollBar!.scrollToY(state.slideMaxScroll)
      this.isGoToSlideMAXPos = false
    } else {
      if (state.scrollCentralY > state.scrollYMax) {
        state.scrollCentralY = state.scrollYMax
      }
      this.vScrollBar!.scrollToY(state.scrollCentralY)
    }

    if (this.canScrollHor) {
      if (state.scrollCentralX > state.scrollYmax) {
        state.scrollCentralX = state.scrollYmax
      }
      this.hScrollBar!.scrollToX(state.scrollCentralX, true)
    }

    this.zoomFreeSlideIndex = -1
    this.scrolledFlag = 'top'
  }

  scrollByDeltaY(deltaY: number) {
    const { scrollY, slideMinScroll, slideMaxScroll } = this.state
    if (
      this.vScrollBar &&
      scrollY + deltaY >= slideMinScroll &&
      scrollY + deltaY <= slideMaxScroll
    ) {
      this.vScrollBar.scrollByDeltaY(deltaY, false)
    }
  }

  scrollByDeltaX(deltaX: number) {
    if (this.hScrollBar) {
      this.hScrollBar.scrollByDeltaX(deltaX)
    }
  }

  calcSlideRenderInfo(rect: SlideRect) {
    this.state.calcSlideRenderInfo(rect)
  }

  private onScrollH(_sender, scrollX, maxX) {
    const editorUI = this.editorUI
    if (false === editorUI.editorKit.isEditorUIInitComplete) return
    if (this.scrollLocker.isLocked()) return

    const state = this.state
    state.scrollX = scrollX
    state.scrollYmax = maxX

    if (this.isRePaintOnScroll === true) {
      editorUI.rerenderOnScrollOrZoom()
    }
  }

  private onScrollV(_sender, scrollY, maxY) {
    const editorUI = this.editorUI
    const state = this.state
    if (false === editorUI.editorKit.isEditorUIInitComplete) return
    if (this.scrollLocker.isLocked()) return

    if (!editorUI.renderingController.isEmptySlides) {
      const curSlideIndex = editorUI.renderingController.curSlideIndex
      let isChangeSlide = true
      if (
        -1 !== this.zoomFreeSlideIndex &&
        this.zoomFreeSlideIndex === editorUI.renderingController.curSlideIndex
      ) {
        isChangeSlide = false
      }

      if (isChangeSlide) {
        if (curSlideIndex !== editorUI.renderingController.curSlideIndex) {
          if (this.isGoToSlideMAXPos) {
            if (curSlideIndex >= editorUI.renderingController.curSlideIndex) {
              this.isGoToSlideMAXPos = false
            }
          }

          editorUI.goToSlide(curSlideIndex)
          return
        } else if (state.slideMaxScroll < scrollY) {
          state.scrollY = state.slideMaxScroll
          this.isGoToSlideMAXPos = false
          return
        }
      } else {
        editorUI.goToSlide(this.zoomFreeSlideIndex)
      }
    } else {
      if (this.startVScroll) return
    }

    state.scrollY = scrollY
    state.scrollYMax = maxY
    this.isGoToSlideMAXPos = false

    if (this.isRePaintOnScroll === true) editorUI.rerenderOnScrollOrZoom()
  }

  private onSelectScrollVEnd(_sender, _e) {
    const editorUI = this.editorUI
    if (this.scrollLocker.isLocked() || !this.startVScroll) {
      return
    }

    if (editorUI.renderingController.isEmptySlides) {
      this.startVScroll = false
      this.vScrollBar!.scrollByDeltaY(0, true)
      return
    }

    this.startVScroll = false
    this.vScrollBar!.scrollByDeltaY(0, true)
  }

  private correctVScrollByDeltaY(
    delta: number
  ): undefined | { isChange: boolean; pos: number } {
    const state = this.state
    if (this.scrollLocker.isLocked()) return

    this.isGoToSlideMAXPos = true
    const res = { isChange: false, pos: delta }

    if (
      state.scrollY > state.slideMinScroll &&
      state.scrollY + delta < state.slideMinScroll
    ) {
      res.pos = state.slideMinScroll - state.scrollY
      res.isChange = true
    } else if (
      state.scrollY < state.slideMaxScroll &&
      state.scrollY + delta > state.slideMaxScroll
    ) {
      res.pos = state.slideMaxScroll - state.scrollY
      res.isChange = true
    }

    return res
  }

  getCurCenterPos() {
    if (!this.centerPosition) {
      this.centerPosition = getViewportCenterMMPos(this.editorUI)
    }
    return this.centerPosition
  }

  updateCurCenterPos() {
    this.centerPosition = getViewportCenterMMPos(this.editorUI)
  }

  resetCurCenterPos() {
    this.centerPosition = undefined
  }
}

function genSettingsFromScrolls(component: ScrollComponent) {
  const editorUI = component.editorUI
  const settings = new ScrollBarSettings()
  settings.screenWidth = editorUI.editorView.getWidth()
  settings.screenHeight = editorUI.editorView.getHeight()
  settings.vscrollDistPerTime = 45
  settings.hscrollDistPerTime = 45
  settings.contentHeight = component.state.contentHeight
  settings.contentWidth = component.state.contentWidth
  settings.bgColor = EditorSkin.Background_Light
  settings.screenWidth = shrinkByRetinaRatio(settings.screenWidth)
  settings.screenHeight = shrinkByRetinaRatio(settings.screenHeight)
  settings.extraHeight = shrinkByRetinaRatio(settings.extraHeight)
  return settings
}
class ScrollState {
  component: ScrollComponent
  scrollY: number = 0
  scrollX: number = 0
  scrollYMax: number = 1
  scrollYmax: number = 1
  scrollCentralX: number = 0
  scrollCentralY: number = 0
  contentWidth: number = 0
  contentHeight: number = 0
  slideMinScroll: number = 0
  slideMaxScroll: number = 0
  constructor(scrolls: ScrollComponent) {
    this.component = scrolls
  }

  getScrollYRatio() {
    return this.scrollYMax !== 0 ? this.scrollY / this.scrollYMax : 0
  }

  getScrollXRatio() {
    return this.scrollYmax !== 0 ? this.scrollX / this.scrollYmax : 0
  }

  isValid() {
    return this.contentWidth !== 0 && this.contentHeight !== 0
  }

  onUpdateScrolls() {
    this.scrollYmax = this.component.canScrollHor
      ? this.component.hScrollBar!.getMaxScrollX()
      : 0
    this.scrollYMax = this.component.vScrollBar!.getMaxScrollY()

    if (this.scrollX >= this.scrollYmax) {
      this.scrollX = this.scrollYmax
    }
    if (this.scrollY >= this.scrollYMax) {
      this.scrollY = this.scrollYMax
    }
  }

  onUpdateDocumentSize() {
    const stateInfo = getDocumentScollStateInfo(this.component.editorUI)
    const contentWidth =
      stateInfo.horizontalRight - stateInfo.horizontalLeft + 1
    let contentHeight = stateInfo.verticalBottom - stateInfo.verticalTop + 1
    contentHeight = Math.max(contentHeight, stateInfo.canvasHeight)

    this.contentWidth = contentWidth
    this.contentHeight = contentHeight

    this.slideMinScroll = 0
    this.slideMaxScroll =
      this.slideMinScroll + contentHeight - stateInfo.canvasHeight
  }

  calcSlideRenderInfo(slideCurrentRect: SlideRect) {
    const component = this.component
    const {
      centerX,
      centerXOfSlide,
      horizontalLeft,
      centerY,
      centerYOfSlide,
      verticalTop,
      slideWidth,
      slideHeight,
    } = getDocumentScollStateInfo(component.editorUI)
    if (this.scrollY <= this.slideMinScroll) {
      this.scrollY = this.slideMinScroll
    }
    if (this.scrollY >= this.slideMaxScroll) {
      this.scrollY = this.slideMaxScroll
    }

    const x = -this.scrollX + centerX - centerXOfSlide - horizontalLeft
    const y =
      -(this.scrollY - this.slideMinScroll) +
      centerY -
      centerYOfSlide -
      verticalTop

    const xc = centerX - centerXOfSlide
    const yc = centerY - centerYOfSlide
    this.scrollCentralX = centerX - centerXOfSlide - horizontalLeft - xc
    this.scrollCentralY =
      this.slideMinScroll + centerY - centerYOfSlide - verticalTop - yc

    slideCurrentRect.left = x
    slideCurrentRect.top = y
    slideCurrentRect.right = x + slideWidth
    slideCurrentRect.bottom = y + slideHeight
  }
}

function getDocumentScollStateInfo(editorUI: EditorUI): EditorDemensionInfo {
  const zoom = ZoomValue.value
  const bounds = editorUI.slideRenderer.getBounds()

  return calculateDimensionInfo(editorUI, zoom, bounds)
}

function getViewportCenterMMPos(editorUI: EditorUI) {
  const curSlideRect = editorUI.renderingController.curSlideRect
  const pixEditorWidth = editorUI.editorView.getPxWidth()
  const pixEditorHeight = editorUI.editorView.getPxHeight()
  const { x: editorOffL, y: editorOffY } = editorUI
  const { L, T } = editorUI.mainContainer.PositionInPx
  const [mainOffL, mainOffT] = [L * Factor_mm_to_pix, T * Factor_mm_to_pix]

  const curCenterX = (mainOffL + ((pixEditorWidth / 2) >> 0)) >> 0
  const curCenterY = (mainOffT + ((pixEditorHeight / 2) >> 0)) >> 0
  const relativePixX = (curCenterX + editorOffL) >> 0
  const relativePixY = (curCenterY + editorOffY) >> 0
  const { x, y } = convertPixToInnerMMPos(editorUI, relativePixX, relativePixY)
  return { xInMM: x, yInMM: y, ...curSlideRect }
}
