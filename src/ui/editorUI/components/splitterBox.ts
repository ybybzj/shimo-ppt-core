import { EditorUI } from '../EditorUI'
import { stopEvent } from '../../../common/dom/events'
import { toInt } from '../../../common/utils'
import { Factor_mm_to_pix } from '../../../core/common/const/unit'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { EditorUtil } from '../../../globals/editor'
import { splitterStates } from '../splitterStates'
import { DOM_ID, SplitterBoxHeight, SplitterBoxWidth } from '../controls/const'
import { BrowserInfo } from '../../../common/browserInfo'

export class SplitterBox {
  editorUI: EditorUI
  lockedDisplay = false

  get dom() {
    return this.editorUI.editorControl.elements[DOM_ID.SplitterBox]
  }

  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI
  }

  onMouseDown(_e: PointerEvent) {
    const editorUI = this.editorUI
    if (false === editorUI.editorKit.isEditorUIInitComplete) return
    if (EditorUtil.inputManager?.checkAndClearIME()) return
    editorUI.splitters.toggleThumbnailDisplay()
    stopEvent(_e)
  }

  onMouseMove(_e: PointerEvent) {
    stopEvent(_e)
  }
  onMouseUp(_e: PointerEvent) {
    stopEvent(_e)
  }

  onResize() {
    if (!BrowserInfo.isTablet) {
      if (this.lockedDisplay) {
        this.dom.style.display = 'none'
        this.lockedDisplay = false
      }
      return
    }
    if (!this.lockedDisplay) {
      this.dom.style.display = 'block'
      this.lockedDisplay = true
    }

    const { thumbnailSplitterPos } = splitterStates
    const thumbnailOffsetX = thumbnailSplitterPos * Factor_mm_to_pix
    const boxOffsetX = -SplitterBoxWidth / 2
    const offsetX = toInt(Math.max(0, thumbnailOffsetX + boxOffsetX))
    if (
      (offsetX === 0 && !EditorSettings.isRTL) ||
      (0 < offsetX && EditorSettings.isRTL)
    ) {
      this.dom.style.transform = 'rotate(180deg)'
    } else {
      this.dom.style.transform = ''
    }
    if (EditorSettings.isRTL) {
      this.dom.style.right = offsetX + 'px'
    } else {
      this.dom.style.left = offsetX + 'px'
    }
    const offsetY =
      this.editorUI.thumbnailsManager.control.getPxHeight() / 2 -
      SplitterBoxHeight / 2
    this.dom.style.top = offsetY + 'px'
  }
}
