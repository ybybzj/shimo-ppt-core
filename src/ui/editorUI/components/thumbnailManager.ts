import { EditorSkin, FocusTarget } from '../../../core/common/const/drawing'
import {
  GlobalEvents,
  listenPointerEvents,
  PointerEventArg,
  PointerEventInfo,
  PointerEventType,
  POINTER_BUTTON,
} from '../../../common/dom/events'
import { ScrollBarSettings, ScrollBar } from '../scroll'
import { getCanvasDrawer } from '../../../core/graphic/CanvasDrawer'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { Dict, Nullable } from '../../../../liber/pervasive'
import { BrowserInfo } from '../../../common/browserInfo'
import { debounce, limitInvoke, toInt } from '../../../common/utils'
import {
  Factor_mm_to_pix,
  Factor_pix_to_mm,
} from '../../../core/common/const/unit'
import { CacheImage, ImagePoolManager } from '../../../core/graphic/imageCache'
import { EditorUI } from '../EditorUI'
import {
  Evt_onThumbnailSlideDoubleClick,
  Evt_toggleCollaboratorStatus,
} from '../../../editor/events/EventNames'
import { EditorKit } from '../../../editor/global'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { DomWheelEvt } from '../../../common/dom/wheelEvent'
import { splitterStates, SCROLLBAR_WIDTH_PX } from '../splitterStates'
import { Dom } from '../../../common/dom/vdom'
import {
  UIControl,
  UIControlContainer,
  ControlCreaterMakers,
} from '../Controls'
import {
  isSlideHasAnimation,
  isSlideHasTransition,
} from '../../../core/utilities/slide'
import { EditorUtil } from '../../../globals/editor'
import { translator } from '../../../globals/translator'
import {
  SlideTransitionImage,
  SlideHiddenIconImage,
  CollaStatusImage2x,
  CollaStatusImage,
  SlideCommentImage,
  SlideAnimationImage,
} from '../../../images/images'
import { FontSimpleInfo } from '../../../fonts/FontKit'
import { syncInterfaceState } from '../../../editor/interfaceState/syncInterfaceState'
import { CSS_CLASS, DOM_ID, DOM_STYLE } from '../controls/const'
import { getCtrlParamsById } from '../controls/rtl'
import { renderPresentationSlide } from '../../../core/render/slide'
import { isCurAddingComment } from '../../states/utils'
import { calculateForRendering } from '../../../core/calculation/calculate/calculateForRendering'
import { startEditAction } from '../../../core/calculation/informChanges/startAction'
import { endAddComment, startAddComment } from '../../features/comment/utils'
import { keyCodeMap } from '../utils/keys'
import {
  AnimateScrollType,
  DRAG_MIN_DELTA,
  emitThumbnailContextMenu,
  handleThumbnailTouchEnd,
  handleThumbnailTouchMove,
  handleThumbnailTouchStart,
  resetThumbnailUIState,
} from '../utils/thumbnailTouches'
import { calculateRenderingStateForSlides } from '../../../core/calculation/calculate/presentation'
import { ContextMenuTypes } from '../../../core/properties/props'

type ClientStatus = {
  clientId: string
}
export type CollaboratorStatus = {
  [SlideRefId: string]: ClientStatus[]
}

const { C, CC } = ControlCreaterMakers

const ContainerCtrlSettings = {
  name: DOM_ID.ThumbnailContainer.Id,
  node: Dom.div(
    `#${DOM_ID.ThumbnailContainer.Id}.${CSS_CLASS.Block}`,
    { style: DOM_STYLE.ThumbnailContainer.Style },
    [
      Dom.div(`#${DOM_ID.ThumbnailContainer.Splitter}.${CSS_CLASS.Block}`, {
        style: DOM_STYLE.ThumbnailContainer.Splitter,
      }),
      Dom.placeholder(DOM_ID.ThumbnailContainer.BackGround),
      Dom.placeholder(DOM_ID.ThumbnailContainer.Thumbnail),
      Dom.placeholder(DOM_ID.ThumbnailContainer.Scroll.Id),
    ]
  ),
} as const

/** @param bounds [l,t,r,b,isAbsL,isAbsT,isAbsR,isAbsB,absW,absH */
const ContainerCtrl = CC<
  {
    [DOM_ID.ThumbnailContainer.Id]: HTMLDivElement
    [DOM_ID.ThumbnailContainer.Splitter]: HTMLDivElement
  },
  [
    typeof DOM_ID.ThumbnailContainer.BackGround,
    typeof DOM_ID.ThumbnailContainer.Thumbnail,
    typeof DOM_ID.ThumbnailContainer.Scroll.Id,
  ],
  typeof ContainerCtrlSettings
>(ContainerCtrlSettings)

const BackgroundCtrlSettings = {
  name: DOM_ID.ThumbnailContainer.BackGround,
  node: Dom.canvas(
    `#${DOM_ID.ThumbnailContainer.BackGround}.${CSS_CLASS.Block}`,
    {
      style: DOM_STYLE.ThumbnailContainer.BackGround,
    }
  ),
} as const

const BackgroundCtrl = C<
  { [DOM_ID.ThumbnailContainer.BackGround]: HTMLCanvasElement },
  typeof BackgroundCtrlSettings
>(BackgroundCtrlSettings)

const ThumbnailCtrlSettings = {
  name: DOM_ID.ThumbnailContainer.Thumbnail,
  node: Dom.canvas(
    `#${DOM_ID.ThumbnailContainer.Thumbnail}.${CSS_CLASS.Block}`,
    { style: DOM_STYLE.ThumbnailContainer.Thumbnail }
  ),
} as const

const ThumbnailCtrl = C<
  {
    [DOM_ID.ThumbnailContainer.Thumbnail]: HTMLCanvasElement
  },
  typeof ThumbnailCtrlSettings
>(ThumbnailCtrlSettings)

const ThumbnailScrollerCtrlSettings = {
  name: DOM_ID.ThumbnailContainer.Scroll.Id,
  node: Dom.div(
    `#${DOM_ID.ThumbnailContainer.Scroll.Id}`,
    { style: DOM_STYLE.ThumbnailContainer.Scroll.Style },
    [
      Dom.div(
        `#${DOM_ID.ThumbnailContainer.Scroll.Container.Id}.${CSS_CLASS.Block}`,
        { style: DOM_STYLE.ThumbnailContainer.Scroll.Container.Style }
      ),
    ]
  ),
} as const

const ThumbnailScrollerCtrl = C<
  {
    [DOM_ID.ThumbnailContainer.Scroll.Id]: HTMLDivElement
    [DOM_ID.ThumbnailContainer.Scroll.Container.Id]: HTMLDivElement
  },
  typeof ThumbnailScrollerCtrlSettings
>(ThumbnailScrollerCtrlSettings)

const ThumbnailDefaultFont: FontSimpleInfo = {
  fontFamily: { name: 'Arial', index: -1 },
  italic: false,
  bold: false,
  fontSize: 10.5,
}

/** mm size */
const ThumbnailIconMMSize = 5
const COLOR_SELECT = '#5BA0E7'
const COLOR_HOVER = '#ADCFF3'
const COLOR_HOVER_SELECT = '#5BA0E7'
const COLOR_DRAG = '#DEDEDE'
const LINE_WIDTH_DRAG = 3
const BORDER_WIDTH = 2
const SHADOW_WIDTH = 1
const TH_CACHE_NUM = BrowserInfo.isMobile ? 10 : 50

export class ThumbnailsManager {
  editorUI: EditorUI
  isScrollVisible = true
  /** scroll 触发 reRender */
  needRerender = false
  /** 记录每个数字的宽度 */
  private widthOfDigits: number[] = []
  scrollY = 0
  scrollYMax = 0
  private isVisible = false
  needUpdate = false
  /** thumbnail RTL 布局相关的开关，可独立全局 RTL 单独设置 */
  isRTL: boolean
  slideThumbnails: SlideThumbnail[] = []
  /** 可视区内需要绘制 slide 的起始 index */
  private startIndex = -1
  /** 可视区内需要绘制 slide 的截止 index */
  private endIndex = -1
  offsetPixL: number
  offsetPixT: number
  offsetPixR: number
  offsetPixB: number
  private borderWidth: number
  isEmptyDrawed = false
  cacheManager: ImagePoolManager
  isDraging = false
  isSimpleDrag = false
  dragStartIndex = -1
  dragEndIndex = -1
  dragStartX = -1
  dragStartY = -1
  private dragIconElement: Nullable<HTMLCanvasElement>
  /** 拖拽到顶部和底部时需要滚动跟随 */
  private scrollTopTimer: any = -1
  private scrollBottomTimer: any = -1
  private needUpdateScrollerIndex: Nullable<number> = null
  private forceUpdate = false // change
  private collabStatus: CollaboratorStatus = {}
  private collabStatusState = {
    isMouseInCollabStatus: false,
    slideIndex: -1,
    width: 0,
  }
  pointerEvtInfo: PointerEventInfo = new PointerEventInfo()
  scrollPointerEvtInfo: PointerEventInfo = new PointerEventInfo()

  isMultiSelectState: Boolean | 'pending' = false
  scrollBar?: ScrollBar
  control!: UIControlContainer<
    typeof DOM_ID.ThumbnailContainer.Id,
    HTMLDivElement,
    {
      [DOM_ID.ThumbnailContainer.Id]: HTMLDivElement
      [DOM_ID.ThumbnailContainer.Splitter]: HTMLDivElement
    },
    [
      typeof DOM_ID.ThumbnailContainer.BackGround,
      typeof DOM_ID.ThumbnailContainer.Thumbnail,
      typeof DOM_ID.ThumbnailContainer.Scroll.Id,
    ]
  >
  private thumbnailsBackground!: UIControl<
    typeof DOM_ID.ThumbnailContainer.BackGround,
    HTMLCanvasElement,
    { [DOM_ID.ThumbnailContainer.BackGround]: HTMLCanvasElement }
  >
  thumbnails!: UIControl<
    typeof DOM_ID.ThumbnailContainer.Thumbnail,
    HTMLCanvasElement,
    { [DOM_ID.ThumbnailContainer.Thumbnail]: HTMLCanvasElement }
  >
  private thumbnailsScroll!: UIControl<
    typeof DOM_ID.ThumbnailContainer.Scroll.Id,
    HTMLDivElement,
    {
      [DOM_ID.ThumbnailContainer.Scroll.Id]: HTMLDivElement
      [DOM_ID.ThumbnailContainer.Scroll.Container.Id]: HTMLDivElement
    }
  >
  private _unlisten: (() => void) | undefined
  get presentation() {
    return this.editorUI.editorKit.presentation
  }
  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI
    this.isRTL = EditorSettings.isRTL

    this.offsetPixL = this.isRTL ? 8 : 0
    this.offsetPixT = 0
    this.offsetPixR = this.isRTL ? 0 : 4
    this.offsetPixB = 0
    this.borderWidth = 4

    this.cacheManager = new ImagePoolManager()
    this.dragIconElement = null
    this.init()
  }
  init() {
    const editorUI = this.editorUI
    this.control = ContainerCtrl(
      ...getCtrlParamsById(ContainerCtrlSettings.name, editorUI)!
    )
    this.thumbnailsBackground = BackgroundCtrl(
      ...getCtrlParamsById(BackgroundCtrlSettings.name, editorUI)!
    )
    this.thumbnails = ThumbnailCtrl(
      ...getCtrlParamsById(ThumbnailCtrlSettings.name, editorUI)!
    )
    this.thumbnailsScroll = ThumbnailScrollerCtrl(
      ...getCtrlParamsById(ThumbnailScrollerCtrlSettings.name, editorUI)!
    )

    this.control.add(this.thumbnailsBackground)
    this.control.add(this.thumbnails)
    this.control.add(this.thumbnailsScroll)

    if (EditorSettings.isMobile) {
      this.thumbnailsScroll.element.style.display = 'none'
    }
  }

  handlePointerEvent<T extends PointerEventType>(et: T, e: PointerEventArg<T>) {
    if (et === 'pointerdown') {
      this.pointerEvtInfo.lockPointerType(e as PointerEvent)
    }
    const isTouchMode = this.pointerEvtInfo.isTouchMode()
    switch (et) {
      case 'pointerdown': {
        if (isTouchMode) {
          handleThumbnailTouchStart(this, e as PointerEvent)
        } else {
          this.onPointerDown(e as PointerEvent)
        }
        break
      }
      case 'pointermove': {
        if (isTouchMode) {
          handleThumbnailTouchMove(this, e as PointerEvent)
        } else {
          this.onPointerMove(e as PointerEvent)
        }
        break
      }
      case 'pointerup': {
        if (isTouchMode) {
          handleThumbnailTouchEnd(this, e as PointerEvent)
        } else {
          this.onPointerUp(e as PointerEvent)
        }
        this.pointerEvtInfo.unLockPointerType()
        break
      }
      case 'pointerout': {
        this.onPointerOut(e as PointerEvent)
        break
      }
      case 'wheel': {
        this.onWheel(e as DomWheelEvt)
        break
      }
    }
  }
  initEvents() {
    const control = this.thumbnails.element
    const evts: PointerEventType[] = [
      'pointerdown',
      'pointermove',
      'pointerup',
      'pointerout',
      'wheel',
    ]
    this._unlisten = listenPointerEvents(control, evts, this)
    // this.initMobileEvents()
  }

  removeEvents() {
    if (this._unlisten) {
      this._unlisten()
    }

    if (this.scrollBar) {
      this.scrollBar.removeEvents()
    }
  }

  onMouseDownOutside() {
    if (this.control && this.control.element) {
      this.control.element.style.pointerEvents = 'none'
    }
  }

  onMouseUpOutside() {
    if (this.control && this.control.element) {
      this.control.element.style.pointerEvents = ''
    }
  }

  onDragSplitter() {
    if (!EditorSettings.isEnableThumbnail && !EditorSettings.isShowThumbnail) {
      return
    }
    if (this.isRTL) {
      this.control.bounds.R = 0
    } else {
      this.control.bounds.R = splitterStates.thumbnailSplitterPos
    }

    this.control.bounds.witdh = splitterStates.thumbnailSplitterPos

    if (0 !== splitterStates.thumbnailSplitterPos) {
      this.showControl()
    } else {
      this.hideControl()
    }
  }

  onUpdateDocumentSize() {
    this.updateSizes()
  }
  showControl() {
    this.control.element.style.display = 'block'
    this.editorUI.editorKit.emitThumbnailsShow()
  }
  hideControl() {
    this.control.element.style.display = 'none'
    this.editorUI.editorKit.emitThumbnailsShow()
  }

  getSlidePosInfoByIndex = (index) => {
    if (index < 0 || index >= this.slideThumbnails.length) return null

    const slideThumbnail = this.slideThumbnails[index]
    const pos = {
      x: this.editorUI.x + slideThumbnail.left,
      y: this.editorUI.x + slideThumbnail.top,
      w: slideThumbnail.right - slideThumbnail.left + 1,
      h: slideThumbnail.bottom - slideThumbnail.top + 1,
    }
    return pos
  }

  /** 根据 mousePos */
  getMMPosAndSlidePosByMousePos(x: number, y: number) {
    const pos = {
      x: x,
      y: y,
      slideIndex: -1,
      isMouseInCollabStatus: false,
    }
    pos.x -= this.editorUI.x
    pos.y -= this.editorUI.y

    const countOfSlides = this.slideThumbnails.length
    const thumbnailContainerW = this.control.bounds.witdh * Factor_mm_to_pix
    for (let i = 0; i < countOfSlides; i++) {
      const drawRect = this.slideThumbnails[i]

      if (pos.y >= drawRect.top && pos.y <= drawRect.bottom) {
        pos.slideIndex = i
        const { refId } = this.presentation.slides[pos.slideIndex]
        const status = refId && this.collabStatus[refId]
        const { height } = this.getCollabStatusSize(true)
        const width = this.collabStatusState.width / BrowserInfo.PixelRatio
        const { top, right, left } = drawRect
        // 相对 Thumbnail 内部的 pos(后续若支持隐藏 Scroller 需要适配, Todo 收束 RTL 逻辑)
        let relativePosX = pos.x
        if (this.isRTL) {
          relativePosX -=
            this.editorUI.width - thumbnailContainerW + SCROLLBAR_WIDTH_PX
        }

        const hasCoActors = status && status.length
        const isPosXInner = this.isRTL
          ? relativePosX >= right - width && relativePosX <= right
          : relativePosX >= left && relativePosX <= left + width

        const isPosYInner = pos.y >= top && pos.y <= top + height
        if (hasCoActors && isPosXInner && isPosYInner) {
          pos.isMouseInCollabStatus = true
        }
        break
      }
    }

    return pos
  }

  /** 获取当前 x,y 所处的 thumbnail index */
  getSlideIndexMousePos = (x, y) => {
    if (this.isRTL) {
      const mainContainerBounds = this.editorUI.mainContainer.PositionInPx
      x -= (mainContainerBounds.R - mainContainerBounds.L) * Factor_mm_to_pix
    }
    const pos = { x: x, y: y }
    pos.x -= this.editorUI.x
    pos.y -= this.editorUI.y

    const { L, T, R, B } = this.thumbnails.PositionInPx
    const borderR = (R - L) * Factor_mm_to_pix
    const borderT = (B - T) * Factor_mm_to_pix

    if (pos.x < 0 || pos.x > borderR || pos.y < 0 || pos.y > borderT) {
      return -1
    }

    const countOfSlides = this.slideThumbnails.length
    if (0 === countOfSlides) return -1

    let min = Math.abs(pos.y - this.slideThumbnails[0].top)
    let retIndex = 0

    for (let i = 0; i < countOfSlides; i++) {
      const minT = Math.abs(pos.y - this.slideThumbnails[i].top)
      const minB = Math.abs(pos.y - this.slideThumbnails[i].bottom)

      if (minT < min) {
        min = minT
        retIndex = i
      }
      if (minB < min) {
        min = minB
        retIndex = i + 1
      }
    }

    return retIndex
  }

  onPointerDown(e) {
    if (EditorUtil.inputManager?.checkAndClearIME()) return

    const control = this.thumbnails.element
    const pointerEvt = this.pointerEvtInfo
    if (pointerEvt.isLocked === true && pointerEvt.target !== control) {
      return false
    }

    this.pointerEvtInfo.onDown(e, true)
    if (pointerEvt.button == null) {
      pointerEvt.button = 0
    }

    this.onFocus()

    const { x, y, button: mouseButton } = pointerEvt
    const pos = this.getMMPosAndSlidePosByMousePos(x, y)
    const posIndex = pos.slideIndex
    const { editorUI } = this

    if (posIndex === -1) {
      if (mouseButton === 2) {
        emitThumbnailContextMenu(this, { x, y })
      }
      return false
    }

    if (this.pointerEvtInfo.ctrlKey) {
      if (this.slideThumbnails[posIndex].selected === true) {
        this.slideThumbnails[posIndex].selected = false
        const arr = this.getSelectedSlideIndices()
        if (0 === arr.length) {
          this.slideThumbnails[posIndex].selected = true
          this.goToSlide(posIndex)
        } else {
          this.updateEditLayer()
          editorUI.goToSlide(arr[0], undefined, true)
          this.goToSlide(arr[0])
        }
      } else {
        this.slideThumbnails[posIndex].selected = true
        this.updateEditLayer()
        editorUI.goToSlide(posIndex, undefined, true)
        this.goToSlide(posIndex)
      }
    } else if (this.pointerEvtInfo.shiftKey) {
      this.resetSelection()

      let _max = posIndex
      let _min = editorUI.renderingController.curSlideIndex
      if (_min > _max) {
        const _temp = _max
        _max = _min
        _min = _temp
      }

      for (let i = _min; i <= _max; i++) {
        this.slideThumbnails[i].selected = true
      }

      this.updateEditLayer()
      this.goToSlide(posIndex)
      this.syncSelectedSlideIndexes()
      syncInterfaceState(editorUI.editorKit)
    } else if (0 === mouseButton || 2 === mouseButton) {
      if (
        POINTER_BUTTON.Left === mouseButton &&
        this.pointerEvtInfo.clicks >= 2
      ) {
        EditorKit.emitEvent(Evt_onThumbnailSlideDoubleClick, posIndex)
        return false
      }

      // getting ready for the track
      if (POINTER_BUTTON.Left === mouseButton) {
        this.isDraging = true
        this.isSimpleDrag = true
        this.dragStartIndex = posIndex
        this.dragStartX = x
        this.dragStartY = y
      }

      const isClickOnMultiSelectedSlide =
        this.getSelectedSlideIndices().indexOf(posIndex) >= 0

      if (this.slideThumbnails[posIndex].selected) {
        if (!(mouseButton === 2 && isClickOnMultiSelectedSlide)) {
          editorUI.goToSlide(posIndex, undefined, true)
        }

        if (mouseButton === 2 && !this.pointerEvtInfo.ctrlKey) {
          emitThumbnailContextMenu(this, { x, y })
        }

        return false
      }

      this.resetSelection()
      this.slideThumbnails[posIndex].selected = true
      this.updateEditLayer()
      editorUI.goToSlide(posIndex, undefined, true)
      this.goToSlide(posIndex)
    }

    if (mouseButton === 2 && !this.pointerEvtInfo.ctrlKey) {
      emitThumbnailContextMenu(this, { x, y })
    }

    this.syncSelectedSlideIndexes()
    this.render()

    return false
  }

  goToSlide = (slideIndex: number) => {
    const top = this.slideThumbnails[slideIndex].top - this.borderWidth
    const bottom = this.slideThumbnails[slideIndex].bottom + this.borderWidth

    if (top < 0) {
      const height = bottom - top
      const targetY = slideIndex * height + (slideIndex + 1) * this.borderWidth
      this.scrollBar!.scrollToY(targetY)
    } else {
      const element = this.thumbnails.element
      const height = toInt(element.style.height) || element.height
      if (bottom > height) {
        this.scrollBar!.scrollByDeltaY(bottom - height)
      }
    }
  }

  selectSlides = (slideIndices: number | number[], ignoreScroll = false) => {
    EditorKit.emitEvent(Evt_toggleCollaboratorStatus, { show: false })

    const countOfSlides = this.slideThumbnails.length
    slideIndices = Array.isArray(slideIndices) ? slideIndices : [slideIndices]
    let needUpdate = false

    for (let i = 0; i < countOfSlides; i++) {
      const thumb = this.slideThumbnails[i]
      if (slideIndices.includes(i) && thumb.selected === false) {
        thumb.selected = true
        needUpdate = true
      } else if (!slideIndices.includes(i) && thumb.selected === true) {
        thumb.selected = false
        needUpdate = true
      }
    }

    // 跳转到最后选中的缩略图上
    if (ignoreScroll !== true && needUpdate) {
      for (let i = slideIndices.length - 1; i >= 0; i--) {
        if (0 <= slideIndices[i] && slideIndices[i] < countOfSlides) {
          this.needUpdateScrollerIndex = slideIndices[i]
        }
      }
    }

    this.syncSelectedSlideIndexes()
    this.needUpdate = needUpdate
  }

  private checkScrollByIndex() {
    if (this.needUpdateScrollerIndex != null && this.scrollBar != null) {
      // this.editorUI.goToSlide(this.needUpdateScrollerIndex)
      this.goToSlide(this.needUpdateScrollerIndex)
      this.needUpdateScrollerIndex = null
    }
  }

  clearCacheForce = () => {
    const countOfSlides = this.slideThumbnails.length
    for (let i = 0; i < countOfSlides; i++) {
      this.updateSlideThumbnailDirty(i)
    }
    this.needUpdate = true
  }

  calculateState = () => {
    this.onUpdateDocumentSize()
    this.clearCacheForce()
  }

  onPointerMove(e) {
    const control = this.thumbnails.element
    const mouseEvent = this.pointerEvtInfo
    if (mouseEvent.isLocked === true && mouseEvent.target !== control) {
      return
    }
    this.pointerEvtInfo.onMove(e)
    if (mouseEvent.target !== control) {
      return
    }

    if (this.isDraging) {
      // this is a slide track
      if (this.isSimpleDrag && !EditorSettings.isViewMode) {
        if (
          Math.abs(this.dragStartX - mouseEvent.x) > DRAG_MIN_DELTA ||
          Math.abs(this.dragStartY - mouseEvent.y) > DRAG_MIN_DELTA
        ) {
          this.isSimpleDrag = false
        }
      }

      if (!this.isSimpleDrag) {
        // you need to determine the active position between slides
        this.dragEndIndex = this.getSlideIndexMousePos(
          mouseEvent.x,
          mouseEvent.y
        )
      }

      this.updateEditLayer()

      // now you need to see if you need to scroll
      if (this.isScrollVisible) {
        const innerY = mouseEvent.y - this.editorUI.y
        const absRect = this.thumbnails.PositionInPx
        const maxY = (absRect.B - absRect.T) * Factor_mm_to_pix

        let scrollType = AnimateScrollType.None
        if (/* _Y >= 0 && */ innerY < 30) {
          scrollType = AnimateScrollType.Top
        } else if (innerY >= maxY - 30 /* && _Y < _YMax*/) {
          scrollType = AnimateScrollType.Bottom
        }

        this.checkNeedAnimateScrollsByType(scrollType)
      }

      if (!this.isSimpleDrag) {
        let dragingCursor = 'default'
        if (BrowserInfo.webkit) dragingCursor = '-webkit-grabbing'
        else if (BrowserInfo.mozilla) {
          dragingCursor = '-moz-grabbing'
        }

        this.thumbnails.element.style.cursor = dragingCursor
      }

      return
    }

    const pos = this.getMMPosAndSlidePosByMousePos(mouseEvent.x, mouseEvent.y)
    let needUpdateEditLayer = false
    if (
      pos.isMouseInCollabStatus !==
        this.collabStatusState.isMouseInCollabStatus ||
      (pos.isMouseInCollabStatus &&
        pos.slideIndex !== this.collabStatusState.slideIndex)
    ) {
      this.collabStatusState.isMouseInCollabStatus = pos.isMouseInCollabStatus
      this.collabStatusState.slideIndex = pos.slideIndex
      const thumbnail = this.slideThumbnails[pos.slideIndex]
      if (thumbnail) {
        const thumbnailContainerW = this.control.bounds.witdh * Factor_mm_to_pix
        EditorKit.emitEvent(Evt_toggleCollaboratorStatus, {
          show: this.collabStatusState.isMouseInCollabStatus,
          page: pos.slideIndex,
          left: thumbnail.left,
          right: thumbnailContainerW - (thumbnail.right + SCROLLBAR_WIDTH_PX),
          top: thumbnail.top + this.getCollabStatusSize(true).height,
        })
      } else {
        EditorKit.emitEvent(Evt_toggleCollaboratorStatus, {
          show: false,
        })
      }
      this.render() // hover到协作信息上更新深色背景
      needUpdateEditLayer = true
    }

    const countOfSlides = this.slideThumbnails.length
    for (let i = 0; i < countOfSlides; i++) {
      if (this.slideThumbnails[i].focused) {
        needUpdateEditLayer = true
        this.slideThumbnails[i].focused = false
      }
    }

    let cursor = 'default'

    if (pos.slideIndex !== -1) {
      this.slideThumbnails[pos.slideIndex].focused = true
      needUpdateEditLayer = true
      cursor = 'pointer'
    }
    if (needUpdateEditLayer) {
      this.updateEditLayer()
    }

    this.thumbnails.element.style.cursor = cursor
  }

  checkNeedAnimateScrollsByType = (type: AnimateScrollType) => {
    if (type === AnimateScrollType.None) {
      if (this.scrollTopTimer !== -1) {
        clearInterval(this.scrollTopTimer)
        this.scrollTopTimer = -1
      }

      if (this.scrollBottomTimer !== -1) {
        clearInterval(this.scrollBottomTimer)
        this.scrollBottomTimer = -1
      }
    }

    if (type === AnimateScrollType.Top) {
      if (this.scrollBottomTimer !== -1) {
        clearInterval(this.scrollBottomTimer)
        this.scrollBottomTimer = -1
      }

      if (-1 === this.scrollTopTimer) {
        this.scrollTopTimer = setInterval(this.onScrollTop, 50)
      }
      return
    }
    if (type === AnimateScrollType.Bottom) {
      if (this.scrollTopTimer !== -1) {
        clearInterval(this.scrollTopTimer)
        this.scrollTopTimer = -1
      }

      if (-1 === this.scrollBottomTimer) {
        this.scrollBottomTimer = setInterval(this.onScrollBottom, 50)
      }
      return
    }
  }

  onScrollTop = () => {
    this.scrollBar!.scrollByDeltaY(-45)
  }

  onScrollBottom = () => {
    this.scrollBar!.scrollByDeltaY(45)
  }

  onPointerUp(e) {
    const oldEventTarget = this.pointerEvtInfo.target

    if (this.pointerEvtInfo.isLocked) {
      // 同步 editorUi.checkMobileInput, 但不需要检查输入
      if (!BrowserInfo.isTablet) {
        this.editorUI.focusInput()
      } else {
        // 缩略图区域目前并不涉及输入, 暂时不需要考虑外接键盘场景?
        this.editorUI.blurInput()
        this.editorUI.focusEditor()
      }
    }

    this.pointerEvtInfo.onUp(e)

    const control = this.thumbnails.element
    if (this.pointerEvtInfo.target !== control) {
      if (oldEventTarget !== control) {
        return
      }
    }

    this.checkNeedAnimateScrollsByType(AnimateScrollType.None)

    if (!this.isDraging) return

    // now let's see if this is just a select, or is it a track
    if (this.isSimpleDrag) {
      if (
        Math.abs(this.dragStartX - this.pointerEvtInfo.x) > 10 ||
        Math.abs(this.dragStartY - this.pointerEvtInfo.y) > 10
      ) {
        this.isSimpleDrag = false
      }
    }

    if (this.isSimpleDrag) {
      // it's just a select
      this.resetSelection()
      this.slideThumbnails[this.dragStartIndex].selected = true
      this.render()
      const isClickOnMultiSelectedSlide =
        this.getSelectedSlideIndices().indexOf(this.dragStartIndex) >= 0
      if (this.pointerEvtInfo.button === 0 && isClickOnMultiSelectedSlide) {
        this.editorUI.goToSlide(this.dragStartIndex, undefined, true)
        this.updateEditLayer()
      }
    } else {
      // this is a track
      this.dragEndIndex = this.getSlideIndexMousePos(
        this.pointerEvtInfo.x,
        this.pointerEvtInfo.y
      )

      if (-1 !== this.dragEndIndex) {
        const indices = this.getSelectedSlideIndices()
        const newSelectedSlidesPosArr = this.presentation.reorderSlides(
          this.dragEndIndex,
          indices
        )
        if (newSelectedSlidesPosArr && newSelectedSlidesPosArr.length) {
          this.selectAll(newSelectedSlidesPosArr)
        }
        this.clearCacheForce()
      }

      this.updateEditLayer()
    }
    resetThumbnailUIState(this)
    this.syncSelectedSlideIndexes()
    this.onPointerMove(e)
  }

  onPointerOut(_e) {
    this.slideThumbnails.forEach((thumb) => (thumb.focused = false))
    this.updateEditLayer()
  }

  onWheel({ eventObj: e }: DomWheelEvt) {
    if (false === this.isScrollVisible || !this.scrollBar) {
      return
    }

    if (GlobalEvents.keyboard.ctrlKey) return
    EditorKit.emitEvent(Evt_toggleCollaboratorStatus, {
      show: false,
    })

    const delta = (e.deltaY * 5) >> 0
    this.scrollBar.scrollBy(0, delta)
    e.preventDefault()
    return false
  }

  /** 根据全局样式设置对应样式, 测量(字体)页标数字宽度, 初始化拖拽图标 */
  onControlInit() {
    const font = {
      fontFamily: { name: 'Arial', index: -1 },
      italic: false,
      bold: false,
      fontSize: 10,
    }
    EditorUtil.FontKit.setFont(font)

    for (let i = 0; i < 10; i++) {
      const { width } = EditorUtil.FontKit.MeasureCode(('' + i).charCodeAt(0))
      if (width) {
        this.widthOfDigits[i] = width
      } else {
        this.widthOfDigits[i] = 10
      }
    }

    this.offsetPixT = 17
    this.offsetPixB = this.offsetPixT
    this.offsetPixR = 8
    this.borderWidth = 7

    this.dragIconElement = document.createElement('canvas')
    const [drawIconW, drawIconH] = [9, 9]
    this.dragIconElement.width = drawIconW
    this.dragIconElement.height = drawIconH
    const dragIconCtx = this.dragIconElement.getContext('2d')!
    const iconData = dragIconCtx.createImageData(drawIconW, drawIconH)
    const pixels = iconData.data

    let ind = 0
    for (let j = 0; j < drawIconH; ++j) {
      const off1 = j > drawIconW >> 1 ? drawIconW - j - 1 : j
      const off2 = drawIconW - off1 - 1

      for (let r = 0; r < drawIconW; ++r) {
        if (r <= off1 || r >= off2) {
          pixels[ind++] = 183
          pixels[ind++] = 183
          pixels[ind++] = 183
          pixels[ind++] = 255
        } else {
          pixels[ind + 3] = 0
          ind += 4
        }
      }
    }
    dragIconCtx.putImageData(iconData, 0, 0)
  }

  private updateSizes() {
    const { width: slideWidth, height: slideHeight } = this.presentation
    let widthInMM = this.control.PositionInPx.R - this.control.PositionInPx.L
    let heightInMM = this.control.PositionInPx.B - this.control.PositionInPx.T
    let pxw = (widthInMM * Factor_mm_to_pix) >> 0

    if (widthInMM < 1 || heightInMM < 0) {
      this.isVisible = false
      return
    }
    this.isVisible = true

    pxw -= this.offsetPixR

    let dW = 0
    if (this.widthOfDigits.length > 5) dW = this.widthOfDigits[5]

    const countOfSlides = this.presentation.slides.length
    const charCount = ('' + (countOfSlides + 1)).length
    const offset = Math.max((dW * Factor_mm_to_pix * charCount + 10) >> 0, 30)
    if (this.isRTL) {
      this.offsetPixR = offset
    } else {
      this.offsetPixL = offset
    }

    pxw -= this.offsetPixL

    const valueOfHeightSlide = ((pxw * slideHeight) / slideWidth) >> 0
    let valueOfHeightPix =
      this.offsetPixT + this.offsetPixT + valueOfHeightSlide * countOfSlides
    if (countOfSlides > 0) {
      valueOfHeightPix += (countOfSlides - 1) * 3 * this.borderWidth
    }

    let scrollY = 0
    if (this.scrollYMax !== 0) {
      scrollY = this.scrollY / this.scrollYMax
    }

    const pixThumbsHeight = (heightInMM * Factor_mm_to_pix) >> 0
    if (valueOfHeightPix < pixThumbsHeight) {
      if (
        this.isScrollVisible &&
        EditorSkin.ThumbnailScrollBarHiddenInsteadOfUnvisible
      ) {
        this.thumbnails.bounds.R = 0
        this.thumbnailsBackground.bounds.R = 0
        this.thumbnailsScroll.bounds.witdh = 0

        this.control.resize(
          this.editorUI.width * Factor_pix_to_mm,
          this.editorUI.height * Factor_pix_to_mm,
          this.editorUI
        )
      } else {
        this.thumbnailsScroll.element.style.display = 'none'
      }
      this.isScrollVisible = false
      this.scrollY = 0
    } else {
      if (!this.isScrollVisible) {
        if (EditorSkin.ThumbnailScrollBarHiddenInsteadOfUnvisible) {
          this.thumbnailsBackground.bounds.R =
            SCROLLBAR_WIDTH_PX * Factor_pix_to_mm
          this.thumbnails.bounds.R = SCROLLBAR_WIDTH_PX * Factor_pix_to_mm

          const width = EditorSkin.Name === 'flat' ? 10 : SCROLLBAR_WIDTH_PX
          this.thumbnailsScroll.bounds.witdh = width * Factor_pix_to_mm
        } else {
          this.thumbnailsScroll.element.style.display = 'block'
        }

        this.control.resize(
          this.editorUI.width * Factor_pix_to_mm,
          this.editorUI.height * Factor_pix_to_mm,
          this.editorUI
        )
      }
      this.isScrollVisible = true

      const thumbnailAbsPos = this.thumbnails.PositionInPx
      widthInMM = thumbnailAbsPos.R - thumbnailAbsPos.L
      heightInMM = thumbnailAbsPos.B - thumbnailAbsPos.T
      pxw = (widthInMM * Factor_mm_to_pix) >> 0
      pxw -= this.offsetPixL + this.offsetPixR

      const pxh = ((pxw * slideHeight) / slideWidth) >> 0
      // TODO
      let totalPixHeight =
        this.offsetPixT + this.offsetPixT + pxh * countOfSlides
      if (countOfSlides > 0) {
        totalPixHeight += (countOfSlides - 1) * 3 * this.borderWidth
      }

      // now you need to set the dimensions
      const settings = new ScrollBarSettings()
      const element = this.thumbnails.element
      const width = toInt(element.style.width) || element.width
      const height = toInt(element.style.height) || element.height
      settings.screenWidth = width
      settings.screenHeight = height
      settings.bgColor = EditorSkin.Background_Thumbnails

      this.thumbnailsScroll.elements[
        DOM_ID.ThumbnailContainer.Scroll.Container.Id
      ].style.height = toInt(totalPixHeight) + 'px'

      if (this.scrollBar) {
        this.scrollBar.resetBySettings(settings)
        this.scrollBar.isHScroll = false
      } else {
        this.scrollBar = new ScrollBar(this.thumbnailsScroll.element, settings)

        this.scrollBar.onMouseLock = (evt: PointerEvent) => {
          this.scrollPointerEvtInfo.onDown(evt, true, true)
        }
        this.scrollBar.onUnLockMouse = (evt: PointerEvent) => {
          this.scrollPointerEvtInfo.onUp(evt)
        }

        this.scrollBar.bind('scrollV', (evt) => {
          this.needRerender = true
          this.verticalScroll(evt.scrollDest, evt.maxScrollY)
          this.debounceUpdateScroll()
        })
        this.scrollBar.isHScroll = false
      }
    }

    if (this.isScrollVisible) {
      const posY = (scrollY * this.scrollBar!.getMaxScrollY()) >> 0
      this.scrollBar!.scrollToY(posY)
    }
    this.editorUI.splitters.splitterBox.onResize()
    this.calculateAllThumbnailsPos()
    this.needUpdate = true
  }

  debounceUpdateScroll = debounce(() => {
    this.needRerender = false
    this.editorUI.editorKit.resourcesLoadManager.updateSlideLoadingQueue(
      this.startIndex,
      this.endIndex
    )
  }, 600)

  onFocus = () => {
    this.editorUI.editorKit.emitEndAddShape()

    // 点击获取焦点
    if (!BrowserInfo.isTablet) {
      this.editorUI.focusInput()
    } else {
      this.editorUI.blurInput()
      this.editorUI.focusEditor()
    }

    this.presentation.setFocusTargetType(FocusTarget.Thumbnails)
  }

  onBlur = () => {
    this.isMultiSelectState = false
    const curSlideIndex = this.presentation.currentSlideIndex

    if (this.slideThumbnails[curSlideIndex]) {
      this.resetSelection()
      this.slideThumbnails[curSlideIndex].selected = true
      this.syncSelectedSlideIndexes()
      this.render()
    }
  }

  resetSelection = () => {
    const countOfSlides = this.slideThumbnails.length
    for (let i = 0; i < countOfSlides; i++) {
      this.slideThumbnails[i].selected = false
    }
  }

  private verticalScroll(scrollY, maxY) {
    if (
      false === this.editorUI.editorKit.isEditorUIInitComplete ||
      false === this.isScrollVisible
    ) {
      return
    }

    this.scrollY = Math.max(0, Math.min(scrollY, maxY))
    this.scrollYMax = maxY

    this.calculateAllThumbnailsPos()
    this.needUpdate = true

    if (!EditorSettings.isMobile) {
      this.presentation.setFocusTargetType(FocusTarget.Thumbnails)
    }
  }

  /** 重新计算每个 thumbnail 相对全局的位置 */
  private calculateAllThumbnailsPos = () => {
    if (!this.isVisible) return

    const canvas = this.thumbnails.element
    if (null == canvas) return

    const { width: slideWidth, height: slideHeight } = this.presentation
    const height = canvas.height
    let [isFoundFirst, isFoundEnd] = [false, false]
    const curScrollY = this.scrollY >> 0
    const { L, R } = this.thumbnails.PositionInPx
    const widthInMM = R - L

    let pxw = (widthInMM * Factor_mm_to_pix) >> 0
    pxw -= this.offsetPixL + this.offsetPixR
    const pxh = ((pxw * slideHeight) / slideWidth) >> 0

    let offL = this.offsetPixT
    const countOfSlides = this.presentation.slides.length
    for (let i = 0; i < countOfSlides; i++) {
      const slideRef = this.presentation.slides[i].getRefId()
      if (i >= this.slideThumbnails.length) {
        this.slideThumbnails[i] = new SlideThumbnail(slideRef)
        if (0 === i) this.slideThumbnails[0].selected = true
      } else {
        this.slideThumbnails[i].updateSlideIndex(slideRef)
      }
      if (false === isFoundFirst) {
        if (offL + pxh > curScrollY) {
          this.startIndex = i
          isFoundFirst = true
        }
      }

      const thumbnail = this.slideThumbnails[i]
      thumbnail.left = this.offsetPixL
      thumbnail.top = offL - curScrollY
      thumbnail.right = thumbnail.left + pxw
      thumbnail.bottom = thumbnail.top + pxh

      if (false === isFoundEnd) {
        if (thumbnail.bottom > height) {
          this.endIndex = i
          isFoundEnd = true
        }
      }
      offL += pxh + 3 * this.borderWidth
    }

    const thCount = this.slideThumbnails.length
    if (thCount > countOfSlides) {
      this.slideThumbnails.splice(countOfSlides, thCount - countOfSlides)
    }
    if (false === isFoundEnd) {
      this.endIndex = countOfSlides - 1
    }
    //
    // 清除缓存
    for (let i = 0; i < this.startIndex - TH_CACHE_NUM; i++) {
      const thumbnail = this.slideThumbnails[i]
      this.cacheManager.release(thumbnail.cacheImage)
      thumbnail.cacheImage = null
    }
    for (let i = this.endIndex + TH_CACHE_NUM; i < countOfSlides; i++) {
      const thumbnail = this.slideThumbnails[i]
      this.cacheManager.release(thumbnail.cacheImage)
      thumbnail.cacheImage = null
    }

    this.syncSelectedSlideIndexes()
  }

  render() {
    const canvas = this.thumbnails.element
    if (!this.isVisible || null == canvas) {
      return
    }

    const editorUI = this.editorUI
    const presentation = editorUI.presentation
    const context = canvas.getContext('2d')!
    const { width, height } = canvas
    const dpr = BrowserInfo.PixelRatio

    context.clearRect(0, 0, width, height)

    const widthInMM = width * Factor_pix_to_mm
    const heightInMM = height * Factor_pix_to_mm
    const drawCxt = getCanvasDrawer(
      context,
      width,
      height,
      widthInMM,
      heightInMM
    )
    drawCxt.updateTransform(1, 0, 0, 1, 0, 0)
    EditorUtil.FontKit.setFont(ThumbnailDefaultFont)

    EditorUtil.FontKit.executeWithDefaults(() => {
      if (this.startIndex < 0 || this.endIndex < 0) {
        this.updateEditLayer()
        return
      }
      for (let i = this.startIndex; i <= this.endIndex; i++) {
        const thumbnail = this.slideThumbnails[i]

        const l = Math.round(thumbnail.left * dpr) // thumbnail.right = ofsetPixL
        const t = Math.round(thumbnail.top * dpr)
        const r = Math.round(thumbnail.right * dpr) // thumbnail.right = ofsetPixR + valueOfWidthSlide
        const w = Math.round((thumbnail.right - thumbnail.left) * dpr)
        const h = Math.round((thumbnail.bottom - thumbnail.top) * dpr)
        thumbnail.draw(context, l, t, w, h)

        if (thumbnail.selected) {
          drawCxt.setBrushColor(65, 70, 75, 255)
        } else {
          drawCxt.setBrushColor(65, 70, 75, 153)
        }

        // TODO, 考虑滚动条的位置以及宽度是否调整
        // 缩略图左/右侧到左/右侧边界的中点
        const centerOffsetPixL = this.isRTL
          ? (width + r) / (2 * dpr)
          : l / (2 * dpr)
        const mmCenterOffsetL = centerOffsetPixL * Factor_pix_to_mm
        const x = dpr * mmCenterOffsetL
        const y = dpr * (thumbnail.top * Factor_pix_to_mm + 3.5)

        // 幻灯片序号
        context.textAlign = 'center'
        drawCxt.fillString(x, y, `${i + 1}`, true)!
        const slide = presentation.slides[i]
        // 设计资源须以 32 * 32px (2x图)为基准
        const icons: {
          desc: string
          src: HTMLImageElement
          args?: Dict
        }[] = []
        if (slide) {
          const isSlideVisible = slide.isVisible()
          if (EditorSettings.isSupportComment) {
            const slideComment = slide.getSlideComments()
            const comments = slideComment && slideComment.getValidComments()
            if (comments && comments.length) {
              icons.push({
                desc: 'comment',
                src: SlideCommentImage,
                args: { commentsNum: comments.length },
              })
            }
          }
          if (isSlideHasTransition(slide)) {
            icons.push({
              desc: 'transition',
              src: SlideTransitionImage,
            })
          }
          if (isSlideHasAnimation(slide)) {
            icons.push({
              desc: 'animation',
              src: SlideAnimationImage,
            })
          }
          if (icons.length) {
            const mmSize = ThumbnailIconMMSize * dpr // w === h
            let yOffset = 7 * Factor_pix_to_mm * dpr
            icons.forEach((icon) => {
              const iconWidthInMM = (icon.src.width * Factor_pix_to_mm) / 2
              const startMMX = dpr * (mmCenterOffsetL - iconWidthInMM / 2)
              const startMMY = y + yOffset

              const needHandleRTL = this.isRTL && icon.desc === 'comment'
              if (needHandleRTL) {
                context.save()
                const startPixX = dpr * (centerOffsetPixL - icon.src.width / 2)
                context.translate(canvas.width + startPixX, 0)
                context.scale(-1, 1)
              }

              drawCxt._drawImageElement(
                icon.src,
                startMMX,
                startMMY,
                mmSize,
                mmSize
              )
              if (needHandleRTL) {
                context.restore()
              }
              // if (icon.desc === 'comment') {
              //   this.drawSlideCommentNums(
              //     icon.args!.commentsNum,
              //     startMMX,
              //     startMMY
              //   )
              // }
              yOffset += mmSize + Factor_pix_to_mm * dpr
            })
          }
          // 隐藏幻灯片
          if (!isSlideVisible) {
            context.fillStyle = 'rgba(255, 255, 255, 0.6)'
            context.fillRect(l, t, w, h)
            const width =
              (SlideHiddenIconImage.width / 2) * BrowserInfo.PixelRatio
            context.drawImage(
              SlideHiddenIconImage,
              l + w / 2 - width / 2,
              t + h / 2 - width / 2,
              width,
              width
            )
          }
        }
        this.drawSlideCollabStatus(this.isRTL ? r : l, t, i)
      }
      this.updateEditLayer()
    })
    drawCxt.resetState()
  }

  updateEditLayer() {
    const canvas = this.thumbnailsBackground.element
    if (!this.isVisible || null == canvas) return

    const context = canvas.getContext('2d')!
    const { width, height } = canvas

    context.fillStyle = EditorSkin.Background_Thumbnails
    context.fillRect(0, 0, width, height)

    context.fillStyle = COLOR_SELECT
    const countOfSlides = this.slideThumbnails.length
    for (let i = 0; i < countOfSlides; i++) {
      const th = this.slideThumbnails[i]

      const x = Math.round(th.left * BrowserInfo.PixelRatio)
      const y = Math.round(th.top * BrowserInfo.PixelRatio)
      const r = Math.round(th.right * BrowserInfo.PixelRatio)
      const b = Math.round(th.bottom * BrowserInfo.PixelRatio)

      this.drawShadow(context, x, y, r, b)

      if (th.selected && th.focused) {
        this.drawBorderRect(COLOR_HOVER_SELECT, context, x, y, r, b)
      } else if (th.selected) {
        this.drawBorderRect(COLOR_SELECT, context, x, y, r, b)
      } else if (th.focused) {
        this.drawBorderRect(COLOR_HOVER, context, x, y, r, b)
      }
    }

    if (this.isDraging && !this.isSimpleDrag && -1 !== this.dragEndIndex) {
      // now you just need to draw a line
      context.strokeStyle = COLOR_DRAG
      let y = (0.5 * this.offsetPixT) >> 0
      if (this.dragEndIndex > 0) {
        const targetPosThumb = this.slideThumbnails[this.dragEndIndex - 1]
        y = (targetPosThumb.bottom + 1.5 * this.borderWidth) >> 0
      }
      y *= BrowserInfo.PixelRatio

      let [leftX, rightX] = [0, width]
      if (this.slideThumbnails.length > 0) {
        leftX = this.slideThumbnails[0].left + 4
        rightX = this.slideThumbnails[0].right - 4
      }
      leftX *= BrowserInfo.PixelRatio
      rightX *= BrowserInfo.PixelRatio

      context.lineWidth = LINE_WIDTH_DRAG
      context.beginPath()
      context.moveTo(leftX, y + 0.5)
      context.lineTo(rightX, y + 0.5)
      context.stroke()
      context.beginPath()

      if (null != this.dragIconElement) {
        context.drawImage(
          this.dragIconElement,
          0,
          0,
          (this.dragIconElement.width + 1) >> 1,
          this.dragIconElement.height,
          leftX - this.dragIconElement.width,
          y - (this.dragIconElement.height >> 1),
          (this.dragIconElement.width + 1) >> 1,
          this.dragIconElement.height
        )

        context.drawImage(
          this.dragIconElement,
          this.dragIconElement.width >> 1,
          0,
          (this.dragIconElement.width + 1) >> 1,
          this.dragIconElement.height,
          rightX + (this.dragIconElement.width >> 1),
          y - (this.dragIconElement.height >> 1),
          (this.dragIconElement.width + 1) >> 1,
          this.dragIconElement.height
        )
      }
    }
  }

  private drawBorderRect(color, ctx: CanvasRenderingContext2D, x, y, r, b) {
    ctx.beginPath()
    ctx.strokeStyle = color
    const w = BORDER_WIDTH
    ctx.lineWidth = w
    ctx.rect(x - w, y - w, r - x + w * 2, b - y + w * 2)
    ctx.stroke()
    ctx.beginPath()
  }

  private drawShadow(ctx, x, y, r, b) {
    ctx.save()
    ctx.beginPath()
    const w = SHADOW_WIDTH
    ctx.lineWidth = w
    ctx.shadowBlur = 2 * BrowserInfo.PixelRatio
    ctx.shadowColor = 'rgba(0, 0, 0, 0.25)'
    ctx.strokeStyle = EditorSkin.Background_Thumbnails
    ctx.rect(x + w / 2, y + w / 2, r - x - w, b - y - w)
    ctx.stroke()
    ctx.shadowOffsetX = 0
    ctx.shadowOffsetY = 0
    ctx.shadowBlur = 0
    ctx.restore()
  }

  updateAll() {
    this.forceUpdate = true
  }

  updateSlideThumbnailDirty(index: number, isDirty = true) {
    if (this.slideThumbnails.length > index && this.slideThumbnails[index]) {
      this.slideThumbnails[index].isDirty = isDirty
    }
  }

  checkNeedUpdate() {
    // 正在滚动或不可见
    if (
      !this.isVisible ||
      !this.widthOfDigits.length ||
      this.editorUI.editorKit.collaborationManager.getIsCoediting()
    ) {
      return
    }

    if (this.needRerender) {
      this.render()
      this.needUpdate = false
      return
    }

    if (this.forceUpdate === true) {
      const countOfSlides = this.presentation.slides.length
      for (let i = 0; i <= countOfSlides; i++) {
        this.updateSlideThumbnailDirty(i)
      }
      this.needUpdate = true
      this.forceUpdate = false
    }

    if (!this.needUpdate) {
      // 检查当前��口是否需要重绘
      for (let i = this.startIndex; i <= this.endIndex; i++) {
        const thumbnail = this.slideThumbnails[i]
        if (thumbnail?.isCacheInvalid()) {
          this.needUpdate = true
          break
        }
      }
    }

    if (this.needUpdate) {
      this.onCheckUpdatePaint()
    }
  }
  private _findPaintIndex(): number {
    let dirtyIdx: number = -1
    let emptyIdx: number = -1
    for (let i = this.startIndex; i <= this.endIndex; i++) {
      const slide = this.presentation.slides[i]
      if (slide == null || slide.isLoading === true) {
        break
      }

      const thumbnail = this.slideThumbnails[i]

      // draw empty thumbnail first
      if (emptyIdx === -1 && thumbnail.isEmpty()) {
        emptyIdx = i
        break
      }

      if (
        dirtyIdx === -1 &&
        null != thumbnail.cacheImage &&
        thumbnail.isCacheInvalid()
      ) {
        dirtyIdx = i
      }
    }

    return emptyIdx > -1 ? emptyIdx : dirtyIdx
  }

  onCheckUpdatePaint = limitInvoke(() => {
    this.checkScrollByIndex()
    const renderIdx = this._findPaintIndex()

    if (renderIdx > -1) {
      const slide = this.presentation.slides[renderIdx]
      const thumbnail = this.slideThumbnails[renderIdx]
      this.cacheManager.release(thumbnail.cacheImage)
      thumbnail.cacheImage = null
      const w = Math.round(
        (thumbnail.right - thumbnail.left) * BrowserInfo.PixelRatio
      )
      const h = Math.round(
        (thumbnail.bottom - thumbnail.top) * BrowserInfo.PixelRatio
      )
      thumbnail.cacheImage = this.cacheManager.getImage(w, h)

      const { width: slideWidth, height: slideHeight } = this.presentation
      const drawCxt = getCanvasDrawer(
        thumbnail.cacheImage.image!.ctx,
        w,
        h,
        slideWidth,
        slideHeight
      )
      drawCxt.isNoRenderEmptyPlaceholder = true
      drawCxt.isRenderThumbnail = true

      drawCxt.updateTransform(1, 0, 0, 1, 0, 0)
      calculateRenderingStateForSlides([slide])
      renderPresentationSlide(this.presentation, renderIdx, drawCxt)
      drawCxt.resetState()
      this.updateSlideThumbnailDirty(renderIdx, false)
    }
    this.render()
    this.needUpdate = false
  }, 60)

  private getSelectedSlideRange() {
    let min = this.editorUI.renderingController.curSlideIndex
    let max = min
    const thCount = this.slideThumbnails.length
    for (let i = 0; i < thCount; i++) {
      if (this.slideThumbnails[i].selected) {
        if (i < min) min = i
        if (i > max) max = i
      }
    }
    return [min, max]
  }

  // 需要重新计算时用 getSelectedTumbnailIndexes
  syncSelectedSlideIndexes() {
    const selectedIndices = this.getSelectedTumbnailIndexes()
    this.presentation.setSelectedSlideIndexes(selectedIndices)
  }

  // 仅取数
  getSelectedSlideIndices() {
    const presentation = this.presentation
    return presentation.slideSelectManager.getSelectedIndices()
  }

  // onMouseDown 时 sync to presentation
  private getSelectedTumbnailIndexes() {
    const indexes: number[] = []
    const thCount = this.slideThumbnails.length
    for (let i = 0; i < thCount; i++) {
      if (this.slideThumbnails[i].selected) {
        indexes[indexes.length] = i
      }
    }
    return indexes
  }

  shiftSelect(isTop: boolean, isEnd: boolean) {
    const countOfSlides = this.editorUI.renderingController.countOfSlides
    const [min, max] = this.getSelectedSlideRange()

    let slideIndex = this.editorUI.renderingController.curSlideIndex
    if (isEnd) {
      slideIndex = isTop ? 0 : countOfSlides - 1
    } else if (isTop) {
      if (min !== slideIndex) {
        slideIndex = min - 1
        if (slideIndex < 0) slideIndex = 0
      } else {
        slideIndex = max - 1
        if (slideIndex < 0) slideIndex = 0
      }
    } else {
      if (min !== slideIndex) {
        slideIndex = min + 1
        if (slideIndex >= countOfSlides) slideIndex = countOfSlides - 1
      } else {
        slideIndex = max + 1
        if (slideIndex >= countOfSlides) slideIndex = countOfSlides - 1
      }
    }

    let _max = slideIndex
    let _min = this.editorUI.renderingController.curSlideIndex
    if (_min > _max) {
      ;[_max, _min] = [_min, _max]
    }

    for (let i = 0; i < _min; i++) {
      this.slideThumbnails[i].selected = false
    }
    for (let i = _min; i <= _max; i++) {
      this.slideThumbnails[i].selected = true
    }
    for (let i = _max + 1; i < countOfSlides; i++) {
      this.slideThumbnails[i].selected = false
    }

    this.syncSelectedSlideIndexes()
    syncInterfaceState(this.editorUI.editorKit)
    this.updateEditLayer()
    this.goToSlide(slideIndex)
  }

  onKeyDown = (e) => {
    const control = this.thumbnails.element
    const { keyboard: keyEvent } = GlobalEvents
    const mouse = this.pointerEvtInfo
    if (mouse.isLocked === true && mouse.target === control) {
      e.preventDefault()
      return false
    }
    GlobalEvents.keyboard.onKeyEvent(e)
    const { editorUI, presentation } = this
    const selectedIndices = this.getSelectedSlideIndices()
    const curSlideIndex = presentation.currentSlideIndex
    const countOfSlides = presentation.slides.length
    const canEdit = presentation.canEdit()

    switch (keyEvent.keyCode) {
      case keyCodeMap.enter: {
        if (!canEdit) return
        if (selectedIndices.length > 0 && !keyEvent.ctrlKey) {
          const lastSlideIndex = selectedIndices[selectedIndices.length - 1]
          editorUI.goToSlide(lastSlideIndex)
          presentation.addNextSlide()
          return false
        } else {
          return
        }
      }
      case keyCodeMap.backSpace:
      case keyCodeMap.delete: {
        if (!canEdit) break
        const { renderingController, editorHandler: handler } = editorUI
        if (!editorUI.editorKit.isSupportEmptySlides) {
          if (selectedIndices.length === renderingController.countOfSlides) {
            selectedIndices.splice(0, 1)
          }
        }
        if (selectedIndices.length !== 0) {
          handler.deleteSlides(selectedIndices)
        }
        if (0 === presentation.slides.length) {
          this.needUpdate = true
        }
        break
      }
      case keyCodeMap.pageDown:
      case keyCodeMap.arrowDown: {
        if (keyEvent.ctrlKey && keyEvent.shiftKey) {
          if (canEdit) {
            startEditAction(EditActionFlag.action_Presentation_MoveSlidesToEnd)
            presentation.moveSlides(selectedIndices, presentation.slides.length)
            calculateForRendering()
            syncInterfaceState(editorUI.editorKit)
          }
        } else if (keyEvent.ctrlKey) {
          if (canEdit) {
            let canMove = false
            let firstIndex
            for (let i = selectedIndices.length - 1; i > -1; i--) {
              if (i === selectedIndices.length - 1) {
                if (selectedIndices[i] < presentation.slides.length - 1) {
                  canMove = true
                  firstIndex = i
                  break
                }
              } else {
                if (Math.abs(selectedIndices[i] - selectedIndices[i + 1]) > 1) {
                  canMove = true
                  firstIndex = i
                  break
                }
              }
            }
            if (canMove) {
              startEditAction(
                EditActionFlag.action_Presentation_MoveSlidesToNext
              )
              for (let i = firstIndex; i > -1; --i) {
                presentation.moveSlides(
                  [selectedIndices[i]],
                  selectedIndices[i] + 2
                )
              }
              calculateForRendering()
              syncInterfaceState(editorUI.editorKit)
            }
            //return false;
          }
        }

        if (!keyEvent.shiftKey) {
          if (curSlideIndex < countOfSlides - 1) {
            editorUI.goToSlide(curSlideIndex + 1)
          }
        } else if (keyEvent.ctrlKey) {
          if (countOfSlides > 0) {
            editorUI.goToSlide(countOfSlides - 1)
          }
        } else {
          this.shiftSelect(false, false)
        }
        break
      }
      case keyCodeMap.pageUp:
      case keyCodeMap.arrowUp: {
        if (keyEvent.ctrlKey && keyEvent.shiftKey) {
          if (presentation.canEdit()) {
            startEditAction(
              EditActionFlag.action_Presentation_MoveSlidesToStart
            )
            presentation.moveSlides(selectedIndices, 0)
            calculateForRendering()
            syncInterfaceState(editorUI.editorKit)
          }
        } else if (keyEvent.ctrlKey) {
          if (presentation.canEdit()) {
            let canMove = false
            let firstIndex
            for (let i = 0; i < selectedIndices.length; ++i) {
              if (i === 0) {
                if (selectedIndices[i] > 0) {
                  canMove = true
                  firstIndex = i
                  break
                }
              } else {
                if (Math.abs(selectedIndices[i] - selectedIndices[i - 1]) > 1) {
                  canMove = true
                  firstIndex = i
                  break
                }
              }
            }
            if (canMove) {
              startEditAction(
                EditActionFlag.action_Presentation_MoveSlidesToPrev
              )
              for (let i = firstIndex; i > -1; --i) {
                presentation.moveSlides(
                  [selectedIndices[i]],
                  selectedIndices[i] - 1
                )
              }
              calculateForRendering()
              syncInterfaceState(editorUI.editorKit)
            }
            //return false;
          }
        }
        if (!keyEvent.shiftKey) {
          if (curSlideIndex > 0) {
            editorUI.goToSlide(curSlideIndex - 1)
          }
        } else if (keyEvent.ctrlKey) {
          if (countOfSlides > 0) {
            editorUI.goToSlide(0)
          }
        } else {
          this.shiftSelect(true, false)
        }
        break
      }
      case keyCodeMap.home: {
        if (!keyEvent.shiftKey) {
          if (curSlideIndex > 0) {
            editorUI.goToSlide(0)
          }
        } else {
          if (curSlideIndex > 0) {
            this.shiftSelect(true, true)
          }
        }
        break
      }
      case keyCodeMap.end: {
        if (!keyEvent.shiftKey) {
          if (curSlideIndex !== countOfSlides - 1) {
            editorUI.goToSlide(countOfSlides - 1)
          }
        } else {
          if (curSlideIndex > 0) {
            this.shiftSelect(false, true)
          }
        }
        break
      }
      case keyCodeMap.keyA: {
        if (keyEvent.ctrlKey) {
          this.selectAll()
        }
        break
      }
      case keyCodeMap.keyC:
      case keyCodeMap.keyX:
      case keyCodeMap.keyV: {
        if (keyEvent.ctrlKey) return undefined
        break
      }
      case keyCodeMap.keyY:
      case keyCodeMap.keyZ: {
        if (keyEvent.ctrlKey) return true
        break
      }
      case keyCodeMap.keyM: {
        if (keyEvent.ctrlKey) {
          if (keyEvent.shiftKey) {
            if (!EditorSettings.isSupportComment) return undefined
            const handler = editorUI.editorHandler
            if (!EditorSettings.isShowComment) {
              EditorSettings.isShowComment = true
              EditorKit.emitToggleShowComment()
            }
            const isAddingComment = isCurAddingComment(handler)
            if (!isAddingComment) {
              startAddComment(handler)
            } else {
              endAddComment(handler)
            }
            editorUI.triggerRender(true)
          } else {
            EditorKit.addSlide()
          }
          e.preventDefault()
          return false
        }
        break
      }
      case keyCodeMap.keyD: {
        if (!keyEvent.ctrlKey) break
        if (canEdit) {
          EditorKit.duplicateSlide()
        }
        e.preventDefault()
        return false
      }
      case 57351:
      case keyCodeMap.f10: {
        if (keyEvent.keyCode === 57351 || keyEvent.shiftKey) {
          const selectedIndices = this.getSelectedSlideIndices()
          const minSlideIndex = Math.min(...selectedIndices)
          const pixPos = this.getSlidePosInfoByIndex(minSlideIndex)
          // Todo
          if (pixPos) {
            EditorKit.emitContextMenu({
              type: ContextMenuTypes.Thumbnails,
              absX: pixPos.x,
              absY: pixPos.y,
            })
          }
          return false
        }
        break
      }
      default:
        return
    }
    this.render()
    e.preventDefault()
    return false
  }

  updateCollaboratorStatus(status: CollaboratorStatus) {
    this.collabStatus = status
    this.render()
  }

  updateSlideTransitionStatus() {
    this.render()
  }

  /**
   * @param n comment 数量
   * @param x comment icon 的起点 x
   * @param y comment icon 的起点 y
   */
  drawSlideCommentNums(n: number, x, y) {
    const dpr = BrowserInfo.PixelRatio
    const w = (SlideCommentImage.width * dpr) / 2
    const h = (SlideCommentImage.height * dpr) / 2
    const canvas = this.thumbnails.element
    const ctx = canvas.getContext('2d')!
    ctx.font = 8 * dpr + 'px PingFang SC'
    ctx.strokeStyle = '#FFFFFF'
    ctx.textAlign = 'center'
    ctx.textBaseline = 'middle'
    const text = n > 99 ? '99' : n.toString()
    const startX = x * Factor_mm_to_pix + w / 2 + 0.5
    const startY = y * Factor_mm_to_pix + h / 2
    ctx.strokeText(text, startX, startY)
    ctx.restore()
  }

  drawSlideCollabStatus(x: number, y: number, i: number) {
    if (EditorSettings.isViewMode || i < this.startIndex || i > this.endIndex) {
      return
    }

    const { refId } = this.presentation.slides[i]
    const status = refId && this.collabStatus[refId]
    if (status && status.length) {
      const canvas = this.thumbnails.element
      const ctx = canvas.getContext('2d')!
      const hoverText = translator.getValue('人正在协作')
      const { isMouseInCollabStatus, slideIndex } = this.collabStatusState
      const mouseInCollabStatus = isMouseInCollabStatus && slideIndex === i
      const collabNum = status.length
      const { height, imageSize, padding, textOffsetX, fontSize } =
        this.getCollabStatusSize()
      ctx.save()

      ctx.font = `${fontSize}px PingFang SC`
      const text = `${collabNum}${mouseInCollabStatus ? hoverText : ''}`
      const textWidth = ctx.measureText(text).width
      const contentWidth = textOffsetX + textWidth + padding * 3
      this.collabStatusState.width = contentWidth
      if (mouseInCollabStatus) {
        ctx.fillStyle = '#2C3033'
        ctx.globalAlpha = 0.8
      } else {
        ctx.fillStyle = '#676B6F'
        ctx.globalAlpha = 0.7
      }
      ctx.beginPath()
      ctx.fillRect(this.isRTL ? x - contentWidth : x, y, contentWidth, height)
      ctx.restore()
      ctx.save()
      ctx.fillStyle = '#FFFFFF'
      ctx.textAlign = 'center'
      ctx.textBaseline = 'middle'
      const textCenterX =
        x + (this.isRTL ? -1 : 1) * (textOffsetX + textWidth / 2)
      ctx.font = `${fontSize}px PingFang SC`
      ctx.fillText(text, textCenterX, y + height / 2)
      const image = BrowserInfo.isRetina ? CollaStatusImage2x : CollaStatusImage
      ctx.drawImage(
        image,
        x + (this.isRTL ? -imageSize - padding : padding),
        y + (height - imageSize) / 2,
        imageSize,
        imageSize
      )
      ctx.restore()
    }
  }

  getCollabStatusSize(ignoreRetina = false) {
    let height = 20
    let imageSize = height - 4
    let padding = 1.6
    let textOffsetX = imageSize + padding * 2
    let fontSize = 11
    if (!ignoreRetina) {
      height *= BrowserInfo.PixelRatio
      imageSize *= BrowserInfo.PixelRatio
      padding *= BrowserInfo.PixelRatio
      textOffsetX *= BrowserInfo.PixelRatio
      fontSize *= BrowserInfo.PixelRatio
    }
    return {
      height,
      imageSize,
      padding,
      textOffsetX,
      fontSize,
    }
  }

  selectAll(indexes?: number[]) {
    if (indexes) {
      this.slideThumbnails.forEach((th) => {
        th.selected = false
      })

      indexes.forEach((pos) => {
        this.slideThumbnails[pos].selected = true
      })
    } else {
      this.slideThumbnails.forEach((th) => {
        th.selected = true
      })
    }

    this.syncSelectedSlideIndexes()
    syncInterfaceState(this.editorUI.editorKit)
    this.updateEditLayer()
  }

  /** 取 thumbnail 右下角(RTL下为左下角) editorUI 内相对坐标 */
  getSlideThumbnailPosByIndex(index) {
    const slideThumbnail = this.slideThumbnails[index]
    if (!slideThumbnail) return
    let x: number
    if (this.isRTL) {
      const mainContainerWidth = this.editorUI.mainContainer.getPxWidth()
      x = mainContainerWidth + SCROLLBAR_WIDTH_PX + slideThumbnail.left
    } else {
      x = slideThumbnail.right
    }
    return { x, y: slideThumbnail.bottom }
  }

  setRTL(isRTL: boolean) {
    this.isRTL = isRTL
    this.offsetPixL = this.isRTL ? 8 : 0
    this.offsetPixR = this.isRTL ? 0 : 4
  }
}

class SlideThumbnail {
  left = 0
  top = 0
  right = 0
  bottom = 0
  isDirty = false
  cacheImage: Nullable<CacheImage>
  selected = false
  focused = false
  private slideRef: string
  constructor(slideRef: string) {
    this.cacheImage = null
    this.slideRef = slideRef
  }
  updateSlideIndex(slideRef: string) {
    if (this.slideRef !== slideRef) {
      this.isDirty = true
    }
    this.slideRef = slideRef
  }
  draw = (context: CanvasRenderingContext2D, x, y, w, h) => {
    if (w <= 0 || h <= 0) return

    const canvasImg = this.cacheImage?.image
    if (canvasImg != null) {
      // w = canvasImg.width
      // h = canvasImg.height
      context.drawImage(canvasImg, x, y)
    } else {
      context.fillStyle = '#FFFFFF'
      context.fillRect(x, y, w, h)
    }
  }

  isEmpty() {
    return this.cacheImage?.image == null
  }

  isCacheInvalid() {
    if (this.isDirty || null == this.cacheImage) {
      return true
    }

    const w = (this.right - this.left) * BrowserInfo.PixelRatio
    const h = (this.bottom - this.top) * BrowserInfo.PixelRatio

    if (
      Math.abs(this.cacheImage.image!.width - w) > 1 ||
      Math.abs(this.cacheImage.image!.height - h) > 1
    ) {
      this.isDirty = true
      return true
    }
  }
}
