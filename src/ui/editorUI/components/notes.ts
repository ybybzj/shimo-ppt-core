import {
  BrowserInfo,
  enlargeByRetinaRatio,
  shrinkByRetinaRatio,
} from '../../../common/browserInfo'
import {
  stopEvent,
  PointerEventType,
  PointerEventArg,
  listenPointerEvents,
} from '../../../common/dom/events'
import { Dom } from '../../../common/dom/vdom'
import { DomWheelEvt } from '../../../common/dom/wheelEvent'
import { FocusTarget, EditorSkin } from '../../../core/common/const/drawing'
import {
  Factor_mm_to_pix,
  Factor_pix_to_mm,
} from '../../../core/common/const/unit'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { EditorUtil } from '../../../globals/editor'
import {
  UIControl,
  UIControlContainer,
  ControlCreaterMakers,
} from '../Controls'
import { EditorUI } from '../EditorUI'
import { splitterStates } from '../splitterStates'
import { ScrollBarSettings, ScrollBar } from '../scroll'
import { getCanvasDrawer } from '../../../core/graphic/CanvasDrawer'
import { EditLayer } from '../editLayer'
import {
  handleMouseDownForNotes,
  handleMouseMoveForNotes,
  handleMouseUpForNotes,
} from '../utils/pointers'
import { CSS_CLASS, DOM_ID, DOM_STYLE } from '../controls/const'
import { getCtrlParamsById } from '../controls/rtl'
import { EventNames } from '../../../editor/events'
import { getAndUpdateCursorPositionForPresentation } from '../../../core/utilities/cursor/cursorPosition'
import {
  checkNotesClearSelectScroll,
  checkNotesTriggerSelectScroll,
  handleNotesTouchEnd,
  handleNotesTouchMove,
  handleNotesTouchStart,
} from '../utils/notesPointer'
const { C, CC } = ControlCreaterMakers

const NotesContainerCtrlSettings = {
  name: DOM_ID.MainContainer.NotesContainer.Id,
  node: Dom.div(
    `#${DOM_ID.MainContainer.NotesContainer.Id}.${CSS_CLASS.Block}`,
    { style: DOM_STYLE.MainContainer.NotesContainer.Style },
    [
      Dom.placeholder(DOM_ID.MainContainer.NotesContainer.Notes),
      Dom.placeholder(DOM_ID.MainContainer.NotesContainer.EditLayer),
      Dom.placeholder(DOM_ID.MainContainer.NotesContainer.ScrollContainer.Id),
    ]
  ),
} as const

const NotesContainerControl = CC<
  { [DOM_ID.MainContainer.NotesContainer.Id]: HTMLDivElement },
  [
    typeof DOM_ID.MainContainer.NotesContainer.Notes,
    typeof DOM_ID.MainContainer.NotesContainer.EditLayer,
    typeof DOM_ID.MainContainer.NotesContainer.ScrollContainer.Id,
  ],
  typeof NotesContainerCtrlSettings
>(NotesContainerCtrlSettings)

const NotesCtrlSettings = {
  name: DOM_ID.MainContainer.NotesContainer.Notes,
  node: Dom.canvas(
    `#${DOM_ID.MainContainer.NotesContainer.Notes}.${CSS_CLASS.Block}`,
    { style: DOM_STYLE.MainContainer.NotesContainer.Notes }
  ),
} as const

const NotesControl = C<
  { [DOM_ID.MainContainer.NotesContainer.Notes]: HTMLCanvasElement },
  typeof NotesCtrlSettings
>(NotesCtrlSettings)

const NotesEditLayerCtrlSettings = {
  name: DOM_ID.MainContainer.NotesContainer.EditLayer,
  node: Dom.canvas(
    `#${[DOM_ID.MainContainer.NotesContainer.EditLayer]}.${CSS_CLASS.Block}`,
    { style: DOM_STYLE.MainContainer.NotesContainer.EditLayer }
  ),
} as const

const NotesEditLayerControl = C<
  {
    [DOM_ID.MainContainer.NotesContainer.EditLayer]: HTMLCanvasElement
  },
  typeof NotesEditLayerCtrlSettings
>(NotesEditLayerCtrlSettings)

const NotesScrollerCtrlSettings = {
  name: DOM_ID.MainContainer.NotesContainer.ScrollContainer.Id,
  node: Dom.div(
    `#${DOM_ID.MainContainer.NotesContainer.ScrollContainer.Id}`,
    { style: DOM_STYLE.MainContainer.NotesContainer.ScrollContainer.Style },
    [
      Dom.div(
        `#${DOM_ID.MainContainer.NotesContainer.ScrollContainer.Scroll}.${CSS_CLASS.Block}`,
        { style: DOM_STYLE.MainContainer.NotesContainer.ScrollContainer.Scroll }
      ),
    ]
  ),
} as const

const NotesScrollerContainerControl = C<
  {
    [DOM_ID.MainContainer.NotesContainer.ScrollContainer.Id]: HTMLDivElement
    [DOM_ID.MainContainer.NotesContainer.ScrollContainer.Scroll]: HTMLDivElement
  },
  typeof NotesScrollerCtrlSettings
>(NotesScrollerCtrlSettings)

export class NotesComponent {
  width = 0
  height = 0
  editorUI: EditorUI
  needRepaint = false
  slideIndex = -1
  editLayer: EditLayer
  offsetX = 10
  offsetY = 10
  scroll = 0
  maxOfScroll = 0
  selectScrollTimerId: any
  control!: UIControlContainer<
    typeof DOM_ID.MainContainer.NotesContainer.Id,
    HTMLDivElement,
    { [DOM_ID.MainContainer.NotesContainer.Id]: HTMLDivElement },
    [
      typeof DOM_ID.MainContainer.NotesContainer.Notes,
      typeof DOM_ID.MainContainer.NotesContainer.EditLayer,
      typeof DOM_ID.MainContainer.NotesContainer.ScrollContainer.Id,
    ]
  >

  notes!: UIControl<
    typeof DOM_ID.MainContainer.NotesContainer.Notes,
    HTMLCanvasElement,
    { [DOM_ID.MainContainer.NotesContainer.Notes]: HTMLCanvasElement }
  >

  notesEditLayer!: UIControl<
    typeof DOM_ID.MainContainer.NotesContainer.EditLayer,
    HTMLCanvasElement,
    { [DOM_ID.MainContainer.NotesContainer.EditLayer]: HTMLCanvasElement }
  >

  notesScroll!: UIControl<
    typeof DOM_ID.MainContainer.NotesContainer.ScrollContainer.Id,
    HTMLDivElement,
    {
      [DOM_ID.MainContainer.NotesContainer.ScrollContainer.Id]: HTMLDivElement
      [DOM_ID.MainContainer.NotesContainer.ScrollContainer
        .Scroll]: HTMLDivElement
    }
  >
  scrollBar?: ScrollBar

  private _unlistenEvents: undefined | (() => void)
  get presentation() {
    return this.editorUI.editorKit.presentation
  }

  /** 共用画板区域事件(cursor 也是共用的) */
  get pointerEvtInfo() {
    return this.editorUI.pointerEvtInfo
  }
  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI
    this.selectScrollTimerId = -1
    this.init()
    this.editLayer = new EditLayer(this.notesEditLayer, this.editorUI)
  }
  init() {
    this.control = NotesContainerControl(
      ...getCtrlParamsById(NotesContainerCtrlSettings.name, this.editorUI)!
    )
    this.notes = NotesControl(
      ...getCtrlParamsById(NotesCtrlSettings.name, this.editorUI)!
    )
    this.notesEditLayer = NotesEditLayerControl(
      ...getCtrlParamsById(NotesEditLayerCtrlSettings.name, this.editorUI)!
    )
    this.notesScroll = NotesScrollerContainerControl(
      ...getCtrlParamsById(NotesScrollerCtrlSettings.name, this.editorUI)!
    )

    this.control.add(this.notes)
    this.control.add(this.notesEditLayer)
    this.control.add(this.notesScroll)

    if (!EditorSettings.isNotesEnabled) {
      this.control.element.style.display = 'none'
    } else {
      this.notes.element.style.backgroundColor = EditorSkin.Background_Light
      this.control.element.style.backgroundColor = EditorSkin.Background_Light
      this.control.element.style.borderTop =
        '1px solid ' + EditorSkin.Color_Splitter
    }
  }

  handlePointerEvent<T extends PointerEventType>(et: T, e: PointerEventArg<T>) {
    if (et === 'pointerdown') {
      this.pointerEvtInfo.lockPointerType(e as PointerEvent)
    }
    const isTouchMode = this.pointerEvtInfo.isTouchMode()
    switch (et) {
      case 'pointerdown': {
        this.editorUI.textCursor.setCheckScroll(true)
        if (isTouchMode) {
          handleNotesTouchStart(this, e as PointerEvent)
        } else {
          this.onMouseDown(e as PointerEvent)
        }
        break
      }
      case 'pointermove': {
        if (isTouchMode) {
          handleNotesTouchMove(this, e as PointerEvent)
        } else {
          this.onMouseMove(e as PointerEvent)
        }
        break
      }
      case 'pointerup': {
        if (isTouchMode) {
          handleNotesTouchEnd(this, e as PointerEvent)
        } else {
          this.onMouseUp(e as PointerEvent)
        }
        this.pointerEvtInfo.unLockPointerType()
        break
      }
      case 'wheel': {
        this.onMouseWheel(e as DomWheelEvt)
        break
      }
    }
  }
  initEvents() {
    const elem = this.notes.element
    const editLayerElem = this.notesEditLayer.element

    const evts: PointerEventType[] = ['pointerdown', 'pointermove', 'pointerup']
    const unlistenNotesElm = listenPointerEvents(elem, evts, this)
    const unlistenEditorLayer = listenPointerEvents(editLayerElem, evts, this)

    const unlistenControl = listenPointerEvents(
      this.control.element,
      ['wheel'],
      this
    )

    this._unlistenEvents = () => {
      unlistenNotesElm()
      unlistenEditorLayer()
      unlistenControl()
    }
    this.editorUI.editorKit.emitEvent(
      EventNames.Evt_onNotesShow,
      EditorSettings.isNotesEnabled
    )
  }

  removeEvents() {
    if (this._unlistenEvents) {
      this._unlistenEvents()
    }

    if (this.scrollBar) {
      this.scrollBar.removeEvents()
    }
  }

  render() {
    const element = this.notes.element
    const ctx = element.getContext('2d')!
    ctx.clearRect(0, 0, element.width, element.height)
    ctx.fillStyle = EditorSkin.Background_Light
    ctx.fillRect(0, 0, element.width, element.height)

    if (-1 === this.slideIndex) {
      this.needRepaint = false
      return
    }

    const factor = Factor_mm_to_pix * BrowserInfo.PixelRatio

    const w_mm = this.width
    const h_mm = this.height
    const w_px = (w_mm * factor) >> 0
    const h_px = (h_mm * factor) >> 0

    const drawCxt = getCanvasDrawer(ctx, w_px, h_px)

    drawCxt.saveState()
    drawCxt.setCoordTransform(this.offsetX, enlargeByRetinaRatio(-this.scroll))
    drawCxt.updateTransform(1, 0, 0, 1, 0, 0)

    if (EditorSettings.isViewMode) {
      drawCxt.isInViewMode = true
    }

    // 渲染注释字符时不受 ZoomValue.Value 影响
    EditorUtil.FontKit.executeIgnoreZoom(() => {
      this.presentation.drawNotes(this.slideIndex, drawCxt)
    })

    this.needRepaint = false
    drawCxt.restoreState()
    drawCxt.resetState()
  }

  calculateState(slideIndex, width, height) {
    const needChangedSlide = this.slideIndex !== slideIndex
    this.slideIndex = slideIndex
    this.width = width
    this.height = height
    this.needRepaint = true

    const element = this.notes.element
    const settings = new ScrollBarSettings()
    settings.screenWidth = element.width
    settings.screenHeight = element.height
    settings.vscrollDistPerTime = 45
    settings.hscrollDistPerTime = 45
    settings.contentWidth = 1
    settings.contentHeight =
      2 * this.offsetY + ((height * Factor_mm_to_pix) >> 0)
    settings.minHeight = 5
    settings.bgColor = EditorSkin.Background_Light
    settings.screenWidth = shrinkByRetinaRatio(settings.screenWidth)
    settings.screenHeight = shrinkByRetinaRatio(settings.screenHeight)

    this.maxOfScroll = Math.max(
      0,
      settings.contentHeight - settings.screenHeight
    )
    if (this.scroll > this.maxOfScroll) this.scroll = this.maxOfScroll

    this.notesScroll.elements[
      DOM_ID.MainContainer.NotesContainer.ScrollContainer.Scroll
    ].style.height = settings.contentHeight + 'px'

    if (this.scrollBar) {
      this.scrollBar.resetBySettings(settings, 'v')
      if (needChangedSlide) this.scrollBar.scrollToY(0)
    } else {
      this.scrollBar = new ScrollBar(this.notesScroll.element, settings)

      this.scrollBar.onMouseLock = (evt: PointerEvent) => {
        this.pointerEvtInfo.onDown(evt, true)
      }
      this.scrollBar.onUnLockMouse = (evt: PointerEvent) => {
        this.pointerEvtInfo.onUp(evt)
      }

      this.scrollBar.bind('scrollV', (evt) => {
        this.scroll =
          ((this.maxOfScroll * evt.scrollDest) / Math.max(evt.maxScrollY, 1)) >>
          0
        this.needRepaint = true

        this.editorUI.renderingController.onUpdateEditLayer()
      })
    }
  }

  onDragSplitter() {
    if (
      !EditorSettings.isNotesFeatureEnabled &&
      !EditorSettings.isNotesEnabled
    ) {
      return
    }

    const control = this.control
    control.bounds.height = splitterStates.notesSplitterPos

    if (EditorSettings.isNotesEnabled) {
      if (control.bounds.height < 1) {
        control.bounds.height = 1
      }
    }

    if (splitterStates.notesSplitterPos <= 1) {
      if (EditorSettings.isNotesEnabled) this.editorUI.setNotesEnable(false)
      this.editorUI.editorKit.emitEvent(EventNames.Evt_onNotesShow, false)
    } else {
      if (!EditorSettings.isNotesEnabled) this.editorUI.setNotesEnable(true)
      this.editorUI.editorKit.emitEvent(EventNames.Evt_onNotesShow, true)
    }
  }

  checkRender() {
    if (this.needRepaint && EditorSettings.isNotesEnabled) this.render()
  }

  prepareRender() {
    this.needRepaint = true
  }

  onMouseDown(e: PointerEvent) {
    const editorUI = this.editorUI
    if (-1 === editorUI.renderingController.curSlideIndex) return

    this.pointerEvtInfo.onDown(e, true)

    if (!BrowserInfo.isMobile) {
      editorUI.focusInput()
      stopEvent(e)
    }

    this.presentation.setFocusTargetType(FocusTarget.EditorView)
    if (!EditorSettings.isNotesEnabled) return
    const pos = this.convertPixToInnerMMPos(
      this.pointerEvtInfo.x,
      this.pointerEvtInfo.y
    )

    checkNotesTriggerSelectScroll(this)

    const needRepaint = editorUI.renderingController.checkPlaceHolder(
      pos,
      'mouseDown'
    )
    if (needRepaint === true) {
      stopEvent(e)
      return
    }

    handleMouseDownForNotes(editorUI, this.pointerEvtInfo, pos.x, pos.y)
  }

  onMouseMove(e?: PointerEvent) {
    const editorUI = this.editorUI
    if (-1 === editorUI.renderingController.curSlideIndex) return

    if (e) this.pointerEvtInfo.onMove(e)
    if (!EditorSettings.isNotesEnabled) return

    const pos = this.convertPixToInnerMMPos(
      this.pointerEvtInfo.x,
      this.pointerEvtInfo.y
    )
    const needRepaint = editorUI.renderingController.checkHoverEffect(pos)
    if (needRepaint === true) {
      return
    }
    handleMouseMoveForNotes(editorUI, this.pointerEvtInfo, pos.x, pos.y)
  }

  onMouseUp(e: PointerEvent) {
    const editorUI = this.editorUI
    if (-1 === editorUI.renderingController.curSlideIndex) return

    this.pointerEvtInfo.onUp(e)

    checkNotesClearSelectScroll(this)
    if (!EditorSettings.isNotesEnabled) return

    const pos = this.convertPixToInnerMMPos(
      this.pointerEvtInfo.x,
      this.pointerEvtInfo.y
    )

    const needRepaint = editorUI.renderingController.checkPlaceHolder(
      pos,
      'mouseUp'
    )
    if (needRepaint === true) {
      return
    }
    handleMouseUpForNotes(editorUI, this.pointerEvtInfo, pos.x, pos.y)

    editorUI.isLockMouse = false
    this.editorUI.checkMobileInput()
  }

  onMouseWheel({ eventObj: e, origin }: DomWheelEvt) {
    if (false === this.editorUI.editorKit.isEditorUIInitComplete) return

    let ctrl = false
    if (origin.metaKey !== undefined) ctrl = origin.ctrlKey || origin.metaKey
    else ctrl = origin.ctrlKey

    if (true === ctrl) {
      stopEvent(origin)
      return false
    }

    this.editorUI.textCursor.setCheckScroll(false)

    const deltaY = (e.deltaY ?? 0) >> 0

    if (0 !== deltaY) this.scrollBar!.scrollBy(0, deltaY)

    const event = getDefaultEvent(this)
    this.onMouseMove(event as PointerEvent)

    stopEvent(origin)
    return false
  }

  scrollByDelta(deltaY: number, e?: PointerEvent) {
    if (deltaY === 0 || !this.scrollBar) return

    this.scrollBar!.scrollBy(0, deltaY)
    const event = getDefaultEvent(this)
    if (e) stopEvent(e)
    this.onMouseMove(event as PointerEvent)
  }

  onSelectScroll = () => {
    if (false === this.editorUI.editorKit.isEditorUIInitComplete) return

    const _y =
      this.pointerEvtInfo.y -
      this.editorUI.y -
      ((this.control.PositionInPx.T * Factor_mm_to_pix + 0.5) >> 0)

    const minY = 0
    const maxY = this.notes.PositionInPx.B * Factor_mm_to_pix

    let scrollY = 0
    if (_y < minY) {
      let delta = 30
      if (20 > minY - _y) delta = 10

      scrollY = -delta
    } else if (_y > maxY) {
      let delta = 30
      if (20 > _y - maxY) delta = 10

      scrollY = delta
    }

    if (0 !== scrollY) {
      this.scrollBar!.scrollByDeltaY(scrollY)
      this.onMouseMove()
    }
  }

  calcOnResize() {
    if (this.presentation) {
      if (!this.presentation.onResizeForNotes()) {
        this.calculateState(this.slideIndex, this.width, this.height)
        getAndUpdateCursorPositionForPresentation(this.presentation)
      }
    }
  }

  onResize() {
    const editorUI = this.editorUI
    if (EditorSettings.isNotesEnabled) {
      let { notesSplitterPos, notesSplitterPosMin } = splitterStates
      const { height: editorHeight, mainContent } = editorUI
      const pos = editorHeight - ((notesSplitterPos * Factor_mm_to_pix) >> 0)
      const minHeight = 30 * Factor_mm_to_pix
      if (pos < minHeight) {
        notesSplitterPos = (editorUI.height - minHeight) / Factor_mm_to_pix
        if (notesSplitterPos < notesSplitterPosMin) {
          notesSplitterPos = 1
          splitterStates.updateNotesSplitterPos(notesSplitterPos)
        }

        if (notesSplitterPos <= 1) {
          this.notes.element.style.display = 'none'
          this.notesScroll.element.style.display = 'none'
        } else {
          this.notes.element.style.display = 'block'
          this.notesScroll.element.style.display = 'block'
        }

        mainContent.bounds.B = notesSplitterPos + EditorSkin.Width_InMM_Splitter
        mainContent.bounds.isPxB = true
        this.control.bounds.height = notesSplitterPos
      }
    }
  }

  getPixWidth() {
    let w = this.notes.element.width - enlargeByRetinaRatio(30)
    if (w < 10) w = 10
    w = shrinkByRetinaRatio(w)
    return w / Factor_mm_to_pix
  }

  getStartX() {
    return (
      (EditorSettings.isRTL
        ? 0
        : enlargeByRetinaRatio(this.notes.PositionInPx.L * Factor_mm_to_pix)) +
      this.offsetX
    )
  }

  convertPixToInnerMMPos(mouseX, mousesY) {
    const editorUI = this.editorUI
    const offsetL =
      (editorUI.x +
        ((editorUI.mainContainer.PositionInPx.L + this.notes.PositionInPx.L) *
          Factor_mm_to_pix +
          0.5)) >>
      0
    const offsetT =
      editorUI.y + ((this.control.PositionInPx.T * Factor_mm_to_pix + 0.5) >> 0)
    const x =
      (mouseX - offsetL - shrinkByRetinaRatio(this.offsetX)) * Factor_pix_to_mm
    const y = (mousesY - offsetT + this.scroll) * Factor_pix_to_mm
    return { x, y }
  }
}

function getDefaultEvent(notes: NotesComponent) {
  return {
    pageX: notes.pointerEvtInfo.x,
    pageY: notes.pointerEvtInfo.y,
    clientX: notes.pointerEvtInfo.x,
    clientY: notes.pointerEvtInfo.y,
    altKey: notes.pointerEvtInfo.altKey,
    shiftKey: notes.pointerEvtInfo.shiftKey,
    ctrlKey: notes.pointerEvtInfo.ctrlKey,
    metaKey: notes.pointerEvtInfo.ctrlKey,
    target: notes.pointerEvtInfo.target,
  }
}
