import { EditorUI } from '../EditorUI'
import { valuesOfDict } from '../../../../liber/pervasive'
import {
  listenPointerEvents,
  PointerEventArg,
  PointerEventInfo,
  PointerEventType,
  stopEvent,
} from '../../../common/dom/events'
import { Dom, toElement } from '../../../common/dom/vdom'
import { toInt } from '../../../common/utils'
import { EditorSkin } from '../../../core/common/const/drawing'
import {
  Factor_mm_to_pix,
  Factor_pix_to_mm,
} from '../../../core/common/const/unit'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { EditorUtil } from '../../../globals/editor'
import { splitterStates } from '../splitterStates'
import {
  ANCHOR_LEFT,
  ANCHOR_RIGHT,
  ANCHOR_TOP,
  DOM_ID,
  DOM_STYLE,
} from '../controls/const'
import { BrowserInfo } from '../../../common/browserInfo'
import { SplitterBox } from './splitterBox'

/** 分割线 dom 内侧仍可以拖拽的范围(外侧会被 editorUI handle), 方便操作 */

const SplitterDragableAreaPixSizeWithScrollbar = 3
const SplitterDragableAreaPixSize = BrowserInfo.isTablet
  ? SplitterDragableAreaPixSizeWithScrollbar + 7
  : SplitterDragableAreaPixSizeWithScrollbar

/** 水平垂直以分割线的方向而非操作的方向划分，如 note 分隔线垂直调整，但属于水平分割线 */
export const SplitterStatus = {
  Unset: 0,
  Vertical: 1,
  Horizontal: 2,
} as const

export type SplitterStatusValus = valuesOfDict<typeof SplitterStatus>
export class Splitters {
  editorUI: EditorUI
  dom?: HTMLDivElement
  splitterBox: SplitterBox
  status: SplitterStatusValus = SplitterStatus.Unset
  isHovered = false
  pointerEvtInfo: PointerEventInfo = new PointerEventInfo()
  private _unlistenEvents: undefined | (() => void)

  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI
    this.splitterBox = new SplitterBox(editorUI)
  }

  handlePointerEvent<T extends PointerEventType>(et: T, e: PointerEventArg<T>) {
    switch (et) {
      case 'pointerdown': {
        this.onMouseDown(e as PointerEvent)
        break
      }
      case 'pointermove': {
        this.onMouseMove(e as PointerEvent)
        break
      }
      case 'pointerup': {
        this.onMouseUp(e as PointerEvent)
        break
      }
    }
  }

  initEvents() {
    const editorUI = this.editorUI
    const evts: PointerEventType[] = ['pointerdown', 'pointermove', 'pointerup']
    this._unlistenEvents = listenPointerEvents(
      editorUI.editorControl.element,
      evts,
      this,
      true
    )
  }

  removeEvents() {
    if (this._unlistenEvents) {
      this._unlistenEvents()
    }
  }

  onMouseDown(e: PointerEvent) {
    const editorUI = this.editorUI
    if (false === editorUI.editorKit.isEditorUIInitComplete) return
    if (EditorUtil.inputManager?.checkAndClearIME()) return
    if (this.status !== SplitterStatus.Unset) return false
    if ((e.target as HTMLDivElement)?.id === DOM_ID.SplitterBox) {
      this.splitterBox.onMouseDown(e)
    }
    let isHit = false

    const originClicks = this.pointerEvtInfo.clicks
    this.pointerEvtInfo.onDown(e, true)
    this.pointerEvtInfo.clicks = originClicks
    const {
      thumbnailSplitterStartX,
      thumbnailSplitterEndX,
      notesSplitterStartY,
      notesSplitterEndY,
    } = this.getSplitterState()
    const { x, y } = this.pointerEvtInfo
    const { posX, posY } = convertGlobalToInnerCoords(x, y, editorUI)

    if (
      EditorSettings.isEnableThumbnail &&
      posX >=
        thumbnailSplitterStartX - SplitterDragableAreaPixSizeWithScrollbar &&
      posX <= thumbnailSplitterEndX + SplitterDragableAreaPixSize &&
      posY >= 0 &&
      posY <= editorUI.height
    ) {
      editorUI.mouseCursor.setCursorType('col-resize')
      this.dom = createSplitterElm(SplitterStatus.Vertical, editorUI)
      this.status = SplitterStatus.Vertical
      isHit = true
    }
    if (
      posX >= thumbnailSplitterEndX &&
      posX <= editorUI.width &&
      posY >= notesSplitterStartY - SplitterDragableAreaPixSizeWithScrollbar &&
      posY <= notesSplitterEndY + SplitterDragableAreaPixSize
    ) {
      editorUI.editorControl.element.style.cursor = 'row-resize'
      editorUI.mouseCursor.setCursorType('row-resize')
      this.dom = createSplitterElm(SplitterStatus.Horizontal, editorUI)
      this.status = SplitterStatus.Horizontal
      isHit = true
    } else {
      editorUI.editorControl.element.style.cursor = 'default'
    }

    if (isHit) {
      this.appendToElm(editorUI.editorControl.element)
      if (editorUI.mainContainer && editorUI.mainContainer.element) {
        editorUI.mainContainer.element.style.pointerEvents = 'none'
      }
      editorUI.thumbnailsManager.onMouseDownOutside()
      stopEvent(e)
    }
  }

  getSplitterState = () => {
    const editorUI = this.editorUI
    const { thumbnailSplitterPos, notesSplitterPos } = splitterStates
    const mmSplitterWidth = EditorSkin.Width_InMM_Splitter
    const factor = Factor_mm_to_pix
    const [thumbnailSplitterStartX, thumbnailSplitterEndX] = [
      thumbnailSplitterPos * Factor_mm_to_pix,
      (thumbnailSplitterPos + mmSplitterWidth) * factor,
    ]
    const [notesSplitterStartY, notesSplitterEndY] = [
      editorUI.height - (notesSplitterPos + mmSplitterWidth) * factor,
      editorUI.height - notesSplitterPos * factor,
    ]
    return {
      thumbnailSplitterStartX,
      thumbnailSplitterEndX,
      notesSplitterStartY,
      notesSplitterEndY,
    }
  }

  onMouseMove(e: PointerEvent) {
    const editorUI = this.editorUI
    if (false === editorUI.editorKit.isEditorUIInitComplete) return
    if ((e.target as HTMLDivElement)?.id === DOM_ID.SplitterBox) {
      this.splitterBox.onMouseMove(e)
    }

    let isHit = false
    const { x, y } = this.pointerEvtInfo
    const { posX, posY, isMouseYLeave } = convertGlobalToInnerCoords(
      x,
      y,
      editorUI
    )

    this.pointerEvtInfo.onMove(e)

    // Hover
    if (null == this.dom) {
      const {
        thumbnailSplitterStartX,
        thumbnailSplitterEndX,
        notesSplitterStartY,
        notesSplitterEndY,
      } = this.getSplitterState()

      if (
        EditorSettings.isEnableThumbnail &&
        posX >= thumbnailSplitterStartX - SplitterDragableAreaPixSize &&
        posX <= thumbnailSplitterEndX + SplitterDragableAreaPixSize &&
        posY >= 0 &&
        posY <= notesSplitterStartY
      ) {
        editorUI.editorControl.element.style.cursor = 'col-resize'
        editorUI.mouseCursor.setCursorType('col-resize')
        this.isHovered = true
      } else if (
        posX >= thumbnailSplitterEndX &&
        posX <= editorUI.width &&
        posY >= notesSplitterStartY - SplitterDragableAreaPixSize &&
        posY <= notesSplitterEndY + SplitterDragableAreaPixSize
      ) {
        editorUI.editorControl.element.style.cursor = 'row-resize'
        editorUI.mouseCursor.setCursorType('row-resize')
        this.isHovered = true
      } else {
        if (this.isHovered === true) {
          editorUI.editorControl.element.style.cursor = 'default'
          editorUI.mouseCursor.setCursorType('default')
        }
        this.isHovered = false
      }
    }
    // drag spliter
    else {
      const {
        thumbnailSplitterPosMin,
        thumbnailSplitterPosMax,
        notesSplitterPosMin,
        notesSplitterPosMax,
      } = splitterStates
      // resize thumbnail
      if (SplitterStatus.Vertical === this.status) {
        const minX = toInt(thumbnailSplitterPosMin * Factor_mm_to_pix)
        const maxX = toInt(thumbnailSplitterPosMax * Factor_mm_to_pix)
        let _posX = posX
        if (_posX > maxX) _posX = maxX
        else if (_posX < minX / 2) _posX = 0
        else if (_posX < minX) _posX = minX
        editorUI.editorControl.element.style.cursor = 'col-resize'
        editorUI.mouseCursor.setCursorType('col-resize')
        if (EditorSettings.isRTL) {
          this.dom.style.right = _posX + 'px'
        } else {
          this.dom.style.left = _posX + 'px'
        }
      }
      // resize note
      else {
        const maxY =
          editorUI.height - toInt(notesSplitterPosMin * Factor_mm_to_pix)
        let minY =
          editorUI.height - toInt(notesSplitterPosMax * Factor_mm_to_pix)

        if (minY < 30 * Factor_mm_to_pix) minY = 30 * Factor_mm_to_pix

        const _c = toInt(notesSplitterPosMin * Factor_mm_to_pix)
        let _posY = posY
        if (_posY > maxY + _c / 2) _posY = editorUI.height
        else if (_posY > maxY) _posY = maxY
        else if (_posY < minY) _posY = minY

        this.dom.style.top =
          _posY -
          toInt(EditorSkin.Width_InMM_Splitter * Factor_mm_to_pix) +
          'px'
      }
      isHit = true
    }
    if (isHit) {
      stopEvent(e)
      if (isMouseYLeave) {
        this.onMouseUp(e)
      }
    }
  }
  onMouseUp(e: PointerEvent) {
    const editorUI = this.editorUI
    if ((e.target as HTMLDivElement)?.id === DOM_ID.SplitterBox) {
      this.splitterBox.onMouseMove(e)
    }

    if (
      false === editorUI.editorKit.isEditorUIInitComplete ||
      this.dom == null
    ) {
      return
    }
    editorUI.mouseCursor.unLock()

    if (editorUI.mainContainer && editorUI.mainContainer.element) {
      editorUI.mainContainer.element.style.pointerEvents = ''
    }
    editorUI.thumbnailsManager.onMouseUpOutside()

    let isHit = false
    this.pointerEvtInfo.onUp(e)
    if (null != this.dom) {
      const posX = parseInt(
        EditorSettings.isRTL ? this.dom.style.right : this.dom.style.left
      )

      if (this.status === SplitterStatus.Vertical) {
        const _posX = posX * Factor_pix_to_mm
        if (Math.abs(splitterStates.thumbnailSplitterPos - _posX) > 1) {
          splitterStates.updateThumbnailSplitterPos(_posX)
        }
      } else {
        const posY = parseInt(this.dom.style.top)
        const _posY =
          (editorUI.height - posY) * Factor_pix_to_mm -
          EditorSkin.Width_InMM_Splitter
        if (Math.abs(splitterStates.notesSplitterPos - _posY) > 1) {
          splitterStates.updateNotesSplitterPos(_posY)
        }
      }
      this.removeSplitterDiv(editorUI.editorControl.element)
      isHit = true
    }
    if (isHit) {
      this.onResize()
      stopEvent(e)
    }
  }

  onResize() {
    const editorUI = this.editorUI
    const { mainContainer, thumbnailsManager, mainContent, notesView } =
      editorUI
    const isRTL = editorUI.thumbnailsManager.isRTL
    const { thumbnailSplitterPos, notesSplitterPos } = splitterStates

    if (!EditorSettings.isEnableThumbnail) {
      mainContainer.anchor = ANCHOR_LEFT | ANCHOR_TOP | ANCHOR_RIGHT
      mainContainer.bounds.L = 0
      mainContainer.bounds.R = 0
    } else {
      mainContainer.bounds.L = isRTL ? 0 : thumbnailSplitterPos
      mainContainer.bounds.R = isRTL ? thumbnailSplitterPos : 0
    }

    mainContent.bounds.B = EditorSettings.isNotesEnabled
      ? notesSplitterPos + EditorSkin.Width_InMM_Splitter
      : 1000
    mainContent.bounds.isPxB = EditorSettings.isNotesEnabled

    if (!isRTL) {
      mainContainer.element.style.borderLeft =
        '1px solid ' + EditorSkin.Color_Splitter
    } else {
      mainContainer.element.style.borderRight =
        '1px solid ' + EditorSkin.Color_Splitter
    }
    if (!EditorSettings.isEnableThumbnail) {
      mainContainer.element.style.borderLeft = '0'
      mainContainer.element.style.borderRight = '0'
    }

    this.splitterBox.onResize()
    thumbnailsManager.onDragSplitter()
    notesView.onDragSplitter()
    editorUI.resize()
  }

  toggleThumbnailDisplay() {
    if (!EditorSettings.isEnableThumbnail) {
      return
    }
    if (splitterStates.thumbnailSplitterPos === 0) {
      EditorSettings.isShowThumbnail = true
      splitterStates.resetThumbnailSplitterPos()
    } else {
      EditorSettings.isShowThumbnail = false
      splitterStates.updateThumbnailSplitterPos(0)
    }
    this.onResize()

    this.editorUI.editorKit.emitThumbnailsShow()
  }

  private appendToElm(elm: HTMLElement) {
    if (this.dom) {
      elm.appendChild(this.dom)
    }
  }
  private removeSplitterDiv(elm: HTMLElement) {
    if (this.dom) {
      elm.removeChild(this.dom)
      this.dom = undefined
      this.status = SplitterStatus.Unset
    }
  }
}

function createSplitterElm(
  splitterType: Exclude<SplitterStatusValus, typeof SplitterStatus.Unset>,
  editorUI: EditorUI
) {
  const elm = toElement(
    Dom.div(`#${DOM_ID.Splitter}`, {
      style: DOM_STYLE.Splitter,
      attrs: { ['contentEditable']: 'false' },
    })
  ) as HTMLDivElement

  const lineWidthInMM = EditorSkin.Width_InMM_Splitter
  const { thumbnailSplitterPos, notesSplitterPos } = splitterStates
  if (splitterType === SplitterStatus.Vertical) {
    if (EditorSettings.isRTL) {
      elm.style.right = toInt(thumbnailSplitterPos * Factor_mm_to_pix) + 'px'
    } else {
      elm.style.left = toInt(thumbnailSplitterPos * Factor_mm_to_pix) + 'px'
    }

    elm.style.top = '0px'
    elm.style.width = toInt(lineWidthInMM * Factor_mm_to_pix) + 'px'
    elm.style.height = editorUI.height + 'px'
  }
  // 操作 notes 水平分割线
  if (splitterType === SplitterStatus.Horizontal) {
    const left = editorUI.thumbnailsManager.isRTL ? 0 : thumbnailSplitterPos
    elm.style.left = toInt((left + lineWidthInMM) * Factor_mm_to_pix) + 'px'
    elm.style.top =
      editorUI.height -
      toInt((notesSplitterPos + lineWidthInMM) * Factor_mm_to_pix) +
      1 +
      'px'
    elm.style.width =
      editorUI.width -
      toInt((thumbnailSplitterPos + lineWidthInMM) * Factor_mm_to_pix) +
      'px'
    elm.style.height = toInt(lineWidthInMM * Factor_mm_to_pix) + 'px'
  }

  return elm
}

function convertGlobalToInnerCoords(mouseX, mouseY, editorUI: EditorUI) {
  const mainContainerH = editorUI.mainContainer.getPxHeight()
  const totalH = mainContainerH + editorUI.y - SplitterDragableAreaPixSize
  const isMouseYLeave = mouseY >= totalH + 20
  const posX = EditorSettings.isRTL
    ? editorUI.width + editorUI.x - mouseX
    : mouseX - editorUI.x
  const posY = mouseY - editorUI.y
  return { posX, posY, isMouseYLeave }
}
