import { valuesOfDict } from '../../../liber/pervasive'
import { Factor_pix_to_mm } from '../../core/common/const/unit'
import { SCROLLBAR_WIDTH_PX } from './splitterStates'

export const SCROLLBAR_WIDTH_MM = SCROLLBAR_WIDTH_PX * Factor_pix_to_mm

export const KeyEvent_NoPrevent = 0x0000
export const KeyEvent_DefaultPrevent = 0x0001
export const KeyEvent_PreventPress = 0x0002
export const KeyEvent_PreventAll = 0xffff
export const SCROLL_DEBOUNCE_LIMIT = 30

export const FitMode = {
  Custom: 0,
  Width: 1,
  Window: 2,
} as const

export type FitModeValue = (typeof FitMode)[keyof typeof FitMode]

export const StatusOfFormatPainter = {
  OFF: 0,
  ON: 1,
  MULTIPLE: 2,
} as const

export type StatusOfFormatPainterValues = valuesOfDict<
  typeof StatusOfFormatPainter
>
