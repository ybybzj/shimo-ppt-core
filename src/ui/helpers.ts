import { BrowserInfo } from '../common/browserInfo'
import { Factor_mm_to_pix } from '../core/common/const/unit'
import { ZoomValue } from '../core/common/zoomValue'
import { ClipRect } from '../core/graphic/type'
import { SlideDrawerCONST } from './rendering/Drawer/const'
import { EditorUI } from './editorUI/EditorUI'
import { WithBounds } from '../core/graphic/Bounds'

export function getClipRectForPPTShow(editorUI: EditorUI): ClipRect {
  const { width, height } = editorUI.showManager.canvas!
  const { width: wMM, height: hMM } = editorUI.presentation

  const displayAspect = width / height
  const presAspect = wMM / hMM

  let h = height
  let w = h * presAspect
  let t = 0
  let l = (width - w) >> 1
  if (presAspect > displayAspect) {
    w = width
    h = w / presAspect
    l = 0
    t = (height - h) >> 1
  }
  return {
    x: l >> 0,
    y: t >> 0,
    w: w >> 0,
    h: h >> 0,
  }
}

export interface EditorDemensionInfo {
  slideWidth: number
  slideHeight: number
  canvasWidth: number
  canvasHeight: number
  centerX: number
  centerXOfSlide: number
  horizontalLeft: number
  horizontalRight: number
  centerY: number
  centerYOfSlide: number
  verticalTop: number
  verticalBottom: number
}

export function calculateDimensionInfo(
  editorUI: EditorUI,
  zoom: number,
  bounds: WithBounds,
  targetCanvas?: HTMLCanvasElement
): EditorDemensionInfo {
  const factor = (zoom * Factor_mm_to_pix) / 100
  const logicWidth = editorUI.presentation.width
  const logicHeight = editorUI.presentation.height
  const slideWidth = (factor * logicWidth) >> 0
  const slideHeight = (factor * logicHeight) >> 0

  let [srcW, srcH] = targetCanvas
    ? [targetCanvas.width, targetCanvas.height]
    : [editorUI.editorView.getWidth(), editorUI.editorView.getHeight()]

  srcW = (srcW / BrowserInfo.PixelRatio) >> 0
  srcH = (srcH / BrowserInfo.PixelRatio) >> 0

  let { minX, minY, maxX, maxY } = bounds
  minX = (minX / BrowserInfo.PixelRatio) >> 0
  minY = (minY / BrowserInfo.PixelRatio) >> 0
  maxX = (maxX / BrowserInfo.PixelRatio) >> 0
  maxY = (maxY / BrowserInfo.PixelRatio) >> 0

  const centerX = (srcW / 2) >> 0
  const centerXOfSlide = ((factor * logicWidth) / 2) >> 0
  const horizontalLeft = Math.min(
    0,
    centerX - (centerXOfSlide - minX) - SlideDrawerCONST.BORDER
  )
  const horizontalRight = Math.max(
    srcW - 1,
    centerX + (maxX - centerXOfSlide) + SlideDrawerCONST.BORDER
  )

  const centerY = (srcH / 2) >> 0
  const centerYOfSlide = ((factor * logicHeight) / 2) >> 0
  const verticalTop = Math.min(
    0,
    centerY - (centerYOfSlide - minY) - SlideDrawerCONST.BORDER
  )
  const verticalBottom = Math.max(
    srcH - 1,
    centerY + (maxY - centerYOfSlide) + SlideDrawerCONST.BORDER
  )

  return {
    slideWidth: slideWidth,
    slideHeight: slideHeight,
    canvasWidth: srcW,
    canvasHeight: srcH,
    centerX,
    centerXOfSlide: centerXOfSlide,
    horizontalLeft: horizontalLeft,
    horizontalRight: horizontalRight,
    centerY,
    centerYOfSlide: centerYOfSlide,
    verticalTop: verticalTop,
    verticalBottom: verticalBottom,
  }
}

export function getSlideWH(
  editorUI: EditorUI,
  scale?: number
): { w_mm: number; h_mm: number; w_px: number; h_px: number } {
  const zoom = scale != null ? ZoomValue.value * scale : ZoomValue.value
  const presentation = editorUI.presentation
  const factor = ((zoom * Factor_mm_to_pix) / 100) * BrowserInfo.PixelRatio

  const w_mm = presentation.width
  const h_mm = presentation.height
  const w_px = (w_mm * factor) >> 0
  const h_px = (h_mm * factor) >> 0
  return {
    w_mm,
    h_mm,
    w_px,
    h_px,
  }
}

export function getSlideCurrentScale(editorUI: EditorUI): {
  sx: number
  sy: number
} {
  const presentation = editorUI.presentation
  const factor =
    ((ZoomValue.value * Factor_mm_to_pix) / 100) * BrowserInfo.PixelRatio

  const w_mm = presentation.width
  const h_mm = presentation.height
  const w_px = w_mm * factor
  const h_px = h_mm * factor
  return {
    sx: w_px / w_mm,
    sy: h_px / h_mm,
  }
}
