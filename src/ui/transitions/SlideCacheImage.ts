export class SlideImageCache {
  slideImage: any
  color: { r: number; g: number; b: number }
  constructor() {
    this.slideImage = null
    this.color = { r: 0, g: 0, b: 0 }
  }
}
