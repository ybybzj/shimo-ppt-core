import { Dict, Nullable } from '../../../liber/pervasive'
import { BrowserInfo } from '../../common/browserInfo'
import { CanvasDrawer } from '../../core/graphic/CanvasDrawer'

import { ClipRect } from '../../core/graphic/type'

import { Slide } from '../../core/Slide/Slide'

import { SlideElement } from '../../core/SlideElement/type'
import { EditorUI } from '../editorUI/EditorUI'
import { getClipRectForPPTShow } from '../helpers'
import {
  playNext,
  ShapeAnimationPlayer,
  ShapeAnimationUpdater,
  StopPlayStatus,
  playPrev,
  makeShapeAnimationPlayer,
  resetPlay,
  setPlayerCursor,
  ShapeAnimationDrawer,
  isPlayerPlaying,
  isPlayerAutoPlay,
  resetPlayerCursor,
  isPlayerCursorAtEnd,
  drawFrame,
  resetPlayerCursorAtEnd,
  stopPlay,
  stopPlayerAutopPlay,
  isPlayerCursorAtBeginning,
  getPlayerCursor,
} from '../../re/export/ShapeAnimation'
import { ShowManager } from './ShowManager'
import { drawAnimShape, drawStaticOfSlide, makeDrawer } from './animation'
import { each } from '../../../liber/l/each'
import { isAutoTransition } from './utils'
import { logger } from '../../lib/debug/log'
import {
  getSlideElementPreview,
  ShapeImageData,
} from '../../core/render/toImage'

export class AnimationManager {
  editorUI: EditorUI
  showManager: ShowManager
  animationPlayer?: ShapeAnimationPlayer
  private spsImgCache: Dict<ShapeImageData> = {}
  private staticImgCache: Dict<HTMLCanvasElement> = {}
  private drawInfo: {
    rect: ClipRect
    clearRect: ClipRect
    scale: number
    slide?: Slide
    ctx?: CanvasRenderingContext2D
  } = {
    rect: { x: 0, y: 0, h: 0, w: 0 },
    scale: 1.0,
    clearRect: { x: 0, y: 0, h: 0, w: 0 },
  }
  private playersCache: SlideAnimPlayerMap
  drawAnim?: ShapeAnimationDrawer
  private disableAutoPlay: boolean = false

  get presentation() {
    return this.editorUI.editorKit.presentation
  }

  constructor(editorUI: EditorUI, showManager: ShowManager) {
    this.editorUI = editorUI
    this.showManager = showManager
    this.playersCache = new SlideAnimPlayerMap(this)
  }

  next() {
    if (this.animationPlayer && this.drawAnim) {
      playNext(this.animationPlayer, this.drawAnim)
    }
  }

  prev() {
    if (this.animationPlayer) {
      this.showManager.checkSlideTimer.cancel()
      if (this.drawAnim) {
        playPrev(this.animationPlayer, this.drawAnim)
      }
    }
  }

  end = (stopStatus: StopPlayStatus) => {
    logger.log(`[${stopStatus}]animation stopped~~~`)

    if (stopStatus === 'StopAtEnd') {
      const { slide } = this.drawInfo
      if (
        !this.showManager.checkSlideTimer.isTiming() &&
        slide &&
        isAutoTransition(this.editorUI, slide)
      ) {
        this.showManager.checkAdvanceAfterTransition(slide, () => {
          this.drawAnim = undefined
          this.animationPlayer = undefined
        })
      } else {
        this.showManager.checkSlideTimer.cancel()
        this.drawAnim = undefined
        this.animationPlayer = undefined
        this.showManager.nextSlide()
      }
    } else if (stopStatus === 'StopAtBeginning') {
      if (
        !this.showManager.isFirstUnhiddenSlide() ||
        this.showManager.isLoop()
      ) {
        this.animationPlayer = undefined
        this.drawAnim = undefined
      }
      this.showManager.prevSlide()
    }
  }
  resetPlayer(slide: Slide) {
    if (slide) {
      const player = this.playersCache.get(slide)
      if (player) {
        resetPlayerCursor(player)
      }
    }
  }

  stopAutoPlay(isStopAutoPlay: boolean) {
    this.disableAutoPlay = isStopAutoPlay
    if (this.animationPlayer) {
      stopPlayerAutopPlay(this.animationPlayer, isStopAutoPlay)
    }
  }

  stop() {
    if (this.isPlaying() && this.animationPlayer && this.drawAnim) {
      stopPlay(this.animationPlayer, this.drawAnim)
    }
  }

  drawFrame(slide: Slide, isBackward = false, animationCursor?: string) {
    this.stopAnim()
    this.preStartPPTShow()
    if (slide) {
      this.animationPlayer = this.playersCache.get(slide)
      if (this.animationPlayer) {
        stopPlayerAutopPlay(this.animationPlayer, this.disableAutoPlay)
        this.drawAnim = makeDrawer(this.editorUI, {
          drawInfo: this.drawInfo,
          drawShape: this.drawShape,
          drawStatic: this.drawStatic,
        })
        if (animationCursor != null) {
          setPlayerCursor(this.animationPlayer, animationCursor)
          drawFrame(this.animationPlayer, this.drawAnim)
        } else {
          if (isBackward === true) {
            resetPlayerCursorAtEnd(this.animationPlayer)
          } else {
            resetPlayerCursor(this.animationPlayer)
          }
        }
      }
    }
  }

  // backWard 退回，继续播放 or 固定尾帧
  start(slide: Slide, isBackward = false, animationCursor?: string) {
    if (slide == null) {
      return
    }
    this.drawFrame(slide, isBackward, animationCursor)
    if (this.animationPlayer) {
      const isAutoStart =
        !isBackward &&
        ((isPlayerCursorAtBeginning(this.animationPlayer) &&
          slide.animation.autoStart) ||
          isPlayerAutoPlay(this.animationPlayer))
      // 播放完毕则检查是否自动切页，否则检查是否自动播放(退回不自动播放)
      if (isPlayerCursorAtEnd(this.animationPlayer) && !isBackward) {
        this.showManager.checkAdvanceAfterTransition(slide)
      } else if (isAutoStart && this.drawAnim) {
        playNext(this.animationPlayer, this.drawAnim)
      }
    } else {
      if (!isBackward) {
        this.showManager.checkAdvanceAfterTransition(slide)
      }
    }
  }

  getCursorStateForSlide(slide: Slide): Nullable<string> {
    if (slide) {
      const animationPlayer = this.playersCache.get(slide)
      if (animationPlayer) {
        return getPlayerCursor(animationPlayer)
      }
    }
  }
  clear() {
    this.playersCache.reset()
    this.spsImgCache = {}
    this.staticImgCache = {}
  }

  isPlaying() {
    return this.animationPlayer && isPlayerPlaying(this.animationPlayer)
  }

  isPlayShapeAnimation() {
    return !!this.animationPlayer
  }

  getShapeImage(sp: SlideElement) {
    const { image } = getSlideElementPreview(sp, 1)
    return image
  }

  private stopAnim() {
    if (this.animationPlayer) {
      resetPlay(this.animationPlayer)
      this.animationPlayer = undefined
    }
  }

  private preStartPPTShow() {
    const { x, y, w, h } = getClipRectForPPTShow(this.editorUI)
    this.drawInfo.rect.x = (x / BrowserInfo.PixelRatio) >> 0
    this.drawInfo.rect.y = (y / BrowserInfo.PixelRatio) >> 0
    this.drawInfo.rect.w = (w / BrowserInfo.PixelRatio) >> 0
    this.drawInfo.rect.h = (h / BrowserInfo.PixelRatio) >> 0

    const element = this.editorUI.showManager.canvas

    const scale = w / this.presentation.width
    this.drawInfo.scale = scale

    this.drawInfo.clearRect = {
      x: 0,
      y: 0,
      w: element!.width,
      h: element!.height,
    }

    const ctx = element!.getContext('2d')!
    this.drawInfo.ctx = ctx

    const curSlideIndex = this.editorUI.showManager.currentSlideIndex
    const slide = this.presentation.slides[curSlideIndex]
    this.drawInfo.slide = slide
  }

  private drawShape = (
    sp: SlideElement,
    scale: number,
    g: CanvasDrawer,
    percentage: number,
    transformUpdater: ShapeAnimationUpdater
  ) => {
    drawAnimShape(sp, scale, g, percentage, transformUpdater, (creater) => {
      let cacheImgData = this.spsImgCache[sp.getId()]
      if (cacheImgData == null) {
        cacheImgData = creater()
        this.spsImgCache[sp.getId()] = cacheImgData
      }
      return cacheImgData
    })
  }

  private drawStatic = (slide: Slide, scale: number, g: CanvasDrawer) => {
    const rect = {
      x: this.drawInfo.rect.x * BrowserInfo.PixelRatio,
      y: this.drawInfo.rect.y * BrowserInfo.PixelRatio,
      w: this.drawInfo.rect.w * BrowserInfo.PixelRatio,
      h: this.drawInfo.rect.h * BrowserInfo.PixelRatio,
    }
    drawStaticOfSlide(slide, scale, g, rect, (creater) => {
      const slideId = slide.getId()
      let cacheImg = this.staticImgCache[slideId]
      if (cacheImg == null) {
        cacheImg = creater()
        this.staticImgCache[slideId] = cacheImg
      }
      return cacheImg
    })
  }
}

class SlideAnimPlayerMap {
  private map: Record<string, ShapeAnimationPlayer> = {}
  manager: AnimationManager
  constructor(manager: AnimationManager) {
    this.manager = manager
  }
  get(slide: Slide): ShapeAnimationPlayer | undefined {
    const id = slide.getId()

    if (this.map[id] == null) {
      const autoPlay = () => isAutoTransition(this.manager.editorUI, slide)

      const queue = slide.animation.getAnimQueue()
      // const queue = genAnimQueue(slide)
      const newPlayer =
        queue && makeShapeAnimationPlayer(queue, this.manager.end, autoPlay)
      if (newPlayer) {
        this.map[id] = newPlayer
      }
    }
    return this.map[id]
  }
  reset() {
    each((player) => {
      resetPlay(player)
    }, this.map)
    this.map = {}
  }
}
