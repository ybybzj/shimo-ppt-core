import { BrowserInfo } from '../../common/browserInfo'
import { Slide } from '../../core/Slide/Slide'
import { EditorSettings, SlideElementPos } from '../../exports/type'
import { SlideElement } from '../../core/SlideElement/type'
import { EditorUI } from '../editorUI/EditorUI'
import { ShowManager } from './ShowManager'

export function fillStyledRect(
  ctx: CanvasRenderingContext2D,
  color: string | { r: number; g: number; b: number },
  x: number,
  y: number,
  w: number,
  h: number
) {
  ctx.beginPath()
  ctx.fillStyle =
    typeof color === 'string'
      ? color
      : 'rgb(' + color.r + ',' + color.g + ',' + color.b + ')'
  ctx.fillRect(x, y, w, h)
  ctx.beginPath()
}

export function createCanvasImage(w: number, h: number) {
  const canvas = document.createElement('canvas')
  canvas.width = w
  canvas.height = h
  return canvas
}

export function isPlayingAnimation(editorUI: EditorUI): boolean {
  return (
    editorUI.transitionManager.isPlaying() ||
    editorUI.animationPreviewer.isPlaying()
  )
}

export function isAutoTransition(editorUI: EditorUI, slide: Slide): boolean {
  if (editorUI == null || slide == null) {
    return false
  }
  const presentation = editorUI.presentation

  const forceAutoTransition = editorUI.showManager.isForceAutoPlay()
  return (
    forceAutoTransition ||
    !!(
      presentation?.isUseTiming() &&
      slide.timing &&
      slide.timing.advanceAfter === true
    )
  )
}

const MinDist = 206
export function getSpPosInShow(
  showManager: ShowManager,
  slide: Slide,
  sp: SlideElement
): SlideElementPos {
  // const { left, top } = showManager.canvas!.getBoundingClientRect()
  const { x, y, w, h } = showManager.transitionManager.rect
  const P_R = BrowserInfo.PixelRatio
  const { l, t, r, b } = sp.bounds

  const [slideW, slideH] = [slide.width, slide.height]
  const [pctL, pctT] = [l / slideW, t / slideH]
  const [pctW, pctH] = [(r - l) / slideW, (b - t) / slideH]

  const [absX, absY] = [x / P_R + (w * pctL) / P_R, y / P_R + (h * pctT) / P_R]
  const [width, height] = [(w * pctW) / P_R, (h * pctH) / P_R]

  const adjust_absX =
    absX < MinDist
      ? MinDist
      : w / P_R - absX < MinDist
      ? w / P_R - MinDist
      : absX
  return {
    width,
    height,
    absX: EditorSettings.isRTL ? w / P_R - adjust_absX : adjust_absX,
    absY,
  }
}
