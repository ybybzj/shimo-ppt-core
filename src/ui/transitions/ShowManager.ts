import { BrowserInfo } from '../../common/browserInfo'
import {
  GlobalEvents,
  listenPointerEvents,
  PointerEventArg,
  PointerEventType,
  POINTER_BUTTON,
  stopEvent,
  getPointerEventPos,
  PointerEventInfo,
} from '../../common/dom/events'
import { Dom, toElement } from '../../common/dom/vdom'
import { isFiniteNumber, isNumber, throttle } from '../../common/utils'
import { EditorSettings } from '../../core/common/EditorSettings'
import { ImagePoolManager } from '../../core/graphic/imageCache'
import { Slide } from '../../core/Slide/Slide'
import {
  SlideTransition,
  TransitionTypes,
} from '../../core/Slide/SlideTransition'
import { zoomValueForFitToSlide } from '../../core/utilities/zoom'
import {
  Evt_onPPTShowAction,
  Evt_onPPTShowForceAutoPlay,
  Evt_onPPTShowPlayMode,
  Evt_viewAttachment,
  Evt_viewSourceImage,
} from '../../editor/events/EventNames'
import { EditorKit } from '../../editor/global'
import { translator } from '../../globals/translator'
import { EditorUI } from '../editorUI/EditorUI'
import {
  handleMouseDown,
  handleMouseMove,
  handleMouseUp,
} from '../editorUI/utils/pointers'
import { drawCacheImage } from './animation'
import { SlideImageCache } from './SlideCacheImage'
import { gShowSetting } from './PPTShowSettings'
import { AnimationManager } from './AnimationManager'
import { TransitionManager } from './TransitionManager'
import { isAutoTransition } from './utils'
import { KeyEvent_PreventAll } from '../editorUI/const'
import { keyCodeMap } from '../editorUI/utils/keys'
import { DomWheelEvt } from '../../common/dom/wheelEvent'
import { ImageLikeTypes } from '../../exports/type'
import { AudioManager } from './AudioManager'

type ShowActionT =
  | 'prev'
  | 'next'
  | { slideNum: number }
  | 'pause'
  | 'toggleAutoPlay'
  | 'autoNext'

export const ShowAction: {
  prev: 'prev'
  next: 'next'
  goto: (n: number) => { slideNum: number }
} = {
  prev: 'prev',
  next: 'next',
  goto: (n) => ({
    slideNum: n,
  }),
}

export interface ShowState {
  slideNum: number
  isForceAutoPlay?: boolean
  isTransitionPlaying?: boolean
  isBackward?: boolean
  animationCursor?: string
  action?: ShowActionT
}

const FORCE_TRANSITION_AFTER = 3000

export interface ShowStartOption {
  isInPlayMode?: boolean
  isForceAutoPlay?: boolean
}

export class ShowManager {
  editorUI: EditorUI
  transitionManager: TransitionManager
  showContainer: HTMLDivElement | null
  containerWidth: number
  containerHeight: number
  endShowElement?: HTMLElement
  currentSlideIndex: number
  countOfSlides: number
  isInShowMode: boolean

  endContentForShow: HTMLElement | null
  canvas: HTMLCanvasElement | null
  layer: HTMLCanvasElement | null
  private _unlistenEvtsForCanvas: undefined | (() => void)
  private _unlistenEvtsForLayer: undefined | (() => void)
  private _unlistenEvtsForEndContentForShow: undefined | (() => void)

  checkSlideTimer: TransitionTimer
  imgPoolManager: ImagePoolManager
  animationManager: AnimationManager
  audioManager: AudioManager
  cacheImages: [SlideImageCache | null, SlideImageCache | null]
  slideIndices: [CacheIndexInfo, CacheIndexInfo]
  hasMouseDown: boolean
  startSlideIndex: number
  eventOptions: {
    eventEnabled?: boolean
    keycodeDisabled?: number
  }
  private _isInPlayMode: boolean
  private _unhiddenSlideIndex: number
  private _zoomValue?: number
  private mouseUpTimer: any
  private _isForceAutoPlay: boolean = false
  private pointer: PointerEventInfo = new PointerEventInfo()

  get presentation() {
    return this.editorUI.editorKit.presentation
  }
  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI
    this.transitionManager = new TransitionManager(editorUI, true)
    this.animationManager = new AnimationManager(editorUI, this)
    this.audioManager = new AudioManager(editorUI, this)
    this.containerWidth = 0
    this.containerHeight = 0
    this.showContainer = null
    this.endContentForShow = null
    this.endShowElement = EditorKit.endShowElement

    this.currentSlideIndex = -1
    this.countOfSlides = 0

    this.isInShowMode = false
    this.canvas = null
    this.layer = null

    this._isInPlayMode = true
    this.animationManager.stopAutoPlay(false)

    this.checkSlideTimer = new TransitionTimer()

    this.imgPoolManager = new ImagePoolManager()
    this.cacheImages = [null, null]
    this.slideIndices = [{ index: -1 }, { index: -1 }]

    this.hasMouseDown = false
    this.startSlideIndex = -1
    this._unhiddenSlideIndex = -1
    this.eventOptions = {
      eventEnabled: true,
    }
  }

  clearCache() {
    if (this.cacheImages[0]) {
      this.imgPoolManager.release(this.cacheImages[0].slideImage)
    }
    this.cacheImages[0] = null
    this.slideIndices[0] = { index: -1 }
    if (this.cacheImages[1]) {
      this.imgPoolManager.release(this.cacheImages[1].slideImage)
    }
    this.cacheImages[1] = null
    this.slideIndices[1] = { index: -1 }
  }

  private cacheSlideImg(
    slideIndex: number,
    index: number,
    frame: 'first' | 'last'
  ) {
    const cacheImage = new SlideImageCache()
    drawCacheImage(
      this.editorUI,
      slideIndex,
      this.transitionManager.rect,
      (w, h) => {
        const img = this.imgPoolManager.getImage(w, h)
        cacheImage.slideImage = img
        return img.image!
      },
      frame
    )

    this.cacheImages[index] = cacheImage
    this.slideIndices[index] = {
      index: slideIndex,
      frame: frame,
    }
  }

  private updateZoomValue() {
    const presentation = this.editorUI?.presentation
    const containerDemension =
      this.containerWidth > 0 && this.containerHeight > 0
        ? {
            width: () => this.containerWidth,
            height: () => this.containerHeight,
          }
        : undefined
    this._zoomValue = zoomValueForFitToSlide(presentation, containerDemension)
  }

  zoomValue() {
    return this._zoomValue
  }

  isInPlayMode() {
    return !!this._isInPlayMode
  }

  isInTransitionOrAnimation() {
    return (
      this.transitionManager.isPlaying() || this.animationManager.isPlaying()
    )
  }

  isForceAutoPlay() {
    return this._isForceAutoPlay
  }

  toggleForceAutoPlay() {
    this._isForceAutoPlay = !this._isForceAutoPlay
    if (this._isForceAutoPlay === true && !this.isInTransitionOrAnimation()) {
      this.next()
    } else {
      const progressState = this.getProgressState()
      const state: ShowState = {
        ...progressState,
        action: 'toggleAutoPlay',
      }

      this.editorUI.editorKit.emitEvent(Evt_onPPTShowAction, state)
    }

    this.editorUI.editorKit.emitEvent(
      Evt_onPPTShowForceAutoPlay,
      this._isForceAutoPlay
    )
  }

  private prepareTransition(isFirst: boolean, isBackward: boolean) {
    let slideIndexInfo1: CacheIndexInfo = { index: -1 }
    let slideIndexInfo2: CacheIndexInfo = { index: -1 }

    this.transitionManager.isBackward = false
    if (isFirst) {
      slideIndexInfo1 = { index: -1 }
      slideIndexInfo2 = {
        index: this.currentSlideIndex,
        frame: 'first',
      }
    } else if (!isBackward) {
      slideIndexInfo1 = {
        index: this.getPrevUnhiddenSlideIndex(true),
        frame: 'last',
      }
      slideIndexInfo2 = {
        index: this.currentSlideIndex,
        frame: 'first',
      }
    } else {
      this.transitionManager.isBackward = true
      slideIndexInfo1 = {
        index: this.getPrevUnhiddenSlideIndex(true),
        frame: 'last',
      }
      slideIndexInfo2 = {
        index: this.currentSlideIndex,
        frame: 'first',
      }
    }

    this.transitionManager.calcRectForPPTShow()
    ;[0, 1].forEach((index) => {
      if (
        this.slideIndices[index].index !== -1 &&
        !isIndexInfoEq(this.slideIndices[index], slideIndexInfo1) &&
        !isIndexInfoEq(this.slideIndices[index], slideIndexInfo2)
      ) {
        if (this.cacheImages[index]) {
          this.imgPoolManager.release(this.cacheImages[index]?.slideImage)
        }
        this.cacheImages[index] = null
        this.slideIndices[index] = { index: -1 }
      }
    })

    if (slideIndexInfo1.index === -1) {
      this.transitionManager.curCacheImg.slideImage = null
    } else {
      if (isIndexInfoEq(slideIndexInfo1, this.slideIndices[0])) {
        this.transitionManager.curCacheImg.slideImage =
          this.cacheImages[0]!.slideImage.image
      } else if (isIndexInfoEq(slideIndexInfo1, this.slideIndices[1])) {
        this.transitionManager.curCacheImg.slideImage =
          this.cacheImages[1]!.slideImage.image
      } else {
        if (-1 === this.slideIndices[0].index) {
          this.cacheSlideImg(slideIndexInfo1.index, 0, 'last')
          this.transitionManager.curCacheImg.slideImage =
            this.cacheImages[0]!.slideImage.image
        } else {
          this.cacheSlideImg(slideIndexInfo1.index, 1, 'last')
          this.transitionManager.curCacheImg.slideImage =
            this.cacheImages[1]!.slideImage.image
        }
      }
    }

    if (slideIndexInfo2.index === -1) {
      this.transitionManager.nextCacheImg.slideImage = null
    } else {
      if (isIndexInfoEq(slideIndexInfo2, this.slideIndices[0])) {
        this.transitionManager.nextCacheImg.slideImage =
          this.cacheImages[0]!.slideImage.image
      } else if (isIndexInfoEq(slideIndexInfo2, this.slideIndices[1])) {
        this.transitionManager.nextCacheImg.slideImage =
          this.cacheImages[1]!.slideImage.image
      } else {
        if (-1 === this.slideIndices[0].index) {
          this.cacheSlideImg(slideIndexInfo2.index, 0, 'first')
          this.transitionManager.nextCacheImg.slideImage =
            this.cacheImages[0]!.slideImage.image
        } else {
          this.cacheSlideImg(slideIndexInfo2.index, 1, 'first')
          this.transitionManager.nextCacheImg.slideImage =
            this.cacheImages[1]!.slideImage.image
        }
      }
    }
  }

  private prepareSlideCache(isBackward = false): number {
    let index = -1
    if (
      this.currentSlideIndex < 0 ||
      this.currentSlideIndex >= this.countOfSlides
    ) {
      index = -1
    } else {
      const frame = isBackward ? 'last' : 'first'
      const curIndexInfo: CacheIndexInfo = {
        index: this.currentSlideIndex,
        frame: frame,
      }
      if (!isIndexInfoEq(curIndexInfo, this.slideIndices[0])) {
        if (this.cacheImages[0]) {
          this.imgPoolManager.release(this.cacheImages[0].slideImage)
        }
        this.cacheImages[0] = null
        this.slideIndices[0] = { index: -1 }
      }
      if (!isIndexInfoEq(curIndexInfo, this.slideIndices[1])) {
        if (this.cacheImages[1]) {
          this.imgPoolManager.release(this.cacheImages[1].slideImage)
        }
        this.cacheImages[1] = null
        this.slideIndices[1] = { index: -1 }
      }

      if (isIndexInfoEq(curIndexInfo, this.slideIndices[0])) {
        index = 0
      } else if (isIndexInfoEq(curIndexInfo, this.slideIndices[1])) {
        index = 1
      } else {
        this.cacheSlideImg(this.currentSlideIndex, 0, frame)
        index = 0
      }
    }
    return index
  }

  private correctCurSlideIndex() {
    this.countOfSlides = this.editorUI.renderingController.countOfSlides
    if (this.currentSlideIndex > this.countOfSlides) {
      this.currentSlideIndex = this.countOfSlides
    }
  }

  handlePointerEvent<T extends PointerEventType>(et: T, e: PointerEventArg<T>) {
    switch (et) {
      case 'pointerdown': {
        this.onMouseDown(e as PointerEvent)
        break
      }
      case 'pointermove': {
        this.onMouseMove(e as PointerEvent)
        break
      }
      case 'pointerup': {
        this.onMouseUp(e as PointerEvent)
        break
      }
      case 'wheel': {
        this.onMouseWheel(e as DomWheelEvt)
        break
      }
    }
  }
  start = (id: string, startSlidePos: number, options?: ShowStartOption) => {
    this.startSlideIndex = startSlidePos
    if (-1 === startSlidePos) startSlidePos = 0

    this.countOfSlides = this.editorUI.renderingController.countOfSlides
    this.showContainer = document.getElementById(id) as any
    if (
      !this.showContainer ||
      startSlidePos < 0 ||
      startSlidePos >= this.countOfSlides
    ) {
      return
    }
    const { clientWidth, clientHeight } = this.showContainer
    this.containerWidth = clientWidth * BrowserInfo.PixelRatio
    this.containerHeight = clientHeight * BrowserInfo.PixelRatio

    this.updateZoomValue()

    this.isInShowMode = true
    this.canvas = toElement(
      Dom.canvas('', {
        style: {
          ['position']: 'absolute',
          ['margin']: '0',
          ['padding']: '0',
          ['left']: '0px',
          ['top']: '0px',
          ['width']: '100%',
          ['height']: '100%',
          ['z-index']: '2',
          ['background-color']: '#000000',
        },
        attrs: {
          ['width']: this.containerWidth,
          ['height']: this.containerHeight,
        },
      })
    )

    this.currentSlideIndex = startSlidePos

    this.editorUI.editorKit.emitSlideChangedInShow(this.currentSlideIndex)

    this._unlistenEvtsForCanvas = listenPointerEvents(
      this.canvas,
      ['pointerdown', 'pointermove', 'pointerup', 'wheel'],
      this
    )

    this.showContainer.appendChild(this.canvas)

    const { isInPlayMode, isForceAutoPlay } = options ?? {}
    this.togglePlayMode(isInPlayMode !== false)
    this._isForceAutoPlay = isForceAutoPlay === true

    this.slideIndices[0] = { index: -1 }
    this.slideIndices[1] = { index: -1 }

    this.toSlide(true, true)
  }

  end = () => {
    if (!this.isInShowMode) return

    this.togglePlayMode(false)

    this.stopTransition()
    this.stopAudio()

    if (this.endContentForShow) {
      if (this._unlistenEvtsForEndContentForShow) {
        this._unlistenEvtsForEndContentForShow()
      }
      this.showContainer?.removeChild(this.endContentForShow)
      this.endContentForShow = null
    }

    if (this.layer) {
      if (this._unlistenEvtsForLayer) {
        this._unlistenEvtsForLayer()
      }
      this.showContainer?.removeChild(this.layer)
      this.layer = null
    }

    if (this._unlistenEvtsForCanvas) {
      this._unlistenEvtsForCanvas()
    }
    this.showContainer?.removeChild(this.canvas!)
    this.canvas = null

    let originSlideIndex = this.currentSlideIndex

    this.currentSlideIndex = -1
    this.showContainer = null
    this.isInShowMode = false
    this.animationManager.clear()

    const layerCtx = this.editorUI.animationLayer.element.getContext('2d')!
    layerCtx.setTransform(1, 0, 0, 1, 0, 0)

    this.editorUI.editorKit.emitEndShow()

    if (originSlideIndex < 0) originSlideIndex = 0
    const countOfSlides = this.editorUI.renderingController.countOfSlides
    if (originSlideIndex >= countOfSlides) originSlideIndex = countOfSlides - 1

    if (0 <= originSlideIndex) {
      this.editorUI.goToSlide(originSlideIndex, true)
    }

    this.startSlideIndex = -1
    gShowSetting.setDefault()
  }

  private checkEndContentForPPTShow(): boolean {
    let isEnd = false
    if (this.currentSlideIndex === this.countOfSlides) {
      if (!gShowSetting.isUseEndSlide) {
        return true
      }
      if (null == this.endContentForShow) {
        this.endContentForShow = document.createElement('div')
        this.endContentForShow.setAttribute(
          'style',
          'position:absolute;margin:0px;padding:0px;left:0px;top:0px;width:100%;height:100%;z-index:4;background-color:#000000;text-align:center;font-family:monospace;font-size:12pt;color:#FFFFFF;'
        )
        if (this.endShowElement instanceof HTMLElement) {
          this.endContentForShow.appendChild(this.endShowElement)
        } else {
          this.updateEndContent()
        }

        this._unlistenEvtsForEndContentForShow = listenPointerEvents(
          this.endContentForShow,
          ['pointerdown', 'pointermove', 'pointerup', 'wheel'],
          this
        )

        this.showContainer?.appendChild(this.endContentForShow)
      }
      isEnd = true
    } else if (null != this.endContentForShow) {
      this.showContainer?.removeChild(this.endContentForShow)
      this.endContentForShow = null
    }
    return isEnd
  }

  private toSlide(
    isUseTransition: boolean,
    isFirst: boolean,
    animationCursor?
  ) {
    this.updateCurrentSlideIndex(this.currentSlideIndex)
    const isTransitionPlaying = this.transitionManager.isPlaying()
    this.transitionManager.isBackward = false
    this.stopTransition()

    if (this.checkEndContentForPPTShow()) {
      this.audioManager.stop()
      return
    }

    const curSlide = this.presentation.slides[this.currentSlideIndex]
    if (isUseTransition) {
      const timing = curSlide?.timing
      if (
        timing &&
        timing.type !== TransitionTypes.None &&
        timing.duration &&
        timing.duration > 0
      ) {
        this.startTransition(timing, isFirst, false)
        this.checkSlideAudio(curSlide)
        return
      }
    }
    if (!isTransitionPlaying) {
      this.onDrawSlide(false, false, animationCursor)
      this.checkSlideAudio(curSlide)
    }
  }

  private startBackwardSlide(): number | undefined {
    const isSlideTransitionPlaying = this.transitionManager.isPlaying()
    this.transitionManager.isBackward = true
    this.stopTransition()

    if (this.currentSlideIndex === this.countOfSlides) {
      this.updateCurrentSlideIndex(this.getPrevUnhiddenSlideIndex(true))
      this.onDrawSlide(false, true)

      if (null != this.endContentForShow) {
        this.showContainer?.removeChild(this.endContentForShow)
        this.endContentForShow = null
      }

      return
    }

    if (0 >= this.currentSlideIndex) {
      this.updateCurrentSlideIndex(this.getFirstUnhiddenSlideIndex())
      return
    }

    if (isSlideTransitionPlaying) {
      return
    }

    const { timing } = this.presentation.slides[this.currentSlideIndex]

    if (
      timing.type !== TransitionTypes.None &&
      timing.duration &&
      timing.duration > 0
    ) {
      this.startTransition(timing, false, true)
      return this.getPrevUnhiddenSlideIndex(true)
    } else {
      this.updateCurrentSlideIndex(this.getPrevUnhiddenSlideIndex(true))
      this.onDrawSlide(false, true)
    }
  }

  private stopTransition() {
    this.checkSlideTimer.cancel()
    if (this.transitionManager.isPlaying()) {
      this.transitionManager.end()
    } else {
      this.transitionManager.emitEnd()
    }
    // reset backward
    this.transitionManager.isBackward = false
  }

  private checkLayer() {
    if (null == this.layer) {
      this.layer = document.createElement('canvas')
      this.layer.setAttribute(
        'style',
        'position:absolute;margin:0;padding:0;left:0px;top:0px;width:100%;height:100%;z-index:3;'
      )
      this.layer.width = this.canvas!.width
      this.layer.height = this.canvas!.height

      this._unlistenEvtsForLayer = listenPointerEvents(
        this.layer,
        ['pointerdown', 'pointermove', 'pointerup', 'wheel'],
        this
      )

      this.showContainer?.appendChild(this.layer)
    }
  }

  private startTransition(timing: SlideTransition, isFirst, isBackward) {
    this.checkLayer()

    this.transitionManager.type = timing.type!
    this.transitionManager.option = timing.option!
    this.transitionManager.duration = timing.duration!

    this.prepareTransition(isFirst, isBackward)
    this.transitionManager.start()
  }

  /** 跳转 slide 后检查音频播放状态 */
  private checkSlideAudio(slide: Slide) {
    if (!slide || !EditorSettings.isSupportAudio) return
    this.audioManager.goToSlide(slide)
  }

  private stopAudio() {
    this.audioManager.reset()
  }

  onTransitionEnd() {
    if (this.transitionManager.isBackward) {
      this.currentSlideIndex = this.getPrevUnhiddenSlideIndex(true)
      this.editorUI.editorKit.emitSlideChangedInShow(this.currentSlideIndex)
    }

    this.onDrawSlide(true, this.transitionManager.isBackward)
  }

  private drawSlideOnTransition(needClearLayer: boolean, isBackward = false) {
    if (needClearLayer && this.layer) {
      const ctx = this.layer.getContext('2d')!
      const { x, y, w, h } = this.transitionManager.rect
      ctx.clearRect(x, y, w, h)
    }

    this.transitionManager.calcRectForPPTShow()
    const index = this.prepareSlideCache(isBackward)
    const slideImage = this.cacheImages[index]?.slideImage.image
    if (slideImage) {
      const { x, y, w, h } = this.transitionManager.rect
      this.canvas?.getContext('2d')?.drawImage(slideImage, x, y, w, h)
    }
  }

  /** 切页时调用, 注意会清除 Animation Cache */
  onDrawSlide(needClearLayer: boolean, isBackward = false, animationCursor?) {
    this.drawSlideOnTransition(needClearLayer, isBackward)
    const slide = this.presentation.slides[this.currentSlideIndex]
    this.animationManager.start(slide, isBackward, animationCursor)
  }

  syncDrawSlide(needClearLayer: boolean, isBackward = false, animationCursor?) {
    this.drawSlideOnTransition(needClearLayer, isBackward)
    const slide = this.presentation.slides[this.currentSlideIndex]
    this.animationManager.drawFrame(slide, isBackward, animationCursor)
  }

  checkAdvanceAfterTransition(slide: Slide, callback?: () => void) {
    if (isAutoTransition(this.editorUI, slide)) {
      const waitTime =
        slide && slide.timing && slide.timing.advanceAfter
          ? slide.timing.advanceDuration!
          : FORCE_TRANSITION_AFTER
      // 自动切页
      this.checkSlideTimer.exeAfter(
        () => {
          if (this._isInPlayMode) {
            this.autoNext()
          }
        },
        waitTime,
        callback
      )
    } else {
      if (callback) {
        callback()
      }
    }
  }

  redrawCurrentSlide() {
    const index = this.prepareSlideCache()
    const slideImage = this.cacheImages[index]?.slideImage.image
    if (slideImage) {
      const { x, y, w, h } = this.transitionManager.rect
      this.canvas?.getContext('2d')?.drawImage(slideImage, x, y, w, h)
    }
  }

  private isSlideVisible(slideIndex: number) {
    const isStartSlide = slideIndex === this.startSlideIndex
    const isTmpUnhiddenSlide =
      -1 !== this._unhiddenSlideIndex && slideIndex === this._unhiddenSlideIndex
    if (isStartSlide || isTmpUnhiddenSlide) return true

    return this.presentation.isSlideVisible(slideIndex)
  }
  goToNextUnhiddenSlide() {
    this.currentSlideIndex++
    while (this.currentSlideIndex < this.countOfSlides) {
      if (this.isSlideVisible(this.currentSlideIndex)) break

      this.currentSlideIndex++
    }
  }
  private goToPrevUnhiddenSlide() {
    this.currentSlideIndex--
    while (this.currentSlideIndex >= 0) {
      if (this.isSlideVisible(this.currentSlideIndex)) break

      this.currentSlideIndex--
    }
  }

  getPrevUnhiddenSlideIndex(noLoop: boolean) {
    let slideIndex = this.currentSlideIndex - 1
    while (slideIndex >= 0) {
      if (this.isSlideVisible(slideIndex)) return slideIndex

      --slideIndex
    }

    if (true === noLoop || !this.isLoop()) return -1

    slideIndex = this.countOfSlides - 1
    while (slideIndex > this.currentSlideIndex) {
      if (this.isSlideVisible(slideIndex)) return slideIndex

      --slideIndex
    }

    return this.countOfSlides
  }

  getNextUnhiddenSlide() {
    let slideIndex = this.currentSlideIndex + 1
    while (slideIndex < this.countOfSlides) {
      if (this.isSlideVisible(slideIndex)) return slideIndex

      ++slideIndex
    }

    if (!this.isLoop()) return this.countOfSlides

    slideIndex = 0
    while (slideIndex < this.currentSlideIndex) {
      if (this.isSlideVisible(slideIndex)) return slideIndex

      ++slideIndex
    }

    return -1
  }
  getFirstUnhiddenSlideIndex() {
    let slideIndex = 0
    while (slideIndex < this.countOfSlides) {
      if (this.isSlideVisible(slideIndex)) return slideIndex
      ++slideIndex
    }
    return 0
  }

  getCurSlideIndex() {
    return this.currentSlideIndex
  }

  private getLastUnhiddenSlide() {
    let slideIndex = this.countOfSlides - 1
    while (slideIndex >= 0) {
      if (this.isSlideVisible(slideIndex)) return slideIndex
      --slideIndex
    }
    return this.countOfSlides - 1
  }

  autoNext() {
    this.togglePlayMode(true)
    const progressState = this.getProgressState()
    const state: ShowState = {
      ...progressState,
      isTransitionPlaying: false,
      ['action']: 'autoNext',
    }
    this.editorUI.editorKit.emitEvent(Evt_onPPTShowAction, state)

    this._unhiddenSlideIndex = this.currentSlideIndex
    this.goToNextUnhiddenSlide()
    if (this.currentSlideIndex === this.countOfSlides && this.isLoop()) {
      this.currentSlideIndex = this.getFirstUnhiddenSlideIndex()
    }
    this.editorUI.editorKit.emitSlideChangedInShow(this.currentSlideIndex)
    this.toSlide(true, false)
    this._unhiddenSlideIndex = -1
  }

  next(isStopTransition = false) {
    this.togglePlayMode(true)
    const progressState = this.getProgressState()
    const state: ShowState = {
      ...progressState,
      ['action']: ShowAction.next,
    }
    this.editorUI.editorKit.emitEvent(Evt_onPPTShowAction, state)

    if (
      !state.isTransitionPlaying &&
      this.animationManager.isPlayShapeAnimation()
    ) {
      this.animationManager.next()
    } else {
      this.nextSlide(isStopTransition)
    }
  }
  prev(isStopTransition = false) {
    this.togglePlayMode(false)
    const progressState = this.getProgressState()
    const state: ShowState = {
      ...progressState,
      ['isBackward']: true,
      ['action']: ShowAction.prev,
    }
    this.editorUI.editorKit.emitEvent(Evt_onPPTShowAction, state)
    if (
      !state.isTransitionPlaying &&
      this.animationManager.isPlayShapeAnimation()
    ) {
      this.animationManager.prev()
    } else {
      this.prevSlide(isStopTransition)
    }
  }
  nextSlide(isStopTransition = false) {
    if (!this.isInShowMode) return

    this._unhiddenSlideIndex = this.currentSlideIndex

    this.correctCurSlideIndex()

    if (isStopTransition) {
      this.stopTransition()
    }

    const isPlaying = this.transitionManager.isPlaying()
    if (!isPlaying) {
      this.goToNextUnhiddenSlide()
      if (this.isLoop() && this.currentSlideIndex >= this.countOfSlides) {
        this.currentSlideIndex = this.getFirstUnhiddenSlideIndex()
      }
      const slide = this.presentation.slides[this.currentSlideIndex]
      this.animationManager.resetPlayer(slide)
    }

    if (
      this.currentSlideIndex > this.countOfSlides ||
      (!gShowSetting.isUseEndSlide &&
        this.currentSlideIndex === this.countOfSlides)
    ) {
      if (gShowSetting.isEndOnlyByEsc) {
        return
      }
      this.end()
    } else {
      this.editorUI.editorKit.emitSlideChangedInShow(this.currentSlideIndex)
      this.toSlide(!isPlaying, false)
    }

    this._unhiddenSlideIndex = -1
  }
  isLoop() {
    return this.presentation.isLoopShow()
  }
  isFirstUnhiddenSlide() {
    return this.getFirstUnhiddenSlideIndex() === this.currentSlideIndex
  }
  prevSlide(isStopTransition = false) {
    if (!this.isInShowMode) return

    if (isStopTransition) {
      this.stopTransition()
    }

    this._unhiddenSlideIndex = this.currentSlideIndex

    let prevSlideIndex
    if (!this.isFirstUnhiddenSlide()) {
      this.correctCurSlideIndex()

      const targetSlideIndex = this.startBackwardSlide()
      prevSlideIndex = isNumber(targetSlideIndex)
        ? targetSlideIndex
        : this.currentSlideIndex
      this.editorUI.editorKit.emitSlideChangedInShow(prevSlideIndex)
    } else if (this.isLoop()) {
      this.correctCurSlideIndex()
      this.currentSlideIndex = this.countOfSlides
      const targetSlideIndex = this.startBackwardSlide()
      prevSlideIndex = isNumber(targetSlideIndex)
        ? targetSlideIndex
        : this.currentSlideIndex
      this.editorUI.editorKit.emitSlideChangedInShow(prevSlideIndex)
    }

    if (prevSlideIndex != null) {
      this.checkSlideAudio(this.presentation.slides[prevSlideIndex])
    }
    this._unhiddenSlideIndex = -1
  }

  goToSlide(slideIndex: number) {
    if (!this.isInShowMode) return

    this.togglePlayMode(true)
    this.correctCurSlideIndex()

    if (
      slideIndex === this.currentSlideIndex ||
      slideIndex < 0 ||
      slideIndex >= this.countOfSlides
    ) {
      return
    }

    if (this.transitionManager.isPlaying()) {
      this.stopTransition()
    }

    if (this.animationManager.isPlaying()) {
      this.animationManager.stop()
    }

    const progressState = this.getProgressState()
    const state: ShowState = {
      ...progressState,
      ['action']: ShowAction.goto(slideIndex),
    }
    this.editorUI.editorKit.emitEvent(Evt_onPPTShowAction, state)

    this.currentSlideIndex = slideIndex
    this.editorUI.editorKit.emitSlideChangedInShow(this.currentSlideIndex)
    this.toSlide(true, false)
  }

  /** 供远程演示客户端同步播放进度用 */
  syncAnimStateForShow(state: ShowState) {
    const {
      slideNum,
      animationCursor,
      isTransitionPlaying,
      isBackward,
      isForceAutoPlay,
    } = {
      slideNum: state['slideNum'],
      animationCursor: state['animationCursor'],
      isTransitionPlaying: state['isTransitionPlaying'],
      isBackward: state['isBackward'],
      isForceAutoPlay: state['isForceAutoPlay'],
    }
    if (!this.isInShowMode) return

    if (state.action === 'toggleAutoPlay') {
      this._isForceAutoPlay = isForceAutoPlay!
      return
    }

    this.transitionManager.stopAnim()
    this.transitionManager.params = {}
    this.transitionManager.curCacheImg.slideImage = null
    this.transitionManager.nextCacheImg.slideImage = null
    this.transitionManager.emitEnd()

    this.correctCurSlideIndex()
    if (slideNum < 0 || this.countOfSlides < slideNum) return
    this.currentSlideIndex = slideNum

    if (isTransitionPlaying === true) {
      if (isBackward === true) {
        this.currentSlideIndex = this.getPrevUnhiddenSlideIndex(true)
      }
      this.onDrawSlide(true, isBackward)
      return
    }

    this.syncDrawSlide(false, false, animationCursor)

    this._isForceAutoPlay = isForceAutoPlay!
    switch (state.action) {
      case 'next': {
        this.next()
        break
      }
      case 'prev': {
        this.prev()
        break
      }
      case 'pause': {
        this.pause()
        break
      }
      case 'autoNext': {
        this.autoNext()
        break
      }
      default:
        if (state.action != null) {
          const { slideNum } = state.action
          this.goToSlide(slideNum)
        }
    }
  }
  play() {
    if (this.isInPlayMode()) {
      return
    }
    this.next()
  }

  pause() {
    this.togglePlayMode(false)
    this.animationManager.stop()
    if (this.transitionManager.isPlaying() || this.checkSlideTimer.isTiming()) {
      this.stopTransition()
    }
    const progressState = this.getProgressState()
    const state: ShowState = {
      ...progressState,
      ['action']: 'pause',
    }
    this.editorUI.editorKit.emitEvent(Evt_onPPTShowAction, state)
  }

  togglePlayMode(enablePlay: boolean) {
    if (enablePlay !== this._isInPlayMode) {
      this._isInPlayMode = enablePlay
      this.animationManager.stopAutoPlay(!enablePlay)
      this.editorUI.editorKit.emitEvent(Evt_onPPTShowPlayMode, enablePlay)
    }
  }
  private onTriggerKeyCode = (code: number) => {
    if (
      this.checkEventDisabled() ||
      (isFiniteNumber(code) && this.eventOptions.keycodeDisabled === code)
    ) {
      return false
    }

    switch (code) {
      case keyCodeMap.enter:
      case keyCodeMap.pageDown:
      case keyCodeMap.arrowDown: {
        this.next()
        break
      }
      case keyCodeMap.arrowRight: {
        if (EditorSettings.isRTL) {
          this.prev()
        } else {
          this.next()
        }
        break
      }
      case keyCodeMap.space: {
        if (this.isInTransitionOrAnimation()) {
          this.pause()
        } else {
          this.next()
        }
        break
      }
      case keyCodeMap.pageUp:
      case keyCodeMap.arrowUp: {
        this.prev()
        break
      }
      case keyCodeMap.arrowLeft: {
        if (EditorSettings.isRTL) {
          this.next()
        } else {
          this.prev()
        }
        break
      }
      case keyCodeMap.home: {
        this.goToSlide(this.getFirstUnhiddenSlideIndex())
        break
      }
      case keyCodeMap.end: {
        this.goToSlide(this.getLastUnhiddenSlide())
        break
      }
      case keyCodeMap.esc: {
        this.end()
        break
      }
      default:
        break
    }
  }
  onKeyDown = (e) => {
    if (this.checkEventDisabled()) return false

    GlobalEvents.keyboard.onKeyEvent(e)

    this.onTriggerKeyCode(GlobalEvents.keyboard.keyCode)

    this.editorUI.isKeydownWithoutPress = true
    return false
  }
  private onMouseDown(e: PointerEvent) {
    const mouseInfo = this.getMouseInfo(e, true)

    if (mouseInfo) {
      const ret = handleMouseDown(
        this.editorUI,
        this.pointer,
        mouseInfo.x,
        mouseInfo.y
      )
      if (ret === KeyEvent_PreventAll) {
        // mouse up will not sended!!!
        handleMouseUp(this.editorUI, this.pointer, mouseInfo.x, mouseInfo.y)
        return
      }
    }
    if (this.checkEventDisabled()) return false

    this.hasMouseDown = true
    e.preventDefault()
    return false
  }
  private onMouseMove(e: PointerEvent) {
    const mouseInfo = this.getMouseInfo(e)
    if (mouseInfo) {
      handleMouseMove(this.editorUI, this.pointer, mouseInfo.x, mouseInfo.y)
    }
  }

  private getMousePos(e) {
    if (
      this.currentSlideIndex >= 0 &&
      this.currentSlideIndex < this.countOfSlides
    ) {
      const pos = getPointerEventPos(e)
      const [pointerX, pointerY] = pos ? pos : [this.pointer.x, this.pointer.y]
      const { left, top } = this.canvas!.getBoundingClientRect()
      let { x, y, w, h } = this.transitionManager.rect
      x = x / BrowserInfo.PixelRatio + left
      y = y / BrowserInfo.PixelRatio + top
      w /= BrowserInfo.PixelRatio
      h /= BrowserInfo.PixelRatio
      const mmW = this.presentation.width
      const mmH = this.presentation.height
      let _x = pointerX - x
      let _y = pointerY - y

      _x = (_x * mmW) / w
      _y = (_y * mmH) / h
      return { x: _x, y: _y, slideNum: this.currentSlideIndex }
    }
    return null
  }

  /** 会调用 GlobalEvents 的 pointerDown, 注意调用顺序以及不要重复调用*/
  getMouseInfo(e, isMouseDown = false) {
    const transition = this.transitionManager
    if (
      this.currentSlideIndex >= 0 &&
      this.currentSlideIndex < this.countOfSlides
    ) {
      this.pointer.onDown(e, isMouseDown, false)

      const { left, top } = this.canvas!.getBoundingClientRect()
      let { x, y, w, h } = transition.rect
      x = x / BrowserInfo.PixelRatio + left
      y = y / BrowserInfo.PixelRatio + top
      w /= BrowserInfo.PixelRatio
      h /= BrowserInfo.PixelRatio
      const mmW = this.presentation.width
      const mmH = this.presentation.height

      let _x = this.pointer.x - x
      let _y = this.pointer.y - y

      _x = (_x * mmW) / w
      _y = (_y * mmH) / h

      return { x: _x, y: _y, slideNum: this.currentSlideIndex }
    }
    return null
  }
  private onMouseUp(e: PointerEvent, isForce?) {
    if (this.checkEventDisabled()) return false

    if (!this.hasMouseDown && true !== isForce) return

    if (this.pointer.isLocked) {
      this.pointer.isLocked = false
    }

    this.hasMouseDown = false

    const editorKit = this.editorUI.editorKit
    const isDbClick = 2 <= this.pointer.clicks
    const isLeftMouse = this.pointer.button === POINTER_BUTTON.Left
    const isInIransition = this.isInTransitionOrAnimation()

    if (isLeftMouse && !isInIransition) {
      const mouseInfo = this.getMousePos(e)
      if (mouseInfo) {
        const info = editorKit.getHittestInfoByMousePosition(
          mouseInfo.x,
          mouseInfo.y,
          false
        )
        if (info?.['image']?.['type'] === ImageLikeTypes.audio) {
          if (isDbClick) {
            if (this.mouseUpTimer) {
              clearTimeout(this.mouseUpTimer)
            }
          }
          const audioInfo = info['image']
          const slide = this.presentation.slides[this.currentSlideIndex]
          this.audioManager.handleHitAudio(slide, audioInfo)
          return
        }
      }
    }

    if (isDbClick && isLeftMouse && !isInIransition) {
      if (this.mouseUpTimer) {
        clearTimeout(this.mouseUpTimer)
      }
      const mouseInfo = this.getMouseInfo(e, true)
      if (mouseInfo) {
        const info = editorKit.getHittestInfoByMousePosition(
          mouseInfo.x,
          mouseInfo.y,
          false
        )
        if (info?.['image']) {
          switch (info['image']['type']) {
            case ImageLikeTypes.image:
            case ImageLikeTypes.chart:
              editorKit.emitEvent(Evt_viewSourceImage, info['image'])
              break
            case ImageLikeTypes.attachment:
              editorKit.emitEvent(Evt_viewAttachment, info['image'])
              break
            case ImageLikeTypes.video:
              break
            case ImageLikeTypes.audio:
              break
          }
        }
      }
      return
    }

    this.mouseUpTimer = setTimeout(() => {
      // next slide
      this.correctCurSlideIndex()

      if (
        this.transitionManager.isPlaying() ||
        this.currentSlideIndex < 0 ||
        this.currentSlideIndex >= this.countOfSlides
      ) {
        this.next()
      } else {
        const { timing } = this.presentation.slides[this.currentSlideIndex]
        if (timing.advanceOnMouseClick === true) {
          this.next()
        }
      }

      stopEvent(e)
    }, 300)

    return false
  }

  private throttleScrollGoToSlide = throttle(
    ({ deltaX, deltaY }: { deltaX: number; deltaY: number }) => {
      const delta = deltaX === 0 ? deltaY : deltaX
      if (delta > 0) {
        this.next()
      } else {
        this.prev()
      }
    },
    1000
  )

  private onMouseWheel({ eventObj: evt, origin: e }: DomWheelEvt) {
    if (this.checkEventDisabled()) return false

    this.throttleScrollGoToSlide({
      deltaX: evt.deltaX,
      deltaY: evt.deltaY,
    })

    stopEvent(e)
    return false
  }

  resize() {
    if (!this.isInShowMode) return

    const { clientWidth, clientHeight } = this.showContainer!
    const _width = clientWidth * BrowserInfo.PixelRatio
    const _height = clientHeight * BrowserInfo.PixelRatio

    if (_width === this.containerWidth && _height === this.containerHeight) {
      return
    }

    this.containerWidth = _width
    this.containerHeight = _height

    this.updateZoomValue()

    this.canvas!.width = _width
    this.canvas!.height = _height

    this.transitionManager.calcRectForPPTShow()

    this.slideIndices[0] = { index: -1 }
    this.slideIndices[1] = { index: -1 }

    if (this.layer) {
      this.layer.width = this.canvas!.width
      this.layer.height = this.canvas!.height
    }

    // todo 首次进入演示多次 toSlide
    if (this.currentSlideIndex < this.countOfSlides) {
      this.toSlide(this.transitionManager.isPlaying(), false)
    } else if (!gShowSetting.isUseEndSlide) {
      // 不使用结束页时需要跳回到最后一页
      this.currentSlideIndex = this.getLastUnhiddenSlide()
      this.toSlide(this.transitionManager.isPlaying(), false)
    }
  }

  checkEventDisabled(): boolean {
    return this.eventOptions.eventEnabled === false
  }

  setEventEnabled(enabled: boolean, keyCode?: number) {
    this.eventOptions = {
      eventEnabled: enabled,
      keycodeDisabled: keyCode,
    }
  }

  updateEndContent() {
    if (this.endContentForShow) {
      this.endContentForShow.innerHTML = translator.getValue(
        gShowSetting.textOfEndingSlide
      )
    }
  }
  private getProgressState(): {
    slideNum: number
    isForceAutoPlay?: boolean
    isTransitionPlaying?: boolean
    isBackward?: boolean
    animationCursor?: string
  } {
    const state: {
      slideNum: number
      isForceAutoPlay?: boolean
      isTransitionPlaying?: boolean
      isBackward?: boolean
      animationCursor?: string
    } = {
      ['slideNum']: this.currentSlideIndex,
    }

    const cursorState = this.animationManager.getCursorStateForSlide(
      this.presentation.slides[this.currentSlideIndex]
    )

    if (cursorState) {
      state['animationCursor'] = cursorState
    }

    if (this.transitionManager.isPlaying()) {
      state['isTransitionPlaying'] = true
    }

    state['isForceAutoPlay'] = this.isForceAutoPlay()
    return state
  }

  updateCurrentSlideIndex(index: number) {
    this.currentSlideIndex = index
    this.presentation.updateCurrentSlideIndex(this.currentSlideIndex)
  }
}

interface CacheIndexInfo {
  index: number
  frame?: 'first' | 'last'
}

function isIndexInfoEq(info1: CacheIndexInfo, info2: CacheIndexInfo) {
  if (info1.index === -1 && info2.index === -1) {
    return true
  } else if (info1.index === -1 || info2.index === -1) {
    return false
  } else {
    return info1.index === info2.index && info1.frame === info2.frame
  }
}

class TransitionTimer {
  private timerId
  private callback?: () => void
  constructor() {}
  exeAfter(fn: () => void, after: number, callback?: () => void) {
    this.callback = callback
    if (this.timerId != null) {
      clearTimeout(this.timerId)
      this.timerId = undefined
    }
    this.timerId = setTimeout(() => {
      if (this.callback) {
        this.callback()
        this.callback = undefined
      }
      this.timerId = undefined
      fn()
    }, after)
  }
  cancel() {
    if (this.timerId != null) {
      clearTimeout(this.timerId)
      this.timerId = undefined
    }
  }
  isTiming() {
    return this.timerId != null
  }
}
