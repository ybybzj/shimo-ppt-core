import { Dict } from '../../../liber/pervasive'
import { BrowserInfo } from '../../common/browserInfo'
import { getScaleFactor } from '../../core/graphic/drawUtils'
import { CanvasDrawer } from '../../core/graphic/CanvasDrawer'

import { ClipRect } from '../../core/graphic/type'

import { Slide } from '../../core/Slide/Slide'

import { SlideElement } from '../../core/SlideElement/type'
import { EditorUI } from '../editorUI/EditorUI'
import { calculateDimensionInfo } from '../helpers'
import {
  playNext,
  ShapeAnimationPlayer,
  ShapeAnimationUpdater,
  StopPlayStatus,
  resetPlay,
} from '../../re/export/ShapeAnimation'
import {
  drawAnimShape,
  drawStaticOfSlide,
  genAnimPlayerForSlide,
  makeDrawer,
} from './animation'
import { TimeNode } from '../../core/Slide/Animation'
import { EditActionFlag } from '../../core/EditActionFlag'
import { longActions } from '../../globals/longActions'
import { Evt_onAnimationEnd } from '../../editor/events/EventNames'
import { zoomValueForFitToSlide } from '../../core/utilities/zoom'
import { logger } from '../../lib/debug/log'
import { ShapeImageData } from '../../core/render/toImage'
import { WithBounds } from '../../core/graphic/Bounds'
export class AnimationPreviewer {
  editorUI: EditorUI
  animationPlayer?: ShapeAnimationPlayer
  private spsImgCache: Dict<ShapeImageData> = {}
  private staticImgCache: Dict<HTMLCanvasElement> = {}
  private drawInfo: {
    rect: ClipRect
    clearRect: ClipRect
    scale: number
    slide?: Slide
    ctx?: CanvasRenderingContext2D
  } = {
    rect: { x: 0, y: 0, h: 0, w: 0 },
    scale: 1.0,
    clearRect: { x: 0, y: 0, h: 0, w: 0 },
  }

  get presentation() {
    return this.editorUI.editorKit.presentation
  }

  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI
  }

  end = (stopStatus: StopPlayStatus) => {
    logger.log(`[${stopStatus}]animation stopped~~~`)
    this.spsImgCache = {}
    this.staticImgCache = {}
    this.stopAnim()
    longActions.end(EditActionFlag.action_Presentation_PreviewSlideAnimation)

    const element = this.editorUI.animationLayer.element
    element.style.visibility = 'hidden'
    this.editorUI.editorView.element.style.visibility = 'visible'
    const ctx = element.getContext('2d')!
    ctx.setTransform(1, 0, 0, 1, 0, 0)

    this.editorUI.prepareRender()
    this.editorUI.editorKit.emitEvent(Evt_onAnimationEnd)
  }

  start(timeNodes?: TimeNode[]) {
    this.stopAnim()

    this.preStartPlayPreview()
    const { slide } = this.drawInfo
    if (slide) {
      timeNodes = timeNodes ?? slide.animation.timeNodes
      const drawer = makeDrawer(this.editorUI, {
        drawInfo: this.drawInfo,
        drawStatic: this.drawStatic,
        drawShape: this.drawShape,
      })
      this.animationPlayer = genAnimPlayerForSlide(slide, timeNodes, {
        onEnd: this.end,
        autoPlay: () => true,
      })
      if (this.animationPlayer) {
        longActions.start(
          EditActionFlag.action_Presentation_PreviewSlideAnimation
        )
        playNext(this.animationPlayer, drawer)
      }
    }
  }

  isPlaying() {
    return !!this.animationPlayer
  }

  private stopAnim() {
    if (this.animationPlayer) {
      resetPlay(this.animationPlayer)
      this.animationPlayer = undefined
    }
  }

  private preStartPlayPreview() {
    const editorUI = this.editorUI
    const element = editorUI.animationLayer.element
    const zoom = zoomValueForFitToSlide(editorUI.presentation, {
      width: () => element.width,
      height: () => element.height,
    })
    this.drawInfo.scale = getScaleFactor(zoom)
    this.drawInfo.clearRect = {
      x: 0,
      y: 0,
      w: element.width,
      h: element.height,
    }
    const bounds: WithBounds = {
      ...editorUI.slideRenderer.getBounds(),
      minX: 0,
      minY: 0,
    }
    const info = calculateDimensionInfo(editorUI, zoom, bounds, element)

    this.drawInfo.rect.x =
      info.centerX - info.centerXOfSlide - info.horizontalLeft
    this.drawInfo.rect.y = info.centerY - info.centerYOfSlide - info.verticalTop
    this.drawInfo.rect.w = info.slideWidth
    this.drawInfo.rect.h = info.slideHeight

    const ctx = element.getContext('2d')!
    editorUI.editorView.element.style.visibility = 'hidden'
    element.style.visibility = 'visible'
    this.drawInfo.ctx = ctx

    const { curSlideIndex } = this.editorUI.renderingController
    const slide = this.presentation.slides[curSlideIndex]
    this.drawInfo.slide = slide
  }

  private drawShape = (
    sp: SlideElement,
    scale: number,
    g: CanvasDrawer,
    percentage: number,
    transformUpdater: ShapeAnimationUpdater
  ) => {
    drawAnimShape(sp, scale, g, percentage, transformUpdater, (creater) => {
      let cacheImgData = this.spsImgCache[sp.getId()]
      if (cacheImgData == null) {
        cacheImgData = creater()
        this.spsImgCache[sp.getId()] = cacheImgData
      }
      return cacheImgData
    })
  }

  private drawStatic = (slide: Slide, scale: number, g: CanvasDrawer) => {
    const rect = {
      x: this.drawInfo.rect.x * BrowserInfo.PixelRatio,
      y: this.drawInfo.rect.y * BrowserInfo.PixelRatio,
      w: this.drawInfo.rect.w * BrowserInfo.PixelRatio,
      h: this.drawInfo.rect.h * BrowserInfo.PixelRatio,
    }
    drawStaticOfSlide(slide, scale, g, rect, (creater) => {
      const slideId = slide.getId()
      let cacheImg = this.staticImgCache[slideId]
      if (cacheImg == null) {
        cacheImg = creater()
        this.staticImgCache[slideId] = cacheImg
      }
      return cacheImg
    })
  }
}
