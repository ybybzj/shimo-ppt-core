import { isNumber } from '../../common/utils'
import {
  AudioNode,
  findSpAudioNode,
  readAudioNodeToSetting,
} from '../../core/Slide/Animation'
import { Slide } from '../../core/Slide/Slide'
import { getMediaFromShape } from '../../core/utilities/shape/getters'
import { HittestInfo, ImageLikeTypes } from '../../editor/proto'
import { AudioAction } from '../../exports/type'
import { SlideElement } from '../../core/SlideElement/type'
import { EditorUI } from '../editorUI/EditorUI'
import type { ShowManager } from './ShowManager'
import { getSpPosInShow } from './utils'

type AudioPlayNode = { slideIndex: number } & AudioNode

export class AudioManager {
  editorUI: EditorUI
  showManager: ShowManager
  /** Todo: 自然播放完毕等情况是否需要外部 ui 主动维护该状态 */
  currentAudios: AudioPlayNode[]

  get presentation() {
    return this.editorUI.editorKit.presentation
  }

  constructor(editorUI: EditorUI, showManager: ShowManager) {
    this.editorUI = editorUI
    this.showManager = showManager
    this.currentAudios = []
  }

  /** 第一次进入需要播放, 跳页回来也需要检查当前播放的 Nodes 是否在其中，不重新播放? */
  goToSlide(slide: Slide) {
    const audioNodes = slide.animation.audioNodes
    const autoPlayNodes = audioNodes.filter(
      (node) => node.triggerType !== 'ClickEffect'
    )
    if (0 < autoPlayNodes.length) {
      autoPlayNodes.forEach((node) => this.playAudio(slide, node))
    }
    this.checkNeedStop(slide)
  }

  stop() {
    if (this.currentAudios.length < 1) return
    for (const audioNode of this.currentAudios) {
      this.stopAudio(audioNode)
    }
    this.currentAudios.length = 0
  }

  reset() {
    this.stop()
  }

  handleHitAudio(
    slide: Slide,
    audioInfo: HittestInfo['image'] & { type: typeof ImageLikeTypes.audio }
  ) {
    const sp = slide.getSpTree().find((sp) => sp.refId === audioInfo['refId'])
    if (sp && isNumber(slide.index)) {
      const audioNode = findSpAudioNode(slide, sp)
      if (!audioNode || audioNode.hideWhenShow) return
      const audioAction = genAudioAction(slide, sp, 'toggle', true)
      if (audioAction) {
        const pos = getSpPosInShow(this.showManager, slide, sp)
        this.editorUI.editorKit.emitAudioAction(audioAction, pos)
      }
      const index = this.currentAudios.findIndex(
        (node) => node.spRefId === audioNode.spRefId
      )

      if (-1 < index) {
        this.currentAudios.splice(index, 1)
      } else {
        this.currentAudios.push({ slideIndex: slide.index, ...audioNode })
      }
    }
  }

  private playAudio(slide: Slide, audioNode: AudioNode) {
    const sp = slide
      .getSpTree()
      .find((sp) => sp.getRefId() === audioNode.spRefId)
    if (!sp || !isNumber(slide.index)) return

    const audioAction = genAudioAction(slide, sp, 'start', true)
    if (audioAction) {
      if (
        !this.currentAudios.some((node) => {
          return node.spRefId === audioNode.spRefId
        })
      ) {
        this.currentAudios.push({ slideIndex: slide.index, ...audioNode })
      }

      const pos = getSpPosInShow(this.showManager, slide, sp)
      this.editorUI.editorKit.emitAudioAction(audioAction, pos)
    }
  }

  private checkNeedStop(slide?: Slide) {
    const l = this.currentAudios.length
    const toRemovedIdxes: number[] = []
    for (let i = l - 1; i >= 0; i--) {
      const audioNode = this.currentAudios[i]
      const startIndex = audioNode.slideIndex
      const acrossSlideNum = audioNode.numSlide ?? 1
      const needStop = this.isSlideAcrossOver(startIndex, acrossSlideNum, slide)
      if (needStop) {
        this.stopAudio(audioNode)
        toRemovedIdxes.push(i)
      }
    }
    if (toRemovedIdxes.length > 0) {
      for (const idx of toRemovedIdxes) {
        this.currentAudios.splice(idx, 1)
      }
    }
  }

  private stopAudio(audioNode: AudioPlayNode) {
    const slide = this.presentation.slides[audioNode.slideIndex]
    const sp = slide
      .getSpTree()
      .find((sp) => sp.getRefId() === audioNode.spRefId)
    if (!sp || !isNumber(slide.index)) return

    const audioAction = genAudioAction(slide, sp, 'end', true)
    if (audioAction) {
      audioAction['hidePlayer'] = true
      this.editorUI.editorKit.emitAudioAction(audioAction)
    }
  }

  /** 被隐藏的 slide 默认计入范围 */
  private isSlideAcrossOver(
    startIndex: number,
    acrossSlideNum: number,
    slide?: Slide,
    ignoreHiddenSlide = false
  ) {
    const curSlideIndex = slide?.index ?? this.presentation.currentSlideIndex
    if (!isNumber(curSlideIndex) || curSlideIndex < startIndex) {
      return true
    }
    if (curSlideIndex - startIndex < acrossSlideNum) {
      return false
    }
    let isAcrossOver = false
    let leftAcrossNum = acrossSlideNum

    for (let i = startIndex; i < curSlideIndex; i++) {
      const slide = this.presentation.slides[i]

      if (!slide || (ignoreHiddenSlide && !slide.isVisible())) continue
      leftAcrossNum--
      if (leftAcrossNum < 1) {
        isAcrossOver = true
        break
      }
    }
    return isAcrossOver
  }
}

export function genAudioAction(
  slide: Slide,
  sp: SlideElement,
  action: AudioAction['action'],
  withSetting = false
): AudioAction | undefined {
  const mediaInfo = getMediaFromShape(sp)
  if (!mediaInfo || !mediaInfo.media) return
  const audioAction: AudioAction = {
    ['action']: action,
    ['url']: mediaInfo.media,
  }
  if (withSetting === true) {
    const audioNode = findSpAudioNode(slide, sp)
    if (!audioNode) return
    const audioSetting = readAudioNodeToSetting(slide, audioNode)!
    if (!audioSetting) return
    audioAction['setting'] = audioSetting
  }
  return audioAction
}

export function checkShouldAudioSpHide(slide: Slide, sp: SlideElement) {
  const audioNode = findSpAudioNode(slide, sp)
  if (!audioNode) return false
  return audioNode.hideWhenShow
}
