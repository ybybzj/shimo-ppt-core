import { EditorKit } from '../../editor/global'

class PPTShowSettings {
  isEndOnlyByEsc!: boolean
  isUseEndSlide!: boolean
  textOfEndingSlide!: string
  constructor() {
    this.setDefault()
  }
  setDefault() {
    this.isEndOnlyByEsc = false
    this.isUseEndSlide = true
    this.textOfEndingSlide = 'The end of slide preview. Click to exit.'
  }
  setEndOnlyByEsc(val: boolean) {
    this.isEndOnlyByEsc = val
  }
  setTextOfEndingSlide(val: string) {
    this.textOfEndingSlide = val
    EditorKit.editorUI.showManager.updateEndContent()
  }
  setUseEndSlide(val: boolean) {
    this.isUseEndSlide = val
  }
}

export const gShowSetting = new PPTShowSettings()
