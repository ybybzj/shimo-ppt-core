import {
  Evt_onSlideTransitionEnd,
  Evt_onSlideTransitionStart,
} from '../../editor/events/EventNames'
import { EditorKit } from '../../editor/global'
import { BrowserInfo } from '../../common/browserInfo'
import { EditorSkin } from '../../core/common/const/drawing'
import { EditorUI } from '../editorUI/EditorUI'
import { Matrix2D, MatrixUtils } from '../../core/graphic/Matrix'

import { SlideImageCache as SlideCacheImage } from './SlideCacheImage'
import { longActions } from '../../globals/longActions'
import { EditActionFlag } from '../../core/EditActionFlag'
import { ClipRect } from '../../core/graphic/type'

import {
  createAnimation,
  stopAnimation,
  startAnimation,
  isAnimationPlaying,
  animation,
} from '../../lib/utils/animation'
import { calculateDimensionInfo, getClipRectForPPTShow } from '../helpers'
import { drawCacheImage } from './animation'
import { zoomValueForFitToSlide } from '../../core/utilities/zoom'
import { WithBounds } from '../../core/graphic/Bounds'
import { createCanvasImage, fillStyledRect } from './utils'
import {
  TransitionOptions,
  TransitionTypes,
} from '../../core/Slide/SlideTransition'

export class TransitionManager {
  editorUI: EditorUI
  type: (typeof TransitionTypes)[keyof typeof TransitionTypes]
  option: -1 | (typeof TransitionOptions)[keyof typeof TransitionOptions]
  duration: number
  startTime: number
  endTime: number
  curTime: number
  curCacheImg: SlideCacheImage
  nextCacheImg: SlideCacheImage
  rect: ClipRect = { x: 0, y: 0, h: 0, w: 0 }
  params: {
    firstOfNextHalf?: boolean
    transCacheCanvas?: HTMLCanvasElement
  } = {}
  isBackward: boolean
  isShow?: boolean
  animation?: animation
  constructor(editorUI: EditorUI, isShow?: boolean) {
    this.editorUI = editorUI

    this.type = 0
    this.option = 0
    this.duration = 0

    this.startTime = 0
    this.endTime = 0
    this.curTime = 0

    this.curCacheImg = new SlideCacheImage()
    this.nextCacheImg = new SlideCacheImage()
    this.isBackward = false
    this.isShow = !!isShow
  }

  getViewAndContext() {
    const mainView = this.isShow
      ? this.editorUI.showManager.canvas!
      : this.editorUI.animationLayer.element
    // console.log('isAnimationLayer', mainView.id === 'sm-owpbz')
    const layerView = this.isShow
      ? this.editorUI.showManager.layer!
      : this.editorUI.renderingController.editLayer.canvas

    return {
      mainView,
      mainCtx: mainView.getContext('2d')!,
      layerView,
      layerCtx: layerView.getContext('2d')!,
    }
  }

  getCurrentSlideIndex() {
    let index = 0
    if (!this.isShow) {
      const { curSlideIndex: curSlideIndex, countOfSlides: countOfSlides } =
        this.editorUI.renderingController
      index = curSlideIndex
      if (index >= countOfSlides) {
        index = countOfSlides - 1
      }
    } else if (this.editorUI?.showManager) {
      index = this.editorUI.showManager.getPrevUnhiddenSlideIndex(true)
    }
    return index
  }

  clearMainViewBackground() {
    const { mainCtx, mainView } = this.getViewAndContext()
    fillStyledRect(
      mainCtx,
      this.isShow ? EditorSkin.Background_Dark : EditorSkin.Background_Light,
      0,
      0,
      mainView.width,
      mainView.height
    )
  }

  clearLayerViewBackground() {
    const { layerCtx } = this.getViewAndContext()
    const { x, y, w, h } = this.rect
    if (!this.isShow) {
      const editLayer = this.editorUI.renderingController.editLayer
      editLayer.clear()
      editLayer.updateByRect(x, y, w, h)
    } else {
      layerCtx.clearRect(x, y, w, h)
    }
  }

  calcPercent() {
    const pct = (this.curTime - this.startTime) / this.duration
    if (this.isBackward) {
      return 1 - pct
    }
    return pct
  }

  startAnim(callback) {
    this.stopAnim()
    this.animation = createAnimation(callback)

    startAnimation(this.animation)
  }

  stopAnim() {
    if (this.animation) {
      stopAnimation(this.animation)
    }
  }

  private calculateRectForPreview = () => {
    const editorUI = this.editorUI
    const element = editorUI.animationLayer.element
    const zoom = zoomValueForFitToSlide(editorUI.presentation, {
      width: () => element.width,
      height: () => element.height,
    })

    const bounds: WithBounds = {
      ...editorUI.slideRenderer.getBounds(),
      minX: 0,
      minY: 0,
    }
    const info = calculateDimensionInfo(editorUI, zoom, bounds, element)

    this.rect.x = info.centerX - info.centerXOfSlide - info.horizontalLeft
    this.rect.y = info.centerY - info.centerYOfSlide - info.verticalTop
    this.rect.w = info.slideWidth
    this.rect.h = info.slideHeight
  }

  calcRectForPPTShow = () => {
    const { x, y, w, h } = getClipRectForPPTShow(this.editorUI)
    this.rect.x = x
    this.rect.y = y
    this.rect.w = w
    this.rect.h = h
  }

  private resetTransform = () => {
    const { mainCtx, layerCtx } = this.getViewAndContext()
    if (!this.isShow) {
      mainCtx.setTransform(
        BrowserInfo.PixelRatio,
        0,
        0,
        BrowserInfo.PixelRatio,
        0,
        0
      )
      layerCtx.setTransform(
        BrowserInfo.PixelRatio,
        0,
        0,
        BrowserInfo.PixelRatio,
        0,
        0
      )
    } else {
      mainCtx?.setTransform(1, 0, 0, 1, 0, 0)
      layerCtx?.setTransform(1, 0, 0, 1, 0, 0)
    }
  }
  private drawLastFrameImage = (slideIndex: number | null) => {
    if (null == slideIndex) {
      slideIndex = getCurrentSlideIndex(this.editorUI)
    }

    if (slideIndex > 0) {
      this.curCacheImg.slideImage = drawCacheImage(
        this.editorUI,
        slideIndex - 1,
        this.rect,
        createCanvasImage,
        'last'
      )
    }
  }
  private drawFirstFrameImage = (slideIndex: number | null) => {
    if (null == slideIndex) {
      slideIndex = getCurrentSlideIndex(this.editorUI)
    }

    if (slideIndex >= 0) {
      this.nextCacheImg.slideImage = drawCacheImage(
        this.editorUI,
        slideIndex,
        this.rect,
        createCanvasImage,
        'first'
      )
    }
  }

  start = () => {
    this.stopAnim()
    if (!this.isShow) {
      longActions.start(
        EditActionFlag.action_Presentation_PreviewSlideTransition
      )
      this.editorUI.editorView.element.style.visibility = 'hidden'
      this.editorUI.animationLayer.element.style.visibility = 'visible'
      this.calculateRectForPreview()
      const currentSlideIndex = this.getCurrentSlideIndex()
      this.drawLastFrameImage(currentSlideIndex)
      this.drawFirstFrameImage(currentSlideIndex)
    }

    this.startTime = new Date().getTime()
    this.endTime = this.startTime + this.duration

    const ctx = this.editorUI.animationLayer.element.getContext('2d')!
    ctx.setTransform(BrowserInfo.PixelRatio, 0, 0, BrowserInfo.PixelRatio, 0, 0)

    EditorKit.emitEvent(Evt_onSlideTransitionStart)

    let animFn
    switch (this.type) {
      case TransitionTypes.Fade: {
        animFn = this.animFade
        break
      }
      case TransitionTypes.Push: {
        animFn = this.animPush
        break
      }
      case TransitionTypes.Wipe: {
        animFn = this.animWipe
        break
      }
      case TransitionTypes.Split: {
        animFn = this.animSplit
        break
      }
      case TransitionTypes.Uncover: {
        animFn = this.animUnCover
        break
      }
      case TransitionTypes.Cover: {
        animFn = this.animCover
        break
      }
      case TransitionTypes.Clock: {
        animFn = this.animClock
        break
      }
      case TransitionTypes.Zoom: {
        animFn = this.animZoom
        break
      }
    }

    if (typeof animFn === 'function') {
      this.startAnim(animFn)
    } else {
      this.end()
    }
  }

  emitEnd() {
    longActions.end(EditActionFlag.action_Presentation_PreviewSlideTransition)
    EditorKit.emitEvent(Evt_onSlideTransitionEnd)
  }

  end = () => {
    this.stopAnim()

    this.params = {}
    this.emitEnd()
    this.editorUI.animationLayer.element.style.visibility = 'hidden'
    this.editorUI.editorView.element.style.visibility = 'visible'
    if (this.isShow) {
      this.editorUI.showManager.onTransitionEnd()
      this.curCacheImg.slideImage = null
      this.nextCacheImg.slideImage = null
      return
    }

    this.curCacheImg.slideImage = null
    this.nextCacheImg.slideImage = null

    const { mainCtx } = this.getViewAndContext()
    mainCtx.setTransform(1, 0, 0, 1, 0, 0)

    this.editorUI.prepareRender()
  }

  isPlaying() {
    return !!this.animation ? isAnimationPlaying(this.animation) : false
  }

  checkTimeEnd() {
    this.curTime = new Date().getTime()
    const isTimeEnd = this.curTime >= this.endTime
    if (isTimeEnd) {
      this.end()
    }
    return isTimeEnd
  }

  // animations
  animFade = () => {
    if (this.checkTimeEnd()) {
      return
    }

    this.resetTransform()

    const { x, y, w, h } = this.rect

    if (!this.isPlaying()) {
      this.params = { firstOfNextHalf: true }

      this.clearMainViewBackground()
      const { mainCtx } = this.getViewAndContext()

      if (!this.isBackward) {
        if (null != this.curCacheImg.slideImage) {
          mainCtx.drawImage(this.curCacheImg.slideImage, x, y, w, h)
        } else {
          fillStyledRect(mainCtx, this.curCacheImg.color, x, y, w, h)
        }
      } else {
        fillStyledRect(mainCtx, EditorSkin.Background_Dark, x, y, w, h)
      }
    }

    const { layerCtx } = this.getViewAndContext()
    this.clearLayerViewBackground()

    const pct = this.calcPercent()

    switch (this.option) {
      case TransitionOptions.FadeSmoothly:
        layerCtx.globalAlpha = pct

        if (null != this.nextCacheImg.slideImage) {
          layerCtx.drawImage(this.nextCacheImg.slideImage, x, y, w, h)
        } else {
          fillStyledRect(layerCtx, this.nextCacheImg.color, x, y, w, h)
        }

        layerCtx.globalAlpha = 1
        break
      case TransitionOptions.FadeThroughBlack:
        if (!this.isBackward) {
          if (this.params.firstOfNextHalf) {
            if (pct > 0.5) {
              const { mainCtx } = this.getViewAndContext()
              fillStyledRect(mainCtx, EditorSkin.Background_Dark, x, y, w, h)
              this.params.firstOfNextHalf = false
            }
          }

          if (this.params.firstOfNextHalf) {
            layerCtx.globalAlpha = 2 * pct
            fillStyledRect(layerCtx, EditorSkin.Background_Dark, x, y, w, h)
          } else {
            layerCtx.globalAlpha = 2 * (pct - 0.5)

            if (null != this.nextCacheImg.slideImage) {
              layerCtx.drawImage(this.nextCacheImg.slideImage, x, y, w, h)
            } else {
              fillStyledRect(layerCtx, this.nextCacheImg.color, x, y, w, h)
            }
          }
        } else {
          if (this.params.firstOfNextHalf) {
            if (pct < 0.5) {
              const { mainCtx } = this.getViewAndContext()

              if (null != this.curCacheImg.slideImage) {
                mainCtx.drawImage(this.curCacheImg.slideImage, x, y, w, h)
              } else {
                fillStyledRect(mainCtx, this.curCacheImg.color, x, y, w, h)
              }

              this.params.firstOfNextHalf = false
            }
          }

          if (!this.params.firstOfNextHalf) {
            layerCtx.globalAlpha = 2 * pct
            fillStyledRect(layerCtx, EditorSkin.Background_Dark, x, y, w, h)
          } else {
            layerCtx.globalAlpha = 2 * (pct - 0.5)

            if (null != this.nextCacheImg.slideImage) {
              layerCtx.drawImage(this.nextCacheImg.slideImage, x, y, w, h)
            } else {
              fillStyledRect(layerCtx, this.nextCacheImg.color, x, y, w, h)
            }
          }
        }
        layerCtx.globalAlpha = 1
        break
    }
  }

  animPush = () => {
    if (this.checkTimeEnd()) {
      return
    }

    this.resetTransform()

    let { x, y, w, h } = this.rect

    if (!this.isPlaying()) {
      this.params = { firstOfNextHalf: true }
      this.clearMainViewBackground()
    }

    let [x1, y1] = [0, 0]

    let xx = this.rect.x
    let yy = this.rect.y
    let ww = this.rect.w
    let hh = this.rect.h

    let [_x1, _y1] = [0, 0]

    const pct = this.calcPercent()

    const _offX = (w * (1 - pct)) >> 0
    const _offY = (h * (1 - pct)) >> 0

    switch (this.option) {
      case TransitionOptions.DirectionLeft: {
        x1 = _offX
        w -= _offX

        xx += w
        ww -= w
        break
      }
      case TransitionOptions.DirectionRight: {
        x += _offX
        w -= _offX

        _x1 = w
        ww -= w
        break
      }
      case TransitionOptions.DirectionTop: {
        y1 = _offY
        h -= _offY

        yy += h
        hh -= h
        break
      }
      case TransitionOptions.DirectionBottom: {
        y += _offY
        h -= _offY

        _y1 = h
        hh -= h
        break
      }
      default:
        break
    }

    const { layerCtx } = this.getViewAndContext()
    this.clearLayerViewBackground()

    if (ww > 0 && hh > 0) {
      if (null != this.curCacheImg.slideImage) {
        layerCtx.drawImage(
          this.curCacheImg.slideImage,
          _x1,
          _y1,
          ww,
          hh,
          xx,
          yy,
          ww,
          hh
        )
      } else {
        fillStyledRect(layerCtx, this.nextCacheImg.color, xx, yy, ww, hh)
      }
    }

    if (w > 0 && h > 0) {
      if (null != this.nextCacheImg.slideImage) {
        layerCtx.drawImage(
          this.nextCacheImg.slideImage,
          x1,
          y1,
          w,
          h,
          x,
          y,
          w,
          h
        )
      } else {
        fillStyledRect(layerCtx, this.nextCacheImg.color, x, y, w, h)
      }
    }
  }

  animWipe = () => {
    if (this.checkTimeEnd()) {
      return
    }

    this.resetTransform()
    const { x, y, w, h } = this.rect
    if (!this.isPlaying()) {
      this.clearMainViewBackground()
      const { mainCtx } = this.getViewAndContext()

      if (null != this.curCacheImg.slideImage) {
        mainCtx.drawImage(this.curCacheImg.slideImage, x, y, w, h)
      } else {
        fillStyledRect(mainCtx, this.curCacheImg.color, x, y, w, h)
      }
    }

    const pct = this.calcPercent()

    const { layerCtx } = this.getViewAndContext()
    this.clearLayerViewBackground()

    const _factorWipeLen = 1

    switch (this.option) {
      case TransitionOptions.DirectionLeft: {
        if (!this.isPlaying()) {
          const tmpCanvas = document.createElement('canvas')
          tmpCanvas.width = 256
          tmpCanvas.height = 1
          const tmpCtx = tmpCanvas.getContext('2d')!
          const imageData = tmpCtx.createImageData(256, 1)
          for (let i = 0; i < 256; i++) imageData.data[4 * i + 3] = 255 - i
          tmpCtx.putImageData(imageData, 0, 0)

          this.params = { transCacheCanvas: tmpCanvas }
        }

        const startOfX = (x - _factorWipeLen * w) >> 0
        const _xPos = (startOfX + pct * (1 + _factorWipeLen) * w) >> 0
        const graduateWidth = (_factorWipeLen * w) >> 0

        if (_xPos > x) {
          if (_xPos + graduateWidth > x + w) {
            layerCtx.beginPath()

            fillStyledRect(
              layerCtx,
              EditorSkin.Background_Dark,
              x,
              y,
              _xPos - x + 1,
              h
            )

            const srcW = ((256 * (w - _xPos + x)) / graduateWidth) >> 0
            if (srcW > 0 && w - _xPos + x > 0) {
              layerCtx.drawImage(
                this.params.transCacheCanvas!,
                0,
                0,
                srcW,
                1,
                _xPos,
                y,
                w - _xPos + x,
                h
              )
            }
          } else {
            layerCtx.beginPath()

            fillStyledRect(
              layerCtx,
              EditorSkin.Background_Dark,
              x,
              y,
              _xPos - x + 1,
              h
            )

            if (graduateWidth > 0) {
              layerCtx.drawImage(
                this.params.transCacheCanvas!,
                _xPos,
                y,
                graduateWidth,
                h
              )
            }
          }
        } else {
          const srcW = _xPos + graduateWidth - x
          const srcWW = (256 * (_xPos + graduateWidth - x)) / graduateWidth

          if (srcW > 0 && srcWW > 0) {
            layerCtx.drawImage(
              this.params.transCacheCanvas!,
              256 - srcWW,
              0,
              srcWW,
              1,
              x,
              y,
              srcW,
              h
            )
          }
        }
        break
      }
      case TransitionOptions.DirectionRight: {
        if (!this.isPlaying()) {
          const tmpCanvas = document.createElement('canvas')
          tmpCanvas.width = 256
          tmpCanvas.height = 1
          const tmpCtx = tmpCanvas.getContext('2d')!
          const imageData = tmpCtx.createImageData(256, 1)
          for (let i = 0; i < 256; i++) imageData.data[4 * i + 3] = i
          tmpCtx.putImageData(imageData, 0, 0)

          this.params = { transCacheCanvas: tmpCanvas }
        }

        const rightOfDest = x + w

        const startOfX = (rightOfDest + _factorWipeLen * w) >> 0
        const _xPos = (startOfX - pct * (1 + _factorWipeLen) * w) >> 0
        const graduateWidth = (_factorWipeLen * w) >> 0

        if (_xPos < rightOfDest) {
          if (_xPos - graduateWidth < x) {
            fillStyledRect(
              layerCtx,
              EditorSkin.Background_Dark,
              _xPos,
              y,
              rightOfDest - _xPos,
              h
            )

            const srcW =
              ((256 * (x - _xPos + graduateWidth)) / graduateWidth) >> 0
            if (srcW > 0 && _xPos - x > 0) {
              layerCtx.drawImage(
                this.params.transCacheCanvas!,
                srcW,
                0,
                256 - srcW,
                1,
                x,
                y,
                _xPos - x,
                h
              )
            }
          } else {
            fillStyledRect(
              layerCtx,
              EditorSkin.Background_Dark,
              _xPos,
              y,
              rightOfDest - _xPos + 1,
              h
            )

            if (graduateWidth > 0) {
              layerCtx.drawImage(
                this.params.transCacheCanvas!,
                _xPos - graduateWidth,
                y,
                graduateWidth,
                h
              )
            }
          }
        } else {
          const _graduateWidth = startOfX - _xPos
          if (_graduateWidth > 0) {
            const srcW = (256 * _graduateWidth) / graduateWidth

            if (srcW > 0 && rightOfDest - _xPos + graduateWidth > 0) {
              layerCtx.drawImage(
                this.params.transCacheCanvas!,
                0,
                0,
                srcW,
                1,
                _xPos - graduateWidth,
                y,
                rightOfDest - _xPos + graduateWidth,
                h
              )
            }
          }
        }
        break
      }
      case TransitionOptions.DirectionTop: {
        if (!this.isPlaying()) {
          const tmpCanvas = document.createElement('canvas')
          tmpCanvas.width = 1
          tmpCanvas.height = 256
          const tmpCtx = tmpCanvas.getContext('2d')!
          const imageData = tmpCtx.createImageData(1, 256)
          for (let i = 0; i < 256; i++) imageData.data[4 * i + 3] = 255 - i
          tmpCtx.putImageData(imageData, 0, 0)

          this.params = { transCacheCanvas: tmpCanvas }
        }

        const yStartPos = (y - _factorWipeLen * h) >> 0
        const yPos = (yStartPos + pct * (1 + _factorWipeLen) * h) >> 0
        const graduateHeight = (_factorWipeLen * h) >> 0

        if (yPos > y) {
          if (yPos + graduateHeight > y + h) {
            fillStyledRect(
              layerCtx,
              EditorSkin.Background_Dark,
              x,
              y,
              w,
              yPos - y + 1
            )

            const srcImgH = ((256 * (h - yPos + y)) / graduateHeight) >> 0
            if (srcImgH > 0 && h - yPos + y > 0) {
              layerCtx.drawImage(
                this.params.transCacheCanvas!,
                0,
                0,
                1,
                srcImgH,
                x,
                yPos,
                w,
                h - yPos + y
              )
            }
          } else {
            fillStyledRect(
              layerCtx,
              EditorSkin.Background_Dark,
              x,
              y,
              w,
              yPos - y + 1
            )

            if (graduateHeight > 0) {
              layerCtx.drawImage(
                this.params.transCacheCanvas!,
                x,
                yPos,
                w,
                graduateHeight
              )
            }
          }
        } else {
          const srcImgH = yPos + graduateHeight - y
          const srcImgHH = (256 * (yPos + graduateHeight - y)) / graduateHeight

          if (srcImgH > 0 && srcImgHH > 0) {
            layerCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              256 - srcImgHH,
              1,
              srcImgHH,
              x,
              y,
              w,
              srcImgH
            )
          }
        }
        break
      }
      case TransitionOptions.DirectionBottom: {
        if (!this.isPlaying()) {
          const tmpCanvas = document.createElement('canvas')
          tmpCanvas.width = 1
          tmpCanvas.height = 256
          const tmpCtx = tmpCanvas.getContext('2d')!
          const imageData = tmpCtx.createImageData(1, 256)
          for (let i = 0; i < 256; i++) imageData.data[4 * i + 3] = i
          tmpCtx.putImageData(imageData, 0, 0)

          this.params = { transCacheCanvas: tmpCanvas }
        }

        const bottomOrgDest = y + h
        const startPosY = (bottomOrgDest + _factorWipeLen * h) >> 0
        const _yPos = (startPosY - pct * (1 + _factorWipeLen) * h) >> 0
        const _graduateHeight = (_factorWipeLen * h) >> 0

        if (_yPos < bottomOrgDest) {
          if (_yPos - _graduateHeight < y) {
            fillStyledRect(
              layerCtx,
              EditorSkin.Background_Dark,
              x,
              _yPos,
              w,
              bottomOrgDest - _yPos
            )

            const srcImgH = ((256 * (_yPos - y)) / _graduateHeight) >> 0
            if (srcImgH > 0 && _yPos - y > 0) {
              layerCtx.drawImage(
                this.params.transCacheCanvas!,
                0,
                256 - srcImgH,
                1,
                srcImgH,
                x,
                y,
                w,
                _yPos - y
              )
            }
          } else {
            fillStyledRect(
              layerCtx,
              EditorSkin.Background_Dark,
              x,
              _yPos,
              w,
              bottomOrgDest - _yPos
            )

            if (_graduateHeight > 0) {
              layerCtx.drawImage(
                this.params.transCacheCanvas!,
                x,
                _yPos - _graduateHeight,
                w,
                _graduateHeight
              )
            }
          }
        } else {
          const srcImgH = bottomOrgDest - (_yPos - _graduateHeight)
          const srcImgHH = (256 * srcImgH) / _graduateHeight

          if (srcImgH > 0 && srcImgHH > 0) {
            layerCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              0,
              1,
              srcImgHH,
              x,
              bottomOrgDest - srcImgH,
              w,
              srcImgH
            )
          }
        }
        break
      }
      case TransitionOptions.DirectionTopLeft: {
        if (!this.isPlaying()) {
          const tmpCanvas = document.createElement('canvas')
          tmpCanvas.width = 256
          tmpCanvas.height = 1
          const tmpCtx = tmpCanvas.getContext('2d')!
          const imageData = tmpCtx.createImageData(256, 1)
          for (let i = 0; i < 256; i++) imageData.data[4 * i + 3] = 255 - i
          tmpCtx.putImageData(imageData, 0, 0)

          this.params = { transCacheCanvas: tmpCanvas }
        }

        const ang = Math.atan(h / w)
        const sin = Math.sin(ang)
        const cos = Math.cos(ang)

        const sinW = w * sin
        const hDistance = 2 * sinW
        const wDistance = w * cos + h * sin

        const factorX = -sin
        const factorY = -cos

        const graduateWidth = (_factorWipeLen * hDistance) >> 0

        const _cX = x + w / 2
        const _cY = y + h / 2
        const startX = _cX + (sinW + graduateWidth / 2) * factorX
        const startY = _cY + (sinW + graduateWidth / 2) * factorY

        const posX = startX - factorX * pct * (graduateWidth + hDistance)
        const posY = startY - factorY * pct * (graduateWidth + hDistance)

        layerCtx.save()
        layerCtx.beginPath()
        layerCtx.rect(x, y, w, h)
        layerCtx.clip()
        layerCtx.beginPath()

        layerCtx.translate(posX, posY)
        layerCtx.rotate(Math.PI / 2 - ang)

        fillStyledRect(
          layerCtx,
          EditorSkin.Background_Dark,
          -sinW - graduateWidth,
          -wDistance / 2,
          graduateWidth,
          wDistance
        )

        layerCtx.drawImage(
          this.params.transCacheCanvas!,
          -sinW,
          -wDistance / 2,
          hDistance,
          wDistance
        )

        layerCtx.restore()
        break
      }
      case TransitionOptions.DirectionTopRight: {
        if (!this.isPlaying()) {
          const tmpCanvas = document.createElement('canvas')
          tmpCanvas.width = 256
          tmpCanvas.height = 1
          const tmpCtx = tmpCanvas.getContext('2d')!
          const imageData = tmpCtx.createImageData(256, 1)
          for (let i = 0; i < 256; i++) imageData.data[4 * i + 3] = i
          tmpCtx.putImageData(imageData, 0, 0)

          this.params = { transCacheCanvas: tmpCanvas }
        }

        const ang = Math.atan(h / w)
        const sin = Math.sin(ang)
        const cos = Math.cos(ang)

        const sinW = w * sin
        const wDistance = 2 * sinW
        const hDistance = w * cos + h * sin

        const factorX = sin
        const factorY = -cos

        const graduateWidth = (_factorWipeLen * wDistance) >> 0

        const _cX = x + w / 2
        const _cY = y + h / 2
        const startX = _cX + (sinW + graduateWidth / 2) * factorX
        const startY = _cY + (sinW + graduateWidth / 2) * factorY

        const posX = startX - factorX * pct * (graduateWidth + wDistance)
        const posY = startY - factorY * pct * (graduateWidth + wDistance)

        layerCtx.save()
        layerCtx.beginPath()
        layerCtx.rect(x, y, w, h)
        layerCtx.clip()
        layerCtx.beginPath()

        layerCtx.translate(posX, posY)
        layerCtx.rotate(ang - Math.PI / 2)

        fillStyledRect(
          layerCtx,
          EditorSkin.Background_Dark,
          sinW,
          -hDistance / 2,
          graduateWidth,
          hDistance
        )

        layerCtx.drawImage(
          this.params.transCacheCanvas!,
          -sinW,
          -hDistance / 2,
          wDistance,
          hDistance
        )

        layerCtx.restore()
        break
      }
      case TransitionOptions.DirectionBottomLeft: {
        if (!this.isPlaying()) {
          const tmpCanvas = document.createElement('canvas')
          tmpCanvas.width = 256
          tmpCanvas.height = 1
          const tmpCtx = tmpCanvas.getContext('2d')!
          const imageData = tmpCtx.createImageData(256, 1)
          for (let i = 0; i < 256; i++) imageData.data[4 * i + 3] = 255 - i
          tmpCtx.putImageData(imageData, 0, 0)

          this.params = { transCacheCanvas: tmpCanvas }
        }

        const ang = Math.atan(h / w)
        const sin = Math.sin(ang)
        const cos = Math.cos(ang)

        const sinW = w * sin
        const wDistance = 2 * sinW
        const hDistance = w * cos + h * sin

        const factorX = sin
        const factorY = -cos

        const graduateWidth = (_factorWipeLen * wDistance) >> 0

        const _cX = x + w / 2
        const _cY = y + h / 2
        const startX = _cX - (sinW + graduateWidth / 2) * factorX
        const startY = _cY - (sinW + graduateWidth / 2) * factorY

        const posX = startX + factorX * pct * (graduateWidth + wDistance)
        const posY = startY + factorY * pct * (graduateWidth + wDistance)

        layerCtx.save()
        layerCtx.beginPath()
        layerCtx.rect(x, y, w, h)
        layerCtx.clip()
        layerCtx.beginPath()

        layerCtx.translate(posX, posY)
        layerCtx.rotate(ang - Math.PI / 2)

        fillStyledRect(
          layerCtx,
          EditorSkin.Background_Dark,
          -sinW - graduateWidth,
          -hDistance / 2,
          graduateWidth,
          hDistance
        )

        layerCtx.drawImage(
          this.params.transCacheCanvas!,
          -sinW,
          -hDistance / 2,
          wDistance,
          hDistance
        )

        layerCtx.restore()
        break
      }
      case TransitionOptions.DirectionBottomRight: {
        if (!this.isPlaying()) {
          const tmpCanvas = document.createElement('canvas')
          tmpCanvas.width = 256
          tmpCanvas.height = 1
          const tmpCtx = tmpCanvas.getContext('2d')!
          const imageData = tmpCtx.createImageData(256, 1)
          for (let i = 0; i < 256; i++) imageData.data[4 * i + 3] = i
          tmpCtx.putImageData(imageData, 0, 0)

          this.params = { transCacheCanvas: tmpCanvas }
        }

        const ang = Math.atan(h / w)
        const sin = Math.sin(ang)
        const cos = Math.cos(ang)

        const sinW = w * sin
        const hDistance = 2 * sinW
        const wDistance = w * cos + h * sin

        const factorX = sin
        const factorY = cos

        const graduateWidth = (_factorWipeLen * hDistance) >> 0

        const _cX = x + w / 2
        const _cY = y + h / 2
        const startX = _cX + (sinW + graduateWidth / 2) * factorX
        const startY = _cY + (sinW + graduateWidth / 2) * factorY

        const posX = startX - factorX * pct * (graduateWidth + hDistance)
        const posY = startY - factorY * pct * (graduateWidth + hDistance)

        layerCtx.save()
        layerCtx.beginPath()
        layerCtx.rect(x, y, w, h)
        layerCtx.clip()
        layerCtx.beginPath()

        layerCtx.translate(posX, posY)
        layerCtx.rotate(Math.PI / 2 - ang)

        fillStyledRect(
          layerCtx,
          EditorSkin.Background_Dark,
          sinW,
          -wDistance / 2,
          graduateWidth,
          wDistance
        )

        layerCtx.drawImage(
          this.params.transCacheCanvas!,
          -sinW,
          -wDistance / 2,
          hDistance,
          wDistance
        )

        layerCtx.restore()
        break
      }
      default:
        break
    }

    layerCtx.globalCompositeOperation = 'source-atop'
    if (null != this.nextCacheImg.slideImage) {
      layerCtx.drawImage(this.nextCacheImg.slideImage, x, y, w, h)
    } else {
      fillStyledRect(layerCtx, this.nextCacheImg.color, x, y, w, h)
    }

    layerCtx.globalCompositeOperation = 'source-over'
  }

  animSplit = () => {
    if (this.checkTimeEnd()) {
      return
    }

    this.resetTransform()

    const { x, y, w, h } = this.rect

    const pct = this.calcPercent()

    const { layerCtx: mainCtx } = this.getViewAndContext()
    this.clearLayerViewBackground()

    if (!this.isPlaying()) {
      const { mainCtx } = this.getViewAndContext()
      this.clearMainViewBackground()
      if (null != this.curCacheImg.slideImage) {
        mainCtx.drawImage(this.curCacheImg.slideImage, x, y, w, h)
      } else {
        fillStyledRect(mainCtx, this.curCacheImg.color, x, y, w, h)
      }
    }

    switch (this.option) {
      case TransitionOptions.SplitVerticalOut: {
        if (!this.isPlaying()) {
          const tmpCanvas = document.createElement('canvas')
          const __w = 256 + 255
          tmpCanvas.width = __w
          tmpCanvas.height = 1
          const tmpCtx = tmpCanvas.getContext('2d')!
          const imageData = tmpCtx.createImageData(tmpCanvas.width, 1)
          for (let i = 0; i < 256; i++) imageData.data[4 * i + 3] = i
          for (let i = 256; i < __w; i++) {
            imageData.data[4 * i + 3] = __w - i - 1
          }
          tmpCtx.putImageData(imageData, 0, 0)

          this.params = { transCacheCanvas: tmpCanvas }
        }

        const _cX = x + w / 2

        if (pct <= 0.5) {
          const _w = (pct * 2 * w) >> 0
          const _w2 = _w >> 1

          if (_w > 0 && _w2 > 0) {
            fillStyledRect(
              mainCtx,
              EditorSkin.Background_Dark,
              Math.max(x, _cX - _w2 / 2 - 1),
              y,
              Math.min(_w2 + 2, w),
              h
            )

            const _w4 = _w2 >> 1
            const _x = _cX - _w2
            const _r = _cX + _w4
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              0,
              255,
              1,
              _x,
              y,
              _w4,
              h
            )
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              255,
              0,
              255,
              1,
              _r,
              y,
              _w4,
              h
            )
          }
        } else {
          const _w = (pct * w) >> 0
          const _w2 = _w >> 1

          fillStyledRect(
            mainCtx,
            EditorSkin.Background_Dark,
            Math.max(x, _cX - _w2 - 1),
            y,
            Math.min(_w + 2, w),
            h
          )

          const _graduateWidth = (w - _w) >> 1

          if (_graduateWidth > 0) {
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              0,
              255,
              1,
              x,
              y,
              _graduateWidth,
              h
            )
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              255,
              0,
              255,
              1,
              _cX + _w2,
              y,
              _graduateWidth,
              h
            )
          }
        }
        break
      }
      case TransitionOptions.SplitVerticalIn: {
        if (!this.isPlaying()) {
          const tmpCanvas = document.createElement('canvas')
          const __w = 256 + 255
          tmpCanvas.width = __w
          tmpCanvas.height = 1
          const tmpCtx = tmpCanvas.getContext('2d')!
          const imageData = tmpCtx.createImageData(tmpCanvas.width, 1)
          for (let i = 0; i < 256; i++) imageData.data[4 * i + 3] = i
          for (let i = 256; i < __w; i++) {
            imageData.data[4 * i + 3] = __w - i - 1
          }
          tmpCtx.putImageData(imageData, 0, 0)

          this.params = { transCacheCanvas: tmpCanvas }
        }

        const _cX = x + w / 2

        if (pct <= 0.5) {
          const _w = (pct * 2 * w) >> 0
          const _w2 = _w >> 1
          const _w4 = _w2 >> 1

          if (_w4 > 0) {
            fillStyledRect(
              mainCtx,
              EditorSkin.Background_Dark,
              x,
              y,
              _w4 + 1,
              h
            )
            fillStyledRect(
              mainCtx,
              EditorSkin.Background_Dark,
              x + w - _w4 - 1,
              y,
              _w4 + 1,
              h
            )

            const _x = x + _w4
            const _r = x + w - _w2
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              255,
              0,
              255,
              1,
              _x,
              y,
              _w4,
              h
            )
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              0,
              255,
              1,
              _r,
              y,
              _w4,
              h
            )
          }
        } else {
          const _w = (pct * w) >> 0
          const _w2 = _w >> 1

          fillStyledRect(mainCtx, EditorSkin.Background_Dark, x, y, _w2 + 1, h)
          fillStyledRect(
            mainCtx,
            EditorSkin.Background_Dark,
            x + w - _w2 - 1,
            y,
            _w2 + 1,
            h
          )

          const _graduateWidth = (w - _w) >> 1

          if (_graduateWidth > 0) {
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              255,
              0,
              255,
              1,
              x + _w2,
              y,
              _graduateWidth,
              h
            )
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              0,
              255,
              1,
              _cX,
              y,
              _graduateWidth,
              h
            )
          }
        }
        break
      }
      case TransitionOptions.SplitHorizontalOut: {
        if (!this.isPlaying()) {
          const tmpCanvas = document.createElement('canvas')
          const __w = 256 + 255
          tmpCanvas.width = 1
          tmpCanvas.height = __w
          const tmpCtx = tmpCanvas.getContext('2d')!
          const imageData = tmpCtx.createImageData(1, __w)
          for (let i = 0; i < 256; i++) imageData.data[4 * i + 3] = i
          for (let i = 256; i < __w; i++) {
            imageData.data[4 * i + 3] = __w - i - 1
          }
          tmpCtx.putImageData(imageData, 0, 0)

          this.params = { transCacheCanvas: tmpCanvas }
        }

        const _cY = y + h / 2

        if (pct <= 0.5) {
          const _h = (pct * 2 * h) >> 0
          const _h2 = _h >> 1

          if (_h > 0 && _h2 > 0) {
            fillStyledRect(
              mainCtx,
              EditorSkin.Background_Dark,
              x,
              Math.max(_cY - _h2 / 2 - 1),
              w,
              Math.min(_h2 + 2, h)
            )

            const _h4 = _h2 >> 1
            const _y = _cY - _h2
            const _b = _cY + _h4

            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              0,
              1,
              255,
              x,
              _y,
              w,
              _h4
            )
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              255,
              1,
              255,
              x,
              _b,
              w,
              _h4
            )
          }
        } else {
          const _h = (pct * h) >> 0
          const _h2 = _h >> 1

          fillStyledRect(
            mainCtx,
            EditorSkin.Background_Dark,
            x,
            Math.max(y, _cY - _h2 - 1),
            w,
            Math.min(_h + 2, h)
          )

          const _radH = (h - _h) >> 1

          if (_radH > 0) {
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              0,
              1,
              255,
              x,
              y,
              w,
              _radH
            )
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              255,
              1,
              255,
              x,
              _cY + _h2,
              w,
              _radH
            )
          }
        }
        break
      }
      case TransitionOptions.SplitHorizontalIn: {
        if (!this.isPlaying()) {
          const tmpCanvas = document.createElement('canvas')
          const __w = 256 + 255
          tmpCanvas.width = 1
          tmpCanvas.height = __w
          const tmpCtx = tmpCanvas.getContext('2d')!
          const imageData = tmpCtx.createImageData(1, __w)
          for (let i = 0; i < 256; i++) imageData.data[4 * i + 3] = i
          for (let i = 256; i < __w; i++) {
            imageData.data[4 * i + 3] = __w - i - 1
          }
          tmpCtx.putImageData(imageData, 0, 0)

          this.params = { transCacheCanvas: tmpCanvas }
        }

        const _cY = y + h / 2

        if (pct <= 0.5) {
          const _h = (pct * 2 * h) >> 0
          const _h2 = _h >> 1
          const _h4 = _h2 >> 1

          if (_h4 > 0) {
            fillStyledRect(
              mainCtx,
              EditorSkin.Background_Dark,
              x,
              y,
              w,
              _h4 + 1
            )
            fillStyledRect(
              mainCtx,
              EditorSkin.Background_Dark,
              x,
              y + h - _h4 - 1,
              w,
              _h4 + 1
            )

            const _y = y + _h4
            const _b = y + h - _h2
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              255,
              1,
              255,
              x,
              _y,
              w,
              _h4
            )
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              0,
              1,
              255,
              x,
              _b,
              w,
              _h4
            )
          }
        } else {
          const _h = (pct * h) >> 0
          const _h2 = _h >> 1

          fillStyledRect(mainCtx, EditorSkin.Background_Dark, x, y, w, _h2 + 1)
          fillStyledRect(
            mainCtx,
            EditorSkin.Background_Dark,
            x,
            y + h - _h2 - 1,
            w,
            _h2 + 1
          )

          const _radH = (h - _h) >> 1

          if (_radH > 0) {
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              255,
              1,
              255,
              x,
              y + _h2,
              w,
              _radH
            )
            mainCtx.drawImage(
              this.params.transCacheCanvas!,
              0,
              0,
              1,
              255,
              x,
              _cY,
              w,
              _radH
            )
          }
        }
        break
      }
      default:
        break
    }

    mainCtx.globalCompositeOperation = 'source-atop'
    if (null != this.nextCacheImg.slideImage) {
      mainCtx.drawImage(this.nextCacheImg.slideImage, x, y, w, h)
    } else {
      fillStyledRect(mainCtx, this.nextCacheImg.color, x, y, w, h)
    }

    mainCtx.globalCompositeOperation = 'source-over'
  }
  animUnCover = () => {
    if (this.checkTimeEnd()) {
      return
    }

    this.resetTransform()

    let { x, y, w, h } = this.rect
    if (!this.isPlaying()) {
      const { mainCtx } = this.getViewAndContext()
      this.clearMainViewBackground()

      if (null != this.nextCacheImg.slideImage) {
        mainCtx.drawImage(this.nextCacheImg.slideImage, x, y, w, h)
      } else {
        fillStyledRect(mainCtx, this.nextCacheImg.color, x, y, w, h)
      }
    }

    let _x1 = 0
    let _y1 = 0

    const pct = this.calcPercent()

    const _offX = (w * pct) >> 0
    const _offY = (h * pct) >> 0

    switch (this.option) {
      case TransitionOptions.DirectionLeft: {
        x += _offX
        w -= _offX
        break
      }
      case TransitionOptions.DirectionRight: {
        _x1 = _offX
        w -= _offX
        break
      }
      case TransitionOptions.DirectionTop: {
        y += _offY
        h -= _offY
        break
      }
      case TransitionOptions.DirectionBottom: {
        _y1 = _offY
        h -= _offY
        break
      }
      case TransitionOptions.DirectionTopLeft: {
        x += _offX
        y += _offY
        w -= _offX
        h -= _offY
        break
      }
      case TransitionOptions.DirectionTopRight: {
        _x1 = _offX
        y += _offY
        w -= _offX
        h -= _offY
        break
      }
      case TransitionOptions.DirectionBottomLeft: {
        x += _offX
        _y1 = _offY
        w -= _offX
        h -= _offY
        break
      }
      case TransitionOptions.DirectionBottomRight: {
        _x1 = _offX
        _y1 = _offY
        w -= _offX
        h -= _offY
        break
      }
      default:
        break
    }

    this.clearLayerViewBackground()

    const { layerCtx } = this.getViewAndContext()

    if (w > 0 && h > 0) {
      if (null != this.curCacheImg.slideImage) {
        layerCtx.drawImage(
          this.curCacheImg.slideImage,
          _x1,
          _y1,
          w,
          h,
          x,
          y,
          w,
          h
        )
      } else {
        fillStyledRect(layerCtx, this.curCacheImg.color, x, y, w, h)
      }
    }
  }
  animCover = () => {
    if (this.checkTimeEnd()) {
      return
    }

    this.resetTransform()

    let { x, y, w, h } = this.rect

    if (!this.isPlaying()) {
      const { mainCtx } = this.getViewAndContext()
      this.clearMainViewBackground()

      if (null != this.curCacheImg.slideImage) {
        mainCtx.drawImage(
          this.curCacheImg.slideImage,
          this.rect.x,
          this.rect.y,
          this.rect.w,
          this.rect.h
        )
      } else {
        fillStyledRect(mainCtx, this.curCacheImg.color, x, y, w, h)
      }
    }

    let _x1 = 0
    let _y1 = 0

    const pct = this.calcPercent()

    const _offX = (w * (1 - pct)) >> 0
    const _offY = (h * (1 - pct)) >> 0

    switch (this.option) {
      case TransitionOptions.DirectionLeft: {
        _x1 = _offX
        w -= _offX
        break
      }
      case TransitionOptions.DirectionRight: {
        x += _offX
        w -= _offX
        break
      }
      case TransitionOptions.DirectionTop: {
        _y1 = _offY
        h -= _offY
        break
      }
      case TransitionOptions.DirectionBottom: {
        y += _offY
        h -= _offY
        break
      }
      case TransitionOptions.DirectionTopLeft: {
        _x1 = _offX
        _y1 = _offY
        w -= _offX
        h -= _offY
        break
      }
      case TransitionOptions.DirectionTopRight: {
        x += _offX
        _y1 = _offY
        w -= _offX
        h -= _offY
        break
      }
      case TransitionOptions.DirectionBottomLeft: {
        _x1 = _offX
        y += _offY
        w -= _offX
        h -= _offY
        break
      }
      case TransitionOptions.DirectionBottomRight: {
        x += _offX
        y += _offY
        w -= _offX
        h -= _offY
        break
      }
      default:
        break
    }

    this.clearLayerViewBackground()
    const { layerCtx } = this.getViewAndContext()

    if (w > 0 && h > 0) {
      if (null != this.nextCacheImg.slideImage) {
        layerCtx.drawImage(
          this.nextCacheImg.slideImage,
          _x1,
          _y1,
          w,
          h,
          x,
          y,
          w,
          h
        )
      } else {
        fillStyledRect(layerCtx, this.nextCacheImg.color, x, y, w, h)
      }
    }
  }
  animClock = () => {
    if (this.checkTimeEnd()) {
      return
    }

    this.resetTransform()
    const { x, y, w, h } = this.rect

    if (!this.isPlaying()) {
      const { mainCtx } = this.getViewAndContext()
      this.clearMainViewBackground()

      if (null != this.curCacheImg.slideImage) {
        mainCtx.drawImage(this.curCacheImg.slideImage, x, y, w, h)
      } else {
        fillStyledRect(mainCtx, this.curCacheImg.color, x, y, w, h)
      }
    }

    const pct = this.calcPercent()

    const angle1 = Math.atan(w / h)
    const angle2 = Math.PI / 2 - angle1
    let _offset = 0

    this.clearLayerViewBackground()

    const { layerCtx } = this.getViewAndContext()

    layerCtx.save()
    layerCtx.beginPath()

    const _cX = x + w / 2
    const _cY = y + h / 2

    switch (this.option) {
      case TransitionOptions.ClockClockwise: {
        const _angle = 2 * Math.PI * pct
        let _x = 0
        let _y = 0

        const curPart = ((2 * _angle) / Math.PI) >> 0
        const leftPart = _angle - (curPart * Math.PI) / 2

        switch (curPart) {
          case 0: {
            if (leftPart > angle1) {
              _offset = (w * Math.tan(Math.PI / 2 - leftPart)) / 2

              _x = x + w
              _y = _cY - _offset

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(_x, y)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            } else {
              _offset = (h * Math.tan(leftPart)) / 2

              _x = _cX + _offset
              _y = y

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            }

            break
          }
          case 1: {
            if (leftPart > angle2) {
              _offset = (h * Math.tan(Math.PI / 2 - leftPart)) / 2

              _x = _cX + _offset
              _y = y + h

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(x + w, y)
              layerCtx.lineTo(x + w, y + h)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            } else {
              _offset = (w * Math.tan(leftPart)) / 2

              _x = x + w
              _y = _cY + _offset

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(_x, y)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            }

            break
          }
          case 2: {
            if (leftPart > angle1) {
              _offset = (w * Math.tan(Math.PI / 2 - leftPart)) / 2

              _x = x
              _y = _cY + _offset

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(x + w, y)
              layerCtx.lineTo(x + w, y + h)
              layerCtx.lineTo(x, y + h)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            } else {
              _offset = (h * Math.tan(leftPart)) / 2

              _x = _cX - _offset
              _y = y + h

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(x + w, y)
              layerCtx.lineTo(x + w, y + h)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            }

            break
          }
          case 3: {
            if (leftPart > angle2) {
              _offset = (h * Math.tan(Math.PI / 2 - leftPart)) / 2

              _x = _cX - _offset
              _y = y

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(x + w, y)
              layerCtx.lineTo(x + w, y + h)
              layerCtx.lineTo(x, y + h)
              layerCtx.lineTo(x, y)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            } else {
              _offset = (w * Math.tan(leftPart)) / 2

              _x = x
              _y = _cY - _offset

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(x + w, y)
              layerCtx.lineTo(x + w, y + h)
              layerCtx.lineTo(x, y + h)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            }

            break
          }
        }
        break
      }
      case TransitionOptions.ClockCounterclockwise: {
        const _angle = 2 * Math.PI * pct
        let _x = 0
        let _y = 0

        const curPart = ((2 * _angle) / Math.PI) >> 0
        const leftPart = _angle - (curPart * Math.PI) / 2

        switch (curPart) {
          case 0: {
            if (leftPart > angle1) {
              _offset = (w * Math.tan(Math.PI / 2 - leftPart)) / 2

              _x = x
              _y = _cY - _offset

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(_x, y)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            } else {
              _offset = (h * Math.tan(leftPart)) / 2

              _x = _cX - _offset
              _y = y

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            }

            break
          }
          case 1: {
            if (leftPart > angle2) {
              _offset = (h * Math.tan(Math.PI / 2 - leftPart)) / 2

              _x = _cX - _offset
              _y = y + h

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(x, y)
              layerCtx.lineTo(x, y + h)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            } else {
              _offset = (w * Math.tan(leftPart)) / 2

              _x = x
              _y = _cY + _offset

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(_x, y)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            }

            break
          }
          case 2: {
            if (leftPart > angle1) {
              _offset = (w * Math.tan(Math.PI / 2 - leftPart)) / 2

              _x = x + w
              _y = _cY + _offset

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(x, y)
              layerCtx.lineTo(x, y + h)
              layerCtx.lineTo(x + w, y + h)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            } else {
              _offset = (h * Math.tan(leftPart)) / 2

              _x = _cX + _offset
              _y = y + h

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(x, y)
              layerCtx.lineTo(x, y + h)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            }

            break
          }
          case 3: {
            if (leftPart > angle2) {
              _offset = (h * Math.tan(Math.PI / 2 - leftPart)) / 2

              _x = _cX + _offset
              _y = y

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(x, y)
              layerCtx.lineTo(x, y + h)
              layerCtx.lineTo(x + w, y + h)
              layerCtx.lineTo(x + w, y)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            } else {
              _offset = (w * Math.tan(leftPart)) / 2

              _x = x + w
              _y = _cY - _offset

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX, y)
              layerCtx.lineTo(x, y)
              layerCtx.lineTo(x, y + h)
              layerCtx.lineTo(x + w, y + h)
              layerCtx.lineTo(_x, _y)
              layerCtx.closePath()
            }

            break
          }
        }
        break
      }
      case TransitionOptions.ClockWedge: {
        const _angle = Math.PI * pct

        const curPart = ((2 * _angle) / Math.PI) >> 0
        const leftPart = _angle - (curPart * Math.PI) / 2

        switch (curPart) {
          case 0: {
            if (leftPart > angle1) {
              _offset = (w * Math.tan(Math.PI / 2 - leftPart)) / 2

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(x, _cY - _offset)
              layerCtx.lineTo(x, y)
              layerCtx.lineTo(x + w, y)
              layerCtx.lineTo(x + w, _cY - _offset)
              layerCtx.closePath()
            } else {
              _offset = (h * Math.tan(leftPart)) / 2

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX - _offset, y)
              layerCtx.lineTo(_cX + _offset, y)
              layerCtx.closePath()
            }

            break
          }
          case 1: {
            if (leftPart > angle2) {
              _offset = (h * Math.tan(Math.PI / 2 - leftPart)) / 2

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(_cX - _offset, y + h)
              layerCtx.lineTo(x, y + h)
              layerCtx.lineTo(x, y)
              layerCtx.lineTo(x + w, y)
              layerCtx.lineTo(x + w, y + h)
              layerCtx.lineTo(_cX + _offset, y + h)
              layerCtx.closePath()
            } else {
              _offset = (w * Math.tan(leftPart)) / 2

              layerCtx.moveTo(_cX, _cY)
              layerCtx.lineTo(x, _cY + _offset)
              layerCtx.lineTo(x, y)
              layerCtx.lineTo(x + w, y)
              layerCtx.lineTo(x + w, _cY + _offset)
            }

            break
          }
        }
        break
      }
      default:
        break
    }

    layerCtx.clip()

    if (w > 0 && h > 0) {
      if (null != this.nextCacheImg.slideImage) {
        layerCtx.drawImage(this.nextCacheImg.slideImage, x, y, w, h)
      } else {
        fillStyledRect(layerCtx, this.nextCacheImg.color, x, y, w, h)
      }
    }

    layerCtx.restore()
  }
  animZoom = () => {
    if (this.checkTimeEnd()) {
      return
    }
    this.resetTransform()

    const { x, y, w, h } = this.rect

    let pct = this.calcPercent()

    const { mainCtx, layerCtx } = this.getViewAndContext()

    switch (this.option) {
      case TransitionOptions.ZoomIn: {
        this.clearMainViewBackground()

        const _w = (0.5 * w * (1 + pct)) >> 0
        const _h = (0.5 * h * (1 + pct)) >> 0
        const _x = (w - _w) >> 1
        const _y = (h - _h) >> 1

        const _x1 = (0.25 * w - _x) >> 0
        const _y1 = (0.25 * h - _y) >> 0
        const _w1 = w - 2 * _x1
        const _h1 = h - 2 * _y1

        if (_w > 0 && _h > 0) {
          if (null != this.nextCacheImg.slideImage) {
            mainCtx.drawImage(
              this.nextCacheImg.slideImage,
              x + _x,
              y + _y,
              _w,
              _h
            )
          } else {
            fillStyledRect(
              mainCtx,
              this.nextCacheImg.color,
              x + _x,
              y + _y,
              _w,
              _h
            )
          }
        }

        mainCtx.globalAlpha = 1 - pct
        if (null != this.curCacheImg.slideImage) {
          mainCtx.drawImage(
            this.curCacheImg.slideImage,
            _x1,
            _y1,
            _w1,
            _h1,
            x,
            y,
            w,
            h
          )
        } else {
          fillStyledRect(mainCtx, this.curCacheImg.color, x, y, w, h)
        }
        mainCtx.globalAlpha = 1

        break
      }
      case TransitionOptions.ZoomOut: {
        this.clearMainViewBackground()

        pct = 1 - pct
        const _w = (0.5 * w * (1 + pct)) >> 0
        const _h = (0.5 * h * (1 + pct)) >> 0
        const _x = (w - _w) >> 1
        const _y = (h - _h) >> 1

        const _x1 = (0.25 * w - _x) >> 0
        const _y1 = (0.25 * h - _y) >> 0
        const _w1 = w - 2 * _x1
        const _h1 = h - 2 * _y1

        if (_w > 0 && _h > 0) {
          if (null != this.curCacheImg.slideImage) {
            mainCtx.drawImage(
              this.curCacheImg.slideImage,
              x + _x,
              y + _y,
              _w,
              _h
            )
          } else {
            fillStyledRect(
              mainCtx,
              this.curCacheImg.color,
              x + _x,
              y + _y,
              _w,
              _h
            )
          }
        }

        mainCtx.globalAlpha = 1 - pct
        if (null != this.nextCacheImg.slideImage) {
          mainCtx.drawImage(
            this.nextCacheImg.slideImage,
            _x1,
            _y1,
            _w1,
            _h1,
            x,
            y,
            w,
            h
          )
        } else {
          fillStyledRect(mainCtx, this.nextCacheImg.color, x, y, w, h)
        }
        mainCtx.globalAlpha = 1

        break
      }
      case TransitionOptions.ZoomAndRotate: {
        if (!this.isPlaying()) {
          this.clearMainViewBackground()
          if (null != this.curCacheImg.slideImage) {
            mainCtx.drawImage(
              this.curCacheImg.slideImage,
              this.rect.x,
              this.rect.y,
              this.rect.w,
              this.rect.h
            )
          } else {
            fillStyledRect(mainCtx, this.curCacheImg.color, x, y, w, h)
          }
        }

        this.clearLayerViewBackground()

        let _angle = -45 + 405 * pct
        const _scale = 0.05 + 0.95 * pct
        _angle *= Math.PI / 180

        layerCtx.save()
        layerCtx.beginPath()
        layerCtx.rect(x, y, w, h)
        layerCtx.clip()
        layerCtx.beginPath()

        const _xC = x + w / 2
        const _yC = y + h / 2

        const localMatrix = new Matrix2D()
        MatrixUtils.translateMatrix(localMatrix, -_xC, -_yC)
        MatrixUtils.rotateMatrix(localMatrix, _angle)
        MatrixUtils.scaleMatrix(localMatrix, _scale, _scale)
        MatrixUtils.translateMatrix(localMatrix, _xC, _yC)

        layerCtx.transform(
          localMatrix.sx,
          localMatrix.shy,
          localMatrix.shx,
          localMatrix.sy,
          localMatrix.tx,
          localMatrix.ty
        )

        if (null != this.nextCacheImg.slideImage) {
          layerCtx.drawImage(this.nextCacheImg.slideImage, x, y, w, h)
        } else {
          fillStyledRect(layerCtx, this.nextCacheImg.color, x, y, w, h)
        }

        layerCtx.restore()

        break
      }
      default:
        break
    }
  }
}

function getCurrentSlideIndex(editorUI: EditorUI) {
  const { curSlideIndex: curSlideIndex, countOfSlides: countOfSlides } =
    editorUI.renderingController
  let slideIndex = editorUI.showManager.isInShowMode
    ? editorUI.showManager.currentSlideIndex
    : curSlideIndex
  if (slideIndex >= countOfSlides) {
    slideIndex = countOfSlides - 1
  }
  return slideIndex
}
