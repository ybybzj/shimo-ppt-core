import { Nullable } from '../../../liber/pervasive'
import { BrowserInfo } from '../../common/browserInfo'
import { EditorSkin } from '../../core/common/const/drawing'
import {
  getCanvasDrawer,
  getCanvasDrawerWithScale,
  CanvasDrawer,
} from '../../core/graphic/CanvasDrawer'
import { createMatrix, Matrix2D, MatrixUtils } from '../../core/graphic/Matrix'
import { ClipRect } from '../../core/graphic/type'
import {
  restoreCxtTransformReset,
  setCxtTransformReset,
  toggleCxtTransformReset,
} from '../../core/graphic/utils'
import {
  renderPresentationSlide,
  renderSlide,
  renderSlideStaticContent,
} from '../../core/render/slide'
import { makeAnimQueue, TimeNode } from '../../core/Slide/Animation'
import { Slide } from '../../core/Slide/Slide'
import { getFlipTransformForSp } from '../../core/utilities/shape/draw'
import {
  AnimationUpdateResult_toUpdateInfo,
  drawEndFrame,
  drawStartFrame,
  makeShapeAnimationPlayer,
  ShapeAnimationDrawer,
  ShapeAnimationUpdater,
  StopPlayStatus,
} from '../../re/export/ShapeAnimation'
import { SlideElement } from '../../core/SlideElement/type'
import { EditorUI } from '../editorUI/EditorUI'
import {
  getSlideElementPreview,
  ShapeImageData,
} from '../../core/render/toImage'
import { calculateRenderingStateForSlides } from '../../core/calculation/calculate/presentation'

export function genAnimPlayerForSlide(
  slide: Slide,
  timeNodes: TimeNode[],
  {
    onEnd,
    autoPlay = () => false,
  }: {
    onEnd?: (stopStatus: StopPlayStatus) => void
    autoPlay?: () => boolean
  }
) {
  const animQueue = makeAnimQueue(slide, timeNodes)
  if (!animQueue) return
  return makeShapeAnimationPlayer(animQueue, onEnd, autoPlay)
}

export interface DrawInfo {
  rect: ClipRect
  clearRect: ClipRect
  scale: number
  slide?: Slide
  ctx?: CanvasRenderingContext2D
}

export function makeDrawer(
  editorUI: EditorUI,
  {
    drawInfo,
    drawStatic,
    drawShape,
  }: {
    drawInfo: DrawInfo
    drawStatic: (slide: Slide, scale: number, g: CanvasDrawer) => void
    drawShape: (
      sp: SlideElement,
      scale: number,
      g: CanvasDrawer,
      percentage: number,
      transformUpdater: ShapeAnimationUpdater
    ) => void
  }
): ShapeAnimationDrawer {
  return (
    transformUpdater: ShapeAnimationUpdater,
    percentage: number = 1.0
  ) => {
    const { slide, ctx } = drawInfo
    if (slide == null || ctx == null) {
      return
    }
    const { rect, scale: scaleFactor, clearRect } = drawInfo
    if (editorUI.showManager.isInShowMode) {
      const editLayer = editorUI.showManager.layer
      const layerCtx = editLayer && editLayer.getContext('2d')
      if (layerCtx) {
        layerCtx.clearRect(0, 0, editLayer.width, editLayer.height)
      }
    } else {
      const editLayer = editorUI.renderingController.editLayer
      editLayer.clear()
    }

    const canvasDrawer = getCanvasDrawerWithScale(ctx, scaleFactor, scaleFactor)

    const resetState = setCxtTransformReset(canvasDrawer, false)

    const isInShowMode = editorUI.showManager.isInShowMode
    ctx.fillStyle = isInShowMode
      ? EditorSkin.Background_Dark
      : EditorSkin.Background_AnimationPreview
    ctx.fillRect(clearRect.x, clearRect.y, clearRect.w, clearRect.h)
    ctx.save()
    const clipRect = new Path2D()
    clipRect.rect(
      rect.x * BrowserInfo.PixelRatio,
      rect.y * BrowserInfo.PixelRatio,
      rect.w * BrowserInfo.PixelRatio,
      rect.h * BrowserInfo.PixelRatio
    )
    ctx.clip(clipRect)

    restoreCxtTransformReset(canvasDrawer, resetState)
    canvasDrawer.setCoordTransform(
      rect.x * BrowserInfo.PixelRatio,
      rect.y * BrowserInfo.PixelRatio
    )
    canvasDrawer.updateTransform(1, 0, 0, 1, 0, 0)
    canvasDrawer.isNoRenderEmptyPlaceholder = true

    canvasDrawer.isInShowMode = true

    drawStatic(slide, scaleFactor, canvasDrawer)

    for (let i = 0; i < slide.cSld.spTree.length; ++i) {
      drawShape(
        slide.cSld.spTree[i],
        scaleFactor,
        canvasDrawer,
        percentage ?? 1.0,
        transformUpdater
      )
    }
    ctx.restore()
    canvasDrawer.resetState()
  }
}

type createCanvasImage = (w: number, h: number) => HTMLCanvasElement

export function drawCacheImage(
  editorUI: EditorUI,
  slideIndex: number,
  rect: ClipRect,
  createCanvasImage: createCanvasImage,
  whichFrame?: 'first' | 'last'
) {
  const slide = editorUI.presentation.slides[slideIndex]

  if (slide) {
    const { w, h } = rect
    const { width: wMM, height: hMM } = editorUI.presentation

    const slideImage = createCanvasImage(w, h)

    const drawCxt = getCanvasDrawer(
      slideImage.getContext('2d')!,
      w,
      h,
      wMM,
      hMM
    )
    drawCxt.updateTransform(1, 0, 0, 1, 0, 0)
    drawCxt.isNoRenderEmptyPlaceholder = true

    // 确保演示中每一张均 calc 过(此处默认切页在动画之前)
    calculateRenderingStateForSlides([slide])

    drawCxt.isInShowMode = true
    if (whichFrame == null) {
      renderPresentationSlide(editorUI.presentation, slideIndex, drawCxt)
    } else {
      const drawer = (transformUpdater, percentage = 1.0) => {
        const scale = w / wMM
        renderSlideStaticContent(slide, drawCxt)

        for (let i = 0; i < slide.cSld.spTree.length; ++i) {
          drawAnimShape(
            slide.cSld.spTree[i],
            scale,
            drawCxt,
            percentage,
            transformUpdater
          )
        }
      }

      drawFrame(slide, drawCxt, drawer, whichFrame)
    }
    drawCxt.resetState()
    return slideImage
  }
}
function drawFrame(
  slide: Slide,
  g: CanvasDrawer,
  drawer: ShapeAnimationDrawer,
  which: 'first' | 'last'
) {
  const queue = slide.animation.getAnimQueue()
  if (!queue) {
    renderSlide(slide, g)
  } else {
    if (which === 'first') {
      drawStartFrame(queue, drawer)
    } else {
      drawEndFrame(queue, drawer)
    }
  }
}

export function drawAnimShape(
  sp: SlideElement,
  scale: number,
  g: CanvasDrawer,
  percentage: number,
  transformUpdater: ShapeAnimationUpdater,
  createCacheImgData?: (creater: () => ShapeImageData) => ShapeImageData
) {
  const cacheImgData = createCacheImgData
    ? createCacheImgData(() => getSlideElementPreview(sp, scale))
    : getSlideElementPreview(sp, scale)

  const bounds = cacheImgData.bounds

  toggleCxtTransformReset(g, false)
  const initAng = (sp.rot * 360) / (2 * Math.PI)
  const updates = AnimationUpdateResult_toUpdateInfo(
    transformUpdater(sp, percentage, {
      x: 0,
      y: 0,
      rot: initAng,
      w: 1,
      h: 1,
      alpha: 1,
      visibility: true,
    }),
    sp
  )

  if (
    sp.hideWhenShow ||
    updates === 'NoDraw' ||
    (updates !== 'NoUpdate' && !updates.visibility)
  ) {
    sp.updateAnimationDrawInfo({ action: 'NoDraw' })
    return
  }

  let transform: Matrix2D, alpha: Nullable<number>
  if (updates === 'NoUpdate') {
    // invert the flip transform that is already applied when drawing the cache image
    transform = MatrixUtils.invertMatrix(getFlipTransformForSp(sp))
    MatrixUtils.multiplyMatrixes(transform, createMatrix(sp.transform))
  } else {
    transform = updates.transform
    alpha = updates.alpha
  }
  g.applyTransformMatrix(transform, false)

  const oldAplha = g.context.globalAlpha
  if (alpha != null && alpha !== oldAplha) g.context.globalAlpha = alpha

  g.context.drawImage(
    cacheImgData.image,
    -bounds.offsetX,
    -bounds.offsetY,
    bounds.width,
    bounds.height
  )

  g.context.globalAlpha = oldAplha
  return
}

export function drawStaticOfSlide(
  slide: Slide,
  scale: number,
  g: CanvasDrawer,
  rect: ClipRect,
  createCacheImg?: (creater: () => HTMLCanvasElement) => HTMLCanvasElement
) {
  const cacheImg = createCacheImg
    ? createCacheImg(() => drawStaticContentImageForSlide(slide, scale))
    : drawStaticContentImageForSlide(slide, scale)

  g.context.drawImage(cacheImg, rect.x, rect.y, rect.w, rect.h)
}

function drawStaticContentImageForSlide(slide: Slide, scaleFactor: number) {
  const im = document.createElement('canvas')
  im.width = slide.width * scaleFactor
  im.height = slide.height * scaleFactor

  const g = getCanvasDrawerWithScale(
    im.getContext('2d')!,
    scaleFactor,
    scaleFactor
  )
  g.updateTransform(1, 0, 0, 1, 0, 0)
  g.isNoRenderEmptyPlaceholder = true

  g.isInShowMode = true

  renderSlideStaticContent(slide, g)
  g.resetState()
  return im
}
