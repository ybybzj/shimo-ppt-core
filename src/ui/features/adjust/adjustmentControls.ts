import {
  Slide_Default_Width,
  Slide_Default_Height,
} from '../../../core/common/const/drawing'
import { degFactor } from '../../../core/SlideElement/const'
import { GraphicsBoundsChecker } from '../../../core/graphic/Bounds'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { EditLayerRenderElement } from '../common/EditLayerRenderElement'
import { InstanceType } from '../../../core/instanceTypes'
import {
  Geometry,
  PolarAdjustHandleProps,
  XYAdjustHandleProps,
} from '../../../core/SlideElement/geometry/Geometry'
import { Matrix2D } from '../../../core/graphic/Matrix'
import { Shape } from '../../../core/SlideElement/Shape'
import { Nullable } from '../../../../liber/pervasive'
import { isNumber } from '../../../common/utils'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../../core/common/utils'
import { calculateGeometry } from '../../../core/calculation/calculate/geometry'
import { calcXYPointsByTransform } from '../../../core/utilities/shape/calcs'
import { checkBoundsByXyPoints } from '../common/utils'
import { updateCalcStatusForGeometry } from '../../../core/calculation/updateCalcStatus/spProperties'
import { informGeometryEditChange } from '../../../core/calculation/informChanges/geometryChange'
import { EditActionFlag } from '../../../core/EditActionFlag'

export class XYAdjustmentControl {
  readonly instanceType = InstanceType.XYAdjustmentControl
  target: Shape
  isTracked = false
  geometry!: Geometry
  extX: number
  extY: number
  transform!: Matrix2D
  invertTransform: Matrix2D
  adjustment: Nullable<XYAdjustHandleProps>
  isX = false
  isY = false
  refX: Nullable<string> = null
  refY: Nullable<string> = null
  minX!: number
  maxX!: number
  minY!: number
  maxY!: number
  minRealativeX!: number
  maxRealativeX!: number
  minRealativeY!: number
  maxRealativeY!: number
  factorX?: number
  factorY?: number
  layerRenderElement!: EditLayerRenderElement
  constructor(targetSp: Shape, adjIndex: number) {
    const s = turnOffRecordChanges()
    this.target = targetSp
    this.isTracked = false

    if (targetSp.spPr?.geometry) {
      this.geometry = targetSp.spPr.geometry.clone()
    } else if (targetSp.calcedGeometry) {
      this.geometry = targetSp.calcedGeometry.clone()
    }
    this.extX = targetSp.extX ? targetSp.extX : 0.1
    this.extY = targetSp.extY ? targetSp.extY : 0.1
    this.transform = targetSp.transform.clone()
    this.invertTransform = targetSp.invertTransform
    const pen = targetSp.pen
    const brush = targetSp.brush

    calculateGeometry(this.geometry, this.extX, this.extY, true)
    this.adjustment = this.geometry.ahXYLst[adjIndex]
    const adjustment = this.adjustment

    if (adjustment !== null && typeof adjustment === 'object') {
      const { gdRefX: refX, minX, maxX } = adjustment
      const gdLst = this.geometry.gdLst
      const isValidGuideX = refX && isNumber(gdLst[refX!])
      const isValidMinMaxX = isNumber(minX) && isNumber(maxX)
      if (isValidGuideX && isValidMinMaxX) {
        gdLst[refX] = minX
        calculateGeometry(this.geometry, this.extX, this.extY, true)
        this.minX = adjustment.posX

        gdLst[refX] = maxX
        calculateGeometry(this.geometry, this.extX, this.extY, true)
        this.maxX = adjustment.posX

        const realMaxX = Math.max(this.maxX, this.minX)
        const realMinX = Math.min(this.maxX, this.minX)

        this.minRealativeX = Math.min(minX, maxX)
        this.maxRealativeX = Math.max(minX, maxX)

        if (realMaxX - realMinX > 0) {
          this.factorX = (maxX - minX) / (this.maxX - this.minX)
          this.isX = true
        }
      }

      const { gdRefY: refY, minY, maxY } = adjustment
      const isValidGuideY = refY && isNumber(gdLst[refY!])
      const isValidMinMaxY = isNumber(minY) && isNumber(maxY)
      if (isValidGuideY && isValidMinMaxY) {
        gdLst[refY] = minY
        calculateGeometry(this.geometry, this.extX, this.extY, true)
        this.minY = adjustment.posY

        gdLst[refY] = maxY
        calculateGeometry(this.geometry, this.extX, this.extY, true)
        this.maxY = adjustment.posY

        const realMaxY = Math.max(this.maxY, this.minY)
        const realMinY = Math.min(this.maxY, this.minY)

        this.minRealativeY = Math.min(minY, maxY)
        this.maxRealativeY = Math.max(minY, maxY)

        if (realMaxY - realMinY > 0) {
          this.factorY = (maxY - minY) / (this.maxY - this.minY)
          this.isY = true
        }
      }
      if (this.isX) {
        this.refX = refX
      }
      if (this.isY) {
        this.refY = refY
      }
    }
    this.layerRenderElement = new EditLayerRenderElement(
      this.geometry,
      this.extX,
      this.extY,
      brush,
      pen,
      this.transform
    )
    restoreRecordChangesState(s)
  }

  getBounds() {
    const checker = new GraphicsBoundsChecker()
    checker.init(
      Slide_Default_Width,
      Slide_Default_Height,
      Slide_Default_Width,
      Slide_Default_Height
    )
    this.layerRenderElement.checkBounds(checker)
    const tr = this.target.transform as Matrix2D
    const { extX, extY } = this.target
    const { xArr, yArr } = calcXYPointsByTransform(tr, extX, extY)
    checkBoundsByXyPoints(checker, xArr, yArr)

    return checker.bounds
  }

  draw(editLayerDrawer: EditLayerDrawer) {
    this.layerRenderElement.draw(editLayerDrawer)
  }

  track(x, y) {
    this.isTracked = true
    const invertTransform = this.invertTransform
    const tx = invertTransform.XFromPoint(x, y)
    const ty = invertTransform.YFromPoint(x, y)
    const adjustment = this.adjustment!
    let isDirty = false

    if (this.isX) {
      const gdRefX = adjustment.gdRefX!
      const newX = adjustment.minX! + this.factorX! * (tx - this.minX)

      if (newX <= this.maxRealativeX && newX >= this.minRealativeX) {
        if (this.geometry.gdLst[gdRefX] !== newX) {
          isDirty = true
        }
        this.geometry.gdLst[gdRefX] = newX
      } else if (newX > this.maxRealativeX) {
        if (this.geometry.gdLst[gdRefX] !== this.maxRealativeX) {
          isDirty = true
        }
        this.geometry.gdLst[gdRefX] = this.maxRealativeX
      } else {
        if (this.geometry.gdLst[gdRefX] !== this.minRealativeX) {
          isDirty = true
        }
        this.geometry.gdLst[gdRefX] = this.minRealativeX
      }
    }

    if (this.isY) {
      const gdRefY = adjustment.gdRefY!
      const newY = adjustment.minY! + this.factorY! * (ty - this.minY)

      if (newY <= this.maxRealativeY && newY >= this.minRealativeY) {
        if (this.geometry.gdLst[gdRefY] !== newY) {
          isDirty = true
        }
        this.geometry.gdLst[gdRefY] = newY
      } else if (newY > this.maxRealativeY) {
        if (this.geometry.gdLst[gdRefY] !== this.maxRealativeY) {
          isDirty = true
        }
        this.geometry.gdLst[gdRefY] = this.maxRealativeY
      } else {
        if (this.geometry.gdLst[gdRefY] !== this.minRealativeY) {
          isDirty = true
        }
        this.geometry.gdLst[gdRefY] = this.minRealativeY
      }
    }
    calculateGeometry(this.geometry, this.extX, this.extY, isDirty)
  }

  trackEnd() {
    if (!this.isTracked) {
      return
    }
    if (!this.target.spPr!.geometry) {
      this.target.spPr!.setGeometry(this.geometry.clone())
    }
    // 实际上 Connector 的 extX/Y 应根据起点终点去计算得出(最后影响gdLst[adj1/w/h])
    // 因此起终点水平时, 若想对连接线进行操作, 则手动对其赋值
    if (!this.target.extX && this.isX && this.target.spPr) {
      this.target.spPr.xfrm!.setExtX(this.extX)
    }
    if (!this.target.extY && this.isY && this.target.spPr) {
      this.target.spPr.xfrm!.setExtY(this.extY)
    }
    const targetGeometry = this.target.spPr!.geometry!
    if (this.isX) {
      targetGeometry.addAdjValue(
        this.refX!,
        this.geometry.gdLst[this.adjustment?.gdRefX!] + ''
      )
    }
    if (this.isY) {
      targetGeometry.addAdjValue(
        this.refY!,
        this.geometry.gdLst[this.adjustment?.gdRefY!] + ''
      )
    }
    if (this.isX || this.isY) {
      calculateGeometry(targetGeometry, this.extX, this.extY, true)
      updateCalcStatusForGeometry(targetGeometry) // 触发 render
      informGeometryEditChange(
        targetGeometry,
        EditActionFlag.action_SlideElements_EndControl,
        []
      )
    }
  }
}

export class PolarAdjustmentControl {
  readonly instanceType = InstanceType.PolarAdjustmentControl
  isTracked = false
  target: Shape
  geometry!: Geometry
  extX: number
  extY: number
  transform: Matrix2D
  invertTransform: Matrix2D
  adjustment: Nullable<PolarAdjustHandleProps>
  handleRadius = false
  handleAngle = false
  minRadius!: number
  maxRadius!: number
  minRealativeRadius!: number
  maxRealativeRadius!: number
  factorR!: number
  minAng!: number
  maxAng!: number
  layerRenderElement!: EditLayerRenderElement
  constructor(targetSp: Shape, adjIndex: number) {
    const s = turnOffRecordChanges()
    this.target = targetSp

    if (targetSp.spPr?.geometry) {
      this.geometry = targetSp.spPr.geometry.clone()
    } else if (targetSp.calcedGeometry) {
      this.geometry = targetSp.calcedGeometry.clone()
    }
    this.extX = targetSp.extX
    this.extY = targetSp.extY
    this.transform = targetSp.transform
    this.invertTransform = targetSp.invertTransform
    const pen = targetSp.pen
    const brush = targetSp.brush

    calculateGeometry(this.geometry, this.extX, this.extY)
    this.adjustment = this.geometry.ahPolarLst[adjIndex]
    const adjustment = this.adjustment

    if (adjustment !== null && typeof adjustment === 'object') {
      const gdLst = this.geometry.gdLst
      const { gdRefR: refR, minR, maxR } = adjustment
      const isValidGuideR = refR && isNumber(gdLst[refR!])
      const isValidMinMaxR = isNumber(minR) && isNumber(maxR)
      if (isValidGuideR && isValidMinMaxR) {
        if (gdLst[refR] !== minR) {
          gdLst[refR] = minR
          calculateGeometry(this.geometry, this.extX, this.extY, true)
        }
        let dx = adjustment.posX - this.extX * 0.5
        let dy = adjustment.posY - this.extX * 0.5
        this.minRadius = Math.sqrt(dx * dx + dy * dy)

        if (gdLst[refR] !== maxR) {
          gdLst[refR] = maxR
          calculateGeometry(this.geometry, this.extX, this.extY, true)
        }
        dx = adjustment.posX - this.extX * 0.5
        dy = adjustment.posY - this.extY * 0.5
        this.maxRadius = Math.sqrt(dx * dx + dy * dy)

        const maxRadius = Math.max(this.maxRadius, this.minRadius)
        const minRadius = Math.min(this.maxRadius, this.minRadius)

        this.minRealativeRadius = Math.min(minR, maxR)
        this.maxRealativeRadius = Math.max(minR, maxR)

        if (maxRadius - minRadius > 0) {
          this.factorR = (maxR - minR) / (this.maxRadius - this.minRadius)
          this.handleRadius = true
        }
      }

      const { gdRefAng, minAng, maxAng } = adjustment
      const isValidGuideAng = gdRefAng && isNumber(gdLst[gdRefAng!])
      const isValidMinMaxAng = isNumber(minAng) && isNumber(maxAng)
      if (isValidGuideAng && isValidMinMaxAng) {
        this.handleAngle = true
        this.minAng = Math.min(minAng, maxAng)
        this.maxAng = Math.max(minAng, maxAng)
      }
    }

    this.layerRenderElement = new EditLayerRenderElement(
      this.geometry,
      this.extX,
      this.extY,
      brush,
      pen,
      this.transform
    )
    restoreRecordChangesState(s)
  }
  draw = (editLayerDrawer: EditLayerDrawer) => {
    this.layerRenderElement.draw(editLayerDrawer)
  }

  track = (x, y) => {
    this.isTracked = true
    const invertTransform = this.invertTransform
    const tx = invertTransform.XFromPoint(x, y)
    const ty = invertTransform.YFromPoint(x, y)
    const adjustment = this.adjustment!

    let posXReltativeCenter = tx - this.extX * 0.5
    let posYReltativeCenter = ty - this.extY * 0.5
    let isDirty = false
    if (this.handleRadius) {
      const gdRefR = adjustment.gdRefR!
      const _radius = Math.sqrt(
        Math.pow(posXReltativeCenter, 2) + Math.pow(posYReltativeCenter, 2)
      )
      const radius =
        adjustment.minR! + this.factorR * (_radius - this.minRadius)

      if (
        radius <= this.maxRealativeRadius &&
        radius >= this.minRealativeRadius
      ) {
        this.geometry.gdLst[gdRefR] = radius
        isDirty = true
      } else if (radius > this.maxRealativeRadius) {
        this.geometry.gdLst[gdRefR] = this.maxRealativeRadius
        isDirty = true
      } else {
        this.geometry.gdLst[gdRefR] = this.minRealativeRadius
        isDirty = true
      }
    }

    if (this.handleAngle) {
      const gdRefAng = adjustment.gdRefAng!
      if (this.geometry.preset === 'mathNotEqual') {
        posYReltativeCenter = -posYReltativeCenter
        posXReltativeCenter = -posXReltativeCenter
      }
      let angle = Math.atan2(posYReltativeCenter, posXReltativeCenter)
      while (angle < 0) angle += 2 * Math.PI
      while (angle >= 2 * Math.PI) angle -= 2 * Math.PI

      angle *= degFactor
      if (angle >= this.minAng && angle <= this.maxAng) {
        this.geometry.gdLst[gdRefAng] = angle
        isDirty = true
      } else if (angle >= this.maxAng) {
        this.geometry.gdLst[gdRefAng] = this.maxAng
        isDirty = true
      } else if (angle <= this.minAng) {
        this.geometry.gdLst[gdRefAng] = this.minAng
        isDirty = true
      }
    }
    calculateGeometry(this.geometry, this.extX, this.extY, isDirty)
  }

  trackEnd = () => {
    if (!this.isTracked) {
      return
    }

    if (!this.target.spPr!.geometry) {
      this.target.spPr!.setGeometry(this.geometry.clone())
    }
    const targetGeometry = this.target.spPr!.geometry!
    if (this.handleRadius) {
      targetGeometry.addAdjValue(
        this.adjustment?.gdRefR!,
        this.geometry.gdLst[this.adjustment?.gdRefR!] + ''
      )
    }
    if (this.handleAngle) {
      targetGeometry.addAdjValue(
        this.adjustment?.gdRefAng!,
        this.geometry.gdLst[this.adjustment?.gdRefAng!] + ''
      )
    }
    if (this.handleRadius || this.handleAngle) {
      calculateGeometry(targetGeometry, this.extX, this.extY, true)
      updateCalcStatusForGeometry(targetGeometry) // 触发 render
      informGeometryEditChange(
        targetGeometry,
        EditActionFlag.action_SlideElements_EndControl,
        []
      )
    }
  }
  getBounds(...args: any[]) {
    return XYAdjustmentControl.prototype.getBounds.apply(this, args)
  }
}

export type AdjustControls = XYAdjustmentControl | PolarAdjustmentControl
