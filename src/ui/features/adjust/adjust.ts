import { Dict, Nullable } from '../../../../liber/pervasive'
import { PointerEventInfo } from '../../../common/dom/events'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { collectCxnsFromSlideElements } from '../../../core/utilities/presentation/getters'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { InstanceType } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import {
  AdjustControls,
  PolarAdjustmentControl,
  XYAdjustmentControl,
} from './adjustmentControls'
import { HoverInfo } from '../../states/type'
import { handleUIStateOnMouseMove } from '../../editorUI/utils/pointers'
import { ConnectionShape } from '../../../core/SlideElement/ConnectionShape'
import { Shape } from '../../../core/SlideElement/Shape'
import {
  calculateConnectorsAfterControl,
  checkConnectorControl,
  findConnectorTargets,
} from '../connector/utils'
import { ConnectorControl } from '../connector/connectorControl'
import { startEditAction } from '../../../core/calculation/informChanges/startAction'
import { calculateForRendering } from '../../../core/calculation/calculate/calculateForRendering'
import { adjustXfrmOfGroup } from '../../../core/utilities/shape/group'

export class AdjCheckState {
  readonly instanceType = InstanceType.AdjCheckState
  handler: IEditorHandler
  majorTarget: Shape
  preControls: Array<AdjustControls | ConnectorControl>
  group: Nullable<GroupShape>

  constructor(
    handler: IEditorHandler,
    majorTarget: Shape,
    adjIndex: number,
    isPolar = false
  ) {
    this.handler = handler
    this.majorTarget = majorTarget
    this.group = majorTarget.group
    this.preControls = isPolar
      ? [new PolarAdjustmentControl(majorTarget, adjIndex)]
      : [new XYAdjustmentControl(majorTarget, adjIndex)]
  }

  getHoverInfo(_e: PointerEventInfo, _x: number, _y: number): HoverInfo {
    const sp = this.group ? this.group : this.majorTarget
    return { objectId: sp.getId(), cursorType: 'crosshair' }
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(e, x, y) {
    handleAdjustCheckOnMouseMove(this, e, x, y)
  }

  onEnd(_e, _x, _y) {
    this.handler.changeToState(InstanceType.UIInitState, this.handler)
  }
}

export class AdjState {
  readonly instanceType = InstanceType.AdjState
  handler: IEditorHandler
  majorTarget: SlideElement
  controls: Array<AdjustControls | ConnectorControl>
  group: Nullable<GroupShape>

  constructor(
    handler: IEditorHandler,
    majorTarget: SlideElement,
    controls: Array<AdjustControls | ConnectorControl>,
    group?: Nullable<GroupShape>
  ) {
    this.handler = handler
    this.majorTarget = majorTarget
    this.controls = controls
    this.group = group
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(e: PointerEventInfo, x, y) {
    handleAdjustOnMouseMove(this, e, x, y)
  }

  onEnd(_e, _x, _y) {
    handleAdjustOnMouseUp(this)
  }
}

function handleAdjustCheckOnMouseMove(
  state: AdjCheckState,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  const { handler, preControls } = state
  checkConnectorControl(handler, preControls)
  handler.changeToState(
    InstanceType.AdjState,
    handler,
    state.majorTarget,
    preControls,
    state.group
  )
  handleUIStateOnMouseMove(handler, e, x, y)
}

function handleAdjustOnMouseMove(state: AdjState, e: PointerEventInfo, x, y) {
  if (!e.isLocked) {
    state.onEnd(e, x, y)
    return
  }
  const { handler, controls } = state
  controls.forEach((control) => control.track(x, y))
  handler.updateEditLayer()
}

function handleAdjustOnMouseUp(state: AdjState) {
  const { handler, controls } = state
  const targets: SlideElement[] = []
  const sps: Dict<boolean> = {}

  controls.forEach((control) => {
    const sp = control.target as SlideElement | undefined
    if (!sp) return
    targets.push(sp)
    sps[sp.getId()] = true
    if (sp.instanceType === InstanceType.GroupShape) {
      for (let i = 0; i < sp.slideElementsInGroup.length; ++i) {
        sps[sp.slideElementsInGroup[i].getId()] = true
      }
    }
  })

  const allConnectors = collectCxnsFromSlideElements(
    handler.presentation,
    targets,
    []
  )
  let connectors: ConnectionShape[] = []
  if (state.group) {
    allConnectors.forEach((connector) => {
      const { startSp, endSp } = findConnectorTargets(connector)
      if (
        (startSp && !sps[startSp.getId()]) ||
        (endSp && !sps[endSp.getId()]) ||
        !sps[connector.getId()]
      ) {
        connectors.push(connector)
      }
    })
  } else {
    connectors = allConnectors
  }
  startEditAction(EditActionFlag.action_SlideElements_EndAdjust)
  controls.forEach((control) => control.trackEnd())
  calculateConnectorsAfterControl(connectors)

  if (state.group) {
    adjustXfrmOfGroup(state.group)
  }
  calculateForRendering()

  handler.updateEditLayer()
  handler.changeToState(InstanceType.UIInitState, handler)
}

export type ChangeAdjStateTypes = AdjCheckState | AdjState
