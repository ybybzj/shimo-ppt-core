import { CommentStatusType } from '../../../core/common/const/drawing'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { renderCommentsOnEditLayer } from '../../../core/render/comment'
import { InstanceType } from '../../../core/instanceTypes'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { handleUpdateUICursor } from '../../editorUI/utils/updates'
export class StartAddNewCommentState {
  readonly instanceType = InstanceType.StartAddNewCommentState
  handler: IEditorHandler
  constructor(handler: IEditorHandler) {
    this.handler = handler
  }

  onStart(_e, _x, y) {
    const editorUI = this.handler.editorUI
    this.handler.updateEditLayer()
    const afterIndex = editorUI.presentation.getCurrentCommentIndex(y)
    const slideRefId = this.handler.getCurrentSlide().getRefId()
    this.handler.editorUI.editorKit.emitStartAddComment(afterIndex, slideRefId)
    return false
  }

  onMove(_e, _x, _y) {
    this.handler.editorUI.renderingController.onUpdateEditLayer()
  }

  onEnd(_e, x, y) {
    const handler = this.handler
    handler.changeToState(InstanceType.AddNewCommentState, handler, x, y)
    handleUpdateUICursor(handler, x, y)
    this.handler.updateEditLayer()
  }
}

export class AddNewCommentState {
  readonly instanceType = InstanceType.AddNewCommentState
  handler: IEditorHandler
  x: number
  y: number
  constructor(handler: IEditorHandler, x: number, y: number) {
    this.handler = handler
    this.x = x
    this.y = y
  }

  onStart(_e, _x, _y) {
    this.handler.editorUI.editorKit.emitEndAddComment()
    return false
  }

  onMove() {}

  onEnd(_e, x, y) {
    this.handler.changeToState(InstanceType.UIInitState, this.handler)
    handleUpdateUICursor(this.handler, x, y)
    this.handler.updateEditLayer()
  }

  drawControl(editLayerDrawer: EditLayerDrawer) {
    if (!EditorSettings.isShowComment) return
    const status = CommentStatusType.Select
    renderCommentsOnEditLayer(editLayerDrawer, status, this.x, this.y)
  }
}

export type CommentStateTypes = StartAddNewCommentState | AddNewCommentState
