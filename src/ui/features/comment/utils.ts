import { Nullable } from '../../../../liber/pervasive'
import { PointerEventInfo } from '../../../common/dom/events'
import {
  CommentStatusType,
  FocusTarget,
} from '../../../core/common/const/drawing'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { Comment } from '../../../core/Slide/Comments/Comment'
import {
  getHeightOfComment,
  getWidthOfComment,
} from '../../../core/utilities/helpers'
import {
  Evt_onHoveredCommentChange,
  Evt_onSelectedCommentChange,
} from '../../../editor/events/EventNames'
import { syncInterfaceState } from '../../../editor/interfaceState/syncInterfaceState'
import { InstanceType, isInstanceTypeOf } from '../../../core/instanceTypes'
import { IEditorHandler } from '../../rendering/editorHandler/type'
import { handleUpdateUICursor } from '../../editorUI/utils/updates'
import { ZoomValue } from '../../../core/common/zoomValue'
import { InputCommentData } from '../../../core/utilities/comments'

/** 更新 cursorType, 是否重绘由外部判断 */
export function startAddComment(handler: IEditorHandler) {
  const presentation = handler.presentation
  if (presentation.slides[presentation.currentSlideIndex]) {
    presentation.isNotesFocused = false
    presentation.setFocusType(FocusTarget.EditorView)
    handler.changeToState(InstanceType.StartAddNewCommentState, handler)
    handleUpdateUICursor(
      handler,
      handler.presentation.cursorPosition.x,
      handler.presentation.cursorPosition.y,
      new PointerEventInfo()
    )
  }
}

export function endAddComment(handler: IEditorHandler) {
  if (
    isInstanceTypeOf(handler.curState, InstanceType.StartAddNewCommentState) ||
    isInstanceTypeOf(handler.curState, InstanceType.AddNewCommentState)
  ) {
    handler.changeToState(InstanceType.UIInitState, handler)
    handleUpdateUICursor(
      handler,
      handler.presentation.cursorPosition.x,
      handler.presentation.cursorPosition.y,
      new PointerEventInfo()
    )
  }
  handler.editorUI.editorKit.emitEndAddComment()
}
/**
 * @useage (取消)选中评论
 * @desc HoveredObjectChange 应统一采用 gTableId, 但评论与外部的交互只能依赖 uid
 * @TODO 统一为全局 Evt_onHoveredObjectChange 事件
 */
export function hoverCommentById(handler: IEditorHandler, uid?: string) {
  const editorUI = handler.editorUI
  const presentation = editorUI.presentation
  const slide = presentation.slides[presentation.currentSlideIndex]
  if (!slide) return
  const slideComment = slide.slideComments
  if (!slideComment || !slideComment.comments.length) return
  const comments = slideComment.comments
  const hoveredObject = handler.hoveredObject
  if (uid) {
    for (const comment of comments) {
      if (comment.getUid() === uid) {
        if (!hoveredObject || hoveredObject.getId() !== comment.getId()) {
          handler.hoveredObject = comment
          editorUI.triggerRender(true)
          editorUI.editorKit.emitEvent(
            Evt_onHoveredCommentChange,
            comment.toJSON()
          )
        }
      }
    }
  } else if (
    hoveredObject &&
    hoveredObject.instanceType === InstanceType.Comment
  ) {
    handler.hoveredObject = null
    editorUI.triggerRender(true)
    editorUI.editorKit.emitEvent(Evt_onHoveredCommentChange)
  }
}

export function selectCommentById(handler: IEditorHandler, uid?: string) {
  const editorUI = handler.editorUI
  const presentation = editorUI.presentation
  const slide = presentation.slides[presentation.currentSlideIndex]
  if (!slide) return
  const slideComment = slide.slideComments
  if (!slideComment || !slideComment.comments.length) return
  const comments = slideComment.comments
  let selectIndex = -1 // 旧评论选中到新评论仅发送一次事件
  let deSelectIndex = -1
  for (let i = comments.length - 1; i >= 0; i--) {
    if (uid) {
      if (comments[i].uid === uid) {
        if (!comments[i].selected) {
          comments[i].selected = true
          selectIndex = i
        } else {
          // selection hit should trigger event
          editorUI.editorKit.emitEvent(
            Evt_onSelectedCommentChange,
            comments[i].toJSON()
          )
          return
        }
      } else {
        if (comments[i].selected) {
          comments[i].selected = false
          deSelectIndex = i
        }
      }
    } else {
      if (comments[i].selected) {
        comments[i].selected = false
        deSelectIndex = i
      }
    }
  }
  if (selectIndex > -1) {
    editorUI.triggerRender(true)
    editorUI.editorKit.emitEvent(
      Evt_onSelectedCommentChange,
      comments[selectIndex].toJSON()
    )
    const comment = comments[selectIndex]
    const commentW = getWidthOfComment(CommentStatusType.Select)
    const commentH = getWidthOfComment(CommentStatusType.Hover)
    editorUI.scrollToPosition(comment.x, comment.y, commentW, commentH)
  }
  if (deSelectIndex > -1 && selectIndex < 0) {
    editorUI.triggerRender(true)
    editorUI.editorKit.emitEvent(Evt_onSelectedCommentChange)
  }
}

export function showComments(handler: IEditorHandler) {
  if (!EditorSettings.isShowComment) {
    EditorSettings.isShowComment = true
    handler.editorUI.triggerRender(true)
    handler.editorUI.scrolls.onGoToSlide(handler.presentation.currentSlideIndex)
  }
}

export function onAddComment(
  handler: IEditorHandler,
  cmContentData: InputCommentData
) {
  const editorUI = handler.editorUI
  const presentation = editorUI.presentation
  let comment: Nullable<Comment>
  if (handler.curState.instanceType === InstanceType.AddNewCommentState) {
    const cmtXY = {
      x: handler.curState.x,
      y: handler.curState.y,
    }
    comment = presentation.addComment(cmContentData, cmtXY)
    endAddComment(handler)
  } else {
    comment = presentation.addComment(cmContentData)
  }

  if (comment == null) {
    return
  }

  editorUI.editorKit.emitAddComment()
  editorUI.triggerRender(true)

  syncInterfaceState(editorUI.editorKit)
  return comment
}

export function onHideComments(handler: IEditorHandler) {
  const editorUI = handler.editorUI
  const presentation = editorUI.presentation
  if (presentation.hideComments()) {
    editorUI.triggerRender(true)
  }
}

export function hitComment(
  handler: IEditorHandler,
  comment: Comment,
  mmX,
  mmY
) {
  const isHovered = handler.hoveredObject?.getId() === comment.id

  const status = comment.selected
    ? CommentStatusType.Select
    : isHovered
    ? CommentStatusType.Hover
    : CommentStatusType.Default
  let commentWidth = getWidthOfComment(status) ?? 0
  let commentHeight = getHeightOfComment(status) ?? 0
  if (!EditorSettings.canZoomComment) {
    commentWidth = (commentWidth * 100) / ZoomValue.value
    commentHeight = (commentHeight * 100) / ZoomValue.value
  }

  return (
    mmX > comment.x &&
    mmX < comment.x + commentWidth &&
    mmY > comment.y &&
    mmY < comment.y + commentHeight
  )
}
