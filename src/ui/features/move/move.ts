import { PointerEventInfo, POINTER_BUTTON } from '../../../common/dom/events'
import { THRESHOLD_DELTA } from '../../../core/common/const/unit'
import { IEditorHandler } from '../../rendering/editorHandler/type'
import { InstanceType } from '../../../core/instanceTypes'
import { getHyperlinkFromSp } from '../../rendering/helpers'
import { handleMediaPlay } from '../../states/hitHandlers'
import { SlideElement } from '../../../core/SlideElement/type'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { MoveControlTypes } from './moveControls'
import { hookManager, Hooks } from '../../../core/hooks'
import { syncInterfaceState } from '../../../editor/interfaceState/syncInterfaceState'
import {
  handleUIStateOnMouseDown,
  handleUIStateOnMouseMove,
  handleUIStateOnMouseUp,
} from '../../editorUI/utils/pointers'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { ConnectorControl } from '../connector/connectorControl'
import {
  handleMoveControlsMove,
  handleMoveControlEnd,
  makeMoveControls,
} from './utils'
import { checkConnectorControl } from '../connector/utils'
import { Nullable } from '../../../../liber/pervasive'

export class MoveCheckState {
  readonly instanceType = InstanceType.MoveCheckState
  handler: IEditorHandler
  majorTarget: SlideElement
  startX: number
  startY: number
  shift: boolean
  ctrl: boolean
  isSelected: boolean
  isInside: boolean
  preControls: MoveControlTypes[]

  constructor(
    handler: IEditorHandler,
    startX: number,
    startY: number,
    shift: boolean,
    ctrl: boolean,
    majorTarget: SlideElement,
    isSelected: boolean,
    isInside: boolean
  ) {
    this.handler = handler
    this.majorTarget = majorTarget
    this.startX = startX
    this.startY = startY
    this.shift = shift
    this.ctrl = ctrl
    this.isSelected = isSelected
    this.isInside = isInside
    this.preControls = makeMoveControls(handler)
  }

  onStart(e: PointerEventInfo, x, y) {
    this.onEnd(e, x, y)
    return true
  }

  onMove(e: PointerEventInfo, x, y) {
    handleMoveCheckOnMouseMove(this, e, x, y)
  }

  onEnd(e: PointerEventInfo, x, y) {
    handleMediaPlay(this.handler, this.majorTarget, e, x, y)

    // ctrl/cmd + click 直接打开链接
    if (this.majorTarget && e.ctrlKey === true) {
      const hyperlink = getHyperlinkFromSp(this.majorTarget, x, y)
      if (hyperlink) {
        this.handler.editorUI.editorKit.emitHyperlinkClick(hyperlink.value)
      }
    }
    if (e.clicks > 1) {
      this.handler.changeToState(InstanceType.UIInitState, this.handler)
      hookManager.invoke(Hooks.Emit.EmitDoubleClick, this.majorTarget)
      return
    }
    return handleMoveCheckOnMouseUp(this, e, x, y)
  }
}
export class MoveInGroupCheckState {
  readonly instanceType = InstanceType.MoveInGroupCheckState
  handler: IEditorHandler
  group: GroupShape
  startX: number
  startY: number
  shift: boolean
  ctrl: boolean
  majorTarget: SlideElement
  isSelected: boolean
  preControls: MoveControlTypes[]

  constructor(
    handler: IEditorHandler,
    group: GroupShape,
    startX: number,
    startY: number,
    shift: boolean,
    ctrl: boolean,
    majorTarget: SlideElement,
    isSelected: boolean
  ) {
    this.handler = handler
    this.group = group
    this.startX = startX
    this.startY = startY
    this.shift = shift
    this.ctrl = ctrl
    this.majorTarget = majorTarget
    this.isSelected = isSelected
    this.preControls = makeMoveControls(handler)
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(e: PointerEventInfo, x, y) {
    handleMoveCheckOnMouseMove(this, e, x, y)
  }

  onEnd(e: PointerEventInfo, x, y) {
    const handler = this.handler
    if (e.ctrlKey && this.isSelected) {
      handler.selectionState.deselect(this.majorTarget)
      handler.editFocusContainer &&
        syncInterfaceState(handler.editorUI.editorKit)
      handler.updateEditLayer()
    }

    handleMediaPlay(handler, this.majorTarget, e, x, y)

    if (e.clicks > 1) {
      handler.changeToState(InstanceType.UIInitState, handler)
      hookManager.invoke(Hooks.Emit.EmitDoubleClick, this.majorTarget)
      return
    }

    handler.changeToState(InstanceType.UIInitState, handler)
  }
}

export class MoveState {
  readonly instanceType = InstanceType.MoveState
  handler: IEditorHandler
  majorTarget: SlideElement
  startX: number
  startY: number
  controls: Array<MoveControlTypes | ConnectorControl>
  group: Nullable<GroupShape>

  constructor(
    handler: IEditorHandler,
    majorTarget: SlideElement,
    startX: number,
    startY: number,
    controls?: Array<MoveControlTypes | ConnectorControl>,
    group?: Nullable<GroupShape>
  ) {
    this.handler = handler
    this.majorTarget = majorTarget
    this.startX = startX
    this.startY = startY
    this.controls = controls ?? makeMoveControls(handler)
    this.group = group
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(e: PointerEventInfo, x: number, y: number) {
    handleMoveOnMouseMove(this, e, x, y)
  }

  onEnd(e: PointerEventInfo, _x, _y) {
    const { handler, controls, group } = this
    handleMoveControlEnd(handler, controls, e, group)
  }

  drawControl(editLayerDrawer: EditLayerDrawer) {
    for (let i = 0; i < this.controls.length; ++i) {
      this.controls[i].draw(editLayerDrawer)
    }
  }
}

// helpers
function handleMoveCheckOnMouseMove(
  state: MoveCheckState | MoveInGroupCheckState,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  const handler = state.handler
  if (handler.isInShowMod()) return
  if (!e.isLocked) {
    state.onEnd(e, x, y)
    return
  }
  const deltaX = Math.abs(state.startX - x)
  const deltaY = Math.abs(state.startY - y)
  if (e.isMove && (deltaX > THRESHOLD_DELTA || deltaY > THRESHOLD_DELTA)) {
    if (!e.ctrlKey) {
      checkConnectorControl(handler, state.preControls)
    }
    const isGroup = state.instanceType === InstanceType.MoveInGroupCheckState
    handler.changeToState(
      InstanceType.MoveState,
      handler,
      state.majorTarget,
      state.startX,
      state.startY,
      state.preControls,
      isGroup ? state.group : undefined
    )
    // 马上触发 move(Ingroup)State 的 onMove
    handleUIStateOnMouseMove(state.handler, e, x, y)
  }
}

function handleMoveCheckOnMouseUp(
  state: MoveCheckState,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  const handler = state.handler
  const editorKit = handler.editorUI.editorKit
  handler.changeToState(InstanceType.UIInitState, handler)
  let isHandled = false
  if (
    !state.shift &&
    !state.ctrl &&
    state.isInside &&
    state.isSelected &&
    e.button !== POINTER_BUTTON.Right
  ) {
    switch (state.majorTarget.instanceType) {
      case InstanceType.GroupShape: {
        handler.selectionState.reset()
        handler.selectionState.selectedGroup = state.majorTarget
        handleUIStateOnMouseDown(handler, e, x, y)
        handleUIStateOnMouseUp(handler, e, x, y)
        handler.editFocusContainer && syncInterfaceState(editorKit)
        isHandled = true
        break
      }
    }
  }
  if (!isHandled) {
    if ((e.ctrlKey || handler.isMultiSelectState) && state.isSelected) {
      handler.selectionState.deselect(state.majorTarget)
      handler.editFocusContainer && syncInterfaceState(editorKit)
      handler.updateEditLayer()
    }
  }
}

function handleMoveOnMouseMove(
  state: MoveState,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  if (!e.isLocked) {
    state.onEnd(e, x, y)
    return
  }
  const { handler, startX, startY, controls } = state
  handleMoveControlsMove(handler, controls, e, { x, y, startX, startY })
}

export type MoveStateTypes = MoveCheckState | MoveInGroupCheckState | MoveState
