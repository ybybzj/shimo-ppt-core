import { ObjectsAlignType } from '../../../core/common/const/attrs'
import { PointerEventInfo } from '../../../common/dom/events'
import { isNumber } from '../../../common/utils'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { InstanceType, isInstanceTypeOf } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import {
  MoveControlTypes,
  MoveGroupControl,
  MoveSingleControl,
} from './moveControls'
import { DrawControlTrackFunc } from '../common/type'
import {
  calculateConnectorsAfterControl,
  checkConnectorControl,
  collectCxnSpsFromControls,
} from '../connector/utils'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { Dict, Nullable } from '../../../../liber/pervasive'
import { startEditAction } from '../../../core/calculation/informChanges/startAction'
import { resetConnectionShapesId } from '../../../core/utilities/helpers'
import { isGroup, isSlideElement } from '../../../core/utilities/shape/asserts'
import { cloneSlideElement } from '../../../core/utilities/shape/clone'
import {
  calcTransform,
  calcXYPointsByTransform,
} from '../../../core/utilities/shape/calcs'
import {
  setParentForSlideElement,
  addToParentSpTree,
  setGroup,
} from '../../../core/utilities/shape/updates'
import { syncInterfaceState } from '../../../editor/interfaceState/syncInterfaceState'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { ConnectorControl } from '../connector/connectorControl'
import { calculateForRendering } from '../../../core/calculation/calculate/calculateForRendering'
import { adjustXfrmOfGroup } from '../../../core/utilities/shape/group'
import { EditorUtil } from '../../../globals/editor'
import { informEntityPropertyChange } from '../../../core/calculation/informChanges/propertyChange'
import { ConnectionShape } from '../../../core/SlideElement/ConnectionShape'

export function createMoveControl(
  sp: SlideElement,
  handler: IEditorHandler,
  drawFunc?: DrawControlTrackFunc
) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      return new MoveSingleControl(sp, handler, drawFunc)
    case InstanceType.ImageShape:
      return new MoveSingleControl(sp, handler, drawFunc)
    case InstanceType.GroupShape:
      return new MoveGroupControl(sp, handler)
    case InstanceType.GraphicFrame:
      return new MoveSingleControl(sp, handler)
  }
}

export function canMove(sp: SlideElement): boolean {
  if (EditorSettings.isViewMode) {
    return false
  }
  switch (sp.instanceType) {
    case InstanceType.Shape:
      return true
    case InstanceType.ImageShape:
      return true
    case InstanceType.GroupShape:
      return true
    case InstanceType.GraphicFrame:
      return true
  }
}

export function makeMoveControls(handler: IEditorHandler) {
  const selectedSps = handler.getTargetSelectedElements()
  const moveControls: MoveControlTypes[] = []
  for (let i = 0; i < selectedSps.length; ++i) {
    if (canMove(selectedSps[i])) {
      moveControls.push(createMoveControl(selectedSps[i], this))
    }
  }
  return moveControls
}

export function moveSelectedElements(handler: IEditorHandler, dx, dy) {
  if (EditorSettings.isViewMode) return
  const selectionState = handler.selectionState

  if (selectionState == null) return
  const controls = makeMoveControls(handler)
  checkConnectorControl(handler, controls)
  const selectedSps = handler.getTargetSelectedElements()
  const firstSp = selectedSps[0]

  for (let i = 0; i < controls.length; ++i) {
    const control = controls[i] as MoveControlTypes
    control.track(dx, dy)
  }

  handleMoveControlEnd(handler, controls, undefined, firstSp.group)
  // Crop Side Effect, need Calc after outside operation
  if (handler.curState.instanceType === InstanceType.CropState) {
    handler.curState.refreshState()
  }
}

export function alignByDirection(
  handler: IEditorHandler,
  alignType: (typeof ObjectsAlignType)[keyof typeof ObjectsAlignType],
  dir: {
    horizental?: 'left' | 'right' | 'center'
    vertical?: 'top' | 'bottom' | 'center'
  }
) {
  const isSelected = alignType === ObjectsAlignType.Selected
  const selectedSps = handler.getTargetSelectedElements()
  if (selectedSps.length < 1 || (!dir.horizental && !dir.vertical)) return
  const bounds = getSlideElementsBounds(selectedSps)
  const allBounds = bounds.allBounds
  const hasSelect = isSelected && selectedSps.length > 1
  const curSlide = handler.getCurrentSlide()
  let xPos: number | undefined
  let offXGetter: (bound: (typeof allBounds)[number]) => number
  let yPos: number | undefined
  let offYGetter: (bound: (typeof allBounds)[number]) => number

  const isReltative = hasSelect && selectedSps.length > 1
  if (dir.horizental) {
    if (dir.horizental === 'left') {
      xPos = isReltative ? bounds.minX : 0
      offXGetter = (bound) => bound.minX
    } else if (dir.horizental === 'center') {
      xPos = isReltative ? (bounds.minX + bounds.maxX) / 2 : curSlide.width / 2
      offXGetter = (bound) => (bound.maxX + bound.minX) / 2
    } else {
      xPos = isReltative ? bounds.maxX : curSlide.width
      offXGetter = (bound) => bound.maxX
    }
  }
  if (dir.vertical) {
    if (dir.vertical === 'top') {
      yPos = isReltative ? bounds.minY : 0
      offYGetter = (bound) => bound.minY
    } else if (dir.vertical === 'center') {
      yPos = isReltative ? (bounds.minY + bounds.maxY) / 2 : curSlide.height / 2
      offYGetter = (bound) => (bound.maxY + bound.minY) / 2
    } else {
      yPos = isReltative ? bounds.maxY : curSlide.height
      offYGetter = (bound) => bound.maxY
    }
  }

  const controls = makeMoveControls(handler)
  const firstSp = selectedSps[0]

  for (let i = 0; i < controls.length; ++i) {
    const moveControl = controls[i]
    const dx = isNumber(xPos) ? xPos - offXGetter!(allBounds[i]) : 0
    const dy = isNumber(yPos) ? yPos - offYGetter!(allBounds[i]) : 0
    moveControl.track(dx, dy)
  }
  handleMoveControlEnd(handler, controls, undefined, firstSp.group)
  handler.syncEditorUIState()
}

export function distributeHorizental(handler: IEditorHandler, alignType) {
  const isSelected = alignType === ObjectsAlignType.Selected

  startEditAction(EditActionFlag.action_Presentation_DisttributeHorizontal)
  const selectedSps = handler.getTargetSelectedElements()

  if (selectedSps.length > 0) {
    const bounds = getSlideElementsBounds(selectedSps)
    const allBounds = bounds.allBounds
    const controls = makeMoveControls(handler)
    const sortControls: Array<{
      control: MoveSingleControl | MoveGroupControl
      bound: (typeof allBounds)[number]
    }> = []
    for (let i = 0; i < selectedSps.length; ++i) {
      const control = controls[i]
      sortControls.push({ control, bound: allBounds[i] })
    }
    // 按 x 轴位置从左到右排序
    sortControls.sort((prev, next) => {
      const [prevSp, nextSp] = [prev.control.target, next.control.target]
      const xCenterPrev = prevSp.x + prevSp.extX / 2
      const xCenterNext = nextSp.x + nextSp.extX / 2
      return xCenterPrev - xCenterNext
    })
    const sumWidth = sortControls.reduce((sum, cur) => {
      return sum + cur.control.target.extX
    }, 0)

    let leftX: number, rightX: number, gapX: number
    if (isSelected && selectedSps.length > 2) {
      leftX = sortControls[0].control.target.x
      const rightSp = sortControls[sortControls.length - 1].control.target
      rightX = rightSp.x + rightSp.extX
      gapX = (rightX - leftX - sumWidth) / (sortControls.length - 1)
    } else {
      const curSlide = handler.getCurrentSlide()
      if (sumWidth < curSlide.width) {
        gapX = (curSlide.width - sumWidth) / (sortControls.length + 1)
        leftX = gapX
        rightX = curSlide.width - gapX
      } else {
        leftX = 0
        rightX = curSlide.width
        gapX = (curSlide.width - sumWidth) / (sortControls.length - 1)
      }
    }
    const firstSp = selectedSps[0]

    let lastX = leftX
    for (let i = 0; i < sortControls.length; ++i) {
      const sp = sortControls[i].control.target
      sortControls[i].control.track(lastX - sp.x, 0)
      lastX += gapX + sp.extX
    }
    handleMoveControlEnd(handler, controls, undefined, firstSp.group)
  }
  calculateForRendering()

  handler.syncEditorUIState()
}

export function handleMoveControlEnd(
  handler: IEditorHandler,
  controls: Array<MoveControlTypes | ConnectorControl>,
  e?: PointerEventInfo,
  group?: Nullable<GroupShape>
) {
  const { selectionState } = handler
  const isMoveInGroup = isGroup(group)
  // copy & move
  if (e?.ctrlKey) {
    if (isMoveInGroup) selectionState.resetGroupSelection()
    const originIdMap: Dict = {}
    const copies: SlideElement[] = []
    startEditAction(EditActionFlag.action_SlideElements_CopyCtrl)
    // 需要保留生成 Point 之前的状态
    selectionState.reset()
    for (let i = 0; i < controls.length; ++i) {
      const targetSp = controls[i].target as SlideElement | undefined
      if (!isSlideElement(targetSp)) continue
      const dup = cloneSlideElement(targetSp, originIdMap)
      originIdMap[targetSp.id] = dup.id

      if (handler.editFocusContainer?.cSld) {
        setParentForSlideElement(dup, handler.editFocusContainer)
        const isGroupShape = isInstanceTypeOf(dup, InstanceType.GroupShape)
        if (
          !dup.spPr?.xfrm ||
          (isGroupShape && !dup.spPr.xfrm.isNotEmptyForGroup()) ||
          (isGroupShape && !dup.spPr.xfrm.isValid())
        ) {
          calcTransform(dup)
        }
      }

      if (isMoveInGroup) {
        setGroup(dup, targetSp.group)
        dup.group!.addToSpTree(dup.group!.spTree.length, dup)
      } else {
        addToParentSpTree(dup)
      }

      copies.push(dup)
      selectionState.select(dup)
      controls[i].target = dup
      controls[i].trackEnd()
      handler.startCalculate()
      if (handler.editFocusContainer?.cSld) {
        syncInterfaceState(handler.editorUI.editorKit)
      }
    }
    resetConnectionShapesId(copies, originIdMap)
    if (isMoveInGroup) {
      adjustXfrmOfGroup(group)
    }
  }
  // move
  else {
    const needToResetCxnSps: [ConnectionShape, boolean, boolean][] = []
    const connectors = collectCxnSpsFromControls(
      handler,
      controls,
      needToResetCxnSps,
      true
    )
    startEditAction(EditActionFlag.action_SlideElements_EndMove)
    for (let i = 0; i < controls.length; ++i) {
      controls[i].trackEnd()
    }
    for (const cxnSpInfo of needToResetCxnSps) {
      // 没有参与 move的相关连接线 则重置连接信息

      const [cxnSp, resetStart, resetEnd] = cxnSpInfo
      const nvUniSpPr = cxnSp.nvSpPr!.nvUniSpPr

      let [isResetBegin, isResetEnd] = [false, false]
      const startSp = EditorUtil.entityRegistry.getEntityById(nvUniSpPr.stCxnId)
      const endSp = EditorUtil.entityRegistry.getEntityById(nvUniSpPr.endCxnId)
      if (resetStart && startSp) isResetBegin = true
      if (resetEnd && endSp) isResetEnd = true

      if (isResetEnd || isResetBegin) {
        const newNvUniSpPr = nvUniSpPr.clone()
        if (isResetBegin) {
          newNvUniSpPr.stCxnId = null
          newNvUniSpPr.stCxnIdx = null
        }
        if (isResetEnd) {
          newNvUniSpPr.endCxnId = null
          newNvUniSpPr.endCxnIdx = null
        }
        cxnSp.nvSpPr!.setUniSpPr(newNvUniSpPr)
        informEntityPropertyChange(
          cxnSp,
          EditActionFlag.edit_CxnSp_SetUniSpPr,
          nvUniSpPr,
          newNvUniSpPr
        )
      }
    }

    calculateConnectorsAfterControl(connectors)

    if (isMoveInGroup) {
      adjustXfrmOfGroup(group)
    }

    calculateForRendering()
  }
  handler.changeToState(InstanceType.UIInitState, handler)
  handler.updateEditLayer()
}

/** 单方向移动, 取位移大者(目前以 Shift 触发) */
function checkStraightMove(e: PointerEventInfo, x, y, startX, startY) {
  let [endX, endY] = [x, y]
  if (e.shiftKey) {
    const absDx = Math.abs(startX - x)
    const absDy = Math.abs(startY - y)
    if (absDx > absDy) {
      ;[endX, endY] = [x, startY]
    } else {
      ;[endX, endY] = [startX, y]
    }
  }
  return [endX, endY]
}

export function handleMoveControlsMove<
  T extends MoveControlTypes | ConnectorControl,
>(
  handler: IEditorHandler,
  controls: Array<T>,
  e: PointerEventInfo,
  posInfo: { x; y; startX; startY },
  moveHandler?: (control: T, tx: number, ty: number) => void
) {
  const { x, y, startX, startY } = posInfo
  const [endX, endY] = checkStraightMove(e, x, y, startX, startY)
  const [dx, dy] = [endX - startX, endY - startY]
  const [checkedDx, checkedDy] =
    handler.editorUI.guidelinesManager.checkGuidelines(
      handler.getCurrentSpTree(),
      controls.map((control) => {
        const shape = control.target
        return {
          snapXs: shape.snapXs.map((x) => x + dx),
          snapYs: shape.snapYs.map((y) => y + dy),
          shape: control.target,
        }
      })
    )
  const [tx, ty] = [dx + checkedDx, dy + checkedDy]
  controls.forEach((control) => {
    if (moveHandler) {
      moveHandler(control, tx, ty)
    } else {
      control.track(tx, ty)
    }
  })
  handler.updateEditLayer()
}

export function distributeVertical(handler: IEditorHandler, alignType) {
  const isSelected = alignType === ObjectsAlignType.Selected
  startEditAction(EditActionFlag.action_Presentation_DisttributeVertical)
  const selectedSps = handler.getTargetSelectedElements()

  if (selectedSps.length > 0) {
    const bounds = getSlideElementsBounds(selectedSps)
    const alBounds = bounds.allBounds
    const controls = makeMoveControls(handler)
    const sortControls: Array<{
      control: MoveSingleControl | MoveGroupControl
      bound: (typeof alBounds)[number]
    }> = []
    for (let i = 0; i < selectedSps.length; ++i) {
      const control = controls[i]
      sortControls.push({ control, bound: alBounds[i] })
    }
    // 按 y 轴位置从上到下排序
    sortControls.sort((prev, next) => {
      const [prevSp, nextSp] = [prev.control.target, next.control.target]
      const yCenterPrev = prevSp.y + prevSp.extY / 2
      const yCenterNext = nextSp.y + nextSp.extY / 2
      return yCenterPrev - yCenterNext
    })
    const sumHeight = sortControls.reduce((sum, cur) => {
      return sum + cur.control.target.extY
    }, 0)

    let topY: number, bottomY: number, gapY: number
    if (isSelected && selectedSps.length > 2) {
      topY = sortControls[0].control.target.y
      const bottomSp = sortControls[sortControls.length - 1].control.target
      bottomY = bottomSp.y + bottomSp.extY
      gapY = (bottomY - topY - sumHeight) / (sortControls.length - 1)
    } else {
      const curSlide = handler.getCurrentSlide()
      if (sumHeight < curSlide.height) {
        gapY = (curSlide.height - sumHeight) / (sortControls.length + 1)
        topY = gapY
        bottomY = curSlide.height - gapY
      } else {
        topY = 0
        bottomY = curSlide.height
        gapY = (curSlide.height - sumHeight) / (sortControls.length - 1)
      }
    }
    const firstSp = selectedSps[0]

    let lastY = topY
    for (let i = 0; i < sortControls.length; ++i) {
      const sp = sortControls[i].control.target
      sortControls[i].control.track(0, lastY - sp.y)
      lastY += gapY + sp.extY
    }
    handleMoveControlEnd(handler, controls, undefined, firstSp.group)
  }
  calculateForRendering()

  handler.syncEditorUIState()
}

function getSlideElementsBounds(sps: SlideElement[]) {
  const allBounds: Array<ReturnType<typeof getSlideElementBound>> = []
  let [sumWidth, sumHeight] = [0, 0]
  let minX, minY, maxX, maxY, bounds
  for (let i = 0; i < sps.length; ++i) {
    bounds = getSlideElementBound(sps[i])
    allBounds.push(bounds)
    if (i === 0) {
      minX = bounds.minX
      minY = bounds.minY
      maxX = bounds.maxX
      maxY = bounds.maxY
    } else {
      minX = Math.min(minX, bounds.minX)
      minY = Math.min(minY, bounds.minY)
      maxX = Math.max(maxX, bounds.maxX)
      maxY = Math.max(maxY, bounds.maxY)
    }
    sumWidth += bounds.maxX - bounds.minX
    sumHeight += bounds.maxY - bounds.minY
  }
  return {
    allBounds,
    minX,
    maxX,
    minY,
    maxY,
    sumWidth,
    sumHeight,
  }
}

function getSlideElementBound(sp: SlideElement) {
  const tr = sp.transform
  const { xArr, yArr } = calcXYPointsByTransform(tr, sp.extX, sp.extY)
  return {
    minX: Math.min(...xArr),
    minY: Math.min(...yArr),
    maxX: Math.max(...xArr),
    maxY: Math.max(...yArr),
  }
}
