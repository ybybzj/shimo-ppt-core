import { InstanceType } from '../../../core/instanceTypes'
import { Matrix2D, MatrixUtils } from '../../../core/graphic/Matrix'
import { GraphicsBoundsChecker } from '../../../core/graphic/Bounds'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { EditLayerRenderElement } from '../common/EditLayerRenderElement'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { Shape } from '../../../core/SlideElement/Shape'
import { ImageShape } from '../../../core/SlideElement/Image'
import { SlideElement } from '../../../core/SlideElement/type'
import { GraphicFrame } from '../../../core/SlideElement/GraphicFrame'
import { DrawControlTrackFunc } from '../common/type'
import {
  TransformProps,
  calcTransformFormProps,
  calcXYPointsByTransform,
} from '../../../core/utilities/shape/calcs'
import { FillEffects } from '../../../core/SlideElement/attrs/fill/fill'
import { Ln } from '../../../core/SlideElement/attrs/line'
import { getGeometryForSp } from '../../../core/utilities/shape/geometry'
import { getBlipFillForSp } from '../../../core/utilities/shape/getters'
import { updateSpPrXfrm } from '../../../core/utilities/shape/updates'
import { ensureSpPrXfrm } from '../../../core/utilities/shape/attrs'
import { calcScaleFactorsOfGroup } from '../../../core/utilities/shape/group'
import { checkBoundsByXyPoints } from '../common/utils'

export class MoveSingleControl {
  readonly instanceType = InstanceType.MoveSingleControl
  handler: IEditorHandler
  isTracked = false
  target: SlideElement
  /** 注意其 transform 也被 layerRenderElement 所依赖，用于绘制 */
  transform: Matrix2D
  x!: number
  y!: number
  recentXDiff = 0
  recentYDiff = 0
  brush: FillEffects
  pen: Ln
  layerRenderElement: EditLayerRenderElement
  invertTransformOfGrp: null | Matrix2D = null
  drawFunc?: DrawControlTrackFunc
  constructor(target: Shape | ImageShape | GraphicFrame, handler, drawFunc?) {
    this.handler = handler
    this.drawFunc = drawFunc
    this.target = target
    this.transform = new Matrix2D()

    const blipFill = getBlipFillForSp(this.target)
    if (blipFill) {
      this.brush = FillEffects.Blip(blipFill)
    } else {
      this.brush = this.target.brush!
    }
    this.pen = this.target.pen!

    this.layerRenderElement = new EditLayerRenderElement(
      getGeometryForSp(this.target),
      this.target.extX,
      this.target.extY,
      this.brush,
      this.pen,
      this.transform
    )

    if (this.target.group) {
      this.invertTransformOfGrp = this.target.group.invertTransform.clone()
      this.invertTransformOfGrp.tx = 0
      this.invertTransformOfGrp.ty = 0
    }
  }
  track = (dx, dy) => {
    this.isTracked = true
    ;[this.recentXDiff, this.recentYDiff] = [dx, dy]
    const sp = this.target
    const dx2 = sp.group ? this.invertTransformOfGrp!.XFromPoint(dx, dy) : dx
    const dy2 = sp.group ? this.invertTransformOfGrp!.YFromPoint(dx, dy) : dy
    this.x = sp.x + dx2
    this.y = sp.y + dy2

    const calcProps: TransformProps = {
      pos: { x: this.x, y: this.y },
      size: { w: sp.extX, h: sp.extY },
      flipH: sp.flipH,
      flipV: sp.flipV,
      rot: sp.rot,
      group: sp.group,
    }
    calcTransformFormProps(this.transform, calcProps)
    this.layerRenderElement.setTransform(this.transform)
  }

  draw = (editLayerDrawer: EditLayerDrawer) => {
    if (this.drawFunc) {
      this.drawFunc(this, editLayerDrawer)
      return
    }
    this.layerRenderElement.draw(editLayerDrawer)
  }

  trackEnd = () => {
    if (!this.isTracked) {
      return
    }
    const sp = this.target
    let scale = { cx: 1, cy: 1, coff: 1 }
    let [chOffX, chOffY] = [0, 0]
    ensureSpPrXfrm(sp)
    if (sp.group) {
      scale = calcScaleFactorsOfGroup(sp.group)
      chOffX = sp.group.spPr!.xfrm!.chOffX!
      chOffY = sp.group.spPr!.xfrm!.chOffY!
    }
    const xfrm = sp.spPr!.xfrm!
    // const [offX, offY] = [xfrm.offX!, xfrm.offY!]
    xfrm.setOffX(this.x / scale.cx + chOffX)
    xfrm.setOffY(this.y / scale.cy + chOffY)
  }
  getBounds() {
    const checker = new GraphicsBoundsChecker()
    this.layerRenderElement.checkBounds(checker)
    const tr = this.transform
    const { extX, extY } = this.target
    const { xArr, yArr } = calcXYPointsByTransform(tr, extX, extY)
    checkBoundsByXyPoints(checker, xArr, yArr)

    return checker.bounds
  }
}

export class MoveGroupControl {
  readonly instanceType = InstanceType.MoveGroupControl
  handler: IEditorHandler
  isTracked = false
  x!: number
  y!: number
  target: GroupShape
  transform: Matrix2D
  layerRenderElements: EditLayerRenderElement[]
  transformArr: Matrix2D[] = []
  constructor(target: GroupShape, handler: IEditorHandler) {
    this.handler = handler
    this.target = target
    this.transform = new Matrix2D()
    this.layerRenderElements = []
    const childSps = target.getSlideElementsInGrp()
    const invertTransform = target.invertTransform
    for (let i = 0; i < childSps.length; ++i) {
      const childTransform = childSps[i].transform.clone()
      MatrixUtils.multiplyMatrixes(childTransform, invertTransform)
      this.transformArr[i] = childTransform
      this.layerRenderElements[i] = new EditLayerRenderElement(
        getGeometryForSp(childSps[i]),
        childSps[i].extX,
        childSps[i].extY,
        childSps[i].brush,
        childSps[i].pen,
        new Matrix2D()
      )
    }
  }
  track = (dx: number, dy: number) => {
    this.isTracked = true
    const sp = this.target
    this.x = sp.x + dx
    this.y = sp.y + dy

    const calcProps: TransformProps = {
      pos: { x: this.x, y: this.y },
      size: { w: sp.extX, h: sp.extY },
      flipH: sp.flipH,
      flipV: sp.flipV,
      rot: sp.rot,
    }
    calcTransformFormProps(this.transform, calcProps)

    for (let i = 0; i < this.layerRenderElements.length; ++i) {
      const spTransform = this.transformArr[i].clone()
      MatrixUtils.multiplyMatrixes(spTransform, this.transform)
      this.layerRenderElements[i].setTransform(spTransform)
    }
  }

  draw = (editLayerDrawer: EditLayerDrawer) => {
    for (let i = 0; i < this.layerRenderElements.length; ++i) {
      this.layerRenderElements[i].draw(editLayerDrawer)
    }
  }

  getBounds = () => {
    const checker = new GraphicsBoundsChecker()
    for (let i = 0; i < this.layerRenderElements.length; ++i) {
      this.layerRenderElements[i].checkBounds(checker)
    }

    return checker.bounds
  }

  trackEnd = () => {
    if (!this.isTracked) return
    updateSpPrXfrm(this.target)
    const xfrm = this.target.spPr!.xfrm!
    xfrm.setOffX(this.x)
    xfrm.setOffY(this.y)
  }
}

export type MoveControlTypes = MoveSingleControl | MoveGroupControl
