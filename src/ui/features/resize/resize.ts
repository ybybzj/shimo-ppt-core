import { Nullable } from '../../../../liber/pervasive'
import { PointerEventInfo } from '../../../common/dom/events'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { InstanceType } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { handleUIStateOnMouseMove } from '../../editorUI/utils/pointers'
import { ResizeControlTypes } from './resizeControls'
import { canResize } from '../../../core/utilities/shape/operations'
import { createResizeControl } from '../../rendering/editorHandler/utils'
import {
  calculateConnectorsAfterControl,
  checkConnectorControl,
  collectCxnSpsFromControls,
} from '../connector/utils'
import { ConnectorControl } from '../connector/connectorControl'
import { getRelDirectionByAbs, handleResizeOnMouseMove } from './utils'
import { startEditAction } from '../../../core/calculation/informChanges/startAction'
import { calculateForRendering } from '../../../core/calculation/calculate/calculateForRendering'
import { adjustXfrmOfGroup } from '../../../core/utilities/shape/group'

function makeResizeControls(handler: IEditorHandler, absDir, isGroup) {
  const controls: ResizeControlTypes[] = []
  const selectionState = handler.selectionState
  const selectedSps = isGroup
    ? selectionState.selectedElements
    : selectionState.selectedSlideElements

  selectedSps.forEach((sp) => {
    if (canResize(sp)) controls.push(createResizeControl(sp, absDir, handler))
  })
  return controls
}

export class ResizeCheckState {
  readonly instanceType = InstanceType.ResizeCheckState
  handler: IEditorHandler
  majorTarget: SlideElement
  /** 绝对方向, 从左上角开始依次为 7 - 0 - 1 ～ 6 */
  absDir: number
  preControls: Array<ResizeControlTypes | ConnectorControl>
  group: Nullable<GroupShape>

  constructor(
    handler: IEditorHandler,
    majorTarget: SlideElement,
    absDir: number,
    group?: Nullable<GroupShape>
  ) {
    this.handler = handler
    this.majorTarget = majorTarget
    this.absDir = absDir
    this.preControls = makeResizeControls(handler, absDir, !!group)
    this.group = group
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(e: PointerEventInfo, x, y) {
    if (!this.group && !e.isLocked) {
      this.onEnd(e, x, y)
      return
    }
    handleResizeCheckOnMouseMove(this, e, x, y)
  }

  onEnd(_e, _x, _y) {
    this.handler.changeToState(InstanceType.UIInitState, this.handler)
  }
}

export class ResizeState {
  readonly instanceType = InstanceType.ResizeState
  handler: IEditorHandler
  majorTarget: SlideElement
  /** 绝对方向， 从左上角开始依次为 7 - 0 - 1 ～ 6 */
  absDir: number
  /** 相对方向，相对形状本身方向不变， 无 flip 无 rot 时从左上角开始依次为 0 - 1 - 2 ～ 7 */
  relDir: number
  controls: Array<ResizeControlTypes | ConnectorControl>
  group: Nullable<GroupShape>

  constructor(
    handler: IEditorHandler,
    majorTarget: SlideElement,
    relDir: number,
    absDir: number,
    controls: Array<ResizeControlTypes | ConnectorControl>,
    group?: Nullable<GroupShape>
  ) {
    this.handler = handler
    this.majorTarget = majorTarget
    this.relDir = relDir
    this.absDir = absDir
    this.controls = controls
    this.group = group
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(e: PointerEventInfo, x, y) {
    if (!this.group && !e.isLocked) {
      this.onEnd(e, x, y)
      return
    }
    const { handler, controls, majorTarget: sp, relDir, absDir } = this
    handleResizeOnMouseMove(handler, controls, sp, relDir, absDir, e, x, y)
  }

  onEnd(_e, _x, _y) {
    handleResizeOnMouseUp(this)
  }
}

function handleResizeCheckOnMouseMove(
  state: ResizeCheckState,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  const { handler, preControls, majorTarget, absDir, group } = state
  checkConnectorControl(handler, preControls)
  handler.changeToState(
    InstanceType.ResizeState,
    state.handler,
    majorTarget,
    getRelDirectionByAbs(majorTarget, absDir),
    absDir,
    preControls,
    group
  )
  handleUIStateOnMouseMove(handler, e, x, y)
}

function handleResizeOnMouseUp(state: ResizeState) {
  if (EditorSettings.isViewMode) return
  const { handler, controls } = state
  const connectors = collectCxnSpsFromControls(handler, controls)
  startEditAction(EditActionFlag.action_SlideElements_EndResize)
  controls.forEach((control) => control.trackEnd())
  calculateConnectorsAfterControl(connectors)
  if (state.group) {
    adjustXfrmOfGroup(state.group)
  }
  calculateForRendering()

  handler.changeToState(InstanceType.UIInitState, handler)
  handler.updateEditLayer()
}

export type ResizeStateTypes = ResizeCheckState | ResizeState
