import { PointerEventInfo } from '../../../common/dom/events'
import {
  getFinalFlipH,
  getFinalFlipV,
} from '../../../core/utilities/shape/getters'
import { getInvertTransform } from '../../../core/utilities/shape/calcs'
import { SlideElement } from '../../../core/SlideElement/type'
import {
  AbsoluteDirections,
  ABS_DIRECTION,
  RelativeDirections,
  REL_DIRECTION,
} from '../../../core/common/const/ui'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { ConnectorControl } from '../connector/connectorControl'
import { ResizeControlTypes } from './resizeControls'
import { calcSnapXYs } from '../../../core/calculation/calculate/utils'

/** @param sp mainTarget */
export function handleResizeOnMouseMove<
  T extends ResizeControlTypes | ConnectorControl,
>(
  handler: IEditorHandler,
  controls: Array<T>,
  sp: SlideElement,
  relDir: number,
  absDir: number,
  e: PointerEventInfo,
  x: number,
  y: number,
  cb?: (control: T, scaleX: number, scaleY: number) => void
) {
  const track = (x: number, y: number) => {
    const { scaleX, scaleY } = getResizeFactoricients(sp, relDir, x, y)
    controls.forEach((control) => {
      if (cb) {
        cb(control, scaleX, scaleY)
      } else {
        control.track(scaleX, scaleY, e, x, y)
      }
    })
  }

  track(x, y)

  const shapeSnapXYInfoArr = controls.map((control) => {
    const ctr = control as ResizeControlTypes
    const { snapXs, snapYs } = calcSnapXYs(
      ctr.curExtX,
      ctr.curExtY,
      ctr.transform
    )
    return {
      snapXs,
      snapYs,
      shape: control.target,
    }
  })

  const [dx, dy] = handler.editorUI.guidelinesManager.checkGuidelines(
    handler.getCurrentSpTree(),
    shapeSnapXYInfoArr,
    absDir
  )
  // 吸附效果
  if (dx !== 0 || dy !== 0) {
    track(x + dx, y + dy)
  }

  handler.updateEditLayer()
}

export function getResizeFactoricients(sp: SlideElement, relDir, x, y) {
  const cx = sp.extX > 0 ? sp.extX : 0.01
  const cy = sp.extY > 0 ? sp.extY : 0.01

  const invertTransform = getInvertTransform(sp)
  const tx = invertTransform.XFromPoint(x, y)
  const ty = invertTransform.YFromPoint(x, y)

  switch (relDir) {
    case REL_DIRECTION.LT:
      return { scaleX: (cx - tx) / cx, scaleY: (cy - ty) / cy }
    case REL_DIRECTION.T:
      return { scaleX: (cy - ty) / cy, scaleY: 0 }
    case REL_DIRECTION.RT:
      return { scaleX: (cy - ty) / cy, scaleY: tx / cx }
    case REL_DIRECTION.R:
      return { scaleX: tx / cx, scaleY: 0 }
    case REL_DIRECTION.RB:
      return { scaleX: tx / cx, scaleY: ty / cy }
    case REL_DIRECTION.B:
      return { scaleX: ty / cy, scaleY: 0 }
    case REL_DIRECTION.LB:
      return { scaleX: ty / cy, scaleY: (cx - tx) / cx }
    case REL_DIRECTION.L:
      return { scaleX: (cx - tx) / cx, scaleY: 0 }
  }
  return { scaleX: 1, scaleY: 1 }
}

/** 根据绝对方向结合 shape 的翻转等信息计算相对方向 */
export function getRelDirectionByAbs(
  sp: SlideElement,
  absDir: number
): RelativeDirections {
  const hc = sp.extX * 0.5
  const vc = sp.extY * 0.5
  const transform = sp.getTransform()
  const y1 = transform.YFromPoint(hc, 0)
  const y3 = transform.YFromPoint(sp.extX, vc)
  const y5 = transform.YFromPoint(hc, sp.extY)
  const y7 = transform.YFromPoint(0, vc)

  let northByRel: RelativeDirections
  const flipH = getFinalFlipH(sp)
  const flipV = getFinalFlipV(sp)
  switch (Math.min(y1, y3, y5, y7)) {
    case y1: {
      northByRel = REL_DIRECTION.T
      break
    }
    case y3: {
      northByRel = REL_DIRECTION.R
      break
    }
    case y5: {
      northByRel = REL_DIRECTION.B
      break
    }
    default: {
      northByRel = REL_DIRECTION.L
      break
    }
  }
  const isSameFlip = (!flipH && !flipV) || (flipH && flipV)
  if (isSameFlip) return ((northByRel + absDir) % 8) as RelativeDirections
  return ((northByRel - absDir + 8) % 8) as RelativeDirections
}

/** 根据相对方向获取排除 Flip/rot 等因素的绝对的位置方向 */
export function getAbsDirectionByRel(
  sp: SlideElement,
  relDir: number // typeof REL_DIRECTION[keyof typeof REL_DIRECTION]
): AbsoluteDirections {
  const northInRel = getRelDirectionByAbs(sp, ABS_DIRECTION.N)
  const flipH = getFinalFlipH(sp)
  const flipV = getFinalFlipV(sp)
  const isSameFlip = (!flipH && !flipV) || (flipH && flipV)
  if (isSameFlip) {
    return ((relDir - northInRel + ABS_DIRECTION.N + 8) %
      8) as AbsoluteDirections
  }
  return ((ABS_DIRECTION.N - (relDir - northInRel) + 8) %
    8) as AbsoluteDirections
}
