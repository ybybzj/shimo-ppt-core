import {
  Slide_Default_Height,
  Slide_Default_Width,
} from '../../../core/common/const/drawing'
import {
  isRotateIn45D,
  checkAlmostEqual,
  normalizeRotValue,
  turnOffRecordChanges,
  restoreRecordChangesState,
} from '../../../core/common/utils'

import { EditorUtil } from '../../../globals/editor'
import { Matrix2D, MatrixUtils } from '../../../core/graphic/Matrix'
import { GraphicsBoundsChecker } from '../../../core/graphic/Bounds'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { EditLayerRenderElement } from '../common/EditLayerRenderElement'
import { isChartImage } from '../../../core/utilities/shape/asserts'
import { IEditorHandler } from '../../rendering/editorHandler/type'
import { Nullable } from '../../../../liber/pervasive'
import { SlideElement } from '../../../core/SlideElement/type'
import { InstanceType } from '../../../core/instanceTypes'
import { Geometry } from '../../../core/SlideElement/geometry/Geometry'
import { calculateSrcRectFunc } from '../crop/utils'
import { DrawControlTrackFunc } from '../common/type'
import { EditorSettings } from '../../../core/common/EditorSettings'
import {
  TextVerticalType,
  TextAutoFitType,
  ShapeLocks,
} from '../../../core/SlideElement/const'
import {
  calcTransformFormProps,
  calcXYPointsByTransform,
  TransformProps,
} from '../../../core/utilities/shape/calcs'
import { PresetShapeTypes } from '../../../exports/graphic'
import {
  calcConnectionInfoByXY,
  calculateCxnSpPr,
  ConnectionShape,
} from '../../../core/SlideElement/ConnectionShape'
import { getShapeCoordBound } from '../../../core/utilities/shape/draw'
import {
  getBlipFillForSp,
  getFinalFlipH,
  getFinalFlipV,
  getFinalRotate,
} from '../../../core/utilities/shape/getters'
import { FillEffects } from '../../../core/SlideElement/attrs/fill/fill'
import { Ln } from '../../../core/SlideElement/attrs/line'
import { SpPr } from '../../../core/SlideElement/attrs/shapePrs'
import {
  calculateSizesForGraphicFrame,
  calculateTableForGraphicFrame,
} from '../../../core/calculation/calculate/graphicFrame'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { PointerEventInfo } from '../../../common/dom/events'
import {
  AbsoluteDirections,
  ABS_DIRECTION,
  LINE_DIRECTION,
  RelativeDirections,
} from '../../../core/common/const/ui'
import { getRelDirectionByAbs } from './utils'
import { Shape } from '../../../core/SlideElement/Shape'
import { getGeometryForSp } from '../../../core/utilities/shape/geometry'
import {
  ConnectorInfo,
  convertToConnectorInfo,
} from '../../../core/SlideElement/ConnectorInfo'
import { checkBoundsByXyPoints, getAllSingularSps } from '../common/utils'
import { checkLock } from '../../../core/utilities/shape/locks'
import {
  renderConnectors,
  connectorHitShape,
  findConnectorInfoForSp,
} from '../connector/utils'
import {
  checkShapeBodyAutoFitReset,
  ensureSpPrXfrm,
} from '../../../core/utilities/shape/attrs'
import { calculateGeometry } from '../../../core/calculation/calculate/geometry'
import {
  calcScaleFactorsOfGroup,
  normalizeGroupExts,
} from '../../../core/utilities/shape/group'
import { BlipFill } from '../../../core/SlideElement/attrs/fill/blipFill'

const SIZE_MIN = 1.27
const REL_TO_ABS_FLIP_H = [2, 1, 0, 7, 6, 5, 4, 3] as const
const REL_TO_ABS_FLIP_V = [6, 5, 4, 3, 2, 1, 0, 7] as const
const REL_TO_ABS_FLIP_HV = [4, 5, 6, 7, 0, 1, 2, 3] as const
const REL_CORNOR_MOD = 2

/** 以 0 为起点的 ABS */
export const ABS_FORMAT_DIRECTION = {
  NW: 0,
  N: 1,
  NE: 2,
  E: 3,
  SE: 4,
  S: 5,
  SW: 6,
  W: 7,
} as const

const ABS_FORMAT_MOD4_DIRECTION = {
  NW_SE: 0,
  N_S: 1,
  NE_SW: 2,
  E_W: 3,
} as const

export class ResizeSingleControl {
  readonly instanceType = InstanceType.ResizeSingleControl
  handler!: IEditorHandler
  aspect!: number
  /** 是否需要互换 scaleX/Y */
  needChangeCoef!: boolean
  brush!: FillEffects
  centerX!: number
  centerY!: number
  cos!: number
  isKeepCentral = false
  isTracked = false
  target!: SlideElement
  startSp: Nullable<SlideElement>
  endSp: Nullable<SlideElement>
  beginSpId: Nullable<string>
  beginSpIdx: Nullable<number>
  endSpId: Nullable<string>
  endSpIdx: Nullable<number>
  isClickCornor = false
  sin!: number
  relDir!: RelativeDirections
  absDir!: AbsoluteDirections
  fixedX!: number
  fixedY!: number
  absDirMod4!: number
  originExtX!: number
  originExtY!: number
  originFlipH!: boolean
  originFlipV!: boolean
  /** 实际使用的宽高，如若原宽高为 0 时， 会赋 0.01 最小长度 */
  oldExtX!: number
  oldExtY!: number
  curExtX!: number
  curExtY!: number
  curFlipH!: boolean
  curFlipV!: boolean
  curX!: number
  curY!: number
  curRot!: number
  transform!: Matrix2D
  geometry!: Geometry
  pen?: Nullable<Ln>
  isConnector!: boolean
  isLine!: boolean
  /** 需要绘制的内容， 注意其 brush 即 this.brush 的引用 */
  layerRenderElement!: EditLayerRenderElement
  newShape!: null | SlideElement
  spPr: Nullable<SpPr> // TODO, need xfrm only
  drawFunc?: DrawControlTrackFunc
  constructor(
    sp: SlideElement,
    absDir, // 顺时针左上角起点为 7 的绝对方向, 需要转换为与 rel 相同的以 0 为起点
    handler: IEditorHandler,
    drawFunc?: DrawControlTrackFunc
  ) {
    const s = turnOffRecordChanges()
    initResizeControl(this, sp, absDir)
    this.handler = handler
    this.drawFunc = drawFunc

    const preset = getGeometryForSp(sp).preset as PresetShapeTypes
    const isConnector =
      sp.instanceType === InstanceType.Shape && sp.isConnectionShape()
    this.isLine = preset === 'line'

    const selectedSps = handler.selectionState.selectedSlideElements
    if (isConnector && selectedSps.length === 1) {
      const stId = sp.nvSpPr!.nvUniSpPr.stCxnId
      const endId = sp.nvSpPr!.nvUniSpPr.endCxnId
      this.startSp = EditorUtil.entityRegistry.getEntityById(stId)
      this.endSp = EditorUtil.entityRegistry.getEntityById(endId)
      this.isConnector = true
    }

    this.geometry = getGeometryForSp(sp).clone()
    this.brush = createBrushFromSp(sp)
    this.pen = sp.pen

    this.layerRenderElement = new EditLayerRenderElement(
      this.geometry,
      this.originExtX,
      this.originExtY,
      this.brush,
      this.pen,
      this.transform
    )
    restoreRecordChangesState(s)
  }

  /** line 按住 shift 后方向应该不再可变 */
  track(
    scaleX: number,
    scaleY: number,
    e: PointerEventInfo,
    x: number,
    y: number
  ) {
    this.isTracked = true
    const s = turnOffRecordChanges()
    if (this.isConnector) {
      this.resizeConnectionShape(scaleX, scaleY, e.shiftKey, x, y)
    } else {
      if (!e.ctrlKey) {
        this.resize(scaleX, scaleY, e.shiftKey)
      } else {
        resizeControlWithCenterKeep(this, scaleX, scaleY, e.shiftKey)
      }
    }
    restoreRecordChangesState(s)
  }

  /**
   * 除手动设置和同时操作多个 shape 旋转, 直线 connector rot 始终为 0， 操作后需要保证效果不变 rot 矫正为 0
   * bentConnector 等需通过计算得出 rot
   * */
  resizeConnectionShape(scaleX, scaleY, shiftKey: boolean, x, y) {
    const nvUniSpPr = (this.target as ConnectionShape).nvSpPr!.nvUniSpPr
    let startSp: Nullable<SlideElement> =
      EditorUtil.entityRegistry.getEntityById(nvUniSpPr.stCxnId)
    if (startSp?.isDeleted) {
      startSp = null
    }
    let endSp: Nullable<SlideElement> = EditorUtil.entityRegistry.getEntityById(
      nvUniSpPr.endCxnId
    )
    if (endSp?.isDeleted) {
      endSp = null
    }
    const sps: SlideElement[] = []
    getAllSingularSps(this.handler.getCurrentSpTree(), sps)
    let connectorInfo: Nullable<ConnectorInfo> = null
    let newShape: SlideElement | null = null
    this.newShape = null
    ;[this.beginSpId, this.beginSpIdx] = [null, null]
    ;[this.endSpId, this.endSpIdx] = [null, null]

    for (let i = sps.length - 1; i > -1; --i) {
      const sp = sps[i]
      if (sp === this.target) {
        continue
      }
      connectorInfo = findConnectorInfoForSp(sp, x, y, false)
      if (connectorInfo) {
        newShape = sp
        this.newShape = newShape
        if (sp.instanceType !== InstanceType.Shape || !sp.isConnectionShape()) {
          break
        }
      }
    }
    if (!this.newShape) {
      for (let i = sps.length - 1; i > -1; --i) {
        if (sps[i] === this.target) {
          continue
        }
        const sp = connectorHitShape(sps[i], x, y)
        if (sp) {
          this.newShape = sp
          break
        }
      }
    }
    let startConnectorInfo: Nullable<ConnectorInfo>
    let endConnectorInfo: Nullable<ConnectorInfo>
    // handle startPoint
    if (this.relDir === LINE_DIRECTION.START) {
      if (endSp) {
        const { ang, x, y } =
          getGeometryForSp(endSp).cxnLst[nvUniSpPr.endCxnIdx!]
        let [flipH, flipV, rot] = [endSp.flipH, endSp.flipV, endSp.rot]

        if (endSp.group) {
          rot = normalizeRotValue(getFinalRotate(endSp.group) + rot)
          if (getFinalFlipH(endSp.group)) {
            flipH = !flipH
          }
          if (getFinalFlipV(endSp.group)) {
            flipV = !flipV
          }
        }
        endConnectorInfo = convertToConnectorInfo(
          rot,
          flipH,
          flipV,
          endSp.transform,
          endSp.bounds,
          { idx: nvUniSpPr.endCxnIdx!, ang: ang!, x, y }
        )
      }
      startConnectorInfo = connectorInfo!
      if (startConnectorInfo) {
        this.beginSpId = this.newShape!.id
        this.beginSpIdx = startConnectorInfo.idx!
      }
    }
    // handle endPoint, TODO simplify
    else {
      if (startSp) {
        const { ang, x, y } =
          getGeometryForSp(startSp).cxnLst[nvUniSpPr.stCxnIdx!]
        let [flipH, flipV, rot] = [startSp.flipH, startSp.flipV, startSp.rot]
        if (startSp.group) {
          rot = normalizeRotValue(getFinalRotate(startSp.group) + rot)
          if (getFinalFlipH(startSp.group)) {
            flipH = !flipH
          }
          if (getFinalFlipV(startSp.group)) {
            flipV = !flipV
          }
        }
        startConnectorInfo = convertToConnectorInfo(
          rot,
          flipH,
          flipV,
          startSp.transform,
          startSp.bounds,
          { idx: nvUniSpPr.stCxnIdx!, ang: ang!, x, y }
        )
      }
      endConnectorInfo = connectorInfo!

      if (endConnectorInfo) {
        this.endSpId = this.newShape!.id
        this.endSpIdx = endConnectorInfo.idx
      }
    }

    if (startConnectorInfo || endConnectorInfo) {
      const { transform, extX, extY } = this.target
      if (!startConnectorInfo) {
        if (this.relDir === LINE_DIRECTION.START) {
          startConnectorInfo = calcConnectionInfoByXY(endConnectorInfo!, x, y)
        } else {
          const _x = transform.XFromPoint(0, 0)
          const _y = transform.YFromPoint(0, 0)
          startConnectorInfo = calcConnectionInfoByXY(endConnectorInfo!, _x, _y)
        }
      }
      if (!endConnectorInfo) {
        if (this.relDir === LINE_DIRECTION.START) {
          const _x = transform.XFromPoint(extX, extY)
          const _y = transform.YFromPoint(extX, extY)
          endConnectorInfo = calcConnectionInfoByXY(startConnectorInfo, _x, _y)
        } else {
          endConnectorInfo = calcConnectionInfoByXY(startConnectorInfo, x, y)
        }
      }
    }

    // 只要连接到连接点, 则两 param 存在
    if (startConnectorInfo && endConnectorInfo) {
      this.spPr = calculateCxnSpPr(
        startConnectorInfo,
        endConnectorInfo,
        this.target.spPr!.geometry!.preset,
        this.layerRenderElement.pen?.w
      )
      if (this.spPr && this.spPr.xfrm) {
        if (this.target.group) {
          const _xc = this.spPr.xfrm.offX! + this.spPr.xfrm.extX! / 2.0
          const _yc = this.spPr.xfrm.offY! + this.spPr.xfrm.extY! / 2.0
          const xc = this.target.group.invertTransform.XFromPoint(_xc, _yc)
          const yc = this.target.group.invertTransform.YFromPoint(_xc, _yc)
          this.spPr.xfrm.setOffX(xc - this.spPr.xfrm.extX! / 2.0)
          this.spPr.xfrm.setOffY(yc - this.spPr.xfrm.extY! / 2.0)
          this.spPr.xfrm.setFlipH(
            getFinalFlipH(this.target.group)
              ? !this.spPr.xfrm.flipH
              : this.spPr.xfrm.flipH
          )
          this.spPr.xfrm.setFlipV(
            getFinalFlipV(this.target.group)
              ? !this.spPr.xfrm.flipV
              : this.spPr.xfrm.flipV
          )
          this.spPr.xfrm.setRot(
            normalizeRotValue(
              this.spPr.xfrm.rot! - getFinalRotate(this.target.group)
            )
          )
        }
        this.curExtX = this.spPr.xfrm.extX
        this.curExtY = this.spPr.xfrm.extY
        this.curFlipH = this.spPr.xfrm.flipH
        this.curFlipV = this.spPr.xfrm.flipV
        this.curX = this.spPr.xfrm.offX
        this.curY = this.spPr.xfrm.offY
        this.curRot = this.spPr.xfrm.rot
        this.calcTransform()
        this.geometry = this.spPr.geometry!
        this.layerRenderElement.geometry = this.geometry
        calculateGeometry(
          this.geometry,
          this.spPr.xfrm.extX!,
          this.spPr.xfrm.extY!
        )
      }
    } else {
      this.spPr = null
      this.geometry = this.target.spPr!.geometry!.clone()
      this.layerRenderElement.geometry = this.geometry
      this.resize(scaleX, scaleY, shiftKey)
    }
  }

  resize(scaleX: number, scaleY: number, shiftKey: boolean) {
    resizeControl(this, scaleX, scaleY, shiftKey)
  }

  calcTransform() {
    const sp = this.target
    const calcProps: TransformProps = {
      pos: { x: this.curX, y: this.curY },
      size: { w: this.curExtX, h: this.curExtY },
      flipH: this.curFlipH,
      flipV: this.curFlipV,
      rot: this.curRot,
      group: sp.group,
    }
    calcTransformFormProps(this.transform, calcProps)
    this.layerRenderElement.setTransform(this.transform)

    // TODO, remove sideEffect
    const curState = this.handler.curState
    if (
      EditorSettings.isSupportCrop &&
      curState.instanceType === InstanceType.CropState
    ) {
      if (sp === curState.originObject) {
        ;(this.brush.fill as BlipFill).srcRect = calculateSrcRectFunc(
          this.transform,
          getShapeCoordBound(this.layerRenderElement),
          curState.cropBackGround.invertTransform,
          curState.cropBackGround.extX,
          curState.cropBackGround.extY
        )
      } else {
        ;(this.brush.fill as BlipFill).srcRect?.set(0, 0, 100, 100)
      }
    }
  }

  draw(editLayerDrawer: EditLayerDrawer | GraphicsBoundsChecker, transform?) {
    if (this.drawFunc) {
      this.drawFunc(this, editLayerDrawer, transform)
      return
    }
    if (this.newShape) {
      renderConnectors(this.newShape, editLayerDrawer)
    }
    if (editLayerDrawer.instanceType === InstanceType.EditLayerDrawer) {
      this.layerRenderElement.draw(editLayerDrawer, transform)
    } else {
      this.layerRenderElement.checkBounds(editLayerDrawer)
    }
  }

  getBounds() {
    const checker = new GraphicsBoundsChecker()

    this.draw(checker)
    const { transform: tr, curExtX, curExtY } = this
    const { xArr, yArr } = calcXYPointsByTransform(tr, curExtX, curExtY)
    checkBoundsByXyPoints(checker, xArr, yArr)

    return checker.bounds
  }

  trackEnd() {
    if (!this.isTracked) {
      return
    }
    const sp = this.target
    if (!this.isConnector || !this.spPr) {
      let scaleInfo = { cx: 1, cy: 1, coff: 1 }
      let chOffX = 0
      let chOffY = 0
      if (sp.group) {
        scaleInfo = calcScaleFactorsOfGroup(sp.group)
        chOffX = sp.group.spPr!.xfrm!.chOffX
        chOffY = sp.group.spPr!.xfrm!.chOffY
      }

      ensureSpPrXfrm(sp)
      const xfrm = sp.spPr!.xfrm!

      const newOffX = this.curX / scaleInfo.cx + chOffX
      const newOffY = this.curY / scaleInfo.cy + chOffY
      switch (sp.instanceType) {
        case InstanceType.ImageShape:
        case InstanceType.Shape:
          const newExtX = this.curExtX / scaleInfo.cx
          const newExtY = this.curExtY / scaleInfo.cy
          const bodyPr =
            sp.instanceType === InstanceType.Shape
              ? sp.txBody?.getBodyPr()
              : undefined
          if (bodyPr?.textFit?.type === TextAutoFitType.SHAPE) {
            if (bodyPr.vert === TextVerticalType.eaVert) {
              if (
                !checkAlmostEqual(newExtX, xfrm.extX, 0.1) &&
                checkAlmostEqual(newExtY, xfrm.extY, 0.1)
              ) {
                return
              }
            } else {
              if (
                checkAlmostEqual(newExtX, xfrm.extX, 0.1) &&
                !checkAlmostEqual(newExtY, xfrm.extY, 0.1)
              ) {
                return
              }
            }
          }
          xfrm.setOffX(newOffX)
          xfrm.setOffY(newOffY)

          const isEnlarge = newExtX * newExtY - xfrm.extX * xfrm.extY > 0.1

          xfrm.setExtX(newExtX)
          xfrm.setExtY(newExtY)
          xfrm.setFlipH(this.curFlipH)
          xfrm.setFlipV(this.curFlipV)

          // only refresh bitmap when image is enlarged
          if (sp.instanceType === InstanceType.ImageShape && isEnlarge) {
            const imgSrc = getBlipFillForSp(sp)?.imageSrcId
            const loadableImage = imgSrc
              ? this.handler.editorUI.editorKit.imageLoader.imagesMap[imgSrc]
              : undefined
            if (loadableImage) {
              loadableImage.updateSizeHint(newExtX, newExtY)
              loadableImage.reloadBitmap()
            }
          }

          break
        case InstanceType.GraphicFrame:
          const oldX = xfrm.offX
          const oldY = xfrm.offY
          const newX = newOffX
          const newY = newOffY
          sp.graphicObject!.resize(this.curExtX, this.curExtY)
          calculateTableForGraphicFrame(sp)
          calculateSizesForGraphicFrame(sp)
          if (!this.isKeepCentral) {
            if (!checkAlmostEqual(oldX, newX, 0.5)) {
              xfrm.setOffX(newOffX - sp.extX + this.curExtX)
            }
            if (!checkAlmostEqual(oldY, newY, 0.5)) {
              xfrm.setOffY(newOffY - sp.extY + this.curExtY)
            }
          } else {
            xfrm.setOffX(this.curX + this.curExtX / 2.0 - sp.extX / 2)
            xfrm.setOffY(this.curY + this.curExtY / 2.0 - sp.extY / 2)
          }
          break
        default:
          break
      }

      if (this.isConnector) {
        const nvSpPr = (sp as Shape).nvSpPr!
        const nvUniSpPr = nvSpPr.nvUniSpPr.clone()
        if (this.relDir === LINE_DIRECTION.START) {
          nvUniSpPr.stCxnIdx = this.beginSpIdx
          nvUniSpPr.stCxnId = this.beginSpId
          nvSpPr.setUniSpPr(nvUniSpPr)
        } else {
          nvUniSpPr.endCxnIdx = this.endSpIdx
          nvUniSpPr.endCxnId = this.endSpId
          nvSpPr.setUniSpPr(nvUniSpPr)
        }
      }
    } else {
      const sp = this.target as Shape
      ensureSpPrXfrm(sp)

      const xfrm = sp.spPr!.xfrm!
      const localXfrm = this.spPr.xfrm!
      xfrm.setOffX(localXfrm.offX)
      xfrm.setOffY(localXfrm.offY)
      xfrm.setExtX(localXfrm.extX)
      xfrm.setExtY(localXfrm.extY)
      xfrm.setRot(localXfrm.rot)
      xfrm.setFlipH(localXfrm.flipH)
      xfrm.setFlipV(localXfrm.flipV)
      sp.spPr!.setGeometry(this.spPr.geometry!.clone())
      const nvUniSpPr = sp.nvSpPr!.nvUniSpPr.clone()
      if (this.relDir === LINE_DIRECTION.START) {
        nvUniSpPr.stCxnIdx = this.beginSpIdx
        nvUniSpPr.stCxnId = this.beginSpId
        sp.nvSpPr!.setUniSpPr(nvUniSpPr)
      } else {
        nvUniSpPr.endCxnIdx = this.endSpIdx
        nvUniSpPr.endCxnId = this.endSpId
        sp.nvSpPr!.setUniSpPr(nvUniSpPr)
      }
    }
    // TODO, 直线连接线进入操作状态时，需要保持其位置不变，将 rot 矫正为 0
    checkShapeBodyAutoFitReset(sp)
  }
}

export class ResizeGroupControl {
  readonly instanceType = InstanceType.ResizeGroupControl
  isTracked = false
  target!: GroupShape
  parent?: ResizeGroupControl
  absDir!: AbsoluteDirections
  relDir!: RelativeDirections
  transform!: Matrix2D
  isSwapCoef!: boolean
  childControls: Array<SpForResizeInGroup | ResizeGroupControl> = []
  isClickCornor!: boolean
  aspect: any
  sin!: number
  cos!: number
  fixedX!: number
  fixedY!: number
  absDirMod4!: number
  centerX!: number
  centerY!: number
  originExtX!: number
  originExtY!: number
  originFlipH!: boolean
  originFlipV!: boolean
  oldExtX!: number
  oldExtY!: number
  curExtX!: number
  curExtY!: number
  curFlipH!: boolean
  curFlipV!: boolean
  curX!: number
  curY!: number
  curRot!: number
  needChangeCoef = false
  distXOfCenter?: number
  distYOfCenter?: number
  constructor(group: GroupShape, absDir, parent?: ResizeGroupControl) {
    const s = turnOffRecordChanges()
    initResizeControl(this, group, absDir)
    this.parent = parent
    this.relDir = getRelDirectionByAbs(group, absDir)
    this.isSwapCoef = !isRotateIn45D(group.rot)

    const childSps = group.spTree
    for (let i = 0; i < childSps.length; ++i) {
      const childSp = childSps[i]
      if (childSp.instanceType === InstanceType.GroupShape) {
        this.childControls[i] = new ResizeGroupControl(childSp, null, this)
      } else {
        this.childControls[i] = new SpForResizeInGroup(childSp, this)
      }
    }
    if (parent) {
      const { extX, extY, x, y } = group
      this.distXOfCenter = x + extX * 0.5 - parent.curExtX * 0.5
      this.distYOfCenter = y + extY * 0.5 - parent.curExtY * 0.5
    }
    restoreRecordChangesState(s)
  }
  track(scaleX, scaleY, e: PointerEventInfo) {
    const s = turnOffRecordChanges()
    this.isTracked = true
    if (!e.ctrlKey) {
      this.resize(scaleX, scaleY, e.shiftKey)
    } else {
      resizeControlWithCenterKeep(this, scaleX, scaleY, e.shiftKey)
    }
    restoreRecordChangesState(s)
  }

  resize(scaleX, scaleY, shiftKey) {
    resizeControl(this, scaleX, scaleY, shiftKey)
  }

  updateSizeWithParent(kw, kh) {
    this.isTracked = true
    const [_kw, _kh] = this.isSwapCoef ? [kh, kw] : [kw, kh]
    const xfrm = this.target.spPr!.xfrm!
    const parent = this.parent!
    this.curExtX = xfrm.extX * _kw
    this.curExtY = xfrm.extY * _kh

    this.curX =
      this.distXOfCenter! * kw + parent.curExtX * 0.5 - this.curExtX * 0.5
    this.curY =
      this.distYOfCenter! * kh + parent.curExtY * 0.5 - this.curExtY * 0.5

    const calcProps: TransformProps = {
      pos: { x: this.curX, y: this.curY },
      size: { w: this.curExtX, h: this.curExtY },
      flipH: xfrm.flipH,
      flipV: xfrm.flipV,
      rot: this.curRot,
    }
    calcTransformFormProps(this.transform, calcProps)

    MatrixUtils.multiplyMatrixes(this.transform, parent.transform)
    for (let i = 0; i < this.childControls.length; ++i) {
      this.childControls[i].updateSizeWithParent(_kw, _kh)
    }
  }

  draw(graphics) {
    for (let i = 0; i < this.childControls.length; ++i) {
      this.childControls[i].draw(graphics)
    }
  }
  getBounds() {
    const checker = new GraphicsBoundsChecker()
    this.draw(checker)
    const { transform: tr, curExtX, curExtY } = this
    const { xArr, yArr } = calcXYPointsByTransform(tr, curExtX, curExtY)
    checkBoundsByXyPoints(checker, xArr, yArr)

    return checker.bounds
  }

  trackEnd() {
    if (!this.isTracked) {
      return
    }
    if (!this.target.group) {
      normalizeGroupExts(this.target)
    }

    ensureSpPrXfrm(this.target)
    const xfrm = this.target.spPr!.xfrm!
    xfrm.setOffX(this.curX)
    xfrm.setOffY(this.curY)
    xfrm.setExtX(this.curExtX)
    xfrm.setExtY(this.curExtY)
    xfrm.setChExtX(this.curExtX)
    xfrm.setChExtY(this.curExtY)
    xfrm.setFlipH(this.curFlipH)
    xfrm.setFlipV(this.curFlipV)
    for (let i = 0; i < this.childControls.length; ++i) {
      this.childControls[i].trackEnd()
    }

    checkShapeBodyAutoFitReset(this.target)
  }
}

class SpForResizeInGroup {
  target!: SlideElement
  parent!: ResizeGroupControl
  x!: number
  y!: number
  extX!: number
  extY!: number
  rot!: number
  flipH!: boolean
  flipV!: boolean
  transform!: Matrix2D
  isSwapCoef!: boolean
  distXOfCenter!: number
  distYOfCenter!: number
  geometry!: Geometry
  layerRenderElement!: EditLayerRenderElement
  constructor(sp: SlideElement, parent: ResizeGroupControl) {
    const s = turnOffRecordChanges()
    this.target = sp
    this.parent = parent
    ;[this.x, this.y] = [sp.x, sp.y]
    ;[this.extX, this.extY] = [sp.extX, sp.extY]
    ;[this.flipH, this.flipV] = [sp.flipH, sp.flipV]
    this.rot = sp.rot
    this.transform = sp.transform.clone()
    this.isSwapCoef = !isRotateIn45D(this.rot)
    this.distXOfCenter = this.x + this.extX * 0.5 - this.parent.curExtX * 0.5
    this.distYOfCenter = this.y + this.extY * 0.5 - this.parent.curExtY * 0.5

    this.geometry = getGeometryForSp(sp).clone()
    calculateGeometry(this.geometry, this.extX, this.extY)

    this.layerRenderElement = new EditLayerRenderElement(
      this.geometry,
      this.extX,
      this.extY,
      createBrushFromSp(sp),
      sp.pen,
      this.transform
    )
    restoreRecordChangesState(s)
  }

  updateSizeWithParent(kw: number, kh: number) {
    const [scaleX, scaleY] = this.isSwapCoef ? [kh, kw] : [kw, kh]
    const sp = this.target
    ;[this.extX, this.extY] = [sp.extX * scaleX, sp.extY * scaleY]

    this.x =
      this.distXOfCenter * kw + this.parent.curExtX * 0.5 - this.extX * 0.5
    this.y =
      this.distYOfCenter * kh + this.parent.curExtY * 0.5 - this.extY * 0.5

    this.layerRenderElement.updateExt(this.extX, this.extY)

    /** ignore group transform at before */
    const calcProps: TransformProps = {
      pos: { x: this.x, y: this.y },
      size: { w: this.extX, h: this.extY },
      flipH: this.flipH,
      flipV: this.flipV,
      rot: this.rot,
    }
    calcTransformFormProps(this.transform, calcProps)
    this.layerRenderElement.setTransform(this.transform)
    MatrixUtils.multiplyMatrixes(this.transform, this.parent.transform)
  }

  draw(editLayerDrawer) {
    this.layerRenderElement.draw(editLayerDrawer)
  }

  getBounds() {
    const checker = new GraphicsBoundsChecker()
    checker.init(
      Slide_Default_Width,
      Slide_Default_Height,
      Slide_Default_Width,
      Slide_Default_Height
    )
    this.draw(checker)
    return {
      l: checker.bounds.minX,
      t: checker.bounds.minY,
      r: checker.bounds.maxX,
      b: checker.bounds.maxY,
    }
  }

  trackEnd() {
    ensureSpPrXfrm(this.target)
    const xfrm = this.target.spPr!.xfrm!
    xfrm.setOffX(this.x)
    xfrm.setOffY(this.y)
    xfrm.setExtX(this.extX)
    xfrm.setExtY(this.extY)

    checkShapeBodyAutoFitReset(this.target)
  }

  updateTransform() {
    this.transform.reset()
    const t = this.transform

    MatrixUtils.translateMatrix(t, -this.extX * 0.5, -this.extY * 0.5)
    MatrixUtils.rotateMatrix(t, -this.rot)
    if (this.flipH) {
      MatrixUtils.scaleMatrix(t, -1, 1)
    }
    if (this.flipV) {
      MatrixUtils.scaleMatrix(t, 1, -1)
    }
    MatrixUtils.translateMatrix(
      t,
      this.x + this.extX * 0.5,
      this.y + this.extY * 0.5
    )
    if (this.parent) {
      MatrixUtils.multiplyMatrixes(t, this.parent.transform)
    }
  }
}

function initResizeControl(
  control: ResizeControlTypes,
  sp: SlideElement,
  absDir
) {
  control.target = sp
  const { flipH, flipV, extX, extY, x, y, rot } = sp

  control.relDir = getRelDirectionByAbs(sp, absDir)
  control.absDir = getFormatAbsDirByRelAndFlip(control.relDir, flipH, flipV)
  control.absDirMod4 = control.absDir % 4
  control.transform = sp.transform.clone()
  control.isClickCornor = control.relDir % REL_CORNOR_MOD === 0
  control.aspect = control.isClickCornor
    ? calcAspect(control.absDir, extX, extY)
    : 0

  const [halfW, halfH] = [sp.extX / 2, sp.extY / 2]
  ;[control.fixedX, control.fixedY] = getSpFixedPosByAbsDir(sp, control.absDir)
  ;[control.curX, control.curY] = [x, y]
  ;[control.centerX, control.centerY] = [x + halfW, y + halfH]
  ;[control.originExtX, control.originExtY] = [extX, extY]
  ;[control.curExtX, control.curExtY] = [extX, extY]
  ;[control.originFlipH, control.originFlipV] = [flipH, flipV]
  ;[control.curFlipH, control.curFlipV] = [flipH, flipV]
  ;[control.sin, control.cos] = [Math.sin(rot), Math.cos(rot)]
  control.curRot = rot
  control.oldExtX = extX === 0 ? 0.01 : extX
  control.oldExtY = extY === 0 ? 0.01 : extY
  control.needChangeCoef = control.absDir % 2 === 0 && flipH !== flipV
}

function getFormatAbsDirByRelAndFlip(relDir: RelativeDirections, flipH, flipV) {
  if (!flipH && !flipV) {
    return relDir
  } else if (flipH && !flipV) {
    return REL_TO_ABS_FLIP_H[relDir]
  } else if (!flipH && flipV) {
    return REL_TO_ABS_FLIP_V[relDir]
  } else {
    return REL_TO_ABS_FLIP_HV[relDir]
  }
}

function getSpFixedPosByAbsDir(sp: SlideElement, absDir: AbsoluteDirections) {
  const [sin, cos] = [Math.sin(sp.rot), Math.cos(sp.rot)]
  const [spHalfW, spHalfH] = [sp.extX / 2, sp.extY / 2]
  let fixedX: number, fixedY: number
  switch (absDir) {
    case ABS_FORMAT_DIRECTION.NW:
    case ABS_FORMAT_DIRECTION.N: {
      fixedX = spHalfW * cos - spHalfH * sin + spHalfW + sp.x
      fixedY = spHalfW * sin + spHalfH * cos + spHalfH + sp.y
      break
    }
    case ABS_FORMAT_DIRECTION.NE:
    case ABS_FORMAT_DIRECTION.E: {
      fixedX = -spHalfW * cos - spHalfH * sin + spHalfW + sp.x
      fixedY = -spHalfW * sin + spHalfH * cos + spHalfH + sp.y
      break
    }
    case ABS_FORMAT_DIRECTION.SE:
    case ABS_FORMAT_DIRECTION.S: {
      fixedX = -spHalfW * cos + spHalfH * sin + spHalfW + sp.x
      fixedY = -spHalfW * sin - spHalfH * cos + spHalfH + sp.y
      break
    }
    case ABS_FORMAT_DIRECTION.SW:
    case ABS_FORMAT_DIRECTION.W: {
      fixedX = spHalfW * cos + spHalfH * sin + spHalfW + sp.x
      fixedY = spHalfW * sin - spHalfH * cos + spHalfH + sp.y
      break
    }
  }
  return [fixedX, fixedY] as const
}

function calcAspect(absDir: AbsoluteDirections, w, h) {
  const checkedW = Math.max(SIZE_MIN, w)
  const checkedH = Math.max(SIZE_MIN, h)
  return absDir === ABS_DIRECTION.E || absDir === ABS_DIRECTION.S
    ? checkedW / checkedH
    : checkedH / checkedW
}

function createBrushFromSp(sp: SlideElement) {
  let brush: FillEffects
  const blipFill = getBlipFillForSp(sp)
  if (blipFill) {
    brush = FillEffects.Blip(blipFill)
  } else {
    brush = sp.brush!
  }
  return brush
}

function resizeControl(
  ctrl: ResizeControlTypes,
  scaleX: number,
  scaleY: number,
  shiftKey
) {
  const isGroup = ctrl.instanceType === InstanceType.ResizeGroupControl
  if (!isGroup) {
    ctrl.isKeepCentral = false
  }
  const { target: sp, sin, cos, absDir } = ctrl
  const isLine = !isGroup && ctrl.isLine
  ;[scaleX, scaleY] = checkFlipLock(sp, scaleX, scaleY)
  ;[scaleX, scaleY] = checkAspectLock(ctrl, scaleX, scaleY, shiftKey)
  if (ctrl.needChangeCoef) {
    ;[scaleX, scaleY] = [scaleY, scaleX]
  }

  let realH, realW, absH, absW
  let resizedHalfW, resizedHalfH, calHalfW, calHalfH
  switch (absDir) {
    case ABS_FORMAT_DIRECTION.NW:
    case ABS_FORMAT_DIRECTION.N: {
      if (absDir === ABS_FORMAT_DIRECTION.NW) {
        realW = ctrl.oldExtX * scaleX
        absW = Math.abs(realW)
        ctrl.curExtX = absW >= SIZE_MIN ? absW : isLine ? 0 : SIZE_MIN
        ctrl.curFlipH = realW < 0 ? !ctrl.originFlipH : ctrl.originFlipH
      } else {
        ;[scaleX, scaleY] = [scaleY, scaleX]
      }
      realH = ctrl.oldExtY * scaleY
      absH = Math.abs(realH)
      ctrl.curExtY = absH >= SIZE_MIN ? absH : isLine ? 0 : SIZE_MIN
      if (realH < 0) {
        ctrl.curFlipV = !ctrl.originFlipV
        // 线段按住 shift 必须保持方向相同或完全相反
        if (isLine && shiftKey) {
          ctrl.curFlipH = !ctrl.originFlipH
        }
      } else {
        const hasFlipedH = ctrl.curFlipH !== ctrl.originFlipH
        if (isLine && shiftKey && hasFlipedH) {
          ctrl.curFlipV = !ctrl.originFlipV
        } else {
          ctrl.curFlipV = ctrl.originFlipV
        }
      }
      resizedHalfW = ctrl.curExtX * 0.5
      resizedHalfH = ctrl.curExtY * 0.5

      const hasFlipedH = ctrl.curFlipH !== ctrl.originFlipH
      const hasFlipedV = ctrl.curFlipV !== ctrl.originFlipV
      calHalfW = hasFlipedH ? -resizedHalfW : resizedHalfW
      calHalfH = hasFlipedV ? -resizedHalfH : resizedHalfH

      ctrl.curX =
        ctrl.fixedX + (-calHalfW * cos + calHalfH * sin) - resizedHalfW
      ctrl.curY =
        ctrl.fixedY + (-calHalfW * sin - calHalfH * cos) - resizedHalfH
      break
    }
    case ABS_FORMAT_DIRECTION.NE:
    case ABS_FORMAT_DIRECTION.E: {
      if (absDir === ABS_FORMAT_DIRECTION.NE) {
        ;[scaleX, scaleY] = [scaleY, scaleX]
        realH = ctrl.oldExtY * scaleY
        absH = Math.abs(realH)
        ctrl.curExtY = absH >= SIZE_MIN ? absH : isLine ? 0 : SIZE_MIN
        ctrl.curFlipV = realH < 0 ? !ctrl.originFlipV : ctrl.originFlipV
      }

      realW = ctrl.oldExtX * scaleX
      absW = Math.abs(realW)
      ctrl.curExtX = absW >= SIZE_MIN ? absW : isLine ? 0 : SIZE_MIN
      if (realW < 0) {
        ctrl.curFlipH = !ctrl.originFlipH
        if (isLine && shiftKey) {
          ctrl.curFlipV = !ctrl.originFlipV
        }
      } else {
        ctrl.curFlipH = ctrl.originFlipH
        const hasFlipedV = ctrl.curFlipV !== ctrl.originFlipV
        if (isLine && shiftKey && hasFlipedV) {
          ctrl.curFlipH = !ctrl.originFlipH
        }
      }

      resizedHalfW = ctrl.curExtX * 0.5
      resizedHalfH = ctrl.curExtY * 0.5

      const hasFlipedH = ctrl.curFlipH !== ctrl.originFlipH
      const hasFlipedV = ctrl.curFlipV !== ctrl.originFlipV
      calHalfW = hasFlipedH ? -resizedHalfW : resizedHalfW
      calHalfH = hasFlipedV ? -resizedHalfH : resizedHalfH

      ctrl.curX = ctrl.fixedX + (calHalfW * cos + calHalfH * sin) - resizedHalfW
      ctrl.curY = ctrl.fixedY + (calHalfW * sin - calHalfH * cos) - resizedHalfH
      break
    }
    case ABS_FORMAT_DIRECTION.SE:
    case ABS_FORMAT_DIRECTION.S: {
      if (absDir === ABS_FORMAT_DIRECTION.SE) {
        realW = ctrl.oldExtX * scaleX
        absW = Math.abs(realW)
        ctrl.curExtX = absW >= SIZE_MIN ? absW : isLine ? 0 : SIZE_MIN
        ctrl.curFlipH = realW < 0 ? !ctrl.originFlipH : ctrl.originFlipH
      } else {
        ;[scaleX, scaleY] = [scaleY, scaleX]
      }

      realH = ctrl.oldExtY * scaleY
      absH = Math.abs(realH)
      ctrl.curExtY = absH >= SIZE_MIN ? absH : isLine ? 0 : SIZE_MIN
      if (realH < 0) {
        ctrl.curFlipV = !ctrl.originFlipV
        if (isLine && shiftKey) {
          ctrl.curFlipH = !ctrl.originFlipH
        }
      } else {
        ctrl.curFlipV = ctrl.originFlipV
        const hasFlipedH = ctrl.curFlipH !== ctrl.originFlipH
        if (isLine && shiftKey && hasFlipedH) {
          ctrl.curFlipV = !ctrl.originFlipV
        }
      }

      resizedHalfW = ctrl.curExtX * 0.5
      resizedHalfH = ctrl.curExtY * 0.5
      const hasFlipedH = ctrl.curFlipH !== ctrl.originFlipH
      const hasFlipedV = ctrl.curFlipV !== ctrl.originFlipV
      calHalfW = hasFlipedH ? -resizedHalfW : resizedHalfW
      calHalfH = hasFlipedV ? -resizedHalfH : resizedHalfH

      ctrl.curX = ctrl.fixedX + (calHalfW * cos - calHalfH * sin) - resizedHalfW
      ctrl.curY = ctrl.fixedY + (calHalfW * sin + calHalfH * cos) - resizedHalfH
      break
    }
    case ABS_FORMAT_DIRECTION.SW:
    case ABS_FORMAT_DIRECTION.W: {
      if (absDir === ABS_FORMAT_DIRECTION.SW) {
        realH = ctrl.oldExtY * scaleX
        absH = Math.abs(realH)
        ctrl.curExtY = absH >= SIZE_MIN ? absH : isLine ? 0 : SIZE_MIN
        ctrl.curFlipV = realH < 0 ? !ctrl.originFlipV : ctrl.originFlipV
      } else {
        ;[scaleX, scaleY] = [scaleY, scaleX]
      }

      realW = ctrl.oldExtX * scaleY
      absW = Math.abs(realW)
      ctrl.curExtX = absW >= SIZE_MIN ? absW : isLine ? 0 : SIZE_MIN
      if (realW < 0) {
        ctrl.curFlipH = !ctrl.originFlipH
        if (isLine && shiftKey) {
          ctrl.curFlipV = !ctrl.originFlipV
        }
      } else {
        ctrl.curFlipH = ctrl.originFlipH
        const hasFlipedV = ctrl.curFlipV !== ctrl.originFlipV
        if (isLine && shiftKey && hasFlipedV) {
          ctrl.curFlipH = !ctrl.originFlipH
        }
      }

      resizedHalfW = ctrl.curExtX * 0.5
      resizedHalfH = ctrl.curExtY * 0.5
      const hasFlipedH = ctrl.curFlipH !== ctrl.originFlipH
      const hasFlipedV = ctrl.curFlipV !== ctrl.originFlipV
      calHalfW = hasFlipedH ? -resizedHalfW : resizedHalfW
      calHalfH = hasFlipedV ? -resizedHalfH : resizedHalfH

      ctrl.curX =
        ctrl.fixedX + (-calHalfW * cos - calHalfH * sin) - resizedHalfW
      ctrl.curY =
        ctrl.fixedY + (-calHalfW * sin + calHalfH * cos) - resizedHalfH
      break
    }
  }
  const needHandleCrop =
    !isGroup && ctrl.handler.curState.instanceType === InstanceType.CropState
  if (needHandleCrop) {
    ;[ctrl.curFlipH, ctrl.curFlipV] = [ctrl.originFlipH, ctrl.originFlipV]
  }

  calcControlOnResize(ctrl)
  if (!isGroup && ctrl.isConnector) {
    if (ctrl.relDir === LINE_DIRECTION.START) {
      ctrl.beginSpIdx = null
      ctrl.beginSpId = null
    } else if (ctrl.relDir === LINE_DIRECTION.END) {
      ctrl.endSpIdx = null
      ctrl.endSpId = null
    }
  }
}

function resizeControlWithCenterKeep(
  control: ResizeControlTypes,
  scaleX: number,
  scaleY: number,
  shiftKey
) {
  const { target: sp, absDirMod4, originFlipH, originFlipV } = control
  const isGroup = control.instanceType === InstanceType.ResizeGroupControl
  const isLine = !isGroup && control.isLine
  if (!isGroup) {
    if (isLine) {
      control.resize(scaleX, scaleY, shiftKey)
      return
    }
    control.isKeepCentral = true
    ;[scaleX, scaleY] = checkFlipLock(sp, scaleX, scaleY)
  }

  let realW, realH, absW, absH
  ;[scaleX, scaleY] = checkAspectLock(control, scaleX, scaleY, shiftKey)
  if (control.needChangeCoef) [scaleX, scaleY] = [scaleY, scaleX]

  switch (absDirMod4) {
    case ABS_FORMAT_MOD4_DIRECTION.NW_SE:
    case ABS_FORMAT_MOD4_DIRECTION.N_S:
      if (absDirMod4 === ABS_FORMAT_MOD4_DIRECTION.NW_SE) {
        realW = control.oldExtX * scaleX
        absW = Math.abs(realW)
        control.curExtX = isLine || absW >= SIZE_MIN ? absW : SIZE_MIN
        control.curFlipH = realW < 0 ? !originFlipH : originFlipH
      } else {
        ;[scaleX, scaleY] = [scaleY, scaleX]
      }
      realH = control.oldExtY * scaleY
      absH = Math.abs(realH)
      control.curExtY = isLine || absH >= SIZE_MIN ? absH : SIZE_MIN
      control.curFlipV = realH < 0 ? !originFlipV : originFlipV
      break
    case ABS_FORMAT_MOD4_DIRECTION.NE_SW:
    case ABS_FORMAT_MOD4_DIRECTION.E_W:
      if (absDirMod4 === ABS_FORMAT_MOD4_DIRECTION.NE_SW) {
        ;[scaleX, scaleY] = [scaleY, scaleX]
        realH = control.oldExtY * scaleY
        absH = Math.abs(realH)
        control.curExtY = isLine || absH >= SIZE_MIN ? absH : SIZE_MIN
        control.curFlipV = realH < 0 ? !originFlipV : originFlipV
      }
      realW = control.oldExtX * scaleX
      absW = Math.abs(realW)
      control.curExtX = absW >= isLine || SIZE_MIN ? absW : SIZE_MIN
      control.curFlipH = realW < 0 ? !originFlipH : originFlipH
      break
  }
  control.curX = control.centerX - control.curExtX * 0.5
  control.curY = control.centerY - control.curExtY * 0.5

  calcControlOnResize(control)
}

/** 表格, 图表等不允许 flip */
function checkFlipLock(sp: SlideElement, scaleX: number, scaleY) {
  if (sp.instanceType === InstanceType.GraphicFrame || isChartImage(sp)) {
    if (scaleX < 0) scaleX = 0
    if (scaleY < 0) scaleY = 0
  }
  return [scaleX, scaleY] as const
}

// 锁定纵横比(shift / 非 crop 状态的 image)
function checkAspectLock(
  control: ResizeControlTypes,
  scaleX: number,
  scaleY: number,
  shiftKey
) {
  const isGroup = control.instanceType === InstanceType.ResizeGroupControl
  const needHandleCrop =
    !isGroup && control.handler.curState.instanceType === InstanceType.CropState
  const { target: sp, isClickCornor } = control

  if (
    shiftKey === true ||
    (checkLock(sp, ShapeLocks.noModifyAspect) &&
      isClickCornor &&
      !needHandleCrop)
  ) {
    if (!isGroup && (control.isLine || control.isConnector)) {
      if (control.originExtX <= 0) scaleX = 1
      if (control.originExtY <= 0) scaleY = 1
    }

    if (Math.abs(scaleX / scaleY) >= 1) {
      scaleY = Math.abs(scaleX) * (scaleY >= 0 ? 1 : -1)
    } else {
      scaleX = Math.abs(scaleY) * (scaleX >= 0 ? 1 : -1)
    }
  }
  return [scaleX, scaleY] as const
}

function calcControlOnResize(control: ResizeControlTypes) {
  const { curX, curY, curExtX, curExtY, curFlipH, curFlipV, curRot } = control
  const isGroup = control.instanceType === InstanceType.ResizeGroupControl
  if (!isGroup) {
    calculateGeometry(control.geometry, curExtX, curExtY)
    control.layerRenderElement.updateExt(curExtX, curExtY)
  }
  const sp = control.target
  const calcProps: TransformProps = {
    pos: { x: curX, y: curY },
    size: { w: curExtX, h: curExtY },
    flipH: curFlipH,
    flipV: curFlipV,
    rot: curRot,
    group: sp.group,
  }
  calcTransformFormProps(control.transform, calcProps)
  if (isGroup) {
    const kw = curExtX / control.originExtX
    const kh = curExtY / control.originExtY
    for (let i = 0; i < control.childControls.length; ++i) {
      control.childControls[i].updateSizeWithParent(kw, kh)
    }
  } else {
    control.layerRenderElement.setTransform(control.transform)

    // TODO, remove sideEffect
    const curState = control.handler.curState
    if (curState.instanceType === InstanceType.CropState) {
      if (sp === curState.originObject) {
        ;(control.brush.fill as BlipFill).srcRect = calculateSrcRectFunc(
          control.transform,
          getShapeCoordBound(control.layerRenderElement),
          curState.cropBackGround.invertTransform!,
          curState.cropBackGround.extX,
          curState.cropBackGround.extY
        )
      } else {
        ;(control.brush.fill as BlipFill).srcRect?.set(0, 0, 100, 100)
      }
    }
  }
}

export type ResizeControlTypes = ResizeSingleControl | ResizeGroupControl
