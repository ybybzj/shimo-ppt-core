import { PointerEventInfo } from '../../../common/dom/events'
import {
  collectSelectedContentInParagraph,
  collectSelectedTextContent,
} from '../../../copyPaste/copy/collectTextContent'
import { gClipboard } from '../../../copyPaste/gClipboard'
import { ParaCollection } from '../../../core/Paragraph/ParaCollection'
import { updateClosestPosForParagraph } from '../../../core/Paragraph/utils/position'
import { Presentation } from '../../../core/Slide/Presentation'
import { Shape } from '../../../core/SlideElement/Shape'
import { TextFit } from '../../../core/SlideElement/attrs/TextFit'
import { BodyPr } from '../../../core/SlideElement/attrs/bodyPr'
import { FillEffects } from '../../../core/SlideElement/attrs/fill/fill'
import { SpPr, Xfrm } from '../../../core/SlideElement/attrs/shapePrs'
import { TextAutoFitType, TextWarpType } from '../../../core/SlideElement/const'
import { getMaxTextContentWidthByLimit } from '../../../core/TextDocument/utils/getter'
import { calculateShapeContent } from '../../../core/calculation/calculate/shapeContent'
import { Factor_mm_to_pix } from '../../../core/common/const/unit'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../../core/common/utils'
import { ZoomValue } from '../../../core/common/zoomValue'
import {
  getBase64ImgForSlideElement,
  getBase64ImgForSlideElements,
  getSlideElementPreview,
} from '../../../core/render/toImage'
import { ParaPr } from '../../../core/textAttributes/ParaPr'
import { checkExtentsByTextContent } from '../../../core/utilities/shape/extents'
import { resetTxBodyForShape } from '../../../core/utilities/shape/updates'
import { InstanceType } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import { EditorUI } from '../../editorUI/EditorUI'
import {
  convertGlobalInnerMMToPixPos,
  convertPixToInnerMMPos,
} from '../../editorUI/utils/utils'
import { IEditorHandler } from '../../rendering/editorHandler/type'
import { getHyperlinkFromSp } from '../../rendering/helpers'
import { MIN_SIZE } from '../addShape/addGeometryControl'
import { SelectionState } from '../../../core/Slide/SelectionState'
import { BrowserInfo } from '../../../common/browserInfo'

/** 在此阶段为选中的 shape 生成对应的 dom */
export class DragOutState {
  readonly instanceType = InstanceType.DragOutState
  handler: IEditorHandler
  dom?: HTMLElement
  hasDrag = false
  private onDragStart?: (e: DragEvent) => void
  private onDragEnd?: (e: DragEvent) => void
  private onPointerUp?: (e: PointerEvent) => void

  constructor(handler: IEditorHandler) {
    this.handler = handler
    this.initDom()
  }

  private initDom() {
    this.clearDom()
    const editorUI = this.handler.editorUI
    const selectionState = this.handler.presentation.selectionState
    if (!selectionState.hasSelection) return

    const dragDomInfo = makeDragableDom(editorUI, selectionState)

    if (dragDomInfo == null) return

    const { dom, src, width, height, link, downloadUrl } = dragDomInfo
    const dragImg = new Image(width, height)
    dragImg.src = src

    this.onDragStart = (e) => {
      e.stopPropagation()

      if (!e.dataTransfer) return
      this.hasDrag = true
      gClipboard.updataDragData(e.dataTransfer)

      if (link) {
        e.dataTransfer.setData('text/plain', link)
        e.dataTransfer.setData('text/uri-list', link)
      }

      if (downloadUrl) {
        e.dataTransfer.dropEffect = 'copy'
        e.dataTransfer.effectAllowed = 'all'
        const fileName = (/\/([^/?]+)(?:\b|(?:\?.*))$/.exec(downloadUrl) || [
          undefined,
          'download',
        ])[1]!
        const r = e.dataTransfer.setData(
          'DownloadURL',
          `:${fileName}:${downloadUrl}`
        )
        console.log(r, e.dataTransfer)
      }

      e.dataTransfer.setDragImage(dragImg, 0, 0)
    }

    this.onDragEnd = (e: DragEvent) => {
      e.stopPropagation()
      this.onResetState()
    }

    this.onPointerUp = (e) => {
      e.stopPropagation()
      this.onResetState()
      this.handler.editorUI.focusForKeyHandling()
    }

    dom.addEventListener('dragstart', this.onDragStart)
    dom.addEventListener('dragend', this.onDragEnd)
    dom.addEventListener('pointerup', this.onPointerUp)
    editorUI.editorElement.appendChild(dom)
    this.dom = dom
  }

  private onResetState() {
    this.handler.changeToState(InstanceType.UIInitState, this.handler)
  }

  private clearDom() {
    if (this.dom) {
      if (this.onDragEnd) {
        this.dom.removeEventListener('dragend', this.onDragEnd)
        this.onDragEnd = undefined
      }
      if (this.onDragStart) {
        this.dom.removeEventListener('dragstart', this.onDragStart)
        this.onDragStart = undefined
      }
      if (this.onPointerUp) {
        this.dom.removeEventListener('pointerup', this.onPointerUp)
        this.dom.removeEventListener('pointercancel', this.onPointerUp)
      }

      this.dom.remove()
      this.dom = undefined
      this.hasDrag = false
    }
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(_e: PointerEventInfo, _x: number, _y: number) {
    if (this.hasDrag) {
      this.handler.endDragOut()
    }
  }

  onEnd(_e: PointerEventInfo, _x, _y) {
    this.onResetState()
  }

  beforeLeave() {
    this.clearDom()
  }
}

function makeDragableDom(
  editorUI: EditorUI,
  selectionState: SelectionState
):
  | {
      dom: HTMLElement
      src: string
      width
      height
      link?: string
      downloadUrl?: string
    }
  | undefined {
  let dom: HTMLElement
  let src: string
  let link: string | undefined
  let downloadUrl: string | undefined

  const sps = selectionState.selectionStatusInfo.selectedElements

  const result = getBase64ImgForSlideElements(sps)
  if (result === '') return undefined

  src = result.src

  if (sps.length === 1) {
    const sp = sps[0]

    switch (sp.instanceType) {
      case InstanceType.ImageShape: {
        const ratio = Factor_mm_to_pix * (ZoomValue.value / 100) * BrowserInfo.PixelRatio
        const image = getSlideElementPreview(sp, ratio).image
        try {
          src = image.toDataURL('image/png')
        } catch {
          return undefined
        }

        if (sp.isOleObject() && sp.uri != null) {
          downloadUrl = editorUI.editorKit.transformImageSrcFunc
            ? editorUI.editorKit.transformImageSrcFunc(sp.uri, 'compressed')
            : sp.uri
        }
        const img = document.createElement('img')
        const imgSrc = sp.blipFill.encryptUrl ?? sp.blipFill.imageSrcId
        // use original src url to support drag download
        img.src = editorUI.editorKit.transformImageSrcFunc
          ? editorUI.editorKit.transformImageSrcFunc(imgSrc, 'compressed')
          : imgSrc
        dom = img
        const { x, y } = sp
        const { x: absX, y: absY } = convertGlobalInnerMMToPixPos(
          editorUI,
          x,
          y
        )
        const { sx, shx, sy, shy } = sp.getTransform()
        const [width, height] = [sp.extX * ratio, sp.extY * ratio]
        dom.style.width = `${width}px`
        dom.style.height = `${height}px`
        dom.style.transform = `matrix(${sx}, ${shy}, ${shx}, ${sy}, ${absX}, ${absY})`
        dom.draggable = true
        dom.style['-webkit-user-drag'] = 'auto'
        // dom.style.opacity = '0'
        dom.style['filter'] =
          'opacity(0.8) drop-shadow(6px 6px 10px rgba(50, 50, 50, .5))'

        dom.style.zIndex = '8'
        dom.style.cursor = 'pointer'
        dom.style.position = 'absolute'

        return { dom, src, width, height, link, downloadUrl }
      }
      case InstanceType.Shape:
      case InstanceType.GraphicFrame:
        dom = document.createElement('div')
        const { pointerEvtInfo: e, editorHandler: handler } = editorUI
        const textSp = handler.getTargetTextSp()
        const targetTextDoc = editorUI.presentation.getCurrentTextTarget()

        // 拖拽文本特殊处理
        if (textSp && targetTextDoc) {
          const paraCollection = new ParaCollection()

          const { lastX, lastY } = e
          const pos = convertPixToInnerMMPos(editorUI, lastX, lastY)
          const paraHyperlink = getHyperlinkFromSp(textSp, pos.x, pos.y)
          const s = turnOffRecordChanges()
          // 拖拽链接单独处理
          if (paraHyperlink) {
            link = paraHyperlink.getValue()
            // paraHyperlink.selectAll()
            const para = paraHyperlink.paragraph
            const textDoc = para.parent
            const index = para.children.elements.findIndex(
              (i) => i === paraHyperlink
            )
            if (-1 < index && textDoc === targetTextDoc) {
              const newPara = para.clone(para.parent)
              newPara.selection.isUse = true
              newPara.selection.startIndex = index
              newPara.selection.endIndex = index
              const newHyperlink = newPara.children.at(index)
              if (newHyperlink) {
                newHyperlink.selectAll()
              }
              collectSelectedContentInParagraph(newPara, paraCollection)
            }
          }
          // 仅光标选中不存在选取时, 按全选处理
          else {
            const oldSetToAll = targetTextDoc.isSetToAll
            if (targetTextDoc.selection.isUse !== true) {
              targetTextDoc.isSetToAll = true
            }

            collectSelectedTextContent(targetTextDoc, paraCollection)
            targetTextDoc.isSetToAll = oldSetToAll
          }

          paraCollection.elements.forEach(({ item: para }) => {
            para.parent = targetTextDoc
            para._calcParaPr()
          })
          if (0 < paraCollection.elements.length) {
            const shape = genShapeFromTextContent(
              editorUI.presentation,
              paraCollection
            )
            src = getBase64ImgForSlideElement(shape)
          }

          restoreRecordChangesState(s)
        } else {
          // dom.style.background = `rgba(0,0,255,0.8)`
          dom.style.background = `center / cover no-repeat url(${src})`
        }
        break
      default:
        dom = document.createElement('div')
        break
    }
  } else {
    dom = document.createElement('div')
  }
  const { width: imgW, height: imgH } = result
  const ratio = ZoomValue.value / 100
  const [width, height] = [imgW * ratio, imgH * ratio]
  const { l, t } = mergeSpBounds(sps)
  const { x: absX, y: absY } = convertGlobalInnerMMToPixPos(editorUI, l, t)

  dom.style.width = `${width}px`
  dom.style.height = `${height}px`
  dom.style.transform = `translate(${absX}px, ${absY}px)`
  dom.style.background = `center / cover no-repeat url(${src})`

  // Todo: cssText?
  dom.draggable = true
  // dom.style.opacity = '0'
  dom.style['filter'] =
    'opacity(0.8) drop-shadow(6px 6px 10px rgba(50, 50, 50, .5))'

  dom.style.zIndex = '8'
  dom.style.cursor = 'pointer'
  dom.style.position = 'absolute'

  return { dom, src, width, height, link, downloadUrl }
}

function mergeBounds(b1: { l; t; r; b }, b2: { l; t; r; b }) {
  return {
    l: Math.min(b1.l, b2.l),
    t: Math.min(b1.t, b2.t),
    r: Math.max(b1.r, b2.r),
    b: Math.max(b1.b, b2.b),
  }
}

function mergeSpBounds(sps: SlideElement[]) {
  const getSpBounds = (sp: SlideElement) => {
    const { l, t, r, b } = sp.bounds
    return { l, t, r, b }
  }
  const firstSp = sps[0]
  if (sps.length === 1) {
    return getSpBounds(firstSp)
  }
  return sps.reduce((acc, cur) => {
    return mergeBounds(acc, cur.bounds)
  }, getSpBounds(firstSp))
}

function genShapeFromTextContent(
  presentation: Presentation,
  paraCollection: ParaCollection
) {
  const s = turnOffRecordChanges()
  const curSlide = presentation.slides[presentation.currentSlideIndex]

  const shape = new Shape()
  shape.setSpPr(new SpPr())
  shape.spPr!.setParent(shape)
  shape.spPr!.setXfrm(new Xfrm(shape.spPr))
  const xfrm = shape.spPr!.xfrm!
  xfrm.setParent(shape.spPr)
  xfrm.setOffX(0)
  xfrm.setOffY(0)
  const fill = FillEffects.NoFill()
  shape.spPr!.setFill(fill)
  const bodyPr = BodyPr.Default()
  bodyPr.textFit = TextFit.new(TextAutoFitType.SHAPE)
  bodyPr.wrap = TextWarpType.SQUARE

  let paraPr: ParaPr | undefined
  const txDef = curSlide?.layout?.master!.theme?.txDef
  if (txDef) {
    const { bodyPr: txDefBodyPr, lstStyle, spPr } = txDef
    if (txDefBodyPr) {
      bodyPr.merge(txDefBodyPr)
    }
    const defaultParaPr = lstStyle?.levels[9]
    if (defaultParaPr) {
      paraPr = defaultParaPr
    }

    if (spPr.fillEffects) {
      shape.spPr!.setFill(spPr.fillEffects.clone())
    }
    if (spPr.ln) {
      if (shape.spPr!.ln) {
        shape.spPr!.ln.merge(spPr.ln)
      } else {
        shape.spPr!.setLn(spPr.ln.clone())
      }
    }
  }
  resetTxBodyForShape(shape, bodyPr, paraPr)
  const oldOffX = xfrm.offX
  const oldOffY = xfrm.offY
  calculateShapeContent(shape)
  checkExtentsByTextContent(shape)
  xfrm.setExtX(MIN_SIZE)
  xfrm.setOffX(oldOffX)
  xfrm.setOffY(oldOffY)

  const textDoc = shape.txBody!.textDoc!
  const paragraph = textDoc.children.at(0)
  if (paragraph) {
    const adjPos = {
      paragraph: paragraph,
      pos: paragraph.currentElementPosition(false, false),
    }
    updateClosestPosForParagraph(paragraph, adjPos)
    textDoc.insertSelection(paraCollection, adjPos, true, false)
  }

  const body_pr = shape.getBodyPr()!
  const w =
    getMaxTextContentWidthByLimit(textDoc, presentation.width / 2) +
    body_pr.lIns! +
    body_pr.rIns!
  const h = textDoc.getWholeHeight() + body_pr.tIns! + body_pr.bIns!
  xfrm.setExtX(w)
  xfrm.setExtY(h)
  xfrm.setOffX((presentation.width - w) / 2)
  xfrm.setOffY((presentation.height - h) / 2)
  shape.x = xfrm.offX
  shape.y = xfrm.offY
  shape.extX = xfrm.extX
  shape.extY = xfrm.extY
  restoreRecordChangesState(s)
  return shape
}
