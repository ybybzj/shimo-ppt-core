import { Nullable } from '../../../../liber/pervasive'
import { PointerEventInfo } from '../../../common/dom/events'
import {
  MouseMoveData,
  MouseMoveDataTypes,
} from '../../../core/properties/MouseMoveData'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { InstanceType } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { handleUIStateOnMouseMove } from '../../editorUI/utils/pointers'
import { convertGlobalInnerMMToPixPos } from '../../editorUI/utils/utils'
import { HoverInfo } from '../../states/type'
import { RotateControlTypes } from './rotateControls'
import { createRotateControl } from '../../rendering/editorHandler/utils'
import { canRotate } from '../../../core/utilities/shape/operations'
import { getSpRotateAng } from '../../../core/utilities/shape/calcs'
import {
  calculateConnectorsAfterControl,
  checkConnectorControl,
  collectCxnSpsFromControls,
} from '../connector/utils'
import { handleUpdateUICursor } from '../../editorUI/utils/updates'
import { ConnectorControl } from '../connector/connectorControl'
import { startEditAction } from '../../../core/calculation/informChanges/startAction'
import { calculateForRendering } from '../../../core/calculation/calculate/calculateForRendering'
import { adjustXfrmOfGroup } from '../../../core/utilities/shape/group'

function makeRotateControls(handler: IEditorHandler, isGroup: boolean) {
  const controls: RotateControlTypes[] = []
  const selectionState = handler.selectionState
  const selectedSps = isGroup
    ? selectionState.selectedElements
    : selectionState.selectedSlideElements

  selectedSps.forEach((sp) => {
    if (canRotate(sp)) controls.push(createRotateControl(sp))
  })
  return controls
}

export class RotateCheckState {
  readonly instanceType = InstanceType.RotateCheckState
  handler: IEditorHandler
  majorTarget: SlideElement
  preControls: Array<RotateControlTypes | ConnectorControl>
  group: Nullable<GroupShape>

  constructor(
    handler: IEditorHandler,
    majorTarget: SlideElement,
    group?: Nullable<GroupShape>
  ) {
    this.handler = handler
    this.majorTarget = majorTarget
    this.preControls = makeRotateControls(handler, !!group)
    this.group = group
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(e: PointerEventInfo, x, y) {
    handleRotateCheckOnMouseMove(this, e, x, y)
  }

  onEnd(_e, _x, _y) {
    this.handler.changeToState(InstanceType.UIInitState, this.handler)
  }
}

export class RotateState {
  readonly instanceType = InstanceType.RotateState
  handler: IEditorHandler
  majorTarget: SlideElement
  controls: Array<RotateControlTypes | ConnectorControl>
  group: Nullable<GroupShape>

  constructor(
    handler: IEditorHandler,
    majorTarget: SlideElement,
    controls: Array<RotateControlTypes | ConnectorControl>,
    group?: Nullable<GroupShape>
  ) {
    this.handler = handler
    this.majorTarget = majorTarget
    this.controls = controls
    this.group = group
  }

  getHoverInfo(_e: PointerEventInfo, x: number, y: number) {
    return getRotateStateCursorHoverInfo(this, x, y)
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(e: PointerEventInfo, x, y) {
    if (!e.isLocked) {
      this.onEnd(e, x, y)
      return
    }
    handleRotateOnMouseMove(this, e, x, y)
  }

  onEnd(_e, x, y) {
    handleRotateOnMouseUp(this, x, y)
  }
}

function getRotateStateCursorHoverInfo(
  state: RotateState,
  x: number,
  y: number
): HoverInfo {
  const handler = state.handler
  const controls = state.controls
  const ret: HoverInfo = {
    objectId: state.majorTarget && state.majorTarget.getId(),
    cursorType: 'crosshair',
  }
  if (
    (controls.length >= 1 &&
      controls[0].instanceType === InstanceType.RotateSingleControl) ||
    controls[0].instanceType === InstanceType.RotateGroupControl
  ) {
    const mouseMoveData = new MouseMoveData()
    const pixPos = convertGlobalInnerMMToPixPos(handler.editorUI, x, y)
    mouseMoveData.absX = pixPos.x
    mouseMoveData.absY = pixPos.y
    mouseMoveData.type = MouseMoveDataTypes.Rotate
    const angle = ((180 * controls[0].rot) / Math.PI).toFixed(2)
    mouseMoveData.rotateInfo = { state: 'Track', angle }
    ret.mouseData = mouseMoveData
  }
  return ret
}

function handleRotateCheckOnMouseMove(
  state: RotateCheckState,
  e: PointerEventInfo,
  x,
  y
) {
  if (!e.isLocked) {
    state.onEnd(e, x, y)
    return
  }
  const { handler, majorTarget, preControls, group } = state
  checkConnectorControl(handler, preControls)
  handler.changeToState(
    InstanceType.RotateState,
    handler,
    majorTarget,
    preControls,
    group
  )
  handleUIStateOnMouseMove(handler, e, x, y)
}

function handleRotateOnMouseMove(
  state: RotateState,
  e: PointerEventInfo,
  x,
  y
) {
  const angle = getSpRotateAng(state.majorTarget, x, y)
  for (let i = 0; i < state.controls.length; ++i) {
    ;(state.controls[i] as RotateControlTypes).track(angle, e)
  }
  state.handler.updateEditLayer()
}

function handleRotateOnMouseUp(state: RotateState, x, y) {
  const { handler, controls } = state
  const connectors = collectCxnSpsFromControls(handler, controls)
  startEditAction(EditActionFlag.action_SlideElements_EndRotate)
  controls.forEach((control) => control.trackEnd())
  calculateConnectorsAfterControl(connectors)
  if (state.group) {
    adjustXfrmOfGroup(state.group)
  }
  calculateForRendering()

  handler.changeToState(InstanceType.UIInitState, handler)
  handleUpdateUICursor(handler, x, y)
  handler.updateEditLayer()
  if (handler.editorUI.isInTouchMode()) {
    handler.editorUI.screenTipManager.hide()
  }
}

export type RotateStateTypes = RotateCheckState | RotateState
