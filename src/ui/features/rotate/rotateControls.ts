import { Matrix2D, MatrixUtils } from '../../../core/graphic/Matrix'
import { GraphicsBoundsChecker } from '../../../core/graphic/Bounds'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import {
  getInvertTransform,
  calcTransformFormProps,
  TransformProps,
  calcXYPointsByTransform,
} from '../../../core/utilities/shape/calcs'
import { EditLayerRenderElement } from '../common/EditLayerRenderElement'
import { InstanceType } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { PointerEventInfo } from '../../../common/dom/events'
import { FillEffects } from '../../../core/SlideElement/attrs/fill/fill'
import { getGeometryForSp } from '../../../core/utilities/shape/geometry'
import { getBlipFillForSp } from '../../../core/utilities/shape/getters'
import { updateSpPrXfrm } from '../../../core/utilities/shape/updates'
import { ensureSpPrXfrm } from '../../../core/utilities/shape/attrs'
import { checkBoundsByXyPoints } from '../common/utils'

const RAD_MIN = 0.07
const DEGREE_STEP = 15 // for shift rotate

export class RotateSingleControl {
  readonly instanceType = InstanceType.RotateSingleControl
  isTracked = false
  target: SlideElement
  transform: Matrix2D
  layerRenderElement: EditLayerRenderElement
  rot: number

  constructor(target: SlideElement) {
    this.target = target
    this.transform = new Matrix2D()
    let brush
    const blipFill = getBlipFillForSp(target)
    if (blipFill) {
      brush = FillEffects.Blip(blipFill)
    } else {
      brush = target.brush
    }
    this.layerRenderElement = new EditLayerRenderElement(
      getGeometryForSp(target),
      target.extX,
      target.extY,
      brush,
      target.pen,
      this.transform
    )

    this.rot = target.rot
  }
  draw = (editLayerDrawer: EditLayerDrawer, transform?: Matrix2D) => {
    this.layerRenderElement.draw(editLayerDrawer, transform)
  }

  track = (rot, e: PointerEventInfo) => {
    this.isTracked = true
    this.rot = formatRot(rot + this.target.rot, e.shiftKey)

    const sp = this.target
    const calcProps: TransformProps = {
      pos: { x: sp.x, y: sp.y },
      size: { w: sp.extX, h: sp.extY },
      flipH: sp.flipH,
      flipV: sp.flipV,
      rot: this.rot,
      group: sp.group,
    }
    calcTransformFormProps(this.transform, calcProps)
    this.layerRenderElement.setTransform(this.transform)
  }

  trackEnd = () => {
    if (!this.isTracked) {
      return
    }
    ensureSpPrXfrm(this.target)
    this.target.spPr!.xfrm!.setRot(this.rot)
  }

  getBounds = () => {
    const checker = new GraphicsBoundsChecker()

    this.layerRenderElement.checkBounds(checker)
    const tr = this.transform
    const { extX, extY } = this.target
    const { xArr, yArr } = calcXYPointsByTransform(tr, extX, extY)
    checkBoundsByXyPoints(checker, xArr, yArr)

    return checker.bounds
  }
}

export class RotateGroupControl {
  readonly instanceType = InstanceType.RotateGroupControl
  isTracked = false
  target: GroupShape
  transform: Matrix2D
  layerRenderElements: EditLayerRenderElement[] = []
  childTransforms: Matrix2D[] = []
  rot: number
  constructor(target: GroupShape) {
    this.target = target
    this.transform = new Matrix2D()
    const childSps = target.getSlideElementsInGrp()
    const invertTransform = getInvertTransform(target)
    for (let i = 0; i < childSps.length; ++i) {
      const tmpChildSp = childSps[i].getTransform().clone()
      MatrixUtils.multiplyMatrixes(tmpChildSp, invertTransform)
      this.childTransforms[i] = tmpChildSp
      this.layerRenderElements[i] = new EditLayerRenderElement(
        getGeometryForSp(childSps[i]),
        childSps[i].extX,
        childSps[i].extY,
        childSps[i].brush,
        childSps[i].pen,
        new Matrix2D()
      )
    }
    this.rot = target.rot
  }
  draw = (editLayerDrawer: EditLayerDrawer) => {
    for (let i = 0; i < this.layerRenderElements.length; ++i) {
      this.layerRenderElements[i].draw(editLayerDrawer)
    }
  }

  getBounds = () => {
    const checker = new GraphicsBoundsChecker()
    for (let i = 0; i < this.layerRenderElements.length; ++i) {
      this.layerRenderElements[i].checkBounds(checker)
    }
    const tr = this.transform
    const { extX, extY } = this.target
    const { xArr, yArr } = calcXYPointsByTransform(tr, extX, extY)
    checkBoundsByXyPoints(checker, xArr, yArr)

    return checker.bounds
  }

  track = (rot, e: PointerEventInfo) => {
    this.isTracked = true
    this.rot = formatRot(rot + this.target.rot, e.shiftKey)

    const sp = this.target
    const calcProps: TransformProps = {
      pos: { x: sp.x, y: sp.y },
      size: { w: sp.extX, h: sp.extY },
      flipH: sp.flipH,
      flipV: sp.flipV,
      rot: this.rot,
      group: sp.group,
    }
    calcTransformFormProps(this.transform, calcProps)
    for (let i = 0; i < this.layerRenderElements.length; ++i) {
      const newTransform = this.childTransforms[i].clone()
      MatrixUtils.multiplyMatrixes(newTransform, this.transform)
      this.layerRenderElements[i].setTransform(newTransform)
    }
  }

  trackEnd = () => {
    if (!this.isTracked) {
      return
    }

    updateSpPrXfrm(this.target)
    this.target.spPr!.xfrm!.setRot(this.rot)
  }
}

// helpers
function formatRot(rot: number, byStep?: boolean) {
  while (rot < 0) rot += 2 * Math.PI
  while (rot >= 2 * Math.PI) rot -= 2 * Math.PI

  if (rot < RAD_MIN || rot > 2 * Math.PI - RAD_MIN) rot = 0
  if (Math.abs(rot - Math.PI * 0.5) < RAD_MIN) rot = Math.PI * 0.5
  if (Math.abs(rot - Math.PI) < RAD_MIN) rot = Math.PI
  if (Math.abs(rot - 1.5 * Math.PI) < RAD_MIN) rot = 1.5 * Math.PI
  if (byStep) {
    const radParts = 180 / DEGREE_STEP
    rot = (Math.PI / radParts) * Math.floor((radParts * rot) / Math.PI)
  }
  return rot
}

export type RotateControlTypes = RotateSingleControl | RotateGroupControl
