import { PointerEventInfo, POINTER_BUTTON } from '../../../common/dom/events'
import { TableSelectionType } from '../../../core/common/const/table'
import { GraphicFrame } from '../../../core/SlideElement/GraphicFrame'
import { Shape } from '../../../core/SlideElement/Shape'
import { isEavertTextContent } from '../../../core/utilities/shape/getters'
import { syncInterfaceState } from '../../../editor/interfaceState/syncInterfaceState'
import { InstanceType } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import { IEditorHandler } from '../../rendering/editorHandler/type'
import { cursorExtType } from '../../editorUI/uiCursor'
/* import { defaultGetHoverInfo } from '../../states/initState' */
import {
  HoverInfo,
  TextEditingHitState,
  TextEdititngHitStateValues,
} from '../../states/type'
import { checkHitInTextSelection } from '../../states/hittests'
import { Evt_hideContextMenu } from '../../../editor/events/EventNames'
import { touchToggleContextMenu } from '../../editorUI/utils/utils'
import { handleSelectionStartForSp, handleSelectionEndForSp } from './utils'
import { getHyperlinkFromSp } from '../../rendering/helpers'
import { StatusOfFormatPainter } from '../../editorUI/const'

export class TouchSelectTextState {
  readonly instanceType = InstanceType.TouchSelectTextState
  handler: IEditorHandler
  majorTarget: SlideElement
  startX: number
  startY: number
  targetStartX: number
  targetStartY: number
  hitState: TextEdititngHitStateValues
  constructor(
    handler: IEditorHandler,
    majorTarget: SlideElement,
    hitState: TextEdititngHitStateValues,
    startX: number,
    startY: number,
    targetStartX = startX,
    targetStartY = startY
  ) {
    this.handler = handler
    this.majorTarget = majorTarget
    this.hitState = hitState
    this.startX = startX
    this.startY = startY
    this.targetStartX = targetStartX
    this.targetStartY = targetStartY
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(e: PointerEventInfo, x, y) {
    if (!e.isLocked) {
      this.onEnd(e, x, y)
      return
    }
    const dx = x - this.startX
    const dy = y - this.startY
    if (Math.abs(dx) < 0.001 && Math.abs(dy) < 0.001) {
      return
    }
    const targetX = this.targetStartX + dx
    const targetY = this.targetStartY + dy

    const sp = this.majorTarget as Shape | GraphicFrame

    switch (this.hitState) {
      case TextEditingHitState.Cursor: {
        handleSelectionStartForSp(sp, this.handler, targetX, targetY, e)
        break
      }
      case TextEditingHitState.SelectionEdge: {
        handleSelectionEndForSp(sp, this.handler, targetX, targetY, e)
        break
      }
      case TextEditingHitState.InSelection: {
        break
      }
    }

    this.handler.editorUI.editorKit.emitEvent(Evt_hideContextMenu)
    this.handler.checkSelectionState()
    this.handler.editorUI.renderingController.onUpdateEditLayer()
  }

  onEnd(e: PointerEventInfo, x: number, y: number) {
    const handler = this.handler
    const sp = this.majorTarget as Shape | GraphicFrame

    const dx = x - this.startX
    const dy = y - this.startY

    const targetX = this.targetStartX + dx
    const targetY = this.targetStartY + dy

    switch (this.hitState) {
      case TextEditingHitState.Cursor: {
        handleSelectionEndForSp(sp, this.handler, targetX, targetY, e)
        break
      }
      case TextEditingHitState.SelectionEdge: {
        handleSelectionEndForSp(sp, this.handler, targetX, targetY, e)
        break
      }
      case TextEditingHitState.InSelection: {
        if (!checkHitInTextSelection(sp, targetX, targetY)) {
          handleSelectionStartForSp(sp, this.handler, targetX, targetY, e)
        }
        break
      }
    }
    const editorUI = this.handler.editorUI
    // touchEmitEditorContextMenu(editorUI, { mmX: targetX, mmY: targetY })
    this.handler.editorUI.editorKit.emitEvent(Evt_hideContextMenu)
    if (
      this.handler.touchAction === 'press' ||
      this.handler.touchAction === 'longPress'
    ) {
      touchToggleContextMenu(editorUI, { mmX: targetX, mmY: targetY })
    }

    // 统一弹菜单
    handler.checkSelectionState()
    syncInterfaceState(handler.editorUI.editorKit)
    handler.changeToState(InstanceType.UIInitState, handler)

    const editorKit = handler.editorUI.editorKit
    if (editorKit && StatusOfFormatPainter.OFF !== editorKit.paintFormatState) {
      handler.pasteTextContentFormat()
      if (StatusOfFormatPainter.ON === editorKit.paintFormatState) {
        editorKit.emitPaintFormat(StatusOfFormatPainter.OFF)
      }
    }
  }
}

/** 文本输入 / Graphic col/row 调整分隔线 */
export class SelectTextState {
  readonly instanceType = InstanceType.SelectTextState
  handler: IEditorHandler
  majorTarget: SlideElement
  startX: number
  startY: number
  constructor(
    handler: IEditorHandler,
    majorTarget: SlideElement,
    startX: number,
    startY: number
  ) {
    this.handler = handler
    this.majorTarget = majorTarget
    this.startX = startX
    this.startY = startY
  }

  getHoverInfo(_e: PointerEventInfo, _x: number, _y: number): HoverInfo {
    return {
      objectId: this.majorTarget.id,
      cursorType: isEavertTextContent((this.majorTarget as Shape)?.txBody)
        ? cursorExtType.VerticalText
        : cursorExtType.Text,
    }
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(e: PointerEventInfo, x, y) {
    if (!e.isLocked) {
      this.onEnd(e, x, y)
      return
    }
    const dx = x - this.startX
    const dy = y - this.startY
    if (Math.abs(dx) < 0.001 && Math.abs(dy) < 0.001) {
      return
    }

    const sp = this.majorTarget as Shape | GraphicFrame

    handleSelectionEndForSp(sp, this.handler, x, y, e)

    if (
      !(
        sp.instanceType === InstanceType.GraphicFrame &&
        sp.graphicObject!.selection.selectionType === TableSelectionType.BORDER
      )
    ) {
      this.handler.checkSelectionState()
    }
    this.handler.editorUI.renderingController.onUpdateEditLayer()
  }

  onEnd(e: PointerEventInfo, x: number, y: number) {
    let originCtrlKey
    const handler = this.handler
    const sp = this.majorTarget as Shape | GraphicFrame

    if (handler.isInShowMod()) {
      originCtrlKey = e.ctrlKey
      e.ctrlKey = true
    }

    handleSelectionEndForSp(sp, this.handler, x, y, e)

    if (handler.isInShowMod()) {
      e.ctrlKey = originCtrlKey
    }
    handler.checkSelectionState()
    syncInterfaceState(handler.editorUI.editorKit)
    /* const hoverInfo = defaultGetHoverInfo(handler, e, x, y) */
    handler.changeToState(InstanceType.UIInitState, handler)
    const editorKit = handler.editorUI.editorKit
    const hyperlink = getHyperlinkFromSp(sp, x, y)
    if (hyperlink) {
      handler.editFocusContainer!.onUpdateSlide()
      if (
        e.button === POINTER_BUTTON.Left &&
        (e.ctrlKey || handler.isInShowMod())
      ) {
        editorKit.emitHyperlinkClick(hyperlink.value)
      }
    }
    if (editorKit && StatusOfFormatPainter.OFF !== editorKit.paintFormatState) {
      handler.pasteTextContentFormat()
    }
    this.handler.editorUI.editorKit.emitEvent(Evt_hideContextMenu)

    if (
      this.handler.touchAction === 'press' ||
      this.handler.touchAction === 'longPress'
    ) {
      touchToggleContextMenu(this.handler.editorUI, { mmX: x, mmY: y })
    }
  }
}
