import { syncInterfaceState } from '../../../editor/interfaceState/syncInterfaceState'
import { InstanceType } from '../../../core/instanceTypes'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'

export class RectSelectState {
  readonly instanceType = InstanceType.RectSelectState
  handler: IEditorHandler
  rect: { x; y; w; h }
  startX: number
  startY: number

  constructor(handler: IEditorHandler, startX, startY) {
    this.handler = handler
    this.startX = startX
    this.startY = startY
    this.rect = { x: startX, y: startY, w: 0, h: 0 }
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(_e, x, y) {
    this.rect.w = x - this.startX
    this.rect.h = y - this.startY
    this.handler.updateEditLayer()
  }

  onEnd(_e, _x, _y) {
    const handler = this.handler
    const spArr = handler.getCurrentSpTree()
    const { x, y, w, h } = this.rect
    const [l, r] = [Math.min(x, x + w), Math.max(x, x + w)]
    const [t, b] = [Math.min(y, y + h), Math.max(y, y + h)]

    for (let i = 0; i < spArr!.length; ++i) {
      const sp = spArr![i]
      const matrix = sp.transform
      const ltX = matrix.XFromPoint(0, 0)
      const ltY = matrix.YFromPoint(0, 0)
      if (ltX < l || ltX > r || ltY < t || ltY > b) continue

      const rbX = matrix.XFromPoint(sp.extX, sp.extY)
      const rbY = matrix.YFromPoint(sp.extX, sp.extY)
      if (rbX < l || rbX > r || rbY < t || rbY > b) continue

      const lbX = matrix.XFromPoint(0, sp.extY)
      const lbY = matrix.YFromPoint(0, sp.extY)
      if (lbX < l || lbX > r || lbY < t || lbY > b) continue

      const rtX = matrix.XFromPoint(sp.extX, 0)
      const rtY = matrix.YFromPoint(sp.extX, 0)
      if (rtX < l || rtX > r || rtY < t || rtY > b) continue

      handler.selectionState.select(sp)
    }

    handler.changeToState(InstanceType.UIInitState, handler)
    handler.updateEditLayer()
    syncInterfaceState(handler.editorUI.editorKit)
  }

  drawControl(editLayerDrawer: EditLayerDrawer) {
    const { x, y, w, h } = this.rect
    editLayerDrawer.drawSelectionRect(x, y, w, h)
  }
}
