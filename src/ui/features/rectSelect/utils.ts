import {
  PointerEventInfo,
  POINTER_BUTTON,
  POINTER_EVENT_TYPE,
} from '../../../common/dom/events'
import { getAndUpdateCursorPositionForTargetOrTextContent } from '../../../core/utilities/cursor/cursorPosition'
import { getPresentation } from '../../../core/utilities/finders'
import {
  setSelectionStartForTextDocument,
  setSelectionEndForTextDocument,
  setSelectionStartForTable,
  setSelectionEndForTable,
} from '../../../core/utilities/selection/setter'
import { getTextDocumentOfSp } from '../../../core/utilities/shape/getters'
import { isCoordsInSelectionForTextDocument } from '../../../core/utilities/tableOrTextDocContent/helpers'
import { InstanceType } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import { IEditorHandler } from '../../rendering/editorHandler/type'

export function handleSelectionStartForSp(
  sp: SlideElement,
  handler: IEditorHandler,
  x: number,
  y: number,
  e: PointerEventInfo
) {
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      const textDoc = getTextDocumentOfSp(sp)
      if (textDoc) {
        const tx = sp.invertTextTransform!.XFromPoint(x, y)
        const ty = sp.invertTextTransform!.YFromPoint(x, y)
        if (e.button === POINTER_BUTTON.Right) {
          if (isCoordsInSelectionForTextDocument(textDoc, tx, ty)) {
            sp.isRightButtonPressed = true
            return
          }
        }
        if (!e.shiftKey) {
          setSelectionStartForTextDocument(textDoc, tx, ty, e)
        } else {
          if (!textDoc.hasTextSelection()) {
            textDoc.startSelectFromCurrentPosition()
          }
          setSelectionEndForTextDocument(textDoc, tx, ty, e)
        }
      }
      break
    }
    case InstanceType.GraphicFrame: {
      if (POINTER_BUTTON.Right === e.button) {
        sp.isRightButtonPressed = true
        return
      }

      const table = sp.graphicObject
      if (table) {
        const isInTextEditing = handler.selectionState?.selectedTextSp != null
        const tx = sp.invertTransform.XFromPoint(x, y)
        const ty = sp.invertTransform.YFromPoint(x, y)

        const cursorPosition = getPresentation(sp)?.cursorPosition
        if (!e.shiftKey) {
          if (cursorPosition) {
            cursorPosition.x = tx
            cursorPosition.y = ty
          }
          setSelectionStartForTable(table, tx, ty, e, isInTextEditing)
        } else {
          if (!table.hasTextSelection()) {
            table.startSelectFromCurrentPosition()
          }
          setSelectionEndForTable(table, tx, ty, e, isInTextEditing)
        }
        getAndUpdateCursorPositionForTargetOrTextContent(table)
      }

      break
    }
  }
}

export function handleSelectionEndForSp(
  sp: SlideElement,
  handler: IEditorHandler,
  x: number,
  y: number,
  e: PointerEventInfo
) {
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      const textDoc = getTextDocumentOfSp(sp)
      if (textDoc) {
        const tx = sp.invertTextTransform!.XFromPoint(x, y)
        const ty = sp.invertTextTransform!.YFromPoint(x, y)
        if (!(e.type === POINTER_EVENT_TYPE.Up && sp.isRightButtonPressed)) {
          setSelectionEndForTextDocument(textDoc, tx, ty, e)
        }
      }
      sp.isRightButtonPressed = false

      break
    }
    case InstanceType.GraphicFrame: {
      if (POINTER_EVENT_TYPE.Move === e.type) {
        sp.isRightButtonPressed = false
      }
      if (sp.isRightButtonPressed && POINTER_EVENT_TYPE.Up === e.type) {
        sp.isRightButtonPressed = false
        return
      }
      const table = sp.graphicObject
      if (table) {
        const isInTextEditing = handler.selectionState?.selectedTextSp != null
        const tx = sp.invertTransform.XFromPoint(x, y)
        const ty = sp.invertTransform.YFromPoint(x, y)
        setSelectionEndForTable(table, tx, ty, e, isInTextEditing)
      }
    }
  }
}
