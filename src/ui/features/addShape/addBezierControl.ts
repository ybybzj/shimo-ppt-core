import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../../core/common/utils'

import { Geometry } from '../../../core/SlideElement/geometry/Geometry'
import { Matrix2D } from '../../../core/graphic/Matrix'
import { GraphicsBoundsChecker } from '../../../core/graphic/Bounds'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { toggleCxtTransformReset } from '../../../core/graphic/utils'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { InstanceType } from '../../../core/instanceTypes'
import { ShapeRenderingInfo } from '../../../core/render/drawShape/type'
import { drawShapeByInfoWithDrawer } from '../../../core/render/drawShape/drawShape'
import { ShapeRenderingState } from '../../../core/graphic/ShapeRenderingState'
import { drawShapeStroke } from '../../../core/render/drawShape/drawStroke'
import { setDeletedForSp } from '../../../core/utilities/shape/updates'
import { Ln } from '../../../core/SlideElement/attrs/line'
import { SpPr, Xfrm } from '../../../core/SlideElement/attrs/shapePrs'
import { ShapeStyle } from '../../../core/SlideElement/attrs/ShapeStyle'
import { calculateForShape } from '../../../core/calculation/calculate/shape'
import { Shape } from '../../../core/SlideElement/Shape'
import {
  PathAddCmd,
  addPathOperationToGeom,
  setRectToGeom,
} from '../../../core/SlideElement/geometry/operations'
import { Theme } from '../../../core/SlideElement/attrs/theme'

export const SplineCommandType = {
  moveTo: 0,
  lineTo: 1,
  bezier: 2,
} as const

export class MoveToCommand {
  readonly id = SplineCommandType.moveTo
  x: any
  y: any
  constructor(x, y) {
    this.x = x
    this.y = y
  }
}

export class LineToCommand {
  readonly id = SplineCommandType.lineTo
  x: any
  y: any
  constructor(x, y) {
    this.x = x
    this.y = y
  }
  changeFinalPoint = (x, y) => {
    this.x = x
    this.y = y
  }
}

export class BezierCommand {
  readonly id = SplineCommandType.bezier
  x1: any
  y1: any
  x2: any
  y2: any
  x3: any
  y3: any
  constructor(x1, y1, x2, y2, x3, y3) {
    this.x1 = x1
    this.y1 = y1
    this.x2 = x2
    this.y2 = y2
    this.x3 = x3
    this.y3 = y3
  }
  changeFinalPoint = (x, y) => {
    this.x3 = x
    this.y3 = y
    this.x2 = this.x1 + (this.x3 - this.x1) * 0.5
    this.y2 = this.y1 + (this.y3 - this.y1) * 0.5
  }
}

export type SplineCommand = MoveToCommand | LineToCommand | BezierCommand

/** 曲线 controller */
export class Spline {
  readonly instanceType = InstanceType.Spline
  path!: SplineCommand[]
  handler!: IEditorHandler
  Matrix!: Matrix2D
  transform!: Matrix2D
  style!: ShapeStyle
  pen!: Ln
  splineForRender!: SplineForRender
  constructor(handler: IEditorHandler, theme: Theme, master?, layout?, slide?) {
    const s = turnOffRecordChanges()
    this.handler = handler
    this.path = []
    this.Matrix = new Matrix2D()
    this.transform = new Matrix2D()

    const style = ShapeStyle.Default()
    style.fillRef!.color.updateBy(theme, slide, layout, master, {
      r: 0,
      g: 0,
      b: 0,
      a: 255,
    })
    let rgba = style.fillRef!.color.rgba
    const pen = theme.getLnStyle(style.lnRef!.idx, style.lnRef!.color)
    style.lnRef!.color.updateBy(theme, slide, layout, master)
    rgba = style.lnRef!.color.rgba
    this.style = style

    if (pen.fillEffects) {
      pen.fillEffects.calculate(theme, slide, layout, master, rgba)
    }
    this.pen = pen
    this.splineForRender = new SplineForRender(this)
    restoreRecordChangesState(s)
  }

  draw = (g: EditLayerDrawer) => {
    this.splineForRender.render(g)
  }

  getShape = (_editContainer) => {
    const paths = this.path
    const startPath = paths[0] as MoveToCommand
    let xMax = startPath.x,
      yMax = startPath.y,
      xMin = xMax,
      yMin = yMax
    let i

    let isClosed = false

    // 第二个以后都为贝塞尔曲线
    if (paths.length > 2) {
      const secondPath = paths[1] as BezierCommand
      const endPath = paths[paths.length - 1] as BezierCommand
      const dx = startPath.x - endPath.x3
      const dy = startPath.y - endPath.y3
      if (Math.sqrt(dx * dx + dy * dy) < 3) {
        isClosed = true
        endPath.x3 = startPath.x
        endPath.y3 = startPath.y
        let vx, vy
        if (paths.length > 3) {
          const secondToLastPath = paths[paths.length - 2] as BezierCommand
          vx = (secondPath.x3 - secondToLastPath.x3) / 6
          vy = (secondPath.y3 - secondToLastPath.y3) / 6
        } else {
          vx = -(secondPath.y3 - startPath.y) / 6
          vy = (secondPath.x3 - startPath.x) / 6
        }

        secondPath.x1 = startPath.x + vx
        secondPath.y1 = startPath.y + vy
        endPath.x2 = startPath.x - vx
        endPath.y2 = startPath.y - vy
      }
    }

    let minX = startPath.x
    let maxX = minX
    let minY = startPath.y
    let maxY = minY
    let [lastX, lastY] = [startPath.x, startPath.y]

    for (let index = 1; index < paths.length; ++index) {
      const path = paths[index]
      if (path.id === SplineCommandType.lineTo) {
        if (minX > path.x) minX = path.x
        if (maxX < path.x) maxX = path.x
        if (minY > path.y) minY = path.y
        if (maxY < path.y) maxY = path.y

        lastX = path.x
        lastY = path.y
      } else {
        const { x1, x2, x3, y1, y2, y3 } = path as BezierCommand
        const bezierPartition = partitionBezier(
          lastX,
          lastY,
          x1,
          y1,
          x2,
          y2,
          x3,
          y3
        )
        for (let i = 1; i < bezierPartition.length; ++i) {
          const curPt = bezierPartition[i]
          if (minX > curPt.x) minX = curPt.x
          if (maxX < curPt.x) maxX = curPt.x
          if (minY > curPt.y) minY = curPt.y
          if (maxY < curPt.y) maxY = curPt.y

          lastX = x3
          lastY = y3
        }
      }
    }

    xMin = minX
    xMax = maxX
    yMin = minY
    yMax = maxY
    const shape = new Shape()

    shape.setSpPr(new SpPr())
    shape.spPr!.setParent(shape)
    shape.spPr!.setXfrm(new Xfrm(shape.spPr))
    shape.spPr!.xfrm!.setParent(shape.spPr)
    shape.spPr!.xfrm!.setOffX(xMin)
    shape.spPr!.xfrm!.setOffY(yMin)
    shape.spPr!.xfrm!.setExtX(xMax - xMin)
    shape.spPr!.xfrm!.setExtY(yMax - yMin)
    shape.setStyle(ShapeStyle.Default())

    const geometry = new Geometry()
    const w = xMax - xMin,
      h = yMax - yMin
    let kw, kh, pathW, pathH
    if (w > 0) {
      pathW = 43200
      kw = 43200 / w
    } else {
      pathW = 0
      kw = 0
    }
    if (h > 0) {
      pathH = 43200
      kh = 43200 / h
    } else {
      pathH = 0
      kh = 0
    }
    addPathOperationToGeom(
      geometry,
      PathAddCmd.create,
      undefined,
      isClosed ? 'norm' : 'none',
      undefined,
      pathW,
      pathH
    )
    setRectToGeom(geometry, 'l', 't', 'r', 'b')
    for (i = 0; i < paths.length; ++i) {
      const path = paths[i]
      switch (path.id) {
        case SplineCommandType.moveTo: {
          addPathOperationToGeom(
            geometry,
            PathAddCmd.moveTo,
            (((path.x - xMin) * kw) >> 0) + '',
            (((path.y - yMin) * kh) >> 0) + ''
          )
          break
        }
        case SplineCommandType.lineTo: {
          addPathOperationToGeom(
            geometry,
            PathAddCmd.lineTo,
            (((path.x - xMin) * kw) >> 0) + '',
            (((path.y - yMin) * kh) >> 0) + ''
          )
          break
        }
        case SplineCommandType.bezier: {
          addPathOperationToGeom(
            geometry,
            PathAddCmd.cubicBezTo,
            (((path.x1 - xMin) * kw) >> 0) + '',
            (((path.y1 - yMin) * kh) >> 0) + '',
            (((path.x2 - xMin) * kw) >> 0) + '',
            (((path.y2 - yMin) * kh) >> 0) + '',
            (((path.x3 - xMin) * kw) >> 0) + '',
            (((path.y3 - yMin) * kh) >> 0) + ''
          )
          break
        }
      }
    }
    if (isClosed) {
      addPathOperationToGeom(geometry, PathAddCmd.close)
    }
    shape.spPr!.setGeometry(geometry)
    setDeletedForSp(shape, false)
    calculateForShape(shape)
    shape.x = xMin
    shape.y = yMin
    return shape
  }

  addPathOp = (pathOp) => {
    this.path.push(pathOp)
  }
  getBounds = () => {
    const checker = new GraphicsBoundsChecker()
    this.splineForRender.checkBounds(checker)

    return checker.bounds
  }
}

class SplineForRender {
  spline: Spline
  pen: Ln
  transform: Matrix2D
  Matrix: Matrix2D
  constructor(spline: Spline) {
    this.spline = spline
    this.pen = spline.pen
    this.transform = spline.transform
    this.Matrix = spline.Matrix
  }
  render(graphics: EditLayerDrawer) {
    toggleCxtTransformReset(graphics, false)
    graphics.applyTransformMatrix(this.Matrix)
    const renderingInfo: ShapeRenderingInfo = {
      pen: this.pen,
      matrix: this.transform,
    }
    drawShapeByInfoWithDrawer(renderingInfo, this, graphics)
  }

  checkBounds(checker: GraphicsBoundsChecker) {
    toggleCxtTransformReset(checker, false)
    checker.applyTransformMatrix(this.Matrix)
    for (let i = 0; i < this.spline.path.length; ++i) {
      const spline = this.spline.path[i]
      switch (spline.id) {
        case SplineCommandType.moveTo: {
          checker._moveTo(spline.x, spline.y)
          break
        }
        case SplineCommandType.lineTo: {
          checker._lineTo(spline.x, spline.y)
          break
        }
        case SplineCommandType.bezier: {
          checker._bezierCurveTo(
            spline.x1,
            spline.y1,
            spline.x2,
            spline.y2,
            spline.x3,
            spline.y3
          )
          break
        }
      }
    }
    if (checker.needCheckLineWidth) {
      checker.correctBoundsWithSx()
    }
  }
  draw(state: ShapeRenderingState, g: EditLayerDrawer) {
    g._end()
    for (let i = 0; i < this.spline.path.length; ++i) {
      const spline = this.spline.path[i]
      switch (spline.id) {
        case SplineCommandType.moveTo: {
          g._moveTo(spline.x, spline.y)
          break
        }
        case SplineCommandType.lineTo: {
          g._lineTo(spline.x, spline.y)
          break
        }
        case SplineCommandType.bezier: {
          g._bezierCurveTo(
            spline.x1,
            spline.y1,
            spline.x2,
            spline.y2,
            spline.x3,
            spline.y3
          )
          break
        }
      }
    }
    state.isRect = false
    drawShapeStroke(state, g)
  }
}
// helper
function partitionBezier(
  x0: number,
  y0: number,
  x1: number,
  y1: number,
  x2: number,
  y2: number,
  x3: number,
  y3: number
): Array<{ x: number; y: number }> {
  const dx01 = x1 - x0
  const dy01 = y1 - y0
  const dx12 = x2 - x1
  const dy12 = y2 - y1
  const dx23 = x3 - x2
  const dy23 = y3 - y2

  const r01 = Math.sqrt(dx01 * dx01 + dy01 * dy01)
  const r12 = Math.sqrt(dx12 * dx12 + dy12 * dy12)
  const r23 = Math.sqrt(dx23 * dx23 + dy23 * dy23)

  if (Math.max(r01, r12, r23) < 1) {
    return [
      { x: x0, y: y0 },
      { x: x1, y: y1 },
      { x: x2, y: y2 },
      { x: x3, y: y3 },
    ]
  }

  const x01 = (x0 + x1) * 0.5
  const y01 = (y0 + y1) * 0.5

  const x12 = (x1 + x2) * 0.5
  const y12 = (y1 + y2) * 0.5

  const x23 = (x2 + x3) * 0.5
  const y23 = (y2 + y3) * 0.5

  const x012 = (x01 + x12) * 0.5
  const y012 = (y01 + y12) * 0.5

  const x123 = (x12 + x23) * 0.5
  const y123 = (y12 + y23) * 0.5

  const x0123 = (x012 + x123) * 0.5
  const y0123 = (y012 + y123) * 0.5

  return partitionBezier(x0, y0, x01, y01, x012, y012, x0123, y0123).concat(
    partitionBezier(x0123, y0123, x123, y123, x23, y23, x3, y3)
  )
}
