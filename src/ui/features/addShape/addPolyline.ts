import { InstanceType } from '../../../core/instanceTypes'
import { PolyLine } from './addPolylineControl'
import { convertPxToMM } from '../../../core/utilities/helpers'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { PointerEventInfo } from '../../../common/dom/events'
import { HoverInfo } from '../../states/type'
import { getThemeForEditContainer } from '../../../core/utilities/presentation/getters'
import { AddGeometryState } from './addGeometry'
import {
  BezierState,
  QuadraticBezierState,
  QuadraticBezierClickState,
  BezierClickState,
  CubicBezierState,
  CubicBezierClickState,
} from './addBezier'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'

/** 任意曲线(多点近似拟合), pre 绘制状态 */
export class PolyLineState {
  readonly instanceType = InstanceType.PolyLineState
  handler: IEditorHandler
  control!: PolyLine

  constructor(handler: IEditorHandler) {
    this.handler = handler
  }

  onStart(_e, x, y) {
    const handler = this.handler
    const theme = getThemeForEditContainer(handler.presentation)!
    this.control = new PolyLine(handler, theme, null)
    this.control.points.push({ x: x, y: y })
    handler.selectionState.reset()
    handler.updateEditLayer()
    const minDelta = convertPxToMM(1)
    handler.changeToState(
      InstanceType.PolyLineClickState,
      handler,
      minDelta,
      this.control
    )
    return true
  }

  onMove() {}

  onEnd() {
    this.handler.changeToState(InstanceType.UIInitState, this.handler)
    this.handler.editorUI.editorKit.emitEndAddShape()
  }
}

/** 任意曲线, 绘制状态 */
export class PolyLineClickState {
  readonly instanceType = InstanceType.PolyLineClickState
  handler: IEditorHandler
  minDelta: number
  isStart!: boolean
  control: PolyLine

  constructor(handler: IEditorHandler, minDelta, control: PolyLine) {
    this.handler = handler
    this.minDelta = minDelta
    this.control = control
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(_e, x, y) {
    const splineControl = this.control
    const lastPoint = splineControl.points[splineControl.points.length - 1]
    const dx = x - lastPoint.x
    const dy = y - lastPoint.y

    if (Math.sqrt(dx * dx + dy * dy) >= this.minDelta) {
      splineControl.points.push({ x, y })
      this.handler.updateEditLayer()
    }
  }

  onEnd(e: PointerEventInfo, x, y) {
    if (this.control.points.length > 1) {
      this.isStart = true
      AddGeometryState.prototype.onEnd.call(this, e, x, y)
    } else {
      this.handler.updateEditLayer()
      this.handler.changeToState(InstanceType.UIInitState, this.handler)
      this.handler.editorUI.editorKit.emitEndAddShape()
    }
  }

  drawControl(editLayerDrawer: EditLayerDrawer) {
    if (this.control) this.control.draw(editLayerDrawer)
  }
}

/** 任意形状, preClick 状态, 预处理相关状态，生成起点 */
export class PolyShapeState {
  readonly instanceType = InstanceType.PolyShapeState
  handler: IEditorHandler
  control!: PolyLine
  constructor(handler: IEditorHandler) {
    this.handler = handler
  }

  onStart(_e, x, y) {
    const handler = this.handler
    handler.selectionState.reset()
    handler.updateEditLayer()
    const theme = getThemeForEditContainer(this.handler.presentation)!
    this.control = new PolyLine(this.handler, theme, null)
    this.control.points.push({ x: x, y: y })
    handler.changeToState(
      InstanceType.PolyShapePreClickState,
      handler,
      x,
      y,
      this.control
    )
    return true
  }

  onMove(_e, _x, _y) {}
  onEnd(_e, _x, _y) {}
}

export class PolyShapePreClickState {
  readonly instanceType = InstanceType.PolyShapePreClickState
  handler: IEditorHandler
  x: number
  y: number
  control: PolyLine
  constructor(handler: IEditorHandler, x, y, control: PolyLine) {
    this.handler = handler
    this.x = x
    this.y = y
    this.control = control
  }

  onStart({ clicks }: PointerEventInfo, _x, _y) {
    if (clicks > 1) {
      this.handler.editorUI.editorKit.emitEndAddShape()
      this.handler.changeToState(InstanceType.UIInitState, this.handler)
      return true
    }
    return false
  }

  onMove(_e, x, y) {
    if (this.x !== x || this.y !== y) {
      this.control.points.push({ x, y })
      this.handler.changeToState(
        InstanceType.PolyShapeClickState,
        this.handler,
        this.control
      )
    }
  }

  onEnd(_e, _x, _y) {}
}

/** 任意形状, 绘制直线/(检查)闭合 */
export class PolyShapeClickState {
  readonly instanceType = InstanceType.PolyShapeClickState
  handler: IEditorHandler
  minSize: number
  isStart!: boolean
  control: PolyLine
  constructor(handler: IEditorHandler, control: PolyLine) {
    this.handler = handler
    this.minSize = convertPxToMM(1)
    this.control = control
  }

  getHoverInfo(_e: PointerEventInfo, _x: number, _y: number): HoverInfo {
    return { objectId: '1', cursorType: 'crosshair' }
  }

  onStart(e: PointerEventInfo, x, y) {
    const polyLineControl = this.control
    polyLineControl.points.push({ x, y })

    if (
      e.clicks > 1 ||
      polyLineControl.isClosed(this.handler.editFocusContainer)
    ) {
      this.isStart = true
      AddGeometryState.prototype.onEnd.call(this, e, x, y)
      return true
    }
    return false
  }

  onMove(e: PointerEventInfo, x, y) {
    const points = this.control.points
    if (!e.isLocked) {
      points[points.length - 1] = { x, y }
    } else {
      const lastPoint = points[points.length - 1]
      const dx = x - lastPoint.x
      const dy = y - lastPoint.y

      if (Math.sqrt(dx * dx + dy * dy) >= this.minSize) {
        points.push({ x, y })
      }
    }
    this.handler.updateEditLayer()
  }

  onEnd(e: PointerEventInfo, x, y) {
    if (e.clicks > 1) {
      this.isStart = true
      AddGeometryState.prototype.onEnd.call(this, e, x, y)
    }
  }

  drawControl(editLayerDrawer: EditLayerDrawer) {
    if (this.control) this.control.draw(editLayerDrawer)
  }
}

export type ShapeStateTypes =
  | AddGeometryState
  | BezierState
  | BezierClickState
  | QuadraticBezierState
  | QuadraticBezierClickState
  | CubicBezierState
  | CubicBezierClickState
  | PolyLineState
  | PolyLineClickState
  | PolyShapeState
  | PolyShapePreClickState
  | PolyShapeClickState
