import { Geometry } from '../../../core/SlideElement/geometry/Geometry'
import { Matrix2D } from '../../../core/graphic/Matrix'
import { GraphicsBoundsChecker } from '../../../core/graphic/Bounds'
import { convertPxToMM, mmsPerPt } from '../../../core/utilities/helpers'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { toggleCxtTransformReset } from '../../../core/graphic/utils'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { InstanceType } from '../../../core/instanceTypes'
import { ShapeRenderingInfo } from '../../../core/render/drawShape/type'
import { drawShapeByInfoWithDrawer } from '../../../core/render/drawShape/drawShape'
import { ShapeRenderingState } from '../../../core/graphic/ShapeRenderingState'
import { drawShapeStroke } from '../../../core/render/drawShape/drawStroke'
import { setDeletedForSp } from '../../../core/utilities/shape/updates'
import { Ln } from '../../../core/SlideElement/attrs/line'
import { SpPr, Xfrm } from '../../../core/SlideElement/attrs/shapePrs'
import { ShapeStyle } from '../../../core/SlideElement/attrs/ShapeStyle'
import { calculateForShape } from '../../../core/calculation/calculate/shape'
import { Shape } from '../../../core/SlideElement/Shape'
import { Theme } from '../../../core/SlideElement/attrs/theme'
import { Slide } from '../../../core/Slide/Slide'
import { Nullable } from '../../../../liber/pervasive'
import { Notes } from '../../../core/Slide/Notes'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../../core/common/utils'
import {
  PathAddCmd,
  addPathOperationToGeom,
  setRectToGeom,
} from '../../../core/SlideElement/geometry/operations'

/** 自由曲线(画笔，松开即结束)/形状 controller */
export class PolyLine {
  readonly instanceType = InstanceType.Polyline
  handler!: IEditorHandler
  /** 多点近似拟合 */
  points!: Array<{ x: number; y: number }>
  Matrix!: Matrix2D
  transform!: Matrix2D
  style!: ShapeStyle
  pen!: Ln
  polylineRender!: PolylineRender
  constructor(handler: IEditorHandler, theme: Theme, master, layout?, slide?) {
    const s = turnOffRecordChanges()
    this.handler = handler
    this.points = []
    this.Matrix = new Matrix2D()
    this.transform = new Matrix2D()

    const style = ShapeStyle.Default()
    style.fillRef!.color.updateBy(theme, slide, layout, master, {
      r: 0,
      g: 0,
      b: 0,
      a: 255,
    })
    let rgba = style.fillRef!.color.rgba
    const pen = theme.getLnStyle(style.lnRef!.idx, style.lnRef!.color)
    style.lnRef!.color.updateBy(theme, slide, layout, master)
    rgba = style.lnRef!.color.rgba
    this.style = style

    if (pen.fillEffects) {
      pen.fillEffects.calculate(theme, slide, layout, master, rgba)
    }

    this.pen = pen
    this.polylineRender = new PolylineRender(this)
    restoreRecordChangesState(s)
  }

  draw(g: EditLayerDrawer) {
    this.polylineRender.render(g)
  }

  getBounds() {
    const checker = new GraphicsBoundsChecker()
    this.polylineRender.checkerBounds(checker)
    const bounds = checker.bounds

    return bounds
  }

  getShape(container: Slide) {
    let xMax = this.points[0].x,
      yMax = this.points[0].y,
      xMin = xMax,
      yMin = yMax
    let i

    const isClosed = this.isClosed(container)
    const _n = isClosed ? this.points.length - 1 : this.points.length
    for (i = 1; i < _n; ++i) {
      if (this.points[i].x > xMax) {
        xMax = this.points[i].x
      }
      if (this.points[i].y > yMax) {
        yMax = this.points[i].y
      }

      if (this.points[i].x < xMin) {
        xMin = this.points[i].x
      }

      if (this.points[i].y < yMin) {
        yMin = this.points[i].y
      }
    }

    const shape = new Shape()

    shape.setSpPr(new SpPr())
    shape.spPr!.setParent(shape)
    shape.spPr!.setXfrm(new Xfrm(shape.spPr))
    shape.spPr!.xfrm!.setParent(shape.spPr)
    shape.spPr!.xfrm!.setOffX(xMin)
    shape.spPr!.xfrm!.setOffY(yMin)
    shape.spPr!.xfrm!.setExtX(xMax - xMin)
    shape.spPr!.xfrm!.setExtY(yMax - yMin)
    shape.setStyle(ShapeStyle.Default())
    const geometry = new Geometry()

    const [w, h] = [xMax - xMin, yMax - yMin]
    let kw, kh, pathW, pathH
    if (w > 0) {
      pathW = 43200
      kw = 43200 / w
    } else {
      pathW = 0
      kw = 0
    }
    if (h > 0) {
      pathH = 43200
      kh = 43200 / h
    } else {
      pathH = 0
      kh = 0
    }
    addPathOperationToGeom(
      geometry,
      PathAddCmd.create,
      undefined,
      isClosed ? 'norm' : 'none',
      undefined,
      pathW,
      pathH
    )
    setRectToGeom(geometry, 'l', 't', 'r', 'b')
    addPathOperationToGeom(
      geometry,
      PathAddCmd.moveTo,
      (((this.points[0].x - xMin) * kw) >> 0) + '',
      (((this.points[0].y - yMin) * kh) >> 0) + ''
    )
    for (i = 1; i < _n; ++i) {
      addPathOperationToGeom(
        geometry,
        PathAddCmd.lineTo,
        (((this.points[i].x - xMin) * kw) >> 0) + '',
        (((this.points[i].y - yMin) * kh) >> 0) + ''
      )
    }
    if (isClosed) {
      addPathOperationToGeom(geometry, PathAddCmd.close)
    }

    shape.spPr!.setGeometry(geometry)
    setDeletedForSp(shape, false)
    calculateForShape(shape)
    shape.x = xMin
    shape.y = yMin
    return shape
  }

  isClosed(container: Nullable<Slide | Notes>) {
    const threshold = container ? convertPxToMM(3) : mmsPerPt(3)

    let isClosed = false
    if (this.points.length > 2) {
      const dx = this.points[0].x - this.points[this.points.length - 1].x
      const dy = this.points[0].y - this.points[this.points.length - 1].y
      if (Math.sqrt(dx * dx + dy * dy) < threshold) {
        isClosed = true
      }
    }

    return isClosed
  }
}

class PolylineRender {
  polyline: PolyLine
  pen: Ln
  transform: Matrix2D
  matrix: Matrix2D
  constructor(polyline: PolyLine) {
    this.polyline = polyline
    this.pen = polyline.pen
    this.transform = polyline.transform
    this.matrix = polyline.Matrix
  }
  render(graphics: EditLayerDrawer) {
    toggleCxtTransformReset(graphics, false)
    graphics.applyTransformMatrix(this.matrix)
    const renderingInfo: ShapeRenderingInfo = {
      pen: this.pen,
      matrix: this.transform,
    }
    drawShapeByInfoWithDrawer(renderingInfo, this, graphics)
  }

  checkerBounds(checker: GraphicsBoundsChecker) {
    toggleCxtTransformReset(checker, false)
    checker.applyTransformMatrix(this.matrix)
    if (this.polyline.points.length < 2) {
      return
    }
    checker._moveTo(this.polyline.points[0].x, this.polyline.points[0].y)
    for (let i = 1; i < this.polyline.points.length; ++i) {
      checker._lineTo(this.polyline.points[i].x, this.polyline.points[i].y)
    }
    if (checker.needCheckLineWidth) {
      checker.correctBoundsWithSx()
    }
  }
  draw(state: ShapeRenderingState, g: EditLayerDrawer) {
    g._end()
    if (this.polyline.points.length < 2) {
      return
    }
    g._moveTo(this.polyline.points[0].x, this.polyline.points[0].y)
    for (let i = 1; i < this.polyline.points.length; ++i) {
      g._lineTo(this.polyline.points[i].x, this.polyline.points[i].y)
    }
    state.isRect = false
    drawShapeStroke(state, g)
  }
}
