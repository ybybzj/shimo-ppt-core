import { Nullable } from '../../../../liber/pervasive'
import { PointerEventInfo } from '../../../common/dom/events'
import { THRESHOLD_DELTA } from '../../../core/common/const/unit'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { startEditAction } from '../../../core/calculation/informChanges/startAction'
import { calculateForShape } from '../../../core/calculation/calculate/shape'
import { SlideLayout } from '../../../core/Slide/SlideLayout'
import { SlideMaster } from '../../../core/Slide/SlideMaster'
import { getThemeForEditContainer } from '../../../core/utilities/presentation/getters'
import { getParentsOfElement } from '../../../core/utilities/shape/getters'
import { addToParentSpTree } from '../../../core/utilities/shape/updates'
import { syncInterfaceState } from '../../../editor/interfaceState/syncInterfaceState'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { PresetShapeTypes } from '../../../exports/graphic'
import { InstanceType } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { AddGeometryControl, isPresetOfConnector } from './addGeometryControl'
import { getExtents } from './utils'
import { renderConnectors, findConnectorInfoForSp } from '../connector/utils'
import { getAllSingularSps } from '../common/utils'
import {
  handleSelectionStartForSp,
  handleSelectionEndForSp,
} from '../rectSelect/utils'

/** 规则形状 */
export class AddGeometryState {
  readonly instanceType = InstanceType.AddGeometryState
  handler: IEditorHandler
  preset: PresetShapeTypes
  isStart = false
  isMoved = false
  startX: Nullable<number>
  startY: Nullable<number>
  connectTarget: null | SlideElement = null
  control!: AddGeometryControl

  options?: {
    vertType: number
  }
  constructor(handler: IEditorHandler, preset: PresetShapeTypes, options?) {
    this.handler = handler
    this.preset = preset
    this.options = options
    this.startX = null
    this.startY = null
  }

  onStart(_e, x: number, y: number) {
    this.startX = x
    this.startY = y
    if (!this.handler.editFocusContainer?.cSld) return

    const parents = getParentsOfElement(this.handler.editFocusContainer)
    const layout = parents.layout
    const master = parents.master
    const slide = parents.slide
    this.control = new AddGeometryControl(
      this.preset,
      x,
      y,
      getThemeForEditContainer(this.handler.presentation),
      master as SlideMaster,
      layout as SlideLayout,
      slide,
      this.handler
    )
    this.isStart = true
  }

  onMove(e: PointerEventInfo, x: number, y: number) {
    const handler = this.handler
    // 如果新增的是 connector 则需要绘制触点
    if (isPresetOfConnector(this.preset)) {
      const sps: SlideElement[] = []
      getAllSingularSps(handler.getCurrentSpTree(), sps)
      let target: SlideElement | null = null
      for (let i = sps.length - 1; i > -1; --i) {
        const sp = sps[i]
        const connectorInfo = findConnectorInfoForSp(
          sp,
          x,
          y,
          handler.editorUI.isInTouchMode()
        )
        if (connectorInfo) {
          target = sp
          if (
            sp.instanceType !== InstanceType.Shape ||
            !sp.isConnectionShape()
          ) {
            break
          }
        }
      }

      if (this.connectTarget !== target) {
        this.connectTarget = target
        handler.updateEditLayer()
      }
    }
    if (this.isStart && e.isLocked) {
      const absDeltaX = Math.abs(this.startX! - x)
      const absDeltaY = Math.abs(this.startY! - y)
      if (
        !this.isMoved &&
        (absDeltaX > THRESHOLD_DELTA || absDeltaY > THRESHOLD_DELTA)
      ) {
        this.isMoved = true
      }
      this.control.track(e, x, y)
      handler.updateEditLayer()
    }
  }

  onEnd(e: PointerEventInfo, x, y) {
    if (!this.isStart || EditorSettings.isViewMode) return
    const _this = this
    const handler = this.handler
    const control = this.control
    const isActuallyMoved = this.isMoved
    if (!this.isMoved && this instanceof AddGeometryState) {
      const { extX, extY } = getExtents(this.preset)
      const mouseEvtHandler = new PointerEventInfo()
      mouseEvtHandler.lock()
      this.onMove(
        mouseEvtHandler,
        this.startX ?? 0 + extX,
        this.startY ?? 0 + extY
      )
    }
    const callback = (isLock) => {
      if (isLock) {
        startEditAction(EditActionFlag.action_Slide_AddNewShape)
        const container = handler.editFocusContainer
        const shape = control.getShape(container!, {
          isActuallyMoved,
          ...this.options,
        })

        if (!(container && container.cSld)) {
          if (shape.spPr!.xfrm!.offX! < 0) {
            shape.spPr!.xfrm!.setOffX(0)
          }
          if (shape.spPr!.xfrm!.offY! < 0) {
            shape.spPr!.xfrm!.setOffY(0)
          }
        }

        if (container && container.cSld) {
          shape.setParent(handler.editFocusContainer)
          shape.resetCalcStatus()
        }
        addToParentSpTree(shape)
        handler.selectionState.reset()
        handler.selectionState.select(shape)
        if (_this.preset === 'textRect') {
          handler.selectionState.setTextSelection(shape)
          calculateForShape(shape)
          handleSelectionStartForSp(shape, handler, x, y, e)
          handleSelectionEndForSp(shape, handler, x, y, e)
        }
        handler.startCalculate()
        syncInterfaceState(handler.editorUI.editorKit)
      }
    }

    callback(true)
    this.handler.updateEditLayer()
    this.handler.editorUI.editorKit.emitEndAddShape()
    this.handler.changeToState(InstanceType.UIInitState, this.handler)
  }

  drawControl(editLayerDrawer: EditLayerDrawer) {
    // draw new shape
    if (this.control) {
      this.control.draw(editLayerDrawer)
    }

    // draw connector contact
    if (this.connectTarget) {
      const { renderingController, presentation } = this.handler.editorUI
      const { left, top, right, bottom } = renderingController.curSlideRect
      const editLayer = renderingController.editLayer
      const { width, height } = presentation
      editLayerDrawer.init(editLayer, left, top, right, bottom, width, height)

      renderConnectors(this.connectTarget, editLayerDrawer)
      editLayerDrawer.resetLayerBounds()
    }
  }
}
