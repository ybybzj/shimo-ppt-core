import { PresetShapeTypes } from '../../../exports/graphic'
import { InstanceType } from '../../../core/instanceTypes'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { ShapeTypes, SHAPE_EXT_ASPECTS, SHAPE_EXT_X } from './const'

function startNewShapeControl(
  handler: IEditorHandler,
  presetGeom: PresetShapeTypes,
  options?: { vertType?: number }
) {
  if (handler.presentation.isNotesFocused === true) {
    handler.presentation.isNotesFocused = false
    handler.updateEditLayer()
  }
  switch (presetGeom) {
    // 曲线，bezier
    case ShapeTypes['Lines']['Spline']: {
      handler.changeToState(InstanceType.BezierState, handler)
      break
    }
    // 自由曲线
    case ShapeTypes['Lines']['Polyline1']: {
      handler.changeToState(InstanceType.PolyLineState, handler)
      break
    }
    // 任意形状
    case ShapeTypes['Lines']['Polyline2']: {
      handler.changeToState(InstanceType.PolyShapeState, handler)
      break
    }
    // 常规形状
    default: {
      handler.changeToState(
        InstanceType.AddGeometryState,
        handler,
        presetGeom,
        options
      )
      break
    }
  }
}

export function startAddShape(
  handler: IEditorHandler,
  preset: PresetShapeTypes,
  isApply,
  options?: { vertType?: number }
) {
  const { presentation } = handler
  if (presentation.slides[presentation.currentSlideIndex]) {
    if (!(isApply === false)) {
      presentation.beforeStartAddShape()
      startNewShapeControl(handler, preset, options)
    } else {
      handler.changeToState(InstanceType.UIInitState, handler)
      handler.updateEditLayer()
      handler.editorUI.editorKit.emitEndAddShape()
    }
  }
}

export function getExtents(preset: string): {
  extX: number
  extY: number
} {
  let [extX, extY] = [25.4, 25.4]
  if (typeof SHAPE_EXT_X[preset] === 'number') {
    extX = SHAPE_EXT_X[preset]
  }
  if (typeof SHAPE_EXT_ASPECTS[preset] === 'number') {
    const aspect = SHAPE_EXT_ASPECTS[preset]
    extY = extX / aspect
  }
  return { extX, extY }
}
