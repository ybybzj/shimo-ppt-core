import { PointerEventInfo } from '../../../common/dom/events'
import { getThemeForEditContainer } from '../../../core/utilities/presentation/getters'
import { InstanceType } from '../../../core/instanceTypes'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import {
  Spline,
  MoveToCommand,
  LineToCommand,
  BezierCommand,
  SplineCommandType,
} from './addBezierControl'
import { AddGeometryState } from './addGeometry'

// --------------------- (贝塞尔)曲线  --------------------- //

/** 曲线 handle 第一次 click，确定起点 */
export class BezierState {
  readonly instanceType = InstanceType.BezierState
  handler: IEditorHandler
  control!: Spline

  constructor(handler: IEditorHandler) {
    this.handler = handler
  }

  onStart(_e, x, y) {
    const theme = getThemeForEditContainer(this.handler.presentation)
    if (theme == null) return false
    this.control = new Spline(this.handler, theme)
    this.control.path.push(new MoveToCommand(x, y))
    this.handler.changeToState(
      InstanceType.BezierClickState,
      this.handler,
      x,
      y,
      this.control
    )

    this.handler.selectionState.reset()
    this.handler.updateEditLayer()
    return true
  }

  onMove(_e, _X, _Y) {}

  onEnd(_e, _X, _Y) {
    this.handler.editorUI.editorKit.emitEndAddShape()
    this.handler.changeToState(InstanceType.UIInitState, this.handler)
  }

  drawControl(editLayerDrawer: EditLayerDrawer) {
    if (this.control) this.control.draw(editLayerDrawer)
  }
}

export class BezierClickState {
  readonly instanceType = InstanceType.BezierClickState
  handler: IEditorHandler
  startX: number
  startY: number
  control: Spline

  constructor(handler, startX, startY, control: Spline) {
    this.handler = handler
    this.startX = startX
    this.startY = startY
    this.control = control
  }

  onStart(_e, _x, _y) {
    return false
  }

  onMove(_e, x, y) {
    if (this.startX === x && this.startY === y) {
      return
    }

    const tr_x = x
    const tr_y = y
    this.control.path.push(new LineToCommand(tr_x, tr_y))
    this.handler.changeToState(
      InstanceType.QuadraticBezierState,
      this.handler,
      this.control
    )

    this.handler.updateEditLayer()
  }

  onEnd(_e, _x, _y) {}

  drawControl(editLayerDrawer: EditLayerDrawer) {
    if (this.control) this.control.draw(editLayerDrawer)
  }
}

/** 曲线 handle 第二次 click，确定第二个控制点(直线)/结束点 */
export class QuadraticBezierState {
  readonly instanceType = InstanceType.QuadraticBezierState
  handler: IEditorHandler
  isStart!: boolean
  control: Spline

  constructor(handler: IEditorHandler, control: Spline) {
    this.handler = handler
    this.control = control
  }

  onStart(e: PointerEventInfo, x, y) {
    if (e.clicks >= 2) {
      this.isStart = true
      AddGeometryState.prototype.onEnd.call(this, e, x, y)
      return true
    }
    return false
  }

  onMove(_e, x, y) {
    const paths = this.control.path
    const secondPath = paths[1] as LineToCommand
    secondPath.changeFinalPoint(x, y)
    this.handler.updateEditLayer()
  }

  onEnd(e: PointerEventInfo, x, y) {
    if (e.clicks < 2) {
      this.handler.changeToState(
        InstanceType.QuadraticBezierClickState,
        this.handler,
        x,
        y,
        this.control
      )
    }
  }

  drawControl(editLayerDrawer: EditLayerDrawer) {
    if (this.control) this.control.draw(editLayerDrawer)
  }
}

/** 曲线第三次 click 的预处理， 将第二个 lineTo path 改为 bezier/关闭直线 */
export class QuadraticBezierClickState {
  readonly instanceType = InstanceType.QuadraticBezierClickState
  handler: IEditorHandler
  startX: number
  startY: number
  isStart!: boolean
  control: Spline

  constructor(handler, startX, startY, control: Spline) {
    this.handler = handler
    this.startX = startX
    this.startY = startY
    this.control = control
  }

  onStart(e: PointerEventInfo, x, y) {
    if (e.clicks >= 2) {
      this.isStart = true
      AddGeometryState.prototype.onEnd.call(this, e, x, y)
      return true
    }
    return false
  }

  onMove(_e, x, y) {
    if (x === this.startX && y === this.startY) {
      return
    }

    const splineControl = this.control

    // 修改之前的 path 为 bezier 曲线
    const firstPath = splineControl.path[0] as MoveToCommand
    const secondPath = splineControl.path[1] as LineToCommand
    const { x: x0, y: y0 } = firstPath
    const { x: x3, y: y3 } = secondPath
    const [x6, y6] = [x, y]

    const vx = (x6 - x0) / 6
    const vy = (y6 - y0) / 6

    const x2 = x3 - vx
    const y2 = y3 - vy

    const x4 = x3 + vx
    const y4 = y3 + vy

    const x1 = (x0 + x2) * 0.5
    const y1 = (y0 + y2) * 0.5

    const x5 = (x4 + x6) * 0.5
    const y5 = (y4 + y6) * 0.5

    splineControl.path.length = 1
    splineControl.path.push(new BezierCommand(x1, y1, x2, y2, x3, y3))
    splineControl.path.push(new BezierCommand(x4, y4, x5, y5, x6, y6))
    this.handler.updateEditLayer()
    this.handler.changeToState(
      InstanceType.CubicBezierState,
      this.handler,
      this.control
    )
  }

  onEnd(e: PointerEventInfo, x, y) {
    if (e.clicks >= 2) {
      this.isStart = true
      AddGeometryState.prototype.onEnd.call(this, e, x, y)
    }
  }

  drawControl(editLayerDrawer: EditLayerDrawer) {
    if (this.control) this.control.draw(editLayerDrawer)
  }
}

/** 曲线 handle (>= 3)次 click，确定第 bezier 控制点/结束点 */
export class CubicBezierState {
  readonly instanceType = InstanceType.CubicBezierState
  handler: IEditorHandler
  isStart!: boolean
  control: Spline

  constructor(handler: IEditorHandler, control: Spline) {
    this.handler = handler
    this.control = control
  }

  onStart(e: PointerEventInfo, x, y) {
    if (e.clicks >= 2) {
      this.isStart = true
      AddGeometryState.prototype.onEnd.call(this, e, x, y)
      return true
    }
    return false
  }

  onMove(_e, x, y) {
    const paths = this.control.path
    const finalCommand = paths[paths.length - 1] as BezierCommand
    const secondToLastCommand = paths[paths.length - 2] as BezierCommand
    const thirdToLastCommand = paths[paths.length - 3] as
      | BezierCommand
      | MoveToCommand

    let x0, y0
    if (thirdToLastCommand.id === SplineCommandType.moveTo) {
      ;[x0, y0] = [thirdToLastCommand.x, thirdToLastCommand.y]
    } else {
      ;[x0, y0] = [thirdToLastCommand.x3, thirdToLastCommand.y3]
    }

    const [x3, y3] = [secondToLastCommand.x3, secondToLastCommand.y3]
    const [x6, y6] = [x, y]

    const vx = (x6 - x0) / 6
    const vy = (y6 - y0) / 6

    const [x2, y2] = [x3 - vx, y3 - vy]
    const [x4, y4] = [x3 + vx, y3 + vy]

    const x5 = (x4 + x6) * 0.5
    const y5 = (y4 + y6) * 0.5

    if (thirdToLastCommand.id === SplineCommandType.moveTo) {
      secondToLastCommand.x1 = (x0 + x2) * 0.5
      secondToLastCommand.y1 = (y0 + y2) * 0.5
    }

    secondToLastCommand.x2 = x2
    secondToLastCommand.y2 = y2
    secondToLastCommand.x3 = x3
    secondToLastCommand.y3 = y3

    finalCommand.x1 = x4
    finalCommand.y1 = y4
    finalCommand.x2 = x5
    finalCommand.y2 = y5
    finalCommand.x3 = x6
    finalCommand.y3 = y6

    this.handler.updateEditLayer()
  }

  onEnd(e: PointerEventInfo, x, y) {
    if (e.clicks < 2) {
      this.handler.changeToState(
        InstanceType.CubicBezierClickState,
        this.handler,
        x,
        y,
        this.control
      )
    }
  }

  drawControl(editLayerDrawer: EditLayerDrawer) {
    if (this.control) this.control.draw(editLayerDrawer)
  }
}

/** bezier 新增节点的预处理, 生成新的 commandBezier 占位 / 关闭曲线 */
export class CubicBezierClickState {
  readonly instanceType = InstanceType.CubicBezierClickState
  handler: IEditorHandler
  startX: number
  startY: number
  isStart!: boolean
  control: Spline

  constructor(handler, startX, startY, control: Spline) {
    this.handler = handler
    this.startX = startX
    this.startY = startY
    this.control = control
  }

  onStart(e: PointerEventInfo, x, y) {
    if (e.clicks >= 2) {
      this.isStart = true
      AddGeometryState.prototype.onEnd.call(this, e, x, y)

      return true
    }
    return false
  }

  onMove(_e, x, y) {
    if (x === this.startX && y === this.startY) {
      return
    }
    const paths = this.control.path
    const finalCommand = paths[paths.length - 1] as BezierCommand
    const secondToLastCommand = paths[paths.length - 2]

    let x0, y0
    if (secondToLastCommand.id === SplineCommandType.moveTo) {
      ;[x0, y0] = [secondToLastCommand.x, secondToLastCommand.y]
    } else {
      x0 = (secondToLastCommand as BezierCommand).x3
      y0 = (secondToLastCommand as BezierCommand).y3
    }

    const [x3, y3] = [finalCommand.x3, finalCommand.y3]
    const [x6, y6] = [x, y]

    const vx = (x6 - x0) / 6
    const vy = (y6 - y0) / 6

    const [x2, y2] = [x3 - vx, y3 - vy]
    const [x4, y4] = [x3 + vx, y3 + vy]

    const x1 = (x2 + x0) * 0.5
    const y1 = (y2 + y0) * 0.5

    const x5 = (x4 + x6) * 0.5
    const y5 = (y4 + y6) * 0.5

    if (secondToLastCommand.id === SplineCommandType.moveTo) {
      finalCommand.x1 = x1
      finalCommand.y1 = y1
    }
    finalCommand.x2 = x2
    finalCommand.y2 = y2

    paths.push(new BezierCommand(x4, y4, x5, y5, x6, y6))
    this.handler.updateEditLayer()
    this.handler.changeToState(
      InstanceType.CubicBezierState,
      this.handler,
      this.control
    )
  }

  onEnd(e: PointerEventInfo, x, y) {
    if (e.clicks >= 2) {
      this.isStart = true
      AddGeometryState.prototype.onEnd.call(this, e, x, y)
    }
  }

  drawControl(editLayerDrawer: EditLayerDrawer) {
    if (this.control) this.control.draw(editLayerDrawer)
  }
}
