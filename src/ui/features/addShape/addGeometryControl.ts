import { FillKIND } from '../../../core/common/const/attrs'
import { isNumber } from '../../../common/utils'
import { EditorSettings } from '../../../core/common/EditorSettings'
import {
  LineEndWidthTypes,
  LineEndType,
  TextOverflowType,
  TextWarpType,
  TextAutoFitType,
  TextVerticalType,
} from '../../../core/SlideElement/const'
import { createGeometry } from '../../../core/SlideElement/geometry/creators'
import {
  calcConnectionInfoByXY,
  calculateCxnSpPr,
  ConnectionShape,
} from '../../../core/SlideElement/ConnectionShape'

import { gLineStylePresets } from '../../../core/SlideElement/attrs/creators'
import { Matrix2D } from '../../../core/graphic/Matrix'
import { GraphicsBoundsChecker } from '../../../core/graphic/Bounds'
import { ADD_SHAPE_MIN_WIDTH, SHAPE_EXT_ASPECTS } from './const'
import { EditLayerRenderElement } from '../common/EditLayerRenderElement'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { IEditorHandler } from '../../rendering/editorHandler/type'
import { checkExtentsByTextContent } from '../../../core/utilities/shape/extents'
import {
  calcTransformFormProps,
  calcXYPointsByTransform,
  TransformProps,
} from '../../../core/utilities/shape/calcs'
import { InstanceType } from '../../../core/instanceTypes'
import {
  resetTxBodyForShape,
  setDeletedForSp,
} from '../../../core/utilities/shape/updates'
import { BodyPr } from '../../../core/SlideElement/attrs/bodyPr'

import { FillEffects } from '../../../core/SlideElement/attrs/fill/fill'
import { Ln, LnOptions } from '../../../core/SlideElement/attrs/line'
import {
  SpPr,
  Xfrm,
  UniNvPr,
  CNvUniSpPr,
} from '../../../core/SlideElement/attrs/shapePrs'
import { TextFit } from '../../../core/SlideElement/attrs/TextFit'
import { Theme } from '../../../core/SlideElement/attrs/theme'
import { calculateShapeContent } from '../../../core/calculation/calculate/shapeContent'
import { Shape } from '../../../core/SlideElement/Shape'
import { PresetShapeTypes } from '../../../exports/graphic'
import { Nullable } from '../../../../liber/pervasive'
import { SlideMaster } from '../../../core/Slide/SlideMaster'
import { SlideLayout } from '../../../core/Slide/SlideLayout'
import { Slide } from '../../../core/Slide/Slide'
import { DefaultShapeAttributes } from '../../../core/SlideElement/attrs/DefaultShapeAttributes'
import { ShapeStyle } from '../../../core/SlideElement/attrs/ShapeStyle'
import { Notes } from '../../../core/Slide/Notes'
import { PointerEventInfo } from '../../../common/dom/events'
import { SlideElement } from '../../../core/SlideElement/type'
import { Geometry } from '../../../core/SlideElement/geometry/Geometry'
import { ConnectorInfo } from '../../../core/SlideElement/ConnectorInfo'
import { checkBoundsByXyPoints, getAllSingularSps } from '../common/utils'
import {
  renderConnectors,
  connectorHitShape,
  findConnectorInfoForSp,
} from '../connector/utils'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../../core/common/utils'
import { ThemeColor } from '../../../io/dataType/spAttrs'
import { ParaPr } from '../../../core/textAttributes/ParaPr'

export const MIN_SIZE = ADD_SHAPE_MIN_WIDTH
const HALF_SHAPE_MIN_SIZE = MIN_SIZE / 2.0

export class AddGeometryControl {
  readonly instanceType = InstanceType.AddGeometryControl
  preset: PresetShapeTypes
  startX: number
  startY: number
  x!: number
  y!: number
  extX!: number
  extY!: number
  rot = 0
  numberOfArrows = 0
  transform: Matrix2D
  theme?: Nullable<Theme>
  handler?: IEditorHandler
  isConnector = false
  startCxnParams: null | ConnectorInfo = null
  shapeDrawConnectors: Nullable<SlideElement>
  recentSpPr: Nullable<SpPr>
  startSp: Nullable<SlideElement>
  endSp: Nullable<SlideElement>
  endCxnParams: null | ConnectorInfo = null
  geomtry!: Geometry
  isLine!: boolean
  layerRenderElement!: EditLayerRenderElement
  flipH!: boolean
  flipV!: boolean

  constructor(
    preset: PresetShapeTypes,
    startX: number,
    startY: number,
    theme: Nullable<Theme>,
    master: Nullable<SlideMaster>,
    layout: Nullable<SlideLayout>,
    slide: Nullable<Slide>,
    handler?: IEditorHandler
  ) {
    this.preset = preset
    this.startX = startX
    this.startY = startY
    this.transform = new Matrix2D()
    this.theme = theme
    this.handler = handler

    const s = turnOffRecordChanges()
    if (handler) {
      this.isConnector = isPresetOfConnector(preset)
      if (this.isConnector) {
        const spArr: SlideElement[] = []
        getAllSingularSps(handler.getCurrentSpTree(), spArr)
        for (let i = spArr.length - 1; i > -1; --i) {
          const cxnParams = findConnectorInfoForSp(
            spArr[i],
            startX,
            startY,
            handler.editorUI.isInTouchMode()
          )
          if (cxnParams) {
            this.startCxnParams = cxnParams
            this.startX = cxnParams.x
            this.startY = cxnParams.y
            this.startSp = spArr[i]
            break
          }
        }
      }
    }

    let style: ShapeStyle
    const isTextRect = preset === 'textRect'
    ;[this.preset, this.numberOfArrows] = getPresetAndArrowByGeom(preset)
    this.isLine = this.preset === 'line'

    const spDef = theme?.spDef
    if (!isTextRect) {
      if (!this.isConnector && spDef && spDef.style) {
        style = spDef.style.clone()
      } else {
        style = ShapeStyle.Default(this.preset)
      }
    } else {
      style = ShapeStyle.TextRectDefault()
    }
    let brush: FillEffects = theme?.getFillStyle(style.fillRef?.idx, undefined)!
    style.fillRef?.color.updateBy(theme, slide, layout, master, {
      r: 0,
      g: 0,
      b: 0,
      a: 255,
    })
    let rgba = style.fillRef?.color.rgba
    if (style.fillRef?.color && !style.fillRef?.color.isEmpty()) {
      if (brush.fill && brush.fill.type === FillKIND.SOLID) {
        brush.fill.color = style.fillRef.color.clone()
      }
    }
    let pen = theme?.getLnStyle(style.lnRef?.idx, style.lnRef?.color)!
    style.lnRef?.color.updateBy(theme, slide, layout, master)
    rgba = style.lnRef?.color.rgba

    if (isTextRect) {
      ;[pen, brush] = checkTextRectPenBrush(pen, brush, slide)
    }
    checkArrowPen(pen, this.numberOfArrows)

    const isLineLike = gLineStylePresets[this.preset]
    if (!this.isConnector && !isLineLike && !isTextRect) {
      checkShapeDefPenBrush(spDef, pen, brush)
    }

    this.geomtry = createGeometry(isTextRect ? 'rect' : this.preset)
    if (pen.fillEffects) {
      pen.fillEffects.calculate(theme, slide, layout, master, rgba)
    }
    brush.calculate(theme, slide, layout, master, rgba)

    this.layerRenderElement = new EditLayerRenderElement(
      this.geomtry,
      5,
      5,
      brush,
      pen,
      this.transform
    )
    restoreRecordChangesState(s)
  }

  track(e: PointerEventInfo, x: number, y: number) {
    let isConnectorHandled = false
    this.shapeDrawConnectors = null
    this.recentSpPr = null

    this.endSp = null
    this.endCxnParams = null

    if (EditorSettings.isSupportCxnShape && this.isConnector && this.handler) {
      const sps: SlideElement[] = []
      getAllSingularSps(this.handler.getCurrentSpTree(), sps)
      let cxnParams: ConnectorInfo | null = null
      let endCxnParams: ConnectorInfo | null = null
      for (let i = sps.length - 1; i > -1; --i) {
        const sp = sps[i]
        cxnParams = findConnectorInfoForSp(
          sps[i],
          x,
          y,
          this.handler.editorUI.isInTouchMode()
        )
        if (cxnParams) {
          endCxnParams = cxnParams
          this.shapeDrawConnectors = sp
          this.endSp = sp
          this.endCxnParams = endCxnParams
          if (
            sp.instanceType !== InstanceType.Shape ||
            !sp.isConnectionShape()
          ) {
            break
          }
        }
      }

      if (this.startCxnParams || endCxnParams) {
        let startParams = this.startCxnParams
        let endParams = endCxnParams
        if (!startParams) {
          startParams = calcConnectionInfoByXY(
            endParams!,
            this.startX,
            this.startY
          )
        } else if (!endParams) {
          endParams = calcConnectionInfoByXY(startParams, x, y)
        }
        const spPr = calculateCxnSpPr(
          startParams,
          endParams!,
          this.preset,
          this.layerRenderElement.pen?.w
        )
        this.flipH = spPr.xfrm!.flipH === true
        this.flipV = spPr.xfrm!.flipV === true
        this.rot = isNumber(spPr.xfrm!.rot) ? spPr.xfrm!.rot : 0
        this.extX = spPr.xfrm!.extX!
        this.extY = spPr.xfrm!.extY!
        this.x = spPr.xfrm!.offX!
        this.y = spPr.xfrm!.offY!
        this.layerRenderElement = new EditLayerRenderElement(
          spPr.geometry!,
          5,
          5,
          this.layerRenderElement.brush,
          this.layerRenderElement.pen,
          this.transform
        )
        isConnectorHandled = true
        this.recentSpPr = spPr
      }
      if (!this.shapeDrawConnectors) {
        for (let i = sps.length - 1; i > -1; --i) {
          const sp = connectorHitShape(sps[i], x, y)
          if (sp) {
            endCxnParams = cxnParams
            this.shapeDrawConnectors = sp
            break
          }
        }
      }
      if (false === isConnectorHandled) {
        this.layerRenderElement = new EditLayerRenderElement(
          this.geomtry,
          5,
          5,
          this.layerRenderElement.brush,
          this.layerRenderElement.pen,
          this.transform
        )
      }
    }

    if (false === isConnectorHandled) {
      const dx = x - this.startX
      const absDx = Math.abs(dx)
      const dy = y - this.startY
      const absDy = Math.abs(dy)
      this.flipH = false
      this.flipV = false
      this.rot = 0

      if (this.isLine || this.isConnector) {
        if (x < this.startX) {
          this.flipH = true
        }
        if (y < this.startY) {
          this.flipV = true
        }
      }
      if (
        !(e.ctrlKey || e.shiftKey) ||
        (e.ctrlKey && !e.shiftKey && this.isLine)
      ) {
        this.extX = absDx >= MIN_SIZE ? absDx : this.isLine ? 0 : MIN_SIZE
        this.extY = absDy >= MIN_SIZE ? absDy : this.isLine ? 0 : MIN_SIZE
        if (dx >= 0) {
          this.x = this.startX
        } else {
          this.x = absDx >= MIN_SIZE ? x : this.startX - this.extX
        }

        if (dy >= 0) {
          this.y = this.startY
        } else {
          this.y = absDy >= MIN_SIZE ? y : this.startY - this.extY
        }
      } else if (e.ctrlKey && !e.shiftKey) {
        if (absDx >= HALF_SHAPE_MIN_SIZE) {
          this.x = this.startX - absDx
          this.extX = 2 * absDx
        } else {
          this.x = this.startX - HALF_SHAPE_MIN_SIZE
          this.extX = MIN_SIZE
        }

        if (absDy >= HALF_SHAPE_MIN_SIZE) {
          this.y = this.startY - absDy
          this.extY = 2 * absDy
        } else {
          this.y = this.startY - HALF_SHAPE_MIN_SIZE
          this.extY = MIN_SIZE
        }
      } else if (!e.ctrlKey && e.shiftKey) {
        let newWidth, newHeight
        const aspect = isNumber(SHAPE_EXT_ASPECTS[this.preset])
          ? SHAPE_EXT_ASPECTS[this.preset]
          : 1

        if (absDy === 0) {
          newWidth = absDx > MIN_SIZE ? absDx : MIN_SIZE
          newHeight = absDx / aspect
        } else {
          if (absDx / absDy >= aspect) {
            newWidth = absDx
            newHeight = absDx / aspect
          } else {
            newHeight = absDy
            newWidth = absDy * aspect
          }
        }

        if (newWidth < MIN_SIZE || newHeight < MIN_SIZE) {
          const aspect = newWidth / newHeight
          if (newHeight < MIN_SIZE && newWidth < MIN_SIZE) {
            if (newHeight < newWidth) {
              newHeight = MIN_SIZE
              newWidth = newHeight * aspect
            } else {
              newWidth = MIN_SIZE
              newHeight = newWidth / aspect
            }
          } else if (newHeight < MIN_SIZE) {
            newHeight = MIN_SIZE
            newWidth = newHeight * aspect
          } else {
            newWidth = MIN_SIZE
            newHeight = newWidth / aspect
          }
        }
        this.extX = newWidth
        this.extY = newHeight
        if (dx >= 0) this.x = this.startX
        else this.x = this.startX - this.extX

        if (dy >= 0) this.y = this.startY
        else this.y = this.startY - this.extY

        if (this.isLine) {
          const angle = Math.atan2(dy, dx)
          if (
            (angle >= 0 && angle <= Math.PI / 8) ||
            (angle <= 0 && angle >= -Math.PI / 8) ||
            (angle >= (7 * Math.PI) / 8 && angle <= Math.PI)
          ) {
            this.extY = 0
            this.y = this.startY
          } else if (
            (angle >= (3 * Math.PI) / 8 && angle <= (5 * Math.PI) / 8) ||
            (angle <= (-3 * Math.PI) / 8 && angle >= (-5 * Math.PI) / 8)
          ) {
            this.extX = 0
            this.x = this.startX
          }
        }
      } else {
        let newWidth, newHeight
        const aspcet = isNumber(SHAPE_EXT_ASPECTS[this.preset])
          ? SHAPE_EXT_ASPECTS[this.preset]
          : 1
        if (absDy === 0) {
          newWidth = absDx > HALF_SHAPE_MIN_SIZE ? absDx * 2 : MIN_SIZE
          newHeight = newWidth / aspcet
        } else {
          if (absDx / absDy >= aspcet) {
            newWidth = absDx * 2
            newHeight = newWidth / aspcet
          } else {
            newHeight = absDy * 2
            newWidth = newHeight * aspcet
          }
        }

        if (newWidth < MIN_SIZE || newHeight < MIN_SIZE) {
          const aspect = newWidth / newHeight
          if (newHeight < MIN_SIZE && newWidth < MIN_SIZE) {
            if (newHeight < newWidth) {
              newHeight = MIN_SIZE
              newWidth = newHeight * aspect
            } else {
              newWidth = MIN_SIZE
              newHeight = newWidth / aspect
            }
          } else if (newHeight < MIN_SIZE) {
            newHeight = MIN_SIZE
            newWidth = newHeight * aspect
          } else {
            newWidth = MIN_SIZE
            newHeight = newWidth / aspect
          }
        }
        this.extX = newWidth
        this.extY = newHeight
        this.x = this.startX - this.extX * 0.5
        this.y = this.startY - this.extY * 0.5
      }
    }

    this.layerRenderElement.updateExt(this.extX, this.extY)
    const calcProps: TransformProps = {
      pos: { x: this.x, y: this.y },
      size: { w: this.extX, h: this.extY },
      flipH: this.flipH,
      flipV: this.flipV,
      rot: this.rot,
    }
    calcTransformFormProps(this.transform, calcProps)
    this.layerRenderElement.setTransform(this.transform)
  }

  draw(editLayerDrawer: EditLayerDrawer | GraphicsBoundsChecker) {
    if (this.shapeDrawConnectors) {
      renderConnectors(this.shapeDrawConnectors, editLayerDrawer)
    }
    if (editLayerDrawer.instanceType === InstanceType.EditLayerDrawer) {
      this.layerRenderElement.draw(editLayerDrawer)
    } else {
      this.layerRenderElement.checkBounds(editLayerDrawer)
    }
  }

  getShape(
    container: Slide | Notes,
    options: {
      isActuallyMoved?: boolean
      vertType?: number
    } = {}
  ) {
    // 非线段形状设置最小大小
    if (!this.isLine) {
      this.extX = Math.max(this.extX, ADD_SHAPE_MIN_WIDTH)
      this.extY = Math.max(this.extY, ADD_SHAPE_MIN_WIDTH)
    }

    let spPr: SpPr, shape: ConnectionShape | Shape
    if (this.isConnector) {
      shape = new ConnectionShape()
      shape.spTreeParent = container.cSld

      if (this.recentSpPr) {
        spPr = this.recentSpPr.clone()
      } else {
        spPr = new SpPr()
        spPr.setXfrm(new Xfrm(spPr))
        const xfrm = spPr.xfrm!
        xfrm.setParent(spPr)
        const x = this.x
        const y = this.y
        xfrm.setOffX(x)
        xfrm.setOffY(y)
        xfrm.setExtX(this.extX)
        xfrm.setExtY(this.extY)
        xfrm.setFlipH(this.flipH)
        xfrm.setFlipV(this.flipV)
      }

      shape.setSpPr(spPr)
      spPr.setParent(shape)

      const nvUniPr = new UniNvPr(shape)
      nvUniPr.cNvPr.setId(container.cSld.genNewMaxId())
      shape.setNvSpPr(nvUniPr)

      const nvUniSpPr = new CNvUniSpPr()
      if (this.startSp && this.startCxnParams) {
        nvUniSpPr.stCxnIdx = this.startCxnParams.idx
        nvUniSpPr.stCxnId = this.startSp.id
      }
      if (this.endSp && this.endCxnParams) {
        nvUniSpPr.endCxnIdx = this.endCxnParams.idx
        nvUniSpPr.endCxnId = this.endSp.id
      }
      shape.nvSpPr!.setUniSpPr(nvUniSpPr)
    } else {
      shape = new Shape()
      shape.spTreeParent = container.cSld

      shape.setSpPr(new SpPr())
      shape.spPr!.setParent(shape)
      const x = this.x
      const y = this.y
      shape.spPr!.setXfrm(new Xfrm(shape.spPr))
      const xfrm = shape.spPr!.xfrm!
      xfrm.setParent(shape.spPr)
      xfrm.setOffX(x)
      xfrm.setOffY(y)
      xfrm.setExtX(this.extX)
      xfrm.setExtY(this.extY)
      xfrm.setFlipH(this.flipH)
      xfrm.setFlipV(this.flipV)
    }

    setDeletedForSp(shape, false)

    if (this.preset === 'textRect') {
      shape.spPr!.setGeometry(createGeometry('rect'))
      if (!container || !container.cSld) {
        shape.setStyle(ShapeStyle.TextRectDefault())
        const { ln, fill } = createTextRectPenBrush()
        shape.spPr!.setFill(fill)
        shape.spPr!.setLn(ln)
      } else {
        const fill = FillEffects.NoFill()
        shape.spPr!.setFill(fill)
      }

      const bodyPr = BodyPr.Default()

      let isNeedCheckExtents = false
      if (container) {
        if (!container.cSld) {
          bodyPr.vertOverflow = TextOverflowType.Clip
        } else {
          bodyPr.textFit = TextFit.new(TextAutoFitType.SHAPE)
          bodyPr.wrap = TextWarpType.SQUARE
          isNeedCheckExtents = true
        }
      }

      let paraPr: ParaPr | undefined
      const txDef = this.theme?.txDef
      if (txDef) {
        const { bodyPr: txDefBodyPr, lstStyle, spPr } = txDef
        if (txDefBodyPr) {
          bodyPr.merge(txDefBodyPr)
        }
        const defaultParaPr = lstStyle?.levels[9]
        if (defaultParaPr) {
          paraPr = defaultParaPr
        }

        if (spPr.fillEffects) {
          shape.spPr!.setFill(spPr.fillEffects.clone())
        }
        if (spPr.ln) {
          if (shape.spPr!.ln) {
            shape.spPr!.ln.merge(spPr.ln)
          } else {
            shape.spPr!.setLn(spPr.ln.clone())
          }
        }
      }
      const { isActuallyMoved, vertType } = options
      if (isActuallyMoved === false) {
        bodyPr.wrap = TextWarpType.NONE
      } else {
        bodyPr.wrap = TextWarpType.SQUARE
      }
      if (vertType === TextVerticalType.eaVert) {
        bodyPr.vert = vertType
      }

      resetTxBodyForShape(shape, bodyPr, paraPr)

      if (isNeedCheckExtents) {
        const oldOffX = shape.spPr!.xfrm!.offX
        const oldOffY = shape.spPr!.xfrm!.offY
        shape.setParent(container)
        calculateShapeContent(shape)
        checkExtentsByTextContent(shape, true)
        shape.spPr!.xfrm!.setOffX(oldOffX)
        shape.spPr!.xfrm!.setOffY(oldOffY)
      }
    } else {
      if (!shape.spPr!.geometry) {
        shape.spPr!.setGeometry(createGeometry(this.preset))
      }
      shape.setStyle(ShapeStyle.Default(this.preset))
      if (this.numberOfArrows > 0) {
        const opts: LnOptions = {
          tailEnd: { type: LineEndType.Arrow, len: LineEndWidthTypes.Mid },
        }

        if (this.numberOfArrows === 2) {
          opts.headEnd = {
            type: LineEndType.Arrow,
            len: LineEndWidthTypes.Mid,
          }
        }
        const ln = Ln.new(opts)
        shape.spPr!.setLn(ln as Ln)
      }
      const spDef = this.theme && this.theme.spDef
      // TODO: support line default
      if (!this.isConnector && !gLineStylePresets[this.preset] && spDef) {
        if (spDef.style) {
          shape.setStyle(spDef.style.clone())
        }
        if (spDef.spPr) {
          if (spDef.spPr.fillEffects) {
            shape.spPr!.setFill(spDef.spPr.fillEffects.clone())
          }
          if (spDef.spPr.ln) {
            if (shape.spPr!.ln) {
              shape.spPr!.ln.merge(spDef.spPr.ln)
            } else {
              shape.spPr!.setLn(spDef.spPr.ln.clone())
            }
          }
        }
      }
    }
    shape.x = this.x
    shape.y = this.y
    return shape
  }

  getBounds() {
    const checker = new GraphicsBoundsChecker()
    this.draw(checker)
    const tr = this.transform
    const { xArr, yArr } = calcXYPointsByTransform(tr, this.extX, this.extY)
    checkBoundsByXyPoints(checker, xArr, yArr)

    return checker.bounds
  }
}

function getPresetAndArrowByGeom(presetGeom: PresetShapeTypes) {
  let preset = presetGeom
  let arrowCount = 0
  const l = presetGeom.length
  const arrowPreset = 'WithArrow'
  const arrowPreset2 = 'WithTwoArrows'
  if (presetGeom.indexOf(arrowPreset) > -1) {
    preset = presetGeom.substring(0, l - arrowPreset.length) as any
    arrowCount = 1
  }
  if (presetGeom.indexOf(arrowPreset2) > -1) {
    preset = presetGeom.substring(0, l - arrowPreset2.length) as any
    arrowCount = 2
  }
  return [preset, arrowCount] as const
}

function checkTextRectPenBrush(
  pen: Ln,
  brush: FillEffects,
  slide: Nullable<Slide>
) {
  let checkedPen: Ln
  let checkedBrush: FillEffects
  if (slide) {
    checkedPen = Ln.NoFill()
    checkedBrush = FillEffects.NoFill()
  } else {
    const { ln, fill } = createTextRectPenBrush()
    pen.merge(ln)
    brush.merge(fill)
    checkedPen = pen.clone()
    checkedBrush = brush.clone()
  }
  return [checkedPen, checkedBrush] as const
}

const createTextRectPenBrush = () => {
  const ln = Ln.new({
    w: 6350,
    fillEffects: FillEffects.SolidPrst('black'),
  })

  const fill = FillEffects.SolidScheme(ThemeColor.Light1)

  return { ln, fill }
}

function checkArrowPen(pen: Ln, countOfArrows: number) {
  const opts: LnOptions = {}
  if (countOfArrows > 0) {
    opts.tailEnd = {
      type: LineEndType.Arrow,
      len: LineEndWidthTypes.Mid,
    }

    if (countOfArrows === 2) {
      opts.headEnd = {
        type: LineEndType.Arrow,
        len: LineEndWidthTypes.Mid,
      }
    }

    pen.fromOptions(opts)
  }
}

function checkShapeDefPenBrush(
  spDef: Nullable<DefaultShapeAttributes>,
  pen: Ln,
  brush: FillEffects
) {
  if (spDef && spDef.spPr) {
    if (spDef.spPr.fillEffects) {
      brush.merge(spDef.spPr.fillEffects)
    }

    if (spDef.spPr.ln) {
      pen.merge(spDef.spPr.ln)
    }
  }
}

export function isPresetOfConnector(presetStr) {
  if (typeof presetStr === 'string' && presetStr.length > 0) {
    if (
      presetStr === 'flowChartOffpageConnector' ||
      presetStr === 'flowChartConnector' ||
      presetStr === 'flowChartOnlineStorage'
    ) {
      return false
    }
    return (
      presetStr.toLowerCase().indexOf('line') > -1 ||
      presetStr.toLowerCase().indexOf('connector') > -1
    )
  }
  return false
}
