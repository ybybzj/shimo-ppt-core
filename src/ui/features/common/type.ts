import { GraphicsBoundsChecker } from '../../../core/graphic/Bounds'
import { Matrix2D } from '../../../core/graphic/Matrix'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { AddGeometryControl } from '../addShape/addGeometryControl'
import {
  XYAdjustmentControl,
  PolarAdjustmentControl,
} from '../adjust/adjustmentControls'
import { MoveSingleControl, MoveGroupControl } from '../move/moveControls'
import { PolyLine } from '../addShape/addPolylineControl'
import {
  ResizeSingleControl,
  ResizeGroupControl,
} from '../resize/resizeControls'
import {
  RotateGroupControl,
  RotateSingleControl,
} from '../rotate/rotateControls'
import { Spline } from '../addShape/addBezierControl'

export type ShapeEditControl =
  | AddGeometryControl
  | XYAdjustmentControl
  | PolarAdjustmentControl
  | MoveSingleControl
  | MoveGroupControl
  | ResizeSingleControl
  | ResizeGroupControl
  | RotateSingleControl
  | RotateGroupControl
  | Spline
  | PolyLine

export type DrawControlTrackFunc = (
  control: MoveSingleControl | ResizeSingleControl,
  editLayer: EditLayerDrawer | GraphicsBoundsChecker,
  transform?: Matrix2D
) => void
