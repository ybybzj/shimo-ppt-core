import { Nullable } from '../../../../liber/pervasive'
import { GraphicsBoundsChecker } from '../../../core/graphic/Bounds'
import { InstanceType } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'

export function getAllSingularSps(
  spArr: Nullable<SlideElement[]>,
  ret: SlideElement[]
) {
  if (spArr == null) return
  for (let i = 0; i < spArr.length; ++i) {
    const sp = spArr[i]
    if (sp.instanceType === InstanceType.GroupShape) {
      getAllSingularSps(sp.spTree, ret)
    } else {
      ret.push(sp)
    }
  }
  // 优先处理非连接线
  ret.sort((sp1, sp2) => {
    const isSp1Cxp =
      sp1.instanceType === InstanceType.Shape && sp1.isConnectionShape()
    const isSp2Cxp =
      sp2.instanceType === InstanceType.Shape && sp2.isConnectionShape()
    return isSp1Cxp && !isSp2Cxp ? -1 : !isSp1Cxp && isSp2Cxp ? 1 : 0
  })
}

export function drawControlsOnEditLayer(
  handler: IEditorHandler,
  editLayerDrawer: EditLayerDrawer
) {
  const curState = handler.curState
  // common drawer
  if (curState.controls) {
    curState.controls.forEach((control) => {
      control.draw(editLayerDrawer, undefined)
    })
  }
  // 各 state 定制的 drawer
  if (curState.drawControl) {
    curState.drawControl(editLayerDrawer)
  }
}

export function checkBoundsByXyPoints(
  checker: GraphicsBoundsChecker,
  xArr: number[],
  yArr: number[]
) {
  // 这里 minX 是可能比 maxX 小的, 所以需要先 push 进去最后一起比较
  xArr.push(checker.bounds.minX)
  xArr.push(checker.bounds.maxX)
  yArr.push(checker.bounds.minY)
  yArr.push(checker.bounds.maxY)

  checker.bounds.minX = Math.min(...xArr)
  checker.bounds.maxX = Math.max(...xArr)
  checker.bounds.minY = Math.min(...yArr)
  checker.bounds.maxY = Math.max(...yArr)
}
