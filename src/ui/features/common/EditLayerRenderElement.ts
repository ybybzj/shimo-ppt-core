import { Nullable } from '../../../../liber/pervasive'
import { FillKIND } from '../../../core/common/const/attrs'
import { Matrix2D } from '../../../core/graphic/Matrix'

import { toggleCxtTransformReset } from '../../../core/graphic/utils'
import { Geometry } from '../../../core/SlideElement/geometry/Geometry'
import { checkBounds_Geometry } from '../../../core/checkBounds/slideElement'
import { drawShapeByInfo } from '../../../core/render/drawShape/drawShape'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { FillEffects } from '../../../core/SlideElement/attrs/fill/fill'
import { Ln } from '../../../core/SlideElement/attrs/line'
import { GraphicsBoundsChecker } from '../../../core/graphic/Bounds'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../../core/common/utils'
import { calculateGeometry } from '../../../core/calculation/calculate/geometry'

export class EditLayerRenderElement {
  geometry!: Geometry
  private _extX!: number
  private _extY!: number
  brush: Nullable<FillEffects>
  pen: Nullable<Ln>
  transform!: Matrix2D
  get extX() {
    return this._extX
  }
  get extY() {
    return this._extY
  }

  constructor(
    geometry: Geometry,
    extX: number,
    extY: number,
    brush: Nullable<FillEffects>,
    pen: Nullable<Ln>,
    transform: Matrix2D
  ) {
    this.init(geometry, extX, extY, brush, pen, transform)
  }

  init(
    geometry: Geometry,
    extX: number,
    extY: number,
    brush: Nullable<FillEffects>,
    pen: Nullable<Ln>,
    transform: Matrix2D
  ) {
    this.geometry = geometry
    this._extX = extX
    this._extY = extY

    let tmpBrush: Nullable<FillEffects>, tmpPen: Nullable<Ln>

    // 没有填充/边框的形状, 默认黑色填充以便识别
    if (!brush?.fill || brush.fill.type === FillKIND.NOFILL) {
      tmpBrush = createBrush()
    } else {
      tmpBrush = brush
    }

    if (
      !pen ||
      pen.w === 0 ||
      !pen?.fillEffects?.fill ||
      pen.fillEffects.fill.type === FillKIND.NOFILL
    ) {
      tmpPen = createPen()
    } else {
      tmpPen = pen
    }

    this.brush = tmpBrush
    this.pen = tmpPen
    this.transform = transform
  }

  updateTransform(extX: number, extY: number, transform: Matrix2D) {
    this._extX = extX
    this._extY = extY
    this.transform = transform
  }

  updateExt(extX: number, extY: number) {
    this._extX = extX
    this._extY = extY
    this.geometry && calculateGeometry(this.geometry, extX, extY)
  }

  setTransform(transform: Matrix2D) {
    this.transform = transform
  }

  draw(editLayerDrawer: EditLayerDrawer, transform?: Matrix2D) {
    const orgTransform = this.transform
    if (transform) {
      this.setTransform(transform)
    }
    if (this.isRenderGeometry()) {
      editLayerDrawer.saveState()
      toggleCxtTransformReset(editLayerDrawer, false)
      editLayerDrawer.applyTransformMatrix(this.transform)
      drawShapeByInfo(this, editLayerDrawer, this.geometry)
      editLayerDrawer.restoreState()
    } else {
      editLayerDrawer.saveState()
      toggleCxtTransformReset(editLayerDrawer, false)
      editLayerDrawer.applyTransformMatrix(this.transform)
      editLayerDrawer._begin()
      editLayerDrawer._moveTo(0, 0)
      editLayerDrawer._lineTo(this._extX, 0)
      editLayerDrawer._lineTo(this._extX, this._extY)
      editLayerDrawer._lineTo(0, this._extY)
      editLayerDrawer._close()
      editLayerDrawer.setPenColor(0, 0, 0, 160)
      editLayerDrawer.setPenWidth(500)
      editLayerDrawer.drawStroke()
      editLayerDrawer.setBrushColor(255, 255, 255, 128)
      editLayerDrawer.drawFill()
      editLayerDrawer._end()
      editLayerDrawer.restoreState()

      if (editLayerDrawer.editLayer) editLayerDrawer.editLayer.resetAll = true
    }
    if (transform) {
      this.setTransform(orgTransform)
    }
  }

  isRenderGeometry() {
    return (
      this.geometry &&
      ((this.pen?.fillEffects?.fill &&
        this.pen.fillEffects.fill.type !== FillKIND.NOFILL) ||
        (this.brush?.fill && this.brush.fill.type !== FillKIND.NOFILL))
    )
  }

  checkBounds(checker: GraphicsBoundsChecker) {
    checker.applyTransformMatrix(this.transform)
    if (this.geometry) {
      checkBounds_Geometry(this.geometry, checker)
    } else {
      checker.rect(0, 0, this._extX, this._extY)
    }
    checker.reset()
  }
}

export function createBrush(): FillEffects {
  const s = turnOffRecordChanges()
  const brush = FillEffects.SolidRGBA(255, 255, 255, 255)

  restoreRecordChangesState(s)
  return brush as FillEffects
}

export function createPen(): Ln {
  const s = turnOffRecordChanges()

  const pen = Ln.new({
    fillEffects: FillEffects.SolidRGBA(0, 0, 0),
  })

  restoreRecordChangesState(s)
  return pen
}
