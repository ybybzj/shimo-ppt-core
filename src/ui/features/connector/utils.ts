import { EditorUtil } from '../../../globals/editor'
import {
  collectCxnShapes,
  getFinalFlipH,
  getFinalFlipV,
  getFinalRotate,
  getTopMostGroupShape,
} from '../../../core/utilities/shape/getters'
import { InstanceType } from '../../../core/instanceTypes'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { ConnectorControl } from './connectorControl'
import { ShapeEditControl } from '../common/type'
import { MoveGroupControl, MoveSingleControl } from '../move/moveControls'
import {
  PolarAdjustmentControl,
  XYAdjustmentControl,
} from '../adjust/adjustmentControls'
import {
  ResizeGroupControl,
  ResizeSingleControl,
} from '../resize/resizeControls'
import {
  RotateGroupControl,
  RotateSingleControl,
} from '../rotate/rotateControls'
import { SlideElement } from '../../../core/SlideElement/type'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { GraphicsBoundsChecker } from '../../../core/graphic/Bounds'
import { getGeometryForSp } from '../../../core/utilities/shape/geometry'
import { Dict, Nullable } from '../../../../liber/pervasive'
import { hitInnerArea, hitPath, hitTextRect } from '../../states/hittests'
import { Geometry } from '../../../core/SlideElement/geometry/Geometry'
import { Matrix2D } from '../../../core/graphic/Matrix'
import { isNumber } from '../../../common/utils'
import { normalizeRotValue } from '../../../core/common/utils'
import {
  ConnectorInfo,
  convertToConnectorInfo,
} from '../../../core/SlideElement/ConnectorInfo'
import { convertPxToMM } from '../../../core/utilities/helpers'
import { Shape } from '../../../core/SlideElement/Shape'
import { ConnectionShape } from '../../../core/SlideElement/ConnectionShape'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { adjustXfrmOfGroup } from '../../../core/utilities/shape/group'
import { collectCxnsFromSlideElements } from '../../../core/utilities/presentation/getters'
import {
  CONTROL_CIRCLE_RADIUS,
  TOUCH_CONTROL_CIRCLE_RADIUS,
} from '../../../core/common/const/control'

function needCheckConnectorContrl(control: ShapeEditControl): boolean {
  switch (control.instanceType) {
    case InstanceType.MoveSingleControl:
    case InstanceType.XYAdjustmentControl:
    case InstanceType.PolarAdjustmentControl:
    case InstanceType.ResizeSingleControl:
    case InstanceType.RotateSingleControl:
      return true
    default:
      return false
  }
}

// Todo: 未支持 group 内部的连接线 follow control
export type CxnParentControls =
  | MoveSingleControl
  // | MoveGroupControl
  | XYAdjustmentControl
  | PolarAdjustmentControl
  | ResizeSingleControl
  // | ResizeGroupControl
  | RotateSingleControl
  // | RotateGroupControl
  | ConnectorControl

/** 根据 parentControls 生成跟随的 connectorControl */
export function checkConnectorControl(
  handler: IEditorHandler,
  controls: ShapeEditControl[]
) {
  if (controls.length > 0 && needCheckConnectorContrl(controls[0])) {
    const connectors = collectCxnShapes(handler.getCurrentSpTree())
    const connectorControls: ConnectorControl[] = []

    connectors.forEach((connector) => {
      const stId = connector.getStCxnId()
      const endId = connector.getEndCxnId()
      let startControl: null | CxnParentControls = null
      let endControl: null | CxnParentControls = null
      let startSp: Nullable<SlideElement> = null
      let endSp: Nullable<SlideElement> = null
      if (stId != null || endId != null) {
        for (let i = 0; i < controls.length; ++i) {
          const control = controls[i] as CxnParentControls
          if (control.target === connector) {
            endControl = null
            startControl = null
            break
          }
          const preSp = control.target
          if (preSp.id === stId) {
            startControl = control
          }
          if (preSp.id === endId) {
            endControl = control
          }
        }
      }
      if (startControl || endControl) {
        if (startControl) {
          startSp = startControl.target
        } else {
          if (stId != null) {
            startSp = EditorUtil.entityRegistry.getEntityById<Shape>(stId)
            if (startSp && startSp.isDeleted) {
              startSp = null
            }
          }
        }
        if (endControl) {
          endSp = endControl.target
        } else if (endId != null) {
          endSp = EditorUtil.entityRegistry.getEntityById<Shape>(endId)
          if (endSp && endSp.isDeleted) {
            endSp = null
          }
        }
        connectorControls.push(
          new ConnectorControl(
            connector,
            startControl,
            endControl,
            startSp,
            endSp
          )
        )
      }
    })

    let oldLength = 0
    const loopCheck = (cxnControls: ConnectorControl[]) => {
      if (cxnControls.length > 0 && cxnControls.length > oldLength) {
        oldLength = cxnControls.length
        checkConnectorControl(handler, cxnControls)
      }
    }

    loopCheck(connectorControls)

    for (let i = 0; i < connectorControls.length; ++i) {
      controls.push(connectorControls[i])
    }
  }
}

export function renderConnectors(
  sp: SlideElement,
  editLayerDrawer: EditLayerDrawer | GraphicsBoundsChecker
) {
  const geom = getGeometryForSp(sp)
  drawConnectorsFromGeom(geom, editLayerDrawer, sp.transform)
}

function drawConnectorsFromGeom(
  geom: Geometry,
  editLayerDrawer: EditLayerDrawer | GraphicsBoundsChecker,
  transform: Matrix2D
) {
  if (editLayerDrawer.instanceType !== InstanceType.EditLayerDrawer) {
    return
  }
  const oldAlpha = editLayerDrawer.editLayer.context.globalAlpha
  editLayerDrawer.editLayer.context.globalAlpha = 1

  for (let i = 0; i < geom.cxnLst.length; i++) {
    editLayerDrawer.drawTouchPoints(
      [{ x: geom.cxnLst[i].x, y: geom.cxnLst[i].y }],
      transform
    )
  }

  if (isNumber(oldAlpha)) {
    editLayerDrawer.editLayer.context.globalAlpha = oldAlpha
  }
}

export function connectorHitShape(
  sp: SlideElement,
  x: number,
  y: number
): Nullable<SlideElement> {
  if (sp.instanceType === InstanceType.GroupShape) {
    for (let i = sp.spTree.length - 1; i > -1; --i) {
      const hitSp = connectorHitShape(sp.spTree[i], x, y)
      if (hitSp) {
        return hitSp
      }
    }
    return null
  } else {
    if (_hit(sp, x, y)) {
      return sp
    }
    return null
  }
}

function _hit(sp: SlideElement, x: number, y: number): boolean {
  // 这里暂时不针对触屏扩大范围
  if (hitInnerArea(sp, x, y) || hitPath(sp, x, y, false)) {
    return true
  }
  if (sp.instanceType === InstanceType.Shape) {
    return hitTextRect(sp, x, y)
  } else {
    return false
  }
}

export function findConnectorTargets(connector: ConnectionShape) {
  // 实际上这里类型不会是 Table
  const startSp = EditorUtil.entityRegistry.getEntityById(
    connector.getStCxnId()
  ) as Nullable<SlideElement>
  const endSp = EditorUtil.entityRegistry.getEntityById(
    connector.getEndCxnId()
  ) as Nullable<SlideElement>
  return { startSp, endSp }
}

/** 获取 controls 中 targets 需要关联计算的 connectors */
export function collectCxnSpsFromControls(
  handler: IEditorHandler,
  controls: Array<
    | CxnParentControls
    | ResizeGroupControl
    | RotateGroupControl
    | MoveGroupControl
  >,
  needToResetCxnSps?: [ConnectionShape, boolean, boolean][],
  isMove = false
) {
  const targets: SlideElement[] = []
  const targetDict: Dict = {}
  for (let i = 0; i < controls.length; ++i) {
    const tartgetSp = controls[i].target as SlideElement | undefined
    if (!tartgetSp) continue
    targets.push(tartgetSp)
    targetDict[tartgetSp.getId()] = true

    if (tartgetSp.instanceType === InstanceType.GroupShape) {
      for (let j = 0; j < tartgetSp.slideElementsInGroup.length; ++j) {
        targetDict[tartgetSp.slideElementsInGroup[j].getId()] = true
      }
    }
  }
  const allConnectors = collectCxnsFromSlideElements(
    handler.presentation,
    targets,
    []
  )

  const selectedElements = handler.presentation.selectionState.selectedElements
  const connectors: ConnectionShape[] = []
  for (let i = 0; i < allConnectors.length; ++i) {
    const connector = allConnectors[i]
    const { startSp, endSp } = findConnectorTargets(connector)
    const needResetStart = !!(startSp && !targetDict[startSp.getId()])
    const needResetEnd = !!(endSp && !targetDict[endSp.getId()])
    const isUnSelectedCxn = !targetDict[connector.getId()]
    if (isMove) {
      if (isUnSelectedCxn) connectors.push(connector)
      if (
        needToResetCxnSps != null &&
        selectedElements.includes(connector) &&
        (needResetStart || needResetEnd)
      ) {
        needToResetCxnSps.push([connector, needResetStart, needResetEnd])
      }
    } else {
      connectors.push(connector)
    }
  }
  return connectors
}

/** 操作结束后，关联 connectors 的位置大小需要重新计算 */
export function calculateConnectorsAfterControl(connectors: ConnectionShape[]) {
  const groupMaps: Dict<GroupShape> = {}
  connectors.forEach((connector) => {
    connector.calcTransform()
    const topMostGroup = getTopMostGroupShape(connector)
    if (topMostGroup) {
      groupMaps[topMostGroup.id] = topMostGroup
    }
  })

  for (const id in groupMaps) {
    if (groupMaps.hasOwnProperty(id)) {
      adjustXfrmOfGroup(groupMaps[id])
    }
  }
}

export function findConnectorInfoForSp(
  sp: SlideElement,
  x: number,
  y: number,
  isTouch: boolean
): ConnectorInfo | null {
  if (sp.instanceType === InstanceType.GroupShape) {
    for (let i = sp.spTree.length - 1; i > -1; --i) {
      const conInfo = findConnectorInfoForSp(sp.spTree[i], x, y, isTouch)
      if (conInfo) {
        return conInfo
      }
    }
    return null
  } else {
    const connGeom = getGeomCxnInfo(sp, x, y, isTouch)
    if (connGeom) {
      let rot = sp.rot
      let flipH = sp.flipH
      let flipV = sp.flipV
      if (sp.group) {
        rot = normalizeRotValue(getFinalRotate(sp.group) + rot)
        if (getFinalFlipH(sp.group)) {
          flipH = !flipH
        }
        if (getFinalFlipV(sp.group)) {
          flipV = !flipV
        }
      }
      return convertToConnectorInfo(
        rot,
        flipH,
        flipV,
        sp.transform,
        sp.bounds,
        connGeom
      )
    }
    return null
  }
}

function getGeomCxnInfo(
  sp: SlideElement,
  x: number,
  y: number,
  isTouch: boolean
) {
  const geom = getGeometryForSp(sp)
  const invertTransform = sp.invertTransform
  const _x = invertTransform.XFromPoint(x, y)
  const _y = invertTransform.YFromPoint(x, y)

  const pxRadius = isTouch ? TOUCH_CONTROL_CIRCLE_RADIUS : CONTROL_CIRCLE_RADIUS
  const distance = convertPxToMM(pxRadius)

  let dx, dy
  for (let i = 0; i < geom.cxnLst.length; i++) {
    dx = _x - geom.cxnLst[i].x
    dy = _y - geom.cxnLst[i].y

    if (Math.sqrt(dx * dx + dy * dy) < distance) {
      return {
        idx: i,
        ang: geom.cxnLst[i].ang!,
        x: geom.cxnLst[i].x,
        y: geom.cxnLst[i].y,
      }
    }
  }
  return null
}
