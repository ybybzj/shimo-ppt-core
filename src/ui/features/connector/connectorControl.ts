import {
  normalizeRotValue,
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../../core/common/utils'
import { InstanceType } from '../../../core/instanceTypes'
import { MatrixUtils } from '../../../core/graphic/Matrix'
import { XYAdjustmentControl } from '../adjust/adjustmentControls'
import {
  calcConnectionInfoByXY,
  calculateCxnSpPr,
  ConnectionShape,
} from '../../../core/SlideElement/ConnectionShape'
import { Bounds, GraphicBounds } from '../../../core/graphic/Bounds'
import {
  getConnectionInfo,
  calcTransformFormProps,
  TransformProps,
} from '../../../core/utilities/shape/calcs'
import {
  getFinalFlipH,
  getFinalFlipV,
  getFinalRotate,
} from '../../../core/utilities/shape/getters'
import { Nullable } from '../../../../liber/pervasive'
import { CxnParentControls } from './utils'
import { SlideElement } from '../../../core/SlideElement/type'
import { SpPr } from '../../../core/SlideElement/attrs/shapePrs'
import { convertToConnectorInfo } from '../../../core/SlideElement/ConnectorInfo'
import { calculateGeometry } from '../../../core/calculation/calculate/geometry'

/** 操作其它对象影响到附着的连接线时即通过 ConnectorControl 控制(Cxn 只会与最细粒度的 sp 相连, 不会与 group 相连) */
export class ConnectorControl extends XYAdjustmentControl {
  readonly subInstanceType = InstanceType.ConnectorControl
  target: ConnectionShape
  startControl: Nullable<CxnParentControls>
  endControl: Nullable<CxnParentControls>
  startSp: Nullable<SlideElement>
  endSp: Nullable<SlideElement>
  startX: number
  startY: number
  endX: number
  endY: number
  spPr: SpPr
  constructor(
    connector: ConnectionShape,
    startControl: Nullable<CxnParentControls>,
    endControl: Nullable<CxnParentControls>,
    startSp: Nullable<SlideElement>,
    endSp: Nullable<SlideElement>
  ) {
    super(connector, -1)
    this.target = connector
    this.startControl = startControl
    this.endControl = endControl
    this.startSp = startSp
    this.endSp = endSp

    const { transform, extX, extY } = connector
    this.startX = transform.XFromPoint(0, 0)
    this.startY = transform.YFromPoint(0, 0)
    this.endX = transform.XFromPoint(extX, extY)
    this.endY = transform.YFromPoint(extX, extY)

    const s = turnOffRecordChanges()
    this.spPr = connector.spPr!.clone()

    restoreRecordChangesState(s)
  }

  track() {
    const s = turnOffRecordChanges()
    const ret = this._track()
    restoreRecordChangesState(s)
    return ret
  }

  _track() {
    const target = this.target
    const [startControl, endControl] = [this.startControl, this.endControl]
    const nvUniSpPr = target.nvSpPr!.nvUniSpPr

    let startConnectInfo = this.getConnectInfoFromParentControl(startControl)
    let endConnectInfo = this.getConnectInfoFromParentControl(endControl, false)

    if (startConnectInfo || endConnectInfo) {
      let isMoveInGroup = false

      if (!startConnectInfo) {
        if (this.startSp && nvUniSpPr.stCxnIdx !== null) {
          startConnectInfo = getConnectionInfo(this.startSp, nvUniSpPr.stCxnIdx)
        } else {
          if (endControl?.instanceType === InstanceType.MoveSingleControl) {
            const { recentXDiff, recentYDiff } = endControl
            let [dx, dy] = [recentXDiff, recentYDiff]
            if (target.group) {
              isMoveInGroup = true
              const invertTransform = target.group.invertTransform.clone()
              ;[invertTransform.tx, invertTransform.ty] = [0, 0]
              dx = invertTransform.XFromPoint(recentXDiff, recentYDiff)
              dy = invertTransform.YFromPoint(recentXDiff, recentYDiff)
            }
            this.spPr = target.spPr!.clone()
            this.spPr.xfrm!.offX += dx
            this.spPr.xfrm!.offY += dy
            this.geometry = this.spPr.geometry!
            this.layerRenderElement.geometry = this.geometry
            this.calcTransform()
            return
          }
        }
        if (!startConnectInfo) {
          startConnectInfo = calcConnectionInfoByXY(
            endConnectInfo!,
            this.startX,
            this.startY
          )
        }
      }

      if (!endConnectInfo) {
        if (this.endSp && nvUniSpPr.endCxnIdx !== null) {
          endConnectInfo = getConnectionInfo(this.endSp, nvUniSpPr.endCxnIdx)
        } else {
          if (startControl?.instanceType === InstanceType.MoveSingleControl) {
            const { recentXDiff, recentYDiff } = startControl
            let [dx, dy] = [recentXDiff, recentYDiff]
            if (target.group) {
              isMoveInGroup = true
              const invertTransform = target.group.invertTransform.clone()
              ;[invertTransform.tx, invertTransform.ty] = [0, 0]
              dx = invertTransform.XFromPoint(recentXDiff, recentYDiff)
              dy = invertTransform.YFromPoint(recentXDiff, recentYDiff)
            }
            this.spPr = target.spPr!.clone()
            const xfrm = this.spPr.xfrm!
            xfrm.offX += dx
            xfrm.offY += dy
            this.geometry = this.spPr.geometry!
            this.layerRenderElement.geometry = this.geometry

            if (!target.group || isMoveInGroup) {
              xfrm.setOffX(xfrm.offX)
              xfrm.setOffY(xfrm.offY)
              xfrm.setFlipH(xfrm.flipH)
              xfrm.setFlipV(xfrm.flipV)
              xfrm.setRot(xfrm.rot)
            } else {
              const _xc = xfrm.offX + xfrm.extX / 2.0
              const _yc = xfrm.offY + xfrm.extY / 2.0
              const xc = target.group.invertTransform.XFromPoint(_xc, _yc)
              const yc = target.group.invertTransform.YFromPoint(_xc, _yc)
              xfrm.setOffX(xc - xfrm.extX / 2.0)
              xfrm.setOffY(yc - xfrm.extY / 2.0)
              xfrm.setFlipH(
                getFinalFlipH(target.group) ? !xfrm.flipH : xfrm.flipH
              )
              xfrm.setFlipV(
                getFinalFlipV(target.group) ? !xfrm.flipV : xfrm.flipV
              )
              xfrm.setRot(
                normalizeRotValue(xfrm.rot - getFinalRotate(target.group))
              )
            }

            this.calcTransform()
            return
          }
        }
        if (!endConnectInfo) {
          endConnectInfo = calcConnectionInfoByXY(
            startConnectInfo,
            this.endX,
            this.endY
          )
        }
      }

      this.spPr = calculateCxnSpPr(
        startConnectInfo,
        endConnectInfo,
        target.spPr!.geometry?.preset,
        this.layerRenderElement.pen?.w
      )
      const xfrm = this.spPr.xfrm!
      if (!target.group) {
        xfrm.setOffX(xfrm.offX)
        xfrm.setOffY(xfrm.offY)
        xfrm.setFlipH(xfrm.flipH)
        xfrm.setFlipV(xfrm.flipV)
        xfrm.setRot(xfrm.rot)
      } else {
        const _xc = xfrm.offX + xfrm.extX / 2.0
        const _yc = xfrm.offY + xfrm.extY / 2.0
        const xc = target.group.invertTransform.XFromPoint(_xc, _yc)
        const yc = target.group.invertTransform.YFromPoint(_xc, _yc)
        xfrm.setOffX(xc - xfrm.extX / 2.0)
        xfrm.setOffY(yc - xfrm.extY / 2.0)
        xfrm.setFlipH(getFinalFlipH(target.group) ? !xfrm.flipH : xfrm.flipH)
        xfrm.setFlipV(getFinalFlipV(target.group) ? !xfrm.flipV : xfrm.flipV)
        xfrm.setRot(normalizeRotValue(xfrm.rot - getFinalRotate(target.group)))
      }

      this.geometry = this.spPr.geometry!
      this.layerRenderElement.geometry = this.geometry
      this.calcTransform()
    }
  }

  calcTransform() {
    const target = this.target
    const xfrm = this.spPr.xfrm!
    calculateGeometry(this.geometry, xfrm.extX, xfrm.extY)

    const calcProps: TransformProps = {
      pos: { x: xfrm.offX, y: xfrm.offY },
      size: { w: xfrm.extX, h: xfrm.extY },
      flipH: xfrm.flipH,
      flipV: xfrm.flipV,
      rot: xfrm.rot,
    }
    calcTransformFormProps(this.transform, calcProps)
    this.layerRenderElement.setTransform(this.transform)

    if (target.group) {
      MatrixUtils.multiplyMatrixes(this.transform, target.group.transform)
    }
  }

  trackEnd() {
    const targetXfrm = this.target.spPr!.xfrm!
    const sourceXfrm = this.spPr.xfrm!
    targetXfrm.setOffX(sourceXfrm.offX)
    targetXfrm.setOffY(sourceXfrm.offY)
    targetXfrm.setExtX(sourceXfrm.extX)
    targetXfrm.setExtY(sourceXfrm.extY)
    targetXfrm.setFlipH(sourceXfrm.flipH)
    targetXfrm.setFlipV(sourceXfrm.flipV)
    targetXfrm.setRot(sourceXfrm.rot)
    this.target.spPr!.setGeometry(this.spPr.geometry!.clone())
  }

  convertControlBounds(controlBounds: Bounds) {
    return new GraphicBounds(
      controlBounds.minX,
      controlBounds.minY,
      controlBounds.maxX,
      controlBounds.maxY
    )
  }

  getConnectInfoFromParentControl(
    control: Nullable<CxnParentControls>,
    startFlag = true
  ) {
    if (!control) return null
    const target = this.target
    const connectorInfo = target.nvSpPr!.nvUniSpPr

    const sp = control.target
    const bounds = this.convertControlBounds(control.getBounds())
    let [flipH, flipV, rot] = [sp.flipH, sp.flipV, sp.rot]

    if (control.instanceType === InstanceType.RotateSingleControl) {
      rot = control.rot
    }
    if (control.instanceType === InstanceType.ResizeSingleControl) {
      flipH = !!control.curFlipH
      flipV = !!control.curFlipV
    }

    if (sp.group) {
      rot = normalizeRotValue(getFinalRotate(sp.group) + rot)
      if (getFinalFlipH(sp.group)) {
        flipH = !flipH
      }
      if (getFinalFlipV(sp.group)) {
        flipV = !flipV
      }
    }
    const transform = control.layerRenderElement.transform
    const idx = startFlag ? connectorInfo.stCxnIdx : connectorInfo.endCxnIdx
    if (idx != null) {
      const cxnProps = control.layerRenderElement.geometry.cxnLst[idx]
      if (cxnProps != null) {
        const { ang, x, y } = cxnProps
        return convertToConnectorInfo(rot, flipH, flipV, transform, bounds, {
          idx: idx!,
          ang: ang!,
          x,
          y,
        })
      }
    }
  }
}
