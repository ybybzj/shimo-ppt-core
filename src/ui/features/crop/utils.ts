import { Nullable } from '../../../../liber/pervasive'
import { isNumber } from '../../../common/utils'
import { gDefaultColorMap } from '../../../core/color/clrMap'
import { EditorSettings } from '../../../core/common/EditorSettings'
import {
  checkAlmostEqual,
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../../core/common/utils'
import { GraphicsBoundsChecker, WithBounds } from '../../../core/graphic/Bounds'
import { Matrix2D, MatrixUtils } from '../../../core/graphic/Matrix'
import { toggleCxtTransformReset } from '../../../core/graphic/utils'
import { createGeometry } from '../../../core/SlideElement/geometry/creators'
import { buildImageShape } from '../../../core/utilities/shape/creators'
import { ImageShape } from '../../../core/SlideElement/Image'
import { Shape } from '../../../core/SlideElement/Shape'
import { convertPxToMM } from '../../../core/utilities/helpers'
import { isChartImage } from '../../../core/utilities/shape/asserts'
import {
  getBlipFill,
  getParentsOfElement,
} from '../../../core/utilities/shape/getters'
import {
  getInvertTransform,
  calculateGeometryForSp,
  calcTransform,
} from '../../../core/utilities/shape/calcs'
import { isVideoImage } from '../../../core/utilities/shape/image'
import {
  Evt_onCropSizeChange,
  Evt_onEndCropElement,
  Evt_onStartCropElement,
} from '../../../editor/events/EventNames'
import { PresetShapeTypes } from '../../../exports/graphic'
import { InstanceType } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import {
  CONTROL_CROP_CENTRAL_DOT_NEED_MIN_WIDTH,
  CONTROL_CROP_CORNER_SIZE,
  EditLayerDrawer,
} from '../../rendering/Drawer/EditLayerDrawer'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { MoveSingleControl } from '../move/moveControls'
import { ResizeSingleControl } from '../resize/resizeControls'
import { CropState } from './cropState'
import { getShapeCoordBound } from '../../../core/utilities/shape/draw'
import { drawShape } from '../../../core/render/drawShape/drawShape'
import { FillEffects } from '../../../core/SlideElement/attrs/fill/fill'
import { calculateForImageShape } from '../../../core/calculation/calculate/image'
import {
  DIRECTION_UNHIT,
  REL_DIRECTION,
  EditControlType,
} from '../../../core/common/const/ui'
import { ensureSpPrXfrm } from '../../../core/utilities/shape/attrs'
import { CalcStatus } from '../../../core/calculation/updateCalcStatus/calcStatus'
import { setTransformCalcStatus } from '../../../core/calculation/updateCalcStatus/utils'
import { adjustXfrmOfGroup } from '../../../core/utilities/shape/group'
import { RelativeRect } from '../../../core/SlideElement/attrs/fill/RelativeRect'
import { BlipFill } from '../../../core/SlideElement/attrs/fill/blipFill'
import { createPen } from '../common/EditLayerRenderElement'
import { SolidFill } from '../../../core/SlideElement/attrs/fill/SolidFill'
import {
  CONTROL_CIRCLE_RADIUS,
  TOUCH_CONTROL_CIRCLE_RADIUS,
} from '../../../core/common/const/control'

export function isValidCropImage(img: ImageShape) {
  return !isVideoImage(img) && !isChartImage(img)
}

export function getCropImage(handler: IEditorHandler): Nullable<ImageShape> {
  const selectedElements = handler.selectionState.selectedElements
  // 暂时关闭多选支持
  if (selectedElements.length !== 1) return
  const sp = selectedElements[0]
  if (sp.instanceType === InstanceType.ImageShape && isValidCropImage(sp)) {
    return sp
  }
}

export function startCrop(
  handler: IEditorHandler,
  img?: ImageShape,
  triggerUI = true
) {
  if (!EditorSettings.isSupportCrop || EditorSettings.isViewMode) return
  const sp = img
    ? isValidCropImage(img)
      ? img
      : getCropImage(handler)
    : getCropImage(handler)
  if (!sp) return
  ensureSrcRect(sp)
  ensureSpPrXfrm(sp)
  const cropBackGround = createCropBackGround(sp)
  if (!cropBackGround) return
  handler.changeToState(InstanceType.CropState, handler, sp, cropBackGround)
  // 注意这里必须要 CheckSlide
  if (triggerUI !== false) {
    handler.editorUI.triggerRender()
    handler.editorUI.editorKit.emitEvent(Evt_onStartCropElement, {
      width: sp.extX,
      height: sp.extY,
    })
  }
}

export function endCrop(handler: IEditorHandler) {
  handler.changeToState(InstanceType.UIInitState, handler)
  handler.editorUI.pointerEvtInfo.clicks = 0
  handler.editorUI.triggerRender()
  handler.editorUI.editorKit.emitEvent(Evt_onEndCropElement)
}

export function createCropBackGround(img: ImageShape): ImageShape | null {
  const s = turnOffRecordChanges()
  const originBoundsInfo = calcOriginBounds(img)
  if (!originBoundsInfo) return null
  const { srcId, transformedX, transformedY, extX, extY } = originBoundsInfo

  const bgImage = buildImageShape(
    { url: srcId },
    transformedX,
    transformedY,
    extX,
    extY
  )
  bgImage.parent = img.parent
  const bgImgXfrm = bgImage.spPr!.xfrm!
  bgImgXfrm.setRot(img.rot)
  bgImgXfrm.setFlipH(img.flipH)
  bgImgXfrm.setFlipV(img.flipV)
  setTransformCalcStatus(bgImage)
  calculateGeometryForSp(bgImage)
  bgImage.setCalcStatusOf(CalcStatus.ImageShape.Bounds)
  calculateForImageShape(bgImage)
  bgImage.invertTransform = MatrixUtils.invertMatrixFrom(
    bgImage.invertTransform,
    bgImage.transform
  )
  restoreRecordChangesState(s)
  return bgImage
}

/**
 * @param fit  以现在的srcRect为基准，<背景图>等比缩放，使裁剪框正好包裹住背景
 * @param fill 以现在的srcRect为基准，<背景图>等比缩放，使背景正好包裹住裁剪框
 * @param img 指定 img 而非选中的图片进行裁剪
 * @param triggerUI 是否 ui 触发否则无需发送事件/更新视图
 */
export function cropByOption(
  handler: IEditorHandler,
  option: 'fit' | 'fill',
  img?: ImageShape,
  triggerUI = true
) {
  checkCropState(handler, img, triggerUI)
  const curState = handler.curState as CropState
  const { originObject, cropBackGround } = curState
  const originBounds = getShapeCoordBound(originObject)
  const curW = originBounds.maxX - originBounds.minX
  const curH = originBounds.maxY - originBounds.minY
  const backGroundBounds = getShapeCoordBound(cropBackGround)
  const bgW = backGroundBounds.maxX - backGroundBounds.minX
  const bgH = backGroundBounds.maxY - backGroundBounds.minY
  const [scaleX, scaleY] = [curW / bgW, curH / bgH]
  const scale =
    option === 'fit' ? Math.min(scaleX, scaleY) : Math.max(scaleX, scaleY)
  const deltaX = (curW - bgW * scale) / 2
  const deltaY = (curH - bgH * scale) / 2

  const s = turnOffRecordChanges()
  const xfrm = cropBackGround.spPr?.xfrm!
  xfrm.setOffX(originObject.x + deltaX)
  xfrm.setOffY(originObject.y + deltaY)
  xfrm.setExtX(xfrm.extX! * scale)
  xfrm.setExtY(xfrm.extY! * scale)
  restoreRecordChangesState(s)

  calculateForImageShape(cropBackGround)
  calculateSpSrcRect(originObject, cropBackGround)
  if (triggerUI === false) {
    endCrop(handler)
  } else {
    handler.editorUI.editorKit.emitEvent(Evt_onCropSizeChange, {
      width: originObject.extX,
      height: originObject.extY,
    })
  }
  handleUpdateCrop(handler)
}

/**
 * 对<裁剪框>进行缩放,以当前短边为比例大者，将长边缩放至对应比例, 即始终缩小
 * @param ratio width / height
 * */
export function cropByRatio(
  handler: IEditorHandler,
  ratio: number,
  img?: ImageShape
) {
  checkCropState(handler, img)
  const curState = handler.curState as CropState
  const { extX: width, extY: height } = curState.originObject
  const curRatio = width / height
  if (checkAlmostEqual(ratio, curRatio, 0.01)) return
  let size: { w: number; h: number }
  if (ratio < 1) {
    // 纵向
    size =
      width > height
        ? { w: height * ratio, h: height }
        : width / ratio > height
        ? { w: height * ratio, h: height }
        : { w: width, h: width / ratio }
  } else {
    // 横向
    size =
      width > height
        ? height * ratio > width
          ? { w: width, h: width / ratio }
          : { w: height * ratio, h: height }
        : { w: width, h: width / ratio }
  }
  cropBySize(handler, size)
}

/** 保持裁剪框中心不变，调整裁剪框大小 */
export function cropBySize(
  handler: IEditorHandler,
  size: { w: number; h: number },
  img?: ImageShape
) {
  checkCropState(handler, img)
  const curState = handler.curState as CropState
  const { originObject, cropBackGround } = curState

  const cx = originObject.x + originObject.extX / 2
  const cy = originObject.y + originObject.extY / 2
  const { w, h } = size
  const [newX, newY] = [cx - w / 2, cy - h / 2]

  const xfrm = originObject.spPr!.xfrm!
  xfrm.setExtX(w)
  xfrm.setExtY(h)
  xfrm.setOffX(newX)
  xfrm.setOffY(newY)

  originObject.setCalcStatusOf(CalcStatus.ImageShape.Geometry)
  setTransformCalcStatus(originObject)
  calculateForImageShape(originObject)
  calculateSpSrcRect(originObject, cropBackGround)

  handleUpdateCrop(handler)

  handler.editorUI.editorKit.emitEvent(Evt_onCropSizeChange, {
    width: originObject.extX,
    height: originObject.extY,
  })
}

export function resetSrcRect(handler: IEditorHandler, img?: ImageShape) {
  checkCropState(handler, img)
  const curState = handler.curState as CropState
  const { originObject, cropBackGround } = curState
  const srcRect = originObject.blipFill.srcRect?.clone() ?? new RelativeRect()
  const xfrm = cropBackGround.spPr!.xfrm!.clone()
  if (originObject.group) {
    const groupXfrm = originObject.group.spPr!.xfrm!
    xfrm.setOffX(xfrm.offX! - groupXfrm.offX!)
    xfrm.setOffY(xfrm.offY! - groupXfrm.offY!)
  }
  srcRect.set(0, 0, 100, 100)
  originObject.spPr?.setXfrm(xfrm)
  originObject.setCalcStatusOf(CalcStatus.ImageShape.Geometry)
  originObject.setCalcStatusOf(CalcStatus.ImageShape.Bounds)
  setTransformCalcStatus(originObject)
  calculateForImageShape(originObject)
  setSpSrcRect(originObject, srcRect)

  handleUpdateCrop(handler)
  handler.editorUI.editorKit.emitEvent(Evt_onCropSizeChange, {
    width: originObject.extX,
    height: originObject.extY,
  })
}

// ---------------------------------- pure function ---------------------------------- //
export function cropByGeometry(
  img: ImageShape,
  presetGeometry: PresetShapeTypes
) {
  if (img.spPr == null) return
  const geometry = createGeometry(presetGeometry)
  img.spPr.setGeometry(geometry)
}

// TODO
export function cropByBounds(
  img: ImageShape,
  originBounds: { x; y; w; h },
  backGroundBounds?: { x; y; w; h }
) {
  const cropBackGround = createCropBackGround(img)
  if (!cropBackGround) return
  if (backGroundBounds) {
    cropBackGround.x = backGroundBounds.x
    cropBackGround.y = backGroundBounds.y
    cropBackGround.extX = backGroundBounds.w
    cropBackGround.extY = backGroundBounds.h
  }
  img.spPr!.xfrm!.offX = originBounds.x
  img.spPr!.xfrm!.offY = originBounds.y
  img.spPr!.xfrm!.extX = originBounds.w
  img.spPr!.xfrm!.extY = originBounds.h
  calculateSpSrcRect(img, cropBackGround)
}

// ---------------------------------- drawers ---------------------------------- //

export function drawCropOnEditLayer(
  editLayerDrawer: EditLayerDrawer,
  handler: IEditorHandler
) {
  const curState = handler.curState
  if (curState.instanceType !== InstanceType.CropState) return
  const { originObject, cropBackGround, controlInfo } = curState

  // Resize/Move 时不再绘制底图
  if (originObject && (!controlInfo || !controlInfo.control.isTracked)) {
    if (editLayerDrawer.canvasDrawer) {
      const orgGlobalAlpha = editLayerDrawer.canvasDrawer.context.globalAlpha
      editLayerDrawer.canvasDrawer.setGlobalAlpha(1.0)
      drawCropImage(handler, editLayerDrawer, cropBackGround)
      editLayerDrawer.resetLayerBounds()
      editLayerDrawer.canvasDrawer.setGlobalAlpha(orgGlobalAlpha)
    }

    editLayerDrawer.drawControl({
      type: EditControlType.SHAPE,
      matrix: cropBackGround.getTransform(),
      left: 0,
      top: 0,
      width: cropBackGround.extX,
      height: cropBackGround.extY,
      isLine: false,
      canRotate: false,
    })

    if (editLayerDrawer.canvasDrawer) {
      const orgGlobalAlpha = editLayerDrawer.canvasDrawer.context.globalAlpha
      editLayerDrawer.canvasDrawer.setGlobalAlpha(1.0)
      drawCropImage(handler, editLayerDrawer, originObject)
      editLayerDrawer.resetLayerBounds()
      editLayerDrawer.canvasDrawer.setGlobalAlpha(orgGlobalAlpha)
    }

    editLayerDrawer.drawControl({
      type: EditControlType.CROP,
      matrix: originObject.getTransform(),
      left: 0,
      top: 0,
      width: originObject.extX,
      height: originObject.extY,
      isLine: false,
      canRotate: false,
    })
  }
}

export function drawControlTrackForCrop(
  control: ResizeSingleControl | MoveSingleControl,
  editLayerDrawer: EditLayerDrawer | GraphicsBoundsChecker,
  transform?
) {
  const curState = control.handler.curState
  if (
    curState.instanceType !== InstanceType.CropState ||
    !control.isTracked ||
    !curState.controlInfo ||
    editLayerDrawer.instanceType === InstanceType.GraphicsBoundsChecker
  ) {
    return
  }
  const { target, handler } = control
  const { originObject, cropBackGround } = curState
  const canvasDrawer = editLayerDrawer.canvasDrawer
  const oldAlpha = canvasDrawer.context.globalAlpha
  const cropBlipFill = control.brush.fill as BlipFill
  const cropSolidFill = createCropBrushFromRGB(cropBackGround, 128, 128, 128)
    .fill as SolidFill
  canvasDrawer.setGlobalAlpha(1)

  if (target === cropBackGround) {
    // draw cropBackground image
    control.layerRenderElement.draw(editLayerDrawer, transform)
    // draw gray rect
    control.brush.fill = cropSolidFill
    canvasDrawer.setGlobalAlpha(0.5)
    control.layerRenderElement.draw(editLayerDrawer, transform)
    // 绘制新的 srcRect 效果
    canvasDrawer.setGlobalAlpha(1)
    control.brush.fill = cropBlipFill

    // draw highlight originImg
    if (originObject.blipFill) {
      const orgPen = originObject.pen
      const orgBrush = originObject.brush
      const orgSrcRect = originObject.blipFill.srcRect!
      // 这里若有 flip 则计算的 srcRect 正负会有错误
      const [lastestExtX, lastestExtY] =
        control.instanceType === InstanceType.ResizeSingleControl
          ? [control.curExtX, control.curExtY]
          : [target.extX, target.extY]
      const srcRect = calculateSrcRectFunc(
        originObject.transform,
        getShapeCoordBound(originObject),
        MatrixUtils.invertMatrix(control.transform),
        lastestExtX,
        lastestExtY
      )
      if (isBlankSrcRect(srcRect)) {
        control.brush.transparent = 100
      }
      originObject.pen = createPen()
      originObject.blipFill.srcRect = srcRect
      drawCropImage(handler, editLayerDrawer, originObject)

      originObject.blipFill.srcRect = orgSrcRect
      originObject.pen = orgPen
      originObject.brush = orgBrush
      if (isBlankSrcRect(srcRect)) {
        control.brush.transparent = 0
      }
    }
  } else if (target === originObject) {
    // draw originImg gray rect
    const originCropBrush = createCropBrushFromRGB(originObject, 250, 250, 250)
    control.brush.fill = originCropBrush.fill
    control.layerRenderElement.draw(editLayerDrawer, transform)
    // draw background
    drawCropImage(handler, editLayerDrawer, cropBackGround)
    // draw originImage
    control.brush.fill = cropBlipFill
    control.layerRenderElement.pen = createPen()

    control.brush.transparent = 0
    // if (isBlankSrcRect(cropBlipFill.srcRect)) {
    //   control.brush.transparent = 100
    // }
    control.layerRenderElement.draw(editLayerDrawer, transform)
    // if (isBlankSrcRect(cropBlipFill.srcRect)) {
    //   control.brush.transparent = 0
    // }
  }
  if (isNumber(oldAlpha)) {
    canvasDrawer.setGlobalAlpha(oldAlpha)
  }
}

export function drawCropImage(
  handler: IEditorHandler,
  editLayerDrawer: EditLayerDrawer,
  sp: ImageShape
) {
  const curState = handler.curState
  if (curState.instanceType !== InstanceType.CropState) return
  toggleCxtTransformReset(editLayerDrawer, false)
  editLayerDrawer.applyTransformMatrix(sp.transform)
  const [orgPen, orgBrush] = [sp.pen, sp.brush]
  const brush = FillEffects.Blip(sp.blipFill)
  brush.transparent = sp.blipFill?.getTransparent()

  // draw border
  if (sp.pen) {
    drawShape(sp, editLayerDrawer, sp.calcedGeometry)
  }

  // draw image
  ;[sp.pen, sp.brush] = [null, brush]
  drawShape(sp, editLayerDrawer, sp.calcedGeometry)

  // draw shadow rect
  if (sp === curState.cropBackGround) {
    sp.brush = createCropBrushFromRGB(sp, 128, 128, 128)
    drawShape(sp, editLayerDrawer, sp.calcedGeometry)
  }

  ;[sp.pen, sp.brush] = [orgPen, orgBrush]
  editLayerDrawer.reset()
  toggleCxtTransformReset(editLayerDrawer, true)
}

// ---------------------------------- getter/setters ---------------------------------- //
function setSpSrcRect(sp: ImageShape | Shape, srcRect: RelativeRect) {
  if (sp.instanceType === InstanceType.ImageShape) {
    const blipFill = sp.blipFill!.clone()
    blipFill.srcRect = srcRect
    sp.setBlipFill(blipFill)
  } else {
    const brush = sp.brush!.clone()
    if (brush.fill && brush.fill.instanceType === InstanceType.BlipFill) {
      brush!.fill!.srcRect = srcRect
      sp.spPr!.setFill(brush)
    }
  }
}

// ---------------------------------- calculators ---------------------------------- //
function calcOriginBounds(img: ImageShape) {
  const blipFill = getBlipFill(img)
  if (!blipFill || !blipFill.srcRect) return
  const srcRect = blipFill.srcRect
  const srcId = blipFill.imageSrcId
  const offL = isNumber(srcRect.l) ? srcRect.l : 0
  const offT = isNumber(srcRect.t) ? srcRect.t : 0
  const offR = isNumber(srcRect.r) ? srcRect.r : 0
  const offB = isNumber(srcRect.b) ? srcRect.b : 0
  const wp = (offR - offL) / 100.0
  const hp = (offB - offT) / 100.0

  const { minX: x, minY: y, maxX, maxY } = getShapeCoordBound(img)
  const [w, h] = [maxX - x, maxY - y]
  const [extX, extY] = [w / wp, h / hp]
  const cx = (-extX * offL) / 100.0 + x + extX / 2.0
  const cy = (-extY * offT) / 100.0 + y + extY / 2.0
  const transformedXCenter = img.transform.XFromPoint(cx, cy)
  const transformedYCenter = img.transform.YFromPoint(cx, cy)
  const transformedX = transformedXCenter - extX / 2.0
  const transformedY = transformedYCenter - extY / 2.0
  return { srcId, transformedX, transformedY, extX, extY }
}

export function calculateSpSrcRect(sp: ImageShape, cropBackGround: ImageShape) {
  const orgTransform = sp.transform.clone()

  const s = turnOffRecordChanges()
  calcTransform(sp)
  calculateGeometryForSp(sp)
  restoreRecordChangesState(s)

  sp.transform = orgTransform
  setSpSrcRect(
    sp,
    calculateSrcRectFunc(
      sp.transform,
      getShapeCoordBound(sp),
      cropBackGround.invertTransform,
      cropBackGround.extX,
      cropBackGround.extY
    )
  )
}

/** crop api 调用后的统一刷新处理 */
export function handleUpdateCrop(handler: IEditorHandler) {
  const curState = handler.curState
  if (curState.instanceType !== InstanceType.CropState) return
  checkUpdateCropInGroup(handler)
  handler.updateEditLayer()
}

export function checkUpdateCropInGroup(handler: IEditorHandler) {
  const { originObject } = handler.curState as CropState
  if (originObject.group) {
    adjustXfrmOfGroup(originObject.group)
    return true
  }
}

// ---------------------------------- Helpers ---------------------------------- //

export function createCropBrushFromRGB(sp: ImageShape, r, g, b) {
  const originBrush = FillEffects.SolidRGBA(r, g, b)
  originBrush.transparent = 100
  const parents = getParentsOfElement(sp)
  originBrush.calculate(
    parents.theme,
    parents.slide,
    parents.layout,
    parents.master,
    { r: 0, g: 0, b: 0, a: 255 },
    gDefaultColorMap
  )
  return originBrush
}

export function ensureSrcRect(sp: SlideElement) {
  if (sp == null) {
    return null
  }
  switch (sp.instanceType) {
    case InstanceType.GroupShape:
    case InstanceType.GraphicFrame:
      return
    case InstanceType.ImageShape:
      if (!sp.blipFill.srcRect) {
        const blipFill = sp.blipFill
        blipFill.srcRect = new RelativeRect()
        blipFill.srcRect.l = 0
        blipFill.srcRect.t = 0
        blipFill.srcRect.r = 100
        blipFill.srcRect.b = 100
      }
      break
    case InstanceType.Shape:
      const brush = sp.brush
      if (
        brush?.fill?.instanceType === InstanceType.BlipFill &&
        !brush.fill.srcRect
      ) {
        const fill = brush.fill
        fill.srcRect = new RelativeRect()
        fill.srcRect.l = 0
        fill.srcRect.t = 0
        fill.srcRect.r = 100
        fill.srcRect.b = 100
      }
      break
  }
}

export function checkCropState(
  handler: IEditorHandler,
  img?: ImageShape,
  triggerUI = true
) {
  if (handler.curState.instanceType !== InstanceType.CropState) {
    startCrop(handler, img, triggerUI)
  }
}

export function calculateSrcRectFunc(
  originMatrix: Matrix2D,
  originBound: WithBounds,
  backgroundInvertTransform: Matrix2D,
  backgroundExtX,
  backgroundExtY
) {
  // calculate originSp absolute postion
  const ltAbsX = originMatrix.XFromPoint(originBound.minX, originBound.minY)
  const ltAbsY = originMatrix.YFromPoint(originBound.minX, originBound.minY)
  const rbAbsX = originMatrix.XFromPoint(originBound.maxX, originBound.maxY)
  const rbAbsY = originMatrix.YFromPoint(originBound.maxX, originBound.maxY)

  // calculate originSp relative position with background(when no Flip)
  const ltRelX = backgroundInvertTransform.XFromPoint(ltAbsX, ltAbsY)
  const ltRelY = backgroundInvertTransform.YFromPoint(ltAbsX, ltAbsY)
  const rbRelX = backgroundInvertTransform.XFromPoint(rbAbsX, rbAbsY)
  const rbRelY = backgroundInvertTransform.YFromPoint(rbAbsX, rbAbsY)

  // calculate inner x/y percentage
  const srcRect = new RelativeRect()
  const l = 100 * (ltRelX / backgroundExtX)
  const t = 100 * (ltRelY / backgroundExtY)
  const r = 100 * (rbRelX / backgroundExtX)
  const b = 100 * (rbRelY / backgroundExtY)

  // without cal flip, so use min/max
  srcRect.l = Math.min(l, r)
  srcRect.t = Math.min(t, b)
  srcRect.r = Math.max(l, r)
  srcRect.b = Math.max(t, b)

  return srcRect
}

export function isBlankSrcRect(srcRect?: RelativeRect): boolean {
  if (!srcRect) return true
  const { l, t, r, b } = srcRect
  if (l == null || t == null || r == null || b == null) return false
  const hasXAcross = !((l < 0 && r < 0) || (l > 100 && r > 100))
  const hasYAcross = !((t < 0 && b < 0) || (t > 100 && b > 100))
  return !hasXAcross || !hasYAcross
}

// ---------------------------------- Hit ---------------------------------- //

/** 判断是否命中裁剪的 img */
export function hitCropHandles(img: ImageShape, x, y, isTouch: boolean) {
  const invertTransform = getInvertTransform(img)

  const { extX, extY } = img
  const tx = invertTransform.XFromPoint(x, y)
  const ty = invertTransform.YFromPoint(x, y)
  const mmPerPix = convertPxToMM(1)
  const pixPerMM = 1 / mmPerPix
  const pxDotRadius = isTouch
    ? TOUCH_CONTROL_CIRCLE_RADIUS
    : CONTROL_CIRCLE_RADIUS
  const mmDotRadius = pxDotRadius * mmPerPix

  const centralDotNeed = CONTROL_CROP_CENTRAL_DOT_NEED_MIN_WIDTH
  const width = (extX * pixPerMM + 1) >> 1
  const needDrawXCentralDot = width > centralDotNeed ? true : false
  const cornerW = mmPerPix * Math.min(width, CONTROL_CROP_CORNER_SIZE)

  const height = (extY * pixPerMM + 1) >> 1
  const needDrawYCentralDot = height > centralDotNeed ? true : false
  const cornerH = mmPerPix * Math.min(height, CONTROL_CROP_CORNER_SIZE)

  if (hitInnerRect(tx, ty, 0, 0, cornerW, mmDotRadius)) {
    return REL_DIRECTION.LT
  }
  if (hitInnerRect(tx, ty, 0, 0, mmDotRadius, cornerH)) {
    return REL_DIRECTION.LT
  }

  if (needDrawXCentralDot) {
    if (
      hitInnerRect(
        tx,
        ty,
        extX / 2 - cornerW / 2,
        0,
        extX / 2 + cornerW / 2,
        mmDotRadius
      )
    ) {
      return REL_DIRECTION.T
    }
    if (
      hitInnerRect(
        tx,
        ty,
        extX / 2 - cornerW / 2,
        extY - mmDotRadius,
        extX / 2 + cornerW / 2,
        extY
      )
    ) {
      return REL_DIRECTION.B
    }
  }

  if (hitInnerRect(tx, ty, extX - cornerW, 0, extX, mmDotRadius)) {
    return REL_DIRECTION.RT
  }
  if (hitInnerRect(tx, ty, extX - mmDotRadius, 0, extX, cornerH)) {
    return REL_DIRECTION.RT
  }

  if (needDrawYCentralDot) {
    if (
      hitInnerRect(
        tx,
        ty,
        extX - mmDotRadius,
        extY / 2 - cornerH / 2,
        extX,
        extY / 2 + cornerH / 2
      )
    ) {
      return REL_DIRECTION.R
    }
    if (
      hitInnerRect(
        tx,
        ty,
        0,
        extY / 2 - cornerH / 2,
        mmDotRadius,
        extY / 2 + cornerH / 2
      )
    ) {
      return REL_DIRECTION.L
    }
  }

  if (hitInnerRect(tx, ty, extX - mmDotRadius, extY - cornerH, extX, extY)) {
    return REL_DIRECTION.RB
  }
  if (hitInnerRect(tx, ty, extX - cornerW, extY - mmDotRadius, extX, extY)) {
    return REL_DIRECTION.RB
  }

  if (hitInnerRect(tx, ty, 0, extY - height, mmDotRadius, extY)) {
    return REL_DIRECTION.LB
  }
  if (hitInnerRect(tx, ty, 0, extY - mmDotRadius, cornerW, extY)) {
    return REL_DIRECTION.LB
  }
  return DIRECTION_UNHIT
}

function hitInnerRect(x, y, l, t, r, b) {
  return x >= l && x <= r && y >= t && y <= b
}
