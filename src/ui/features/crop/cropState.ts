import {
  calculateSpSrcRect,
  calculateSrcRectFunc,
  ensureSrcRect,
  checkUpdateCropInGroup,
  createCropBackGround,
  drawControlTrackForCrop,
  drawCropOnEditLayer,
  endCrop,
  hitCropHandles,
} from './utils'
import { Nullable } from '../../../../liber/pervasive'
import { isDict } from '../../../common/utils'
import { MatrixUtils } from '../../../core/graphic/Matrix'
import { ImageShape } from '../../../core/SlideElement/Image'
import {
  hitBoundingRect,
  hitInnerArea,
  hitInControls,
} from '../../states/hittests'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { InstanceType } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import { EditLayerDrawer } from '../../rendering/Drawer/EditLayerDrawer'
import { IEditorHandler } from '../../rendering/editorHandler/EditorHandler'
import { createResizeControl } from '../../rendering/editorHandler/utils'
import { MoveSingleControl } from '../move/moveControls'
import { ResizeSingleControl } from '../resize/resizeControls'
import { HoverInfo } from '../../states/type'
import { Evt_onCropSizeChange } from '../../../editor/events/EventNames'
import { PointerEventInfo } from '../../../common/dom/events'
import { getShapeCoordBound } from '../../../core/utilities/shape/draw'
import { calculateGeometryForSp } from '../../../core/utilities/shape/calcs'
import { BlipFill } from '../../../core/SlideElement/attrs/fill/blipFill'
import { CursorTypesByAbsDirection } from '../../../core/common/const/ui'
import { calculateForImageShape } from '../../../core/calculation/calculate/image'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { createMoveControl, handleMoveControlsMove } from '../move/utils'
import {
  getAbsDirectionByRel,
  getRelDirectionByAbs,
  handleResizeOnMouseMove,
} from '../resize/utils'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../../core/common/utils'
import { startEditAction } from '../../../core/calculation/informChanges/startAction'
import { calculateForRendering } from '../../../core/calculation/calculate/calculateForRendering'
import { CalcStatus } from '../../../core/calculation/updateCalcStatus/calcStatus'
import { setTransformCalcStatus } from '../../../core/calculation/updateCalcStatus/utils'
import { logger } from '../../../lib/debug/log'

type ReiszeControlInfo = {
  type: 'Resize'
  control: ResizeSingleControl
  resizeRelativeDirection: number
}
type MoveControlInfo = {
  type: 'Move'
  control: MoveSingleControl
  startPos: { x; y }
}

export class CropState {
  readonly instanceType = InstanceType.CropState
  handler: IEditorHandler
  originObject: ImageShape
  cropBackGround: ImageShape
  controlInfo?: ReiszeControlInfo | MoveControlInfo
  isHit: boolean

  constructor(
    handler: IEditorHandler,
    originObject: ImageShape,
    cropBackGround: ImageShape
  ) {
    this.handler = handler
    this.originObject = originObject

    this.cropBackGround = cropBackGround
    this.isHit = false
  }

  getHoverInfo(e: PointerEventInfo, x: number, y: number): Nullable<HoverInfo> {
    const ret = getSelectedCropImageHoverInfo(this.handler, e, x, y)
    return ret
  }

  onStart(e, x, y) {
    // 判断是否处于裁剪框操作区
    const ret = getSelectedCropImageHoverInfo(this.handler, e, x, y)
    if (!ret) {
      this.isHit = false
      return false
    }
    const handleImg =
      ret.objectId === this.originObject.getId()
        ? this.originObject
        : this.cropBackGround
    // 是否为 resizeType
    this.isHit = true
    if (ret.absDirection != null) {
      const resizeRelativeDirection = getRelDirectionByAbs(
        handleImg,
        ret.absDirection
      )
      const control = createResizeControl(
        handleImg,
        ret.absDirection,
        this.handler,
        drawControlTrackForCrop
      ) as ResizeSingleControl
      this.controlInfo = { type: 'Resize', control, resizeRelativeDirection }
    } else if (ret.cursorType === 'move') {
      // move
      const startPos = { x, y }
      const control = createMoveControl(
        handleImg,
        this.handler,
        drawControlTrackForCrop
      ) as MoveSingleControl
      this.controlInfo = { type: 'Move', control, startPos }
    }
    return true
  }

  onMove(e: PointerEventInfo, x, y): Nullable<HoverInfo> {
    if (!e.isMove) {
      return
    }
    const ret = getSelectedCropImageHoverInfo(this.handler, e, x, y)

    if (this.controlInfo) {
      const handler = this.handler
      // handle resize cropImg
      if (this.controlInfo.type === 'Resize') {
        const { control, resizeRelativeDirection: dir } = this.controlInfo
        const { target } = control
        let cb: (ctrl: ResizeSingleControl, scaleX: number, scaleY) => void

        if (target === this.cropBackGround) {
          cb = (control, scaleX, scaleY) => {
            const s = turnOffRecordChanges()
            control.track(scaleX, scaleY, e, x, y)
            restoreRecordChangesState(s)
          }
        } else {
          cb = (control, scaleX, scaleY) => {
            control.track(scaleX, scaleY, e, x, y)
            const srcRect = (control.brush.fill as BlipFill).srcRect
            if (!srcRect) {
              logger.error('[NullSrcRect]Crop.Resize.onMove')
              return
            }
            const { l, t, r, b } = srcRect
            const [wp, hp] = [(r! - l!) / 100, (b! - t!) / 100]
            handler.editorUI.editorKit.emitEvent(Evt_onCropSizeChange, {
              width: this.cropBackGround.extX * wp,
              height: this.cropBackGround.extY * hp,
            })
          }
        }
        handleResizeOnMouseMove(
          handler,
          [control],
          target,
          dir,
          control.absDir,
          e,
          x,
          y,
          cb
        )
      }
      // handle move cropImg
      if (this.controlInfo.type === 'Move') {
        const { control, startPos } = this.controlInfo
        const posInfo = { x, y, startX: startPos.x, startY: startPos.y }
        let cb: (ctrl: MoveSingleControl, tx: number, ty: number) => void

        if (control.target === this.cropBackGround) {
          cb = (ctrl, tx, ty) => {
            const s = turnOffRecordChanges()
            control.track(tx, ty)
            restoreRecordChangesState(s)
            ;(ctrl.brush.fill as BlipFill).srcRect?.set(0, 0, 100, 100)
          }
        } else {
          cb = (ctrl, tx, ty) => {
            ctrl.track(tx, ty)
            ;(ctrl.brush.fill as BlipFill).srcRect = calculateSrcRectFunc(
              ctrl.transform,
              getShapeCoordBound(ctrl.layerRenderElement),
              this.cropBackGround.invertTransform,
              this.cropBackGround.extX,
              this.cropBackGround.extY
            )
          }
        }
        handleMoveControlsMove(handler, [control], e, posInfo, cb)
      }
      handler.updateEditLayer()
    }
    return ret
  }

  onEnd(_e, _x, _y) {
    const handler = this.handler
    if (this.controlInfo && this.controlInfo.control.isTracked) {
      if (!this.isHit) {
        this.controlInfo = undefined
        endCrop(handler)
        return
      }
      const _this = this
      const target = this.controlInfo.control.target

      // resizeState
      if (this.controlInfo.type === 'Resize') {
        const control = this.controlInfo.control
        startEditAction(EditActionFlag.action_Presentation_Crop)
        if (target === _this.cropBackGround) {
          const s = turnOffRecordChanges()
          control.trackEnd()
          calculateGeometryForSp(target)
          restoreRecordChangesState(s)
        } else {
          control.trackEnd()
        }
        target.transform = control.transform
        target.invertTransform = MatrixUtils.invertMatrixFrom(
          target.invertTransform,
          control.transform
        )
        target.extX = control.curExtX
        target.extY = control.curExtY
        calculateSpSrcRect(_this.originObject, _this.cropBackGround)
        calculateForRendering()
      }
      // MoveState
      else if (this.controlInfo.type === 'Move') {
        const control = this.controlInfo.control
        startEditAction(EditActionFlag.action_Presentation_Crop)
        control.trackEnd()
        target.transform = control.transform
        target.invertTransform = MatrixUtils.invertMatrixFrom(
          target.invertTransform,
          control.transform
        )
        calculateSpSrcRect(_this.originObject, _this.cropBackGround)
        calculateForRendering()
      }
      this.controlInfo = undefined
      checkUpdateCropInGroup(handler)
      handler.updateEditLayer()
    } else {
      this.controlInfo = undefined
      if (this.isHit) {
        this.isHit = false
        return
      }
      endCrop(handler)
    }
  }

  /** 裁剪时原图不应继续绘制在 slide 图层 */
  needDrawOnSlide(sp: SlideElement) {
    return sp !== this.originObject
  }

  needPaintControlTrack() {
    return this.controlInfo && this.controlInfo.control.isTracked
  }

  /** 注意需要在 shapeControlDrawer.canvasDrawer 初始化之后*/
  drawStatic(editLayerDrawer: EditLayerDrawer) {
    drawCropOnEditLayer(editLayerDrawer, this.handler)
  }

  drawControl(editLayerDrawer: EditLayerDrawer) {
    if (!EditorSettings.isSupportCrop) return
    this.drawStatic(editLayerDrawer)
    if (!this.needPaintControlTrack()) return
    this.controlInfo!.control.draw(editLayerDrawer)
  }

  /** applyChange 后可以正确的重置 cropBackground 的位置 */
  refreshState() {
    this.isHit = false
    // 检查是否已被删除
    const { spTree } = this.handler.getCurrentSlide().cSld
    const hasDeleted = this.originObject.refId
      ? !isDict(spTree.find((sp) => sp.refId === this.originObject.refId))
      : true
    if (hasDeleted) {
      endCrop(this.handler)
      return
    }
    // 需要 calcTransform 校正 tx/ty
    ensureSrcRect(this.originObject)
    this.originObject.setCalcStatusOf(CalcStatus.ImageShape.Geometry)
    setTransformCalcStatus(this.originObject)
    calculateForImageShape(this.originObject)
    const cropBackGround = createCropBackGround(this.originObject)
    if (cropBackGround) this.cropBackGround = cropBackGround
    this.handler.updateEditLayer()
    this.handler.editorUI.editorKit.emitEvent(Evt_onCropSizeChange, {
      width: this.originObject.extX,
      height: this.originObject.extY,
    })
  }
}

/**
 * 判断是否命中裁剪对象/背景, 边框/内容，即优先级: Resize > Move
 * */
function getSelectedCropImageHoverInfo(
  handler: IEditorHandler,
  _e,
  x,
  y
): Nullable<HoverInfo> {
  const { curState: cropState } = handler
  if (cropState.instanceType !== InstanceType.CropState) return
  let ret: Nullable<HoverInfo>
  const { originObject, cropBackGround } = cropState
  const isTouchMode = handler.editorUI.isInTouchMode()
  // 是否命中原图边框缩放操作点
  const originImgBorderRelativeDirection = hitCropHandles(
    originObject,
    x,
    y,
    isTouchMode
  )
  if (originImgBorderRelativeDirection > -1) {
    ret = getCropSelectHoverInfo(
      handler,
      originImgBorderRelativeDirection,
      originObject
    )
  }
  if (ret) return ret
  // 是否命中背景图边框缩放操作点
  const bgImgRelativeBorderDirection = hitInControls(
    cropBackGround,
    x,
    y,
    handler.editorUI.isInTouchMode()
  )
  if (bgImgRelativeBorderDirection > -1) {
    ret = getCropSelectHoverInfo(
      handler,
      bgImgRelativeBorderDirection,
      cropBackGround
    )
  }
  if (ret) return ret
  // 命中原图边框则操作原图移动(暂不允许旋转)
  if (hitBoundingRect(originObject, x, y, isTouchMode, false)) {
    return {
      objectId: originObject.getId(),
      cursorType: 'move',
    }
  }
  // 否则统一操作背景图移动
  if (
    hitInnerArea(originObject, x, y) ||
    hitBoundingRect(cropBackGround, x, y, isTouchMode) ||
    hitInnerArea(cropBackGround, x, y)
  ) {
    return {
      objectId: cropBackGround.getId(),
      cursorType: 'move',
    }
  }
  return ret
}

function getCropSelectHoverInfo(
  _handler: IEditorHandler,
  hit: number, // typeof REL_DIRECTION[ keyof typeof  REL_DIRECTION],
  img: ImageShape
): HoverInfo {
  const absoluteDirection = getAbsDirectionByRel(img, hit)
  return {
    objectId: img.getId(),
    cursorType: CursorTypesByAbsDirection[absoluteDirection],
    absDirection: absoluteDirection,
  }
}
