import { BrowserInfo } from '../../../common/browserInfo'
import { isNumber } from '../../../common/utils'
import { calculateRenderingStateForSlides } from '../../../core/calculation/calculate/presentation'
import { getCanvasDrawer } from '../../../core/graphic/CanvasDrawer'
import { renderPresentationSlide } from '../../../core/render/slide'
import { IEditorKit } from '../../../editor/type'
import { logger } from '../../../lib/debug/log'
import { getPrintSlideIndexes } from './print'

const DefaultToImageDpi = 96.0
const MAX_CANVAS_ALLOC_BYTES = (BrowserInfo.mozilla ? 451 : 256) * 1024 * 1024
const MAX_PX_DIMENSION = 32767 // ie 下为 8192

export interface ToImageLayoutSettings {
  margin?: { l: number; r: number; t: number; b: number }
  dpi?: number
}

/** 一次性生成长图(空间换空间) */
export function downloadByCanvasImage(
  editor: IEditorKit,
  indexes: number[] = [],
  layoutSetting: ToImageLayoutSettings = {
    dpi: DefaultToImageDpi,
    margin: { l: 0, r: 0, t: 0, b: 4 },
  }
) {
  logger.time('downloadByImage')
  const result = prepareComposedCanvasByIndex(editor, indexes, layoutSetting)
  if (!result || isNumber(result)) return
  let canvas = result as HTMLCanvasElement | undefined
  const aDom = document.createElement('a')
  canvas!.toBlob((res) => {
    const url = URL.createObjectURL(res!)
    aDom.href = url
    aDom.download = Date.now() + '.png'
    aDom.click()
    aDom.remove()
    canvas!.remove()
    canvas = undefined as any
    URL.revokeObjectURL(url)
    logger.timeEnd('downloadByImage')
  })
}

export function prepareComposedCanvasByIndex(
  editor: IEditorKit,
  indexes: number[],
  layoutSetting: ToImageLayoutSettings = {
    dpi: DefaultToImageDpi,
    margin: { l: 0, r: 0, t: 0, b: 4 },
  }
): HTMLCanvasElement | number {
  logger.time('prepareComposedCanvas')
  const { presentation } = editor.editorUI
  const { width, height } = presentation
  const margin = layoutSetting.margin ?? { l: 0, r: 0, t: 0, b: 4 }
  const toImageDpi = layoutSetting.dpi ?? DefaultToImageDpi
  const toImageMM2Px = toImageDpi / 25.4
  const pixWidth = (width * toImageMM2Px) >> 0
  const pixHeight = (height * toImageMM2Px) >> 0
  const slideIndexes = getPrintSlideIndexes(editor, false, 'all')
  const count = slideIndexes.length
  const totalWidth = (pixWidth + margin.l + margin.r) >> 0
  const totalHeight = (count * (pixHeight + margin.t + margin.b)) >> 0
  if (totalHeight > MAX_PX_DIMENSION) {
    const maxSlideCount = (MAX_PX_DIMENSION / pixHeight) >> 0
    return maxSlideCount
  }
  if (totalWidth * totalHeight * 4 > MAX_CANVAS_ALLOC_BYTES) {
    const maxSlideCount = Math.floor(
      MAX_CANVAS_ALLOC_BYTES / (4 * totalWidth * pixHeight)
    )
    return maxSlideCount
  }
  const startX = margin.l >> 0
  let startY = margin.t >> 0

  const canvas = document.createElement('canvas')
  const ctx = canvas.getContext('2d')!
  canvas.width = totalWidth
  canvas.height = totalHeight

  let tmpCanvas = document.createElement('canvas')
  tmpCanvas.width = pixWidth
  tmpCanvas.height = pixHeight
  const tmpCtx = tmpCanvas.getContext('2d')!
  const drawer = getCanvasDrawer(tmpCtx, pixWidth, pixHeight, width, height)
  drawer.isNoRenderEmptyPlaceholder = true
  drawer.isRenderThumbnail = true

  for (let i = 0; i < count; i++) {
    tmpCtx.save()
    drawer.saveState()
    drawer.updateTransform(1, 0, 0, 1, 0, 0)
    const slideNum = slideIndexes[i]
    calculateRenderingStateForSlides([presentation.slides[slideNum]])
    renderPresentationSlide(presentation, slideNum, drawer)
    ctx.drawImage(tmpCanvas, startX, startY, pixWidth, pixHeight)
    drawer.restoreState()
    tmpCtx.clearRect(0, 0, pixWidth, pixHeight)
    tmpCtx.restore()
    startY += pixHeight + margin.t + margin.b
  }
  drawer.resetState()
  tmpCanvas.remove()
  tmpCanvas = undefined as any
  logger.timeEnd('prepareComposedCanvas')
  return canvas
}

// 基于 jimp 的 merge base64 方案暂未提供(编解码时间过长)
