import { getCanvasDrawer } from '../../../core/graphic/CanvasDrawer'
import { renderPresentationSlide } from '../../../core/render/slide'
import { IEditorKit } from '../../../editor/type'
import { isNumber } from '../../../common/utils'
import { Slide } from '../../../core/Slide/Slide'
import { calculateRenderingStateForSlides } from '../../../core/calculation/calculate/presentation'

const DesignDpi = 72
const DefaultPrintDpi = 96.0 // 300

/**
 * 打印版式的一些规则
 * - pagePadding - 纸张的内边距, 固定不变(只和纸张大小有关)
 * - contentMargin - (多个 Slide 组成的)内容的外边距, 随 scale 缩放(基数也受纸张大小影响)
 * - slidePadding - 每个 Slide 独立的内边距, 随 slide 缩放(基数同 pagePadding)
 * - 框 1px, 不缩放
 * - layout 为 Slides 则只有 pagePadding 没有 contentMargin
 * */

/** 设计图(72dpi)A4纸张, 不同版式下每个独立 slide 的基准尺寸, 使用前需按 dpi 缩放校准 */
const DesignA4SizePx = {
  ['slides']: {
    ['hor']: [730, 411],
    ['ver']: [539, 303],
  },
  ['handouts']: {
    1: {
      ['hor']: [634, 358],
      ['ver']: [489, 275],
    },
    2: {
      ['hor']: [316, 178],
      ['ver']: [486, 274],
    },
    3: {
      ['hor']: [220, 124],
      ['ver']: [244, 137],
    },
    4: {
      ['hor']: [316, 178],
      ['ver']: [244, 137],
    },
    6: {
      ['hor']: [220, 124],
      ['ver']: [244, 137],
    },
    9: {
      ['hor']: [220, 124],
      ['ver']: [164, 92],
    },
  },
} as const

/** 竖屏幻灯片, （竖 A4 纸, 竖 894, 横595） */
const DesignEavertA4SizePx = {
  ['slides']: {
    ['hor']: [undefined, 501],
    ['ver']: [undefined, 760],
  },
  ['handouts']: {
    1: {
      ['hor']: [undefined, 501],
      ['ver']: [undefined, 760],
    },
    2: {
      ['hor']: [undefined, 501],
      ['ver']: [undefined, 370],
    },
    3: {
      ['hor']: [undefined, 240],
      ['ver']: [undefined, 240],
    },
    4: {
      ['hor']: [undefined, 230],
      ['ver']: [undefined, 360],
    },
    6: {
      ['hor']: [undefined, 160],
      ['ver']: [undefined, 240],
    },
    9: {
      ['hor']: [undefined, 150],
      ['ver']: [undefined, 240],
    },
  },
} as const

const DefaultA4InnerPadding = {
  ['hor']: 56,
  ['ver']: 28,
} as const

export class PrintCanvasCache {
  canvas: HTMLCanvasElement
  width: number
  height: number
  malloced = true

  constructor(width, height) {
    this.canvas = document.createElement('canvas')
    this.canvas.width = width
    this.canvas.height = height
    this.width = width
    this.height = height
  }

  free() {
    this.canvas.remove()
    this.malloced = false
  }
}

const PrintCanvasBuffer: {
  canvasCaches: PrintCanvasCache[]
  getCacheCanvas: (width: number, height: number) => PrintCanvasCache
  checkGC: () => void
} = {
  canvasCaches: [],
  getCacheCanvas(width, height): PrintCanvasCache {
    const caches = PrintCanvasBuffer.canvasCaches
    for (let i = 0; i < caches.length; i++) {
      const cache = caches[i]
      if (cache.width === width && cache.height === height && !cache.malloced) {
        cache.malloced = true
        PrintCanvasBuffer.checkGC()
        return cache
      }
    }
    const newCanvasCache = new PrintCanvasCache(width, height)
    PrintCanvasBuffer.canvasCaches.push(newCanvasCache)
    return newCanvasCache
  },

  checkGC() {
    const caches = PrintCanvasBuffer.canvasCaches
    for (let l = caches.length, i = l - 1; i > 0; i--) {
      if (!caches[i].malloced) {
        caches[i].canvas = undefined as any
        delete caches[i]
        caches.splice(i, 1)
      }
    }
  },
}

export const PrintPageSize = {
  A3: [297, 420], // mm
  A4: [210, 297],
  A5: [148, 210],
  B5: [176, 250],
  B4: [250, 353],
} as const

export type PrintSlideCountPerPage = 1 | 2 | 3 | 4 | 6 | 9

export type LayoutInfo =
  | {
      readonly type: 'slides'
    }
  | {
      readonly type: 'handouts'
      slidesPerPage: 1 | 2 | 3 | 4 | 6 | 9
      needPlaceHolder?: boolean
    }

export type PrintRangeType = 'all' | 'selected' | 'current'
export type PrintDirection = 'br' | 'bl' | 'lb' | 'rb' // to right, then to bottom

export interface PrintLayoutSettings {
  layout: LayoutInfo
  pageSize: { mmWidth: number; mmHeight: number }
  direction: PrintDirection
  needBorder: boolean
  isPrintHidden: boolean
  /** scale < 100 时，缩放单个 slide 在 page 中占据的大小, scale > 100 时，slide 大小不变，内容缩放 */
  scaleByPage: number
  rangeType: PrintRangeType
  dpi?: number
}

export function paintPrintPageBySettings(
  editor: IEditorKit,
  pageIndex: number,
  settings: PrintLayoutSettings = {
    layout: { type: 'slides' },
    pageSize: { mmWidth: 210, mmHeight: 297 },
    direction: 'rb',
    needBorder: false,
    isPrintHidden: false,
    scaleByPage: 100,
    rangeType: 'all',
    dpi: DefaultPrintDpi,
  }
): PrintCanvasCache {
  const { presentation } = editor
  const { width, height } = presentation
  const { layout, pageSize, direction, scaleByPage } = settings
  const { mmWidth, mmHeight } = pageSize
  const printDpi = settings.dpi ?? DefaultPrintDpi
  const printMM2Px = printDpi / 25.4

  const slideIndexes = getPrintSlideIndexesAtPage(editor, pageIndex, settings)
  const { size, poses } = calcSizeByPageLayout(
    layout,
    width / height,
    pageSize,
    direction,
    printDpi,
    scaleByPage
  )

  const pixPageWidth = (mmWidth * printMM2Px) >> 0
  const pixPageHeight = (mmHeight * printMM2Px) >> 0
  const pageCanvasObj = PrintCanvasBuffer.getCacheCanvas(
    pixPageWidth,
    pixPageHeight
  )
  const pageCanvas = pageCanvasObj.canvas
  const pageCtx = pageCanvas.getContext('2d')!
  pageCtx.fillStyle = '#FFFFFF'
  pageCtx.fillRect(0, 0, pixPageWidth, pixPageHeight)

  const zoom = Math.max(1, scaleByPage / 100)
  let tmpCanvas = document.createElement('canvas')

  const pixTmpCanvasWidth = size.width * zoom
  const pixTmpCanvasHeight = size.height * zoom
  tmpCanvas.width = pixTmpCanvasWidth
  tmpCanvas.height = pixTmpCanvasHeight
  const tmpCtx = tmpCanvas.getContext('2d')!
  const drawer = getCanvasDrawer(
    tmpCtx,
    pixTmpCanvasWidth,
    pixTmpCanvasHeight,
    width,
    height
  )
  drawer.isNoRenderEmptyPlaceholder = true
  drawer.isRenderThumbnail = true
  let phCount = 0

  const isToLeft = direction === 'bl' || direction === 'lb'
  let placehodlerSkipNum = 0

  for (let posIndex = 0; posIndex < poses.length; posIndex++) {
    const pos = poses[posIndex]
    tmpCtx.save()
    if (!pos.isPlaceHolder) {
      const indexDelta = pos.relIndex
      const slideIndex = slideIndexes[indexDelta]

      if (!isNumber(slideIndex)) {
        if (isToLeft) {
          placehodlerSkipNum++
        }
        continue
      }
      phCount++
      drawer.saveState()
      drawer.updateTransform(1, 0, 0, 1, 0, 0)

      calculateRenderingStateForSlides([presentation.slides[slideIndex]])
      renderPresentationSlide(presentation, slideIndex, drawer)
      const clipX = ((pixTmpCanvasWidth * (1 - 1 / zoom)) / 2) >> 0
      const clipY = ((pixTmpCanvasHeight * (1 - 1 / zoom)) / 2) >> 0
      // source == dest, 避免通过 drawImage 缩放
      pageCtx.drawImage(
        tmpCanvas,
        clipX,
        clipY,
        pixTmpCanvasWidth / zoom,
        pixTmpCanvasHeight / zoom,
        pos.startX,
        pos.startY,
        size.width,
        size.height
      )
      if (settings.needBorder) {
        pageCtx.beginPath()
        pageCtx.lineWidth = 1
        pageCtx.strokeStyle = '#000000'

        pageCtx.strokeRect(
          pos.startX - 1,
          pos.startY - 1,
          size.width + 1,
          size.height + 1
        )
      }
      drawer.restoreState()
    } else if (placehodlerSkipNum > 0) {
      placehodlerSkipNum--
    } else if (phCount > 0) {
      const lineLength = 7
      const lineWidth = (1 * printMM2Px) >> 0
      const lineGap = ((pixTmpCanvasHeight - lineWidth) / (lineLength - 1)) >> 0
      tmpCtx.lineWidth = lineWidth
      tmpCtx.strokeStyle = '#C6C8C9'
      for (let i = 0; i < lineLength; i++) {
        const y =
          i === 0
            ? Math.ceil(lineWidth / 2)
            : i === lineLength - 1
            ? pixTmpCanvasHeight - Math.ceil(lineWidth / 2)
            : Math.ceil(lineWidth / 2) + i * lineGap

        tmpCtx.beginPath()
        tmpCtx.moveTo(0, y)
        tmpCtx.lineTo(pixTmpCanvasWidth, y)
        tmpCtx.stroke()
      }
      pageCtx.drawImage(
        tmpCanvas,
        pos.startX,
        pos.startY,
        size.width,
        size.height
      )
      phCount--
    }
    tmpCtx.clearRect(0, 0, pixTmpCanvasWidth, pixTmpCanvasHeight)
    tmpCtx.restore()
  }

  drawer.resetState()
  tmpCanvas.remove()
  tmpCanvas = undefined as any
  // Debug
  // const aDom = document.createElement('a')
  // pageCanvas.toBlob((res) => {
  //   const url = URL.createObjectURL(res!)
  //   aDom.href = url
  //   aDom.download = Date.now() + '.png'
  //   aDom.click()
  //   aDom.remove()
  //   pageCanvas.remove()
  //   URL.revokeObjectURL(url)
  // })
  return pageCanvasObj
}

// layout utils

/** @returns 返回位置矩阵, [[...row1], [...row2], ...] */
function calcSizeByPageLayout(
  layout: LayoutInfo,
  slideAspectRatio: number,
  pageSize: { mmWidth: number; mmHeight: number },
  direction: PrintDirection,
  printDpi: number,
  scale = 100
): {
  size: { width: number; height: number }
  poses: Array<{ startX; startY; relIndex; isPlaceHolder?: boolean }>
} {
  const { mmWidth, mmHeight } = pageSize
  const pageDirection = mmWidth > mmHeight ? 'hor' : 'ver'
  const isHor = pageDirection === 'hor'
  // scale < 100 时缩小 slideGroup 尺寸, 大于 100 时仅缩放内容尺寸不变
  const pageScale = Math.min(scale, 100) / 100
  const printMM2Px = printDpi / 25.4
  const designRatio = printDpi / DesignDpi
  const pixPageWidth = mmWidth * printMM2Px
  const pixPageHeight = mmHeight * printMM2Px
  const [a4Width, a4Height] = PrintPageSize.A4
  const [coeffX, coeffY] = isHor
    ? [mmWidth / a4Height, mmHeight / a4Width]
    : [mmWidth / a4Width, mmHeight / a4Height]

  let slideWidth, slideHeight

  const isEavert = slideAspectRatio < 1
  const designA4SizePx = isEavert ? DesignEavertA4SizePx : DesignA4SizePx

  if (layout.type === 'slides') {
    ;[slideWidth, slideHeight] = designA4SizePx['slides'][pageDirection]
    if (isEavert) {
      slideWidth = slideHeight * slideAspectRatio
    }
    ;[slideWidth, slideHeight] = [
      (slideWidth * coeffX * pageScale * designRatio) >> 0,
      (slideHeight * coeffY * pageScale * designRatio) >> 0,
    ]

    const startX = ((pixPageWidth - slideWidth) / 2) >> 0
    const startY = ((pixPageHeight - slideHeight) / 2) >> 0
    const size = {
      width: slideWidth,
      height: slideHeight,
    }
    return { size, poses: [{ startX, startY, relIndex: 0 }] }
  } else {
    const basicPagePadding = DefaultA4InnerPadding[pageDirection] * designRatio
    const diffX = (pixPageWidth * (1 - pageScale)) / 2
    const diffY = (pixPageHeight * (1 - pageScale)) / 2
    const pagePaddingX = Math.max(0, basicPagePadding * coeffX - diffX)
    const pagePaddingY = Math.max(0, basicPagePadding * coeffY - diffY)
    const contentWidth = pixPageWidth * pageScale - pagePaddingX * 2
    const contentHeight = pixPageHeight * pageScale - pagePaddingY * 2
    const { slidesPerPage: n, needPlaceHolder } = layout
    ;[slideWidth, slideHeight] = designA4SizePx['handouts'][n][pageDirection]
    if (isEavert) {
      slideWidth = slideHeight * slideAspectRatio
    }
    ;[slideWidth, slideHeight] = [
      (slideWidth * coeffX * pageScale * designRatio) >> 0,
      (slideHeight * coeffY * pageScale * designRatio) >> 0,
    ]

    const size = { width: slideWidth, height: slideHeight }
    const poses: Array<{ startX; startY; relIndex; isPlaceHolder? }> = []

    const [hCount, vCount] = calcHandoutsLayout(n, isHor, needPlaceHolder)
    const totalCount = hCount * vCount
    const totalInnerPaddingX = contentWidth - slideWidth * hCount
    const totalInnerPaddingY = contentHeight - slideHeight * vCount
    const innerPaddingX = totalInnerPaddingX / (2 * hCount)
    const innerPaddingY = totalInnerPaddingY / (2 * vCount)

    let mappedRelIndex = 0
    const mappingIndexes = calcHandoutsSortByDirection(
      n,
      mmWidth >= mmHeight,
      direction
    )
    for (let i = 0; i < totalCount; i++) {
      const xIndex = i % hCount
      const yIndex = Math.floor(i / hCount)
      const startX =
        (diffX +
          pagePaddingX +
          (1 + 2 * xIndex) * innerPaddingX +
          xIndex * slideWidth) >>
        0
      const startY =
        (diffY +
          pagePaddingY +
          (1 + 2 * yIndex) * innerPaddingY +
          yIndex * slideHeight) >>
        0
      const isPlaceHolder =
        n === 3 && ((isHor && yIndex === 1) || (!isHor && xIndex === 1))
      const relIndex = mappingIndexes[mappedRelIndex]
      poses.push({ startX, startY, isPlaceHolder, relIndex })
      if (!isPlaceHolder) mappedRelIndex++
    }
    return { size, poses }
  }
}

/** 计算 layout 矩阵大小 */
function calcHandoutsLayout(
  slidePerPage: PrintSlideCountPerPage,
  isHor: boolean,
  needPlaceHolder = true
) {
  switch (slidePerPage) {
    case 1:
      return [1, 1]
    case 2:
      return isHor ? [2, 1] : [1, 2]
    case 3:
      if (needPlaceHolder) {
        return isHor ? [3, 2] : [2, 3]
      } else {
        return isHor ? [3, 1] : [1, 3]
      }
    case 6:
      return isHor ? [3, 2] : [2, 3]
    case 4:
      return [2, 2]
    case 9:
      return [3, 3]
  }
}

/** 要将选中 slide 的 index 顺序映射为 poses 内的坐标 */
function calcHandoutsSortByDirection(
  slidePerPage: PrintSlideCountPerPage,
  isHor: boolean,
  dir: PrintDirection
) {
  const [hCount, vCount] = calcHandoutsLayout(slidePerPage, isHor, false)

  const posMatrix = Array(vCount)
    .fill(0)
    .map((_i) => Array(hCount).fill(0)) as number[][]
  let posIndex = 0
  switch (dir) {
    case 'br':
      for (let colIndex = 0; colIndex < hCount; colIndex++) {
        for (let rowIndex = 0; rowIndex < vCount; rowIndex++) {
          posMatrix[rowIndex][colIndex] = posIndex++
        }
      }
      break
    case 'bl':
      for (let colIndex = hCount - 1; colIndex >= 0; colIndex--) {
        for (let rowIndex = 0; rowIndex < vCount; rowIndex++) {
          posMatrix[rowIndex][colIndex] = posIndex++
        }
      }
      break
    case 'lb':
      for (let rowIndex = 0; rowIndex < vCount; rowIndex++) {
        for (let colIndex = hCount - 1; colIndex >= 0; colIndex--) {
          posMatrix[rowIndex][colIndex] = posIndex++
        }
      }
      break
    case 'rb':
      for (let rowIndex = 0; rowIndex < vCount; rowIndex++) {
        for (let colIndex = 0; colIndex < hCount; colIndex++) {
          posMatrix[rowIndex][colIndex] = posIndex++
        }
      }
      break
  }
  return posMatrix.reduce((acc, row) => acc.concat(row), [])
}

export function getPrintSlideIndexes(
  editor: IEditorKit,
  isPrintHidden = false,
  rangeType: PrintRangeType
) {
  const { presentation } = editor
  const { slides } = presentation
  const canPrintSlide = (slide: Slide) => {
    return slide.isVisible() || isPrintHidden
  }

  switch (rangeType) {
    case 'all':
      const filter = (slides: Slide[]) => {
        return slides
          .map((slide, i) => {
            if (canPrintSlide(slide)) {
              return i
            }
          })
          .filter((item) => item !== undefined) as number[]
      }
      return filter(slides)
    case 'selected':
      const targets: number[] = []
      const indexes = presentation.getSelectedSlideIndices()
      for (let i = 0; i < slides.length; i++) {
        if (indexes.includes(i) && slides[i] && canPrintSlide(slides[i])) {
          targets.push(i)
        }
      }
      return targets
    case 'current':
      const curIndex = presentation.currentSlideIndex
      return canPrintSlide(slides[curIndex]) ? [curIndex] : []
  }
}

export function getPrintSlideIndexesAtPage(
  editor: IEditorKit,
  pageIndex: number,
  settings: PrintLayoutSettings = {
    layout: { type: 'handouts', slidesPerPage: 3 },
    pageSize: { mmWidth: 210, mmHeight: 297 },
    direction: 'rb',
    needBorder: false,
    isPrintHidden: false,
    scaleByPage: 100,
    rangeType: 'all',
  }
) {
  const slidePerPage =
    settings.layout.type === 'handouts' ? settings.layout.slidesPerPage : 1
  const slideIndexes = getPrintSlideIndexes(
    editor,
    settings.isPrintHidden,
    settings.rangeType
  )

  const startRelIndex = pageIndex * slidePerPage
  const endRelIndex = (pageIndex + 1) * slidePerPage
  return slideIndexes.slice(startRelIndex, endRelIndex)
}
