import { Nullable } from '../../../../liber/pervasive'
import {
  ManagedArray,
  ManagedSlice,
  ManagedSliceUtil,
} from '../../../common/managedArray'
import { checkAlmostEqual } from '../../../core/common/utils'
import { SnapPos } from './type'
import { updateSnapPosByCheckMinDiff } from './utils'

export type SnapXYPosCheckerCosumer = (
  snapXPos: ManagedSlice<SnapPos>,
  snapYPos: ManagedSlice<SnapPos>,
  minDiffOfX: Nullable<number>,
  minDiffOfY: Nullable<number>
) => void

export class SnapXYPosChecker {
  snapXPos: ManagedArray<SnapPos> = new ManagedArray<SnapPos>()
  snapYPos: ManagedArray<SnapPos> = new ManagedArray<SnapPos>()
  private snapXPosZeroDiff: ManagedArray<SnapPos> = new ManagedArray<SnapPos>()
  private snapYPosZeroDiff: ManagedArray<SnapPos> = new ManagedArray<SnapPos>()
  minDiffOfX: Nullable<number>
  minDiffOfY: Nullable<number>
  private isCalcZeroDiff?: boolean
  constructor(absDir?: Nullable<number>) {
    this.isCalcZeroDiff = absDir == null
  }
  setAbsDir(absDir: Nullable<number>) {
    this.isCalcZeroDiff = absDir == null
  }

  reset(absDir?: Nullable<number>) {
    this.snapXPos.reset()
    this.snapYPos.reset()
    this.snapXPosZeroDiff.reset()
    this.snapYPosZeroDiff.reset()
    this.isCalcZeroDiff = absDir == null
    this. minDiffOfX = undefined
    this. minDiffOfY = undefined
  }

  checkSnapXPos(pos: SnapPos | ManagedSlice<SnapPos>, diff: number) {
    const isZeroDiff = checkAlmostEqual(0, diff, 0.01)
    if (!this.isCalcZeroDiff && isZeroDiff) {
      if (ManagedSliceUtil.isManagedSlice(pos)) {
        ManagedSliceUtil.forEach(pos, (_pos) =>
          this.snapXPosZeroDiff.push(_pos)
        )
      } else {
        this.snapXPosZeroDiff.push(pos)
      }
      return true
    }

    const { minDiff, isAdd } = updateSnapPosByCheckMinDiff(
      this.snapXPos,
      this.minDiffOfX,
      pos,
      diff
    )
    this.minDiffOfX = minDiff
    return isAdd
  }

  checkSnapYPos(pos: SnapPos | ManagedSlice<SnapPos>, diff: number) {
    const isZeroDiff = checkAlmostEqual(0, diff, 0.01)
    if (!this.isCalcZeroDiff && isZeroDiff) {
      if (ManagedSliceUtil.isManagedSlice(pos)) {
        ManagedSliceUtil.forEach(pos, (_pos) =>
          this.snapYPosZeroDiff.push(_pos)
        )
      } else {
        this.snapYPosZeroDiff.push(pos)
      }
      return true
    }
    const { minDiff, isAdd } = updateSnapPosByCheckMinDiff(
      this.snapYPos,
      this.minDiffOfY,
      pos,
      diff
    )
    this.minDiffOfY = minDiff
    return isAdd
  }

  addToSnapXPos(pos: SnapPos) {
    this.snapXPos.push(pos)
  }

  addToSnapYPos(pos: SnapPos) {
    this.snapYPos.push(pos)
  }

  eachX(iter: (pos: SnapPos) => void) {
    ManagedSliceUtil.forEach(this.snapXPos, iter)
  }
  eachY(iter: (pos: SnapPos) => void) {
    ManagedSliceUtil.forEach(this.snapYPos, iter)
  }

  consumeInfo(consumer: SnapXYPosCheckerCosumer) {
    if (this.minDiffOfX == null && this.snapXPosZeroDiff.length) {
      this.minDiffOfX = 0
    }
    if (this.minDiffOfY == null && this.snapYPosZeroDiff.length) {
      this.minDiffOfY = 0
    }
    if (this.snapXPosZeroDiff.length > 0) {
      ManagedSliceUtil.forEach(this.snapXPosZeroDiff, (pos) => {
        this.snapXPos.push(pos)
      })
    }
    if (this.snapYPosZeroDiff.length > 0) {
      ManagedSliceUtil.forEach(this.snapYPosZeroDiff, (pos) => {
        this.snapYPos.push(pos)
      })
    }
    consumer(this.snapXPos, this.snapYPos, this.minDiffOfX, this.minDiffOfY)
    this.reset()
  }
}

let _checker: undefined | SnapXYPosChecker
export function getPosChecker(absDir?: Nullable<number>, isCreate = false) {
  if (isCreate) return new SnapXYPosChecker(absDir)

  if (_checker == null) {
    _checker = new SnapXYPosChecker(absDir)
  } else {
    _checker.reset(absDir)
  }
  return _checker
}
