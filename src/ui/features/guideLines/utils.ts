import { Nullable } from '../../../../liber/pervasive'
import {
  ManagedArray,
  ManagedSlice,
  ManagedSliceUtil,
} from '../../../common/managedArray'
import { isFiniteNumber } from '../../../common/utils'
import { ABS_DIRECTION } from '../../../core/common/const/attrs'
import { checkAlmostEqual } from '../../../core/common/utils'
import { SlideElement } from '../../../core/SlideElement/type'
import { SnapInfo, SnapPos } from './type'

export const MinDistanceForSnap = 1.27

export const uniqueArray = (arr: any[]) =>
  arr.filter((item, i) => arr.indexOf(item) === i)

export function updateSnapPosByCheckMinDiff(
  snapPosArr: ManagedArray<SnapPos>,
  minDiff: Nullable<number>,
  pos: SnapPos | ManagedSlice<SnapPos>,
  diff: number
) {
  let isAdd = false
  let destPos: SnapPos | ManagedSlice<SnapPos> | undefined
  if (Math.abs(diff) <= MinDistanceForSnap) {
    if (minDiff == null) {
      minDiff = diff
      destPos = pos
    } else {
      if (checkAlmostEqual(minDiff, diff, 0.01)) {
        destPos = pos
      } else if (Math.abs(minDiff) > Math.abs(diff)) {
        minDiff = diff
        snapPosArr.reset()
        destPos = pos
      }
    }
    if (destPos) {
      if (ManagedSliceUtil.isManagedSlice(destPos)) {
        ManagedSliceUtil.forEach(destPos, (pos) => {
          if (isFiniteNumber(pos.pos)) {
            snapPosArr.push(pos)
          }
        })
      } else if (isFiniteNumber(destPos.pos)) {
        snapPosArr.push(destPos)
      }
      isAdd = true
    }
  }

  return {
    minDiff,
    isAdd,
  }
}

export function getMinAndMaxArrayValue(
  snapArrays: Array<number[] | number>
): [number, number] {
  let minV = Number.MAX_VALUE,
    maxV = Number.MIN_VALUE
  for (let i = 0, l = snapArrays.length; i < l; i++) {
    const item = snapArrays[i]
    if (Array.isArray(item)) {
      const [_minV, _maxV] = getMinAndMaxArrayValue(item)
      if (_minV < minV) minV = _minV
      if (_maxV > maxV) maxV = _maxV
    } else {
      if (item > maxV) maxV = item
      if (item < minV) minV = item
    }
  }

  return [minV, maxV]
}

export function reduceMinAndMaxValue<T>(
  arr: T[],
  getMinAndMaxV: (item: T) => [number, number]
): [number, number] {
  let minV = Number.MAX_VALUE,
    maxV = Number.MIN_VALUE
  for (let i = 0, l = arr.length; i < l; i++) {
    const [_minV, _maxV] = getMinAndMaxV(arr[i])
    if (_minV < minV) minV = _minV
    if (_maxV > maxV) maxV = _maxV
  }

  return [minV, maxV]
}

export const checkClosestSnapPosition = (
  pos: number,
  targetPosArr: number[]
): Nullable<SnapInfo> => {
  let diff: number | undefined
  let ret: SnapInfo | undefined
  targetPosArr.forEach((targetPos) => {
    const dy = targetPos - pos
    if (diff == null) {
      diff = dy
      ret = { dist: dy, pos: { pos: targetPos } }
    } else {
      if (Math.abs(dy) < Math.abs(diff)) {
        diff = dy
        ret = { dist: dy, pos: { pos: targetPos } }
      }
    }
  })
  return ret
}

export const getSeparateDirByAbsDir = (absDir: number) => {
  let isWDir = false
  let isEDir = false
  let isNDir = false
  let isSDir = false
  switch (absDir) {
    case ABS_DIRECTION.W:
      isWDir = true
      break
    case ABS_DIRECTION.E:
      isEDir = true
      break
    case ABS_DIRECTION.N:
      isNDir = true
      break
    case ABS_DIRECTION.S:
      isSDir = true
      break
    case ABS_DIRECTION.NW:
      isNDir = true
      isWDir = true
      break
    case ABS_DIRECTION.NE:
      isNDir = true
      isEDir = true
      break
    case ABS_DIRECTION.SW:
      isSDir = true
      isWDir = true
      break
    case ABS_DIRECTION.SE:
      isSDir = true
      isEDir = true
      break
  }
  return {
    isWDir,
    isEDir,
    isNDir,
    isSDir,
  }
}

export function checkBoundsOverlap(
  snapPosX: number[],
  snapPosY: number[],
  testSp: SlideElement
) {
  const [rl, rr] = getMinAndMaxArrayValue(snapPosX)
  const [rt, rb] = getMinAndMaxArrayValue(snapPosY)
  const [tl, tr] = getMinAndMaxArrayValue(testSp.snapXs)
  const [tt, tb] = getMinAndMaxArrayValue(testSp.snapYs)
  const d = 0.01
  const isOverlapY = !(tl > rr + d || tr < rl - d)
  const isOverlapX = !(tt > rb + d || tb < rt - d)

  return { isOverlapY, isOverlapX }
}

export function binsert<T>(arr: T[], item: T, cmp: (a: T, b: T) => -1 | 0 | 1) {
  const l = arr.length
  if (l === 0) {
    arr.push(item)
  } else if (l === 1) {
    const cmpR = cmp(arr[0], item)
    if (cmpR < 0) {
      arr.push(item)
    } else if (cmpR > 0) {
      arr.unshift(item)
    }
  } else {
    let startIdx = 0
    let endIdx = l - 1
    let handled = false
    while (endIdx > startIdx) {
      const midIdx = ((endIdx + startIdx) / 2) >> 0
      const midItem = arr[midIdx]
      const cmpR = cmp(midItem, item)
      if (cmpR === 0) {
        handled = true
        break
      } else if (cmpR < 0) {
        startIdx = midIdx + 1
      } else {
        endIdx = midIdx - 1
      }
    }

    if (handled === false && arr[startIdx]) {
      const cmpR = cmp(arr[startIdx], item)
      if (cmpR < 0) {
        arr.splice(startIdx + 1, 0, item)
      } else if (cmpR > 0) {
        arr.splice(startIdx, 0, item)
      }
    }
  }
}

export function cmpPosRange(r1: [number, number], r2: [number, number]) {
  let min1 = r1[0]
  let max1 = r1[1]
  if (min1 > max1) {
    const t = min1
    min1 = max1
    max1 = t
  }

  let min2 = r2[0]
  let max2 = r2[1]
  if (min2 > max2) {
    const t = min2
    min2 = max2
    max2 = t
  }

  const diffMin = min1 - min2
  if (Math.abs(diffMin) > 0.01) {
    return diffMin > 0 ? 1 : -1
  }

  const diffMax = max1 - max2
  if (Math.abs(diffMax) > 0.01) {
    return diffMax > 0 ? 1 : -1
  }

  return 0
}
