import { Nullable } from '../../../../liber/pervasive'
import { ManagedSlice } from '../../../common/managedArray'
import { SlideElement } from '../../../core/SlideElement/type'

export type SnapPos = {
  pos: number
  rangePos?: [number, number]
  isVertical?: boolean
  withArrow?: boolean
  color?: string
  offset?: number
}

export interface SnapInfo {
  dist: number
  pos: SnapPos
}

export type SnapCompareInfo = SnapInfo & { sps: SlideElement[] }

export interface ShapeSnapXYInfo {
  snapXs: number[]
  snapYs: number[]
  shape: SlideElement
}

export interface CheckedSnapXYInfo {
  snapXPos: ManagedSlice<SnapPos>
  snapYPos: ManagedSlice<SnapPos>
  minDiffOfX: Nullable<number>
  minDiffOfY: Nullable<number>
}
