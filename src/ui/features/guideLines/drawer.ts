import { isNumber } from '../../../common/utils'
import { EditorSkin } from '../../../core/common/const/drawing'
import { checkAlmostEqual } from '../../../core/common/utils'
import { EditorUI } from '../../editorUI/EditorUI'
import { convertInnerMMToPixPos } from '../../editorUI/utils/utils'
import { SnapPos } from './type'

export const Guideline_Distance_Offset = 5
export const Guideline_Align_Offset = 20
const Guideline_Min_Arrow_Length = 5

export function drawGuidelines(
  editorUI: EditorUI,
  guidelines: readonly SnapPos[]
) {
  const horLines = guidelines.filter((line) => !line.isVertical)
  const verLines = guidelines.filter((line) => line.isVertical)
  removeDuplicatedLines(horLines).forEach((line) => {
    drawHorizontalGuideLines(editorUI, line)
  })
  removeDuplicatedLines(verLines).forEach((line) => {
    drawVerticalGuideLines(editorUI, line)
  })
}

const drawVerticalGuideLines = (editorUI: EditorUI, xPos: SnapPos) => {
  const editLayer = editorUI.renderingController.editLayer
  const { pos, rangePos, withArrow } = xPos
  if (!rangePos) {
    const pixPos = convertInnerMMToPixPos(editorUI, pos, 0)
    editLayer.drawVerticalLine(pixPos.x, EditorSkin.Color_GuideLine)
  } else {
    const color = xPos.color
      ? xPos.color
      : withArrow
      ? EditorSkin.Color_GuideLine_Light
      : EditorSkin.Color_GuideLine
    const isShortArrowLine = checkShortArrowLine(xPos)
    const offset = isNumber(xPos.offset)
      ? xPos?.offset
      : withArrow
      ? 0
      : Guideline_Align_Offset
    const drawArrow = withArrow && !isShortArrowLine
    const startPos = convertInnerMMToPixPos(editorUI, pos, rangePos[0])
    const endPos = convertInnerMMToPixPos(editorUI, pos, rangePos[1])
    editLayer.vertLineFromPointToPoint(
      startPos.x,
      startPos.y,
      endPos.y,
      offset,
      color,
      drawArrow
    )
    editLayer.context.stroke()
  }
}
const drawHorizontalGuideLines = (editorUI: EditorUI, yPos: SnapPos) => {
  const editLayer = editorUI.renderingController.editLayer
  const { pos, rangePos, withArrow } = yPos
  if (!rangePos) {
    const pixPos = convertInnerMMToPixPos(editorUI, 0, pos)
    editLayer.drawHorizontalLine(pixPos.y, EditorSkin.Color_GuideLine)
  } else {
    const color = yPos.color
      ? yPos.color
      : withArrow
      ? EditorSkin.Color_GuideLine_Light
      : EditorSkin.Color_GuideLine
    const isShortArrowLine = checkShortArrowLine(yPos)
    const offset = isNumber(yPos.offset)
      ? yPos?.offset
      : withArrow
      ? 0
      : Guideline_Align_Offset
    const drawArrow = withArrow && !isShortArrowLine
    const startPos = convertInnerMMToPixPos(editorUI, rangePos[0], pos)
    const endPos = convertInnerMMToPixPos(editorUI, rangePos[1], pos)
    editLayer.horLineFromPointToPoint(
      startPos.y,
      startPos.x,
      endPos.x,
      offset,
      color,
      drawArrow
    )
    editLayer.context.stroke()
  }
}

const checkShortArrowLine = (line: SnapPos): boolean => {
  if (line.withArrow && line.rangePos) {
    const [minPos, maxPos] = line.rangePos
    if (Math.abs(maxPos - minPos) < Guideline_Min_Arrow_Length) {
      return true
    }
  }
  return false
}

/**
 * 合并重复的线，如有相交取并集
 */
const removeDuplicatedLines = (guidelines: SnapPos[]): SnapPos[] => {
  const throughLines = guidelines
    .filter((line) => !line.rangePos && !line.withArrow)
    .reduce((allLines, nextLine) => {
      if (
        !allLines.find((line) => checkAlmostEqual(line.pos, nextLine.pos, 0.01))
      ) {
        allLines.push(nextLine)
      }
      return allLines
    }, [] as SnapPos[])
  const rangeLines = guidelines.filter(
    (line) => line.rangePos && !line.withArrow
  )
  const arrowLines = guidelines.filter((line) => line.withArrow)
  const filteredRangeLines = rangeLines.reduce((allLines, nextLine) => {
    const equalThroughLine = throughLines.find((throughLine) =>
      checkAlmostEqual(throughLine.pos, nextLine.pos, 0.01)
    )
    if (!equalThroughLine) {
      const intersectLine = allLines.find((line) => {
        if (!checkAlmostEqual(line.pos, nextLine.pos, 0.01)) {
          return
        }
        const [startPos, endPos] = line.rangePos!
        const [startPosN, endPosN] = nextLine.rangePos!
        return (
          (startPosN >= startPos && startPosN < endPos) ||
          (endPosN >= startPos && endPosN < endPos)
        )
      })
      if (intersectLine) {
        const allPos = [...intersectLine.rangePos!, ...nextLine.rangePos!]
        intersectLine.rangePos = [Math.min(...allPos), Math.max(...allPos)]
      }
      if (!intersectLine) {
        allLines.push(nextLine)
      }
    }

    return allLines
  }, [] as SnapPos[])
  return [...throughLines, ...filteredRangeLines, ...arrowLines]
}
