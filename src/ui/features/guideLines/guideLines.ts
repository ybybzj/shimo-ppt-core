/* eslint-disable @typescript-eslint/no-unused-vars */
import { Nullable } from '../../../../liber/pervasive'
import {
  ManagedArray,
  ManagedSlice,
  ManagedSliceUtil,
} from '../../../common/managedArray'
import { isFiniteNumber } from '../../../common/utils'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { checkAlmostEqual } from '../../../core/common/utils'
import { Presentation } from '../../../core/Slide/Presentation'
import { SlideElement } from '../../../core/SlideElement/type'
import { EditorUI } from '../../editorUI/EditorUI'
import { drawGuidelines, Guideline_Distance_Offset } from './drawer'
import {
  getPosChecker,
  SnapXYPosChecker,
  SnapXYPosCheckerCosumer,
} from './SnapXYChecker'
import { ShapeSnapXYInfo, SnapCompareInfo, SnapPos } from './type'
import {
  getMinAndMaxArrayValue,
  checkClosestSnapPosition,
  getSeparateDirByAbsDir,
  MinDistanceForSnap,
  reduceMinAndMaxValue,
  updateSnapPosByCheckMinDiff,
  checkBoundsOverlap,
  binsert,
  cmpPosRange,
} from './utils'

function filterRelativeSpsForGuide(
  spTree: Nullable<SlideElement[]>,
  shapeSnapXYInfoArr: ShapeSnapXYInfo[]
) {
  if (spTree == null || spTree.length < 1) {
    return
  }

  const res = spTree.reduce(
    (res, testSp) => {
      for (let i = 0, l = shapeSnapXYInfoArr.length; i < l; i++) {
        const { shape: refSp, snapXs, snapYs } = shapeSnapXYInfoArr[i]
        if (refSp !== testSp) {
          const { isOverlapX, isOverlapY } = checkBoundsOverlap(
            snapXs,
            snapYs,
            testSp
          )

          if (isOverlapX || isOverlapY) {
            res.spTree.push(testSp)

            if (isOverlapX) {
              res.overlapX.push(testSp)
            }
            if (isOverlapY) {
              res.overlapY.push(testSp)
            }
            break
          }
        }
      }
      return res
    },
    {
      spTree: [] as SlideElement[],
      overlapX: [] as SlideElement[],
      overlapY: [] as SlideElement[],
    }
  )

  return res
}

const snapXYPosChecker = getPosChecker(null, true)
function checkConsumer(
  snapXPos: ManagedSlice<SnapPos>,
  snapYPos: ManagedSlice<SnapPos>,
  minDiffOfX: Nullable<number>,
  minDiffOfY: Nullable<number>
) {
  if (isFiniteNumber(minDiffOfX) && snapXPos) {
    snapXYPosChecker.checkSnapXPos(snapXPos, minDiffOfX!)
  }
  if (isFiniteNumber(minDiffOfY) && snapYPos) {
    snapXYPosChecker.checkSnapYPos(snapYPos, minDiffOfY!)
  }
}

export class GuidelinesManager {
  editorUI: EditorUI
  private guidelinePosArr: ManagedArray<SnapPos> = new ManagedArray<SnapPos>()
  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI
  }

  draw() {
    drawGuidelines(this.editorUI, this.guidelinePosArr.arraySlice)
    this.guidelinePosArr.reset()
  }

  addGuideLine(pos: SnapPos) {
    this.guidelinePosArr.push(pos)
  }

  checkGuidelines(
    _spTree: Nullable<SlideElement[]>,
    shapeSnapXYInfoArr: ShapeSnapXYInfo[],
    absDir?: Nullable<number>
  ): number[] {
    const filteredResult = filterRelativeSpsForGuide(
      _spTree,
      shapeSnapXYInfoArr
    )

    if (filteredResult == null) return [0, 0]

    const { spTree, overlapX, overlapY } = filteredResult
    const { snapSlideXPos, snapSlideYPos } = getSlideSnapPos(
      this.editorUI.presentation
    )

    if (EditorSettings.isAlignGuidelineEnabled) {
      checkAlignmentGuidelines(
        spTree,
        shapeSnapXYInfoArr,
        absDir,
        snapSlideXPos,
        snapSlideYPos,
        checkConsumer
      )
    }
    if (
      EditorSettings.isMeasureGuidelineEnabled &&
      spTree &&
      spTree.length > 0
    ) {
      // const { overlappedShapesX, overlappedShapesY } =
      //   checkRowOrColumnOverlappedShapes(spTree, shapeSnapXYInfoArr)
      // const allOverlappedShapes = [...overlappedShapesX, ...overlappedShapesY]

      const overlappedShapesX = overlapX!
      const overlappedShapesY = overlapY!

      if (absDir != null) {
        checkSizeGuidelines(spTree, shapeSnapXYInfoArr, absDir, checkConsumer)
      }

      checkDistanceGuidelinesForSlide(
        overlappedShapesX,
        overlappedShapesY,
        shapeSnapXYInfoArr,
        snapSlideXPos,
        snapSlideYPos,
        absDir,
        checkConsumer
      )
      checkElementDistanceGuidelines(
        overlappedShapesX,
        overlappedShapesY,
        shapeSnapXYInfoArr,
        absDir,
        checkConsumer
      )
    }

    let minDiffOfX, minDiffOfY
    snapXYPosChecker.consumeInfo(
      (snapXPos, snapYPos, _minDiffOfX, _minDiffOfY) => {
        minDiffOfX = _minDiffOfX
        minDiffOfY = _minDiffOfY
        if (minDiffOfX == null || Math.abs(minDiffOfX) > MinDistanceForSnap) {
          minDiffOfX = 0
        } else if (snapXPos.length > 0) {
          ManagedSliceUtil.forEach(snapXPos, (pos) => this.addGuideLine(pos))
        } else {
          minDiffOfX = 0
        }

        if (minDiffOfY == null || Math.abs(minDiffOfY) > MinDistanceForSnap) {
          minDiffOfY = 0
        } else if (snapYPos.length > 0) {
          ManagedSliceUtil.forEach(snapYPos, (pos) => this.addGuideLine(pos))
        } else {
          minDiffOfY = 0
        }
      }
    )
    return [minDiffOfX, minDiffOfY]
  }
}

function getSlideSnapPos(presentation: Presentation) {
  const { width: slideWidth, height: slideHeight } = presentation
  const snapSlideXPos = [0, slideWidth / 2, slideWidth]
  const snapSlideYPos = [0, slideHeight / 2, slideHeight]
  return {
    snapSlideXPos,
    snapSlideYPos,
  }
}

//checkers
function checkAlignmentGuidelines(
  spTree: Nullable<SlideElement[]>,
  shapeSnapXYInfoArr: ShapeSnapXYInfo[],
  absDir: Nullable<number>,
  snapSlideXPos: number[],
  snapSlideYPos: number[],
  consumer: SnapXYPosCheckerCosumer
) {
  const snapXYPosChecker = getPosChecker(absDir)
  // 水平
  shapeSnapXYInfoArr.forEach(({ snapXs, snapYs, shape }) => {
    snapXs.forEach((snapX) => {
      const snapInfo = checkClosestSnapPosition(snapX, snapSlideXPos)
      if (snapInfo) {
        snapInfo.pos.isVertical = true
        snapXYPosChecker.checkSnapXPos(snapInfo.pos, snapInfo.dist)
      }
    })
    if (spTree && spTree.length > 0) {
      getMinSnapXYInfoBySpTree(
        snapXs,
        spTree,
        true,
        (sp) => sp === shape,
        (snapInfo) => {
          snapXYPosChecker.checkSnapXPos(
            {
              pos: snapInfo.pos.pos,
              rangePos: getMinAndMaxArrayValue([
                snapYs,
                reduceMinAndMaxValue(snapInfo.sps, (o) =>
                  getMinAndMaxArrayValue(o.snapYs)
                ),
              ]),
              isVertical: true,
            },
            snapInfo.dist
          )
        }
      )
    }
  })

  // 垂直
  shapeSnapXYInfoArr.forEach(({ snapXs, snapYs, shape }) => {
    snapYs.forEach((snapY) => {
      const snapInfo = checkClosestSnapPosition(snapY, snapSlideYPos)
      if (snapInfo) {
        snapXYPosChecker.checkSnapYPos(snapInfo.pos, snapInfo.dist)
      }
    })

    if (spTree && spTree.length > 0) {
      getMinSnapXYInfoBySpTree(
        snapYs,
        spTree,
        false,
        (sp) => sp === shape,
        (snapInfo) => {
          const pos: SnapPos = {
            ...snapInfo.pos,
            rangePos: getMinAndMaxArrayValue([
              snapXs,
              reduceMinAndMaxValue(snapInfo.sps, (sp) =>
                getMinAndMaxArrayValue(sp.snapXs)
              ),
            ]),
          }
          snapXYPosChecker.checkSnapYPos(pos, snapInfo.dist)
        }
      )
    }
  })

  snapXYPosChecker.consumeInfo(consumer)
}

function checkSizeGuidelines(
  rowOrColumnOverlappedShapes: SlideElement[],
  shapeSnapXYInfoArr: ShapeSnapXYInfo[],
  absDir: number,
  consumer: SnapXYPosCheckerCosumer
) {
  const { isWDir, isEDir, isNDir, isSDir } = getSeparateDirByAbsDir(absDir)

  const snapXYPosChecker = getPosChecker(absDir)

  if (isWDir || isEDir) {
    checkXYPosFor(
      'w',
      rowOrColumnOverlappedShapes,
      shapeSnapXYInfoArr,
      isEDir,
      snapXYPosChecker
    )
  }
  if (isNDir || isSDir) {
    checkXYPosFor(
      'h',
      rowOrColumnOverlappedShapes,
      shapeSnapXYInfoArr,
      isSDir,
      snapXYPosChecker
    )
  }

  snapXYPosChecker.eachX((pos) => {
    pos.withArrow = true
    pos.isVertical = false
  })
  snapXYPosChecker.eachY((pos) => {
    pos.withArrow = true
    pos.isVertical = true
  })

  snapXYPosChecker.consumeInfo(consumer)
}

function checkDistanceGuidelinesForSlide(
  overlappedShapesX: SlideElement[],
  overlappedShapesY: SlideElement[],
  shapeSnapXYInfoArr: ShapeSnapXYInfo[],
  snapSlideXPos: number[],
  snapSlideYPos: number[],
  absDir: Nullable<number>,
  consumer: SnapXYPosCheckerCosumer
) {
  const [slideLeftXPos, slideCenterXPos, slideRightXPos] = snapSlideXPos
  const [slideTopYPos, slideCenterYPos, slideBottomYPos] = snapSlideYPos

  const snapXYPosChecker = getPosChecker(absDir)

  for (const { snapXs, snapYs } of shapeSnapXYInfoArr) {
    const [minXPos, maxXPos] = getMinAndMaxArrayValue(snapXs)
    const [minYPos, maxYPos] = getMinAndMaxArrayValue(snapYs)
    /*
     * 水平方向
     */
    for (const sp of overlappedShapesX) {
      if (minXPos > slideCenterXPos) {
        const diffSnapShapeXLeft = minXPos - slideCenterXPos
        const [_, r] = getMinAndMaxArrayValue(sp.snapXs)
        if (r < slideCenterXPos) {
          const w = slideCenterXPos - r
          const diff = w - diffSnapShapeXLeft
          const [minSpYPos, maxSpYPos] = getMinAndMaxArrayValue(sp.snapYs)
          const posY = maxSpYPos + Guideline_Distance_Offset
          const pos: SnapPos = {
            pos: posY,
            rangePos: [r, slideCenterXPos],
            withArrow: true,
          }
          const isAdd = snapXYPosChecker.checkSnapXPos(pos, diff)
          if (isAdd) {
            const pos = maxYPos + Guideline_Distance_Offset
            const rangeEndPos = slideCenterXPos + w
            snapXYPosChecker.addToSnapXPos({
              pos: maxYPos + Guideline_Distance_Offset,
              rangePos: [slideCenterXPos, rangeEndPos],
              withArrow: true,
            })

            // 辅助线与形状的对齐线
            snapXYPosChecker.addToSnapXPos({
              pos: r,
              rangePos: [minSpYPos, posY],
              isVertical: true,
              withArrow: false,
            })
            snapXYPosChecker.addToSnapXPos({
              pos: rangeEndPos,
              rangePos: [minYPos, pos],
              isVertical: true,
              withArrow: false,
            })
          }
        }
      } else if (maxXPos < slideCenterXPos) {
        const diffSnapShapeDist = slideCenterXPos - maxXPos
        const [l] = getMinAndMaxArrayValue(sp.snapXs)
        if (l > slideCenterXPos) {
          const w = l - slideCenterXPos
          const diff = diffSnapShapeDist - w
          const [minSpYPos, maxSpYPos] = getMinAndMaxArrayValue(sp.snapYs)
          const posY = maxSpYPos + Guideline_Distance_Offset
          const pos: SnapPos = {
            pos: posY,
            rangePos: [slideCenterXPos, l],
            withArrow: true,
          }
          const isAdd = snapXYPosChecker.checkSnapXPos(pos, diff)
          if (isAdd) {
            const pos = maxYPos + Guideline_Distance_Offset
            const rangeStartPos = slideCenterXPos - w
            snapXYPosChecker.addToSnapXPos({
              pos,
              rangePos: [rangeStartPos, slideCenterXPos],
              withArrow: true,
            })

            // 辅助线与形状的对齐线
            snapXYPosChecker.addToSnapXPos({
              pos: l,
              rangePos: [minSpYPos, posY],
              isVertical: true,
              withArrow: false,
            })
            snapXYPosChecker.addToSnapXPos({
              pos: rangeStartPos,
              rangePos: [minYPos, pos],
              isVertical: true,
              withArrow: false,
            })
          }
        }
      }

      if (maxXPos > slideCenterXPos) {
        const diffSnapShapeXRight = slideRightXPos - maxXPos
        const [l] = getMinAndMaxArrayValue(sp.snapXs)
        if (l < slideCenterXPos) {
          const w = l - slideLeftXPos
          const diff = diffSnapShapeXRight - w
          const [minSpYPos, maxSpYPos] = getMinAndMaxArrayValue(sp.snapYs)
          const posY = maxSpYPos + Guideline_Distance_Offset
          const pos: SnapPos = {
            pos: posY,
            rangePos: [slideLeftXPos, l],
            withArrow: true,
          }
          const isAdd = snapXYPosChecker.checkSnapXPos(pos, diff)
          if (isAdd) {
            const rangeStartPos = slideRightXPos - w
            const pos = maxYPos + Guideline_Distance_Offset
            snapXYPosChecker.addToSnapXPos({
              pos,
              rangePos: [rangeStartPos, slideRightXPos],
              withArrow: true,
            })

            // 辅助线与形状的对齐线
            snapXYPosChecker.addToSnapXPos({
              pos: l,
              rangePos: [minSpYPos, posY],
              isVertical: true,
              withArrow: false,
            })
            snapXYPosChecker.addToSnapXPos({
              pos: rangeStartPos,
              rangePos: [minYPos, pos],
              isVertical: true,
              withArrow: false,
            })
          }
        }
      } else if (minXPos < slideCenterXPos) {
        const diffSnapShapeDist = minXPos - slideLeftXPos
        const [_, r] = getMinAndMaxArrayValue(sp.snapXs)
        if (r > slideCenterXPos) {
          const w = slideRightXPos - r
          const diff = w - diffSnapShapeDist
          const [minSpYPos, maxSpYPos] = getMinAndMaxArrayValue(sp.snapYs)
          const posY = maxSpYPos + Guideline_Distance_Offset

          const pos: SnapPos = {
            pos: posY,
            rangePos: [r, slideRightXPos],
            withArrow: true,
          }
          const isAdd = snapXYPosChecker.checkSnapXPos(pos, diff)
          if (isAdd) {
            const rangeEndPos = slideLeftXPos + w
            const pos = maxYPos + Guideline_Distance_Offset
            snapXYPosChecker.addToSnapXPos({
              pos,
              rangePos: [slideLeftXPos, slideLeftXPos + w],
              withArrow: true,
            })

            // 辅助线与形状的对齐线
            snapXYPosChecker.addToSnapXPos({
              pos: r,
              rangePos: [minSpYPos, posY],
              isVertical: true,
              withArrow: false,
            })
            snapXYPosChecker.addToSnapXPos({
              pos: rangeEndPos,
              rangePos: [minYPos, pos],
              isVertical: true,
              withArrow: false,
            })
          }
        }
      }
    }

    /*
     * 垂直方向
     */

    for (const sp of overlappedShapesY) {
      if (minYPos > slideCenterYPos) {
        const diffSnapShapeYLeft = minYPos - slideCenterYPos
        const [_, b] = getMinAndMaxArrayValue(sp.snapYs)
        if (b < slideCenterYPos) {
          const w = slideCenterYPos - b
          const diff = w - diffSnapShapeYLeft
          const [minSpXPos, maxSpXPos] = getMinAndMaxArrayValue(sp.snapXs)
          const posX = maxSpXPos + Guideline_Distance_Offset
          const pos: SnapPos = {
            pos: posX,
            rangePos: [b, slideCenterYPos],
            isVertical: true,
            withArrow: true,
          }
          const isAdd = snapXYPosChecker.checkSnapYPos(pos, diff)
          if (isAdd) {
            const rangeEndPos = slideCenterYPos + w
            const pos = maxXPos + Guideline_Distance_Offset
            snapXYPosChecker.addToSnapYPos({
              pos: pos,
              rangePos: [slideCenterYPos, rangeEndPos],
              isVertical: true,
              withArrow: true,
            })

            // 辅助线与形状的对齐线
            snapXYPosChecker.addToSnapYPos({
              pos: b,
              rangePos: [minSpXPos, posX],
              withArrow: false,
            })
            snapXYPosChecker.addToSnapYPos({
              pos: rangeEndPos,
              rangePos: [minYPos, pos],
              withArrow: false,
            })
          }
        }
      } else if (maxYPos < slideCenterYPos) {
        const diffSnapShapeDist = slideCenterYPos - maxYPos
        const [t] = getMinAndMaxArrayValue(sp.snapYs)
        if (t > slideCenterYPos) {
          const w = t - slideCenterYPos
          const diff = diffSnapShapeDist - w
          const [minSpXPos, maxSpXPos] = getMinAndMaxArrayValue(sp.snapXs)
          const posX = maxSpXPos + Guideline_Distance_Offset
          const pos: SnapPos = {
            pos: posX,
            rangePos: [slideCenterYPos, t],
            isVertical: true,
            withArrow: true,
          }
          const isAdd = snapXYPosChecker.checkSnapYPos(pos, diff)
          if (isAdd) {
            const rangeStartPos = slideCenterYPos - w
            const pos = maxXPos + Guideline_Distance_Offset
            snapXYPosChecker.addToSnapYPos({
              pos,
              rangePos: [slideCenterYPos - w, slideCenterYPos],
              isVertical: true,
              withArrow: true,
            })

            // 辅助线与形状的对齐线
            snapXYPosChecker.addToSnapYPos({
              pos: t,
              rangePos: [minSpXPos, posX],
              withArrow: false,
            })
            snapXYPosChecker.addToSnapYPos({
              pos: rangeStartPos,
              rangePos: [minYPos, pos],
              withArrow: false,
            })
          }
        }
      }

      if (maxYPos > slideCenterYPos) {
        const diffSnapShapeXRight = slideBottomYPos - maxYPos
        const [t] = getMinAndMaxArrayValue(sp.snapYs)
        if (t < slideCenterYPos) {
          const w = t - slideTopYPos
          const diff = diffSnapShapeXRight - w
          const [minSpXPos, maxSpXPos] = getMinAndMaxArrayValue(sp.snapXs)
          const posX = maxSpXPos + Guideline_Distance_Offset
          const pos: SnapPos = {
            pos: posX,
            rangePos: [slideTopYPos, t],
            isVertical: true,
            withArrow: true,
          }
          const isAdd = snapXYPosChecker.checkSnapYPos(pos, diff)
          if (isAdd) {
            const rangeStartPos = slideBottomYPos - w
            const pos = maxXPos + Guideline_Distance_Offset
            snapXYPosChecker.addToSnapYPos({
              pos: maxXPos + Guideline_Distance_Offset,
              rangePos: [rangeStartPos, slideBottomYPos],
              isVertical: true,
              withArrow: true,
            })

            // 辅助线与形状的对齐线
            snapXYPosChecker.addToSnapYPos({
              pos: t,
              rangePos: [minSpXPos, posX],
              withArrow: false,
            })
            snapXYPosChecker.addToSnapYPos({
              pos: rangeStartPos,
              rangePos: [minYPos, pos],
              withArrow: false,
            })
          }
        }
      } else if (minYPos < slideCenterYPos) {
        /*
         * 当前操作形状左边在幻灯片中线左侧，形状左边到幻灯片左边的距离
         */
        const diffSnapShapeDist = minYPos - slideTopYPos
        const [_, b] = getMinAndMaxArrayValue(sp.snapYs)
        if (b > slideCenterYPos) {
          const w = slideBottomYPos - b
          const diff = w - diffSnapShapeDist
          const [minSpXPos, maxSpXPos] = getMinAndMaxArrayValue(sp.snapXs)
          const posX = maxSpXPos + Guideline_Distance_Offset
          const pos: SnapPos = {
            pos: posX,
            rangePos: [b, slideBottomYPos],
            isVertical: true,
            withArrow: true,
          }
          const isAdd = snapXYPosChecker.checkSnapYPos(pos, diff)
          if (isAdd) {
            const rangeEndPos = slideTopYPos + w
            const pos = maxXPos + Guideline_Distance_Offset
            snapXYPosChecker.addToSnapYPos({
              pos,
              rangePos: [slideTopYPos, slideTopYPos + w],
              isVertical: true,
              withArrow: true,
            })

            // 辅助线与形状的对齐线
            snapXYPosChecker.addToSnapYPos({
              pos: b,
              rangePos: [minSpXPos, posX],
              withArrow: false,
            })
            snapXYPosChecker.addToSnapYPos({
              pos: rangeEndPos,
              rangePos: [minYPos, pos],
              withArrow: false,
            })
          }
        }
      }
    }
  }

  if (snapXYPosChecker.snapXPos.length) {
    snapXYPosChecker.addToSnapXPos({
      pos: slideCenterXPos,
      isVertical: true,
    })
  }
  if (snapXYPosChecker.snapYPos.length) {
    snapXYPosChecker.addToSnapYPos({
      pos: slideCenterYPos,
      isVertical: false,
    })
  }
  snapXYPosChecker.consumeInfo(consumer)
}

interface ShapeDistancePosInfo {
  dist: number
  pos: SnapPos
  alignPosArr: SnapPos[]
  isLeftOrUpper: boolean
}

const _snapPosTemp: ManagedArray<SnapPos> = new ManagedArray<SnapPos>()

function handleInfoCheck(
  compareDistInfo: ShapeDistancePosInfo,
  distInfo: ShapeDistancePosInfo,
  minDiffVal: Nullable<number>,
  snapPoses: ManagedArray<SnapPos>
) {
  const diff = distInfo.isLeftOrUpper
    ? distInfo.dist - compareDistInfo.dist
    : compareDistInfo.dist - distInfo.dist
  const pos = compareDistInfo.pos
  const { minDiff, isAdd } = updateSnapPosByCheckMinDiff(
    snapPoses,
    minDiffVal,
    pos,
    diff
  )

  if (isAdd) {
    compareDistInfo.alignPosArr.forEach((alignPos) => {
      _snapPosTemp.push(alignPos)
    })
  }

  return minDiff
}
function checkElementDistanceGuidelines(
  overlappedShapesX: SlideElement[],
  overlappedShapesY: SlideElement[],
  shapeSnapXYInfoArr: ShapeSnapXYInfo[],
  absDir: Nullable<number>,
  consumer: SnapXYPosCheckerCosumer
) {
  if (overlappedShapesX.length < 2 && overlappedShapesY.length < 2) {
    return
  }

  const snapXYPosChecker = getPosChecker(absDir)

  const overlappedShapesDistanceX = _calcOverlappedShapesDistances(
    true,
    overlappedShapesX,
    calcDistanceBySnapPos
  )
  const overlappedShapesDistanceY = _calcOverlappedShapesDistances(
    false,
    overlappedShapesY,
    calcDistanceBySnapPos
  )

  shapeSnapXYInfoArr.forEach(({ snapXs, snapYs }) => {
    /*
     * 水平方向
     */
    const snapShapeToOverlappedShapesDistInfosForHorizontal =
      overlappedShapesX.reduce((res, sp) => {
        const info = calcDistanceBySnapPos(
          true,
          { snapXs: sp.snapXs, snapYs: sp.snapYs },
          { snapXs, snapYs }
        )

        if (info?.pos?.rangePos != null) {
          binsert(res, info, (info1, info2) =>
            cmpPosRange(info1.pos.rangePos!, info2.pos.rangePos!)
          )
        }
        return res
      }, [] as ShapeDistancePosInfo[])

    snapShapeToOverlappedShapesDistInfosForHorizontal.forEach((distInfo) => {
      let minDiffOfTemp
      _snapPosTemp.reset()

      for (const compareDistInfo of snapShapeToOverlappedShapesDistInfosForHorizontal) {
        if (compareDistInfo === distInfo) continue
        minDiffOfTemp = handleInfoCheck(
          compareDistInfo,
          distInfo,
          minDiffOfTemp,
          _snapPosTemp
        )
      }
      for (const compareDistInfo of overlappedShapesDistanceX) {
        minDiffOfTemp = handleInfoCheck(
          compareDistInfo,
          distInfo,
          minDiffOfTemp,
          _snapPosTemp
        )
      }

      correctFinalSnapPos(distInfo, minDiffOfTemp)

      _snapPosTemp.push(distInfo.pos)
      distInfo.alignPosArr.forEach((alignPos) => {
        _snapPosTemp.push(alignPos)
      })
      snapXYPosChecker.checkSnapXPos(_snapPosTemp, minDiffOfTemp)
    })
    /*
     * 垂直方向
     */
    const snapShapeToOverlappedShapesDistInfosForVertical =
      overlappedShapesY.reduce((res, sp) => {
        const info = calcDistanceBySnapPos(
          false,
          { snapXs: sp.snapXs, snapYs: sp.snapYs },
          { snapXs, snapYs }
        )

        if (info?.pos?.rangePos != null) {
          binsert(res, info, (info1, info2) =>
            cmpPosRange(info1.pos.rangePos!, info2.pos.rangePos!)
          )
        }
        return res
      }, [] as ShapeDistancePosInfo[])

    snapShapeToOverlappedShapesDistInfosForVertical.forEach((distInfo) => {
      _snapPosTemp.reset()
      let minDiffOfTemp

      for (const compareDistInfo of snapShapeToOverlappedShapesDistInfosForVertical) {
        if (compareDistInfo === distInfo) continue
        minDiffOfTemp = handleInfoCheck(
          compareDistInfo,
          distInfo,
          minDiffOfTemp,
          _snapPosTemp
        )
      }
      for (const compareDistInfo of overlappedShapesDistanceY) {
        minDiffOfTemp = handleInfoCheck(
          compareDistInfo,
          distInfo,
          minDiffOfTemp,
          _snapPosTemp
        )
      }

      correctFinalSnapPos(distInfo, minDiffOfTemp)
      _snapPosTemp.push(distInfo.pos)
      distInfo.alignPosArr.forEach((alignPos) => {
        _snapPosTemp.push(alignPos)
      })

      snapXYPosChecker.checkSnapYPos(_snapPosTemp, minDiffOfTemp)
    })
  })

  snapXYPosChecker.consumeInfo(consumer)
}

function checkXYPosFor(
  wOrH: 'w' | 'h',
  sps: SlideElement[],
  shapeSnapXYInfoArr: ShapeSnapXYInfo[],
  isDirOpposit: boolean, // E/S is true
  checker: SnapXYPosChecker
) {
  const isCheckW = wOrH === 'w'
  shapeSnapXYInfoArr.forEach(({ snapXs, snapYs }) => {
    const snapVs = isCheckW ? snapXs : snapYs
    const [minV, maxV] = getMinAndMaxArrayValue(snapVs)
    const dist = maxV - minV

    sps.forEach((sp) => {
      const spSnapVs = isCheckW ? sp.snapXs : sp.snapYs
      const [spMinV, spMaxV] = getMinAndMaxArrayValue(spSnapVs)
      const spDist = spMaxV - spMinV
      const diff = isDirOpposit ? spDist - dist : dist - spDist

      const posToAdd = isCheckW ? sp.snapYs[2] : sp.snapXs[2]

      const pos: SnapPos = {
        pos: posToAdd,
        rangePos: getMinAndMaxArrayValue(spSnapVs),
      }
      if (isCheckW) {
        checker.checkSnapXPos(pos, diff)
      } else {
        checker.checkSnapYPos(pos, diff)
      }
    })

    const checkSnapPos = isCheckW ? checker.snapXPos : checker.snapYPos
    const rangePosAtFirst = checkSnapPos.at(0)?.rangePos
    if (rangePosAtFirst) {
      const spRangeDiff = rangePosAtFirst[1] - rangePosAtFirst[0]
      let rangePos: [number, number] = [minV, maxV]

      if (isDirOpposit) {
        rangePos = [minV, minV + spRangeDiff]
      } else {
        rangePos = [maxV - spRangeDiff, maxV]
      }
      const snapShapePos: SnapPos = {
        pos: isCheckW ? snapYs[2] : snapXs[2],
        rangePos,
      }
      if (isCheckW) {
        checker.addToSnapXPos(snapShapePos)
      } else {
        checker.addToSnapYPos(snapShapePos)
      }
    }
  })
}

// helpers

const _tempSnapCompareInfos: ManagedArray<SnapCompareInfo> =
  new ManagedArray<SnapCompareInfo>()
function getMinSnapXYInfoBySpTree(
  snapPosV: number[],
  sps: SlideElement[],
  isDx: boolean,
  needIgnore: (sp: SlideElement) => boolean,
  iter: (info: SnapCompareInfo) => void
) {
  const [minPosV, maxPosV] = getMinAndMaxArrayValue(snapPosV)
  const diffPos = maxPosV - minPosV
  snapPosV.forEach((posV) => {
    let tempInfo: SnapCompareInfo | undefined
    for (let i = 0, l = sps.length; i < l; ++i) {
      const sp = sps[i]
      if (needIgnore && needIgnore(sp)) {
        continue
      }

      const snapVs = isDx ? sp.snapXs : sp.snapYs
      const [minV, maxV] = getMinAndMaxArrayValue(snapVs)
      const d = maxV - minV

      const isSizeEqual = checkAlmostEqual(d, diffPos, 0.01)

      if (!isSizeEqual && snapVs.length > 0) {
        const { dist, snapPos } = snapVs.reduce(
          (res, snapPos) => {
            const d = Math.abs(snapPos - posV)
            if (res.dist === -1 || res.dist > d) {
              res.dist = d
              res.snapPos = snapPos
            }
            return res
          },
          {
            dist: -1,
            snapPos: -1,
          } as { dist: number; snapPos: number }
        )

        if (dist !== -1) {
          if (tempInfo == null) {
            tempInfo = {
              dist,
              pos: { pos: snapPos },
              sps: [sp],
            }
          } else if (Math.abs(dist - tempInfo.dist) < 0.1) {
            if (!tempInfo.sps.includes(sp)) {
              tempInfo.sps.push(sp)
            }
          } else if (dist < tempInfo.dist) {
            tempInfo.dist = dist
            tempInfo.pos.pos = snapPos
            tempInfo.sps = [sp]
          }
        }
      }
    }
    if (tempInfo) {
      _tempSnapCompareInfos.push(tempInfo)
    }
  })
  ManagedSliceUtil.forEach(_tempSnapCompareInfos, iter)
  _tempSnapCompareInfos.reset()
}

function calcDistanceBySnapPos(
  isHor: boolean,
  shapeSnapXYsA: { snapXs: number[]; snapYs: number[] },
  shapeSnapXYsB: { snapXs: number[]; snapYs: number[] }
): ShapeDistancePosInfo | undefined {
  const [minXPosA, maxXPosA] = getMinAndMaxArrayValue(shapeSnapXYsA.snapXs)
  const [minYPosA, maxYPosA] = getMinAndMaxArrayValue(shapeSnapXYsA.snapYs)
  const [minXPosB, maxXPosB] = getMinAndMaxArrayValue(shapeSnapXYsB.snapXs)
  const [minYPosB, maxYPosB] = getMinAndMaxArrayValue(shapeSnapXYsB.snapYs)
  const [minPosA, maxPosA] = isHor ? [minXPosA, maxXPosA] : [minYPosA, maxYPosA]
  const [minPosB, maxPosB] = isHor ? [minXPosB, maxXPosB] : [minYPosB, maxYPosB]

  const maxYPosAB = Math.max(maxYPosA, maxYPosB)
  const maxXPosAB = Math.max(maxXPosA, maxXPosB)
  if (minPosA > maxPosB) {
    const pos: SnapPos = {
      pos: (isHor ? maxYPosAB : maxXPosAB) + Guideline_Distance_Offset,
      rangePos: [maxPosB, minPosA],
      withArrow: true,
      isVertical: !isHor,
    }
    const alignPosArr: SnapPos[] = [
      {
        pos: isHor ? minXPosA : minYPosA,
        rangePos: isHor
          ? [minYPosA, maxYPosAB + Guideline_Distance_Offset]
          : [minXPosA, maxXPosAB + Guideline_Distance_Offset],
        withArrow: false,
        isVertical: isHor,
      },
      {
        pos: isHor ? maxXPosB : maxYPosB,
        rangePos: isHor
          ? [minYPosB, maxYPosAB + Guideline_Distance_Offset]
          : [minXPosB, maxXPosAB + Guideline_Distance_Offset],
        withArrow: false,
        isVertical: isHor,
      },
    ]
    return {
      dist: minPosA - maxPosB,
      pos,
      alignPosArr,
      isLeftOrUpper: true,
    }
  } else if (maxPosA < minPosB) {
    const pos: SnapPos = {
      pos: (isHor ? maxYPosAB : maxXPosAB) + Guideline_Distance_Offset,
      rangePos: [maxPosA, minPosB],
      withArrow: true,
      isVertical: !isHor,
    }
    const alignPosArr: SnapPos[] = [
      {
        pos: isHor ? maxXPosA : maxYPosA,
        rangePos: isHor
          ? [minYPosA, maxYPosAB + Guideline_Distance_Offset]
          : [minXPosA, maxXPosAB + Guideline_Distance_Offset],
        withArrow: false,
        isVertical: isHor,
      },
      {
        pos: isHor ? minXPosB : minYPosB,
        rangePos: isHor
          ? [minYPosB, maxYPosAB + Guideline_Distance_Offset]
          : [minXPosB, maxXPosAB + Guideline_Distance_Offset],
        withArrow: false,
        isVertical: isHor,
      },
    ]
    return {
      dist: minPosB - maxPosA,
      pos,
      alignPosArr,
      isLeftOrUpper: false,
    }
  }
}

function _calcOverlappedShapesDistances(
  isHor: boolean,
  sps: SlideElement[],
  calculator: (
    isHor: boolean,
    shapeSnapXYsA: { snapXs: number[]; snapYs: number[] },
    shapeSnapXYsB: { snapXs: number[]; snapYs: number[] }
  ) => ShapeDistancePosInfo | undefined
): ShapeDistancePosInfo[] {
  if (sps.length <= 1) {
    return []
  }
  let targetSnapXY = { snapXs: sps[0].snapXs, snapYs: sps[0].snapYs }
  const distances: ShapeDistancePosInfo[] = []
  for (let i = 1, l = sps.length; i < l; i++) {
    const spSnapXY = { snapXs: sps[i].snapXs, snapYs: sps[i].snapYs }
    const distInfo = calculator(isHor, spSnapXY, targetSnapXY)
    targetSnapXY = spSnapXY
    if (distInfo?.pos.rangePos != null) {
      binsert(distances, distInfo, (d1, d2) =>
        cmpPosRange(d1.pos.rangePos!, d2.pos.rangePos!)
      )
    }
  }

  return distances
}

// 这里对操作形状的距离线位置进行修复，实现吸附效果
function correctFinalSnapPos(distInfo: ShapeDistancePosInfo, diff: number) {
  const { pos, alignPosArr, isLeftOrUpper } = distInfo
  if (isLeftOrUpper) {
    if (pos.rangePos) {
      pos.rangePos[0] = pos.rangePos[0] + diff
    }
    alignPosArr[1].pos = alignPosArr[1].pos + diff
  } else {
    if (pos.rangePos) {
      pos.rangePos[1] = pos.rangePos[1] + diff
    }
    alignPosArr[1].pos = alignPosArr[1].pos + diff
  }
}
