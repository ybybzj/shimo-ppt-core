export interface RenderingRectInfo {
  /** 画布左上角(起点)到容器左侧的 px 距离 */
  left: number
  /** 画布左上角(起点)到容器上方的 px 距离 */
  top: number
  /** 画布右下角到容器左侧的 px 距离 */
  right: number
  /** 画布右下角到容器上方的 px 距离 */
  bottom: number
  base64Img?: any
}

export interface RenderingSlideInfo {
  renderingRect: RenderingRectInfo
  widthInMM: number
  heightInMM: number
}
