import { PlaceholderType } from '../../../core/SlideElement/const'

import { isPlaceholder } from '../../../core/utilities/shape/asserts'
import { getPlaceholderType } from '../../../core/utilities/shape/getters'

import {
  drawRectWithDashBorder,
  drawAclinicRectWithDashBorder,
} from '../../../core/graphic/drawUtils'
import {
  getCanvasDrawer,
  CanvasDrawer,
} from '../../../core/graphic/CanvasDrawer'
import { toggleCxtTransformReset } from '../../../core/graphic/utils'
import { renderSlideBackground } from '../../../core/render/slide'
import { SlideLayout } from '../../../core/Slide/SlideLayout'
import { drawSlideElement } from '../../../core/render/drawSlideElement'
import { Nullable } from '../../../../liber/pervasive'
import { FillEffects } from '../../../core/SlideElement/attrs/fill/fill'
import { calculateLayoutPlaceholders } from '../../../core/calculation/calculate/slideLayout'
import { calculateSlideElement } from '../../../core/calculation/calculate/slideElement'

export class LayoutPreviewsDrawer {
  canvas: Nullable<HTMLCanvasElement>
  widthInMM: number
  heightInMM: number
  widthInPx: number
  heightInPx: number

  constructor() {
    this.canvas = null
    this.widthInMM = 0
    this.heightInMM = 0
    this.widthInPx = 0
    this.heightInPx = 0
  }

  draw(g: CanvasDrawer, layout: SlideLayout, useBg, useMasterSp) {
    let bgFill
    let rgba = { r: 0, g: 0, b: 0, a: 255 }

    const master = layout.master!
    const theme = master.theme!
    if (layout != null) {
      if (layout.cSld.bg != null) {
        if (null != layout.cSld.bg.bgPr) {
          bgFill = layout.cSld.bg.bgPr.fill
        } else if (layout.cSld.bg.bgRef != null) {
          layout.cSld.bg.bgRef.color.updateBy(theme, null, layout, master, rgba)
          rgba = layout.cSld.bg.bgRef.color.rgba
          bgFill = theme.themeElements.fmtScheme.getFillStyle(
            layout.cSld.bg.bgRef?.idx
          )
        }
      } else if (master != null) {
        if (master.cSld.bg != null) {
          if (null != master.cSld.bg.bgPr) {
            bgFill = master.cSld.bg.bgPr.fill
          } else if (master.cSld.bg.bgRef != null) {
            master.cSld.bg.bgRef.color.updateBy(
              theme,
              null,
              layout,
              master,
              rgba
            )
            rgba = master.cSld.bg.bgRef.color.rgba
            bgFill = theme.themeElements.fmtScheme.getFillStyle(
              master.cSld.bg.bgRef.idx
            )
          }
        } else {
          bgFill = FillEffects.SolidRGBA(255, 255, 255, 255)
        }
      }
    }

    if (bgFill != null) {
      bgFill.calculate(theme, null, layout, master, rgba)
    }

    if (useBg !== false) {
      renderSlideBackground(g, bgFill, this.widthInMM, this.heightInMM)
    }

    const { sx, sy } = g.getCoordTransformScale()

    if (useMasterSp !== false) {
      if (layout.showMasterSp === true || layout.showMasterSp == null) {
        master.draw(g)
      }
    }

    for (let i = 0; i < layout.cSld.spTree.length; i++) {
      const sp = layout.cSld.spTree[i]
      calculateSlideElement(sp)
      if (isPlaceholder(sp)) {
        const phType = getPlaceholderType(sp)
        let usePlaceHolder = true
        switch (phType) {
          case PlaceholderType.dt:
          case PlaceholderType.ftr:
          case PlaceholderType.hdr:
          case PlaceholderType.sldNum: {
            usePlaceHolder = false
            break
          }
          default:
            break
        }
        if (!usePlaceHolder) continue

        drawSlideElement(sp, g)
        if (!sp.pen || !sp.pen.fillEffects || sp.pen.fillEffects.isNoFill()) {
          const ctx = g.context
          ctx.globalAlpha = 1
          const matrix = sp.transform
          let x = 1
          let y = 1
          let r = Math.max(sp.extX - 1, 1)
          let b = Math.max(sp.extY - 1, 1)

          const isIntGrid = g.getCxtTransformReset()
          if (!isIntGrid) toggleCxtTransformReset(g, true)

          if (matrix) {
            const x1 = sx * matrix.XFromPoint(x, y)
            const y1 = sy * matrix.YFromPoint(x, y)

            const x2 = sx * matrix.XFromPoint(r, y)
            const y2 = sy * matrix.YFromPoint(r, y)

            const x3 = sx * matrix.XFromPoint(x, b)
            const y3 = sy * matrix.YFromPoint(x, b)

            const x4 = sx * matrix.XFromPoint(r, b)
            const y4 = sy * matrix.YFromPoint(r, b)

            if (Math.abs(matrix.shx) < 0.001 && Math.abs(matrix.shy) < 0.001) {
              x = x1
              if (x > x2) x = x2
              if (x > x3) x = x3

              r = x1
              if (r < x2) r = x2
              if (r < x3) r = x3

              y = y1
              if (y > y2) y = y2
              if (y > y3) y = y3

              b = y1
              if (b < y2) b = y2
              if (b < y3) b = y3

              x >>= 0
              y >>= 0
              r >>= 0
              b >>= 0

              ctx.lineWidth = 1

              ctx.strokeStyle = '#FFFFFF'
              ctx.beginPath()
              ctx.strokeRect(x + 0.5, y + 0.5, r - x, b - y)
              ctx.strokeStyle = '#000000'
              ctx.beginPath()
              drawAclinicRectWithDashBorder(ctx, x, y, r, b, 2, 2)
              ctx.beginPath()
            } else {
              ctx.lineWidth = 1

              ctx.strokeStyle = '#000000'
              ctx.beginPath()
              ctx.moveTo(x1, y1)
              ctx.lineTo(x2, y2)
              ctx.lineTo(x4, y4)
              ctx.lineTo(x3, y3)
              ctx.closePath()
              ctx.stroke()
              ctx.strokeStyle = '#FFFFFF'
              ctx.beginPath()
              drawRectWithDashBorder(ctx, x1, y1, x2, y2, x3, y3, x4, y4, 2, 2)
              ctx.beginPath()
            }
          } else {
            x = (sx * x) >> 0
            y = (sy * y) >> 0
            r = (sx * r) >> 0
            b = (sy * b) >> 0

            ctx.lineWidth = 1

            ctx.strokeStyle = '#000000'
            ctx.beginPath()
            ctx.strokeRect(x + 0.5, y + 0.5, r - x, b - y)
            ctx.strokeStyle = '#FFFFFF'
            ctx.beginPath()
            drawAclinicRectWithDashBorder(ctx, x, y, r, b, 2, 2)
            ctx.beginPath()
          }

          if (!isIntGrid) toggleCxtTransformReset(g, true)
        }
      } else {
        drawSlideElement(sp, g)
      }
    }
  }

  getThumbnail(layout: SlideLayout, useBg?, useMasterSp?, useLayoutSp?) {
    calculateLayoutPlaceholders(layout)

    const h = 67
    const w = ((this.widthInMM * h) / this.heightInMM) >> 0

    this.widthInPx = w
    this.heightInPx = h

    if (this.canvas == null) {
      this.canvas = document.createElement('canvas')
    }

    this.canvas.width = w
    this.canvas.height = h

    const ctx = this.canvas.getContext('2d')!
    const drawCxt = getCanvasDrawer(ctx, w, h, this.widthInMM, this.heightInMM)

    drawCxt.updateTransform(1, 0, 0, 1, 0, 0)
    drawCxt.isRenderThumbnail = true
    this.draw(drawCxt, layout, useBg, useMasterSp)
    drawCxt.resetState()
    try {
      return this.canvas.toDataURL('image/png')
    } catch (err) {
      this.canvas = null
      if (null == useBg && null == useMasterSp && null == useLayoutSp) {
        return this.getThumbnail(layout, true, true, false)
      } else if (useBg && useMasterSp && !useLayoutSp) {
        return this.getThumbnail(layout, true, false, false)
      } else if (useBg && !useMasterSp && !useLayoutSp) {
        return this.getThumbnail(layout, false, false, false)
      }
    }

    return ''
  }
}
