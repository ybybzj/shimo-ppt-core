import { BrowserInfo } from '../../../common/browserInfo'
import { Matrix2D } from '../../../core/graphic/Matrix'
import {
  drawAclinicRectByLineTo,
  drawRectByLineTo,
} from '../../../core/graphic/drawUtils'
import { CanvasDrawer } from '../../../core/graphic/CanvasDrawer'
import { EditLayer } from '../../editorUI/editLayer'
import { TrackRotateImage } from '../../../images/images'
import { InstanceType } from '../../../core/instanceTypes'
import { Nullable } from '../../../../liber/pervasive'
import { RenderingSlideInfo } from '../type'

import { toggleCxtTransformReset } from '../../../core/graphic/utils'
import { EditControlType } from '../../../core/common/const/ui'
import { CONTROL_DISTANCE_ROTATE } from '../../../core/common/const/control'

// const CONTROL_RECT_SIZE = 8
const CONTROL_DISTANCE_ROTATE2 = 18
const CONTROL_WRAPPOINTS_SIZE = 6
/** 旋转图标尺寸 */
const CONTROL_ROTATE_IMAGE_WIDTH = 16
/** 控点尺寸 */
const CONTROL_DOT_SIZE = 8 // 8px black + 2px border

const COLOR_WHITE = '#FFFFFF'
const COLOR_BLUE = '#5BA0E7'
const COLOR_GREY = '#41464B'
const COLOR_BLACK = '#000000'
const COLOR_SELECTION_RECT_FILL = 'rgba(51,102,204,255)'
const COLOR_SELECTION_RECT_BORDER = '#9ADBFE'

const RectTransformType = {
  none: 0,
  Flip: 1,
  Eavert: 2,
}

export const CONTROL_CROP_CORNER_SIZE = 17
export const CONTROL_CROP_CENTRAL_DOT_NEED_MIN_WIDTH = 40

export class EditLayerDrawer {
  readonly instanceType = InstanceType.EditLayerDrawer
  ctx: Nullable<CanvasRenderingContext2D>
  editLayer!: EditLayer
  canvasDrawer!: CanvasDrawer
  maxEps: number
  curSlideRenderingInfo: Nullable<RenderingSlideInfo>

  constructor() {
    this.maxEps = 0
    this.curSlideRenderingInfo = null
  }

  init(
    editLayer: EditLayer,
    x: number,
    y: number,
    r: number,
    b: number,
    widthInMM: number,
    heightInMM: number
  ) {
    this.editLayer = editLayer
    this.ctx = this.editLayer.context
    this.canvasDrawer = new CanvasDrawer()
    const scale = BrowserInfo.PixelRatio

    this.canvasDrawer.init(
      this.ctx,
      scale * (r - x),
      scale * (b - y),
      widthInMM,
      heightInMM
    )

    this.canvasDrawer.setCoordTransform(scale * x, scale * y)
    toggleCxtTransformReset(this.canvasDrawer, false)
    this.ctx.globalAlpha = 0.5 // layer default half transparent
  }

  // draw styles
  setPenColor(r, g, b, a) {
    this.canvasDrawer.setPenColor(r, g, b, a)
  }

  setPenWidth(w: number) {
    this.canvasDrawer.setPenWidth(w)

    const x1 = this.canvasDrawer._XFromPoint(0, 0)
    const y1 = this.canvasDrawer._YFromPoint(0, 0)
    const x2 = this.canvasDrawer._XFromPoint(1, 1)
    const y2 = this.canvasDrawer._YFromPoint(1, 1)

    const len = (x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)
    const factor = Math.sqrt(len / 2)

    let eps = ((w * factor) / 1000) >> 0
    eps += 5

    if (eps > this.maxEps) this.maxEps = eps
  }

  setPenDash(params: Nullable<number[]>) {
    this.canvasDrawer.setPenDash(params)
  }

  setBrushColor(r, g, b, a) {
    this.canvasDrawer.setBrushColor(r, g, b, a)
  }

  // path commands
  _begin() {
    this.canvasDrawer._begin()
  }

  _end() {
    this.canvasDrawer._end()
  }

  _close() {
    this.canvasDrawer._close()
  }

  _moveTo(x, y) {
    this.canvasDrawer._moveTo(x, y)

    const _x = this.canvasDrawer._XFromPoint(x, y)
    const _y = this.canvasDrawer._YFromPoint(x, y)
    this.editLayer.update(_x, _y)
  }

  _lineTo(x, y) {
    this.canvasDrawer._lineTo(x, y)

    const _x = this.canvasDrawer._XFromPoint(x, y)
    const _y = this.canvasDrawer._YFromPoint(x, y)
    this.editLayer.update(_x, _y)
  }

  _bezierCurveTo(x1, y1, x2, y2, x3, y3) {
    this.canvasDrawer._bezierCurveTo(x1, y1, x2, y2, x3, y3)

    const _x1 = this.canvasDrawer._XFromPoint(x1, y1)
    const _y1 = this.canvasDrawer._YFromPoint(x1, y1)

    const _x2 = this.canvasDrawer._XFromPoint(x2, y2)
    const _y2 = this.canvasDrawer._YFromPoint(x2, y2)

    const _x3 = this.canvasDrawer._XFromPoint(x3, y3)
    const _y3 = this.canvasDrawer._YFromPoint(x3, y3)

    this.editLayer.update(_x1, _y1)
    this.editLayer.update(_x2, _y2)
    this.editLayer.update(_x3, _y3)
  }

  _quadraticCurveTo(x1, y1, x2, y2) {
    this.canvasDrawer._quadraticCurveTo(x1, y1, x2, y2)

    const _x1 = this.canvasDrawer._XFromPoint(x1, y1)
    const _y1 = this.canvasDrawer._YFromPoint(x1, y1)

    const _x2 = this.canvasDrawer._XFromPoint(x2, y2)
    const _y2 = this.canvasDrawer._YFromPoint(x2, y2)

    this.editLayer.update(_x1, _y1)
    this.editLayer.update(_x2, _y2)
  }

  drawStroke(p?: Path2D) {
    this.canvasDrawer.drawStroke(p)
  }

  drawFill(p?: Path2D) {
    this.canvasDrawer.drawFill(p)
  }

  // canvas state
  save() {
    this.canvasDrawer.save()
  }

  restore() {
    this.canvasDrawer.restore()
  }

  clip(p?: Path2D) {
    this.canvasDrawer.clip(p)
  }

  // transform
  reset() {
    this.canvasDrawer.reset()
  }

  applyTransformMatrix(m) {
    this.canvasDrawer.applyTransformMatrix(m)
  }

  transform(sx, shy, shx, sy, tx, ty) {
    this.canvasDrawer.updateTransform(sx, shy, shx, sy, tx, ty)
  }

  resetLayerBounds() {
    this.editLayer.resetTransform()
    this.editLayer.minX -= this.maxEps
    this.editLayer.minY -= this.maxEps
    this.editLayer.maxX += this.maxEps
    this.editLayer.maxY += this.maxEps
  }

  setLayer(layer: EditLayer) {
    this.editLayer = layer
    this.ctx = this.editLayer.context
  }

  saveState() {
    this.canvasDrawer.saveState()
  }

  restoreState() {
    this.canvasDrawer.restoreState()
  }

  drawControl(params: {
    type: (typeof EditControlType)[keyof typeof EditControlType]
    matrix: Matrix2D
    left: number
    top: number
    width: number
    height: number
    isLine: boolean
    canRotate: boolean
    strokeStyle?: string
  }) {
    const layer = this.editLayer
    const { type, matrix, left, top, width, height, isLine, canRotate } = params
    this.curSlideRenderingInfo = layer.editorUI.getSlideRenderingInfo()

    const posInfo = getTrackRectPosition(
      matrix,
      left,
      top,
      width,
      height,
      this.editLayer.editorUI.getSlideRenderingInfo()
    )
    const ctx = layer.context
    ctx.lineWidth = 1
    ctx.beginPath()

    const orgGlobalAlpha = ctx.globalAlpha
    ctx.globalAlpha = 1

    switch (type) {
      case EditControlType.SHAPE: {
        drawShapeControl(this, matrix, isLine, canRotate, posInfo)
        break
      }
      case EditControlType.TEXT: {
        drawTextControl(this, matrix, canRotate, posInfo)
        break
      }
      case EditControlType.CROP: {
        drawCropControl(this, posInfo)
        break
      }
      case EditControlType.HoveringOutline:
      case EditControlType.ViewMod: {
        drawHoveringOutLine(this, type, isLine, posInfo)
        break
      }
      default:
        break
    }

    ctx.globalAlpha = orgGlobalAlpha
  }

  drawSelectionRect(x, y, w, h) {
    const editLayer = this.editLayer
    this.curSlideRenderingInfo = editLayer.editorUI.getSlideRenderingInfo()
    const { renderingRect, widthInMM, heightInMM } = this.curSlideRenderingInfo
    const { left, top, right, bottom } = renderingRect

    const viewPortWidth = right - renderingRect.left
    const viewPortHeight = bottom - renderingRect.top
    const coffX = viewPortWidth / widthInMM
    const coffY = viewPortHeight / heightInMM

    let startX = (left + coffX * x) >> 0
    let startY = (top + coffY * y) >> 0

    let endX = (left + coffX * (x + w)) >> 0
    let endY = (top + coffY * (y + h)) >> 0

    if (startX > endX) {
      ;[startX, endX] = [endX, startX]
    }
    if (startY > endY) {
      ;[startY, endY] = [endY, startY]
    }
    const [width, height] = [endX - startX, endY - startY]

    editLayer.updateByRect(startX, startY, width, height)
    const ctx = editLayer.context
    editLayer.resetTransform()
    const oldAlpha = ctx.globalAlpha
    ctx.globalAlpha = 0.5
    ctx.beginPath()
    ctx.fillStyle = COLOR_SELECTION_RECT_FILL
    ctx.strokeStyle = COLOR_SELECTION_RECT_BORDER
    ctx.lineWidth = 1
    ctx.fillRect(startX, startY, width, height)
    ctx.beginPath()
    ctx.strokeRect(startX - 0.5, startY - 0.5, width + 1, height + 1)
    ctx.globalAlpha = oldAlpha
  }

  drawAdjustControl(matrix: Matrix2D, x: number, y: number) {
    const editLayer = this.editLayer
    this.curSlideRenderingInfo = editLayer.editorUI.getSlideRenderingInfo()

    const drawRect = this.curSlideRenderingInfo.renderingRect

    const l = drawRect.left
    const t = drawRect.top
    const w = drawRect.right - drawRect.left
    const h = drawRect.bottom - drawRect.top

    const factorX = w / this.curSlideRenderingInfo.widthInMM
    const factorY = h / this.curSlideRenderingInfo.heightInMM

    const cx = (l + factorX * matrix.XFromPoint(x, y)) >> 0
    const cy = (t + factorY * matrix.YFromPoint(x, y)) >> 0
    const ctx = editLayer.context

    ctx.save()
    ctx.shadowOffsetX = 0
    ctx.shadowOffsetY = 1
    ctx.shadowBlur = 2
    ctx.shadowColor = 'rgba(0, 0, 0, 0.3)'
    ctx.fillStyle = COLOR_BLUE
    ctx.strokeStyle = COLOR_WHITE

    this.drawControlPoint(cx, cy)

    ctx.fill()
    ctx.stroke()
    ctx.restore()
    ctx.beginPath()
  }

  /** 传入整数坐标 */
  drawControlPoint(
    x: number,
    y: number,
    rotInfo?: { ex1: number; ey1: number; ex2: number; ey2: number }
  ) {
    const layer = this.editLayer
    x += 0.5
    y += 0.5
    if (rotInfo) {
      const { ex1, ey1, ex2, ey2 } = rotInfo
      layer.addRectByEllipse(x, y, CONTROL_DOT_SIZE, ex1, ey1, ex2, ey2)
    } else {
      layer.addRect(
        x - CONTROL_DOT_SIZE / 2 - 1,
        y - CONTROL_DOT_SIZE / 2 - 1,
        CONTROL_DOT_SIZE,
        CONTROL_DOT_SIZE
      )
    }
  }

  drawTouchPoints(points: Array<{ x; y: number }>, matrix: Matrix2D) {
    const length = points.length
    if (0 === length) return

    const editLayer = this.editLayer
    const ctx = editLayer.context

    this.curSlideRenderingInfo = editLayer.editorUI.getSlideRenderingInfo()

    const drawRect = this.curSlideRenderingInfo.renderingRect

    const l = drawRect.left * BrowserInfo.PixelRatio
    const t = drawRect.top * BrowserInfo.PixelRatio
    const w = (drawRect.right - drawRect.left) * BrowserInfo.PixelRatio
    const h = (drawRect.bottom - drawRect.top) * BrowserInfo.PixelRatio

    const factorX = w / this.curSlideRenderingInfo.widthInMM
    const factorY = h / this.curSlideRenderingInfo.heightInMM

    const trXs = new Array(length)
    const trYs = new Array(length)
    for (let i = 0; i < length; i++) {
      trXs[i] = (l + factorX * matrix.XFromPoint(points[i].x, points[i].y)) >> 0
      trYs[i] = (t + factorY * matrix.YFromPoint(points[i].x, points[i].y)) >> 0
    }

    ctx.beginPath()
    for (let i = 0; i < length; i++) {
      if (0 === i) ctx.moveTo(trXs[i], trYs[i])
      else ctx.lineTo(trXs[i], trYs[i])

      editLayer.update(trXs[i], trYs[i])
    }

    ctx.closePath()
    ctx.lineWidth = 1
    ctx.strokeStyle = '#FF0000'
    ctx.stroke()

    ctx.beginPath()
    for (let i = 0; i < length; i++) {
      editLayer.addRectByCircle(
        trXs[i] + 0.5,
        trYs[i] + 0.5,
        CONTROL_WRAPPOINTS_SIZE
      )
    }
    ctx.strokeStyle = '#FFFFFF'
    ctx.fillStyle = '#000000'
    ctx.fill()
    ctx.stroke()

    ctx.beginPath()
  }

  drawRotateControlBg(ctx, x, y) {
    ctx.save()
    ctx.beginPath()
    ctx.fillStyle = COLOR_GREY
    ctx.strokeStyle = COLOR_WHITE
    ctx.lineWidth = 1
    ctx.shadowOffsetX = 0
    ctx.shadowOffsetY = 1
    ctx.shadowBlur = 2
    ctx.shadowColor = 'rgba(0, 0, 0, 0.3)'
    const radius = CONTROL_ROTATE_IMAGE_WIDTH / 2
    this.editLayer.addEllipse(x + radius, y + radius, radius)
    ctx.fill()
    ctx.stroke()
    ctx.restore()
    ctx.shadowOffsetX = 0
    ctx.shadowOffsetY = 0
    ctx.shadowBlur = 0
  }
}

interface RectPositionInfo {
  pos: {
    x1: number
    x2: number
    x3: number
    x4: number
    y1: number
    y2: number
    y3: number
    y4: number
  }
  width: number
  height: number
  transformType: (typeof RectTransformType)[keyof typeof RectTransformType]
  /** 无旋转, 无 flip, 可以直接 fillRect */
  canFillRect: boolean
  /** 无旋转, 存在 flip, 但计算 transform 消除 flip 影响后可以直接 fillRect */
  canFillRectWithflip: boolean
}

/**
 * @usage 根据传入参数计算对应在 mainView 中的坐标
 * @param left 原位置上需要 offset left 的 pixel
 * @return pos 为 transform 后的坐标
 * */
export function getTrackRectPosition(
  matrix: Matrix2D,
  left: number,
  top: number,
  width: number,
  height: number,
  slideDrawingInfo: RenderingSlideInfo
): RectPositionInfo {
  let canFillRect = false
  const drawRect = slideDrawingInfo.renderingRect
  const offsetLeft = drawRect.left
  const offsetTop = drawRect.top
  const w = drawRect.right - drawRect.left
  const h = drawRect.bottom - drawRect.top

  /** 包含了 zoom,dpr 等的综合比例 */
  const factorX = w / slideDrawingInfo.widthInMM
  const factorY = h / slideDrawingInfo.heightInMM

  const r = left + width
  const b = top + height

  const transformX1 = offsetLeft + factorX * matrix.XFromPoint(left, top)
  const transformY1 = offsetTop + factorY * matrix.YFromPoint(left, top)

  const transformX2 = offsetLeft + factorX * matrix.XFromPoint(r, top)
  const transformY2 = offsetTop + factorY * matrix.YFromPoint(r, top)

  const transformX3 = offsetLeft + factorX * matrix.XFromPoint(left, b)
  const transformY3 = offsetTop + factorY * matrix.YFromPoint(left, b)

  const transformX4 = offsetLeft + factorX * matrix.XFromPoint(r, b)
  const transformY4 = offsetTop + factorY * matrix.YFromPoint(r, b)

  const [x1, y1] = [transformX1 >> 0, transformY1 >> 0]
  let [x2, y2] = [transformX2 >> 0, transformY2 >> 0]
  let [x3, y3] = [transformX3 >> 0, transformY3 >> 0]
  let [x4, y4] = [transformX4 >> 0, transformY4 >> 0]

  // P1 ----- P2
  // ||       ||
  // P3 ----- P4

  const EPS = 0.001
  const similarRect =
    Math.abs(transformX1 - transformX3) < EPS &&
    Math.abs(transformX2 - transformX4) < EPS &&
    Math.abs(transformY1 - transformY2) < EPS &&
    Math.abs(transformY3 - transformY4) < EPS
  /** 0 - 90, 270 - 360˚ */
  const oddHalfPi = x1 < x2
  /** 90 - 270˚ */
  const notHorStright = y1 < y3

  // 近似 FlipH, 可以 fillRect
  if (similarRect && oddHalfPi && notHorStright) {
    ;[x3, x4, y2, y4] = [x1, x2, y1, y3]
    canFillRect = true
  }

  let canFillRectWithflip = canFillRect
  let transformType: (typeof RectTransformType)[keyof typeof RectTransformType] =
    RectTransformType.none

  // 可以通过 Flip 校准
  if (similarRect && !canFillRect) {
    ;[x3, x4, y2, y4] = [x1, x2, y1, y3]
    canFillRectWithflip = true
    transformType = RectTransformType.Flip
  }

  const Eavert =
    Math.abs(transformX1 - transformX2) < EPS &&
    Math.abs(transformX3 - transformX4) < EPS &&
    Math.abs(transformY1 - transformY3) < EPS &&
    Math.abs(transformY2 - transformY4) < EPS

  // 近似 90˚ 旋转
  if (!canFillRectWithflip && Eavert) {
    ;[x2, x4, y3, y4] = [x1, x3, y1, y2]
    canFillRectWithflip = true
    transformType = RectTransformType.Eavert
  }

  return {
    pos: { x1, x2, x3, x4, y1, y2, y3, y4 },
    width: factorX * width,
    height: factorY * height,
    transformType,
    canFillRectWithflip,
    canFillRect,
  }
}

function drawShapeControl(
  layerDrawer: EditLayerDrawer,
  matrix: Matrix2D,
  isLine: boolean,
  canRotate: boolean,
  posInfo: RectPositionInfo
) {
  const layer = layerDrawer.editLayer
  const ctx = layer.context

  const { pos, canFillRect, canFillRectWithflip } = posInfo
  const { x1, x2, x3, x4, y1, y2, y3, y4 } = pos
  const linesInfo = calcLinesInfo(posInfo)
  const { pixLengthX, pixLengthY, needXControl, needYControl } = linesInfo

  // 无旋转，无 Flip
  if (canFillRect) {
    const [rectW, rectH] = [x4 - x1, y4 - y1]
    layer.updateByRect(x1, y1, rectW, rectH)

    if (!isLine) {
      ctx.strokeStyle = COLOR_BLACK
      ctx.rect(x1 + 0.5, y2 + 0.5, rectW, rectH)
      ctx.stroke()
      ctx.beginPath()
    }

    ctx.strokeStyle = COLOR_WHITE
    const centerX = ((x1 + x2) / 2) >> 0

    // 绘制旋转控点
    if (!isLine && canRotate) {
      if (TrackRotateImage.loaded) {
        const imageWidth = TrackRotateImage.width / 2
        const imageHeight = TrackRotateImage.height / 2
        const w = CONTROL_ROTATE_IMAGE_WIDTH
        const xi = (centerX + 0.5 - w / 2) >> 0
        const yi = y1 - CONTROL_DISTANCE_ROTATE - (w >> 1)

        layerDrawer.drawRotateControlBg(ctx, xi, yi)

        layer.updateByRect(xi, yi, w, w)
        const offsetX = (w - imageWidth) / 2
        const offsetY = (w - imageHeight) / 2 - 1
        ctx.drawImage(
          TrackRotateImage,
          xi + offsetX,
          yi + offsetY,
          imageWidth,
          imageHeight
        )
      }

      ctx.beginPath()
      ctx.strokeStyle = COLOR_BLACK
      ctx.moveTo(centerX + 0.5, y1)
      ctx.lineTo(centerX + 0.5, y1 - CONTROL_DISTANCE_ROTATE2)
      ctx.stroke()
      ctx.beginPath()
    }

    ctx.strokeStyle = COLOR_WHITE
    ctx.fillStyle = COLOR_GREY

    ctx.save()
    ctx.shadowOffsetX = 0
    ctx.shadowOffsetY = 1
    ctx.shadowBlur = 2
    ctx.shadowColor = 'rgba(0, 0, 0, 0.3)'

    // 绘制角控点
    layerDrawer.drawControlPoint(x1, y1)
    if (!isLine) {
      layerDrawer.drawControlPoint(x2, y2)
      layerDrawer.drawControlPoint(x3, y3)
    }
    layerDrawer.drawControlPoint(x4, y4)

    // 绘制边控点
    if (!isLine) {
      if (needXControl) {
        const cx = ((x1 + x2) / 2) >> 0
        layerDrawer.drawControlPoint(cx, y1)
        layerDrawer.drawControlPoint(cx, y3)
      }

      if (needYControl) {
        const cy = ((y1 + y3) / 2) >> 0
        layerDrawer.drawControlPoint(x2, cy)
        layerDrawer.drawControlPoint(x1, cy)
      }
    }

    ctx.fill()
    ctx.stroke()
    ctx.restore()
    ctx.beginPath()
  }

  // 存在旋转或 Flip
  else {
    let [_x1, _y1, _x2, _y2] = [x1, y1, x2, y2]
    let [_x3, _y3, _x4, _y4] = [x3, y3, x4, y4]

    // 校准 fLip
    if (canFillRectWithflip) {
      _x1 = x1
      if (x2 < _x1) _x1 = x2
      if (x3 < _x1) _x1 = x3
      if (x4 < _x1) _x1 = x4

      _x4 = x1
      if (x2 > _x4) _x4 = x2
      if (x3 > _x4) _x4 = x3
      if (x4 > _x4) _x4 = x4

      _y1 = y1
      if (y2 < _y1) _y1 = y2
      if (y3 < _y1) _y1 = y3
      if (y4 < _y1) _y1 = y4

      _y4 = y1
      if (y2 > _y4) _y4 = y2
      if (y3 > _y4) _y4 = y3
      if (y4 > _y4) _y4 = y4

      _x2 = _x4
      _y2 = _y1
      _x3 = _x1
      _y3 = _y4
    }

    if (!isLine) {
      ctx.strokeStyle = COLOR_BLACK
      if (canFillRectWithflip) {
        ctx.rect(_x1 + 0.5, _y2 + 0.5, _x4 - _x1, _y4 - _y1)
        ctx.stroke()
        ctx.beginPath()
      } else {
        ctx.moveTo(x1, y1)
        ctx.lineTo(x2, y2)
        ctx.lineTo(x4, y4)
        ctx.lineTo(x3, y3)
        ctx.closePath()
        ctx.stroke()
      }
    }

    ctx.strokeStyle = COLOR_WHITE

    layer.update(x1, y1)
    layer.update(x2, y2)
    layer.update(x3, y3)
    layer.update(x4, y4)

    let ex1 = (x2 - x1) / pixLengthX // cos(Rot)
    let ey1 = (y2 - y1) / pixLengthX // sin(Rot)
    let ex2 = (x1 - x3) / pixLengthY
    let ey2 = (y1 - y3) / pixLengthY

    const absEX1 = Math.abs(ex1) < 0.01
    const absEY1 = Math.abs(ey1) < 0.01
    const absEX2 = Math.abs(ex2) < 0.01
    const absEY2 = Math.abs(ey2) < 0.01

    if (absEX2 && absEY2) {
      if (absEX1 && absEY1) {
        ex1 = 1
        ey1 = 0
        ex2 = 0
        ey2 = 1
      } else {
        ex2 = -ey1
        ey2 = ex1
      }
    } else if (absEX1 && absEY1) {
      ex1 = ey2
      ey1 = -ex2
    }

    const xc1 = (x1 + x2) / 2
    const yc1 = (y1 + y2) / 2

    ctx.beginPath()

    // 绘制旋转控点
    if (!isLine && canRotate) {
      if (TrackRotateImage.loaded) {
        const imageWidth = TrackRotateImage.width / 2
        const imageHeight = TrackRotateImage.height / 2

        let xi = xc1 + ex2 * CONTROL_DISTANCE_ROTATE
        let yi = yc1 + ey2 * CONTROL_DISTANCE_ROTATE
        const w = CONTROL_ROTATE_IMAGE_WIDTH
        let _w2 = CONTROL_ROTATE_IMAGE_WIDTH / 2

        if (canFillRectWithflip) {
          xi >>= 0
          yi >>= 0
          _w2 >>= 0
          _w2 += 1
        }

        const _matrix = matrix.clone()
        _matrix.tx = 0
        _matrix.ty = 0
        const _xx = _matrix.XFromPoint(0, 1)
        const _yy = _matrix.YFromPoint(0, 1)
        const _angle = Math.atan2(_xx, -_yy) - Math.PI
        const _px = Math.cos(_angle)
        const _py = Math.sin(_angle)

        ctx.save()
        ctx.translate(xi, yi)
        ctx.transform(_px, _py, -_py, _px, 0, 0)
        layerDrawer.drawRotateControlBg(ctx, -_w2 + 1, -_w2 + 1)
        const offsetX = (w - imageWidth) / 2 + 1
        const offsetY = (w - imageHeight) / 2
        ctx.drawImage(
          TrackRotateImage,
          -_w2 + offsetX,
          -_w2 + offsetY,
          imageWidth,
          imageHeight
        )
        ctx.restore()
        layer.updateByRect(xi - _w2, yi - _w2, w, w)
      }

      ctx.beginPath()
      ctx.strokeStyle = COLOR_BLACK
      if (!canFillRectWithflip) {
        ctx.moveTo(xc1, yc1)
        ctx.lineTo(
          xc1 + ex2 * CONTROL_DISTANCE_ROTATE2,
          yc1 + ey2 * CONTROL_DISTANCE_ROTATE2
        )
      } else {
        ctx.moveTo((xc1 >> 0) + 0.5, (yc1 >> 0) + 0.5)
        ctx.lineTo(
          ((xc1 + ex2 * CONTROL_DISTANCE_ROTATE2) >> 0) + 0.5,
          ((yc1 + ey2 * CONTROL_DISTANCE_ROTATE2) >> 0) + 0.5
        )
      }

      ctx.stroke()

      ctx.beginPath()
    }

    ctx.strokeStyle = COLOR_WHITE
    ctx.fillStyle = COLOR_GREY
    ctx.shadowOffsetX = 0
    ctx.shadowOffsetY = 1
    ctx.shadowBlur = 2
    ctx.shadowColor = 'rgba(0, 0, 0, 0.3)'

    // 绘制顶角控点
    if (!canFillRectWithflip) {
      layerDrawer.drawControlPoint(x1, y1, { ex1, ey1, ex2, ey2 })
      if (!isLine) {
        layerDrawer.drawControlPoint(x2, y2, { ex1, ey1, ex2, ey2 })
        layerDrawer.drawControlPoint(x3, y3, { ex1, ey1, ex2, ey2 })
      }
      layerDrawer.drawControlPoint(x4, y4, { ex1, ey1, ex2, ey2 })
    } else {
      if (!isLine) {
        layerDrawer.drawControlPoint(_x1, _y1)
        layerDrawer.drawControlPoint(_x2, _y2)
        layerDrawer.drawControlPoint(_x3, _y3)
        layerDrawer.drawControlPoint(_x4, _y4)
      } else {
        layerDrawer.drawControlPoint(x1, y1)
        layerDrawer.drawControlPoint(x4, y4)
      }
    }

    // 绘制边控点(仅边长度足够时)
    if (!isLine) {
      if (!canFillRectWithflip) {
        if (needXControl) {
          const [cx12, cy12] = [(x1 + x2) / 2, (y1 + y2) / 2]
          const [cx34, cy34] = [(x3 + x4) / 2, (y3 + y4) / 2]
          layerDrawer.drawControlPoint(cx12, cy12, { ex1, ey1, ex2, ey2 })
          layerDrawer.drawControlPoint(cx34, cy34, { ex1, ey1, ex2, ey2 })
        }
        if (needYControl) {
          const [cx24, cy24] = [(x2 + x4) / 2, (y2 + y4) / 2]
          const [cx31, cy31] = [(x3 + x1) / 2, (y3 + y1) / 2]
          layerDrawer.drawControlPoint(cx24, cy24, { ex1, ey1, ex2, ey2 })
          layerDrawer.drawControlPoint(cx31, cy31, { ex1, ey1, ex2, ey2 })
        }
      } else {
        const cx = ((_x1 + _x2) / 2) >> 0
        const cy = ((_y1 + _y3) / 2) >> 0

        if (needXControl) {
          layerDrawer.drawControlPoint(cx, _y1)
          layerDrawer.drawControlPoint(cx, _y3)
        }

        if (needYControl) {
          layerDrawer.drawControlPoint(_x2, cy)
          layerDrawer.drawControlPoint(_x1, cy)
        }
      }
    }

    ctx.fill()
    ctx.stroke()
    ctx.beginPath()
  }
  ctx.shadowOffsetX = 0
  ctx.shadowOffsetY = 0
  ctx.shadowBlur = 0
}

function drawTextControl(
  layerDrawer: EditLayerDrawer,
  matrix: Matrix2D,
  canRotate: boolean,
  posInfo: RectPositionInfo
) {
  const layer = layerDrawer.editLayer
  const ctx = layer.context

  const { pos, canFillRect, canFillRectWithflip } = posInfo
  const { x1, x2, x3, x4, y1, y2, y3, y4 } = pos
  const linesInfo = calcLinesInfo(posInfo)
  const { pixLengthX, pixLengthY, needXControl, needYControl } = linesInfo

  // 无旋转，无 Flip
  if (canFillRect) {
    const [rectW, rectH] = [x4 - x1, y4 - y1]
    layer.updateByRect(x1, y1, rectW, rectH)

    ctx.strokeStyle = COLOR_BLUE
    drawAclinicRectByLineTo(ctx, x1, y1, x4, y4)
    ctx.stroke()

    ctx.strokeStyle = COLOR_WHITE
    ctx.beginPath()

    // 绘制旋转控点
    if (canRotate) {
      const cx = ((x1 + x2) / 2) >> 0
      if (TrackRotateImage.loaded) {
        const imageWidth = TrackRotateImage.width / 2
        const imageHeight = TrackRotateImage.height / 2
        const w = CONTROL_ROTATE_IMAGE_WIDTH
        const xi = (cx + 0.5 - w / 2) >> 0
        const yi = y1 - CONTROL_DISTANCE_ROTATE - (w >> 1)

        layerDrawer.drawRotateControlBg(ctx, xi, yi)

        const offsetX = (w - imageWidth) / 2
        const offsetY = (w - imageHeight) / 2 - 1
        layer.updateByRect(xi, yi, w, w)
        ctx.drawImage(
          TrackRotateImage,
          xi + offsetX,
          yi + offsetY,
          imageWidth,
          imageHeight
        )
      }

      ctx.beginPath()
      ctx.strokeStyle = COLOR_BLUE
      ctx.moveTo(cx + 0.5, y1)
      ctx.lineTo(cx + 0.5, y1 - CONTROL_DISTANCE_ROTATE2)
      ctx.stroke()

      ctx.beginPath()
    }

    ctx.strokeStyle = COLOR_WHITE
    ctx.fillStyle = COLOR_GREY
    ctx.shadowOffsetX = 0
    ctx.shadowOffsetY = 1
    ctx.shadowBlur = 2
    ctx.shadowColor = 'rgba(0, 0, 0, 0.3)'

    // 绘制角控点
    layerDrawer.drawControlPoint(x1, y1)
    layerDrawer.drawControlPoint(x2, y2)
    layerDrawer.drawControlPoint(x3, y3)
    layerDrawer.drawControlPoint(x4, y4)

    // 绘制边控点
    if (needXControl) {
      const cx = ((x1 + x2) / 2) >> 0
      layerDrawer.drawControlPoint(cx, y1)
      layerDrawer.drawControlPoint(cx, y3)
    }
    if (needYControl) {
      const cy = ((y1 + y3) / 2) >> 0
      layerDrawer.drawControlPoint(x2, cy)
      layerDrawer.drawControlPoint(x1, cy)
    }

    ctx.fill()
    ctx.stroke()
    ctx.beginPath()
  }

  // 存在旋转或 Flip
  else {
    let [_x1, _y1, _x2] = [x1, y1, x2]
    let [_y3, _x4, _y4] = [y3, x4, y4]

    // 校准 fLip
    if (canFillRectWithflip) {
      _x1 = x1
      if (x2 < _x1) _x1 = x2
      if (x3 < _x1) _x1 = x3
      if (x4 < _x1) _x1 = x4

      _x4 = x1
      if (x2 > _x4) _x4 = x2
      if (x3 > _x4) _x4 = x3
      if (x4 > _x4) _x4 = x4

      _y1 = y1
      if (y2 < _y1) _y1 = y2
      if (y3 < _y1) _y1 = y3
      if (y4 < _y1) _y1 = y4

      _y4 = y1
      if (y2 > _y4) _y4 = y2
      if (y3 > _y4) _y4 = y3
      if (y4 > _y4) _y4 = y4

      _x2 = _x4
      /* _y2 = _y1 */
      /* _x3 = _x1 */
      _y3 = _y4
    }

    layer.update(x1, y1)
    layer.update(x2, y2)
    layer.update(x3, y3)
    layer.update(x4, y4)

    ctx.strokeStyle = COLOR_BLUE
    if (!canFillRectWithflip) {
      drawRectByLineTo(ctx, x1, y1, x2, y2, x3, y3, x4, y4)
      ctx.stroke()
      ctx.beginPath()
    } else {
      drawAclinicRectByLineTo(ctx, _x1, _y1, _x4, _y4)
      ctx.stroke()
      ctx.beginPath()
    }

    ctx.strokeStyle = COLOR_WHITE

    let ex1 = (x2 - x1) / pixLengthX
    let ey1 = (y2 - y1) / pixLengthX
    let ex2 = (x1 - x3) / pixLengthY
    let ey2 = (y1 - y3) / pixLengthY

    const absEX1 = Math.abs(ex1) < 0.01
    const absEY1 = Math.abs(ey1) < 0.01
    const absEX2 = Math.abs(ex2) < 0.01
    const absEY2 = Math.abs(ey2) < 0.01

    if (absEX2 && absEY2) {
      if (absEX1 && absEY1) {
        ex1 = 1
        ey1 = 0
        ex2 = 0
        ey2 = 1
      } else {
        ex2 = -ey1
        ey2 = ex1
      }
    } else if (absEX1 && absEY1) {
      ex1 = ey2
      ey1 = -ex2
    }

    const xc1 = (x1 + x2) / 2
    const yc1 = (y1 + y2) / 2

    ctx.beginPath()

    if (canRotate) {
      if (TrackRotateImage.loaded) {
        const imageWidth = TrackRotateImage.width / 2
        const imageHeight = TrackRotateImage.height / 2
        let xi = xc1 + ex2 * CONTROL_DISTANCE_ROTATE
        let yi = yc1 + ey2 * CONTROL_DISTANCE_ROTATE
        const w = CONTROL_ROTATE_IMAGE_WIDTH
        let _w2 = CONTROL_ROTATE_IMAGE_WIDTH / 2

        if (canFillRectWithflip) {
          xi >>= 0
          yi >>= 0
          _w2 >>= 0
          _w2 += 1
        }

        //ctx.setTransform(ex1, ey1, -ey1, ex1, xi, yi);

        const _matrix = matrix.clone()
        _matrix.tx = 0
        _matrix.ty = 0
        const trX = _matrix.XFromPoint(0, 1)
        const trY = _matrix.YFromPoint(0, 1)
        const angle = Math.atan2(trX, -trY) - Math.PI
        const px = Math.cos(angle)
        const py = Math.sin(angle)

        ctx.save()
        ctx.translate(xi, yi)
        ctx.transform(px, py, -py, px, 0, 0)
        layerDrawer.drawRotateControlBg(ctx, -_w2 + 1, -_w2 + 1)
        const offsetX = (w - imageWidth) / 2 + 1
        const offsetY = (w - imageHeight) / 2
        ctx.drawImage(
          TrackRotateImage,
          -_w2 + offsetX,
          -_w2 + offsetY,
          imageWidth,
          imageHeight
        )
        ctx.restore()
        layer.updateByRect(xi - _w2, yi - _w2, w, w)
      }

      ctx.beginPath()
      ctx.strokeStyle = COLOR_BLUE

      if (!canFillRectWithflip) {
        ctx.moveTo(xc1, yc1)
        ctx.lineTo(
          xc1 + ex2 * CONTROL_DISTANCE_ROTATE2,
          yc1 + ey2 * CONTROL_DISTANCE_ROTATE2
        )
      } else {
        ctx.moveTo((xc1 >> 0) + 0.5, (yc1 >> 0) + 0.5)
        ctx.lineTo(
          ((xc1 + ex2 * CONTROL_DISTANCE_ROTATE2) >> 0) + 0.5,
          ((yc1 + ey2 * CONTROL_DISTANCE_ROTATE2) >> 0) + 0.5
        )
      }

      ctx.stroke()
      ctx.beginPath()
    }

    ctx.strokeStyle = COLOR_WHITE
    ctx.fillStyle = COLOR_GREY
    ctx.shadowOffsetX = 0
    ctx.shadowOffsetY = 1
    ctx.shadowBlur = 2
    ctx.shadowColor = 'rgba(0, 0, 0, 0.3)'

    // 绘制顶角控点
    if (!canFillRectWithflip) {
      layerDrawer.drawControlPoint(x1, y1, { ex1, ey1, ex2, ey2 })
      layerDrawer.drawControlPoint(x2, y2, { ex1, ey1, ex2, ey2 })
      layerDrawer.drawControlPoint(x3, y3, { ex1, ey1, ex2, ey2 })
      layerDrawer.drawControlPoint(x4, y4, { ex1, ey1, ex2, ey2 })
    } else {
      layerDrawer.drawControlPoint(x1, y1)
      layerDrawer.drawControlPoint(x2, y2)
      layerDrawer.drawControlPoint(x3, y3)
      layerDrawer.drawControlPoint(x4, y4)
    }

    // 绘制边控点
    if (!canFillRectWithflip) {
      if (needXControl) {
        const [cx12, cy12] = [(x1 + x2) / 2, (y1 + y2) / 2]
        const [cx34, cy34] = [(x3 + x4) / 2, (y3 + y4) / 2]
        layerDrawer.drawControlPoint(cx12, cy12, { ex1, ey1, ex2, ey2 })
        layerDrawer.drawControlPoint(cx34, cy34, { ex1, ey1, ex2, ey2 })
      }
      if (needYControl) {
        const [cx24, cy24] = [(x2 + x4) / 2, (y2 + y4) / 2]
        const [cx31, cy31] = [(x3 + x1) / 2, (y3 + y1) / 2]
        layerDrawer.drawControlPoint(cx24, cy24, { ex1, ey1, ex2, ey2 })
        layerDrawer.drawControlPoint(cx31, cy31, { ex1, ey1, ex2, ey2 })
      }
    } else {
      const cx = ((_x1 + _x2) / 2) >> 0
      const cy = ((_y1 + _y3) / 2) >> 0

      if (needXControl) {
        layerDrawer.drawControlPoint(cx, _y1)
        layerDrawer.drawControlPoint(cx, _y3)
      }
      if (needYControl) {
        layerDrawer.drawControlPoint(_x2, cy)
        layerDrawer.drawControlPoint(_x1, cy)
      }
    }

    ctx.fill()
    ctx.stroke()
    ctx.beginPath()
  }
  ctx.shadowOffsetX = 0
  ctx.shadowOffsetY = 0
  ctx.shadowBlur = 0
}

function drawCropControl(
  layerDrawer: EditLayerDrawer,
  posInfo: RectPositionInfo
) {
  const layer = layerDrawer.editLayer
  const ctx = layer.context

  const { pos, canFillRect } = posInfo
  const { x1, x2, x3, x4, y1, y2, y3, y4 } = pos

  let pixLengthX = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2))
  let pixLengthY = Math.sqrt(Math.pow(x1 - x3, 2) + Math.pow(y1 - y3, 2))

  if (pixLengthX < 1) pixLengthX = 1
  if (pixLengthY < 1) pixLengthY = 1

  const centralDotNeed = CONTROL_CROP_CENTRAL_DOT_NEED_MIN_WIDTH

  if (canFillRect) {
    layer.updateByRect(x1, y1, x4 - x1, y4 - y1)
    const width = (x4 - x1 + 1) >> 1
    const isDrawXCentralDot = width > centralDotNeed ? true : false
    const cornerW = Math.min(width, CONTROL_CROP_CORNER_SIZE)

    const height = (y4 - y1 + 1) >> 1
    const isDrawYCentralDot = height > centralDotNeed ? true : false
    const cornerH = Math.min(height, CONTROL_CROP_CORNER_SIZE)

    ctx.rect(x1 + 0.5, y2 + 0.5, x4 - x1 + 1, y4 - y1)
    ctx.strokeStyle = COLOR_BLACK
    ctx.stroke()

    ctx.beginPath()

    ctx.strokeStyle = COLOR_GREY
    ctx.fillStyle = COLOR_BLACK

    ctx.moveTo(x1 + 0.5, y1 + 0.5)
    ctx.lineTo(x1 + cornerW + 0.5, y1 + 0.5)
    ctx.lineTo(x1 + cornerW + 0.5, y1 + 5.5)
    ctx.lineTo(x1 + 5.5, y1 + 5.5)
    ctx.lineTo(x1 + 5.5, y1 + cornerW + 0.5)
    ctx.lineTo(x1 + 0.5, y1 + cornerW + 0.5)
    ctx.closePath()

    ctx.moveTo(x2 - cornerW + 0.5, y2 + 0.5)
    ctx.lineTo(x2 + 0.5, y2 + 0.5)
    ctx.lineTo(x2 + 0.5, y2 + cornerH + 0.5)
    ctx.lineTo(x2 - 4.5, y2 + cornerH + 0.5)
    ctx.lineTo(x2 - 4.5, y2 + 5.5)
    ctx.lineTo(x2 - cornerW + 0.5, y2 + 5.5)
    ctx.closePath()

    ctx.moveTo(x4 - 4.5, y4 - cornerH + 0.5)
    ctx.lineTo(x4 + 0.5, y4 - cornerH + 0.5)
    ctx.lineTo(x4 + 0.5, y4 + 0.5)
    ctx.lineTo(x4 - cornerW + 0.5, y4 + 0.5)
    ctx.lineTo(x4 - cornerW + 0.5, y4 - 4.5)
    ctx.lineTo(x4 - 4.5, y4 - 4.5)
    ctx.closePath()

    ctx.moveTo(x3 + 0.5, y3 - cornerH + 0.5)
    ctx.lineTo(x3 + 5.5, y3 - cornerH + 0.5)
    ctx.lineTo(x3 + 5.5, y3 - 4.5)
    ctx.lineTo(x3 + cornerW + 0.5, y3 - 4.5)
    ctx.lineTo(x3 + cornerW + 0.5, y3 + 0.5)
    ctx.lineTo(x3 + 0.5, y3 + 0.5)
    ctx.closePath()

    if (isDrawXCentralDot) {
      const cx = (x4 + x1 - cornerW) >> 1
      ctx.moveTo(cx + 0.5, y1 + 0.5)
      ctx.lineTo(cx + cornerW + 0.5, y1 + 0.5)
      ctx.lineTo(cx + cornerW + 0.5, y1 + 5.5)
      ctx.lineTo(cx + 0.5, y1 + 5.5)
      ctx.closePath()

      ctx.moveTo(cx + 0.5, y4 - 4.5)
      ctx.lineTo(cx + cornerW + 0.5, y4 - 4.5)
      ctx.lineTo(cx + cornerW + 0.5, y4)
      ctx.lineTo(cx + 0.5, y4 + 0.5)
      ctx.closePath()
    }

    if (isDrawYCentralDot) {
      const cy = (y4 + y1 - cornerH) >> 1
      ctx.moveTo(x1 + 0.5, cy + 0.5)
      ctx.lineTo(x1 + 5.5, cy + 0.5)
      ctx.lineTo(x1 + 5.5, cy + cornerH + 0.5)
      ctx.lineTo(x1 + 0.5, cy + cornerH + 0.5)
      ctx.closePath()

      ctx.moveTo(x4 - 4.5, cy + 0.5)
      ctx.lineTo(x4 + 0.5, cy + 0.5)
      ctx.lineTo(x4 + 0.5, cy + cornerH + 0.5)
      ctx.lineTo(x4 - 4.5, cy + cornerH + 0.5)
      ctx.closePath()
    }

    ctx.fill()
    ctx.stroke()

    ctx.beginPath()
  } else {
    layer.update(x1, y1)
    layer.update(x2, y2)
    layer.update(x3, y3)
    layer.update(x4, y4)

    let ex1 = (x2 - x1) / pixLengthX
    let ey1 = (y2 - y1) / pixLengthX
    let ex2 = (x1 - x3) / pixLengthY
    let ey2 = (y1 - y3) / pixLengthY

    const absEX1 = Math.abs(ex1) < 0.01
    const absEY1 = Math.abs(ey1) < 0.01
    const absEX2 = Math.abs(ex2) < 0.01
    const absEY2 = Math.abs(ey2) < 0.01

    if (absEX2 && absEY2) {
      if (absEX1 && absEY1) {
        ex1 = 1
        ey1 = 0
        ex2 = 0
        ey2 = 1
      } else {
        ex2 = -ey1
        ey2 = ex1
      }
    } else if (absEX1 && absEY1) {
      ex1 = ey2
      ey1 = -ex2
    }

    const width = pixLengthX >> 1
    const isDrawXCentralDot = width > centralDotNeed ? true : false
    const cornerW = Math.min(width, CONTROL_CROP_CORNER_SIZE)
    const height = pixLengthY >> 1
    const isDrawYCentralDot = height > centralDotNeed ? true : false
    const cornerH = Math.min(height, CONTROL_CROP_CORNER_SIZE)

    ctx.moveTo(x1, y1)
    ctx.lineTo(x2, y2)
    ctx.lineTo(x4, y4)
    ctx.lineTo(x3, y3)
    ctx.closePath()
    ctx.strokeStyle = COLOR_BLACK
    ctx.stroke()

    ctx.beginPath()

    ctx.strokeStyle = COLOR_GREY
    ctx.fillStyle = COLOR_BLACK

    const offsetX1 = cornerW * ex1
    const offsetX2 = 5 * ex1
    const offsetX3 = -cornerH * ex2
    const offsetX4 = -5 * ex2
    const offsetY1 = cornerW * ey1
    const offsetY2 = 5 * ey1
    const offsetY3 = -cornerH * ey2
    const offsetY4 = -5 * ey2

    ctx.moveTo(x1, y1)
    ctx.lineTo(x1 + offsetX1, y1 + offsetY1)
    ctx.lineTo(x1 + offsetX1 + offsetX4, y1 + offsetY1 + offsetY4)
    ctx.lineTo(x1 + offsetX2 + offsetX4, y1 + offsetY2 + offsetY4)
    ctx.lineTo(x1 + offsetX2 + offsetX3, y1 + offsetY2 + offsetY3)
    ctx.lineTo(x1 + offsetX3, y1 + offsetY3)
    ctx.closePath()

    ctx.moveTo(x2 - offsetX1, y2 - offsetY1)
    ctx.lineTo(x2, y2)
    ctx.lineTo(x2 + offsetX3, y2 + offsetY3)
    ctx.lineTo(x2 + offsetX3 - offsetX2, y2 + offsetY3 - offsetY2)
    ctx.lineTo(x2 - offsetX2 + offsetX4, y2 - offsetY2 + offsetY4)
    ctx.lineTo(x2 - offsetX1 + offsetX4, y2 - offsetY1 + offsetY4)
    ctx.closePath()

    ctx.moveTo(x4 - offsetX3 - offsetX2, y4 - offsetY3 - offsetY2)
    ctx.lineTo(x4 - offsetX3, y4 - offsetY3)
    ctx.lineTo(x4, y4)
    ctx.lineTo(x4 - offsetX1, y4 - offsetY1)
    ctx.lineTo(x4 - offsetX1 - offsetX4, y4 - offsetY1 - offsetY4)
    ctx.lineTo(x4 - offsetX2 - offsetX4, y4 - offsetY2 - offsetY4)
    ctx.closePath()

    ctx.moveTo(x3 - offsetX3, y3 - offsetY3)
    ctx.lineTo(x3 - offsetX3 + offsetX2, y3 - offsetY3 + offsetY2)
    ctx.lineTo(x3 - offsetX4 + offsetX2, y3 - offsetY4 + offsetY2)
    ctx.lineTo(x3 + offsetX1 - offsetX4, y3 + offsetY1 - offsetY4)
    ctx.lineTo(x3 + offsetX1, y3 + offsetY1)
    ctx.lineTo(x3, y3)
    ctx.closePath()

    if (isDrawXCentralDot) {
      let cx = x1 + (ex1 * (pixLengthX - cornerW)) / 2
      let cy = y1 + (ey1 * (pixLengthX - cornerW)) / 2
      ctx.moveTo(cx, cy)
      ctx.lineTo(cx + offsetX1, cy + offsetY1)
      ctx.lineTo(cx + offsetX1 + offsetX4, cy + offsetY1 + offsetY4)
      ctx.lineTo(cx + offsetX4, cy + offsetY4)
      ctx.closePath()

      cx = x3 + (ex1 * (pixLengthX - cornerW)) / 2 - offsetX4
      cy = y3 + (ey1 * (pixLengthX - cornerW)) / 2 - offsetY4
      ctx.moveTo(cx, cy)
      ctx.lineTo(cx + offsetX1, cy + offsetY1)
      ctx.lineTo(cx + offsetX1 + offsetX4, cy + offsetY1 + offsetY4)
      ctx.lineTo(cx + offsetX4, cy + offsetY4)
      ctx.closePath()
    }

    if (isDrawYCentralDot) {
      let cx = x1 - (ex2 * (pixLengthY - cornerH)) / 2
      let cy = y1 - (ey2 * (pixLengthY - cornerH)) / 2
      ctx.moveTo(cx, cy)
      ctx.lineTo(cx + offsetX2, cy + offsetY2)
      ctx.lineTo(cx + offsetX2 + offsetX3, cy + offsetY2 + offsetY3)
      ctx.lineTo(cx + offsetX3, cy + offsetY3)
      ctx.closePath()

      cx = x2 - (ex2 * (pixLengthY - cornerH)) / 2 - offsetX2
      cy = y2 - (ey2 * (pixLengthY - cornerH)) / 2 - offsetY2
      ctx.moveTo(cx, cy)
      ctx.lineTo(cx + offsetX2, cy + offsetY2)
      ctx.lineTo(cx + offsetX2 + offsetX3, cy + offsetY2 + offsetY3)
      ctx.lineTo(cx + offsetX3, cy + offsetY3)
      ctx.closePath()
    }

    ctx.fill()
    ctx.stroke()

    ctx.beginPath()
  }
}

function drawHoveringOutLine(
  layerDrawer: EditLayerDrawer,
  type: typeof EditControlType.HoveringOutline | typeof EditControlType.ViewMod,
  isLine: boolean,
  posInfo: RectPositionInfo
) {
  const layer = layerDrawer.editLayer
  const ctx = layer.context

  const { pos, canFillRect, canFillRectWithflip } = posInfo
  const { x1, x2, x3, x4, y1, y2, y3, y4 } = pos

  let strokeStyle = COLOR_BLACK
  let alpha = 1
  if (type === EditControlType.HoveringOutline) {
    strokeStyle = COLOR_GREY
    alpha = 0.3
  }
  ctx.save()
  ctx.strokeStyle = strokeStyle
  ctx.globalAlpha = alpha
  if (canFillRect) {
    layer.updateByRect(x1, y1, x4 - x1, y4 - y1)
    if (!isLine) {
      ctx.rect(x1 + 0.5, y2 + 0.5, x4 - x1, y4 - y1)
      ctx.stroke()
      ctx.beginPath()
    }
  } else {
    let [_x1, _y1, _y2, _x4, _y4] = [x1, y1, y2, x4, y4]

    if (canFillRectWithflip) {
      _x1 = x1
      if (x2 < _x1) _x1 = x2
      if (x3 < _x1) _x1 = x3
      if (x4 < _x1) _x1 = x4

      _x4 = x1
      if (x2 > _x4) _x4 = x2
      if (x3 > _x4) _x4 = x3
      if (x4 > _x4) _x4 = x4

      _y1 = y1
      if (y2 < _y1) _y1 = y2
      if (y3 < _y1) _y1 = y3
      if (y4 < _y1) _y1 = y4

      _y4 = y1
      if (y2 > _y4) _y4 = y2
      if (y3 > _y4) _y4 = y3
      if (y4 > _y4) _y4 = y4

      _y2 = _y1
    }

    if (!isLine) {
      layer.update(x1, y1)
      layer.update(x2, y2)
      layer.update(x3, y3)
      layer.update(x4, y4)
      if (canFillRectWithflip) {
        ctx.rect(_x1 + 0.5, _y2 + 0.5, _x4 - _x1, _y4 - _y1)
        ctx.stroke()
        ctx.beginPath()
      } else {
        ctx.beginPath()
        ctx.moveTo(x1, y1)
        ctx.lineTo(x2, y2)
        ctx.lineTo(x4, y4)
        ctx.lineTo(x3, y3)
        ctx.closePath()
        ctx.stroke()
      }
    }
  }
  ctx.restore()
}

/** 根据长度计算边长度以及是否需要边控点等信息 */
function calcLinesInfo(posInfo: RectPositionInfo) {
  const { pos, transformType } = posInfo
  const { x1, x2, x3, y1, y2, y3 } = pos

  let pixLengthX = Math.sqrt(Math.pow(x1 - x2, 2) + Math.pow(y1 - y2, 2))
  let pixLengthY = Math.sqrt(Math.pow(x1 - x3, 2) + Math.pow(y1 - y3, 2))

  if (pixLengthX < 1) pixLengthX = 1
  if (pixLengthY < 1) pixLengthY = 1
  let needXControl = pixLengthX >= 30 ? true : false
  let needYControl = pixLengthY >= 30 ? true : false
  if (transformType === RectTransformType.Eavert) {
    ;[needXControl, needYControl] = [needYControl, needXControl]
  }
  return { pixLengthX, pixLengthY, needXControl, needYControl }
}
