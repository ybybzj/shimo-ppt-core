import { EditorKit } from '../../../editor/global'
import { Factor_pix_to_mm } from '../../../core/common/const/unit'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { TextDocument } from '../../../core/TextDocument/TextDocument'
import { ParaRun } from '../../../core/Paragraph/ParaContent/ParaRun'
import { SlideMaster } from '../../../core/Slide/SlideMaster'
import { LayoutType } from '../../../core/SlideElement/const'

import { ParaPr } from '../../../core/textAttributes/ParaPr'
import { TextPr } from '../../../core/textAttributes/TextPr'
import { EditorUtil } from '../../../globals/editor'
import { getCanvasDrawer } from '../../../core/graphic/CanvasDrawer'
import { toggleCxtTransformReset } from '../../../core/graphic/utils'
import { translator } from '../../../globals/translator'
import { calculateParagraphByColumn } from '../../../core/Paragraph/typeset/calculate'
import { renderParagraph } from '../../../core/Paragraph/typeset/render'
import { renderSlideBackground } from '../../../core/render/slide'
import { BrowserInfo } from '../../../common/browserInfo'
import { Nullable, valuesOfDict } from '../../../../liber/pervasive'
import { updateRGBAByThemeColorId } from '../../../core/color/complexColor'
import { FillEffects } from '../../../core/SlideElement/attrs/fill/fill'
import { calculateForSlideLayout } from '../../../core/calculation/calculate/slideLayout'
import { moveCursorToStartPosInParagraph } from '../../../core/utilities/cursor/moveToEdgePos'
import { ThemeColor, ThemeColorValue } from '../../../io/dataType/spAttrs'
import { InstanceType } from '../../../core/instanceTypes'
import { ColorType } from '../../../core/common/const/attrs'

export class MasterPreviewsDrawer {
  canvas: Nullable<HTMLCanvasElement>
  widthInMM: number
  heightInMM: number
  widthInPx: number
  heightInPx: number

  constructor() {
    this.canvas = null
    this.widthInMM = 0
    this.heightInMM = 0

    this.widthInPx = 0
    this.heightInPx = 0
  }

  getThumbnail(master: SlideMaster, useBg?, useMasterSp?) {
    this.widthInPx = 200
    this.heightInPx = 108

    const ratio = 100 / 38

    if (this.canvas == null) {
      this.canvas = document.createElement('canvas')
    }

    const w = this.widthInPx * BrowserInfo.PixelRatio
    const h = this.heightInPx * BrowserInfo.PixelRatio

    this.canvas.width = w
    this.canvas.height = h

    const ctx = this.canvas.getContext('2d')!
    const drawCxt = getCanvasDrawer(ctx, w, h, this.widthInMM, this.heightInMM)
    drawCxt.updateTransform(1, 0, 0, 1, 0, 0)

    let bgFill: Nullable<FillEffects>
    let rgba = { r: 0, g: 0, b: 0, a: 255 }

    const layout = master.layouts.find(
      (l) =>
        l.type === LayoutType.Title ||
        l.cSld.name === '标题幻灯片' ||
        l.cSld.name === 'Title Slide' ||
        translator.getValue(l.cSld.name) === 'Title Slide'
    )
    const theme = master.theme!

    if (layout != null && layout.cSld.bg != null) {
      if (null != layout.cSld.bg.bgPr) {
        bgFill = layout.cSld.bg.bgPr.fill
      } else if (layout.cSld.bg.bgRef != null) {
        layout.cSld.bg.bgRef.color.updateBy(theme, null, layout, master, rgba)
        rgba = layout.cSld.bg.bgRef.color.rgba
        bgFill = theme.themeElements.fmtScheme.getFillStyle(
          layout.cSld.bg.bgRef.idx,
          layout.cSld.bg.bgRef.color
        )
      }
    } else if (master != null) {
      if (master.cSld.bg != null) {
        if (null != master.cSld.bg.bgPr) bgFill = master.cSld.bg.bgPr.fill
        else if (master.cSld.bg.bgRef != null) {
          master.cSld.bg.bgRef.color.updateBy(theme, null, layout, master, rgba)
          rgba = master.cSld.bg.bgRef.color.rgba
          bgFill = theme.themeElements.fmtScheme.getFillStyle(
            master.cSld.bg.bgRef.idx,
            master.cSld.bg.bgRef.color
          )
        }
      } else {
        bgFill = FillEffects.SolidRGBA(255, 255, 255, 255)
      }
    }

    if (bgFill != null) {
      bgFill.calculate(theme, null, layout, master, rgba)
    }

    if (useBg !== false) {
      renderSlideBackground(drawCxt, bgFill, this.widthInMM, this.heightInMM)
    }

    if (useMasterSp !== false) {
      if (null == layout) {
        master.draw(drawCxt)
      } else {
        if (layout.showMasterSp === true || layout.showMasterSp == null) {
          master.draw(drawCxt)
        }
        calculateForSlideLayout(layout)
        layout.draw(drawCxt)
      }
    }

    drawCxt.reset()
    toggleCxtTransformReset(drawCxt, true)

    const needColorW = 6 * ratio * BrowserInfo.PixelRatio
    const needColorH = 3 * ratio * BrowserInfo.PixelRatio
    let needColorStartX = 4 * ratio * BrowserInfo.PixelRatio
    const needColorStartY = 31 * ratio * BrowserInfo.PixelRatio
    const colorDelta = 1 * ratio * BrowserInfo.PixelRatio

    const _rgba = { r: rgba.r, g: rgba.g, b: rgba.b, a: rgba.a }
    for (let i = 0; i < 6; i++) {
      ctx.beginPath()
      updateRGBAByThemeColorId(
        i as valuesOfDict<typeof ThemeColor>,
        _rgba,
        theme,
        null,
        null,
        master,
        rgba
      )
      drawCxt.setBrushColor(_rgba.r, _rgba.g, _rgba.b, 255)

      ctx.fillRect(needColorStartX, needColorStartY, needColorW, needColorH)
      needColorStartX += needColorW + colorDelta
    }
    ctx.beginPath()

    // text
    EditorUtil.ChangesStack.stopRecording()
    const orgMode = EditorSettings.isViewMode
    EditorSettings.isViewMode = true

    let themeColorId: ThemeColorValue = ThemeColor.Text1 // tx1
    const ccolor =
      bgFill?.fill?.instanceType === InstanceType.SolidFill
        ? bgFill.fill.color
        : null
    const _ccolor = ccolor?.getColor()
    if (_ccolor?.type === ColorType.SCHEME) {
      const clrId = _ccolor.id
      // 如果文字颜色与背景相同，尝试向后取 lt1, dk1
      if (clrId === themeColorId) {
        themeColorId = ThemeColor.Light1 // lt1
      }
      if (clrId === themeColorId) {
        themeColorId = ThemeColor.Dark1 // dk1
      }
    }

    updateRGBAByThemeColorId(
      themeColorId,
      _rgba,
      theme,
      null,
      null,
      master,
      rgba
    )

    const textPr1 = TextPr.new()
    const fontScheme = theme.themeElements.fontScheme
    textPr1.fontFamily = {
      name: fontScheme.majorFont.latin!,
      index: -1,
    }
    textPr1.rFonts.ascii = {
      name: fontScheme.majorFont.latin!,
      index: -1,
    }
    textPr1.fontSize = 8 * ratio
    const fill = FillEffects.SolidRGBA(_rgba.r, _rgba.g, _rgba.b, 255)

    textPr1.fillEffects = fill
    const textPr2 = TextPr.new()
    textPr2.fontFamily = {
      name: fontScheme.minorFont.latin!,
      index: -1,
    }
    textPr2.rFonts.ascii = {
      name: fontScheme.minorFont.latin!,
      index: -1,
    }
    textPr2.fontSize = 8 * ratio
    textPr2.fillEffects = fill.clone()

    const textDoc = TextDocument.new(EditorKit.presentation, 0, 0, 1000, 1000)
    const paragraph = textDoc.children.at(0)!
    moveCursorToStartPosInParagraph(paragraph)
    const paraPr = ParaPr.new()
    paragraph.pr = paraPr
    let paraRun = ParaRun.new(paragraph, textPr1)
    paraRun.insertText('A')
    paragraph.addItemAt(0, paraRun)
    paraRun = ParaRun.new(paragraph, textPr2)
    paraRun.insertText('a')
    paragraph.addItemAt(1, paraRun)
    paragraph.renderingState.resetDimension(0, 0, 1000, 1000, 0)
    calculateParagraphByColumn(paragraph, 0)

    TextPr.recycle(textPr1)
    TextPr.recycle(textPr2)

    drawCxt.resetState()
    drawCxt.init(
      ctx,
      this.widthInPx,
      this.heightInPx,
      this.widthInPx * Factor_pix_to_mm,
      this.heightInPx * Factor_pix_to_mm
    )
    drawCxt.updateTransform(1, 0, 0, 1, 0, 0)
    const textX = 8 * Factor_pix_to_mm * ratio * BrowserInfo.PixelRatio
    const textY =
      (this.heightInPx - 11 * ratio) * Factor_pix_to_mm * BrowserInfo.PixelRatio

    const firstLine = paragraph.renderingState.getLine(0)
    if (firstLine) {
      firstLine.segment.XForRender = textX
      firstLine.y = textY
    }
    renderParagraph(paragraph, 0, drawCxt)

    EditorUtil.ChangesStack.enableRecording()
    EditorSettings.isViewMode = orgMode
    drawCxt.resetState()
    try {
      return this.canvas.toDataURL('image/png')
    } catch (err) {
      this.canvas = null
      if (undefined === useBg && undefined === useMasterSp) {
        return this.getThumbnail(master, true, false)
      } else if (useBg && !useMasterSp) {
        return this.getThumbnail(master, false, false)
      }
    }
    return ''
  }
}
