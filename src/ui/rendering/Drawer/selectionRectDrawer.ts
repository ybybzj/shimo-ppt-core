import { Nullable } from '../../../../liber/pervasive'
import { shrinkByRetinaRatio } from '../../../common/browserInfo'
import { EditorSkin } from '../../../core/common/const/drawing'
import { Factor_mm_to_pix } from '../../../core/common/const/unit'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { Matrix2D, MatrixUtils } from '../../../core/graphic/Matrix'
import { EditorUI } from '../../editorUI/EditorUI'

export function drawSelectionRect(
  editorUI: EditorUI,
  slideIndex: number,
  x: number,
  y: number,
  width: number,
  height: number,
  matrix?: Nullable<Matrix2D>
) {
  const renderingController = editorUI.renderingController
  if (
    slideIndex < 0 ||
    slideIndex !== renderingController.curSlideIndex ||
    Math.abs(width) < 0.001 ||
    Math.abs(height) < 0.001
  ) {
    return
  }
  const presentation = editorUI.presentation

  const { left, right, top, bottom } = renderingController.curSlideRect
  let l = left
  let t = top
  const w = right - left
  const h = bottom - top
  let factorX = w / presentation.width
  let factorY = h / presentation.height

  let editLayer = editorUI.renderingController.editLayer
  const notes = editorUI.notesView
  if (EditorSettings.isNotesEnabled && notes && presentation.isNotesFocused) {
    editLayer = notes.editLayer
    l = shrinkByRetinaRatio(notes.offsetX)
    t = -notes.scroll
    factorX = Factor_mm_to_pix
    factorY = Factor_mm_to_pix
  }

  const ctx = editLayer.context

  ctx.save()

  ctx.fillStyle = EditorSkin.Color_Selection
  ctx.beginPath()
  if (null == matrix || MatrixUtils.isIdentity(matrix)) {
    const _x = ((l + factorX * x + 0.5) >> 0) - 0.5
    const _y = ((t + factorY * y + 0.5) >> 0) - 0.5

    const _r = ((l + factorX * (x + width) + 0.5) >> 0) - 0.5
    const _b = ((t + factorY * (y + height) + 0.5) >> 0) - 0.5

    if (_x < editLayer.minX) editLayer.minX = _x
    if (_r > editLayer.maxX) editLayer.maxX = _r

    if (_y < editLayer.minY) editLayer.minY = _y
    if (_b > editLayer.maxY) editLayer.maxY = _b

    ctx.rect(_x, _y, _r - _x + 1, _b - _y + 1)
  } else {
    const _x1 = matrix.XFromPoint(x, y)
    const _y1 = matrix.YFromPoint(x, y)

    const _x2 = matrix.XFromPoint(x + width, y)
    const _y2 = matrix.YFromPoint(x + width, y)

    const _x3 = matrix.XFromPoint(x + width, y + height)
    const _y3 = matrix.YFromPoint(x + width, y + height)

    const _x4 = matrix.XFromPoint(x, y + height)
    const _y4 = matrix.YFromPoint(x, y + height)

    let x1 = l + factorX * _x1
    let y1 = t + factorY * _y1

    let x2 = l + factorX * _x2
    let y2 = t + factorY * _y2

    let x3 = l + factorX * _x3
    let y3 = t + factorY * _y3

    let x4 = l + factorX * _x4
    let y4 = t + factorY * _y4

    if (MatrixUtils.isOnlyTranslate(matrix)) {
      x1 = (x1 >> 0) + 0.5
      y1 = (y1 >> 0) + 0.5

      x2 = (x2 >> 0) + 0.5
      y2 = (y2 >> 0) + 0.5

      x3 = (x3 >> 0) + 0.5
      y3 = (y3 >> 0) + 0.5

      x4 = (x4 >> 0) + 0.5
      y4 = (y4 >> 0) + 0.5
    }

    editLayer.update(x1, y1)
    editLayer.update(x2, y2)
    editLayer.update(x3, y3)
    editLayer.update(x4, y4)

    const ctx = editLayer.context
    ctx.moveTo(x1, y1)
    ctx.lineTo(x2, y2)
    ctx.lineTo(x3, y3)
    ctx.lineTo(x4, y4)
    ctx.closePath()
  }
  ctx.globalAlpha = 0.4
  ctx.fill()
  ctx.globalAlpha = 1.0
  ctx.restore()
}
