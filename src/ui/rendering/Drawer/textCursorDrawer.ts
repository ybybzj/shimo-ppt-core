import { Matrix2D, MatrixUtils } from '../../../core/graphic/Matrix'

import { Factor_mm_to_pix } from '../../../core/common/const/unit'

import { EditorUI } from '../../editorUI/EditorUI'

import { BrowserInfo, shrinkByRetinaRatio } from '../../../common/browserInfo'

import { convertInnerMMToPixPos } from '../../editorUI/utils/utils'

import { ZoomValue } from '../../../core/common/zoomValue'

import { Nullable, valuesOfDict } from '../../../../liber/pervasive'
import { Bounds } from '../../../core/graphic/Bounds'
import { SelectionCursorColorStyle } from './const'
import { limitInvoke } from '../../../common/utils'
import { ColorRGBA } from '../../../core/color/type'
import { styleStringFromRGBA } from '../../../core/graphic/CanvasDrawer'
import { getHitTestContext } from '../../../core/graphic/hittest'

export const cursorBlinkSwitch = {
  isOn: false,
  _prevOn: null,
  setInit(b: boolean) {
    this.isOn = b
    this._prevOn = null
  },
  isStateChanged() {
    return this.isOn !== this._prevOn
  },
  getAndSync() {
    this._prevOn = this.isOn
    return this.isOn
  },
  switchState: limitInvoke(() => {
    const self = cursorBlinkSwitch
    self.isOn = !self.isOn
  }, 600),
}

export function drawTextCursor(
  editorUI: EditorUI,
  x: number, // mm
  y: number, // mm
  size: number,
  cursorColor: ColorRGBA,
  matrix: Nullable<Matrix2D>
) {
  const drawTouchHanlde = editorUI.isInTouchMode()
  const isTransparent =
    !drawTouchHanlde && cursorBlinkSwitch.getAndSync() === false

  const dpr = BrowserInfo.PixelRatio
  const isFocusNotes = editorUI.presentation.isNotesFocused
  const zoomValue = isFocusNotes ? 100 : ZoomValue.value
  const editLayer = isFocusNotes
    ? editorUI.notesView.editLayer
    : editorUI.renderingController.editLayer
  const ctx = editLayer.context

  let width = 2 * dpr
  let height = ((size * zoomValue * Factor_mm_to_pix * dpr) / 100) >> 0

  let top: number, left: number, mmX: number, mmY: number
  if (null != matrix && !MatrixUtils.isOnlyTranslate(matrix)) {
    const x1 = matrix.XFromPoint(x, y)
    const y1 = matrix.YFromPoint(x, y)
    const x2 = matrix.XFromPoint(x, y + size)
    const y2 = matrix.YFromPoint(x, y + size)
    const pos1 = convertInnerMMToPixPos(editorUI, x1, y1)
    const pos2 = convertInnerMMToPixPos(editorUI, x2, y2)

    mmX = (x1 + x2) / 2
    mmY = (y1 + y2) / 2

    width = (Math.abs(pos1.x - pos2.x) >> 0) + 1
    height = (Math.abs(pos1.y - pos2.y) >> 0) + 1

    if (2 > width) width = 2
    if (2 > height) height = 2

    left = (Math.min(pos1.x, pos2.x) * dpr) >> 0
    top = (Math.min(pos1.y, pos2.y) * dpr) >> 0

    ctx.save()

    ctx.setTransform(1, 0, 0, 1, left, top)

    if (width === 2 || height === 2) {
      width = width * dpr
      height = height * dpr

      if (isTransparent) {
        ctx.clearRect(0, 0, width, height)
      }

      ctx.fillStyle = styleStringFromRGBA(
        isTransparent ? alterRgbColor(cursorColor) : cursorColor
      )

      ctx.fillRect(0, 0, width, height)
    } else {
      width = width * dpr
      height = height * dpr
      if (isTransparent) {
        ctx.globalCompositeOperation = 'destination-out'
      }
      ctx.beginPath()
      ctx.strokeStyle = styleStringFromRGBA(cursorColor)
      ctx.lineWidth = 2

      if ((pos1.x - pos2.x) * (pos1.y - pos2.y) >= 0) {
        ctx.moveTo(0, 0)
        ctx.lineTo(width, height)
      } else {
        ctx.moveTo(0, height)
        ctx.lineTo(width, 0)
      }

      ctx.stroke()
    }

    if (drawTouchHanlde) {
      const cursorDir = getCursorDirByPoints(pos1.x, pos1.y, pos2.x, pos2.y)
      drawCursorHanlde(ctx, width, height, circleRadius, dpr, cursorDir)
    }

    ctx.restore()
  } else {
    // let _x = 0
    if (null != matrix) {
      x += matrix.tx
      y += matrix.ty
    }
    mmX = x
    mmY = y + size / 2

    const pos = convertInnerMMToPixPos(editorUI, x, y)

    if (isFocusNotes) {
      const notesView = editorUI.notesView
      pos.x = x * Factor_mm_to_pix + shrinkByRetinaRatio(notesView.getStartX())
      pos.y = y * Factor_mm_to_pix - notesView.scroll
    }

    left = (pos.x * dpr) >> 0
    top = (pos.y * dpr) >> 0

    ctx.save()

    ctx.setTransform(1, 0, 0, 1, left, top)
    if (isTransparent) {
      ctx.clearRect(0, 0, width, height)
    }
    ctx.fillStyle = styleStringFromRGBA(
      isTransparent ? alterRgbColor(cursorColor) : cursorColor
    )
    ctx.fillRect(0, 0, width, height)

    /*-- for debug ---*/
    // const factor = (ZoomValue.Value * Factor_mm_to_pix) / 100
    // const cursorX = x * factor * dpr
    // console.log(`cursorX:`, cursorX)
    // ctx.fillStyle = 'blue'
    // ctx.fillRect(cursorX, 0, width, height)
    //
    // ctx.fillStyle = 'green'
    // ctx.font = '85px Arial, ArialMT, Arial, Arial, Arial'
    // const s = ZoomValue.Value / 100
    // const _text = 'abcdfjgkjfzxcvxcvkdkafghgfhfjfjgjhfjfgkhj'
    // ctx.scale(s, s)
    // ctx.fillText(_text, 0, height * 1.5)
    // console.log(`text width:`, ctx.measureText(_text).width * s)

    if (drawTouchHanlde) {
      drawCursorHanlde(ctx, width, height, circleRadius, dpr, CursorDir.TB)
    }

    ctx.restore()
  }

  editLayer.updateByRect(left / dpr, top / dpr, width, height)
  return {
    mmX,
    mmY,
    left,
    top,
    height,
  }
}

export function hitTestTextCursor(
  px: number, // mm
  py: number, // mm
  x: number,
  y: number,
  size: number,
  editorUI: EditorUI
):
  | { hit: false }
  | { hit: true; mmX: number; mmY: number; isHitHandle: boolean } {
  const dpr = BrowserInfo.PixelRatio
  const textCursor = editorUI.textCursor
  const ctx = getHitTestContext()

  const isFocusNotes = editorUI.presentation.isNotesFocused
  const zoomValue = isFocusNotes ? 100 : ZoomValue.value

  const matrix = textCursor.matrix

  let width = 2 * dpr
  let height = ((size * zoomValue * Factor_mm_to_pix * dpr) / 100) >> 0

  const hitWidth = 15
  let top: number, left: number, mmX: number, mmY: number
  if (null != matrix && !MatrixUtils.isOnlyTranslate(matrix)) {
    const x1 = matrix.XFromPoint(x, y)
    const y1 = matrix.YFromPoint(x, y)
    const x2 = matrix.XFromPoint(x, y + size)
    const y2 = matrix.YFromPoint(x, y + size)
    const pos1 = convertInnerMMToPixPos(editorUI, x1, y1)
    const pos2 = convertInnerMMToPixPos(editorUI, x2, y2)

    mmX = (x1 + x2) / 2
    mmY = (y1 + y2) / 2
    width = (Math.abs(pos1.x - pos2.x) >> 0) + 1
    height = (Math.abs(pos1.y - pos2.y) >> 0) + 1

    if (2 > width) width = 2
    if (2 > height) height = 2

    left = (Math.min(pos1.x, pos2.x) * dpr) >> 0
    top = (Math.min(pos1.y, pos2.y) * dpr) >> 0

    const testPos = convertInnerMMToPixPos(editorUI, px, py)
    px = testPos.x * dpr
    py = testPos.y * dpr
    px -= left
    py -= top

    const cursorDir = getCursorDirByPoints(pos1.x, pos1.y, pos2.x, pos2.y)

    if (width === 2 || height === 2) {
      const widthB = (width === 2 ? hitWidth : width) * dpr
      const heightB = (height === 2 ? hitWidth : height) * dpr
      const lx = width === 2 ? -1 * (hitWidth / 2) * dpr : 0
      const ty = height === 2 ? -1 * (hitWidth / 2) * dpr : 0

      width = width * dpr
      height = height * dpr

      const hitCursor = Bounds.containPoint(px, py, lx, ty, widthB, heightB)
      const hitHandle = hitTestInCursorHandle(
        px,
        py,
        width,
        height,
        circleRadius,
        dpr,
        cursorDir
      )

      if (hitCursor || hitHandle) {
        return { hit: true, mmX, mmY, isHitHandle: hitHandle === true }
      } else {
        return { hit: false }
      }
    } else {
      width = width * dpr
      height = height * dpr
      ctx.beginPath()
      ctx.lineWidth = hitWidth * dpr

      if ((pos1.x - pos2.x) * (pos1.y - pos2.y) >= 0) {
        ctx.moveTo(0, 0)
        ctx.lineTo(width, height)
      } else {
        ctx.moveTo(0, height)
        ctx.lineTo(width, 0)
      }

      ctx.closePath()
      const hitCursor = ctx.isPointInStroke(px, py)
      const hitHandle = hitTestInCursorHandle(
        px,
        py,
        width,
        height,
        circleRadius,
        dpr,
        cursorDir
      )

      if (hitCursor || hitHandle) {
        return { hit: true, mmX, mmY, isHitHandle: hitHandle }
      } else {
        return { hit: false }
      }
    }
  } else {
    if (null != matrix) {
      x += matrix.tx
      y += matrix.ty
    }

    mmX = x
    mmY = y + size / 2
    const pos = convertInnerMMToPixPos(editorUI, x, y)

    if (isFocusNotes) {
      const notesView = editorUI.notesView
      const dx = shrinkByRetinaRatio(notesView.getStartX())
      const dy = -1 * notesView.scroll
      pos.x = x * Factor_mm_to_pix + dx
      pos.y = y * Factor_mm_to_pix + dy
      px = (px * Factor_mm_to_pix + dx) * dpr
      py = (py * Factor_mm_to_pix + dy) * dpr
    } else {
      const testPos = convertInnerMMToPixPos(editorUI, px, py)
      px = testPos.x * dpr
      py = testPos.y * dpr
    }

    left = (pos.x * dpr) >> 0
    top = (pos.y * dpr) >> 0

    px -= left
    py -= top
    const lx = -1 * (hitWidth / 2) * dpr
    const hitCursor = Bounds.containPoint(px, py, lx, 0, hitWidth * dpr, height)
    const hitHandle = hitTestInCursorHandle(
      px,
      py,
      width,
      height,
      circleRadius,
      dpr,
      CursorDir.TB
    )
    if (hitCursor || hitHandle) {
      return { hit: true, mmX, mmY, isHitHandle: hitHandle }
    } else {
      return { hit: false }
    }
  }
}

const CursorDir = {
  TLBR: 1,
  TRBL: 2,
  BRTL: 3,
  BLTR: 4,
  TB: 5,
  BT: 6,
  LR: 7,
  RL: 8,
} as const

const circleRadius = 13
function _isApproximate(a: number, b: number) {
  return (Math.abs(a - b) >> 0) + 1 <= 2
}

function getCursorDirByPoints(sx: number, sy: number, ex: number, ey: number) {
  if (_isApproximate(sx, ex)) {
    // TB/BT
    return sy > ey ? CursorDir.BT : CursorDir.TB
  }

  if (_isApproximate(sy, ey)) {
    // LR/RL
    return sx > ex ? CursorDir.RL : CursorDir.LR
  }

  if (sx < ex) {
    // L
    return sy > ey ? CursorDir.BLTR : CursorDir.TLBR
  } else {
    // R
    return sy > ey ? CursorDir.BRTL : CursorDir.TRBL
  }
}

function getCircleXY(
  width: number,
  height: number,
  r: number,
  dpr: number,
  dir: valuesOfDict<typeof CursorDir>
): Nullable<{ x: number; y: number }> {
  const radius = r * dpr
  const zeroLen = 2 * dpr
  if (width <= zeroLen || height <= zeroLen) {
    switch (dir) {
      case CursorDir.TB: {
        if (width <= zeroLen) {
          return {
            x: 0,
            y: height + radius,
          }
        }
        break
      }
      case CursorDir.BT: {
        if (width <= zeroLen) {
          return {
            x: 0,
            y: -1 * radius,
          }
        }
        break
      }
      case CursorDir.RL: {
        if (height <= zeroLen) {
          return {
            x: -1 * radius,
            y: 0,
          }
        }
        break
      }

      case CursorDir.LR: {
        if (height <= zeroLen) {
          return {
            x: width + radius,
            y: 0,
          }
        }
        break
      }
    }
  } else {
    const hypotenuse = getHypotenuse(width, height)
    const dx = (radius * width) / hypotenuse
    const dy = (radius * height) / hypotenuse
    switch (dir) {
      case CursorDir.TLBR: {
        return {
          x: width + dx,
          y: height + dy,
        }
      }
      case CursorDir.TRBL: {
        return {
          x: -1 * dx,
          y: height + dy,
        }
      }
      case CursorDir.BLTR: {
        return {
          x: width + dx,
          y: -1 * dy,
        }
      }
      case CursorDir.BRTL: {
        return {
          x: -1 * dx,
          y: -1 * dy,
        }
      }
    }
  }
}

function circle(
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  r: number
) {
  ctx.save()
  ctx.globalAlpha = 1
  ctx.fillStyle = styleStringFromRGBA(SelectionCursorColorStyle)
  ctx.beginPath()
  ctx.arc(x, y, r, 0, 2 * Math.PI)

  ctx.fill()
  ctx.closePath()

  ctx.restore()
}

function drawCursorHanlde(
  ctx: CanvasRenderingContext2D,
  width: number,
  height: number,
  r: number,
  dpr: number,
  dir: valuesOfDict<typeof CursorDir>
) {
  const cp = getCircleXY(width, height, r, dpr, dir)
  if (cp) {
    const { x: cx, y: cy } = cp
    circle(ctx, cx, cy, r * dpr)
  }
}

function isPTInCircle(
  px: number,
  py: number,
  cx: number,
  cy: number,
  r: number
) {
  const distance = getHypotenuse(px - cx, py - cy)
  return distance <= r
}

function hitTestInCursorHandle(
  px: number,
  py: number,
  width: number,
  height: number,
  r: number,
  dpr: number,
  dir: valuesOfDict<typeof CursorDir>
) {
  const cp = getCircleXY(width, height, r, dpr, dir)
  if (cp) {
    const { x: cx, y: cy } = cp
    return isPTInCircle(px, py, cx, cy, r * dpr)
  }
  return false
}

// heplers
function getHypotenuse(opposite: number, adjacent: number) {
  return Math.sqrt(
    Math.pow(Math.abs(opposite), 2) + Math.pow(Math.abs(adjacent), 2)
  )
}

function alterRgbColor(c: ColorRGBA): ColorRGBA {
  return { ...c, a: 0 }
}
