export const SlideDrawerCONST = {
  BORDER: 10,
} as const

export const SelectionCursorColorStyle = {
  r: 91,
  g: 160,
  b: 231,
  a: 255,
}
