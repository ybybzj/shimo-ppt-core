import { BrowserInfo } from '../../../common/browserInfo'
import { EditorSkin } from '../../../core/common/const/drawing'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { ZoomValue } from '../../../core/common/zoomValue'
import { Bounds, GraphicsBoundsChecker } from '../../../core/graphic/Bounds'
import { drawAclinicRectWithDashBorder } from '../../../core/graphic/drawUtils'
import {
  getCanvasDrawer,
  getCanvasDrawerWithScale,
  CanvasDrawer,
} from '../../../core/graphic/CanvasDrawer'
import { renderPresentationSlide } from '../../../core/render/slide'
import {
  getCellBoundsByIndex,
  getSpStrokeWidth,
} from '../../../core/utilities/shape/getters'
import { translator } from '../../../globals/translator'
import { InstanceType } from '../../../core/instanceTypes'
import { EditorUI } from '../../editorUI/EditorUI'
import { getSlideCurrentScale, getSlideWH } from '../../helpers'
import { checkCollaborationUsers, COUI_LineWidth } from '../RenderController'
import { getTrackRectPosition } from './EditLayerDrawer'
import { checkBounds_PresentationSlide } from '../../../core/checkBounds/slide'
import { debounce, isNumber } from '../../../common/utils'
import { isLineShape } from '../../../core/utilities/shape/asserts'
// import { setImageSmoothingForCanvasCtx } from '../../../common/dom/utils'
import { Nullable } from '../../../../liber/pervasive'
import {
  getOffscreenCanvas,
  isSupportOffscreenCanvas,
} from '../../../common/dom/OffscreenCanvas'
import { Factor_pix_to_mm } from '../../../core/common/const/unit'

const SLIDE_SIZE_ADJUSTMENT = 20 // !magic number, but useful
const MAX_CACHE_CAVAS_SIDE_LENGTH = 2048
const MAX_SLIDE_CACHE_SIZE = 0.5 * 104857600 // 100 * 0.5 megabytes
// const DisableCache = true
const DisableCache = BrowserInfo.isSafariInMacOs ? true : false

export class SlideRenderer {
  editorUI: EditorUI
  private canvasForCache: Nullable<HTMLCanvasElement>
  private canvasCtxForCache: Nullable<CanvasRenderingContext2D>

  offScreenCanvas: Nullable<HTMLCanvasElement | OffscreenCanvas>

  boundsInfo: GraphicsBoundsChecker
  isEmptySlides: boolean
  private isCached: boolean
  private needRerender: boolean = true
  get presentation() {
    return this.editorUI.editorKit.presentation
  }
  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI

    this.isCached = false
    this.canvasForCache = null
    this.canvasCtxForCache = null

    this.boundsInfo = new GraphicsBoundsChecker()

    this.isEmptySlides = false
  }

  private isPlaying() {
    return this.editorUI.transitionManager.isPlaying()
  }

  getBounds() {
    return this.boundsInfo.bounds
  }

  updateRenderInfo(
    slideIndex: number,
    options?: { keepCache: boolean; scaleVal?: number; drawFromCache?: boolean }
  ) {
    this.isEmptySlides = false

    // zoom & pannig 时无需重新绘制 bounds, 同步缩放即可
    if (options?.keepCache && isNumber(options.scaleVal)) {
      updateSlideBounds(this, slideIndex, options.scaleVal)
    } else {
      updateSlideBounds(this, slideIndex)
    }

    if (options?.keepCache === true) {
      if (options.drawFromCache === false) {
        this.needRerender = true
      }
      return
    }

    this.isCached = false
  }

  private _prepareCacheDrawInfo() {
    const { minX, maxX, minY, maxY } = this.boundsInfo.bounds
    const width = maxX - minX + 1
    const height = maxY - minY + 1

    let newCacheWith = width
    let newCacheHeight = height

    let scale = 1

    if (4 * newCacheWith * newCacheHeight > MAX_SLIDE_CACHE_SIZE) {
      // if (maxSide > maxSideLength) {
      const maxSide = Math.max(newCacheWith, newCacheHeight)
      const maxSideLength = MAX_CACHE_CAVAS_SIDE_LENGTH * BrowserInfo.PixelRatio
      // 限制缓存大小
      if (maxSide > maxSideLength) {
        scale =
          newCacheHeight > newCacheWith
            ? maxSideLength / newCacheHeight
            : maxSideLength / newCacheWith
      }
      this.needRerender = true
    }

    newCacheWith = (scale * newCacheWith) >> 0
    newCacheHeight = (scale * newCacheHeight) >> 0

    return {
      scale,
      width: newCacheWith,
      height: newCacheHeight,
    }
  }

  private _prepareCacheCanvas(newCacheWith: number, newCacheHeight: number) {
    this.canvasForCache =
      this.canvasForCache || document.createElement('canvas')

    if (this.canvasForCache.width !== newCacheWith) {
      this.canvasForCache.width = newCacheWith
    }
    if (this.canvasForCache.height !== newCacheHeight) {
      this.canvasForCache.height = newCacheHeight
    }
    this.canvasCtxForCache = this.canvasForCache.getContext('2d')!

    this.canvasCtxForCache.setTransform(1, 0, 0, 1, 0, 0)
    this.canvasCtxForCache.clearRect(0, 0, newCacheWith, newCacheHeight)
    this.canvasCtxForCache.save()

    this.canvasCtxForCache.fillStyle = EditorSkin.Background_Light
    this.canvasCtxForCache.fillRect(0, 0, newCacheWith, newCacheHeight)
    this.canvasCtxForCache.restore()
  }

  private _drawCache = debounce((slideIndex: number) => {
    const { scale, width, height } = this._prepareCacheDrawInfo()

    this._prepareCacheCanvas(width, height)
    const bounds = this.boundsInfo.bounds
    const { sx, sy } = getSlideCurrentScale(this.editorUI)

    const drawCxt = getCanvasDrawerWithScale(
      this.canvasCtxForCache!,
      sx * scale,
      sy * scale
    )

    const tx = -bounds.minX
    const ty = -bounds.minY
    drawCxt.setCoordTransform(tx * scale, ty * scale)
    drawCxt.updateTransform(1, 0, 0, 1, 0, 0)
    if (EditorSettings.isViewMode) {
      drawCxt.isInViewMode = true
    }
    renderSlide(this, slideIndex, drawCxt)
    drawCxt.resetState()
    this.isCached = true
  }, 50)

  private _prepareMirrorRenderingCtx(renderCanvas: HTMLCanvasElement) {
    const needDrawImage = !isSupportOffscreenCanvas
    if (this.offScreenCanvas == null) {
      this.offScreenCanvas = getOffscreenCanvas(renderCanvas)
    }

    const canvas = this.offScreenCanvas
    const context = canvas.getContext('2d')! as CanvasRenderingContext2D
    const orgContext = needDrawImage ? renderCanvas.getContext('2d')! : context

    const width = canvas.width
    const height = canvas.height

    _resetSlideCanvas(context, width, height)

    if (needDrawImage) {
      orgContext.clearRect(0, 0, width, height)
      // _resetSlideCanvas(orgContext, width, height)
    }

    return { context, orgContext, needDrawImage }
  }

  drawSlide(canvas: HTMLCanvasElement, slideIndex: number) {
    if (this.isPlaying()) {
      return
    }
    const {
      context: drawCtx,
      orgContext,
      needDrawImage,
    } = this._prepareMirrorRenderingCtx(canvas)

    const rect = this.editorUI.renderingController.curSlideRect
    const bounds = this.boundsInfo.bounds

    const x = Math.round(rect.left * BrowserInfo.PixelRatio + bounds.minX)
    const y = Math.round(rect.top * BrowserInfo.PixelRatio + bounds.minY)

    if (this.isEmptySlides) {
      const mainCtx = needDrawImage ? orgContext : drawCtx
      renderEmptySlide(this, mainCtx, x, y)
      return
    }

    if (!DisableCache && this.isCached === false) {
      this._drawCache(slideIndex)
    }

    const w_px = bounds.maxX - bounds.minX + 0.5
    const h_px = bounds.maxY - bounds.minY + 0.5

    const prepareCtx = drawCtx
    // const prepareCtx = needDrawImage ? orgContext : drawCtx
    prepareCtx.clearRect(
      x,
      y,
      w_px + 2 * SLIDE_SIZE_ADJUSTMENT,
      h_px + 2 * SLIDE_SIZE_ADJUSTMENT
    )

    prepareCtx.fillStyle = EditorSkin.Background_Light
    prepareCtx.fillRect(
      x,
      y,
      w_px + 2 * SLIDE_SIZE_ADJUSTMENT,
      h_px + 2 * SLIDE_SIZE_ADJUSTMENT
    )
    // 绘制幻灯片阴影
    const shadowRect = {
      x: Math.round(rect.left * BrowserInfo.PixelRatio),
      y: Math.round(rect.top * BrowserInfo.PixelRatio),
      w: Math.round((rect.right - rect.left) * BrowserInfo.PixelRatio),
      h: Math.round((rect.bottom - rect.top) * BrowserInfo.PixelRatio),
    }
    prepareCtx.save()
    prepareCtx.shadowBlur = 10
    prepareCtx.shadowColor = 'rgba(0, 0, 0, 0.15)'
    prepareCtx.fillStyle = EditorSkin.Background_Light
    prepareCtx.fillRect(shadowRect.x, shadowRect.y, shadowRect.w, shadowRect.h)
    prepareCtx.shadowBlur = 0
    prepareCtx.restore()
    if (DisableCache || this.needRerender || this.isCached === false) {
      drawCtx.save()
      renderSlideWith(
        this,
        drawCtx,
        slideIndex,
        renderSlide,
        x,
        y,
        undefined,
        EditorSettings.isClipByEditorView
      )
      drawCtx.restore()
      renderSlideWith(this, drawCtx, slideIndex, drawSlideAdornment, x, y)
      if (needDrawImage) {
        orgContext.drawImage(this.offScreenCanvas!, 0, 0)
      }
      this.needRerender = false
    } else if (this.isCached) {
      // orgContext.save()
      // setImageSmoothingForCanvasCtx(orgContext, false)
      orgContext.drawImage(
        this.canvasForCache!,
        x >> 0,
        y >> 0,
        w_px >> 0,
        h_px >> 0
      )
      // orgContext.restore()
      renderSlideWith(this, orgContext, slideIndex, drawSlideAdornment, x, y)
    }
  }
}

//helpers
function updateSlideBounds(
  slideRenderer: SlideRenderer,
  slideIndex: number,
  scaleVal?: number
) {
  if (-1 === slideIndex) slideRenderer.isEmptySlides = true

  const boundsInfo = slideRenderer.boundsInfo

  if (isNumber(scaleVal)) {
    if (scaleVal !== 1) {
      boundsInfo.scale(scaleVal)
    }
    return
  }
  const { w_mm, h_mm, w_px, h_px } = getSlideWH(slideRenderer.editorUI)
  boundsInfo.init(w_px, h_px, w_mm, h_mm)
  boundsInfo.transform(1, 0, 0, 1, 0, 0)

  if (slideRenderer.isEmptySlides) {
    boundsInfo._begin()
    boundsInfo._moveTo(0, 0)
    boundsInfo._lineTo(w_mm, 0)
    boundsInfo._lineTo(w_mm, h_mm)
    boundsInfo._lineTo(0, h_mm)
    boundsInfo._close()

    return
  }

  renderSlide(slideRenderer, slideIndex, boundsInfo)
  drawSlideAdornment(slideRenderer, slideIndex, boundsInfo)
}

const EmptySlideTextHeight = 60

function renderEmptySlide(
  slideRenderer: SlideRenderer,
  renderCtx: CanvasRenderingContext2D,
  left: number,
  top: number
) {
  const bounds = slideRenderer.boundsInfo.bounds
  const w = bounds.maxX - bounds.minX + 1
  const h = bounds.maxY - bounds.minY + 1

  renderCtx.lineWidth = 1
  renderCtx.strokeStyle = '#000000'

  renderCtx.save()
  renderCtx.beginPath()
  drawAclinicRectWithDashBorder(
    renderCtx,
    left >> 0,
    top >> 0,
    (left + w) >> 0,
    (top + h) >> 0,
    2,
    2
  )
  renderCtx.beginPath()

  renderCtx.fillStyle = '#3C3C3C'
  renderCtx.textAlign = 'center'

  const fontVal = (((ZoomValue.value * 60) / 100) >> 0) + 'px Arial'
  renderCtx.font = fontVal

  const text = translator.getValue('Click to add first slide')
  const yPos = (top >> 0) + ((h - EmptySlideTextHeight) >> 1)
  renderCtx.fillText(text, left + w / 2, yPos)
  renderCtx.restore()
}

const renderBounds = new Bounds()
function renderSlide(
  slideRenderer: SlideRenderer,
  slideIndex: number,
  drawCxt: GraphicsBoundsChecker | CanvasDrawer,
  onlyRenderWithinViewport = false
) {
  const presentation = slideRenderer.presentation
  if (drawCxt.instanceType === InstanceType.CanvasDrawer) {
    if (onlyRenderWithinViewport) {
      const curSlideRect =
        slideRenderer.editorUI.renderingController.curSlideRect
      const factor = (Factor_pix_to_mm * 100) / ZoomValue.value
      const left = -curSlideRect.left * factor
      const top = -curSlideRect.top * factor
      const right =
        (slideRenderer.offScreenCanvas!.width - curSlideRect.left) * factor
      const bottom =
        (slideRenderer.offScreenCanvas!.height - curSlideRect.top) * factor
      renderBounds.reset(left, top, right, bottom)
    }
    renderPresentationSlide(
      presentation,
      slideIndex,
      drawCxt,
      onlyRenderWithinViewport ? renderBounds : undefined
    )
  } else {
    checkBounds_PresentationSlide(presentation, slideIndex, drawCxt)
  }
}

function drawSlideAdornment(
  slideRenderer: SlideRenderer,
  slideIndex: number,
  drawCxt: GraphicsBoundsChecker | CanvasDrawer
) {
  slideRenderer.presentation.renderComments(slideIndex, drawCxt)
  if (
    drawCxt.instanceType === InstanceType.CanvasDrawer &&
    !EditorSettings.isViewMode
  ) {
    drawCollaborationBorder(slideRenderer.editorUI, slideIndex, drawCxt.context)
  }
}

export function drawCollaborationBorder(
  editorUI: EditorUI,
  slideIndex: number,
  ctx: CanvasRenderingContext2D
) {
  const { presentation, renderingController } = editorUI

  if (slideIndex === presentation.currentSlideIndex) {
    const coStatus = renderingController.getCollborationStatus()
    const curSlide = presentation.slides[presentation.currentSlideIndex]
    if (!curSlide) return
    const slideRefs = Object.keys(coStatus)
    const slideRef = slideRefs.find((ref) => curSlide.getRefId() === ref)
    if (!slideRef) return
    const shapeRefsDict = coStatus[slideRef]
    const shapeRefs = Object.keys(shapeRefsDict)
    const spTree = curSlide.cSld.spTree
    const handler = editorUI.editorHandler
    const selectedSps = handler.getSelectedSlideElements()
    const dpr = BrowserInfo.PixelRatio

    for (const sp of spTree) {
      const spRef = sp.getRefId()
      if (!shapeRefs.includes(spRef) || selectedSps.includes(sp)) continue
      const shapeState = shapeRefsDict[spRef]
      const users = checkCollaborationUsers(shapeState.users)
      if (!users.length || isLineShape(sp)) continue

      const slideRenderingInfo = editorUI.getSlideRenderingInfo()
      const { transform, extX, extY } = sp
      // Table cell 协作状态绘制
      const isTable = sp?.instanceType === InstanceType.GraphicFrame
      if (isTable) {
        let hasSelectTable = false
        const tmpTransform = transform.clone()
        for (const { tableInfo, color } of users) {
          if (!tableInfo) {
            hasSelectTable = true
            continue
          }
          const { rowIndex, cellIndex } = tableInfo
          const bounds = getCellBoundsByIndex(sp, rowIndex, cellIndex)
          if (!bounds) continue
          ;[tmpTransform.tx, tmpTransform.ty] = [bounds.l, bounds.t]
          const { pos } = getTrackRectPosition(
            tmpTransform,
            0,
            0,
            bounds.w,
            bounds.h,
            slideRenderingInfo
          )
          for (const key in pos) {
            pos[key] *= dpr
          }
          const { x1: startX, y1: startY, x2: endX, y3: endY } = pos
          const [cellWidth, cellHeight] = [endX - startX, endY - startY]
          ctx.save()
          ctx.beginPath()
          ctx.lineWidth = COUI_LineWidth
          ctx.strokeStyle = color
          ctx.strokeRect(startX, startY, cellWidth, cellHeight)
          ctx.restore()
        }
        if (!hasSelectTable) return
      }
      const strokeWidth = getSpStrokeWidth(sp)
      const borderOffset = strokeWidth / 2
      const info = getTrackRectPosition(
        transform,
        0 - borderOffset,
        0 - borderOffset,
        extX + 2 * borderOffset,
        extY + 2 * borderOffset,
        slideRenderingInfo
      )
      const {
        pos,
        canFillRect: isClever,
        canFillRectWithflip: valueOfIsCleverWithTransform,
      } = info
      for (const key in pos) {
        pos[key] *= dpr
      }
      const { x1, y1, x2, y2, x3, y3, x4, y4 } = pos
      const latesetUser = users[0]

      ctx.save()
      ctx.lineWidth = COUI_LineWidth
      ctx.strokeStyle = latesetUser.color
      let [_x1, _y1, _x4, _y4] = [x1, y1, x4, y4]
      if (isClever) {
        ctx.rect(_x1, y2, _x4 - _x1, _y4 - _y1)
        ctx.stroke()
        ctx.beginPath()
      } else {
        let _y2 = y2

        if (valueOfIsCleverWithTransform) {
          _x1 = x1
          if (x2 < _x1) _x1 = x2
          if (x3 < _x1) _x1 = x3
          if (x4 < _x1) _x1 = x4

          _x4 = x1
          if (x2 > _x4) _x4 = x2
          if (x3 > _x4) _x4 = x3
          if (x4 > _x4) _x4 = x4

          _y1 = y1
          if (y2 < _y1) _y1 = y2
          if (y3 < _y1) _y1 = y3
          if (y4 < _y1) _y1 = y4

          _y4 = y1
          if (y2 > _y4) _y4 = y2
          if (y3 > _y4) _y4 = y3
          if (y4 > _y4) _y4 = y4

          _y2 = _y1
          ctx.rect(_x1, _y2, _x4 - _x1, _y4 - _y1)
          ctx.stroke()
          ctx.beginPath()
        } else {
          ctx.beginPath()
          ctx.moveTo(x1, y1)
          ctx.lineTo(x2, y2)
          ctx.lineTo(x4, y4)
          ctx.lineTo(x3, y3)
          ctx.closePath()
          ctx.stroke()
        }
      }
      ctx.restore()
    }
  }
}

function renderSlideWith(
  slideRenderer: SlideRenderer,
  ctx: CanvasRenderingContext2D,
  slideIndex: number,
  drawer: (
    slideRenderer: SlideRenderer,
    slideIndex: number,
    cxt: GraphicsBoundsChecker | CanvasDrawer,
    onlyRenderWithinViewport?: boolean
  ) => void,
  left: number,
  top: number,
  ignoreEps = true,
  onlyRenderWithinViewport = false
) {
  const bounds = slideRenderer.boundsInfo.bounds
  const { w_mm, h_mm, w_px, h_px } = getSlideWH(slideRenderer.editorUI)
  const drawCxt = getCanvasDrawer(ctx, w_px, h_px, w_mm, h_mm)
  const tx = left - bounds.minX + (ignoreEps ? 0 : SLIDE_SIZE_ADJUSTMENT)
  const ty = top - bounds.minY + (ignoreEps ? 0 : SLIDE_SIZE_ADJUSTMENT)
  drawCxt.setCoordTransform(tx, ty)
  drawCxt.updateTransform(1, 0, 0, 1, 0, 0)
  if (EditorSettings.isViewMode) {
    drawCxt.isInViewMode = true
  }
  drawer(slideRenderer, slideIndex, drawCxt, onlyRenderWithinViewport)
  drawCxt.resetState()
}

function _resetSlideCanvas(
  context: CanvasRenderingContext2D,
  width: number,
  height: number
) {
  const fillStyle = context.fillStyle
  context.clearRect(0, 0, width, height)
  context.fillStyle = EditorSkin.Background_Light
  context.fillRect(0, 0, width, height)
  context.fillStyle = fillStyle
}
