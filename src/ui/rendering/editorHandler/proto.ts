/* eslint-disable no-redeclare */
import { IEditorHandler } from './EditorHandler'
import { Nullable, valuesOfDict } from '../../../../liber/pervasive'
import { EditorKit } from '../../../editor/global'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { InstanceType } from '../../../core/instanceTypes'
import { RenderController } from '../RenderController'
import { Matrix2D } from '../../../core/graphic/Matrix'
import { ParaText } from '../../../core/Paragraph/RunElement/ParaText'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { TextPr } from '../../../core/textAttributes/TextPr'
import {
  drawAdjustmentPoints,
  updateSelectionStateForSlideElement,
} from './utils'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { mmsPerPt } from '../../../core/utilities/helpers'
import {
  isGroup,
  isLineShape,
  isPlaceholderWithEmptyContent,
  isSlideElement,
  isSlideElementInPPT,
} from '../../../core/utilities/shape/asserts'
import { getTextDocumentOfSp } from '../../../core/utilities/shape/getters'
import { canRotate } from '../../../core/utilities/shape/operations'
import { addToParentSpTree } from '../../../core/utilities/shape/updates'
import {
  PointerEventInfo,
  POINTER_EVENT_TYPE,
} from '../../../common/dom/events'
import { Paragraph } from '../../../core/Paragraph/Paragraph'
import { checkExtentsByTextContent } from '../../../core/utilities/shape/extents'
import { SlideElement } from '../../../core/SlideElement/type'
import { ParaRun } from '../../../core/Paragraph/ParaContent/ParaRun'
import { ParaCollection } from '../../../core/Paragraph/ParaCollection'
import { AddGeometryControl } from '../../features/addShape/addGeometryControl'
import { UIOperationStateKinds } from '../../states/type'
import { createUIState, UIStateParams } from '../../states/creator'
import { syncInterfaceState } from '../../../editor/interfaceState/syncInterfaceState'

import {
  bringBackward,
  bringForward,
  bringToFront,
  sendToBack,
} from '../../../core/utilities/presentation/reorder'
import { PPTPositionState } from '../../../collaboration/PPTPosition'
import { getPositionStateForTableOrTextDoc } from '../../../core/utilities/tableOrTextDocContent/position'
import {
  moveCursorDownInTable,
  moveCursorDownInTextDocument,
  moveCursorUpForTableOrContent,
} from '../../../core/utilities/cursor/moveHorizontal'
import {
  moveCursorToEndPosInTable,
  moveCursorToEndPosInTextDocument,
  moveCursorToStartPosInTable,
  moveCursorToStartPosInTextDocument,
} from '../../../core/utilities/cursor/moveToEdgePos'

import {
  moveCursorForwardInTextDocument,
  moveCursorLeftInTable,
  moveCursorLeftInTextDocument,
  moveCursorRightInTable,
  moveCursorRightInTextDocument,
} from '../../../core/utilities/cursor/moveVertical'
import {
  moveCursorToEndOfLineInTable,
  moveCursorToEndOfLineInTextDocument,
  moveCursorToStartOfLineInTable,
  moveCursorToStartOfLineInTextDocument,
} from '../../../core/utilities/cursor/moveToEdgeOfLine'
import { moveSelectedElements } from '../../features/move/utils'
import { moveCursorOutsideParaRun } from '../../../core/utilities/cursor/paraItem'
import {
  drawSelectionForTable,
  drawSelectionForTextDocument,
} from '../../../core/utilities/selection/draw'
import {
  getCurrentParagraph,
  getCurrentTextPr,
} from '../../../core/utilities/tableOrTextDocContent/getter'
import { EditControlType } from '../../../core/common/const/ui'
import { startEditAction } from '../../../core/calculation/informChanges/startAction'
import { calculateForRendering } from '../../../core/calculation/calculate/calculateForRendering'
import { FocusTarget } from '../../../core/common/const/drawing'
import { handleDoubleClickEmptyShape } from '../../editorUI/utils/pointers'
import {
  setSelectionEndForTable,
  setSelectionEndForTextDocument,
  setSelectionStartForTable,
  setSelectionStartForTextDocument,
} from '../../../core/utilities/selection/setter'
import { NumberingFormat } from '../../../core/common/numberingFormat'
import { calcGroupShapeRenderingRect } from '../../../core/utilities/shape/group'
import { BrowserInfo } from '../../../common/browserInfo'

const moveDistance = mmsPerPt(1)

export function reset(this: IEditorHandler, ignoreContentSelect?: any) {
  this.selectionState.reset(ignoreContentSelect)
  this.changeToState(InstanceType.UIInitState, this)
}

export function saveUIState(handler: IEditorHandler) {
  return { curState: handler.curState }
}

export function hasTextSelection(this: IEditorHandler) {
  const textDoc = this.presentation.getCurrentTextTarget()
  if (textDoc) {
    return textDoc.hasTextSelection()
  } else {
    const selectedElements = this.selectionState?.selectedElements || []
    return selectedElements.length > 0
  }
}

export function createTextDocument(this: IEditorHandler) {
  const selectionState = this.selectionState
  const selectedTextSp = selectionState?.selectedTextSp

  const selectedElements = selectionState?.selectedElements || []
  if (selectedTextSp) {
    return
  }

  if (selectedElements.length === 1) {
    if (selectedElements[0].instanceType === InstanceType.Shape) {
      const shape = selectedElements[0]
      if (!shape.txBody) {
        shape.createTxBody()
      }
      selectionState!.setTextSelection(shape)
    }
  }
}

export function drawSelection(
  this: IEditorHandler,
  renderingController: RenderController
) {
  const isViewMode = EditorSettings.isViewMode
  const selectionStatusInfo = this.selectionState?.selectionStatusInfo
  const drawControl = (
    sp: SlideElement,
    type: valuesOfDict<typeof EditControlType>
  ) => {
    type = isViewMode ? EditControlType.ViewMod : type
    let x = 0
    let y = 0
    let extX = sp.extX
    let extY = sp.extY
    if (isGroup(sp)) {
      const rect = calcGroupShapeRenderingRect(0, 0, extX, extY)
      x = rect.x
      y = rect.y
      extX = rect.extX
      extY = rect.extY
    }
    renderingController.drawControl(
      type,
      sp.getTransform(),
      x,
      y,
      extX,
      extY,
      isLineShape(sp),
      canRotate(sp)
    )
  }

  const drawAdjustmentsFunc = (sp: SlideElement) => {
    if (!isViewMode) {
      drawAdjustmentPoints(renderingController, sp)
    }
  }

  const drawControlForGroup = (
    group: GroupShape,
    selectedElements: SlideElement[],
    type: typeof EditControlType.TEXT | typeof EditControlType.SHAPE
  ) => {
    drawControl(group, EditControlType.TEXT)

    for (const sp of selectedElements) {
      drawControl(sp, type)
    }
    if (selectedElements.length === 1) {
      drawAdjustmentsFunc(selectedElements[0])
    }
  }

  if (selectionStatusInfo) {
    switch (selectionStatusInfo.status) {
      case 'group_text': {
        const group = selectionStatusInfo.group
        const selectedElements = selectionStatusInfo.selectedElements || []
        drawControlForGroup(group, selectedElements, EditControlType.TEXT)

        break
      }
      case 'group_collection': {
        const group = selectionStatusInfo.group
        const selectedElements = selectionStatusInfo.selectedElements || []
        drawControlForGroup(group, selectedElements, EditControlType.SHAPE)
        break
      }
      case 'root_text': {
        const selectedTextSp = selectionStatusInfo.selectedTextSp

        drawControl(selectedTextSp, EditControlType.TEXT)
        drawAdjustmentsFunc(selectedTextSp)
        break
      }
      case 'root_collection': {
        const selectedElements = selectionStatusInfo.selectedElements || []
        for (let i = 0; i < selectedElements.length; ++i) {
          const sp = selectedElements[i]

          drawControl(sp, EditControlType.SHAPE)
        }
        if (selectedElements.length === 1) {
          drawAdjustmentsFunc(selectedElements[0])
        }
        break
      }
    }
  }

  return
}

export function addNewParagraph(this: IEditorHandler) {
  this.presentation.addNewParagraph()
  syncInterfaceState(this.editorUI.editorKit)
}

export function deleteSelectedElements(this: IEditorHandler) {
  startEditAction(EditActionFlag.action_Slide_Remove)
  this.selectionState?.resetRootSelection()
  this._remove(-1)
  calculateForRendering()
}

export function changeToState<K extends UIOperationStateKinds>(
  this: IEditorHandler,
  kind: K,
  ...args: UIStateParams<K>
) {
  this.curState.beforeLeave?.()
  const newState = createUIState(kind, ...args)
  this.curState = newState
}

let lastPara: Nullable<Paragraph>
let lastNumPHRendered: boolean | undefined
export function checkSelectionState(this: IEditorHandler) {
  const renderingController = this.editorUI.renderingController
  const textSp = this.getTargetTextSp()

  if (isSlideElement(textSp) && isSlideElementInPPT(textSp)) {
    updateSelectionStateForSlideElement(this.editorUI, textSp)

    // selection 切换时，重绘段落的 Numbering 符号; 切换到空placeholder时，需要重绘
    const textDoc = getTextDocumentOfSp(textSp)
    const para = textDoc && getCurrentParagraph(textDoc)
    const isDrawNumberingPlaceholder =
      para?.paraNumbering?.bulletInfo &&
      para.paraNumbering.bulletInfo.type !== NumberingFormat.None &&
      para.isEmpty()

    if (
      (isDrawNumberingPlaceholder !== lastNumPHRendered ||
        (lastPara !== para && isDrawNumberingPlaceholder) ||
        isPlaceholderWithEmptyContent(textSp)) &&
      this.editFocusContainer != null
    ) {
      const editorUI = this.editorUI
      editorUI.slideRenderer.updateRenderInfo(
        editorUI.renderingController.curSlideIndex
      )

      editorUI.needRerender = true
      editorUI.renderingController.onUpdateEditLayer()
    }
    lastPara = para
    lastNumPHRendered = isDrawNumberingPlaceholder
  } else {
    if (lastNumPHRendered === true) {
      const editorUI = this.editorUI
      editorUI.slideRenderer.updateRenderInfo(
        editorUI.renderingController.curSlideIndex
      )

      editorUI.needRerender = true
      editorUI.renderingController.onUpdateEditLayer()
    }
    lastPara = undefined
    lastNumPHRendered = undefined
    this.editorUI.textCursor.updateTransform(null)
    this.editorUI.textCursor.hide()
    renderingController.setInTextSelection(false)
  }
}

/** 快捷键删除/UI调用接口删除 */
export function remove(this: IEditorHandler, dir, isRemoveOnlySelection?) {
  const presentation = this.presentation
  if (presentation.getFocusType() === FocusTarget.Thumbnails) {
    this.deleteSlides(presentation.getSelectedSlideIndices())
    return
  }
  if (this.selectionState.hasSelection) {
    startEditAction(EditActionFlag.action_Slide_Remove)
    this._remove(dir, isRemoveOnlySelection)
    calculateForRendering()

    syncInterfaceState(this.editorUI.editorKit)
  }
}

export function _remove(this: IEditorHandler, dir?, isRemoveOnlySelection?) {
  this.presentation.onRemove(dir, isRemoveOnlySelection)
  this.updateEditLayer()
}

export function selectNextElement(this: IEditorHandler, direction) {
  const selectionState = this.selectionState
  if (selectionState && selectionState.hasSelection) {
    let i
    if (direction > 0) {
      const selectNextFunc = (
        handler: IEditorHandler,
        lastSelectedSp: SlideElement
      ) => {
        const spTree = handler.getCurrentSpTree() || []

        if (spTree.length > 0) {
          for (i = spTree.length - 1; i > -1; --i) {
            if (spTree[i] === lastSelectedSp) break
          }
          if (i > -1) {
            const sp = spTree[i < spTree.length - 1 ? i + 1 : 0]
            selectionState.reset()
            selectionState.select(sp)
            if (sp) {
              this.scrollElementIntoView(sp)
            }
          }
        }
      }

      const groupSelection = selectionState.groupSelection
      if (groupSelection) {
        const selectedGroup = groupSelection.group
        for (i = selectedGroup.slideElementsInGroup.length - 1; i > -1; --i) {
          if (selectedGroup.slideElementsInGroup[i].selected) {
            break
          }
        }
        if (i > -1) {
          if (i < selectedGroup.slideElementsInGroup.length - 1) {
            selectionState.resetGroupSelection()
            selectionState.select(selectedGroup.slideElementsInGroup[i + 1])
          } else {
            selectNextFunc(this, selectedGroup)
          }
        }
      } else {
        const selectedSps = this.getSelectedSlideElements()
        const lastSelectedSp = selectedSps[selectedSps.length - 1]
        if (lastSelectedSp.instanceType === InstanceType.GroupShape) {
          selectionState.reset()
          selectionState.select(lastSelectedSp)
          selectionState.selectedGroup = lastSelectedSp
          selectionState.select(lastSelectedSp.slideElementsInGroup[0])
        } else {
          selectNextFunc(this, lastSelectedSp)
        }
      }
    } else {
      const selectPrevFunc = (
        handler: IEditorHandler,
        firstSelectedSp: SlideElement
      ) => {
        const spTree = handler.getCurrentSpTree() || []

        if (spTree.length > 0) {
          for (i = 0; i < spTree.length; ++i) {
            if (spTree[i] === firstSelectedSp) break
          }
          if (i < spTree.length) {
            const sp = spTree[i > 0 ? i - 1 : spTree.length - 1]
            selectionState.reset()
            selectionState.select(sp)
            if (sp) {
              this.scrollElementIntoView(sp)
            }
          }
        }
      }
      const groupSelection = selectionState.groupSelection
      if (groupSelection) {
        const selectedGroup = groupSelection.group
        for (i = 0; i < selectedGroup.slideElementsInGroup.length; ++i) {
          if (selectedGroup.slideElementsInGroup[i].selected) {
            break
          }
        }
        if (i < selectedGroup.slideElementsInGroup.length) {
          if (i > 0) {
            selectionState.resetGroupSelection()
            selectionState.select(selectedGroup.slideElementsInGroup[i - 1])
          } else {
            selectPrevFunc(this, selectedGroup)
          }
        } else {
          return
        }
      } else {
        const selectedSps = this.getSelectedSlideElements()
        const firstSelectedSp = selectedSps[0]
        if (firstSelectedSp.instanceType === InstanceType.GroupShape) {
          selectionState.reset()
          selectionState.select(firstSelectedSp)
          selectionState.selectedGroup = firstSelectedSp
          selectionState.select(
            firstSelectedSp.slideElementsInGroup[
              firstSelectedSp.slideElementsInGroup.length - 1
            ]
          )
        } else {
          selectPrevFunc(this, firstSelectedSp)
        }
      }
    }
    this.updateEditLayer()
    if (this.editFocusContainer) {
      syncInterfaceState(this.editorUI.editorKit)
    }
  }
}

export function scrollElementIntoView(
  this: IEditorHandler,
  element: SlideElement
) {
  if (element) {
    const { snapXs, snapYs } = element
    const centerX = snapXs[2]
    const centerY = snapYs[2]
    this.editorUI.scrollToPosition(centerX, centerY, 0, 0)
  }
}

export function moveCursorToStartPosition(this: IEditorHandler) {
  const target = this.presentation.getCurrentTextTarget(true)
  if (target) {
    if (target.instanceType === InstanceType.Table) {
      moveCursorToStartPosInTable(target)
    } else {
      moveCursorToStartPosInTextDocument(target)
    }
    this.checkSelectionState()
    this.syncEditorUIState()
  }
}

export function moveCursorToEndPosition(this: IEditorHandler) {
  const target = this.presentation.getCurrentTextTarget(true)
  if (target) {
    if (target.instanceType === InstanceType.Table) {
      moveCursorToEndPosInTable(target)
    } else {
      moveCursorToEndPosInTextDocument(target)
    }
    this.checkSelectionState()
    this.syncEditorUIState()
  }
}

export function moveCursorLeft(
  this: IEditorHandler,
  isAddToSelect: boolean /*Shift*/,
  moveWord: boolean /*Ctrl*/
) {
  const selectionState = this.selectionState
  if (!selectionState) return
  const selectedTextSp = selectionState.selectedTextSp
  if (selectedTextSp) {
    if (selectedTextSp.instanceType === InstanceType.GraphicFrame) {
      moveCursorLeftInTable(
        selectedTextSp.graphicObject!,
        isAddToSelect,
        moveWord
      )
    } else {
      const target = this.presentation.getCurrentTextTarget(true)
      if (target) {
        if (target.instanceType === InstanceType.Table) {
          moveCursorLeftInTable(target, isAddToSelect, moveWord)
        } else {
          moveCursorLeftInTextDocument(target, isAddToSelect, moveWord)
        }
      }
    }
    this.checkSelectionState()
  } else {
    if (!selectionState.hasSelection) return
    moveSelectedElements(this, -1 * moveDistance, 0)
  }
  this.syncEditorUIState()
}

export function moveCursorRight(
  this: IEditorHandler,
  addToSelect,
  word,
  _isFromPaste?,
  isMoveForward?
) {
  const selectionState = this.selectionState
  if (!selectionState) return
  const selectedTextSp = selectionState.selectedTextSp
  if (selectedTextSp) {
    if (selectedTextSp.instanceType === InstanceType.GraphicFrame) {
      moveCursorRightInTable(selectedTextSp.graphicObject!, addToSelect, word)
    } else {
      const target = this.presentation.getCurrentTextTarget(true)
      if (target) {
        if (target.instanceType === InstanceType.Table) {
          moveCursorRightInTable(target, addToSelect, word)
        } else {
          moveCursorRightInTextDocument(
            target,
            addToSelect,
            word,
            isMoveForward
          )
        }
      }
    }
    this.checkSelectionState()
  } else {
    if (!selectionState.hasSelection) return
    moveSelectedElements(this, moveDistance, 0)
  }
  this.syncEditorUIState()
}

export function moveCursorUp(this: IEditorHandler, addToSelect) {
  const selectionState = this.selectionState
  if (!selectionState) return
  const selectedTextSp = selectionState.selectedTextSp
  if (selectedTextSp) {
    if (selectedTextSp.instanceType === InstanceType.GraphicFrame) {
      moveCursorUpForTableOrContent(selectedTextSp.graphicObject!, addToSelect)
    } else {
      const target = this.presentation.getCurrentTextTarget(true)
      if (target) {
        moveCursorUpForTableOrContent(target, addToSelect)
      }
    }
    this.checkSelectionState()
  } else {
    if (!selectionState.hasSelection) return
    moveSelectedElements(this, 0, -1 * moveDistance)
  }
  this.syncEditorUIState()
}

export function moveCursorDown(this: IEditorHandler, addToSelect) {
  const selectionState = this.selectionState
  if (!selectionState) return
  const selectedTextSp = selectionState.selectedTextSp
  if (selectedTextSp) {
    if (selectedTextSp.instanceType === InstanceType.GraphicFrame) {
      moveCursorDownInTable(selectedTextSp.graphicObject!, addToSelect)
    } else {
      const target = this.presentation.getCurrentTextTarget(true)
      if (target) {
        if (target.instanceType === InstanceType.TextDocument) {
          moveCursorDownInTextDocument(target, addToSelect)
        } else {
          moveCursorDownInTable(target, addToSelect)
        }
      }
    }
    this.checkSelectionState()
  } else {
    if (!selectionState.hasSelection) return
    moveSelectedElements(this, 0, moveDistance)
  }
  this.syncEditorUIState()
}

export function moveCursorToEndOfLine(this: IEditorHandler, addToSelect) {
  const target = this.presentation.getCurrentTextTarget(true)
  if (target) {
    if (target.instanceType === InstanceType.TextDocument) {
      moveCursorToEndOfLineInTextDocument(target, addToSelect)
    } else {
      moveCursorToEndOfLineInTable(target, addToSelect)
    }

    this.checkSelectionState()
    this.syncEditorUIState()
  }
}

export function moveCursorToStartOfLine(this: IEditorHandler, addToSelect) {
  const target = this.presentation.getCurrentTextTarget(true)
  if (target) {
    if (target.instanceType === InstanceType.Table) {
      moveCursorToStartOfLineInTable(target, addToSelect)
    } else {
      moveCursorToStartOfLineInTextDocument(target, addToSelect)
    }
    this.checkSelectionState()
    this.syncEditorUIState()
  }
}

export function selectAll(this: IEditorHandler) {
  this.presentation.selectAll()
  this.syncEditorUIState()
  this.checkSelectionState()
}

/** 逻辑上等同双击选中更多文本 */
export function selectText(this: IEditorHandler) {
  if (this.textEditingState == null) return
  const sp = this.textEditingState.sp
  const pointerEvt = this.editorUI.pointerEvtInfo
  const { targetX: tx, targetY: ty } = this.editorUI.textCursor
  if (sp.instanceType === InstanceType.Shape) {
    if (!getTextDocumentOfSp(sp)) {
      handleDoubleClickEmptyShape(this, sp)
    }
    const textDoc = getTextDocumentOfSp(sp)
    if (textDoc) {
      setSelectionStartForTextDocument(textDoc, tx, ty, pointerEvt)
      setSelectionEndForTextDocument(textDoc, tx, ty, {
        type: POINTER_EVENT_TYPE.Up,
        clicks: 2,
      })
      this.checkSelectionState()
    }
  } else if (sp.instanceType === InstanceType.GraphicFrame) {
    const table = sp.graphicObject
    if (table) {
      setSelectionStartForTable(table, tx, ty, pointerEvt, true)
      setSelectionEndForTable(
        table,
        tx,
        ty,
        {
          ...pointerEvt,
          clicks: 2,
          type: POINTER_EVENT_TYPE.Up,
        } as PointerEventInfo,
        true
      )
      this.checkSelectionState()
    }
  }
}

export function resetSelectionState(this: IEditorHandler) {
  this.selectionState.reset()
  this.changeToState(InstanceType.UIInitState, this)
  this.checkSelectionState()
}

export function savePositionState(
  this: IEditorHandler,
  state: PPTPositionState
) {
  const textTarget = this.presentation.getCurrentTextTarget(true)
  if (textTarget) {
    getPositionStateForTableOrTextDoc(textTarget, false, false, state.positions)
    getPositionStateForTableOrTextDoc(
      textTarget,
      true,
      true,
      state.startPositions
    )
    getPositionStateForTableOrTextDoc(
      textTarget,
      true,
      false,
      state.endPositions
    )
    state.RenderingSelection = textTarget.selection.isUse
  }
  state.selectionInfo = this.selectionState?.getSelectionState()
  state.UIState = saveUIState(this)
}

export function getSelectionTextTransform(this: IEditorHandler) {
  let result = this.selectionState?.selectedTextSp?.textTransform

  if (result) {
    result = result.clone()
    return result
  }
  return new Matrix2D()
}

export function drawSelectionForText(this: IEditorHandler) {
  const target = this.presentation.getCurrentTextTarget(true)
  if (target) {
    EditorKit.editorUI.textCursor.updateTransform(
      this.getSelectionTextTransform()
    )

    if (target.instanceType === InstanceType.Table) {
      drawSelectionForTable(target)
    } else {
      drawSelectionForTextDocument(target)
    }
  }
}

export function addTextWithPr(
  this: IEditorHandler,
  text: string,
  options: {
    textPr?: TextPr
    moveCursorOutside?: boolean
  } = {}
) {
  const presentation = this.presentation
  if (!presentation.canEdit()) {
    return
  }
  const { slides, currentSlideIndex, width, height } = presentation
  const { textPr, moveCursorOutside } = options
  let textTarget = presentation.getCurrentTextTarget(true)

  if (!textTarget && this.selectionState?.hasSelection) {
    for (let i = 0; i < text.length; i++) {
      const item = ParaText.new(text[i].charCodeAt(0))
      presentation.addToParagraph(item, false, true)
    }
    return
  }

  let needUpdateXfrmShape
  if (!textTarget) {
    if (!slides.length) {
      return
    }
    const curSlide = slides[currentSlideIndex]
    const shapeEditControl = new AddGeometryControl(
      'textRect',
      0,
      0,
      curSlide.layout?.master!.theme,
      curSlide.layout?.master,
      curSlide.layout,
      curSlide
    )
    shapeEditControl.track(new PointerEventInfo(), 0, 0)
    const shape = shapeEditControl.getShape(curSlide)
    shape.setParent(curSlide)
    addToParentSpTree(shape)
    textTarget = getTextDocumentOfSp(shape)
    needUpdateXfrmShape = shape
  }
  if (textTarget) {
    textTarget.remove(-1, true, true)
    const currentTextPr = getCurrentTextPr(textTarget) ?? undefined
    const paragraph = getCurrentParagraph(textTarget)
    if (paragraph && paragraph.parent) {
      const _paragraph = Paragraph.new(paragraph.parent)
      const run = ParaRun.new(_paragraph, currentTextPr)
      run.insertText(text)
      _paragraph.addItemAt(0, run)

      if (textPr) {
        run.applyPr(textPr)
      }

      const adjPos = paragraph.getCurrentAdjacentPosition()

      const paraCollection = new ParaCollection()
      paraCollection.add(_paragraph, false)
      paraCollection.endCollect()
      paragraph.parent.insertSelection(paraCollection, adjPos)

      if (textTarget.hasTextSelection()) {
        if (moveCursorOutside) {
          textTarget.removeSelection()
          moveCursorOutsideParaRun(run, false)
        } else {
          if (textTarget.instanceType === InstanceType.TextDocument) {
            moveCursorForwardInTextDocument(textTarget, false, false)
          } else {
            moveCursorRightInTable(textTarget, false, false)
          }
        }
      } else if (moveCursorOutside) {
        moveCursorOutsideParaRun(run, false)
      }
      const selectTextSp = this.getTargetTextSp()
      if (selectTextSp) {
        checkExtentsByTextContent(selectTextSp)
      }
    }
  }

  if (needUpdateXfrmShape) {
    const shape = needUpdateXfrmShape
    const w = 50
    const h = 30
    shape.spPr.xfrm.setExtX(w)
    shape.spPr.xfrm.setExtY(h)
    shape.spPr.xfrm.setOffX((width - w) / 2)
    shape.spPr.xfrm.setOffY((height - h) / 2)
  }
  calculateForRendering()
}

export function onBringToFront(this: IEditorHandler) {
  bringToFront(this.presentation)
  this.syncEditorUIState()
}

export function onBringForward(this: IEditorHandler) {
  bringForward(this.presentation)
  this.syncEditorUIState()
}

export function onSendToBack(this: IEditorHandler) {
  sendToBack(this.presentation)
  this.syncEditorUIState()
}

export function onBringBackward(this: IEditorHandler) {
  bringBackward(this.presentation)
  this.syncEditorUIState()
}

export function updateEditLayer(
  this: IEditorHandler,
  cb?: () => void | undefined
) {
  this.editorUI.renderingController.onUpdateEditLayer(cb)
}

// 切换应用后focus
let focusForSwitchApp;
export function startDragOut(this: IEditorHandler) {
  if (!EditorSettings.canDragOut) return
  const selectedSp = this.getSelectedSlideElements()[0]
  if (!selectedSp) return
  this.changeToState(InstanceType.DragOutState, this)
  function windowFocus(this: IEditorHandler) {
    if (!this.isDragging()) {
      this.endDragOut()
    }
  }
  if (BrowserInfo.inWindows && !focusForSwitchApp) {
    focusForSwitchApp = windowFocus.bind(this)
    window.addEventListener('focus', focusForSwitchApp, true)
  }
}

export function endDragOut(this: IEditorHandler) {
  if (focusForSwitchApp) {
    window.removeEventListener('focus', focusForSwitchApp, true)
    focusForSwitchApp = null
  }
  if (!EditorSettings.canDragOut) return
  if (this.curState.instanceType !== InstanceType.DragOutState) return
  this.changeToState(InstanceType.UIInitState, this)
}

export function isDragging(this: IEditorHandler) {
  if (!EditorSettings.canDragOut) return false
  if (this.curState.instanceType !== InstanceType.DragOutState) return false
  return this.curState.hasDrag
}

export function canScroll(this: IEditorHandler) {
  if (this.curState.instanceType === InstanceType.DragOutState) {
    return false
  }
  return true
}

export function canZoom(this: IEditorHandler) {
  if (this.curState.instanceType === InstanceType.DragOutState) {
    return false
  }
  return true
}
