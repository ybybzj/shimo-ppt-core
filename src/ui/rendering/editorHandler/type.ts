import * as Proto from './proto'
import * as SlideProto from './slideProto'
import * as SelectionProto from './selectionProto'
import * as PresentationProto from './presentationProto'
import * as CommnetsProto from '../../features/comment/utils'
import * as TableProto from './tableProto'
import { EditorHandlerCtor } from './ctor'
import { EditorUI } from '../../editorUI/EditorUI'

export type IEditorHandler = EditorHandlerCtor &
  typeof Proto &
  typeof SlideProto &
  typeof SelectionProto &
  typeof PresentationProto &
  typeof TableProto &
  typeof CommnetsProto & {
    new (editorUI: EditorUI): IEditorHandler
    constructor: IEditorHandler
  }
