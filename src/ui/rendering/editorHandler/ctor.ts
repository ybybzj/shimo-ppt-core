import { Nullable } from '../../../../liber/pervasive'
import { Comment } from '../../../core/Slide/Comments/Comment'
import { InstanceType } from '../../../core/instanceTypes'
import { SlideElement, TextSlideElement } from '../../../core/SlideElement/type'
import { EditorUI } from '../../editorUI/EditorUI'
import { isInShowMode } from '../../editorUI/utils/utils'
import {
  TextEditingState,
  TextEditingStateValus,
  UIOperationStates,
} from '../../states/type'
import { UIInitState } from '../../states/UIInitState'
import { IEditorHandler } from './type'
import { EditorTouchAction } from '../../editorUI/utils/touches'
import { getTextDocumentOfSp } from '../../../core/utilities/shape/getters'

export class EditorHandlerCtor {
  readonly instanceType = InstanceType.EditorHandler
  curState: UIOperationStates
  hoveredObject: Nullable<Comment | SlideElement>
  touchAction?: EditorTouchAction
  isMultiSelectState = false
  editorUI: EditorUI

  get presentation() {
    return this.editorUI.editorKit.presentation
  }

  get editFocusContainer() {
    const presentation = this.presentation
    return presentation.getCurrentFocusEditContainer()
  }
  get selectionState() {
    const presentation = this.presentation
    return presentation.selectionState
  }

  get textEditingState(): Nullable<{
    state: TextEditingStateValus
    sp: TextSlideElement
  }> {
    const textSp = this.selectionState.selectedTextSp
    if (this.isInShowMod() || textSp == null) {
      return undefined
    }

    const textDoc = getTextDocumentOfSp(textSp)
    if (textDoc && textDoc.hasNoSelection() === false) {
      return { state: TextEditingState.Selecting, sp: textSp }
    }

    return this.editorUI.textCursor.isVisible()
      ? { state: TextEditingState.Modifying, sp: textSp }
      : undefined
  }

  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI
    this.curState = new UIInitState(this as unknown as IEditorHandler)
    this.hoveredObject = null
  }

  isInShowMod() {
    return isInShowMode(this.editorUI)
  }
}
