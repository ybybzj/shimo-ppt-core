import { Presentation } from '../../../core/Slide/Presentation'
import {
  addTableColumn,
  addTableRow,
  distributeTableCells,
  mergeTableCells,
  removeTable,
  removeTableColumn,
  removeTableRow,
  splitTableCells,
} from '../../../core/utilities/presentation/table'
import { syncSelectionState } from '../../editorUI/utils/updates'
import { IEditorHandler } from './type'

export function _applyTableOperation<Args extends any[]>(
  this: IEditorHandler,
  tblOpFn: (
    presentation: Presentation,
    ...args: Args
  ) => { result; isApplied: boolean },
  ...args: Args
) {
  const presentation = this.presentation
  const { result, isApplied } = tblOpFn(presentation, ...args)
  if (isApplied) {
    syncSelectionState(this.editorUI)
    this.syncEditorUIState()
  }

  return result
}
export function onRemoveTable(this: IEditorHandler) {
  const presentation = this.presentation
  const removed = removeTable(presentation)
  if (removed) {
    syncSelectionState(this.editorUI)
    this.syncEditorUIState()
  }
  return removed
}
export function onAddTableRow(this: IEditorHandler, isBefore) {
  return this._applyTableOperation(addTableRow, isBefore)
}

export function onAddTableColumn(this: IEditorHandler, isBefore) {
  return this._applyTableOperation(addTableColumn, isBefore)
}

export function onRemoveTableRow(this: IEditorHandler) {
  return this._applyTableOperation(removeTableRow)
}

export function onRemoveTableColumn(this: IEditorHandler) {
  return this._applyTableOperation(removeTableColumn)
}

export function onDistributeTableCells(this: IEditorHandler, isHorizontal) {
  return this._applyTableOperation(distributeTableCells, isHorizontal)
}

export function onMergeTableCells(this: IEditorHandler) {
  return this._applyTableOperation(mergeTableCells)
}

export function onSplitTableCells(this: IEditorHandler, cols, rows) {
  return this._applyTableOperation(splitTableCells, cols, rows)
}
