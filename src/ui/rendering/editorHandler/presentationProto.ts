import { IEditorHandler } from './type'
import {
  insertHyperlinkFunc,
  canInsertHyperlink as _canInsertHyperlink,
  updateHyperlinkFunc,
  removeHyperLinkFunc,
} from '../../../core/utilities/presentation/hyperlink'
import { SlideMaster } from '../../../core/Slide/SlideMaster'
import { loadPPTPositionState } from './utils'
import { PPTPositionState } from '../../../collaboration/PPTPosition'
import { HyperlinkOptions } from '../../../core/properties/options'

// ------------------------- comment about  -------------------------

export function insertHyperlink(
  this: IEditorHandler,
  hyperlinkOptions: HyperlinkOptions
) {
  insertHyperlinkFunc(this.presentation, hyperlinkOptions)
  this.syncEditorUIState()
}

export function updateHyperlink(
  this: IEditorHandler,
  hyperlinkOptions: HyperlinkOptions
) {
  updateHyperlinkFunc(this.presentation, hyperlinkOptions)
  this.syncEditorUIState()
}

export function removeHyperlink(this: IEditorHandler) {
  removeHyperLinkFunc(this.presentation)
  this.syncEditorUIState()
}

export function canInsertHyperlink(
  this: IEditorHandler,
  isCheckInHyperlink?: boolean
) {
  return _canInsertHyperlink(this.presentation, isCheckInHyperlink)
}

export function onChangeLayout(
  this: IEditorHandler,
  indexes: number[],
  master: SlideMaster,
  layoutIndex: number
) {
  const presentation = this.presentation
  let selectionStateState: PPTPositionState | undefined
  if (presentation.slides[presentation.currentSlideIndex]) {
    selectionStateState = {
      positions: [],
      startPositions: [],
      endPositions: [],
      currentSlideIndex: presentation.currentSlideIndex,
      isNotesFocused: presentation.isNotesFocused,
    }

    this.savePositionState(selectionStateState)
  }

  presentation.changeLayout(indexes, master, layoutIndex)

  if (selectionStateState) {
    presentation.removeSelection()
    loadPPTPositionState(this, selectionStateState)
  }
  this.syncEditorUIState()
}
