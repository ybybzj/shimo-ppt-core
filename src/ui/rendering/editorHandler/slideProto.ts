/* eslint-disable curly */
import { IEditorHandler } from './EditorHandler'
import { isNumber } from '../../../common/utils'
import { Slide } from '../../../core/Slide/Slide'
import { isPlaceholder } from '../../../core/utilities/shape/asserts'
import {
  testTextDocOfShape,
  getSpImages,
} from '../../../core/utilities/shape/getters'
import { checkHitBounds, hitInnerArea } from '../../states/hittests'
import {
  addToParentSpTree,
  setGroup,
  setParentForSlideElement,
} from '../../../core/utilities/shape/updates'
import {
  checkHitInSlideInnerArea,
  getSlideBackgroundImageSrcId,
} from '../../../core/utilities/slide'
import { EditorUtil } from '../../../globals/editor'
import { InstanceType, isInstanceTypeOf } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import { Dict, Nullable } from '../../../../liber/pervasive'
import {
  convertGlobalInnerMMToPixPos,
  convertInnerMMToPixPos,
} from '../../editorUI/utils/utils'
import { syncInterfaceState } from '../../../editor/interfaceState/syncInterfaceState'
import { findClosestParent } from '../../../core/utilities/finders'
import { Evt_onSlideBackgroundChange } from '../../../editor/events/EventNames'
import { cloneSlideElement } from '../../../core/utilities/shape/clone'
import { calculateForRendering } from '../../../core/calculation/calculate/calculateForRendering'
import { ensureSpPrXfrm } from '../../../core/utilities/shape/attrs'
import {
  adjustXfrmOfGroup,
  normalizeGroupExts,
} from '../../../core/utilities/shape/group'
import { HoveredObjectInfo } from '../../../core/properties/SelectedObject'
import { calcTransform } from '../../../core/utilities/shape/calcs'
import { Factor_mm_to_pix } from '../../../core/common/const/unit'
import { ZoomValue } from '../../../core/common/zoomValue'
import { resetConnectionShapesId } from '../../../core/utilities/helpers'
import { each } from '../../../../liber/l/each'
import { applySlideElementFormat } from '../../../core/utilities/shape/formatting'

export function getCurrentSlide(this: IEditorHandler): Slide {
  const presentation = this.presentation
  return presentation.slides[presentation.currentSlideIndex]
}

export function getCurrentSpTree(
  this: IEditorHandler
): SlideElement[] | undefined {
  return this.editFocusContainer?.getSpTree()
}

export function copyFormat(this: IEditorHandler) {
  const spForFormatCopy =
    this.presentation.selectionState.selectedSpForFormatting

  if (spForFormatCopy != null) {
    this.presentation.copySpFormat(spForFormatCopy)
  } else {
    this.presentation.copyTextContentFormat()
  }
}

export function pastePaintFormat(this: IEditorHandler) {
  const { selectedTextSp, selectedElements } = this.presentation.selectionState
  if (selectedTextSp != null) {
    this.pasteTextContentFormat()
  } else {
    if (this.presentation.spFormatCopied !== null) {
      each((item) => {
        applySlideElementFormat(item, this.presentation.spFormatCopied!)
      }, selectedElements)
      this.syncEditorUIState()
    }
  }
}

export function isViewMode(this: IEditorHandler) {
  return this.editorUI.editorKit.getViewMode()
}

export function deleteSlides(this: IEditorHandler, indexes: number[]) {
  const toSlideIndex = this.presentation.deleteSlides(indexes)
  if (toSlideIndex != null) {
    this.editorUI.goToSlide(toSlideIndex, true)
    this.editorUI.calculatePreviews()
  }
}

export function startCalculate(this: IEditorHandler) {
  calculateForRendering()
}

export function pasteTextContentFormat(this: IEditorHandler) {
  if (this.presentation.pasteTextContentFormat()) {
    this.syncEditorUIState()
  }
}

export function copySelectedElements(this: IEditorHandler) {
  let i
  const d = 5.0
  // TODO: (groupSelection?? handler) ?
  const selectedGroup = this.selectionState.selectedGroup
  const selectedElements = [...this.selectionState.selectedElements]
  this.selectionState.reset(undefined)
  if (selectedGroup) {
    normalizeGroupExts(selectedGroup)
  }
  if (!this.editFocusContainer) return
  const originIdMap: Dict = {}
  const copies: SlideElement[] = []
  for (i = 0; i < selectedElements.length; ++i) {
    const sp = selectedElements[i]
    const dup = cloneSlideElement(sp, originIdMap)
    originIdMap[sp.id] = dup.id

    dup.x = sp.x
    dup.y = sp.y
    dup.extX = sp.extX
    dup.extY = sp.extY
    ensureSpPrXfrm(dup, true)
    dup.spPr!.xfrm!.setOffX(dup.x + d)
    dup.spPr!.xfrm!.setOffY(dup.y + d)
    setParentForSlideElement(dup, this.editFocusContainer)
    const isGroupShape = isInstanceTypeOf(dup, InstanceType.GroupShape)
    if (
      !dup.spPr?.xfrm ||
      (isGroupShape && !dup.spPr.xfrm.isNotEmptyForGroup()) ||
      (isGroupShape && !dup.spPr.xfrm.isValid())
    ) {
      calcTransform(dup)
    }
    if (!selectedGroup) {
      addToParentSpTree(dup)
    } else {
      setGroup(dup, sp.group)
      dup.group!.addToSpTree(dup.group!.spTree.length, dup)
    }
    this.selectionState.select(dup)
    copies.push(dup)
  }
  resetConnectionShapesId(copies, originIdMap)
  if (selectedGroup) {
    adjustXfrmOfGroup(selectedGroup)
  }
}

export function resetStateForSlide(this: IEditorHandler) {
  const slide = this.getCurrentSlide()
  if (slide) {
    let needPaint = false
    if (testTextDocOfShape(this.presentation.getCurrentTextTarget(false))) {
      needPaint = true
    }
    this.reset(true)

    if (needPaint) {
      const curSlideIndex = this.presentation.currentSlideIndex
      this.editorUI.onCalculateSlide(curSlideIndex)
    }
    this.checkSelectionState()
  }
}

export function checkIsHitOnErrorImage(
  this: IEditorHandler,
  x: number,
  y: number
): boolean {
  const slide = this.getCurrentSlide()
  let hitObject = checkHitObject(slide.cSld.spTree, x, y)
  if (!hitObject) {
    if (checkHitSlideBackgroundHasErrorLoadedImage(slide, x, y)) {
      return true
    }
    if (slide.layout) {
      const shapes = slide.layout.cSld.spTree.filter((sp) => !isPlaceholder(sp))
      hitObject = checkHitObject(shapes, x, y)
      if (
        !hitObject &&
        checkHitSlideBackgroundHasErrorLoadedImage(slide.layout, x, y)
      ) {
        return true
      }
    }
  }
  if (!hitObject) {
    if (slide.layout?.master) {
      const shapes = slide.layout.master.cSld.spTree.filter(
        (sp) => !isPlaceholder(sp)
      )
      hitObject = checkHitObject(shapes, x, y)
      if (
        !hitObject &&
        checkHitSlideBackgroundHasErrorLoadedImage(slide.layout.master, x, y)
      ) {
        return true
      }
    }
  }

  if (hitObject && checkObjectHasErrorLoadedImage(hitObject)) {
    return true
  }
  return false
}

function checkHitObject(
  spTree: SlideElement[],
  x: number,
  y: number
): SlideElement | undefined {
  const spTreeLen = spTree.length
  for (let i = spTreeLen - 1; i >= 0; --i) {
    const sp = spTree[i]
    if (checkHitBounds(sp, x, y) === false) continue

    if (sp.instanceType === InstanceType.GroupShape) {
      const hitObject = checkHitObject(sp.slideElementsInGroup, x, y)
      if (hitObject) {
        return hitObject
      }
    } else if (hitInnerArea(sp, x, y)) {
      return sp
    }
  }
}

function checkObjectHasErrorLoadedImage(sp): boolean {
  const imagesMap = getSpImages(sp)
  const images = Object.keys(imagesMap)
  const imagesLen = images.length
  if (imagesLen) {
    for (let i = 0; i < imagesLen; i++) {
      if (EditorUtil.imageLoader.checkErrorImageUrl(images[i])) {
        return true
      }
    }
  }

  return false
}

function checkHitSlideBackgroundHasErrorLoadedImage(slide, x, y): boolean {
  if (slide && checkHitInSlideInnerArea(slide, x, y)) {
    const imageSrcId = getSlideBackgroundImageSrcId(slide)
    if (imageSrcId && EditorUtil.imageLoader.checkErrorImageUrl(imageSrcId)) {
      return true
    }
  }
  return false
}

/** @usage 根据 id[] 选中 shape, isExclusive 判断是否取消之前选中状态 */
export function selectObjectByIds(
  this: IEditorHandler,
  ids: string[],
  isExclusive = false
) {
  if (isExclusive) {
    this.selectionState.reset()
  }
  if (!ids.length) return
  let firstSp: SlideElement | undefined
  for (const id of ids) {
    const sp = EditorUtil.entityRegistry.getEntityById(id) as
      | SlideElement
      | undefined
    if (sp) {
      if (!firstSp) firstSp = sp
      const slide = findClosestParent(
        sp,
        (parent) => parent.instanceType === InstanceType.Slide
      )
      if (slide) {
        const presentation = this.presentation
        const targetPage = (slide as Slide).index!
        if (presentation.currentSlideIndex !== targetPage) {
          this.editorUI.goToSlide(targetPage)
        }
        this.selectionState.select(sp)
      }
    }
  }
  if (firstSp) {
    this.editorUI.scrollToPosition(
      firstSp.x,
      firstSp.y,
      firstSp.extX,
      firstSp.extY
    )
  }
  this.updateEditLayer()
  syncInterfaceState(this.editorUI.editorKit)
}

// 这里获取的是相对 editorElement 的位置
export function getSpPosById(this: IEditorHandler, id: string) {
  const sp = EditorUtil.entityRegistry.getEntityById(id) as
    | SlideElement
    | undefined
  if (!sp || sp.parent?.instanceType !== InstanceType.Slide) return
  const { editorUI } = this
  const { l, t } = sp.bounds
  const { x: absX, y: absY } = convertGlobalInnerMMToPixPos(editorUI, l, t)
  const ratio = Factor_mm_to_pix * (ZoomValue.value / 100)
  const [width, height] = [sp.extX * ratio, sp.extY * ratio]
  return { width, height, absX, absY }
}

export function selectElementByRefId(this: IEditorHandler, refId: string) {
  const slides = this.presentation.slides
  for (const slide of slides) {
    const sps = slide.getSpTree()
    const sp = sps.find((s) => s.refId === refId)
    if (sp) {
      this.selectObjectByIds([sp.id], true)
      break
    }
  }
}

// helpers
export function assembleHoverObjectInfo(
  this: IEditorHandler,
  mouseX?: number,
  mouseY?: number
): Nullable<HoveredObjectInfo> {
  const hoveredObject = this.hoveredObject
  if (!hoveredObject) return
  const mmX = hoveredObject.x
  const mmY = hoveredObject.y
  const pixPos = convertInnerMMToPixPos(this.editorUI, mmX, mmY)
  const hoveredInfo: HoveredObjectInfo = {
    objectId: hoveredObject.getId(),
    objectMMCoords: { x: mmX, y: mmY },
    objectPixCoords: { x: pixPos.x, y: pixPos.y }, // TODO
  }
  if (isNumber(mouseX) && isNumber(mouseY)) {
    hoveredInfo.cursorCoords = { x: mouseX, y: mouseY }
  }
  return hoveredInfo
}

export function syncEditorUIState(this: IEditorHandler) {
  syncInterfaceState(this.editorUI.editorKit)
}

export function onChangeBackground(this: IEditorHandler, bg, slideIndices) {
  this.presentation.changeBackground(bg, slideIndices)
  for (let i = 0; i < slideIndices.length; ++i) {
    this.editorUI.onCalculateSlide(slideIndices[i])
  }
  this.syncEditorUIState()
  this.editorUI.editorKit.emitEvent(Evt_onSlideBackgroundChange)
}
