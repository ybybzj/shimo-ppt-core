import * as Proto from './proto'
import * as SlideProto from './slideProto'
import * as SelectionProto from './selectionProto'
import * as PresentationProto from './presentationProto'
import * as CommentsProto from '../../features/comment/utils'
import * as TableProto from './tableProto'
import { EditorHandlerCtor } from './ctor'
import { IEditorHandler } from './type'
import { extend } from '../../../../liber/extend'

extend(EditorHandlerCtor.prototype, Proto)
extend(EditorHandlerCtor.prototype, SlideProto)
extend(EditorHandlerCtor.prototype, SelectionProto)
extend(EditorHandlerCtor.prototype, PresentationProto)
extend(EditorHandlerCtor.prototype, CommentsProto)
extend(EditorHandlerCtor.prototype, TableProto)
export * from './type'
export const EditorHandler = EditorHandlerCtor as unknown as IEditorHandler
