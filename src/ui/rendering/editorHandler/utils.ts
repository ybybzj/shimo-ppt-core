import { IEditorHandler } from './type'
import { InstanceType } from '../../../core/instanceTypes'
import { SlideElement } from '../../../core/SlideElement/type'
import { Shape } from '../../../core/SlideElement/Shape'
import { GraphicFrame } from '../../../core/SlideElement/GraphicFrame'
import {
  RotateGroupControl,
  RotateSingleControl,
} from '../../features/rotate/rotateControls'
import {
  ResizeGroupControl,
  ResizeSingleControl,
} from '../../features/resize/resizeControls'
import { Nullable } from '../../../../liber/pervasive'
import {
  isSlideElement,
  isSlideElementInPPT,
} from '../../../core/utilities/shape/asserts'
import { getTextDocumentOfSp } from '../../../core/utilities/shape/getters'
import { UIOperationStates } from '../../states/type'
import { DrawControlTrackFunc } from '../../features/common/type'
import { EditorUI } from '../../editorUI/EditorUI'
import { Matrix2D } from '../../../core/graphic/Matrix'
import { PPTPositionState } from '../../../collaboration/PPTPosition'
import { TableOrTextDocument } from '../../../core/utilities/type'
import { getAndUpdateCursorPositionForTargetOrTextContent } from '../../../core/utilities/cursor/cursorPosition'
import { drawSelectionForTable } from '../../../core/utilities/selection/draw'
import { setContentSelectionInTableOrTextDocument } from '../../../core/utilities/selection/setter'
import { setPositionStateForTableOrTextDoc } from '../../../core/utilities/tableOrTextDocContent/position'
import { Geometry } from '../../../core/SlideElement/geometry/Geometry'
import { RenderController } from '../RenderController'

export function loadPPTPositionState(
  handler: IEditorHandler,
  positionState: PPTPositionState
) {
  const selectionState = handler.selectionState
  const pptSelectInfo = positionState?.selectionInfo
  if (selectionState && pptSelectInfo) {
    if (pptSelectInfo.selectedGroup) {
      if (
        isSlideElementInPPT(pptSelectInfo.selectedGroup) &&
        !pptSelectInfo.selectedGroup.group
      ) {
        selectionState.select(pptSelectInfo.selectedGroup)
        selectionState.resetGroupSelection()

        const state: PPTPositionState = {
          selectionInfo: pptSelectInfo.groupSelection,
          positions: positionState.positions,
          startPositions: positionState.startPositions,
          endPositions: positionState.endPositions,
          RenderingSelection: positionState.RenderingSelection,
        }
        loadPPTPositionState(handler, state)
      }
    } else if (pptSelectInfo.textSp) {
      const textSp = pptSelectInfo.textSp
      if (isSlideElementInPPT(textSp)) {
        selectionState.select(textSp)
        const textTarget = (
          textSp.instanceType === InstanceType.GraphicFrame
            ? textSp.graphicObject
            : getTextDocumentOfSp(textSp)
        ) as Nullable<TableOrTextDocument>

        if (textTarget) {
          if (true === positionState.RenderingSelection) {
            setContentSelectionInTableOrTextDocument(
              textTarget,
              positionState.startPositions,
              positionState.endPositions,
              0,
              0,
              0
            )
          } else {
            setPositionStateForTableOrTextDoc(
              textTarget,
              positionState.positions,
              0,
              0
            )
          }
          selectionState.setTextSelection(pptSelectInfo.textSp)
        }
      }
    } else if (pptSelectInfo.selection != null) {
      for (let i = 0; i < pptSelectInfo.selection.length; ++i) {
        const shape = pptSelectInfo.selection[i].object
        if (isSlideElementInPPT(shape)) {
          selectionState.select(shape)
        }
      }
    }
  }
  restoreEditState(handler, positionState)

  return !!selectionState?.hasSelection
}

export function restoreEditState(
  handler: IEditorHandler,
  positionState?: PPTPositionState
) {
  if (positionState && positionState.UIState) {
    const state: {
      curState: UIOperationStates
    } = positionState.UIState

    handler.curState = state.curState
    // Crop Side Effect: cropBackGround 为生成的临时数据，因此需要手动 calc
    if (state.curState.instanceType === InstanceType.CropState) {
      state.curState.refreshState()
    }
  }
}

export function getAllShapes(
  arrOfSp: SlideElement[],
  allSps?: Nullable<SlideElement[]>
) {
  let ret = allSps
  if (!ret) {
    ret = []
  }
  for (let i = 0; i < arrOfSp.length; ++i) {
    const sp = arrOfSp[i]
    if (sp.instanceType === InstanceType.Shape) {
      ret.push(sp)
    } else if (sp.instanceType === InstanceType.GroupShape) {
      getAllShapes(sp.spTree, ret)
    }
  }
  return ret
}

export function createRotateControl(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      return new RotateSingleControl(sp)
    case InstanceType.ImageShape:
      return new RotateSingleControl(sp)
    case InstanceType.GroupShape:
      return new RotateGroupControl(sp)
    case InstanceType.GraphicFrame:
      return new RotateSingleControl(sp)
  }
}

export function createResizeControl(
  sp: SlideElement,
  absoluteDirection,
  handler: IEditorHandler,
  drawFunc?: DrawControlTrackFunc
) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      return new ResizeSingleControl(sp, absoluteDirection, handler, drawFunc)
    case InstanceType.ImageShape:
      return new ResizeSingleControl(sp, absoluteDirection, handler, drawFunc)
    case InstanceType.GroupShape:
      return new ResizeGroupControl(sp, absoluteDirection)
    case InstanceType.GraphicFrame:
      return new ResizeSingleControl(sp, absoluteDirection, handler)
  }
}

export function updateSelectionStateForSlideElement(
  editorUI: EditorUI,
  sp: SlideElement
) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      onUpdateSelectionState(editorUI, sp)
      break
    case InstanceType.GraphicFrame:
      onUpdateSelectionStateForGraphicFrame(editorUI, sp)
  }
}

function onUpdateSelectionState(editorUI: EditorUI, sp: Shape) {
  const controller = editorUI.renderingController
  const cursor = editorUI.textCursor
  const textDoc = getTextDocumentOfSp(sp)
  const textTransform = sp.textTransform
  if (textDoc) {
    let matrix: null | Matrix2D = null
    if (textTransform) {
      matrix = textTransform.clone()
    }
    cursor.updateTransform(matrix)
    if (true === textDoc.hasTextSelection()) {
      if (false === textDoc.hasNoSelection()) {
        cursor.hide()
        controller.setInTextSelection(true)
        controller.onUpdateEditLayer()
      } else {
        if (true !== textDoc.selection.start) {
          textDoc.removeSelection()
        }
        controller.setInTextSelection(false)
        getAndUpdateCursorPositionForTargetOrTextContent(textDoc)

        cursor.show()
      }
    } else {
      controller.setInTextSelection(false)
      getAndUpdateCursorPositionForTargetOrTextContent(textDoc)

      cursor.show()
    }
  } else {
    cursor.updateTransform(new Matrix2D())
    cursor.hide()
    controller.setInTextSelection(false)
  }
}

function onUpdateSelectionStateForGraphicFrame(
  editorUI: EditorUI,
  graphicFrame: GraphicFrame
) {
  const textDocument = editorUI.renderingController
  const cursor = editorUI.textCursor
  const graphicObject = graphicFrame.graphicObject
  if (textDocument) {
    if (graphicObject) {
      if (
        true === graphicObject.hasTextSelection() &&
        !graphicObject.hasNoSelection()
      ) {
        cursor.updateTransform(graphicFrame.transform)
        cursor.hide()
        textDocument.setInTextSelection(true)
        drawSelectionForTable(graphicObject)
        textDocument.onUpdateEditLayer()
      } else {
        textDocument.setInTextSelection(false)
        getAndUpdateCursorPositionForTargetOrTextContent(graphicObject)
        cursor.updateTransform(graphicFrame.transform)
        cursor.show()
      }
    } else {
      cursor.updateTransform(null)
      cursor.hide()
      textDocument.setInTextSelection(false)
    }
  }
}

// Todo, move
export function drawAdjustmentPoints(
  renderingController: RenderController,
  sp: SlideElement
) {
  if (!isSlideElement(sp)) {
    return
  }

  let geom: Geometry | undefined
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      if (sp.spPr?.geometry) {
        geom = sp.spPr.geometry
      }

      break
    }
    case InstanceType.ImageShape: {
      if (sp.calcedGeometry) {
        geom = sp.calcedGeometry
      }
      break
    }
  }

  if (geom == null) {
    return
  }

  const transform = sp.transform
  const xyAdjustments = geom.ahXYLst
  for (let i = 0, l = xyAdjustments.length; i < l; ++i) {
    renderingController.drawAdjustControl(
      transform,
      xyAdjustments[i].posX,
      xyAdjustments[i].posY
    )
  }

  const polarAdjustments = geom.ahPolarLst
  for (let i = 0, l = polarAdjustments.length; i < l; ++i) {
    renderingController.drawAdjustControl(
      transform,
      polarAdjustments[i].posX,
      polarAdjustments[i].posY
    )
  }
}
