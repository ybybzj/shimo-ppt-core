import { Nullable } from '../../../../liber/pervasive'
import { PPTSelectState } from '../../../core/Slide/SelectionState'
import { GraphicFrame } from '../../../core/SlideElement/GraphicFrame'
import { Shape } from '../../../core/SlideElement/Shape'
import { SlideElement } from '../../../core/SlideElement/type'
import { IEditorHandler } from './type'

export function getTargetTextSp(
  this: IEditorHandler
): Nullable<Shape | GraphicFrame> {
  return this.selectionState?.selectedTextSp
}
export function getTargetSelectedElements(
  this: IEditorHandler
): SlideElement[] {
  return this.selectionState?.selectedElements || []
}

export function getSelectedSlideElements(this: IEditorHandler) {
  return this.selectionState?.selectedSlideElements || []
}

export function setSelectionState(this: IEditorHandler, state: PPTSelectState) {
  this.reset()
  this.selectionState.setDocSelectInfo(state)
}
