import { PhButtonState } from '../../../core/common/const/attrs'
import { Matrix2D } from '../../../core/graphic/Matrix'
import { EditLayer } from '../../editorUI/editLayer'
import { RenderingRectInfo } from '../type'
import { PlaceholdersController } from './placeholdersController'

const SizeOfButton = 45
const SizeOfButtonImage = 35
const SizeOfButtonBetween = 8

type Point = { x: number; y: number }

export class Placeholder {
  placeholderControl: PlaceholdersController
  // 关联形状的 Id
  id: any
  // 按钮类型
  buttons: number[]
  // 对应按钮显示状态
  states: number[]
  anchor: {
    slideIndex: number
    rect: { x: number; y: number; w: number; h: number }
    transform: Matrix2D
  }
  constructor() {
    this.placeholderControl = null!
    // parent shape id
    this.id = null
    this.buttons = []
    this.states = []

    // position
    this.anchor = {
      slideIndex: -1,
      rect: { x: 0, y: 0, w: 0, h: 0 },
      transform: null!,
    }
  }

  getCenterPoint(
    curSlideRect: RenderingRectInfo,
    slideWidthInMM: number,
    slideHeightInMM: number
  ): Point {
    let cxMM = this.anchor.rect.x + this.anchor.rect.w / 2
    let cyMM = this.anchor.rect.y + this.anchor.rect.h / 2
    if (this.anchor.transform) {
      const tmpMMCx = cxMM
      const tmpMMCy = cyMM
      cxMM = this.anchor.transform.XFromPoint(tmpMMCx, tmpMMCy)
      cyMM = this.anchor.transform.YFromPoint(tmpMMCx, tmpMMCy)
    }

    const slideWidthInPx = curSlideRect.right - curSlideRect.left
    const slideHeightInPx = curSlideRect.bottom - curSlideRect.top

    return {
      x: Math.round(
        curSlideRect.left + (cxMM * slideWidthInPx) / slideWidthInMM
      ),
      y: Math.round(
        curSlideRect.top + (cyMM * slideHeightInPx) / slideHeightInMM
      ),
    }
  }

  // calculation of all button rects
  getCurButtonRects(pointOfCenter: Point, scale: Point): Array<Point> {
    // maximum 2 rows
    const count = this.buttons.length
    const numOfCols = count < 3 ? count : (this.buttons.length + 1) >> 1
    const countDiff = count - numOfCols

    const sizeTotalHor =
      numOfCols * SizeOfButton + (numOfCols - 1) * SizeOfButtonBetween
    const sizeTotalHorDiff =
      countDiff * SizeOfButton + (countDiff - 1) * SizeOfButtonBetween
    let sizeTotalVer = count > 0 ? SizeOfButton : 0
    if (count > numOfCols) {
      sizeTotalVer += SizeOfButton + SizeOfButtonBetween
    }

    const parentWidth = (this.anchor.rect.w * scale.x) >> 0
    const parentHeight = (this.anchor.rect.h * scale.y) >> 0

    if (
      sizeTotalHor + (SizeOfButtonBetween << 1) > parentWidth ||
      sizeTotalVer + (SizeOfButtonBetween << 1) > parentHeight
    ) {
      return []
    }

    const xStart = pointOfCenter.x - (sizeTotalHor >> 1)
    const yStart =
      pointOfCenter.y -
      ((count === numOfCols
        ? SizeOfButton
        : 2 * SizeOfButton + SizeOfButtonBetween) >>
        1)

    const ret: Array<Point> = []
    let x = xStart
    let y = yStart
    let i = 0
    while (i < numOfCols) {
      ret.push({ x, y })
      x += SizeOfButton + SizeOfButtonBetween
      i++
    }

    x = xStart + ((sizeTotalHor - sizeTotalHorDiff) >> 1)
    y = yStart + SizeOfButton + SizeOfButtonBetween
    while (i < count) {
      ret.push({ x: x, y: y })
      x += SizeOfButton + SizeOfButtonBetween
      i++
    }

    return ret
  }

  isInside(
    x: number,
    y: number,
    curSlideRect: RenderingRectInfo,
    slideWidthInMM: number,
    slideHeightInMM: number,
    menuPosition?
  ) {
    const pointOfCenter = this.getCenterPoint(
      curSlideRect,
      slideWidthInMM,
      slideHeightInMM
    )
    const scale = {
      x: (curSlideRect.right - curSlideRect.left) / slideWidthInMM,
      y: (curSlideRect.bottom - curSlideRect.top) / slideHeightInMM,
    }
    const rects = this.getCurButtonRects(pointOfCenter, scale)

    const px = Math.round(
      curSlideRect.left +
        (x * (curSlideRect.right - curSlideRect.left)) / slideWidthInMM
    )
    const py = Math.round(
      curSlideRect.top +
        (y * (curSlideRect.bottom - curSlideRect.top)) / slideHeightInMM
    )

    for (let i = 0; i < rects.length; i++) {
      const { x, y } = rects[i]
      if (
        px >= x &&
        px <= x + SizeOfButton &&
        py >= y &&
        py <= y + SizeOfButton
      ) {
        if (menuPosition) {
          menuPosition.x = x
          menuPosition.y = y
        }
        return i
      }
    }

    return -1
  }

  onMouseDown(
    x: number,
    y: number,
    curSlideRect: RenderingRectInfo,
    slideWidthInMM: number,
    slideHeightInMM: number
  ) {
    const menuPosition = { x: 0, y: 0 }
    const btnIndex = this.isInside(
      x,
      y,
      curSlideRect,
      slideWidthInMM,
      slideHeightInMM,
      menuPosition
    )

    if (-1 === btnIndex) return false

    if (this.states[btnIndex] === PhButtonState.Active) {
      this.states[btnIndex] = PhButtonState.Over
      this.placeholderControl.renderingController.onUpdateEditLayer()
      return true
    }
    this.placeholderControl.execCallback(this.buttons[btnIndex], this)
    return true
  }

  onMouseMove(
    x: number,
    y: number,
    curSlideRect: RenderingRectInfo,
    slideWidthInMM: number,
    slideHeightInMM: number,
    checker: { needUpdateEditLayer: boolean }
  ) {
    const btnIndex = this.isInside(
      x,
      y,
      curSlideRect,
      slideWidthInMM,
      slideHeightInMM
    )

    // maybe they didn't hit the button, but the state could change => the interface needs to be redrawn
    let isUpdate = false
    for (let i = 0; i < this.buttons.length; i++) {
      if (i === btnIndex) {
        if (this.states[i] === PhButtonState.None) {
          this.states[i] = PhButtonState.Over
          isUpdate = true
        }
      } else {
        if (this.states[i] === PhButtonState.Over) {
          this.states[i] = PhButtonState.None
          isUpdate = true
        }
      }
    }
    if (isUpdate) {
      checker.needUpdateEditLayer = true
    }

    return -1 !== btnIndex
  }

  onMouseUp(_x, _y, _curSlideRect, _slideWidthInMM, _slideHeightInMM) {
    //
  }

  draw(
    editLayer: EditLayer,
    curSlideRect: RenderingRectInfo,
    slideWidthInMM: number,
    slideHeightInMM: number
  ) {
    const pointOfCenter = this.getCenterPoint(
      curSlideRect,
      slideWidthInMM,
      slideHeightInMM
    )
    const scale = {
      x: (curSlideRect.right - curSlideRect.left) / slideWidthInMM,
      y: (curSlideRect.bottom - curSlideRect.top) / slideHeightInMM,
    }
    const rects = this.getCurButtonRects(pointOfCenter, scale)

    if (rects.length !== this.buttons.length) return

    const imgOffset = (SizeOfButton - SizeOfButtonImage) >> 1

    const ctx = editLayer.context
    for (let i = 0; i < this.buttons.length; i++) {
      editLayer.update(rects[i].x, rects[i].y)
      editLayer.update(rects[i].x + SizeOfButton, rects[i].y + SizeOfButton)

      const buttonIcons = this.placeholderControl.icons[this.buttons[i]]

      const img =
        this.states[i] === PhButtonState.Active
          ? buttonIcons[0]
          : buttonIcons[1]
      if (img) {
        const orgGlobalAlpha = ctx.globalAlpha
        ctx.globalAlpha = this.states[i] === PhButtonState.None ? 0.5 : 1

        ctx.beginPath()
        ctx.drawImage(
          img,
          rects[i].x + imgOffset,
          rects[i].y + imgOffset,
          SizeOfButtonImage,
          SizeOfButtonImage
        )

        ctx.globalAlpha = orgGlobalAlpha
      }
    }
  }
}
