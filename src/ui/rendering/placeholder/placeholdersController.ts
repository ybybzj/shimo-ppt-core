import { PhButtonType } from '../../../core/common/const/attrs'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { EditorKit } from '../../../editor/global'
import {
  PlaceholderIcon_Image,
  PlaceholderIcon_Image_Active,
} from '../../../images/images'
import { EditLayer } from '../../editorUI/editLayer'
import { RenderController } from '../RenderController'
import { RenderingRectInfo } from '../type'
import { Placeholder } from './placeholder'

export class PlaceholdersController {
  renderingController: RenderController
  placeholders: Placeholder[] = []
  icons: Record<string, HTMLImageElement[]> = {}
  constructor(controller: RenderController) {
    this.renderingController = controller
    this.registerIcons()
  }

  registerIcons() {
    this.icons[PhButtonType.Image] = [
      PlaceholderIcon_Image,
      PlaceholderIcon_Image_Active,
    ]
  }

  execCallback(type: number, placeholder: Placeholder) {
    switch (type) {
      case PhButtonType.Image:
        EditorKit.onInsertPlaceholderImage(placeholder.id)
        break
    }
  }

  draw(
    editLayer: EditLayer,
    slideIndex: number,
    curSlideRect: RenderingRectInfo,
    slideWidthInMM: number,
    slideHeightInMM: number
  ) {
    for (let i = 0; i < this.placeholders.length; i++) {
      if (this.placeholders[i].anchor.slideIndex !== slideIndex) {
        continue
      }

      this.placeholders[i].draw(
        editLayer,
        curSlideRect,
        slideWidthInMM,
        slideHeightInMM
      )
    }
  }

  onMouseDown(
    pos: { x: number; y: number },
    curSlideRect: RenderingRectInfo,
    slideWidthInMM: number,
    slideHeightInMM: number
  ) {
    const curSlideIndex = this.renderingController.curSlideIndex
    if (EditorSettings.isViewMode) return false
    for (let i = 0; i < this.placeholders.length; i++) {
      if (this.placeholders[i].anchor.slideIndex !== curSlideIndex) continue

      if (
        this.placeholders[i].onMouseDown(
          pos.x,
          pos.y,
          curSlideRect,
          slideWidthInMM,
          slideHeightInMM
        )
      ) {
        return true
      }
    }
    return false
  }

  onMouseMove(
    pos: { x: number; y: number },
    curSlideRect: RenderingRectInfo,
    slideWidthInMM: number,
    slideHeightInMM: number
  ) {
    const checker = { needUpdateEditLayer: false }
    let isButton = false
    const curSlideIndex = this.renderingController.curSlideIndex
    for (let i = 0; i < this.placeholders.length; i++) {
      if (this.placeholders[i].anchor.slideIndex !== curSlideIndex) continue

      const val = EditorSettings.isViewMode
        ? null
        : this.placeholders[i].onMouseMove(
            pos.x,
            pos.y,
            curSlideRect,
            slideWidthInMM,
            slideHeightInMM,
            checker
          )

      if (val) {
        isButton = true
      }
    }

    if (isButton) {
      this.renderingController.editorUI.mouseCursor.setCursorType('pointer')
    }

    if (checker.needUpdateEditLayer && this.renderingController) {
      this.renderingController.onUpdateEditLayer()
    }

    return isButton
  }

  onMouseUp(
    pos: { x: number; y: number },
    curSlideRect: RenderingRectInfo,
    slideWidthInMM: number,
    slideHeightInMM: number
  ) {
    return this.onMouseMove(pos, curSlideRect, slideWidthInMM, slideHeightInMM)
  }

  update(objects: Placeholder[]) {
    const count = this.placeholders.length
    const newCount = objects ? objects.length : 0
    if (count !== newCount) return this.onUpdate(objects)

    let t1, t2
    for (let i = 0; i < count; i++) {
      const placeholder = this.placeholders[i]
      if (placeholder.id !== objects[i].id) {
        return this.onUpdate(objects)
      }

      if (placeholder.anchor.slideIndex !== objects[i].anchor.slideIndex) {
        return this.onUpdate(objects)
      }

      t1 = placeholder.anchor.rect
      t2 = objects[i].anchor.rect

      if (
        Math.abs(t1.x - t2.x) > 0.001 ||
        Math.abs(t1.y - t2.y) > 0.001 ||
        Math.abs(t1.w - t2.w) > 0.001 ||
        Math.abs(t1.h - t2.h) > 0.001
      ) {
        return this.onUpdate(objects)
      }

      t1 = placeholder.anchor.transform
      t2 = objects[i].anchor.transform

      if (!t1 && !t2) continue

      if ((t1 && !t2) || (!t1 && t2)) return this.onUpdate(objects)

      if (
        Math.abs(t1.sx - t2.sx) > 0.001 ||
        Math.abs(t1.sy - t2.sy) > 0.001 ||
        Math.abs(t1.shx - t2.shx) > 0.001 ||
        Math.abs(t1.shy - t2.shy) > 0.001 ||
        Math.abs(t1.tx - t2.tx) > 0.001 ||
        Math.abs(t1.ty - t2.ty) > 0.001
      ) {
        return this.onUpdate(objects)
      }
    }
  }

  private onUpdate(objects: Placeholder[]) {
    this.placeholders = objects ? objects : []
    this.placeholders.forEach((placeholder) => {
      placeholder.placeholderControl = this
    })
    this.renderingController.onUpdateEditLayer()
  }
}
