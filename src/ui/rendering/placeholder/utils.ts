import { PhButtonState, PhButtonType } from '../../../core/common/const/attrs'
import { isSlide } from '../../../core/common/utils'
import { Slide } from '../../../core/Slide/Slide'
import { PlaceholderType } from '../../../core/SlideElement/const'
import { isPlaceholderWithEmptyContent } from '../../../core/utilities/shape/asserts'
import { getPlaceholderType } from '../../../core/utilities/shape/getters'
import { SlideElement } from '../../../core/SlideElement/type'
import { Placeholder } from './placeholder'

export function getPlaceholdersControlsInSlide(slide: Slide): Placeholder[] {
  const ret: Placeholder[] = []
  const { spTree } = slide.cSld
  for (let i = 0; i < spTree.length; ++i) {
    const sp = spTree[i]
    if (isPlaceholderWithEmptyContent(sp)) {
      const placeholder = createPlaceholderControlByType(sp)
      if (placeholder.buttons.length > 0) {
        ret.push(placeholder)
      }
    }
  }
  return ret
}

export function createPlaceholderControlByType(
  shape: SlideElement
): Placeholder {
  const phType = getPlaceholderType(shape)
  let buttons: number[] = []
  switch (phType) {
    case null: {
      buttons.push(PhButtonType.Table)
      buttons.push(PhButtonType.Chart)
      buttons.push(PhButtonType.Image)
      buttons.push(PhButtonType.ImageUrl)
      break
    }
    case PlaceholderType.body: {
      break
    }
    case PlaceholderType.chart: {
      buttons.push(PhButtonType.Chart)
      break
    }
    case PlaceholderType.clipArt: {
      buttons.push(PhButtonType.Image)
      buttons.push(PhButtonType.ImageUrl)
      break
    }
    case PlaceholderType.ctrTitle: {
      break
    }
    case PlaceholderType.dgm: {
      break
    }
    case PlaceholderType.dt: {
      break
    }
    case PlaceholderType.ftr: {
      break
    }
    case PlaceholderType.hdr: {
      break
    }
    case PlaceholderType.media: {
      break
    }
    case PlaceholderType.obj: {
      buttons.push(PhButtonType.Table)
      buttons.push(PhButtonType.Chart)
      buttons.push(PhButtonType.Image)
      buttons.push(PhButtonType.ImageUrl)
      break
    }
    case PlaceholderType.pic: {
      buttons.push(PhButtonType.Image)
      buttons.push(PhButtonType.ImageUrl)
      break
    }
    case PlaceholderType.sldImg: {
      buttons.push(PhButtonType.Image)
      buttons.push(PhButtonType.ImageUrl)
      break
    }
    case PlaceholderType.sldNum: {
      break
    }
    case PlaceholderType.subTitle: {
      break
    }
    case PlaceholderType.tbl: {
      buttons.push(PhButtonType.Table)
      break
    }
    case PlaceholderType.title: {
      break
    }
  }
  // FIXME: 只启用图片类型
  buttons = buttons.filter((type) => type === PhButtonType.Image)

  let slideIndex = 0
  if (isSlide(shape.parent)) {
    slideIndex = shape.parent.index!
  }
  return createPlaceholderSp(
    shape.id,
    buttons,
    slideIndex,
    { x: 0, y: 0, w: shape.extX, h: shape.extY },
    shape.transform
  )
}

function createPlaceholderSp(
  id: string,
  buttons: number[],
  slideIndex: number,
  rect,
  transform
): Placeholder {
  const placeholder = new Placeholder()
  placeholder.id = id
  placeholder.buttons = buttons
  placeholder.anchor.slideIndex = slideIndex
  placeholder.anchor.rect = rect
  placeholder.anchor.transform = transform

  for (let i = 0; i < placeholder.buttons.length; i++) {
    placeholder.states[i] = PhButtonState.None
  }

  return placeholder
}
