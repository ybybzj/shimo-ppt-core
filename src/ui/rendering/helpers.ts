import { getTextDocumentOfSp } from '../../core/utilities/shape/getters'
import { InstanceType } from '../../core/instanceTypes'
import { SlideElement } from '../../core/SlideElement/type'

export function getHyperlinkFromSp(sp: SlideElement, x, y) {
  const textDoc = getTextDocumentOfSp(sp, x, y)
  const invertTextTransform =
    (sp.instanceType === InstanceType.Shape ||
      sp.instanceType === InstanceType.GraphicFrame) &&
    sp.invertTextTransform

  if (textDoc && invertTextTransform) {
    const tx = invertTextTransform.XFromPoint(x, y)
    const ty = invertTextTransform.YFromPoint(x, y)
    const paraIndex = textDoc.getElementIndexByXY(tx, ty)
    const para = textDoc.children.at(paraIndex)
    if (para) {
      if (para.isHitInText(tx, ty, 0)) {
        const hyperlink = para.getHyperlinkByXY(tx, ty, 0)
        if (hyperlink) {
          return hyperlink
        }
      }
    }
  }
  return null
}
