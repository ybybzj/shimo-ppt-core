import { EditorUI } from '../editorUI/EditorUI'
import { Matrix2D } from '../../core/graphic/Matrix'
import { EditorSettings } from '../../core/common/EditorSettings'
import { getTrackRectPosition, EditLayerDrawer } from './Drawer/EditLayerDrawer'
import { EditLayer } from '../editorUI/editLayer'
import { endExec } from '../../lib/utils/fn'
import { RenderingRectInfo } from './type'
import { InstanceType } from '../../core/instanceTypes'
import { PlaceholdersController } from './placeholder/placeholdersController'
import { isPlayingAnimation } from '../transitions/utils'
import { Dict, Nullable } from '../../../liber/pervasive'
import { SlideElement } from '../../core/SlideElement/type'
import {
  isGroup,
  isLineShape,
  isSlideElementInPPT,
} from '../../core/utilities/shape/asserts'
import { Factor_mm_to_pix } from '../../core/common/const/unit'
import {
  CollaborationShapeState,
  CollborationUserInfo,
} from '../../exports/view'
import { getPlaceholdersControlsInSlide } from './placeholder/utils'
import {
  getCurStateHoverInfo,
  needDrawControls,
  needDrawHoverEffect,
  needDrawSelectEffect,
} from '../states/utils'
import { TableCell } from '../../core/Table/TableCell'
import { each } from '../../../liber/l/each'
import {
  getCellBoundsByIndex,
  getSpStrokeWidth,
  getTopMostGroupShape,
} from '../../core/utilities/shape/getters'
import { drawControlsOnEditLayer } from '../features/common/utils'
import { EditControlType } from '../../core/common/const/ui'
import { ManagedSliceUtil } from '../../common/managedArray'
import { drawSelectionForTableCells } from '../../core/utilities/selection/draw'
import { calcGroupShapeRenderingRect } from '../../core/utilities/shape/group'

/** 协作 UI 单用户单次激活的有效时间 */
const COUI_ValidityDuration = 60000 //ms
export const COUI_LineWidth = 2 // px

export class RenderController {
  editorUI: EditorUI
  isEmptySlides: boolean = false
  curSlideRect: RenderingRectInfo = {
    left: 0,
    top: 0,
    right: 0,
    bottom: 0,
    base64Img: null,
  }
  editLayerDrawer: EditLayerDrawer
  editLayer!: EditLayer
  private placeholderControl: PlaceholdersController
  private needRerender = false
  private hoveringSp: Nullable<SlideElement> = null
  private hoveringTableCell: Nullable<TableCell> = null
  private isInTextSelection: boolean = false
  /** slideRefId: {} */
  private collaborationStatus: Dict<CollaborationShapeState> = {}
  private slideIndexForUpdatePlaceholder: Nullable<number> = null

  get presentation() {
    return this.editorUI.editorKit.presentation
  }

  get countOfSlides() {
    const presentation = this.presentation
    return presentation ? presentation.slides.length : 0
  }

  get curSlideIndex() {
    const presentation = this.presentation
    return presentation ? presentation.currentSlideIndex : -1
  }

  constructor(editorUI: EditorUI) {
    this.editorUI = editorUI

    this.editLayerDrawer = new EditLayerDrawer()
    this.placeholderControl = new PlaceholdersController(this)
  }

  onEditorUIInit() {
    this.editLayer = new EditLayer(this.editorUI.editLayer, this.editorUI)
    this.editLayerDrawer.setLayer(this.editLayer)
  }

  onUpdatePlaceholder(slideIndex: number) {
    this.slideIndexForUpdatePlaceholder = slideIndex
  }

  checkRender() {
    if (this.slideIndexForUpdatePlaceholder != null) {
      if (this.placeholderControl) {
        const slideIndex = this.slideIndexForUpdatePlaceholder
        const slide = this.presentation.slides[slideIndex]
        if (slide) {
          const placeholders = getPlaceholdersControlsInSlide(slide)
          this.placeholderControl.update(placeholders)
        }
      }
      this.slideIndexForUpdatePlaceholder = null
    }
    if (this.needRerender === true) {
      this.rerenderEditLayer()
    }

    this.editorUI.textCursor.checkRender()
  }

  rerenderEditLayer() {
    const editorUI = this.editorUI
    const presentation = editorUI.presentation
    const curIndex = this.curSlideIndex

    let notesEditLayer: EditLayer
    let isFocusNotes = false
    // 播放动画时不更新(清空)
    if (isPlayingAnimation(editorUI)) {
      return
    }
    if (EditorSettings.isNotesEnabled && editorUI.notesView) {
      notesEditLayer = editorUI.notesView.editLayer
      notesEditLayer.resetTransform()
      notesEditLayer.clear()

      if (presentation.isNotesFocused) {
        isFocusNotes = true
      }
    }

    const editLayer = this.editLayer
    editLayer.resetTransform()
    editLayer.clear()

    const ctx = editLayer.context

    if (this.isInTextSelection) {
      if (!isFocusNotes) {
        this.drawContainerTextSelection('slide')
      } else if (EditorSettings.isNotesEnabled) {
        this.drawContainerTextSelection('note')
      }
    } else if (presentation.selectionState.selectedTable != null) {
      drawSelectionForTableCells(presentation.selectionState.selectedTable)
    } else {
      ctx.globalAlpha = 1.0
    }

    if (presentation != null && curIndex >= 0 && !isFocusNotes) {
      const handler = this.editorUI.editorHandler
      const curSlide = presentation.slides[curIndex]
      // 根据当前状态判断是否需要绘制各类 adornment
      if (curSlide && needDrawSelectEffect(handler)) {
        handler.drawSelection(this)
      }
      if (needDrawHoverEffect(handler)) {
        this.drawHoveringEffect()
      }
      if (needDrawControls(handler) && -1 !== curIndex) {
        const editLayerDrawer = this.editLayerDrawer
        const { left, top, right, bottom } = this.curSlideRect
        const { width, height } = presentation
        editLayerDrawer.init(editLayer, left, top, right, bottom, width, height)
        drawControlsOnEditLayer(handler, editLayerDrawer)
        editLayerDrawer.resetLayerBounds()
        editLayer.resetTransform()
      }
    }

    if (this.placeholderControl.placeholders.length > 0 && curIndex >= 0) {
      this.placeholderControl.draw(
        editLayer,
        this.curSlideIndex,
        this.curSlideRect,
        presentation.width,
        presentation.height
      )
    }

    this.editorUI.guidelinesManager.draw()
    this.needRerender = false
  }

  onUpdateEditLayer = endExec(() => {
    this.needRerender = true
  })

  drawContainerTextSelection(container: 'slide' | 'note') {
    const editorUI = this.editorUI
    const { presentation, editorHandler: handler } = editorUI
    const curIndex = this.curSlideIndex

    if (curIndex === -1) return
    if (container === 'slide') {
      handler.drawSelectionForText()
    } else {
      presentation.slides[curIndex].drawNotesSelection()
    }
  }

  setInTextSelection(isInTextSelection: boolean) {
    this.isInTextSelection = isInTextSelection
    if (false === this.isInTextSelection) {
      this.editorUI.textCursor.onClearSelection()
      this.onUpdateEditLayer()
      this.editLayer.context.globalAlpha = 1.0
    }
  }

  getInTextSelection() {
    return this.isInTextSelection
  }

  drawHoveringEffect() {
    const sp = this.hoveringSp
    const { presentation, editorHandler: handler } = this.editorUI
    const curSlide = presentation.slides[presentation.currentSlideIndex]
    const selectedSps = handler.getSelectedSlideElements()
    if (
      !sp ||
      !isSlideElementInPPT(sp) ||
      !curSlide ||
      selectedSps.includes(sp)
    ) {
      return
    }
    const status = this.collaborationStatus
    const slideRefs = Object.keys(status)
    const slideRef = slideRefs.find((ref) => curSlide.getRefId() === ref)

    // 优先绘制协作悬浮框
    if (slideRef) {
      const shapeRefsDict = status[slideRef]
      const shapeRefs = Object.keys(shapeRefsDict)
      const shapeRef = shapeRefs.find((ref) => ref === sp.getRefId())
      if (shapeRef) {
        const shapeState = shapeRefsDict[shapeRef]
        const users = checkCollaborationUsers(shapeState.users)
        if (users.length) {
          this.drawCollborationHoverEffect(sp, users)
          return
        }
      }
    }

    let x = 0
    let y = 0
    let extX = sp.extX
    let extY = sp.extY
    if (isGroup(sp)) {
      const rect = calcGroupShapeRenderingRect(0, 0, extX, extY)
      x = rect.x
      y = rect.y
      extX = rect.extX
      extY = rect.extY
    }

    this.drawControl(
      EditControlType.HoveringOutline,
      sp.getTransform(),
      x,
      y,
      extX,
      extY,
      isLineShape(sp),
      false
    )
  }

  /** @param users 按最近时间降序排列 */
  drawCollborationHoverEffect(sp: SlideElement, users: CollborationUserInfo[]) {
    if (users.length < 1) return
    const matrix = sp.getTransform().clone()
    const { extX, extY } = sp
    const textColor = '#FFFFFF'
    const MAXWIDTH = 80
    const Margin = 4
    const LineHeight = 5 * Factor_mm_to_pix
    const editLayer = this.editLayerDrawer.editLayer
    const slideRenderingInfo = this.editorUI.getSlideRenderingInfo()
    const ctx = editLayer.context
    let textMaxLength = 0

    ctx.beginPath()
    ctx.save()
    ctx.globalAlpha = 1
    ctx.font = `12px PingFang SC`

    // draw sp co-users
    const isRTL = EditorSettings.isRTL
    const borderOffset = getSpStrokeWidth(sp) / 2
    const { pos } = getTrackRectPosition(
      matrix,
      0 - borderOffset,
      0 - borderOffset,
      extX + 2 * borderOffset,
      extY + 2 * borderOffset,
      slideRenderingInfo
    )
    let [startX, startY] = isRTL ? [pos.x1, pos.y1] : [pos.x2, pos.y2]
    const borderOffsetX = (isRTL ? -1 : 1) * (sp.flipH ? -1 : 1)
    const borderOffsetY = sp.flipV ? 1 : -1
    ;[startX, startY] = [startX + borderOffsetX, startY + borderOffsetY]

    for (let i = 0; i < users.length; i++) {
      const name = users[i].name
      let textWidth = ctx.measureText(name).width + Margin * 2
      let j = 2
      while (MAXWIDTH < textWidth) {
        users[i].name = name.slice(0, name.length - j) + '...'
        textWidth = ctx.measureText(users[i].name).width + Margin * 2
        j++
      }
      textMaxLength = Math.max(textMaxLength, textWidth)
    }

    const drawFunc = (
      startX: number,
      startY: number,
      users: CollborationUserInfo[],
      isRTL = false
    ) => {
      const lineNum = users.length
      if (isRTL) {
        startX -= textMaxLength
      }
      const height = lineNum * LineHeight + Margin
      editLayer.updateByRect(startX, startY, textMaxLength, height)
      ctx.fillStyle = users[0].color
      ctx.fillRect(startX, startY, textMaxLength, height)
      ctx.textBaseline = 'middle'
      ctx.fillStyle = textColor
      ctx.textAlign = 'left'
      for (let i = 0; i < lineNum; i++) {
        ctx.fillText(
          users[i].name,
          startX + Margin,
          startY + Margin / 2 + ((2 * i + 1) * LineHeight) / 2
        )
      }
    }
    const spHoverListBottom = startY + users.length * (LineHeight + Margin)

    // draw TableCell co-users
    const isTable = sp.instanceType === InstanceType.GraphicFrame
    const curCell = this.hoveringTableCell
    if (isTable && curCell && curCell.parent) {
      const curRow = curCell.parent
      const cellUsers: CollborationUserInfo[] = []

      each((user) => {
        if (user.tableInfo) {
          const { rowIndex, cellIndex } = user.tableInfo
          if (rowIndex === curRow.index && cellIndex === curCell.index) {
            cellUsers.push(user)
          }
        }
      }, users)

      if (cellUsers.length > 0) {
        const [rowIndex, cellIndex] = [curRow.index, curCell.index]
        const bounds = getCellBoundsByIndex(sp, rowIndex, cellIndex)
        if (!bounds) return
        const { w, h, l, t } = bounds
        ;[matrix.tx, matrix.ty] = [l, t]
        const { pos } = getTrackRectPosition(
          matrix,
          0,
          0,
          w,
          h,
          slideRenderingInfo
        )
        const offset = COUI_LineWidth / 2
        const [cellStartX, cellStartY] = isRTL
          ? [pos.x1 - offset, pos.y1 - offset]
          : [pos.x2 + offset, pos.y2 - offset]
        drawFunc(cellStartX, cellStartY, cellUsers, isRTL)
        // 检查 cell 协作者列表是否会被表格所覆盖
        if (cellStartY < spHoverListBottom) {
          ctx.restore()
          return
        }
      }
    }
    const spUsers = users.filter((user) => user.tableInfo == null)
    if (spUsers.length > 0) drawFunc(startX, startY, spUsers, isRTL)
    ctx.restore()
  }

  getCollborationStatus() {
    return this.collaborationStatus
  }

  updateCollaborationStatus(status: Dict<CollaborationShapeState>) {
    this.collaborationStatus = status
  }

  drawControl(
    type: (typeof EditControlType)[keyof typeof EditControlType],
    matrix: Matrix2D,
    left: number,
    top: number,
    width: number,
    height: number,
    isLine: boolean,
    canRotate: boolean,
    strokeStyle?: string
  ) {
    this.editLayerDrawer.drawControl({
      type,
      matrix,
      left,
      top,
      width,
      height,
      isLine,
      canRotate,
      strokeStyle,
    })
  }

  drawAdjustControl(matrix, x, y) {
    this.editLayerDrawer.drawAdjustControl(matrix, x, y)
  }

  checkPlaceHolder(
    pos: { x: number; y: number },
    op: 'mouseDown' | 'mouseMove' | 'mouseUp'
  ) {
    let handled = false
    const { placeholderControl, curSlideRect, presentation } = this
    const { width, height } = presentation
    switch (op) {
      case 'mouseDown':
        if (placeholderControl.onMouseDown(pos, curSlideRect, width, height)) {
          handled = true
        }
        break
      case 'mouseMove':
        if (placeholderControl.onMouseMove(pos, curSlideRect, width, height)) {
          handled = true
        }
        break
      case 'mouseUp':
        if (placeholderControl.onMouseUp(pos, curSlideRect, width, height)) {
          handled = true
        }
        break
    }
    if (handled) {
      this.onUpdateEditLayer()
    }
    return handled
  }

  checkHoverEffect(pos: { x; y }) {
    const info = this.editorUI.editorKit.checkSlideShapeHitByPosition(
      pos.x,
      pos.y
    )
    const sp = getTopMostGroupShape(info?.sp) ?? info?.sp
    const needRepaint = this.checkPlaceHolder(pos, 'mouseMove')

    if (sp !== this.hoveringSp) {
      this.onUpdateEditLayer()
    }
    this.hoveringSp = sp

    let hoveringTableCell: Nullable<TableCell>
    if (sp && sp.instanceType === InstanceType.GraphicFrame) {
      const handler = this.editorUI.editorHandler
      const hoverInfo = getCurStateHoverInfo(handler, pos.x, pos.y)
      if (hoverInfo) {
        const rows = sp.graphicObject!.children
        ManagedSliceUtil.forEach(rows, (row) => {
          ManagedSliceUtil.forEach(row.children, (cell) => {
            if (cell.getId() === hoverInfo.objectId) {
              hoveringTableCell = cell
              return
            }
          })
        })
      }
    }
    if (this.hoveringTableCell !== hoveringTableCell) {
      this.onUpdateEditLayer()
      this.hoveringTableCell = hoveringTableCell
    }

    return needRepaint
  }

  clearAdornment() {
    this.hoveringSp = null
  }
}

/** 将协作人信息按有效期过滤，按最新活跃排序 */
export function checkCollaborationUsers(users: CollborationUserInfo[]) {
  const curTime = Date.now()
  return users
    .filter((user) => curTime - user.lastActiveTime < COUI_ValidityDuration)
    .sort((user, _user) => _user.lastActiveTime - user.lastActiveTime)
}
