import { BrowserInfo } from '../common/browserInfo'
import { gClipboard } from '../copyPaste/gClipboard'
import { stopEvent, GlobalEvents } from '../common/dom/events'
import { EditorUtil } from '../globals/editor'
import { IEditorKit } from '../editor/type'
import { Dom, toElement } from '../common/dom/vdom'
import { EditorKit } from '../editor/global'
import { EditorSettings } from '../core/common/EditorSettings'
import { eachOfUnicodeString } from '../common/utils'
import { DOM_ID, DOM_STYLE } from './editorUI/controls/const'
import { keyCodeMap } from './editorUI/utils/keys'

let spaceDownEvt: undefined | KeyboardEvent
function isSpaceKeyInLinux(e: KeyboardEvent) {
  return BrowserInfo.linux && BrowserInfo.chrome && e.keyCode === 32
}

export class TextInputManager {
  editor: IEditorKit
  containerElm!: HTMLDivElement
  inputAreaElm: HTMLDivElement | HTMLTextAreaElement | null = null
  editorElement: HTMLDivElement
  focusElement: HTMLDivElement | HTMLTextAreaElement | null = null
  lockFocusElement = false
  enableKeyEvents = false
  containerOffX = 0
  containerOffY = 0
  text: string
  compositionStart: number
  compositionEnd: number
  isInComposition = false
  isInApiComposition: boolean
  /** 相对于 EditorPage y===0 处 */
  notesAreaOffY: number
  editorContainerWidth: number
  editorContainerHeight: number
  recentTextsForReplace: string[] = []
  islastReplace = false
  isKeyPressDisabled = false
  inputAreaElmVisibility = true

  private _inputBeforeComposition: string
  private editable: boolean = true
  private inputAreaOffset = 50
  private inputAreaWidth = 200

  constructor(editorKit) {
    this.editor = editorKit
    this.editorElement = this.editor.editorUI.editorElement // core-container
    if (BrowserInfo.isTablet) {
      this.editorElement.tabIndex = 1
    }

    // current text info
    this._inputBeforeComposition = ''
    this.text = ''
    this.compositionStart = 0
    this.compositionEnd = 0
    this.isInApiComposition = false
    this.notesAreaOffY = 0

    // editor_sdk div sizes (for visible textarea)
    this.editorContainerWidth = 0
    this.editorContainerHeight = 0
  }

  init(parentId) {
    const parentDom = document.getElementById(parentId)!

    this.containerElm = toElement(
      Dom.div(`#${DOM_ID.TextInputWrap.InputContainer.Id}`, {
        style: DOM_STYLE.TextInputWrap.InputContainer.Style,
      })
    )

    if (BrowserInfo.chrome) this.containerElm.style.position = 'fixed'
    else this.containerElm.style.position = 'absolute'

    if (this.editable) {
      this.inputAreaElm = document.createElement('textarea')
    } else {
      this.inputAreaElm = document.createElement('div')
      this.inputAreaElm.setAttribute('contentEditable', 'true')
    }
    this.inputAreaElm.id = DOM_ID.TextInputWrap.InputContainer.InputArea

    let style =
      'left:-' +
      (this.inputAreaWidth >> 1) +
      'px;top:' +
      -this.inputAreaOffset +
      'px;'
    style +=
      'background:transparent;border:none;position:absolute;text-shadow:0 0 0 #000;outline:none;color:transparent;width:' +
      this.inputAreaWidth +
      'px;height:50px;'
    style +=
      'overflow:hidden;padding:0px;margin:0px;font-family:arial;font-size:10pt;resize:none;font-weight:normal;box-sizing:content-box;-moz-box-sizing:content-box;-webkit-box-sizing:content-box;touch-action:none;'
    this.inputAreaElm.setAttribute('style', style)
    this.inputAreaElm.setAttribute('spellcheck', 'false')

    this.inputAreaElm.setAttribute('autocapitalize', 'none')
    this.inputAreaElm.setAttribute('autocomplete', 'off')
    this.inputAreaElm.setAttribute('autocorrect', 'off')
    //
    this.containerElm.appendChild(this.inputAreaElm)

    const parentStyle = getComputedStyle(parentDom)
    // need another parent. so that it scrolls, not parentdom
    const textInputWrapElem = toElement(
      Dom.div(`#${DOM_ID.TextInputWrap.Id}`, {
        style: {
          ['background']: 'transparent',
          ['border']: 'none',
          ['position']: 'absolute',
          ['padding']: '0px',
          ['margin']: '0px',
          ['z-index']: '0',
          ['pointer-events']: 'none',
          ['overflow']: 'hidden',
          ['left']: parentStyle.left,
          ['top']: parentStyle.top,
          ['width']: parentStyle.width,
          ['height']: parentStyle.height,
        },
      })
    )

    textInputWrapElem.appendChild(this.containerElm)
    parentDom.parentNode!.appendChild(textInputWrapElem)

    // events:
    this.inputAreaElm.onkeydown = (e) => {
      if (BrowserInfo.isSafariInMacOs || BrowserInfo.weChat) {
        const cmdKey = e.ctrlKey || e.metaKey ? true : false
        const copyPasteCode =
          e.keyCode === 67 || e.keyCode === 88 || e.keyCode === 86
        if (cmdKey && copyPasteCode) this.isKeyPressDisabled = true
        else this.isKeyPressDisabled = false
      }

      if (isSpaceKeyInLinux(e)) {
        spaceDownEvt = e
        return
      }
      const result = this.onKeyDown(e)
      // 部分浏览器不支持 return false
      if (result === false) {
        stopEvent(e)
      }
      return result
    }
    if (BrowserInfo.isTablet) {
      this.editorElement.onkeydown = (e) => {
        if (document.activeElement !== this.inputAreaElm) {
          this.inputAreaElm?.onkeydown!(e)
        }
      }
    }
    this.inputAreaElm.onkeypress = (e) => {
      if (this.isKeyPressDisabled === true) {
        // macOS Sierra send keypress before copy event
        this.isKeyPressDisabled = false
        const cmdKey = e.ctrlKey || e.metaKey ? true : false
        if (cmdKey) return
      }

      if (spaceDownEvt != null) {
        if (spaceDownEvt.keyCode === e.keyCode) {
          let ret = this.onKeyDown(spaceDownEvt)
          if (ret !== false) {
            ret = this.onKeyPress(e)
          }
          spaceDownEvt = undefined
          return ret
        }
        spaceDownEvt = undefined

        return
      }
      const result = this.onKeyPress(e)
      if (result === false) {
        stopEvent(e)
      }
      return result
    }
    if (BrowserInfo.isTablet) {
      this.editorElement.onkeypress = (e) => {
        if (document.activeElement !== this.inputAreaElm) {
          this.inputAreaElm?.onkeypress!(e)
        }
      }
    }
    this.inputAreaElm.onkeyup = (e) => {
      this.isKeyPressDisabled = false
      return this.onKeyUp(e)
    }
    if (BrowserInfo.isTablet) {
      this.editorElement.onkeyup = (e) => {
        if (document.activeElement !== this.inputAreaElm) {
          this.inputAreaElm?.onkeyup!(e)
        }
      }
    }
    this.inputAreaElm.addEventListener(
      'input',
      (e) => {
        return this.onInput(e)
      },
      false
    )

    this.inputAreaElm.addEventListener(
      'compositionstart',
      (_e) => {
        this.isInComposition = true
      },
      false
    )
    this.inputAreaElm.addEventListener(
      'compositionupdate',
      (e) => {
        this.isInComposition = true
        this.onInput(e)
      },
      false
    )
    this.inputAreaElm.addEventListener(
      'compositionend',
      (e) => {
        this.isInComposition = false
        this.onInput(e)
      },
      false
    )
    this.editor.updateInputPos()
  }

  onResize(mainContainerId) {
    const inputElm = document.getElementById(DOM_ID.TextInputWrap.Id)!
    const mainContainer = document.getElementById(mainContainerId)
    if (!mainContainer) return

    if (BrowserInfo.chrome) {
      const domRect = mainContainer.getBoundingClientRect()
      this.containerOffX = domRect.left
      this.containerOffY = domRect.top
    }

    const width = mainContainer.style.width
    if ((null == width || '' === width) && window.getComputedStyle) {
      const s = window.getComputedStyle(mainContainer)
      inputElm.style.left = s.left
      inputElm.style.top = s.top
      inputElm.style.width = s.width
      inputElm.style.height = s.height
    } else {
      inputElm.style.left = mainContainer.style.left
      inputElm.style.top = mainContainer.style.top
      inputElm.style.width = width
      inputElm.style.height = mainContainer.style.height
    }

    if (EditorSettings.isMobile) {
      const inputContainer = document.getElementById(
        DOM_ID.TextInputWrap.InputContainer.Id
      )!
      const inputArea = document.getElementById(
        DOM_ID.TextInputWrap.InputContainer.InputArea
      )!
      ;(inputContainer.parentNode as HTMLElement).style.pointerEvents = ''

      inputContainer.style.left = '-100px'
      inputContainer.style.top = '-100px'
      inputContainer.style.right = '-100px'
      inputContainer.style.bottom = '-100px'
      inputContainer.style.width = 'auto'
      inputContainer.style.height = 'auto'

      inputArea.style.left = '0px'
      inputArea.style.top = '0px'
      inputArea.style.right = '0px'
      inputArea.style.bottom = '0px'
      inputArea.style.width = '100%'
      inputArea.style.height = '100%'

      if (BrowserInfo.ie) {
        document.body.style['msTouchAction'] = 'none'
        document.body.style['touchAction'] = 'none'
      }
    }

    const editorUIElm = document.getElementById(EditorKit.targetElementId)!
    this.editorContainerWidth = editorUIElm.clientWidth
    this.editorContainerHeight = editorUIElm.clientHeight
  }

  setVisiblityForInputArea(isShow: boolean) {
    if (isShow !== this.inputAreaElmVisibility) {
      const visibility = isShow ? 'visible' : 'hidden'
      if (this.inputAreaElm) {
        this.inputAreaElm.style['visibility'] = visibility
      }
      this.inputAreaElmVisibility = isShow
    }
  }

  move(x, y) {
    if (BrowserInfo.isMobile) return

    // const target = document.getElementById(this.TargetId)!
    const target = this.editor.editorUI.textCursor
    let xPos = x ? x : target.left
    let yPos = (y ? y : target.top) + target.height

    if (BrowserInfo.safari && BrowserInfo.isMobile) xPos = -100

    xPos /= BrowserInfo.PixelRatio
    yPos /= BrowserInfo.PixelRatio

    const left = xPos + this.containerOffX
    const top =
      yPos + this.containerOffY + this.notesAreaOffY + this.inputAreaOffset

    this.containerElm.style.left = `${left}px`
    this.containerElm.style.top = `${top}px`

    this.inputAreaElm!.scrollTop = this.inputAreaElm!.scrollHeight
  }

  clear(isFromFocusEvent = false) {
    if (!this.editable) {
      this.inputAreaElm!.innerHTML = ''
    } else {
      ;(this.inputAreaElm as HTMLTextAreaElement).value = ''
    }

    if (isFromFocusEvent !== true) this.inputAreaElm!.focus()

    this._inputBeforeComposition = ''
    this.text = ''
    this.compositionStart = 0
    this.compositionEnd = 0
    this.isInComposition = false
  }

  getInputAreaValue() {
    return this.editable
      ? (this.inputAreaElm as HTMLTextAreaElement).value
      : this.inputAreaElm!.innerText
  }

  onInput(e) {
    if (EditorSettings.isViewMode) {
      stopEvent(e)
      return false
    }

    // current text value
    this.text = this.getInputAreaValue()
    this.text = this.text.split('&nbsp;').join(' ')

    let codes: any[] = []
    if (this.isInComposition || this.isInApiComposition) {
      let startIndexInIE = -1

      if (true) {
        const target = e.target
        if (target['msGetInputContext']) {
          const ctx = target['msGetInputContext']()

          if (ctx) {
            startIndexInIE = ctx['compositionStartOffset']
          }
        }
      }

      this.compositionEnd = this.text.length
      this.compositionStart = this._inputBeforeComposition.length

      let tempInputText = this.text.substr(this.compositionStart)

      eachOfUnicodeString(tempInputText, (code) => codes.push(code))

      if (startIndexInIE > this.compositionStart) {
        tempInputText = tempInputText.substr(
          0,
          startIndexInIE - this.compositionStart
        )

        codes = []

        eachOfUnicodeString(tempInputText, (code) => codes.push(code))
        this.onCompositeReplace(codes)
        this.onCompositeEnd()
        this._inputBeforeComposition = this.text.substr(0, startIndexInIE)
        this.compositionStart = startIndexInIE

        codes = []
        tempInputText = this.text.substr(this.compositionStart)

        eachOfUnicodeString(tempInputText, (code) => codes.push(code))
        this.onCompositeReplace(codes)
      } else {
        this.onCompositeReplace(codes)
      }

      if (!this.isInComposition) {
        this.onCompositeEnd()
        this._inputBeforeComposition = this.text
      }
    } else {
      const inputText = this.text.substr(this._inputBeforeComposition.length)

      eachOfUnicodeString(inputText, (code) => codes.push(code))
      this.directlyInputText(codes)
      this._inputBeforeComposition = this.text
    }

    if (!this.isInComposition) {
      if (this.text.length > 0) {
        const lastCode = this.text.charCodeAt(this.text.length - 1)
        if (lastCode === 12290 || lastCode === 46) {
          stopEvent(e)

          if (BrowserInfo.ie && !BrowserInfo.edge) {
            setTimeout(() => {
              EditorUtil.inputManager.clear()
              EditorUtil.inputManager.inputAreaElm!.focus()
            }, 0)
          } else {
            this.clear()
          }

          return false
        }
      }
    }
  }

  directlyInputText(codes) {
    this.onCompositeReplace(codes)
    this.onCompositeEnd()
  }

  onKeyDown(e: KeyboardEvent) {
    GlobalEvents.keyboard.onKeyEvent(e)
    const ret = this.editor.editorUI.onKeyDown(e)

    switch (e.keyCode) {
      case keyCodeMap.backSpace:
      case keyCodeMap.tab:
      case keyCodeMap.enter:
      case keyCodeMap.pageUp:
      case keyCodeMap.pageDown:
      case keyCodeMap.end:
      case keyCodeMap.home:
      case keyCodeMap.arrowLeft:
      case keyCodeMap.arrowUp:
      case keyCodeMap.arrowRight:
      case keyCodeMap.arrowDown: {
        if (e.keyCode !== keyCodeMap.enter || !GlobalEvents.keyboard.ctrlKey) {
          this.clear()
          return false
        }
        break
      }
      case keyCodeMap.insert:
      case keyCodeMap.delete: {
        if (!GlobalEvents.keyboard.ctrlKey && !GlobalEvents.keyboard.shiftKey) {
          // copy/cut/paste
          this.clear()
          return false
        }
        break
      }
      default:
        break
    }
    return ret
  }

  onKeyPress(e: KeyboardEvent) {
    if (!this.editor.IsFocus() || EditorSettings.isViewMode) {
      stopEvent(e)
      return false
    }

    if (this.isInComposition) return

    if (
      (e.which === 13 && e.keyCode === 13) ||
      (e.which === 10 && e.keyCode === 10)
    ) {
      stopEvent(e)
      return false
    }

    const ret = this.editor.onKeyPress(e) as undefined

    switch (e.which) {
      case 46: {
        stopEvent(e)
        this.clear()
        return false
      }
      default:
        break
    }

    stopEvent(e)
    return ret
  }

  onKeyUp(e: KeyboardEvent) {
    GlobalEvents.keyboard.onKeyUp()
    this.editor.editorUI.onKeyUp(e)
  }

  resetLastCompositeText() {
    this.recentTextsForReplace = []
    this.islastReplace = false
  }

  onCompositeReplace(val) {
    if (!this.isInApiComposition) {
      this.editor.startCompositeInput()
      this.resetLastCompositeText()
    }
    this.isInApiComposition = true

    if (this.islastReplace) {
      if (val.length === this.recentTextsForReplace.length) {
        let isEqual = true
        for (let valueOfC = 0; valueOfC < val.length; valueOfC++) {
          if (val[valueOfC] !== this.recentTextsForReplace[valueOfC]) {
            isEqual = false
            break
          }
        }

        if (isEqual) return
      }
    }

    this.editor.replaceCompositeInput(val)
    this.recentTextsForReplace = val.slice()
    this.islastReplace = true
  }

  onCompositeEnd() {
    if (!this.isInApiComposition) return

    this.isInApiComposition = false
    this.editor.endComposite()
    this.resetLastCompositeText()
  }

  // 输入
  focusInpustElement() {
    const documentHasFocus = document.hasFocus()
    this.enableKeyEvents = true
    if (document.activeElement && documentHasFocus) {
      const id = document.activeElement.id
      if (id === DOM_ID.TextInputWrap.InputContainer.InputArea) {
        return
      }
    }
    if (!documentHasFocus) {
      window.focus()
    }
    if (!this.inputAreaElmVisibility) {
      this.setVisiblityForInputArea(true)
    }
    this.inputAreaElm!.focus()
  }

  // 快捷键
  focusEditorElement() {
    const documentHasFocus = document.hasFocus()
    this.enableKeyEvents = true
    if (document.activeElement && documentHasFocus) {
      const id = document.activeElement.id
      if (id === this.editorElement.id) {
        return
      }
    }
    if (!documentHasFocus) {
      window.focus()
    }
    this.editorElement.focus()
  }

  blurInpustElement() {
    const documentHasFocus = document.hasFocus()
    if (document.activeElement && documentHasFocus) {
      const id = document.activeElement.id
      if (id === DOM_ID.TextInputWrap.InputContainer.InputArea) {
        this.setVisiblityForInputArea(false)
        this.inputAreaElm!.blur()
      }
    }
  }
  checkAndClearIME() {
    if (!this.isInComposition) return false

    setTimeout(function () {
      EditorUtil.inputManager.clear()
    }, 10)

    return true
  }
}

function getKeyboardInputElement(elem, lvl: number) {
  let testElem = elem
  for (let l = 0; testElem && l < lvl; ++l, testElem = testElem.parentNode) {
    if (testElem.hasAttribute && testElem.hasAttribute('data-canfocused')) {
      return 'true'
    }
  }
  return null
}

export function initInputManager(editorKit: IEditorKit, parentId: string) {
  if (EditorUtil.inputManager) return

  const inputManager = (EditorUtil.inputManager = new TextInputManager(
    editorKit
  ))
  inputManager.init(parentId)
  gClipboard.init(editorKit)
  gClipboard.inputManager = inputManager

  document.addEventListener(
    'focus',
    (e) => {
      const manager = EditorUtil.inputManager
      const oldFoucus = manager.focusElement
      manager.focusElement = e.target as any

      if (manager.isInComposition) {
        manager.onCompositeEnd()
        manager.clear()
      }

      manager.clear(true)

      const oldLockFocusElement = manager.lockFocusElement
      manager.lockFocusElement = false

      if (manager.enableKeyEvents === false) {
        manager.focusElement = null
        return
      }

      if (
        manager.focusElement &&
        (manager.focusElement.id === manager.inputAreaElm!.id ||
          manager.focusElement.id === manager.editorElement.id)
      ) {
        manager.editor.editorUI.setKeyEventsFocus(true)

        if (oldLockFocusElement) {
          manager.focusElement = oldFoucus
        } else manager.focusElement = null

        return
      }

      manager.lockFocusElement = false

      let isEditable = false
      let name
      if (manager.focusElement) {
        name = manager.focusElement.nodeName
        if (name) name = name.toUpperCase()

        if ('INPUT' === name || 'TEXTAREA' === name) isEditable = true
        else if ('DIV' === name) {
          if (manager.focusElement.getAttribute('contenteditable') === 'true') {
            isEditable = true
          }
        }
      }
      if ('IFRAME' === name) {
        manager.editor.editorUI.setKeyEventsFocus(false)
        manager.focusElement = null
        return
      }

      const editorInput = getKeyboardInputElement(manager.focusElement, 10)

      if (editorInput === 'true') {
        manager.editor.editorUI.setKeyEventsFocus(false)
        manager.focusElement = null
        return
      }

      if (isEditable && editorInput !== 'false') {
        manager.editor.editorUI.setKeyEventsFocus(false)
        manager.focusElement = null
        return
      }

      manager.lockFocusElement = true // ie focus async
      if (!EditorSettings.isMobile) {
        if (!BrowserInfo.isTablet) {
          manager.editor.editorUI.focusInput()
        } else {
          manager.editor.editorUI.checkMobileInput()
        }
      }
      manager.focusElement = null
      manager.editor.editorUI.setKeyEventsFocus(true)
    },
    true
  )

  document.body.addEventListener(
    'blur',
    (e) => {
      if (EditorSettings.isMobile && !BrowserInfo.isTablet) return
      const { editorUI } = EditorUtil.inputManager.editor
      const isInShowMod = editorUI.showManager.isInShowMode
      if (isInShowMod && !e.relatedTarget) {
        editorUI.checkDemoFocus()
      }
      const isEditorFocus = editorUI.isFocus
      // 确保不影响手动操作占有的焦点
      if (!isEditorFocus && !e.relatedTarget) {
        if (!EditorSettings.isMobile && !BrowserInfo.isTablet) {
          editorUI.focusInput()
        } else {
          editorUI.checkMobileInput()
        }
      }
    },
    true
  )

  // send focus
  if (!EditorSettings.isMobile) {
    // todo: distinguish pad
    EditorUtil.inputManager.inputAreaElm!.focus()
  }
  EditorUtil.inputManager.onResize(parentId)
}
