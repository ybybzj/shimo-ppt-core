import { Nullable } from '../../../liber/pervasive'
import { PointerEventInfo, POINTER_EVENT_TYPE } from '../../common/dom/events'
import { ParaHyperlink } from '../../core/Paragraph/ParaContent/ParaHyperlink'
import { GraphicFrame } from '../../core/SlideElement/GraphicFrame'
import { GroupShape } from '../../core/SlideElement/GroupShape'
import { ImageShape } from '../../core/SlideElement/Image'
import { Shape } from '../../core/SlideElement/Shape'
import { SlideElement, TextSlideElement } from '../../core/SlideElement/type'
import { TextDocument } from '../../core/TextDocument/TextDocument'
import {
  DIRECTION_UNHIT,
  DIRECTION_ROT,
  RelativeDirections,
  CursorTypesByAbsDirection,
} from '../../core/common/const/ui'
import { isSlide } from '../../core/common/utils'
import { InstanceType } from '../../core/instanceTypes'
import {
  MouseMoveData,
  MouseMoveDataTypes,
} from '../../core/properties/MouseMoveData'
import {
  isChartImage,
  isCustomImage,
  isPlaceholderWithEmptyContent,
} from '../../core/utilities/shape/asserts'
import {
  getHyperlinkFromShape,
  isEavertTextContent,
} from '../../core/utilities/shape/getters'
import { isAudioImage, isVideoImage } from '../../core/utilities/shape/image'
import { EditorSettings } from '../../exports/type'
import { EditorUI } from '../editorUI/EditorUI'
import { cursorExtType } from '../editorUI/uiCursor'
import {
  convertGlobalInnerMMToPixPos,
  cancelEmitMenu,
} from '../editorUI/utils/utils'
import { hitComment } from '../features/comment/utils'
import { getAbsDirectionByRel } from '../features/resize/utils'
import { IEditorHandler } from '../rendering/editorHandler/type'
import { getHyperlinkFromSp } from '../rendering/helpers'
import {
  checkHitInTextSelection,
  hitInControls,
  hitBoundingRect,
  checkHitBounds,
  hitInnerArea,
  hitPath,
  hitTextRect,
  hitInVideoPlayButtonArea,
  updateCursorTypeWithTextDocument,
  hitToAdjustmentPoints,
} from './hittests'
import { HoverInfo, TextEditingState, TextEditingHitState } from './type'

export type HitInfo =
  | CommentHitInfo
  | TouchCursorHitInfo
  | SelectedElementsHitInfo
  | SlideElementsHitInfo
export interface CommentHitInfo {
  type: 'comment'
  hoverInfo: HoverInfo
  selectIndex: number
}

export interface TouchCursorHitInfo {
  type: 'touchCursor'
  hoverInfo: HoverInfo
  textSp: TextSlideElement
  hitState:
    | {
        state: typeof TextEditingHitState.InSelection
      }
    | {
        state: typeof TextEditingHitState.SelectionEdge
        hitResult: {
          otherEdgeXY: {
            x: number
            y: number
          }
          XY: {
            x: number
            y: number
          }
        }
      }
    | {
        state: typeof TextEditingHitState.Cursor
        hitResult: {
          mmX: number
          mmY: number
        }
      }
}

export interface SelectedElementsHitInfo {
  type: 'selectedElements'
  hoverInfo: HoverInfo
  sp: SlideElement
  group?: GroupShape
  hitState:
    | {
        state: 'AdjPoint'
        adjHitInfo: ReturnType<typeof hitToAdjustmentPoints>
      }
    | {
        state: 'Control'
        relDir: RelativeDirections | typeof DIRECTION_ROT
      }
    | {
        state: 'InBound'
      }
}

export interface SlideElementsHitInfo {
  type: 'slideElements'
  hoverInfo: HoverInfo
  sp: SlideElement
  group?: GroupShape
  hitState:
    | {
        state: 'HitText'
      }
    | {
        state: 'HitInBound'
        isDoubleTap?: boolean
      }
    | {
        state: 'HitMedia'
        mediaType: 'video' | 'audio'
      }
    | {
        state: 'HitSpHyperlink'
        link: string
      }
    | {
        state: 'SelectTextInGraphicFrame'
        canMouseSetCursor: boolean
        isCurEditing: boolean
      }
    | {
        state: 'NoHandle'
      }
}
/**
 * @uasge 根据 mm 坐标给出事件响应状态信息，
 * @note 优先级: Comment > Group(内部选中 > 内部所有) > 普通 Shape (选中 > 所有)
 * @return 命中信息, undefined 代表未命中任何 shape
 * */
export function getHitInfo(
  handler: IEditorHandler,
  param: {
    pos: { mmX: number; mmY: number }
    e?: PointerEventInfo
    startTextDocOfSp?: Nullable<TextDocument> // updateTextSelection
  }
): Nullable<HitInfo> {
  const { pos, e: event } = param
  const { selectionState } = handler
  const { mmX: x, mmY: y } = pos
  const e = event ?? new PointerEventInfo()
  let hitInfo: Nullable<HitInfo>

  // Comment
  if (EditorSettings.isSupportComment && EditorSettings.isShowComment) {
    hitInfo = hitInfoOfComments(handler, e, x, y)
    if (hitInfo) return hitInfo
  }

  // handle hit cursor (include text selection edge cursors)
  if (e.clicks < 2 && handler.editorUI.isInTouchMode()) {
    const hitInfo = hitInfoOfTouchCursor(handler, e, x, y)
    if (hitInfo) return hitInfo
  }

  // 是否命中已选中 Group
  const selectedGroup = selectionState.selectedGroup
  if (selectedGroup) {
    // 是否命中已选中 Group 内选中
    hitInfo = hitInfoOfSelectedElements(handler, e, x, y, selectedGroup)
    if (hitInfo) {
      return hitInfo
    }
    // 是否命中已选中 Group 内 Shape
    const childSps = selectedGroup.slideElementsInGroup
    hitInfo = hitInfoOfSlideElements(handler, childSps, e, x, y, selectedGroup)
    if (hitInfo) {
      return hitInfo
    }
  }

  // 是否命中已选中普通对象, 演示模式没有选中对象
  hitInfo = hitInfoOfSelectedElements(handler, e, x, y, null)
  if (hitInfo) {
    return hitInfo
  }

  // 是否命中普通对象
  const allSps = handler.getCurrentSpTree()
  hitInfo = hitInfoOfSlideElements(handler, allSps, e, x, y, null)
  if (hitInfo) {
    return hitInfo
  }

  // 未命中
  return
}
/** 返回坐标命中评论情况(默认最上层), 处理 Hover/Select 相关交互效果 */
function hitInfoOfComments(
  handler: IEditorHandler,
  e: PointerEventInfo,
  mmX: number,
  mmY: number
): Nullable<CommentHitInfo> {
  const slide = handler.editFocusContainer
  if (!isSlide(slide) || !slide.slideComments) return
  const comments = slide.slideComments ? slide.slideComments.comments : []
  const checkFunc = handler.editorUI.editorKit.shouldDisplayCommentFunc
  let selectIndex = -1 // 旧评论选中到新评论仅发送一次事件

  let hoverInfo: Nullable<HoverInfo>
  for (let i = comments.length - 1; i > -1; --i) {
    if (hitComment(handler, comments[i], mmX, mmY)) {
      const needHandleComment = checkFunc
        ? checkFunc(comments[i].toJSON())
        : true
      if (!needHandleComment) return

      hoverInfo = { objectId: comments[i].id, cursorType: 'pointer' }
      selectIndex = i // select 已选中评论也发送
      break
    }
  }

  return hoverInfo ? { type: 'comment', hoverInfo, selectIndex } : undefined
}

function hitInfoOfTouchCursor(
  handler: IEditorHandler,
  e: PointerEventInfo,
  x: number,
  y: number
): Nullable<TouchCursorHitInfo> {
  if (handler.textEditingState != null) {
    const textCursor = handler.editorUI.textCursor
    const textSp = handler.textEditingState.sp
    const hoverInfo: HoverInfo = {
      objectId: textSp.getId(),
      cursorType: cursorExtType.Text,
    }

    switch (handler.textEditingState.state) {
      case TextEditingState.Selecting: {
        const isHitInSelection = checkHitInTextSelection(textSp, x, y)

        const hitRes = textCursor.hitTestXYForSelectionEdge(x, y)
        if (
          (hitRes.hit !== true || hitRes.isHitHandle !== true) &&
          isHitInSelection
        ) {
          return {
            type: 'touchCursor',
            textSp,
            hoverInfo,
            hitState: { state: TextEditingHitState.InSelection },
          }
        }

        if (hitRes.hit) {
          return {
            type: 'touchCursor',
            textSp,
            hoverInfo,
            hitState: {
              state: TextEditingHitState.SelectionEdge,
              hitResult: hitRes,
            },
          }
        }
        break
      }
      case TextEditingState.Modifying: {
        const hitRes = textCursor.hitTestXYForCursor(x, y)
        if (hitRes.hit === true) {
          return {
            type: 'touchCursor',
            textSp,
            hoverInfo,
            hitState: {
              state: TextEditingHitState.Cursor,
              hitResult: hitRes,
            },
          }
        }
        break
      }
    }
  }
}

function makeMouseMoveDataByMMPos(
  editorUI: EditorUI,
  mmX: number,
  mmY: number
) {
  const mouseMoveData = new MouseMoveData()
  const pixPos = convertGlobalInnerMMToPixPos(editorUI, mmX, mmY)
  mouseMoveData.absX = pixPos.x
  mouseMoveData.absY = pixPos.y
  return mouseMoveData
}

/** 判断是否命中已选中对象的入口*/
function hitInfoOfSelectedElements(
  handler: IEditorHandler,
  e: PointerEventInfo,
  x: number,
  y: number,
  group: Nullable<GroupShape>
): Nullable<SelectedElementsHitInfo> {
  if (handler.isInShowMod()) {
    return
  }
  const { editorUI, selectionState } = handler
  const selectedSps = group
    ? selectionState.selectedElements
    : selectionState.selectedSlideElements
  const isTouchMode = editorUI.isInTouchMode()
  let hoverInfo: Nullable<HoverInfo>
  if (!EditorSettings.isViewMode) {
    // 命中 Geometry 调节节点
    if (selectedSps.length === 1) {
      const selectedSp = selectedSps[0]
      const hitAdjInfo = hitToAdjustmentPoints(selectedSp, x, y, isTouchMode)
      if (hitAdjInfo.hit) {
        hoverInfo = { objectId: selectedSp.getId(), cursorType: 'crosshair' }
        return {
          type: 'selectedElements',
          hoverInfo,
          sp: selectedSp,
          group: group ?? undefined,
          hitState: { state: 'AdjPoint', adjHitInfo: hitAdjInfo },
        }
      }
    }
    // 命中边框控制点
    if (!hoverInfo) {
      for (let i = selectedSps.length - 1; i > -1; --i) {
        const selectedSp = selectedSps[i]
        const relDir = hitInControls(selectedSp, x, y, isTouchMode)
        if (relDir !== DIRECTION_UNHIT) {
          const absDirection = getAbsDirectionByRel(selectedSp, relDir)
          hoverInfo = {
            objectId: selectedSp.getId(),
            cursorType:
              relDir === DIRECTION_ROT
                ? 'crosshair'
                : CursorTypesByAbsDirection[absDirection],
          }
          if (relDir === DIRECTION_ROT) {
            const mouseMoveData = makeMouseMoveDataByMMPos(editorUI, x, y)
            mouseMoveData.type = MouseMoveDataTypes.Rotate
            mouseMoveData.rotateInfo = { state: 'Hover' }
            hoverInfo.mouseData = mouseMoveData
          }
          return {
            type: 'selectedElements',
            hoverInfo,
            sp: selectedSp,
            group: group ?? undefined,
            hitState: { state: 'Control', relDir },
          }
        }
      }
    }
  }
  // 命中内容
  if (!hoverInfo) {
    for (let i = selectedSps.length - 1; i > -1; --i) {
      const sp = selectedSps[i]
      if (hitBoundingRect(selectedSps[i], x, y, isTouchMode)) {
        if (hitInVideoPlayButtonArea(sp, x, y)) {
          hoverInfo = { objectId: sp.getId(), cursorType: 'pointer' }
        } else {
          hoverInfo = { objectId: sp.getId(), cursorType: 'move' }
        }
        return {
          type: 'selectedElements',
          hoverInfo,
          sp,
          group: group ?? undefined,
          hitState: { state: 'InBound' },
        }
      }
    }
  }
}

/**
 * 判断是否命中对象的入口
 * @param spArr 需要遍历的 slideElement 数组
 */
function hitInfoOfSlideElements(
  handler: IEditorHandler,
  spArr: Nullable<SlideElement[]>,
  e: PointerEventInfo,
  x: number,
  y: number,
  group: Nullable<GroupShape>
): Nullable<SlideElementsHitInfo> {
  let ret: Nullable<SlideElementsHitInfo>
  if (!spArr) {
    return
  }
  const container = handler.editFocusContainer
  const isInNotes = container?.instanceType === InstanceType.Notes
  for (let i = spArr.length - 1; i > -1; --i) {
    const sp = spArr[i]
    if (!isInNotes && checkHitBounds(sp, x, y) === false) {
      continue
    }

    switch (sp.instanceType) {
      case InstanceType.Shape:
      case InstanceType.ImageShape: {
        ret = hitInfoOfShapeAndImage(sp, handler, e, x, y, group)
        break
      }
      case InstanceType.GroupShape: {
        ret = hitInfoOfGroupShape(sp, handler, e, x, y)
        break
      }
      case InstanceType.GraphicFrame: {
        ret = hitInfoOfGraphicFrame(sp, handler, e, x, y, group)
        break
      }
    }
    if (ret) {
      return ret
    }
    if (handler.isInShowMod()) {
      const hitInner = hitInnerArea(sp, x, y)
      const isHitPath = hitPath(sp, x, y, handler.editorUI.isInTouchMode())
      if (hitInner || isHitPath) {
        return ret
      }
    }
  }
  return ret
}

function hitInfoOfShapeAndImage(
  sp: Shape | ImageShape,
  handler: IEditorHandler,
  e: PointerEventInfo,
  x: number,
  y: number,
  group: Nullable<GroupShape>
): Nullable<SlideElementsHitInfo> {
  const hitInner = hitInnerArea(sp, x, y)
  const isHitPath = hitPath(sp, x, y, handler.editorUI.isInTouchMode())
  const inTextRect = hitTextRect(sp, x, y)

  const { editorUI } = handler
  const isTouchMode = editorUI.isInTouchMode()
  const textObject = handler.getTargetTextSp()
  const container = handler.editFocusContainer
  const isInNotes = container?.instanceType === InstanceType.Notes

  if (inTextRect) {
    const textLinkInfo = getHyperlinkFromSp(sp, x, y)
    if (handler.isInShowMod()) {
      if (!textLinkInfo) {
        return
      }
    }

    const canMouseSetCursor = !isTouchMode && (!e.shiftKey || textObject === sp)
    const isCurEditing = handler.textEditingState != null && textObject === sp
    const needEnterEditing =
      isTouchMode && (e.clicks > 1 || isInNotes) && !handler.isMultiSelectState
    const canTouchSetCursor = isCurEditing || needEnterEditing
    if (canMouseSetCursor || canTouchSetCursor) {
      if (!isCurEditing && needEnterEditing) {
        e.clicks = 1
        cancelEmitMenu() // 由于这里手动设置了 click count, 需要保持双击不触发菜单逻辑
      }
      return hitInfoOfText(handler, sp as Shape, e, x, y, group, textLinkInfo)
    } else {
      return {
        type: 'slideElements',
        hoverInfo: hoverInfoOfContent(sp, x, y),
        sp,
        group: group ?? undefined,
        hitState: { state: 'HitInBound' },
      }
    }
  } else if (!inTextRect && (hitInner || isHitPath)) {
    const mediaPlayInfo = getShapesMediaPlayPointerInfo(handler, [sp], x, y)
    if (mediaPlayInfo) {
      return {
        type: 'slideElements',
        hoverInfo: mediaPlayInfo,
        sp,
        group: group ?? undefined,
        hitState: { state: 'HitMedia', mediaType: 'video' },
      }
    }

    const spLinkInfo = getHyperlinkOfSpHoverInfo(
      handler,
      sp,
      e,
      inTextRect,
      x,
      y
    )

    if (spLinkInfo) {
      const { hoverInfo, link } = spLinkInfo
      return {
        type: 'slideElements',
        hoverInfo: hoverInfo,
        sp,
        group: group ?? undefined,
        hitState: { state: 'HitSpHyperlink', link },
      }
    }

    if (handler.isInShowMod()) {
      return
    }

    if (isAudioImage(sp) && e.clicks === 1 && !handler.isViewMode()) {
      const hoverInfo = {
        objectId: sp.getId(),
        cursorType: 'pointer',
      }
      return {
        type: 'slideElements',
        sp,
        group: group ?? undefined,
        hoverInfo,
        hitState: { state: 'HitMedia', mediaType: 'audio' },
      }
    }
    // 触屏双击直接进入编辑态(默认在尾部)
    if (
      isTouchMode &&
      sp.instanceType === InstanceType.Shape &&
      (e.clicks > 1 || isInNotes)
    ) {
      const isCurEditing = handler.textEditingState != null && textObject === sp
      if (!isCurEditing && !handler.isMultiSelectState) {
        return {
          type: 'slideElements',
          hoverInfo: hoverInfoOfTextSp(sp),
          sp,
          group: group ?? undefined,
          hitState: { state: 'HitInBound', isDoubleTap: true },
        }
      }
    }
    return {
      type: 'slideElements',
      hoverInfo: hoverInfoOfContent(sp, x, y),
      sp,
      group: group ?? undefined,
      hitState: {
        state: 'HitInBound',
      },
    }
  }
  return
}

function hitInfoOfShapeImageInGroupShape(
  handler: IEditorHandler,
  group: GroupShape,
  childSp: Shape | ImageShape,
  e: PointerEventInfo,
  x: number,
  y: number
): Nullable<SlideElementsHitInfo> {
  const hitInner = hitInnerArea(childSp, x, y)
  const isHitPath = hitPath(childSp, x, y, handler.editorUI.isInTouchMode())
  const inTextRect = hitTextRect(childSp, x, y)

  if (!inTextRect && (hitInner || isHitPath)) {
    let hoverInfo = getShapesMediaPlayPointerInfo(handler, [childSp], x, y)
    if (hoverInfo) {
      return {
        type: 'slideElements',
        hoverInfo,
        sp: childSp,
        group,
        hitState: {
          state: 'HitMedia',
          mediaType: 'video',
        },
      }
    }

    const spHoverInfo = getHyperlinkOfSpHoverInfo(
      handler,
      childSp,
      e,
      inTextRect,
      x,
      y
    )
    if (spHoverInfo) {
      const { hoverInfo, link } = spHoverInfo
      return {
        type: 'slideElements',
        hoverInfo,
        sp: childSp,
        group,
        hitState: {
          state: 'HitSpHyperlink',
          link,
        },
      }
    }

    const isSelectGroup = group?.selected && e.type !== POINTER_EVENT_TYPE.Down
    hoverInfo = isSelectGroup
      ? hoverInfoOfContent(childSp, x, y)
      : hoverInfoOfContent(group, x, y)
    const _sp = isSelectGroup ? childSp : group
    const _grp = isSelectGroup ? group : undefined
    return {
      type: 'slideElements',
      hoverInfo,
      sp: _sp,
      group: _grp,
      hitState: {
        state: 'HitInBound',
      },
    }
  } else if (inTextRect) {
    const hyperlink = getHyperlinkFromSp(childSp, x, y)
    if (handler.isInShowMod()) {
      if (!hyperlink) {
        return
      }
    }

    const textSp = handler.getTargetTextSp()

    const { editorUI, isMultiSelectState } = handler
    const isTouchMode = editorUI.isInTouchMode()
    const canMouseSetCursor =
      !isTouchMode && ((!e.ctrlKey && !e.shiftKey) || textSp === childSp)
    const isCurEditing = handler.textEditingState != null && textSp === childSp
    const needEnterEditing = isTouchMode && e.clicks > 1 && !isMultiSelectState
    const canTouchSetCursor = isCurEditing || needEnterEditing
    // Todo: 重复逻辑
    if (canMouseSetCursor || canTouchSetCursor) {
      if (!isCurEditing && needEnterEditing) {
        e.clicks = 1
        cancelEmitMenu() // 由于这里手动设置了 click count, 需要保持双击不触发菜单逻辑
      }
      return hitInfoOfText(handler, childSp as Shape, e, x, y, group, hyperlink)
    } else {
      const hoverInfo = hoverInfoOfContent(group, x, y)
      return {
        type: 'slideElements',
        hoverInfo,
        sp: group,
        group: undefined,
        hitState: {
          state: 'HitInBound',
        },
      }
    }
  }
}

function hitInfoOfGroupShape(
  groupShape: GroupShape,
  handler: IEditorHandler,
  e: PointerEventInfo,
  x: number,
  y: number
): Nullable<SlideElementsHitInfo> {
  const childSps = groupShape.getSlideElementsInGrp()
  let ret: Nullable<SlideElementsHitInfo>
  for (let j = childSps.length - 1; j > -1; --j) {
    const sp = childSps[j]
    if (checkHitBounds(sp, x, y) === false) {
      continue
    }

    switch (sp.instanceType) {
      case InstanceType.Shape:
      case InstanceType.ImageShape: {
        ret = hitInfoOfShapeImageInGroupShape(
          handler,
          groupShape,
          sp as Shape | ImageShape,
          e,
          x,
          y
        )
        if (ret) return ret
        break
      }
    }
  }
  return
}

function hitInfoOfGraphicFrame(
  graphicFrame: GraphicFrame,
  handler: IEditorHandler,
  e: PointerEventInfo,
  x: number,
  y: number,
  group: Nullable<GroupShape>
): Nullable<SlideElementsHitInfo> {
  const { selectionState } = handler
  const isTouchMode = handler.editorUI.isInTouchMode()
  if (hitBoundingRect(graphicFrame, x, y, isTouchMode)) {
    const hoverInfo = hoverInfoOfContent(graphicFrame, x, y)
    return {
      type: 'slideElements',
      hoverInfo,
      sp: graphicFrame,
      group: group ?? undefined,
      hitState: {
        state: 'HitInBound',
      },
    }
  } else {
    const textSp = handler.getTargetTextSp()
    const isHitTextRect = hitTextRect(graphicFrame, x, y)
    const isHitInnerArea = hitInnerArea(graphicFrame, x, y)
    const hyperlink = getHyperlinkFromSp(graphicFrame, x, y)
    if (isHitTextRect && handler.isInShowMod()) {
      if (!hyperlink) {
        return
      }

      if ((!e.ctrlKey && !e.shiftKey) || textSp === graphicFrame) {
        return hitInfoOfText(handler, graphicFrame, e, x, y, group, hyperlink)
      } else {
        const hoverInfo = hoverInfoOfContent(graphicFrame, x, y)
        return {
          type: 'slideElements',
          hoverInfo,
          sp: graphicFrame,
          group: group ?? undefined,
          hitState: {
            state: 'HitInBound',
          },
        }
      }
    } else if (isHitInnerArea) {
      if (isHitTextRect && hyperlink && e.ctrlKey) {
        return hitInfoOfText(handler, graphicFrame, e, x, y, group, hyperlink)
      }

      const isMouseMultiSelect =
        !isTouchMode && e.ctrlKey && selectionState.selectedElements.length
      const isTouchMultiSelect = isTouchMode && handler.isMultiSelectState
      const isMultiSelect = isMouseMultiSelect || isTouchMultiSelect

      const hoverInfo = graphicFrame.getCursorInfoByPos(x, y, e) ?? {
        objectId: graphicFrame.getId(),
        cursorType: cursorExtType.Text,
        updated: true,
      }
      if (isMultiSelect) {
        hoverInfo.cursorType = cursorExtType.Move
      }
      const isCurEditing =
        handler.textEditingState != null && textSp === graphicFrame

      const canTouchSelectCell = isTouchMode && !isMultiSelect
      const canTouchSetCursor = isCurEditing || canTouchSelectCell
      const canMouseSetCursor = !isTouchMode && !isMultiSelect
      // 触屏双击进入单元格编辑或者选中态单击
      if (
        (canMouseSetCursor || canTouchSetCursor) &&
        (graphicFrame.selected || !isTouchMode)
      ) {
        return {
          type: 'slideElements',
          hoverInfo,
          sp: graphicFrame,
          group: group ?? undefined,
          hitState: {
            state: 'SelectTextInGraphicFrame',
            canMouseSetCursor,
            isCurEditing,
          },
        }
      } else {
        return {
          type: 'slideElements',
          hoverInfo: hoverInfoOfContent(graphicFrame, x, y),
          sp: graphicFrame,
          group: group ?? undefined,
          hitState: {
            state: 'HitInBound',
          },
        }
      }
    }
  }
  return
}
/**
 * 已经命中 Text 区域, 检测 HyperLink 的相关处理
 * @notes ctrl 时, 对 HyperLink 操作必须先选中 Paragraph, 否则取不到 link 作 move 处理
 */
function getHyperlinkOfSpHoverInfo(
  handler: IEditorHandler,
  sp: Shape | ImageShape,
  e: PointerEventInfo,
  hitInText: boolean,
  x: number,
  y: number
): Nullable<{
  hoverInfo: HoverInfo
  link: string
}> {
  if (!handler.editFocusContainer?.cSld) return null
  const ret: HoverInfo = {
    objectId: sp.getId(),
    cursorType: cursorExtType.Move,
  }

  // 禁用图表和视频超链接
  if (
    isChartImage(sp) ||
    isVideoImage(sp) ||
    isAudioImage(sp) ||
    isCustomImage(sp)
  ) {
    return null
  }

  const hyperlink = getHyperlinkFromShape(sp)
  const link = hyperlink?.id
  if (link != null) {
    if (handler.curState.instanceType !== InstanceType.SelectTextState) {
      const editorUI = handler.editorUI
      const renderingController = editorUI.renderingController
      const mouseMoveData = new MouseMoveData()
      const pixPos = convertGlobalInnerMMToPixPos(
        renderingController.editorUI,
        x,
        y
      )
      mouseMoveData.absX = pixPos.x
      mouseMoveData.absY = pixPos.y
      mouseMoveData.type = MouseMoveDataTypes.Hyperlink
      mouseMoveData.hyperlink = {
        text: null,
        value: link,
        toolTip: hyperlink!.tooltip,
      }
      if (e.ctrlKey || handler.isInShowMod()) {
        ret.cursorType = cursorExtType.Pointer
      } else {
        if (hitInText) {
          ret.cursorType = isEavertTextContent((sp as Shape).txBody)
            ? cursorExtType.VerticalText
            : cursorExtType.Text
        }
      }
      ret.mouseData = mouseMoveData
    }
    return { hoverInfo: ret, link }
  }
  return null
}

function getShapesMediaPlayPointerInfo(
  handler: IEditorHandler,
  shapesArr: Nullable<SlideElement[]>,
  x: number,
  y: number
): Nullable<HoverInfo> {
  if (shapesArr?.length) {
    for (let i = shapesArr.length - 1; i >= 0; i--) {
      const shape = shapesArr[i]
      switch (shape.instanceType) {
        case InstanceType.ImageShape: {
          if (!shape.isOleObject()) {
            const ret = getMediaPlayPointerInfo(handler, shape, x, y)
            if (ret) {
              return ret
            }
          }
          break
        }
        case InstanceType.GroupShape: {
          const childSps = shape.getSlideElementsInGrp()
          const ret = getShapesMediaPlayPointerInfo(handler, childSps, x, y)
          if (ret) {
            return ret
          }
          break
        }
      }
    }
  }
}

function getMediaPlayPointerInfo(
  handler: IEditorHandler,
  shape: SlideElement,
  x: number,
  y: number
): Nullable<HoverInfo> {
  if (
    handler.isInShowMod() &&
    handler.editorUI.showManager.isInTransitionOrAnimation()
  ) {
    return
  }

  if (hitInVideoPlayButtonArea(shape, x, y)) {
    return {
      objectId: shape.getId(),
      cursorType: 'pointer',
    }
  }
}

/** 命中 SlideElement 内容区域的处理 */
function hoverInfoOfContent(sp: SlideElement, x: number, y: number): HoverInfo {
  if (hitInVideoPlayButtonArea(sp, x, y)) {
    return { objectId: sp.getId(), cursorType: 'pointer' }
  }

  return { objectId: sp.getId(), cursorType: 'move' }
}

function hoverInfoOfTextSp(sp: Shape): HoverInfo {
  return {
    objectId: sp.getId(),
    cursorType: isEavertTextContent((sp as Shape).txBody)
      ? cursorExtType.VerticalText
      : cursorExtType.Text,
  }
}

/**
 * 已经命中 Text 区域的处理(已经包含 HyperLink 的处理)
 * @notes ctrl 时, 对 HyperLink 操作必须先选中 Paragraph, 否则取不到 link 作 move 处理
 * @param x mmX
 * @param y mmY
 * Todo: 修改调用处
 */
function hitInfoOfText(
  handler: IEditorHandler,
  sp: Shape | GraphicFrame,
  e: PointerEventInfo,
  x: number,
  y: number,
  group: Nullable<GroupShape>,
  hyperlink: Nullable<ParaHyperlink>
): Nullable<SlideElementsHitInfo> {
  let hoverInfo: Nullable<HoverInfo>
  if (
    handler.editFocusContainer == null ||
    (handler.isViewMode() && isPlaceholderWithEmptyContent(sp))
  ) {
    hoverInfo = { objectId: sp.getId(), cursorType: 'default' }
    return {
      type: 'slideElements',
      hoverInfo,
      sp,
      group: group ?? undefined,
      hitState: { state: 'NoHandle' },
    }
  }
  hoverInfo = hoverInfoOfTextSp(sp as Shape)
  const container = handler.editFocusContainer
  const isInShowMod = handler.isInShowMod()

  // 触屏长按文本无操作
  if (handler.touchAction === 'longPress') {
    return {
      type: 'slideElements',
      hoverInfo,
      sp,
      group: group ?? undefined,
      hitState: { state: 'NoHandle' },
    }
  }

  const isInNotes = container.instanceType === InstanceType.Notes
  if ((e.ctrlKey || isInShowMod) && !isInNotes) {
    if (!hyperlink) {
      hoverInfo = hoverInfoOfContent(sp, x, y)
      return {
        type: 'slideElements',
        hoverInfo,
        sp,
        group: group ?? undefined,
        hitState: { state: 'HitInBound' },
      }
    }
  }

  if (hyperlink) {
    if (isInShowMod || e.ctrlKey) {
      if (hyperlink) {
        hoverInfo.hyperlink = hyperlink
        hoverInfo.cursorType = 'pointer'
      } else {
        return null
      }
    } else {
      if (hyperlink) {
        hoverInfo.hyperlink = hyperlink
      }
    }
  } else if (
    !isInShowMod &&
    container?.cSld &&
    handler.curState.instanceType !== InstanceType.SelectTextState
  ) {
    const documentRet = updateCursorTypeWithTextDocument(sp, x, y, e)
    if (documentRet) {
      hoverInfo = documentRet
    }
    hoverInfo.updated = false
  }

  return {
    type: 'slideElements',
    hoverInfo,
    sp,
    group: group ?? undefined,
    hitState: { state: 'HitText' },
  }
}
