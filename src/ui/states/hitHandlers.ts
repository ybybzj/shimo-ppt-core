import { Nullable } from '../../../liber/pervasive'
import { BrowserInfo } from '../../common/browserInfo'
import { PointerEventInfo, POINTER_BUTTON } from '../../common/dom/events'
import { GraphicFrame } from '../../core/SlideElement/GraphicFrame'
import { GroupShape } from '../../core/SlideElement/GroupShape'
import { ImageShape } from '../../core/SlideElement/Image'
import { Shape } from '../../core/SlideElement/Shape'
import { SlideElement, TextSlideElement } from '../../core/SlideElement/type'
import { TextDocument } from '../../core/TextDocument/TextDocument'
import { DIRECTION_ROT, RelativeDirections } from '../../core/common/const/ui'
import { Factor_mm_to_pix } from '../../core/common/const/unit'
import { isSlide } from '../../core/common/utils'
import { InstanceType, isInstanceTypeOf } from '../../core/instanceTypes'
import { moveCursorToEndPosInTextDocument } from '../../core/utilities/cursor/moveToEdgePos'
import { isGroup } from '../../core/utilities/shape/asserts'
import {
  getTextDocumentOfSp,
  getMediaFromShape,
  testTextDocOfShape,
} from '../../core/utilities/shape/getters'
import { canRotate, canResize } from '../../core/utilities/shape/operations'
import {
  Evt_hideContextMenu,
  Evt_onMediaPlay,
} from '../../editor/events/EventNames'
import { EditorSettings, SlideElementPos } from '../../exports/type'
import { handleDoubleClickEmptyShape } from '../editorUI/utils/pointers'
import {
  cancelEmitMenu,
  touchToggleContextMenu,
  touchEmitEditorContextMenu,
  toggleZoomMode,
} from '../editorUI/utils/utils'
import { canMove } from '../features/move/utils'
import {
  handleSelectionStartForSp,
  handleSelectionEndForSp,
} from '../features/rectSelect/utils'
import { getAbsDirectionByRel } from '../features/resize/utils'
import { getTrackRectPosition } from '../rendering/Drawer/EditLayerDrawer'
import { IEditorHandler } from '../rendering/editorHandler/type'
import { genAudioAction } from '../transitions/AudioManager'
import { hitInVideoPlayButtonArea } from './hittests'
import { HoverInfo, TextEditingHitState } from './type'
import {
  SelectedElementsHitInfo,
  SlideElementsHitInfo,
  TouchCursorHitInfo,
  getHitInfo,
} from './hitInfo'
import { Comment } from '../../core/Slide/Comments/Comment'

/**
 * @uasge 根据 mm 坐标判断事件响应
 * @note 优先级: Comment > Group(内部选中 > 内部所有) > 普通 Shape (选中 > 所有)
 * @return 命中信息, undefined 代表未命中任何 shape
 * */
export function handleHitPostion(
  handler: IEditorHandler,
  param: {
    pos: { mmX: number; mmY: number }
    e: PointerEventInfo
    startTextDocOfSp?: Nullable<TextDocument> // updateTextSelection
  }
) {
  const hitInfo = getHitInfo(handler, param)
  const { mmX: x, mmY: y } = param.pos
  const e = param.e
  const startTextDocOfSp = param.startTextDocOfSp

  if (hitInfo) {
    switch (hitInfo.type) {
      case 'comment': {
        handleComments(handler, hitInfo.selectIndex)
        break
      }
      case 'touchCursor': {
        handleTouchCursor(handler, hitInfo, x, y, e)
        break
      }
      case 'selectedElements': {
        handleSelectedElements(
          handler,
          hitInfo,
          e ?? new PointerEventInfo(),
          x,
          y
        )
        if (startTextDocOfSp) {
          checkLocalTextSelection(handler, startTextDocOfSp)
        }
        break
      }
      case 'slideElements': {
        handleSlideElements(handler, hitInfo, e ?? new PointerEventInfo(), x, y)
        if (startTextDocOfSp) {
          checkLocalTextSelection(handler, startTextDocOfSp)
        }
        break
      }
    }
    return true
  }

  // 未命中
  handleComments(handler)

  // 退出多选态
  if (handler.isMultiSelectState) {
    handler.isMultiSelectState = false
  }
  const { editorUI, touchAction: touchMode } = handler
  if (editorUI.isInTouchMode()) {
    // 触屏-单击空白处, 非多选态, 有选中内容则清除选中, 无选中内容展开收起菜单
    // 触屏-单击空白处, 多选态, 有选中内容则清除选中, 无选中内容展开收起菜单
    // 触屏-双击空白处, 非多选态, 调整缩放
    // 触屏-双击空白处, 多选态, 退出多选取消选中(Todo: 由于单击也会触发退出多选，最终会响应缩放，这里不建议再延时处理，暂不处理)
    // 触屏-长按空白处, 非多选态, 取消选中/展开菜单
    // 触屏-长按空白处, 多选态, 退出多选/展开菜单
    if (touchMode === 'tap' || touchMode === 'longPress') {
      if (handler.selectionState.hasSelection) {
        handler.selectionState.reset()
        handler.editorUI.editorKit.emitEvent(Evt_hideContextMenu)
      } else {
        touchToggleContextMenu(handler.editorUI, { mmX: x, mmY: y })
      }
      // Todo: 触屏双击, 多选态则退出多选, 否则调整缩放至适应窗口/最大
      if (e.clicks > 1) {
        toggleZoomMode(editorUI)
      }
      return true
    }
  } else {
    handler.selectionState.reset()
  }

  const curContainer = handler.editFocusContainer
  if (startTextDocOfSp) {
    curContainer?.onUpdateSlide()
  }
  if (curContainer?.cSld && !handler.isInShowMod()) {
    handler.changeToState(InstanceType.RectSelectState, handler, x, y)
  }
  return false
}
/** 返回坐标命中评论情况(默认最上层), 处理 Hover/Select 相关交互效果 */
function handleComments(
  handler: IEditorHandler,
  selectedIndex?: number
): Nullable<HoverInfo> {
  const slide = handler.editFocusContainer
  if (!isSlide(slide) || !slide.slideComments) return
  const comments = slide.slideComments ? slide.slideComments.comments : []
  if (comments.length <= 0) return
  const selectedCmt =
    selectedIndex != null ? comments[selectedIndex] : undefined
  const deselectedComments: Comment[] = []
  for (const cmt of comments) {
    if (selectedCmt !== cmt && cmt.selected) {
      deselectedComments.push(cmt)
    }
  }

  // 选中/取消选中
  const editorUI = handler.editorUI
  let needTrigger = false
  for (const cmt of deselectedComments) {
    cmt.selected = false
    needTrigger = true
  }

  if (selectedCmt) {
    if (selectedCmt.selected === false) {
      selectedCmt.selected = true
    }
    const cmData = selectedCmt.toJSON()
    editorUI.editorKit.emitSelectedCommentChangeCallback(cmData)
    needTrigger = true
  } else if (deselectedComments.length > 0) {
    editorUI.editorKit.emitSelectedCommentChangeCallback()
  }
  if (needTrigger) {
    editorUI.triggerRender(true)
  }
}

function handleTouchCursor(
  handler: IEditorHandler,
  hitInfo: TouchCursorHitInfo,
  x: number,
  y: number,
  e?: PointerEventInfo
) {
  const { textSp, hitState } = hitInfo
  switch (hitState.state) {
    case TextEditingHitState.InSelection: {
      handler.changeToState(
        InstanceType.TouchSelectTextState,
        handler,
        textSp,
        TextEditingHitState.InSelection,
        x,
        y
      )
      break
    }
    case TextEditingHitState.SelectionEdge: {
      const {
        XY: { x: targetX, y: targetY },
        otherEdgeXY: { x: otherX, y: otherY },
      } = hitState.hitResult
      e = e ?? new PointerEventInfo()
      handleSelectionStartForSp(textSp, handler, otherX, otherY, e)

      handleSelectionEndForSp(textSp, handler, targetX, targetY, e)
      handler.changeToState(
        InstanceType.TouchSelectTextState,
        handler,
        textSp,
        TextEditingHitState.SelectionEdge,
        x,
        y,
        targetX,
        targetY
      )
      break
    }
    case TextEditingHitState.Cursor: {
      const { mmX: targetX, mmY: targetY } = hitState.hitResult
      handler.changeToState(
        InstanceType.TouchSelectTextState,
        handler,
        textSp,
        TextEditingHitState.Cursor,
        x,
        y,
        targetX,
        targetY
      )
      break
    }
  }
}

/** 判断是否命中已选中对象的入口*/
function handleSelectedElements(
  handler: IEditorHandler,
  hitInfo: SelectedElementsHitInfo,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  const { hitState, sp, group } = hitInfo
  switch (hitState.state) {
    case 'AdjPoint': {
      handleHitAdjust(handler, hitState.adjHitInfo, sp, group)
      break
    }
    case 'Control': {
      handleSelectHit(handler, hitState.relDir, sp, group)
      break
    }
    case 'InBound': {
      handleHitContent(handler, sp, e, x, y, group, true)
      break
    }
  }
}

/**
 * 判断是否命中对象的入口
 * @param spArr 需要遍历的 slideElement 数组
 */
function handleSlideElements(
  handler: IEditorHandler,
  hitInfo: SlideElementsHitInfo,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  const { sp, group, hitState } = hitInfo
  switch (hitState.state) {
    case 'HitText': {
      handleHitText(handler, sp as Shape | GraphicFrame, e, x, y)
      break
    }
    case 'HitInBound': {
      if (hitState.isDoubleTap) {
        handleDoubleTapContent(handler, sp as Shape, e)
      } else {
        handleHitContent(handler, sp, e, x, y, group, false)
      }
      break
    }
    case 'HitSpHyperlink': {
      const link = hitState.link
      if (e.ctrlKey || handler.isInShowMod()) {
        handler.editorUI.editorKit.emitHyperlinkClick(link)
      }
      break
    }
    case 'HitMedia': {
      if (hitState.mediaType === 'audio') {
        handleAudio(handler, sp)
        handleHitContent(handler, sp, e, x, y, group, false)
      }
      break
    }
    case 'SelectTextInGraphicFrame': {
      const { isCurEditing, canMouseSetCursor } = hitState
      handleSelectTextInGraphicFrame(
        handler,
        sp as GraphicFrame,
        e,
        x,
        y,
        isCurEditing,
        canMouseSetCursor
      )
      break
    }
    case 'NoHandle': {
      break
    }
  }
}

function handleSelectTextInGraphicFrame(
  handler: IEditorHandler,
  graphicFrame: GraphicFrame,
  e: PointerEventInfo,
  x: number,
  y: number,
  isCurEditing: boolean,
  canMouseSetCursor: boolean
) {
  const needEnterEditing = e.clicks > 1
  if (!isCurEditing && needEnterEditing) {
    e.clicks = 1
    cancelEmitMenu() // 由于这里手动设置了 click count, 需要保持双击不触发菜单逻辑
  }
  if (needEnterEditing || canMouseSetCursor) {
    changeToTextEditingState(graphicFrame, handler)
  } else if (graphicFrame.selected !== true) {
    graphicFrame.graphicObject?.removeSelection()
    handler.selectionState.select(graphicFrame)
  }

  handleSelectionStartForSp(graphicFrame, handler, x, y, e)

  handler.changeToState(
    InstanceType.SelectTextState,
    handler,
    graphicFrame,
    x,
    y
  )
}

/**
 * 已经 Select 一个 SlideElement, 且鼠标正处于操作区
 * @param relDir resize: 左上角顺时针为起点0直至7(line起点0,终点4). Rotate:8, -1: 未选中
 */
function handleSelectHit(
  handler: IEditorHandler,
  relDir: RelativeDirections | typeof DIRECTION_ROT,
  selectedSp: SlideElement,
  group: Nullable<GroupShape>
) {
  const selectionState = handler.selectionState

  if (relDir === DIRECTION_ROT) {
    if (canRotate(selectedSp)) {
      handler.changeToState(
        InstanceType.RotateCheckState,
        handler,
        selectedSp,
        group
      )
      if (!isGroup(group)) {
        selectionState?.resetRootSelection()
      } else {
        selectionState?.resetGroupSelection()
      }
      handler.updateEditLayer()
    }
  } else if (canResize(selectedSp)) {
    const absoluteDirection = getAbsDirectionByRel(selectedSp, relDir)
    handler.changeToState(
      InstanceType.ResizeCheckState,
      handler,
      selectedSp,
      absoluteDirection,
      group
    )
    if (!isGroup(group)) {
      selectionState?.resetRootSelection()
    } else {
      selectionState?.resetGroupSelection()
    }
    handler.updateEditLayer()
  }
}

export function handleShapesMediaPlay(
  handler: IEditorHandler,
  shapesArr: Nullable<SlideElement[]>,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  if (shapesArr?.length) {
    for (let i = shapesArr.length - 1; i >= 0; i--) {
      const shape = shapesArr[i]
      switch (shape.instanceType) {
        case InstanceType.ImageShape:
          if (
            !shape.isOleObject() &&
            handleMediaPlay(handler, shape, e, x, y)
          ) {
            return
          }
          break
        case InstanceType.GroupShape:
          const childSps = shape.getSlideElementsInGrp()
          handleShapesMediaPlay(handler, childSps, e, x, y)
          break
      }
    }
  }
}

function handleAudio(handler: IEditorHandler, audioSp: SlideElement) {
  const curSlide = handler.getCurrentSlide()
  const audioAction = genAudioAction(curSlide, audioSp, 'end', true)
  if (audioAction) {
    const pos = getAudioElmPosInPx(handler, audioSp as ImageShape)
    handler.editorUI.editorKit.emitAudioAction(audioAction, pos)
    if (handler.touchAction === 'tap') {
      cancelEmitMenu()
    }
  }
}

export function handleMediaPlay(
  handler: IEditorHandler,
  shape: SlideElement,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  if (
    handler.isInShowMod() &&
    handler.editorUI.showManager.isInTransitionOrAnimation()
  ) {
    return
  }
  if (
    e.button === POINTER_BUTTON.Left &&
    isInstanceTypeOf(shape, InstanceType.ImageShape) &&
    hitInVideoPlayButtonArea(shape, x, y)
  ) {
    const media = getMediaFromShape(shape)
    handler.editorUI.editorKit.emitEvent(Evt_onMediaPlay, media)
    return true
  }
}

/** 命中 SlideElement 内容区域的处理 */
function handleHitContent(
  handler: IEditorHandler,
  sp: SlideElement,
  e: PointerEventInfo,
  x: number,
  y: number,
  group: Nullable<GroupShape>,
  isInSelect: boolean
) {
  const selectionState = handler.selectionState
  const { editorUI, touchAction } = handler
  const isSelected = sp.selected
  const isMultiSelect = handler.isMultiSelectState || e.ctrlKey
  // 触屏状态下展开/收起菜单
  if (!(isMultiSelect || e.shiftKey) && !isSelected) {
    selectionState.reset()
  }
  // touch
  if (touchAction != null) {
    // 点击
    if (touchAction === 'tap' || touchAction === 'press') {
      // 非多选态，未选中则选中并展开菜单; 已选中则 toggle 菜单
      if (!isMultiSelect) {
        if (isSelected) {
          touchToggleContextMenu(editorUI, { mmX: x, mmY: y })
        } else {
          selectionState.select(sp)
          touchEmitEditorContextMenu(editorUI, { mmX: x, mmY: y })
        }
      }
      // 多选态，普通对象未选中则选中并展开菜单；已选中则取消选中
      // isMultiSelect === true
      else {
        // 组合特殊处理，未选中则选中组合；已选中且仅有组合对象被选中才进入下级对象多选态, 否则取消对组合的多选
        if (sp.group) {
          if (!sp.group.selected) {
            selectionState.select(sp.group)
            touchEmitEditorContextMenu(editorUI, { mmX: x, mmY: y })
          } else {
            const selectedSps = selectionState.selectedElements
            if (selectedSps.length > 1 && selectedSps.includes(sp.group)) {
              selectionState.deselect(sp.group)
            } else {
              selectionState.select(sp)
            }
          }
        } else {
          if (!sp.selected) {
            selectionState.select(sp)
            touchEmitEditorContextMenu(editorUI, { mmX: x, mmY: y })
          } else {
            // deselect 逻辑已经在 moveCheckState 中处理过, do nothing
          }
        }
      }
    }
    // 长按
    else if (touchAction === 'longPress') {
      // 进入多选
      if (!isMultiSelect) {
        handler.isMultiSelectState = true
        // 非多选态长按非选中态对象, 选中并进入多选态
        // 非多选态长按选中对象，保持选中并进入多选态
        if (!isSelected) {
          selectionState.select(sp.group ?? sp)
        } else {
          // do nothing
        }
      }

      // 多选态长按非选中对象，加入选中并展开菜单
      // 多选态长按已选中对象，展开菜单
      if (isMultiSelect) {
        // 组合特殊处理，已选中组合则进入子对象多选态
        if (sp.group) {
          if (sp.group.selected) {
            selectionState.select(sp)
          } else {
            selectionState.select(sp.group)
          }
        } else {
          if (!isSelected) selectionState.select(sp)
          touchEmitEditorContextMenu(editorUI, { mmX: x, mmY: y })
        }
      }
    }
  }
  // mouse
  else {
    if (e.ctrlKey || !sp.selected) {
      selectionState.select(sp)
    }
  }
  selectionState.setTextSelection(null)

  if (canMove(sp)) {
    if (!isGroup(group)) {
      selectionState.resetRootSelection()

      handler.changeToState(
        InstanceType.MoveCheckState,
        handler,
        x,
        y,
        e.shiftKey,
        e.ctrlKey,
        sp,
        isSelected,
        !isInSelect
      )
    } else {
      handler.changeToState(
        InstanceType.MoveInGroupCheckState,
        handler,
        group,
        x,
        y,
        e.shiftKey,
        e.ctrlKey,
        sp,
        isSelected
      )
    }
    if (
      e.clicks > 1 &&
      !e.shiftKey &&
      !e.ctrlKey &&
      selectionState.selectedElements.length === 1
    ) {
      if (sp.instanceType === InstanceType.Shape && !sp.isConnectionShape()) {
        if (!getTextDocumentOfSp(sp)) {
          handleDoubleClickEmptyShape(handler, sp)
        }
      }
    }
  }
  handler.updateEditLayer()
}

function handleDoubleTapContent(
  handler: IEditorHandler,
  sp: Shape,
  e: PointerEventInfo
) {
  const textDoc = getTextDocumentOfSp(sp)
  if (textDoc) {
    e.clicks = 1
    cancelEmitMenu()
    moveCursorToEndPosInTextDocument(textDoc)
    changeToTextEditingState(sp, handler)
  } else {
    handleDoubleClickEmptyShape(handler, sp)
  }
}

/**
 * 已经命中 Text 区域的处理(已经包含 HyperLink 的处理)
 * @notes ctrl 时, 对 HyperLink 操作必须先选中 Paragraph, 否则取不到 link 作 move 处理
 * @param x mmX
 * @param y mmY
 * Todo: 修改调用处
 */
function handleHitText(
  handler: IEditorHandler,
  sp: Shape | GraphicFrame,
  e: PointerEventInfo,
  x: number,
  y: number
) {
  const isInShowMod = handler.isInShowMod()

  changeToTextEditingState(sp, handler)

  const originCtrlKey = e.ctrlKey
  if (isInShowMod && !e.ctrlKey) {
    e.ctrlKey = true
  }

  handleSelectionStartForSp(sp, handler, x, y, e)

  if (isInShowMod) {
    e.ctrlKey = originCtrlKey
  }
  handler.changeToState(InstanceType.SelectTextState, handler, sp, x, y)
}

/**
 * 已经 Select 一个具有特殊调节操作点的 Shape, 且鼠标正处调节操作区(如弯曲点, 非起/终点的 resize 节点)
 * @param hit bentconector: {hit: true, isPolarAdj: false, adjIndex: 0, warp: false}
 * @note 普通横纵大小调节生成 XYAdjustmentControl, 环形 geometry 生成 PolarAdjustmentControl
 */
function handleHitAdjust(
  handler: IEditorHandler,
  hit: {
    hit: boolean
    isPolarAdj: any
    adjIndex: any
  },
  selectedSp: SlideElement,
  group: Nullable<GroupShape>
) {
  const selectionState = handler.selectionState
  handler.changeToState(
    InstanceType.AdjCheckState,
    handler,
    selectedSp as Shape,
    hit.adjIndex,
    hit.isPolarAdj
  )
  if (!isGroup(group)) {
    selectionState.resetRootSelection()
  } else {
    selectionState.resetGroupSelection()
  }
  handler.updateEditLayer()
}

function checkLocalTextSelection(
  handler: IEditorHandler,
  startDoc: TextDocument | null | undefined
) {
  const endDoc = testTextDocOfShape(handler.presentation.getCurrentTextTarget())
  if ((startDoc || endDoc) && startDoc !== endDoc) {
    handler.editFocusContainer?.onUpdateSlide()
  }
}

// helpers
function changeToTextEditingState(
  sp: TextSlideElement,
  handler: IEditorHandler
) {
  const selectionState = handler.selectionState
  if (selectionState && selectionState.selectedTextSp !== sp) {
    selectionState.reset(true)
    selectionState.setTextSelection(sp)
    handler.editorUI.focusInput()
  }
}

const MinDist = 206
function getAudioElmPosInPx(
  handler: IEditorHandler,
  image: ImageShape
): SlideElementPos {
  const slideRenderInfo = handler.editorUI.getSlideRenderingInfo()
  const { pos, width, height } = getTrackRectPosition(
    image.getTransform(),
    0,
    0,
    image.extX,
    image.extY,
    slideRenderInfo
  )

  const renderCanvas = handler.editorUI.editorRenderingCanvas!
  const w = renderCanvas.width / BrowserInfo.PixelRatio
  const { L, T } = handler.editorUI.mainContainer.PositionInPx
  const offsetL = L * Factor_mm_to_pix
  const offsetT = T * Factor_mm_to_pix

  const [absX, absY] = [pos.x1, pos.y1]

  const adjust_absX =
    absX < MinDist ? MinDist : w - absX < MinDist ? w - MinDist - 20 : absX
  return {
    width,
    height,
    absX:
      (EditorSettings.isRTL ? w - adjust_absX + 160 : adjust_absX) + offsetL,
    absY: absY + offsetT,
  }
}
