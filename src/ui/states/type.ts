import { Nullable, valuesOfDict } from '../../../liber/pervasive'
import { MouseMoveData } from '../../core/properties/MouseMoveData'
import { CropState } from '../features/crop/cropState'
import { ChangeAdjStateTypes } from '../features/adjust/adjust'
import { CommentStateTypes } from '../features/comment/commentState'
import { UIInitState } from './UIInitState'
import { MoveStateTypes } from '../features/move/move'
import { ResizeStateTypes } from '../features/resize/resize'
import { RotateStateTypes } from '../features/rotate/rotate'
import { ShapeStateTypes } from '../features/addShape/addPolyline'
import { EditLayerDrawer } from '../rendering/Drawer/EditLayerDrawer'
import { ShapeEditControl } from '../features/common/type'
import { RectSelectState } from '../features/rectSelect/rectSelectState'
import {
  SelectTextState,
  TouchSelectTextState,
} from '../features/rectSelect/selectTextState'
import { DragOutState } from '../features/dragOut/dragOut'
import { ParaHyperlink } from '../../core/Paragraph/ParaContent/ParaHyperlink'
import { PointerEventInfo } from '../../common/dom/events'

export type UIOperationStates = {
  getHoverInfo?: (
    e: PointerEventInfo,
    x: number,
    y: number
  ) => Nullable<HoverInfo>
  drawControl?: (editLayerDrawer: EditLayerDrawer) => void
  controls?: ShapeEditControl[]
  beforeLeave?: () => void
  onStart: (e: PointerEventInfo, x: number, y: number) => boolean // is handled
  onMove: (e: PointerEventInfo, x: number, y: number) => void
  onEnd: (e: PointerEventInfo, x: number, y: number) => void
} & (
  | UIInitState
  | RectSelectState
  | SelectTextState
  | TouchSelectTextState
  | CommentStateTypes
  | ChangeAdjStateTypes
  | MoveStateTypes
  | ResizeStateTypes
  | RotateStateTypes
  | ShapeStateTypes
  | CropState
  | DragOutState
)

export type UIOperationStateKinds = UIOperationStates['instanceType']

type ExtractState<
  T extends { instanceType: UIOperationStateKinds },
  K extends UIOperationStateKinds,
> = T extends { instanceType: K } ? T : never

export type UIStateOfKind<K extends UIOperationStateKinds> = ExtractState<
  UIOperationStates,
  K
>

export type HoverInfo = {
  objectId: string
  cursorType: string
  /** 为 true 则当次不更新 cursorType */
  updated?: boolean
  hyperlink?: ParaHyperlink
  /** 在 CursorComponent 进行 cursor 相关统一处理时需要的数据*/
  mouseData?: MouseMoveData
  absDirection?: number
}

export const TextEditingState = {
  Selecting: 1,
  Modifying: 2,
} as const

export type TextEditingStateValus = valuesOfDict<typeof TextEditingState>

export const TextEditingHitState = {
  SelectionEdge: 1,
  InSelection: 2,
  Cursor: 3,
} as const

export type TextEdititngHitStateValues = valuesOfDict<
  typeof TextEditingHitState
>
