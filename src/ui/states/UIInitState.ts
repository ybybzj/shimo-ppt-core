import { Nullable } from '../../../liber/pervasive'
import { PointerEventInfo } from '../../common/dom/events'
import { testTextDocOfShape } from '../../core/utilities/shape/getters'

import { InstanceType } from '../../core/instanceTypes'
import { IEditorHandler } from '../rendering/editorHandler/EditorHandler'
import { HoverInfo } from './type'
import { getHitInfo } from './hitInfo'
import { handleHitPostion, handleShapesMediaPlay } from './hitHandlers'

export class UIInitState {
  readonly instanceType = InstanceType.UIInitState
  handler: IEditorHandler
  constructor(handler: IEditorHandler) {
    this.handler = handler
  }

  getHoverInfo(e: PointerEventInfo, x: number, y: number): Nullable<HoverInfo> {
    const param = {
      pos: { mmX: x, mmY: y },
      e,
    }
    const hitInfo = getHitInfo(this.handler, param)
    if (hitInfo) {
      return hitInfo.hoverInfo
    }
  }

  /** x, y 均为 mm 单位 **/
  onStart(e: PointerEventInfo, x: number, y: number) {
    const handler = this.handler
    const startTextDocOfSp = testTextDocOfShape(
      handler.presentation.getCurrentTextTarget()
    )
    const param = { pos: { mmX: x, mmY: y }, e, startTextDocOfSp }
    const handled = handleHitPostion(handler, param)
    return handled
  }

  onMove(_e: PointerEventInfo, _x: number, _y: number) {}

  onEnd(e: PointerEventInfo, x: number, y: number) {
    const handler = this.handler
    handleShapesMediaPlay(handler, handler.getCurrentSpTree(), e, x, y)
  }
}
