import { InstanceType } from '../../core/instanceTypes'
import { CropState } from '../features/crop/cropState'
import { AdjCheckState, AdjState } from '../features/adjust/adjust'
import {
  StartAddNewCommentState,
  AddNewCommentState,
} from '../features/comment/commentState'
import { UIInitState } from './UIInitState'
import {
  MoveCheckState,
  MoveState,
  MoveInGroupCheckState,
} from '../features/move/move'
import { ResizeCheckState, ResizeState } from '../features/resize/resize'
import { RotateCheckState, RotateState } from '../features/rotate/rotate'
import {
  PolyLineState,
  PolyShapeState,
  PolyShapePreClickState,
  PolyShapeClickState,
  PolyLineClickState,
} from '../features/addShape/addPolyline'
import { UIOperationStateKinds, UIStateOfKind } from './type'
import { AddGeometryState } from '../features/addShape/addGeometry'
import {
  BezierState,
  BezierClickState,
  QuadraticBezierState,
  QuadraticBezierClickState,
  CubicBezierState,
  CubicBezierClickState,
} from '../features/addShape/addBezier'
import { RectSelectState } from '../features/rectSelect/rectSelectState'
import {
  SelectTextState,
  TouchSelectTextState,
} from '../features/rectSelect/selectTextState'
import { DragOutState } from '../features/dragOut/dragOut'

type GetCtorParams<T> = T extends { new (...args: infer Params): any }
  ? Params
  : never

const uiCtorMap = {
  [InstanceType.UIInitState]: UIInitState,
  [InstanceType.RectSelectState]: RectSelectState,
  [InstanceType.TouchSelectTextState]: TouchSelectTextState,
  [InstanceType.SelectTextState]: SelectTextState,
  [InstanceType.StartAddNewCommentState]: StartAddNewCommentState,
  [InstanceType.AddNewCommentState]: AddNewCommentState,
  [InstanceType.AdjState]: AdjState,
  [InstanceType.AdjCheckState]: AdjCheckState,
  [InstanceType.MoveCheckState]: MoveCheckState,
  [InstanceType.MoveState]: MoveState,
  [InstanceType.MoveInGroupCheckState]: MoveInGroupCheckState,
  [InstanceType.DragOutState]: DragOutState,
  [InstanceType.ResizeCheckState]: ResizeCheckState,
  [InstanceType.ResizeState]: ResizeState,
  [InstanceType.RotateCheckState]: RotateCheckState,
  [InstanceType.RotateState]: RotateState,
  [InstanceType.CropState]: CropState,
  [InstanceType.AddGeometryState]: AddGeometryState,
  [InstanceType.BezierState]: BezierState,
  [InstanceType.BezierClickState]: BezierClickState,
  [InstanceType.QuadraticBezierState]: QuadraticBezierState,
  [InstanceType.QuadraticBezierClickState]: QuadraticBezierClickState,
  [InstanceType.CubicBezierState]: CubicBezierState,
  [InstanceType.CubicBezierClickState]: CubicBezierClickState,
  [InstanceType.PolyLineState]: PolyLineState,
  [InstanceType.PolyLineClickState]: PolyLineClickState,
  [InstanceType.PolyShapeState]: PolyShapeState,
  [InstanceType.PolyShapePreClickState]: PolyShapePreClickState,
  [InstanceType.PolyShapeClickState]: PolyShapeClickState,
} as const

export type UIStateParams<K extends UIOperationStateKinds> = GetCtorParams<
  (typeof uiCtorMap)[K]
>

export function createUIState<K extends UIOperationStateKinds>(
  kind: K,
  ...args: UIStateParams<K>
): UIStateOfKind<K> {
  const Ctor = uiCtorMap[kind] as { new (...args: any[]): any }
  const ret = new Ctor(...args)

  return ret as UIStateOfKind<K>
}
