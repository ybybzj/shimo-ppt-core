import { Nullable } from '../../../liber/pervasive'
import { FillKIND } from '../../core/common/const/attrs'
import { isNumber } from '../../common/utils'
import { VideoPlayButtonImage } from '../../images/images'
import { InstanceType } from '../../core/instanceTypes'
import { SlideElement, TextSlideElement } from '../../core/SlideElement/type'
import { HoverInfo } from './type'
import {
  Factor_pix_to_mm,
  ScaleOfPPTXSizes,
} from '../../core/common/const/unit'
import { GraphicFrame } from '../../core/SlideElement/GraphicFrame'
import { getHitTestContext, hitLine } from '../../core/graphic/hittest'
import { Shape } from '../../core/SlideElement/Shape'
import { convertPxToMM } from '../../core/utilities/helpers'
import {
  isLineShape,
  isPlaceholderWithEmptyContent,
  isPlaceholder,
  isGroup,
} from '../../core/utilities/shape/asserts'
import { getTextDocumentOfSp } from '../../core/utilities/shape/getters'
import { canRotate } from '../../core/utilities/shape/operations'
import {
  getInvertTransform,
  getInvertTextTransformWithAnimation,
  getInvertTransformWithAnimation,
} from '../../core/utilities/shape/calcs'
import { isVideoImage } from '../../core/utilities/shape/image'
import { getPresentation } from '../../core/utilities/finders'
import {
  DirectionValue,
  DIRECTION_ROT,
  DIRECTION_UNHIT,
  GroupShapeRenderingMargin,
  REL_DIRECTION,
} from '../../core/common/const/ui'
import { MatrixUtils } from '../../core/graphic/Matrix'
import { isCoordsInSelectionForTextDocument } from '../../core/utilities/tableOrTextDocContent/helpers'
import {
  CONTROL_CIRCLE_RADIUS,
  CONTROL_DISTANCE_ROTATE,
  TOUCH_CONTROL_CIRCLE_RADIUS,
} from '../../core/common/const/control'
import { calcGroupShapeRenderingRect } from '../../core/utilities/shape/group'
import { Geometry } from '../../core/SlideElement/geometry/Geometry'

export function hitBoundingRect(
  sp: SlideElement,
  x: number,
  y: number,
  isTouch: boolean,
  checkRotate = true
): boolean {
  if (sp.parent?.instanceType === InstanceType.Notes) {
    return false
  }
  const invertTransform = getInvertTransformWithAnimation(sp)
  if (invertTransform == null) {
    return false
  }
  const transform = invertTransform.clone()
  if (isGroup(sp)) {
    MatrixUtils.translateMatrix(
      transform,
      GroupShapeRenderingMargin,
      GroupShapeRenderingMargin
    )
  }
  const tx = transform.XFromPoint(x, y)
  const ty = transform.YFromPoint(x, y)

  const hitCtx = getHitTestContext()
  switch (sp.instanceType) {
    case InstanceType.Shape:
    case InstanceType.ImageShape:
    case InstanceType.GroupShape: {
      let extX = sp.extX
      let extY = sp.extY
      if (isGroup(sp)) {
        const rect = calcGroupShapeRenderingRect(0, 0, extX, extY)
        x = rect.x
        y = rect.y
        extX = rect.extX
        extY = rect.extY
      }
      return (
        !isLineShape(sp) &&
        (hitLine(hitCtx, tx, ty, 0, 0, extX, 0, isTouch) ||
          hitLine(hitCtx, tx, ty, extX, 0, extX, extY, isTouch) ||
          hitLine(hitCtx, tx, ty, extX, extY, 0, extY, isTouch) ||
          hitLine(hitCtx, tx, ty, 0, extY, 0, 0, isTouch) ||
          (checkRotate &&
            canRotate(sp) &&
            hitLine(
              hitCtx,
              tx,
              ty,
              extX * 0.5,
              0,
              extX * 0.5,
              -1 * convertPxToMM(CONTROL_DISTANCE_ROTATE),
              isTouch
            )))
      )
    }
    case InstanceType.GraphicFrame: {
      return (
        hitLine(hitCtx, tx, ty, 0, 0, sp.extX, 0, isTouch) ||
        hitLine(hitCtx, tx, ty, sp.extX, 0, sp.extX, sp.extY, isTouch) ||
        hitLine(hitCtx, tx, ty, sp.extX, sp.extY, 0, sp.extY, isTouch) ||
        hitLine(hitCtx, tx, ty, 0, sp.extY, 0, 0, isTouch)
      )
    }
  }
}

export function hitInnerArea(
  sp: SlideElement,
  xMM: number, // 距离幻灯片内容左边距离
  yMM: number
): boolean {
  switch (sp.instanceType) {
    case InstanceType.Shape:
    case InstanceType.ImageShape: {
      if (
        ((sp.brush?.fill && sp.brush.fill.type !== FillKIND.NOFILL) ||
          sp.blipFill) &&
        checkHitBounds(sp, xMM, yMM)
      ) {
        const invertTransform = getInvertTransformWithAnimation(sp)
        if (invertTransform == null) {
          return false
        }
        const tx = invertTransform.XFromPoint(xMM, yMM) // 距离形状左边距离
        const ty = invertTransform.YFromPoint(xMM, yMM)
        if ((sp.spPr as any)?.geometry?.pathLst.length > 0) {
          const ctx = getHitTestContext()
          return sp.spPr!.geometry!.hitTestInnerArea(ctx, tx, ty)
        }
        return tx > 0 && tx < sp.extX && ty > 0 && ty < sp.extY
      }
      return false
    }
    case InstanceType.GraphicFrame: {
      const invertTransform = getInvertTransformWithAnimation(sp)
      if (invertTransform == null) {
        return false
      }
      const tx = invertTransform.XFromPoint(xMM, yMM)
      const ty = invertTransform.YFromPoint(xMM, yMM)
      return tx > 0 && tx < sp.extX && ty > 0 && ty < sp.extY
    }
    default:
      return false
  }
}

export function hitTextRect(sp: SlideElement, x: number, y: number): boolean {
  switch (sp.instanceType) {
    case InstanceType.GroupShape:
    case InstanceType.ImageShape:
      return false
    case InstanceType.Shape: {
      const selectedTextSp = getPresentation(sp)?.selectionState.selectedTextSp
      if (sp.parent?.instanceType === InstanceType.Notes) {
        return true
      }

      const isForceWord =
        isPlaceholderWithEmptyContent(sp) ||
        (isPlaceholder(sp) && selectedTextSp === sp)
      if (isForceWord) {
        if (hitTextRectWord(sp, x, y)) {
          return true
        }
      }

      const textDoc = getTextDocumentOfSp(sp)
      const invertTextTransform = getInvertTextTransformWithAnimation(sp)
      if (textDoc && invertTextTransform) {
        const tx = invertTextTransform.XFromPoint(x, y)
        const ty = invertTextTransform.YFromPoint(x, y)
        return (
          tx > 0 && tx < sp.contentWidth! && ty > 0 && ty < sp.contentHeight
        )
      }
      return false
    }
    case InstanceType.GraphicFrame:
      return hitInnerArea(sp, x, y)
  }
}

/**
 * @usage 是否命中边框操作区,
 * @return 左上角 0 开始，顺时针计数，rotate 圆角标为8
 * */
export function hitInControls(
  sp: SlideElement,
  x: number,
  y: number,
  isTouch: boolean
): DirectionValue {
  if (sp.parent?.instanceType === InstanceType.Notes) {
    return -1
  }
  return _hitInControls(x, y, sp, isTouch)
}

export function hitPath(
  sp: SlideElement,
  x: number,
  y: number,
  isTouch: boolean
): boolean {
  if (!sp || sp.instanceType === InstanceType.GraphicFrame) {
    return false
  }
  if (!checkHitBounds(sp, x, y)) return false

  const invertTransform = getInvertTransformWithAnimation(sp)
  if (invertTransform == null) {
    return false
  }
  const tx = invertTransform.XFromPoint(x, y)
  const ty = invertTransform.YFromPoint(x, y)

  if (sp.spPr?.geometry) {
    const ctx = getHitTestContext()
    return sp.spPr.geometry.hitPath(ctx, tx, ty, isTouch)
  } else return hitBoundingRect(sp, x, y, isTouch)
}

/**
 * - 计算 element 内 docContennt cursorType ,统一 return 至最上级 UpdateUiCursorType 处理
 * @calledBy handletHitText
 * */
export function updateCursorTypeWithTextDocument(
  sp: Shape | GraphicFrame,
  x: number,
  y: number,
  _e
): Nullable<HoverInfo> {
  const textDoc = getTextDocumentOfSp(sp)
  const invertTextTransform = getInvertTextTransformWithAnimation(sp)
  if (invertTextTransform && textDoc) {
    const tx = invertTextTransform.XFromPoint(x, y)
    const ty = invertTextTransform.YFromPoint(x, y)
    return textDoc.getCursorInfoByPos(tx, ty)
  }
}

// helpers
function hitTextRectWord(sp: Shape, mmX, mmY) {
  const textDoc = getTextDocumentOfSp(sp)
  const invertTransform = getInvertTransformWithAnimation(sp)
  if (textDoc && invertTransform) {
    const tx = invertTransform.XFromPoint(mmX, mmY)
    const ty = invertTransform.YFromPoint(mmX, mmY)
    let x, y, w, h

    if (
      sp.spPr?.geometry?.rect &&
      isNumber(sp.spPr.geometry.rect.l) &&
      isNumber(sp.spPr.geometry.rect.t) &&
      isNumber(sp.spPr.geometry.rect.r) &&
      isNumber(sp.spPr.geometry.rect.b)
    ) {
      x = sp.spPr.geometry.rect.l
      y = sp.spPr.geometry.rect.t
      w = sp.spPr.geometry.rect.r - sp.spPr.geometry.rect.l
      h = sp.spPr.geometry.rect.b - sp.spPr.geometry.rect.t
    } else {
      x = 0
      y = 0
      w = sp.extX
      h = sp.extY
    }
    return tx > x && tx < x + w && ty > y && ty < y + h
  }
  return false
}

function _hitInControls(
  x,
  y,
  sp: SlideElement,
  isTouch: boolean
): DirectionValue {
  const invertTransform = getInvertTransform(sp)!
  const transform = invertTransform.clone()
  if (isGroup(sp)) {
    MatrixUtils.translateMatrix(
      transform,
      GroupShapeRenderingMargin,
      GroupShapeRenderingMargin
    )
  }
  const tx = transform.XFromPoint(x, y)
  const ty = transform.YFromPoint(x, y)
  const pxRadius = isTouch ? TOUCH_CONTROL_CIRCLE_RADIUS : CONTROL_CIRCLE_RADIUS
  let radius = convertPxToMM(pxRadius)

  radius *= radius //  避免后续开方，这里都用平方计算

  let threshold = 2 * radius
  let relDirection: DirectionValue = DIRECTION_UNHIT
  const isLine = !!isLineShape(sp)

  let [squareX, squareY] = [Math.pow(tx, 2), Math.pow(ty, 2)]
  let dist = squareX + squareY
  if (dist < threshold) {
    threshold = dist
    relDirection = REL_DIRECTION.LT
  }

  let extX = sp.extX
  let extY = sp.extY
  if (isGroup(sp)) {
    const rect = calcGroupShapeRenderingRect(0, 0, extX, extY)
    x = rect.x
    y = rect.y
    extX = rect.extX
    extY = rect.extY
  }

  const hc = extX * 0.5
  let dx = tx - hc
  squareX = dx * dx
  dist = squareX + squareY
  if (dist < threshold && !isLine) {
    threshold = dist
    relDirection = REL_DIRECTION.T
  }

  dx = tx - extX
  squareX = dx * dx
  dist = squareX + squareY
  if (dist < threshold && !isLine) {
    threshold = dist
    relDirection = REL_DIRECTION.RT
  }

  const vc = extY * 0.5
  let dy = ty - vc
  squareY = dy * dy
  dist = squareX + squareY
  if (dist < threshold && !isLine) {
    threshold = dist
    relDirection = REL_DIRECTION.R
  }

  dy = ty - extY
  squareY = dy * dy
  dist = squareX + squareY
  if (dist < threshold) {
    threshold = dist
    relDirection = REL_DIRECTION.RB
  }

  dx = tx - hc
  squareX = dx * dx
  dist = squareX + squareY
  if (dist < threshold && !isLine) {
    threshold = dist
    relDirection = REL_DIRECTION.B
  }

  dx = tx
  squareX = dx * dx
  dist = squareX + squareY
  if (dist < threshold && !isLine) {
    threshold = dist
    relDirection = REL_DIRECTION.LB
  }

  dy = ty - vc
  squareY = dy * dy
  dist = squareX + squareY
  if (dist < threshold && !isLine) {
    threshold = dist
    relDirection = REL_DIRECTION.L
  }

  if (canRotate(sp) && !isLine) {
    const rotDist = convertPxToMM(CONTROL_DISTANCE_ROTATE)
    dy = ty + rotDist
    squareY = dy * dy
    dx = tx - hc
    squareX = dx * dx

    dist = squareX + squareY
    if (dist < threshold) {
      threshold = dist
      relDirection = DIRECTION_ROT
    }
  }

  dx = tx - hc
  dy = ty - vc
  dist = dx * dx + dy * dy
  if (dist < threshold && !isLine) {
    threshold = dist
    relDirection = DIRECTION_UNHIT
  }

  if (threshold < radius) return relDirection

  return -1
}

export function checkHitBounds(
  sp: SlideElement,
  x: number,
  y: number
): boolean {
  const { bounds, pen } = sp
  const [_x, _y] = [x - bounds.x, y - bounds.y]
  const delta = 3 + (pen && isNumber(pen.w) ? pen.w / ScaleOfPPTXSizes : 0)
  const isXHit = _x >= -delta && _x <= bounds.w + delta
  const isYHit = _y >= -delta && _y <= bounds.h + delta
  return isXHit && isYHit
}

export function hitInVideoPlayButtonArea(
  sp: SlideElement,
  xMM: number,
  yMM: number
): boolean {
  switch (sp.instanceType) {
    case InstanceType.ImageShape: {
      if (isVideoImage(sp) && checkHitBounds(sp, xMM, yMM)) {
        const w = sp.extX
        const h = sp.extY

        let width = (VideoPlayButtonImage.width / 2) * Factor_pix_to_mm
        let height = (VideoPlayButtonImage.height / 2) * Factor_pix_to_mm
        if (w < width) {
          height = (height / width) * w
          width = w
        }
        if (h < height) {
          width = (width / height) * h
          height = h
        }

        const invertTransform = getInvertTransformWithAnimation(sp)
        if (invertTransform == null) {
          return false
        }
        const xTransformMM = invertTransform.XFromPoint(xMM, yMM) // 距离形状左边距离
        const yTransformMM = invertTransform.YFromPoint(xMM, yMM)
        const paddingX = (w - width) / 2
        const paddingY = (h - height) / 2

        return (
          xTransformMM >= paddingX &&
          xTransformMM < w - paddingX &&
          yTransformMM >= paddingY &&
          yTransformMM < h - paddingY
        )
      }
      return false
    }
    default:
      return false
  }
}

export function checkHitInTextSelection(
  sp: TextSlideElement,
  x: number,
  y: number
) {
  const textDoc = getTextDocumentOfSp(sp)
  if (textDoc == null) return false

  const tx = sp.invertTextTransform!.XFromPoint(x, y)
  const ty = sp.invertTextTransform!.YFromPoint(x, y)

  return isCoordsInSelectionForTextDocument(textDoc, tx, ty)
}

/**  判断是否命中 Connector 的调节操作区(如弯曲点, 非起/终点的 resize 节点) */
export function hitToAdjustmentPoints(
  sp: SlideElement,
  x: number,
  y: number,
  isTouch: boolean
): { hit: boolean; isPolarAdj: boolean | null; adjIndex: number | null } {
  if (
    sp.instanceType === InstanceType.GroupShape ||
    sp.instanceType === InstanceType.GraphicFrame
  ) {
    return { hit: false, isPolarAdj: null, adjIndex: null }
  }

  let invertTransform = getInvertTransform(sp)
  let tx = invertTransform.XFromPoint(x, y)
  let ty = invertTransform.YFromPoint(x, y)
  const geometry: Nullable<Geometry> = sp.calcedGeometry || sp.spPr?.geometry
  if (geometry) {
    invertTransform = getInvertTransform(sp)
    tx = invertTransform.XFromPoint(x, y)
    ty = invertTransform.YFromPoint(x, y)
    const pxRadius = isTouch
      ? TOUCH_CONTROL_CIRCLE_RADIUS
      : CONTROL_CIRCLE_RADIUS
    const ret = geometry.hitAdj(tx, ty, convertPxToMM(pxRadius))
    if (ret.hit) {
      return ret
    }
  }
  return { hit: false, isPolarAdj: null, adjIndex: null }
}
