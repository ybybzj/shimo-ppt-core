import { Nullable } from '../../../liber/pervasive'
import { PointerEventInfo } from '../../common/dom/events'
import { UIOperationStates, HoverInfo } from './type'
import { InstanceType, isInstanceTypeOf } from '../../core/instanceTypes'
import { IEditorHandler } from '../rendering/editorHandler/EditorHandler'
import { cursorExtType } from '../editorUI/uiCursor'

/** @usage 获取当前 Hover 状态的唯一入口,*/
export function getCurStateHoverInfo(
  handler: IEditorHandler,
  x: number,
  y: number,
  e?: PointerEventInfo
): Nullable<HoverInfo> {
  const state = handler.curState
  let ret: Nullable<HoverInfo>
  if (state.getHoverInfo) {
    ret = state.getHoverInfo(e || new PointerEventInfo(), x, y)
    return ret
  }
  return getCurStateDefaultRet(state)
}

export function isCurAddingShape(handler: IEditorHandler) {
  const curState = handler.curState
  return (
    isInstanceTypeOf(curState, InstanceType.AddGeometryState) ||
    isInstanceTypeOf(curState, InstanceType.BezierState) ||
    isInstanceTypeOf(curState, InstanceType.PolyLineState) ||
    isInstanceTypeOf(curState, InstanceType.PolyShapeState) ||
    curState.controls
  )
}

export function isCurAddingComment(handler: IEditorHandler) {
  const curState = handler.curState
  return (
    isInstanceTypeOf(curState, InstanceType.StartAddNewCommentState) ||
    isInstanceTypeOf(curState, InstanceType.AddNewCommentState)
  )
}

export function needDrawSelectEffect(handler: IEditorHandler) {
  if (handler.isViewMode()) return false
  const state = handler.curState
  switch (state.instanceType) {
    case InstanceType.RectSelectState:
    case InstanceType.CropState:
    case InstanceType.AddNewCommentState:
      return false
  }
  return true
}

export function needDrawHoverEffect(handler: IEditorHandler) {
  if (handler.isViewMode()) return false
  const state = handler.curState
  switch (state.instanceType) {
    case InstanceType.RectSelectState:
    case InstanceType.CropState:
      return false
  }
  return true
}

export function needDrawControls(handler: IEditorHandler) {
  if (handler.curState.instanceType === InstanceType.UIInitState) return false
  return true
}

function getCurStateDefaultRet(state: UIOperationStates) {
  let ret: Nullable<HoverInfo>
  switch (state.instanceType) {
    case InstanceType.StartAddNewCommentState:
      ret = { objectId: '1', cursorType: cursorExtType.CommentPainter }
      break
    case InstanceType.AddNewCommentState:
      ret = { objectId: '1', cursorType: 'pointer' }
      break
    case InstanceType.MoveCheckState:
    case InstanceType.MoveState:
    case InstanceType.MoveInGroupCheckState:
      ret = { objectId: state.majorTarget.getId(), cursorType: 'move' }
      break
    case InstanceType.AdjState:
    case InstanceType.ResizeCheckState:
    case InstanceType.ResizeState:
    case InstanceType.RotateCheckState:
      ret = { objectId: state.majorTarget.getId(), cursorType: 'crosshair' }
      break
    case InstanceType.AddGeometryState:
    case InstanceType.BezierState:
    case InstanceType.BezierClickState:
    case InstanceType.QuadraticBezierState:
    case InstanceType.QuadraticBezierClickState:
    case InstanceType.CubicBezierState:
    case InstanceType.CubicBezierClickState:
    case InstanceType.PolyLineState:
    case InstanceType.PolyLineClickState:
    case InstanceType.PolyShapeState:
    case InstanceType.PolyShapePreClickState:
    case InstanceType.PolyShapeClickState:
      ret = { objectId: '1', cursorType: 'crosshair' }
      break
    default:
      ret = { objectId: '1', cursorType: 'default' }
      break
  }
  return ret
}
