import { IEditorKit } from '../editor/type'
import { Nullable } from '../../liber/pervasive'
import { BrowserInfo } from '../common/browserInfo'
import { getCanvasDrawer } from '../core/graphic/CanvasDrawer'
import { renderPresentationSlide } from '../core/render/slide'
import { calculateRenderingStateForSlides } from '../core/calculation/calculate/presentation'
interface CacheImage {
  cacheImage: CanvasImage
  width: number
  height: number
}

const defaultBgColor = '#B1B1B1'

export interface SlidePreviewImageInfo {
  width: number
  height: number
  slideNum: number
  type?: string
  quality?: number
}

type CanvasImage = HTMLCanvasElement & { ctx: CanvasRenderingContext2D }

const imageBuffer: {
  MAX_CACHE_COUNT: number
  cacheImages: CacheImage[]
  getCacheImage(w: number, h: number): CacheImage
  getCanvasImage(w: number, h: number): CanvasImage | null
} = {
  MAX_CACHE_COUNT: 3,
  cacheImages: [],
  getCacheImage(w: number, h: number): CacheImage {
    let found
    for (let i = 0, l = imageBuffer.cacheImages.length; i < l; i++) {
      const cacheImage = imageBuffer.cacheImages[i]
      if (cacheImage.width === w && cacheImage.height === h) {
        found = cacheImage
        break
      }
    }

    if (found == null) {
      found = {
        cacheImage: createImageBuffer(w, h),
        width: w,
        height: h,
      }

      imageBuffer.cacheImages.push(found)

      const len = imageBuffer.cacheImages.length
      if (len > imageBuffer.MAX_CACHE_COUNT) {
        imageBuffer.cacheImages = imageBuffer.cacheImages.slice(
          len - imageBuffer.MAX_CACHE_COUNT
        )
      }
    }

    return found
  },
  getCanvasImage(w: number, h: number): CanvasImage | null {
    if (w <= 0 || h <= 0) {
      return null
    }

    const image = imageBuffer.getCacheImage(w, h)
    const ctx = image.cacheImage.ctx
    if (ctx) {
      ctx.globalAlpha = 1.0
      ctx.fillStyle = defaultBgColor
      ctx.fillRect(0, 0, w, h)
    }
    return image.cacheImage
  },
}

function createImageBuffer(w: number, h: number): CanvasImage {
  const image = document.createElement('canvas') as CanvasImage

  image.width = w
  image.height = h

  const ctx = image.getContext('2d')
  if (ctx) {
    ctx.fillStyle = defaultBgColor
    ctx.fillRect(0, 0, w, h)
    ctx.globalAlpha = 1.0
    image.ctx = ctx
  }
  return image
}

export class SlidePreviewImages {
  private countOfSlides: number = 0

  private editor: IEditorKit
  constructor(editor: IEditorKit) {
    this.editor = editor
  }

  calculateState() {
    this.checkCount()
  }

  checkCount() {
    this.countOfSlides = this.editor.presentation.slides.length
  }

  getSlidePreviews(previewInfos: SlidePreviewImageInfo[]): {
    [k: number]: { imgBase64: string }
  } {
    const result: {
      [k: number]: { imgBase64: string }
    } = {}

    this.checkCount()
    const countOfSlides = this.countOfSlides

    for (const { width, height, slideNum } of previewInfos) {
      if (slideNum >= 0 && slideNum < countOfSlides) {
        const imgBase64 = this.getPreviewImageBase64(slideNum, width, height)
        if (imgBase64 != null) {
          result[slideNum] = {
            imgBase64,
          }
        }
      }
    }

    return result
  }

  async getSlidePreviewsAsync(previewInfos: SlidePreviewImageInfo[]): Promise<{
    [k: number]: { imageData: string }
  }> {
    const result: {
      [k: number]: { imageData: string }
    } = {}

    this.checkCount()
    const countOfSlides = this.countOfSlides

    for (const { width, height, slideNum, type, quality } of previewInfos) {
      if (slideNum >= 0 && slideNum < countOfSlides) {
        let imageData
        try {
          imageData = await this.getPreviewImageData(
            slideNum,
            width,
            height,
            type,
            quality
          )
        } catch (_e) {}
        if (imageData != null) {
          result[slideNum] = {
            imageData,
          }
        }
      }
    }

    return result
  }

  private getPreviewImageBase64(
    slideIndex: number,
    w: number,
    h: number
  ): Nullable<string> {
    const canvasImage = this.getPreviewCanvasImage(slideIndex, w, h)
    if (!canvasImage) {
      return null
    }
    return canvasImage.toDataURL('image/png')
  }

  private async getPreviewImageData(
    slideIndex: number,
    w: number,
    h: number,
    type = 'image/png',
    quality = 1
  ) {
    const canvasImage = this.getPreviewCanvasImage(slideIndex, w, h)
    if (!canvasImage) {
      return null
    }
    return new Promise((res) => {
      canvasImage.toBlob(
        (data) => {
          res(data)
        },
        type,
        quality
      )
    })
  }

  private getPreviewCanvasImage(
    slideIndex: number,
    w: number,
    h: number
  ): Nullable<CanvasImage> {
    w *= BrowserInfo.PixelRatio
    h *= BrowserInfo.PixelRatio

    if (w <= 0 || h <= 0) {
      return null
    }

    const cacheImage = imageBuffer.getCanvasImage(w, h)

    if (!cacheImage) {
      return null
    }

    const ctx = cacheImage.ctx
    if (ctx == null) {
      return null
    }
    const presentation = this.editor.presentation
    const slideWidthInMM = presentation.width
    const slideHeightInMM = presentation.height
    const drawCxt = getCanvasDrawer(ctx, w, h, slideWidthInMM, slideHeightInMM)
    drawCxt.isInPreview = true

    drawCxt.isNoRenderEmptyPlaceholder = true
    drawCxt.setCoordTransform(0, 0)

    drawCxt.updateTransform(1, 0, 0, 1, 0, 0)
    calculateRenderingStateForSlides([presentation.slides[slideIndex]])
    renderPresentationSlide(this.editor.presentation, slideIndex, drawCxt)
    drawCxt.isInPreview = false
    drawCxt.resetState()
    return cacheImage
  }
}
