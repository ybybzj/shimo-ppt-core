import { Dict, Nullable, Optional } from '../../liber/pervasive'
import { hasOwnKey } from '../../liber/hasOwnKey'
import { stringifyValuesOfJSON, NULL_VALUE, toJSON } from './utils'

/**
 * IO interface
 */
interface Readable<T> {
  toJSON(): T
}

export function readJSON<T extends Dict>(o: Readable<T>): T {
  return toJSON(o)
}
export function readAttributes<T extends Dict>(
  o: Readable<T>
): { [k in keyof T]: number | boolean | NULL_VALUE | string } {
  return stringifyValuesOfJSON(toJSON(o))
}

/**
 *  Data model
 *  reuse the type definition in @shimo/modoc due to the nature of collaborative app
 *  -- modeling data in terms of a list of operations
 */
//#region common definition
export type AttributeValue = string | number | boolean | null

export type AttributeObject = Optional<{
  [key: string]: AttributeValue | AttributeObject
}>

export interface DeltaOpData<T extends Operation[] = Operation[]> {
  delta: T
}

export interface JSONOpData<T extends Dict = Dict> {
  object: T
}

export interface MapOpData<T extends Operation[] = Operation[]> {
  slide: { [k: string]: T }
}

export type EmptyJSONData = JSONOpData<{}>

export type EmptyMapData = {
  slide: {}
}

export type EmptyDeltaData = 1
export type OperationAction = 'insert' | 'retain' | 'remove'
//#endregion common definition

//#region IOperation

/**
 * operation data type definition
 */
// type OperationData = string | number | IDeltaData<IOperation[]>

export type OperationData =
  | string
  | number
  | DeltaOpData<Operation[]>
  | JSONOpData
  | MapOpData<Operation[]>

export type OperationDataType = 'string' | 'number' | 'delta' | 'json' | 'map'
export function getOperationDataType(data: OperationData): OperationDataType {
  if (typeof data === 'number') {
    return 'number'
  } else if (typeof data === 'string') {
    return 'string'
  } else if (hasOwnKey(data, 'delta')) {
    return 'delta'
  } else if (hasOwnKey(data, 'object')) {
    return 'json'
  } else {
    // hasOwnKey(data, 'slide')
    return 'map'
  }
}
type OperationInfo<
  T extends OperationData,
  Attr extends AttributeObject = AttributeObject,
> = {
  data: T
  attributes: Attr
}

export type InsertOperation<
  T extends OperationData = OperationData,
  Attr extends AttributeObject = AttributeObject,
> = {
  action: 'insert'
} & OperationInfo<T, Attr>

export type RetainOperation<
  T extends OperationData = OperationData,
  Attr extends AttributeObject = AttributeObject,
> = {
  action: 'retain'
} & OperationInfo<T, Attr>

export type RemoveOperation<
  T extends OperationData = OperationData,
  Attr extends AttributeObject = AttributeObject,
> = {
  action: 'remove'
} & OperationInfo<T, Attr>

export type Operation<
  T extends OperationData = OperationData,
  Attr extends AttributeObject = AttributeObject,
> =
  | InsertOperation<T, Attr>
  | RetainOperation<T, Attr>
  | RemoveOperation<T, Attr>

export type InsertOperationWithType<
  Type extends string,
  T extends OperationData = OperationData,
  Atts extends AttributeObject = AttributeObject,
> = InsertOperation<T, Atts & { opType: Type }>
export type RetainOperationWithType<
  Type extends string,
  T extends OperationData = OperationData,
  Atts extends AttributeObject = AttributeObject,
> = RetainOperation<T, Atts & { opType: Type }>
export type RemoveOperationWithType<
  Type extends string,
  T extends OperationData = OperationData,
  Atts extends AttributeObject = AttributeObject,
> = RemoveOperation<T, Atts & { opType: Type }>

export type OperationWithType<
  Type extends string,
  Action extends OperationAction,
  T extends OperationData = OperationData,
> = { action: Action } & OperationInfo<T, AttributeObject & { opType: Type }>

export function lengthOfOperation(op: Operation): number {
  switch (typeof op['data']) {
    case 'string':
      return (op['data'] as string).length
    case 'number':
      return op['data'] === op['data'] ? op['data'] : 1 // eslint-disable-line no-self-compare
    default:
      return 1
  }
}

export function lengthOfOperations(ops: Nullable<Operation[]>): number {
  if (ops == null) return 0

  let l = 0
  for (const op of ops) {
    l += lengthOfOperation(op)
  }

  return l
}
export type OperationComposer = (
  src: Operation[],
  target: Operation[]
) => Operation[]

function makeOperation<D extends OperationData, Attr extends AttributeObject>(
  action: OperationAction,
  data: D,
  inputAttributes?: Attr
): Operation<D, Attr> {
  const result = {
    ['action']: action,
    ['data']: data,
  } as Operation<D, Attr>

  if (inputAttributes != null) {
    result['attributes'] = inputAttributes
  }
  return result
}

function Insert<D extends OperationData, A extends AttributeObject>(
  data: D,
  attributes?: A
): InsertOperation<D, A> {
  return makeOperation('insert', data, attributes) as InsertOperation<D, A>
}
function Retain<D extends OperationData, A extends AttributeObject>(
  data: D,
  attributes?: A
): RetainOperation<D, A> {
  return makeOperation('retain', data, attributes) as RetainOperation<D, A>
}
function Remove<D extends OperationData, A extends AttributeObject>(
  data: D,
  attributes?: A
): RemoveOperation<D, A> {
  return makeOperation('remove', data, attributes) as RemoveOperation<D, A>
}

export const Op = {
  Insert,
  Retain,
  Remove,
}

export const OpData = {
  Delta: <T extends Operation[]>(delta: T): DeltaOpData<T> => ({
    ['delta']: delta,
  }),
  JSON: <T extends Dict>(data: T): JSONOpData<T> => ({
    ['object']: data,
  }),
  Map: <T extends Operation[]>(data: Dict<T>): MapOpData<T> => ({
    ['slide']: data,
  }),
}

export const EmptyOpData: {
  Delta: EmptyDeltaData
  Map: EmptyMapData
  JSON: EmptyJSONData
} = {
  Delta: 1,
  Map: { slide: {} },
  JSON: { object: {} },
}

export function withType<
  Type extends string,
  T extends OperationData,
  Action extends OperationAction,
  OP extends { action: Action } & OperationInfo<T>,
>(op: OP, type: Type) {
  const attrs: AttributeObject = op['attributes'] ? op['attributes'] : {}
  ;(attrs as any)['opType'] = type
  op['attributes'] = attrs as AttributeObject & { opType: Type }
  return op as unknown as { action: Action } & OperationInfo<
    T,
    AttributeObject & { opType: Type }
  >
}

// export function makeOperationMap<T extends Dict<Operation>>(
//   ops: T,
//   opsLayout: Array<keyof T>
// ): Operation[] {
//   const result: Operation[] = []
//   opsLayout.forEach((k) => {
//     result.push(withType(ops[k], '' + k))
//   })

//   return result
// }

//#endregion IOperation
