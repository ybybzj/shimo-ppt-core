import { TextBody } from '../core/SlideElement/TextBody'
import { Operation } from './operation'
import { loadPPTDoc } from './loaders/PPTDoc'
// import { applyPPTDoc } from './appliers/PPTDoc'
import { applyPPTDoc } from './appliers/PPTDoc'
import { IEditorKit } from '../editor/type'
import { Dict, Nullable } from '../../liber/pervasive'
import { useLatest } from '../lib/utils/promise'
import { refreshPresentation } from '../editor/utils/editor'
import { gClipboard } from '../copyPaste/gClipboard'
import { logger } from '../lib/debug/log'
import { startPerfTiming } from '../common/utils'
import { invalidAllMastersThumbnails } from '../ui/editorUI/utils/utils'
import { printModoc, stringToSlice } from '../re/export/modoc'
import { longActions } from '../globals/longActions'
import { EditActionFlag } from '../core/EditActionFlag'

export class PPTYApplier {
  textBodyTextFit: TextBody[]
  editor: IEditorKit
  themeReplaceImagesMap: Dict | null
  afterApplyPromise: (...args: any[]) => Promise<any>
  constructor(editor: IEditorKit) {
    this.textBodyTextFit = []
    this.themeReplaceImagesMap = null
    this.editor = editor
    this.afterApplyPromise = useLatest(() => this.afterApply())
  }

  reset() {
    this.textBodyTextFit = []
  }

  async load<InputData>(data: InputData): Promise<Nullable<Operation[]>> {
    const presentation = this.editor.presentation
    const deltaSlice = stringToSlice((data as any).stringify())
    const pptOps = await loadPPTDoc(
      deltaSlice,
      presentation,
      {
        textBodyTextFit: this.textBodyTextFit,
      },
      true
    )
    this.checkTextFit()
    this.reset()
    return pptOps
  }

  apply(data: Operation[], callback: (appliedData: Operation[]) => void) {
    printModoc(data, 'applying delta ==>')
    const presentation = this.editor.presentation

    const currentOps = this.editor.collaborationManager.currentPptIOData()
    const getApplyIOTiming = startPerfTiming()

    const appliedData = applyPPTDoc(data, currentOps ?? [], presentation, {
      textBodyTextFit: this.textBodyTextFit,
      pptInfoData: this.editor.collaborationManager.currentInfoData(),
    })

    const applyIOTiming = getApplyIOTiming()

    this.editor.trackPerf('IO_APPLY', applyIOTiming)
    logger.log('applyPPTDoc:', applyIOTiming)

    // logger.log(`afterApply delta ==>`, appliedData)
    this.checkTextFit()
    this.applyAsyncAssets(() => callback(appliedData))
  }

  async applyAsyncAssets(
    callback?: () => void,
    isFromPaste?: boolean
  ): Promise<any> {
    if (isFromPaste) {
      longActions.start(EditActionFlag.action_Paste_Assets)
    }
    if (callback) {
      callback()
    }
    const resourcesLoadManager = this.editor.resourcesLoadManager
    await this.editor.imageLoader.loadBase64()

    if (this.editor.pasteCallbackFunc && isFromPaste === true) {
      const currentSourceFileId = this.editor.sourceFileId
      const sourceFileId = gClipboard.copySourceFileId
      if (sourceFileId && sourceFileId !== currentSourceFileId) {
        const assets = Object.keys(resourcesLoadManager._assetsForPasting)
        // console.info(`images for pasteCallback =>`, images)
        // console.info(`assets for pasteCallback =>`, assets)
        // const files = images.concat(assets)
        if (assets.length > 0) {
          await this.editor.pasteCallbackFunc({
            files: assets,
            sourceFileId,
          })
        }
      }
    }
    const [loadFontsResult] = await this.afterApplyPromise()
    resourcesLoadManager.resetApplyCollector()
    if (loadFontsResult !== false) {
      refreshPresentation(this.editor, loadFontsResult)
      invalidAllMastersThumbnails(this.editor.editorUI)
    }
    this.afterApplyPromise = useLatest(() => this.afterApply())
    if (isFromPaste) {
      longActions.end(EditActionFlag.action_Paste_Assets)
    }
  }

  applyWithThemeImageReplacement(callback, imagesMap: Record<string, string>) {
    this.themeReplaceImagesMap = imagesMap
    callback()
    this.themeReplaceImagesMap = null
  }

  afterApply(): Promise<any> {
    const resourcesLoadManager = this.editor.resourcesLoadManager
    const { applyImagesMap, applyFontsMap } = this.editor.resourcesLoadManager
    const images = Object.keys(applyImagesMap)
    const fonts = Object.keys(applyFontsMap)

    const loadFontsPromise =
      fonts && fonts.length > 0
        ? resourcesLoadManager.loadFonts(fonts)
        : Promise.resolve(false)
    const loadImagesPromise =
      images.length > 0
        ? Promise.all(resourcesLoadManager.loadImages(images))
        : Promise.resolve(false)
    const loaders = [loadFontsPromise, loadImagesPromise]
    return Promise.all(loaders)
  }

  private checkTextFit() {
    this.textBodyTextFit.forEach((txBody) => {
      txBody.checkTextFit()
    })
    this.textBodyTextFit.length = 0
  }
}
