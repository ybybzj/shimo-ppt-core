export const NotesMasterDataLayout = ['attrs', 'csld'] as const

export const NotesMasterDataLayoutTypeMap = {
  ['attrs']: 'json',
  ['csld']: 'delta',
} as const
