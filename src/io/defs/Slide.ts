export const SlideDataLayout = ['attrs', 'csld'] as const
export const SlideDataLayoutTypeMap = {
  ['attrs']: 'json',
  ['csld']: 'delta',
} as const
