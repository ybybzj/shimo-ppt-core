export const SlideLayoutDataLayout = ['attrs', 'csld'] as const
export const SlideLayoutDataLayoutTypeMap = {
  ['attrs']: 'json',
  ['csld']: 'delta',
} as const
