export const SlideMasterDataLayout = ['attrs', 'layoutRefs', 'csld'] as const

export const SlideMasterDataLayoutTypeMap = {
  ['attrs']: 'json',
  ['layoutRefs']: 'json',
  ['csld']: 'delta',
} as const
