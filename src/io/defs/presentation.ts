export const PresentationDataLayout = [
  'Pres',
  'PresentationPr',
  'TableStyle',
  'Slides',
  'SlideMaster',
  'Notes',
  'NotesMaster',
] as const

export const PresentationDataLayoutTypeMap = {
  ['Pres']: 'json',
  ['PresentationPr']: 'json',
  ['TableStyle']: 'json',
  ['Slides']: 'json',
  ['SlideMaster']: 'json',
  ['Notes']: 'json',
  ['NotesMaster']: 'json',
} as const
