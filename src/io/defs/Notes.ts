export const NotesDataLayout = ['attrs', 'csld'] as const
export const NotesDataLayoutTypeMap = {
  ['attrs']: 'json',
  ['csld']: 'delta',
} as const
