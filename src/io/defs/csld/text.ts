export const TextBodyDataLayout = ['bodyPr', 'listStyle', 'docContent'] as const
export const TextBodyDataLayoutTypeMap = {
  ['bodyPr']: 'json',
  ['listStyle']: 'json',
  ['docContent']: 'delta',
} as const
