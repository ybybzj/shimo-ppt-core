export const TableDataLayout = ['attrs', 'grid', 'rows'] as const

export const TableDataLayoutMap = {
  ['attrs']: 'json',
  ['grid']: 'json',
  ['rows']: 'delta',
} as const

export const TableRowDataLayout = ['attrs', 'cells'] as const
export const TableRowDataLayoutMap = {
  ['attrs']: 'json',
  ['cells']: 'delta',
} as const

export const TableCellDataLayout = ['attrs', 'txBody'] as const
export const TableCellDataLayoutMap = {
  ['attrs']: 'json',
  ['txBody']: 'delta',
} as const
