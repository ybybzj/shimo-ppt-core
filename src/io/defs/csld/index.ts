export const CSldDataLayout = ['attrs', 'spTree'] as const
export const CSldDataLayoutTypeMap = {
  ['attrs']: 'json',
  ['spTree']: 'map',
} as const
