export const OleDataLayout = ['attrs', 'pic'] as const

export const OleDataLayoutMap = {
  ['attrs']: 'json',
  ['pic']: 'delta',
} as const
