export const GroupShapeDataLayout = ['nvGrpSpPr', 'grpSpPr', 'spTree'] as const
export const GroupShapeDataLayoutTypeMap = {
  ['nvGrpSpPr']: 'json',
  ['grpSpPr']: 'json',
  ['spTree']: 'map',
} as const
