export const GraphicFrameDataLayout = ['attrs', 'graphic'] as const
export const GraphicFrameDataLayoutMap = {
  ['attrs']: 'json',
  ['graphic']: 'delta',
} as const
