export const ImageDataLayout = ['nvPicPr', 'blipFill', 'spPr', 'style'] as const
export const ImageDataLayoutMap = {
  ['nvPicPr']: 'json',
  ['blipFill']: 'json',
  ['spPr']: 'json',
  ['style']: 'json',
} as const
