export const ChartDataLayout = ['attrs', 'pic'] as const

export const ChartDataLayoutMap = {
  ['attrs']: 'json',
  ['pic']: 'delta',
} as const
