export const ConnectionShapeDataLayout = ['nvCxnSpPr', 'spPr', 'style'] as const
export const ConnectionShapeDataLayoutTypeMap = {
  ['nvCxnSpPr']: 'json',
  ['spPr']: 'json',
  ['style']: 'json',
} as const
