export const ShapeDataLayout = [
  'attrs',
  'nvSpPr',
  'spPr',
  'style',
  'txBody',
] as const
export const ShapeDataLayoutTypeMap = {
  ['attrs']: 'json',
  ['nvSpPr']: 'json',
  ['spPr']: 'json',
  ['style']: 'json',
  ['txBody']: 'delta',
} as const
