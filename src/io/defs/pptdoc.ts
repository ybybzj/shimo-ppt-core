import { Presentation } from '../../core/Slide/Presentation'
import { TextBody } from '../../core/SlideElement/TextBody'
import { PPTInfoData } from '../infoData'

export const PPTDocDataLayout = [
  'presentation',
  'layouts',
  'slideMasters',
  'slides',
  'themes',
  'notesMasters',
  'notes',
  'comments',
] as const

export const PPTDocDataLayoutTypeMap = {
  ['presentation']: 'delta',
  ['layouts']: 'map',
  ['slideMasters']: 'map',
  ['slides']: 'map',
  ['themes']: 'map',
  ['notesMasters']: 'map',
  ['notes']: 'map',
  ['comments']: 'map',
} as const

export interface HelperDataForFromJSON {
  textBodyTextFit: TextBody[]
  pptInfoData?: PPTInfoData
  forceSync?: boolean
}

export interface FromJSONParams {
  presentation: Presentation
  helperData?: HelperDataForFromJSON
  ignoreRId?: boolean
}
