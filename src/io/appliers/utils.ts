import { each } from '../../../liber/l/each'
import { Dict, Nullable, valuesOfDict } from '../../../liber/pervasive'
import { MapOperation } from '../../re/export/modoc'
import { DataLayoutOpMap } from '../loaders/utils'
import {
  Operation,
  OperationDataType,
  getOperationDataType,
  lengthOfOperation,
} from '../operation'

export interface ArrIter<T> {
  hasNext: () => boolean
  next: () => undefined | T
  peekNext: () => undefined | T
  reset: () => void
}

export function makeArrIter<T>(arr: T[]): ArrIter<T> {
  let i = 0
  const arrLen = arr.length
  return {
    hasNext: () => {
      return i < arrLen
    },
    next: () => {
      return arr[i++]
    },
    peekNext: () => {
      return arr[i]
    },
    reset: () => {
      i = 0
    },
  }
}

type MapToOpUnion<M extends Record<string, Operation>> = valuesOfDict<{
  [K in keyof M]: {
    opType: K
    op: M[K]
    appliedOp: Nullable<M[K]>
    isEmptyOp: boolean
  }
}>

export type OperationConsumer<
  L extends readonly string[],
  M extends { [key in L[number]]: OperationDataType },
> = (
  arg: MapToOpUnion<DataLayoutOpMap<L, M>>
) => Nullable<AppliedConsumeResult> | void

export type AppliedConsumeResult =
  | 'retain'
  | 'remove'
  | 'update'
  | 'failed'
  | 'break'
export function consumeDeltaOperations<
  L extends readonly string[],
  M extends { [key in L[number]]: OperationDataType },
  // C extends OperationConsumer<L, M>,
>(
  changes: Operation[],
  appliedOps: Nullable<Operation[]>,
  dataLayout: L,
  dataLayoutMap: M,
  consumer: OperationConsumer<L, M>
): AppliedConsumeResult {
  let result: AppliedConsumeResult = 'retain'
  const changesIter = makeArrIter(changes)
  const appliedOpsIter = makeArrIter(appliedOps ?? [])
  const dataLayoutLen = dataLayout.length

  let index = 0

  while (result !== 'break' && changesIter.hasNext() && index < dataLayoutLen) {
    const op = changesIter.next()!
    const opLen = lengthOfOperation(op)
    const opDataType = getOperationDataType(op['data'])

    if (op['action'] === 'remove') {
      // 固定layout的数据结构整体remove
      if (index === 0 && opLen >= dataLayoutLen) {
        result = 'remove'
        break
      } else if (opLen >= dataLayoutLen - index) {
        // 不处理后续全是删除的情况
        break
      } else {
        // 跳过删除op
        continue
      }
    }

    let appliedOpIndex = 0
    // 固定layout的数据结构中的每个item的data类型一定是json/delta/map这三种, 才是被认为是有内容的
    // 另外，insert的op的data类型是number， 且data === 1，被认为是空op，需要经过结下来的处理逻辑
    if (
      (opDataType === 'number' && (opLen > 1 || op.action === 'retain')) ||
      opDataType === 'string'
    ) {
      index += opLen
      if (appliedOps != null) {
        // 由于存在 action: 'insert', data: 2} 的 oldOp, iter 的次数实际小于 length
        while (
          appliedOps &&
          appliedOpsIter.hasNext() &&
          appliedOpIndex < opLen
        ) {
          const appliedOp = appliedOpsIter.next()!
          const appliedOplength = lengthOfOperation(appliedOp)
          appliedOpIndex += appliedOplength
        }
      }
      continue
    }

    const opType: L[number] = dataLayout[index++]
    const expectedDataType: OperationDataType = dataLayoutMap[opType]
    const isEmptyOp =
      opDataType === 'number' && opLen === 1 && op.action === 'insert'
    if (
      (expectedDataType === opDataType || isEmptyOp) &&
      (!op['attributes'] ||
        !op['attributes'].opType ||
        op['attributes'].opType === opType)
    ) {
      let appliedOp
      if (appliedOps != null) {
        appliedOp = appliedOpsIter.next()
      }
      result =
        consumer({
          opType,
          op: op as DataLayoutOpMap<L, M>[L[number]],
          appliedOp: appliedOp as Nullable<DataLayoutOpMap<L, M>[L[number]]>,
          isEmptyOp,
        }) ?? 'update'
    } else {
      console.warn(
        `[consumeDeltaOperations]invalid change op with opType "${op['attributes'].opType}" and dataType "${opDataType}", expect "${opType}" and "${expectedDataType}"`
      )
      return 'failed'
    }
  }
  return result
}

export function handleMapDataApplying<Item>(
  op: MapOperation,
  refs: Nullable<string[]>,
  currentItems: Dict<Item>,
  appliedOp: Nullable<MapOperation>,
  genItem: (rId: string, data: Operation[]) => void,
  applyItem?: (
    item: Item,
    data: Operation[],
    appliedData: Operation[],
    rId: string
  ) => void
) {
  const opData = op.data['slide']
  if (op.action === 'insert') {
    refs = refs ?? Object.keys(opData)
    if (refs.length > 0) {
      refs.forEach((rId) => {
        const data = opData[rId]
        if (data) {
          genItem(rId, data)
        }
      })
    }
  } else if (op.action === 'retain') {
    const appliedData = appliedOp?.data['slide'] || {}
    each((itemData, rId) => {
      const appliedItemData = appliedData[rId]
      const item = currentItems[rId]
      if (item && applyItem) {
        applyItem(item, itemData, appliedItemData, rId)
      } else {
        genItem(rId, itemData)
      }
    }, op.data['slide'])
  }
}

export function getOperationByOpType<
  L extends readonly string[],
  M extends { [key in L[number]]: OperationDataType },
  T extends L[number],
>(
  ops: Operation[],
  opType: T,
  dataLayout: L,
  dataTypeLayoutMap: M
): Nullable<DataLayoutOpMap<L, M>[T]> {
  const iter = makeArrIter(ops)
  let ret: Nullable<DataLayoutOpMap<L, M>[T]>
  let i = 0
  while (iter.hasNext() && i < dataLayout.length) {
    const op = iter.next()!
    // Todo, check op
    const curOpType = op.attributes && op.attributes['opType']
    if (curOpType === opType) {
      const expectedDataType = dataTypeLayoutMap[opType]
      const dataType = getOperationDataType(op.data)
      if (expectedDataType === dataType) {
        ret = op as DataLayoutOpMap<L, M>[T]
      }
      break
    }
    i += lengthOfOperation(op)
  }
  return ret
}
