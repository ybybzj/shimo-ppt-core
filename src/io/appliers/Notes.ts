import { Notes } from '../../core/Slide/Notes'
import { NotesDataLayout, NotesDataLayoutTypeMap } from '../defs/Notes'

import { Dict, Nullable } from '../../../liber/pervasive'
import {
  FromJSONParams,
  PPTDocDataLayout,
  PPTDocDataLayoutTypeMap,
} from '../defs/pptdoc'
import { applyCSld } from './csld'
import { fromClrMapOverride } from '../../core/color/clrMap'
import { MapOperation } from '../../re/export/modoc'
import {
  consumeDeltaOperations,
  getOperationByOpType,
  handleMapDataApplying,
} from './utils'
import { Operation } from '../operation'
export function applyNotes(
  op: MapOperation,
  appliedOp: Nullable<MapOperation>,
  notes: Dict<Notes>,
  params: FromJSONParams,
  cb?: (rId: string, newNotes: Notes) => void,
  notesToMasterMap?: Dict<string>
) {
  handleMapDataApplying(
    op,
    null,
    notes,
    appliedOp,
    (rId, data) => {
      const newNotes = genSlideNote(data, params, rId, ({ notesMasterRef }) => {
        if (notesToMasterMap) {
          notesToMasterMap[rId] = notesMasterRef
        }
      })
      if (cb) {
        cb(rId, newNotes)
      }
    },
    (notesItem, data, oldData, rId) => {
      applySlideNotes(
        data,
        oldData,
        notesItem,
        params,
        rId,
        ({ notesMasterRef }) => {
          if (notesToMasterMap) {
            notesToMasterMap[rId] = notesMasterRef
            if (cb) {
              cb(rId, notesItem)
            }
          }
        }
      )
    }
  )
}

function applySlideNotes(
  data: Operation[],
  appliedData: Nullable<Operation[]>,
  notes: Notes,
  params: FromJSONParams,
  rId?: Nullable<string>,
  cb?: (arg: { notesMasterRef: string }) => void
) {
  if (rId && params.ignoreRId !== true) {
    notes.setRefId(rId)
  }

  return consumeDeltaOperations(
    data,
    appliedData,
    NotesDataLayout,
    NotesDataLayoutTypeMap,
    ({ opType: type, op, appliedOp }) => {
      if (type === 'attrs') {
        const opData = op.data['object'] ?? {}
        const showMasterPhAnim = opData['showMasterPhAnim']

        const showMasterSp = opData['showMasterSp']

        const clrMapData = opData['clrMap']

        const notesMasterRef = opData['notesMasterRef']

        if (showMasterPhAnim != null) {
          notes.setShowMasterPhAnim(showMasterPhAnim)
        }

        if (showMasterSp != null) {
          notes.setShowMasterSp(showMasterSp)
        }

        const clrMap = fromClrMapOverride(clrMapData)
        if (clrMap) {
          notes.setClrMapOverride(clrMap)
        }

        // establish ref links
        if (notesMasterRef != null && typeof cb === 'function') {
          cb({ notesMasterRef })
        }
      } else if (type === 'csld') {
        const opData = op.data['delta']

        if (opData) {
          applyCSld(opData, appliedOp?.data['delta'], notes.cSld, notes, params)
        }
      }
    }
  )
}

function genSlideNote(
  data: Operation[],
  params: FromJSONParams,
  rId?: Nullable<string>,
  cb?: (arg: { notesMasterRef: string }) => void
): Notes {
  const notes = new Notes()
  applySlideNotes(data, null, notes, params, rId, cb)
  return notes
}

export function genSlideNoteFromInputData(
  inputData: Operation[],
  rid: string,
  params: FromJSONParams
): Nullable<{ note: Notes; notesMasterRef: Nullable<string> }> {
  const notesOp = getOperationByOpType(
    inputData,
    'notes',
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap
  )
  if (notesOp) {
    const data = notesOp.data['slide'][rid]
    if (data == null) {
      return null
    }
    const notesMasterRefContent: {
      notesMasterRef?: string
    } = {}
    const note = genSlideNote(
      data,
      params,
      rid,
      ({ notesMasterRef }) =>
        (notesMasterRefContent.notesMasterRef = notesMasterRef)
    )

    return {
      note,
      notesMasterRef: notesMasterRefContent.notesMasterRef,
    }
  }
  return null
}
