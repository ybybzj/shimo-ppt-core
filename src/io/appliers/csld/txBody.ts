import { isFiniteNumber } from '../../../common/utils'
import { ParaHyperlink } from '../../../core/Paragraph/ParaContent/ParaHyperlink'
import { ParaRun } from '../../../core/Paragraph/ParaContent/ParaRun'
import { PresentationField } from '../../../core/Paragraph/ParaContent/PresentationField'
import { Paragraph } from '../../../core/Paragraph/Paragraph'
import { ParaNewLine } from '../../../core/Paragraph/RunElement/ParaNewLine'
import { TextBody } from '../../../core/SlideElement/TextBody'
import { BodyPr } from '../../../core/SlideElement/attrs/bodyPr'
import { TextDocument } from '../../../core/TextDocument/TextDocument'
import { InstanceType } from '../../../core/instanceTypes'
import { ParaPr } from '../../../core/textAttributes/ParaPr'
import { TextListStyle } from '../../../core/textAttributes/TextListStyle'
import { TextPr } from '../../../core/textAttributes/TextPr'
import { cloneObj } from '../../../re/io/ModocModel'
import {
  TextBodyDataLayout,
  TextBodyDataLayoutTypeMap,
} from '../../defs/csld/text'
import { FromJSONParams } from '../../defs/pptdoc'
import {
  Operation,
  OperationDataType,
  getOperationDataType,
  lengthOfOperation,
} from '../../operation'
import {
  StringifiedJSON,
  fromJSON,
  parseStringifiedJSON,
  stripInvalidXMLCharacters,
} from '../../utils'
import { consumeDeltaOperations, makeArrIter } from '../utils'

export function applyTextBody(
  data: Operation[],
  textBody: TextBody,
  { helperData }: FromJSONParams
): void {
  consumeDeltaOperations(
    data,
    null,
    TextBodyDataLayout,
    TextBodyDataLayoutTypeMap,
    ({ opType: type, op }) => {
      // only handle insert op
      if (op.action !== 'insert') {
        return
      }

      switch (type) {
        case 'bodyPr': {
          const opData = op.data['object']
          const bodyPr = new BodyPr()
          fromJSON(bodyPr, opData || {})
          textBody.setBodyPr(bodyPr)
          if (textBody.bodyPr && textBody.bodyPr.textFit) {
            if (helperData) {
              helperData.textBodyTextFit.push(textBody)
            }
          }

          break
        }
        case 'listStyle': {
          const opData = op.data['object']
          const styles = new TextListStyle()
          const listStyle = opData['listStyle'] ?? opData
          if (Array.isArray(listStyle)) {
            fromJSON(styles, listStyle)
          }
          textBody.setTextListStyle(styles)

          break
        }
        case 'docContent': {
          const opData = op.data['delta']
          if (opData) {
            const textDoc = TextDocument.new(textBody, 0, 0, 0, 0)
            applyTextDocument(opData, textDoc)

            textBody.setContent(textDoc)
          }
          break
        }
      }
    }
  )
}

export function applyTextDocument(ops: Operation[], textDoc: TextDocument) {
  const result = applyParagraphs(ops, textDoc)
  if (result.length > 0) {
    textDoc.children.clear()
  }

  result.forEach((paragraph) => {
    textDoc.insertParagraph(textDoc.children.length, paragraph)
  })
}

export function applyParagraphs(ops: Operation[], textDoc?: TextDocument) {
  const result: Paragraph[] = []

  let paragraph = Paragraph.new(textDoc)

  let index = 0

  const iter = makeArrIter(ops)

  while (iter.hasNext()) {
    const op = iter.next()!

    if (!isSupportedOp(op)) {
      continue
    }
    const attributes = cloneObj(op.attributes || {})
    const dataType = getOperationDataType(op.data)
    // 暂时只处理insert
    const attrs = parseStringifiedJSON(attributes as StringifiedJSON)
    if (dataType === 'number' && attrs['paraEnd']) {
      const rPr = attrs['rPr']
      const pPr = attrs['pPr']
      const textPr = attrs['textPr']

      Array.from({ length: lengthOfOperation(op) }, () => {
        if (pPr) {
          fromJSON(paragraph.pr, pPr)
          paragraph.setPr(paragraph.pr)
        }

        if (textPr) {
          fromJSON(paragraph.paraTextPr.textPr, textPr)
          paragraph.paraTextPr.fromTextPrData(paragraph.paraTextPr.textPr)
        }

        if (rPr) {
          const runPrOfEndParaRun = TextPr.new()
          fromJSON(runPrOfEndParaRun, rPr)
          if (
            runPrOfEndParaRun.fillEffects &&
            !runPrOfEndParaRun.fillEffects.fill
          ) {
            runPrOfEndParaRun.fillEffects = undefined
          }

          paragraph.paraTextPr.fromTextPrData(runPrOfEndParaRun) // endRunProperties

          const endParaRun = paragraph.children.at(
            paragraph.children.length - 1
          )
          if (endParaRun && endParaRun.instanceType === InstanceType.ParaRun) {
            endParaRun.pr = runPrOfEndParaRun
          }
        }

        result.push(paragraph)
        paragraph = Paragraph.new(textDoc)
      })
      index = 0
    } else if (dataType === 'number' && attrs['paraField'] === true) {
      index = applyParaFieldContent(op, paragraph, index)
    } else {
      applyParaRunContent(op, dataType, paragraph, index++)
    }
  }

  return result
}

function applyParaRunContent(
  op: Operation,
  dataType: OperationDataType,
  paragraph: Paragraph,
  index: number
) {
  const attributes = cloneObj(op.attributes || {})
  const attrs = parseStringifiedJSON(attributes as StringifiedJSON)
  const isHyperLink =
    null != attrs['hlinkClick'] || null != attrs['hlinkMouseOver']

  const rPr = TextPr.new()
  fromJSON(rPr, isHyperLink ? { ...attrs, ['underline']: 'sng' } : attrs)
  let hyperlink: ParaHyperlink | undefined
  if (isHyperLink) {
    hyperlink = ParaHyperlink.new(paragraph)
    if (rPr.hlink != null) {
      hyperlink.fromCTHyperlink(rPr.hlink)
    }
  }

  const newRun = ParaRun.new(paragraph, rPr)
  TextPr.recycle(rPr)

  if (dataType === 'string') {
    const text = stripInvalidXMLCharacters(op.data as string)
    newRun.insertText(text)
  } else if (attrs['break'] === true) {
    const breakNum = isFiniteNumber(op.data) ? (op.data as number) : 1
    for (let i = 0; i < breakNum; i++) {
      newRun.addItemAt(0, ParaNewLine.new())
    }
  }

  if (hyperlink != null) {
    hyperlink.addItemAt(0, newRun, false)
    paragraph.addItemAt(index, hyperlink)
  } else {
    paragraph.addItemAt(index, newRun)
  }
}

function applyParaFieldContent(
  op: Operation,
  paragraph: Paragraph,
  index: number
): number {
  const attributes = cloneObj(op.attributes || {})
  const attrs = parseStringifiedJSON(attributes as StringifiedJSON)
  const fld = new PresentationField(paragraph)
  const { id, type, text } = attrs

  if (id != null) {
    fld.setGuid(id)
  }
  if (type != null) {
    fld.setFieldType(type)
  }
  if (text != null) {
    fld.insertText(text)
  }

  if (attrs['rPr']) {
    const rPr = TextPr.new()
    fromJSON(rPr, attrs['rPr'])
    fld.fromTextPr(rPr)
  }

  if (attrs['pPr']) {
    const pPr = ParaPr.new()
    fromJSON(pPr, attrs['pPr'])
    fld.setParaPr(pPr)
  }

  paragraph.addItemAt(index++, ParaRun.new(paragraph))
  paragraph.addItemAt(index++, fld)
  paragraph.addItemAt(index++, ParaRun.new(paragraph))

  return index
}

// helpers
function isSupportedOp(op: Operation): boolean {
  const dataType = getOperationDataType(op.data)
  return (
    lengthOfOperation(op) > 0 &&
    (dataType === 'string' ||
      dataType === 'delta' ||
      (dataType === 'number' &&
        !!op.attributes &&
        (op.attributes.paraEnd === true ||
          op.attributes.break === true ||
          op.attributes.paraField === true)))
  )
}
