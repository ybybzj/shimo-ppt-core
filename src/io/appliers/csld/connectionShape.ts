import { Operation } from '../../operation'
import { FromJSONParams } from '../../defs/pptdoc'
import { Nullable } from '../../../../liber/pervasive'
import { readFromJSON } from '../../utils'
import { UniNvPr, SpPr } from '../../../core/SlideElement/attrs/shapePrs'
import { isNumber } from '../../../common/utils'
import { ShapeStyle } from '../../../core/SlideElement/attrs/ShapeStyle'
import { SpTypes } from '../../dataType/spAttrs'
import { ConnectionShape } from '../../../core/SlideElement/ConnectionShape'
import {
  ConnectionShapeDataLayout,
  ConnectionShapeDataLayoutTypeMap,
} from '../../defs/csld/connectionShape'
import { collectCxnSpToSpMapState } from '../../../core/utilities/shape/manageCxnSpState'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import {
  setDeletedForSp,
  setParentForSlideElement,
} from '../../../core/utilities/shape/updates'
import { CSld } from '../../../core/Slide/CSld'
import { setLocks } from '../../../core/utilities/shape/locks'
import { consumeDeltaOperations } from '../utils'

export function applyConnectionShape(
  data: Operation[],
  shape: ConnectionShape,
  rId: Nullable<string>,
  params: FromJSONParams
) {
  if (rId != null && params.ignoreRId !== true) {
    shape.setRefId(rId)
  }
  consumeDeltaOperations(
    data,
    null,
    ConnectionShapeDataLayout,
    ConnectionShapeDataLayoutTypeMap,
    ({ opType, op }) => {
      switch (opType) {
        case 'nvCxnSpPr': {
          const opData = op.data['object']
          const nvCxnSpPrData = opData['nvSpPr'] || {}
          const pr = readFromJSON(
            nvCxnSpPrData,
            UniNvPr,
            [shape],
            [SpTypes.ConnectionShape]
          )
          if (pr) {
            shape.setNvSpPr(pr)
            if (isNumber(pr.nvUniSpPr?.locks)) {
              setLocks(shape, pr.nvUniSpPr!.locks)
            }
            collectCxnSpToSpMapState(shape)
          }

          break
        }
        case 'spPr': {
          const opData = op.data['object']
          const spPrData = opData['spPr']
          const spPr = readFromJSON(spPrData, SpPr)
          if (spPr) {
            shape.setSpPr(spPr)
            spPr.setParent(shape)
          }
          break
        }
        case 'style': {
          const opData = op.data['object']
          const style = readFromJSON(opData, ShapeStyle)
          if (style) {
            shape.setStyle(style)
          }

          break
        }
      }
    }
  )
}

export function genConnectionShape(
  data: Operation[],
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>,
  rId?: Nullable<string>
): ConnectionShape {
  const shape = new ConnectionShape()
  shape.spTreeParent = spTreeParent
  setDeletedForSp(shape, false)
  if (mainObject) {
    setParentForSlideElement(shape, mainObject)
  }
  applyConnectionShape(data, shape, rId, params)
  return shape
}
