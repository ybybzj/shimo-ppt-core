import { Nullable } from '../../../../liber/pervasive'
import { CSld } from '../../../core/Slide/CSld'
import { ShapeLocks } from '../../../core/SlideElement/const'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { ImageShape } from '../../../core/SlideElement/Image'
import { OleObject } from '../../../core/SlideElement/OleObject'
import { setLock } from '../../../core/utilities/shape/locks'
import { setDeletedForSp } from '../../../core/utilities/shape/updates'
import { OLEObjectAttrs } from '../../dataType/csld'
import { OleDataLayout, OleDataLayoutMap } from '../../defs/csld/oleObject'
import { FromJSONParams } from '../../defs/pptdoc'
import { Operation } from '../../operation'
import { consumeDeltaOperations } from '../utils'
import { applyImage } from './image'

export function applyOle(
  data: Operation[],
  oleObject: OleObject,
  rId: Nullable<string>,
  params: FromJSONParams
) {
  if (rId != null && params.ignoreRId !== true) {
    oleObject.setRefId(rId)
  }

  consumeDeltaOperations(
    data,
    null,
    OleDataLayout,
    OleDataLayoutMap,
    ({ opType, op }) => {
      switch (opType) {
        case 'attrs': {
          const opData = op.data['object']
          const attrsData = opData as OLEObjectAttrs
          oleObject.attrsFromJSON(attrsData)
          break
        }
        case 'pic': {
          const opData = op.data['delta']
          applyImage(opData, oleObject, null, params)

          break
        }
      }
    }
  )
}

export function genOle(
  data: Operation[],
  rId: Nullable<string>,
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>
): ImageShape {
  const oleObject = new OleObject()
  oleObject.spTreeParent = spTreeParent
  setLock(oleObject, ShapeLocks.noModifyAspect, true)
  setDeletedForSp(oleObject, false)
  if (mainObject) {
    oleObject.setParent(mainObject)
  }
  applyOle(data, oleObject, rId, params)
  if (mainObject?.cSld?.checkUniNvPrForSps) {
    mainObject.cSld.checkUniNvPrForSps([oleObject])
  }
  return oleObject
}
