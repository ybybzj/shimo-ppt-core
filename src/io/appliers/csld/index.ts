import { Operation } from '../../operation'
import { CSld } from '../../../core/Slide/CSld'
import { FromJSONParams } from '../../defs/pptdoc'
import { CSldDataLayout, CSldDataLayoutTypeMap } from '../../defs/csld'
import { Dict, Nullable } from '../../../../liber/pervasive'
import { readFromJSON } from '../../utils'
import { Bg } from '../../../core/SlideElement/attrs/bg'

import { SlideElement } from '../../../core/SlideElement/type'
import { Presentation } from '../../../core/Slide/Presentation'
import { hasOwnKey } from '../../../../liber/hasOwnKey'
import { isHiddenSp, applySpTree } from './groupShape'
import { EditorUtil } from '../../../globals/editor'
import {
  clearCxnState,
  applyCxnSpToSpMapState,
  collectCxnStateForSp,
} from '../../../core/utilities/shape/manageCxnSpState'
import { setDeletedForSp } from '../../../core/utilities/shape/updates'
import { InstanceType, isInstanceTypeOf } from '../../../core/instanceTypes'
import { Slide } from '../../../core/Slide/Slide'
import { deleteSlideElementfromParent } from '../../../core/utilities/shape/delete'
import { updateCalcStatusForSlide } from '../../../core/calculation/updateCalcStatus/slide'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { consumeDeltaOperations } from '../utils'

export function applyCSld(
  data: Operation[],
  appliedData: Nullable<Operation[]>,
  csld: CSld,
  mainObject: Nullable<any>,
  params: FromJSONParams
): { needRerender: boolean; updatedSpRefs?: Dict<string> } {
  clearCxnState()
  let needRerender = false
  let updatedSpRefs

  consumeDeltaOperations(
    data,
    appliedData,
    CSldDataLayout,
    CSldDataLayoutTypeMap,
    ({ opType, op, appliedOp }) => {
      switch (opType) {
        case 'attrs': {
          const opData = op.data['object']
          if (hasOwnKey(opData, 'name')) {
            csld.name = opData['name']
          }

          if (hasOwnKey(opData, 'bg')) {
            // has key means need to update
            const bg = readFromJSON(opData['bg'], Bg)
            csld.bg = bg
            if (isInstanceTypeOf(csld.parent, InstanceType.Slide)) {
              updateCalcStatusForSlide(
                csld.parent as Slide,
                EditActionFlag.edit_Slide_SetBg
              )
            }
          }

          break
        }
        case 'spTree': {
          const updateSp = (sp: SlideElement, rid: Nullable<string>) => {
            if (sp && !isHiddenSp(sp)) {
              sp.spTreeParent = csld
            }
            if (params.ignoreRId === true && rid != null) {
              updatedSpRefs = updatedSpRefs ?? {}
              updatedSpRefs[rid] = sp.getRefId()
            }
          }

          // 更新现有连接线
          csld.spTree.forEach((sp) => {
            collectCxnStateForSp(sp)
          })

          needRerender = applySpTree(
            csld,
            op,
            appliedOp,
            mainObject,
            params,
            updateSp,
            removeSp
          )

          // 替换 cxnSp 中 stCxnId、endCxnId 为 oo 中的 id
          applyCxnSpToSpMapState()

          break
        }
      }
    }
  )

  return { needRerender: needRerender, updatedSpRefs }
}

export function removeSp(sp: SlideElement, presentation: Presentation) {
  presentation.selectionState.deselect(sp)
  deleteSlideElementfromParent(sp, true)

  setDeletedForSp(sp, true)

  EditorUtil.entityRegistry.removeEntityById(sp.id)
}
