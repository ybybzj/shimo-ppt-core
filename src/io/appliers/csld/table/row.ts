import { Operation, getOperationDataType } from '../../../operation'
import { TableRow } from '../../../../core/Table/TableRow'
import {
  TableRowDataLayout,
  TableRowDataLayoutMap,
} from '../../../defs/csld/table'
import { applyTableCell } from './cell'
import { Nullable } from '../../../../../liber/pervasive'
// import { idGenerator } from '../../../../globals/IdGenerator'
import { LineHeightRule } from '../../../../core/common/const/attrs'
import { ScaleOfPPTXSizes } from '../../../../core/common/const/unit'
import { isNumber } from '../../../../common/utils'
import { consumeDeltaOperations } from '../../utils'

export function applyTableRow(data: Operation[], row: TableRow) {
  let rowHeight
  consumeDeltaOperations(
    data,
    null,
    TableRowDataLayout,
    TableRowDataLayoutMap,
    ({ opType, op }) => {
      switch (opType) {
        case 'attrs': {
          const opData = op.data['object']
          rowHeight = opData['height'] / ScaleOfPPTXSizes
          // insert 时不需要重复计算
          if (isNumber(rowHeight) && op.action === 'retain') {
            setRowHeight(row, rowHeight)
          }

          break
        }
        case 'cells':
          {
            const cellsOpData = op.data['delta']
            let cellIndex = 0
            let hasInert = false
            for (const cellOp of cellsOpData) {
              const cellOpDataType = getOperationDataType(cellOp.data)
              switch (cellOp.action) {
                case 'insert': {
                  hasInert = true
                  const attributes = cellOp.attributes || {}
                  if (cellOpDataType === 'number' && attributes['isEmpty']) {
                    // skip empty cell
                    break
                  }

                  if (cellOpDataType === 'delta') {
                    const cell = row.addCell(cellIndex)
                    applyTableCell(cellOp.data['delta'], cell)
                    cellIndex += 1
                  }
                  break
                }
                case 'retain': {
                  if (cellOpDataType === 'number') {
                    cellIndex += cellOp.data as number
                    break
                  }

                  if (cellOpDataType === 'delta') {
                    const cell = row.getCellByStartGridColIndex(cellIndex)
                    cell && applyTableCell(cellOp.data['delta'], cell)
                    cellIndex += 1
                  }
                  break
                }
              }
            }

            if (hasInert) {
              row.updateCellIndices(0)
              setRowHeight(row, rowHeight)
            }
          }
          break
      }
    }
  )
}

function setRowHeight(row: TableRow, height: Nullable<number>) {
  let maxTopMargin = 0,
    maxBottomMargin = 0,
    maxTopBorder = 0,
    maxBottomBorder = 0

  const rowHeight = height ?? 5

  for (let i = 0, l = row.children.length; i < l; ++i) {
    const cell = row.children.at(i)!
    const margins = cell.getMargins()
    if (margins.bottom!.w > maxBottomMargin) {
      maxBottomMargin = margins.bottom!.w
    }
    if (margins.top!.w > maxTopMargin) {
      maxTopMargin = margins.top!.w
    }
    const borders = cell.getBorders()
    if (borders.top.size! > maxTopBorder) {
      maxTopBorder = borders.top.size!
    }
    if (borders.bottom.size! > maxBottomBorder) {
      maxBottomBorder = borders.bottom.size!
    }
    // apply 过后需要重新触发一次 tabelCell 的 recalc
    cell.invalidateCalcedPrs(false)
  }
  row.setHeight({
    value: Math.max(
      1,
      rowHeight -
        maxTopMargin -
        maxBottomMargin -
        maxTopBorder / 2 -
        maxBottomBorder / 2
    ),
    heightRule: LineHeightRule.AtLeast,
  })
}
