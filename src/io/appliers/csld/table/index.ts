import { Table } from '../../../../core/Table/Table'
import { Operation, getOperationDataType } from '../../../operation'
import { TableDataLayout, TableDataLayoutMap } from '../../../defs/csld/table'
import { FromJSONParams } from '../../../defs/pptdoc'
import { TableLookData } from '../../../dataType/table'

import { Nullable } from '../../../../../liber/pervasive'
import { readFromJSON, decodeNullableValue } from '../../../utils'
import { TablePr } from '../../../../core/Table/attrs/TablePr'
import { TableLook } from '../../../../core/Table/common'
import { ScaleOfPPTXSizes } from '../../../../core/common/const/unit'
import { applyTableRow } from './row'
import { TableStyleData } from '../../../dataType/style'
import { CFillEffectsData } from '../../../dataType/spAttrs'
import { CShd } from '../../../../core/Table/attrs/CShd'
import { ShdType } from '../../../../core/common/const/attrs'
import { gGlobalTableStyles } from '../../../../core/Slide/GlobalTableStyles'
import { FillEffects } from '../../../../core/SlideElement/attrs/fill/fill'
import { TableStyle } from '../../../../core/Table/attrs/TableStyle'
import { consumeDeltaOperations } from '../../utils'

export function applyTable(
  table: Table,
  data: Operation[],
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  { presentation: _ }: FromJSONParams
): Table {
  consumeDeltaOperations(
    data,
    null,
    TableDataLayout,
    TableDataLayoutMap,
    ({ opType, op }) => {
      switch (opType) {
        case 'attrs': {
          const opData = op.data['object']
          const dataOfStyle = opData['style'] as TableStyleData | undefined
          const unifillData = opData['fill'] as CFillEffectsData
          const Look = opData['look'] as TableLookData

          const styleName = decodeNullableValue(
            dataOfStyle && dataOfStyle['name']
          )
          const styleId = dataOfStyle && dataOfStyle['id']
          const style = getTableStyle(styleId, styleName)
          if (style) {
            if (styleId != null) {
              style.guid = styleId
            }
            const noClearFormatting = op.action === 'retain'
            table.setTableStyleId(style.id, noClearFormatting)
          }

          const tablePr = table.pr ?? TablePr.new()
          const unifill = readFromJSON(unifillData, FillEffects)

          if (unifill) {
            tablePr.shd = CShd.new({
              value: ShdType.Clear,
              fillEffects: unifill,
            })
            table.setPr(tablePr)
          }

          const tableLook = readFromJSON(Look, TableLook)
          if (tableLook) {
            table.setTableLook(tableLook)
          }

          break
        }
        case 'grid': {
          const opData = op.data['object']
          if (Array.isArray(opData['grid'])) {
            const grid = opData['grid'].map((n) => n / ScaleOfPPTXSizes)
            table.setTableGrid(grid)
          }
          break
        }
        case 'rows': {
          const rowsData = op.data['delta']
          let rowIndex = 0
          let isInsert = false
          for (let index = 0, l = rowsData.length; index < l; index++) {
            const rowOp = rowsData[index]
            const rowOpDataType = getOperationDataType(rowOp.data)
            switch (rowOp.action) {
              case 'insert': {
                if (rowOpDataType === 'delta') {
                  const row = table.addRow(index, 0)
                  applyTableRow(rowOp.data['delta'], row)
                  isInsert = true
                }

                break
              }
              case 'retain': {
                if (rowOpDataType === 'number') {
                  rowIndex += rowOp.data as number
                }
                if (rowOpDataType === 'delta') {
                  const row = table.getRow(rowIndex)
                  if (row) {
                    applyTableRow(rowOp.data['delta'], row)
                  }
                  rowIndex += 1
                }
                break
              }
            }
          }

          if (isInsert) {
            table.updateIndices()
            if (
              table.children.length > 0 &&
              table.children.at(0)!.cellsCount > 0
            ) {
              table.curCell = table.children.at(0)!.getCellByIndex(0)
            }
          }
        }
      }
    }
  )

  return table
}

//helper
function getTableStyle(
  styleGUID: Nullable<string>,
  styleName: Nullable<string>
): Nullable<TableStyle> {
  if (!styleName && !styleGUID) {
    return undefined
  }
  let result: Nullable<TableStyle>
  if (styleGUID != null) {
    const sid = gGlobalTableStyles.tableStyles.getIdByGuid(styleGUID)
    result = sid ? gGlobalTableStyles.tableStyles.Get(sid) : undefined
  }

  if (result == null && styleName != null) {
    const sid = gGlobalTableStyles.tableStyles.getIdByName(styleName)
    result = sid ? gGlobalTableStyles.tableStyles.Get(sid) : undefined
  }

  return result
}
