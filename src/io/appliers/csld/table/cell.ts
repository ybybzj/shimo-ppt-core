import { Operation } from '../../../operation'
import { TableCell } from '../../../../core/Table/TableCell'
import {
  TableCellDataLayout,
  TableCellDataLayoutMap,
} from '../../../defs/csld/table'
import { readFromJSON } from '../../../utils'
import { TableCellPr } from '../../../../core/Table/attrs/TableCellPr'
import {
  TextBodyDataLayout,
  TextBodyDataLayoutTypeMap,
} from '../../../defs/csld/text'
import { CTableCellPrData } from '../../../dataType/style'
import { VMergeType } from '../../../../core/common/const/table'
import { consumeDeltaOperations } from '../../utils'
import { applyTextDocument } from '../txBody'

export function applyTableCell(data: Operation[], cell: TableCell) {
  consumeDeltaOperations(
    data,
    null,
    TableCellDataLayout,
    TableCellDataLayoutMap,
    ({ opType, op, isEmptyOp }) => {
      switch (opType) {
        case 'attrs': {
          const opData = op.data['object']
          const rowSpan = opData['rowSpan'] as number
          const gridSpan = opData['gridSpan'] as number
          const vMerge = opData['vMerge'] as boolean
          const pr = opData['pr'] as CTableCellPrData
          if (pr === null) {
            cell.setPr(TableCellPr.new())
          } else {
            const cellPr = readFromJSON(pr, TableCellPr)
            if (cellPr) {
              cell.setPr(cellPr)
            }
          }

          // 如果是合并单元格操作则一定会全量替换
          if (rowSpan && rowSpan > 1) {
            cell.setVMerge(VMergeType.Restart)
          }

          if (gridSpan && gridSpan > 1) {
            cell.setGridSpan(gridSpan)
          } else {
            cell.setGridSpan(1)
          }

          if (vMerge === true && cell.pr.vMerge !== VMergeType.Restart) {
            cell.setVMerge(VMergeType.Continue)
          }

          break
        }
        case 'txBody': {
          if (isEmptyOp) {
            cell.textDoc.clearContent()
            break
          }
          const opData = op.data['delta']
          // only handle docContent
          consumeDeltaOperations(
            opData,
            null,
            TextBodyDataLayout,
            TextBodyDataLayoutTypeMap,
            ({ opType, op }) => {
              switch (opType) {
                case 'bodyPr':
                case 'listStyle':
                  break
                case 'docContent': {
                  applyTextDocument(op.data['delta'], cell.textDoc)

                  break
                }
              }
            }
          )

          break
        }
      }
    }
  )
}
