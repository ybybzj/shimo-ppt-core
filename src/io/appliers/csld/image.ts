import { ImageShape } from '../../../core/SlideElement/Image'
import { Operation } from '../../operation'
import { Nullable } from '../../../../liber/pervasive'
import { ImageDataLayout, ImageDataLayoutMap } from '../../defs/csld/image'
import {
  UniNvPrData,
  CBlipFillData,
  CSpPrData,
  CShapeStyleData,
  SpTypes,
  ImageSubTypes,
} from '../../dataType/spAttrs'
import { readFromJSON } from '../../utils'

import { FromJSONParams } from '../../defs/pptdoc'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { getMediaFromShape } from '../../../core/utilities/shape/getters'
import { hookManager, Hooks } from '../../../core/hooks'
import { CSld } from '../../../core/Slide/CSld'
import { setDeletedForSp } from '../../../core/utilities/shape/updates'
import { BlipFill } from '../../../core/SlideElement/attrs/fill/blipFill'
import { UniNvPr, SpPr } from '../../../core/SlideElement/attrs/shapePrs'
import { ShapeStyle } from '../../../core/SlideElement/attrs/ShapeStyle'
import { setLock } from '../../../core/utilities/shape/locks'
import { ShapeLocks } from '../../../core/SlideElement/const'
import { consumeDeltaOperations } from '../utils'

export function applyImage(
  data: Operation[],
  image: ImageShape,
  rId: Nullable<string>,
  params: FromJSONParams
) {
  if (rId != null && params.ignoreRId !== true) {
    image.setRefId(rId)
  }

  consumeDeltaOperations(
    data,
    null,
    ImageDataLayout,
    ImageDataLayoutMap,
    ({ opType, op }) => {
      switch (opType) {
        case 'nvPicPr': {
          const opData = op.data['object']
          const nvPicPrData = opData['nvPicPr'] as UniNvPrData
          const nvPicPr = readFromJSON(nvPicPrData, UniNvPr, undefined, [
            SpTypes.Pic,
          ])
          if (nvPicPr != null) {
            image.setNvPicPr(nvPicPr)
          }

          break
        }
        case 'blipFill': {
          const opData = op.data['object']
          const blipFillData = opData['blipFill'] as CBlipFillData
          const blipFill = readFromJSON(blipFillData, BlipFill)
          if (blipFill != null && blipFill.imageSrcId != null) {
            image.setBlipFill(blipFill)
          }

          break
        }
        case 'spPr': {
          const opData = op.data['object']
          const spPrData = opData['spPr'] as CSpPrData
          const spPr = readFromJSON(spPrData, SpPr)
          if (spPr != null) {
            spPr.setParent(image)
            // if (spPr.geometry == null) {
            //   spPr.setGeometry(CreateGeometry('rect'))
            // }
            image.setSpPr(spPr)
          }

          break
        }
        case 'style': {
          const opData = op.data['object']
          const dataOfStyle = opData as CShapeStyleData
          const style = readFromJSON(dataOfStyle, ShapeStyle)
          if (style != null) {
            image.setStyle(style)
          }

          break
        }
      }
    }
  )

  checkUpdateErrorMedia(image)
}

export function genImage(
  data: Operation[],
  rId: Nullable<string>,
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>,
  subType?: ImageSubTypes
): ImageShape {
  const image = new ImageShape()
  image.spTreeParent = spTreeParent
  setLock(image, ShapeLocks.noModifyAspect, true)
  setDeletedForSp(image, false)
  if (mainObject) {
    image.setParent(mainObject)
  }
  applyImage(data, image, rId, params)
  if (mainObject?.cSld?.checkUniNvPrForSps) {
    mainObject.cSld.checkUniNvPrForSps([image])
  }
  image.subType = subType
  if (!image.subType) {
    hookManager.invoke(Hooks.EditorUI.OnUpdateImageSubType, {
      image,
    })
  }
  return image
}

function checkUpdateErrorMedia(image: ImageShape) {
  const media = getMediaFromShape(image)
  const imageUrl = image.blipFill?.imageSrcId
  if (media && imageUrl) {
    hookManager.invoke(Hooks.Attrs.CheckKnownErrorImageUrl, {
      url: imageUrl,
      media: media.media,
    })
  }
}
