import { Operation } from '../../operation'
import { Shape } from '../../../core/SlideElement/Shape'
import { FromJSONParams } from '../../defs/pptdoc'
import { ShapeDataLayout, ShapeDataLayoutTypeMap } from '../../defs/csld/shape'
import { Nullable } from '../../../../liber/pervasive'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { readFromJSON } from '../../utils'
import { UniNvPr, SpPr } from '../../../core/SlideElement/attrs/shapePrs'
import { isNumber } from '../../../common/utils'
import { ShapeStyle } from '../../../core/SlideElement/attrs/ShapeStyle'
import { TextBody } from '../../../core/SlideElement/TextBody'
import { applyTextBody } from './txBody'
import { hasOwnKey } from '../../../../liber/hasOwnKey'
import { SpTypes } from '../../dataType/spAttrs'
import {
  clearSlideElementContent,
  setDeletedForSp,
  setParentForSlideElement,
} from '../../../core/utilities/shape/updates'
import { CSld } from '../../../core/Slide/CSld'
import { setLocks } from '../../../core/utilities/shape/locks'
import { consumeDeltaOperations } from '../utils'

export function applyShape(
  data: Operation[],
  shape: Shape,
  rId: Nullable<string>,
  _selectedGroup: Nullable<GroupShape>,
  params: FromJSONParams
) {
  if (rId != null && params.ignoreRId !== true) {
    shape.setRefId(rId)
  }

  consumeDeltaOperations(
    data,
    null,
    ShapeDataLayout,
    ShapeDataLayoutTypeMap,
    ({ opType, op, isEmptyOp }) => {
      switch (opType) {
        case 'attrs': {
          const opData = op.data['object']
          if (hasOwnKey(opData, 'useBgFill')) {
            shape.attrUseBgFill = opData['useBgFill']
          }

          break
        }
        case 'nvSpPr': {
          const opData = op.data['object']
          const nvSpPrData = opData['nvSpPr'] || {}
          const pr = readFromJSON(nvSpPrData, UniNvPr, [shape], [SpTypes.Shape])
          if (pr) {
            shape.setNvSpPr(pr)
            if (isNumber(pr.nvUniSpPr?.locks)) {
              setLocks(shape, pr.nvUniSpPr!.locks)
            }
          }

          break
        }
        case 'spPr': {
          const opData = op.data['object']
          const spPrData = opData['spPr']
          const spPr = readFromJSON(spPrData, SpPr)
          if (spPr) {
            shape.setSpPr(spPr)
            spPr.setParent(shape)
          }

          break
        }
        case 'style': {
          const opData = op.data['object']
          const style = readFromJSON(opData, ShapeStyle)
          if (style) {
            shape.setStyle(style)
          }

          break
        }
        case 'txBody': {
          clearSlideElementContent(shape)
          if (isEmptyOp) {
            break
          }

          const opData = op.data['delta']
          let txbody: TextBody

          if (shape) {
            if (shape.txBody) {
              txbody = shape.txBody
            } else {
              txbody = new TextBody(shape)
            }
          } else {
            txbody = new TextBody(shape)
          }

          applyTextBody(opData, txbody, params)

          shape.setTxBody(txbody)

          break
        }
      }
    }
  )
}

export function genShape(
  data: Operation[],
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>,
  rId?: Nullable<string>
): Shape {
  const shape = new Shape()
  shape.spTreeParent = spTreeParent
  setDeletedForSp(shape, false)
  if (mainObject) {
    setParentForSlideElement(shape, mainObject)
  }
  applyShape(data, shape, rId, null, params)
  return shape
}
