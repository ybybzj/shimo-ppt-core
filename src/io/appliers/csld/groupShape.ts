import { Operation, getOperationDataType } from '../../operation'
import { FromJSONParams } from '../../defs/pptdoc'
import {
  GroupShapeDataLayout,
  GroupShapeDataLayoutTypeMap,
} from '../../defs/csld/groupShape'
import { Nullable, Dict } from '../../../../liber/pervasive'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { readFromJSON, scaleValue } from '../../utils'
import { isNumber } from '../../../common/utils'
import { ImageSubTypes, SpTypes } from '../../dataType/spAttrs'
import { SlideElement } from '../../../core/SlideElement/type'
import { Presentation } from '../../../core/Slide/Presentation'
import { each } from '../../../../liber/l/each'
import { genShape, applyShape } from './shape'
import { genImage, applyImage } from './image'
import { genGraphicFrame, applyGraphicFrame } from './graphicFrame'
import { ConnectionShape } from '../../../core/SlideElement/ConnectionShape'
import { applyConnectionShape, genConnectionShape } from './connectionShape'
import { collectCNvNidToSpIdMap } from '../../../core/utilities/shape/manageCxnSpState'
import {
  onSlideElemntRemoved,
  setDeletedForSp,
  setGroup,
  setParentForSlideElement,
} from '../../../core/utilities/shape/updates'
import { InstanceType } from '../../../core/instanceTypes'
import { checkErrorImage } from '../../../images/errorImageSrc'
import { isEmptySlideElement } from '../../../core/utilities/shape/asserts'
import { CSld } from '../../../core/Slide/CSld'
import { updateCalcStatusForGroup } from '../../../core/calculation/updateCalcStatus/group'
import { SpPr, UniNvPr, Xfrm } from '../../../core/SlideElement/attrs/shapePrs'
import { setLocks } from '../../../core/utilities/shape/locks'
import { applyOle, genOle } from './oleObject'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { logger } from '../../../lib/debug/log'
import { adjustXfrmOfGroup } from '../../../core/utilities/shape/group'
import { applyChartObject, genChartObject } from './chartObject'
import { ChartObject } from '../../../core/SlideElement/ChartObject'
import { Hooks, hookManager } from '../../../core/hooks'
import { ScaleOfPPTXSizes } from '../../../core/common/const/unit'
import { DeltaOperation, MapOperation } from '../../../re/export/modoc'
import { consumeDeltaOperations } from '../utils'

export function applyGroupShape(
  data: Operation[],
  group: GroupShape,
  rId: Nullable<string>,
  params: FromJSONParams
) {
  if (rId != null && params.ignoreRId !== true) {
    group.setRefId(rId)
  }
  consumeDeltaOperations(
    data,
    null,
    GroupShapeDataLayout,
    GroupShapeDataLayoutTypeMap,
    ({ opType, op }) => {
      switch (opType) {
        case 'nvGrpSpPr': {
          const opData = op.data['object']
          const nvSpPrData = opData['nvGrpSpPr'] || {}
          const pr = readFromJSON(nvSpPrData, UniNvPr, undefined, [
            SpTypes.Shape,
          ])
          if (pr) {
            group.setNvSpPr(pr)
            if (isNumber(pr.nvUniSpPr?.locks)) {
              setLocks(group, pr.nvUniSpPr!.locks)
            }
          }

          break
        }
        case 'grpSpPr': {
          const opData = op.data['object']
          const spPrData = opData['grpSpPr']
          const spPr = readFromJSON(spPrData, SpPr)
          if (spPr) {
            group.setSpPr(spPr)
            spPr.setParent(group)
          }

          break
        }
        case 'spTree': {
          const updateSp = (sp: SlideElement) => {
            if (sp && !isHiddenSp(sp) && sp.spPr && sp.spPr.xfrm) {
              sp.spTreeParent = group
              setGroup(sp, group)
            }
          }
          const removeSp = (sp: SlideElement) => {
            if (sp) {
              for (let i = group.spTree.length - 1; i > -1; --i) {
                if (group.spTree[i].getId() === sp.getId()) {
                  group.spTree.splice(i, 1)
                  onSlideElemntRemoved(sp)
                  return
                }
              }
            }
          }

          applySpTree(
            group,
            op,
            undefined,
            group.parent,
            params,
            updateSp,
            removeSp
          )
          updateCalcStatusForGroup(
            group,
            EditActionFlag.edit_GroupShapeAddToSpTree
          )

          break
        }
      }
    }
  )
  checkGroupShapeXfrm(group)
}

function checkGroupShapeXfrm(group: GroupShape) {
  if (!group || !group.spPr) {
    return
  }
  if (!group.spPr.xfrm && group.spTree.length > 0) {
    const xfrm = new Xfrm(group.spPr)
    xfrm.setOffX(0)
    xfrm.setOffY(0)
    xfrm.setChOffX(0)
    xfrm.setChOffY(0)
    xfrm.setExtX(50)
    xfrm.setExtY(50)
    xfrm.setChExtX(50)
    xfrm.setChExtY(50)
    group.spPr.setXfrm(xfrm)
    adjustXfrmOfGroup(group)
    group.spPr!.xfrm!.setParent(group.spPr)
  }
}

export function genGroupShape(
  data: Operation[],
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>,
  rId?: Nullable<string>
): GroupShape {
  const shape = new GroupShape()
  shape.spTreeParent = spTreeParent
  setDeletedForSp(shape, false)
  if (mainObject) {
    setParentForSlideElement(shape, mainObject)
  }
  applyGroupShape(data, shape, rId, params)
  return shape
}

export function applySpTree(
  csld: CSld | GroupShape,
  op: MapOperation,
  appliedOp: Nullable<MapOperation>,
  mainObject: Nullable<any>,
  params: FromJSONParams,
  updateSp: (sp: SlideElement, rId: Nullable<string>) => void,
  removeSp: (sp: SlideElement, presentation: Presentation) => void
): boolean {
  let needRerender = true
  appliedOp = appliedOp ?? op
  const isAppliedOpEmpty = isInvalidMapItem(appliedOp)
  const appliedOpData = isAppliedOpEmpty ? {} : appliedOp.data['slide']

  // get update info
  const removedSps: Dict<SlideElement> = {}
  const oldOrderStr = getSpOrderStr(csld.spTree)
  const currentSpRefMap = csld.spTree.reduce((result, sp) => {
    const rid = sp.getRefId()
    result[rid] = sp
    // collect removed sps
    if (appliedOpData[rid] == null) {
      removedSps[rid] = sp
    }
    return result
  }, {} as Dict<SlideElement>)

  const updateSpData: Dict<Operation> = Object.keys(op.data['slide']).reduce(
    (result, key) => {
      const spOp = op.data['slide'][key][0]
      if (spOp) {
        result[key] = spOp
      }
      return result
    },
    {} as Dict<Operation>
  )

  if (!isAppliedOpEmpty) {
    // sort sp ops
    const shapesOps = Object.keys(appliedOpData)
      .reduce(
        (result, rId) => {
          const shapeOp = appliedOpData[rId][0]
          if (shapeOp && getOperationDataType(shapeOp.data) === 'delta') {
            const info: {
              shapeOp: DeltaOperation
              rId: string
              spType?: SpTypes
            } = { shapeOp: shapeOp as DeltaOperation, rId }

            const spType = shapeOp?.attributes?.spType
            if (spType) {
              info.spType = spType as SpTypes
            }
            result.push(info)
          }
          return result
        },
        [] as Array<{ shapeOp: DeltaOperation; rId: string; spType?: SpTypes }>
      )
      .sort(({ shapeOp: op1, rId: rId1 }, { shapeOp: op2, rId: rId2 }) => {
        const op1Order = (
          op1.attributes ? op1.attributes['order'] : -1
        ) as number
        const op2Order = (
          op2.attributes ? op2.attributes['order'] : -1
        ) as number
        return op1Order !== op2Order
          ? op1Order - op2Order
          : rId1 < rId2
            ? -1
            : 1
      })

    const shapes = shapesOps
      .reduce((result, { shapeOp: op, rId, spType: opSpType }) => {
        const info = getSpItemsUpdateInfo(
          updateSpData,
          currentSpRefMap,
          rId,
          params.ignoreRId
        )
        if (info) {
          switch (info.updateAction) {
            case 'replace': {
              let sp: SlideElement | undefined = info.sp
              const curSpType = getSpTypeFromSlideElement(info.sp)
              if (opSpType === curSpType) {
                applySp(op, info.sp, params, rId)
              } else {
                if (sp) {
                  onSlideElemntRemoved(sp)
                }
                sp = genSp(op, params, csld, mainObject, rId)
              }
              if (sp) {
                result.push(sp)
                collectCNvNidToSpIdMap(sp)
                needRerender = false
              }
              break
            }
            case 'update': {
              if (getOperationDataType(info.updateOp.data) === 'delta') {
                applySp(info.updateOp as DeltaOperation, info.sp, params, rId)
              } else {
                applySp(op, info.sp, params, rId)
              }

              result.push(info.sp)
              collectCNvNidToSpIdMap(info.sp)
              needRerender = false
              break
            }
            case 'add': {
              const sp = genSp(op, params, csld, mainObject, rId)
              if (sp && !isEmptySlideElement(sp)) {
                updateSp(sp, rId)
                result.push(sp)
                collectCNvNidToSpIdMap(sp)
              }
              needRerender = false
              break
            }
            case 'retain': {
              result.push(info.sp)
              collectCNvNidToSpIdMap(info.sp)
              break
            }
          }
        }

        return result
      }, [] as SlideElement[])
      .filter((sp) => !isHiddenSp(sp) && !isEmptySlideElement(sp))

    // remove before update spTree
    if (Object.keys(removedSps).length > 0) {
      each((sp) => {
        removeSp(sp, params.presentation)
      }, removedSps)
    }

    if (typeof (csld as CSld).checkUniNvPrForSps === 'function') {
      csld.spTree = []
      shapes.forEach((shape) => {
        ;(csld as CSld).addSpAtIndex(shape, csld.spTree.length)
      })
    } else {
      csld.spTree = shapes
    }
  } else {
    // remove before update spTree
    if (Object.keys(removedSps).length > 0) {
      each((sp) => {
        removeSp(sp, params.presentation)
      }, removedSps)
    }

    csld.spTree = []
  }

  if (needRerender === true && oldOrderStr === getSpOrderStr(csld.spTree)) {
    needRerender = false
  }
  return needRerender
}

function getSpOrderStr(spTree: SlideElement[]): string {
  if (!spTree || spTree.length <= 0) {
    return ''
  }

  return spTree.map((sp) => sp.getRefId()).join('|')
}

type SpUpdateInfo =
  | {
      updateAction: 'add'
    }
  | {
      updateAction: 'retain' | 'replace'
      sp: SlideElement
    }
  | {
      updateAction: 'update'
      sp: SlideElement
      updateOp: Operation
    }

export function getSpItemsUpdateInfo(
  updateSpData: Dict<Operation>,
  currentSpRefMap: Dict<SlideElement>,
  targetRid: string,
  ignoreRId?: boolean
): SpUpdateInfo | undefined {
  if (ignoreRId === true) {
    return {
      updateAction: 'add',
    }
  }
  const spUpdateOp = updateSpData[targetRid]
  const targetSp = currentSpRefMap[targetRid]
  if (spUpdateOp) {
    switch (spUpdateOp.action) {
      case 'insert': {
        return targetSp
          ? {
              updateAction: 'replace',
              sp: targetSp,
            }
          : {
              updateAction: 'add',
            }
      }
      case 'retain': {
        if (targetSp) {
          if (getOperationDataType(spUpdateOp.data) === 'number') {
            return {
              updateAction: 'retain',
              sp: targetSp,
            }
          } else {
            return {
              updateAction: 'update',
              sp: targetSp,
              updateOp: spUpdateOp,
            }
          }
        }
      }
    }
  } else if (targetSp) {
    return {
      updateAction: 'retain',
      sp: targetSp,
    }
  }
}

function genSp(
  op: DeltaOperation,
  params: FromJSONParams,
  csld: CSld | GroupShape,
  mainObject?: Nullable<any>,
  rId?: Nullable<string>
): SlideElement | undefined {
  let sp
  const opData = op.data['delta']
  if (op.attributes) {
    switch (op.attributes['spType']) {
      case 'sp': {
        sp = genShape(opData, params, csld, mainObject, rId)
        break
      }
      case 'grpSp': {
        sp = genGroupShape(opData, params, csld, mainObject, rId)
        break
      }
      case 'ole': {
        sp = genOle(opData, rId, params, csld, mainObject)
        break
      }
      case 'pic': {
        const subType = op.attributes['subType'] as ImageSubTypes
        const image = genImage(opData, rId, params, csld, mainObject, subType)
        if (image.blipFill && !checkErrorImage(image.blipFill.imageSrcId)) {
          sp = image
          const xfrm = image.spPr?.xfrm
          const imgSrc = image.blipFill?.imageSrcId
          if (imgSrc != null && xfrm?.extX != null && xfrm?.extY != null) {
            hookManager.invoke(
              Hooks.Attrs.CollectImageUrlParamsFromDataChange,
              {
                imgSrc,
                type: 'image',
                extX: scaleValue(xfrm.extX, ScaleOfPPTXSizes)!,
                extY: scaleValue(xfrm.extY, ScaleOfPPTXSizes)!,
              }
            )
          }
        }
        break
      }
      case 'graphicFrame': {
        sp = genGraphicFrame(opData, params, csld, mainObject, rId)
        break
      }
      case 'cxnSp': {
        sp = genConnectionShape(opData, params, csld, mainObject, rId)
        break
      }
      case 'chart': {
        sp = genChartObject(
          opData,
          rId,
          params,
          csld,
          mainObject
        ) as ChartObject
        const imgSrc = sp.blipFill?.imageSrcId

        const xfrm = sp.spPr?.xfrm
        if (imgSrc != null && xfrm?.extX != null && xfrm?.extY != null) {
          hookManager.invoke(Hooks.Attrs.CollectImageUrlParamsFromDataChange, {
            imgSrc,
            type: 'chart',
            extX: scaleValue(xfrm.extX, ScaleOfPPTXSizes)!,
            extY: scaleValue(xfrm.extY, ScaleOfPPTXSizes)!,
          })
        }

        break
      }

      // ...
    }
  }
  return sp
}

export function applySp(
  op: DeltaOperation,
  sp: SlideElement,
  params: FromJSONParams,
  rId?: Nullable<string>
) {
  if (!sp) return
  const opSpType = op.attributes ? op.attributes['spType'] : null
  let spSpType
  const opData = op.data['delta']
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      if (sp.isConnectionShape()) {
        const cxnSp = sp as ConnectionShape
        applyConnectionShape(opData, cxnSp, rId, params)
        spSpType = 'cxnSp'
      } else {
        applyShape(opData, sp, rId, null, params)
        spSpType = 'sp'
      }
      break
    }
    case InstanceType.ImageShape: {
      if (sp.isOleObject()) {
        applyOle(opData, sp, rId, params)
        spSpType = 'ole'
      } else if (sp.isChartObject()) {
        applyChartObject(opData, sp, rId, params)
        spSpType = 'chart'
      } else {
        applyImage(opData, sp, rId, params)
        spSpType = 'pic'
      }
      break
    }
    case InstanceType.GroupShape: {
      applyGroupShape(opData, sp, rId, params)
      spSpType = 'grpSp'
      break
    }
    case InstanceType.GraphicFrame: {
      applyGraphicFrame(opData, sp, rId, params)
      spSpType = 'graphicFrame'
      break
    }
  }

  if (opSpType != null && spSpType != null && opSpType !== spSpType) {
    logger.warn(
      `[applySp]invalid input stream opType "${opSpType}", expect "${spSpType}"`
    )
  }
}

function isInvalidMapItem(map: MapOperation): boolean {
  if (getOperationDataType(map.data) !== 'map') {
    return true
  }
  const mapData = map.data['slide']

  return Object.keys(mapData).length <= 0
}

export function isHiddenSp(sp) {
  if (!sp) return false
  let nvProps = sp.nvSpPr
  if (!nvProps) nvProps = sp.nvPicPr
  if (!nvProps) nvProps = sp.nvGrpSpPr

  if (!nvProps) return false

  if (nvProps.cNvPr && nvProps.cNvPr.isHidden) return true

  return false
}

function getSpTypeFromSlideElement(sp: SlideElement): SpTypes {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      if (sp.isConnectionShape()) {
        return 'cxnSp'
      }
      return 'sp'
    case InstanceType.ImageShape:
      if (sp.isOleObject()) {
        return 'ole'
      } else if (sp.isChartObject()) {
        return 'chart'
      } else {
        return 'pic'
      }
    case InstanceType.GroupShape:
      return 'grpSp'
    case InstanceType.GraphicFrame:
      return 'graphicFrame'
  }
}
