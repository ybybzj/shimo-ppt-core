import { Operation } from '../../operation'
import { GraphicFrame } from '../../../core/SlideElement/GraphicFrame'
import { Nullable } from '../../../../liber/pervasive'
import { FromJSONParams } from '../../defs/pptdoc'
import {
  GraphicFrameDataLayout,
  GraphicFrameDataLayoutMap,
} from '../../defs/csld/graphicFrame'
import { readFromJSON } from '../../utils'
import { applyTable } from './table'
import { isNumber } from '../../../common/utils'
import { UniNvPrData, CXfrmData, SpTypes } from '../../dataType/spAttrs'
import { InstanceType, isInstanceTypeOf } from '../../../core/instanceTypes'
import { Table } from '../../../core/Table/Table'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import {
  setDeletedForSp,
  setParentForSlideElement,
} from '../../../core/utilities/shape/updates'
import { CSld } from '../../../core/Slide/CSld'
import { UniNvPr, SpPr, Xfrm } from '../../../core/SlideElement/attrs/shapePrs'
import { consumeDeltaOperations } from '../utils'

export function applyGraphicFrame(
  data: Operation[],
  graphicFrame: GraphicFrame,
  rId: Nullable<string>,
  params: FromJSONParams
) {
  if (rId != null && params.ignoreRId !== true) {
    graphicFrame.setRefId(rId)
  }

  consumeDeltaOperations(
    data,
    null,
    GraphicFrameDataLayout,
    GraphicFrameDataLayoutMap,
    ({ opType, op }) => {
      switch (opType) {
        case 'attrs': {
          const opData = op.data['object']
          const nvGraphicFramePrData = opData[
            'nvGraphicFramePr'
          ] as Nullable<UniNvPrData>

          const xfrmData = opData['xfrm'] as Nullable<CXfrmData>

          const nvGraphicFramePr = readFromJSON(
            nvGraphicFramePrData,
            UniNvPr,
            undefined,
            [SpTypes.GraphicFrame]
          )
          if (nvGraphicFramePr) {
            graphicFrame.setNvSpPr(nvGraphicFramePr)
          }

          if (xfrmData) {
            if (!graphicFrame.spPr) {
              graphicFrame.setSpPr(new SpPr())
              graphicFrame.spPr!.setParent(graphicFrame)
            }
            const xfrm = readFromJSON(xfrmData, Xfrm, [graphicFrame.spPr!])
            if (xfrm) {
              graphicFrame.spPr!.setXfrm(xfrm)
              xfrm.setParent(graphicFrame.spPr!)
            }
          }

          break
        }
        case 'graphic': {
          const opData = op.data['delta']
          switch (op.action) {
            case 'insert': {
              let table = graphicFrame.graphicObject
              let rowIndex, cellIndex
              if (table && table.instanceType === InstanceType.Table) {
                const oldCurCell = table.curCell
                if (oldCurCell) {
                  cellIndex = oldCurCell.index
                  rowIndex = oldCurCell.parent.index
                }
                table.reset(graphicFrame, 0, 0, undefined)
              } else {
                table = new Table(graphicFrame, 0, 0, undefined)
              }

              applyTable(table, opData, params)
              const xfrm = graphicFrame.spPr?.xfrm
              if (xfrm) {
                table.resetDimension(0, 0, xfrm.extX, 100000)
              }

              graphicFrame.setGraphicObject(table)

              if (isNumber(rowIndex) && isNumber(cellIndex)) {
                const targetRow = table.children.at(rowIndex)
                const targetCell = targetRow && targetRow.children.at(cellIndex)
                if (targetCell) {
                  table.curCell = targetCell
                }
              }
              break
            }
            case 'retain': {
              const table = graphicFrame.graphicObject
              if (!table) break
              table && applyTable(table, opData, params)
              const xfrm = graphicFrame.spPr?.xfrm
              if (xfrm) {
                table.resetDimension(0, 0, xfrm.extX, 100000)
              }
              break
            }
          }
        }
      }
    }
  )
}

export function genGraphicFrame(
  data: Operation[],
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>,
  rId?: Nullable<string>
): GraphicFrame {
  const graphicFrame = new GraphicFrame()
  graphicFrame.spTreeParent = spTreeParent
  setDeletedForSp(graphicFrame, false)
  if (mainObject) {
    setParentForSlideElement(graphicFrame, mainObject)
  }

  applyGraphicFrame(data, graphicFrame, rId, params)

  if (isInstanceTypeOf(mainObject, InstanceType.SlideLayout)) {
    params.presentation.adjustGraphicFrameRowHeight(graphicFrame)
  }

  return graphicFrame
}
