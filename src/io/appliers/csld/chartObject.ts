import { Nullable } from '../../../../liber/pervasive'
import { CSld } from '../../../core/Slide/CSld'
import { ChartObject } from '../../../core/SlideElement/ChartObject'
import { ShapeLocks } from '../../../core/SlideElement/const'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { ImageShape } from '../../../core/SlideElement/Image'
import { setLock } from '../../../core/utilities/shape/locks'
import { setDeletedForSp } from '../../../core/utilities/shape/updates'
import { ChartAttrs } from '../../dataType/csld'
import {
  ChartDataLayout,
  ChartDataLayoutMap,
} from '../../defs/csld/chartObject'
import { FromJSONParams } from '../../defs/pptdoc'
import { Operation } from '../../operation'
import { consumeDeltaOperations } from '../utils'
import { applyImage } from './image'

export function applyChartObject(
  data: Operation[],
  chartObject: ChartObject,
  rId: Nullable<string>,
  params: FromJSONParams
) {
  if (rId != null && params.ignoreRId !== true) {
    chartObject.setRefId(rId)
  }

  consumeDeltaOperations(
    data,
    null,
    ChartDataLayout,
    ChartDataLayoutMap,
    ({ opType, op }) => {
      switch (opType) {
        case 'attrs': {
          const opData = op.data['object']
          const attrsData = opData as ChartAttrs
          chartObject.attrsFromJSON(attrsData)

          break
        }
        case 'pic': {
          const opData = op.data['delta']
          applyImage(opData, chartObject, null, params)
          break
        }
      }
    }
  )
}

export function genChartObject(
  data: Operation[],
  rId: Nullable<string>,
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>
): ImageShape {
  const chartObject = new ChartObject()
  chartObject.spTreeParent = spTreeParent
  setLock(chartObject, ShapeLocks.noModifyAspect, true)
  setDeletedForSp(chartObject, false)
  if (mainObject) {
    chartObject.setParent(mainObject)
  }
  applyChartObject(data, chartObject, rId, params)
  if (mainObject?.cSld?.checkUniNvPrForSps) {
    mainObject.cSld.checkUniNvPrForSps([chartObject])
  }
  return chartObject
}
