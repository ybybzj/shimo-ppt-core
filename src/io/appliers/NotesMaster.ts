import { NotesMaster } from '../../core/Slide/NotesMaster'
import {
  NotesMasterDataLayout,
  NotesMasterDataLayoutTypeMap,
} from '../defs/NotesMaster'

import { Dict, Nullable } from '../../../liber/pervasive'
import {
  FromJSONParams,
  PPTDocDataLayout,
  PPTDocDataLayoutTypeMap,
} from '../defs/pptdoc'
import { readFromJSON } from '../utils'
import { applyCSld } from './csld'
import { TextListStyle } from '../../core/textAttributes/TextListStyle'
import { SlideMaster } from '../../core/Slide/SlideMaster'
import { CSld } from '../../core/Slide/CSld'
import { ClrMap } from '../../core/color/clrMap'
import { HF } from '../../core/SlideElement/attrs/hf'
import { Theme } from '../../core/SlideElement/attrs/theme'
import { MapOperation } from '../../re/export/modoc'
import {
  AppliedConsumeResult,
  consumeDeltaOperations,
  getOperationByOpType,
  handleMapDataApplying,
} from './utils'
import { Operation } from '../operation'
export function applyNotesMasters(
  op: MapOperation,
  appliedOp: Nullable<MapOperation>,
  notesMasters: Dict<NotesMaster>,
  defaultTheme: Nullable<Theme>,
  params: FromJSONParams,
  cb?: (rId: string, newNotesMaster: NotesMaster) => void,
  themeSlideMastersMap?: Dict<(SlideMaster | NotesMaster)[]>
) {
  handleMapDataApplying(
    op,
    null, // refsInfo.noteMasters,
    notesMasters,
    appliedOp,
    (rId, data) => {
      const newNotesMaster = new NotesMaster()
      applyNotesMaster(data, rId, newNotesMaster, params, ({ themeRef }) => {
        if (themeSlideMastersMap) {
          themeSlideMastersMap[themeRef] = themeSlideMastersMap[themeRef] || []
          if (themeSlideMastersMap[themeRef].indexOf(newNotesMaster) < 0) {
            themeSlideMastersMap[themeRef].push(newNotesMaster)
          }
        }
      })
      if (defaultTheme) {
        newNotesMaster.theme = defaultTheme
      }
      if (cb) {
        cb(rId, newNotesMaster)
      }
    },
    (notesMaster, data, _appliedData, rId) => {
      applyNotesMaster(data, rId, notesMaster, params, ({ themeRef }) => {
        if (themeSlideMastersMap) {
          themeSlideMastersMap[themeRef] = themeSlideMastersMap[themeRef] || []
          if (themeSlideMastersMap[themeRef].indexOf(notesMaster) < 0) {
            themeSlideMastersMap[themeRef].push(notesMaster)
          }
          if (cb) {
            cb(rId, notesMaster)
          }
        }
      })
    }
  )
}

function applyNotesMaster(
  data: Operation[],
  rId: Nullable<string>,
  notesMaster: NotesMaster,
  params: FromJSONParams,
  cb?: (arg: { themeRef: string }) => void
): AppliedConsumeResult {
  if (rId && params.ignoreRId !== true) {
    notesMaster.setRefId(rId)
  }
  return consumeDeltaOperations(
    data,
    null,
    NotesMasterDataLayout,
    NotesMasterDataLayoutTypeMap,
    ({ opType: type, op }) => {
      switch (type) {
        case 'attrs': {
          const opData = op.data['object']
          const clrMapData = opData['clrMap']
          const hfData = opData['hf']
          const txStylesData = opData['notesStyles']
          const themeRef = opData['themeRef']

          if (notesMaster.clrMap) {
            notesMaster.clrMap.fromJSON(clrMapData)
          } else {
            const clrMap = readFromJSON(clrMapData, ClrMap)
            if (clrMap) {
              notesMaster.clrMap = clrMap
            }
          }

          const hf = readFromJSON(hfData, HF)
          if (hf) {
            notesMaster.hf = hf
          }

          const txStyles = readFromJSON(txStylesData, TextListStyle)
          if (txStyles) {
            notesMaster.setTxStyles(txStyles)
          }

          if (themeRef != null && cb) {
            cb({ themeRef })
          }

          break
        }
        case 'csld': {
          const opData = op.data['delta']
          const cSld = notesMaster.cSld ?? new CSld(notesMaster)
          applyCSld(opData, null, cSld, notesMaster, params)

          if (cSld.bg) {
            notesMaster.changeBackground(cSld.bg)
          }
          notesMaster.setCSldName(cSld.name)

          break
        }
      }
    }
  )
}

export function genSlideNoteMasterFromInputData(
  inputData: Operation[],
  rid: string,
  defaultTheme: Nullable<Theme>,
  params: FromJSONParams,
  cb?: (rId: string, newNotesMaster: NotesMaster) => void,
  themeSlideMastersMap?: Dict<(SlideMaster | NotesMaster)[]>
): Nullable<NotesMaster> {
  const ops = getOperationByOpType(
    inputData,
    'notesMasters',
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap
  )
  if (ops) {
    const data = ops.data['slide'][rid]
    if (data == null) {
      return null
    }

    const notesMaster = new NotesMaster()
    applyNotesMaster(data, rid, notesMaster, params, ({ themeRef }) => {
      if (themeSlideMastersMap) {
        themeSlideMastersMap[themeRef] = themeSlideMastersMap[themeRef] || []
        if (themeSlideMastersMap[themeRef].indexOf(notesMaster) < 0) {
          themeSlideMastersMap[themeRef].push(notesMaster)
        }
      }
    })
    if (defaultTheme) {
      notesMaster.theme = defaultTheme
    }
    if (cb) {
      cb(rid, notesMaster)
    }

    return notesMaster
  }
  return null
}
