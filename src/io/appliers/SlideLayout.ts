import { Operation } from '../operation'
import { SlideLayout } from '../../core/Slide/SlideLayout'
import { FromJSONParams } from '../defs/pptdoc'
import { SlideLayoutDataLayout } from '../defs/SlideLayout'
import { readFromJSON } from '../utils'
import { fromClrMapOverride } from '../../core/color/clrMap'
import { HF } from '../../core/SlideElement/attrs/hf'
import { CSld } from '../../core/Slide/CSld'
import { applyCSld } from './csld'
import { Nullable } from '../../../liber/pervasive'
import { fromSlideLayoutType } from '../dataType/slideLayout'
import { SlideTransition } from '../../core/Slide/SlideTransition'
import { Presentation } from '../../core/Slide/Presentation'
import { AppliedConsumeResult, consumeDeltaOperations } from './utils'
import { SlideDataLayoutTypeMap } from '../defs/Slide'

export function applySlideLayout(
  data: Operation[],
  appliedOps: Nullable<Operation[]>,
  layout: SlideLayout,
  rId: Nullable<string>,
  params: FromJSONParams
): AppliedConsumeResult {
  if (rId && params.ignoreRId !== true) layout.setRefId(rId)

  return consumeDeltaOperations(
    data,
    appliedOps,
    SlideLayoutDataLayout,
    SlideDataLayoutTypeMap,
    ({ opType: type, op, appliedOp}) => {
      if (type === 'attrs') {
        const opData = op.data['object']
        const matchingName = opData['matchingName']
        const preserve = opData['preserve']
        const showMasterPhAnim = opData['showMasterPhAnim']
        const showMasterSp = opData['showMasterSp']
        const userDrawn = opData['userDrawn']

        const type =
          opData['type'] != null
            ? fromSlideLayoutType(opData['type'])
            : undefined
        const clrMap = opData['clrMap']
        const HFData = opData['hf']

        if (matchingName != null) {
          layout.setMatchingName(matchingName)
        }

        if (preserve != null) {
          layout.preserve = preserve
        }

        if (showMasterPhAnim != null) {
          layout.setShowPhAnim(showMasterPhAnim)
        }

        if (showMasterSp != null) {
          layout.setShowMasterSp(showMasterSp)
        }

        if (userDrawn != null) {
          layout.userDrawn = userDrawn
        }

        if (type != null) {
          layout.setType(type)
        }

        if (clrMap != null) {
          const clr_map = fromClrMapOverride(clrMap)

          if (clr_map) {
            layout.setClrMapOverride(clr_map)
          }
        }

        if (HFData != null) {
          const hf = readFromJSON(HFData, HF)
          if (hf) {
            layout.hf = hf
          }
        }

        if ('transition' in opData) {
          const transitionData = opData['transition']

          if (transitionData === null) {
            layout.transition = undefined
          } else if (transitionData != null) {
            const transition = readFromJSON(transitionData, SlideTransition)

            if (transition != null) {
              layout.transition = new SlideTransition()
              layout.transition.reset()
              layout.transition.applyProps(transition)
            }
          }
        }
      } else if (type === 'csld') {
        const cSld = layout.cSld ?? new CSld(layout)
        applyCSld(op.data['delta'], appliedOp?.data['delta'], cSld, layout, params)

        if (cSld.bg) {
          layout.changeBackground(cSld.bg)
        }
        layout.setCSldName(cSld.name)
      }
    }
  )
}

export function genLayout(
  data: Operation[],
  presentation: Presentation,
  params: FromJSONParams,
  rId?: Nullable<string>
) {
  const layout = new SlideLayout(presentation)
  layout.setSlideSize(presentation.width, presentation.height)
  applySlideLayout(data, null, layout, rId, params)
  return layout
}
