import { each } from '../../../liber/l/each'
import { Dict, Nullable } from '../../../liber/pervasive'
import { SlideComments } from '../../core/Slide/Comments/SlideComments'
import { MapOperation } from '../../re/export/modoc'
import { CmContentData } from '../dataType/comment'
import { FromJSONParams } from '../defs/pptdoc'
import { readFromJSON } from '../utils'

export function applyComments(
  op: MapOperation,
  appliedOp: Nullable<MapOperation>,
  _params: FromJSONParams,
  cb: (rId: string, slideComment: Nullable<SlideComments>) => void
) {
  const opData = op.data['slide']
  const appliedOpData = appliedOp?.data['slide']
  const mapData =
    op.action === 'insert'
      ? opData
      : op.action === 'retain'
      ? appliedOpData
      : undefined
  // 初始化 insert
  if (mapData) {
    each((data, rId) => {
      const streamItem = data[0]
      let commentsDict: Dict<CmContentData> = {}
      if (streamItem.data['object']) {
        commentsDict = streamItem.data['object'] as Dict<CmContentData>
      }
      const slideComment = readFromJSON(commentsDict, SlideComments)
      cb(rId, slideComment)
    }, mapData)
  }
}
