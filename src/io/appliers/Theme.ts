import { Nullable } from '../../../liber/pervasive'
import { FromJSONParams } from '../defs/pptdoc'

import { CThemeData } from '../dataType/theme'
import { readFromJSON, readFromJSONArray } from '../utils'
import { DefaultShapeAttributes } from '../../core/SlideElement/attrs/DefaultShapeAttributes'
import {
  Theme,
  ThemeElements,
  ExtraClrScheme,
} from '../../core/SlideElement/attrs/theme'
import { Operation, getOperationDataType } from '../operation'
import { MapOperation } from '../../re/export/modoc'
import { each } from '../../../liber/l/each'
import { AppliedConsumeResult } from './utils'
export function applyThemes(
  op: MapOperation,
  params: FromJSONParams,
  cb?: (rId: string, theme: Theme) => void
) {
  each((data, rId) => {
    const theme = new Theme()
    theme.presentation = params.presentation
    const applyResult = applyTheme(data, theme, rId, params)
    if (applyResult !== 'remove' && cb) {
      cb(rId, theme)
    }
  }, op.data['slide'])
}

function applyTheme(
  data: Operation[],
  theme: Theme,
  rId: Nullable<string>,
  params: FromJSONParams
): AppliedConsumeResult {
  if (rId && params.ignoreRId !== true) theme.setRefId(rId)
  const dataItem = data[0]
  if (dataItem && getOperationDataType(dataItem.data) === 'json') {
    const themeData = dataItem.data['object'] as CThemeData
    const name = themeData['name']
    const themeElementsData = themeData['themeElements']
    const objectDefaultsData = themeData['objectDefaults']
    const extraClrSchemeLstData = themeData['extraClrSchemeLst']
    if (name != null) {
      theme.setName(name)
    }

    if (themeElementsData != null) {
      const themeElements = readFromJSON(themeElementsData, ThemeElements)
      if (themeElements) {
        theme.setFontScheme(themeElements.fontScheme)
        theme.setFormatScheme(themeElements.fmtScheme)
        theme.changeColorScheme(themeElements.clrScheme)
      }
    }

    if (objectDefaultsData != null) {
      const spDef = readFromJSON(
        objectDefaultsData['spDef'],
        DefaultShapeAttributes
      )
      if (spDef != null) {
        theme.setSpDef(spDef)
      }
      const lnDef = readFromJSON(
        objectDefaultsData['lnDef'],
        DefaultShapeAttributes
      )
      if (lnDef != null) {
        theme.setLnDef(lnDef)
      }
      const txDef = readFromJSON(
        objectDefaultsData['txDef'],
        DefaultShapeAttributes
      )
      if (txDef != null) {
        theme.setTxDef(txDef)
      }
    }

    if (extraClrSchemeLstData != null) {
      const extraClrSchemeLst = readFromJSONArray(
        extraClrSchemeLstData,
        ExtraClrScheme
      )?.filter(Boolean)

      if (extraClrSchemeLst) {
        theme.extraClrSchemeLst = extraClrSchemeLst as ExtraClrScheme[]
      }
    }
    return 'update'
  } else if (dataItem?.action === 'remove') {
    return 'remove'
  } else {
    return 'retain'
  }
}
