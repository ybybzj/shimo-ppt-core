import { SlideMaster } from '../../core/Slide/SlideMaster'
import { SlideLayout } from '../../core/Slide/SlideLayout'
import { Dict, Nullable } from '../../../liber/pervasive'
import { FromJSONParams } from '../defs/pptdoc'
import {
  SlideMasterDataLayout,
  SlideMasterDataLayoutTypeMap,
} from '../defs/SlideMaster'
import { readFromJSON } from '../utils'
import { TextStyles } from '../../core/textAttributes/TextStyles'
import { applyCSld } from './csld'
import { SlideTransition } from '../../core/Slide/SlideTransition'
import { CSld } from '../../core/Slide/CSld'
import { ClrMap } from '../../core/color/clrMap'
import { HF } from '../../core/SlideElement/attrs/hf'
import { Operation } from '../operation'
import { consumeDeltaOperations } from './utils'

export function applySlideMaster(
  data: Operation[],
  rId: Nullable<string>,
  slideMaster: SlideMaster,
  slideLayouts: Nullable<Dict<SlideLayout>>,
  params: FromJSONParams,
  cb?: (arg: { themeRef: string }) => void
) {
  if (rId && params.ignoreRId !== true) {
    slideMaster.setRefId(rId)
  }
  return consumeDeltaOperations(
    data,
    null,
    SlideMasterDataLayout,
    SlideMasterDataLayoutTypeMap,
    ({ opType: type, op }) => {
      switch (type) {
        case 'attrs': {
          const opData = op.data['object']
          const themeId = opData['themeId']
          const preserve = opData['preserve']
          const clrMapData = opData['clrMap']
          const hfData = opData['hf']
          const txStylesData = opData['txStyles']
          const themeRef = opData['themeRef']

          if (themeId != null) {
            slideMaster.themeId = themeId
          }

          if (preserve != null) {
            slideMaster.preserve = preserve
          }

          const clrMap = readFromJSON(clrMapData, ClrMap)
          if (clrMap) {
            slideMaster.setClrMapOverride(clrMap)
          }

          const hf = readFromJSON(hfData, HF)
          if (hf) {
            slideMaster.hf = hf
          }

          if ('transition' in opData) {
            const transitionData = opData['transition']

            if (transitionData === null) {
              slideMaster.transition = undefined
            } else if (transitionData != null) {
              const transition = readFromJSON(transitionData, SlideTransition)

              if (transition != null) {
                slideMaster.transition = new SlideTransition()
                slideMaster.transition.reset()
                slideMaster.transition.applyProps(transition)
              }
            }
          }

          const txStyles = readFromJSON(txStylesData, TextStyles)
          if (txStyles) {
            slideMaster.setTextStyles(txStyles)
          }

          if (themeRef != null && cb) {
            cb({ themeRef })
          }

          break
        }
        case 'csld': {
          const opData = op.data['delta']
          const cSld = slideMaster.cSld ?? new CSld(slideMaster)
          applyCSld(opData, null, cSld, slideMaster, params)

          if (cSld.bg) {
            slideMaster.changeBackground(cSld.bg)
          }
          slideMaster.setCSldName(cSld.name)

          break
        }
        case 'layoutRefs': {
          if (slideLayouts) {
            const layouts: string[] = op.data['object']['layoutRefs'] ?? []
            layouts.forEach((layoutId) => {
              const layout = slideLayouts[layoutId]
              const isExistingLayout = slideMaster.layouts.find(
                (existingLayout) => existingLayout === layout
              )
              if (layout && !isExistingLayout) {
                slideMaster.addLayout(layout)
                layout.setMaster(slideMaster)
              }
            })
          }
          break
        }
      }
    }
  )
}
