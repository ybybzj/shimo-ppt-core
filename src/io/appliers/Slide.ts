import { Dict, Nullable } from '../../../liber/pervasive'
import { EditActionFlag } from '../../core/EditActionFlag'
import { CSld } from '../../core/Slide/CSld'
import { Presentation } from '../../core/Slide/Presentation'
import { Slide } from '../../core/Slide/Slide'
import { SlideLayout } from '../../core/Slide/SlideLayout'
import { SlideTransition } from '../../core/Slide/SlideTransition'
import { updateCalcStatusForSlide } from '../../core/calculation/updateCalcStatus/slide'
import { fromClrMapOverride } from '../../core/color/clrMap'
import { animationFixer } from '../animationsFixer'
import { SlideDataLayout, SlideDataLayoutTypeMap } from '../defs/Slide'
import { FromJSONParams } from '../defs/pptdoc'
import { Operation } from '../operation'
import { fromJSON, readFromJSON } from '../utils'
import { applyCSld } from './csld'
import { AppliedConsumeResult, consumeDeltaOperations } from './utils'

export function applySlide(
  data: Operation[],
  oldData: Nullable<Operation[]>,
  slide: Slide,
  params: FromJSONParams,
  rId?: Nullable<string>,
  slideLayouts?: Nullable<Dict<SlideLayout>>,
  cb?: (arg: { notesRef?: string; commentsRef?: string }) => void
) {
  if (rId && params.ignoreRId !== true) {
    slide.setRefId(rId)
  }

  let layoutChanged = false
  return consumeDeltaOperations(
    data,
    oldData,
    SlideDataLayout,
    SlideDataLayoutTypeMap,
    ({ opType: type, op, appliedOp }) => {
      const opData = op.data['object']
      if (type === 'attrs' && opData) {
        const show = opData['show']

        const showMasterPhAnim = opData['showMasterPhAnim']

        const showMasterSp = opData['showMasterSp']

        const clrMapData = opData['clrMap']

        const layoutRef = opData['layoutRef']

        if (show != null) {
          slide.setShow(show)
        }

        if (showMasterPhAnim != null) {
          slide.setShowPhAnim(showMasterPhAnim)
        }

        if (showMasterSp != null) {
          slide.setShowMasterSp(showMasterSp)
        }

        const clrMap = fromClrMapOverride(clrMapData)
        if (clrMap) {
          slide.setClrMapOverride(clrMap)
        }

        if ('transition' in opData) {
          const timingData = opData['transition']
          if (timingData === null) {
            // reset timing
            slide.timing?.reset()
          } else if (timingData != null) {
            const timing = readFromJSON(timingData, SlideTransition)
            if (timing != null) {
              slide.timing?.reset()
              slide.applyTiming(timing)
            }
          }
        }
        if ('timing' in opData) {
          const animationData = opData['timing']
          if (!animationData) {
            slide.animation.reset()
          } else {
            const needFixAnimation = fromJSON(slide.animation, animationData)
            if (needFixAnimation === true) {
              animationFixer.add(slide.animation)
            }
          }
        }

        // establish ref links
        if (slideLayouts && layoutRef != null) {
          const layout =
            slideLayouts[layoutRef] ?? Object.values(slideLayouts)[0]
          if (layout) {
            slide.setLayout(layout)
            slide.master = layout.master!
            slide.cSld = new CSld(slide)
            layoutChanged = true
          }
        }
        if ('notesRef' in opData) {
          const notesRef = opData['notesRef']
          if (notesRef && cb) {
            cb({ notesRef })
          } else if (notesRef === null) {
            //remove
            slide.removeNotes()
          }
        }
        if ('commentsRef' in opData) {
          const commentsRef = opData['commentsRef']
          if (commentsRef && cb) {
            cb({ commentsRef })
            if (slide.slideComments) slide.slideComments.setRefId(commentsRef)
          } else if (commentsRef === null) {
            slide.slideComments = null
          }
        }
      } else if (type === 'csld') {
        let opData = op.data['delta']
        if (layoutChanged === true && appliedOp != null) {
          opData = appliedOp.data['delta']
        }

        if (opData) {
          const { needRerender, updatedSpRefs } = applyCSld(
            opData,
            layoutChanged === true ? null : appliedOp?.data['delta'],
            slide.cSld,
            slide,
            params
          )

          if (needRerender === true) {
            updateCalcStatusForSlide(slide, EditActionFlag.edit_Slide_SetBg)
          }

          if (updatedSpRefs != null) {
            updateSpRefsForAnimation(slide, updatedSpRefs)
          }
        }
      }
      return 'update' as AppliedConsumeResult
    }
  )
}

export function genSlide(
  data: Operation[],
  presentation: Presentation,
  params: FromJSONParams,
  rId?: Nullable<string>,
  slideLayouts?: Nullable<Dict<SlideLayout>>,
  cb?: (arg: { notesRef?: string; commentsRef?: string }) => void
): Slide {
  const slide = new Slide(presentation, null, null)
  applySlide(data, null, slide, params, rId, slideLayouts, cb)
  slide.setSlideSize(presentation.width, presentation.height)
  return slide
}

//helper
function updateSpRefsForAnimation(slide: Slide, updatedSpRefs: Dict<string>) {
  const animationNodes = slide.animation.timeNodes

  for (const node of animationNodes) {
    const updatedRef = updatedSpRefs[node.spRefId]
    if (updatedRef != null) {
      node.spRefId = updatedRef
    }
  }
}
