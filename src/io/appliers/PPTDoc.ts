import { each } from '../../../liber/l/each'
import { Dict, Nullable } from '../../../liber/pervasive'
import { hookManager, Hooks } from '../../core/hooks'
import { SlideComments } from '../../core/Slide/Comments/SlideComments'
import { createNotes, Notes } from '../../core/Slide/Notes'
import { createNotesMaster, NotesMaster } from '../../core/Slide/NotesMaster'
import { Presentation } from '../../core/Slide/Presentation'
import { Slide } from '../../core/Slide/Slide'
import { SlideMaster } from '../../core/Slide/SlideMaster'
import {
  createDefaultMasterSlide,
  createDefaultSlideLayout,
} from '../../core/Slide/slideUtils'
import { Theme } from '../../core/SlideElement/attrs/theme'
import {
  DEFAULT_THEME_NAME,
  createDefaultTheme,
} from '../../core/SlideElement/attrs/creators'
import {
  reorderSlideMasters,
  reorderSlides,
} from '../../core/utilities/presentation/reorder'
import {
  FromJSONParams,
  HelperDataForFromJSON,
  PPTDocDataLayout,
  PPTDocDataLayoutTypeMap,
} from '../defs/pptdoc'
import { genPPTInfoData, getRefItemMap } from '../infoData'
import { Operation } from '../operation'

import { applyAndReadPresentation, PresentationRefsInfo } from './Presentation'

import { ResourcesContainerType } from '../../editor/utils/resourcesLoadManager'
import { consumeDeltaOperations, handleMapDataApplying } from './utils'
import { composeDelta } from '../../re/export/modoc'
import { applySlideLayout, genLayout } from './SlideLayout'
import { applySlideMaster } from './SlideMaster'
import { applySlide, genSlide } from './Slide'
import { applyThemes } from './Theme'
import {
  applyNotesMasters,
  genSlideNoteMasterFromInputData,
} from './NotesMaster'
import { applyNotes, genSlideNoteFromInputData } from './Notes'
import { applyComments } from './Comment'

const getDefaultTheme = (() => {
  let defaultTheme: Theme
  return (presentation: Presentation) => {
    defaultTheme = defaultTheme ?? createDefaultTheme(presentation)
    defaultTheme.presentation = presentation
    return defaultTheme
  }
})()

export function applyPPTDoc(
  changes: Operation[],
  currentOps: Operation[],
  presentation: Presentation,
  helperData: HelperDataForFromJSON
): Operation[] {
  const appliedOps = composeDelta(currentOps, changes)
  const currentInfo = helperData?.pptInfoData ?? genPPTInfoData(presentation)

  let refsInfo: PresentationRefsInfo = {
    slides: currentInfo.slides.map((slide) => slide.getRefId()),
    slideMasters: currentInfo.slideMasters.map((slideMaster) =>
      slideMaster.getRefId()
    ),
    notes: currentInfo.notes.map((note) => note.getRefId()),
    noteMasters: currentInfo.noteMasters.map((noteMaster) =>
      noteMaster.getRefId()
    ),
  }
  const params: FromJSONParams = {
    presentation,
    helperData,
  }

  const defaultTheme = getDefaultTheme(presentation)

  const oldSlideRefsOrder = refsInfo.slides.join(',')
  const oldSlideMasterRefsOrder = refsInfo.slideMasters.join(',')
  const currentSlides = getRefItemMap(currentInfo.slides)
  const currentLayouts = getRefItemMap(currentInfo.layouts)
  const addedSlides: Dict<Slide> = {}
  const deleteSlideRefs: Dict<boolean> = {}

  const themes: Dict<Theme> = {}
  const themeSlideMastersMap: Dict<Array<SlideMaster | NotesMaster>> = {}

  const oldNotesMasterRefsStr = refsInfo.noteMasters.slice().sort().join(',')
  const notesMasters = getRefItemMap(currentInfo.noteMasters)

  const notes = getRefItemMap(currentInfo.notes)
  const notesToMasterMap: Dict<string> = {}
  const notesToSlideMap: Dict<string> = {}
  const comments: Dict<Nullable<SlideComments>> = {}
  const commentsToSlideMap: Dict<string> = {}
  let shouldSentCommentChangeEvt = false

  consumeDeltaOperations(
    changes,
    appliedOps,
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    ({ opType: type, op, appliedOp }) => {
      switch (type) {
        case 'presentation': {
          refsInfo = applyAndReadPresentation(
            op.data['delta'],
            appliedOp?.data['delta'],
            presentation,
            refsInfo
          )
          emitGroupCollectedResources(type)

          break
        }
        case 'layouts': {
          handleMapDataApplying(
            op,
            null,
            currentLayouts,
            appliedOp,
            (rId, data) => {
              const newLayout = genLayout(data, presentation, params, rId)
              currentLayouts[rId] = newLayout
              emitGroupCollectedResources(type, rId)
            },
            (layout, data, appliedData, rId) => {
              layout.setSlideSize(presentation.width, presentation.height)
              applySlideLayout(data, appliedData, layout, rId, params)
              emitGroupCollectedResources(type, rId)
            }
          )
          break
        }

        case 'slideMasters': {
          const opData = op.data['slide']
          if (refsInfo.slideMasters && refsInfo.slideMasters.length > 0) {
            refsInfo.slideMasters.forEach((rId) => {
              const data = opData[rId]
              if (data) {
                const replaceIndex = presentation.slideMasters.findIndex(
                  (master) => master.getRefId() === rId
                )
                const master =
                  -1 < replaceIndex
                    ? presentation.slideMasters[replaceIndex]
                    : new SlideMaster(params.presentation)
                applySlideMaster(
                  data,
                  rId,
                  master,
                  currentLayouts,
                  params,
                  ({ themeRef }) => {
                    themeSlideMastersMap[themeRef] =
                      themeSlideMastersMap[themeRef] || []
                    if (themeSlideMastersMap[themeRef].indexOf(master) < 0) {
                      themeSlideMastersMap[themeRef].push(master)
                    }
                  }
                )

                master.setSlideSize(presentation.width, presentation.height)

                if (replaceIndex <= -1) {
                  master.theme = defaultTheme
                  presentation.slideMasters.push(master)
                }

                if (!presentation.isLoadComplete) {
                  // 保留第一次加载所有主题
                  presentation.preservedSlideMasters.push(master)
                }
              }

              emitGroupCollectedResources(type, rId)
            })
          }
          break
        }
        case 'slides': {
          handleMapDataApplying(
            op,
            refsInfo.slides,
            currentSlides,
            appliedOp,
            (rId, data) => {
              const newSlide = genSlide(
                data,
                presentation,
                params,
                rId,
                currentLayouts,
                ({ notesRef, commentsRef }) => {
                  if (notesRef) notesToSlideMap[notesRef] = rId
                  if (commentsRef) commentsToSlideMap[commentsRef] = rId
                }
              )

              addedSlides[rId] = newSlide

              emitGroupCollectedResources(type, rId)
            },
            (slide, data, oldData, rId) => {
              const result = applySlide(
                data,
                oldData,
                slide,
                params,
                null,
                currentLayouts,
                ({ notesRef, commentsRef }) => {
                  if (notesRef) notesToSlideMap[notesRef] = rId
                  if (commentsRef) commentsToSlideMap[commentsRef] = rId
                }
              )
              if (result === 'remove') {
                deleteSlideRefs[rId] = true
                shouldSentCommentChangeEvt = true
              }

              emitGroupCollectedResources(type, rId)
            }
          )
          break
        }
        case 'themes': {
          applyThemes(op, params, (rId, theme) => {
            themes[rId] = theme
            emitGroupCollectedResources(type, rId)
          })
          break
        }
        case 'notesMasters': {
          applyNotesMasters(
            op,
            appliedOp,
            notesMasters,
            defaultTheme,
            params,
            (rId, newNotesMaster) => {
              notesMasters[rId] = newNotesMaster
              emitGroupCollectedResources(type, rId)
            },
            themeSlideMastersMap
          )
          break
        }
        case 'notes': {
          applyNotes(
            op,
            appliedOp,
            notes,
            params,
            (rId, newNotes) => {
              notes[rId] = newNotes
              emitGroupCollectedResources(type, rId)
            },
            notesToMasterMap
          )
          break
        }
        case 'comments': {
          applyComments(op, appliedOp, params, (rId, slideComment) => {
            comments[rId] = slideComment
          })
          shouldSentCommentChangeEvt = true
          break
        }
      }
    }
  )

  // 设置默认值

  if (presentation.slideMasters.length === 0) {
    presentation.slideMasters[0] = createDefaultMasterSlide(
      presentation,
      defaultTheme
    )
  }
  if (presentation.slideMasters[0].layouts.length === 0) {
    presentation.slideMasters[0].layouts[0] = createDefaultSlideLayout(
      presentation.slideMasters[0]
    )
  }

  // 建立引用
  Object.keys(themeSlideMastersMap).forEach((rId) => [
    themeSlideMastersMap[rId].forEach((master) => {
      const theme = themes[rId] || presentation.themesMap[rId]
      if (theme) {
        master.setTheme(theme)
      }
    }),
  ])
  Object.keys(themes).forEach((rId) => {
    presentation.themesMap[rId] = themes[rId]
  })

  const slides: Dict<Slide> = {}
  // 同时删除 Slides 时, 需要修正多余 order 带来的影响，Todo: 确认无影响后下次 orderChange 时去除无用数据
  each(
    (slide, rId) => {
      if (deleteSlideRefs[rId] !== true) {
        slides[rId] = slide
      }
    },
    { ...currentSlides, ...addedSlides }
  )
  refsInfo.slides = refsInfo.slides.filter((rId) => {
    return !!slides[rId]
  })

  // notes 以slide的引用为准
  each((slideRef, notesRef) => {
    let notesMasterRef: Nullable<string> = notesToMasterMap[notesRef]
    let master: Nullable<NotesMaster> = notesMasters[notesMasterRef]
    let notesItem: Nullable<Notes> = notes[notesRef]
    if (notesItem == null) {
      const notesInfo = genSlideNoteFromInputData(appliedOps, notesRef, params)
      notesItem = notesInfo?.note

      notesMasterRef = notesInfo?.notesMasterRef
      if (notesMasterRef != null) {
        master = notesMasters[notesMasterRef]

        if (master == null) {
          master = genSlideNoteMasterFromInputData(
            appliedOps,
            notesMasterRef,
            defaultTheme,
            params,
            (rid, newNotesMaster) => {
              notesMasters[rid] = newNotesMaster
            },
            themeSlideMastersMap
          )
        }
      }
    }

    // set note & noteMaster relation
    if (notesItem && master) {
      notesItem.setNotesMaster(master)
    }
    const slide = slides[slideRef]
    if (notesItem && notesItem.master && slide) {
      slide.setNotes(notesItem)
      notesItem.setSlide(slide)
    }
  }, notesToSlideMap)

  // 增量更新时可以维护 cmommentRef: slideRef对应关系
  each((slideItem, slideRef) => {
    const slideComment = slideItem.slideComments
    if (slideComment) {
      const commentsRef = slideComment?.getRefId()
      if (commentsRef && !(commentsRef in commentsToSlideMap)) {
        commentsToSlideMap[commentsRef] = slideRef
      }
    }
  }, slides)

  // comments 引用
  each((slideRef, commentsRef) => {
    const slideComment = comments[commentsRef]
    const slide = slides[slideRef]
    if (slide && slideComment) {
      slideComment.setRefId(commentsRef)
      slideComment.setSlide(slide)
      slide.slideComments = slideComment // setSlideComments 会生成 changePoints
    }
  }, commentsToSlideMap)

  // update presentation.notesMasters
  const noteMastersRefs: string[] = Object.keys(notesMasters)
  if (oldNotesMasterRefsStr !== noteMastersRefs.slice().sort().join(',')) {
    presentation.notesMasters = noteMastersRefs
      .map((rid) => notesMasters[rid])
      .filter(Boolean)
  }

  if (presentation.notesMasters.length === 0) {
    presentation.notesMasters[0] = createNotesMaster()
    const notesTheme = defaultTheme.clone(`${DEFAULT_THEME_NAME}_NotesMaster`)
    notesTheme.presentation = presentation
    presentation.notesMasters[0].setTheme(notesTheme)
  }

  each((slide) => {
    if (!slide.notes) {
      slide.setNotes(createNotes())
      slide.notes!.setSlide(slide)
      slide.notes!.setNotesMaster(presentation.notesMasters[0])
    }
  }, slides)

  if (
    oldSlideRefsOrder !== refsInfo.slides.join(',') ||
    refsInfo.prSectionList !== undefined
  ) {
    reorderSlides(presentation, refsInfo.slides, slides, refsInfo.prSectionList)
  }

  if (oldSlideMasterRefsOrder !== refsInfo.slideMasters.join(',')) {
    reorderSlideMasters(presentation, refsInfo.slideMasters)
  }

  // Events, Make sure all data are latest
  if (shouldSentCommentChangeEvt) {
    hookManager.invoke(Hooks.Emit.EmitChangeComment)
  }
  return appliedOps
}

function emitGroupCollectedResources(
  type: ResourcesContainerType,
  refId?: string
) {
  hookManager.invoke(Hooks.Attrs.GroupCollectedResources, { type, refId })
}
