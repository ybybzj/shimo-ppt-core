//#region IInputStream

import { Operation } from './operation'

export interface IOAdapter<InputData> {
  fromOperations: (ops?: Operation[]) => InputData
  stringify: (inputData: InputData) => string
}

export function adaptIOAdapter<InputData>(
  adapter: IOAdapter<InputData>
): IOAdapter<InputData> {
  return {
    fromOperations: adapter['fromOperations'],
    stringify: adapter['stringify'],
  }
}
