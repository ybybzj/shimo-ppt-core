import { SlideMaster } from '../../core/Slide/SlideMaster'
import { assignVal, assignJSON } from '../utils'
import { Op, OpData, withType } from '../operation'
import { JSONFromCSld } from './csld'
import { SlideMasterAttrs, SlideMasterData } from '../dataType/slideMaster'

export function getSlideMasterAttrs(
  slideMaster: SlideMaster
): SlideMasterAttrs {
  const data: SlideMasterAttrs = {}
  assignVal(data, 'themeId', slideMaster.themeId)
  assignVal(data, 'preserve', slideMaster.preserve)
  assignJSON(data, 'clrMap', slideMaster.clrMap)
  assignJSON(data, 'hf', slideMaster.hf)
  assignJSON(data, 'txStyles', slideMaster.txStyles)
  assignVal(data, 'themeRef', slideMaster.theme?.getRefId())
  assignJSON(data, 'transition', slideMaster.transition)

  return data
}

export function JSONFromSlideMaster(slideMaster: SlideMaster): SlideMasterData {
  const attrs = Op.Insert(OpData.JSON(getSlideMasterAttrs(slideMaster)))

  const layoutRefs = Op.Insert(
    OpData.JSON({
      layoutRefs: slideMaster.layouts.map((layout) => layout.getRefId()),
    })
  )

  const csld = Op.Insert(OpData.Delta(JSONFromCSld(slideMaster.cSld)))

  return [
    withType(attrs, 'attrs'),
    withType(layoutRefs, 'layoutRefs'),
    withType(csld, 'csld'),
  ]
}
