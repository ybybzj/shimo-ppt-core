import { SlideLayout } from '../../core/Slide/SlideLayout'
import { assignVal, assignJSON } from '../utils'
import { Op, OpData, withType } from '../operation'
import { JSONFromCSld } from './csld'
import {
  SlideLayoutAttrData,
  SlideLayoutData,
  toSlideLayoutType,
} from '../dataType/slideLayout'
import { assignClrMapOverride } from '../../core/color/clrMap'

export function getSlideLayoutAttrs(layout: SlideLayout): SlideLayoutAttrData {
  const data: SlideLayoutAttrData = {}
  assignVal(data, 'matchingName', layout.matchingName)
  assignVal(data, 'preserve', layout.preserve)
  assignVal(data, 'showMasterPhAnim', layout.showMasterPhAnim)
  assignVal(data, 'showMasterSp', layout.showMasterSp)
  assignVal(data, 'userDrawn', layout.userDrawn)
  if (layout.type != null) {
    assignVal(data, 'type', toSlideLayoutType(layout.type))
  }

  assignClrMapOverride(data, 'clrMap', layout.clrMap)
  assignJSON(data, 'hf', layout.hf)

  assignJSON(data, 'transition', layout.transition)
  return data
}

export function JSONFromSlideLayout(layout: SlideLayout): SlideLayoutData {
  const attrs = Op.Insert(OpData.JSON(getSlideLayoutAttrs(layout)))

  const csld = Op.Insert(OpData.Delta(JSONFromCSld(layout.cSld)))

  return [withType(attrs, 'attrs'), withType(csld, 'csld')]
}
