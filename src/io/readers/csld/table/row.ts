import { TableRow } from '../../../../core/Table/TableRow'
import { TableCellInfo } from './util'
import {
  OpData,
  InsertOperationWithType,
  JSONOpData,
  DeltaOpData,
  withType,
  Op,
} from '../../../operation'
import { JSONFromTableCellInfo } from './cell'
import {
  TableRowData,
  TableRowAttrs,
  CellItemData,
} from '../../../dataType/table'
import { scaleValue } from '../../../utils'
import { ScaleOfPPTXSizes } from '../../../../core/common/const/unit'
import { Nullable } from '../../../../../liber/pervasive'

export function JSONFromTableRow(
  row: TableRow,
  cellsInfo: TableCellInfo[]
): TableRowData {
  const rowHeight = getTableRowHeight(row)
  const attrsData = OpData.JSON(
    rowHeight != null ? { ['height']: rowHeight } : {}
  )

  const cellsData = OpData.Delta(cellsInfo.map(JSONFromTableCellInfo))

  const rowData: [
    InsertOperationWithType<'attrs', JSONOpData<TableRowAttrs>>,
    InsertOperationWithType<'cells', DeltaOpData<CellItemData[]>>,
  ] = [
    withType(Op.Insert(attrsData), 'attrs'),
    withType(Op.Insert(cellsData), 'cells'),
  ]

  return Op.Insert(OpData.Delta(rowData), { ['type']: 'row' })
}

export function getTableRowHeight(row: TableRow): Nullable<number> {
  if (row.pr.height != null) {
    let maxTopMargin = 0,
      maxBottomMargin = 0,
      maxTopBorder = 0,
      maxBottomBorder = 0
    for (let i = 0, l = row.children.length; i < l; ++i) {
      const cell = row.children.at(i)!
      const margins = cell.getMargins()
      if (margins.bottom!.w > maxBottomMargin) {
        maxBottomMargin = margins.bottom!.w
      }
      if (margins.top!.w > maxTopMargin) {
        maxTopMargin = margins.top!.w
      }
      const borders = cell.getBorders()
      if (borders.top.size! > maxTopBorder) {
        maxTopBorder = borders.top.size!
      }
      if (borders.bottom.size! > maxBottomBorder) {
        maxBottomBorder = borders.bottom.size!
      }
    }

    return scaleValue(
      row.pr.height.value +
        maxBottomMargin +
        maxTopMargin +
        maxTopBorder / 2 +
        maxBottomBorder / 2,
      ScaleOfPPTXSizes
    )
  }
}
