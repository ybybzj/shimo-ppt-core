import { TableCellInfo } from './util'
import {
  TableCellData,
  TableEmptyCellData,
  CellItemData,
  TableCellAttrs,
} from '../../../dataType/table'
import {
  Op,
  OpData,
  withType,
  InsertOperationWithType,
  JSONOpData,
  DeltaOpData,
  Operation,
} from '../../../operation'

import { assignVal, assignJSON } from '../../../utils'
import { JSONFromContentOnlyTextBody } from '../text'
import { TextBodyData } from '../../../dataType/text'
import { opsFromUpdate } from '../../changes/cchangesToOps/helpers'
import {
  TableCellDataLayout,
  TableCellDataLayoutMap,
} from '../../../defs/csld/table'
import { TextBodyDataLayout } from '../../../defs/csld/text'

export function JSONFromTableCellInfo(cellInfo: TableCellInfo): CellItemData {
  if (cellInfo.isEmpty === true) {
    return JSONFromEmptyCell()
  } else {
    return JSONFromTableCell(cellInfo)
  }
}

function JSONFromTableCell(info: TableCellInfo): TableCellData {
  const { cell } = info
  const cellAttrs: TableCellAttrs = {}

  assignVal(cellAttrs, 'gridSpan', info.gridSpan)

  assignVal(cellAttrs, 'rowSpan', info.rowSpan)

  if (info.vMerge === true) {
    assignVal(cellAttrs, 'vMerge', true)
  }

  assignJSON(cellAttrs, 'pr', cell.pr)

  const attrsData = OpData.JSON(cellAttrs)

  const contentData = OpData.Delta(JSONFromContentOnlyTextBody(cell.textDoc))

  const cellData: [
    InsertOperationWithType<'attrs', JSONOpData<TableCellAttrs>>,
    InsertOperationWithType<'txBody', DeltaOpData<TextBodyData>>
  ] = [
    withType(Op.Insert(attrsData), 'attrs'),
    withType(Op.Insert(contentData), 'txBody'),
  ]

  return Op.Insert(OpData.Delta(cellData), { ['type']: 'cell' })
}

function JSONFromEmptyCell(): TableEmptyCellData {
  const attrs: {
    type: 'cell'
    isEmpty: true
  } = { ['type']: 'cell', ['isEmpty']: true }

  return Op.Insert(1, attrs)
}

export function opsFromUpdateTableCellInfo(
  info: TableCellInfo,
  subType: 'Attrs' | 'Content'
): Operation | undefined {
  // 按照目前的情况不可能产生单独变更为 emptyCell 的情况, 否则丢弃变更
  if (info.isEmpty === true) return
  const { cell } = info

  let cellInnerOp: Operation[] = []
  if (subType === 'Attrs') {
    const cellAttrs: TableCellAttrs = {}
    assignVal(cellAttrs, 'gridSpan', info.gridSpan)
    assignVal(cellAttrs, 'rowSpan', info.rowSpan)
    if (info.vMerge === true) assignVal(cellAttrs, 'vMerge', true)
    assignJSON(cellAttrs, 'pr', cell.pr)
    cellInnerOp = opsFromUpdate<
      typeof TableCellDataLayout,
      typeof TableCellDataLayoutMap,
      'attrs',
      TableCellAttrs
    >(TableCellDataLayout, TableCellDataLayoutMap, 'attrs', cellAttrs)
  } else {
    const contentData = JSONFromContentOnlyTextBody(cell.textDoc)
    const txBodyOp = [Op.Remove(TextBodyDataLayout.length), ...contentData]
    cellInnerOp = opsFromUpdate(
      TableCellDataLayout,
      TableCellDataLayoutMap,
      'txBody',
      txBodyOp
    )
  }
  return withType(Op.Retain(OpData.Delta(cellInnerOp)), 'cell')
}
