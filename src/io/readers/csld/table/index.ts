import { Table } from '../../../../core/Table/Table'
import { TableData, TablePrData, TableRowData } from '../../../dataType/table'
import { generateTableGridInfo } from './util'
import { JSONFromTableRow } from './row'
import { withType, Op, OpData } from '../../../operation'
import { scaleValue, toJSON } from '../../../utils'
import { ScaleOfPPTXSizes } from '../../../../core/common/const/unit'
import { gGlobalTableStyles } from '../../../../core/Slide/GlobalTableStyles'
import { TableStyle } from '../../../../core/Table/attrs/TableStyle'
import { Nullable } from '../../../../../liber/pervasive'

export function JSONFromTable(table: Table): TableData {
  const attrsData = JSONFromTableAttrs(table)

  // grid
  const grid = table.tableGrid
  const gridData = {
    ['grid']: grid.map((n) => scaleValue(n, ScaleOfPPTXSizes)!),
  }

  const rowsData = JSONFromTableRows(table)

  return [
    withType(Op.Insert(OpData.JSON(attrsData)), 'attrs'),
    withType(Op.Insert(OpData.JSON(gridData)), 'grid'),
    withType(Op.Insert(OpData.Delta(rowsData)), 'rows'),
  ]
}

export function JSONFromTableAttrs(table: Table): TablePrData {
  const data: TablePrData = {
    ['look']: toJSON(table.tableLook),
  }

  const style: Nullable<TableStyle> = table.tableStyleId
    ? gGlobalTableStyles.getTableStyle(table.tableStyleId)
    : null
  if (style) {
    data['style'] = {
      ['id']: style.guid,
    }
  }

  const shd = table.pr.shd
  if (shd != null && shd.fillEffects) {
    data['fill'] = toJSON(shd.fillEffects)
  }

  return data
}

function JSONFromTableRows(table: Table): TableRowData[] {
  const gridInfo = generateTableGridInfo(table)
  const result: TableRowData[] = []
  for (let i = 0; i < gridInfo.Rows.length; i++) {
    const rowData = JSONFromTableRow(
      table.children.at(i)!,
      gridInfo.Rows[i].Cells
    )
    result.push(rowData)
  }
  return result
}
