import { Table } from '../../../../core/Table/Table'
import { TableCell } from '../../../../core/Table/TableCell'
import { ManagedSliceUtil } from '../../../../common/managedArray'
import { VMergeType } from '../../../../core/common/const/table'

export interface TableCellInfo {
  cell: TableCell
  gridSpan?: number
  rowSpan?: number
  vMerge: boolean
  isEmpty: boolean
}

interface TableGirdInfo {
  Rows: Array<{ Cells: TableCellInfo[] }>
}

export function generateTableGridInfo(table: Table): TableGirdInfo {
  const rows = table.children
  const cols = table.tableGrid

  const rowLen = rows.length
  const colLen = cols.length

  const gridInfo: TableGirdInfo = {
    Rows: new Array(rowLen),
  }

  // collect cells info by row
  for (let i = 0; i < rowLen; i++) {
    const row = rows.at(i)!
    const cells: TableCellInfo[] = []

    gridInfo.Rows[i] = { Cells: cells }

    ManagedSliceUtil.forEach(row.children, (cell) => {
      const cellInfo: TableCellInfo = {
        cell,
        vMerge: false,
        isEmpty: false,
      }

      if (cell.pr.gridSpan != null && cell.pr.gridSpan > 1) {
        cellInfo.gridSpan = cell.pr.gridSpan
      }
      if (cell.pr.vMerge === VMergeType.Continue) {
        cellInfo.vMerge = true
      }

      cells.push(cellInfo)

      // fill in empty cells
      if (cellInfo.gridSpan != null && cellInfo.gridSpan > 1) {
        for (let t = cellInfo.gridSpan - 1; t > 0; t--) {
          const emptyCellInfo: TableCellInfo = {
            cell,
            vMerge: cellInfo.vMerge,
            isEmpty: true,
          }

          cells.push(emptyCellInfo)
        }
      }
    })
  }

  // update rowSpan info
  for (let colIndex = 0; colIndex < colLen; colIndex++) {
    let rowIndex = 0

    while (rowIndex < rowLen) {
      let rowCount = 1
      for (let i = rowIndex + 1; i < rowLen; i++) {
        const CellsInRow = gridInfo.Rows[i].Cells
        if (colIndex >= CellsInRow.length) {
          // invalid cell index
          continue
        }

        const cellInfo = CellsInRow[colIndex]
        if (cellInfo.vMerge !== true) {
          break
        }

        rowCount += 1
      }

      if (rowCount > 1 && colIndex < gridInfo.Rows[rowIndex].Cells.length) {
        gridInfo.Rows[rowIndex].Cells[colIndex].rowSpan = rowCount
      }
      rowIndex += rowCount
    }
  }

  return gridInfo
}
