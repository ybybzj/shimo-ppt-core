import { OleObject } from "../../../core/SlideElement/OleObject"
import { ImageShapeData, OLEObjectData } from "../../dataType/csld"
import { SpTypes } from "../../dataType/spAttrs"
import { DeltaOpData, EmptyOpData, Op, OpData, withType } from "../../operation"
import { toJSON } from "../../utils"

export function JSONFromOleObject(
  oleObject: OleObject
): OLEObjectData {
  const nvPicPrData = oleObject.nvPicPr
    ? OpData.JSON({ nvPicPr: toJSON(oleObject.nvPicPr, SpTypes.Pic) })
    : EmptyOpData.JSON

  const blipFillJSON = oleObject.blipFill && toJSON(oleObject.blipFill)
  const blipFillData = blipFillJSON
    ? OpData.JSON({ blipFill: blipFillJSON })
    : EmptyOpData.JSON
  const spPrData = oleObject.spPr
    ? OpData.JSON({ spPr: toJSON(oleObject.spPr) })
    : EmptyOpData.JSON
  const dataOfStyle = oleObject.style
    ? OpData.JSON(toJSON(oleObject.style))
    : EmptyOpData.JSON

  const imageShapeData: DeltaOpData<ImageShapeData> = OpData.Delta([
    withType(Op.Insert(nvPicPrData), 'nvPicPr'),
    withType(Op.Insert(blipFillData), 'blipFill'),
    withType(Op.Insert(spPrData), 'spPr'),
    withType(Op.Insert(dataOfStyle), 'style'),
  ])

  const oleAttrsData = OpData.JSON(oleObject.attrsToJSON())

  return [
    withType(Op.Insert(oleAttrsData), 'attrs'),
    withType(Op.Insert(imageShapeData), 'pic')
  ]
}
