import { ChartObject } from '../../../core/SlideElement/ChartObject'
import { ChartObjectData, ImageShapeData } from '../../dataType/csld'
import { SpTypes } from '../../dataType/spAttrs'
import { DeltaOpData, EmptyOpData, Op, OpData, withType } from '../../operation'
import { toJSON } from '../../utils'

export function JSONFromChartObject(chartObject: ChartObject): ChartObjectData {
  const nvPicPrData = chartObject.nvPicPr
    ? OpData.JSON({ nvPicPr: toJSON(chartObject.nvPicPr, SpTypes.Pic) })
    : EmptyOpData.JSON

  const blipFillJSON = chartObject.blipFill && toJSON(chartObject.blipFill)
  const blipFillData = blipFillJSON
    ? OpData.JSON({ blipFill: blipFillJSON })
    : EmptyOpData.JSON
  const spPrData = chartObject.spPr
    ? OpData.JSON({ spPr: toJSON(chartObject.spPr) })
    : EmptyOpData.JSON
  const dataOfStyle = chartObject.style
    ? OpData.JSON(toJSON(chartObject.style))
    : EmptyOpData.JSON

  const imageShapeData: DeltaOpData<ImageShapeData> = OpData.Delta([
    withType(Op.Insert(nvPicPrData), 'nvPicPr'),
    withType(Op.Insert(blipFillData), 'blipFill'),
    withType(Op.Insert(spPrData), 'spPr'),
    withType(Op.Insert(dataOfStyle), 'style'),
  ])

  const chartAttrsData = OpData.JSON(chartObject.attrsToJSON())

  return [
    withType(Op.Insert(chartAttrsData), 'attrs'),
    withType(Op.Insert(imageShapeData), 'pic'),
  ]
}
