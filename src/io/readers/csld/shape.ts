import { withType, Op, OpData, EmptyOpData } from '../../operation'
import { JSONFromTextBody } from './text'
import { assignVal, toJSON } from '../../utils'
import { ShapeAttrsData, ShapeData } from '../../dataType/csld'
import { TextBodyData } from '../../dataType/text'
import { SpTypes } from '../../dataType/spAttrs'
import { Shape } from '../../../core/SlideElement/Shape'

export function getShapeAttris(_shape: Shape): ShapeAttrsData {
  const data: ShapeAttrsData = {}
  if (_shape.attrUseBgFill === true) {
    assignVal(data, 'useBgFill', true)
  }
  return data
}

export function JSONFromShape(_shape: Shape): ShapeData {
  const attrsData = OpData.JSON(getShapeAttris(_shape))
  const nvSpPrData = OpData.JSON({
    nvSpPr: _shape.nvSpPr ? toJSON(_shape.nvSpPr, SpTypes.Shape) : {},
  })
  const spPrData = OpData.JSON({
    spPr: _shape.spPr ? toJSON(_shape.spPr) : {},
  })
  const dataOfStyle = OpData.JSON(_shape.style ? toJSON(_shape.style) : {})
  const data = _shape.txBody ? JSONFromTextBody(_shape.txBody) : []
  const txBodyData =
    data.length > 0 ? OpData.Delta(data as TextBodyData) : EmptyOpData.Delta

  return [
    withType(Op.Insert(attrsData), 'attrs'),
    withType(Op.Insert(nvSpPrData), 'nvSpPr'),
    withType(Op.Insert(spPrData), 'spPr'),
    withType(Op.Insert(dataOfStyle), 'style'),
    withType(Op.Insert(txBodyData), 'txBody'),
  ]
}
