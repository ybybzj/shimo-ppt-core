import { GraphicFrame } from '../../../core/SlideElement/GraphicFrame'
import { GraphicFrameAttrs, GraphicFrameData } from '../../dataType/csld'
import { assignJSON } from '../../utils'
import { JSONFromTable } from './table'
import { Table } from '../../../core/Table/Table'
import { OpData, withType, Op, EmptyOpData } from '../../operation'
import { SpTypes } from '../../dataType/spAttrs'
import { InstanceType, isInstanceTypeOf } from '../../../core/instanceTypes'

export function JSONFromGraphicFrame(
  graphicFrame: GraphicFrame
): GraphicFrameData {
  const attrsData = OpData.JSON(JSONFromGraphicFrameAttrs(graphicFrame))

  const table = graphicFrame.graphicObject
  const isTable = isInstanceTypeOf(table, InstanceType.Table)
  const graphicData = isTable
    ? OpData.Delta(JSONFromTable(table as Table))
    : EmptyOpData.Delta

  return [
    withType(Op.Insert(attrsData), 'attrs'),
    withType(
      Op.Insert(graphicData, isTable ? { type: 'table' } : undefined),
      'graphic'
    ),
  ]
}

export function JSONFromGraphicFrameAttrs(
  graphicFrame: GraphicFrame
): GraphicFrameAttrs {
  const data: GraphicFrameAttrs = {}
  assignJSON(
    data,
    'nvGraphicFramePr',
    graphicFrame.nvGraphicFramePr,
    SpTypes.GraphicFrame
  )
  assignJSON(data, 'xfrm', graphicFrame.spPr?.xfrm)
  return data
}
