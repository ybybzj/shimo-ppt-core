import { withType, Op, OpData } from '../../operation'
import { CxnShapeData } from '../../dataType/csld'
import { SpTypes } from '../../dataType/spAttrs'
import { ConnectionShape } from '../../../core/SlideElement/ConnectionShape'
import { toJSON } from '../../utils'

export function JSONFromConnectionShape(
  _shape: ConnectionShape
): CxnShapeData {
  const nvCxnSpPr = OpData.JSON({
    nvSpPr: _shape.nvSpPr ? toJSON(_shape.nvSpPr, SpTypes.ConnectionShape) : {},
  })
  const spPrData = OpData.JSON({
    spPr: _shape.spPr ? toJSON(_shape.spPr) : {},
  })
  const dataOfStyle = OpData.JSON(_shape.style ? toJSON(_shape.style) : {})

  return [
    withType(Op.Insert(nvCxnSpPr), 'nvCxnSpPr'),
    withType(Op.Insert(spPrData), 'spPr'),
    withType(Op.Insert(dataOfStyle), 'style'),
  ]
}
