import { assignVal, assignJSON, toJSON } from '../../utils'
import { Op, OpData, withType } from '../../operation'
import { JSONFromShape } from './shape'
import { Shape } from '../../../core/SlideElement/Shape'
import { Dict } from 'liber/pervasive'
import {
  CSldAttrData,
  CSldData,
  SpItem,
  GrpShapeData,
} from '../../dataType/csld'
import { JSONFromImageShape } from './image'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { SlideElement } from '../../../core/SlideElement/type'
import { GraphicFrame } from '../../../core/SlideElement/GraphicFrame'
import { JSONFromGraphicFrame } from './graphicFrame'
import { InstanceType } from '../../../core/instanceTypes'
import { SpTypes } from '../../dataType/spAttrs'
import { ConnectionShape } from '../../../core/SlideElement/ConnectionShape'
import { JSONFromConnectionShape } from './connectionShape'
import { CSld } from '../../../core/Slide/CSld'
import { JSONFromOleObject } from './oleObject'
import { JSONFromChartObject } from './chartObject'

export function getCSldAttr(csld: CSld): CSldAttrData {
  const data: CSldAttrData = {}
  assignVal(data, 'name', csld.name)
  assignJSON(data, 'bg', csld.bg)
  return data
}

export function JSONFromSpItem(
  shape: SlideElement,
  order: number
): SpItem | undefined {
  let spData: SpItem | undefined
  switch (shape.instanceType) {
    case InstanceType.Shape: {
      const isConnectionShape = shape.isConnectionShape()
      if (isConnectionShape) {
        spData = [
          Op.Insert(
            OpData.Delta(JSONFromConnectionShape(shape as ConnectionShape)),
            {
              order,
              spType: 'cxnSp',
            }
          ),
        ]
      } else {
        spData = [
          Op.Insert(OpData.Delta(JSONFromShape(shape as Shape)), {
            order,
            spType: 'sp',
          }),
        ]
      }
      break
    }
    case InstanceType.GroupShape: {
      spData = [
        Op.Insert(OpData.Delta(JSONFromGroupShape(shape as GroupShape)), {
          order,
          spType: 'grpSp',
        }),
      ]
      break
    }
    case InstanceType.ImageShape: {
      if (shape.isOleObject()) {
        spData = [
          Op.Insert(OpData.Delta(JSONFromOleObject(shape)), {
            order,
            spType: 'ole',
          }),
        ]
      } else if (shape.isChartObject()) {
        spData = [
          Op.Insert(OpData.Delta(JSONFromChartObject(shape)), {
            order,
            spType: 'chart',
          }),
        ]
      } else {
        spData = [
          Op.Insert(OpData.Delta(JSONFromImageShape(shape)), {
            order,
            spType: 'pic',
            subType: shape.subType,
          }),
        ]
      }
      break
    }
    case InstanceType.GraphicFrame: {
      spData = [
        Op.Insert(OpData.Delta(JSONFromGraphicFrame(shape as GraphicFrame)), {
          order,
          spType: 'graphicFrame',
        }),
      ]
      break
    }
  }
  return spData
}

export function JSONFromGroupShape(_shape: GroupShape): GrpShapeData {
  const nvGrpSpPrData = OpData.JSON({
    nvGrpSpPr: _shape.nvGrpSpPr
      ? toJSON(_shape.nvGrpSpPr, SpTypes.GroupShape)
      : {},
  })
  const grpSpPrData = OpData.JSON({
    grpSpPr: _shape.spPr ? toJSON(_shape.spPr) : {},
  })

  const shapesData: Dict<SpItem> = JSONFromShapes(_shape.spTree)
  const spTreeData = OpData.Map(shapesData)

  return [
    withType(Op.Insert(nvGrpSpPrData), 'nvGrpSpPr'),
    withType(Op.Insert(grpSpPrData), 'grpSpPr'),
    withType(Op.Insert(spTreeData), 'spTree'),
  ]
}

export function JSONFromCSld(csld: CSld): CSldData {
  const attrs = Op.Insert(OpData.JSON(getCSldAttr(csld)))
  const shapesData = JSONFromShapes(csld.spTree)
  const shapes = Op.Insert(OpData.Map(shapesData))

  return [withType(attrs, 'attrs'), withType(shapes, 'spTree')]
}

export function JSONFromShapes(shapes: any[]) {
  const shapesData: Dict<SpItem> = shapes.reduce((result, shape, index) => {
    const rId = shape.getRefId()
    const spData = JSONFromSpItem(shape, index)

    if (spData) {
      result[rId] = spData
    }

    return result
  }, {})
  return shapesData
}
