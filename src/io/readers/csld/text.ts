import { Operation, OpData, withType, Op, EmptyOpData } from '../../operation'
import { Nullable } from '../../../../liber/pervasive'
import { opsFromContentItems } from '../../docContent'
import { TextDocument } from '../../../core/TextDocument/TextDocument'
import { TextBody } from '../../../core/SlideElement/TextBody'
import { TextBodyData } from '../../dataType/text'
import { toJSON } from '../../utils'

export type TextContentData = Operation[]

export function JSONFromCDocumentContent(
  textDoc: Nullable<TextDocument>
): TextContentData {
  return textDoc != null ? opsFromContentItems(textDoc.children.elements) : []
}

export function JSONFromTextBody(txBody: TextBody): TextBodyData {
  const bodyPrData = OpData.JSON(txBody.bodyPr ? toJSON(txBody.bodyPr) : {})

  const listStyleData = OpData.JSON({
    listStyle: txBody.lstStyle ? toJSON(txBody.lstStyle) : [],
  })

  const textContentData = OpData.Delta(
    txBody.textDoc ? JSONFromCDocumentContent(txBody.textDoc) : []
  )

  return [
    withType(Op.Insert(bodyPrData), 'bodyPr'),
    withType(Op.Insert(listStyleData), 'listStyle'),
    withType(Op.Insert(textContentData), 'docContent'),
  ]
}

export function JSONFromContentOnlyTextBody(
  content: TextDocument
): TextBodyData {
  const textContentData = OpData.Delta(
    content ? JSONFromCDocumentContent(content) : []
  )
  return [
    withType(Op.Insert(EmptyOpData.JSON), 'bodyPr'),
    withType(Op.Insert(EmptyOpData.JSON), 'listStyle'),
    withType(Op.Insert(textContentData), 'docContent'),
  ]
}
