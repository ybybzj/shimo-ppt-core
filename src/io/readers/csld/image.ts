import { withType, Op, OpData, EmptyOpData } from '../../operation'
import { ImageShapeData } from '../../dataType/csld'
import { SpTypes } from '../../dataType/spAttrs'
import { ImageShape } from '../../../core/SlideElement/Image'
import { toJSON } from '../../utils'

export function JSONFromImageShape(
  _imageShape: ImageShape
): ImageShapeData {
  const nvPicPrData = _imageShape.nvPicPr
    ? OpData.JSON({ nvPicPr: toJSON(_imageShape.nvPicPr, SpTypes.Pic) })
    : EmptyOpData.JSON

  const blipFillJSON = _imageShape.blipFill && toJSON(_imageShape.blipFill)
  const blipFillData = blipFillJSON
    ? OpData.JSON({ blipFill: blipFillJSON })
    : EmptyOpData.JSON
  const spPrData = _imageShape.spPr
    ? OpData.JSON({ spPr: toJSON(_imageShape.spPr) })
    : EmptyOpData.JSON
  const dataOfStyle = _imageShape.style
    ? OpData.JSON(toJSON(_imageShape.style))
    : EmptyOpData.JSON

  return [
    withType(Op.Insert(nvPicPrData), 'nvPicPr'),
    withType(Op.Insert(blipFillData), 'blipFill'),
    withType(Op.Insert(spPrData), 'spPr'),
    withType(Op.Insert(dataOfStyle), 'style'),
  ]
}
