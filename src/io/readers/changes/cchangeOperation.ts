import { IChangeItem } from '../../../core/changes/EditChangesStack'

import { descriptionFromIdentityFlag } from '../../../core/FlagDescriptions'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { ClassObjectWithId, getAncestorInfo } from './helper'
import {
  ChangeCategory,
  EditChange,
  EntityForChange,
} from '../../../core/changes/type'
import { ActionPoint } from '../../../collaboration/changesCollector'
import { SpUpdateUnit } from '../../../core/changes/updateChange'
import { OrderChange } from '../../../core/changes/orderChange'

export interface ChangeOperation {
  type: EditActionFlag
  typeDescription: string
  operationType: ChangeCategory
  targetId: string
  target: EntityForChange
  targetParents: ClassObjectWithId[]
  change: EditChange
  actionType: EditActionFlag
  actionDescription: string
  updateUnit?: SpUpdateUnit
}

export function ChangeItemToOperation(
  cc: IChangeItem,
  description: EditActionFlag,
  actionType: EditActionFlag,
  addtionInfo?: {
    updateUnit?: SpUpdateUnit
  }
): ChangeOperation {
  const desc =
    cc.changeRecord.editType !== EditActionFlag.edit_Unknown_Unknown
      ? cc.changeRecord.editType
      : description
  const operation: ChangeOperation = {
    type: desc,
    typeDescription: descriptionFromIdentityFlag(desc),
    actionType,
    actionDescription: descriptionFromIdentityFlag(actionType),
    operationType: cc.changeRecord.Category,
    targetId: cc.entity.id,
    target: cc.entity,
    targetParents: getAncestorInfo(cc.entity),
    change: cc.changeRecord,
  }
  if (addtionInfo?.updateUnit) {
    operation.updateUnit = addtionInfo?.updateUnit
  }
  return operation
}

export function OrderChangeOperation(
  target: EntityForChange,
  flag: EditActionFlag,
  action: EditActionFlag
): ChangeOperation {
  const change = new OrderChange(target, flag)
  return ChangeItemToOperation(
    {
      entity: change.targetEntity,
      changeRecord: change,
    },
    flag,
    action
  )
}

export interface ChangesOfAction {
  type: EditActionFlag
  description: string
  cchangeOperations: ChangeOperation[]
}

export function changePointToAction(cp: ActionPoint): ChangesOfAction {
  return {
    type: cp.type,
    description: cp.description,
    cchangeOperations: cp.cchanges.map((cchange) =>
      ChangeItemToOperation(cchange, cp.type, cp.type)
    ),
  }
}
