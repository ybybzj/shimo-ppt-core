import { Slide } from '../../../../core/Slide/Slide'
import {
  opsFromUpdateSlideShapes,
  opsFromUpdate,
  opsFromUpdates,
  keysFromMapData,
  opsFromUpdateSlideMasterRefs,
  getSlideLayoutRefsFromIOData,
} from './helpers'
import { SlideDataLayout, SlideDataLayoutTypeMap } from '../../../defs/Slide'
import { SlideElement } from '../../../../core/SlideElement/type'
import {
  PresentationDataLayout,
  PresentationDataLayoutTypeMap,
} from '../../../defs/presentation'
import { PPTDocDataLayout, PPTDocDataLayoutTypeMap } from '../../../defs/pptdoc'
import {
  Operation,
  Op,
  OpData,
  getOperationDataType,
  lengthOfOperation,
  OperationDataType,
  lengthOfOperations,
} from '../../../operation'
import { CollaborationManager } from '../../../../collaboration/collaborationManager'
import { NotesDataLayout } from '../../../defs/Notes'
import { CommentsDataLength } from '../../../dataType/comment'
import { SlideComments } from '../../../../core/Slide/Comments/SlideComments'
import { Comment } from '../../../../core/Slide/Comments/Comment'
import { Presentation } from '../../../../core/Slide/Presentation'
import { SlideMaster } from '../../../../core/Slide/SlideMaster'
import { SlideMasterDataLayout } from '../../../defs/SlideMaster'
import { SlideLayoutDataLayout } from '../../../defs/SlideLayout'
import { CSldDataLayout, CSldDataLayoutTypeMap } from '../../../defs/csld'
import { DataLayoutOpMap } from '../../../loaders/utils'

import { Nullable } from '../../../../../liber/pervasive'

export function opsFromRemoveShape(
  slide: Slide,
  removedShape: SlideElement,
  collabManager: CollaborationManager<any>
): Operation[] {
  if (!slide || !removedShape) {
    return []
  }
  const removedLength = getRemoveLenForSp(removedShape, slide, collabManager)
  const shapeOps = [Op.Remove(removedLength)]
  const shapeRId = removedShape.getRefId()

  return opsFromUpdateSlideShapes(slide, {
    [shapeRId]: shapeOps,
  })
}

function opFromDeltaByOpType<
  L extends readonly string[],
  OT extends L[number],
  M extends { [key in L[number]]: OperationDataType },
>(
  ops: Operation[],
  opType: OT,
  dataLayout: L,
  dataTypeLayoutMap: M
): Nullable<DataLayoutOpMap<L, M>[OT]> {
  const opLen = ops.length
  let ret: Nullable<Operation>
  let i = 0
  while (i < opLen && i < dataLayout.length) {
    const op = ops[i]
    // Todo, check op
    const curOpType = op.attributes && op.attributes['opType']
    if (curOpType === opType) {
      const expectedDataType = dataTypeLayoutMap[opType]
      const dataType = getOperationDataType(op.data)
      if (
        op.action === 'insert' &&
        (dataType === 'delta' || dataType === 'map') &&
        expectedDataType === dataType
      ) {
        ret = op
        break
      }
    }
    i += lengthOfOperation(op)
  }
  return ret as Nullable<DataLayoutOpMap<L, M>[OT]>
}
function getRemoveLenForSp(
  sp: SlideElement,
  slide: Slide,
  collabManager: CollaborationManager<any>
): number {
  const spRid = sp.getRefId()
  const pptData = collabManager.currentPptIOData()
  if (pptData == null) {
    return 1
  }

  const slidesOp = opFromDeltaByOpType(
    pptData,
    'slides',
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap
  )

  const slideDelta =
    slidesOp && slide.refId ? slidesOp.data.slide[slide.refId] : undefined

  const csldOp = slideDelta
    ? opFromDeltaByOpType(
        slideDelta,
        'csld',
        SlideDataLayout,
        SlideDataLayoutTypeMap
      )
    : undefined

  const spTreeDelta = csldOp
    ? opFromDeltaByOpType(
        csldOp.data.delta,
        'spTree',
        CSldDataLayout,
        CSldDataLayoutTypeMap
      )
    : undefined

  const spDelta = spTreeDelta ? spTreeDelta.data.slide[spRid] : undefined

  return spDelta ? lengthOfOperations(spDelta) : 1
}
export function opsFromRemoveSlide(
  slide: Slide,
  collabManager: CollaborationManager<any>
): Operation[] {
  if (!slide) {
    return []
  }
  const IODataBeforeRead = collabManager.currentPptIOData() ?? []
  const ioDataLength = lengthOfOperations(IODataBeforeRead)

  const updates = {}
  const slideRId = slide.getRefId()
  const slideUpdateOps = {
    [slideRId]: [Op.Remove(SlideDataLayout.length)],
  }
  updates['slides'] = slideUpdateOps

  const notes = slide.notes
  if (notes && !notes.isEmptyNotes()) {
    // 删除notes

    const notesRefsInIOData = keysFromMapData(
      IODataBeforeRead,
      'notes',
      PPTDocDataLayout,
      PPTDocDataLayoutTypeMap
    )

    const notesRef = notes.getRefId()
    // 保险起见,和当前IO的数据进行校准,避免生成多余的删除操作
    if (notesRefsInIOData.indexOf(notesRef) > -1) {
      // notes updates
      const notesUpdateOps = {
        [notesRef]: [Op.Remove(NotesDataLayout.length)],
      }

      updates['notes'] = notesUpdateOps
    }
  }

  // comments update
  const slideComment = slide.slideComments
  if (slideComment && slideComment.comments.length) {
    const slideCommentRef = slideComment.getRefId()
    updates['comments'] = { [slideCommentRef]: [Op.Remove(CommentsDataLength)] }
  }

  return opsFromUpdates(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    ioDataLength,
    updates
  )
}

export function opsFromRemoveSlideOrder(slide: Slide): Operation[] {
  if (!slide) {
    return []
  }
  const slideRId = slide.getRefId()
  const opsFromPresentation = opsFromUpdate(
    PresentationDataLayout,
    PresentationDataLayoutTypeMap,
    'Slides',
    {
      [slideRId]: null,
    }
  )

  return opsFromUpdate(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    'presentation',
    opsFromPresentation
  )
}

export function opsFromRemoveComment(
  slideComment: SlideComments,
  comment: Comment,
  collabManager: CollaborationManager<any>
): Operation[] {
  // 即使删除至空, 也不使用 remove
  const slideCommentRef = slideComment.getRefId()
  const commentId = comment.getUid()
  const IODataBeforeRead = collabManager.currentPptIOData()
  const ioDataLength = lengthOfOperations(IODataBeforeRead)
  const updates = {}
  const commentOps = [Op.Retain(OpData.JSON({ [commentId]: null }))]
  updates['comments'] = { [slideCommentRef]: commentOps }
  return opsFromUpdates(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    ioDataLength,
    updates
  )
}

export function opsFromRemoveSlideMaster(
  presentation: Presentation,
  slideMaster: SlideMaster,
  collabManager: CollaborationManager<any>
): Operation[] {
  if (!slideMaster) return []
  const updates = {}
  const slideMasterRId = slideMaster.getRefId()

  const IODataBeforeRead = collabManager.currentPptIOData() ?? []
  const ioDataLength = lengthOfOperations(IODataBeforeRead)

  const mastersAfterRemove = presentation.slideMasters.filter(
    (master) => master !== slideMaster
  )
  updates['presentation'] = opsFromUpdateSlideMasterRefs(
    mastersAfterRemove,
    IODataBeforeRead
  )

  const layoutRefsBeforeRead = getSlideLayoutRefsFromIOData(IODataBeforeRead)
  updates['layouts'] = slideMaster.layouts.reduce((result, layout) => {
    const slideLayoutRef = layout.getRefId()
    if (layoutRefsBeforeRead[slideLayoutRef]) {
      result[slideLayoutRef] = [Op.Remove(SlideLayoutDataLayout.length)]
    }
    return result
  }, {})
  updates['slideMasters'] = {
    [slideMasterRId]: [Op.Remove(SlideMasterDataLayout.length)],
  }

  return opsFromUpdates(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    ioDataLength,
    updates
  )
}

export function opsFromUpdateThemes(
  presentation: Presentation,
  collabManager: CollaborationManager<any>
): Operation[] {
  const updates = {}
  const IODataBeforeRead = collabManager.currentPptIOData() ?? []
  const ioDataLength = lengthOfOperations(IODataBeforeRead)

  if (presentation.slideMasters) {
    const themeUpdateData = {}
    const usedThemeRefs = [
      ...presentation.slideMasters,
      ...presentation.notesMasters,
    ]
      .map((master) => master?.theme?.getRefId())
      .filter(Boolean)
    const themeRefsInIOData = keysFromMapData(
      IODataBeforeRead,
      'themes',
      PPTDocDataLayout,
      PPTDocDataLayoutTypeMap
    )
    themeRefsInIOData.forEach((themeRef) => {
      if (!usedThemeRefs.includes(themeRef)) {
        themeUpdateData[themeRef] = [Op.Remove(1)]
      }
    })
    updates['themes'] = themeUpdateData
  }

  return opsFromUpdates(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    ioDataLength,
    updates
  )
}
