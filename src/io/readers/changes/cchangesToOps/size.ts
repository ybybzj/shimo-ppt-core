import { Presentation } from '../../../../core/Slide/Presentation'
import { PPTDocDataLayout, PPTDocDataLayoutTypeMap } from '../../../defs/pptdoc'
import {
  PresentationDataLayout,
  PresentationDataLayoutTypeMap,
} from '../../../defs/presentation'
import { Operation } from '../../../operation'
import { opsFromUpdate } from './helpers'

// presentation/Slide Size
export function opsFromUpdateSlideSize(
  presentation: Presentation
): Operation[] {
  const presData = {
    pres: presentation.pres ? presentation.pres.toJSON(presentation) : {},
  }
  const opsFromPresentation = opsFromUpdate(
    PresentationDataLayout,
    PresentationDataLayoutTypeMap,
    'Pres',
    presData
  )

  return opsFromUpdate(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    'presentation',
    opsFromPresentation
  )
}

// shape Size
