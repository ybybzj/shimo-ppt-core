import { Dict } from '../../../../../liber/pervasive'
import { ScaleOfPPTXSizes } from '../../../../core/common/const/unit'
import { Slide } from '../../../../core/Slide/Slide'
import { ChartObject } from '../../../../core/SlideElement/ChartObject'
import { GraphicFrame } from '../../../../core/SlideElement/GraphicFrame'
import { GroupShape } from '../../../../core/SlideElement/GroupShape'
import { ImageShape } from '../../../../core/SlideElement/Image'
import { OleObject } from '../../../../core/SlideElement/OleObject'
import { Shape } from '../../../../core/SlideElement/Shape'
import { Table } from '../../../../core/Table/Table'
import { TableCell } from '../../../../core/Table/TableCell'
import { TableRow } from '../../../../core/Table/TableRow'
import { isSlideElement } from '../../../../core/utilities/shape/asserts'
import { getOrderForSlideElement } from '../../../../core/utilities/shape/getters'
import { InstanceType } from '../../../../core/instanceTypes'
import {
  ChartDataLayout,
  ChartDataLayoutMap,
} from '../../../defs/csld/chartObject'
import {
  ConnectionShapeDataLayout,
  ConnectionShapeDataLayoutTypeMap,
} from '../../../defs/csld/connectionShape'
import {
  GraphicFrameDataLayout,
  GraphicFrameDataLayoutMap,
} from '../../../defs/csld/graphicFrame'
import {
  GroupShapeDataLayout,
  GroupShapeDataLayoutTypeMap,
} from '../../../defs/csld/groupShape'
import { ImageDataLayout, ImageDataLayoutMap } from '../../../defs/csld/image'
import { OleDataLayout, OleDataLayoutMap } from '../../../defs/csld/oleObject'
import {
  ShapeDataLayout,
  ShapeDataLayoutTypeMap,
} from '../../../defs/csld/shape'
import {
  TableDataLayout,
  TableDataLayoutMap,
  TableRowDataLayout,
  TableRowDataLayoutMap,
} from '../../../defs/csld/table'
import {
  EmptyOpData,
  Op,
  OpData,
  Operation,
  withType,
} from '../../../operation'
import { scaleValue, toJSON } from '../../../utils'
import { JSONFromGraphicFrameAttrs } from '../../csld/graphicFrame'
import { JSONFromTableAttrs } from '../../csld/table'
import { opsFromUpdateTableCellInfo } from '../../csld/table/cell'
import { getTableRowHeight } from '../../csld/table/row'
import { generateTableGridInfo } from '../../csld/table/util'
import { ChangeOperation } from '../cchangeOperation'
import { ClassObjectWithId } from '../helper'
import {
  findSpFromTargetParents,
  opsFromUpdate,
  opsFromUpdates,
  opsFromUpdateSlideShapes,
} from './helpers'

export function opsFromUpdateChange(cchange: ChangeOperation): Operation[] {
  const { targetParents, target } = cchange
  const slide = getSlideFromParents(targetParents)
  const sp = isSlideElement(target)
    ? target
    : findSpFromTargetParents(targetParents)
  if (!slide || !sp) return []
  let spItemOps: Dict<Operation[]> | undefined
  switch (sp.instanceType) {
    case InstanceType.Shape:
      spItemOps = opsFromUpdateShape(sp)
      break
    case InstanceType.ImageShape:
      if (sp.isOleObject()) {
        spItemOps = opsFromUpdateOleObject(sp)
      } else if (sp.isChartObject()) {
        spItemOps = opsFromUpdateChartObject(sp)
      } else {
        spItemOps = opsFromUpdateImageShape(sp)
      }
      break
    case InstanceType.GroupShape:
      spItemOps = opsFromUpdateGroupShape(sp)
      break
    case InstanceType.GraphicFrame:
      spItemOps = opsFromUpdateGraphicFrame(cchange)
      break
  }
  if (spItemOps == null) return []
  return opsFromUpdateSlideShapes(slide, spItemOps)
}

/** 目前仅支持 update spPr */
function opsFromUpdateShape(sp: Shape) {
  const spPrData = { spPr: sp.spPr ? toJSON(sp.spPr) : EmptyOpData.JSON }

  const isCxn = sp.isConnectionShape()

  if (isCxn) {
    const cxnShapeInnerOp = opsFromUpdate(
      ConnectionShapeDataLayout,
      ConnectionShapeDataLayoutTypeMap,
      'spPr',
      spPrData
    )
    return opsFromUpdateSpItem('cxnSp', sp, cxnShapeInnerOp)
  } else {
    const shapeInnerOp = opsFromUpdate(
      ShapeDataLayout,
      ShapeDataLayoutTypeMap,
      'spPr',
      spPrData
    )
    return opsFromUpdateSpItem('sp', sp, shapeInnerOp)
  }
}

/** 目前仅支持 update spPr */
function opsFromUpdateImageShape(img: ImageShape) {
  const spPrData = { spPr: img.spPr ? toJSON(img.spPr) : EmptyOpData.JSON }
  const imgInnerOp = opsFromUpdate(
    ImageDataLayout,
    ImageDataLayoutMap,
    'spPr',
    spPrData
  )
  const spItemOps = opsFromUpdateSpItem('pic', img, imgInnerOp)
  return spItemOps
}

/** 目前仅支持 update spPr */
function opsFromUpdateOleObject(ole: OleObject) {
  const spPrData = { spPr: ole.spPr ? toJSON(ole.spPr) : EmptyOpData.JSON }
  const imgInnerOp = opsFromUpdate(
    ImageDataLayout,
    ImageDataLayoutMap,
    'spPr',
    spPrData
  )
  const oleInnnerOp = opsFromUpdate(
    OleDataLayout,
    OleDataLayoutMap,
    'pic',
    imgInnerOp
  )
  const spItemOps = opsFromUpdateSpItem('ole', ole, oleInnnerOp)
  return spItemOps
}

function opsFromUpdateChartObject(chartObj: ChartObject) {
  const spPrData = {
    spPr: chartObj.spPr ? toJSON(chartObj.spPr) : EmptyOpData.JSON,
  }
  const imgInnerOp = opsFromUpdate(
    ImageDataLayout,
    ImageDataLayoutMap,
    'spPr',
    spPrData
  )
  const chartInnnerOp = opsFromUpdate(
    ChartDataLayout,
    ChartDataLayoutMap,
    'pic',
    imgInnerOp
  )
  const spItemOps = opsFromUpdateSpItem('chart', chartObj, chartInnnerOp)
  return spItemOps
}

/** 目前仅支持 update spPr */
function opsFromUpdateGroupShape(group: GroupShape) {
  const spPrData = {
    grpSpPr: group.spPr ? toJSON(group.spPr) : EmptyOpData.JSON,
  }
  const groupInnerOp = opsFromUpdate(
    GroupShapeDataLayout,
    GroupShapeDataLayoutTypeMap,
    'grpSpPr',
    spPrData
  )
  const spItemOps = opsFromUpdateSpItem('grpSp', group, groupInnerOp)
  return spItemOps
}

function opsFromUpdateGraphicFrame(cchange: ChangeOperation) {
  if (!cchange.updateUnit) return
  switch (cchange.updateUnit) {
    case 'TableCellAttrs': {
      const cell = cchange.target as TableCell
      return opsFromUpdateTableCellChanges(cell, 'Attrs')
    }
    case 'TableCellContent': {
      const cell = cchange.target as TableCell
      return opsFromUpdateTableCellChanges(cell, 'Content')
    }
    case 'TableRowAttrs': {
      const row = cchange.target as TableRow
      return opsFromUpdateTableRowChanges(row)
    }
    case 'TableGrid': {
      const table = cchange.target as Table
      return opsFromUpdateTableGridChanges(table)
    }
    case 'TableAttrs': {
      const table = cchange.target as Table
      return opsFromUpdateTableAttrsChanges(table)
    }
    case 'GraphicFrameAttrs': {
      const graphicFrame = cchange.target as GraphicFrame
      return opsFromUpdateGraphicFrameChanges(graphicFrame)
    }
  }
}

function opsFromUpdateTableCellChanges(
  cell: TableCell,
  subType: 'Attrs' | 'Content'
) {
  const row = cell.parent
  const gridInfo = generateTableGridInfo(row.parent)
  const rowInfo = gridInfo.Rows[row.index]
  const cellIndex = rowInfo.Cells.findIndex((info) => info.cell === cell)
  if (cellIndex < 0) return
  const cellInfo = rowInfo.Cells[cellIndex]
  const cellInnerOp = opsFromUpdateTableCellInfo(cellInfo, subType)
  if (!cellInnerOp) return
  return opsFromTableCellOp(cell, cellIndex, cellInnerOp)
}

/** 默认替换 Attrs */
function opsFromUpdateTableRowChanges(row: TableRow) {
  const rowHeight = getTableRowHeight(row)
  const attrsData = rowHeight != null ? { ['height']: rowHeight } : {}

  const rowOpData = opsFromUpdate(
    TableRowDataLayout,
    TableRowDataLayoutMap,
    'attrs',
    attrsData
  )
  return opsFromTableRowData(row, rowOpData)
}

function opsFromUpdateTableAttrsChanges(table: Table) {
  const attrsData = JSONFromTableAttrs(table)
  const tableInnerOp = opsFromUpdates(
    TableDataLayout,
    TableDataLayoutMap,
    TableDataLayout.length,
    {
      attrs: attrsData,
    }
  )
  return opsFromTableData(table, tableInnerOp)
}

function opsFromUpdateTableGridChanges(table: Table) {
  const grid = table.tableGrid
  const gridData = {
    ['grid']: grid.map((n) => scaleValue(n, ScaleOfPPTXSizes)!),
  }
  const tableInnerOp = opsFromUpdates(
    TableDataLayout,
    TableDataLayoutMap,
    TableDataLayout.length,
    {
      grid: gridData,
    }
  )
  return opsFromTableData(table, tableInnerOp)
}

/** 默认替换 Attrs */
function opsFromUpdateGraphicFrameChanges(graphicFrame: GraphicFrame) {
  const attrsData = JSONFromGraphicFrameAttrs(graphicFrame)
  const retainOp = opsFromUpdate(
    GraphicFrameDataLayout,
    GraphicFrameDataLayoutMap,
    'attrs',
    attrsData
  )
  return opsFromUpdateSpItem('graphicFrame', graphicFrame, retainOp)
}

// Helpers
function opsFromTableCellOp(
  cell: TableCell,
  cellIndex: number,
  innerOp: Operation
) {
  const row = cell.parent
  const cellsOpData =
    cellIndex > 0 ? [Op.Retain(cellIndex), innerOp] : [innerOp]

  const rowOpData = opsFromUpdate(
    TableRowDataLayout,
    TableRowDataLayoutMap,
    'cells',
    cellsOpData
  )

  return opsFromTableRowData(row, rowOpData)
}

function opsFromTableRowData(row: TableRow, rowOpData: Operation[]) {
  const rowOp = withType(Op.Retain(OpData.Delta(rowOpData)), 'row')
  const table = row.parent
  const rowsOpData = row.index > 0 ? [Op.Retain(row.index), rowOp] : [rowOp]
  const tableInnerOp = opsFromUpdate(
    TableDataLayout,
    TableDataLayoutMap,
    'rows',
    rowsOpData
  )
  return opsFromTableData(table, tableInnerOp)
}

function opsFromTableData(table: Table, innerOps: Operation[]) {
  const graphicFrame = table.parent
  const graphicFrameInnerOps = opsFromUpdate(
    GraphicFrameDataLayout,
    GraphicFrameDataLayoutMap,
    'graphic',
    innerOps
  )
  return opsFromUpdateSpItem('graphicFrame', graphicFrame, graphicFrameInnerOps)
}

type UpdateSpType =
  | 'sp'
  | 'cxnSp'
  | 'grpSp'
  | 'pic'
  | 'graphicFrame'
  | 'ole'
  | 'chart'

type UpdateSpTypeMap<T extends UpdateSpType> = T extends 'sp'
  ? Shape
  : T extends 'cxnSp'
  ? Shape
  : T extends 'grpSp'
  ? GroupShape
  : T extends 'pic'
  ? ImageShape
  : T extends 'graphicFrame'
  ? GraphicFrame
  : T extends 'ole'
  ? OleObject
  : T extends 'chart'
  ? ChartObject
  : never

function opsFromUpdateSpItem<T extends UpdateSpType>(
  spType: T,
  sp: UpdateSpTypeMap<T>,
  spDataRetainOp: Operation[]
): Dict<Operation[]> {
  const order = getOrderForSlideElement(sp)

  const spItemOp: Operation[] = [
    Op.Retain(
      OpData.Delta(spDataRetainOp),
      spType === 'pic'
        ? { order, spType, subType: (sp as ImageShape).subType }
        : { order, spType }
    ),
  ]

  const rId = sp.getRefId()
  return { [rId]: spItemOp! }
}

function getSlideFromParents(targetParents: ClassObjectWithId[]) {
  const [root, mainContainer, subParent] = targetParents
  if (root?.instance?.instanceType === InstanceType.Presentation) {
    const container = mainContainer?.instance
    const containerType = container?.instanceType
    const subParentType = subParent?.instance?.instanceType
    if (
      containerType === InstanceType.Slide &&
      subParentType === InstanceType.CSld
    ) {
      return container as Slide
    }
  }
}
