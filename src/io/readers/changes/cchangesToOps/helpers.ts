import { Dict, Nullable } from '../../../../../liber/pervasive'
import { type } from '../../../../../liber/type'
import { EntityForChange, EntityObject } from '../../../../core/changes/type'
import { Presentation } from '../../../../core/Slide/Presentation'
import { Slide } from '../../../../core/Slide/Slide'
import { EditActionFlag } from '../../../../core/EditActionFlag'
import {
  InstanceType,
  InstanceTypeIDS,
  isInstanceTypeOf,
} from '../../../../core/instanceTypes'
import { SlideElement } from '../../../../core/SlideElement/type'
import { CSldDataLayout, CSldDataLayoutTypeMap } from '../../../defs/csld'
import { PPTDocDataLayout, PPTDocDataLayoutTypeMap } from '../../../defs/pptdoc'
import { SlideDataLayout, SlideDataLayoutTypeMap } from '../../../defs/Slide'
import {
  Op,
  OpData,
  Operation,
  OperationDataType,
  getOperationDataType,
  withType,
} from '../../../operation'
import {
  ChangeOperation,
  ChangeItemToOperation,
} from '../../../readers/changes/cchangeOperation'
import {
  ClassObjectWithId,
  findClosestParentByInfo,
  isTargetInDoc,
} from '../../../readers/changes/helper'
import {
  PresentationDataLayout,
  PresentationDataLayoutTypeMap,
} from '../../../defs/presentation'
import { SlideMaster } from '../../../../core/Slide/SlideMaster'
import { getMasterSlidesRefs } from '../../Presentation'
import { SlideLayout } from '../../../../core/Slide/SlideLayout'
import {
  SlideLayoutDataLayout,
  SlideLayoutDataLayoutTypeMap,
} from '../../../defs/SlideLayout'
import {
  SlideMasterDataLayout,
  SlideMasterDataLayoutTypeMap,
} from '../../../defs/SlideMaster'
import { GraphicFrame } from '../../../../core/SlideElement/GraphicFrame'
import { fillCollectionsFromChanges } from './updateHelper'
import { isSlideElement } from '../../../../core/utilities/shape/asserts'
import { each } from '../../../../../liber/l/each'

import { breadcrumbTrail } from '../../../../collaboration/breadcrumbTrail'
import { EditorKit } from '../../../../editor/global'
import { EntityContentChange } from '../../../../core/changes/contentChange'
import { ReplaceChange } from '../../../../core/changes/replaceChange'
import { UpdateChange } from '../../../../core/changes/updateChange'
import { composeDelta } from '../../../../re/export/modoc'
import { getOperationByOpType } from '../../../appliers/utils'

type dataType = 'delta' | 'json' | 'map'
type opsType<T extends dataType> = T extends 'delta'
  ? Operation[]
  : T extends 'json'
  ? Dict
  : T extends 'map'
  ? Dict<Operation[]>
  : never
export function opsFromUpdate<
  L extends readonly any[],
  Map extends { [key in L[number]]: dataType },
  T extends L[number],
  SubOps extends opsType<Map[T]>,
>(layout: L, typeMap: Map, opType: T, subOps: SubOps): Operation[] {
  const retainPos = layout.indexOf(opType)
  if (retainPos < 0) {
    EditorKit.trackError('CORE_TRACK', new Error('Invalid opsFromUpdate'), {
      ['layout']: layout,
      ['opType']: opType,
      ['actionTrails']: breadcrumbTrail.get(),
    })
    console.warn(`[opsFromUpdate]invalid layout & opType!`, layout, opType)
    return []
  }

  const retainOps = retainPos > 0 ? [Op.Retain(retainPos)] : []

  const updateOps = opsByOpType(typeMap[opType], opType, subOps)

  return retainOps.concat(updateOps as any[])
}

export type UpdatesForOps<
  L extends readonly string[],
  Map extends { [key in L[number]]: dataType },
> = { [key in L[number]]: opsType<Map[L[number]]> }

export function opsFromUpdates<
  L extends readonly string[],
  Map extends { [key in L[number]]: dataType },
  T extends L[number],
>(
  layout: L,
  typeMap: Map,
  currentDataLength: number,
  updates: Partial<{ [key in T]: opsType<Map[T]> }>
): Operation[] {
  let retainLen = 0
  let insertLen = 0
  const updateKeys = Object.keys(updates)
  let updateCounts = updateKeys.length
  let updateOps: Operation[] = []
  for (let i = 0, l = layout.length; i < l; i++) {
    const opType = layout[i]
    const subOps = updates[opType]
    if (subOps) {
      updateCounts -= 1

      const retainOps = retainLen > 0 ? [Op.Retain(retainLen)] : []
      const insertOps = insertLen > 0 ? [Op.Insert(insertLen)] : []
      updateOps = updateOps.concat(
        retainOps,
        insertOps,
        opsByOpType(
          typeMap[opType],
          opType,
          subOps,
          currentDataLength <= i
        ) as any[]
      )
      retainLen = 0
      insertLen = 0
      if (updateCounts <= 0) {
        break
      }
    } else {
      if (currentDataLength > i) {
        retainLen += 1
      } else {
        insertLen += 1
      }
    }
  }

  if (updateCounts > 0) {
    EditorKit.trackError('CORE_TRACK', new Error('Invalid opsFromUpdates'), {
      ['layout']: layout,
      ['updateKeys']: updateKeys,
      ['actionTrails']: breadcrumbTrail.get(),
    })
    console.warn(`[opsFromUpdate]invalid updates!`, updates)
    return []
  }
  return updateOps
}

function opsByOpType<T extends dataType>(
  dataType: dataType,
  opType: string,
  subOps: opsType<T>,
  isInsert: boolean = false
): Operation[] {
  let updateOps: Operation[] = []
  const opAction: any = isInsert ? Op.Insert : Op.Retain
  switch (dataType) {
    case 'delta': {
      if (Array.isArray(subOps)) {
        updateOps = [
          withType(opAction(OpData.Delta(subOps as Operation[])), opType),
        ]
      }
      break
    }
    case 'map': {
      if (type.isObject(subOps)) {
        updateOps = [
          withType(opAction(OpData.Map(subOps as Dict<Operation[]>)), opType),
        ]
      }
      break
    }
    case 'json': {
      updateOps = [withType(opAction(OpData.JSON(subOps)), opType)]
      break
    }
  }
  return updateOps
}

export function opsFromUpdateSlideShapes(
  slide: Slide,
  shapeOps: Dict<Operation[]>
): Operation[] {
  const csldOps = opsFromUpdate(
    CSldDataLayout,
    CSldDataLayoutTypeMap,
    'spTree',
    shapeOps
  )

  const slideOps = opsFromUpdate(
    SlideDataLayout,
    SlideDataLayoutTypeMap,
    'csld',
    csldOps
  )

  const slideRId = slide.getRefId()
  return opsFromUpdate(PPTDocDataLayout, PPTDocDataLayoutTypeMap, 'slides', {
    [slideRId]: slideOps,
  })
}

export function opsFromUpdateSlideLayoutShapes(
  slideLayout: SlideLayout,
  shapeOps: Dict<Operation[]>
): Operation[] {
  const csldOps = opsFromUpdate(
    CSldDataLayout,
    CSldDataLayoutTypeMap,
    'spTree',
    shapeOps
  )

  const slideLayoutOps = opsFromUpdate(
    SlideLayoutDataLayout,
    SlideLayoutDataLayoutTypeMap,
    'csld',
    csldOps
  )

  const slideLayoutRId = slideLayout.getRefId()
  return opsFromUpdate(PPTDocDataLayout, PPTDocDataLayoutTypeMap, 'layouts', {
    [slideLayoutRId]: slideLayoutOps,
  })
}

export function opsFromUpdateSlideMasterShapes(
  slideMaster: SlideMaster,
  shapeOps: Dict<Operation[]>
): Operation[] {
  const csldOps = opsFromUpdate(
    CSldDataLayout,
    CSldDataLayoutTypeMap,
    'spTree',
    shapeOps
  )

  const slideMasterOps = opsFromUpdate(
    SlideMasterDataLayout,
    SlideMasterDataLayoutTypeMap,
    'csld',
    csldOps
  )

  const slideMasterRId = slideMaster.getRefId()
  return opsFromUpdate(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    'slideMasters',
    {
      [slideMasterRId]: slideMasterOps,
    }
  )
}

export function opsFromUpdateSlide(slideOps: Dict<Operation[]>): Operation[] {
  return opsFromUpdate(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    'slides',
    slideOps
  )
}

export function opsFromUpdateSlideMasterRefs(
  slideMasters: SlideMaster[],
  InputOps: Operation[]
): Operation[] {
  return opsFromUpdate(
    PresentationDataLayout,
    PresentationDataLayoutTypeMap,
    'SlideMaster',
    Object.assign(
      getSlideMasterRefsFromIOData(InputOps),
      getMasterSlidesRefs({ slideMasters } as any)
    )
  )
}

export function readFromCollection<Item>(
  collection: Item[],
  rootDoc: Presentation,
  reader: (rootDoc: Presentation, item: Item, ...args: any[]) => Operation[],
  ...args: any[]
): Operation[] {
  const result = collection.reduce((changeResults, cc) => {
    const readerResult = reader(rootDoc, cc, ...args)
    return composeDelta(changeResults, readerResult)
  }, [] as Operation[])
  return result
}

export const atomicInstanceType: InstanceTypeIDS[] = [
  InstanceType.Shape,
  InstanceType.ImageShape,
  InstanceType.GraphicFrame,
  InstanceType.GroupShape,
] // 用于获取 changes 最近能被整体 toJSON 的父节点

export function compactChanges(
  cchanges: ChangeOperation[],
  presentation: Presentation
): ChangeOperation[] {
  const result: ChangeOperation[] = []
  const replaceChanges: Dict<ReplaceChange> = {}
  /** key 为 updateChange 的 hash, 用于内部去重; closestParentId 用于外部和 ReplaceChange 去重 */
  const updateChangeCollections: Dict<UpdateChange> = {}

  const addOrRemoveSlides: Slide[] = []

  each((cchange) => {
    const { target, targetParents } = cchange
    const closestParent = findTargetParent(
      presentation,
      target,
      targetParents,
      atomicInstanceType
    )
    if (closestParent) {
      const hasBeenReplaced = replaceChanges[closestParent.id] != null
      if (hasBeenReplaced) return
      const isUpdateChange = fillCollectionsFromChanges(
        updateChangeCollections,
        cchange,
        closestParent as GraphicFrame
      )
      if (isUpdateChange !== true) {
        replaceChanges[closestParent.id] = new ReplaceChange(
          closestParent as EntityForChange,
          cchange.type,
          cchange.actionType
        )
        each((updateChange, hash) => {
          if (updateChange.closestParentId === closestParent.id) {
            delete updateChangeCollections[hash]
          }
        }, updateChangeCollections)
      }
    } else {
      if (isTargetInDoc(presentation, target)) {
        // excludes shapes already been removed or added
        const addOrRemoveSp = getContentChangeSp(cchange)
        if (addOrRemoveSp && replaceChanges[addOrRemoveSp.id]) {
          delete replaceChanges[addOrRemoveSp.id]
        }

        const addOrRemoveSlide = getContentChangeSlide(cchange)
        if (
          addOrRemoveSlide &&
          addOrRemoveSlides.indexOf(addOrRemoveSlide) < 0
        ) {
          addOrRemoveSlides.push(addOrRemoveSlide)
        }

        result.push(cchange)
      }
    }
  }, cchanges)

  appendOperationsFromChanges(result, replaceChanges)
  appendOperationsFromChanges(result, updateChangeCollections)

  return result.filter((cchange) => {
    const targetSlide: undefined | Slide =
      cchange.target.instanceType === InstanceType.Slide
        ? cchange.target
        : undefined
    if (targetSlide && addOrRemoveSlides.indexOf(targetSlide) > -1) {
      return false
    }
    const parentSlide = findClosestParentByInfo(
      cchange.targetParents,
      (parent) => parent.instanceType === InstanceType.Slide
    ) as Nullable<Slide>
    return parentSlide == null || addOrRemoveSlides.indexOf(parentSlide) === -1
  })
}

function findTargetParent(
  presentation: Presentation,
  target: EntityObject,
  targetParents: ClassObjectWithId[],
  parentInstanceTypes: InstanceTypeIDS[]
) {
  const isValidAtomicParent = (target: EntityObject) => {
    return isValidClosestParent(presentation, target, parentInstanceTypes)
  }
  return isValidAtomicParent(target)
    ? target
    : findClosestParentByInfo(targetParents, (parent) =>
        isValidAtomicParent(parent)
      )
}

function isValidClosestParent(
  presentation: Presentation,
  target: EntityObject,
  parentInstanceTypes: InstanceTypeIDS[]
) {
  const isShapeInsideGroupShape = (target: EntityObject) => !!target.group
  return (
    isTargetInDoc(presentation, target) &&
    parentInstanceTypes.indexOf(target.instanceType!) > -1 &&
    !isShapeInsideGroupShape(target)
  )
}

export function findSpFromTargetParents(targetParents: ClassObjectWithId[]) {
  const treeLength = targetParents.length
  for (let i = treeLength - 1; 0 <= i; i--) {
    if (isSlideElement(targetParents[i].instance)) {
      return targetParents[i].instance as SlideElement
    }
  }
  return
}

/** @note 确保数据源为纯对象 */
function appendOperationsFromChanges(
  compactChanges: ChangeOperation[],
  changes: Dict<UpdateChange | ReplaceChange>
) {
  each((change) => {
    const changeOperation = ChangeItemToOperation(
      {
        entity: change.targetEntity,
        changeRecord: change,
      },
      change.editType,
      change.actionType,
      change.Category === 'UpdateChange'
        ? { updateUnit: change.updateType }
        : {}
    )
    compactChanges.push(changeOperation)
  }, changes)
}

function getContentChangeSp(cc: ChangeOperation): Nullable<SlideElement> {
  if (
    cc.operationType === 'SpContentChange' &&
    isInstanceTypeOf(cc.target, InstanceType.Slide)
  ) {
    switch (cc.type) {
      case EditActionFlag.edit_Slide_AddToSpTree:
      case EditActionFlag.edit_Slide_RemoveFromSpTree:
        const changeInfo = getContentChangeInfo(
          cc.change as EntityContentChange
        )
        const shape = changeInfo.items[0] as SlideElement
        return shape
    }
  }
}

function getContentChangeSlide(cc: ChangeOperation): Nullable<Slide> {
  if (
    cc.operationType === 'SpContentChange' &&
    isInstanceTypeOf(cc.target, InstanceType.Presentation)
  ) {
    switch (cc.type) {
      case EditActionFlag.edit_Presentation_AddSlide:
      case EditActionFlag.edit_Presentation_RemoveSlide:
        const changeInfo = getContentChangeInfo(
          cc.change as EntityContentChange
        )
        const slide = changeInfo.items[0] as Slide
        return slide
    }
  }
}

function getSlideMasterRefsFromIOData(inputOps: Operation[]) {
  const currentSlideMasterRefs: Dict<any> = {}
  const ops = getOperationByOpType(
    inputOps,
    'presentation',
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap
  )

  if (ops) {
    const slideMaster_ops = getOperationByOpType(
      ops.data['delta'],
      'SlideMaster',
      PresentationDataLayout,
      PresentationDataLayoutTypeMap
    )
    if (slideMaster_ops) {
      Object.keys(slideMaster_ops.data['object']).forEach((ref) => {
        currentSlideMasterRefs[ref] = null
      })
    }
  }
  return currentSlideMasterRefs
}

export function getSlideLayoutRefsFromIOData(inputOps: Operation[]) {
  const currentSlideLayoutRefs: Dict<any> = {}
  const ops = getOperationByOpType(
    inputOps,
    'layouts',
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap
  )

  if (ops) {
    const opData = ops.data['slide']
    if (opData) {
      Object.keys(opData).forEach((ref) => {
        currentSlideLayoutRefs[ref] = true
      })
    }
  }
  return currentSlideLayoutRefs
}
export interface ContentChangeInfo {
  items: EntityObject[]
  index: number
  changeType: 'add' | 'remove'
}
export function getContentChangeInfo(
  cchange: EntityContentChange
): ContentChangeInfo {
  return {
    items: cchange.items,
    index: cchange.index,
    changeType: cchange.isAdd ? 'add' : 'remove',
  }
}

export function keysFromMapData<
  L extends readonly string[],
  OT extends L[number],
  M extends { [k in L[number]]: OperationDataType },
>(stream: Operation[], opType: OT, layout: L, layoutDataTypeMap: M) {
  let keys: string[] = []
  const op = getOperationByOpType(stream, opType, layout, layoutDataTypeMap)
  if (op && getOperationDataType(op.data) === 'map') {
    keys = Object.keys(op.data['slide'])
  }
  return keys
}
