import { Dict, Nullable } from '../../../../../liber/pervasive'
import { ActionPoint } from '../../../../collaboration/changesCollector'
import { CollaborationManager } from '../../../../collaboration/collaborationManager'
import { appendArrays } from '../../../../common/utils'
import { EntityPropertyChange } from '../../../../core/changes/propertyChange'
import { Presentation } from '../../../../core/Slide/Presentation'
import { Slide } from '../../../../core/Slide/Slide'
import { SlideTransition } from '../../../../core/Slide/SlideTransition'
import { isShapeOfTable } from '../../../../core/utilities/shape/asserts'
import { EditActionFlag } from '../../../../core/EditActionFlag'
import { InstanceType, isInstanceTypeOf } from '../../../../core/instanceTypes'
import { SlideElement } from '../../../../core/SlideElement/type'
import { Operation } from '../../../operation'
import {
  ChangeOperation,
  ChangesOfAction,
  changePointToAction,
  OrderChangeOperation,
} from '../../../readers/changes/cchangeOperation'
import {
  opsFromAddComment,
  opsFromAddShape,
  opsFromAddSlide,
  opsFromAddSlideMaster,
} from './add'
import {
  compactChanges,
  getContentChangeInfo,
  readFromCollection,
} from './helpers'
import { opsFromUpdateShapesOrder, opsFromUpdateSlidesOrder } from './orders'
import {
  opsFromAddSlideComment,
  opsFromUpdateComment,
} from './property/comment'
import {
  opsFromSlideChangeLayout,
  opsFromSlideChangeShow,
  opsFromSlideSetAnimation,
  opsFromSlideSetBg,
  opsFromSlideSetTransition,
} from './property/slide'
import { opsForTableStyles } from './property/tableStyles'
import {
  opsFromRemoveComment,
  opsFromRemoveShape,
  opsFromRemoveSlide,
  opsFromRemoveSlideMaster,
  opsFromRemoveSlideOrder,
  opsFromUpdateThemes,
} from './remove'
import { opsFromReplaceChange } from './replaceItem'
import { opsFromUpdateSlideSize } from './size'
import { Comment } from '../../../../core/Slide/Comments/Comment'
import { SlideComments } from '../../../../core/Slide/Comments/SlideComments'
import { SlideMaster } from '../../../../core/Slide/SlideMaster'
import { AnimationNode, SlideAnimation } from '../../../../core/Slide/Animation'
import { ShowProps } from '../../../../core/Slide/ShowProps'
import {
  opsFromPresentationSetPresPr,
  opsFromPresentationSetShowPr,
} from './property/presentation'
import {
  opsFromSlideMasterSetTransition,
  opsFromSlideMasterSetTxStyles,
} from './property/slideMaster'
import { opsFromSlideLayoutSetTransition } from './property/slideLayout'
import { SlideLayout } from '../../../../core/Slide/SlideLayout'
import { animationFixer } from '../../../animationsFixer'
import { opsFromUpdateChange } from './updateItem'
import { logger } from '../../../../lib/debug/log'
import { EntityContentChange } from '../../../../core/changes/contentChange'
import { Bg } from '../../../../core/SlideElement/attrs/bg'
import { TextStyles } from '../../../../core/textAttributes/TextStyles'
import { composeDelta } from '../../../../re/export/modoc'
import { PresProps } from '../../../../core/Slide/PresProps'
import { EntityForChange } from '../../../../core/changes/type'

export function cchangeOpsFromAction(
  action: ChangesOfAction,
  presentation: Presentation,
  reorderOpsCollector: Dict<ChangeOperation>
): ChangeOperation[] {
  let cchangeOps: ChangeOperation[] = []
  switch (action.type) {
    // order change
    case EditActionFlag.action_Presentation_BringBackward:
    case EditActionFlag.action_Presentation_BringForward:
    case EditActionFlag.action_Presentation_SendToBack:
    case EditActionFlag.action_Presentation_BringToFront: {
      const target = action.cchangeOperations[0].target
      if (
        target.instanceType !== InstanceType.SlideAnimation &&
        target.id != null
      ) {
        reorderOpsCollector[target.id] = OrderChangeOperation(
          target,
          action.type,
          action.type
        )
      }

      break
    }
    case EditActionFlag.action_Presentation_MoveSlidesToPrev:
    case EditActionFlag.action_Presentation_MoveSlidesToNext:
    case EditActionFlag.action_Presentation_ShiftSlides: {
      reorderOpsCollector[presentation.id] = OrderChangeOperation(
        presentation,
        action.type,
        action.type
      )

      break
    }
    // add/remove shape
    case EditActionFlag.action_Presentation_CreateGroup:
    case EditActionFlag.action_Slide_AddNewShape:
    case EditActionFlag.action_Presentation_AddTable:
    case EditActionFlag.action_Slide_Remove: {
      let slide: EntityForChange | undefined
      cchangeOps = action.cchangeOperations.filter((cc) => {
        if (
          isInstanceTypeOf(cc.target, InstanceType.Slide) &&
          cc.operationType === 'SpContentChange'
        ) {
          slide = cc.target
          return true
        } else if (
          isInstanceTypeOf(cc.target, InstanceType.UniNvPr) &&
          cc.operationType === 'PropertyChange'
        ) {
          return true
        } else if (
          isInstanceTypeOf(cc.target, InstanceType.SlideAnimation) &&
          cc.operationType === 'PropertyChange'
        ) {
          return true
        } else {
          return false
        }
      })

      if (!slide) {
        cchangeOps = action.cchangeOperations
      } else {
        reorderOpsCollector[slide.id] = OrderChangeOperation(
          slide,
          action.type,
          action.type
        )
      }
      break
    }

    // add/remove slide
    case EditActionFlag.action_Presentation_AddNextSlide:
    // case IdentityFlag.action_Presentation_DeleteSlides:
    case EditActionFlag.action_Presentation_DuplicateSlides: {
      cchangeOps = action.cchangeOperations.filter(
        (cc) =>
          isInstanceTypeOf(cc.target, InstanceType.Presentation) &&
          cc.operationType === 'SpContentChange'
      )
      reorderOpsCollector[presentation.id] = OrderChangeOperation(
        presentation,
        action.type,
        action.type
      )

      break
    }

    // copy/paste
    case EditActionFlag.action_Document_PasteByHotKey:
    case EditActionFlag.action_SlideElements_CopyCtrl: {
      cchangeOps = opsForAddByPaste(action, presentation, reorderOpsCollector)
      break
    }

    // setLayout for slides
    case EditActionFlag.action_Presentation_ChangeLayout: {
      cchangeOps = action.cchangeOperations.filter(
        (cc) =>
          isInstanceTypeOf(cc.target, InstanceType.Slide) &&
          cc.operationType === 'PropertyChange' &&
          cc.type === EditActionFlag.edit_Slide_SetLayout
      )
      break
    }
    case EditActionFlag.action_Presentation_ChangeTheme: {
      cchangeOps = action.cchangeOperations.filter((cc) => {
        return (
          (isInstanceTypeOf(cc.target, InstanceType.Presentation) &&
            cc.operationType === 'SpContentChange' &&
            (cc.type === EditActionFlag.edit_Presentation_AddSlideMaster ||
              cc.type ===
                EditActionFlag.edit_Presentation_RemoveSlideMaster)) ||
          (isInstanceTypeOf(cc.target, InstanceType.Slide) &&
            cc.operationType === 'PropertyChange' &&
            cc.type === EditActionFlag.edit_Slide_SetLayout)
        )
      })
      break
    }
    default: {
      cchangeOps = action.cchangeOperations
    }
  }
  return cchangeOps
}

function opsForAddByPaste(
  action: ChangesOfAction,
  presentation: Presentation,
  reorderOpsCollector: Dict<ChangeOperation>
): ChangeOperation[] {
  let targetOp: ChangeOperation[] | undefined

  const addShapeInfo: Dict<{
    slide: Slide
    ops: ChangeOperation[]
  }> = {}
  for (let i = 0, l = action.cchangeOperations.length; i < l; i++) {
    const cc = action.cchangeOperations[i]
    if (cc.operationType === 'SpContentChange') {
      if (isInstanceTypeOf(cc.target, InstanceType.Presentation)) {
        targetOp = targetOp ?? []
        targetOp.push(cc)
      } else if (isInstanceTypeOf(cc.target, InstanceType.Slide)) {
        const rId = (cc.target as Slide).getRefId()
        const info = addShapeInfo[rId] ?? {
          slide: cc.target,
          ops: [],
        }
        info.ops.push(cc)
        addShapeInfo[rId] = info
      }
    }
  }

  if (targetOp != null) {
    reorderOpsCollector[presentation.id] = OrderChangeOperation(
      presentation,
      action.type,
      action.type
    )
    return targetOp
  }

  const infoRids = Object.keys(addShapeInfo)

  if (infoRids.length > 0) {
    return infoRids.reduce((result, rId) => {
      const info = addShapeInfo[rId]
      const ops = info.ops
      reorderOpsCollector[info.slide.id] = OrderChangeOperation(
        info.slide,
        action.type,
        action.type
      )
      return result.concat(ops)
    }, [] as ChangeOperation[])
  }

  return action.cchangeOperations
}

export function operationsFromCChangeItem<R>(
  presentation: Presentation,
  cc: ChangeOperation,
  collabManager: CollaborationManager<R>
): Operation[] {
  let ops: Operation[] = []
  switch (cc.operationType) {
    case 'SpContentChange': {
      switch (cc.type) {
        case EditActionFlag.edit_Slide_AddToSpTree: {
          const changeInfo = getContentChangeInfo(
            cc.change as EntityContentChange
          )
          const newShape = changeInfo.items[0] as SlideElement
          ops = opsFromAddShape(cc.target as Slide, newShape)

          if (isShapeOfTable(newShape)) {
            return composeDelta(ops, opsForTableStyles(presentation))
          }
          break
        }
        case EditActionFlag.edit_Slide_RemoveFromSpTree: {
          const changeInfo = getContentChangeInfo(
            cc.change as EntityContentChange
          )
          const shape = changeInfo.items[0] as SlideElement
          ops = opsFromRemoveShape(cc.target as Slide, shape, collabManager)

          if (isShapeOfTable(shape)) {
            return composeDelta(ops, opsForTableStyles(presentation))
          }
          break
        }
        case EditActionFlag.edit_Presentation_AddSlide: {
          const changeInfo = getContentChangeInfo(
            cc.change as EntityContentChange
          )
          const slide = changeInfo.items[0] as Slide
          ops = opsFromAddSlide(slide, collabManager)
          break
        }
        case EditActionFlag.edit_Presentation_RemoveSlide: {
          const changeInfo = getContentChangeInfo(
            cc.change as EntityContentChange
          )
          const slide = changeInfo.items[0] as Slide
          const orderOps = opsFromRemoveSlideOrder(slide)
          ops = opsFromRemoveSlide(slide, collabManager)
          return composeDelta(orderOps, ops)
        }
        case EditActionFlag.edit_SlideComments_AddComment: {
          const changeInfo = getContentChangeInfo(
            cc.change as EntityContentChange
          )
          const comment = changeInfo.items[0] as Comment
          ops = opsFromAddComment(
            cc.target as SlideComments,
            comment,
            collabManager
          )
          return ops
        }
        case EditActionFlag.edit_SlideComments_RemoveComment: {
          const changeInfo = getContentChangeInfo(
            cc.change as EntityContentChange
          )
          const comment = changeInfo.items[0] as Comment
          ops = opsFromRemoveComment(
            cc.target as SlideComments,
            comment,
            collabManager
          )
          return ops
        }
        case EditActionFlag.edit_Presentation_AddSlideMaster: {
          const changeInfo = getContentChangeInfo(
            cc.change as EntityContentChange
          )
          const slideMaster = changeInfo.items[0] as SlideMaster
          ops = opsFromAddSlideMaster(presentation, slideMaster, collabManager)
          return ops
        }
        case EditActionFlag.edit_Presentation_RemoveSlideMaster: {
          const changeInfo = getContentChangeInfo(
            cc.change as EntityContentChange
          )
          const slideMaster = changeInfo.items[0] as SlideMaster
          ops = opsFromRemoveSlideMaster(
            presentation,
            slideMaster,
            collabManager
          )
          return ops
        }
        case EditActionFlag.edit_Presentation_UpdateSlideMaster: {
          const changeInfo = getContentChangeInfo(
            cc.change as EntityContentChange
          )
          const slideMaster = changeInfo.items[0] as SlideMaster
          const removeOps = opsFromRemoveSlideMaster(
            presentation,
            slideMaster,
            collabManager
          )
          ops = opsFromAddSlideMaster(presentation, slideMaster, collabManager)
          ops = composeDelta(removeOps, ops)
        }
      }
      break
    }
    case 'OrderChange': {
      if (isInstanceTypeOf(cc.target, InstanceType.Slide)) {
        ops = opsFromUpdateShapesOrder(cc.target as Slide)
      } else if (isInstanceTypeOf(cc.target, InstanceType.Presentation)) {
        ops = opsFromUpdateSlidesOrder(cc.target as Presentation)
      }
      break
    }
    case 'ReplaceChange': {
      ops = opsFromReplaceChange(cc.targetParents, cc.target, collabManager)
      if (isInstanceTypeOf(cc.target, InstanceType.GraphicFrame)) {
        return composeDelta(ops, opsForTableStyles(presentation))
      }
      break
    }
    case 'UpdateChange': {
      ops = opsFromUpdateChange(cc)
      break
    }
    case 'PropertyChange': {
      switch (cc.type) {
        case EditActionFlag.edit_Slide_SetBg: {
          if (isInstanceTypeOf(cc.target, InstanceType.Slide)) {
            ops = opsFromSlideSetBg(
              cc.target as Slide,
              (cc.change as EntityPropertyChange<Bg>).new as Bg
            )
          }
          break
        }
        case EditActionFlag.edit_Slide_SetLayout: {
          if (isInstanceTypeOf(cc.target, InstanceType.Slide)) {
            const slide = cc.target as Slide
            ops = opsFromSlideChangeLayout(slide, slide.layout)
          }
          break
        }
        case EditActionFlag.edit_Slide_SetShow: {
          if (isInstanceTypeOf(cc.target, InstanceType.Slide)) {
            const slide = cc.target as Slide
            ops = opsFromSlideChangeShow(slide, slide.show)
          }
          break
        }
        case EditActionFlag.edit_Slide_SetTiming: {
          if (isInstanceTypeOf(cc.target, InstanceType.Slide)) {
            ops = opsFromSlideSetTransition(
              cc.target as Slide,
              (cc.change as EntityPropertyChange<SlideTransition>)
                .new as SlideTransition
            )
          }
          break
        }

        case EditActionFlag.edit_SlideLayout_SetTransition: {
          if (isInstanceTypeOf(cc.target, InstanceType.SlideLayout)) {
            ops = opsFromSlideLayoutSetTransition(
              cc.target as SlideLayout,
              (cc.change as EntityPropertyChange<SlideTransition>)
                .new as Nullable<SlideTransition>
            )
          }

          break
        }

        case EditActionFlag.edit_SlideMaster_SetTransition: {
          if (isInstanceTypeOf(cc.target, InstanceType.SlideMaster)) {
            ops = opsFromSlideMasterSetTransition(
              cc.target as SlideMaster,
              (cc.change as EntityPropertyChange<SlideTransition>)
                .new as Nullable<SlideTransition>
            )
          }

          break
        }
        case EditActionFlag.edit_Slide_SetAnimation: {
          if (isInstanceTypeOf(cc.target, InstanceType.SlideAnimation)) {
            ops = opsFromSlideSetAnimation(
              cc.target as SlideAnimation,
              (cc.change as EntityPropertyChange<Array<AnimationNode>>)
                .new as Array<AnimationNode>,
              (cc.change as EntityPropertyChange<Array<AnimationNode>>)
                .old as Array<AnimationNode>
            )
          }
          break
        }
        case EditActionFlag.edit_Presentation_SlideSize: {
          if (isInstanceTypeOf(cc.target, InstanceType.Presentation)) {
            ops = opsFromUpdateSlideSize(cc.target as Presentation)
          }
          break
        }
        case EditActionFlag.edit_Slide_SetComments: {
          if (isInstanceTypeOf(cc.target, InstanceType.Slide)) {
            ops = opsFromAddSlideComment(cc.target as Slide, collabManager)
          }
          break
        }
        case EditActionFlag.edit_Comment_Position: {
          if (isInstanceTypeOf(cc.target, InstanceType.Comment)) {
            ops = opsFromUpdateComment(cc.target as Comment, collabManager)
          }
          break
        }
        case EditActionFlag.edit_Presentation_SetShowPr: {
          if (isInstanceTypeOf(cc.target, InstanceType.Presentation)) {
            ops = opsFromPresentationSetShowPr(
              cc.target as Presentation,
              (cc.change as EntityPropertyChange<ShowProps>).new as ShowProps
            )
          }
          break
        }
        case EditActionFlag.edit_SlideMasterSetTxStyles: {
          if (isInstanceTypeOf(cc.target, InstanceType.SlideMaster)) {
            ops = opsFromSlideMasterSetTxStyles(
              cc.target as SlideMaster,
              (cc.change as EntityPropertyChange<TextStyles>).new as TextStyles
            )
          }
          break
        }
        case EditActionFlag.edit_Presentation_SetPresPr: {
          if (isInstanceTypeOf(cc.target, InstanceType.Presentation)) {
            ops = opsFromPresentationSetPresPr(
              cc.target as Presentation,
              (cc.change as EntityPropertyChange<PresProps>).new as PresProps
            )
          }
          break
        }
      }
      break
    }

    case 'EditChange': {
      switch (cc.type) {
        case EditActionFlag.edit_Comment_Change: {
          if (isInstanceTypeOf(cc.target, InstanceType.Comment)) {
            ops = opsFromUpdateComment(cc.target as Comment, collabManager)
          }

          break
        }
      }
      break
    }
    default:
      break
  }
  return ops
}

export function readFromChanges<R>(
  cChanges: ChangeOperation[],
  rootDoc: Presentation,
  collabManager: CollaborationManager<R>
): Operation[] {
  logger.log(`%c Changes:`, 'color: #6eaad7', cChanges)
  const compactedChanges = compactChanges(cChanges, rootDoc)
  logger.log('%c compacted Changes:', 'color: #345ad7', compactedChanges)
  return readFromCollection(
    compactedChanges,
    rootDoc,
    operationsFromCChangeItem,
    collabManager
  )
}

export function readFromChangePoints<R>(
  changePoints: ActionPoint[],
  presentation: Presentation,
  collabManager: CollaborationManager<R>
) {
  const actions = changePoints.map(changePointToAction)
  logger.log(`actions==>`, actions)

  // 顺序改变的ops需要收集起来,最后集中处理
  const reorderOpsCollector: Dict<ChangeOperation> = {}

  // 需要收集所有的cchangOps之后合在一起做compact
  const cchangeOps = actions.reduce((result, action) => {
    appendArrays(
      result,
      cchangeOpsFromAction(action, presentation, reorderOpsCollector)
    )
    return result
  }, [] as ChangeOperation[])

  let result = readFromChanges(cchangeOps, presentation, collabManager)

  const isNeedUpdateTheme = cchangeOps.find(
    (op) => op.type === EditActionFlag.edit_Presentation_RemoveSlideMaster
  )
  if (isNeedUpdateTheme) {
    const updateThemesResult = opsFromUpdateThemes(presentation, collabManager)

    result = composeDelta(result, updateThemesResult)
  }

  const needFixAnimations = animationFixer.get()
  if (needFixAnimations.length > 0) {
    for (const animation of needFixAnimations) {
      const animationResult = opsFromSlideSetAnimation(
        animation,
        animation.timeNodes
      )

      result = composeDelta(result, animationResult)
    }
  }
  animationFixer.reset()

  // group 内的 order 仍然是 replace 整个 group
  const reorderOpsKeys = Object.keys(reorderOpsCollector)
  if (reorderOpsKeys.length > 0) {
    const reorderOps = Object.keys(reorderOpsCollector).reduce((result, k) => {
      result.push(reorderOpsCollector[k])
      return result
    }, [] as ChangeOperation[])

    const reorderResult = readFromChanges(
      reorderOps,
      presentation,
      collabManager
    )
    result = composeDelta(result, reorderResult)
  }

  return result
}
