import { Slide } from '../../../../../core/Slide/Slide'
import { opsFromUpdate } from '../helpers'
import { CSldDataLayout, CSldDataLayoutTypeMap } from '../../../../defs/csld'
import { SlideDataLayout, SlideDataLayoutTypeMap } from '../../../../defs/Slide'
import {
  PPTDocDataLayout,
  PPTDocDataLayoutTypeMap,
} from '../../../../defs/pptdoc'
import { Operation, Op, OpData, withType } from '../../../../operation'
import { SlideLayout } from '../../../../../core/Slide/SlideLayout'
import { JSONFromCSld } from '../../../csld'
import { SlideTransition } from '../../../../../core/Slide/SlideTransition'
import {
  AnimationNode,
  isAnimationNodesEqual,
  SlideAnimation,
} from '../../../../../core/Slide/Animation'
import { toJSON } from '../../../../utils'
import { Bg } from '../../../../../core/SlideElement/attrs/bg'

export function opsFromSlideSetBg(slide: Slide, bg: Bg): Operation[] {
  const csldAttrs = bg ? { bg: toJSON(bg) } : undefined
  const csldOps = csldAttrs
    ? opsFromUpdate(CSldDataLayout, CSldDataLayoutTypeMap, 'attrs', csldAttrs)
    : undefined

  const slideOps = csldOps
    ? opsFromUpdate(SlideDataLayout, SlideDataLayoutTypeMap, 'csld', csldOps)
    : undefined

  const slideRId = slide.getRefId()
  return slideOps
    ? opsFromUpdate(PPTDocDataLayout, PPTDocDataLayoutTypeMap, 'slides', {
        [slideRId]: slideOps,
      })
    : []
}

export function opsFromSlideChangeLayout(
  slide: Slide,
  layout?: SlideLayout
): Operation[] {
  if (!layout) {
    return []
  }

  const layoutRef = layout.getRefId()
  const attrs = Op.Retain(OpData.JSON({ layoutRef }))

  const csld = Op.Retain(
    OpData.Delta([
      Op.Remove(CSldDataLayout.length),
      ...JSONFromCSld(slide.cSld),
    ])
  )

  const slideOps = [withType(attrs, 'attrs'), withType(csld, 'csld')]

  const slideRId = slide.getRefId()
  return slideOps
    ? opsFromUpdate(PPTDocDataLayout, PPTDocDataLayoutTypeMap, 'slides', {
        [slideRId]: slideOps,
      })
    : []
}

export function opsFromSlideChangeShow(
  slide: Slide,
  isShow: boolean
): Operation[] {
  if (isShow == null) {
    return []
  }

  const attrs = Op.Retain(
    OpData.JSON({
      ['show']: !!isShow,
    })
  )

  const slideRId = slide.getRefId()

  return opsFromUpdate(PPTDocDataLayout, PPTDocDataLayoutTypeMap, 'slides', {
    [slideRId]: [withType(attrs, 'attrs')],
  })
}

export function opsFromSlideSetTransition(
  slide: Slide,
  transition: SlideTransition
): Operation[] {
  if (transition == null) {
    return []
  }
  const attrs = Op.Retain(
    OpData.JSON({
      ['transition']: toJSON(transition),
    })
  )

  const slideRId = slide.getRefId()

  return opsFromUpdate(PPTDocDataLayout, PPTDocDataLayoutTypeMap, 'slides', {
    [slideRId]: [withType(attrs, 'attrs')],
  })
}

/** @note 若 oldTimeNodes 为空则强制更新 */
export function opsFromSlideSetAnimation(
  animation: SlideAnimation,
  newNodes: AnimationNode[],
  oldNodes?: AnimationNode[]
): Operation[] {
  const slide = animation.slide

  if (
    (oldNodes && isAnimationNodesEqual(newNodes, oldNodes)) ||
    slide.isDeleted
  ) {
    return []
  }
  const animData = toJSON(animation)
  const attrs = Op.Retain(
    OpData.JSON({
      ['timing']: animData ? animData : {},
    })
  )
  return opsFromUpdate(PPTDocDataLayout, PPTDocDataLayoutTypeMap, 'slides', {
    [slide.getRefId()]: [withType(attrs, 'attrs')],
  })
}
