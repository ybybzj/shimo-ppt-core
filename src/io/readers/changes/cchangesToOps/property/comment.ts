import { CollaborationManager } from '../../../../../collaboration/collaborationManager'
import { Comment } from '../../../../../core/Slide/Comments/Comment'
import { Slide } from '../../../../../core/Slide/Slide'
import {
  PPTDocDataLayout,
  PPTDocDataLayoutTypeMap,
} from '../../../../defs/pptdoc'
import {
  Op,
  OpData,
  Operation,
  lengthOfOperations,
  withType,
} from '../../../../operation'
import { opsFromUpdates } from '../helpers'

export function opsFromUpdateComment(
  comment: Comment,
  collabManager: CollaborationManager<any>
): Operation[] {
  const slideCommentRef = comment.slideComments.getRefId()
  const commentId = comment.getUid()
  const IODataBeforeRead = collabManager.currentPptIOData() ?? []
  const ioDataLength = lengthOfOperations(IODataBeforeRead)
  const updates = {}
  const commentOps = [Op.Retain(OpData.JSON({ [commentId]: comment.toJSON() }))]
  updates['comments'] = { [slideCommentRef]: commentOps }
  return opsFromUpdates(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    ioDataLength,
    updates
  )
}

export function opsFromAddSlideComment(
  slide: Slide,
  collabManager: CollaborationManager<any>
): Operation[] {
  const slideRef = slide.getRefId()
  const slideCommentRef = slide.slideComments!.getRefId()
  const comments = slide.slideComments!.comments
  const commentsJSON = {}
  comments.forEach((comment) => (commentsJSON[comment.getUid()] = comment.toJSON()))
  const IODataBeforeRead = collabManager.currentPptIOData()
  const ioDataLength = lengthOfOperations(IODataBeforeRead)
  const updates = {}

  const slideCommentRefOps = Op.Retain(
    OpData.JSON({
      ['commentsRef']: slideCommentRef,
    })
  )
  const slideOps = { [slideRef]: [withType(slideCommentRefOps, 'attrs')] }
  updates['slides'] = slideOps

  const commentOps = [Op.Insert(OpData.JSON(commentsJSON))]
  updates['comments'] = { [slideCommentRef]: commentOps }

  return opsFromUpdates(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    ioDataLength,
    updates
  )
}
