import { Nullable } from '../../../../../../liber/pervasive'
import { SlideMaster } from '../../../../../core/Slide/SlideMaster'
import { SlideTransition } from '../../../../../core/Slide/SlideTransition'
import { TextStyles } from '../../../../../core/textAttributes/TextStyles'
import {
  PPTDocDataLayout,
  PPTDocDataLayoutTypeMap,
} from '../../../../defs/pptdoc'
import { Op, OpData, Operation, withType } from '../../../../operation'
import { toJSON } from '../../../../utils'
import { opsFromUpdate } from '../helpers'

export function opsFromSlideMasterSetTransition(
  master: SlideMaster,
  transition: Nullable<SlideTransition>
): Operation[] {
  const attrs = Op.Retain(
    OpData.JSON({
      ['transition']: transition == null ? null : toJSON(transition),
    })
  )

  const rid = master.getRefId()

  return opsFromUpdate(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    'slideMasters',
    {
      [rid]: [withType(attrs, 'attrs')],
    }
  )
}

export function opsFromSlideMasterSetTxStyles(
  master: SlideMaster,
  txStyles: Nullable<TextStyles>
): Operation[] {
  const attrs = Op.Retain(
    OpData.JSON({
      ['txStyles']: txStyles == null ? null : toJSON(txStyles),
    })
  )

  const rid = master.getRefId()

  return opsFromUpdate(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    'slideMasters',
    {
      [rid]: [withType(attrs, 'attrs')],
    }
  )
}
