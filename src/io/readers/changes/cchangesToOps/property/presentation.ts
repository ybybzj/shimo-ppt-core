import { PresProps } from '../../../../../core/Slide/PresProps'
import { Presentation } from '../../../../../core/Slide/Presentation'
import { ShowProps } from '../../../../../core/Slide/ShowProps'
import { PresData } from '../../../../dataType/presentation'
import {
  PPTDocDataLayout,
  PPTDocDataLayoutTypeMap,
} from '../../../../defs/pptdoc'
import {
  PresentationDataLayout,
  PresentationDataLayoutTypeMap,
} from '../../../../defs/presentation'
import { Operation } from '../../../../operation'
import { toJSON } from '../../../../utils'
import { opsFromUpdate } from '../helpers'

export function opsFromPresentationSetShowPr(
  _presentation: Presentation,
  showPr: ShowProps
): Operation[] {
  const presentationPrData = showPr ? { showPr: toJSON(showPr) } : undefined
  const presentationOps = presentationPrData
    ? opsFromUpdate(
        PresentationDataLayout,
        PresentationDataLayoutTypeMap,
        'PresentationPr',
        presentationPrData
      )
    : undefined

  return presentationOps
    ? opsFromUpdate(
        PPTDocDataLayout,
        PPTDocDataLayoutTypeMap,
        'presentation',
        presentationOps
      )
    : []
}
export function opsFromPresentationSetPresPr(
  presentation: Presentation,
  presPr: PresProps
): Operation[] {
  const presPrData: { pres: PresData } | undefined = presPr
    ? { pres: toJSON(presPr, presentation) }
    : undefined
  const presentationOps = presPrData
    ? opsFromUpdate(
        PresentationDataLayout,
        PresentationDataLayoutTypeMap,
        'Pres',
        presPrData
      )
    : undefined

  return presentationOps
    ? opsFromUpdate(
        PPTDocDataLayout,
        PPTDocDataLayoutTypeMap,
        'presentation',
        presentationOps
      )
    : []
}
