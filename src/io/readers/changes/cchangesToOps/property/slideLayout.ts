import { Nullable } from '../../../../../../liber/pervasive'
import { SlideLayout } from '../../../../../core/Slide/SlideLayout'
import { SlideTransition } from '../../../../../core/Slide/SlideTransition'
import {
  PPTDocDataLayout,
  PPTDocDataLayoutTypeMap,
} from '../../../../defs/pptdoc'
import { Op, OpData, Operation, withType } from '../../../../operation'
import { toJSON } from '../../../../utils'
import { opsFromUpdate } from '../helpers'

export function opsFromSlideLayoutSetTransition(
  layout: SlideLayout,
  transition: Nullable<SlideTransition>
): Operation[] {
  const attrs = Op.Retain(
    OpData.JSON({
      ['transition']: transition == null ? null : toJSON(transition),
    })
  )

  const rid = layout.getRefId()

  return opsFromUpdate(PPTDocDataLayout, PPTDocDataLayoutTypeMap, 'layouts', {
    [rid]: [withType(attrs, 'attrs')],
  })
}
