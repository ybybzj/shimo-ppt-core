import { Presentation } from '../../../../../core/Slide/Presentation'
import { Operation } from '../../../../operation'
import { getGlobalTableStyles } from '../../../Presentation'
import { isEmptyPropValue } from '../../../../utils'
import {
  PresentationDataLayout,
  PresentationDataLayoutTypeMap,
} from '../../../../defs/presentation'
import { opsFromUpdate } from '../helpers'
import {
  PPTDocDataLayout,
  PPTDocDataLayoutTypeMap,
} from '../../../../defs/pptdoc'

export function opsForTableStyles(presentaion: Presentation): Operation[] {
  const tableStylesData = getGlobalTableStyles(presentaion)
  if (isEmptyPropValue(tableStylesData)) {
    return []
  }

  const presentationOps = opsFromUpdate(
    PresentationDataLayout,
    PresentationDataLayoutTypeMap,
    'TableStyle',
    tableStylesData
  )

  return opsFromUpdate(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    'presentation',
    presentationOps
  )
}
