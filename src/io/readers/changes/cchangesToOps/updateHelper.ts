import { Dict } from '../../../../../liber/pervasive'
import { ManagedSliceUtil } from '../../../../common/managedArray'
import { UpdateChange } from '../../../../core/changes/updateChange'
import { Xfrm } from '../../../../core/SlideElement/attrs/shapePrs'
import { Table } from '../../../../core/Table/Table'
import { TableCell } from '../../../../core/Table/TableCell'
import { TableRow } from '../../../../core/Table/TableRow'
import { findClosestParent } from '../../../../core/utilities/finders'
import { EditActionFlag } from '../../../../core/EditActionFlag'
import { InstanceType, isInstanceTypeOf } from '../../../../core/instanceTypes'
import { SlideElement } from '../../../../core/SlideElement/type'
import { ChangeOperation } from '../cchangeOperation'
import {
  UpdateTableCellContentChange,
  UpdateTableRowAttrsChange,
  UpdateGraphicFrameAttrsChange,
  UpdateTableCellAttrsChange,
  hashUpdateChange,
  UpdateTableGridChange,
} from '../updateChangesForTable'

/** @returns boolean - 是否支持细粒度更新,是则填充 collections, 否则按原 Shape 级别更新 */
export function fillCollectionsFromChanges(
  collections: Dict<UpdateChange>,
  cchange: ChangeOperation,
  sp: SlideElement
): boolean {
  const actionType = cchange.actionType
  switch (actionType) {
    case EditActionFlag.action_Document_MoveTableBorder:
    case EditActionFlag.action_Presentation_ApplyTable: {
      return handleTableApplyAction(collections, cchange, sp)
    }
    case EditActionFlag.action_SlideElements_EndControl:
    case EditActionFlag.action_SlideElements_EndMove:
    case EditActionFlag.action_SlideElements_EndRotate:
    case EditActionFlag.action_SlideElements_EndResize:
      return handleTrackAction(collections, cchange, sp)
  }
  // 文本输入相关(仅处理tablecell)
  if (cchange.type === EditActionFlag.edit_TextContent_Change) {
    return handleTextEditAction(collections, cchange, sp)
  }
  return false
}

/** 将 change 按更新粒度拆分而非业务拆分
 * - 好处: 收束业务逻辑, opsFrom 只需要专注处理 toJSON, 否则新增一个 Action 需要增加对应的 ops 处理; 方便处理内部去重
 * - 缺点: 会导致后续不必要的 merge
 */
function handleTextEditAction(
  collections: Dict<UpdateChange>,
  cchange: ChangeOperation,
  sp: SlideElement
) {
  const { actionType, type: changeType } = cchange
  const id = sp.id
  if (sp.instanceType !== InstanceType.GraphicFrame) return false
  const tableCell = findClosestParent(
    cchange.target,
    (parent) => parent.instanceType === InstanceType.TableCell
  ) as TableCell | undefined
  if (!tableCell) return false
  const tableRow = tableCell.parent
  const graphicFrame = tableRow.parent.parent

  let hash = hashUpdateChange(tableCell, 'TableCellContent')
  if (collections[hash] == null) {
    collections[hash] = new UpdateTableCellContentChange(
      tableCell,
      changeType,
      id,
      actionType
    )
  }

  hash = hashUpdateChange(tableRow, 'TableRowAttrs')
  if (collections[hash] == null) {
    collections[hash] = new UpdateTableRowAttrsChange(
      tableRow,
      changeType,
      id,
      actionType
    )
  }

  hash = hashUpdateChange(graphicFrame, 'GraphicFrameAttrs')
  if (collections[hash] == null) {
    collections[hash] = new UpdateGraphicFrameAttrsChange(
      graphicFrame,
      changeType,
      id,
      actionType
    )
  }
  return true
}

function handleTableApplyAction(
  collections: Dict<UpdateChange>,
  cchange: ChangeOperation,
  sp: SlideElement
): boolean {
  const { actionType, type: changeType } = cchange
  if (sp.instanceType !== InstanceType.GraphicFrame) return false
  switch (actionType) {
    // 调整分隔线(水平 target 为 table, 垂直 target 为 tableRow)
    case EditActionFlag.action_Document_MoveTableBorder: {
      const target = cchange.target as Table | TableRow

      if (
        target.instanceType !== InstanceType.Table &&
        target.instanceType !== InstanceType.TableRow
      ) {
        return false
      }
      const isTableRow = target.instanceType === InstanceType.TableRow
      const table = isTableRow ? target.parent : target
      const graphicFrame = table?.parent
      if (!graphicFrame || !table) return false

      const tableGridHash = hashUpdateChange(table, 'TableGrid')
      if (collections[tableGridHash] == null) {
        collections[tableGridHash] = new UpdateTableGridChange(
          table,
          changeType,
          sp.id,
          actionType
        )
      }
      const frameAttrsHash = hashUpdateChange(graphicFrame, 'GraphicFrameAttrs')
      if (collections[frameAttrsHash] == null) {
        collections[frameAttrsHash] = new UpdateGraphicFrameAttrsChange(
          graphicFrame,
          changeType,
          sp.id,
          actionType
        )
      }
      ManagedSliceUtil.forEach(table.children, (row) => {
        const rowAttrsHash = hashUpdateChange(row, 'TableRowAttrs')
        if (collections[rowAttrsHash] == null) {
          collections[rowAttrsHash] = new UpdateTableRowAttrsChange(
            row,
            changeType,
            sp.id,
            actionType
          )
        }
      })
      return true
    }
    // BorderChange/BackGroundChange
    case EditActionFlag.action_Presentation_ApplyTable: {
      const tableCell = cchange.target as TableCell
      if (tableCell.instanceType !== InstanceType.TableCell) return false

      const hash = hashUpdateChange(tableCell, 'TableCellAttrs')
      if (collections[hash] == null) {
        collections[hash] = new UpdateTableCellAttrsChange(
          tableCell,
          changeType,
          sp.id,
          actionType
        )
      }
      return true
    }
  }
  return false
}

/** @param sp 顶层 closestParent */
function handleTrackAction(
  collections: Dict<UpdateChange>,
  cchange: ChangeOperation,
  sp: SlideElement
) {
  const { actionType, type: changeType, target } = cchange
  const id = sp.id
  // 目前暂时只接收 spPr-xfrm 的细粒度修改, 如存在 connector 位移导致的 nvSpPr change 则转为 replaceChange
  const isValidTarget = isInstanceTypeOf(target, InstanceType.Xfrm)
  if (!isValidTarget) return false
  const hash = hashUpdateChange(sp, 'SpXfrmAttrs')
  switch (sp.instanceType) {
    case InstanceType.Shape:
    case InstanceType.ImageShape:
    case InstanceType.GroupShape:
      switch (changeType) {
        case EditActionFlag.edit_Xfrm_SetOffX:
        case EditActionFlag.edit_Xfrm_SetOffY:
        case EditActionFlag.edit_Xfrm_SetExtX:
        case EditActionFlag.edit_Xfrm_SetExtY:
        case EditActionFlag.edit_Xfrm_SetFlipH:
        case EditActionFlag.edit_Xfrm_SetFlipV:
        case EditActionFlag.edit_Xfrm_SetRot: {
          // group 和 group 内 shape 的变更一律转为 replaceChange
          if (
            sp.instanceType === InstanceType.GroupShape ||
            (target as Xfrm).parent?.parent?.group != null
          ) {
            return false
          }
          if (collections[hash] == null) {
            collections[hash] = new UpdateChange(sp, changeType, id, actionType)
          }
          return true
        }
      }
      return false
    case InstanceType.GraphicFrame: {
      if (
        changeType !== EditActionFlag.edit_Xfrm_SetOffX &&
        changeType !== EditActionFlag.edit_Xfrm_SetOffY
      ) {
        return false
      }
      if (collections[hash] == null) {
        collections[hash] = new UpdateGraphicFrameAttrsChange(
          sp,
          changeType,
          id,
          actionType
        )
      }
      return true
    }
  }
}
