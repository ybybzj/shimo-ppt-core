import { Op, OpData, Operation, lengthOfOperations } from '../../../operation'
import {
  opsFromUpdates,
  opsFromUpdateSlideShapes,
  opsFromUpdateSlideMasterRefs,
} from './helpers'
import { Slide } from '../../../../core/Slide/Slide'
import { JSONFromSlide } from '../../../readers/Slide'
import { JSONFromSpItem } from '../../csld'
import { SlideElement } from '../../../../core/SlideElement/type'
import { CollaborationManager } from '../../../../collaboration/collaborationManager'
import { PPTDocDataLayout, PPTDocDataLayoutTypeMap } from '../../../defs/pptdoc'
import { opsFromInsertSlideNotes } from './replaceItem'
import { SlideComments } from '../../../../core/Slide/Comments/SlideComments'
import { Comment } from '../../../../core/Slide/Comments/Comment'
import { SlideMaster } from '../../../../core/Slide/SlideMaster'
import { JSONFromSlideMaster } from '../../SlideMaster'
import { getLayouts, getThemes } from '../../PPTDoc'
import { Presentation } from '../../../../core/Slide/Presentation'
import { getOrderForSlideElement } from '../../../../core/utilities/shape/getters'

export function opsFromAddShape(
  slide: Slide,
  newShape: SlideElement
): Operation[] {
  if (!slide || !newShape) {
    return []
  }

  const shapeOps = JSONFromSpItem(newShape, getOrderForSlideElement(newShape))
  const shapeRId = newShape.getRefId()

  return shapeOps
    ? opsFromUpdateSlideShapes(slide, {
        [shapeRId]: shapeOps,
      })
    : []
}

export function opsFromAddSlide(
  newSlide: Slide,
  collabManager: CollaborationManager<any>
): Operation[] {
  if (!newSlide) {
    return []
  }

  const slideOps = JSONFromSlide(newSlide)
  const slideRId = newSlide.getRefId()

  const IODataBeforeRead = collabManager.currentPptIOData() ?? []
  const ioDataLength = lengthOfOperations(IODataBeforeRead)
  let updates = {}
  const notes = newSlide.notes
  if (notes && !notes.isEmpty()) {
    if (!notes.slide) {
      notes.slide = newSlide
    }
    updates = opsFromInsertSlideNotes(notes, collabManager)
  }
  const slideUpdateOps = {
    [slideRId]: slideOps,
  }

  updates['slides'] = slideUpdateOps

  return opsFromUpdates(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    ioDataLength,
    updates
  )
}

export function opsFromAddComment(
  slideComment: SlideComments,
  comment: Comment,
  collabManager: CollaborationManager<any>
): Operation[] {
  const slideCommentRef = slideComment.getRefId()
  const commentId = comment.getUid()
  const IODataBeforeRead = collabManager.currentPptIOData() ?? []
  const ioDataLength = lengthOfOperations(IODataBeforeRead)
  const updates = {}
  const commentOps = [Op.Retain(OpData.JSON({ [commentId]: comment.toJSON() }))]
  updates['comments'] = { [slideCommentRef]: commentOps }
  return opsFromUpdates(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    ioDataLength,
    updates
  )
}

export function opsFromAddSlideMaster(
  presentation: Presentation,
  slideMaster: SlideMaster,
  collabManager: CollaborationManager<any>
): Operation[] {
  if (!slideMaster) return []

  const slideMasterOps = JSONFromSlideMaster(slideMaster)
  const slideMasterRId = slideMaster.getRefId()

  const IODataBeforeRead = collabManager.currentPptIOData() ?? []
  const ioDataLength = lengthOfOperations(IODataBeforeRead)
  const masterRefsUpdateOps = opsFromUpdateSlideMasterRefs(
    presentation.slideMasters,
    IODataBeforeRead
  )
  const layoutsUpdateOps = getLayouts({
    layouts: slideMaster.layouts,
  } as any)
  const slideMasterUpdateOps = {
    [slideMasterRId]: slideMasterOps,
  }

  const updates = {
    ['presentation']: masterRefsUpdateOps,
    ['layouts']: layoutsUpdateOps,
    ['slideMasters']: slideMasterUpdateOps,
  }

  const theme = slideMaster.theme
  if (theme) {
    updates['themes'] = getThemes({ themes: [theme] } as any)
  }

  return opsFromUpdates(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    ioDataLength,
    updates
  )
}
