import {
  Operation,
  Op,
  withType,
  OpData,
  lengthOfOperations,
} from '../../../operation'
import { ClassObjectWithId } from '../../../readers/changes/helper'

import { EntityObject } from '../../../../core/changes/type'

import {
  opsFromUpdateSlideShapes,
  opsFromUpdates,
  opsFromUpdate,
  keysFromMapData,
  UpdatesForOps,
  opsFromUpdateSlideLayoutShapes,
  opsFromUpdateSlideMasterShapes,
} from './helpers'
import { Slide } from '../../../../core/Slide/Slide'
import { SlideElement } from '../../../../core/SlideElement/type'
import { JSONFromSpItem } from '../../csld'
import { InstanceType } from '../../../../core/instanceTypes'
import { Notes } from '../../../../core/Slide/Notes'
import { genPPTInfoData } from '../../../infoData'
import { CollaborationManager } from '../../../../collaboration/collaborationManager'
import { JSONFromNotesMaster } from '../../NotesMaster'
import { JSONFromSlideNotes } from '../../Notes'
import { PPTDocDataLayout, PPTDocDataLayoutTypeMap } from '../../../defs/pptdoc'
import { NotesDataLayout, NotesDataLayoutTypeMap } from '../../../defs/Notes'
import { CSldDataLayout, CSldDataLayoutTypeMap } from '../../../defs/csld'
import { Dict } from '../../../../../liber/pervasive'
import {
  PresentationDataLayout,
  PresentationDataLayoutTypeMap,
} from '../../../defs/presentation'
import { JSONFromTheme } from '../../Theme'
import { SlideMaster } from '../../../../core/Slide/SlideMaster'
import { SlideLayout } from '../../../../core/Slide/SlideLayout'
import { getOrderForSlideElement } from '../../../../core/utilities/shape/getters'

/** todo: 批量修改 shape 内的 JSONOpData 属性时, 后续考虑 retain 单独修改(以 ChangeSize 为例)*/
export function opsFromReplaceChange(
  parents: ClassObjectWithId[],
  target: EntityObject,
  collabManager: CollaborationManager<any>
): Operation[] {
  const [root, mainContainer, subParent, subChildParent] = parents

  if (root?.instance?.instanceType === InstanceType.Presentation) {
    const container = mainContainer?.instance
    const containerType = container?.instanceType
    const subParentType = subParent?.instance?.instanceType

    if (containerType === InstanceType.Slide) {
      if (subParentType === InstanceType.CSld) {
        return opsFromReplaceShapeInSlide(
          container as Slide,
          target as SlideElement
        )
      } else if (subParentType === InstanceType.Notes) {
        return opsFromUpdateNotes(
          subParent.instance as Notes,
          target as SlideElement,
          collabManager
        )
      }
    } else if (containerType === InstanceType.SlideMaster) {
      if (subParentType === InstanceType.CSld) {
        return opsFromReplaceShapeInSlideMaster(
          container as SlideMaster,
          target as SlideElement
        )
      } else if (subParentType === InstanceType.SlideLayout) {
        if (
          subChildParent &&
          subChildParent.instance.instanceType === InstanceType.CSld
        ) {
          return opsFromReplaceShapeInSlideLayout(
            subParent.instance as SlideLayout,
            target as SlideElement
          )
        }
      }
    }
  } else {
    console.warn('[opsFromParentTargets] invalid parents!', parents)
  }
  return []
}

function opsFromReplaceShapeInSlide(
  slide: Slide,
  shape: SlideElement
): Operation[] {
  const replaceOps = opsFromReplaceShape(shape)

  return replaceOps ? opsFromUpdateSlideShapes(slide, replaceOps) : []
}

function opsFromReplaceShapeInSlideLayout(
  slideLayout: SlideLayout,
  shape: SlideElement
): Operation[] {
  const replaceOps = opsFromReplaceShape(shape)

  return replaceOps
    ? opsFromUpdateSlideLayoutShapes(slideLayout, replaceOps)
    : []
}

function opsFromReplaceShapeInSlideMaster(
  slideMaster: SlideMaster,
  shape: SlideElement
): Operation[] {
  const replaceOps = opsFromReplaceShape(shape)

  return replaceOps
    ? opsFromUpdateSlideMasterShapes(slideMaster, replaceOps)
    : []
}

function opsFromReplaceShape(
  shape: SlideElement
): Dict<Operation[]> | undefined {
  const rId = shape.getRefId()
  const newShapeData = JSONFromSpItem(shape, getOrderForSlideElement(shape))
  return newShapeData
    ? {
        [rId]: [Op.Remove(1), newShapeData[0]],
      }
    : undefined
}

function opsFromUpdateNotes(
  notes: Notes,
  shape: SlideElement,
  collabManager: CollaborationManager<any>
): Operation[] {
  const infoDataBeforeRead = collabManager.currentInfoData()
  const IODataBeforeRead = collabManager.currentPptIOData()
  const ioDataLength = lengthOfOperations(IODataBeforeRead)

  // 至前判断是否为没有赋值refId且body为空的notes
  const isEmptyNotes = notes.isEmptyNotes()

  let isNewNotes = isEmptyNotes
  if (isNewNotes === false) {
    // [infoDataBeforeRead.notes] all have refId set or with a body that is not empty
    const notesRefsBeforeRead = infoDataBeforeRead
      ? infoDataBeforeRead.notes.map((note) => note.getRefId())
      : []

    const notesRef = notes.getRefId()
    isNewNotes = notesRefsBeforeRead.indexOf(notesRef) <= -1
  }

  const needInsertNotes = isNewNotes && !isEmptyNotes

  if (needInsertNotes) {
    const updates = opsFromInsertSlideNotes(notes, collabManager)
    return opsFromUpdates(
      PPTDocDataLayout,
      PPTDocDataLayoutTypeMap,
      ioDataLength,
      updates
    )
  } else if (!isNewNotes) {
    const notesRef = notes.getRefId()
    if (!notes.isEmpty()) {
      // 更新已有notes
      const replaceOps = opsFromReplaceShape(shape)
      if (!replaceOps) {
        return []
      }
      const csldOps = opsFromUpdate(
        CSldDataLayout,
        CSldDataLayoutTypeMap,
        'spTree',
        replaceOps
      )

      const notesOps = opsFromUpdate(
        NotesDataLayout,
        NotesDataLayoutTypeMap,
        'csld',
        csldOps
      )

      return opsFromUpdates(
        PPTDocDataLayout,
        PPTDocDataLayoutTypeMap,
        ioDataLength,
        {
          ['notes']: {
            [notesRef]: notesOps,
          },
        }
      )
    } else {
      notes.refId = undefined

      // 删除notes
      const updates = {}
      // slide updates
      const slide = notes.slide!
      const slideRef = slide!.getRefId()
      const slideAttrs = Op.Retain(
        OpData.JSON({
          ['notesRef']: null,
        })
      )
      const slideUpdateOps = {
        [slideRef]: [withType(slideAttrs, 'attrs')],
      }
      updates['slides'] = slideUpdateOps

      // notes updates
      const notesUpdateOps = {
        [notesRef]: [Op.Remove(NotesDataLayout.length)],
      }

      updates['notes'] = notesUpdateOps

      return opsFromUpdates(
        PPTDocDataLayout,
        PPTDocDataLayoutTypeMap,
        ioDataLength,
        updates
      )
    }
  }
  return []
}

type TypesOfPPTLayout = UpdatesForOps<
  typeof PPTDocDataLayout,
  typeof PPTDocDataLayoutTypeMap
>
export function opsFromInsertSlideNotes(
  notes: Notes,
  collabManager: CollaborationManager<any>
): Partial<TypesOfPPTLayout> {
  const IODataBeforeRead = collabManager.currentPptIOData() ?? []
  // const ioDataLength = collabManager.ioAdapter().length(IODataBeforeRead)
  const presentation = notes.slide!.presentation
  const noteMasterRefsBeforeRead = keysFromMapData(
    IODataBeforeRead,
    'notesMasters',
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap
  )
  const themeRefsBeforeRead = keysFromMapData(
    IODataBeforeRead,
    'themes',
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap
  )
  const notesRef = notes.getRefId()
  // 插入一个新notes
  const updates: Partial<TypesOfPPTLayout> = {}
  // slide updates
  const slideRef = notes.slide!.getRefId()
  const slideAttrs = Op.Retain(
    OpData.JSON({
      ['notesRef']: notesRef,
    })
  )
  const slideUpdateOps = {
    [slideRef]: [withType(slideAttrs, 'attrs')],
  }
  updates['slides'] = slideUpdateOps

  const currentPPTInfo = genPPTInfoData(presentation)

  const currentNoteMastersRefs = currentPPTInfo.noteMasters.map((noteMaster) =>
    noteMaster.getRefId()
  )

  const currentThemeRefs = currentPPTInfo.themes.map((theme) =>
    theme.getRefId()
  )
  // noteMaster updates
  const noteMasterRef = notes.master?.getRefId()
  if (noteMasterRef && noteMasterRefsBeforeRead.indexOf(noteMasterRef) <= -1) {
    const noteMasterUpdateOps = {
      [noteMasterRef]: JSONFromNotesMaster(notes.master!),
    }

    updates['notesMasters'] = noteMasterUpdateOps

    const opsFromPresentation = opsFromUpdate(
      PresentationDataLayout,
      PresentationDataLayoutTypeMap,
      'NotesMaster',
      {
        noteMasterRefs: currentNoteMastersRefs,
      }
    )

    updates['presentation'] = opsFromPresentation

    const themeRef = notes.master!.theme!.getRefId()

    if (
      themeRefsBeforeRead.indexOf(themeRef) <= -1 &&
      currentThemeRefs.indexOf(themeRef) > -1
    ) {
      const themeOps = {
        [themeRef]: JSONFromTheme(notes.master!.theme!),
      }

      updates['themes'] = themeOps
    }
  }

  // notes updates
  const notesUpdateOps = {
    [notesRef]: JSONFromSlideNotes(notes),
  }

  updates['notes'] = notesUpdateOps

  return updates
}
