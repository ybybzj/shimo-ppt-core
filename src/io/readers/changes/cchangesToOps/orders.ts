import { Operation, Op } from '../../../operation'
import { Presentation } from '../../../../core/Slide/Presentation'
import { PPTDocDataLayout, PPTDocDataLayoutTypeMap } from '../../../defs/pptdoc'
import { opsFromUpdate, opsFromUpdateSlideShapes } from './helpers'
import {
  PresentationDataLayout,
  PresentationDataLayoutTypeMap,
} from '../../../defs/presentation'
import { Slide } from '../../../../core/Slide/Slide'
import { Dict } from '../../../../../liber/pervasive'

export function opsFromUpdateSlidesOrder(
  presentation: Presentation
): Operation[] {
  const slides = presentation.slides
  let slidesRefsData
  if (slides.length <= 0) {
    slidesRefsData = {}
  }

  slidesRefsData = slides.reduce((result, slide, i) => {
    const sldRId = slide.getRefId()
    result[sldRId] = { order: i }
    return result
  }, {})

  const opsFromPresentation = opsFromUpdate(
    PresentationDataLayout,
    PresentationDataLayoutTypeMap,
    'Slides',
    slidesRefsData
  )

  return opsFromUpdate(
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap,
    'presentation',
    opsFromPresentation
  )
}

export function opsFromUpdateShapesOrder(slide: Slide): Operation[] {
  const shapesData: Dict<Operation[]> = slide.cSld.spTree.reduce(
    (result, shape, index) => {
      const rId = shape.getRefId()
      result[rId] = [
        Op.Retain(1, {
          order: index,
        }),
      ]
      return result
    },
    {}
  )

  return opsFromUpdateSlideShapes(slide, shapesData)
}
