import { Presentation } from '../../../core/Slide/Presentation'
import { EntityObject } from '../../../core/changes/type'
import { getParent } from '../../../core/utilities/finders'
import { gGlobalTableStyles } from '../../../core/Slide/GlobalTableStyles'

export interface ClassObjectWithId {
  id: string
  instance: EntityObject // 该节点的具体实例：Shape、Slide
}

export function getAncestorInfo(
  targetClass: EntityObject
  // slide: Slide
): ClassObjectWithId[] {
  let targetParent = getParent(targetClass)
  const result: ClassObjectWithId[] = []
  while (targetParent) {
    result.push({
      id: targetParent.id,
      instance: targetParent,
    })
    const upParent = getParent(targetParent)
    if (upParent === targetParent) {
      console.error(
        '%c [getAncestorIds]invalid parent!',
        'color: #FFCD3A',
        targetClass
      )
      break
    }
    targetParent = upParent
  }
  return result.reverse()
}

export function findClosestParentByInfo(
  parents: ClassObjectWithId[],
  finder: (parent: EntityObject) => boolean
): EntityObject | undefined {
  const len = parents.length
  if (len <= 0) return undefined

  let targetParent: EntityObject | undefined
  for (let i = len - 1; i >= 0; i--) {
    targetParent = parents[i].instance
    if (finder(targetParent)) {
      break
    } else {
      targetParent = undefined
    }
  }

  return targetParent
}

export function getParentIds(
  targetClass: EntityObject,
  presentation: Presentation
): string[] {
  let targetParent = getParent(targetClass, presentation)
  const result: string[] = []
  while (targetParent) {
    result.push(targetParent.id)
    const upParent = getParent(targetParent, presentation)
    if (upParent === targetParent) {
      console.error(
        '%c [getParentIds]invalid parent!',
        'color: #FFCD3A',
        targetClass
      )
      break
    }
    targetParent = upParent
  }
  return result.reverse()
}

export function getClassInstanceType(instance: EntityObject) {
  return instance.instanceType || 'UnKnown'
}

export interface RootElementIdsMap {
  doc: string
  tableStyles: string[]
  noteMasters: string[]
}

export function getRootElementIds(rootDoc: Presentation): RootElementIdsMap {
  const doc = rootDoc.id
  const tableStyles = Object.keys(
    gGlobalTableStyles.tableStyles.getIdGuidMap() || {}
  )
  const noteMasters = rootDoc.notesMasters.map((noteMaster) => noteMaster.id)
  return {
    doc,
    tableStyles,
    noteMasters,
  }
}

export function isTargetInDoc(
  presentation: Presentation,
  target: EntityObject
): boolean {
  const rootIds = getRootElementIds(presentation)
  const rootParentId = getParentIds(target, presentation)[0]
  return (
    isRootElementId(target.id, rootIds) ||
    isRootElementId(rootParentId, rootIds)
  )
}
export function isRootElementId(
  id: string,
  rootIds: RootElementIdsMap
): boolean {
  const { doc, tableStyles, noteMasters } = rootIds

  if (
    id === doc ||
    tableStyles.indexOf(id) > -1 ||
    noteMasters.indexOf(id) > -1
  ) {
    return true
  }

  return false
}
