import { EntityObject } from '../../../core/changes/type'
import { SpUpdateUnit, UpdateChange } from '../../../core/changes/updateChange'
import { GraphicFrame } from '../../../core/SlideElement/GraphicFrame'
import { Table } from '../../../core/Table/Table'
import { TableCell } from '../../../core/Table/TableCell'
import { TableRow } from '../../../core/Table/TableRow'
import { EditActionFlag } from '../../../core/EditActionFlag'

/** 暂定除 GraphicFrame 外其它 sp 均更新 SpPr */

export function hashUpdateChange(
  target: EntityObject,
  updateType: SpUpdateUnit
) {
  return `${target.id}${updateType}`
}

export class UpdateTableCellAttrsChange extends UpdateChange {
  constructor(
    targetEntity: TableCell,
    type: EditActionFlag,
    parentId,
    actionType: EditActionFlag
  ) {
    super(targetEntity, type, parentId, actionType, 'TableCellAttrs')
  }
}

export class UpdateTableCellContentChange extends UpdateChange {
  constructor(
    targetEntity: TableCell,
    type: EditActionFlag,
    parentId,
    actionType: EditActionFlag
  ) {
    super(targetEntity, type, parentId, actionType, 'TableCellContent')
  }
}

export class UpdateTableRowAttrsChange extends UpdateChange {
  constructor(
    targetEntity: TableRow,
    type: EditActionFlag,
    parentId,
    actionType: EditActionFlag
  ) {
    super(targetEntity, type, parentId, actionType, 'TableRowAttrs')
  }
}

export class UpdateTableAttrsChange extends UpdateChange {
  constructor(
    targetEntity: Table,
    type: EditActionFlag,
    parentId,
    actionType: EditActionFlag
  ) {
    super(targetEntity, type, parentId, actionType, 'TableAttrs')
  }
}

export class UpdateTableGridChange extends UpdateChange {
  constructor(
    targetEntity: Table,
    type: EditActionFlag,
    parentId,
    actionType: EditActionFlag
  ) {
    super(targetEntity, type, parentId, actionType, 'TableGrid')
  }
}

export class UpdateGraphicFrameAttrsChange extends UpdateChange {
  constructor(
    targetEntity: GraphicFrame,
    type: EditActionFlag,
    parentId,
    actionType: EditActionFlag
  ) {
    super(targetEntity, type, parentId, actionType, 'GraphicFrameAttrs')
  }
}
