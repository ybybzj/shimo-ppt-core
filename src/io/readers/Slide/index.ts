import { Slide } from '../../../core/Slide/Slide'
import { assignClrMapOverride } from '../../../core/color/clrMap'
import { SlideAttrs, SlideData } from '../../dataType/slide'
import { Op, OpData, withType } from '../../operation'
import { assignJSON, assignVal } from '../../utils'
import { JSONFromCSld } from '../csld'

export function getSlideAttrs(slide: Slide): SlideAttrs {
  const data: SlideAttrs = {}
  assignVal(data, 'show', slide.show)
  assignVal(data, 'showMasterPhAnim', slide.showMasterPhAnim)
  assignVal(data, 'showMasterSp', slide.showMasterSp)

  assignClrMapOverride(data, 'clrMap', slide.clrMap)

  assignJSON(data, 'transition', slide.timing)
  assignVal(data, 'layoutRef', slide.layout?.getRefId())
  const hasNotes = slide.notes && !slide.notes.isEmpty()
  if (hasNotes) {
    assignVal(data, 'notesRef', slide.notes?.getRefId())
  }
  const hasComments = slide.slideComments?.comments?.length ?? false
  if (hasComments) {
    assignVal(data, 'commentsRef', slide.slideComments?.getRefId())
  }
  if (!slide.animation.isEmpty()) {
    assignJSON(data, 'timing', slide.animation)
  }
  return data
}

export function JSONFromSlide(slide: Slide): SlideData {
  const attrs = Op.Insert(OpData.JSON(getSlideAttrs(slide)))

  const csld = Op.Insert(OpData.Delta(JSONFromCSld(slide.cSld)))

  return [withType(attrs, 'attrs'), withType(csld, 'csld')]
}
