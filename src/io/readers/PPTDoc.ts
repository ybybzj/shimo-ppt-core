import { Dict } from '../../../liber/pervasive'
import { Presentation } from '../../core/Slide/Presentation'
import { CommentsData } from '../dataType/comment'
import { NotesData } from '../dataType/notes'
import { NotesMasterData } from '../dataType/notesMaster'
import { PPTDocData } from '../dataType/pptDoc'
import { SlideData } from '../dataType/slide'
import { SlideLayoutData } from '../dataType/slideLayout'
import { SlideMasterData } from '../dataType/slideMaster'
import { ThemeData } from '../dataType/theme'
import { genPPTInfoData, PPTInfoData } from '../infoData'
import { EmptyOpData, Op, OpData, withType } from '../operation'
import { getMapOpData } from './helper'
import { JSONFromSlideNotes } from './Notes'
import { JSONFromNotesMaster } from './NotesMaster'
import { JSONFromPresentation } from './Presentation'
import { JSONFromSlide } from './Slide'
import { JSONFromSlideComment } from './SlideComments'
import { JSONFromSlideLayout } from './SlideLayout'
import { JSONFromSlideMaster } from './SlideMaster'
import { JSONFromTheme } from './Theme'

export function getLayouts({ layouts }: PPTInfoData): Dict<SlideLayoutData> {
  return getMapOpData(layouts, JSONFromSlideLayout) ?? {}
}

export function getSlideMasters({
  slideMasters,
}: PPTInfoData): Dict<SlideMasterData> {
  return getMapOpData(slideMasters, JSONFromSlideMaster) ?? {}
}

export function getSlides({ slides }: PPTInfoData): Dict<SlideData> {
  return getMapOpData(slides, JSONFromSlide) ?? {}
}

export function getThemes({
  themes,
}: PPTInfoData): Dict<ThemeData> | undefined {
  return getMapOpData(themes, JSONFromTheme)
}

export function getNotesMasters({
  noteMasters,
}: PPTInfoData): Dict<NotesMasterData> | undefined {
  return getMapOpData(noteMasters, JSONFromNotesMaster)
}

export function getNotes({ notes }: PPTInfoData): Dict<NotesData> | undefined {
  return getMapOpData(notes, JSONFromSlideNotes)
}

function getComments({
  comments,
}: PPTInfoData): Dict<CommentsData> | undefined {
  return comments && comments.length > 0
    ? comments.reduce((result, slideComment) => {
        const rId = slideComment.getRefId()
        const fromResult = JSONFromSlideComment(slideComment)
        if (fromResult) result[rId] = fromResult
        return result
      }, {})
    : undefined
}

export function JSONFromPPTDoc(presentation: Presentation): PPTDocData {
  const infoData = genPPTInfoData(presentation)
  const presentationData = Op.Insert(
    OpData.Delta(JSONFromPresentation(presentation, infoData, true))
  )
  const layouts = Op.Insert(OpData.Map(getLayouts(infoData)))
  const slideMasters = Op.Insert(OpData.Map(getSlideMasters(infoData)))
  const slides = Op.Insert(OpData.Map(getSlides(infoData)))
  const themesData = getThemes(infoData)
  const themes = Op.Insert(
    themesData ? OpData.Map(themesData) : EmptyOpData.Map
  )

  const noteMastersData = getNotesMasters(infoData)
  const noteMasters = Op.Insert(
    noteMastersData ? OpData.Map(noteMastersData) : EmptyOpData.Map
  )

  const notesData = getNotes(infoData)
  const notes = Op.Insert(notesData ? OpData.Map(notesData) : EmptyOpData.Map)
  const commentsData = getComments(infoData)
  const comments = Op.Insert(
    commentsData ? OpData.Map(commentsData) : EmptyOpData.Map
  )
  return [
    withType(presentationData, 'presentation'),
    withType(layouts, 'layouts'),
    withType(slideMasters, 'slideMasters'),
    withType(slides, 'slides'),
    withType(themes, 'themes'),
    withType(noteMasters, 'notesMasters'),
    withType(notes, 'notes'),
    withType(comments, 'comments'),
  ]
}
