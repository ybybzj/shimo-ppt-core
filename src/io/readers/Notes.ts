import { Notes } from '../../core/Slide/Notes'
import { NotesAttrs, NotesData } from '../dataType/notes'
import { assignVal } from '../utils'
import { Op, OpData, withType } from '../operation'
import { JSONFromCSld } from './csld'
import { assignClrMapOverride } from '../../core/color/clrMap'

export function getNotesAttrs(notes: Notes): NotesAttrs {
  const data: NotesAttrs = {}
  assignVal(data, 'showMasterPhAnim', notes.showMasterPhAnim)
  assignVal(data, 'showMasterSp', notes.showMasterSp)

  assignClrMapOverride(data, 'clrMap', notes.clrMap)

  assignVal(data, 'notesMasterRef', notes.master?.getRefId())

  return data
}

export function JSONFromSlideNotes(notes: Notes): NotesData {
  const attrs = Op.Insert(OpData.JSON(getNotesAttrs(notes)))

  const csld = Op.Insert(OpData.Delta(JSONFromCSld(notes.cSld)))

  return [withType(attrs, 'attrs'), withType(csld, 'csld')]
}
