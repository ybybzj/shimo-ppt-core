import { NotesMaster } from '../../core/Slide/NotesMaster'
import { NotesMasterAttrs, NotesMasterData } from '../dataType/notesMaster'
import { assignVal, assignJSON } from '../utils'
import { Op, OpData, withType } from '../operation'
import { JSONFromCSld } from './csld'
import { HF } from '../../core/SlideElement/attrs/hf'
import { Nullable } from '../../../liber/pervasive'

export function getNotesMasterAttrs(
  notesMaster: NotesMaster
): NotesMasterAttrs {
  const data: NotesMasterAttrs = {}
  assignJSON(data, 'clrMap', notesMaster.clrMap)
  assignJSON(data, 'hf', notesMaster.hf as Nullable<HF>)
  assignJSON(data, 'notesStyles', notesMaster.txStyles)
  assignVal(data, 'themeRef', notesMaster.theme?.getRefId())
  return data
}

export function JSONFromNotesMaster(notesMaster: NotesMaster): NotesMasterData {
  const attrs = Op.Insert(OpData.JSON(getNotesMasterAttrs(notesMaster)))

  const csld = Op.Insert(OpData.Delta(JSONFromCSld(notesMaster.cSld)))

  return [withType(attrs, 'attrs'), withType(csld, 'csld')]
}
