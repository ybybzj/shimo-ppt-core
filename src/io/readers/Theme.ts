import { Theme } from '../../core/SlideElement/attrs/theme'
import {
  ThemeData,
  CThemeData,
  ThemeObjectDefaultsData,
} from '../dataType/theme'
import { Op, OpData } from '../operation'
import {
  assignJSON,
  isEmptyPropValue,
  assignVal,
  assignJSONArray,
} from '../utils'

export function JSONFromTheme(theme: Theme): ThemeData {
  const themeData: CThemeData = {}
  assignVal(themeData, 'name', theme.name)
  assignJSON(themeData, 'themeElements', theme.themeElements)

  const objectDefaults: ThemeObjectDefaultsData = {}
  assignJSON(objectDefaults, 'spDef', theme.spDef)
  assignJSON(objectDefaults, 'lnDef', theme.lnDef)
  assignJSON(objectDefaults, 'txDef', theme.txDef)

  if (!isEmptyPropValue(objectDefaults)) {
    assignVal(themeData, 'objectDefaults', objectDefaults)
  }

  assignJSONArray(themeData, 'extraClrSchemeLst', theme.extraClrSchemeLst)

  return [Op.Insert(OpData.JSON(themeData))]
}
