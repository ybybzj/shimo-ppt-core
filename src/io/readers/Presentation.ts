import { Presentation } from '../../core/Slide/Presentation'
import { Nullable, Dict } from '../../../liber/pervasive'
import { assignVal, isEmptyPropValue, toJSON } from '../utils'
import { PPTInfoData } from '../infoData'
import { Op, OpData, withType } from '../operation'
import {
  CShowPrData,
  PresData,
  GlobalTableStylesData,
  SlideRefData,
  SlideMasterRefData,
  NotesMasterRefData,
  PresentationData,
  CAppData,
  CCoreData,
} from '../dataType/presentation'
import { gGlobalTableStyles } from '../../core/Slide/GlobalTableStyles'

export function getCApp(presentation: Presentation): Nullable<CAppData> {
  const app = presentation.app
  if (app == null) {
    return undefined
  }
  const data: CAppData = {}
  assignVal(data, 'Template', app.Template)
  assignVal(data, 'TotalTime', app.TotalTime)
  assignVal(data, 'Words', app.Words)
  assignVal(data, 'Application', app.Application)
  assignVal(data, 'PresentationFormat', app.PresentationFormat)
  assignVal(data, 'Paragraphs', app.Paragraphs)
  assignVal(data, 'Slides', app.Slides)
  assignVal(data, 'Notes', app.Notes)
  assignVal(data, 'HiddenSlides', app.HiddenSlides)
  assignVal(data, 'MMClips', app.MMClips)
  assignVal(data, 'ScaleCrop', app.ScaleCrop)
  assignVal(data, 'Company', app.Company)
  assignVal(data, 'LinksUpToDate', app.LinksUpToDate)
  assignVal(data, 'SharedDoc', app.SharedDoc)
  assignVal(data, 'HyperlinksChanged', app.HyperlinksChanged)
  assignVal(data, 'AppVersion', app.AppVersion)
  return data
}

export function getCCore(presentation: Presentation): Nullable<CCoreData> {
  const core = presentation.core
  if (core == null) {
    return undefined
  }
  const data: CCoreData = {}
  assignVal(data, 'title', core.title)
  assignVal(data, 'creator', core.creator)
  assignVal(data, 'lastModifiedBy', core.lastModifiedBy)
  assignVal(data, 'revision', core.revision)
  assignVal(data, 'created', core.created)
  assignVal(data, 'modified', core.modified)
  return data
}

export function getShowPr(presentation: Presentation): CShowPrData {
  const showPr = presentation.showPr
  return showPr != null ? toJSON(showPr) : { present: false }
}

export function getPres(presentation: Presentation): PresData {
  const pres = presentation.pres
  return pres == null ? {} : toJSON(pres, presentation)
}

export function getGlobalTableStyles(
  presentation: Presentation,
  ignoreChange: boolean = false
): GlobalTableStylesData {
  const tableStyleIdMap: Dict<string> = {}
  presentation.fillTableStyleIdMap(tableStyleIdMap)
  const data: GlobalTableStylesData = {}

  const oldTableStyleIdMap =
    ignoreChange === false ? gGlobalTableStyles.tableStylesInUseMap : {}

  if (isTableStylesInUseEqual(tableStyleIdMap, oldTableStyleIdMap)) {
    return data
  }

  const oldDefGUID =
    oldTableStyleIdMap[gGlobalTableStyles.defaultIdOfTableStyle!]
  if (
    oldDefGUID !== tableStyleIdMap[gGlobalTableStyles.defaultIdOfTableStyle!]
  ) {
    data['defaultTableStyleId'] = tableStyleIdMap[
      gGlobalTableStyles.defaultIdOfTableStyle!
    ]
      ? tableStyleIdMap[gGlobalTableStyles.defaultIdOfTableStyle!]
      : null
  }

  if (ignoreChange === false && isEmptyPropValue(tableStyleIdMap)) {
    data['styles'] = null
  } else {
    for (const key in tableStyleIdMap) {
      if (tableStyleIdMap.hasOwnProperty(key)) {
        const style = gGlobalTableStyles.getTableStyle(key)
        data['styles'] = data['styles'] ?? {}
        if (style) {
          data['styles'][style.guid] = toJSON(style)
        }
      }
    }
  }

  // update TableStyle in use
  gGlobalTableStyles.updateTableStylesInUseMap(tableStyleIdMap)
  return data
}

function isTableStylesInUseEqual(
  map1: Dict<string>,
  map2: Dict<string>
): boolean {
  const str1 = Object.keys(map1)
    .map((k) => map1[k])
    .sort()
    .join('|')
  const str2 = Object.keys(map2)
    .map((k) => map2[k])
    .sort()
    .join('|')
  return str1 === str2
}

function getSlidesRef({ slides }: PPTInfoData): SlideRefData {
  if (slides.length <= 0) {
    return {}
  }

  return slides.reduce((result, slide, i) => {
    const sldRId = slide.getRefId()
    result[sldRId] = { order: i }
    return result
  }, {})
}

export function getMasterSlidesRefs({
  slideMasters,
}: PPTInfoData): SlideMasterRefData {
  if (slideMasters.length <= 0) {
    return { slideMasterRefs: null }
  }

  return slideMasters.reduce(
    (result, slideMaster, i) => {
      const rId = slideMaster.getRefId()
      result[rId] = { order: i }
      return result
    },
    { slideMasterRefs: null }
  )
}

function getNotesMasterRefs({ noteMasters }: PPTInfoData): NotesMasterRefData {
  return {
    noteMasterRefs: noteMasters.map((noteMaster) => noteMaster.getRefId()),
  }
}

export function JSONFromPresentation(
  presentation: Presentation,
  infoData: PPTInfoData,
  ignoreChange: boolean = false
): PresentationData {
  const Pres = Op.Insert(OpData.JSON({ ['pres']: getPres(presentation) }))
  const PresentationPr = Op.Insert(
    OpData.JSON({ ['showPr']: getShowPr(presentation) })
  )
  const TableStyle = Op.Insert(
    OpData.JSON(getGlobalTableStyles(presentation, ignoreChange))
  )
  const Slides = Op.Insert(OpData.JSON(getSlidesRef(infoData)))
  const SlideMaster = Op.Insert(OpData.JSON(getMasterSlidesRefs(infoData)))
  const NotesMaster = Op.Insert(OpData.JSON(getNotesMasterRefs(infoData)))

  return [
    withType(Pres, 'Pres'),
    withType(PresentationPr, 'PresentationPr'),
    withType(TableStyle, 'TableStyle'),
    withType(Slides, 'Slides'),
    withType(SlideMaster, 'SlideMaster'),
    withType(Op.Insert(1), 'Notes'),
    withType(NotesMaster, 'NotesMaster'),
  ]
}
