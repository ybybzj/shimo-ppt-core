import { Dict, Nullable } from '../../../liber/pervasive'

export function getMapOpData<M extends { getRefId(): string }, N>(
  arr: Nullable<Nullable<M>[]>,
  getter: (m: M) => N
): Dict<N> | undefined {
  return arr && arr.length > 0
    ? arr.reduce((res, item) => {
        if (item != null) {
          const rId = item.getRefId()
          res[rId] = getter(item)
        }
        return res
      }, {} as Dict<N>)
    : undefined
}
