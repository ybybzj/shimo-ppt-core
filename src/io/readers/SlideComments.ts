import { SlideComments } from '../../core/Slide/Comments/SlideComments'
import { CommentsData } from '../dataType/comment'
import { Op, OpData } from '../operation'

export function JSONFromSlideComment(
  slideComments: SlideComments
): CommentsData | null {
  const slideCommentsDict = slideComments.toJSON()
  return slideCommentsDict ? [Op.Insert(OpData.JSON(slideCommentsDict))] : null
}
