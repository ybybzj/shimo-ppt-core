/* eslint-disable no-redeclare */
import { Nullable, Dict, KeysWithValueType } from '../../liber/pervasive'
import { parseJSON } from '../common/utils'

export const NULL_VALUE = '#nil#'
export type NULL_VALUE = typeof NULL_VALUE
export type NullableJSONValue<T> = undefined | NULL_VALUE | T

export function encodeNullableValue<T>(v: Nullable<T>): NullableJSONValue<T> {
  if (v === undefined) {
    return undefined
  } else if (v === null) {
    return NULL_VALUE
  } else {
    return v
  }
}

export function decodeNullableValue<T>(v: NullableJSONValue<T>): Nullable<T> {
  if (v === undefined) {
    return undefined
  } else if (v === NULL_VALUE) {
    return null
  } else {
    return v
  }
}

// helpers for JSON data

// type JSONData<T> = T & { extra?: any } // extra 用于存储透传数据，（用于无损导入导出）

/** toJSON 的包装方法, 正常情况下所有 toJSON 都应该通过此方法调用 */
export function toJSON<D, Args extends any[]>(
  obj: { toJSON: (...args: Args) => D },
  ...args: Args
): D {
  const data = obj.toJSON(...args)
  if (data && obj['extra'] != null) {
    ;(data as any)['extra'] = obj['extra']
  }
  return data as D
}

/** fromJSON 的包装方法, 正常情况下所有 fromJSON 都应该通过此方法调用 */
export function fromJSON<D extends Dict, Args extends any[], R>(
  o: { fromJSON: (data: D, ...args: Args) => R },
  data: D,
  ...args: Args
) {
  const res = o.fromJSON(data, ...args)
  if (data != null && data['extra'] != null) {
    o['extra'] = data['extra']
  }
  return res
}
export function assignVal<
  V,
  T extends Dict,
  K extends KeysWithValueType<T, undefined | V>,
>(obj: T, k: K, v: Nullable<V>) {
  if (v != null) {
    obj[k] = v as T[K]
  }
}

export function assignNullableVal<T>(
  obj: T,
  k: keyof T,
  v: NullableJSONValue<any>
) {
  const _v = decodeNullableValue(v)
  if (_v !== undefined) {
    obj[k] = _v
  }
}

export function assignJSON<
  D,
  T extends Dict,
  K extends KeysWithValueType<T, undefined | D>,
>(
  data: T,
  k: K,
  o: Nullable<{ toJSON: (...args: any[]) => D }>,
  ...args: any[]
) {
  if (o != null) {
    const json = toJSON(o, ...args)

    if (!isEmptyPropValue(json)) {
      data[k] = json as T[K]
    }
  }
}

export function assignJSONArray<
  D,
  T extends Dict,
  K extends KeysWithValueType<T, undefined | D[]>,
>(data: T, k: K, arr: Array<{ toJSON: () => D }>) {
  if (arr.length > 0) {
    data[k] = arr.map((o) => {
      const json = toJSON(o)

      return isEmptyPropValue(json) ? NULL_VALUE : json
    }) as unknown as T[K]
  }
}

export function readFromJSON<
  D,
  I extends { fromJSON: (data: D, ...args: any[]) => void },
>(
  data: Nullable<D>,
  Class: { new (...args: any[]): I },
  ctorArgs?: any[],
  fromJSONArgs?: any[] // fromJSON 可能会产生副作用，故支持传入额外参数控制其行为
): I | undefined {
  let obj: I | undefined
  if (data != null) {
    obj = ctorArgs !== undefined ? new Class(...ctorArgs) : new Class()
    if (fromJSONArgs != null) {
      fromJSON(obj, data, ...fromJSONArgs)
    } else {
      fromJSON(obj, data)
    }
  }
  return obj
}

export function readFromJSONArray<D, I extends { fromJSON: (data: D) => void }>(
  data: Nullable<D[]>,
  Class: { new (): I }
): Array<I | null> | undefined {
  if (data != null) {
    return data.map((o) => {
      const v = decodeNullableValue(o)
      return v === null ? null : (readFromJSON(v, Class) as I)
    })
  } else {
    return undefined
  }
}

export function isEmptyPropValue(o: any): boolean {
  if (o === undefined) {
    return true
  }

  if (Object.prototype.toString.call(o) !== '[object Object]') {
    return false
  }

  const keys = Object.keys(o)
  if (keys.length <= 0) {
    return true
  }

  return keys.every((k) => isEmptyPropValue(o[k]))
}

// eslint-disable-next-line @typescript-eslint/no-unused-vars
function replacer(_k: any, val: any) {
  if (isEmptyPropValue(val)) {
    return undefined
  }

  // if (Object.prototype.toString.call(val) === '[object Object]') {
  //   const keys = Object.keys(val)
  //   const result =
  //     keys.length > 0
  //       ? keys.sort().reduce((result, k) => {
  //           const v = val[k]
  //           if (v !== undefined) {
  //             result[k] = v
  //           }
  //           return result
  //         }, {} as Dict)
  //       : undefined
  //   return isEmptyPropValue(result) ? undefined : result
  // }
  return val
}

function StringifyObjectVal(val: any[] | Dict): string {
  return JSON.stringify(val)
}

export function stringifyValuesOfJSON<T extends Dict>(
  o: T
): { [k in keyof T]: number | boolean | NULL_VALUE | string } {
  return Object.keys(o).reduce((result, k) => {
    const val = o[k]
    if (
      Array.isArray(val) ||
      Object.prototype.toString.call(val) === '[object Object]'
    ) {
      const stringifiedVal = StringifyObjectVal(val)
      if (stringifiedVal !== undefined) {
        result[k] = stringifiedVal
      }
    } else {
      result[k] = val
    }
    return result
  }, {}) as { [k in keyof T]: number | boolean | NULL_VALUE | string }
}

export type StringifiedJSON = Dict<number | boolean | NULL_VALUE | string>

const fieldIdReg = /^{[\dA-Za-z-]+}$/

export function parseStringifiedJSON(obj: StringifiedJSON): Dict {
  Object.keys(obj).forEach((key) => {
    const val = obj[key]
    if (key.length > 1 && key[0] === '.') {
      // key is a keypath

      keyPathToObject(obj, key, val)
    } else {
      if (
        typeof val === 'string' &&
        (val.charAt(0) === '{' || val.charAt(0) === '[')
      ) {
        if (fieldIdReg.test(val)) {
          obj[key] = val
        } else {
          obj[key] = parseJSON(val)
        }
      }
    }
  })

  return obj
}

function keyPathToObject(obj: object, keyPath: string, value: any) {
  const path = keyPath.split('.')
  const length = path.length
  for (let i = 1; i < length - 1; i++) {
    const key = path[i]
    const currentValue = obj[key]
    obj =
      typeof currentValue === 'object' && currentValue !== null
        ? currentValue
        : (obj[key] = {})
  }
  const lastPath = path[length - 1]
  if (
    value === null &&
    // 避免跳过不存在属性 (obj[key] === undefined) 需要赋值为 null 的情况
    obj[lastPath] !== undefined &&
    obj[lastPath] !== null
  ) {
    return
  }
  obj[lastPath] = value
}

export function getUpdateAttrs(
  orgAttrs: Dict<any>,
  newAttrs: Dict<any>
): Dict<any> {
  const result: Dict<any> = {}
  Object.keys(newAttrs).forEach((k) => (result[k] = newAttrs[k]))
  Object.keys(orgAttrs).forEach((k) => {
    if (newAttrs[k] === undefined) {
      result[k] = null
    }
  })
  return result
}

export function mergeDict(a: Dict, b: Dict, keepNull?: boolean): Dict {
  const result: Dict<any> = {}
  Object.keys(b).forEach((k) => {
    const v = b[k]
    if (v !== undefined && (keepNull || v !== null)) {
      result[k] = v
    }
  })

  Object.keys(a).forEach((k) => {
    let v
    if (
      b[k] === undefined &&
      (v = a[k]) !== undefined &&
      (keepNull || v !== null)
    ) {
      result[k] = v
    }
  })

  return result
}

export function scaleValue(
  val: Nullable<number>,
  scale: number
): Nullable<number> {
  if (val === 0) return 0
  return val != null ? (val * scale) >> 0 : val
}

// https://stackoverflow.com/questions/24237008/error-while-exporting-to-excel-data-hexadecimal-value-0x07-is-an-invalid
const regex = /[\x00-\x08\x0B\x0C\x0E-\x1F]/g
export function stripInvalidXMLCharacters(text: string): string {
  return text.replace(regex, '')
}

type Indexable = Dict | any[]

type KeysOfObj<T extends Indexable> = T extends any[]
  ? number
  : string & keyof T

type KeyOf<T extends undefined | Indexable> = KeysOfObj<Exclude<T, undefined>>
type ValueOf<T extends undefined | Indexable, K extends KeyOf<T>> = Exclude<
  T,
  undefined
>[K]
type DictValueOf<T extends undefined | Indexable, K extends KeyOf<T>> = Extract<
  ValueOf<T, K>,
  Dict
>

// export function getValByKeyPath<
//   T extends Indexable,
//   K1 extends KeysOfObj<T>,
//   K2 extends KeyOf<ValueOf<T,  K1>>,
//   K3 extends KeyOf<ValueOf<ValueOf<T,  K1>,  K2>>,
//   K4 extends KeyOf<ValueOf<ValueOf<ValueOf<T,  K1>,  K2>, K3>>,
//   K5 extends KeyOf<ValueOf<ValueOf<ValueOf<ValueOf<T,  K1>,  K2>, K3>, K4>>
// >(o: T, k: `${K1}.${K2}.${K3}.${K4}.${K5}`): ValueOf<ValueOf<ValueOf<ValueOf<ValueOf<T,  K1>,  K2>, K3>, K4>, K5>
/**
 * @usage 根据传入的对象 T, 按照字符串 'T[attr]' 去取其属性, 支持类型校验
 * @note 属性至多嵌套 4 层
 * @example
 * - 如 interface A { a: Array<{ ss: string[] }>,
 * - a: [{ss: ['b']}], 可以取 getValByKeyPath(a, ‘0.ss’) 得到 ['b']
 * */
export function getValByKeyPath<
  T extends Indexable,
  K1 extends KeysOfObj<T>,
  K2 extends KeyOf<DictValueOf<T, K1>>,
  K3 extends KeyOf<DictValueOf<DictValueOf<T, K1>, K2>>,
  K4 extends KeyOf<DictValueOf<DictValueOf<DictValueOf<T, K1>, K2>, K3>>,
>(
  o: T,
  k: `${K1}.${K2}.${K3}.${K4}`
): ValueOf<DictValueOf<DictValueOf<DictValueOf<T, K1>, K2>, K3>, K4>
export function getValByKeyPath<
  T extends Indexable,
  K1 extends KeyOf<T>,
  K2 extends KeyOf<DictValueOf<T, K1>>,
  K3 extends KeyOf<DictValueOf<DictValueOf<T, K1>, K2>>,
>(
  o: T,
  k: `${K1}.${K2}.${K3}`
): ValueOf<DictValueOf<DictValueOf<T, K1>, K2>, K3>
export function getValByKeyPath<
  T extends Indexable,
  K1 extends KeyOf<T>,
  K2 extends KeyOf<DictValueOf<T, K1>>,
>(o: T, k: `${K1}.${K2}`): ValueOf<DictValueOf<T, K1>, K2>
export function getValByKeyPath<T extends Indexable, K1 extends KeyOf<T>>(
  o: T,
  k: `${K1}`
): undefined | ValueOf<T, K1>
export function getValByKeyPath<T extends Indexable>(o: T, k: string) {
  const keys = k.split('.').filter((p) => p !== '')
  let r: any = o
  while (keys.length > 0 && Object(r) === r) {
    const k = keys.shift() as string
    r = r[k]
  }
  return keys.length > 0 ? undefined : r
}
