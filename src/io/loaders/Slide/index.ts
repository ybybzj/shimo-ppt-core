import { Dict, Nullable } from '../../../../liber/pervasive'
import { Presentation } from '../../../core/Slide/Presentation'
import { Slide } from '../../../core/Slide/Slide'
import { SlideLayout } from '../../../core/Slide/SlideLayout'
import { SlideTransition } from '../../../core/Slide/SlideTransition'
import { animationFixer } from '../../animationsFixer'
import { FromJSONParams } from '../../defs/pptdoc'
import { SlideDataLayout, SlideDataLayoutTypeMap } from '../../defs/Slide'
import { fromJSON, readFromJSON } from '../../utils'
import { loadCSld } from '../csld'
import { fromClrMapOverride } from '../../../core/color/clrMap'
import { updateCalcStatusForSlide } from '../../../core/calculation/updateCalcStatus/slide'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { StringSlice } from '../../../re/export/modoc'
import {
  dataSliceFromDeltaByOpType,
  deltaSliceLoader,
  loadDeltaSlice,
} from '../utils'
import { Operation } from '../../operation'

export function isValidSlideDeltaSlice(ds: StringSlice) {
  const data = dataSliceFromDeltaByOpType(
    ds,
    'csld',
    SlideDataLayout,
    SlideDataLayoutTypeMap
  )
  return data != null
}

interface Context {
  slide: Slide
  params: FromJSONParams
  slideLayouts?: Nullable<Dict<SlideLayout>>
  cb?: (arg: { notesRef?: string; commentsRef?: string }) => void
}

export function applySlide(
  data: StringSlice,
  slide: Slide,
  params: FromJSONParams,
  rId?: Nullable<string>,
  slideLayouts?: Nullable<Dict<SlideLayout>>,
  cb?: (arg: { notesRef?: string; commentsRef?: string }) => void,
  needCollectOps = false
) {
  if (rId && params.ignoreRId !== true) {
    slide.setRefId(rId)
  }

  const ctx: Context = { slide, params, slideLayouts, cb }
  const retOps = loadDeltaSlice(
    data,
    SlideDataLayout,
    SlideDataLayoutTypeMap,
    ctx,
    slideLoader,
    needCollectOps
  )

  return retOps
}
const slideLoader: deltaSliceLoader<
  typeof SlideDataLayout,
  typeof SlideDataLayoutTypeMap,
  Context
> = (opType, op, { slide, params, slideLayouts, cb }, needCollectOps) => {
  switch (opType) {
    case 'attrs': {
      const show = op.data['show']

      const showMasterPhAnim = op.data['showMasterPhAnim']

      const showMasterSp = op.data['showMasterSp']

      const clrMapData = op.data['clrMap']

      const layoutRef = op.data['layoutRef']

      if (show != null) {
        slide.setShow(show)
      }

      if (showMasterPhAnim != null) {
        slide.setShowPhAnim(showMasterPhAnim)
      }

      if (showMasterSp != null) {
        slide.setShowMasterSp(showMasterSp)
      }

      const clrMap = fromClrMapOverride(clrMapData)
      if (clrMap) {
        slide.setClrMapOverride(clrMap)
      }

      if ('transition' in op.data) {
        const timingData = op.data['transition']
        if (timingData === null) {
          // reset timing
          slide.timing?.reset()
        } else if (timingData != null) {
          const timing = readFromJSON(timingData, SlideTransition)
          if (timing != null) {
            slide.timing?.reset()
            slide.applyTiming(timing)
          }
        }
      }
      if ('timing' in op.data) {
        const animationData = op.data['timing']
        if (!animationData) {
          slide.animation.reset()
        } else {
          const needFixAnimation = fromJSON(slide.animation, animationData)
          if (needFixAnimation === true) {
            animationFixer.add(slide.animation)
          }
        }
      }

      // establish ref links
      if (slideLayouts && layoutRef != null) {
        const layout = slideLayouts[layoutRef] ?? Object.values(slideLayouts)[0]
        if (layout) {
          slide.setLayout(layout)
          slide.master = layout.master!
        }
      }
      if ('notesRef' in op.data) {
        const notesRef = op.data['notesRef']
        if (notesRef && cb) {
          cb({ notesRef })
        } else if (notesRef === null) {
          //remove
          slide.removeNotes()
        }
      }
      if ('commentsRef' in op.data) {
        const commentsRef = op.data['commentsRef']
        if (commentsRef && cb) {
          cb({ commentsRef })
          if (slide.slideComments) slide.slideComments.setRefId(commentsRef)
        } else if (commentsRef === null) {
          slide.slideComments = null
        }
      }
      return undefined
    }

    case 'csld': {
      const retOp = loadCSld(
        op,
        slide.cSld,
        slide,
        params,
        undefined,
        needCollectOps
      )

      updateCalcStatusForSlide(slide, EditActionFlag.edit_Slide_SetBg)
      return retOp
    }
  }
}

export function loadSlide(
  ds: StringSlice,
  presentation: Presentation,
  params: FromJSONParams,
  rId?: Nullable<string>,
  slideLayouts?: Nullable<Dict<SlideLayout>>,
  cb?: (arg: { notesRef?: string; commentsRef?: string }) => void,
  needCollectOps = false
): { slide: Slide; ops: Nullable<Operation[]> } | undefined {
  const isDataValid = isValidSlideDeltaSlice(ds)

  // 加载数据时忽略非法的 retain 数据
  if (isDataValid) {
    const slide = new Slide(presentation, null, null)
    const ops = applySlide(
      ds,
      slide,
      params,
      rId,
      slideLayouts,
      cb,
      needCollectOps
    )
    slide.setSlideSize(presentation.width, presentation.height)
    return { slide, ops }
  }
}
