import { Dict, Nullable } from '../../../liber/pervasive'
import { SlideComments } from '../../core/Slide/Comments/SlideComments'
import { PackedOpJson, PackedOpMap } from '../../re/export/modoc'
import { CmContentData } from '../dataType/comment'
import { FromJSONParams } from '../defs/pptdoc'
import { readFromJSON } from '../utils'
import { firstItemOfDeltaSlice, getIterFromModocItemMap } from './utils'

export function applyComments(
  op: PackedOpMap,
  _params: FromJSONParams,
  cb: (rId: string, slideComment: Nullable<SlideComments>) => void
) {
  const iter = getIterFromModocItemMap(op)
  while (iter.hasNext()) {
    const { id: rId, delta } = iter.next()
    const streamItem = firstItemOfDeltaSlice(delta) as PackedOpJson
    let commentsDict: Dict<CmContentData> = {}
    if (streamItem.data) {
      commentsDict = streamItem.data as Dict<CmContentData>
    }
    const slideComment = readFromJSON(commentsDict, SlideComments)
    cb(rId, slideComment)
  }
}
