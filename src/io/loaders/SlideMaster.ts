import { SlideMaster } from '../../core/Slide/SlideMaster'
import { SlideLayout } from '../../core/Slide/SlideLayout'
import { Dict, Nullable } from '../../../liber/pervasive'
import { FromJSONParams } from '../defs/pptdoc'
import {
  SlideMasterDataLayout,
  SlideMasterDataLayoutTypeMap,
} from '../defs/SlideMaster'
import { readFromJSON } from '../utils'
import { TextStyles } from '../../core/textAttributes/TextStyles'
import { loadCSld } from './csld'
import { SlideTransition } from '../../core/Slide/SlideTransition'
import { CSld } from '../../core/Slide/CSld'
import { ClrMap } from '../../core/color/clrMap'
import { HF } from '../../core/SlideElement/attrs/hf'
import { StringSlice } from '../../re/export/modoc'
import { deltaSliceLoader, loadDeltaSlice } from './utils'
interface Context {
  slideMaster: SlideMaster
  params: FromJSONParams
  slideLayouts: Nullable<Dict<SlideLayout>>
  cb?: (arg: { themeRef: string }) => void
}

export function applySlideMaster(
  ds: StringSlice,
  rId: Nullable<string>,
  slideMaster: SlideMaster,
  slideLayouts: Nullable<Dict<SlideLayout>>,
  params: FromJSONParams,
  cb?: (arg: { themeRef: string }) => void,
  needCollectOps = false
) {
  if (rId && params.ignoreRId !== true) {
    slideMaster.setRefId(rId)
  }
  const ctx: Context = { slideMaster, params, slideLayouts, cb }
  const retOps = loadDeltaSlice(
    ds,
    SlideMasterDataLayout,
    SlideMasterDataLayoutTypeMap,
    ctx,
    slideMasterLoader,
    needCollectOps
  )
  return retOps
}
const slideMasterLoader: deltaSliceLoader<
  typeof SlideMasterDataLayout,
  typeof SlideMasterDataLayoutTypeMap,
  Context
> = (opType, op, { slideMaster, params, slideLayouts, cb }, needCollectOps) => {
  switch (opType) {
    case 'attrs': {
      const themeId = op.data['themeId']
      const preserve = op.data['preserve']
      const clrMapData = op.data['clrMap']
      const hfData = op.data['hf']
      const txStylesData = op.data['txStyles']
      const themeRef = op.data['themeRef']

      if (themeId != null) {
        slideMaster.themeId = themeId
      }

      if (preserve != null) {
        slideMaster.preserve = preserve
      }

      const clrMap = readFromJSON(clrMapData, ClrMap)
      if (clrMap) {
        slideMaster.setClrMapOverride(clrMap)
      }

      const hf = readFromJSON(hfData, HF)
      if (hf) {
        slideMaster.hf = hf
      }

      if ('transition' in op.data) {
        const transitionData = op.data['transition']

        if (transitionData === null) {
          slideMaster.transition = undefined
        } else if (transitionData != null) {
          const transition = readFromJSON(transitionData, SlideTransition)

          if (transition != null) {
            slideMaster.transition = new SlideTransition()
            slideMaster.transition.reset()
            slideMaster.transition.applyProps(transition)
          }
        }
      }

      const txStyles = readFromJSON(txStylesData, TextStyles)
      if (txStyles) {
        slideMaster.setTextStyles(txStyles)
      }

      if (themeRef != null && cb) {
        cb({ themeRef })
      }
      return undefined
    }
    case 'layoutRefs': {
      if (slideLayouts) {
        const layouts: string[] = op.data['layoutRefs'] ?? []
        layouts.forEach((layoutId) => {
          const layout = slideLayouts[layoutId]
          const isExistingLayout = slideMaster.layouts.find(
            (existingLayout) => existingLayout === layout
          )
          if (layout && !isExistingLayout) {
            slideMaster.addLayout(layout)
            layout.setMaster(slideMaster)
          }
        })
      }
      return undefined
    }
    case 'csld': {
      const cSld = slideMaster.cSld ?? new CSld(slideMaster)
      const retOp = loadCSld(
        op,
        cSld,
        slideMaster,
        params,
        undefined,
        needCollectOps
      )

      if (cSld.bg) {
        slideMaster.changeBackground(cSld.bg)
      }
      slideMaster.setCSldName(cSld.name)
      return retOp
    }
  }
}
