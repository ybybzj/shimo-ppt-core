import { NotesMaster } from '../../core/Slide/NotesMaster'
import {
  NotesMasterDataLayout,
  NotesMasterDataLayoutTypeMap,
} from '../defs/NotesMaster'
import { Dict, Nullable } from '../../../liber/pervasive'
import { FromJSONParams } from '../defs/pptdoc'
import { readFromJSON } from '../utils'
import { loadCSld } from './csld'
import { TextListStyle } from '../../core/textAttributes/TextListStyle'
import { SlideMaster } from '../../core/Slide/SlideMaster'
import { CSld } from '../../core/Slide/CSld'
import { ClrMap } from '../../core/color/clrMap'
import { HF } from '../../core/SlideElement/attrs/hf'
import { Theme } from '../../core/SlideElement/attrs/theme'
import {
  deltaSliceLoader,
  getIterFromModocItemMap,
  loadDeltaSlice,
} from './utils'
import {
  OperationFromPackedOp,
  PackedOpMap,
  StringSlice,
  deltaSliceToOperations,
} from '../../re/export/modoc'
interface Context {
  notesMaster: NotesMaster
  params: FromJSONParams
  cb?: (arg: { themeRef: string }) => void
}

function applyNotesMaster(
  ds: StringSlice,
  rId: Nullable<string>,
  notesMaster: NotesMaster,
  params: FromJSONParams,
  cb?: (arg: { themeRef: string }) => void,
  needCollectOps = false
) {
  if (rId && params.ignoreRId !== true) {
    notesMaster.setRefId(rId)
  }
  const ctx: Context = { notesMaster, params, cb }
  const retOps = loadDeltaSlice(
    ds,
    NotesMasterDataLayout,
    NotesMasterDataLayoutTypeMap,
    ctx,
    notesMasterLoader,
    needCollectOps
  )
  return retOps
}
const notesMasterLoader: deltaSliceLoader<
  typeof NotesMasterDataLayout,
  typeof NotesMasterDataLayoutTypeMap,
  Context
> = (opType, op, { notesMaster, params, cb }, needCollectOps) => {
  switch (opType) {
    case 'attrs': {
      const clrMapData = op.data['clrMap']
      const hfData = op.data['hf']
      const txStylesData = op.data['notesStyles']
      const themeRef = op.data['themeRef']

      if (notesMaster.clrMap) {
        notesMaster.clrMap.fromJSON(clrMapData)
      } else {
        const clrMap = readFromJSON(clrMapData, ClrMap)
        if (clrMap) {
          notesMaster.clrMap = clrMap
        }
      }

      const hf = readFromJSON(hfData, HF)
      if (hf) {
        notesMaster.hf = hf
      }

      const txStyles = readFromJSON(txStylesData, TextListStyle)
      if (txStyles) {
        notesMaster.setTxStyles(txStyles)
      }

      if (themeRef != null && cb) {
        cb({ themeRef })
      }
      return undefined
    }
    case 'csld': {
      const cSld = notesMaster.cSld ?? new CSld(notesMaster)
      const retOp = loadCSld(
        op,
        cSld,
        notesMaster,
        params,
        undefined,
        needCollectOps
      )

      if (cSld.bg) {
        notesMaster.changeBackground(cSld.bg)
      }
      notesMaster.setCSldName(cSld.name)
      return retOp
    }
  }
}

export function loadNotesMasters(
  op: PackedOpMap,
  defaultTheme: Nullable<Theme>,
  params: FromJSONParams,
  cb?: (rId: string, newNotesMaster: NotesMaster) => void,
  themeSlideMastersMap?: Dict<(SlideMaster | NotesMaster)[]>,
  needCollectOps = false
) {
  const retOp: Nullable<OperationFromPackedOp<PackedOpMap>> = needCollectOps
    ? { action: op.action, data: { slide: {} }, attributes: op.attributes }
    : undefined
  const retOpDataMap:
    | OperationFromPackedOp<PackedOpMap>['data']['slide']
    | undefined = needCollectOps ? retOp!.data.slide : undefined

  const iter = getIterFromModocItemMap(op)
  while (iter.hasNext()) {
    const { id: rId, delta } = iter.next()
    const newNotesMaster = new NotesMaster()
    const ops = applyNotesMaster(
      delta,
      rId,
      newNotesMaster,
      params,
      ({ themeRef }) => {
        if (themeSlideMastersMap) {
          themeSlideMastersMap[themeRef] = themeSlideMastersMap[themeRef] || []
          if (themeSlideMastersMap[themeRef].indexOf(newNotesMaster) < 0) {
            themeSlideMastersMap[themeRef].push(newNotesMaster)
          }
        }
      },
      needCollectOps
    )
    if (retOpDataMap != null) {
      retOpDataMap[rId] = ops ?? deltaSliceToOperations(delta)
    }
    if (defaultTheme) {
      newNotesMaster.theme = defaultTheme
    }
    if (cb) {
      cb(rId, newNotesMaster)
    }
  }
  return retOp
}
