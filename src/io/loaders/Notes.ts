import { Notes } from '../../core/Slide/Notes'
import { NotesDataLayout, NotesDataLayoutTypeMap } from '../defs/Notes'
import { Dict, Nullable } from '../../../liber/pervasive'
import { FromJSONParams } from '../defs/pptdoc'
import { loadCSld } from './csld'
import { fromClrMapOverride } from '../../core/color/clrMap'
import {
  deltaSliceLoader,
  getIterFromModocItemMap,
  loadDeltaSlice,
} from './utils'
import {
  OperationFromPackedOp,
  PackedOpMap,
  StringSlice,
  deltaSliceToOperations,
} from '../../re/export/modoc'
import { Operation } from '../operation'
interface Context {
  notes: Notes
  params: FromJSONParams
  cb?: (arg: { notesMasterRef: string }) => void
}
function loadSlideNotes(
  ds: StringSlice,
  notes: Notes,
  params: FromJSONParams,
  rId?: Nullable<string>,
  cb?: (arg: { notesMasterRef: string }) => void,
  needCollectOps = false
) {
  if (rId && params.ignoreRId !== true) {
    notes.setRefId(rId)
  }
  const ctx: Context = { notes, params, cb }
  const retOps = loadDeltaSlice(
    ds,
    NotesDataLayout,
    NotesDataLayoutTypeMap,
    ctx,
    notesLoader,
    needCollectOps
  )
  return retOps
}
const notesLoader: deltaSliceLoader<
  typeof NotesDataLayout,
  typeof NotesDataLayoutTypeMap,
  Context
> = (opType, op, { notes, params, cb }, needCollectOps) => {
  switch (opType) {
    case 'attrs': {
      const showMasterPhAnim = op.data['showMasterPhAnim']
      const showMasterSp = op.data['showMasterSp']
      const clrMapData = op.data['clrMap']
      const notesMasterRef = op.data['notesMasterRef']
      if (showMasterPhAnim != null) {
        notes.setShowMasterPhAnim(showMasterPhAnim)
      }
      if (showMasterSp != null) {
        notes.setShowMasterSp(showMasterSp)
      }
      const clrMap = fromClrMapOverride(clrMapData)
      if (clrMap) {
        notes.setClrMapOverride(clrMap)
      }
      // establish ref links
      if (notesMasterRef != null && typeof cb === 'function') {
        cb({ notesMasterRef })
      }
      return undefined
    }
    case 'csld': {
      return loadCSld(op, notes.cSld, notes, params, undefined, needCollectOps)
    }
  }
}

export function genSlideNote(
  ds: StringSlice,
  params: FromJSONParams,
  rId?: Nullable<string>,
  cb?: (arg: { notesMasterRef: string }) => void,
  needCollectOps = false
): { notes: Notes; ops: Nullable<Operation[]> } {
  const notes = new Notes()
  const ops = loadSlideNotes(ds, notes, params, rId, cb, needCollectOps)
  return { notes, ops }
}

export function loadNotes(
  op: PackedOpMap,
  params: FromJSONParams,
  cb?: (rId: string, newNotes: Notes) => void,
  notesToMasterMap?: Dict<string>,
  needCollectOps = false
) {
  const retOp: Nullable<OperationFromPackedOp<PackedOpMap>> = needCollectOps
    ? { action: op.action, data: { slide: {} }, attributes: op.attributes }
    : undefined
  const retOpDataMap:
    | OperationFromPackedOp<PackedOpMap>['data']['slide']
    | undefined = needCollectOps ? retOp!.data.slide : undefined
  const iter = getIterFromModocItemMap(op)
  while (iter.hasNext()) {
    const { id: rId, delta } = iter.next()
    const { notes: newNotes, ops } = genSlideNote(
      delta,
      params,
      rId,
      ({ notesMasterRef }) => {
        if (notesToMasterMap) {
          notesToMasterMap[rId] = notesMasterRef
        }
      },
      needCollectOps
    )
    if (retOpDataMap != null) {
      retOpDataMap[rId] = ops ?? deltaSliceToOperations(delta)
    }

    if (cb) {
      cb(rId, newNotes)
    }
  }
  return retOp
}
