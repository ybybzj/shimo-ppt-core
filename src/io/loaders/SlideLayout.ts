import { SlideLayout } from '../../core/Slide/SlideLayout'
import { FromJSONParams } from '../defs/pptdoc'
import {
  SlideLayoutDataLayout,
  SlideLayoutDataLayoutTypeMap,
} from '../defs/SlideLayout'
import { readFromJSON } from '../utils'
import { fromClrMapOverride } from '../../core/color/clrMap'
import { HF } from '../../core/SlideElement/attrs/hf'
import { CSld } from '../../core/Slide/CSld'
import { loadCSld } from './csld'
import { Nullable } from '../../../liber/pervasive'
import { fromSlideLayoutType } from '../dataType/slideLayout'
import { SlideTransition } from '../../core/Slide/SlideTransition'
import { StringSlice } from '../../re/export/modoc'
import { deltaSliceLoader, loadDeltaSlice } from './utils'
import { Presentation } from '../../core/Slide/Presentation'
import { Operation } from '../operation'
interface Context {
  layout: SlideLayout
  params: FromJSONParams
}
function applySlideLayout(
  ds: StringSlice,
  layout: SlideLayout,
  rId: Nullable<string>,
  params: FromJSONParams,
  needCollectOps = false
) {
  if (rId && params.ignoreRId !== true) layout.setRefId(rId)
  const ctx: Context = {
    layout,
    params,
  }

  const retOps = loadDeltaSlice(
    ds,
    SlideLayoutDataLayout,
    SlideLayoutDataLayoutTypeMap,
    ctx,
    slideLayoutLoader,
    needCollectOps
  )
  return retOps
}
const slideLayoutLoader: deltaSliceLoader<
  typeof SlideLayoutDataLayout,
  typeof SlideLayoutDataLayoutTypeMap,
  Context
> = (opType, op, { layout, params }, needCollectOps) => {
  switch (opType) {
    case 'attrs': {
      const matchingName = op.data['matchingName']
      const preserve = op.data['preserve']
      const showMasterPhAnim = op.data['showMasterPhAnim']
      const showMasterSp = op.data['showMasterSp']
      const userDrawn = op.data['userDrawn']

      const type =
        op.data['type'] != null
          ? fromSlideLayoutType(op.data['type'])
          : undefined
      const clrMap = op.data['clrMap']
      const HFData = op.data['hf']

      if (matchingName != null) {
        layout.setMatchingName(matchingName)
      }

      if (preserve != null) {
        layout.preserve = preserve
      }

      if (showMasterPhAnim != null) {
        layout.setShowPhAnim(showMasterPhAnim)
      }

      if (showMasterSp != null) {
        layout.setShowMasterSp(showMasterSp)
      }

      if (userDrawn != null) {
        layout.userDrawn = userDrawn
      }

      if (type != null) {
        layout.setType(type)
      }

      if (clrMap != null) {
        const clr_map = fromClrMapOverride(clrMap)

        if (clr_map) {
          layout.setClrMapOverride(clr_map)
        }
      }

      if (HFData != null) {
        const hf = readFromJSON(HFData, HF)
        if (hf) {
          layout.hf = hf
        }
      }

      if ('transition' in op.data) {
        const transitionData = op.data['transition']

        if (transitionData === null) {
          layout.transition = undefined
        } else if (transitionData != null) {
          const transition = readFromJSON(transitionData, SlideTransition)

          if (transition != null) {
            layout.transition = new SlideTransition()
            layout.transition.reset()
            layout.transition.applyProps(transition)
          }
        }
      }
      return undefined
    }
    case 'csld': {
      const cSld = layout.cSld ?? new CSld(layout)
      const retOp = loadCSld(
        op,
        cSld,
        layout,
        params,
        undefined,
        needCollectOps
      )

      if (cSld.bg) {
        layout.changeBackground(cSld.bg)
      }
      layout.setCSldName(cSld.name)
      return retOp
    }
  }
}

export function loadLayout(
  data: StringSlice,
  presentation: Presentation,
  params: FromJSONParams,
  rId?: Nullable<string>,
  needCollectOps = false
): { layout: SlideLayout; ops: Nullable<Operation[]> } {
  const layout = new SlideLayout(presentation)
  layout.setSlideSize(presentation.width, presentation.height)
  const ops = applySlideLayout(data, layout, rId, params, needCollectOps)
  return { layout, ops }
}
