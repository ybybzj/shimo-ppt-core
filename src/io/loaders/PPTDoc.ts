import { each } from '../../../liber/l/each'
import { Dict, Nullable } from '../../../liber/pervasive'
import { hookManager, Hooks } from '../../core/hooks'
import { SlideComments } from '../../core/Slide/Comments/SlideComments'
import { createNotes, Notes } from '../../core/Slide/Notes'
import { createNotesMaster, NotesMaster } from '../../core/Slide/NotesMaster'
import { Presentation } from '../../core/Slide/Presentation'
import { SlideMaster } from '../../core/Slide/SlideMaster'
import {
  createDefaultMasterSlide,
  createDefaultSlideLayout,
} from '../../core/Slide/slideUtils'
import { Theme } from '../../core/SlideElement/attrs/theme'
import {
  DEFAULT_THEME_NAME,
  createDefaultTheme,
} from '../../core/SlideElement/attrs/creators'
import { applysectionPrList } from '../../core/utilities/presentation/reorder'
import {
  FromJSONParams,
  HelperDataForFromJSON,
  PPTDocDataLayout,
  PPTDocDataLayoutTypeMap,
} from '../defs/pptdoc'
import { genPPTInfoData, getRefItemMap } from '../infoData'
import { applyComments } from './Comment'
import { loadNotesMasters } from './NotesMaster'
import { loadPresentationData, PresentationRefsInfo } from './Presentation'
import { applySlide, isValidSlideDeltaSlice } from './Slide'
import { applySlideMaster } from './SlideMaster'
import { ResourcesContainerType } from '../../editor/utils/resourcesLoadManager'
import {
  ModocItem,
  dataFromDelta,
  getIterFromModocItemMap,
  loadDeltaSlice,
} from './utils'
import {
  OperationFromPackedOp,
  PackedOpDelta,
  PackedOpMap,
  StringSlice,
  deltaSliceToOperations,
  opFromPackedOperation,
} from '../../re/export/modoc'
import { loadLayout } from './SlideLayout'
import { loadThemes } from './Theme'
import { loadNotes } from './Notes'
import { Slide } from '../../core/Slide/Slide'
import { SlideLayout } from '../../core/Slide/SlideLayout'
import { turnOffChanges, turnOnChanges } from '../../editor/proto'
import { startPerfTiming } from '../../common/utils'
import { logger } from '../../lib/debug/log'
import { requestIdleCallback } from '../../../re/shim/RequestIdleCallback.shim'
// import { calculateRenderingStateForSlides } from '../../core/calculation/calculate/presentation'
import { EditorSettings } from '../../core/common/EditorSettings'
import { Operation } from '../operation'
// import { EditorKit } from '../../editor/global'
// import { Evt_onRedrawDocument } from '../../editor/events/EventNames'

const getDefaultTheme = (() => {
  let defaultTheme: Theme
  return (presentation: Presentation) => {
    defaultTheme = defaultTheme ?? createDefaultTheme(presentation)
    defaultTheme.presentation = presentation
    return defaultTheme
  }
})()

export async function loadPPTDoc(
  deltaSlice: StringSlice,
  presentation: Presentation,
  helperData: HelperDataForFromJSON,
  needCollectOps = false
) {
  turnOffChanges()
  const currentInfo = genPPTInfoData(presentation)

  const params: FromJSONParams = {
    presentation,
    helperData,
  }

  const defaultTheme = getDefaultTheme(presentation)

  const data = dataFromDelta(
    deltaSlice,
    PPTDocDataLayout,
    PPTDocDataLayoutTypeMap
  )

  /* --- presentation ---*/
  const refsInfo: PresentationRefsInfo = {
    slides: currentInfo.slides.map((slide) => slide.getRefId()),
    slideMasters: currentInfo.slideMasters.map((slideMaster) =>
      slideMaster.getRefId()
    ),
    notes: [],
    noteMasters: [],
  }
  let op: ModocItem | undefined = data['presentation']
  let retOp_presentation: Nullable<OperationFromPackedOp<PackedOpDelta>>
  if (op) {
    retOp_presentation = loadPresentationData(
      op,
      presentation,
      refsInfo,
      needCollectOps
    )
    emitGroupCollectedResources('presentation')
  }
  applysectionPrList(presentation, refsInfo.prSectionList)

  /* --- layouts ---*/
  const currentLayouts = getRefItemMap(currentInfo.layouts)
  op = data['layouts']
  let retOp_layout: Nullable<OperationFromPackedOp<PackedOpMap>>
  if (op) {
    retOp_layout = needCollectOps
      ? { action: op.action, data: { slide: {} }, attributes: op.attributes }
      : undefined
    const retOpDataMap:
      | OperationFromPackedOp<PackedOpMap>['data']['slide']
      | undefined = needCollectOps ? retOp_layout!.data.slide : undefined

    const iter = getIterFromModocItemMap(op)
    while (iter.hasNext()) {
      const { id: rId, delta } = iter.next()
      const { layout: newLayout, ops } = loadLayout(
        delta,
        presentation,
        params,
        rId,
        needCollectOps
      )
      currentLayouts[rId] = newLayout
      if (retOpDataMap != null) {
        retOpDataMap[rId] = ops ?? deltaSliceToOperations(delta)
      }
      emitGroupCollectedResources('layouts', rId)
    }
  }

  /* --- slideMasters ---*/
  const themeSlideMastersMap: Dict<Array<SlideMaster | NotesMaster>> = {}
  op = data['slideMasters']
  let retOp_slideMaster: Nullable<OperationFromPackedOp<PackedOpMap>>
  if (op && refsInfo.slideMasters.length > 0) {
    retOp_slideMaster = needCollectOps
      ? { action: op.action, data: { slide: {} }, attributes: op.attributes }
      : undefined
    const retOpDataMap:
      | OperationFromPackedOp<PackedOpMap>['data']['slide']
      | undefined = needCollectOps ? retOp_slideMaster!.data.slide : undefined
    presentation.slideMasters.length = 0

    const iter = getIterFromModocItemMap(op)
    while (iter.hasNext()) {
      const { id: rId, delta } = iter.next()
      if (refsInfo.slideMasters.indexOf(rId) > -1) {
        const master = new SlideMaster(params.presentation)
        const ops = applySlideMaster(
          delta,
          rId,
          master,
          currentLayouts,
          params,
          ({ themeRef }) => {
            themeSlideMastersMap[themeRef] =
              themeSlideMastersMap[themeRef] || []
            if (themeSlideMastersMap[themeRef].indexOf(master) < 0) {
              themeSlideMastersMap[themeRef].push(master)
            }
          },
          needCollectOps
        )
        if (retOpDataMap != null) {
          retOpDataMap[rId] = ops ?? deltaSliceToOperations(delta)
        }
        master.setSlideSize(presentation.width, presentation.height)

        master.theme = defaultTheme
        presentation.slideMasters.push(master)

        // 保留第一次加载所有主题
        presentation.preservedSlideMasters.push(master)
        emitGroupCollectedResources('slideMasters', rId)
      } else {
        if (retOpDataMap != null) {
          retOpDataMap[rId] = deltaSliceToOperations(delta)
        }
      }
    }
  }

  if (presentation.slideMasters.length === 0) {
    presentation.slideMasters[0] = createDefaultMasterSlide(
      presentation,
      defaultTheme
    )
  }
  if (presentation.slideMasters[0].layouts.length === 0) {
    presentation.slideMasters[0].layouts[0] = createDefaultSlideLayout(
      presentation.slideMasters[0]
    )
  }

  /* --- themes ---*/
  const themes = presentation.themesMap
  op = data['themes']
  let retOp_themes: Nullable<OperationFromPackedOp<PackedOpMap>>
  if (op) {
    retOp_themes = loadThemes(
      op,
      params,
      (rId, theme) => {
        themes[rId] = theme
        emitGroupCollectedResources('themes', rId)
      },
      needCollectOps
    )
  }
  Object.keys(themeSlideMastersMap).forEach((rId) => [
    themeSlideMastersMap[rId].forEach((master) => {
      const theme = themes[rId]
      if (theme) {
        master.setTheme(theme)
      }
    }),
  ])

  /* --- notesMasters ---*/
  const notesMasters = getRefItemMap(currentInfo.noteMasters)
  op = data['notesMasters']
  let retOp_notesMasters: Nullable<OperationFromPackedOp<PackedOpMap>>
  if (op) {
    retOp_notesMasters = loadNotesMasters(
      op,
      defaultTheme,
      params,
      (rId, newNotesMaster) => {
        notesMasters[rId] = newNotesMaster
        emitGroupCollectedResources('notesMasters', rId)
      },
      themeSlideMastersMap
    )
  }
  presentation.notesMasters = Object.keys(notesMasters).map(
    (rid) => notesMasters[rid]
  )

  if (presentation.notesMasters.length === 0) {
    presentation.notesMasters[0] = createNotesMaster()
    const notesTheme = defaultTheme.clone(`${DEFAULT_THEME_NAME}_NotesMaster`)
    notesTheme.presentation = presentation
    presentation.notesMasters[0].setTheme(notesTheme)
  }

  /* --- notes ---*/
  const notes = getRefItemMap(currentInfo.notes)
  const notesToMasterMap: Dict<string> = {}
  op = data['notes']
  let retOp_notes: Nullable<OperationFromPackedOp<PackedOpMap>>
  if (op) {
    retOp_notes = loadNotes(
      op,
      params,
      (rId, newNotes) => {
        notes[rId] = newNotes
        emitGroupCollectedResources('notes', rId)
      },
      notesToMasterMap,
      needCollectOps
    )
  }

  each((notesItem, notesRef) => {
    const notesMasterRef: Nullable<string> = notesToMasterMap[notesRef]
    const master: Nullable<NotesMaster> = notesMasters[notesMasterRef]

    // set note & noteMaster relation
    if (master) {
      notesItem.setNotesMaster(master)
    }
  }, notes)

  /* --- comments ---*/
  const comments: Dict<Nullable<SlideComments>> = {}
  let shouldSentCommentChangeEvt = false
  op = data['comments']
  let retOp_comments: Nullable<OperationFromPackedOp<PackedOpMap>>
  if (op) {
    retOp_comments = needCollectOps ? opFromPackedOperation(op) : undefined
    applyComments(op, params, (rId, slideComment) => {
      comments[rId] = slideComment
    })
    shouldSentCommentChangeEvt = true
  }

  // major slide data will be processed last
  /* --- slides ---*/

  const currentSlides: Array<[string, StringSlice, Slide, number]> = []
  op = data['slides']
  let retOp_slides: Nullable<OperationFromPackedOp<PackedOpMap>>
  let retOpDataMap:
    | OperationFromPackedOp<PackedOpMap>['data']['slide']
    | undefined
  if (op && refsInfo.slides.length > 0) {
    retOp_slides = needCollectOps
      ? { action: op.action, data: { slide: {} }, attributes: op.attributes }
      : undefined
    retOpDataMap = needCollectOps ? retOp_slides!.data.slide : undefined
    presentation.slides.length = 0
    const iter = getIterFromModocItemMap(op)

    while (iter.hasNext()) {
      const { id: rId, delta } = iter.next()
      const order = refsInfo.slides.indexOf(rId)
      if (order > -1) {
        const isDeltaValid = isValidSlideDeltaSlice(delta)
        if (isDeltaValid) {
          const slide = new Slide(presentation, null, null)
          slide.isLoading = true
          slide.setRefId(rId)
          slide.setSlideSize(presentation.width, presentation.height)
          currentSlides.push([rId, delta, slide, order])
        } else {
          if (retOpDataMap != null) {
            retOpDataMap[rId] = deltaSliceToOperations(delta)
          }
        }
      } else {
        if (retOpDataMap != null) {
          retOpDataMap[rId] = deltaSliceToOperations(delta)
        }
      }
    }
  }
  currentSlides.sort((a, b) => a[3] - b[3])
  turnOnChanges()

  if (currentSlides.length > 0) {
    // always sync load first slide
    const [rId, sdata, slide] = currentSlides[0]
    presentation.slides.push(slide)
    slide.udpateIndex(0)

    const ops = loadSlide(
      rId,
      sdata,
      slide,
      presentation,
      params,
      currentLayouts,
      notes,
      comments,
      needCollectOps
    )
    if (retOpDataMap != null) {
      retOpDataMap[rId] = ops ?? deltaSliceToOperations(sdata)
    }

    // currentSlides = currentSlides.slice(0, 2)
    for (let i = 1, l = currentSlides.length; i < l; i++) {
      const slide = currentSlides[i][2]
      presentation.slides.push(slide)
      slide.udpateIndex(i)
    }

    if (currentSlides.length > 1) {
      if (helperData.forceSync !== true && EditorSettings.asyncLoadingSlides) {
        await loadSlidesAsync(
          currentSlides,
          presentation,
          params,
          currentLayouts,
          notes,
          comments,
          retOpDataMap
        )
      } else {
        for (let i = 1, l = currentSlides.length; i < l; i++) {
          const [rId, sdata, slide] = currentSlides[i]
          const ops = loadSlide(
            rId,
            sdata,
            slide,
            presentation,
            params,
            currentLayouts,
            notes,
            comments,
            needCollectOps
          )
          if (retOpDataMap != null) {
            retOpDataMap[rId] = ops ?? deltaSliceToOperations(sdata)
          }
        }
      }
    }
  }
  // Events, Make sure all data are latest
  if (shouldSentCommentChangeEvt) {
    hookManager.invoke(Hooks.Emit.EmitChangeComment)
  }

  if (needCollectOps) {
    const pptOps = loadDeltaSlice(
      deltaSlice,
      PPTDocDataLayout,
      PPTDocDataLayoutTypeMap,
      params,
      (opType, _op, _params, _) => {
        switch (opType) {
          case 'presentation':
            return retOp_presentation
          case 'layouts':
            return retOp_layout
          case 'slideMasters':
            return retOp_slideMaster
          case 'slides':
            return retOp_slides
          case 'themes':
            return retOp_themes
          case 'notesMasters':
            return retOp_notesMasters
          case 'notes':
            return retOp_notes
          case 'comments':
            return retOp_comments
        }
      },
      true
    )
    return pptOps
  }
}

async function loadSlidesAsync(
  slidesInfo: Array<[string, StringSlice, Slide, number]>,
  presentation: Presentation,
  params: FromJSONParams,
  currentLayouts: Dict<SlideLayout>,
  notes: Dict<Notes>,
  comments: Dict<Nullable<SlideComments>>,
  collectOps?: Record<string, Operation[]>
) {
  const needCollectOps = collectOps != null
  const idleP = (fn: (...args: any[]) => void) =>
    new Promise<void>((resolve) => {
      requestIdleCallback((args) => resolve(fn(args)))
      // setTimeout(() => {
      //   resolve(fn())
      // }, 1000)
    })
  let promise = Promise.resolve()
  for (let i = 1, l = slidesInfo.length; i < l; i++) {
    const [rId, sdata, slide] = slidesInfo[i]
    promise = promise.then(() =>
      idleP(() => {
        const ops = loadSlide(
          rId,
          sdata,
          slide,
          presentation,
          params,
          currentLayouts,
          notes,
          comments,
          needCollectOps
        )
        if (collectOps != null) {
          collectOps[rId] = ops ?? deltaSliceToOperations(sdata)
        }
        hookManager.invoke(Hooks.Editor.OnAsyncSlideLoaded, { slide })
      })
    )
  }
  return promise
}

function loadSlide(
  rId: string,
  sdata: StringSlice,
  slide: Slide,
  presentation: Presentation,
  params: FromJSONParams,
  currentLayouts: Dict<SlideLayout>,
  notes: Dict<Notes>,
  comments: Dict<Nullable<SlideComments>>,
  needCollectOps = false
) {
  turnOffChanges()

  const getLoadTiming = startPerfTiming()
  const ops = applySlide(
    sdata,
    slide,
    params,
    rId,
    currentLayouts,
    ({ notesRef, commentsRef }) => {
      if (notesRef) {
        const notesItem: Nullable<Notes> = notes[notesRef]

        if (notesItem && notesItem.master && slide) {
          slide.setNotes(notesItem)
          notesItem.setSlide(slide)
        }
      }
      if (commentsRef) {
        const slideComment = comments[commentsRef]
        if (slideComment) {
          slideComment.setRefId(commentsRef)
          slideComment.setSlide(slide)
          slide.slideComments = slideComment // setSlideComments 会生成 changePoints
        }
      }
    },
    needCollectOps
  )
  if (!slide.notes) {
    slide.setNotes(createNotes())
    slide.notes!.setSlide(slide)
    slide.notes!.setNotesMaster(presentation.notesMasters[0])
  }
  slide.isLoading = false
  emitGroupCollectedResources('slides', rId)

  logger.log(`load slide "${slide.index}" timing:`, getLoadTiming())
  turnOnChanges()
  return ops
}

// helpers
function emitGroupCollectedResources(
  type: ResourcesContainerType,
  refId?: string
) {
  hookManager.invoke(Hooks.Attrs.GroupCollectedResources, { type, refId })
}
