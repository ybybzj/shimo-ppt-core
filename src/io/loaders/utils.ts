import { Nullable, Optional, valuesOfDict } from '../../../liber/pervasive'
import {
  StringSlice,
  getOpIterFromSlice,
  getLengthOfPackedOp,
  getMapIter,
  MapIter_hasNext,
  MapIter_next,
  PackedOpDelta,
  PackedOpJson,
  PackedOpMap,
  PackedOpString,
  PackedOpNumber,
  stringToSlice,
  opFromPackedOperation,
  PackedOperation,
  OperationFromPackedOp,
} from '../../re/export/modoc'
import { ModocDelta } from '../../types/globals/io'

import { Operation, OperationDataType } from '../operation'

type PackedOpTypeMap = {
  delta: PackedOpDelta
  json: PackedOpJson
  map: PackedOpMap
  string: PackedOpString
  number: PackedOpNumber
}

export type ModocItem = valuesOfDict<PackedOpTypeMap>

export function lengthOfModocItem(item: ModocItem): number {
  switch (item.type) {
    case 'number':
      return item.data
    case 'string':
      return item.data.length
    case 'map':
    case 'json':
    case 'delta':
      return 1
  }
}
export type DataLayoutPackedOpMap<
  L extends readonly string[],
  M extends {
    [key in L[number]]: OperationDataType
  },
> = { [key in L[number]]: PackedOpTypeMap[M[key]] }

export type DataLayoutOpMap<
  L extends readonly string[],
  M extends {
    [key in L[number]]: OperationDataType
  },
> = {
  [k in keyof DataLayoutPackedOpMap<L, M>]: OperationFromPackedOp<
    DataLayoutPackedOpMap<L, M>[k]
  >
}

export function dataFromDelta<
  L extends readonly string[],
  M extends { [key in L[number]]: OperationDataType },
>(
  ds: StringSlice,
  dataLayout: L,
  dataTypeLayoutMap: M
): Optional<DataLayoutPackedOpMap<L, M>> {
  const iter = getOpIterFromSlice(ds)
  const ret: Optional<DataLayoutPackedOpMap<L, M>> = {}
  let i = 0
  while (iter.hasNext() && i < dataLayout.length) {
    const op = iter.next()
    // Todo, check op
    const opType = op.attributes && op.attributes['opType']
    const expectedOpType = dataLayout[i]
    const expectedDataType = dataTypeLayoutMap[expectedOpType]
    const dataType = op.type
    if (
      (op.action === 'insert' &&
        (opType === undefined || opType === expectedOpType) &&
        expectedDataType === dataType) ||
      dataType === 'number'
    ) {
      // op is empty when dataType is 'number'
      if (dataType !== 'number') {
        ret[expectedOpType] = op
      }
    } else {
      console.warn(
        `[dataFromDelta]invalid input operation, expect "${expectedOpType} with data type \"${expectedDataType}\""`,
        op
      )
    }
    i += getLengthOfPackedOp(op)
  }

  return ret
}

type MapToOpUnion<M extends Record<string, PackedOperation>> = valuesOfDict<{
  [K in keyof M]: [opType: K, op: M[K]]
}>

export type deltaSliceLoader<
  L extends readonly string[],
  M extends { [key in L[number]]: OperationDataType },
  Context,
> = (
  ...args: [...MapToOpUnion<DataLayoutPackedOpMap<L, M>>, Context, boolean]
) => Nullable<Operation>

export function loadDeltaSlice<
  L extends readonly string[],
  M extends { [key in L[number]]: OperationDataType },
  Context,
>(
  ds: StringSlice,
  dataLayout: L,
  dataTypeLayoutMap: M,
  ctx: Context,
  loader: deltaSliceLoader<L, M, Context>,
  needCollectOps = false
): Nullable<Operation[]> {
  const iter = getOpIterFromSlice(ds)
  let i = 0
  const ops: Nullable<Operation[]> = needCollectOps ? [] : undefined
  while (iter.hasNext() && i < dataLayout.length) {
    const subOp = iter.next()

    // Todo, check op
    const opType = subOp.attributes && subOp.attributes['opType']
    const expectedOpType: L[number] = dataLayout[i]
    const expectedDataType = dataTypeLayoutMap[expectedOpType]
    const dataType = subOp.type

    let convertedOp: Nullable<Operation>
    if (
      (subOp.action === 'insert' &&
        (opType === undefined || opType === expectedOpType) &&
        expectedDataType === dataType) ||
      dataType === 'number'
    ) {
      // op is empty when dataType is 'number'
      if (dataType !== 'number') {
        convertedOp = loader(
          expectedOpType,
          subOp as DataLayoutPackedOpMap<L, M>[L[number]],
          ctx,
          needCollectOps
        )
      }
    } else {
      console.warn(
        `[dataFromDelta]invalid input operation, expect "${expectedOpType} with data type \"${expectedDataType}\""`,
        subOp
      )
    }
    i += getLengthOfPackedOp(subOp)

    if (needCollectOps) {
      const operation =
        convertedOp != null ? convertedOp : opFromPackedOperation(subOp)
      ops!.push(operation)
    }
  }

  if (needCollectOps) {
    while (iter.hasNext()) {
      const subOp = iter.next()
      ops!.push(opFromPackedOperation(subOp))
    }
  }

  return ops
}

export function loadPackedDeltaOp<
  L extends readonly string[],
  M extends { [key in L[number]]: OperationDataType },
  Context,
>(
  op: PackedOpDelta,
  dataLayout: L,
  dataTypeLayoutMap: M,
  ctx: Context,
  loader: deltaSliceLoader<L, M, Context>,
  needCollectOps = false
): Nullable<OperationFromPackedOp<PackedOpDelta>> {
  const { data: ds } = op
  let retOp: Nullable<OperationFromPackedOp<PackedOpDelta>>

  const ops = loadDeltaSlice(
    ds,
    dataLayout,
    dataTypeLayoutMap,
    ctx,
    loader,
    needCollectOps
  )

  if (ops != null) {
    retOp = {
      action: op.action,
      attributes: op.attributes,
      data: { delta: ops },
    }
  }

  return retOp
}
export function dataSliceFromDeltaByOpType<
  L extends readonly string[],
  OT extends L[number],
  M extends { [key in L[number]]: OperationDataType },
>(
  ds: StringSlice,
  opType: OT,
  dataLayout: L,
  dataTypeLayoutMap: M
): Nullable<StringSlice> {
  const iter = getOpIterFromSlice(ds)
  let ret: Nullable<StringSlice>
  let i = 0
  while (iter.hasNext() && i < dataLayout.length) {
    const op = iter.next()
    // Todo, check op
    const curOpType = op.attributes && op.attributes['opType']
    if (curOpType === opType) {
      const expectedDataType = dataTypeLayoutMap[opType]
      const dataType = op.type
      if (
        op.action === 'insert' &&
        (dataType === 'delta' || dataType === 'map') &&
        expectedDataType === dataType
      ) {
        ret = op.data
      }
      break
    }
    i += getLengthOfPackedOp(op)
  }
  return ret
}

export function dataSliceFromMapByRId(
  ds: StringSlice,
  rid: string
): Nullable<StringSlice> {
  const iter = getMapIter(ds)
  let ret: Nullable<StringSlice>
  while (MapIter_hasNext(iter)) {
    const { id: curRId, delta: op } = MapIter_next(iter)
    // Todo, check op
    if (rid === curRId) {
      ret = op
      break
    }
  }
  return ret
}

export function firstItemOfDeltaSlice(
  slice: StringSlice
): undefined | ModocItem {
  const iter = getOpIterFromSlice(slice)
  if (iter.hasNext()) {
    return iter.next()
  }
}

export function modocDeltaToSlice(input: ModocDelta): StringSlice {
  return stringToSlice(input.stringify())
}

export function getIterFromModocItemMap(item: PackedOpMap) {
  const iter = getMapIter(item.data)
  return {
    hasNext() {
      return MapIter_hasNext(iter)
    },
    next() {
      return MapIter_next(iter)
    },
  }
}

export function getIterFromModocItemDelta(item: PackedOpDelta) {
  return getOpIterFromSlice(item.data)
}
