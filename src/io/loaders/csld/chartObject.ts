import { Nullable } from '../../../../liber/pervasive'
import { CSld } from '../../../core/Slide/CSld'
import { ChartObject } from '../../../core/SlideElement/ChartObject'
import { ShapeLocks } from '../../../core/SlideElement/const'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { ImageShape } from '../../../core/SlideElement/Image'
import { setLock } from '../../../core/utilities/shape/locks'
import { setDeletedForSp } from '../../../core/utilities/shape/updates'
import { OperationFromPackedOp, PackedOpDelta } from '../../../re/export/modoc'
import { ChartAttrs } from '../../dataType/csld'
import {
  ChartDataLayout,
  ChartDataLayoutMap,
} from '../../defs/csld/chartObject'
import { FromJSONParams } from '../../defs/pptdoc'
import { deltaSliceLoader, loadPackedDeltaOp } from '../utils'
import { loadImage } from './image'
interface Context {
  chartObject: ChartObject
  params: FromJSONParams
}

function loadChartObject(
  op: PackedOpDelta,
  chartObject: ChartObject,
  rId: Nullable<string>,
  params: FromJSONParams,
  needCollectOps = false
) {
  if (rId != null && params.ignoreRId !== true) {
    chartObject.setRefId(rId)
  }

  const ctx: Context = { chartObject, params }
  const retOp = loadPackedDeltaOp(
    op,
    ChartDataLayout,
    ChartDataLayoutMap,
    ctx,
    loadChartObjectDeltaSlice,
    needCollectOps
  )
  return retOp
}

const loadChartObjectDeltaSlice: deltaSliceLoader<
  typeof ChartDataLayout,
  typeof ChartDataLayoutMap,
  Context
> = (opType, op, { chartObject, params }, needCollectOps) => {
  switch (opType) {
    case 'attrs': {
      const attrsData = op.data as ChartAttrs
      chartObject.attrsFromJSON(attrsData)
      return undefined
    }
    case 'pic': {
      return loadImage(op, chartObject, null, params, needCollectOps)
    }
  }
}

export function genChartObject(
  data: PackedOpDelta,
  rId: Nullable<string>,
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>,
  needCollectOps = false
): { sp: ImageShape; op: Nullable<OperationFromPackedOp<PackedOpDelta>> } {
  const chartObject = new ChartObject()
  chartObject.spTreeParent = spTreeParent
  setLock(chartObject, ShapeLocks.noModifyAspect, true)
  setDeletedForSp(chartObject, false)
  if (mainObject) {
    chartObject.setParent(mainObject)
  }
  const op = loadChartObject(data, chartObject, rId, params, needCollectOps)
  if (mainObject?.cSld?.checkUniNvPrForSps) {
    mainObject.cSld.checkUniNvPrForSps([chartObject])
  }
  return { sp: chartObject, op }
}
