import { Table } from '../../../../core/Table/Table'
import { TableDataLayout, TableDataLayoutMap } from '../../../defs/csld/table'
import { FromJSONParams } from '../../../defs/pptdoc'
import { TableLookData } from '../../../dataType/table'

import { Nullable } from '../../../../../liber/pervasive'
import { readFromJSON, decodeNullableValue } from '../../../utils'
import { TablePr } from '../../../../core/Table/attrs/TablePr'
import { TableLook } from '../../../../core/Table/common'
import { ScaleOfPPTXSizes } from '../../../../core/common/const/unit'
import { loadTableRow } from './row'
import { TableStyleData } from '../../../dataType/style'
import { CFillEffectsData } from '../../../dataType/spAttrs'
import { CShd } from '../../../../core/Table/attrs/CShd'
import { ShdType } from '../../../../core/common/const/attrs'
import { gGlobalTableStyles } from '../../../../core/Slide/GlobalTableStyles'
import { FillEffects } from '../../../../core/SlideElement/attrs/fill/fill'
import { TableStyle } from '../../../../core/Table/attrs/TableStyle'
import { loadPackedDeltaOp, deltaSliceLoader } from '../../utils'
import {
  OperationFromPackedOp,
  PackedOpDelta,
  getOpIterFromSlice,
  opFromPackedOperation,
} from '../../../../re/export/modoc'
import { Operation } from '../../../operation'

export function loadTable(
  table: Table,
  op: PackedOpDelta,
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  { presentation: _ }: FromJSONParams,
  needCollectOps = false
) {
  const ctx: { table: Table } = { table }
  const retOp = loadPackedDeltaOp(
    op,
    TableDataLayout,
    TableDataLayoutMap,
    ctx,
    loadTableDeltaSlice,
    needCollectOps
  )

  return retOp
}

const loadTableDeltaSlice: deltaSliceLoader<
  typeof TableDataLayout,
  typeof TableDataLayoutMap,
  { table: Table }
> = (opType, op, { table }, needCollectOps) => {
  switch (opType) {
    case 'attrs': {
      const dataOfStyle = op.data['style'] as TableStyleData | undefined
      const unifillData = op.data['fill'] as CFillEffectsData
      const Look = op.data['look'] as TableLookData

      const styleName = decodeNullableValue(dataOfStyle && dataOfStyle['name'])
      const styleId = dataOfStyle && dataOfStyle['id']
      const style = getTableStyle(styleId, styleName)
      if (style) {
        if (styleId != null) {
          style.guid = styleId
        }
        table.setTableStyleId(style.id, false)
      }

      const tablePr = table.pr ?? TablePr.new()
      const unifill = readFromJSON(unifillData, FillEffects)

      if (unifill) {
        tablePr.shd = CShd.new({
          value: ShdType.Clear,
          fillEffects: unifill,
        })
        table.setPr(tablePr)
      }

      const tableLook = readFromJSON(Look, TableLook)
      if (tableLook) {
        table.setTableLook(tableLook)
      }
      return undefined
    }
    case 'grid': {
      if (Array.isArray(op.data['grid'])) {
        const grid = op.data['grid'].map((n) => n / ScaleOfPPTXSizes)
        table.setTableGrid(grid)
      }
      return undefined
    }
    case 'rows': {
      let retOp: Nullable<OperationFromPackedOp<typeof op>>
      const iter = getOpIterFromSlice(op.data)
      let index = 0

      const ops: Operation[] | undefined = needCollectOps ? [] : undefined
      while (iter.hasNext()) {
        const rowOp = iter.next()
        let convertedOp: Nullable<Operation>

        if (rowOp.type === 'delta') {
          const row = table.addRow(index++, 0)
          convertedOp = loadTableRow(rowOp, row, needCollectOps)
        }

        if (needCollectOps) {
          ops!.push(
            convertedOp != null ? convertedOp : opFromPackedOperation(rowOp)
          )
        }
      }

      table.updateIndices()
      if (table.children.length > 0 && table.children.at(0)!.cellsCount > 0) {
        table.curCell = table.children.at(0)!.getCellByIndex(0)
      }

      if (needCollectOps) {
        retOp = {
          action: op.action,
          attributes: op.attributes,
          data: { delta: ops! },
        }
      }
      return retOp
    }
  }
}

//helper
function getTableStyle(
  styleGUID: Nullable<string>,
  styleName: Nullable<string>
): Nullable<TableStyle> {
  if (!styleName && !styleGUID) {
    return undefined
  }
  let result: Nullable<TableStyle>
  if (styleGUID != null) {
    const sid = gGlobalTableStyles.tableStyles.getIdByGuid(styleGUID)
    result = sid ? gGlobalTableStyles.tableStyles.Get(sid) : undefined
  }

  if (result == null && styleName != null) {
    const sid = gGlobalTableStyles.tableStyles.getIdByName(styleName)
    result = sid ? gGlobalTableStyles.tableStyles.Get(sid) : undefined
  }

  return result
}
