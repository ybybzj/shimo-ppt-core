import { TableCell } from '../../../../core/Table/TableCell'
import {
  TableCellDataLayout,
  TableCellDataLayoutMap,
} from '../../../defs/csld/table'
import { readFromJSON } from '../../../utils'
import { TableCellPr } from '../../../../core/Table/attrs/TableCellPr'
import { loadTextDocument } from '../../text'
import {
  TextBodyDataLayout,
  TextBodyDataLayoutTypeMap,
} from '../../../defs/csld/text'
import { CTableCellPrData } from '../../../dataType/style'
import { loadPackedDeltaOp, deltaSliceLoader } from '../../utils'
import { VMergeType } from '../../../../core/common/const/table'
import { Operation } from '../../../operation'
import { TextDocument } from '../../../../core/TextDocument/TextDocument'
import {
  OperationFromPackedOp,
  PackedOpDelta,
} from '../../../../re/export/modoc'
import { Nullable } from '../../../../../liber/pervasive'

export function loadTableCell(
  op: PackedOpDelta,
  cell: TableCell,
  needCollectOps = false
) {
  return loadPackedDeltaOp(
    op,
    TableCellDataLayout,
    TableCellDataLayoutMap,
    cell,
    loadCellDeltaSlice,
    needCollectOps
  )
}

const loadCellDeltaSlice: deltaSliceLoader<
  typeof TableCellDataLayout,
  typeof TableCellDataLayoutMap,
  TableCell
> = (opType, op, cell, needCollectOps) => {
  switch (opType) {
    case 'attrs': {
      const rowSpan = op.data['rowSpan'] as number
      const gridSpan = op.data['gridSpan'] as number
      const vMerge = op.data['vMerge'] as boolean
      const pr = op.data['pr'] as CTableCellPrData
      if (pr === null) {
        cell.setPr(TableCellPr.new())
      } else {
        const cellPr = readFromJSON(pr, TableCellPr)
        if (cellPr) {
          cell.setPr(cellPr)
        }
      }

      // 如果是合并单元格操作则一定会全量替换
      if (rowSpan && rowSpan > 1) {
        cell.setVMerge(VMergeType.Restart)
      }

      if (gridSpan && gridSpan > 1) {
        cell.setGridSpan(gridSpan)
      } else {
        cell.setGridSpan(1)
      }

      if (vMerge === true && cell.pr.vMerge !== VMergeType.Restart) {
        cell.setVMerge(VMergeType.Continue)
      }
      return undefined
    }
    case 'txBody': {
      const retOp = loadPackedDeltaOp(
        op,
        TextBodyDataLayout,
        TextBodyDataLayoutTypeMap,
        cell.textDoc,
        loadTxBodyDeltaSlice,
        needCollectOps
      )

      return retOp
    }
  }
}

const loadTxBodyDeltaSlice: deltaSliceLoader<
  typeof TextBodyDataLayout,
  typeof TextBodyDataLayoutTypeMap,
  TextDocument
> = (opType, op, textDoc, needCollectOps) => {
  switch (opType) {
    case 'docContent': {
      const ops: undefined | Operation[] = needCollectOps ? [] : undefined
      loadTextDocument(op.data, textDoc, ops)
      const retOp: Nullable<OperationFromPackedOp<typeof op>> =
        ops != null
          ? {
              action: op.action,
              attributes: op.attributes,
              data: {
                delta: ops,
              },
            }
          : undefined
      return retOp
    }
  }
}
