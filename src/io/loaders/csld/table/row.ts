import { TableRow } from '../../../../core/Table/TableRow'
import {
  TableRowDataLayout,
  TableRowDataLayoutMap,
} from '../../../defs/csld/table'
import { loadTableCell } from './cell'
import { Nullable } from '../../../../../liber/pervasive'

import { LineHeightRule } from '../../../../core/common/const/attrs'
import { ScaleOfPPTXSizes } from '../../../../core/common/const/unit'
import { loadPackedDeltaOp, deltaSliceLoader } from '../../utils'
import {
  OperationFromPackedOp,
  PackedOpDelta,
  getOpIterFromSlice,
  opFromPackedOperation,
} from '../../../../re/export/modoc'
import { VMergeType } from '../../../../core/common/const/table'
import { Operation } from '../../../operation'
interface Context {
  readonly row: TableRow
  rowHeight: Nullable<number>
}

export function loadTableRow(
  op: PackedOpDelta,
  row: TableRow,
  needCollectOps = false
) {
  const ctx: Context = {
    row,
    rowHeight: undefined,
  }

  const retOp = loadPackedDeltaOp(
    op,
    TableRowDataLayout,
    TableRowDataLayoutMap,
    ctx,
    loadRowDeltaSlice,
    needCollectOps
  )

  setRowHeight(row, ctx.rowHeight)
  return retOp
}

const loadRowDeltaSlice: deltaSliceLoader<
  typeof TableRowDataLayout,
  typeof TableRowDataLayoutMap,
  Context
> = (opType, op, ctx, needCollectOps) => {
  switch (opType) {
    case 'attrs': {
      ctx.rowHeight = op.data['height'] / ScaleOfPPTXSizes
      return undefined
    }
    case 'cells': {
      const row = ctx.row
      let cellIndex = 0
      let convertedOp: Nullable<Operation>
      const opsForCells: Operation[] | undefined = needCollectOps
        ? []
        : undefined
      const iter = getOpIterFromSlice(op.data)
      while (iter.hasNext()) {
        const opCell = iter.next()

        if (opCell.type === 'delta') {
          const cell = row.addCell(cellIndex)
          convertedOp = loadTableCell(opCell, cell, needCollectOps)
          cellIndex += 1
        }
        // else{
        // cell is empty
        // }
        if (needCollectOps) {
          const operation =
            convertedOp != null ? convertedOp : opFromPackedOperation(opCell)
          opsForCells!.push(operation)
        }
        //reset convertOp after
        convertedOp = undefined
      }
      row.updateCellIndices(0)
      const retOp: OperationFromPackedOp<typeof op> | undefined = needCollectOps
        ? {
            action: op.action,
            attributes: op.attributes,
            data: {
              delta: opsForCells!,
            },
          }
        : undefined
      return retOp
    }
  }
}

function setRowHeight(row: TableRow, height: Nullable<number>) {
  let maxTopMargin = 0,
    maxBottomMargin = 0,
    maxTopBorder = 0,
    maxBottomBorder = 0

  const rowHeight = height ?? 5

  for (let i = 0, l = row.children.length; i < l; ++i) {
    const cell = row.children.at(i)!
    const cellPr = cell.pr

    const vMerge = cellPr.vMerge
    if (vMerge === VMergeType.Continue) continue

    const margins = cell.getMargins()
    if (margins) {
      if (margins.bottom && margins.bottom.w > maxBottomMargin) {
        maxBottomMargin = margins.bottom.w
      }
      if (margins.top && margins.top.w > maxTopMargin) {
        maxTopMargin = margins.top.w
      }
    }
    // 注意 border 有默认间距
    const borders = cellPr.tableCellBorders != null ? null : cell.getBorders()
    if (borders) {
      if (borders.top && borders.top.size > maxTopBorder) {
        maxTopBorder = borders.top.size
      }
      if (borders.bottom && borders.bottom.size > maxBottomBorder) {
        maxBottomBorder = borders.bottom.size
      }
    }
    // apply 过后需要重新触发一次 tabelCell 的 recalc
    cell.invalidateCalcedPrs(false)
  }

  const newHeight = Math.max(
    1,
    rowHeight -
      maxTopMargin -
      maxBottomMargin -
      maxTopBorder / 2 -
      maxBottomBorder / 2
  )

  row.setHeight({
    value: newHeight,
    heightRule: LineHeightRule.AtLeast,
  })
}
