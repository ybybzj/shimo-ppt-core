import { ImageShape } from '../../../core/SlideElement/Image'
import { Nullable } from '../../../../liber/pervasive'
import { ImageDataLayout, ImageDataLayoutMap } from '../../defs/csld/image'
import {
  UniNvPrData,
  CBlipFillData,
  CSpPrData,
  CShapeStyleData,
  SpTypes,
  ImageSubTypes,
} from '../../dataType/spAttrs'
import { readFromJSON } from '../../utils'

import { FromJSONParams } from '../../defs/pptdoc'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { getMediaFromShape } from '../../../core/utilities/shape/getters'
import { hookManager, Hooks } from '../../../core/hooks'
import { CSld } from '../../../core/Slide/CSld'
import { setDeletedForSp } from '../../../core/utilities/shape/updates'
import { BlipFill } from '../../../core/SlideElement/attrs/fill/blipFill'
import { UniNvPr, SpPr } from '../../../core/SlideElement/attrs/shapePrs'
import { ShapeStyle } from '../../../core/SlideElement/attrs/ShapeStyle'
import { setLock } from '../../../core/utilities/shape/locks'
import { ShapeLocks } from '../../../core/SlideElement/const'
import { deltaSliceLoader, loadPackedDeltaOp } from '../utils'
import { OperationFromPackedOp, PackedOpDelta } from '../../../re/export/modoc'
interface Context {
  image: ImageShape
  params: FromJSONParams
}
export function loadImage(
  op: PackedOpDelta,
  image: ImageShape,
  rId: Nullable<string>,
  params: FromJSONParams,
  needCollectOps = false
) {
  if (rId != null && params.ignoreRId !== true) {
    image.setRefId(rId)
  }

  const ctx: Context = { image, params }
  const retOp = loadPackedDeltaOp(
    op,
    ImageDataLayout,
    ImageDataLayoutMap,
    ctx,
    loadImageDeltaSlice,
    needCollectOps
  )
  checkUpdateErrorMedia(image)
  return retOp
}

const loadImageDeltaSlice: deltaSliceLoader<
  typeof ImageDataLayout,
  typeof ImageDataLayoutMap,
  Context
> = (opType, op, { image }, _) => {
  switch (opType) {
    case 'blipFill': {
      const blipFillData = op.data['blipFill'] as CBlipFillData
      const blipFill = readFromJSON(blipFillData, BlipFill)
      if (blipFill != null && blipFill.imageSrcId != null) {
        image.setBlipFill(blipFill)
        // const imgUrl = blipFill.imageSrcId
        // if(isBase64Url(imgUrl)){
        //   hookManager.invoke(Hooks.Attrs.CollectBase64ImageFromDataChange, {
        //     dataUrl: imgUrl,
        //     sp: image
        //   })
        // }
      }
      return undefined
    }
    case 'nvPicPr': {
      const nvPicPrData = op.data['nvPicPr'] as UniNvPrData
      const nvPicPr = readFromJSON(nvPicPrData, UniNvPr, undefined, [
        SpTypes.Pic,
      ])
      if (nvPicPr != null) {
        image.setNvPicPr(nvPicPr)
      }
      return undefined
    }
    case 'spPr': {
      const spPrData = op.data['spPr'] as CSpPrData
      const spPr = readFromJSON(spPrData, SpPr)
      if (spPr != null) {
        spPr.setParent(image)
        // if (spPr.geometry == null) {
        //   spPr.setGeometry(CreateGeometry('rect'))
        // }
        image.setSpPr(spPr)
      }
      return undefined
    }
    case 'style': {
      const dataOfStyle = op.data as CShapeStyleData
      const style = readFromJSON(dataOfStyle, ShapeStyle)
      if (style != null) {
        image.setStyle(style)
      }
      return undefined
    }
  }
}

export function genImage(
  data: PackedOpDelta,
  rId: Nullable<string>,
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>,
  subType?: ImageSubTypes,
  needCollectOps = false
): { sp: ImageShape; op: Nullable<OperationFromPackedOp<PackedOpDelta>> } {
  const image = new ImageShape()
  image.spTreeParent = spTreeParent
  setLock(image, ShapeLocks.noModifyAspect, true)
  setDeletedForSp(image, false)
  if (mainObject) {
    image.setParent(mainObject)
  }
  const op = loadImage(data, image, rId, params, needCollectOps)
  if (mainObject?.cSld?.checkUniNvPrForSps) {
    mainObject.cSld.checkUniNvPrForSps([image])
  }
  image.subType = subType
  if (!image.subType) {
    hookManager.invoke(Hooks.EditorUI.OnUpdateImageSubType, {
      image,
    })
  }
  return { sp: image, op }
}

function checkUpdateErrorMedia(image: ImageShape) {
  const media = getMediaFromShape(image)
  const imageUrl = image.blipFill?.imageSrcId
  if (media && imageUrl) {
    hookManager.invoke(Hooks.Attrs.CheckKnownErrorImageUrl, {
      url: imageUrl,
      media: media.media,
    })
  }
}
