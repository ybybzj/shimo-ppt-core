import { TextBody } from '../../../core/SlideElement/TextBody'
import { BodyPr } from '../../../core/SlideElement/attrs/bodyPr'
import { TextDocument } from '../../../core/TextDocument/TextDocument'
import { TextListStyle } from '../../../core/textAttributes/TextListStyle'
import { PackedOpDelta } from '../../../re/export/modoc'
import {
  TextBodyDataLayout,
  TextBodyDataLayoutTypeMap,
} from '../../defs/csld/text'
import { FromJSONParams } from '../../defs/pptdoc'
import { Operation } from '../../operation'
import { fromJSON } from '../../utils'
import { loadTextDocument } from '../text'
import { deltaSliceLoader, loadPackedDeltaOp } from '../utils'
interface Context {
  textBody: TextBody
  helperData?: { textBodyTextFit: TextBody[] }
}

export function loadTextBody(
  op: PackedOpDelta,
  textBody: TextBody,
  { helperData }: FromJSONParams,
  needCollectOps = false
) {
  const ctx: Context = {
    textBody,
    helperData,
  }
  const retOp = loadPackedDeltaOp(
    op,
    TextBodyDataLayout,
    TextBodyDataLayoutTypeMap,
    ctx,
    loadTextBodyDeltaSlice,
    needCollectOps
  )
  return retOp
}

const loadTextBodyDeltaSlice: deltaSliceLoader<
  typeof TextBodyDataLayout,
  typeof TextBodyDataLayoutTypeMap,
  Context
> = (opType, op, { textBody, helperData }, needCollectOps) => {
  switch (opType) {
    case 'bodyPr': {
      const bodyPr = new BodyPr()
      fromJSON(bodyPr, op.data || {})
      textBody.setBodyPr(bodyPr)
      if (textBody.bodyPr && textBody.bodyPr.textFit) {
        if (helperData) {
          helperData.textBodyTextFit.push(textBody)
        }
      }
      return undefined
    }

    case 'listStyle': {
      const styles = new TextListStyle()
      const listStyle = op.data['listStyle'] ?? op.data
      if (Array.isArray(listStyle)) {
        fromJSON(styles, listStyle)
      }
      textBody.setTextListStyle(styles)
      return undefined
    }
    case 'docContent': {
      const textDoc = TextDocument.new(textBody, 0, 0, 0, 0)
      const ops: undefined | Operation[] = needCollectOps ? [] : undefined
      loadTextDocument(op.data, textDoc, ops)

      textBody.setContent(textDoc)

      return ops != null
        ? {
            action: op.action,
            attributes: op.attributes,
            data: {
              delta: ops,
            },
          }
        : undefined
    }
  }
}
