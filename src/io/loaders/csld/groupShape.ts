import { FromJSONParams } from '../../defs/pptdoc'
import {
  GroupShapeDataLayout,
  GroupShapeDataLayoutTypeMap,
} from '../../defs/csld/groupShape'
import { Nullable } from '../../../../liber/pervasive'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { readFromJSON, scaleValue } from '../../utils'
import { isNumber } from '../../../common/utils'
import { ImageSubTypes, SpTypes } from '../../dataType/spAttrs'
import { SlideElement } from '../../../core/SlideElement/type'
import { genShape } from './shape'
import { genImage } from './image'
import { genGraphicFrame } from './graphicFrame'
import { genConnectionShape } from './connectionShape'
import { collectCNvNidToSpIdMap } from '../../../core/utilities/shape/manageCxnSpState'
import {
  setDeletedForSp,
  setGroup,
  setParentForSlideElement,
} from '../../../core/utilities/shape/updates'
import { checkErrorImage } from '../../../images/errorImageSrc'
import { isEmptySlideElement } from '../../../core/utilities/shape/asserts'
import { CSld } from '../../../core/Slide/CSld'
import { updateCalcStatusForGroup } from '../../../core/calculation/updateCalcStatus/group'
import { SpPr, UniNvPr, Xfrm } from '../../../core/SlideElement/attrs/shapePrs'
import { setLocks } from '../../../core/utilities/shape/locks'
import { genOle } from './oleObject'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { adjustXfrmOfGroup } from '../../../core/utilities/shape/group'
import {
  deltaSliceLoader,
  firstItemOfDeltaSlice,
  getIterFromModocItemMap,
  loadPackedDeltaOp,
} from '../utils'
import { genChartObject } from './chartObject'
import { Hooks, hookManager } from '../../../core/hooks'
import { ChartObject } from '../../../core/SlideElement/ChartObject'
import { ScaleOfPPTXSizes } from '../../../core/common/const/unit'
import {
  OperationFromPackedOp,
  PackedOpDelta,
  PackedOpMap,
  deltaSliceToOperations,
  opFromPackedOperation,
} from '../../../re/export/modoc'

export function genGroupShape(
  data: PackedOpDelta,
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>,
  rId?: Nullable<string>,
  needCollectOps = false
): { sp: GroupShape; op: Nullable<OperationFromPackedOp<PackedOpDelta>> } {
  const group = new GroupShape()
  group.spTreeParent = spTreeParent
  setDeletedForSp(group, false)
  if (mainObject) {
    setParentForSlideElement(group, mainObject)
  }
  const op = loadGroupShape(data, group, rId, params, needCollectOps)
  return { sp: group, op }
}

interface Context {
  group: GroupShape
  params: FromJSONParams
}

function loadGroupShape(
  op: PackedOpDelta,
  group: GroupShape,
  rId: Nullable<string>,
  params: FromJSONParams,
  needCollectOps = false
) {
  if (rId != null && params.ignoreRId !== true) {
    group.setRefId(rId)
  }
  const ctx: Context = { group, params }
  const retOp = loadPackedDeltaOp(
    op,
    GroupShapeDataLayout,
    GroupShapeDataLayoutTypeMap,
    ctx,
    loadGroupShapeDeltaSlice,
    needCollectOps
  )
  checkGroupShapeXfrm(group)
  return retOp
}

const loadGroupShapeDeltaSlice: deltaSliceLoader<
  typeof GroupShapeDataLayout,
  typeof GroupShapeDataLayoutTypeMap,
  Context
> = (opType, op, { group, params }, needCollectOps) => {
  switch (opType) {
    case 'grpSpPr': {
      const spPrData = op.data['grpSpPr']
      const spPr = readFromJSON(spPrData, SpPr)
      if (spPr) {
        group.setSpPr(spPr)
        spPr.setParent(group)
      }
      return undefined
    }
    case 'nvGrpSpPr': {
      const nvSpPrData = op.data['nvGrpSpPr'] || {}
      const pr = readFromJSON(nvSpPrData, UniNvPr, undefined, [SpTypes.Shape])
      if (pr) {
        group.setNvSpPr(pr)
        if (isNumber(pr.nvUniSpPr?.locks)) {
          setLocks(group, pr.nvUniSpPr!.locks)
        }
      }
      return undefined
    }
    case 'spTree': {
      const updateSp = (sp: SlideElement) => {
        if (sp && sp.spPr && sp.spPr.xfrm) {
          sp.spTreeParent = group
          setGroup(sp, group)
        }
      }

      const retOp = loadSpTree(
        group,
        op,
        group.parent,
        params,
        updateSp,
        needCollectOps
      )
      updateCalcStatusForGroup(group, EditActionFlag.edit_GroupShapeAddToSpTree)
      return retOp
    }
  }
}

function checkGroupShapeXfrm(group: GroupShape) {
  if (!group || !group.spPr) {
    return
  }
  if (!group.spPr.xfrm && group.spTree.length > 0) {
    const xfrm = new Xfrm(group.spPr)
    xfrm.setOffX(0)
    xfrm.setOffY(0)
    xfrm.setChOffX(0)
    xfrm.setChOffY(0)
    xfrm.setExtX(50)
    xfrm.setExtY(50)
    xfrm.setChExtX(50)
    xfrm.setChExtY(50)
    group.spPr.setXfrm(xfrm)
    adjustXfrmOfGroup(group)
    group.spPr!.xfrm!.setParent(group.spPr)
  }
}

export function loadSpTree(
  csld: CSld | GroupShape,
  op: PackedOpMap,
  mainObject: Nullable<any>,
  params: FromJSONParams,
  updateSp: (sp: SlideElement, rId: Nullable<string>) => void,
  needCollectOps = false
): Nullable<OperationFromPackedOp<PackedOpMap>> {
  const retOp: Nullable<OperationFromPackedOp<PackedOpMap>> = needCollectOps
    ? { action: op.action, data: { slide: {} }, attributes: op.attributes }
    : undefined
  const retOpDataMap:
    | OperationFromPackedOp<PackedOpMap>['data']['slide']
    | undefined = needCollectOps ? retOp!.data.slide : undefined

  const iter = getIterFromModocItemMap(op)

  const orderRids: [number, string][] = []
  const spPackedOpMap: Map<string, PackedOpDelta> = new Map()
  while (iter.hasNext()) {
    const { id: rid, delta } = iter.next()
    const spData = firstItemOfDeltaSlice(delta)
    if (spData && spData.type === 'delta') {
      const order = (
        spData.attributes ? spData.attributes['order'] : -1
      ) as number
      orderRids.push([order, rid])
      spPackedOpMap.set(rid, spData)
      if (retOpDataMap != null) {
        retOpDataMap[rid] = deltaSliceToOperations(delta, 1)
      }
    } else {
      if (retOpDataMap != null) {
        retOpDataMap[rid] = deltaSliceToOperations(delta)
      }
    }
  }
  orderRids.sort(([order1, rId1], [order2, rId2]) =>
    order1 !== order2 ? order1 - order2 : rId1 < rId2 ? -1 : 1
  )

  const sps: SlideElement[] = []

  for (const or of orderRids) {
    const rId = or[1]
    const spPackedOp = spPackedOpMap.get(rId)
    if (spPackedOp) {
      const spOpInfo = genSp(
        spPackedOp,
        params,
        csld,
        mainObject,
        rId,
        needCollectOps
      )
      if (spOpInfo != null) {
        const { sp, op } = spOpInfo
        if (retOpDataMap != null && op != null) {
          retOpDataMap[rId].unshift(op)
        }
        if (!isHiddenSp(sp) && !isEmptySlideElement(sp)) {
          updateSp(sp, rId)
          sps.push(sp)
          collectCNvNidToSpIdMap(sp)
        }
      } else {
        if (retOpDataMap != null) {
          retOpDataMap[rId].unshift(opFromPackedOperation(spPackedOp))
        }
      }
    }
  }
  if (typeof (csld as CSld).checkUniNvPrForSps === 'function') {
    csld.spTree = []
    sps.forEach((shape) => {
      ;(csld as CSld).addSpAtIndex(shape, csld.spTree.length)
    })
  } else {
    csld.spTree = sps
  }
  return retOp
}

export function genSp(
  op: PackedOpDelta,
  params: FromJSONParams,
  csld: CSld | GroupShape,
  mainObject?: Nullable<any>,
  rId?: Nullable<string>,
  needCollectOps = false
):
  | {
      sp: SlideElement
      op: Nullable<OperationFromPackedOp<PackedOpDelta>>
    }
  | undefined {
  let ret:
    | {
        sp: SlideElement
        op: Nullable<OperationFromPackedOp<PackedOpDelta>>
      }
    | undefined
  if (op.attributes) {
    switch (op.attributes['spType']) {
      case 'sp': {
        ret = genShape(op, params, csld, mainObject, rId, needCollectOps)
        break
      }
      case 'grpSp': {
        ret = genGroupShape(op, params, csld, mainObject, rId, needCollectOps)
        break
      }
      case 'ole': {
        ret = genOle(op, rId, params, csld, mainObject, needCollectOps)
        break
      }
      case 'pic': {
        const subType = op.attributes['subType'] as ImageSubTypes
        const _ret = genImage(
          op,
          rId,
          params,
          csld,
          mainObject,
          subType,
          needCollectOps
        )
        const image = _ret.sp
        if (image.blipFill && !checkErrorImage(image.blipFill.imageSrcId)) {
          ret = _ret
          const xfrm = image.spPr?.xfrm
          const imgSrc = image.blipFill?.imageSrcId
          if (imgSrc != null && xfrm?.extX != null && xfrm?.extY != null) {
            hookManager.invoke(
              Hooks.Attrs.CollectImageUrlParamsFromDataChange,
              {
                imgSrc,
                type: 'image',
                extX: scaleValue(xfrm.extX, ScaleOfPPTXSizes)!,
                extY: scaleValue(xfrm.extY, ScaleOfPPTXSizes)!,
              }
            )
          }
        }
        break
      }
      case 'graphicFrame': {
        ret = genGraphicFrame(op, params, csld, mainObject, rId, needCollectOps)
        break
      }
      case 'cxnSp': {
        ret = genConnectionShape(
          op,
          params,
          csld,
          mainObject,
          rId,
          needCollectOps
        )
        break
      }
      case 'chart': {
        ret = genChartObject(op, rId, params, csld, mainObject, needCollectOps)
        const sp = ret.sp as ChartObject
        const imgSrc = sp.blipFill?.imageSrcId

        const xfrm = sp.spPr?.xfrm
        if (imgSrc != null && xfrm?.extX != null && xfrm?.extY != null) {
          hookManager.invoke(Hooks.Attrs.CollectImageUrlParamsFromDataChange, {
            imgSrc,
            type: 'chart',
            extX: scaleValue(xfrm.extX, ScaleOfPPTXSizes)!,
            extY: scaleValue(xfrm.extY, ScaleOfPPTXSizes)!,
          })
        }

        break
      }
      // ...
    }
  }

  return ret
}

export function isHiddenSp(sp) {
  if (!sp) return false
  let nvProps = sp.nvSpPr
  if (!nvProps) nvProps = sp.nvPicPr
  if (!nvProps) nvProps = sp.nvGrpSpPr

  if (!nvProps) return false

  if (nvProps.cNvPr && nvProps.cNvPr.isHidden) return true

  return false
}
