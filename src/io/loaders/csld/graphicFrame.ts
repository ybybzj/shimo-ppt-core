import { GraphicFrame } from '../../../core/SlideElement/GraphicFrame'
import { Nullable } from '../../../../liber/pervasive'
import { FromJSONParams } from '../../defs/pptdoc'
import {
  GraphicFrameDataLayout,
  GraphicFrameDataLayoutMap,
} from '../../defs/csld/graphicFrame'
import { readFromJSON } from '../../utils'
import { loadTable } from './table'
import { UniNvPrData, CXfrmData, SpTypes } from '../../dataType/spAttrs'
import { InstanceType, isInstanceTypeOf } from '../../../core/instanceTypes'
import { Table } from '../../../core/Table/Table'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import {
  setDeletedForSp,
  setParentForSlideElement,
} from '../../../core/utilities/shape/updates'
import { CSld } from '../../../core/Slide/CSld'
import { UniNvPr, SpPr, Xfrm } from '../../../core/SlideElement/attrs/shapePrs'
import { deltaSliceLoader, loadPackedDeltaOp } from '../utils'
import { OperationFromPackedOp, PackedOpDelta } from '../../../re/export/modoc'
interface Context {
  graphicFrame: GraphicFrame
  params: FromJSONParams
}
function loadGraphicFrame(
  op: PackedOpDelta,
  graphicFrame: GraphicFrame,
  rId: Nullable<string>,
  params: FromJSONParams,
  needCollectOps = false
) {
  if (rId != null && params.ignoreRId !== true) {
    graphicFrame.setRefId(rId)
  }

  const ctx: Context = { graphicFrame, params }
  const retOp = loadPackedDeltaOp(
    op,
    GraphicFrameDataLayout,
    GraphicFrameDataLayoutMap,
    ctx,
    loadGraphicFrameDeltaSlice,
    needCollectOps
  )

  return retOp
}

const loadGraphicFrameDeltaSlice: deltaSliceLoader<
  typeof GraphicFrameDataLayout,
  typeof GraphicFrameDataLayoutMap,
  Context
> = (opType, op, { graphicFrame, params }, needCollectOps) => {
  switch (opType) {
    case 'attrs': {
      const nvGraphicFramePrData = op.data[
        'nvGraphicFramePr'
      ] as Nullable<UniNvPrData>

      const xfrmData = op.data['xfrm'] as Nullable<CXfrmData>

      const nvGraphicFramePr = readFromJSON(
        nvGraphicFramePrData,
        UniNvPr,
        undefined,
        [SpTypes.GraphicFrame]
      )
      if (nvGraphicFramePr) {
        graphicFrame.setNvSpPr(nvGraphicFramePr)
      }

      if (xfrmData) {
        if (!graphicFrame.spPr) {
          graphicFrame.setSpPr(new SpPr())
          graphicFrame.spPr!.setParent(graphicFrame)
        }
        const xfrm = readFromJSON(xfrmData, Xfrm, [graphicFrame.spPr!])
        if (xfrm) {
          graphicFrame.spPr!.setXfrm(xfrm)
          xfrm.setParent(graphicFrame.spPr!)
        }
      }
      return undefined
    }
    case 'graphic': {
      const table = new Table(graphicFrame, 0, 0, undefined)
      const tableOp = loadTable(table, op, params, needCollectOps)

      const xfrm = graphicFrame.spPr?.xfrm
      if (xfrm) {
        table.resetDimension(0, 0, xfrm.extX, 100000)
      }

      graphicFrame.setGraphicObject(table)
      return tableOp
    }
  }
}

export function genGraphicFrame(
  data: PackedOpDelta,
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>,
  rId?: Nullable<string>,
  needCollectOps = false
): { sp: GraphicFrame; op: Nullable<OperationFromPackedOp<PackedOpDelta>> } {
  const graphicFrame = new GraphicFrame()
  graphicFrame.spTreeParent = spTreeParent
  setDeletedForSp(graphicFrame, false)
  if (mainObject) {
    setParentForSlideElement(graphicFrame, mainObject)
  }

  const op = loadGraphicFrame(data, graphicFrame, rId, params, needCollectOps)

  if (isInstanceTypeOf(mainObject, InstanceType.SlideLayout)) {
    params.presentation.adjustGraphicFrameRowHeight(graphicFrame)
  }

  return { sp: graphicFrame, op }
}
