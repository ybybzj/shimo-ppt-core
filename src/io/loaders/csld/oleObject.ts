import { Nullable } from '../../../../liber/pervasive'
import { CSld } from '../../../core/Slide/CSld'
import { ShapeLocks } from '../../../core/SlideElement/const'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { ImageShape } from '../../../core/SlideElement/Image'
import { OleObject } from '../../../core/SlideElement/OleObject'
import { setLock } from '../../../core/utilities/shape/locks'
import { setDeletedForSp } from '../../../core/utilities/shape/updates'
import { OperationFromPackedOp, PackedOpDelta } from '../../../re/export/modoc'
import { OLEObjectAttrs } from '../../dataType/csld'
import { OleDataLayout, OleDataLayoutMap } from '../../defs/csld/oleObject'
import { FromJSONParams } from '../../defs/pptdoc'
import { deltaSliceLoader, loadPackedDeltaOp } from '../utils'
import { loadImage } from './image'
interface Context {
  oleObject: OleObject
  params: FromJSONParams
}

function loadOle(
  op: PackedOpDelta,
  oleObject: OleObject,
  rId: Nullable<string>,
  params: FromJSONParams,
  needCollectOps = false
) {
  if (rId != null && params.ignoreRId !== true) {
    oleObject.setRefId(rId)
  }

  const ctx: Context = { oleObject, params }

  const retOp = loadPackedDeltaOp(
    op,
    OleDataLayout,
    OleDataLayoutMap,
    ctx,
    loadOleDeltaSlice,
    needCollectOps
  )

  return retOp
}
const loadOleDeltaSlice: deltaSliceLoader<
  typeof OleDataLayout,
  typeof OleDataLayoutMap,
  Context
> = (opType, op, { oleObject, params }, needCollectOps) => {
  switch (opType) {
    case 'attrs': {
      const attrsData = op.data as OLEObjectAttrs
      oleObject.attrsFromJSON(attrsData)
      return undefined
    }
    case 'pic': {
      const retOp = loadImage(op, oleObject, null, params, needCollectOps)
      return retOp
    }
  }
}

export function genOle(
  data: PackedOpDelta,
  rId: Nullable<string>,
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>,
  needCollectOps = false
): { sp: ImageShape; op: Nullable<OperationFromPackedOp<PackedOpDelta>> } {
  const oleObject = new OleObject()
  oleObject.spTreeParent = spTreeParent
  setLock(oleObject, ShapeLocks.noModifyAspect, true)
  setDeletedForSp(oleObject, false)
  if (mainObject) {
    oleObject.setParent(mainObject)
  }
  const op = loadOle(data, oleObject, rId, params, needCollectOps)
  if (mainObject?.cSld?.checkUniNvPrForSps) {
    mainObject.cSld.checkUniNvPrForSps([oleObject])
  }
  return { sp: oleObject, op }
}
