import { FromJSONParams } from '../../defs/pptdoc'
import { Nullable } from '../../../../liber/pervasive'
import { readFromJSON } from '../../utils'
import { UniNvPr, SpPr } from '../../../core/SlideElement/attrs/shapePrs'
import { isNumber } from '../../../common/utils'
import { ShapeStyle } from '../../../core/SlideElement/attrs/ShapeStyle'
import { SpTypes } from '../../dataType/spAttrs'
import { ConnectionShape } from '../../../core/SlideElement/ConnectionShape'
import {
  ConnectionShapeDataLayout,
  ConnectionShapeDataLayoutTypeMap,
} from '../../defs/csld/connectionShape'
import { collectCxnSpToSpMapState } from '../../../core/utilities/shape/manageCxnSpState'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import {
  setDeletedForSp,
  setParentForSlideElement,
} from '../../../core/utilities/shape/updates'
import { CSld } from '../../../core/Slide/CSld'
import { setLocks } from '../../../core/utilities/shape/locks'
import { deltaSliceLoader, loadPackedDeltaOp } from '../utils'
import { OperationFromPackedOp, PackedOpDelta } from '../../../re/export/modoc'
interface Context {
  shape: ConnectionShape
  params: FromJSONParams
}

function loadConnectionShape(
  op: PackedOpDelta,
  shape: ConnectionShape,
  rId: Nullable<string>,
  params: FromJSONParams,
  needCollectOps = false
) {
  if (rId != null && params.ignoreRId !== true) {
    shape.setRefId(rId)
  }

  const ctx: Context = { shape, params }
  const retOp = loadPackedDeltaOp(
    op,
    ConnectionShapeDataLayout,
    ConnectionShapeDataLayoutTypeMap,
    ctx,
    loadConnectionShapeDeltaSlice,
    needCollectOps
  )
  return retOp
}

const loadConnectionShapeDeltaSlice: deltaSliceLoader<
  typeof ConnectionShapeDataLayout,
  typeof ConnectionShapeDataLayoutTypeMap,
  Context
> = (opType, op, { shape }, _needCollectOps) => {
  switch (opType) {
    case 'nvCxnSpPr': {
      const nvCxnSpPrData = op.data['nvSpPr'] || {}
      const pr = readFromJSON(
        nvCxnSpPrData,
        UniNvPr,
        [shape],
        [SpTypes.ConnectionShape]
      )
      if (pr) {
        shape.setNvSpPr(pr)
        if (isNumber(pr.nvUniSpPr?.locks)) {
          setLocks(shape, pr.nvUniSpPr!.locks)
        }
        collectCxnSpToSpMapState(shape)
      }
      return undefined
    }
    case 'spPr': {
      const spPrData = op.data['spPr']
      const spPr = readFromJSON(spPrData, SpPr)
      if (spPr) {
        shape.setSpPr(spPr)
        spPr.setParent(shape)
      }
      return undefined
    }
    case 'style': {
      const style = readFromJSON(op.data, ShapeStyle)
      if (style) {
        shape.setStyle(style)
      }
      return undefined
    }
  }
}

export function genConnectionShape(
  data: PackedOpDelta,
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>,
  rId?: Nullable<string>,
  needCollectOps = false
): { sp: ConnectionShape; op: Nullable<OperationFromPackedOp<PackedOpDelta>> } {
  const shape = new ConnectionShape()
  shape.spTreeParent = spTreeParent
  setDeletedForSp(shape, false)
  if (mainObject) {
    setParentForSlideElement(shape, mainObject)
  }
  const op = loadConnectionShape(data, shape, rId, params, needCollectOps)
  return { sp: shape, op }
}
