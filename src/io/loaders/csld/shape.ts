import { Shape } from '../../../core/SlideElement/Shape'
import { FromJSONParams } from '../../defs/pptdoc'
import { ShapeDataLayout, ShapeDataLayoutTypeMap } from '../../defs/csld/shape'
import { Nullable } from '../../../../liber/pervasive'
import { GroupShape } from '../../../core/SlideElement/GroupShape'
import { readFromJSON } from '../../utils'
import { UniNvPr, SpPr } from '../../../core/SlideElement/attrs/shapePrs'
import { isNumber } from '../../../common/utils'
import { ShapeStyle } from '../../../core/SlideElement/attrs/ShapeStyle'
import { TextBody } from '../../../core/SlideElement/TextBody'
import { hasOwnKey } from '../../../../liber/hasOwnKey'
import { SpTypes } from '../../dataType/spAttrs'
import {
  clearSlideElementContent,
  setDeletedForSp,
  setParentForSlideElement,
} from '../../../core/utilities/shape/updates'
import { CSld } from '../../../core/Slide/CSld'
import { setLocks } from '../../../core/utilities/shape/locks'
import { deltaSliceLoader, loadPackedDeltaOp } from '../utils'
import { loadTextBody } from './txBody'
import { OperationFromPackedOp, PackedOpDelta } from '../../../re/export/modoc'
interface Context {
  shape: Shape
  params: FromJSONParams
}
function loadShape(
  op: PackedOpDelta,
  shape: Shape,
  rId: Nullable<string>,
  params: FromJSONParams,
  needCollectOps = false
) {
  if (rId != null && params.ignoreRId !== true) {
    shape.setRefId(rId)
  }

  const ctx: Context = { shape, params }
  const retOp = loadPackedDeltaOp(
    op,
    ShapeDataLayout,
    ShapeDataLayoutTypeMap,
    ctx,
    loadShapeDeltaSlice,
    needCollectOps
  )
  return retOp
}

const loadShapeDeltaSlice: deltaSliceLoader<
  typeof ShapeDataLayout,
  typeof ShapeDataLayoutTypeMap,
  Context
> = (opType, op, { shape, params }, needCollectOps) => {
  switch (opType) {
    case 'attrs': {
      if (hasOwnKey(op.data, 'useBgFill')) {
        shape.attrUseBgFill = op.data['useBgFill']
      }
      return undefined
    }
    case 'nvSpPr': {
      const nvSpPrData = op.data['nvSpPr'] || {}
      const pr = readFromJSON(nvSpPrData, UniNvPr, [shape], [SpTypes.Shape])
      if (pr) {
        shape.setNvSpPr(pr)
        if (isNumber(pr.nvUniSpPr?.locks)) {
          setLocks(shape, pr.nvUniSpPr!.locks)
        }
      }
      return undefined
    }
    case 'spPr': {
      const spPrData = op.data['spPr']
      const spPr = readFromJSON(spPrData, SpPr)
      if (spPr) {
        shape.setSpPr(spPr)
        spPr.setParent(shape)
      }
      return undefined
    }
    case 'style': {
      const style = readFromJSON(op.data, ShapeStyle)
      if (style) {
        shape.setStyle(style)
      }
      return undefined
    }
    case 'txBody': {
      clearSlideElementContent(shape)
      let txbody: TextBody

      if (shape) {
        if (shape.txBody) {
          txbody = shape.txBody
        } else {
          txbody = new TextBody(shape)
        }
      } else {
        txbody = new TextBody(shape)
      }

      const retOp = loadTextBody(op, txbody, params, needCollectOps)

      shape.setTxBody(txbody)
      return retOp
    }
  }
}

export function genShape(
  data: PackedOpDelta,
  params: FromJSONParams,
  spTreeParent: CSld | GroupShape,
  mainObject?: Nullable<any>,
  rId?: Nullable<string>,
  needCollectOps = false
): { sp: Shape; op: Nullable<OperationFromPackedOp<PackedOpDelta>> } {
  const shape = new Shape()
  shape.spTreeParent = spTreeParent
  setDeletedForSp(shape, false)
  if (mainObject) {
    setParentForSlideElement(shape, mainObject)
  }
  const op = loadShape(data, shape, rId, params, needCollectOps)
  return { sp: shape, op }
}
