import { CSld } from '../../../core/Slide/CSld'
import { FromJSONParams } from '../../defs/pptdoc'
import { CSldDataLayout, CSldDataLayoutTypeMap } from '../../defs/csld'
import { Nullable } from '../../../../liber/pervasive'
import { readFromJSON } from '../../utils'
import { Bg } from '../../../core/SlideElement/attrs/bg'

import { SlideElement } from '../../../core/SlideElement/type'
import { hasOwnKey } from '../../../../liber/hasOwnKey'
import { isHiddenSp, loadSpTree } from './groupShape'
import {
  clearCxnState,
  applyCxnSpToSpMapState,
} from '../../../core/utilities/shape/manageCxnSpState'
import { InstanceType, isInstanceTypeOf } from '../../../core/instanceTypes'
import { Slide } from '../../../core/Slide/Slide'
import { updateCalcStatusForSlide } from '../../../core/calculation/updateCalcStatus/slide'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { deltaSliceLoader, loadPackedDeltaOp } from '../utils'
import { PackedOpDelta } from '../../../re/export/modoc'
interface Context {
  csld: CSld
  mainObject: Nullable<any>
  params: FromJSONParams
  onApplySp?: (sp: SlideElement, rId: Nullable<string>) => void
}

export function loadCSld(
  op: PackedOpDelta,
  csld: CSld,
  mainObject: Nullable<any>,
  params: FromJSONParams,
  onApplySp?: (sp: SlideElement, rId: Nullable<string>) => void,
  needCollectOps = false
) {
  const ctx: Context = { csld, mainObject, params, onApplySp }

  const retOp = loadPackedDeltaOp(
    op,
    CSldDataLayout,
    CSldDataLayoutTypeMap,
    ctx,
    loadCSldDeltaSlice,
    needCollectOps
  )
  return retOp
}
const loadCSldDeltaSlice: deltaSliceLoader<
  typeof CSldDataLayout,
  typeof CSldDataLayoutTypeMap,
  Context
> = (opType, op, { csld, mainObject, params, onApplySp }, needCollectOps) => {
  switch (opType) {
    case 'attrs': {
      if (hasOwnKey(op.data, 'name')) {
        csld.name = op.data['name']
      }

      if (hasOwnKey(op.data, 'bg')) {
        // has key means need to update
        const bg = readFromJSON(op.data['bg'], Bg)
        csld.bg = bg
        if (isInstanceTypeOf(csld.parent, InstanceType.Slide)) {
          updateCalcStatusForSlide(
            csld.parent as Slide,
            EditActionFlag.edit_Slide_SetBg
          )
        }
      }
      return undefined
    }
    case 'spTree': {
      const updateSp = (sp: SlideElement, rId: Nullable<string>) => {
        if (sp && !isHiddenSp(sp)) {
          sp.spTreeParent = csld
          if (onApplySp) {
            onApplySp(sp, rId)
          }
        }
      }

      clearCxnState()

      const retOp = loadSpTree(
        csld,
        op,
        mainObject,
        params,
        updateSp,
        needCollectOps
      )

      // 替换 cxnSp 中 stCxnId、endCxnId 为 oo 中的 id
      applyCxnSpToSpMapState()
      return retOp
    }
  }
}
