import { Dict, Nullable } from '../../../liber/pervasive'
import { ScaleOfPPTXSizes } from '../../core/common/const/unit'
import { PresProps } from '../../core/Slide/PresProps'
import { Presentation } from '../../core/Slide/Presentation'
import { gGlobalTableStyles } from '../../core/Slide/GlobalTableStyles'
import { TextListStyle } from '../../core/textAttributes/TextListStyle'
import {
  CShowPrData,
  CTSectionLst,
  PresData,
  SlideRefData,
} from '../dataType/presentation'
import { TableStyleData } from '../dataType/style'
import {
  PresentationDataLayout,
  PresentationDataLayoutTypeMap,
} from '../defs/presentation'

import { fromJSON, readFromJSON } from '../utils'
import { ShowProps } from '../../core/Slide/ShowProps'
import { changeSlideSizeForPresentation } from '../../core/utilities/presentation/size'
import { TableStyle } from '../../core/Table/attrs/TableStyle'
import { deltaSliceLoader, loadPackedDeltaOp } from './utils'
import { PackedOpDelta } from '../../re/export/modoc'

export interface PresentationRefsInfo {
  slides: string[]
  slideMasters: string[]
  notes: string[]
  noteMasters: string[]
  prSectionList?: CTSectionLst | null
}

interface Context {
  presentation: Presentation
  refsInfo: PresentationRefsInfo
}

export function loadPresentationData(
  op: PackedOpDelta,
  presentation: Presentation,
  refsInfo: PresentationRefsInfo,
  needCollectOps = false
) {
  const ctx: Context = { presentation, refsInfo }
  const retOp = loadPackedDeltaOp(
    op,
    PresentationDataLayout,
    PresentationDataLayoutTypeMap,
    ctx,
    presentationDataLoader,
    needCollectOps
  )
  return retOp
}
const presentationDataLoader: deltaSliceLoader<
  typeof PresentationDataLayout,
  typeof PresentationDataLayoutTypeMap,
  Context
> = (opType, op, { presentation, refsInfo }, _needCollectOps) => {
  switch (opType) {
    case 'Pres': {
      const presData = op.data['pres'] as Nullable<PresData>
      const pres = readFromJSON(presData, PresProps)
      if (pres) {
        presentation.pres = pres
        presentation.defaultTextStyle = pres.defaultTextStyle as TextListStyle
        const sldSizeX = pres.slideSize.cx
        const sldSizeY = pres.slideSize.cy
        if (
          presentation.width !== sldSizeX ||
          presentation.height !== sldSizeY
        ) {
          if (sldSizeX) presentation.width = sldSizeX / ScaleOfPPTXSizes
          if (sldSizeY) presentation.height = sldSizeY / ScaleOfPPTXSizes
          changeSlideSizeForPresentation(
            presentation,
            presentation.width,
            presentation.height
          )
        }
      }
      return undefined
    }
    case 'PresentationPr': {
      const ShowPrData = op.data['showPr'] as Nullable<CShowPrData>
      const showPr = readFromJSON(ShowPrData, ShowProps)
      if (showPr) {
        presentation.showPr = showPr
      }

      const sectionLst = op.data['sectionLst'] as Nullable<CTSectionLst>
      if (sectionLst !== undefined) {
        refsInfo.prSectionList = sectionLst
      }
      return undefined
    }
    case 'TableStyle': {
      const defaultIdOfTableStyle = op.data['defaultTableStyleId'] as string

      const styles = op.data['styles'] as Nullable<Dict<TableStyleData>>

      let defaultId: string | null = null
      if (styles != null) {
        // reset tableStyle in use
        gGlobalTableStyles.updateTableStylesInUseMap({})
        Object.keys(styles).forEach((styleGUID) => {
          const dataOfStyle = styles[styleGUID]

          const style = new TableStyle('', styleGUID)
          fromJSON(style, dataOfStyle)

          gGlobalTableStyles.addTableStyle(style)

          // update tableStyle in use
          gGlobalTableStyles.setTableStylesInUseMap(style.id, styleGUID)

          if (style.guid === defaultIdOfTableStyle) {
            defaultId = style.id
          }
        })
      }

      if (defaultId != null) {
        gGlobalTableStyles.setDefaultTableStyleId(defaultIdOfTableStyle)
      }
      return undefined
    }
    case 'Slides': {
      const data = op.data as SlideRefData
      const slidesRefs = Object.keys(data)
        .filter((id) => !!data[id])
        .sort((rId1, rId2) => {
          const order1 = data[rId1].order ?? -1
          const order2 = data[rId2].order ?? -1
          return order1 !== order2 ? order1 - order2 : rId1 < rId2 ? -1 : 1
        })
      refsInfo.slides = slidesRefs
      return undefined
    }
    case 'SlideMaster': {
      if (op.data['slideMasterRefs']) {
        refsInfo.slideMasters = op.data['slideMasterRefs']
      } else {
        const data = op.data as SlideRefData
        const slideMasters = Object.keys(data)
          .filter((id) => !!data[id])
          .sort((rId1, rId2) => {
            const order1 = data[rId1].order ?? -1
            const order2 = data[rId2].order ?? -1
            return order1 !== order2 ? order1 - order2 : rId1 < rId2 ? -1 : 1
          })
        refsInfo.slideMasters = slideMasters
      }
      return undefined
    }
    case 'NotesMaster': {
      refsInfo.noteMasters = op.data['noteMasterRefs']
      return undefined
    }
  }
}
