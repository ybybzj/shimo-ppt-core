import { Nullable } from '../../../liber/pervasive'
import { FromJSONParams } from '../defs/pptdoc'

import { CThemeData } from '../dataType/theme'
import { readFromJSON, readFromJSONArray } from '../utils'
import { DefaultShapeAttributes } from '../../core/SlideElement/attrs/DefaultShapeAttributes'
import {
  Theme,
  ThemeElements,
  ExtraClrScheme,
} from '../../core/SlideElement/attrs/theme'
import {
  OperationFromPackedOp,
  PackedOpMap,
  StringSlice,
  deltaSliceToOperations,
} from '../../re/export/modoc'
import { firstItemOfDeltaSlice, getIterFromModocItemMap } from './utils'
import { Operation } from '../operation'

function loadTheme(
  ds: StringSlice,
  theme: Theme,
  rId: Nullable<string>,
  params: FromJSONParams,
  needCollectOps = false
): Nullable<Operation[]> {
  if (rId && params.ignoreRId !== true) theme.setRefId(rId)

  const dataItem = firstItemOfDeltaSlice(ds)
  if (dataItem && dataItem.type === 'json') {
    const themeData = dataItem.data as CThemeData
    const name = themeData['name']
    const themeElementsData = themeData['themeElements']
    const objectDefaultsData = themeData['objectDefaults']
    const extraClrSchemeLstData = themeData['extraClrSchemeLst']
    if (name != null) {
      theme.setName(name)
    }

    if (themeElementsData != null) {
      const themeElements = readFromJSON(themeElementsData, ThemeElements)
      if (themeElements) {
        theme.setFontScheme(themeElements.fontScheme)
        theme.setFormatScheme(themeElements.fmtScheme)
        theme.changeColorScheme(themeElements.clrScheme)
      }
    }

    if (objectDefaultsData != null) {
      const spDef = readFromJSON(
        objectDefaultsData['spDef'],
        DefaultShapeAttributes
      )
      if (spDef != null) {
        theme.setSpDef(spDef)
      }
      const lnDef = readFromJSON(
        objectDefaultsData['lnDef'],
        DefaultShapeAttributes
      )
      if (lnDef != null) {
        theme.setLnDef(lnDef)
      }
      const txDef = readFromJSON(
        objectDefaultsData['txDef'],
        DefaultShapeAttributes
      )
      if (txDef != null) {
        theme.setTxDef(txDef)
      }
    }

    if (extraClrSchemeLstData != null) {
      const extraClrSchemeLst = readFromJSONArray(
        extraClrSchemeLstData,
        ExtraClrScheme
      )?.filter(Boolean)

      if (extraClrSchemeLst) {
        theme.extraClrSchemeLst = extraClrSchemeLst as ExtraClrScheme[]
      }
    }
  }

  if (needCollectOps) {
    return deltaSliceToOperations(ds)
  }
}
export function genTheme(
  ds: StringSlice,
  rId: string,
  params: FromJSONParams,
  cb?: (rId: string, theme: Theme) => void,
  needCollectOps = false
): {
  theme: Theme
  ops: Nullable<Operation[]>
} {
  const theme = new Theme()
  theme.presentation = params.presentation
  const ops = loadTheme(ds, theme, rId, params, needCollectOps)
  if (cb) {
    cb(rId, theme)
  }
  return { theme, ops }
}

export function loadThemes(
  op: PackedOpMap,
  params: FromJSONParams,
  cb?: (rId: string, theme: Theme) => void,
  needCollectOps = false
) {
  const retOp: Nullable<OperationFromPackedOp<PackedOpMap>> = needCollectOps
    ? { action: op.action, data: { slide: {} }, attributes: op.attributes }
    : undefined
  const retOpDataMap:
    | OperationFromPackedOp<PackedOpMap>['data']['slide']
    | undefined = needCollectOps ? retOp!.data.slide : undefined
  const iter = getIterFromModocItemMap(op)
  while (iter.hasNext()) {
    const { id: rId, delta } = iter.next()
    const { ops } = genTheme(delta, rId, params, cb, needCollectOps)
    if (retOpDataMap != null) {
      retOpDataMap[rId] = ops ?? deltaSliceToOperations(delta)
    }
  }
  return retOp
}
