import { Paragraph } from '../../core/Paragraph/Paragraph'
import { TextDocument } from '../../core/TextDocument/TextDocument'
import {
  fromJSON,
  parseStringifiedJSON,
  StringifiedJSON,
  stripInvalidXMLCharacters,
} from '../utils'
import { TextPr } from '../../core/textAttributes/TextPr'
import { isFiniteNumber } from '../../common/utils'
import { ParaRun } from '../../core/Paragraph/ParaContent/ParaRun'

import { ParaHyperlink } from '../../core/Paragraph/ParaContent/ParaHyperlink'
import { ParaNewLine } from '../../core/Paragraph/RunElement/ParaNewLine'
import { InstanceType } from '../../core/instanceTypes'
import { ParaPr } from '../../core/textAttributes/ParaPr'
import { PresentationField } from '../../core/Paragraph/ParaContent/PresentationField'
import { ModocItem, lengthOfModocItem } from './utils'
import {
  getOpIterFromSlice,
  opFromPackedOperation,
  PackedOpNumber,
  StringSlice,
} from '../../re/export/modoc'
import { Operation } from '../operation'
import { cloneObj } from '../../re/io/ModocModel'

export function loadTextDocument(
  ds: StringSlice,
  textDoc: TextDocument,
  ops?: Operation[]
) {
  const result = loadParagraphs(ds, textDoc, ops)
  if (result.length > 0) {
    textDoc.children.clear()
  }

  result.forEach((paragraph) => {
    textDoc.insertParagraph(textDoc.children.length, paragraph)
  })
}

export function loadParagraphs(
  ds: StringSlice,
  textDoc?: TextDocument,
  ops?: Operation[]
) {
  const result: Paragraph[] = []

  let paragraph = Paragraph.new(textDoc)

  let index = 0

  const iter = getOpIterFromSlice(ds)
  const needCollectOps = ops != null

  while (iter.hasNext()) {
    const op = iter.next()

    if (needCollectOps) {
      ops.push(opFromPackedOperation(op))
    }

    if (!isSupportedOp(op)) {
      continue
    }
    const attributes = cloneObj(op.attributes || {})
    // 暂时只处理insert
    const attrs = parseStringifiedJSON(attributes as StringifiedJSON)
    if (op.type === 'number' && attrs['paraEnd']) {
      const rPr = attrs['rPr']
      const pPr = attrs['pPr']
      const textPr = attrs['textPr']

      Array.from({ length: lengthOfModocItem(op) }, () => {
        if (pPr) {
          fromJSON(paragraph.pr, pPr)
          paragraph.setPr(paragraph.pr)
        }

        if (textPr) {
          fromJSON(paragraph.paraTextPr.textPr, textPr)
          paragraph.paraTextPr.fromTextPrData(paragraph.paraTextPr.textPr)
        }

        if (rPr) {
          const runPrOfEndParaRun = TextPr.new()
          fromJSON(runPrOfEndParaRun, rPr)
          if (
            runPrOfEndParaRun.fillEffects &&
            !runPrOfEndParaRun.fillEffects.fill
          ) {
            runPrOfEndParaRun.fillEffects = undefined
          }

          paragraph.paraTextPr.fromTextPrData(runPrOfEndParaRun) // endRunProperties

          const endParaRun = paragraph.children.at(
            paragraph.children.length - 1
          )
          if (endParaRun && endParaRun.instanceType === InstanceType.ParaRun) {
            endParaRun.pr = runPrOfEndParaRun
          }
        }

        result.push(paragraph)
        paragraph = Paragraph.new(textDoc)
      })
      index = 0
    } else if (op.type === 'number' && attrs['paraField'] === true) {
      index = loadParaFieldContent(op, paragraph, index)
    } else {
      loadParaRunContent(op, paragraph, index++)
    }
  }

  return result
}

function loadParaRunContent(
  op: ModocItem,
  paragraph: Paragraph,
  index: number
) {
  const attributes = cloneObj(op.attributes || {})
  const attrs = parseStringifiedJSON(attributes as StringifiedJSON)
  const isHyperLink =
    null != attrs['hlinkClick'] || null != attrs['hlinkMouseOver']

  const rPr = TextPr.new()
  fromJSON(rPr, isHyperLink ? { ...attrs, ['underline']: 'sng' } : attrs)
  let hyperlink: ParaHyperlink | undefined
  if (isHyperLink) {
    hyperlink = ParaHyperlink.new(paragraph)
    if (rPr.hlink != null) {
      hyperlink.fromCTHyperlink(rPr.hlink)
    }
  }

  const newRun = ParaRun.new(paragraph, rPr)
  TextPr.recycle(rPr)

  if (op.type === 'string') {
    const text = stripInvalidXMLCharacters(op.data)
    newRun.insertText(text)
  } else if (attrs['break'] === true) {
    const breakNum = isFiniteNumber(op.data) ? (op.data as number) : 1
    for (let i = 0; i < breakNum; i++) {
      newRun.addItemAt(0, ParaNewLine.new())
    }
  }

  if (hyperlink != null) {
    hyperlink.addItemAt(0, newRun, false)
    paragraph.addItemAt(index, hyperlink)
  } else {
    paragraph.addItemAt(index, newRun)
  }
}

function loadParaFieldContent(
  op: PackedOpNumber,
  paragraph: Paragraph,
  index: number
): number {
  const attributes = cloneObj(op.attributes || {})
  const attrs = parseStringifiedJSON(attributes as StringifiedJSON)
  const fld = new PresentationField(paragraph)
  const { id, type, text } = attrs

  if (id != null) {
    fld.setGuid(id)
  }
  if (type != null) {
    fld.setFieldType(type)
  }
  if (text != null) {
    fld.insertText(text)
  }

  if (attrs['rPr']) {
    const rPr = TextPr.new()
    fromJSON(rPr, attrs['rPr'])
    fld.fromTextPr(rPr)
  }

  if (attrs['pPr']) {
    const pPr = ParaPr.new()
    fromJSON(pPr, attrs['pPr'])
    fld.setParaPr(pPr)
  }

  paragraph.addItemAt(index++, ParaRun.new(paragraph))
  paragraph.addItemAt(index++, fld)
  paragraph.addItemAt(index++, ParaRun.new(paragraph))

  return index
}

// helpers
function isSupportedOp(item: ModocItem): boolean {
  return (
    lengthOfModocItem(item) > 0 &&
    (item.type === 'string' ||
      item.type === 'delta' ||
      (item.type === 'number' &&
        !!item.attributes &&
        (item.attributes.paraEnd === true ||
          item.attributes.break === true ||
          item.attributes.paraField === true)))
  )
}
