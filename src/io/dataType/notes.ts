import { ColorMappingOverride } from './spAttrs'
import { InsertOperationWithType, JSONOpData, DeltaOpData } from '../operation'
import { CSldData } from './csld'

export interface NotesAttrs {
  showMasterSp?: boolean
  showMasterPhAnim?: boolean
  clrMap?: ColorMappingOverride
  notesMasterRef?: string
}
export type NotesData = [
  InsertOperationWithType<'attrs', JSONOpData<NotesAttrs>>,
  InsertOperationWithType<'csld', DeltaOpData<CSldData>>
]
