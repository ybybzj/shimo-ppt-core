import {
  DeltaOpData,
  EmptyMapData,
  InsertOperationWithType,
  MapOpData,
} from '../operation'
import { CommentsData } from './comment'
import { NotesData } from './notes'
import { NotesMasterData } from './notesMaster'
import { PresentationData } from './presentation'
import { SlideData } from './slide'
import { SlideLayoutData } from './slideLayout'
import { SlideMasterData } from './slideMaster'
import { ThemeData } from './theme'

export type PPTDocData = [
  InsertOperationWithType<'presentation', DeltaOpData<PresentationData>>,
  InsertOperationWithType<'layouts', MapOpData<SlideLayoutData>>, // rid的前缀 l
  InsertOperationWithType<'slideMasters', MapOpData<SlideMasterData>>, // rid的前缀 sm
  InsertOperationWithType<'slides', MapOpData<SlideData>>, // rid的前缀 s
  InsertOperationWithType<'themes', MapOpData<ThemeData> | EmptyMapData>, // rid的前缀 th
  InsertOperationWithType<
    'notesMasters',
    MapOpData<NotesMasterData> | EmptyMapData
  >, // rid的前缀 nm
  InsertOperationWithType<'notes', MapOpData<NotesData> | EmptyMapData>, // rid的前缀 n
  InsertOperationWithType<'comments', MapOpData<CommentsData> | EmptyMapData> // rid的前缀 cm
]
