import { Dict, Nullable } from '../../../liber/pervasive'
import { InsertOperationWithType, JSONOpData } from '../operation'
import { CommentsAuthorData } from './comment'
import { ComplexColorData } from './spAttrs'
import { TableStyleData, TextListStyleData } from './style'

export interface CCoreData {
  title?: string
  creator?: string
  lastModifiedBy?: string
  revision?: string
  created?: string
  modified?: string
}

export interface CAppData {
  Template?: string
  TotalTime?: number
  Words?: number
  Application?: string
  PresentationFormat?: string
  Paragraphs?: number
  Slides?: number
  Notes?: number
  HiddenSlides?: number
  MMClips?: number
  ScaleCrop?: boolean
  Company?: string
  LinksUpToDate?: boolean
  SharedDoc?: boolean
  HyperlinksChanged?: boolean
  AppVersion?: string
}

type SlideSizeType =
  | 'mm35'
  | 'a3'
  | 'a4'
  | 'b4ISO'
  | 'b4JIS'
  | 'b5ISO'
  | 'b5JIS'
  | 'banner'
  | 'custom'
  | 'hagakiCard'
  | 'ledger'
  | 'letter'
  | 'overhead'
  | 'screen16x10'
  | 'screen16x9'
  | 'screen4x3'

type AttrPrefix<D extends Dict> = {
  [K in keyof D as `attr${Capitalize<K & string>}`]: D[K]
}

export type PresData = AttrPrefix<{
  autoCompressPictures?: boolean
  bookmarkIdSeed?: number
  compatMode?: boolean
  embedTrueTypeFonts?: boolean
  firstSlideNum?: number
  removePersonalInfoOnSave?: boolean
  rtl?: boolean
  saveSubsetFonts?: boolean
  serverZoom?: string
  showSpecialPlsOnTitleSld?: boolean
  strictFirstAndLastChars?: boolean
}> & {
  defaultTextStyle?: TextListStyleData
  sldSz?: { cx?: number; cy?: number; type?: SlideSizeType }
  notesSz?: { cx?: number; cy?: number }
  commentsAuthors?: CommentsAuthorData[]
}

export interface CShowPrData {
  browse?: boolean
  kiosk?: {
    restart: number
  }
  penClr?: ComplexColorData
  present?: boolean
  show?: {
    showAll: boolean
    range?: {
      start: number
      end: number
    }
    custShow?: number
  }
  loop?: boolean
  showAnimation?: boolean
  showNarration?: boolean
  useTimings?: boolean
}

export interface GlobalTableStylesData {
  defaultTableStyleId?: Nullable<string>
  styles?: null | Dict<TableStyleData>
}

export type SlideRefData = Dict<{
  order?: number
}>

export type SlideMasterRefData = Dict<{
  order?: number
} | null>

export interface NotesMasterRefData {
  noteMasterRefs: string[]
}

export interface CTSection {
  name: string
  id: string // guid
  sldLst: string[] // refId[]
}

export type CTSectionLst = CTSection[]

export interface PresentationPrData {
  showPr: CShowPrData
  sectionLst?: CTSectionLst
}
export type PresentationData = [
  InsertOperationWithType<'Pres', JSONOpData<{ pres: PresData }>>,
  InsertOperationWithType<'PresentationPr', JSONOpData<PresentationPrData>>,
  InsertOperationWithType<'TableStyle', JSONOpData<GlobalTableStylesData>>,
  InsertOperationWithType<'Slides', JSONOpData<SlideRefData>>,
  InsertOperationWithType<'SlideMaster', JSONOpData<SlideMasterRefData>>,
  InsertOperationWithType<'Notes', 1>,
  InsertOperationWithType<'NotesMaster', JSONOpData<NotesMasterRefData>>
]
