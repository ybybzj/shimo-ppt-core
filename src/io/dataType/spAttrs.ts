/* eslint-disable no-redeclare */
import { Dict, valuesOfDict } from '../../../liber/pervasive'
import {
  ColorType,
  FillKIND,
  phSZType,
  bodyPrAnchor,
  DashStyle,
  BWModeKind,
  FillGradPathType,
  PresetPatternValToNumMap,
  PresetPatternNumToValMap,
} from '../../core/common/const/attrs'
import {
  TextAutoFitType,
  BulletTypeValue,
  BulletTypefaceType,
  BulletSizeType,
  BulletColorType,
  PlaceholderType,
  LineJoinType,
  LineEndType,
  TextOverflowType,
  TextVerticalType,
  TextWarpType,
  ShapeLocks,
  FontStyleInd,
  PresetColorValues,
  FontStyleIndValues,
} from '../../core/SlideElement/const'

import {
  numberingFormatValueToStr,
  strToNumberingFormatValue,
} from '../../core/common/numberingFormat'
import { GeometryData } from './geometry'
export interface SysColorData {
  type: typeof ColorType.SYS // 4
  val: string
  lastClr: string
}

export interface PrstColorData {
  type: typeof ColorType.PRST // 2
  val?: PresetColorValues
}

export interface RGBColorData {
  type: typeof ColorType.SRGB // 1
  val: string
}

export type ColorSchemeIndex =
  | 'dk1'
  | 'lt1'
  | 'dk2'
  | 'lt2'
  | 'accent1'
  | 'accent2'
  | 'accent3'
  | 'accent4'
  | 'accent5'
  | 'accent6'
  | 'hlink'
  | 'folHlink'

export const ColorSchemeIndex = [
  'dk1',
  'lt1',
  'dk2',
  'lt2',
  'accent1',
  'accent2',
  'accent3',
  'accent4',
  'accent5',
  'accent6',
  'hlink',
  'folHlink',
] as const

export type ColorMappingIndex =
  | 'bg1'
  | 'tx1'
  | 'bg2'
  | 'tx2'
  | 'accent1'
  | 'accent2'
  | 'accent3'
  | 'accent4'
  | 'accent5'
  | 'accent6'
  | 'hlink'
  | 'folHlink'
export const ColorMappingIndex = [
  'bg1',
  'tx1',
  'bg2',
  'tx2',
  'accent1',
  'accent2',
  'accent3',
  'accent4',
  'accent5',
  'accent6',
  'hlink',
  'folHlink',
] as const
export type ClrMapData = {
  [k in ColorMappingIndex]?: ColorSchemeIndex
}
export type ColorMappingOverride =
  | { type: 'useMaster' }
  | { type: 'override'; data: ClrMapData }
  | undefined

export type ClrMapOOData = Dict<number>

export type SchemeColorType = ColorSchemeIndex | ColorMappingIndex | 'phClr'

export interface SchemeColorData {
  type: typeof ColorType.SCHEME // 3
  val: SchemeColorType
}

export const ThemeColor = {
  Accent1: 0,
  Accent2: 1,
  Accent3: 2,
  Accent4: 3,
  Accent5: 4,
  Accent6: 5,
  Background1: 6,
  Background2: 7,
  Dark1: 8,
  Dark2: 9,
  FollowedHyperlink: 10,
  Hyperlink: 11,
  Light1: 12,
  Light2: 13,
  None: 14,
  Text1: 15,
  Text2: 16,
} as const

export const ThemeColorCount = Object.keys(ThemeColor).length
export type ThemeColorValue = valuesOfDict<typeof ThemeColor>

export function toSchemeColorType(v: number): SchemeColorType {
  switch (v) {
    case ThemeColor.Background1:
      return 'bg1'
    case ThemeColor.Background2:
      return 'bg2'
    case ThemeColor.Light1:
      return 'lt1'
    case ThemeColor.Light2:
      return 'lt2'
    case ThemeColor.Dark1:
      return 'dk1'
    case ThemeColor.Dark2:
      return 'dk2'
    case ThemeColor.Text1:
      return 'tx1'
    case ThemeColor.Text2:
      return 'tx2'
    case ThemeColor.Hyperlink:
      return 'hlink'
    case ThemeColor.FollowedHyperlink:
      return 'folHlink'
    case ThemeColor.Accent1:
      return 'accent1'
    case ThemeColor.Accent2:
      return 'accent2'
    case ThemeColor.Accent3:
      return 'accent3'
    case ThemeColor.Accent4:
      return 'accent4'
    case ThemeColor.Accent5:
      return 'accent5'
    case ThemeColor.Accent6:
      return 'accent6'
    case ThemeColor.None:
      return 'phClr'
    default:
      return 'accent1'
  }
}

export function fromSchemeColorType(v: SchemeColorType): ThemeColorValue {
  switch (v) {
    case 'lt1':
      return ThemeColor.Light1
    case 'lt2':
      return ThemeColor.Light2
    case 'bg1':
      return ThemeColor.Background1
    case 'bg2':
      return ThemeColor.Background2
    case 'dk1':
      return ThemeColor.Dark1
    case 'dk2':
      return ThemeColor.Dark2
    case 'tx1':
      return ThemeColor.Text1
    case 'tx2':
      return ThemeColor.Text2
    case 'hlink':
      return ThemeColor.Hyperlink
    case 'folHlink':
      return ThemeColor.FollowedHyperlink
    case 'accent1':
      return ThemeColor.Accent1
    case 'accent2':
      return ThemeColor.Accent2
    case 'accent3':
      return ThemeColor.Accent3
    case 'accent4':
      return ThemeColor.Accent4
    case 'accent5':
      return ThemeColor.Accent5
    case 'accent6':
      return ThemeColor.Accent6
    case 'phClr':
      return ThemeColor.None
    default:
      return ThemeColor.None
  }
}

export function fromClrMapData(map: ClrMapData): ClrMapOOData {
  const result: ClrMapOOData = {}
  Object.keys(map).forEach((mappingIndex) => {
    const key = fromSchemeColorType(mappingIndex as ColorMappingIndex)
    const val = fromSchemeColorType(map[mappingIndex])
    result[key] = val
  })

  return result
}

export function toClrMapData(map: ClrMapOOData): ClrMapData {
  const result: ClrMapData = {}
  Object.keys(map).forEach((key) => {
    const mappingIndex = toSchemeColorType(Number(key))
    const schemeIndex = toSchemeColorType(map[key])
    result[mappingIndex] = schemeIndex
  })

  return result
}

export interface ComplexColorData {
  color?: SysColorData | PrstColorData | RGBColorData | SchemeColorData
  mods?: ColorModData[]
}

export interface CSrcRectData {
  l?: number
  t?: number
  r?: number
  b?: number
}

export type TileFlipMode = 'none' | 'x' | 'y' | 'xy'
export type RectAlignment =
  | 'tl'
  | 't'
  | 'tr'
  | 'l'
  | 'ctr'
  | 'r'
  | 'bl'
  | 'b'
  | 'br'
export interface CBlipFillTileData {
  tx?: number
  ty?: number
  sx?: number
  sy?: number
  flip?: TileFlipMode
  algn?: RectAlignment
}

export function toTileFlipMode(n: number): TileFlipMode | undefined {
  switch (n) {
    case 0:
      return 'none'
    case 1:
      return 'x'
    case 2:
      return 'y'
    case 3:
      return 'xy'
    default:
      return void 0
  }
}

export function fromTileFlipMode(t: TileFlipMode): number | undefined {
  switch (t) {
    case 'none':
      return 0
    case 'x':
      return 1
    case 'y':
      return 2
    case 'xy':
      return 3
  }
}

export function toRectAlignment(n: number): RectAlignment | undefined {
  switch (n) {
    case 0:
      return 'b'
    case 1:
      return 'bl'
    case 2:
      return 'br'
    case 3:
      return 'ctr'
    case 4:
      return 'l'
    case 5:
      return 'r'
    case 6:
      return 't'
    case 7:
      return 'tl'
    case 8:
      return 'tr'
  }
}

export function fromRectAlignment(t: RectAlignment): number | undefined {
  switch (t) {
    case 'b':
      return 0
    case 'bl':
      return 1
    case 'br':
      return 2
    case 'ctr':
      return 3
    case 'l':
      return 4
    case 'r':
      return 5
    case 't':
      return 6
    case 'tl':
      return 7
    case 'tr':
      return 8
  }
}
declare type BilpEffectType =
  | 'alphaBiLevel'
  | 'alphaCeiling'
  | 'alphaFloor'
  | 'alphaInv'
  | 'alphaMod'
  | 'alphaModFix'
  | 'alphaRepl'
  | 'biLevel'
  | 'blur'
  | 'clrChange'
  | 'clrRepl'
  | 'duotone'
  | 'fillOverlay'
  | 'grayscl'
  | 'hsl'
  | 'lum'
  | 'tint'

export interface CBlipEffectData {
  name: BilpEffectType
  val: number
}

export interface CBlipFillData {
  rasterImageId: string
  encryptUrl?: string
  srcRect?: CSrcRectData
  fillRect?: CSrcRectData
  stretch?: boolean
  tile?: CBlipFillTileData
  rotWithShape?: boolean
  effects?: CBlipEffectData[]
}

export interface CSolidFillData {
  color?: ComplexColorData
}

export interface CGsData {
  color?: ComplexColorData
  pos: number
}

export interface GradLinData {
  angle: number
  scale: boolean
}

export type GradPathType = valuesOfDict<typeof FillGradPathType>

export interface GradPathData {
  path: GradPathType
  rect?: CSrcRectData
}

export type ST_TileFlipMode = 'none' | 'x' | 'y' | 'xy'
export interface CGradFillData {
  colors?: CGsData[]
  lin?: GradLinData
  path?: GradPathData
  tileRect?: CSrcRectData
  flip?: ST_TileFlipMode
  rotWithShape?: boolean
}

type PresetPatterns = typeof PresetPatternValToNumMap
export type PresetPatternVal = keyof PresetPatterns
export type PresetPatternNumVal = PresetPatterns[PresetPatternVal]
export interface CPatternFillData {
  ftype: PresetPatternVal
  fgClr?: ComplexColorData
  bgClr?: ComplexColorData
}

export function fromPresetPatternVal(k: PresetPatternVal): PresetPatternNumVal {
  return PresetPatternValToNumMap[k]
}

export function toPresetPatternVal(t: PresetPatternNumVal): PresetPatternVal {
  return PresetPatternNumToValMap[t]
}

export type FillTypeData =
  | {
      type: typeof FillKIND.BLIP
      data: CBlipFillData
    }
  | {
      type: typeof FillKIND.NOFILL
    }
  | {
      type: typeof FillKIND.SOLID
      data: CSolidFillData
    }
  | {
      type: typeof FillKIND.GRAD
      data: CGradFillData
    }
  | {
      type: typeof FillKIND.PATT
      data: CPatternFillData
    }
  | {
      type: typeof FillKIND.GRP
    }

export interface CFillEffectsData {
  fill?: FillTypeData
}

export type LineEndType =
  | 'none'
  | 'triangle'
  | 'stealth'
  | 'diamond'
  | 'oval'
  | 'arrow'
export type LineEndTypeValues =
  | typeof LineEndType.None
  | typeof LineEndType.Arrow
  | typeof LineEndType.Diamond
  | typeof LineEndType.Oval
  | typeof LineEndType.Stealth
  | typeof LineEndType.Triangle
export interface EndArrowData {
  type?: LineEndType

  len?: number
  w?: number
}

export const EffectType = {
  NONE: 0,
  OUTERSHDW: 1,
  GLOW: 2,
  DUOTONE: 3,
  XFRM: 4,
  BLUR: 5,
  PRSTSHDW: 6,
  INNERSHDW: 7,
  REFLECTION: 8,
  SOFTEDGE: 9,
  FILLOVERLAY: 10,
  ALPHACEILING: 11,
  ALPHAFLOOR: 12,
  TINTEFFECT: 13,
  RELOFF: 14,
  LUM: 15,
  HSL: 16,
  GRAYSCL: 17,
  ELEMENT: 18,
  ALPHAREPL: 19,
  ALPHAOUTSET: 20,
  ALPHAMODFIX: 21,
  ALPHABILEVEL: 22,
  BILEVEL: 23,
  DAG: 24,
  FILL: 25,
  CLRREPL: 26,
  CLRCHANGE: 27,
  ALPHAINV: 28,
  ALPHAMOD: 29,
  BLEND: 30,
} as const

export type EffectTypeValues = valuesOfDict<typeof EffectType>
export function fromEffectType(name: string) {
  switch (name) {
    case 'alphaModFix':
      return EffectType.ALPHAMODFIX
    default:
      break
  }
}

export function toEffectType(type) {
  switch (type) {
    case EffectType.ALPHAMODFIX:
      return 'alphaModFix'
    default:
      break
  }
}

export function toLineEndType(n: LineEndTypeValues): LineEndType {
  switch (n) {
    case LineEndType.None:
      return 'none'
    case LineEndType.Arrow:
      return 'arrow'
    case LineEndType.Diamond:
      return 'diamond'
    case LineEndType.Oval:
      return 'oval'
    case LineEndType.Stealth:
      return 'stealth'
    case LineEndType.Triangle:
      return 'triangle'
  }
}

export function fromLineEndType(t: LineEndType): LineEndTypeValues {
  switch (t) {
    case 'none':
      return LineEndType.None
    case 'triangle':
      return LineEndType.Triangle
    case 'stealth':
      return LineEndType.Stealth
    case 'diamond':
      return LineEndType.Diamond
    case 'oval':
      return LineEndType.Oval
    case 'arrow':
      return LineEndType.Arrow
  }
}

export type LineJoinType = 'round' | 'bevel' | 'miter'
export type LineJoinTypeOO =
  | typeof LineJoinType.Empty
  | typeof LineJoinType.Round
  | typeof LineJoinType.Bevel
  | typeof LineJoinType.Miter
export interface LineJoinData {
  type?: LineJoinType

  limit?: boolean
}

export function toLineJoinType(n: LineJoinTypeOO): LineJoinType | undefined {
  switch (n) {
    case LineJoinType.Round:
      return 'round'
    case LineJoinType.Bevel:
      return 'bevel'
    case LineJoinType.Miter:
      return 'miter'
    default:
      return void 0
  }
}

export function fromLineJoinType(t: LineJoinType): LineJoinTypeOO {
  switch (t) {
    case 'round':
      return LineJoinType.Round
    case 'bevel':
      return LineJoinType.Bevel
    case 'miter':
      return LineJoinType.Miter
    default:
      return LineJoinType.Empty
  }
}

export type PresetLineDashStyle =
  | 'solid'
  | 'dot'
  | 'dash'
  | 'lgDash'
  | 'dashDot'
  | 'lgDashDot'
  | 'lgDashDotDot'
  | 'sysDash'
  | 'sysDot'
  | 'sysDashDot'
  | 'sysDashDotDot'

export type PresetLineDashStyleValues =
  | typeof DashStyle.dash
  | typeof DashStyle.dashDot
  | typeof DashStyle.dot
  | typeof DashStyle.lgDash
  | typeof DashStyle.lgDashDot
  | typeof DashStyle.lgDashDotDot
  | typeof DashStyle.solid
  | typeof DashStyle.sysDash
  | typeof DashStyle.sysDashDot
  | typeof DashStyle.sysDashDotDot
  | typeof DashStyle.sysDot

export type PenAlignment = 'ctr' | 'in'
export type CompoundLine = 'sng' | 'dbl' | 'thickThin' | 'thinThick' | 'tri'
export type LineCap = 'rnd' | 'sq' | 'flat'
export interface CLnData {
  fill?: CFillEffectsData
  prstDash?: PresetLineDashStyle
  join?: LineJoinData
  headEnd?: EndArrowData
  tailEnd?: EndArrowData
  algn?: PenAlignment
  cap?: LineCap
  cmpd?: CompoundLine
  w?: number
}

export function toPenAlignment(n: number): PenAlignment | undefined {
  switch (n) {
    case 0:
      return 'ctr'
    case 1:
      return 'in'
  }
}

export function fromPenAlignment(t: PenAlignment): number | undefined {
  switch (t) {
    case 'ctr':
      return 0
    case 'in':
      return 1
  }
}

export function toCompoundLine(n: number): CompoundLine | undefined {
  switch (n) {
    case 0:
      return 'dbl'
    case 1:
      return 'sng'
    case 2:
      return 'thickThin'
    case 3:
      return 'thinThick'
    case 4:
      return 'tri'
  }
}

export function fromCompoundLine(t: CompoundLine): number | undefined {
  switch (t) {
    case 'dbl':
      return 0
    case 'sng':
      return 1
    case 'thickThin':
      return 2
    case 'thinThick':
      return 3
    case 'tri':
      return 4
  }
}

export function toLineCap(n: number): LineCap | undefined {
  switch (n) {
    case 0:
      return 'flat'
    case 1:
      return 'rnd'
    case 2:
      return 'sq'
  }
}

export function fromLineCap(t: LineCap): number | undefined {
  switch (t) {
    case 'flat':
      return 0
    case 'rnd':
      return 1
    case 'sq':
      return 2
  }
}

export function toPresetLineDashVal(
  n: PresetLineDashStyleValues
): PresetLineDashStyle | undefined {
  switch (n) {
    case DashStyle.dash:
      return 'dash'
    case DashStyle.dashDot:
      return 'dashDot'
    case DashStyle.dot:
      return 'dot'
    case DashStyle.lgDash:
      return 'lgDash'
    case DashStyle.lgDashDot:
      return 'lgDashDot'
    case DashStyle.lgDashDotDot:
      return 'lgDashDotDot'
    case DashStyle.solid:
      return 'solid'
    case DashStyle.sysDash:
      return 'sysDash'
    case DashStyle.sysDashDot:
      return 'sysDashDot'
    case DashStyle.sysDashDotDot:
      return 'sysDashDotDot'
    case DashStyle.sysDot:
      return 'sysDot'
  }
}

export function fromPresetLineDashVal(
  t: PresetLineDashStyle
): PresetLineDashStyleValues {
  switch (t) {
    case 'dash':
      return DashStyle.dash
    case 'dashDot':
      return DashStyle.dashDot
    case 'dot':
      return DashStyle.dot
    case 'lgDash':
      return DashStyle.lgDash
    case 'lgDashDot':
      return DashStyle.lgDashDot
    case 'lgDashDotDot':
      return DashStyle.lgDashDotDot
    case 'solid':
      return DashStyle.solid
    case 'sysDash':
      return DashStyle.sysDash
    case 'sysDashDot':
      return DashStyle.sysDashDot
    case 'sysDashDotDot':
      return DashStyle.sysDashDotDot
    case 'sysDot':
      return DashStyle.sysDot
  }
}

export interface StyleRefData {
  idx: number
  color?: ComplexColorData
}

export interface FontRefData {
  idx: string
  color?: ComplexColorData
}

export function fromFontRefIdx(idx: string): FontStyleIndValues {
  switch (idx) {
    case 'major':
      return FontStyleInd.major
    case 'minor':
      return FontStyleInd.minor
    case 'none':
    default:
      return FontStyleInd.none
  }
}

export function toFontRefIdx(idx: number): string {
  switch (idx) {
    case FontStyleInd.major:
      return 'major'
    case FontStyleInd.minor:
      return 'minor'
    case FontStyleInd.none:
    default:
      return 'none'
  }
}

export interface CXfrmData {
  offX?: number
  offY?: number
  extX?: number
  extY?: number
  chOffX?: number
  chOffY?: number
  chExtX?: number
  chExtY?: number
  flipH?: boolean
  flipV?: boolean
  rot?: number
}

type BwMode =
  | 'auto'
  | 'clr'
  | 'gray'
  | 'ltGray'
  | 'invGray'
  | 'grayWhite'
  | 'blackGray'
  | 'blackWhite'
  | 'black'
  | 'white'
  | 'hidden'

export type BwModeKindValue = (typeof BWModeKind)[keyof typeof BWModeKind]

export function toBwMode(bm: BwModeKindValue): BwMode | undefined {
  switch (bm) {
    case BWModeKind.auto:
      return 'auto'
    case BWModeKind.clr:
      return 'clr'
    case BWModeKind.gray:
      return 'gray'
    case BWModeKind.ltGray:
      return 'ltGray'
    case BWModeKind.invGray:
      return 'invGray'
    case BWModeKind.grayWhite:
      return 'grayWhite'
    case BWModeKind.blackGray:
      return 'blackGray'
    case BWModeKind.blackWhite:
      return 'blackWhite'
    case BWModeKind.black:
      return 'black'
    case BWModeKind.white:
      return 'white'
    case BWModeKind.hidden:
      return 'hidden'
  }
}

export function fromBwMode(t: BwMode): BwModeKindValue {
  switch (t) {
    case 'auto':
      return BWModeKind.auto
    case 'clr':
      return BWModeKind.clr
    case 'gray':
      return BWModeKind.gray
    case 'ltGray':
      return BWModeKind.ltGray
    case 'invGray':
      return BWModeKind.invGray
    case 'grayWhite':
      return BWModeKind.grayWhite
    case 'blackGray':
      return BWModeKind.blackGray
    case 'blackWhite':
      return BWModeKind.blackWhite
    case 'black':
      return BWModeKind.black
    case 'white':
      return BWModeKind.white
    case 'hidden':
      return BWModeKind.hidden
  }
}
export interface CSpPrData {
  bwMode?: BwMode
  xfrm?: CXfrmData
  geometry?: GeometryData
  fill?: CFillEffectsData
  ln?: CLnData
}

export interface CGrpSpPrData {
  bwMode?: BwMode
  xfrm?: CXfrmData
  fill?: CFillEffectsData
}

export type BulletColorType = 'followText' | 'color'
export type BulletColorTypeValues = valuesOfDict<typeof BulletColorType>

export interface BulletColorData {
  type?: BulletColorType
  uniColor?: ComplexColorData
}

export function toBulletColorType(
  n: BulletColorTypeValues
): BulletColorType | undefined {
  switch (n) {
    case BulletColorType.CLRTX:
      return 'followText'
    case BulletColorType.CLR:
      return 'color'
    default:
      return void 0
  }
}

export function fromBulletColorType(t: BulletColorType): BulletColorTypeValues {
  switch (t) {
    case 'followText':
      return BulletColorType.CLRTX
    case 'color':
      return BulletColorType.CLR
    default:
      return BulletColorType.NONE
  }
}

export type BulletSizeType = 'followText' | 'percent' | 'point'
export type BulletSizeTypeValues = valuesOfDict<typeof BulletSizeType>

export interface CBulletSizeData {
  type?: BulletSizeType
  val?: number
}

export function toBulletSizeType(
  n: BulletSizeTypeValues
): BulletSizeType | undefined {
  switch (n) {
    case BulletSizeType.TX:
      return 'followText'
    case BulletSizeType.PCT:
      return 'percent'
    case BulletSizeType.PTS:
      return 'point'
    default:
      return void 0
  }
}

export function fromBulletSizeType(t: BulletSizeType): BulletSizeTypeValues {
  switch (t) {
    case 'followText':
      return BulletSizeType.TX
    case 'percent':
      return BulletSizeType.PCT
    case 'point':
      return BulletSizeType.PTS
    default:
      return BulletSizeType.NONE
  }
}
export type BulletTypefaceType = 'followText' | 'font'
export type BulletTypefaceTypeValues = valuesOfDict<typeof BulletTypefaceType>

export interface CBulletTypefaceData {
  typeface?: string
  type?: BulletTypefaceType
}

export function toBulletTypefaceType(
  n: BulletTypefaceTypeValues
): BulletTypefaceType | undefined {
  switch (n) {
    case BulletTypefaceType.TX:
      return 'followText'
    case BulletTypefaceType.BUFONT:
      return 'font'
    default:
      return void 0
  }
}

export function fromBulletTypefaceType(
  t: BulletTypefaceType
): BulletTypefaceTypeValues {
  switch (t) {
    case 'followText':
      return BulletTypefaceType.TX
    case 'font':
      return BulletTypefaceType.BUFONT
    default:
      return BulletTypefaceType.NONE
  }
}

export type BulletType = 'none' | 'autoNumbered' | 'character' | 'picture'
export type BulletTypeValues = valuesOfDict<typeof BulletTypeValue>

export type AutoNumScheme =
  | 'alphaLcParenBoth'
  | 'alphaUcParenBoth'
  | 'alphaLcParenR'
  | 'alphaUcParenR'
  | 'alphaLcPeriod'
  | 'alphaUcPeriod'
  | 'arabicParenBoth'
  | 'arabicParenR'
  | 'arabicPeriod'
  | 'arabicPlain'
  | 'romanLcParenBoth'
  | 'romanUcParenBoth'
  | 'romanLcParenR'
  | 'romanUcParenR'
  | 'romanLcPeriod'
  | 'romanUcPeriod'
  | 'circleNumDbPlain'
  | 'circleNumWdBlackPlain'
  | 'circleNumWdWhitePlain'
  | 'arabicDbPeriod'
  | 'arabicDbPlain'
  | 'ea1ChsPeriod'
  | 'ea1ChsPlain'
  | 'ea1ChtPeriod'
  | 'ea1ChtPlain'
  | 'ea1JpnChsDbPeriod'
  | 'ea1JpnKorPlain'
  | 'ea1JpnKorPeriod'
  | 'arabic1Minus'
  | 'arabic2Minus'
  | 'hebrew2Minus'
  | 'thaiAlphaPeriod'
  | 'thaiAlphaParenR'
  | 'thaiAlphaParenBoth'
  | 'thaiNumPeriod'
  | 'thaiNumParenR'
  | 'thaiNumParenBoth'
  | 'hindiAlphaPeriod'
  | 'hindiNumPeriod'
  | 'hindiNumParenR'
  | 'hindiAlpha1Period'
export interface CBulletTypeData {
  type?: BulletType
  char?: string
  autoNumType?: AutoNumScheme
  startAt?: number
}

export function toBulleType(n: BulletTypeValues): BulletType {
  switch (n) {
    case BulletTypeValue.NONE:
      return 'none'
    case BulletTypeValue.CHAR:
      return 'character'
    case BulletTypeValue.AUTONUM:
      return 'autoNumbered'
    case BulletTypeValue.BLIP:
      return 'picture'
    default:
      return 'none'
  }
}

export function fromBulleType(t: BulletType): BulletTypeValues {
  switch (t) {
    case 'none':
      return BulletTypeValue.NONE
    case 'character':
      return BulletTypeValue.CHAR
    case 'autoNumbered':
      return BulletTypeValue.AUTONUM
    case 'picture':
      return BulletTypeValue.BLIP
    default:
      return BulletTypeValue.NONE
  }
}

export function toAutoNumScheme(n: number): AutoNumScheme | undefined {
  return numberingFormatValueToStr(n) as AutoNumScheme
}
export function fromAutoNumScheme(s: AutoNumScheme): number | undefined {
  return strToNumberingFormatValue(s)
}
export interface CBulletData {
  bulletType?: CBulletTypeData
  bulletColor?: BulletColorData
  bulletSize?: CBulletSizeData
  bulletTypeface?: CBulletTypefaceData
}

export type TextFitType = 'noAutofit' | 'normAutofit' | 'spAutoFit'

export type TextFitTypeOO =
  | typeof TextAutoFitType.NO
  | typeof TextAutoFitType.SHAPE
  | typeof TextAutoFitType.NORMAL
  | -1 // unset
export interface CTextFitData {
  type?: TextFitType
  fontScale?: number
  lnSpcReduction?: number
}

export function toTextFitType(n: TextFitTypeOO): TextFitType {
  switch (n) {
    case TextAutoFitType.NO:
      return 'noAutofit'
    case TextAutoFitType.SHAPE:
      return 'spAutoFit'
    case TextAutoFitType.NORMAL:
      return 'normAutofit'
    default:
      return 'noAutofit'
  }
}

export function fromTextFitType(t: TextFitType): TextFitTypeOO {
  switch (t) {
    case 'noAutofit':
      return TextAutoFitType.NO
    case 'spAutoFit':
      return TextAutoFitType.SHAPE
    case 'normAutofit':
      return TextAutoFitType.NORMAL
    default:
      return TextAutoFitType.NO
  }
}
export type TextAnchorType = 't' | 'ctr' | 'b' | 'just' | 'dist'
export type TextAnchorTypeOO =
  | typeof bodyPrAnchor.b
  | typeof bodyPrAnchor.t
  | typeof bodyPrAnchor.ctr
  | typeof bodyPrAnchor.just
  | typeof bodyPrAnchor.dist
export function toTextAnchorType(
  n: TextAnchorTypeOO
): TextAnchorType | undefined {
  switch (n) {
    case bodyPrAnchor.b:
      return 'b'
    case bodyPrAnchor.t:
      return 't'
    case bodyPrAnchor.ctr:
      return 'ctr'
    case bodyPrAnchor.just:
      return 'just'
    case bodyPrAnchor.dist:
      return 'dist'
    default:
      return void 0
  }
}

export function fromTextAnchorType(t: TextAnchorType): TextAnchorTypeOO {
  switch (t) {
    case 'b':
      return bodyPrAnchor.b
    case 't':
      return bodyPrAnchor.t
    case 'ctr':
      return bodyPrAnchor.ctr
    case 'just':
      return bodyPrAnchor.just
    case 'dist':
      return bodyPrAnchor.dist
  }
}

export type TextVertOverflowType = 'overflow' | 'ellipsis' | 'clip'
export type TextVertOverflowTypeOO =
  | typeof TextOverflowType.Clip
  | typeof TextOverflowType.Ellipsis
  | typeof TextOverflowType.Overflow

export function toTextVertOverflowType(
  n: TextVertOverflowTypeOO
): TextVertOverflowType | undefined {
  switch (n) {
    case TextOverflowType.Clip:
      return 'clip'
    case TextOverflowType.Ellipsis:
      return 'ellipsis'
    case TextOverflowType.Overflow:
      return 'overflow'
    default:
      return void 0
  }
}

export function fromTextVertOverflowType(
  t: TextVertOverflowType
): TextVertOverflowTypeOO {
  switch (t) {
    case 'clip':
      return TextOverflowType.Clip
    case 'ellipsis':
      return TextOverflowType.Ellipsis
    case 'overflow':
      return TextOverflowType.Overflow
  }
}

export type TextHorzOverflowType = 'overflow' | 'clip'
export type TextHorzOverflowTypeOO =
  | typeof TextOverflowType.Overflow
  | typeof TextOverflowType.Clip

export function toTextHorzOverflowType(
  n: TextHorzOverflowTypeOO
): TextHorzOverflowType | undefined {
  switch (n) {
    case TextOverflowType.Clip:
      return 'clip'
    case TextOverflowType.Overflow:
      return 'overflow'
    default:
      return void 0
  }
}

export function fromTextHorzOverflowType(
  t: TextHorzOverflowType
): TextHorzOverflowTypeOO {
  switch (t) {
    case 'clip':
      return TextOverflowType.Clip
    case 'overflow':
      return TextOverflowType.Overflow
  }
}

export type TextVerticalType =
  | 'horz'
  | 'vert'
  | 'vert270'
  | 'wordArtVert'
  | 'eaVert'
  | 'mongolianVert'
  | 'wordArtVertRtl'

export type TextVerticalTypeOO =
  | typeof TextVerticalType.eaVert
  | typeof TextVerticalType.horz
  | typeof TextVerticalType.mongolianVert
  | typeof TextVerticalType.vert
  | typeof TextVerticalType.vert270
  | typeof TextVerticalType.wordArtVert
  | typeof TextVerticalType.wordArtVertRtl

export function toTextVerticalType(
  n: TextVerticalTypeOO
): TextVerticalType | undefined {
  switch (n) {
    case TextVerticalType.eaVert:
      return 'eaVert'
    case TextVerticalType.horz:
      return 'horz'
    case TextVerticalType.mongolianVert:
      return 'mongolianVert'
    case TextVerticalType.vert:
      return 'vert'
    case TextVerticalType.vert270:
      return 'vert270'
    case TextVerticalType.wordArtVert:
      return 'wordArtVert'
    case TextVerticalType.wordArtVertRtl:
      return 'wordArtVertRtl'
    default:
      void 0
  }
}

export function fromTextVerticalType(t: TextVerticalType): TextVerticalTypeOO {
  switch (t) {
    case 'horz':
      return TextVerticalType.horz
    case 'vert':
      return TextVerticalType.vert
    case 'vert270':
      return TextVerticalType.vert270
    case 'wordArtVert':
      return TextVerticalType.wordArtVert
    case 'eaVert':
      return TextVerticalType.eaVert
    case 'mongolianVert':
      return TextVerticalType.mongolianVert
    case 'wordArtVertRtl':
      return TextVerticalType.wordArtVertRtl
  }
}

export type TextWrappingType = 'none' | 'square'
export type TextWrappingTypeOO =
  | typeof TextWarpType.NONE
  | typeof TextWarpType.SQUARE

export function toTextWrappingType(
  n: TextWrappingTypeOO
): TextWrappingType | undefined {
  switch (n) {
    case TextWarpType.NONE:
      return 'none'
    case TextWarpType.SQUARE:
      return 'square'
    default:
      return void 0
  }
}

export function fromTextWrappingType(t: TextWrappingType): TextWrappingTypeOO {
  switch (t) {
    case 'none':
      return TextWarpType.NONE
    case 'square':
      return TextWarpType.SQUARE
  }
}
export interface CBodyPrData {
  flatTx?: number
  anchor?: TextAnchorType
  anchorCtr?: boolean
  bIns?: number
  compatLnSpc?: boolean
  forceAA?: boolean
  fromWordArt?: boolean
  horzOverflow?: TextHorzOverflowType
  lIns?: number
  numCol?: number
  rIns?: number
  rot?: number
  rtlCol?: boolean
  spcCol?: number
  spcFirstLastPara?: boolean
  tIns?: number
  upright?: boolean
  vert?: TextVerticalType
  vertOverflow?: TextVertOverflowType
  wrap?: TextWrappingType
  textFit?: CTextFitData
  prstTxWarp?: GeometryData
}

export interface CShapeStyleData {
  lnRef?: StyleRefData
  fillRef?: StyleRefData
  effectRef?: StyleRefData
  fontRef?: FontRefData
}

export interface CBgPrData {
  fill?: CFillEffectsData
  shadeToTitle: boolean
}

export interface CBgData {
  bwMode?: BwMode
  bgPr?: CBgPrData
  bgRef?: StyleRefData
}

export interface HFData {
  dt: boolean
  ftr: boolean
  hdr: boolean
  sldNum: boolean
}

// ---------------------------------------------------------------------------
export interface HyperlinkPropsData {
  snd?: string
  id?: string
  invalidUrl?: string
  action?: string
  tgtFrame?: string
  tooltip?: string
  history?: boolean
  highlightClick?: boolean
  endSnd?: boolean
}

export interface ColorModData {
  name: string
  val: number
}

export interface CNvPrData {
  id?: number
  name?: string
  isHidden?: boolean
  descr?: string
  title?: string
  hlinkClick?: HyperlinkPropsData
  hlinkHover?: HyperlinkPropsData
}

type MediaType =
  | 'audioCd'
  | 'wavAudioFile'
  | 'audioFile'
  | 'videoFile'
  | 'quickTimeFile'

export interface UniMediaData {
  type?: MediaType
  media?: string
  encryptUrl?: string
}

export function toMediaType(input: 7 | 8): MediaType | undefined {
  switch (input) {
    case 7: //video
      return 'videoFile'
    case 8: //audio
      return 'audioFile'
    default:
      return void 0
  }
}

export function fromMediaType(t: MediaType): 7 | 8 | undefined {
  switch (t) {
    case 'audioCd':
    case 'audioFile':
    case 'wavAudioFile':
      return 8
    case 'quickTimeFile':
    case 'videoFile':
      return 7
    default:
      return void 0
  }
}
export interface NvPrData {
  isPhoto?: boolean
  userDrawn?: boolean
  ph?: PhData
  unimedia?: UniMediaData
}

export type PhType =
  | 'title'
  | 'body'
  | 'ctrTitle'
  | 'subTitle'
  | 'dt'
  | 'sldNum'
  | 'ftr'
  | 'hdr'
  | 'obj'
  | 'chart'
  | 'tbl'
  | 'clipArt'
  | 'dgm'
  | 'media'
  | 'sldImg'
  | 'pic'
export type PhTypeOO =
  | typeof PlaceholderType.body
  | typeof PlaceholderType.chart
  | typeof PlaceholderType.clipArt
  | typeof PlaceholderType.ctrTitle
  | typeof PlaceholderType.dgm
  | typeof PlaceholderType.dt
  | typeof PlaceholderType.ftr
  | typeof PlaceholderType.hdr
  | typeof PlaceholderType.media
  | typeof PlaceholderType.obj
  | typeof PlaceholderType.pic
  | typeof PlaceholderType.sldImg
  | typeof PlaceholderType.sldNum
  | typeof PlaceholderType.subTitle
  | typeof PlaceholderType.tbl
  | typeof PlaceholderType.title

export type PhSZType = 'full' | 'half' | 'quarter'
export type PhSZTypeOO =
  | typeof phSZType.full
  | typeof phSZType.half
  | typeof phSZType.quarter
export interface PhData {
  hasCustomPrompt?: boolean
  idx?: string
  orient?: number
  sz?: PhSZType
  type?: PhType
}

export function toPhType(n: PhTypeOO): PhType {
  switch (n) {
    case PlaceholderType.body:
      return 'body'
    case PlaceholderType.chart:
      return 'chart'
    case PlaceholderType.clipArt:
      return 'clipArt'
    case PlaceholderType.ctrTitle:
      return 'ctrTitle'
    case PlaceholderType.dgm:
      return 'dgm'
    case PlaceholderType.dt:
      return 'dt'
    case PlaceholderType.ftr:
      return 'ftr'
    case PlaceholderType.hdr:
      return 'hdr'
    case PlaceholderType.media:
      return 'media'
    case PlaceholderType.obj:
      return 'obj'
    case PlaceholderType.pic:
      return 'pic'
    case PlaceholderType.sldImg:
      return 'sldImg'
    case PlaceholderType.sldNum:
      return 'sldNum'
    case PlaceholderType.subTitle:
      return 'subTitle'
    case PlaceholderType.tbl:
      return 'tbl'
    case PlaceholderType.title:
      return 'title'
  }
}

export function fromPhType(t: PhType): PhTypeOO {
  switch (t) {
    case 'title':
      return PlaceholderType.title
    case 'body':
      return PlaceholderType.body
    case 'ctrTitle':
      return PlaceholderType.ctrTitle
    case 'subTitle':
      return PlaceholderType.subTitle
    case 'dt':
      return PlaceholderType.dt
    case 'sldNum':
      return PlaceholderType.sldNum
    case 'ftr':
      return PlaceholderType.ftr
    case 'hdr':
      return PlaceholderType.hdr
    case 'obj':
      return PlaceholderType.obj
    case 'chart':
      return PlaceholderType.chart
    case 'tbl':
      return PlaceholderType.tbl
    case 'clipArt':
      return PlaceholderType.clipArt
    case 'dgm':
      return PlaceholderType.dgm
    case 'media':
      return PlaceholderType.media
    case 'sldImg':
      return PlaceholderType.sldImg
    case 'pic':
      return PlaceholderType.pic
  }
}

export function toPhSZType(n: PhSZTypeOO): PhSZType | undefined {
  switch (n) {
    case phSZType.full:
      return 'full'
    case phSZType.half:
      return 'half'
    case phSZType.quarter:
      return 'quarter'
    default:
      return void 0
  }
}

export function fromPhSZType(t: PhSZType): PhSZTypeOO {
  switch (t) {
    case 'full':
      return phSZType.full
    case 'half':
      return phSZType.half
    case 'quarter':
      return phSZType.quarter
  }
}
export const SpTypes = {
  Shape: 'sp',
  Pic: 'pic',
  GraphicFrame: 'graphicFrame',
  GroupShape: 'grpSp',
  ConnectionShape: 'cxnSp',
  Ole: 'ole',
  Chart: 'chart',
} as const

export const SpKinds = {
  Shape: 1,
  ConnectionShape: 2,
  Pic: 3,
  GraphicFrame: 4,
  GroupShape: 5,
  Ole: 6,
  Chart: 7,
} as const

export function spKindToType(
  k: valuesOfDict<typeof SpKinds>
): valuesOfDict<typeof SpTypes> {
  switch (k) {
    case SpKinds.Shape:
      return SpTypes.Shape
    case SpKinds.ConnectionShape:
      return SpTypes.ConnectionShape
    case SpKinds.Pic:
      return SpTypes.Pic
    case SpKinds.GraphicFrame:
      return SpTypes.GraphicFrame
    case SpKinds.GroupShape:
      return SpTypes.GroupShape
    case SpKinds.Ole:
      return SpTypes.Ole
    case SpKinds.Chart:
      return SpTypes.Chart
  }
}

export function spTypeToKind(t: valuesOfDict<typeof SpTypes>): valuesOfDict<typeof SpKinds> {
  switch(t){
    case SpTypes.Shape:
      return SpKinds.Shape
    case SpTypes.ConnectionShape:
      return SpKinds.ConnectionShape
    case SpTypes.Pic:
      return SpKinds.Pic
    case SpTypes.GraphicFrame:
      return SpKinds.GraphicFrame
    case SpTypes.GroupShape:
      return SpKinds.GroupShape
    case SpTypes.Ole:
      return SpKinds.Ole
    case SpTypes.Chart:
      return SpKinds.Chart
  }
}

export type SpTypes = (typeof SpTypes)[keyof typeof SpTypes]

export const ImageSubTypes = {
  Chart: 'chart',
  Custom: 'custom',
} as const

export type ImageSubTypes =
  | (typeof ImageSubTypes)[keyof typeof ImageSubTypes]
  | string

export type NvSpLock =
  | 'noAdjustHandles'
  | 'noChangeArrowheads'
  | 'noChangeAspect'
  | 'noChangeShapeType'
  | 'noCrop'
  | 'noDrilldown'
  | 'noEditPoints'
  | 'noGrp'
  | 'noMove'
  | 'noResize'
  | 'noRot'
  | 'noSelect'
  | 'noTextEdit'
  | 'noUngrp'

const NvSpLockMaskMap = {
  ['noAdjustHandles']: ShapeLocks.noAdjustHandles,
  ['noChangeArrowheads']: ShapeLocks.noChangeArrowheads,
  ['noChangeAspect']: ShapeLocks.noModifyAspect,
  ['noChangeShapeType']: ShapeLocks.noChangeShapeType,
  ['noCrop']: ShapeLocks.noCrop,
  ['noDrilldown']: ShapeLocks.noDrilldown,
  ['noEditPoints']: ShapeLocks.noEditPoints,
  ['noGrp']: ShapeLocks.noGrp,
  ['noMove']: ShapeLocks.noMove,
  ['noResize']: ShapeLocks.noResize,
  ['noRot']: ShapeLocks.noRot,
  ['noSelect']: ShapeLocks.noSelect,
  ['noTextEdit']: ShapeLocks.noTextEdit,
  ['noUngrp']: ShapeLocks.noUngrp,
} as const

const NvSpLocksForSp = [
  'noAdjustHandles',
  'noChangeArrowheads',
  'noChangeAspect',
  'noChangeShapeType',
  'noEditPoints',
  'noGrp',
  'noMove',
  'noResize',
  'noRot',
  'noSelect',
  'noTextEdit',
] as const

const NvSpLocksForPic = [
  'noAdjustHandles',
  'noChangeArrowheads',
  'noChangeAspect',
  'noChangeShapeType',
  'noCrop',
  'noEditPoints',
  'noGrp',
  'noMove',
  'noResize',
  'noRot',
  'noSelect',
] as const

const NvSpLocksForGraphicFrame = [
  'noChangeAspect',
  'noDrilldown',
  'noGrp',
  'noMove',
  'noResize',
  'noSelect',
] as const

const NvSpLocksForGroupShape = [
  'noChangeAspect',
  'noGrp',
  'noMove',
  'noResize',
  'noRot',
  'noSelect',
  'noUngrp',
] as const

const NvSpLocksForCnxShape = [
  'noAdjustHandles',
  'noChangeArrowheads',
  'noChangeAspect',
  'noChangeShapeType',
  'noEditPoints',
  'noGrp',
  'noMove',
  'noResize',
  'noRot',
  'noSelect',
] as const
type NvSpLocksLayout =
  | typeof NvSpLocksForSp
  | typeof NvSpLocksForPic
  | typeof NvSpLocksForGraphicFrame
  | typeof NvSpLocksForGroupShape
  | typeof NvSpLocksForCnxShape

function computeLockMasks<L extends NvSpLocksLayout>(
  locks: NvSpLock[],
  layout: L
): number {
  let result = 0
  const locksSet: {
    [k in NvSpLock]?: true
  } = (() => {
    const r = {}
    for (const l of locks) {
      if (NvSpLockMaskMap[l] != null) {
        r[l] = true
      }
    }
    return r
  })()

  for (const lock of layout) {
    const mask = NvSpLockMaskMap[lock]
    const isEnabled = !!locksSet[lock]
    if (isEnabled) {
      result |= mask | (mask << 1)
    }
  }

  return result
}

export function fromNvSpLocks(
  locks: NvSpLock[],
  spType: SpTypes
): number | undefined {
  if (!Array.isArray(locks)) {
    return undefined
  }
  switch (spType) {
    case 'sp': {
      return computeLockMasks(locks, NvSpLocksForSp)
    }
    case 'pic': {
      return computeLockMasks(locks, NvSpLocksForPic)
    }
    case 'graphicFrame': {
      return computeLockMasks(locks, NvSpLocksForGraphicFrame)
    }
    case 'grpSp': {
      return computeLockMasks(locks, NvSpLocksForGroupShape)
    }
    case 'cxnSp': {
      return computeLockMasks(locks, NvSpLocksForCnxShape)
    }
  }
}

function locksFromLockMasks<L extends NvSpLocksLayout>(
  lockMask: number,
  layout: L
): NvSpLock[] {
  const result: NvSpLock[] = []
  for (const lock of layout) {
    const mask = NvSpLockMaskMap[lock]
    const isEnabled = !!(lockMask & (mask << 1))
    if (isEnabled) {
      result.push(lock)
    }
  }

  return result
}

export function toNvSpLocks(
  lockMask: number,
  spType: SpTypes
): NvSpLock[] | undefined {
  switch (spType) {
    case 'sp': {
      return locksFromLockMasks(lockMask, NvSpLocksForSp)
    }
    case 'pic': {
      return locksFromLockMasks(lockMask, NvSpLocksForPic)
    }
    case 'graphicFrame': {
      return locksFromLockMasks(lockMask, NvSpLocksForGraphicFrame)
    }
    case 'grpSp': {
      return locksFromLockMasks(lockMask, NvSpLocksForGroupShape)
    }
    case 'cxnSp': {
      return locksFromLockMasks(lockMask, NvSpLocksForCnxShape)
    }
  }
}

export interface CNvUniSpPrData {
  locks?: NvSpLock[]
  stCnxIdx?: number
  stCnxId?: number
  endCnxIdx?: number
  endCnxId?: number
}

export interface UniNvPrData {
  cNvPr?: CNvPrData
  nvPr?: NvPrData
  nvUniSpPr?: CNvUniSpPrData // sp中未使用 cnx中使用
}
