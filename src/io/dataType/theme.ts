import {
  ComplexColorData,
  CFillEffectsData,
  CLnData,
  CSpPrData,
  CBodyPrData,
  CShapeStyleData,
  ClrMapData,
} from './spAttrs'
import { TextPrFont, TextListStyleData } from './style'
import { InsertOperation, JSONOpData } from '../operation'

export type ThemeData = [InsertOperation<JSONOpData<CThemeData>>]

export interface CThemeData {
  name?: string
  themeElements?: ThemeElementsData
  objectDefaults?: ThemeObjectDefaultsData
  extraClrSchemeLst?: ExtraClrSchemeData[]
}

export interface ThemeElementsData {
  clrScheme: ClrSchemeData
  fontScheme: FontSchemeData
  fmtScheme: FmtSchemeData
}

export interface ClrSchemeData {
  name?: string
  lt1?: ComplexColorData
  lt2?: ComplexColorData
  dk1?: ComplexColorData
  dk2?: ComplexColorData
  hlink?: ComplexColorData
  folHlink?: ComplexColorData
  accent1?: ComplexColorData
  accent2?: ComplexColorData
  accent3?: ComplexColorData
  accent4?: ComplexColorData
  accent5?: ComplexColorData
  accent6?: ComplexColorData
}

export interface FmtSchemeData {
  name?: string
  fillStyleLst?: CFillEffectsData[]
  lnStyleLst?: CLnData[]
  bgFillStyleLst?: CFillEffectsData[]
  // effectStyleLst 暂不支持
}

export interface FontSchemeData {
  name?: string
  minorFont?: FontCollectionData
  majorFont?: FontCollectionData
}

export interface FontCollectionData {
  latin?: TextPrFont
  ea?: TextPrFont
  cs?: TextPrFont
  //font? 暂不支持
}

export interface ThemeObjectDefaultsData {
  spDef?: DefaultShapeAttributesData
  lnDef?: DefaultShapeAttributesData
  txDef?: DefaultShapeAttributesData
}

export interface DefaultShapeAttributesData {
  spPr?: CSpPrData
  bodyPr?: CBodyPrData
  lstStyle?: TextListStyleData
  style?: CShapeStyleData
}

export interface ExtraClrSchemeData {
  clrScheme?: ClrSchemeData
  clrMap?: ClrMapData
}
