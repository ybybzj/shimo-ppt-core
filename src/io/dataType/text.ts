/* eslint-disable @typescript-eslint/ban-ts-comment */
import {
  InsertOperationWithType,
  JSONOpData,
  DeltaOpData,
  InsertOperation,
  EmptyJSONData,
} from '../operation'
import { CBodyPrData } from './spAttrs'
import { TextListStyleData, CTextPrData, CParaPrData } from './style'
import { Dict } from '../../../liber/pervasive'

export interface ParaHyperlinkAttr {
  hyperlink: true
  value?: string
  toolTip?: string
}
interface ParaEndAttrs {
  rPr?: CTextPrData
  pPr?: CParaPrData
  TextPr?: CTextPrData
}

type ParaRunAttrs<Ext extends Dict = {}> = CTextPrData & Ext

// @ts-ignore
type ParaText = InsertOperation<string, ParaRunAttrs>
type ParaHyperlink = InsertOperation<
  // @ts-ignore
  DeltaOpData<DocContentData>,
  ParaHyperlinkAttr
>
// @ts-ignore
type ParaEnd = InsertOperation<number, ParaEndAttrs>
type ParaField = InsertOperation<
  number,
  // @ts-ignore
  ParaRunAttrs<{ paraField: true }>
>
// @ts-ignore
type ParaBreak = InsertOperation<number, ParaRunAttrs<{ break: true }>>

type RunItem = ParaText | ParaField | ParaBreak | ParaEnd | ParaHyperlink

export type DocContentData = RunItem[]

//textbody
export type TextBodyData = [
  InsertOperationWithType<'bodyPr', JSONOpData<CBodyPrData> | EmptyJSONData>,
  InsertOperationWithType<
    'listStyle',
    JSONOpData<{ listStyle: TextListStyleData }> | EmptyJSONData
  >,
  // @ts-ignore
  InsertOperationWithType<
    'docContent',
    DeltaOpData /**DeltaOpData<DocContentData> */
  >
]
