import { HFData, ColorMappingOverride } from './spAttrs'
import { InsertOperationWithType, JSONOpData, DeltaOpData } from '../operation'
import { CSldData } from './csld'
import { SlideTransitionData } from './transition'
type SlideLayoutType =
  | 'title'
  | 'tx'
  | 'twoColTx'
  | 'tbl'
  | 'txAndChart'
  | 'chartAndTx'
  | 'dgm'
  | 'chart'
  | 'txAndClipArt'
  | 'clipArtAndTx'
  | 'titleOnly'
  | 'blank'
  | 'txAndObj'
  | 'objAndTx'
  | 'objOnly'
  | 'obj'
  | 'txAndMedia'
  | 'mediaAndTx'
  | 'objOverTx'
  | 'txOverObj'
  | 'txAndTwoObj'
  | 'twoObjAndTx'
  | 'twoObjOverTx'
  | 'fourObj'
  | 'vertTx'
  | 'clipArtAndVertTx'
  | 'vertTitleAndTx'
  | 'vertTitleAndTxOverChart'
  | 'twoObj'
  | 'objAndTwoObj'
  | 'twoObjAndObj'
  | 'cust'
  | 'secHead'
  | 'twoTxTwoObj'
  | 'objTx'
  | 'picTx'
export interface SlideLayoutAttrData {
  matchingName?: string
  preserve?: boolean
  showMasterPhAnim?: boolean
  showMasterSp?: boolean
  userDrawn?: boolean
  type?: SlideLayoutType
  clrMap?: ColorMappingOverride
  hf?: HFData
  transition?: SlideTransitionData
}

export function toSlideLayoutType(t: number): SlideLayoutType {
  switch (t) {
    case 18:
      return 'title'
    case 9:
      return 'obj'
    case 16:
      return 'secHead'
    case 21:
      return 'twoObj'
    case 25: //Comparison
      return 'twoTxTwoObj'
    case 19:
      return 'titleOnly'
    case 0:
      return 'blank'
    case 14:
      return 'objTx'
    case 15:
      return 'picTx'
    case 35:
      return 'vertTx'
    case 33:
      return 'vertTitleAndTx'
    default:
      return 'blank'
  }
}

export function fromSlideLayoutType(t: SlideLayoutType): number {
  switch (t) {
    case 'title':
      return 18
    case 'obj':
      return 9
    case 'secHead':
      return 16
    case 'twoObj':
      return 21
    case 'twoTxTwoObj':
      return 25
    case 'titleOnly':
      return 19
    case 'blank':
      return 0
    case 'objTx':
      return 14
    case 'picTx':
      return 15
    case 'vertTx':
      return 35
    case 'vertTitleAndTx':
      return 33
    default:
      return 0
  }
}
export type SlideLayoutData = [
  InsertOperationWithType<'attrs', JSONOpData<SlideLayoutAttrData>>,
  InsertOperationWithType<'csld', DeltaOpData<CSldData>>
]
