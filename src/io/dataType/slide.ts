import { DeltaOpData, InsertOperationWithType, JSONOpData } from '../operation'
import { CT_SlideTiming } from './animation'
import { CSldData } from './csld'
import { ColorMappingOverride } from './spAttrs'
import { SlideTransitionData } from './transition'

export interface SlideAttrs {
  show?: boolean
  showMasterPhAnim?: boolean
  showMasterSp?: boolean
  clrMap?: ColorMappingOverride
  transition?: SlideTransitionData
  timing?: CT_SlideTiming
  layoutRef?: string
  notesRef?: string
  commentsRef?: string
}

export type SlideData = [
  InsertOperationWithType<'attrs', JSONOpData<SlideAttrs>>,
  InsertOperationWithType<'csld', DeltaOpData<CSldData>>
]
