import { ClrMapData, HFData } from './spAttrs'
import { CTextStylesData } from './style'
import { InsertOperationWithType, JSONOpData, DeltaOpData } from '../operation'
import { CSldData } from './csld'
import { SlideTransitionData } from './transition'

export interface SlideMasterAttrs {
  themeId?: string
  preserve?: boolean
  clrMap?: ClrMapData
  hf?: HFData
  txStyles?: CTextStylesData
  themeRef?: string
  transition?: SlideTransitionData
}

export interface SlideLayoutRefData {
  layoutRefs: string[]
}

export type SlideMasterData = [
  InsertOperationWithType<'attrs', JSONOpData<SlideMasterAttrs>>,
  InsertOperationWithType<'layoutRefs', JSONOpData<SlideLayoutRefData>>,
  InsertOperationWithType<'csld', DeltaOpData<CSldData>>
]
