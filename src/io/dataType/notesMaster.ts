import { ClrMapData, HFData } from './spAttrs'
import { TextListStyleData } from './style'
import { InsertOperationWithType, JSONOpData, DeltaOpData } from '../operation'
import { CSldData } from './csld'

export interface NotesMasterAttrs {
  clrMap?: ClrMapData
  hf?: HFData
  notesStyles?: TextListStyleData
  themeRef?: string
}

export type NotesMasterData = [
  InsertOperationWithType<'attrs', JSONOpData<NotesMasterAttrs>>,
  InsertOperationWithType<'csld', DeltaOpData<CSldData>>
]
