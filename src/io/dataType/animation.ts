import { ComplexColorData } from './spAttrs'
export type ST_Percentage = string // regex: -?[0-9]+(\.[0-9]+)?%
export type ST_PositiveFixedPercentage = string // regex: ((100)|([0-9][0-9]?))(\.[0-9][0-9]?)?%
export type ST_PositivePercentage = string // regex: [0-9]+(\.[0-9]+)?%
export type ST_FixedPercentage = string //regex: -?((100)|([0-9][0-9]?))(\.[0-9][0-9]?)?%
export type ST_PercentageVal<
  S extends
    | ST_Percentage
    | ST_PositiveFixedPercentage
    | ST_PositivePercentage
    | ST_FixedPercentage
> = S | number // unit is 1000 if is number

export interface CT_SlideTiming {
  tnLst: CT_TimeNodeList
  bldLst?: any
  // extLst  not support
}
export type CT_TimeNodeList = Array<CT_TimeNode>
export type CT_TimeNode =
  | CT_TLTimeNodeParallel
  | CT_TLTimeNodeSequence
  | CT_TLTimeNodeExclusive
  | CT_TLAnimateBehavior
  | CT_TLAnimateColorBehavior
  | CT_TLAnimateEffectBehavior
  | CT_TLAnimateMotionBehavior
  | CT_TLAnimateRotationBehavior
  | CT_TLAnimateScaleBehavior
  | CT_TLCommandBehavior
  | CT_TLSetBehavior
  | CT_TLMediaNodeAudio
  | CT_TLMediaNodeVideo

//audio
//video

//#region  CT_TLTimeNodeParallel
export interface CT_TLTimeNodeParallel {
  type: 'par'
  cTn: CT_TLCommonTimeNodeData
}

export interface CT_TLCommonTimeNodeData {
  id?: number //unsigned int
  presetID?: number // int
  presetClass?: ST_TLTimeNodePresetClassType
  presetSubtype?: number // int
  dur?: ST_TLTime
  repeatCount?: ST_TLTime //unit: 1000, default: 1000
  repeatDur?: ST_TLTime
  spd?: ST_PercentageVal<ST_Percentage> // default: "100%"
  accel?: ST_PercentageVal<ST_PositiveFixedPercentage> // default: "0%"
  decel?: ST_PercentageVal<ST_PositiveFixedPercentage> // default: "0%"
  autoRev?: boolean // default: false
  restart?: ST_TLTimeNodeRestartType
  fill?: ST_TLTimeNodeFillType
  syncBehavior?: ST_TLTimeNodeSyncType
  tmFilter?: string
  evtFilter?: string
  display?: boolean
  masterRel?: ST_TLTimeNodeMasterRelation
  bldLvl?: number // int
  grpId?: number // unsigned int
  afterEffect?: boolean
  nodeType?: ST_TLTimeNodeType
  nodePh?: boolean
  // complex attrs
  stCondLst?: CT_TLTimeConditionList
  endCondLst?: CT_TLTimeConditionList
  endSync?: CT_TLTimeCondition
  iterate?: CT_TLIterateData
  childTnLst?: CT_TimeNodeList
  subTnLst?: CT_TimeNodeList
}

type ST_TLTime = number | 'indefinite'
export type ST_TLTimeNodePresetClassType =
  | 'entr'
  | 'exit'
  | 'emph'
  | 'path'
  | 'verb'
  | 'mediacall'
type ST_TLTimeNodeRestartType = 'always' | 'whenNotActive' | 'never'
type ST_TLTimeNodeFillType = 'remove' | 'freeze' | 'hold' | 'transition'
type ST_TLTimeNodeSyncType = 'canSlip' | 'locked'
type ST_TLTimeNodeMasterRelation = 'sameClick' | 'lastClick' | 'nextClick'
export type ST_TLTimeNodeType =
  | 'clickEffect'
  | 'withEffect'
  | 'afterEffect'
  | 'mainSeq'
  | 'interactiveSeq'
  | 'clickPar'
  | 'withGroup'
  | 'afterGroup'
  | 'tmRoot'

export type CT_TLTimeConditionList = Array<{
  cond: CT_TLTimeCondition
}>

export interface CT_TLTimeCondition {
  evt?: ST_TLTriggerEvent
  delay?: ST_TLTime
  content?: CT_TLTimeConditionContent
}

type CT_TLTimeConditionContent =
  | {
      type: 'tgtEl'
      tgtEl: CT_TLTimeTargetElement
    }
  | {
      type: 'tn'
      tn: number // ST_TLTimeNodeID
    }
  | {
      type: 'rtn'
      rtn: ST_TLTriggerRuntimeNode
    }

type ST_TLTriggerRuntimeNode = 'first' | 'last' | 'all'
type ST_TLTriggerEvent =
  | 'onBegin'
  | 'onEnd'
  | 'begin'
  | 'end'
  | 'onClick'
  | 'onDblClick'
  | 'onMouseOver'
  | 'onMouseOut'
  | 'onNext'
  | 'onPrev'
  | 'onStopAudio'

export type CT_TLTimeTargetElement =
  | CT_TLTimeTargetElement_sld
  | CT_TLTimeTargetElement_sp

interface CT_TLTimeTargetElement_sld {
  name: 'sldTgt'
}
// sndTgt

export interface CT_TLTimeTargetElement_sp {
  name: 'spTgt'
  spid: string // shape refId
  bg?: boolean
  subSp?: string // shape refId
  txEl?: {
    charRg?: CT_IndexRange
    pRg?: CT_IndexRange
  }
}

interface CT_IndexRange {
  st: number // unsinged int
  end: number //unsigned int
}

interface CT_TLIterateData {
  type?: ST_IterateType // default: 'el'
  backwards?: boolean
  tmAbs?: number
  tmPct?: ST_PercentageVal<ST_PositivePercentage>
}

interface CT_TLCommonMediaNodeData {
  // <-- attrs -->
  vol?: ST_PercentageVal<ST_PositiveFixedPercentage>
  mute?: boolean
  numSld?: number //unsigned int
  showWhenStopped?: boolean // default true
  // <-- sequence -->
  cTn: CT_TLCommonTimeNodeData
  tgtEl: CT_TLTimeTargetElement
}

type ST_IterateType = 'el' | 'wd' | 'lt'
//#endregion

//#region CT_TLTimeNodeSequence
export interface CT_TLTimeNodeSequence {
  type: 'seq'
  concurrent?: boolean
  prevAc?: ST_TLPreviousActionType
  nextAc?: ST_TLNextActionType
  cTn: CT_TLCommonTimeNodeData
  prevCondLst?: CT_TLTimeConditionList
  nextCondLst?: CT_TLTimeConditionList
}

type ST_TLPreviousActionType = 'none' | 'skipTimed'
type ST_TLNextActionType = 'none' | 'seek'
//#endregion

//#region CT_TLTimeNodeExclusive
export interface CT_TLTimeNodeExclusive {
  type: 'excl'
  cTn: CT_TLCommonTimeNodeData
}
//#endregion

//#region CT_TLAnimateBehavior
export interface CT_TLAnimateBehavior {
  type: 'anim'
  // <-- attrs -->
  by?: string
  from?: string
  to?: string
  calcmode?: ST_TLAnimateBehaviorCalcMode
  valueType?: ST_TLAnimateBehaviorValueType
  // <-- sequence -->
  cBhvr: CT_TLCommonBehaviorData
  tavLst?: CT_TLTimeAnimateValueList
}

type ST_TLAnimateBehaviorCalcMode = 'discrete' | 'lin' | 'fmla'
type ST_TLAnimateBehaviorValueType = 'str' | 'num' | 'clr'

export interface CT_TLCommonBehaviorData {
  // <-- attrs -->
  additive?: ST_TLBehaviorAdditiveType
  accumulate?: ST_TLBehaviorAccumulateType
  xfrmType?: ST_TLBehaviorTransformType
  from?: string
  to?: string
  by?: string
  rctx?: string // 'PPT' | 'IE', do not handle
  override?: ST_TLBehaviorOverrideType
  // <-- sequence -->
  cTn: CT_TLCommonTimeNodeData
  tgtEl: CT_TLTimeTargetElement
  attrNameLst?: string[]
}

type ST_TLBehaviorAdditiveType = 'base' | 'sum' | 'repl' | 'mult' | 'none'
type ST_TLBehaviorAccumulateType = 'none' | 'always'
type ST_TLBehaviorTransformType = 'pt' | 'img'
type ST_TLBehaviorOverrideType = 'normal' | 'childStyle'

type CT_TLTimeAnimateValueList = Array<{
  tav: CT_TLTimeAnimateValue
}>

interface CT_TLTimeAnimateValue {
  tm?: ST_TLTimeAnimateValueTime
  fmla?: string
  val?: CT_TLAnimVariant
}

type ST_TLTimeAnimateValueTime =
  | ST_PercentageVal<ST_PositiveFixedPercentage>
  | 'indefinite'

type CT_TLAnimVariant =
  | {
      type: 'bool'
      val: boolean
    }
  | {
      type: 'int'
      val: number
    }
  | {
      type: 'float'
      val: number
    }
  | {
      type: 'str'
      val: string
    }
  | {
      type: 'clr'
      val: ComplexColorData
    }
//#endregion

//#region CT_TLAnimateColorBehavior
export interface CT_TLAnimateColorBehavior {
  type: 'animClr'
  // <-- attrs -->
  clrSpc?: ST_TLAnimateColorSpace
  dir?: ST_TLAnimateColorDirection
  // <-- sequence -->
  cBhvr: CT_TLCommonBehaviorData
  by?: CT_TLByAnimateColorTransform
  from?: ComplexColorData
  to?: ComplexColorData
}
type ST_TLAnimateColorSpace = 'rgb' | 'hsl'
type ST_TLAnimateColorDirection = 'cw' | 'ccw'

type CT_TLByAnimateColorTransform =
  | {
      type: 'rgb'
      r: ST_PercentageVal<ST_FixedPercentage>
      g: ST_PercentageVal<ST_FixedPercentage>
      b: ST_PercentageVal<ST_FixedPercentage>
    }
  | {
      type: 'hsl'
      h: number
      s: ST_PercentageVal<ST_FixedPercentage>
      l: ST_PercentageVal<ST_FixedPercentage>
    }
//#endregion

//#region CT_TLAnimateEffectBehavior
export interface CT_TLAnimateEffectBehavior {
  type: 'animEffect'
  // <-- attrs -->
  transition?: ST_TLAnimateEffectTransition
  filter?: string
  prLst?: string
  // <-- sequence -->
  cBhvr: CT_TLCommonBehaviorData
  progress?: CT_TLAnimVariant
}
type ST_TLAnimateEffectTransition = 'in' | 'out' | 'none'
//#endregion

//#region CT_TLAnimateMotionBehavior
export interface CT_TLAnimateMotionBehavior {
  type: 'animMotion'
  // <-- attrs -->
  origin?: ST_TLAnimateMotionBehaviorOrigin
  path?: string
  pathEditMode?: ST_TLAnimateMotionPathEditMode
  rAng?: number //int
  ptsTypes?: string
  // <-- sequence -->
  cBhvr: CT_TLCommonBehaviorData
  by?: CT_TLPoint
  from?: CT_TLPoint
  to?: CT_TLPoint
  rCtr: CT_TLPoint
}
type ST_TLAnimateMotionBehaviorOrigin = 'parent' | 'layout'
type ST_TLAnimateMotionPathEditMode = 'relative' | 'fixed'
interface CT_TLPoint {
  x: ST_PercentageVal<ST_Percentage>
  y: ST_PercentageVal<ST_Percentage>
}
//#endregion

//#region CT_TLAnimateRotationBehavior
export interface CT_TLAnimateRotationBehavior {
  type: 'animRot'
  // <-- attrs -->
  by?: number
  from?: number
  to?: number

  // <-- sequence -->
  cBhvr: CT_TLCommonBehaviorData
}
//#endregion

//#region CT_TLAnimateScaleBehavior
export interface CT_TLAnimateScaleBehavior {
  type: 'animScale'
  // <-- attrs -->
  zoomContents?: boolean

  // <-- sequence -->
  cBhvr: CT_TLCommonBehaviorData
  by?: CT_TLPoint
  from?: CT_TLPoint
  to?: CT_TLPoint
}
//#endregion

//#region CT_TLCommandBehavior
export interface CT_TLCommandBehavior {
  type: 'cmd'
  // <-- attrs -->
  cmdType?: ST_TLCommandType
  cmd?: string
  // <-- sequence -->
  cBhvr: CT_TLCommonBehaviorData
}

type ST_TLCommandType = 'evt' | 'call' | 'verb'
//#endregion

//#region CT_TLSetBehavior
export interface CT_TLSetBehavior {
  type: 'set'
  // <-- attrs -->
  // <-- sequence -->
  cBhvr: CT_TLCommonBehaviorData
  to?: CT_TLAnimVariant
}
//#endregion

// CT_TLMediaNodeAudio
export interface CT_TLMediaNodeAudio {
  type: 'audio'
  // <-- attrs -->
  isNarration?: boolean
  // <-- sequence -->
  mediaNode: CT_TLCommonMediaNodeData
}

// CT_TLMediaNodeVideo
export interface CT_TLMediaNodeVideo {
  type: 'video'
  // <-- attrs -->
  fullScrn?: boolean
  // <-- sequence -->
  mediaNode: CT_TLCommonMediaNodeData
}

/** 匹配的常量, 不用于选项, 一定是基于 xml 原数据的 */
export const EntranceAnimationTypes = {
  ['appear']: {
    ['id']: 1,
    ['subTypes']: null,
    ['params']: null,
  },
  ['fadeIn']: {
    ['id']: 10,
    ['subTypes']: null,
    ['params']: null,
  },
  ['flyIn']: {
    ['id']: 2,
    ['subTypes']: {
      ['fromTop']: 1,
      ['fromBottom']: 4,
      ['fromLeft']: 8,
      ['fromRight']: 2,
      ['fromTopLeft']: 9,
      ['fromTopRight']: 3,
      ['fromBottomLeft']: 12,
      ['fromBottomRight']: 6,
    },
    ['params']: null,
  },
  ['floatIn-Up']: {
    ['id']: 42,
    ['subTypes']: null,
    ['params']: null,
  },
  ['floatIn-Down']: {
    ['id']: 47,
    ['subTypes']: null,
    ['params']: null,
  },
} as const

export const ExitAnimationTypes = {
  ['disappear']: {
    ['id']: 1,
    ['subTypes']: null,
    ['param']: null,
  },
  ['fadeOut']: {
    ['id']: 10,
    ['subTypes']: null,
    ['params']: null,
  },
  ['flyOut']: {
    ['id']: 2,
    ['subTypes']: {
      ['toTop']: 1,
      ['toBottom']: 4,
      ['toLeft']: 8,
      ['toRight']: 2,
      ['toTopLeft']: 9,
      ['toTopRight']: 3,
      ['toBottomLeft']: 12,
      ['toBottomRight']: 6,
    },
    ['params']: null,
  },
  ['floatOut-Down']: {
    ['id']: 42,
    ['subTypes']: null,
    ['params']: null,
  },
  ['floatOut-Up']: {
    ['id']: 47,
    ['subTypes']: null,
    ['params']: null,
  },
} as const

export const EmphasisAnimationTypes = {
  ['spin']: {
    ['id']: 8,
    ['subTypes']: null,
    ['params']: { rot: 360 },
  },
  ['growShrink']: {
    ['id']: 6,
    ['subTypes']: null,
    ['params']: { sx: 1.5, sy: 1.5 },
  },
  ['blink']: {
    ['id']: 35,
  },
} as const

/** 部分动画采用种类+设置为联合主键，没有 subType，此处为了兼容 ui 数据结构 */
export const CustomAnimTypes = {
  ['Entrance']: {
    ['FloatIn']: 999,
  },
  ['Exit']: {
    ['FloatOut']: 998,
  },
  ['Emphasis']: {},
}

export const SlideAnimationTypes = {
  ['Entrance']: EntranceAnimationTypes,
  ['Exit']: ExitAnimationTypes,
  ['Emphasis']: EmphasisAnimationTypes,
  // path
  // verb
  // mediacall
} as const

export type AnimClass = keyof typeof SlideAnimationTypes
export type AnimTypes<animClass extends AnimClass> =
  keyof typeof SlideAnimationTypes[animClass]
export const FloatInDirection = {
  up: EntranceAnimationTypes['floatIn-Up']['id'],
  down: EntranceAnimationTypes['floatIn-Down']['id'],
} as const

export const FloatOutDirection = {
  up: ExitAnimationTypes['floatOut-Up']['id'],
  down: ExitAnimationTypes['floatOut-Down']['id'],
} as const
