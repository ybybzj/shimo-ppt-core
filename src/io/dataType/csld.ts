import {
  UniNvPrData,
  CSpPrData,
  CGrpSpPrData,
  CShapeStyleData,
  CBlipFillData,
  CXfrmData,
  CBgData,
  SpTypes,
  ImageSubTypes,
} from './spAttrs'
import {
  InsertOperationWithType,
  JSONOpData,
  MapOpData,
  DeltaOpData,
  InsertOperation,
  EmptyJSONData,
  EmptyDeltaData,
} from '../operation'

import { TableData } from './table'
import { TextBodyData } from './text'
import { Dict } from '../../../liber/pervasive'

// shape
export interface ShapeAttrsData {
  useBgFill?: boolean
}

export type ShapeData = [
  InsertOperationWithType<'attrs', JSONOpData<ShapeAttrsData>>,
  InsertOperationWithType<'nvSpPr', JSONOpData<{ nvSpPr: UniNvPrData }>>,
  InsertOperationWithType<'spPr', JSONOpData<{ spPr: CSpPrData }>>,
  InsertOperationWithType<'style', JSONOpData<CShapeStyleData>>,
  InsertOperationWithType<'txBody', DeltaOpData<TextBodyData> | EmptyDeltaData>,
]

// group shape
export type GrpShapeData = [
  InsertOperationWithType<'nvGrpSpPr', JSONOpData<{ nvGrpSpPr: UniNvPrData }>>,
  InsertOperationWithType<'grpSpPr', JSONOpData<{ grpSpPr: CGrpSpPrData }>>,
  InsertOperationWithType<'spTree', MapOpData<SpItem>>,
]

// connection shape
export type CxnShapeData = [
  InsertOperationWithType<'nvCxnSpPr', JSONOpData<{ nvSpPr: UniNvPrData }>>,
  InsertOperationWithType<'spPr', JSONOpData<{ spPr: CSpPrData }>>,
  InsertOperationWithType<'style', JSONOpData<CShapeStyleData>>,
]

// image

export type ImageShapeData = [
  InsertOperationWithType<
    'nvPicPr',
    JSONOpData<{ nvPicPr: UniNvPrData }> | EmptyJSONData
  >,
  InsertOperationWithType<
    'blipFill',
    JSONOpData<{ blipFill: CBlipFillData }> | EmptyJSONData
  >,
  InsertOperationWithType<
    'spPr',
    JSONOpData<{ spPr: CSpPrData }> | EmptyJSONData
  >,
  InsertOperationWithType<'style', JSONOpData<CShapeStyleData> | EmptyJSONData>,
]

// GraphicFrame

export interface GraphicFrameAttrs {
  nvGraphicFramePr?: UniNvPrData
  xfrm?: CXfrmData
}

export type GraphicFrameData = [
  InsertOperationWithType<'attrs', JSONOpData<GraphicFrameAttrs>>,
  InsertOperationWithType<
    'graphic',
    DeltaOpData<TableData> | EmptyDeltaData,
    { type?: 'table' }
  >,
]

export interface OLEObjectAttrs {
  link?: string
  encryptUrl?: string
  extra?: {
    img_w?: number
    img_h?: number
    name?: string
    ole_obj_control?: string
    prog_id?: string
    show_as_icon?: boolean
  } // 无损导入导出的任意数据，前端透传
}
export type OLEObjectData = [
  InsertOperationWithType<'attrs', JSONOpData<OLEObjectAttrs>>,
  InsertOperationWithType<'pic', DeltaOpData<ImageShapeData>>,
]

/* -------- 目前只有离线客户端会有这类数据----------> */
export type ChartAttrs = {
  link?: string // 关联的xslx文件地址
  data?: Dict<any> // 图表绘制数据
}

export type ChartObjectData = [
  InsertOperationWithType<'attrs', JSONOpData<ChartAttrs>>,
  InsertOperationWithType<'pic', DeltaOpData<ImageShapeData>>, // 图表绘制完成的图片信息
]
/* <-------- 目前只有离线客户端会有这类数据----------*/

// CGraphicObject
type GraphicObject<
  SpType extends SpTypes,
  SpData extends
    | ShapeData
    | ImageShapeData
    | OLEObjectData
    | GraphicFrameData
    | GrpShapeData
    | CxnShapeData
    | ChartObjectData,
> = [
  InsertOperation<
    DeltaOpData<SpData>,
    {
      order: number
      spType: SpType
      subType?: ImageSubTypes
    }
  >,
]

export type SpItem =
  | GraphicObject<'sp', ShapeData>
  | GraphicObject<'pic', ImageShapeData>
  | GraphicObject<'ole', OLEObjectData>
  | GraphicObject<'graphicFrame', GraphicFrameData>
  | GraphicObject<'grpSp', GrpShapeData>
  | GraphicObject<'cxnSp', CxnShapeData>
  | GraphicObject<'chart', ChartObjectData>

// csld

export interface CSldAttrData {
  name?: string
  bg?: CBgData
  nvGrpSpPr?: UniNvPrData
  grpSpPr?: CSpPrData
}
export type CSldData = [
  InsertOperationWithType<'attrs', JSONOpData<CSldAttrData>>,
  InsertOperationWithType<'spTree', MapOpData<SpItem>>,
]
