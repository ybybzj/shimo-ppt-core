import { Dict } from '../../../liber/pervasive'
import { InsertOperation, JSONOpData } from '../operation'

export interface CommentsAuthorData {
  id: number
  name: string
  initials: string // todo
  lastidx: number
}

export type CmContentData = LocalCmConotentData | ExtCmConotentData

export interface LocalCmConotentData {
  origin: 'local'
  authorId: number
  idx: number
  uid: string // 唯一标识 id, 原生评论拟 4 位 authorId + 4 位 idx 组成(不足位用 0 填充)
  pos: { x: number; y: number }
  text: string
  additionalData?: string // 对应 OOXML 扩展中的 presenceInfo: userId
  dt?: string
  parentAuthorId?: number
  parentCommentId?: number // parent's uid
  timeZoneBias?: string
}

export interface ExtCmConotentData {
  origin: 'server'
  uid: string // 石墨评论 uid
  pos: { x: number; y: number }
  textExt: any // transparent in/out
}

export type CommentsData = [InsertOperation<JSONOpData<Dict<CmContentData>>>]

export const CommentsDataLength = 1
