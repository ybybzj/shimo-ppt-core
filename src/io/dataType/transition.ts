type CT_OrientationTransitionKind = 'blinds' | 'checker' | 'comb' | 'randomBar'
type CT_SimpleTransition_Kind =
  | 'circle'
  | 'dissolve'
  | 'diamond'
  | 'newsflash'
  | 'plus'
  | 'random'
  | 'wedge'
type CT_EightDirectionTransitionKind = 'cover' | 'pull'
type CT_OptionalBlackTransitionKind = 'cut' | 'fade'
type CT_SideDirectionTransitionKind = 'push' | 'wipe'
type CT_SplitTransitionKind = 'split'
type CT_CornerDirectionTransitionKind = 'strips'
type CT_WheelTransitionKind = 'wheel' | 'wheelReverse'
type CT_InOutTransitionKind = 'zoom' | 'warp'

export type SlideTransitionKind =
  | CT_OrientationTransitionKind
  | CT_SimpleTransition_Kind
  | CT_EightDirectionTransitionKind
  | CT_OptionalBlackTransitionKind
  | CT_SideDirectionTransitionKind
  | CT_SplitTransitionKind
  | CT_CornerDirectionTransitionKind
  | CT_WheelTransitionKind
  | CT_InOutTransitionKind

type ST_Direction = 'horz' | 'vert'
export const ST_Direction_Default = 'horz'
type ST_TransitionInOutDirectionType = 'out' | 'in'
export const ST_TransitionInOutDirection_Default = 'out'
type ST_TransitionSideDirectionType = 'l' | 'r' | 'u' | 'd'
export const ST_TransitionSideDirection_Default = 'l'
type ST_TransitionCornerDirectionType = 'lu' | 'ru' | 'ld' | 'rd'
export const ST_TransitionCornerDirection_Default = 'lu'
type ST_TransitionEightDirectionType =
  | ST_TransitionSideDirectionType
  | ST_TransitionCornerDirectionType
export const ST_TransitionEightDirection_Default = 'l'
export interface CT_OrientationTransition {
  kind: CT_OrientationTransitionKind
  dir?: ST_Direction
}

export interface CT_SimpleTransition {
  kind: CT_SimpleTransition_Kind
}

export interface CT_EightDirectionTransition {
  kind: CT_EightDirectionTransitionKind
  dir?: ST_TransitionEightDirectionType
}

export interface CT_OptionalBlackTransition {
  kind: CT_OptionalBlackTransitionKind
  thruBlk?: boolean //default is false
}

export interface CT_SideDirectionTransition {
  kind: CT_SideDirectionTransitionKind
  dir?: ST_TransitionSideDirectionType
}

export interface CT_SplitTransition {
  kind: CT_SplitTransitionKind
  orient?: ST_Direction
  dir?: ST_TransitionInOutDirectionType
}

export interface CT_CornerDirectionTransition {
  kind: CT_CornerDirectionTransitionKind
  dir?: ST_TransitionCornerDirectionType
}
export interface CT_WheelTransition {
  kind: CT_WheelTransitionKind
  spokes?: number
}

export interface CT_InOutTransition {
  kind: CT_InOutTransitionKind
  dir?: ST_TransitionInOutDirectionType
}

type ST_TransitionSpeed = 'slow' | 'med' | 'fast'

export type SlideTransitionOption =
  | CT_OrientationTransition
  | CT_SimpleTransition
  | CT_EightDirectionTransition
  | CT_OptionalBlackTransition
  | CT_SideDirectionTransition
  | CT_SplitTransition
  | CT_CornerDirectionTransition
  | CT_WheelTransition
  | CT_InOutTransition

export interface SlideTransitionData {
  transition?: SlideTransitionOption
  spd?: ST_TransitionSpeed
  advClick?: boolean
  advTm?: number

  // 微软的扩展字段
  requires?: string
  dur?: number
}
