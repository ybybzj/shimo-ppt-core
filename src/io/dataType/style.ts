import { AlignKind, VertAlignType } from '../../core/common/const/attrs'
import { NULL_VALUE, NullableJSONValue } from '../utils'
import {
  CFillEffectsData,
  StyleRefData,
  CBulletData,
  FontRefData,
  CLnData,
  ComplexColorData,
  HyperlinkPropsData,
  TextVerticalType,
  TextAnchorType,
} from './spAttrs'
import { TextTabAlign } from '../../core/common/const/paragraph'
import { FontHintValues } from '../../fonts/const'
import { valuesOfDict } from '../../../liber/pervasive'

export type TextListStyleData = Array<NULL_VALUE | CParaPrData>

export interface CParaIndData {
  left?: number
  right?: number
  firstLine?: number
}

export interface CParaSpacingData {
  lineSpacing?: Space
  spaceBefore?: Space
  spaceAfter?: Space
}

export interface CHexColorData {
  r?: number
  g?: number
  b?: number
  auto: boolean
}

export interface CReviewInfoData {
  userId: string
  userName: string
  dateTime: string
}

export interface CShdData {
  value: number
  color?: CHexColorData
  unifill?: CFillEffectsData
  fillRef?: StyleRefData
}

export interface CTableCellBorderData {
  lineRef?: StyleRefData
  ln?: CLnData

  // compute from ln
  // unifill?: CUniFillData
  // size?: number
  // value?: number

  // ignore
  // space?: number
  // color?: CDocumentColorData
}

export interface CTextStylesData {
  titleStyle?: TextListStyleData
  bodyStyle?: TextListStyleData
  otherStyle?: TextListStyleData
}

export type CParaTabsData = CParaTabData[]

type AlignTypeString =
  | 'l'
  | 'ctr'
  | 'r'
  | 'just'
  | 'justLow'
  | 'dist'
  | 'thaiDist'

// eslint-disable-next-line @typescript-eslint/no-unused-vars
type FontAlignType = 'auto' | 't' | 'ctr' | 'base' | 'b'

export type JCAlignTypeValues = valuesOfDict<typeof AlignKind> | -1 //unset

export function fromJC(align: AlignTypeString): JCAlignTypeValues {
  switch (align) {
    case 'l':
      return AlignKind.Left
    case 'ctr':
      return AlignKind.Center
    case 'r':
      return AlignKind.Right
    case 'just':
    case 'justLow':
      return AlignKind.Justify
    case 'dist':
    case 'thaiDist':
      return AlignKind.Dist
    default:
      return AlignKind.Center
  }
}

export function toJC(align: JCAlignTypeValues): AlignTypeString {
  switch (align) {
    case AlignKind.Left:
      return 'l'
    case AlignKind.Center:
      return 'ctr'
    case AlignKind.Right:
      return 'r'
    case AlignKind.Justify:
      return 'just'
    case AlignKind.Dist:
      return 'dist'
    default:
      return 'l'
  }
}

type LineSpacingType = 'percent' | 'point'

interface Space {
  value: number
  type: LineSpacingType
}

type TextTabAlignType = 'l' | 'ctr' | 'r' | 'dec'
export type TextTabAlignTypeValues = valuesOfDict<typeof TextTabAlign>

export function fromTextTabAlignType(
  t: TextTabAlignType
): TextTabAlignTypeValues {
  switch (t) {
    case 'ctr':
      return TextTabAlign.Center
    case 'l':
      return TextTabAlign.Left
    case 'r':
      return TextTabAlign.Right
    case 'dec':
      return TextTabAlign.Clear
  }
}

export function toTextTabAlignType(
  n: TextTabAlignTypeValues
): TextTabAlignType | undefined {
  switch (n) {
    case TextTabAlign.Clear:
      return 'dec'
    case TextTabAlign.Left:
      return 'l'
    case TextTabAlign.Right:
      return 'r'
    case TextTabAlign.Center:
      return 'ctr'
  }
}
export interface CParaTabData {
  position?: number
  algn?: TextTabAlignType
}

type Tabs = CParaTabData[]

export interface CParaPrData {
  // ind
  marL?: number
  marR?: number
  indent?: number
  // jc
  algn?: AlignTypeString

  lvl?: number
  // sapcing
  lineSpacing?: Space
  spaceBefore?: Space
  spaceAfter?: Space

  defTabSz?: number
  rtl?: boolean
  // eaLnBrk?: boolean // OO: 未处理
  // fontAlgn?: FontAlignType // OO: 未处理
  // latinLnBrk?: boolean // OO: 未处理
  // hangingPunct?: boolean // OO: 未处理

  bullet?: CBulletData
  tabs?: Tabs
  defaultRunPr?: CTextPrData
}

export interface CRFontsData {
  ascii?: string
  eastAsia?: string
  hAnsi?: string
  cs?: string
  hint?: FontHintValues
}

export interface CLangData {
  bidi?: string
  eastAsia?: string
  val?: string
}

export type UnderlineType =
  | 'none'
  | 'words'
  | 'sng'
  | 'dbl'
  | 'heavy'
  | 'dotted'
  | 'dottedHeavy'
  | 'dash'
  | 'dashHeavy'
  | 'dashLong'
  | 'dashLongHeavy'
  | 'dotDash'
  | 'dotDashHeavy'
  | 'dotDotDash'
  | 'dotDotDashHeavy'
  | 'wavy'
  | 'wavyHeavy'
  | 'wavyDbl'

export type StrikeType = 'noStrike' | 'sngStrike' | 'dblStrike'

export type CapType = 'none' | 'small' | 'all'
type ULineType = 'followText' | 'stroke'

type ULine = CLnData & { type: ULineType }

type UFillType = 'followText' | 'fill'

type UFill = CFillEffectsData & { type: UFillType }

export interface TextPrFont {
  typeface?: string
  panose?: number
  pitchFamily?: number
  charset?: number
}

export function toCapType(
  cap: boolean | undefined,
  smallCap: boolean | undefined
): CapType | undefined {
  if (cap === true) return 'all'
  else if (smallCap === true) return 'small'
  else if (cap === false && smallCap === false) return 'none'
}
export function fromCapType(t: CapType): { cap: boolean; smallCap: boolean } {
  if (t === 'all') {
    return {
      cap: true,
      smallCap: false,
    }
  } else if (t === 'small') {
    return {
      cap: false,
      smallCap: true,
    }
  } else {
    //t === 'none'
    return {
      cap: false,
      smallCap: false,
    }
  }
}
export function toStrikeType(
  strikeout: boolean | undefined
): StrikeType | undefined {
  let strike
  if (strikeout === true) strike = 'sngStrike'
  else if (strikeout === false) strike = 'noStrike'
  return strike
}

export function fromStrikeType(t: StrikeType): { strikeout: boolean } {
  if (t === 'sngStrike') {
    return {
      strikeout: true,
    }
  } else {
    return {
      strikeout: false,
    }
  }
}
export function toTextPrSpacing(n: number): number {
  return ((n * 7200) / 25.4) >> 0
}

export function fromTextPrSpacing(n: number): number {
  return (n * 25.4) / 7200
}
export function toUnderlineType(underline: boolean): UnderlineType {
  return underline === true ? 'sng' : 'none'
}

export function fromUnderlineType(t: UnderlineType): boolean {
  return t === 'none' ? false : true
}

export type TextPrVertAlign =
  | typeof VertAlignType.SubScript
  | typeof VertAlignType.SuperScript
  | typeof VertAlignType.Baseline

export function toBaseline(n: TextPrVertAlign): number | undefined {
  switch (n) {
    case VertAlignType.SubScript:
      return -25000
    case VertAlignType.SuperScript:
      return 30000
  }
}

export function fromBaseline(baseline: number): TextPrVertAlign | undefined {
  if (baseline < 0) return VertAlignType.SubScript
  else if (baseline > 0) return VertAlignType.SuperScript
  else return VertAlignType.Baseline
}

export function toTextPrFontSize(n: number): number {
  return (n * 100) >> 0
}

export function fromTextPrFontSize(n: number): number {
  let size = n / 100
  size = (size * 2 + 0.5) >> 0
  size /= 2
  return size
}

export interface CTextPrData {
  lang?: string

  fontSize?: number
  bold?: boolean
  italic?: boolean
  underline?: UnderlineType
  strike?: StrikeType
  cap?: CapType
  spc?: number
  baseline?: number

  ln?: CLnData
  fill?: CFillEffectsData

  latin?: TextPrFont
  ea?: TextPrFont
  cs?: TextPrFont
  hlinkClick?: HyperlinkPropsData

  // oo
  fontFamily?: { Name: string; Index: number }
  // 暂不支持
  sym?: TextPrFont
  normalizeH?: boolean
  kern?: number
  highlight?: ComplexColorData
  noProof?: boolean
  dirty?: boolean
  err?: boolean
  smtClean?: boolean
  smtId?: number
  bmk?: string
  uLine?: ULine
  uFill?: UFill
  hlinkMouseOver?: HyperlinkPropsData
  kumimoji?: boolean
  altLang?: string
}

// ---------------------------------------------------------------------------

export interface TableStyleData {
  id: string // GUID
  name?: NullableJSONValue<string>
  tblBgFill?: CFillEffectsData
  tblBgFillRef?: StyleRefData
  tableWholeTable?: CTableStylePrData
  tableBand1Horz?: CTableStylePrData
  tableBand2Horz?: CTableStylePrData
  tableBand1Vert?: CTableStylePrData
  tableBand2Vert?: CTableStylePrData
  tableLastCol?: CTableStylePrData
  tableFirstCol?: CTableStylePrData
  tableLastRow?: CTableStylePrData
  tableFirstRow?: CTableStylePrData
  tableNWCell?: CTableStylePrData // TL / nw
  tableNECell?: CTableStylePrData // TR / ne
  tableSWCell?: CTableStylePrData // BL / sw
  tableSECell?: CTableStylePrData // BR / se
}

export interface CTableMeasurementData {
  type: number
  w: number
}

export interface CTableCellPrData {
  fill?: CFillEffectsData
  fillRef?: StyleRefData
  marL?: number
  marR?: number
  marT?: number
  marB?: number
  vert?: TextVerticalType
  anchor?: TextAnchorType

  lnL?: CLnData
  lnR?: CLnData
  lnT?: CLnData
  lnB?: CLnData

  // unsupported
  // anchorCtl?: boolean
  // horzOverflow?
  // cell3D?
  // lnTlToBr?
  // lnBlToTr?
}

export interface CTablePrData {
  tableStyleColBandSize?: number
  tableStyleRowBandSize?: number
  jc?: number
  shd?: CShdData
  tableBorders?: {
    bottom?: CTableCellBorderData
    left?: CTableCellBorderData
    right?: CTableCellBorderData
    top?: CTableCellBorderData
    insideH?: CTableCellBorderData
    insideV?: CTableCellBorderData
  }
  tableCellMar?: {
    bottom?: CTableMeasurementData
    left?: CTableMeasurementData
    right?: CTableMeasurementData
    top?: CTableMeasurementData
  }
}

export interface CTableRowHeightData {
  value: number
  hRule: number
}

type OnOffStyleType = 'on' | 'off' | 'def'
export interface TableCellTextStyle {
  bold?: OnOffStyleType
  italic?: OnOffStyleType
  fontRef?: FontRefData
  color?: ComplexColorData
  latin?: TextPrFont
  ea?: TextPrFont
  cs?: TextPrFont
}

export function toOnOffStyleType(b: boolean): OnOffStyleType {
  return b ? 'on' : 'off'
}

export function fromOnOffStyleType(t: OnOffStyleType): boolean {
  return t === 'on' ? true : false
}

export interface TableCellBorderStyle {
  left?: CTableCellBorderData
  right?: CTableCellBorderData
  top?: CTableCellBorderData
  bottom?: CTableCellBorderData
  insideH?: CTableCellBorderData
  insideV?: CTableCellBorderData
}

export interface TableCellStyle {
  fill?: CFillEffectsData
  fillRef?: StyleRefData
  borders?: TableCellBorderStyle
}
export interface CTableStylePrData {
  txStyle?: TableCellTextStyle
  cellStyle?: TableCellStyle
}

export interface CTableRowPrData {
  height?: CTableRowHeightData
}
