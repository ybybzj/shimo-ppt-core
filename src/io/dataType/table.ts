import { CTableCellPrData, TableStyleData } from './style'
import {
  InsertOperationWithType,
  JSONOpData,
  DeltaOpData,
  InsertOperation,
} from '../operation'
import { TextBodyData } from './text'
import { CFillEffectsData } from './spAttrs'

export interface TableLookData {
  firstCol?: boolean
  firstRow?: boolean
  lastCol?: boolean
  lastRow?: boolean
  bandRow?: boolean
  bandCol?: boolean
  rtl?: boolean
}

export interface TablePrData {
  style?: TableStyleData
  look?: TableLookData
  fill?: CFillEffectsData
  //rtl?: boolean
}

export type TableData = [
  InsertOperationWithType<'attrs', JSONOpData<TablePrData>>,
  InsertOperationWithType<'grid', JSONOpData<{ grid: number[] }>>,
  InsertOperationWithType<'rows', DeltaOpData<TableRowData[]>>
]

export interface TableRowAttrs {
  height?: number
}

export type TableRowData = InsertOperation<
  DeltaOpData<
    [
      InsertOperationWithType<'attrs', JSONOpData<TableRowAttrs>>,
      InsertOperationWithType<'cells', DeltaOpData<CellItemData[]>>
    ]
  >,
  { type: 'row' }
>

export interface TableCellAttrs {
  rowSpan?: number // 垂直合并个数
  gridSpan?: number //水平合并个数
  hMerge?: boolean //是否水平合并
  vMerge?: boolean //是否垂直合并
  pr?: CTableCellPrData
}

export type TableCellData = InsertOperation<
  DeltaOpData<
    [
      InsertOperationWithType<'attrs', JSONOpData<TableCellAttrs>>,
      InsertOperationWithType<'txBody', DeltaOpData<TextBodyData>>
    ]
  >,
  { type: 'cell' }
>
// 当有水平方向的合并单元格时，合并在一起的单元格除了最左边的一个除外，其余单元格均视为空单元格
export type TableEmptyCellData = InsertOperation<
  1,
  {
    type: 'cell'
    isEmpty: true
  }
>

export type CellItemData = TableCellData | TableEmptyCellData
