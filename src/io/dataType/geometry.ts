export interface GeomGuide {
  name: string
  fmla?: string
  formula?: string // 为了兼容老格式
}

export type AdjCoordinate = string | number
type GeomGuideName = string

export const GDFormulaType = {
  MULT_DIV: 0,
  PLUS_MINUS: 1,
  PLUS_DIV: 2,
  IF_ELSE: 3,
  ABS: 4,
  AT2: 5,
  CAT2: 6,
  COS: 7,
  MAX: 8,
  MOD: 9,
  PIN: 10,
  SAT2: 11,
  SIN: 12,
  SQRT: 13,
  TAN: 14,
  VALUE: 15,
  MIN: 16,
} as const

export const GDFormulaTypeStrMap = {
  [GDFormulaType.MULT_DIV]: '*/',
  [GDFormulaType.PLUS_MINUS]: '+-',
  [GDFormulaType.PLUS_DIV]: '+/',
  [GDFormulaType.IF_ELSE]: '?:',
  [GDFormulaType.ABS]: 'abs',
  [GDFormulaType.AT2]: 'at2',
  [GDFormulaType.CAT2]: 'cat2',
  [GDFormulaType.COS]: 'cos',
  [GDFormulaType.MAX]: 'max',
  [GDFormulaType.MOD]: 'mod',
  [GDFormulaType.PIN]: 'pin',
  [GDFormulaType.SAT2]: 'sat2',
  [GDFormulaType.SIN]: 'sin',
  [GDFormulaType.SQRT]: 'sqrt',
  [GDFormulaType.TAN]: 'tan',
  [GDFormulaType.VALUE]: 'val',
  [GDFormulaType.MIN]: 'min',
} as const

export const GDFormulaStringTypeMap = Object.keys(GDFormulaTypeStrMap).reduce(
  (result, t) => {
    const type = Number(t)
    if (typeof type === 'number') {
      result[GDFormulaTypeStrMap[type]] = type
    }
    return result
  },
  {}
)

export type GDFormulaTypeValue =
  typeof GDFormulaType[keyof typeof GDFormulaType]

export interface GDFormualInfo {
  formula: GDFormulaTypeValue
  x?: string
  y?: string
  z?: string
}

export function getGeomGuideFmlaInfo(fmla: string): GDFormualInfo | undefined {
  if (typeof fmla !== 'string') {
    return
  }
  const tokens = fmla.trim().split(' ')
  if (tokens.length <= 0 || GDFormulaStringTypeMap[tokens[0]] == null) {
    return
  }

  return {
    formula: GDFormulaStringTypeMap[tokens[0]],
    x: tokens[1],
    y: tokens[2],
    z: tokens[3],
  }
}

export function GeomGuideFmlaFromInfo(gdInfo: GDFormualInfo): string {
  const fstr = GDFormulaTypeStrMap[gdInfo.formula]
  const xstr = gdInfo.x != null ? ' ' + gdInfo.x : ''
  const ystr = gdInfo.y != null ? ' ' + gdInfo.y : ''
  const zstr = gdInfo.z != null ? ' ' + gdInfo.z : ''
  return `${fstr}${xstr}${ystr}${zstr}`
}

export const AdjustHandleType = {
  XY: 0,
  Polar: 1,
} as const

export interface XYAdjustHandleData {
  type: typeof AdjustHandleType.XY
  x: AdjCoordinate
  y: AdjCoordinate
  gdRefX?: GeomGuideName
  minX?: AdjCoordinate
  maxX?: AdjCoordinate
  gdRefY?: GeomGuideName
  minY?: AdjCoordinate
  maxY?: AdjCoordinate
}

export interface PolarAdjustHandleData {
  type: typeof AdjustHandleType.Polar
  x: AdjCoordinate
  y: AdjCoordinate
  gdRefR?: GeomGuideName
  minR?: AdjCoordinate
  maxR?: AdjCoordinate
  gdRefAng?: GeomGuideName
  minAng?: AdjCoordinate
  maxAng?: AdjCoordinate
}

export interface XYAdjustHandleInfo {
  gdRefX?: GeomGuideName
  minX?: AdjCoordinate
  maxX?: AdjCoordinate

  gdRefY?: GeomGuideName
  minY?: AdjCoordinate
  maxY?: AdjCoordinate

  posX: AdjCoordinate
  posY: AdjCoordinate
}

export interface PolarAdjustHandleInfo {
  gdRefAng?: GeomGuideName
  minAng?: AdjCoordinate
  maxAng?: AdjCoordinate

  gdRefR?: GeomGuideName
  minR?: AdjCoordinate
  maxR?: AdjCoordinate

  posX: AdjCoordinate
  posY: AdjCoordinate
}

export function toXYAdjustHandleData(
  info: XYAdjustHandleInfo
): XYAdjustHandleData {
  return {
    type: AdjustHandleType.XY,
    x: info.posX,
    y: info.posY,
    gdRefX: info.gdRefX,
    minX: info.minX,
    maxX: info.maxX,
    gdRefY: info.gdRefY,
    minY: info.minY,
    maxY: info.maxY,
  }
}

export function fromXYAdjustHandleData(
  data: XYAdjustHandleData
): XYAdjustHandleInfo {
  return {
    gdRefX: data.gdRefX,
    minX: data.minX,
    maxX: data.maxX,

    gdRefY: data.gdRefY,
    minY: data.minX,
    maxY: data.maxY,

    posX: data.x,
    posY: data.y,
  }
}

export function toPolarAdjustHandleData(
  info: PolarAdjustHandleInfo
): PolarAdjustHandleData {
  return {
    type: AdjustHandleType.Polar,
    x: info.posX,
    y: info.posY,
    gdRefR: info.gdRefR,
    minR: info.minR,
    maxR: info.maxR,
    gdRefAng: info.gdRefAng,
    minAng: info.minAng,
    maxAng: info.maxAng,
  }
}

export function fromPolarAdjustHandleData(
  data: PolarAdjustHandleData
): PolarAdjustHandleInfo {
  return {
    gdRefAng: data.gdRefAng,
    minAng: data.minAng,
    maxAng: data.maxAng,

    gdRefR: data.gdRefR,
    minR: data.minR,
    maxR: data.maxR,

    posX: data.x,
    posY: data.y,
  }
}
export type AdjustHandleData = XYAdjustHandleData | PolarAdjustHandleData

export type AdjAngle = string | number

export interface ConnectionSite {
  x: AdjCoordinate
  y: AdjCoordinate
  ang?: AdjAngle
}

export interface GeomRect {
  l: AdjCoordinate
  t: AdjCoordinate
  r: AdjCoordinate
  b: AdjCoordinate
}

// path

export type PathFillMode =
  | 'none'
  | 'norm'
  | 'lighten'
  | 'lightenLess'
  | 'darken'
  | 'darkenLess'

type PositiveCoordinate = number

interface Path2DClose {
  type: 'close'
}
interface Path2DMoveTo {
  type: 'moveTo'
  x: AdjCoordinate
  y: AdjCoordinate
}
interface Path2DLineTo {
  type: 'lnTo'
  x: AdjCoordinate
  y: AdjCoordinate
}

interface Path2DArcTo {
  type: 'arcTo'
  wR: AdjCoordinate
  hR: AdjCoordinate
  stAng: AdjAngle
  swAng: AdjAngle
}

interface Path2DQuadBezierTo {
  type: 'quadBezTo'
  pts: Array<{ x: AdjCoordinate; y: AdjCoordinate }>
}

interface Path2DCubicBezierTo {
  type: 'cubicBezTo'
  pts: Array<{ x: AdjCoordinate; y: AdjCoordinate }>
}

export type Path2DOperation =
  | Path2DClose
  | Path2DMoveTo
  | Path2DLineTo
  | Path2DArcTo
  | Path2DQuadBezierTo
  | Path2DCubicBezierTo

export interface Path2DData {
  w?: PositiveCoordinate
  h?: PositiveCoordinate
  fill?: PathFillMode
  stroke?: boolean
  extrusionOk?: boolean
  cmds?: Path2DOperation[]
}

export interface GeometryData {
  preset?: string // 如果有值则表示 prstGeometry, 否则是custGeometry
  avLst?: GeomGuide[]
  gdLst?: GeomGuide[]
  ahLst?: AdjustHandleData[]
  cxnLst?: ConnectionSite[]
  rect?: GeomRect
  pathLst?: Path2DData[]
}
