import { Dict } from '../../liber/pervasive'
import { SlideComments } from '../core/Slide/Comments/SlideComments'
import { Notes } from '../core/Slide/Notes'
import { NotesMaster } from '../core/Slide/NotesMaster'
import { Presentation } from '../core/Slide/Presentation'
import { Slide } from '../core/Slide/Slide'
import { SlideLayout } from '../core/Slide/SlideLayout'
import { SlideMaster } from '../core/Slide/SlideMaster'
import { Theme } from '../core/SlideElement/attrs/theme'

export interface PPTInfoData {
  themes: Theme[]
  slideMasters: SlideMaster[]
  slides: Slide[]
  notes: Notes[]
  noteMasters: NotesMaster[]
  layouts: SlideLayout[]
  comments: SlideComments[]
}

export function genPPTInfoData(presentation: Presentation) {
  const slideMastersData: SlideMaster[] = []
  const layoutsData: SlideLayout[] = []
  const slidesData: Slide[] = []
  const notesData: Notes[] = []
  const notesMastersData: NotesMaster[] = []
  const commentsData: SlideComments[] = []

  const slides = presentation.slides
  const slideMasters = presentation.slideMasters
  const notesMasters = presentation.notesMasters

  slideMasters.forEach((slideMaster) => {
    slideMastersData.push(slideMaster)
    for (const layout of slideMaster.layouts) {
      layoutsData.push(layout)
    }
  })

  slides.forEach((slide, i) => {
    slidesData[i] = slide
    if (slide.notes && !slide.notes.isEmptyNotes()) {
      notesData.push(slide.notes!)
    }

    if (slide.slideComments && slide.slideComments.comments.length) {
      commentsData.push(slide.slideComments)
    }
  })

  notesMasters.forEach((noteMaster) => {
    notesMastersData.push(noteMaster)
  })
  // collect themes

  const themes: Dict<Theme> = {}

  const mastersLen = slideMastersData.length

  for (let i = 0; i < mastersLen; i++) {
    const t = slideMastersData[i].theme

    if (t == null) {
      continue
    }

    themes[t.getRefId()] = t
  }

  for (let i = 0; i < notesMastersData.length; ++i) {
    const t = notesMastersData[i].theme
    if (t == null) {
      continue
    }

    themes[t.getRefId()] = t
  }

  const themesData = Object.keys(themes).reduce((result, k) => {
    result.push(themes[k])
    return result
  }, [] as Theme[])

  return {
    slideMasters: slideMastersData,
    slides: slidesData,
    notes: notesData, // all have refId set or with a body that is not empty
    noteMasters: notesMastersData,
    layouts: layoutsData,
    themes: themesData,
    comments: commentsData,
  }
}

export function getRefItemMap<T extends { getRefId(): string }>(
  items: T[]
): Dict<T> {
  return items.reduce((result, item) => {
    result[item.getRefId()] = item
    return result
  }, {})
}
