import { ParaRun } from '../core/Paragraph/ParaContent/ParaRun'
import { ParaHyperlink } from '../core/Paragraph/ParaContent/ParaHyperlink'
import { Paragraph } from '../core/Paragraph/Paragraph'
import { Dict, Nullable } from '../../liber/pervasive'
import { readJSON, Operation, Op } from './operation'
import { stringifyValuesOfJSON } from './utils'
import { InstanceType } from '../core/instanceTypes'
import { PresentationField } from '../core/Paragraph/ParaContent/PresentationField'
import { RunContentElement } from '../core/Paragraph/RunElement/type'
import { appendArrays } from '../common/utils'
import { ManagedSliceUtil } from '../common/managedArray'

type ItemWithContents = Paragraph | ParaRun | ParaHyperlink | PresentationField

export type ContentItem = ItemWithContents | RunContentElement
export interface ObjectAtomItem {
  itemType: 'objectItem'
  id: string
  instance: ItemWithContents
}

type RunAtomValueType = 'chars' | 'paraEnd' | 'paraBreak' | 'paraField'

export interface RunAtomItem {
  itemType: 'runItem'
  value: string | number
  valueType: RunAtomValueType
  attributes: Dict<any>
}

function makeRunAtomItem(
  item: RunContentElement,
  attrs: { run: Dict; para: Dict }
): RunAtomItem {
  const isParaEnd =
    item.instanceType === InstanceType.ParaEnd ||
    item.instanceType === InstanceType.ParaNewLine

  const attributes = isParaEnd ? attrs.para : attrs.run

  const { value, type: valueType } = getRunItemValue(item)
  return {
    itemType: 'runItem',
    value,
    valueType,
    attributes,
  }
}

function canMergeToAtomItem(
  currentItem: RunAtomItem,
  item: RunContentElement
): boolean {
  const currentType = currentItem.valueType
  const itemType = item.instanceType
  switch (currentType) {
    case 'chars': {
      return (
        itemType === InstanceType.ParaSpace ||
        itemType === InstanceType.ParaTab ||
        itemType === InstanceType.ParaText
      )
    }
    case 'paraEnd': {
      return itemType === InstanceType.ParaEnd
    }
    case 'paraBreak': {
      return itemType === InstanceType.ParaNewLine
    }
    default:
      return false
  }
}

export interface RetainAtomItem {
  itemType: 'retain'
}

export type AtomItem = ObjectAtomItem | RunAtomItem | RetainAtomItem

export function isObjectContentItem(item: ContentItem): boolean {
  switch (item.instanceType) {
    case InstanceType.ParaHyperlink:
      return true
    default:
      return false
  }
}

interface Options {
  runAttrs?: Dict<any>
  paraAttrs?: Dict<any>
}

function toAtomItems(
  collection: AtomItem[],
  item: ItemWithContents | AtomItem[],
  parentAttr: Options
) {
  if (Array.isArray(item)) {
    if (
      item.length > 0 &&
      (item[0].itemType === 'objectItem' || item[0].itemType === 'runItem')
    ) {
      appendArrays(collection, item)
    }
    return
  }

  let result: Nullable<AtomItem>

  switch (item.instanceType) {
    case InstanceType.ParaHyperlink:
      result = {
        itemType: 'objectItem',
        id: item.id,
        instance: item,
      }
      collection.push(result)
      break

    case InstanceType.Paragraph: {
      ManagedSliceUtil.forEach(item.children, (childItem) =>
        toAtomItems(collection, childItem, parentAttr)
      )
      return
    }

    case InstanceType.ParaRun: {
      const contentLength = item.children.length
      if (contentLength <= 0) {
        return
      }
      const isInHyperlink = item.isInHyperlink()
      const rPr = item.pr
      if (rPr != null && isInHyperlink) {
        const parentHyperlink = item.getParent() as ParaHyperlink
        rPr.hlink = parentHyperlink?.toCTHyperlink()
      }

      const rPrData = readJSON(rPr) || {}
      const pPrData = readJSON(item.paragraph.pr)
      const textPrData = item.paragraph.paraTextPr
        ? readJSON(item.paragraph.paraTextPr.textPr)
        : null

      let runAttrs: Dict = rPrData

      if ((item as PresentationField).paraField === true) {
        const { paraField, Guid, fieldType } = item as PresentationField
        const fieldPPr = (item as PresentationField).pPr
        const fieldPPrData = (fieldPPr && readJSON(fieldPPr)) || {}
        const text = (item as PresentationField).getText()
        runAttrs = {
          paraField,
          id: Guid,
          type: fieldType,
          text,
          pPr: fieldPPrData,
          rPr: rPrData,
        }

        const result: RunAtomItem = {
          itemType: 'runItem',
          value: contentLength,
          valueType: 'paraField',
          attributes: runAttrs,
        }
        collection.push(result)
        return
      }
      const paraAttrs: Dict = {
        pPr: pPrData,
        rPr: rPrData,
      }

      if (textPrData != null) {
        paraAttrs.textPr = textPrData
      }

      const attrs = {
        run: runAttrs,
        para: paraAttrs,
      }

      let result = makeRunAtomItem(item.children.at(0)!, attrs)
      if (result) {
        collection.push(result)
      }
      for (let i = 1; i < contentLength; i++) {
        const nextItem = item.children.at(i)
        if (nextItem == null) continue
        if (canMergeToAtomItem(result, nextItem)) {
          result.value = (result.value as any) + getRunItemValue(nextItem).value
        } else {
          result = makeRunAtomItem(nextItem, attrs)
          if (result) {
            collection.push(result)
          }
        }
      }

      return
    }
  }
}
function getRunItemValue(runItem: RunContentElement): {
  value: 1 | string
  type: RunAtomValueType
} {
  switch (runItem.instanceType) {
    case InstanceType.ParaText:
    case InstanceType.ParaSpace:
    case InstanceType.ParaTab:
      return {
        value: String.fromCodePoint(runItem.value),
        type: 'chars',
      }
    case InstanceType.ParaEnd:
      return {
        value: 1,
        type: 'paraEnd',
      }
    case InstanceType.ParaNewLine:
      return {
        value: 1,
        type: 'paraBreak',
      }
  }
}

const { Retain, Insert } = Op
// const { Delta } = OpData

function opsFromAtomItem(atomItem: AtomItem): Operation[] {
  if (atomItem.itemType === 'retain') {
    return [Retain(1)]
  }
  if (atomItem.itemType === 'objectItem') {
    const item = atomItem.instance
    switch (item.instanceType) {
      case InstanceType.ParaHyperlink:
        return opsFromContentItems(item.children.elements)
      default:
        return []
    }
  } else if (atomItem.itemType === 'runItem') {
    const attributes = atomItem.attributes
    if (attributes && attributes.paraField === true) {
      return [Insert(atomItem.value, stringifyValuesOfJSON(attributes))]
    }
    switch (atomItem.valueType) {
      case 'chars':
        return [Insert(atomItem.value, stringifyValuesOfJSON(attributes))]
      case 'paraEnd':
        attributes.paraEnd = true
        return [Insert(atomItem.value, stringifyValuesOfJSON(attributes))]
      case 'paraBreak':
        attributes.break = true
        return [Insert(atomItem.value, stringifyValuesOfJSON(attributes))]
      default:
        return []
    }
  } else {
    return []
  }
}

function opsFromAtomItems(result: Operation[], atomItems: AtomItem[]) {
  atomItems.forEach((item) => {
    appendArrays(result, opsFromAtomItem(item))
  })
}

export function opsFromContentItems(
  items: readonly ItemWithContents[]
): Operation[] {
  if (items.length <= 0) return []

  const result: Operation[] = []
  items.forEach((item) => {
    const atomItems: AtomItem[] = []
    toAtomItems(atomItems, item, {})
    opsFromAtomItems(result, atomItems)
  })

  return result
}
