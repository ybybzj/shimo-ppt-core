import { SlideAnimation } from '../core/Slide/Animation'

const collection: SlideAnimation[] = []
export const animationFixer = {
  get() {
    return collection
  },
  reset() {
    collection.length = 0
  },
  add(item: SlideAnimation) {
    if (collection.indexOf(item) === -1) {
      collection.push(item)
    }
  },
}
