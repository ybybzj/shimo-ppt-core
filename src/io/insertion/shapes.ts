import { Nullable } from '../../../liber/pervasive'
import { BrowserInfo } from '../../common/browserInfo'
import { ManagedSliceUtil } from '../../common/managedArray'
import { isBase64Url, isNumber } from '../../common/utils'
import { SlideElement } from '../../core/SlideElement/type'
import { Table } from '../../core/Table/Table'
import { calculateRenderingState } from '../../core/calculation/calculate/presentation'
import { calculateSlideElement } from '../../core/calculation/calculate/slideElement'
import { PPT_Default_SIZE } from '../../core/common/const/drawing'
import { Factor_mm_to_pix } from '../../core/common/const/unit'
import { checkAlmostEqual } from '../../core/common/utils'
import { InstanceType } from '../../core/instanceTypes'
import { getSlideElementPreview } from '../../core/render/toImage'
import { isPlaceholder } from '../../core/utilities/shape/asserts'
import { checkExtentsByTextContent } from '../../core/utilities/shape/extents'
import { cloneSpWithFormatting } from '../../core/utilities/shape/formatting'
import {
  applyCxnSpToSpMapState,
  clearCxnState,
  collectCxnStateForSp,
} from '../../core/utilities/shape/manageCxnSpState'

import { changeSlideElementSize } from '../../core/utilities/shape/operations'
import {
  addToParentSpTree,
  setDeletedForSp,
  setParentForSlideElement,
} from '../../core/utilities/shape/updates'
import { EditorKit } from '../../editor/global'
import { IEditorKit } from '../../editor/type'
import { EditorUtil } from '../../globals/editor'
import { getDefaultScaleUpdateInfo } from '../../re/export/Slide'
import { PackedOpDelta, stringToSlice } from '../../re/export/modoc'
import { spKindToType, spTypeToKind } from '../dataType/spAttrs'
import { FromJSONParams } from '../defs/pptdoc'
import { genSp } from '../loaders/csld/groupShape'
import { getRefPresentation } from './refContent'

export type SpPackedData = string

export function packedOpFromSpData(
  spData: SpPackedData
): Nullable<PackedOpDelta> {
  if (spData.length <= 0) return undefined
  const spType = spKindToType(Number(spData[0]) as any)
  if (spType == null) return undefined
  return {
    ['action']: 'insert',
    ['attributes']: {
      ['spType']: spType,
    },
    ['type']: 'delta',
    ['data']: stringToSlice(spData.slice(1)),
  }
}

export function spPackedDataFromTypeAndData(
  spType: string,
  data: string
): Nullable<SpPackedData> {
  if (data.length <= 0) return undefined

  const spKind = spTypeToKind(spType as any)
  if (spKind == null) return undefined

  data = spKind + '' + data
  return data
}

export function slideElementFromPackedOp(
  op: PackedOpDelta,
  editor: IEditorKit,
  isKeepFormatting: boolean,
  assetsUrlMapping?: Record<string, string>
): Nullable<{
  sp: SlideElement
  asserts: string[]
}> {
  if (editor.presentation.slides.length <= 0) return undefined

  let presentation = isKeepFormatting
    ? getRefPresentation()
    : editor.presentation

  if (presentation == null) {
    presentation = editor.presentation
    isKeepFormatting = false
  }

  const slideNum = presentation.slides.length
  if (slideNum <= 0) return undefined

  const curSlide =
    presentation.slides[presentation.currentSlideIndex] ??
    presentation.slides[0]

  editor.resourcesLoadManager.resetApplyCollector()

  const params: FromJSONParams = {
    presentation,
    ignoreRId: true,
  }

  const oldMapping = editor.dataLoader.themeReplaceImagesMap
  if (assetsUrlMapping != null) {
    editor.dataLoader.themeReplaceImagesMap = assetsUrlMapping
  }
  EditorUtil.ChangesStack.stopRecording()
  const asserts: string[] = []
  const { sp } = genSp(op, params, curSlide.cSld, curSlide) ?? {}

  EditorUtil.ChangesStack.enableRecording()

  editor.dataLoader.themeReplaceImagesMap = oldMapping
  editor.resourcesLoadManager.collectImages()

  if (sp == null || isPlaceholder(sp)) {
    editor.resourcesLoadManager.resetApplyCollector()
    return undefined
  }

  for (const url of Object.keys(editor.resourcesLoadManager.applyImagesMap)) {
    if (!isBase64Url(url) && !assetsUrlMapping?.hasOwnProperty(url)) {
      asserts.push(url)
    }
  }

  if (isKeepFormatting === true) {
    EditorUtil.ChangesStack.stopRecording()
    checkExtentsByTextContent(sp)
    calculateSlideElement(sp)

    const resultSp = cloneSpWithFormatting(sp)

    EditorUtil.ChangesStack.enableRecording()

    return { sp: resultSp, asserts }
  } else {
    return { sp, asserts }
  }
}

export function insertSlideElementFromPackedOp(
  op: PackedOpDelta,
  editor: IEditorKit,
  keepFormatting: boolean = false,
  assertUrlsMappings?: Record<string, string>
): boolean {
  const spInfo = slideElementFromPackedOp(
    op,
    editor,
    keepFormatting,
    assertUrlsMappings
  )
  if (spInfo == null) return false

  const presentation = editor.presentation
  const sp = spInfo.sp
  const curSlide = presentation.slides[presentation.currentSlideIndex]!

  if (sp.isDeleted) {
    setDeletedForSp(sp, false)
  }
  setParentForSlideElement(sp, curSlide)
  sp.spTreeParent = curSlide.cSld

  // resize to fit current slide size
  const { scale } = getDefaultScaleUpdateInfo({
    from: PPT_Default_SIZE,
    to: {
      width: presentation.width,
      height: presentation.height,
    },
  })

  if (!checkAlmostEqual(scale, 1)) {
    EditorUtil.ChangesStack.stopRecording()
    changeSlideElementSize(sp, scale, scale, undefined, undefined, true)
    EditorUtil.ChangesStack.enableRecording()
  }
  // update xfrm to center sp
  const xfrm = sp?.spPr?.xfrm
  if (xfrm != null) {
    if (isNumber(xfrm.offX) && isNumber(xfrm.extX)) {
      const width_unit = presentation.width
      xfrm.offX = (width_unit - xfrm.extX) / 2
    }
    if (isNumber(xfrm.offY) && isNumber(xfrm.extY)) {
      const height_unit = presentation.height
      xfrm.offY = (height_unit - xfrm.extY) / 2
    }
  }

  presentation.isNotesFocused = false
  presentation.selectionState.reset()

  clearCxnState()

  if (sp.instanceType === InstanceType.ImageShape) {
    const xfrm = sp.spPr?.xfrm
    const imageUrl = sp.blipFill?.imageSrcId
    // url 是base64的 或者是粘贴成图的image 均上传插入处理
    if (imageUrl && imageUrl.startsWith('data:')) {
      const options = xfrm
        ? {
            size: {
              width: xfrm.extX * Factor_mm_to_pix,
              height: xfrm.extY * Factor_mm_to_pix,
            },
            offset: {
              x: xfrm.offX * Factor_mm_to_pix,
              y: xfrm.offY * Factor_mm_to_pix,
            },
          }
        : undefined
      EditorKit.uploadImagesAndInsert([imageUrl], options)
      return true
    }
  }

  if (sp.instanceType === InstanceType.GraphicFrame && sp.graphicObject) {
    _fillTableCellByGridCount(sp.graphicObject)
  }

  if (sp.instanceType === InstanceType.GraphicFrame) {
    presentation.adjustGraphicFrameRowHeight(sp)
  }

  collectCxnStateForSp(sp)

  addToParentSpTree(sp)
  checkExtentsByTextContent(sp)

  presentation.selectionState.select(sp)

  applyCxnSpToSpMapState()

  calculateRenderingState(presentation)

  editor.dataLoader.applyAsyncAssets()

  return true
}

function _fillTableCellByGridCount(table: Table) {
  const tableGridLength = table.tableGrid.length
  ManagedSliceUtil.forEach(table.children, (row) => {
    let rowGridCount = 0
    ManagedSliceUtil.forEach(row.children, (cell) => {
      rowGridCount += cell.getGridSpan()
    })

    if (rowGridCount < tableGridLength) {
      const newCell = row.addCell(row.cellsCount, null, false)
      newCell.setGridSpan(tableGridLength - rowGridCount)
    }
  })
}

export function peviewImageFromPackedOp(
  packedOp: PackedOpDelta,
  editor: IEditorKit,
  ratio: number = 1
): Nullable<{ canvas: HTMLCanvasElement; assets: string[] }> {
  const spInfo = slideElementFromPackedOp(packedOp, editor, false)
  if (spInfo == null) return
  calculateSlideElement(spInfo.sp)
  const preview = getSlideElementPreview(
    spInfo.sp,
    ratio * Factor_mm_to_pix * BrowserInfo.PixelRatio
  )
  editor.resourcesLoadManager.resetApplyCollector()
  return { ['canvas']: preview.image, ['assets']: spInfo.asserts }
}
