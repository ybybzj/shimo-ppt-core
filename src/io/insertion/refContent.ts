import { Presentation } from '../../core/Slide/Presentation'
import { calculatePresentation } from '../../core/calculation/calculate/presentation'
import { stringToSlice } from '../../re/export/modoc'
import { loadPPTDoc } from '../loaders/PPTDoc'
class ContentStore {
  _content?: string
  isDirty: boolean = false
  updateContent(content: string) {
    this._content = content
    this.isDirty = true
  }
  get content() {
    if (this.isDirty) {
      this.isDirty = false
      return this._content
    }
    return undefined
  }
}

const contentStore = new ContentStore()

export function setRefContent(content: string) {
  contentStore.updateContent(content)
}

const textBodyTextFit: any[] = []

function loadFromData(data: string): Presentation {
  const presentation = new Presentation()
  const deltaSlice = stringToSlice(data)

  loadPPTDoc(
    deltaSlice,
    presentation,
    {
      textBodyTextFit: textBodyTextFit,
      forceSync: true,
    },
    false
  )
  checkTextFit()
  return presentation
}

function checkTextFit() {
  textBodyTextFit.forEach((txBody) => {
    txBody.checkTextFit()
  })
  textBodyTextFit.length = 0
}
let _presentation

export function getRefPresentation() {
  const content = contentStore.content
  if (content != null) {
    _presentation = loadFromData(content)
    calculatePresentation(_presentation)
  }
  return _presentation
}
