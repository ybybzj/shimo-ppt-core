import { EditChangesStack, IChangeItem } from '../core/changes/EditChangesStack'
import { PPTSelectState } from '../core/Slide/SelectionState'
import { EditActionFlag } from '../core/EditActionFlag'
import { descriptionFromIdentityFlag } from '../core/FlagDescriptions'

interface ChangesCollectorOpts {
  changesStack: EditChangesStack
}

export interface ActionPoint {
  type: EditActionFlag
  description: string
  cchanges: IChangeItem[]
  docSelectionInfo: PPTSelectState
}

export class ChangesCollector {
  private changesStack: EditChangesStack

  constructor(opts: ChangesCollectorOpts) {
    this.changesStack = opts.changesStack
  }

  public haveChanges() {
    return this.changesStack.haveChanges()
  }

  // 按照是否可以撤销重做分组
  public getChangeGroups() {
    if (!this.haveChanges()) {
      return []
    }

    const { changesStack } = this

    const pointsGrouper = makeGrouper<boolean, ActionPoint>()
    for (
      let pointIndex = 0;
      pointIndex < changesStack.actions.length;
      pointIndex++
    ) {
      const point = changesStack.actions[pointIndex]
      if (!point || !point.changes || point.changes.length <= 0) {
        continue
      }

      pointsGrouper.add(
        {
          type: point.actionType,
          description: descriptionFromIdentityFlag(point.actionType),
          cchanges: point.changes,
          docSelectionInfo: point.docSelectionInfo,
        },
        !!point.ignoreUndoRedo
      )
    }
    this.clear()
    return pointsGrouper.collect()
  }

  public clear() {
    this.changesStack.clear()
  }
}

function makeGrouper<By extends string | number | boolean, T>() {
  let curBy: By

  const groups: Array<{ group: T[]; by: By }> = []

  return {
    add(v: T, by: By) {
      if (by !== curBy) {
        const newGrp: T[] = [v]
        groups.push({
          group: newGrp,
          by,
        })
        curBy = by
      } else {
        const curGrp = groups[groups.length - 1]
        if (curGrp) {
          curGrp.group.push(v)
        }
      }
    },
    collect(): Array<{ group: T[]; by: By }> {
      const result: Array<{ group: T[]; by: By }> = groups.slice(0)

      groups.length = 0

      return result
    },
  }
}
