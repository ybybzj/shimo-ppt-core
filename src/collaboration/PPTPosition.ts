import { ParaRun } from '../core/Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../core/Paragraph/Paragraph'
import { PPTSelectState } from '../core/Slide/SelectionState'
import {
  PPTPositionStateCollection,
  PositionStateEntity,
  getPositionStateForRun,
} from '../core/utilities/tableOrTextDocContent/position'
import { EditorKit } from '../editor/global'
import { IEditorKit } from '../editor/type'
import { InstanceType, isInstanceTypeOf } from '../core/instanceTypes'
import { TextSlideElement } from '../core/SlideElement/type'
import { loadPPTPositionState } from '../ui/rendering/editorHandler/utils'

export type PPTPositionState = {
  positions: PPTPositionStateCollection
  startPositions: PPTPositionStateCollection
  endPositions: PPTPositionStateCollection
  selectionInfo?: PPTSelectState
} & { [k: string]: any }

export class PPTPosition {
  private positionsMgr: PPTPositionsManager

  constructor() {
    this.positionsMgr = new PPTPositionsManager()
  }
  savePositionState(): PPTPositionState {
    const editor = EditorKit

    const positionState = _savePosistionState(editor)
    this.reset()

    if (positionState.positions) this.add(positionState.positions)
    if (positionState.startPositions) {
      this.add(positionState.startPositions)
    }
    if (positionState.endPositions) this.add(positionState.endPositions)

    return positionState
  }

  restorePositionState(positionState: PPTPositionState) {
    const editor = EditorKit

    if (positionState.positions) this.update(positionState.positions)
    if (positionState.startPositions) {
      this.update(positionState.startPositions)
    }
    if (positionState.endPositions) this.update(positionState.endPositions)
    _restorePositionState(editor, positionState)
  }

  onAdd(entity: PositionStateEntity, index: number) {
    this.positionsMgr.updatePPTPositionsOnAdd(entity, index)
  }

  onRemove(entity: PositionStateEntity, index: number, count: number) {
    this.positionsMgr.updatePPTPositionsOnRemove(entity, index, count)
  }

  onSplitRunStart(splitRun: ParaRun, splitPos: number) {
    this.positionsMgr.onSplitRunStart(splitRun, splitPos)
  }

  onSplitRunEnd(newRun) {
    this.positionsMgr.onSplitRunEnd(newRun)
  }

  private add(posCollection: PPTPositionStateCollection) {
    this.positionsMgr.addPosition(posCollection)
  }

  private update(pos) {
    this.positionsMgr.updatePPTPosition(pos)
  }

  private reset() {
    this.positionsMgr.resetPPTPositions()
  }
}

function _savePosistionState(editorKit: IEditorKit) {
  const presentation = editorKit.presentation
  const controller = editorKit.editorUI.editorHandler
  const positionState: PPTPositionState = {
    positions: [],
    startPositions: [],
    endPositions: [],
    currentSlideIndex: presentation.currentSlideIndex,
    isNotesFocused: presentation.isNotesFocused,
  }

  if (controller) {
    positionState.Slide = presentation.slides[presentation.currentSlideIndex]
    controller.savePositionState(positionState)
  }

  return positionState
}

function _restorePositionState(editorKit: IEditorKit, state: PPTPositionState) {
  const presentation = editorKit.presentation
  const controller = editorKit.editorUI.editorHandler

  const slide = state.Slide
  if (slide) {
    if (slide !== presentation.slides[presentation.currentSlideIndex]) {
      presentation.slides.forEach((s, i) => s.setIndex(i))
      const slideIndexFound = presentation.slides.findIndex((s) => s === slide)
      if (slideIndexFound !== -1) {
        presentation.currentSlideIndex = slideIndexFound
        presentation.isGoToSlide = true
      } else {
        const countOfSlides = presentation.slides.length
        if (presentation.currentSlideIndex >= countOfSlides) {
          presentation.currentSlideIndex = countOfSlides - 1
        }
        presentation.isGoToSlide = true
        if (presentation.slides[presentation.currentSlideIndex]) {
          presentation.isNotesFocused = false
          controller.reset()
        }
        return
      }
    }
    controller.reset()
    if (state.isNotesFocused) {
      if (presentation.slides[presentation.currentSlideIndex].notes) {
        presentation.isNotesFocused = true
      } else {
        presentation.isNotesFocused = false
      }
    } else {
      presentation.isNotesFocused = false
    }
    loadPPTPositionState(controller, state)
  } else {
    if (state.currentSlideIndex === -1) {
      if (presentation.slides.length > 0) {
        presentation.currentSlideIndex = 0
        presentation.isGoToSlide = true
      }
    }
    if (presentation.slides[presentation.currentSlideIndex]) {
      presentation.isNotesFocused = false
      controller.reset()
    }
  }
}

export function setPPTSelectInfo(editorKit: IEditorKit, state: PPTSelectState) {
  const { presentation, editorHandler: controller } = editorKit.editorUI
  if (state.curSlideIndex > -1) {
    const slide = presentation.slides[state.curSlideIndex]
    if (slide) {
      if (state.focusOnNotes) {
        controller.reset()
        if (slide.notes) {
          presentation.isNotesFocused = true

          {
            // 当 notes 被删除，slide 会重新创建 notes
            const { spTree } = slide.notes.cSld
            if (state.textSp && !spTree.includes(state.textSp)) {
              state.textSp = spTree[1] as TextSlideElement
            }
          }

          controller.setSelectionState(state)
        } else {
          presentation.isNotesFocused = false
        }
      } else {
        controller.setSelectionState(state)
      }
    }
  }
  if (state.curSlideIndex !== presentation.currentSlideIndex) {
    presentation.isGoToSlide = true
  }
  presentation.currentSlideIndex = state.curSlideIndex
}

class PPTPositionsManager {
  private positions: PPTPositionStateCollection[]
  private positionsForSplit: Array<{
    posInfo: PPTPositionStateCollection
    newRunIndex: number
  }>
  private positionsMap: Array<{
    startPositions: PPTPositionStateCollection
    endPositions: PPTPositionStateCollection
  }>
  constructor() {
    this.positions = []
    this.positionsForSplit = []
    this.positionsMap = []
  }
  resetPPTPositions() {
    this.positions = []
    this.positionsForSplit = []
    this.positionsMap = []
  }
  addPosition(posCollection: PPTPositionStateCollection) {
    this.positions.push(posCollection)
  }
  updatePPTPositionsOnAdd(entity: PositionStateEntity, index: number) {
    for (let i = 0, l = this.positions.length; i < l; ++i) {
      const posInfo = this.positions[i]
      for (let i = 0, l = posInfo.length; i < l; ++i) {
        const pos = posInfo[i]
        if (
          entity === pos.entity &&
          null != pos.position &&
          (pos.position > index ||
            (pos.position === index &&
              !isInstanceTypeOf(entity, InstanceType.ParaRun)))
        ) {
          pos.position++
          break
        }
      }
    }
  }
  updatePPTPositionsOnRemove(
    entity: PositionStateEntity,
    index: number,
    count: number
  ) {
    for (let i = 0, l = this.positions.length; i < l; ++i) {
      const posInfo = this.positions[i]
      for (let j = 0, l = posInfo.length; j < l; ++j) {
        const positionItem = posInfo[j]
        if (entity === positionItem.entity && null != positionItem.position) {
          if (positionItem.position > index + count) {
            positionItem.position -= count
          } else if (positionItem.position >= index) {
            if (isInstanceTypeOf(entity, InstanceType.Table)) {
              positionItem.position = index
              if (
                posInfo[j + 1] &&
                isInstanceTypeOf(
                  posInfo[j + 1].entity,
                  InstanceType.TableRow
                ) &&
                null != posInfo[j + 1].position &&
                entity.children.at(index)
              ) {
                posInfo[j + 1].position = Math.max(
                  0,
                  Math.min(posInfo[j + 1].position, entity.children.length - 1)
                )
              }
            } else {
              positionItem.position = index
              positionItem.deleted = true
            }
          }

          break
        }
      }
    }
  }
  onSplitRunStart(splitRun: ParaRun, splitPos: number) {
    this.positionsForSplit = []

    for (let i = 0, l = this.positions.length; i < l; ++i) {
      const posInfo = this.positions[i]
      for (let j = 0, l = posInfo.length; j < l; ++j) {
        const positionItem = posInfo[j]
        if (
          splitRun === positionItem.entity &&
          positionItem.position &&
          positionItem.position >= splitPos
        ) {
          this.positionsForSplit.push({
            posInfo: posInfo,
            newRunIndex: positionItem.position - splitPos,
          })
        }
      }
    }
  }
  onSplitRunEnd(newRun) {
    if (!newRun) return

    for (let i = 0, l = this.positionsForSplit.length; i < l; ++i) {
      const newPosInfos: PPTPositionStateCollection = []
      newPosInfos.push({
        entity: newRun,
        position: this.positionsForSplit[i].newRunIndex,
      })
      this.positions.push(newPosInfos)
      this.positionsMap.push({
        startPositions: this.positionsForSplit[i].posInfo,
        endPositions: newPosInfos,
      })
    }
  }
  updatePPTPosition(info: PPTPositionStateCollection) {
    let newPosInfo = info
    for (let i = 0, l = this.positionsMap.length; i < l; ++i) {
      if (this.positionsMap[i].startPositions === newPosInfo) {
        newPosInfo = this.positionsMap[i].endPositions
      }
    }

    if (
      newPosInfo !== info &&
      newPosInfo.length === 1 &&
      newPosInfo[0].entity.instanceType === InstanceType.ParaRun
    ) {
      const run = newPosInfo[0].entity
      const para = run.paragraph
      if (canGetPosState(para, run)) {
        info.length = 0
        getPositionStateForRun(run, newPosInfo[0].position, info)
      }
    } else if (
      info.length > 0 &&
      isInstanceTypeOf(info[info.length - 1].entity, InstanceType.ParaRun)
    ) {
      const run = info[info.length - 1].entity as ParaRun
      const runPos = info[info.length - 1].position
      const para = run.paragraph
      if (canGetPosState(para, run)) {
        info.length = 0
        getPositionStateForRun(run, runPos, info)
      }
    }
  }
  removePPTPosition(info: PPTPositionStateCollection) {
    for (let i = 0, l = this.positions.length; i < l; ++i) {
      if (this.positions[i] === info) {
        this.positions.splice(i, 1)
        return
      }
    }
  }
}

function canGetPosState(para: Paragraph, run: ParaRun) {
  return para && true === para.isInPPT() && true === run.isInParagraph()
}
