import { ChangesCollector } from './changesCollector'
import { Records, Task } from './records'
import { EventNames } from '../editor/events'
import { EditorUtil } from '../globals/editor'

import { turnOffChanges, turnOnChanges } from '../editor/proto'
import { IEditorKit } from '../editor/type'
import { readFromChangePoints } from '../io/readers/changes/cchangesToOps'
import { gSpecialPasteUtil } from '../copyPaste/gSpecialPasteUtil'
import { genPPTInfoData, PPTInfoData } from '../io/infoData'
import { longActions } from '../globals/longActions'
import { syncInterfaceState } from '../editor/interfaceState/syncInterfaceState'
import { debounce, startPerfTiming } from '../common/utils'
import { setPPTSelectInfo } from './PPTPosition'
import { restoreEditState } from '../ui/rendering/editorHandler/utils'
import { logger } from '../lib/debug/log'
import { breadcrumbTrail } from './breadcrumbTrail'
import { searchManager } from '../core/search/SearchManager'
import { findTextInPresentation } from '../core/search/findText'
import { Errors } from '../exports/type'
import { PPTSelectState } from '../core/Slide/SelectionState'
// import { ModocDelta } from '../types/globals/io'
import { calculateRenderingState } from '../core/calculation/calculate/presentation'
import {
  ModocDeltaToOperations,
  // ModocDeltaToOperations,
  composeDelta,
  invertDelta,
  isDeltaEmpty,
  printModoc,
} from '../re/export/modoc'
import { Operation } from '../io/operation'
import { Nullable } from '../../liber/pervasive'

interface CollaborationManagerOpts {
  editorKit: IEditorKit
}

export class CollaborationManager<InputData> {
  private editorKit: IEditorKit
  private _canApplyChange = false
  private get canApplyChange() {
    return this._canApplyChange
  }

  private set canApplyChange(b: boolean) {
    const oldVal = this._canApplyChange
    this._canApplyChange = b
    if (oldVal === false && b === true) {
      this.editorKit.emitEvent(EventNames.Evt_Collab_readyToHandleChanges)
    }
  }
  private applyingChanges = false
  private changesCollector: ChangesCollector
  private pendingChanges?: Operation[]
  private undoRedoRecords: Records
  private pptIOData?: Operation[]
  private pptInfoData?: PPTInfoData
  private pollId?
  private isCoediting: boolean = false

  constructor(opts: CollaborationManagerOpts) {
    this.editorKit = opts.editorKit
    this.changesCollector = new ChangesCollector({
      changesStack: EditorUtil.ChangesStack,
    })
    this.undoRedoRecords = new Records()
  }

  initData(data: Nullable<Operation[]>) {
    if (data != null) {
      this.updateStateData(data)
    }
  }

  private updateStateData(data: Operation[]) {
    const presentation = this.editorKit.presentation
    this.pptIOData = data
    this.pptInfoData = genPPTInfoData(presentation)
  }

  currentPptIOData() {
    return this.pptIOData
  }
  currentInfoData() {
    return this.pptInfoData
  }

  ioAdapter() {
    return this.editorKit.ioAdapter
  }
  public start() {
    if (this.pollId != null) return
    // 主循环函数
    this.canApplyChange = true
    this.changesCollector.clear()
    this.pollId = setInterval(() => this.save(), 200)
  }

  public stop() {
    if (this.pollId != null) {
      clearInterval(this.pollId)
      this.pollId = undefined
    }
  }
  private canAcceptChange() {
    if (this.editorKit.presentation.isLoadComplete === false) {
      return false
    }

    if (this.applyingChanges === true) {
      return false
    }
    if (this.editorKit.presentation.compositeInputInfo) {
      return false
    }
    if (gSpecialPasteUtil.isPasteStart) {
      return false
    }

    if (!longActions.isEmpty()) {
      return false
    }

    return true
  }

  public save() {
    if (!this.canAcceptChange()) {
      this.canApplyChange = false
      return
    }
    if (this.changesCollector.haveChanges()) {
      this.isCoediting = true
      this.sendChange()
      this.canApplyChange = true
      this.debounceSetIsCoeditingOff()
    } else if (!this.canApplyChange) {
      this.canApplyChange = true
    }
  }

  public sendChange() {
    const changeGroups = this.changesCollector.getChangeGroups()
    const presentation = this.editorKit.presentation
    const sendDelta: Operation[] = []

    for (const changeGroup of changeGroups) {
      const { group: changePoints, by: ignoreUndoRedo } = changeGroup
      if (changePoints.length > 0) {
        breadcrumbTrail.add(changePoints)
        // logger.log(
        //   `changePoints ${ignoreUndoRedo ? 'for ignoreUndoRedo' : ''} ==>`,
        //   changePoints
        // )

        const getSendIOTiming = startPerfTiming()

        const operations = readFromChangePoints<InputData>(
          changePoints,
          presentation,
          this
        )

        const sendIOTiming = getSendIOTiming()
        this.editorKit.trackPerf('IO_SEND', sendIOTiming)
        logger.log('readFromChangePoints', sendIOTiming)
        if (operations && !isDeltaEmpty(operations)) {
          const delta = operations
          if (this.pptIOData) {
            logger.time('Invert Ops')
            let undoDelta: Operation[]
            try {
              undoDelta = invertDelta(this.pptIOData, delta)
            } catch (error) {
              // 弹窗报错, TODO: 后续收集 state 变更 log, 通过接口或其他方式记录操作意图
              this.editorKit.trackError('INVERT_ERROR', error, {
                ['delta']: JSON.stringify(delta), // .slice(0, 100),
                ['actionTrails']: breadcrumbTrail.get(),
                // ['notesError']: invalidNotes,
              })
              this.editorKit.emitEvent(
                EventNames.Evt_onError,
                Errors.ErrorChange
              )
              throw new Error()
            }

            logger.timeEnd('Invert Ops')

            // 每次处理groupchange只更新iodata
            console.time('Compose Ops(update pptIOData)')
            this.pptIOData = composeDelta(this.pptIOData, delta)
            console.timeEnd('Compose Ops(update pptIOData)')

            if (undoDelta && !isDeltaEmpty(undoDelta)) {
              printModoc(undoDelta, `Modoc for undo ==>`)
              const primaryDocSelectionInfo = changePoints[0].docSelectionInfo
              this.undoRedoRecords.record([
                undoDelta,
                ignoreUndoRedo,
                primaryDocSelectionInfo,
              ])
              this.informUndoRedo()
            }
          }

          composeDelta(sendDelta, delta)
        }
      }
    }

    // 每次send更新infodata
    this.pptInfoData = genPPTInfoData(presentation)

    if (!isDeltaEmpty(sendDelta)) {
      const sendModocDelta = this.editorKit.ioAdapter.fromOperations(sendDelta)
      logger.log(`-----------sending changes----------------`)
      printModoc(sendModocDelta, `Modoc for send ==>`)
      this.editorKit.emitEvent(EventNames.Evt_onContentChange, sendModocDelta)
    }
    EditorUtil.entityRegistry.resetTemPairs()
  }

  public applyPendingChange() {
    if (this.pendingChanges) {
      this.applyChange({ delta: this.pendingChanges })
      this.pendingChanges = undefined
    }
  }

  public applyChange(
    {
      delta,
      docSelectionInfo,
    }: { delta: any; selectData?; docSelectionInfo?: PPTSelectState },
    appliedCallback?: () => void
  ) {
    if (this.canApplyChange === false) {
      return
    }

    const deltaOps = ModocDeltaToOperations(delta)

    // 演示状态允许接收 change 进入 pending 状态，演示完毕后 apply，避免拒绝接收协作模块过久
    if (this.editorKit.editorUI.showManager.isInShowMode) {
      this.pendingChanges = this.pendingChanges
        ? composeDelta(this.pendingChanges, deltaOps)
        : deltaOps
      return
    }

    this.applyingChanges = true
    const presentation = this.editorKit.presentation
    const searchState = searchManager.saveSearchState()
    const positionState = EditorUtil.pptPosition.savePositionState()
    turnOffChanges()
    const loader = this.editorKit.dataLoader
    loader.reset()

    this.isCoediting = true

    loader.apply(deltaOps, (appliedData) => {
      this.updateStateData(appliedData)

      if (docSelectionInfo) {
        try {
          setPPTSelectInfo(this.editorKit, docSelectionInfo)
        } catch (e) {}
        const handler = this.editorKit.editorUI.editorHandler
        restoreEditState(handler, positionState)
      } else {
        EditorUtil.pptPosition.restorePositionState(positionState)
      }

      calculateRenderingState(presentation)

      this.changesCollector.clear()

      this.editorKit.editorUI.thumbnailsManager.updateAll()
      this.editorKit.editorUI.calculatePreviews()
      this.editorKit.editorUI.updateLayouts()

      syncInterfaceState(this.editorKit)

      if (searchState) {
        const { text, caseSensitive } = searchState
        findTextInPresentation(
          presentation,
          text,
          { caseSensitive: caseSensitive },
          true
        )
      }

      turnOnChanges()

      if (typeof appliedCallback === 'function') {
        appliedCallback()
      }
      this.debounceSetIsCoeditingOff()
    })
    this.applyingChanges = false
  }

  beforeUndoRedoApply() {
    const presentation = this.editorKit.editorUI.presentation
    presentation.selectionState.reset()
  }

  public canHandleChanges(): boolean {
    const presentation = !!this.editorKit.presentation
    return (
      presentation &&
      this.applyingChanges === false &&
      !this.changesCollector.haveChanges() &&
      this.canApplyChange
    )
  }

  public undo(isSpecialPaste?: boolean) {
    let _undoTask: Task | undefined, _redoTaskCs: Operation[] | undefined
    if (
      this.canApplyChange === false ||
      this.applyingChanges === true ||
      !this.canUndo()
    ) {
      return
    }

    while ((_undoTask = this.undoRedoRecords.popUndo())) {
      const [_undoTaskCs, isDisableRecords, docSelectionInfo] = _undoTask
      try {
        _redoTaskCs = invertDelta(this.pptIOData!, _undoTaskCs)
        // _redoTaskCs = this.editorKit.ioAdapter.invert(
        //   this.pptIOData,
        //   _undoTaskCs
        // )
      } catch (error) {
        // updateChange 的 target 被协作者删除后 undo 会 invert 错误，此时不 apply
        this.editorKit.emitEvent(EventNames.Evt_Collab_readyToHandleChanges)
        this.informUndoRedo()
        gSpecialPasteUtil.hideUI(!isSpecialPaste)
        return
      }

      this.undoRedoRecords.pushRedo([
        _redoTaskCs,
        isDisableRecords,
        this.editorKit.editorUI.presentation.selectionState.getSelectionState(),
      ])

      if (!isDisableRecords) {
        this.applyChange({ delta: _undoTaskCs, docSelectionInfo }, () => {
          this.editorKit.emitEvent(EventNames.Evt_onContentChange, _undoTaskCs)
          this.editorKit.emitEvent(EventNames.Evt_Collab_readyToHandleChanges)
          this.informUndoRedo()
          gSpecialPasteUtil.hideUI(!isSpecialPaste)
        }) // 需要将 undo 的 cs 应用到 UI 上，不能直接使用 composeContent, redo 列表需要 rebase 到当前最新修改
        return
      }
    }
  }

  public redo() {
    let _undoTaskCs: Operation[] | undefined, _redoTask: Task | undefined
    if (
      this.canApplyChange === false ||
      this.applyingChanges === true ||
      !this.canRedo()
    ) {
      return
    }
    while ((_redoTask = this.undoRedoRecords.popRedo())) {
      const [_redoTaskCs, isDisableRecords, docSelectionInfo] = _redoTask

      try {
        _undoTaskCs = invertDelta(this.pptIOData!, _redoTaskCs)
        // _undoTaskCs = this.editorKit.ioAdapter.invert(
        //   this.pptIOData,
        //   _redoTaskCs
        // )
      } catch (error) {
        this.editorKit.emitEvent(EventNames.Evt_Collab_readyToHandleChanges)
        this.informUndoRedo()
        gSpecialPasteUtil.hideUI()
        return
      }
      this.undoRedoRecords.record([
        _undoTaskCs,
        isDisableRecords,
        this.editorKit.editorUI.presentation.selectionState.getSelectionState(),
      ])

      if (!isDisableRecords) {
        this.applyChange({ delta: _redoTaskCs, docSelectionInfo }, () => {
          this.editorKit.emitEvent(EventNames.Evt_onContentChange, _redoTaskCs)
          this.editorKit.emitEvent(EventNames.Evt_Collab_readyToHandleChanges)
          this.informUndoRedo()
          gSpecialPasteUtil.hideUI()
        })
        return
      }
    }
  }

  // disable undo/redo during dragging
  public canUndo(): boolean {
    return (
      this.editorKit.canEdit() &&
      this.editorKit.editorUI.pointerEvtInfo.isLocked !== true &&
      this.undoRedoRecords.isUndoable()
    )
  }

  public canRedo(): boolean {
    return (
      this.editorKit.canEdit() &&
      this.editorKit.editorUI.pointerEvtInfo.isLocked !== true &&
      this.undoRedoRecords.isRedoable()
    )
  }

  public clearRedoUndo() {
    this.undoRedoRecords.clear()
    this.informUndoRedo()
  }

  public informUndoRedo() {
    this.editorKit.emitEvent(EventNames.Evt_onCanUndoRedo, {
      canUndo: this.canUndo(),
      canRedo: this.canRedo(),
    })
  }

  public getIsCoediting() {
    return this.isCoediting
  }

  private debounceSetIsCoeditingOff = debounce(() => {
    this.isCoediting = false
  }, 300)

  public isApplyingChanges() {
    return this.applyingChanges
  }
}
