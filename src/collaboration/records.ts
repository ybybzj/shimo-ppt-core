import { find } from '../../liber/l/find'
import { PPTSelectState } from '../core/Slide/SelectionState'
import { Operation } from '../io/operation'
// import { ModocDelta } from '../types/globals/io'

// type Delta = ModocDelta
type Delta = Operation[]
export type Task = [Delta, boolean | undefined, PPTSelectState]
export class Records {
  private undoStack: Task[]
  private redoStack: Task[]
  private readonly MAX_RECORD_LEN = 20

  constructor() {
    this.undoStack = []
    this.redoStack = []
  }

  public record(newChange: Task): void {
    if (this.undoStack.length >= this.MAX_RECORD_LEN) {
      this.undoStack.shift()
    }
    this.undoStack.push(newChange)
  }

  public isUndoable(): boolean {
    return !!find(([, isDisableRecords]) => !isDisableRecords, this.undoStack)
  }

  public isRedoable(): boolean {
    return !!find(([, isDisableRecords]) => !isDisableRecords, this.redoStack)
  }

  public popUndo(): Task | undefined {
    return this.undoStack.pop()
  }

  public pushRedo(redoTask: Task): void {
    if (this.undoStack.length >= this.MAX_RECORD_LEN) {
      this.undoStack.shift()
    }
    this.redoStack.push(redoTask)
  }

  public popRedo(): Task | undefined {
    return this.redoStack.pop()
  }

  public clearUndoStack(): void {
    this.undoStack = []
  }

  public clearRedoStack(): void {
    this.redoStack = []
  }

  public clear(): void {
    this.clearUndoStack()
    this.clearRedoStack()
  }
}
