const MAX_CHANGE_LOCK_DURATION = 30 * 1000
let lockStartTime: number | undefined

/** 若长时间无法发送 change, 需收集状态并发送日志 */
export const changeLockChecker = {
  checkInvalid: () => {
    if (lockStartTime === undefined) {
      lockStartTime = Date.now()
      return false
    } else {
      if (Date.now() - lockStartTime >= MAX_CHANGE_LOCK_DURATION) {
        return true
      }
      return false
    }
  },
  reset() {
    lockStartTime = undefined
  },
}
