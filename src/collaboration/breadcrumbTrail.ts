import { GUID } from '../common/guid'
import { ActionPoint } from './changesCollector'

// 按时间戳来限制大小
const MAX_TrailLength = 100
const MAX_TrailDuration = 1000 * 60

const prefix = GUID(false)
let actionHistory: { action: ActionPoint; timeStamp: number }[] = []

function timeoutActionHistory(
  historyArr: { action: ActionPoint; timeStamp: number }[],
  maxDuration: number = MAX_TrailDuration
) {
  const now_t = Date.now()
  const l = historyArr.length
  if (l <= 0) return historyArr
  if (now_t - historyArr[l - 1].timeStamp <= maxDuration) return historyArr

  const filteredHistory: { action: ActionPoint; timeStamp: number }[] = []
  for (let i = 0; i < l; i++) {
    const item = historyArr[i]
    if (now_t - item.timeStamp > maxDuration) {
      break
    }
    filteredHistory.unshift(item)
  }

  return filteredHistory
}

export const breadcrumbTrail = {
  get: () => {
    actionHistory = timeoutActionHistory(actionHistory)
    return actionHistory.map(({ action }) => `${prefix}_${action.type}`)
  },

  // 与上一个间隔时间过久时应该清空
  add: (actions: ActionPoint[]) => {
    const timeStamp = Date.now()
    const latestTimeStamp =
      actionHistory.length > 0 ? actionHistory[0].timeStamp : 0
    if (timeStamp - latestTimeStamp > MAX_TrailDuration) {
      actionHistory.length = 0
    }
    actionHistory.unshift(
      ...actions.map((action) => {
        return { action, timeStamp }
      })
    )
    if (actionHistory.length >= MAX_TrailLength) {
      actionHistory.length = MAX_TrailLength
    }
  },
}
