import { Nullable, valuesOfDict } from '../../liber/pervasive'
import { SpecialPasteKindValue } from './const'

export const InternalDataPrefix = 'smInternalData2;'
export const InternalDataAttributeKey = 'data-internal-ppt'

export function checkVersionCompatible(version) {
  return ClipboardSettings.copyPasteVersion === Number.parseFloat(version)
}

export const ClipboardSettings = {
  copyPasteVersion: 2,
  isUpdatingPasteDataFromClipboard: false,
  isPastTextOnly: false,
}

export const getIsUpdatingPasteDataFromClipboard = () =>
  ClipboardSettings.isUpdatingPasteDataFromClipboard
export const setIsUpdatingPasteDataFromClipboard = (val) =>
  (ClipboardSettings.isUpdatingPasteDataFromClipboard = val)

export function checkClipboardModificationSupport() {
  if (
    !navigator.permissions?.query ||
    !navigator.clipboard?.read ||
    !navigator.clipboard?.write
  ) {
    return false
  }
  return true
}

export async function queryClipboardPermission(isWrite = false) {
  if (!checkClipboardModificationSupport()) {
    return false
  }
  const permissionName = isWrite ? 'clipboard-write' : 'clipboard-read'
  const permission = await navigator.permissions.query({
    name: permissionName,
  } as any)
  return (
    permission?.['state'] === 'granted' || permission?.['state'] === 'prompt'
  )
}

export const ClipboardDataFormat = {
  TEXT: 1,
  HTML: 2,
  INTERNAL: 3,
  SOURCEFILEID: 4,
  VERSION: 5,
} as const

export type ClipboardDataFormatValue = valuesOfDict<typeof ClipboardDataFormat>

export const ClipboardDataFormatTypeMapArray = {
  [ClipboardDataFormat.TEXT]: 'text/plain',
  [ClipboardDataFormat.HTML]: 'text/html',
  [ClipboardDataFormat.INTERNAL]: 'text/x-custom',
  [ClipboardDataFormat.SOURCEFILEID]: 'text/sourcefileid',
  [ClipboardDataFormat.VERSION]: 'text/version',
} as const

export type SpecialPasteDataFormats = Array<ClipboardDataFormatValue>

interface PositionOfSpecialPasteUIOptions {
  x: number
  y: number
  slideIndex: number
  w: number
  h: number
  needConvertPosition?: boolean
}
export class SpecialPasteUIOptions {
  formats: Nullable<SpecialPasteKindValue[]>
  screenPosition: Nullable<Rect>
  shapeId: Nullable<string>
  position: Nullable<PositionOfSpecialPasteUIOptions>
  pasteSlideIndex: Nullable<number>
  constructor() {
    this.formats = null
    this.screenPosition = null
    this.shapeId = null
    this.position = null
    this.pasteSlideIndex = null
  }

  isReset() {
    let res = false
    if (
      null === this.formats &&
      null === this.screenPosition &&
      null === this.shapeId &&
      null === this.position
    ) {
      res = true
    }
    return res
  }

  reset() {
    this.formats = null
    this.screenPosition = null

    this.shapeId = null
    this.position = null
    this.pasteSlideIndex = null
  }

  setShapeId(val) {
    this.shapeId = val
  }

  setPosition(val: PositionOfSpecialPasteUIOptions) {
    this.position = val
  }

  setScreenPosition(val) {
    this.screenPosition = val
  }

  setPasteSlideIndex(val) {
    this.pasteSlideIndex = val
  }

  updateOptions(val: Nullable<SpecialPasteKindValue[]>) {
    if (val == null) {
      this.formats = []
    } else {
      this.formats = val
    }
  }
}

export class Rect {
  private _x: number
  private _y: number
  private _width: number
  private _height: number
  constructor(x: number, y: number, width: number, height: number) {
    // private members
    this._x = x
    this._y = y
    this._width = width
    this._height = height
  }

  get x() {
    return this._x
  }
  set x(v) {
    this._x = v
  }

  get y() {
    return this._y
  }
  set y(v) {
    this._y = v
  }

  get width() {
    return this._width
  }

  get height() {
    return this._height
  }
}
