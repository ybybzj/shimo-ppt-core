class OffsetStateForCopyShape {
  copyShapeOnly = true
  originSlideIndex = -1
  slidePasteCounts: {
    [slideIndex: number]: number
  } = {}
  reset() {
    this.slidePasteCounts = {}
    this.copyShapeOnly = true
    this.originSlideIndex = -1
  }
  getSlidePasteCount(index) {
    if (this.slidePasteCounts[index] == null) {
      this.slidePasteCounts[index] = -1
    }
    return this.slidePasteCounts[index]
  }
  setSlidePasteCount(index, time) {
    this.slidePasteCounts[index] = time
  }
  setOriginSlideIndex(index) {
    this.originSlideIndex = index
    this.setSlidePasteCount(index, 0)
  }
}
export const gOffsetStateForCopyShape = new OffsetStateForCopyShape()
