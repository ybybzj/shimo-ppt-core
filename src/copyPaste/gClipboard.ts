import { Nullable } from '../../liber/pervasive'
import { GlobalEvents } from '../common/dom/events'
import {
  ClipboardDataFormat,
  checkVersionCompatible,
  InternalDataPrefix,
  queryClipboardPermission,
  setIsUpdatingPasteDataFromClipboard,
  ClipboardSettings,
  ClipboardDataFormatTypeMapArray,
  ClipboardDataFormatValue,
} from './common'
import { gSpecialPasteUtil } from './gSpecialPasteUtil'
import { IEditorKit } from '../editor/type'
import { TextInputManager } from '../ui/textInputManager'
import { EditorSettings } from '../core/common/EditorSettings'
import { correctHtmlData } from './utils'
import { Evt_copyFailedWithoutPermission } from '../editor/events/EventNames'
import { ImageShape } from '../core/SlideElement/Image'
import { EditActionFlag } from '../core/EditActionFlag'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../core/common/utils'
import {
  getSelectedSpText,
  getSelectedText,
} from '../core/utilities/presentation/getters'
import { serializeCopyData } from './copy/serializeCopyData'
import { startEditAction } from '../core/calculation/informChanges/startAction'
import { executePaste, HTMLPasteData } from './paste/paste'
import { longActions } from '../globals/longActions'

import { BrowserInfo } from '../common/browserInfo'

import { deleteSlideElementfromParent } from '../core/utilities/shape/delete'
import { calculateForRendering } from '../core/calculation/calculate/calculateForRendering'

export class ClipboardHandler {
  editorKit!: IEditorKit
  inputManager: Nullable<TextInputManager>

  private isPasting: boolean
  isGettingCopyData: boolean = false
  private clipboardState: {
    clipboardEvent?: ClipboardEvent
  }
  private savedCopyData: Nullable<
    Array<{
      type: ClipboardDataFormatValue
      data: Nullable<string>
    }>
  >
  public copySourceFileId?: string
  constructor() {
    this.isPasting = false
    this.clipboardState = {}
    this.inputManager = null
    this.savedCopyData = null
  }

  _isCopyFromEditor(target) {
    return this.editorKit.editorUI.editorElement.contains(target)
  }

  _handleCopyEvent = (e: ClipboardEvent) => {
    if (
      (EditorSettings.isViewMode && !EditorSettings.isViewModeCopyable) ||
      !EditorSettings.isCopyAble
    ) {
      this.editorKit.emitEvent(Evt_copyFailedWithoutPermission)
      return
    }
    // 避免干扰外部的复制行为如公开分享链接复制
    // tab 模式时会 focus 到 editorContainer, 无法 fire copy event, 会 fallback 到 bodyElement, 暂时先不检查
    if (
      !this.editorKit.IsFocus() ||
      (!BrowserInfo.isTablet && !this._isCopyFromEditor(e.target))
    ) {
      return
    }
    this.clipboardState.clipboardEvent = e
    this.savedCopyData = null
    this.updateCopyData()
    e.preventDefault()
    return false
  }

  _handleCutEvent = (e: ClipboardEvent) => {
    if (
      (EditorSettings.isViewMode && !EditorSettings.isViewModeCopyable) ||
      !EditorSettings.isCopyAble
    ) {
      this.editorKit.emitEvent(Evt_copyFailedWithoutPermission)
      return
    }
    if (!this.editorKit.IsFocus()) return
    this.clipboardState.clipboardEvent = e
    this.savedCopyData = null
    this.updateCopyData()
    removeOnCut(this.editorKit)
    e.preventDefault()
    return false
  }

  _handlePasteEvent = (e: ClipboardEvent) => {
    if (!this.editorKit.IsFocus() || EditorSettings.isViewMode) {
      return
    }

    e.preventDefault()
    this.updateCopySourceFileId()
    this.clipboardState.clipboardEvent = e

    this.isPasting = true

    const clipboardData = e?.clipboardData ?? (window as any).clipboardData
    if (!clipboardData || !clipboardData.getData) {
      this._endPaste()
      return false
    }

    const textData = this._getClipboardData(ClipboardDataFormat.TEXT)

    // handle only paste text
    const isPasteTextOnly = GlobalEvents.keyboard.shiftKey
    if (isPasteTextOnly) {
      ClipboardSettings.isPastTextOnly = true
      pasteData(this.editorKit, ClipboardDataFormat.TEXT, textData)
      this._endPaste()
      ClipboardSettings.isPastTextOnly = false
      return false
    }

    // handle sourcefileId
    this.updateCopySourceFileId(
      this._getClipboardData(ClipboardDataFormat.SOURCEFILEID)
    )

    // handle paste internal data
    const internalData = this._getClipboardData(ClipboardDataFormat.INTERNAL)
    if (internalData && internalData.indexOf(InternalDataPrefix) === 0) {
      const version = this._getClipboardData(ClipboardDataFormat.VERSION)
      const isVersionCompatible = checkVersionCompatible(version)
      if (isVersionCompatible) {
        const data = internalData.substring(InternalDataPrefix.length)
        pasteData(this.editorKit, ClipboardDataFormat.INTERNAL, data, textData)
        this._endPaste()
        return false
      }
    }

    // handle paste html data
    const htmlData = this._getClipboardData(ClipboardDataFormat.HTML)
    if (htmlData) {
      pasteData(
        this.editorKit,
        ClipboardDataFormat.HTML,
        correctHtmlData(htmlData),
        textData
      )
      this._endPaste()
      return false
    }

    const items = clipboardData.items as DataTransferItemList
    // handle paste image data
    if (items?.length > 0) {
      const imageFiles: File[] = []
      const attachmentFiles: File[] = []
      for (let i = 0; i < items.length; ++i) {
        const item = items[i]
        const itemKind = item.kind
        const itemType = item.type
        if (itemKind === 'file') {
          const file = items[i].getAsFile()
          if (itemType.includes('image/') && !itemType.includes('svg')) {
            if (file) imageFiles.push(file)
          } else if (file && file.type !== '') {
            attachmentFiles.push(file)
          }
        }
      }

      // 存在附件则统一走附件上传
      if (attachmentFiles.length > 0) {
        imageFiles.forEach((image) => attachmentFiles.push(image))
        this.editorKit.uploadAttachmentAndInsertOle(attachmentFiles)
        this._endPaste()
        return false
      }
      // 仅复制图片时可先粘贴后上传
      else if (imageFiles.length > 0) {
        /** 暂时关闭快捷粘贴, 容易引起协作数据异常 */
        const firstImageFile = imageFiles[0]
        if (firstImageFile && !firstImageFile.type.includes('svg')) {
          const isUploadImageFirst =
            items.length === 1 ||
            (items.length === 2 &&
              Array.from(items).find((item: any) => item.kind === 'string'))
          if (isUploadImageFirst) {
            this._handlePasteImage(firstImageFile)
            this._endPaste()
            return false
          }
        }
        this.editorKit.uploadImagesAndInsert(imageFiles)
        this._endPaste()
        return false
      }
    }

    // handle text
    if (textData) {
      pasteData(this.editorKit, ClipboardDataFormat.TEXT, textData)
    }
    this._endPaste()
    return false
  }

  // Todo: 修复粘贴 slide 为图片时未走 _handlePasteImage 导致异常
  _handlePasteImage(imageFile: File) {
    const reader = new FileReader()
    const imgLoader = this.editorKit.imageLoader
    reader.addEventListener(
      'load',
      async () => {
        const dataUrl = reader.result as string
        // 先插入, 后上传, 上传过程中所有 change 先收集，上传完毕后统一发送(期间再删除也会 merge)
        longActions.start(EditActionFlag.action_Presentation_AddImage)
        let sp: undefined | ImageShape
        try {
          const preImages = await imgLoader.loadImages([{ url: dataUrl }])
          const previewImg = preImages[0]
          if (previewImg == null) {
            throw new Error('addImage Failed')
          }

          // add preview Image
          sp = this.editorKit.presentation.addImages([previewImg], {
            type: 'image',
          })[0]
          if (sp == null) {
            throw new Error('addImage Failed')
          }

          const result = await this.editorKit.uploadImages([imageFile])

          if (result == null || result.length <= 0) {
            deleteSlideElementfromParent(sp)
          } else {
            const { url, encryptUrl } = result[0]
            await imgLoader.loadImages([{ url }])

            // remove preview image cache
            delete imgLoader.imagesMap[dataUrl]

            sp.blipFill.setImageSrcId(url)
            sp.blipFill.setEncryptUrl(encryptUrl)
            sp.setBlipFill(sp.blipFill)
          }
        } catch (_e) {
          if (sp) {
            deleteSlideElementfromParent(sp)
          }
          this.editorKit.uploadImagesAndInsert([imageFile])
        }

        calculateForRendering()
        this.editorKit.presentation.selectionState.reset()
        this.editorKit.refreshPPT(true)
        longActions.end(EditActionFlag.action_Presentation_AddImage)
      },
      false
    )
    reader.readAsDataURL(imageFile)
  }

  init(api: IEditorKit) {
    this.editorKit = api
    gSpecialPasteUtil.init(api)
    document.addEventListener('copy', this._handleCopyEvent)
    document.addEventListener('cut', this._handleCutEvent)
    document.addEventListener('paste', this._handlePasteEvent)
  }

  checkIsProcessing() {
    return this.isPasting
  }

  _endPaste() {
    this.isPasting = false
  }

  _setClipboardData(format: ClipboardDataFormatValue, data: Nullable<string>) {
    if (null == this.savedCopyData) {
      this.savedCopyData = []
    }
    this.savedCopyData.push({ type: format, data })
    const type = ClipboardDataFormatTypeMapArray[format] ?? ''
    if (type != null && data != null) {
      data =
        format === ClipboardDataFormat.INTERNAL
          ? InternalDataPrefix + data
          : data
      const clipboardEvent = this.clipboardState.clipboardEvent
      const clipboard =
        clipboardEvent?.clipboardData ?? (window as any).clipboardData
      if (!clipboard || !clipboard.setData) {
        return
      }

      try {
        clipboard.setData(type, data)
      } catch (e) {}
    }
  }

  _getClipboardData(format: ClipboardDataFormatValue) {
    const type = ClipboardDataFormatTypeMapArray[format] ?? ''
    if (type) {
      const clipboardEvent = this.clipboardState.clipboardEvent
      const clipboardData =
        clipboardEvent?.clipboardData ?? (window as any).clipboardData
      if (!clipboardData || !clipboardData.getData) {
        return
      }
      try {
        return clipboardData.getData(type)
      } catch (e) {}
    }
  }

  _checkInvoke(checkCopyable = true) {
    if (
      EditorSettings.isViewMode &&
      (checkCopyable !== true || !EditorSettings.isViewModeCopyable)
    ) {
      if (checkCopyable === true) {
        this.editorKit.emitEvent(Evt_copyFailedWithoutPermission)
      }
      return false
    }
    if (this.inputManager?.inputAreaElm) {
      this.inputManager.inputAreaElm.focus()
    }
    this.editorKit.editorUI.setKeyEventsFocus(true)
    return true
  }

  async handleApiCopy() {
    if (this._checkInvoke() === false) {
      return
    }

    let isSucced
    try {
      isSucced = document.execCommand('copy')
    } catch (err) {}

    this.savedCopyData = null
    this.updateCopyData()

    if (!isSucced) {
      const hasPermission = await queryClipboardPermission(true)
      if (hasPermission) {
        this.updateApiCopyData()
      }
    }
  }

  async handleApiCut() {
    if (this._checkInvoke() === false) {
      return
    }

    let isSucced
    try {
      isSucced = document.execCommand('cut')
    } catch (err) {}

    if (!isSucced) {
      this.savedCopyData = null
      const hasPermission = await queryClipboardPermission(true)
      if (hasPermission) {
        this.updateApiCopyData()
      } else {
        this.savedCopyData = null
        this.updateCopyData()
      }
      removeOnCut(this.editorKit)
    }
  }

  async handleApiPaste() {
    if (this._checkInvoke(false) === false) {
      return
    }

    let isSucced
    try {
      isSucced = document.execCommand('paste')
    } catch (err) {}

    if (!isSucced) {
      this.updateCopySourceFileId()
      this.isPasting = true
      const hasPermission = await queryClipboardPermission()
      if (hasPermission) {
        try {
          const { htmlData, textData, imageData } =
            await getPasteDataFromClipboard()
          if (htmlData) {
            pasteData(
              this.editorKit,
              ClipboardDataFormat.HTML,
              correctHtmlData(htmlData)
            )
          } else if (imageData) {
            this._handlePasteImage(imageData)
          } else if (textData) {
            pasteData(this.editorKit, ClipboardDataFormat.TEXT, textData)
          }
        } catch (error) {
          this.pasteLastCopyData()
        }
      } else {
        this.pasteLastCopyData()
      }
      this._endPaste()
    }
  }

  async handleApiPasteDragData(data: DataTransfer) {
    if (this._checkInvoke(false) === false) {
      return
    }
    setIsUpdatingPasteDataFromClipboard(false)
    const getType = (type: ClipboardDataFormatValue) =>
      ClipboardDataFormatTypeMapArray[type]
    const textData = data.getData(getType(ClipboardDataFormat.TEXT))
    const internalData = data.getData(getType(ClipboardDataFormat.INTERNAL))
    const version = data.getData(getType(ClipboardDataFormat.VERSION))
    const htmlData = data.getData(getType(ClipboardDataFormat.HTML))
    const items = data.items
    let imgFile: Nullable<File>
    for (let i = 0; i < items.length; i++) {
      const item = items[i]
      if (item.type.includes('img')) {
        try {
          imgFile = await item.getAsFile()
        } catch {}
        if (imgFile) break
      }
    }

    this.isPasting = true
    if (internalData && internalData.indexOf(InternalDataPrefix) === 0) {
      const isVersionCompatible = checkVersionCompatible(version)
      if (isVersionCompatible) {
        pasteData(
          this.editorKit,
          ClipboardDataFormat.INTERNAL,
          internalData.substring(InternalDataPrefix.length),
          textData
        )
        this._endPaste()
        return
      }
    }
    if (htmlData) {
      pasteData(
        this.editorKit,
        ClipboardDataFormat.HTML,
        correctHtmlData(htmlData),
        textData
      )
      this._endPaste()
      return false
    }
    if (imgFile) {
      this._handlePasteImage(imgFile)
      this._endPaste()
      return false
    }
    if (textData) {
      pasteData(this.editorKit, ClipboardDataFormat.TEXT, textData)
    }
    this._endPaste()
    return false
  }

  private pasteLastCopyData() {
    if (this.savedCopyData) {
      setIsUpdatingPasteDataFromClipboard(false)
      const internalData = this.savedCopyData.find(
        (data) => data.type === ClipboardDataFormat.INTERNAL
      )
      if (internalData != null) {
        pasteData(
          this.editorKit,
          ClipboardDataFormat.INTERNAL,
          internalData.data as string
        )
      } else if (this.savedCopyData[0]) {
        pasteData(
          this.editorKit,
          this.savedCopyData[0].type,
          this.savedCopyData[0].data as string
        )
      }
    }
  }

  async updatePasteDataFromClipboard() {
    setIsUpdatingPasteDataFromClipboard(true)
    // todo imageData
    const { htmlData, textData } = await getPasteDataFromClipboard()
    const state = turnOffRecordChanges()
    if (htmlData) {
      pasteData(
        this.editorKit,
        ClipboardDataFormat.HTML,
        correctHtmlData(htmlData),
        textData
      )
      setIsUpdatingPasteDataFromClipboard(false)
      restoreRecordChangesState(state)
      return
    }
    if (textData) {
      pasteData(this.editorKit, ClipboardDataFormat.TEXT, textData)
      this._endPaste()
      setIsUpdatingPasteDataFromClipboard(false)
      restoreRecordChangesState(state)
      return
    }
    restoreRecordChangesState(state)
  }

  updateCopySourceFileId(id?: string) {
    this.copySourceFileId = id
  }

  private updateCopyData() {
    const editorKit = this.editorKit
    if (!editorKit.presentation) {
      this._setClipboardData(ClipboardDataFormat.TEXT, '')
      return
    }
    const { internalData, textData, htmlData } = getDataForCopy(editorKit)
    this._setClipboardData(
      ClipboardDataFormat.VERSION,
      ClipboardSettings.copyPasteVersion + ''
    )
    this._setClipboardData(ClipboardDataFormat.TEXT, textData)
    this._setClipboardData(ClipboardDataFormat.HTML, htmlData)
    this._setClipboardData(ClipboardDataFormat.INTERNAL, internalData)
    this._setClipboardData(
      ClipboardDataFormat.SOURCEFILEID,
      editorKit.sourceFileId
    )
  }

  updataDragData(dataTransfer: DataTransfer) {
    const editorKit = this.editorKit
    const { internalData, textData, htmlData } = getDataForDrag(editorKit)
    this._setClipboardData(
      ClipboardDataFormat.VERSION,
      ClipboardSettings.copyPasteVersion + ''
    )
    const getType = (type: ClipboardDataFormatValue) =>
      ClipboardDataFormatTypeMapArray[type]

    dataTransfer.setData(
      getType(ClipboardDataFormat.VERSION),
      ClipboardSettings.copyPasteVersion + ''
    )
    dataTransfer.setData(getType(ClipboardDataFormat.TEXT), textData ?? '')
    dataTransfer.setData(getType(ClipboardDataFormat.HTML), htmlData ?? '')
    dataTransfer.setData(
      getType(ClipboardDataFormat.INTERNAL),
      internalData ? InternalDataPrefix + internalData : ''
    )
    dataTransfer.setData(
      getType(ClipboardDataFormat.SOURCEFILEID),
      editorKit.sourceFileId ?? ''
    )
  }

  private updateApiCopyData() {
    const editorKit = this.editorKit
    if (!editorKit.presentation) {
      return
    }
    const { textData, htmlData } = getDataForCopy(editorKit)
    const clipboardItems: ClipboardItem[] = []
    if (textData) {
      const type = 'text/plain'
      const blob = new Blob([textData], { type })
      clipboardItems.push(new ClipboardItem({ [type]: blob }))
      this._setClipboardData(ClipboardDataFormat.TEXT, textData)
    }
    if (htmlData) {
      const type = 'text/html'
      const blob = new Blob([htmlData], { type })
      clipboardItems.push(new ClipboardItem({ [type]: blob }))
      this._setClipboardData(ClipboardDataFormat.HTML, htmlData)
    }
    if (clipboardItems.length) {
      navigator.clipboard.write(clipboardItems)
    }
  }
}

export const gClipboard = new ClipboardHandler()

function getDataForCopy(editorKit: IEditorKit) {
  gClipboard.isGettingCopyData = true
  const textData = getSelectedText(editorKit.presentation, false, {
    insertNewLineAtParaEnd: true,
    insertNewLine: true,
  })
  const { internalData, htmlData } = serializeCopyData(
    editorKit,
    editorKit.sourceFileId
  )

  gClipboard.isGettingCopyData = false
  return {
    internalData,
    textData,
    htmlData,
  }
}

function getDataForDrag(editorKit: IEditorKit) {
  gClipboard.isGettingCopyData = true
  const { presentation, sourceFileId } = editorKit
  const textParams = {
    insertNewLineAtParaEnd: true,
    insertNewLine: true,
  }
  let textData = getSelectedText(presentation, false, textParams)
  if (textData === '') {
    textData = getSelectedSpText(presentation, textParams)
  }
  // 确保拖拽至其他标签时能响应切换标签
  if (!textData || textData === '\r\n') {
    textData = '#\r\n'
  }
  const { internalData, htmlData } = serializeCopyData(
    editorKit,
    sourceFileId,
    true
  )

  gClipboard.isGettingCopyData = false
  return {
    internalData,
    textData,
    htmlData,
  }
}

function pasteData(
  editorKit: IEditorKit,
  format: ClipboardDataFormatValue,
  data: Nullable<string | HTMLPasteData>,
  textData?: Nullable<string>
) {
  if (
    EditorSettings.isViewMode ||
    editorKit.editorUI?.showManager?.isInShowMode
  ) {
    return
  }

  startEditAction(EditActionFlag.action_Document_PasteByHotKey)

  gSpecialPasteUtil.pasteStart()
  executePaste(editorKit, gClipboard, format, data, textData)
}

function removeOnCut(editorKit: IEditorKit) {
  if (!editorKit.canEdit()) {
    return
  }
  editorKit.editorUI.editorHandler.remove(1, true)
}

// getters for clipboard
async function getPasteDataFromClipboard() {
  const items = await navigator.clipboard.read()
  let htmlData: Nullable<string>
  let textData: Nullable<string>
  let imgFile: Nullable<File>
  for (let i = 0; i < items.length; i++) {
    const item = items[i]
    htmlData = htmlData || (await getTextByClipboardItem(item, true))
    textData = textData || (await getTextByClipboardItem(item))
    imgFile = imgFile || (await getImageByClipboardItem(item))
  }
  return { htmlData, textData, imageData: imgFile }
}

async function getTextByClipboardItem(item: ClipboardItem, isHtml = false) {
  const itemType = isHtml ? 'text/html' : 'text/plain'
  if (item && item.types.includes(itemType)) {
    const blob = await item['getType'](itemType)
    return await blob?.text()
  }
}

async function getImageByClipboardItem(item: ClipboardItem) {
  const imageType = item.types.find((type) => type.startsWith('image'))
  if (item && imageType) {
    const blob = await item['getType'](imageType)
    const name = `image.${imageType.substring('image/'.length)}`
    if (blob && name) {
      return new File([blob], name, { type: imageType })
    }
  }
}
