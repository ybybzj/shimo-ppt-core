import { Dict, Nullable, valuesOfDict } from '../../liber/pervasive'
import { isDict } from '../common/utils'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../core/common/utils'

import { ParaCollection } from '../core/Paragraph/ParaCollection'
import { Notes } from '../core/Slide/Notes'
import { Slide } from '../core/Slide/Slide'
import { SlideLayout } from '../core/Slide/SlideLayout'
import { SlideMaster } from '../core/Slide/SlideMaster'
import { Theme } from '../core/SlideElement/attrs/theme'
import { resetConnectionShapesId } from '../core/utilities/helpers'

import { cloneSlideElement } from '../core/utilities/shape/clone'
import { InstanceType, isInstanceTypeOf } from '../core/instanceTypes'
import { SlideElement } from '../core/SlideElement/type'
import { DrawingExtra } from './type'

export const ContentTypeForCopyPaste = {
  Unknown: 0,
  Slides: 1,
  SlideElements: 2,
  TextContent: 3,
} as const

export type ContentTypeForCopyPasteValues = valuesOfDict<
  typeof ContentTypeForCopyPaste
>
export class PresentationContentForCopyPaste {
  slides: Slide[]
  notes: Nullable<Notes>[]

  layoutsIndices: number[]
  layouts: SlideLayout[]
  mastersIndices: number[]
  masters: SlideMaster[]
  themesIndices: number[]
  themes: Theme[]
  slideElements: CopyWrapperOfSlideElement[]
  paraCollection: Nullable<ParaCollection>
  slideWidth: number
  slideHeight: number
  themeName: Nullable<string>
  layoutsRefId: string[]
  mastersRefId: string[]
  themesRefId: string[]
  constructor() {
    this.slides = []
    this.notes = []

    this.layoutsIndices = []
    this.layouts = []
    this.mastersIndices = []
    this.masters = []
    this.themesIndices = []
    this.themes = []
    this.slideElements = []
    this.paraCollection = null
    this.slideWidth = 0
    this.slideHeight = 0
    this.themeName = null
    this.layoutsRefId = []
    this.mastersRefId = []
    this.themesRefId = []
  }

  clone() {
    const ret = new PresentationContentForCopyPaste()
    const s = turnOffRecordChanges()
    for (let i = 0; i < this.slides.length; ++i) {
      const slide = this.slides[i].clone()
      ret.slides.push(slide)
    }
    for (let i = 0; i < this.notes.length; ++i) {
      const idMap = {}
      const notes = this.notes[i]?.clone(idMap)!
      if (notes) {
        resetConnectionShapesId(notes.cSld.spTree, idMap)
      }
      ret.notes.push(notes)
    }

    for (let i = 0; i < this.layoutsIndices.length; ++i) {
      ret.layoutsIndices.push(this.layoutsIndices[i])
    }
    for (let i = 0; i < this.layouts.length; ++i) {
      const idMap = {}
      const layout = this.layouts[i].clone(idMap)
      resetConnectionShapesId(layout.cSld.spTree, idMap)
      ret.layouts.push(layout)
    }
    for (let i = 0; i < this.mastersIndices.length; ++i) {
      ret.mastersIndices.push(this.mastersIndices[i])
    }

    for (let i = 0; i < this.masters.length; ++i) {
      const idMap = {}
      const slideMaster = this.masters[i].clone(idMap)
      resetConnectionShapesId(slideMaster.cSld.spTree, idMap)
      ret.masters.push(slideMaster)
    }

    for (let i = 0; i < this.themesIndices.length; ++i) {
      ret.themesIndices.push(this.themesIndices[i])
    }
    for (let i = 0; i < this.themes.length; ++i) {
      ret.themes.push(this.themes[i].clone())
    }
    for (let i = 0; i < this.layoutsRefId.length; ++i) {
      ret.layoutsRefId.push(this.layoutsRefId[i])
    }
    for (let i = 0; i < this.mastersRefId.length; ++i) {
      ret.mastersRefId.push(this.mastersRefId[i])
    }

    for (let i = 0; i < this.themesRefId.length; ++i) {
      ret.themesRefId.push(this.themesRefId[i])
    }

    const idMap = {}
    const arrOfSp: SlideElement[] = []
    for (let i = 0; i < this.slideElements.length; ++i) {
      ret.slideElements.push(this.slideElements[i].clone(idMap))
      if (ret.slideElements[ret.slideElements.length - 1].slideElement) {
        arrOfSp.push(
          ret.slideElements[ret.slideElements.length - 1].slideElement
        )
      }
    }
    resetConnectionShapesId(arrOfSp, idMap)
    if (this.paraCollection) {
      ret.paraCollection = new ParaCollection()
      const elements = this.paraCollection.elements
      for (let i = 0; i < elements.length; ++i) {
        const element = elements[i]
        const para = elements[i].item

        ret.paraCollection.add(para.clone(para.parent), element.selectedAll)
      }
    }
    ret.slideWidth = this.slideWidth
    ret.slideHeight = this.slideHeight
    ret.themeName = this.themeName
    restoreRecordChangesState(s)
    return ret
  }

  getCopyPasteContentType() {
    if (this.slides.length > 0) {
      return ContentTypeForCopyPaste.Slides
    } else if (this.slideElements.length > 0) {
      return ContentTypeForCopyPaste.SlideElements
    } else if (this.paraCollection && !this.paraCollection.isEmpty()) {
      return ContentTypeForCopyPaste.TextContent
    }
    return ContentTypeForCopyPaste.Unknown
  }
}

export class CopyWrapperOfSlideElement {
  slideElement: SlideElement
  x: number
  y: number
  extX: number
  extY: number
  imageUrl: any
  extra?: DrawingExtra
  constructor(
    sp: SlideElement,
    x: number,
    y: number,
    extX: number,
    extY: number,
    imageUrl?,
    extra?: DrawingExtra
  ) {
    this.slideElement = sp
    this.x = x
    this.y = y
    this.extX = extX
    this.extY = extY
    this.imageUrl = imageUrl
    this.extra = extra
  }

  clone(map: Dict) {
    let copiedSp = this.slideElement
    if (this.slideElement) {
      if (isInstanceTypeOf(this.slideElement, InstanceType.GroupShape)) {
        copiedSp = cloneSlideElement(this.slideElement, map)
      } else {
        copiedSp = cloneSlideElement(this.slideElement)
      }
      if (isDict(map)) {
        map[this.slideElement.id] = copiedSp.id
      }
    }
    return new CopyWrapperOfSlideElement(
      copiedSp,
      this.x,
      this.y,
      this.extX,
      this.extY,
      this.imageUrl,
      this.extra
    )
  }
}
