import { Dict, Nullable } from '../../liber/pervasive'
import { hookManager, Hooks } from '../core/hooks'
import { EditorKit } from '../editor/global'
import { IEditorKit } from '../editor/type'
import {
  getIsUpdatingPasteDataFromClipboard,
  SpecialPasteUIOptions,
} from './common'
import { SpecialPasteKindValue } from './const'

class SpecialPasteUtil {
  editor: IEditorKit | null
  data: Dict
  kind: Nullable<SpecialPasteKindValue>
  isShowUI: boolean
  uiOptions: SpecialPasteUIOptions
  isSpecialPasteStart: boolean
  isSpecialPasteRevert: boolean
  isPasteStart: boolean
  constructor() {
    this.editor = null
    this.data = {}
    this.kind = null
    this.isShowUI = false
    this.uiOptions = new SpecialPasteUIOptions()
    this.isSpecialPasteStart = false
    this.isSpecialPasteRevert = false
    this.isPasteStart = false
  }

  init(editor: IEditorKit) {
    this.editor = editor
    hookManager.register(Hooks.EditorUI.OnUpdateScrolls, () => {
      this.updateUI()
    })
  }

  startSpecialPaste(isRevert: boolean) {
    if (getIsUpdatingPasteDataFromClipboard()) {
      return
    }
    this.isSpecialPasteStart = true
    this.isSpecialPasteRevert = isRevert
  }

  private endSpecialPaste() {
    if (getIsUpdatingPasteDataFromClipboard()) {
      return
    }
    this.isSpecialPasteStart = false
    this.isSpecialPasteRevert = false
  }

  pasteStart() {
    if (getIsUpdatingPasteDataFromClipboard()) {
      return
    }
    this.isPasteStart = true
  }

  pasteEnd() {
    if (getIsUpdatingPasteDataFromClipboard()) {
      return
    }
    this.isPasteStart = false
    if (this.isSpecialPasteStart) {
      this.endSpecialPaste()

      if (!this.uiOptions.isReset()) {
        this.isShowUI = true
        this.editor!.syncSpecialPasteUI(this.uiOptions)
      }
    } else {
      this.kind = null
      this.showUI()
    }
  }

  showUI() {
    if (getIsUpdatingPasteDataFromClipboard()) {
      return
    }
    if (!this.editor || !this.editor.canEdit()) return
    const options = this.uiOptions
    if (options?.formats) {
      if (EditorKit || options.screenPosition) {
        this.isShowUI = true
        this.editor.syncSpecialPasteUI(options)
      }
    }
  }

  hideUI(isNeedClean: boolean = true) {
    if (getIsUpdatingPasteDataFromClipboard()) {
      return
    }
    if (this.editor && this.isShowUI) {
      this.isShowUI = false
      if (isNeedClean) {
        this.resetUI()
      }
      this.editor.hideSpecialPasteUI()
    } else if (isNeedClean) {
      this.resetUI()
    }
  }

  updateUI() {
    if (getIsUpdatingPasteDataFromClipboard()) {
      return
    }
    if (!this.editor || !this.uiOptions || this.uiOptions.isReset()) {
      return
    }

    if (this.isShowUI && !this.isPasteStart) {
      this.editor.updateSpecialPasteUI()
    }
  }

  resetUI() {
    if (getIsUpdatingPasteDataFromClipboard()) {
      return
    }
    this.uiOptions.reset()
  }
}

export const gSpecialPasteUtil = new SpecialPasteUtil()
