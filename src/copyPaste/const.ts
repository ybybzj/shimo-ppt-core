import { valuesOfDict } from '../../liber/pervasive'

export const SpecialPasteKind = {
  sourceFormatting: 1,
  destinationTheme: 2,
  picture: 3,
  keepTextOnly: 4,
} as const
export type SpecialPasteKindValue = valuesOfDict<typeof SpecialPasteKind>
