import { Presentation } from '../../../core/Slide/Presentation'
import { ClrMap } from '../../../core/color/clrMap'
import { Theme } from '../../../core/SlideElement/attrs/theme'
import { Table } from '../../../core/Table/Table'
import { HtmlMakerNode } from './htmlMakerNode'
import { convertTable, convertParagraph } from './utils'
import { Paragraph } from '../../../core/Paragraph/Paragraph'

export class HtmlCopier {
  rootNode: HtmlMakerNode
  presentation: Presentation
  theme: Theme
  clrMap: ClrMap
  constructor(presentation: Presentation) {
    this.presentation = presentation
    this.rootNode = new HtmlMakerNode('root')
    this.theme = presentation.getTheme()!
    this.clrMap = presentation.getColorMap()
  }

  reset() {
    this.rootNode.reset()
    this.theme = this.presentation.getTheme()!
    this.clrMap = this.presentation.getColorMap()
  }

  getHtmlText() {
    return this.rootNode.getInnerHtmlText()
  }

  addTable(target: HtmlMakerNode, table: Table) {
    const clrMap = table.getColorMap()
    const tableNode = convertTable(table, this.theme, clrMap)
    target.appendChild(tableNode)
  }

  addImage(target: HtmlMakerNode, src: string, width: number, height: number) {
    const img = new HtmlMakerNode('img')
    img.attributes['width'] = width
    img.attributes['height'] = height
    img.attributes['src'] = src
    target.appendChild(img)
  }

  addParagraph(target: HtmlMakerNode, paragraph: Paragraph) {
    convertParagraph(target, paragraph, this.theme, this.clrMap)
  }
}
