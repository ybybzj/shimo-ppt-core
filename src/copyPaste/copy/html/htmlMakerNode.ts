import { Dict } from '../../../../liber/pervasive'

export class HtmlMakerNode {
  name: string
  attributes: Dict
  children: HtmlMakerNode[]
  isText: boolean
  constructor(name: string, isText?: true) {
    this.name = name
    this.attributes = {}
    this.children = []
    this.isText = isText === true
  }

  reset() {
    this.children.forEach((node) => node.reset())
    this.children.length = 0
    this.attributes = {}
  }

  appendChild(child: HtmlMakerNode) {
    if (
      child.isText &&
      this.children.length > 0 &&
      this.children[this.children.length - 1].isText
    ) {
      this.children[this.children.length - 1].name += child.name
    } else {
      this.children.push(child)
    }
  }

  addAttributes(attrs: Dict) {
    Object.keys(attrs).forEach((key) => {
      this.attributes[key] = attrs[key]
    })
  }

  wrapChildrenBy(node: HtmlMakerNode) {
    this.children.forEach((child) => node.appendChild(child))
    this.children = [node]
  }

  hasChild() {
    return this.children.length > 0
  }

  getInnerHtmlText() {
    if (this.isText) {
      return this.name
    } else {
      return this.children.reduce((res, node) => {
        return (res += node.getHtmlText())
      }, '')
    }
  }

  private getHtmlText() {
    if (this.isText) {
      return this.name
    } else {
      const inner = this.getInnerHtmlText()
      return `<${this.name}${this.getAttributesText()}${
        inner ? `>${inner}</${this.name}>` : '/>'
      }`
    }
  }

  private getAttributesText() {
    return Object.keys(this.attributes).reduce((res, attribute) => {
      return (res += ` ${attribute}="${this.attributes[attribute]}"`)
    }, '')
  }
}
