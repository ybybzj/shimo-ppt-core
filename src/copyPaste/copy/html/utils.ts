import { Dict, Nullable } from '../../../../liber/pervasive'
import {
  LineHeightRule,
  ShdType,
  VertAlignType,
} from '../../../core/common/const/attrs'
import { stringFromCharCode } from '../../../common/utils'
import { calculateTableGrid } from '../../../core/calculation/calculate/table'
import { ClrMap } from '../../../core/color/clrMap'
import {
  Factor_mm_to_pix,
  Factor_mm_to_pt,
  HightLight_None,
} from '../../../core/common/const/unit'
import { NumberingFormat } from '../../../core/common/numberingFormat'
import { ParaHyperlink } from '../../../core/Paragraph/ParaContent/ParaHyperlink'
import { ParaRun } from '../../../core/Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../../../core/Paragraph/Paragraph'
import { RunContentElement } from '../../../core/Paragraph/RunElement/type'
import { Theme } from '../../../core/SlideElement/attrs/theme'
import { Table } from '../../../core/Table/Table'
import { TableCell } from '../../../core/Table/TableCell'
import { TableCellBorder } from '../../../core/Table/attrs/CellBorder'
import { TableCellPr } from '../../../core/Table/attrs/TableCellPr'
import { TableBorders, TablePr } from '../../../core/Table/attrs/TablePr'
import { TextPr } from '../../../core/textAttributes/TextPr'
import { TextDocument } from '../../../core/TextDocument/TextDocument'
import { InstanceType } from '../../../core/instanceTypes'
import {
  correctTableCellPr,
  escapeString,
  getHexColorFromRGB,
} from '../../utils'
import { HtmlMakerNode } from './htmlMakerNode'
import { TableCellMargins } from '../../../core/Table/attrs/tableMargins'
import { ManagedSliceUtil } from '../../../common/managedArray'
import {
  TCBorderValue,
  TableLengthUnitType,
  VMergeType,
} from '../../../core/common/const/table'

export const convertTable = (table: Table, theme: Theme, clrMap: ClrMap) => {
  const tableNode = new HtmlMakerNode('table')
  const calcedPr = table.getCalcedPr()
  let pr: TablePr | undefined
  if (calcedPr && null != calcedPr.tablePr) pr = calcedPr.tablePr
  let tblStyle = ''
  const isBorder = false
  if (pr) {
    if (
      pr.shd &&
      ShdType.Nil !== pr.shd.value &&
      (pr.shd.color || pr.shd.fillEffects)
    ) {
      tblStyle +=
        'background:' +
        getHexColorFromRGB(theme, clrMap, pr.shd.color, pr.shd.fillEffects) +
        ';'
    }
    if (null != pr.tableBorders) {
      tblStyle += convertTableBorders(pr.tableBorders, theme, clrMap)
    }
  }

  tableNode.addAttributes({
    cellspacing: 0,
    border: isBorder ? 1 : 0,
    cellpadding: 0,
  })
  if (tblStyle) {
    tableNode.addAttributes({
      style: tblStyle,
    })
  }

  ManagedSliceUtil.forEach(table.children, (_row, i) => {
    convertTableRow(tableNode, table, i, table.lastRow, theme, clrMap)
  })
  return tableNode
}

function convertTableRow(
  tableNode: HtmlMakerNode,
  table: Table,
  valueOfCurRow: number,
  valueOfMaxRow: number,
  theme: Theme,
  clrMap: ClrMap
) {
  const row = table.children.at(valueOfCurRow)
  if (row == null) return
  const tr = new HtmlMakerNode('tr')
  calculateTableGrid(table)
  const gridReduced = table.tableGridReduced
  const rowPr = row.getCalcedPr()
  if (
    rowPr?.height &&
    LineHeightRule.Auto !== rowPr.height.heightRule &&
    null != rowPr.height.value
  ) {
    const trStyle = `height: ${rowPr.height.value * Factor_mm_to_pt}pt;`
    tr.addAttributes({
      style: trStyle,
    })
  }
  const tablePr = table.getCalcedPr()?.tablePr
  ManagedSliceUtil.forEach(row.children, (cell) => {
    if (VMergeType.Continue !== cell.getVMerge()) {
      const startGridCol = cell.metrics.startGridCol
      const gridSpan = cell.getGridSpan()
      const width =
        gridReduced[startGridCol + gridSpan - 1] - gridReduced[startGridCol - 1]

      let valueOfRowSpan = table.getVMergeCountBelow(
        valueOfCurRow,
        startGridCol,
        gridSpan
      )
      if (valueOfCurRow + valueOfRowSpan - 1 > valueOfMaxRow) {
        valueOfRowSpan = valueOfMaxRow - valueOfCurRow + 1
        if (valueOfRowSpan <= 0) valueOfRowSpan = 1
      }
      convertTableCell(tr, cell, tablePr, width, valueOfRowSpan, theme, clrMap)
    }
  })

  tableNode.appendChild(tr)
}

function convertTableCell(
  trNode: HtmlMakerNode,
  cell: TableCell,
  tablePr: TablePr,
  width: number,
  rowSpan: number,
  theme: Theme,
  clrMap: ClrMap
) {
  const td = new HtmlMakerNode('td')
  let tdStyle = ''
  if (width > 0) {
    td.addAttributes({
      width: Math.round(width * Factor_mm_to_pix),
    })
    tdStyle += `width:${width * Factor_mm_to_pt}pt;`
  }
  if (rowSpan > 1) {
    td.addAttributes({
      rowspan: rowSpan,
    })
  }

  // const tablePr = null
  if (cell.calcedPrs && cell.calcedPrs.pr) {
    correctTableCellPr(cell.calcedPrs.pr, theme, clrMap)
  }

  let cellPr: TableCellPr | undefined
  if (null != cell.calcedPrs && null != cell.calcedPrs.pr) {
    cellPr = cell.calcedPrs.pr
    if (null != cellPr.gridSpan && cellPr.gridSpan > 1) {
      td.addAttributes({
        colspan: cellPr.gridSpan,
      })
    }
  }
  if (null != cellPr && null != cellPr.shd) {
    if (
      ShdType.Nil !== cellPr.shd.value &&
      (null != cellPr.shd.color || null != cellPr.shd.fillEffects)
    ) {
      tdStyle += `background-color:
        ${getHexColorFromRGB(
          theme,
          clrMap,
          cellPr.shd.color,
          cellPr.shd.fillEffects
        )};`
    }
  } else if (tablePr && tablePr.shd) {
    if (
      ShdType.Nil !== tablePr.shd.value &&
      (tablePr.shd.color || tablePr.shd.fillEffects)
    ) {
      tdStyle += `background-color:
        ${getHexColorFromRGB(
          theme,
          clrMap,
          tablePr.shd.color,
          tablePr.shd.fillEffects
        )};`
    }
  }
  const cellMar: Partial<TableCellMargins> = {}
  if (cellPr && cellPr.margins) {
    mergeDictInto(cellMar, cellPr.margins)
  }
  if (tablePr && tablePr.tableCellMargins) {
    mergeDictInto(cellMar, tablePr.tableCellMargins)
  }
  tdStyle += convertMMMarginToStyle(cellMar, 'padding')
  const cellBorder = cell.getBorders()
  tdStyle += convertTableBorders(cellBorder, theme, clrMap)

  if (tdStyle) {
    td.addAttributes({
      style: tdStyle,
    })
  }

  convertTextDocument(td, cell.textDoc, theme, clrMap)

  trNode.appendChild(td)
}

const convertTextDocument = (
  makerNode: HtmlMakerNode,
  document: TextDocument,
  theme: Theme,
  clrMap: ClrMap
) => {
  if (document?.children) {
    ManagedSliceUtil.forEach(document.children, (para) => {
      const itemType = para?.instanceType
      if (InstanceType.Paragraph === itemType) {
        convertParagraph(makerNode, para, theme, clrMap)
      }
    })
  }
}

export const convertParagraph = (
  makerNode: HtmlMakerNode,
  paragraph: Paragraph,
  theme: Theme,
  clrMap: ClrMap
) => {
  const paraNode = new HtmlMakerNode('p')
  const bulletInfo = paragraph.paraNumbering.bulletInfo
  const isNone = NumberingFormat.None === bulletInfo.type
  let isBullet = false
  let listStyle = ''
  if (!isNone) {
    switch (bulletInfo.type) {
      case NumberingFormat.arabicPeriod:
      case NumberingFormat.arabicParenR: {
        listStyle = 'decimal'
        break
      }
      case NumberingFormat.romanLcPeriod:
        listStyle = 'lower-roman'
        break
      case NumberingFormat.romanUcPeriod:
        listStyle = 'upper-roman'
        break

      case NumberingFormat.alphaLcParenR:
      case NumberingFormat.alphaLcPeriod: {
        listStyle = 'lower-alpha'
        break
      }
      case NumberingFormat.alphaUcParenR:
      case NumberingFormat.alphaUcPeriod: {
        listStyle = 'upper-alpha'
        break
      }

      default:
        listStyle = 'disc'
        isBullet = true
        break
    }
  }

  convertRunContainer(paragraph, paraNode, theme, clrMap)
  if (!paraNode.hasChild()) {
    paraNode.appendChild(new HtmlMakerNode('&nbsp;', true))
  }
  if (isNone) {
    makerNode.appendChild(paraNode)
  } else {
    const li = new HtmlMakerNode('li')
    li.attributes['style'] = 'list-style-type: ' + listStyle
    li.appendChild(paraNode)
    let targetList: HtmlMakerNode | undefined
    if (makerNode.children.length > 0) {
      const prevElem = makerNode.children[makerNode.children.length - 1]
      if (
        (isBullet && 'ul' === prevElem.name) ||
        (!isBullet && 'ol' === prevElem.name)
      ) {
        targetList = prevElem
      }
    }
    if (!targetList) {
      if (isBullet) targetList = new HtmlMakerNode('ul')
      else targetList = new HtmlMakerNode('ol')
      targetList.attributes['style'] = 'padding-left:40px'
      makerNode.appendChild(targetList)
    }
    targetList.appendChild(li)
  }
}

const convertRunContainer = (
  container: Paragraph | ParaHyperlink,
  makerNode: HtmlMakerNode,
  theme: Theme,
  clrMap: ClrMap
) => {
  for (let i = 0; i < container.children.length; i++) {
    const item = container.children.at(i)!
    if (item.instanceType === InstanceType.ParaRun) {
      const span = new HtmlMakerNode('span')
      convertRun(item, span)
      if (span.hasChild()) {
        _convertTextPrToNode(item.getCalcedTextPr(), span, theme, clrMap)
        makerNode.appendChild(span)
      }
    } else if (item.instanceType === InstanceType.ParaHyperlink) {
      const hyperlink = new HtmlMakerNode('a')
      const v = item.getValue()
      const toolTip = item.getToolTip()
      hyperlink.attributes['href'] = escapeString(v)
      hyperlink.attributes['title'] = escapeString(toolTip)

      convertRunContainer(item, hyperlink, theme, clrMap)
      makerNode.appendChild(hyperlink)
    }
  }
}

const convertRun = (item: ParaRun, target: HtmlMakerNode) => {
  for (let i = 0; i < item.children.length; i++) {
    convertRunContent(
      target,
      item.children.at(i),
      item.children.at(i + 1),
      item.children.length
    )
  }
}

function convertRunContent(
  target: HtmlMakerNode,
  item: Nullable<RunContentElement>,
  nextItem: Nullable<RunContentElement>,
  contentLength: number
) {
  if (item == null) return
  switch (item.instanceType) {
    case InstanceType.ParaText:
      const value = stringFromCharCode(item.value)
      if (value) {
        target.appendChild(new HtmlMakerNode(escapeString(value), true))
      }
      break
    case InstanceType.ParaSpace:
      if (
        (nextItem && nextItem.instanceType === InstanceType.ParaSpace) ||
        contentLength === 1
      ) {
        target.appendChild(new HtmlMakerNode('&nbsp;', true))
      } else {
        target.appendChild(new HtmlMakerNode(' ', true))
      }
      break
    case InstanceType.ParaTab: {
      const span = new HtmlMakerNode('span')
      span.attributes['style'] = 'white-space:pre;'
      span.appendChild(new HtmlMakerNode(String.fromCodePoint(0x09), true))
      target.appendChild(span)
      break
    }
    case InstanceType.ParaNewLine: {
      const br = new HtmlMakerNode('br')
      target.appendChild(br)
      const span = new HtmlMakerNode('span')
      span.appendChild(new HtmlMakerNode('&nbsp;', true))
      target.appendChild(span)
      break
    }
  }
}

export const convertTableBorders = (
  borders: TableBorders,
  theme: Theme,
  clrMap: ClrMap
) => {
  let res = ''
  if (null != borders.left) {
    res += convertTableBorder(borders.left, 'border-left', theme, clrMap)
  }
  if (null != borders.top) {
    res += convertTableBorder(borders.top, 'border-top', theme, clrMap)
  }
  if (null != borders.right) {
    res += convertTableBorder(borders.right, 'border-right', theme, clrMap)
  }
  if (null != borders.bottom) {
    res += convertTableBorder(borders.bottom, 'border-bottom', theme, clrMap)
  }
  return res
}

const convertTableBorder = (
  border: TableCellBorder,
  name: string,
  theme: Theme,
  clrMap: ClrMap
) => {
  if (TCBorderValue.None === border.value) {
    return `${name}:none;`
  } else {
    const fillEffects = border.fillEffects
    const size = null != border.size ? border.size * Factor_mm_to_pt : 0.5
    const color = border.color ?? ({ r: 0, g: 0, b: 0 } as any) // Todo, FIXME
    return `${name}
      :
      ${size}
      pt solid
      ${getHexColorFromRGB(theme, clrMap, color, fillEffects)}
      ;`
  }
}

const mergeDictInto = (o1: Dict, o2: Dict) => {
  if (!o1 || !o2) {
    return
  }
  for (const p in o2) {
    if (o2.hasOwnProperty(p) && !o1.hasOwnProperty(p)) {
      const v = o2[p]
      if (null != v) o1[p] = v
    }
  }
}
const convertMMMarginToStyle = (
  margins: Partial<TableCellMargins>,
  styleName: string
) => {
  let res = ''
  let valueOfMarginLeft = 1.9
  let valueOfMarginTop = 0
  let valueOfMarginRight = 1.9
  let valueOfMarginBottom = 0
  if (
    null != margins.left &&
    TableLengthUnitType.MM === margins.left.type &&
    null != margins.left.w
  ) {
    valueOfMarginLeft = margins.left.w
  }
  if (
    null != margins.top &&
    TableLengthUnitType.MM === margins.top.type &&
    null != margins.top.w
  ) {
    valueOfMarginTop = margins.top.w
  }
  if (
    null != margins.right &&
    TableLengthUnitType.MM === margins.right.type &&
    null != margins.right.w
  ) {
    valueOfMarginRight = margins.right.w
  }
  if (
    null != margins.bottom &&
    TableLengthUnitType.MM === margins.bottom.type &&
    null != margins.bottom.w
  ) {
    valueOfMarginBottom = margins.bottom.w
  }
  res =
    styleName +
    ':' +
    valueOfMarginTop * Factor_mm_to_pt +
    'pt ' +
    valueOfMarginRight * Factor_mm_to_pt +
    'pt ' +
    valueOfMarginBottom * Factor_mm_to_pt +
    'pt ' +
    valueOfMarginLeft * Factor_mm_to_pt +
    'pt;'
  return res
}

function _convertTextPrToNode(
  textPr: TextPr,
  target: HtmlMakerNode,
  theme: Theme,
  clrMap: ClrMap
) {
  const attrsArr: string[] = []
  if (textPr.rFonts) {
    const { ascii, hAnsi, eastAsia, cs } = textPr.rFonts
    let fontName = ascii?.name ?? hAnsi?.name ?? eastAsia?.name ?? cs?.name
    if (fontName) {
      const fontScheme = theme?.themeElements?.fontScheme
      if (fontScheme) {
        fontName = fontScheme.checkFont(fontName)
      }
      attrsArr.push(`font-family:'${escapeString(fontName)}'`)
    }
  }
  if (null != textPr.fontSize) {
    attrsArr.push(`font-size:${textPr.fontSize}pt`)
  }
  if (true === textPr.bold) target.wrapChildrenBy(new HtmlMakerNode('b'))
  if (true === textPr.italic) target.wrapChildrenBy(new HtmlMakerNode('i'))
  if (true === textPr.underline) target.wrapChildrenBy(new HtmlMakerNode('u'))
  if (true === textPr.strikeout) target.wrapChildrenBy(new HtmlMakerNode('s'))
  if (null != textPr.highLight && HightLight_None !== textPr.highLight) {
    attrsArr.push(
      'background-color:' +
        getHexColorFromRGB(theme, clrMap, textPr.highLight, null)
    )
  }
  if (textPr.fillEffects) {
    attrsArr.push(
      'color:' + getHexColorFromRGB(theme, clrMap, null, textPr.fillEffects)
    )
  }
  switch (textPr.vertAlign) {
    case VertAlignType.SuperScript:
      attrsArr.push('vertical-align:super')
      break
    case VertAlignType.SubScript:
      attrsArr.push('vertical-align:sub')
      break
  }
  if (attrsArr.length > 0) {
    target.addAttributes({
      style: attrsArr.join(';'),
    })
  }
}
