import { Nullable } from '../../../liber/pervasive'
import { TableSelectionType, VMergeType } from '../../core/common/const/table'
import { ParaRun } from '../../core/Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../../core/Paragraph/Paragraph'
import { ParagraphItem } from '../../core/Paragraph/type'
import { ParaCollection } from '../../core/Paragraph/ParaCollection'
import { Table } from '../../core/Table/Table'
import { TableCell } from '../../core/Table/TableCell'
import { TableRow } from '../../core/Table/TableRow'
import { TableOrTextDocument } from '../../core/utilities/type'
import { InstanceType } from '../../core/instanceTypes'

export function collectSelectedContentInParagraph(
  para: Paragraph,
  paraCollection: ParaCollection
) {
  let newParagraph: Paragraph
  if (para.isSetToAll === true) {
    newParagraph = para.clone(para.parent)
    paraCollection.add(newParagraph, true)
    return
  }

  if (true !== para.selection.isUse) return

  let startIndex = para.selection.startIndex
  let endIndex = para.selection.endIndex
  if (startIndex > endIndex) {
    startIndex = para.selection.endIndex
    endIndex = para.selection.startIndex
  }

  if (true === para.isSelectFromStart() && true === para.isParaEndSelected()) {
    newParagraph = para.clone(para.parent)
    paraCollection.add(newParagraph, true)
  } else {
    newParagraph = Paragraph.new(para.parent)

    newParagraph.setPr(para.pr)
    newParagraph.paraTextPr.setTextPr(para.paraTextPr.textPr)

    for (let i = startIndex; i <= endIndex; i++) {
      const item = para.children.at(i)
      if (item == null) continue
      if (
        (startIndex === i || endIndex === i) &&
        true !== item.isSelectedAll()
      ) {
        const itemContent = copySelectedContentOfParaItem(item)
        for (let j = 0, l = itemContent.length; j < l; j++) {
          newParagraph.addItemAt(i - startIndex + j, itemContent[j])
        }
      } else {
        newParagraph.addItemAt(i - startIndex, item.clone(false))
      }
    }

    paraCollection.add(newParagraph, false)
  }
}

function copySelectedContentOfParaItem(paraItem: ParagraphItem) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    const copiedContent: ParaRun[] = []

    let startIndex = 0
    let endIndex = paraItem.children.length - 1

    if (true === paraItem.state.selection.isUse) {
      startIndex = paraItem.state.selection.startIndex
      endIndex = paraItem.state.selection.endIndex
      if (startIndex > endIndex) {
        startIndex = paraItem.state.selection.endIndex
        endIndex = paraItem.state.selection.startIndex
      }
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const run = paraItem.children.at(i)
      if (run == null) continue
      const copySelected =
        (startIndex === i || endIndex === i) && true !== run.isSelectedAll()

      copiedContent.push(run.clone(copySelected))
    }
    for (let i = 0, l = copiedContent.length; i < l; i++) {
      const run = copiedContent[i]
      run.clearTextFormat(true)
    }

    return copiedContent
  } else {
    return [paraItem.clone(true)]
  }
}

export function collectSelectedTextContent(
  target: TableOrTextDocument,
  paraCollection?: ParaCollection
) {
  if (target.instanceType === InstanceType.TextDocument) {
    if (paraCollection == null) {
      return
    }

    if (target.isSetToAll === true) {
      for (let i = 0, l = target.children.length; i < l; i++) {
        const para = target.children.at(i)
        if (para) {
          const oldSetToAll = para.isSetToAll
          para.isSetToAll = true
          collectSelectedContentInParagraph(para, paraCollection)
          para.isSetToAll = oldSetToAll
        }
      }
      return
    }

    if (true !== target.selection.isUse) return

    let startIndex = target.selection.startIndex
    let endIndex = target.selection.endIndex
    if (startIndex > endIndex) {
      startIndex = target.selection.endIndex
      endIndex = target.selection.startIndex
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const para = target.children.at(i)
      para && collectSelectedContentInParagraph(para, paraCollection)
    }
  } else {
    return collectSelectedContentInTable(target, paraCollection)
  }
}

type CellInfo = {
  cell: Nullable<TableCell>
  gridSpan: number
  isSelected: boolean
}

function collectSelectedContentInTable(
  table: Table,
  paraCollection?: ParaCollection
): Nullable<Table> {
  if (true !== table.selection.isUse) return

  if (true === table.isSetToAll) {
    return table.clone(table.parent)
  }

  if (TableSelectionType.CELL === table.selection.type) {
    let isAllSelected = true
    const selectedCellsCount = table.selection.cellPositions.length

    const rowsInfo: Array<{
      cellsInfo: CellInfo[]
      Selected: boolean
    }> = []

    const rowsCount = table.children.length
    for (let rIdx = 0; rIdx < rowsCount; rIdx++) {
      const row = table.children.at(rIdx)!
      const cellsCount = row.cellsCount

      const cellsInfo: CellInfo[] = []

      let isSelectedRow = false

      cellsInfo.push({
        gridSpan: 0,
        cell: null,
        isSelected: false,
      })

      for (let i = 0; i < cellsCount; i++) {
        const cell = row.getCellByIndex(i)!
        const gridSpan = cell.getGridSpan()
        const vMerge = cell.getVMerge()

        let isSelected = false
        if (vMerge === VMergeType.Restart) {
          for (let j = 0; j < selectedCellsCount; j++) {
            const { cell: ci, row: ri } = table.selection.cellPositions[j]
            if (i === ci && rIdx === ri) {
              isSelected = true
              break
            } else if (rIdx < ri) break
          }
        } else {
          const cell = table.getTopLeftMergedCell(i, rIdx)!
          const rowInfo = rowsInfo[cell.parent.index]
          isSelected = rowInfo?.cellsInfo[cell.index + 1]?.isSelected
        }

        if (false === isSelected) isAllSelected = false
        else isSelectedRow = true

        cellsInfo.push({
          gridSpan,
          cell,
          isSelected,
        })
      }

      cellsInfo.push({
        gridSpan: 0,
        cell: null,
        isSelected: false,
      })

      rowsInfo.push({
        cellsInfo: cellsInfo,
        Selected: isSelectedRow,
      })
    }

    if (true === isAllSelected) {
      return table.clone(table.parent)
    }

    const tableGrid = table.tableGridCalculated.slice()

    let beforeMinGridSpan = -1
    let afterMinGridSpan = -1
    for (let i = 0; i < rowsCount; i++) {
      const cellsInfo = rowsInfo[i].cellsInfo

      if (true !== rowsInfo[i].Selected) continue

      let isBefore = true
      let gridSpanBefore = 0,
        gridSpanAfter = 0
      for (let cellIndex = 0; cellIndex < cellsInfo.length; cellIndex++) {
        const cellInfo = cellsInfo[cellIndex]
        if (true === cellInfo.isSelected) {
          isBefore = false
        } else if (true === isBefore) {
          gridSpanBefore += cellInfo.gridSpan
        } else {
          gridSpanAfter += cellInfo.gridSpan
        }
      }

      if (beforeMinGridSpan > gridSpanBefore || -1 === beforeMinGridSpan) {
        beforeMinGridSpan = gridSpanBefore
      }

      if (afterMinGridSpan > gridSpanAfter || -1 === afterMinGridSpan) {
        afterMinGridSpan = gridSpanAfter
      }
    }

    for (let i = 0; i < rowsCount; i++) {
      const cellsInfo = rowsInfo[i].cellsInfo

      if (true === rowsInfo[i].Selected) {
        cellsInfo[0].gridSpan -= beforeMinGridSpan
        cellsInfo[cellsInfo.length - 1].gridSpan -= afterMinGridSpan
      }
    }

    if (afterMinGridSpan > 0) {
      tableGrid.splice(tableGrid.length - afterMinGridSpan, afterMinGridSpan)
    } // tableGrid.length - (MinAfter - 1) - 1

    if (beforeMinGridSpan > 0) tableGrid.splice(0, beforeMinGridSpan)

    const newTable = new Table(table.parent, 0, 0, tableGrid)

    newTable.setTableStyleId(table.tableStyleId)
    newTable.setTableLook(table.tableLook.clone())
    newTable.setPr(table.pr.clone())

    for (let rowIndex = 0; rowIndex < rowsCount; rowIndex++) {
      const rowInfo = rowsInfo[rowIndex]
      if (true !== rowInfo.Selected) continue

      const cellsInfo = rowInfo.cellsInfo

      const row = TableRow.new(newTable, 0)

      row.setPr(table.children.at(rowIndex)!.pr)

      let isMergedRow = true
      for (let cellIndex = 0; cellIndex < cellsInfo.length; cellIndex++) {
        const cellInfo = cellsInfo[cellIndex]
        if (true === cellInfo.isSelected) {
          row.children.push(cellInfo.cell!.clone(row))

          const vMerge = cellInfo.cell?.getVMerge()
          if (vMerge === VMergeType.Restart) isMergedRow = false
        }
      }

      if (true === isMergedRow) continue

      row.updateCellIndices()

      newTable.children.push(row)
    }

    newTable.updateRowIndices(0)

    if (
      newTable.children.length > 0 &&
      newTable.children.at(0)!.cellsCount > 0
    ) {
      newTable.curCell = newTable.children.at(0)!.getCellByIndex(0)
    }

    return newTable
  } else {
    collectSelectedTextContent(table.curCell!.textDoc, paraCollection)
  }
}
