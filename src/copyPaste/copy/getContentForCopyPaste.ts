import { Dict, Nullable, valuesOfDict } from '../../../liber/pervasive'
import { isDict } from '../../common/utils'
import { calculateForGraphicFrame } from '../../core/calculation/calculate/graphicFrame'
import { checkBounds_SlideElement } from '../../core/checkBounds/slideElement'
import { FocusTarget } from '../../core/common/const/drawing'
import {
  Factor_mm_to_pix,
  Factor_pix_to_mm,
} from '../../core/common/const/unit'
import {
  turnOffRecordChanges,
  restoreRecordChangesState,
} from '../../core/common/utils'
import { GraphicsBoundsChecker } from '../../core/graphic/Bounds'
import { getCanvasDrawer } from '../../core/graphic/CanvasDrawer'
import { Paragraph } from '../../core/Paragraph/Paragraph'
import { drawSlideElement } from '../../core/render/drawSlideElement'
import { getBase64ImgForSlideElement } from '../../core/render/toImage'
import { ParaCollection } from '../../core/Paragraph/ParaCollection'
import { Presentation } from '../../core/Slide/Presentation'
import { resetConnectionShapesId } from '../../core/utilities/helpers'
import { isPlaceholder } from '../../core/utilities/shape/asserts'
import { ensureSpPrXfrm } from '../../core/utilities/shape/attrs'
import {
  cloneSlideElement,
  updateOffsetByGroupParent,
} from '../../core/utilities/shape/clone'
import { buildImageShape } from '../../core/utilities/shape/creators'
import { InstanceType } from '../../core/instanceTypes'
import { SlideElement } from '../../core/SlideElement/type'
import {
  PresentationContentForCopyPaste,
  CopyWrapperOfSlideElement,
} from '../copyPasteContent'
import {
  collectSelectedContentInParagraph,
  collectSelectedTextContent,
} from './collectTextContent'
import { hasCalcStatusSet } from '../../core/calculation/updateCalcStatus/calcStatus'
import { calculateForSlide } from '../../core/calculation/calculate/slide'
import { isBgTransparentForSp } from '../../core/utilities/shape/getters'
import {
  cloneSpWithFormatting,
  saveFormattingForParagraphs,
} from '../../core/utilities/shape/formatting'
import { getDrawingExtra } from '../utils'

export type PptSelectedContentsForCopy = [
  PresentationContentForCopyPaste,
  PresentationContentForCopyPaste,
  PresentationContentForCopyPaste,
]

export const PptSelectedContentsForCopyIndex = {
  destination: 0,
  source: 1,
  image: 2,
} as const

export type PptSelectedContentsForCopyIndexValues = valuesOfDict<
  typeof PptSelectedContentsForCopyIndex
>

export function getContentOfPptForCopyPaste(
  presentation: Presentation,
  forDrag = false
) {
  const s = turnOffRecordChanges()

  const destinationContent = new PresentationContentForCopyPaste()
  const sourceContent = new PresentationContentForCopyPaste()
  const imagesSelectedContent = new PresentationContentForCopyPaste()
  const result: PptSelectedContentsForCopy = [
    destinationContent,
    sourceContent,
    imagesSelectedContent,
  ]

  sourceContent.slideWidth = presentation.width
  sourceContent.slideHeight = presentation.height
  destinationContent.slideWidth = presentation.width
  destinationContent.slideHeight = presentation.height
  imagesSelectedContent.slideWidth = presentation.width
  imagesSelectedContent.slideHeight = presentation.height
  // selection
  if (presentation.slides.length > 0) {
    const focusType = presentation.getFocusType()
    switch (focusType) {
      case FocusTarget.EditorView: {
        const selectTextSp =
          presentation.selectionState.selectedTextSp ??
          presentation.selectionState.selectedTable?.parent
        if (selectTextSp) {
          const targettextTarget = presentation.getCurrentTextTarget()
          if (
            selectTextSp.instanceType === InstanceType.GraphicFrame &&
            !targettextTarget
          ) {
            if (selectTextSp.graphicObject) {
              const graphicFrame = cloneSlideElement(selectTextSp)
              const table = collectSelectedTextContent(
                selectTextSp.graphicObject
              )
              if (table) {
                graphicFrame.setGraphicObject(table)
                table.setParent(graphicFrame)
              }

              const extra = getDrawingExtra(graphicFrame)

              destinationContent.slideElements.push(
                new CopyWrapperOfSlideElement(
                  graphicFrame,
                  selectTextSp.x,
                  selectTextSp.y,
                  selectTextSp.extX,
                  selectTextSp.extY,
                  getBase64ImgForSlideElement(selectTextSp),
                  extra
                )
              )
              graphicFrame.spTreeParent = selectTextSp.spTreeParent
              graphicFrame.parent = selectTextSp.parent
              graphicFrame.isDeleted = false
              calculateForGraphicFrame(graphicFrame)
              sourceContent.slideElements.push(
                new CopyWrapperOfSlideElement(
                  cloneSpWithFormatting(graphicFrame),
                  selectTextSp.x,
                  selectTextSp.y,
                  selectTextSp.extX,
                  selectTextSp.extY,
                  getBase64ImgForSlideElement(selectTextSp),
                  extra
                )
              )
              const image = buildImageShape(
                { url: getBase64ImgForSlideElement(graphicFrame) },
                selectTextSp.x,
                selectTextSp.y,
                graphicFrame.extX,
                graphicFrame.extY
              )
              imagesSelectedContent.slideElements.push(
                new CopyWrapperOfSlideElement(
                  image,
                  0,
                  0,
                  selectTextSp.extX,
                  selectTextSp.extY,
                  getBase64ImgForSlideElement(selectTextSp)
                )
              )
              graphicFrame.parent = null
              graphicFrame.isDeleted = true
            }
          } else {
            if (targettextTarget) {
              let paraCollection = new ParaCollection()
              if (
                forDrag === true &&
                targettextTarget.selection.isUse !== true
              ) {
                const curPara = targettextTarget.children.at(
                  targettextTarget.cursorPosition.childIndex
                )
                const curParaItem = curPara?.children.at(
                  curPara.cursorPosition.elementIndex
                )
                if (
                  curPara &&
                  curParaItem?.instanceType === InstanceType.ParaHyperlink
                ) {
                  const newPara = curPara.clone(curPara?.parent)
                  newPara.selection.isUse = true
                  newPara.selection.startIndex =
                    curPara.cursorPosition.elementIndex
                  newPara.selection.endIndex =
                    curPara.cursorPosition.elementIndex
                  newPara.cursorPosition.elementIndex =
                    curPara.cursorPosition.elementIndex

                  const newHyper = newPara.children.at(
                    curPara.cursorPosition.elementIndex
                  )
                  if (newHyper) {
                    newHyper.selectAll()
                    collectSelectedContentInParagraph(newPara, paraCollection)
                  }
                } else {
                  const oldSetToAll = targettextTarget.isSetToAll
                  targettextTarget.isSetToAll = true
                  collectSelectedTextContent(targettextTarget, paraCollection)
                  targettextTarget.isSetToAll = oldSetToAll
                }
              } else {
                collectSelectedTextContent(targettextTarget, paraCollection)
              }

              destinationContent.paraCollection = paraCollection

              paraCollection = paraCollection.clone()
              // collectSelectedTextContent(targettextTarget, paraCollection)
              const paragraphs: Array<Paragraph> = []
              paraCollection.elements.forEach(({ item: para }) => {
                para.parent = targettextTarget
                para._calcParaPr()
                paragraphs.push(para)
              })
              saveFormattingForParagraphs(
                paragraphs,
                paragraphs,
                targettextTarget.getTheme(),
                targettextTarget.getColorMap()
              )
              sourceContent.paraCollection = paraCollection
            }
          }
        } else {
          const selectedGroup = presentation.selectionState.selectedGroup
          const isNeedRecurseGroup = isDict(selectedGroup)
          const spTree = isNeedRecurseGroup
            ? selectedGroup.spTree
            : presentation.slides[presentation.currentSlideIndex].cSld.spTree
          const theme = presentation.getCurrentFocusEditContainer()?.getTheme()
          if (theme) {
            destinationContent.themeName = theme.name
            sourceContent.themeName = theme.name
            imagesSelectedContent.themeName = theme.name
          }
          ;[destinationContent, sourceContent].forEach((content, i) => {
            if (theme) {
              content.themeName = theme.name
            }
            const idMap = {}
            const isSourceFormatting = i === 1
            collectSelectedElements(
              spTree,
              content.slideElements,
              isNeedRecurseGroup,
              idMap,
              isSourceFormatting
            )
            resetConnectionShapesId(content.slideElements, idMap)
          })

          if (presentation.selectionState.hasSelection) {
            const dFactor = Factor_mm_to_pix
            const w_mm = presentation.width
            const h_mm = presentation.height
            const w_px = (w_mm * dFactor + 0.5) >> 0
            const h_px = (h_mm * dFactor + 0.5) >> 0

            const selectedElements =
              presentation.selectionState.selectedElements
            const slideBoundsCheker = new GraphicsBoundsChecker()
            slideBoundsCheker.init(w_px, h_px, w_mm, h_mm)
            slideBoundsCheker.transform(1, 0, 0, 1, 0, 0)
            slideBoundsCheker.needCheckLineWidth = true
            selectedElements.forEach((object) => {
              checkBounds_SlideElement(object, slideBoundsCheker)
            })

            const { minX, maxX, minY, maxY } = slideBoundsCheker.bounds

            const needPxWidth = maxX - minX + 1
            const needPxHeight = maxY - minY + 1
            if (needPxWidth > 0 && needPxHeight > 0) {
              const canvas = document.createElement('canvas')
              canvas.width = needPxWidth
              canvas.height = needPxHeight

              const ctx = canvas.getContext('2d')!
              const canvasDrawer = getCanvasDrawer(ctx, w_px, h_px, w_mm, h_mm)
              canvasDrawer.setCoordTransform(-minX, -minY)

              canvasDrawer.updateTransform(1, 0, 0, 1, 0, 0)

              canvasDrawer.isInPreview = true
              selectedElements.forEach((object) => {
                canvasDrawer.isBgTransparent = isBgTransparentForSp(object)
                drawSlideElement(object, canvasDrawer)
                canvasDrawer.isBgTransparent = false
              })

              canvasDrawer.isInPreview = false
              canvasDrawer.resetState()

              let base64Img
              try {
                base64Img = canvas.toDataURL('image/png')
              } catch (err) {
                base64Img = ''
              }

              const w = canvas.width * Factor_pix_to_mm
              const h = canvas.height * Factor_pix_to_mm
              const image = buildImageShape(
                { url: base64Img },
                minX * Factor_pix_to_mm,
                minY * Factor_pix_to_mm,
                w,
                h
              )
              imagesSelectedContent.slideElements.push(
                new CopyWrapperOfSlideElement(image, 0, 0, w, h, base64Img)
              )
            }
          }
        }
        break
      }
      case FocusTarget.Thumbnails: {
        const selectedSlideIndices = presentation.getSelectedSlideIndices()
        const layouts: Dict = {}
        const masters: Dict = {}
        const themes: Dict = {}
        // const notesMasters: Dict = {}
        selectedSlideIndices.forEach((slideIndex, i) => {
          const slide = presentation.slides[slideIndex]
          const slideCopy = slide.clone(false)
          const layout = slide.layout

          const layoutIndex = collectRefItems(
            layout,
            i,
            sourceContent.layoutsIndices,
            sourceContent.layouts,
            layouts
          )

          if (layoutIndex !== -1) {
            const master = layout!.master
            const masterIndex = collectRefItems(
              master,
              layoutIndex,
              sourceContent.mastersIndices,
              sourceContent.masters,
              masters
            )

            if (masterIndex !== -1) {
              const theme = master!.theme
              collectRefItems(
                theme,
                masterIndex,
                sourceContent.themesIndices,
                sourceContent.themes,
                themes
              )
            }
          }

          const slideForSource = slide.clone(true)
          if (hasCalcStatusSet(slideForSource)) {
            calculateForSlide(slideForSource)
          }
          sourceContent.slides.push(slideForSource)
          destinationContent.slides.push(slideCopy)

          // 生成 slide 图片
          {
            const base64Img = slide.getBase64Image()
            if (base64Img) {
              slideForSource.base64Img = base64Img

              const image = buildImageShape(
                { url: base64Img },
                0,
                0,
                presentation.width,
                presentation.height
              )
              imagesSelectedContent.slideElements.push(
                new CopyWrapperOfSlideElement(
                  image,
                  0,
                  0,
                  presentation.width,
                  presentation.height,
                  base64Img
                )
              )
            }
          }

          const notes = slide.notes
          if (notes?.isEmptyNotes() === false) {
            const notesCopy = notes
            sourceContent.notes.push(notesCopy)
            destinationContent.notes.push(notesCopy)
          } else {
            sourceContent.notes.push(null)
            destinationContent.notes.push(null)
          }
        })
      }
    }
  }
  restoreRecordChangesState(s)
  return result
}

function collectSelectedElements(
  shapes: SlideElement[],
  sps: CopyWrapperOfSlideElement[],
  includeSpsInGrp: boolean,
  idMap: Dict,
  isSourceFormatting: boolean,
  ignoreExtra: boolean = false
) {
  shapes.forEach((shape) => {
    if (shape.selected) {
      let copiedSp: SlideElement

      if (!isSourceFormatting) {
        copiedSp = cloneSlideElement(shape, idMap)
        if (isPlaceholder(shape)) {
          copiedSp.x = shape.x
          copiedSp.y = shape.y
          copiedSp.extX = shape.extX
          copiedSp.extY = shape.extY
          copiedSp.rot = shape.rot
          ensureSpPrXfrm(copiedSp, true)
        }
      } else {
        copiedSp = cloneSpWithFormatting(shape, idMap)
      }

      if (includeSpsInGrp && shape.group) {
        updateOffsetByGroupParent(copiedSp, shape.group)
      }

      const extra = ignoreExtra === false ? getDrawingExtra(shape) : undefined
      sps.push(
        new CopyWrapperOfSlideElement(
          copiedSp,
          shape.x,
          shape.y,
          shape.extX,
          shape.extY,
          getBase64ImgForSlideElement(shape),
          extra
        )
      )
      if (isDict(idMap)) {
        idMap[shape.id] = copiedSp.id
      }
    }
    if (includeSpsInGrp && shape.instanceType === InstanceType.GroupShape) {
      collectSelectedElements(
        shape.spTree,
        sps,
        includeSpsInGrp,
        idMap,
        isSourceFormatting,
        true
      )
    }
  })
}

function collectRefItems<T extends { getId: () => string }>(
  item: Nullable<T>,
  parentIndex: number,
  indices: number[],
  collection: T[],
  map: Dict<number>
) {
  if (item != null) {
    let subIndex = map[item.getId()]
    if (subIndex == null) {
      subIndex = collection.push(item) - 1
      map[item.getId()] = subIndex
    }
    indices[parentIndex] = subIndex
  } else {
    indices[parentIndex] = -1
  }
  return indices[parentIndex]
}
