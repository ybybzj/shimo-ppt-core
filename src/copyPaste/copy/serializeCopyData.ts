import { IEditorKit } from '../../editor/type'
import { Factor_mm_to_pix } from '../../core/common/const/unit'
import { Presentation } from '../../core/Slide/Presentation'
import { EditorUtil } from '../../globals/editor'
import { InstanceType } from '../../core/instanceTypes'
import { opsFromContentItems } from '../../io/docContent'
import { Op, OpData, Operation } from '../../io/operation'
import { JSONFromCSld } from '../../io/readers/csld'
import { GraphicsBoundsChecker } from '../../core/graphic/Bounds'
import { gSpecialPasteUtil } from '../gSpecialPasteUtil'
import { gOffsetStateForCopyShape } from '../gOffsetStateForCopyShape'
import { CopyResult, DataInsideHtmlType, DrawingExtra } from '../type'
import { checkBounds_Slide } from '../../core/checkBounds/slide'
import { checkBounds_SlideElement } from '../../core/checkBounds/slideElement'
import { ClipboardSettings, InternalDataAttributeKey } from '../common'
import { convertToComputeStylesTable } from '../utils'
import { SlideElement } from '../../core/SlideElement/type'
import { GraphicFrame } from '../../core/SlideElement/GraphicFrame'
import { Slide } from '../../core/Slide/Slide'
import { HtmlCopier } from './html/htmlCopier'
import { ParaCollection } from '../../core/Paragraph/ParaCollection'
import { SlideLayout } from '../../core/Slide/SlideLayout'
import { SlideMaster } from '../../core/Slide/SlideMaster'
import { Notes } from '../../core/Slide/Notes'
// import { NotesMaster } from '../../core/Slide/NotesMaster'
import { Theme } from '../../core/SlideElement/attrs/theme'
import { getMapOpData } from '../../io/readers/helper'
import { JSONFromSlideMaster } from '../../io/readers/SlideMaster'
import { JSONFromSlide } from '../../io/readers/Slide'
import {
  PresentationContentForCopyPaste,
  CopyWrapperOfSlideElement,
} from '../copyPasteContent'
// import { JSONFromNotesMaster } from '../../io/readers/NotesMaster'
import { JSONFromSlideNotes } from '../../io/readers/Notes'
import { JSONFromTheme } from '../../io/readers/Theme'
import { JSONFromSlideLayout } from '../../io/readers/SlideLayout'
import { Dict, Nullable } from '../../../liber/pervasive'
import { getContentOfPptForCopyPaste } from './getContentForCopyPaste'
import { HtmlMakerNode } from './html/htmlMakerNode'
import { IOAdapter } from '../../io/adapter'

export function serializeCopyData(
  editorKit: IEditorKit,
  sourceFileId?: string,
  forDrag?: boolean
) {
  const presentation = editorKit.presentation
  const htmlCopier = new HtmlCopier(presentation)
  gOffsetStateForCopyShape.reset()
  gOffsetStateForCopyShape.setOriginSlideIndex(presentation.currentSlideIndex)
  gSpecialPasteUtil.hideUI()

  const [destination, source, picture] = getContentOfPptForCopyPaste(
    presentation,
    forDrag
  )
  if (
    !source.paraCollection &&
    !source.slideElements?.length &&
    !source.slides?.length
  ) {
    return {}
  }

  const sourceCopyResult = handleSelectedContent(editorKit, source, htmlCopier)
  const destCopyResult = handleSelectedContent(editorKit, destination)
  const pictureCopyResult = handleSelectedContent(editorKit, picture)
  const copyResultArray = [destCopyResult, sourceCopyResult, pictureCopyResult]
  const internalData = JSON.stringify(copyResultArray)
  const htmlData = getHtmlData(htmlCopier, internalData, sourceFileId)

  return {
    internalData,
    htmlData,
  }
}

function getHtmlData(
  htmlCopier: HtmlCopier,
  internalData = '',
  sourceFileId?: string
) {
  // 这里在 html 中放置一份数据
  const wrapDataElement = new HtmlMakerNode('div')
  wrapDataElement.attributes[InternalDataAttributeKey] =
    serializeDataInsideHtml(internalData, sourceFileId)
  htmlCopier.rootNode.wrapChildrenBy(wrapDataElement)

  return '<meta charset="UTF-8">' + htmlCopier.getHtmlText()
}

function handleSelectedContent(
  editorKit: IEditorKit,
  elementsContent: PresentationContentForCopyPaste,
  htmlCopier?: HtmlCopier
): CopyResult {
  const res: CopyResult = {}
  const themeName = elementsContent?.themeName ? elementsContent.themeName : ''

  res['themeName'] = themeName
  res['width'] = (elementsContent.slideWidth * 100000) >> 0
  res['height'] = (elementsContent.slideHeight * 100000) >> 0

  // docContent
  if (elementsContent.paraCollection) {
    res['docContent'] = serializeTextContent(
      editorKit,
      elementsContent.paraCollection,
      htmlCopier
    )
  }

  //Drawings
  if (
    elementsContent.slideElements &&
    elementsContent.slideElements.length > 0
  ) {
    res['drawings'] = serializeSlideElements(
      editorKit,
      elementsContent.slideElements,
      htmlCopier
    )

    const extras = getDrawingExtras(elementsContent.slideElements)
    if (extras) {
      res['drawingsExtra'] = extras
    }
  }

  //Slides
  if (elementsContent.slides && elementsContent.slides.length > 0) {
    res['slides'] = serializeSlides(
      editorKit,
      elementsContent.slides,
      htmlCopier
    )
    //SlidesRefId 根据 refId 存下顺序
    res['slidesRefId'] = elementsContent.slides.map((slide) => {
      return slide.getRefId()
    })
  }

  //Layouts
  if (elementsContent.layouts.length > 0) {
    res['layouts'] = serializeLayouts(editorKit, elementsContent.layouts)
    res['layoutsRefId'] = elementsContent.layouts.map((layout) =>
      layout.getRefId()
    )
    if (elementsContent.layoutsIndices.length > 0) {
      res['layoutsIndexes'] = elementsContent.layoutsIndices
    }
  }

  //Masters
  if (elementsContent.masters && elementsContent.masters.length > 0) {
    res['masters'] = serializeMasters(editorKit, elementsContent.masters)
    res['mastersRefId'] = elementsContent.masters.map(
      (master) => master?.getRefId()
    )
    if (elementsContent.mastersIndices?.length > 0) {
      res['mastersIndexes'] = elementsContent.mastersIndices
    }
  }

  //Notes
  if (elementsContent.notes?.length > 0) {
    res['notes'] = serializeNotes(editorKit, elementsContent.notes)
    res['notesRefId'] = elementsContent.notes.map((note) => {
      return note ? note.getRefId() : ''
    })
  }

  //Themes
  if (elementsContent.themes && elementsContent.themes.length > 0) {
    res['themes'] = serializeTheme(editorKit, elementsContent.themes)
    //ThemesRefId
    if (
      elementsContent.themesIndices &&
      elementsContent.themesIndices.length > 0
    ) {
      res['themesIndexes'] = elementsContent.themesIndices
      res['themesRefId'] = elementsContent.themes.map((theme) =>
        theme.getRefId()
      )
    }
  }
  return res
}

function serializeTextContent(
  editorKit: IEditorKit,
  paraCollection: ParaCollection,
  htmlCopier?: HtmlCopier
) {
  const paras = paraCollection.elements.map((elm) => elm.item)
  if (paras.length > 0) {
    if (htmlCopier) {
      paras.forEach((item) => {
        htmlCopier.addParagraph(htmlCopier.rootNode, item)
      })
    }

    return getDeltaString(editorKit.ioAdapter, opsFromContentItems(paras))
  }
}

function serializeSlideElements(
  editorKit: IEditorKit,
  elements: CopyWrapperOfSlideElement[],
  htmlCopier?: HtmlCopier
) {
  if (elements.length > 0) {
    const shapes = elements.map((item) => item.slideElement).filter(Boolean)
    const ops = JSONFromCSld({
      spTree: shapes,
    } as any)
    if (htmlCopier) {
      elements.forEach((element) => {
        const shape = element.slideElement
        if (shape.instanceType === InstanceType.GraphicFrame) {
          copyGraphicFrameToHtml(
            htmlCopier,
            shape,
            editorKit.presentation,
            elements.length === 1
          )
        } else {
          copySpToHtml(htmlCopier, shape, element)
        }
      })
    }
    return getDeltaString(editorKit.ioAdapter, ops)
  }
}

function getDrawingExtras(
  elements: CopyWrapperOfSlideElement[]
): Nullable<Dict<DrawingExtra>> {
  let hasExtra = false
  const result: Dict<DrawingExtra> = {}
  for (const elm of elements) {
    if (elm.extra != null) {
      hasExtra = true
      result[elm.slideElement.getRefId()] = elm.extra
    }
  }

  return hasExtra ? result : undefined
}

function copyGraphicFrameToHtml(
  htmlCopier: HtmlCopier,
  graphicFrame: GraphicFrame,
  presentation: Presentation,
  hanldeTableStyle: boolean
) {
  const table = graphicFrame.graphicObject
  if (!table) {
    return
  }
  EditorUtil.ChangesStack.stopRecording()
  if (hanldeTableStyle) {
    const theme = presentation.getTheme()
    const clrMap = presentation.getColorMap()
    convertToComputeStylesTable(table, theme, clrMap)
  }
  EditorUtil.ChangesStack.enableRecording()

  htmlCopier.addTable(htmlCopier.rootNode, table)
}

function copySpToHtml(
  htmlCopier: HtmlCopier,
  element: SlideElement,
  sp: CopyWrapperOfSlideElement
) {
  const src = sp.imageUrl
  if (src) {
    const checker = new GraphicsBoundsChecker()
    checkBounds_SlideElement(element, checker)

    let width, height
    if (sp && sp.extX) {
      width = Math.round(sp.extX * Factor_mm_to_pix)
    } else {
      width = Math.round(
        (checker.bounds.maxX - checker.bounds.minX + 1) * Factor_mm_to_pix
      )
    }

    if (sp && sp.extY) {
      height = Math.round(sp.extY * Factor_mm_to_pix)
    } else {
      height = Math.round(
        (checker.bounds.maxY - checker.bounds.minY + 1) * Factor_mm_to_pix
      )
    }

    htmlCopier.addImage(htmlCopier.rootNode, src, width, height)
  }
}

function serializeSlides(
  editorKit: IEditorKit,
  slides: Slide[],
  htmlCopier?: HtmlCopier
) {
  if (slides?.length) {
    if (htmlCopier) {
      for (let i = 0; i < slides.length; ++i) {
        if (i === 0) {
          copySlideToHtml(slides[i], htmlCopier)
          break
        }
      }
    }

    const ops = [
      Op.Insert(OpData.Map(getMapOpData(slides, JSONFromSlide) ?? {})),
    ]
    return getDeltaString(editorKit.ioAdapter, ops)
  }
}

function copySlideToHtml(slide: Slide, htmlCopier: HtmlCopier) {
  const src = slide.getBase64Image() || ''
  const checker = new GraphicsBoundsChecker()
  checkBounds_Slide(slide, checker)
  const width = Math.round(
    (checker.bounds.maxX - checker.bounds.minX + 1) * Factor_mm_to_pix
  )
  const height = Math.round(
    (checker.bounds.maxY - checker.bounds.minY + 1) * Factor_mm_to_pix
  )
  htmlCopier.addImage(htmlCopier.rootNode, src, width, height)
  gOffsetStateForCopyShape.copyShapeOnly = false
}

function serializeLayouts(editorKit: IEditorKit, layouts: SlideLayout[]) {
  if (layouts?.length) {
    const layoutData = getMapOpData(layouts, JSONFromSlideLayout)
    if (layoutData) {
      const ops = [Op.Insert(OpData.Map(layoutData))]
      return getDeltaString(editorKit.ioAdapter, ops)
    }
  }
}

function serializeMasters(editorKit: IEditorKit, masters: SlideMaster[]) {
  if (masters?.length) {
    const ops = [
      Op.Insert(OpData.Map(getMapOpData(masters, JSONFromSlideMaster) as any)),
    ]
    return getDeltaString(editorKit.ioAdapter, ops)
  }
}

function serializeNotes(editorKit: IEditorKit, notes: Nullable<Notes>[]) {
  if (notes.length > 0) {
    const notesData = getMapOpData(notes, JSONFromSlideNotes)
    if (notesData) {
      const ops = [Op.Insert(OpData.Map(notesData))]
      return getDeltaString(editorKit.ioAdapter, ops)
    }
  }
}

function serializeTheme(editorKit: IEditorKit, themes: Nullable<Theme>[]) {
  if (themes?.length) {
    const themeData = getMapOpData(themes, JSONFromTheme)

    if (themeData) {
      const ops = [Op.Insert(OpData.Map(themeData))]
      return getDeltaString(editorKit.ioAdapter, ops)
    }
  }
}

function serializeDataInsideHtml(internalData: string, sourceFileId?: string) {
  const data: DataInsideHtmlType = {
    ['version']: ClipboardSettings.copyPasteVersion,
    ['internalData']: internalData,
  }
  if (sourceFileId != null) {
    data['sourceFileId'] = sourceFileId
  }
  return encodeURIComponent(JSON.stringify(data))
}

function getDeltaString(ioAdapter: IOAdapter<any>, ops: Operation[]): string {
  const delta = ioAdapter.fromOperations(ops)
  return ioAdapter.stringify(delta)
}
