import { Nullable } from '../../liber/pervasive'
import { convertUnitValStrToMMWithUnit } from '../common/utils'
import {
  convertRGBToHexString,
  convertHexStringToRGB,
  convertRGBStringToRGB,
} from '../common/utils/color'

import { CHexColor } from '../core/color/CHexColor'

import { Table } from '../core/Table/Table'

import { Theme } from '../core/SlideElement/attrs/theme'
import { TableCellPr } from '../core/Table/attrs/TableCellPr'

import { FillEffects } from '../core/SlideElement/attrs/fill/fill'
import { ClrMap } from '../core/color/clrMap'

import { TableCellBorder } from '../core/Table/attrs/CellBorder'
import { CShd } from '../core/Table/attrs/CShd'

import {
  CComplexColor,
  complexColorToCHexColor,
  defaultRGBAColor,
  updateRGBA,
} from '../core/color/complexColor'
import { InstanceType, isInstanceTypeOf } from '../core/instanceTypes'
import { getDefaultFontFamilyName } from '../core/common/font'
import { BodyPr } from '../core/SlideElement/attrs/bodyPr'
import { Shape } from '../core/SlideElement/Shape'
import { TextBody } from '../core/SlideElement/TextBody'
import { createTextDocumentFromString } from '../core/utilities/tableOrTextDocContent/creators'
import { ColorRGBA } from '../core/color/type'
import { SlideElement } from '../core/SlideElement/type'
import { DrawingExtra } from './type'
import { findParentSlide } from '../core/utilities/finders'

export function escapeString(str) {
  return str
    .replace(/&/g, '&amp;')
    .replace(/</g, '&lt;')
    .replace(/>/g, '&gt;')
    .replace(/'/g, '&apos;')
    .replace(/"/g, '&quot;')
}

function correctTableCellBorderFillEffects(
  tcBorder: TableCellBorder | CShd | undefined,
  theme: Theme,
  colorMap: ClrMap
) {
  if (tcBorder && tcBorder.fillEffects) {
    tcBorder.fillEffects.check(theme, colorMap)
    const rgba = tcBorder.fillEffects.getRGBAColor()
    tcBorder.color = new CHexColor(rgba.r, rgba.g, rgba.b, false)
  }
}

export function convertToComputeStylesTable(
  table: Table,
  theme: Nullable<Theme>,
  clrMap: Nullable<ClrMap>
) {
  for (let i = 0, l = table.children.length; i < l; i++) {
    const row = table.children.at(i)!
    for (let j = 0, lj = row.children.length; j < lj; j++) {
      const cell = row.children.at(j)!
      const computePr = cell.getCalcedPr()
      cell.pr = computePr
      const shd = computePr.shd!
      const color: ColorRGBA = defaultRGBAColor()
      updateRGBA(color, shd.getColorByTheme(theme, clrMap))
      cell.pr.shd!.fillEffects = FillEffects.SolidRGBA(
        color.r,
        color.g,
        color.b
      )

      if (computePr.tableCellBorders.bottom) {
        computePr.tableCellBorders.bottom.getColorByTheme(theme, clrMap, color)
        cell.pr.tableCellBorders.bottom!.fillEffects = FillEffects.SolidRGBA(
          color.r,
          color.g,
          color.b
        )
      }

      if (computePr.tableCellBorders.top) {
        computePr.tableCellBorders.top.getColorByTheme(theme, clrMap, color)
        cell.pr.tableCellBorders.top!.fillEffects = FillEffects.SolidRGBA(
          color.r,
          color.g,
          color.b
        )
      }

      if (computePr.tableCellBorders.left) {
        computePr.tableCellBorders.left.getColorByTheme(theme, clrMap, color)
        cell.pr.tableCellBorders.left!.fillEffects = FillEffects.SolidRGBA(
          color.r,
          color.g,
          color.b
        )
      }

      if (computePr.tableCellBorders.right) {
        computePr.tableCellBorders.right.getColorByTheme(theme, clrMap, color)
        cell.pr.tableCellBorders.right!.fillEffects = FillEffects.SolidRGBA(
          color.r,
          color.g,
          color.b
        )
      }
    }
  }
}

export function correctTableCellPr(
  cellPr: TableCellPr,
  theme: Theme,
  clrMap: ClrMap
) {
  cellPr.refreshTheme(theme)
  correctTableCellBorderFillEffects(cellPr.shd, theme, clrMap)
  if (cellPr.tableCellBorders) {
    const tcBorders = cellPr.tableCellBorders
    correctTableCellBorderFillEffects(tcBorders.left, theme, clrMap)
    correctTableCellBorderFillEffects(tcBorders.top, theme, clrMap)
    correctTableCellBorderFillEffects(tcBorders.right, theme, clrMap)
    correctTableCellBorderFillEffects(tcBorders.bottom, theme, clrMap)
  }
  return cellPr
}

export function correctHtmlData(data: string) {
  const valueOfIndex = data.indexOf('</html>')
  return -1 !== valueOfIndex
    ? data.substring(0, valueOfIndex + '</html>'.length)
    : data
}

export function getDefaultFontFamily(val: Nullable<string>) {
  return 'smDefaultFont' === val ? getDefaultFontFamilyName() : val
}

export function getDefaultFontSize(val: Nullable<string>) {
  return '0px' === val ? '11pt' : val
}

export function stringValToMM(value: Nullable<string>) {
  if (value == null) return null
  const obj = convertUnitValStrToMMWithUnit(value)
  if (obj && '%' !== obj.type && 'none' !== obj.type) return obj.val
  return null
}

export function parseHexColorFromString(color: Nullable<string>) {
  if (!color || color.length === 0) {
    return null
  }
  switch (color) {
    case 'transparent':
      return null
    case 'aqua':
      return new CHexColor(0, 255, 255)
    case 'black':
      return new CHexColor(0, 0, 0)
    case 'blue':
      return new CHexColor(0, 0, 255)
    case 'fuchsia':
      return new CHexColor(255, 0, 255)
    case 'gray':
      return new CHexColor(128, 128, 128)
    case 'green':
      return new CHexColor(0, 128, 0)
    case 'lime':
      return new CHexColor(0, 255, 0)
    case 'maroon':
      return new CHexColor(128, 0, 0)
    case 'navy':
      return new CHexColor(0, 0, 128)
    case 'olive':
      return new CHexColor(128, 128, 0)
    case 'purple':
      return new CHexColor(128, 0, 128)
    case 'red':
      return new CHexColor(255, 0, 0)
    case 'silver':
      return new CHexColor(192, 192, 192)
    case 'teal':
      return new CHexColor(0, 128, 128)
    case 'white':
      return new CHexColor(255, 255, 255)
    case 'yellow':
      return new CHexColor(255, 255, 0)
    default:
      if (color.startsWith('#')) {
        const rgb = convertHexStringToRGB(color)
        if (rgb) {
          return new CHexColor(rgb.r, rgb.g, rgb.b)
        }
      }
      if (color.startsWith('rgb')) {
        const rgb = convertRGBStringToRGB(color)
        if (rgb) {
          return new CHexColor(rgb.r, rgb.g, rgb.b)
        }
      }
  }
  return null
}

export function getHexColorFromRGB(
  theme: Theme,
  clrMap: ClrMap,
  color: Nullable<CHexColor | CComplexColor>,
  fillEffects: Nullable<FillEffects>
) {
  let rgb: CHexColor
  if (null == color && null != fillEffects) {
    fillEffects.check(theme, clrMap)
    const rgba = fillEffects.getRGBAColor()
    rgb = new CHexColor(rgba.r, rgba.g, rgba.b)
  } else {
    if (isInstanceTypeOf(color, InstanceType.ComplexColor)) {
      rgb = complexColorToCHexColor(color as CComplexColor, theme)
    } else {
      rgb = color as CHexColor
    }
  }

  return `#${convertRGBToHexString(rgb.r, rgb.g, rgb.b)}`
}

export function createTextBodyWithText(
  text: string,
  parent: Shape,
  bodyPr?: BodyPr
) {
  const txtbody = new TextBody(parent)
  txtbody.setBodyPr(bodyPr ?? new BodyPr())

  txtbody.setContent(createTextDocumentFromString(text, txtbody))

  return txtbody
}

export function getDrawingExtra(sp: SlideElement): DrawingExtra | undefined {
  const slide = findParentSlide(sp)
  if (slide) {
    const animation = slide.animation
    const extra = animation.getAnimNodesBySp(sp)
    return extra != null
      ? {
          ['animation']: extra,
        }
      : undefined
  }
  return undefined
}
