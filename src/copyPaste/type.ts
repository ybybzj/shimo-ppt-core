import { Dict } from '../../liber/pervasive'
import { AudioNode, TimeNode } from '../core/Slide/Animation'

export type DrawingExtra = {
  animation: { timeNodes: TimeNode[]; audioNodes: AudioNode[] }
}
export type CopyResult = {
  width?: number
  height?: number
  docContent?: string
  drawings?: string
  drawingsExtra?: Dict<DrawingExtra>
  slides?: string
  slidesRefId?: string[]
  layouts?: string
  layoutsRefId?: string[]
  masters?: string
  mastersRefId?: string[]
  notes?: string
  notesRefId?: string[]
  noteMasters?: string
  notesMastersRefId?: string[]
  notesThemes?: string
  themes?: string
  themesRefId?: string[]
  themeName?: string
  layoutsIndexes?: number[]
  mastersIndexes?: number[]
  notesMastersIndexes?: number[]
  themesIndexes?: number[]
}

export type DataInsideHtmlType = {
  version: number
  internalData: string
  sourceFileId?: string
}
