import { PointerEventInfo } from '../../common/dom/events'
import { appendArrays, isFiniteNumber, isNumber } from '../../common/utils'
import { Factor_mm_to_pix } from '../../core/common/const/unit'
import { TableSelectionType, VMergeType } from '../../core/common/const/table'
import {
  turnOffRecordChanges,
  restoreRecordChangesState,
  checkAlmostEqual,
} from '../../core/common/utils'
import { Paragraph } from '../../core/Paragraph/Paragraph'
import { ParagraphItem } from '../../core/Paragraph/type'
import {
  updateClosestPosForParagraph,
  getParaClosestPos,
} from '../../core/Paragraph/utils/position'
import { ParaCollection } from '../../core/Paragraph/ParaCollection'
import { createNotes } from '../../core/Slide/Notes'
import { Presentation } from '../../core/Slide/Presentation'
import { Slide } from '../../core/Slide/Slide'
import { BodyPr } from '../../core/SlideElement/attrs/bodyPr'
import { GraphicFrame } from '../../core/SlideElement/GraphicFrame'
import { Table } from '../../core/Table/Table'
import { CellPos } from '../../core/Table/type'
import {
  addColumnToTable,
  addRowToTable,
  mergeCellsForTable,
} from '../../core/Table/utils/operations'
import { TextListStyle } from '../../core/textAttributes/TextListStyle'
import { TextPr } from '../../core/textAttributes/TextPr'
import { TextDocument } from '../../core/TextDocument/TextDocument'
import { getTextStyleByPhType } from '../../core/utilities/master'
import { moveCursorForward } from '../../core/utilities/presentation/hookInvoker'
import {
  isPlaceholderWithEmptyContent,
  isPlaceholder,
} from '../../core/utilities/shape/asserts'
import { ensureSpPrXfrm } from '../../core/utilities/shape/attrs'
import { checkExtentsByTextContent } from '../../core/utilities/shape/extents'
import {
  getPlaceholderType,
  getIndexOfPlaceholder,
  getMatchedSlideElementForPh,
  getNvProps,
  getParentsOfElement,
  getHierarchySps,
  getTextDocumentOfSp,
} from '../../core/utilities/shape/getters'
import {
  setDeletedForSp,
  setParentForSlideElement,
  addToParentSpTree,
} from '../../core/utilities/shape/updates'
import { EditorKit } from '../../editor/global'
import { EditorUtil } from '../../globals/editor'
import { InstanceType } from '../../core/instanceTypes'
import {
  getDefaultScaleUpdateInfo,
  ShapeSizeUpdateInfo,
} from '../../re/export/Slide'
import { AddGeometryControl } from '../../ui/features/addShape/addGeometryControl'
import {
  PptSelectedContentsForCopyIndex,
  PptSelectedContentsForCopyIndexValues,
} from '../copy/getContentForCopyPaste'
import {
  ContentTypeForCopyPaste,
  PresentationContentForCopyPaste,
} from '../copyPasteContent'
import { SlideLayout } from '../../core/Slide/SlideLayout'
import { SlideMaster } from '../../core/Slide/SlideMaster'
import { hookManager, Hooks } from '../../core/hooks'
import { getMaxTextContentWidthByLimit } from '../../core/TextDocument/utils/getter'
import { ParaHyperlink } from '../../core/Paragraph/ParaContent/ParaHyperlink'
import { ManagedSliceUtil, RecyclableArray } from '../../common/managedArray'
import { ParaRun } from '../../core/Paragraph/ParaContent/ParaRun'
import { Nullable } from '../../../liber/pervasive'
import { TableOrTextDocument } from '../../core/utilities/type'
import { SlideElement } from '../../core/SlideElement/type'
import { DrawingExtra } from '../type'
import {
  AudioNode,
  TimeNode,
  parseAudioNode,
  parseTimeNode,
} from '../../core/Slide/Animation'
import { startEditAction } from '../../core/calculation/informChanges/startAction'
import { EditActionFlag } from '../../core/EditActionFlag'
import { EditorSettings, Errors } from '../../exports/type'
import {
  applyCxnSpToSpMapState,
  clearCxnState,
  collectCxnStateForSp,
} from '../../core/utilities/shape/manageCxnSpState'

export function insertCopyPasteContent(
  presentation: Presentation,
  contentForCopyPaste: PresentationContentForCopyPaste,
  imageSrcs: string[] = [],
  isEndFormatting = false,
  calcedTextPr?: TextPr,
  indexOfContentForCopyPaste?: PptSelectedContentsForCopyIndexValues
) {
  EditorUtil.entityRegistry.ignoreAddCheck = true
  let ret = false

  const curSlide = presentation.slides[presentation.currentSlideIndex]
  const hasCurrentSlide = !!curSlide
  const contentType = contentForCopyPaste.getCopyPasteContentType()
  switch (contentType) {
    case ContentTypeForCopyPaste.Slides: {
      _insertSlide(presentation, contentForCopyPaste.slides)
      ret = true
      break
    }
    case ContentTypeForCopyPaste.SlideElements: {
      if (hasCurrentSlide) {
        const sp = contentForCopyPaste.slideElements[0].slideElement
        const isCopyOnlyTable =
          contentForCopyPaste.slideElements.length === 1 &&
          sp.instanceType === InstanceType.GraphicFrame &&
          sp.graphicObject
        if (presentation.isNotesFocused && isCopyOnlyTable) {
          ret = _insertTableToNotes(presentation, sp.graphicObject!)
        } else if (
          presentation.selectionState.selectedTextSp?.instanceType ===
            InstanceType.GraphicFrame &&
          presentation.selectionState.selectedTextSp?.graphicObject &&
          isCopyOnlyTable
        ) {
          ret = _insertTableToTable(presentation, contentForCopyPaste)
        } else {
          let hasExtra = false
          const timeNodes: TimeNode[] = []
          const audioNodes: AudioNode[] = []
          ret = insertSlideElements(
            presentation,
            contentForCopyPaste,
            indexOfContentForCopyPaste,
            (sp, extra) => {
              const newTimeNodes = extra['animation']['timeNodes']
              if (newTimeNodes?.length > 0) {
                hasExtra = true
                newTimeNodes.forEach((node) => {
                  node['spRefId'] = sp.getRefId()
                })
                appendArrays(timeNodes, newTimeNodes.map(parseTimeNode))
              }

              const newAudioNodes = extra['animation']['audioNodes']
              if (newAudioNodes?.length > 0) {
                hasExtra = true
                newAudioNodes.forEach((node) => {
                  node['spRefId'] = sp.getRefId()
                })
                appendArrays(audioNodes, newAudioNodes.map(parseAudioNode))
              }
            }
          )

          if (hasExtra) {
            startEditAction(EditActionFlag.action_Presentation_ApplyAnimation)
            curSlide.animation.insertNodes(timeNodes, audioNodes)
          }
        }
        hookManager.invoke(Hooks.Attrs.GroupCollectedResources, {
          type: 'slides',
          refId: curSlide.getRefId(),
          isApply: true,
        })
      }
      break
    }
    case ContentTypeForCopyPaste.TextContent: {
      if (hasCurrentSlide) {
        ret = insertTextContent(
          presentation,
          contentForCopyPaste,
          isEndFormatting,
          calcedTextPr
        )

        hookManager.invoke(Hooks.Attrs.GroupCollectedResources, {
          type: 'slides',
          refId: curSlide.getRefId(),
          isApply: true,
        })
      }
      break
    }
  }
  // 上传并插入图片
  if (
    hasCurrentSlide &&
    (contentType === ContentTypeForCopyPaste.SlideElements ||
      contentType === ContentTypeForCopyPaste.Unknown) &&
    imageSrcs?.length > 0
  ) {
    EditorKit.uploadImagesAndInsert(imageSrcs)
  }

  EditorUtil.entityRegistry.ignoreAddCheck = false
  return ret
}

function _insertSlide(presentation: Presentation, slides: Slide[]) {
  const selectedSlideIndices = presentation.getSelectedSlideIndices()
  const lastSlideIndex =
    selectedSlideIndices.length > 0
      ? selectedSlideIndices[selectedSlideIndices.length - 1]
      : -1

  const selectSlides: number[] = []
  slides.forEach((slide, i) => {
    const index = lastSlideIndex + i + 1
    selectSlides.push(index)
    presentation.insertSlide(index, slide)
    hookManager.invoke(Hooks.Attrs.GroupCollectedResources, {
      type: 'slides',
      refId: slide.getRefId(),
      isApply: true,
    })
  })
  presentation.setSelectedSlideIndexes(selectSlides)
  presentation.currentSlideIndex = lastSlideIndex + 1
  presentation.isGoToSlide = true
  presentation.needUpdatePreview = true
  presentation.isNotesFocused = false
  presentation.calcForEmptyNotes()
}

function _insertTableToNotes(presentation: Presentation, table: Table) {
  const s1 = turnOffRecordChanges()
  const paragraphs: Paragraph[] = []
  for (let i = 0, l = table.children.length; i < l; ++i) {
    const row = table.children.at(i)!
    for (let j = 0, lj = row.children.length; j < lj; ++j) {
      const textDocInCell = row.children.at(j)!.textDoc
      for (let k = 0, lk = textDocInCell.children.length; k < lk; ++k) {
        paragraphs.push(textDocInCell.children.at(k)!.clone(undefined))
      }
    }
  }

  restoreRecordChangesState(s1)

  if (paragraphs.length <= 0) {
    return false
  }

  const paraCollection = new ParaCollection()

  for (const para of paragraphs) {
    paraCollection.add(para, true)
  }

  const contentForCopyPaste = new PresentationContentForCopyPaste()
  contentForCopyPaste.paraCollection = paraCollection
  insertCopyPasteContent(presentation, contentForCopyPaste)
  moveCursorForward()

  return true
}

function _insertTableToTable(
  presentation: Presentation,
  contentForCopyPaste: PresentationContentForCopyPaste
) {
  // paste from table to table
  const targetTable = (
    presentation.selectionState.selectedTextSp as GraphicFrame
  ).graphicObject!
  const startCellPos = targetTable.getSelectedCellPositions()[0]
  const startCell =
    startCellPos &&
    targetTable.getRow(startCellPos.row)?.getCellByIndex(startCellPos.cell)

  if (startCell) {
    const startRowIndex = startCell.parent.index
    const startCellIndex = startCell.index
    const startRow = startCell.parent
    const startCellInfo = startRow.getCellInfo(startCellIndex)

    let currentRowIndex = startRowIndex

    const gf = contentForCopyPaste.slideElements[0].slideElement as GraphicFrame
    const selectTable = gf.graphicObject as Table

    ManagedSliceUtil.forEach(selectTable.children, (row, rowIndex) => {
      let curStartGridCol = startCellInfo.startGridCol
      ManagedSliceUtil.forEach(row.children, (cell, cellIndex) => {
        const gridSpan = cell.getGridSpan()
        const vMerge = cell.getVMerge()

        if (vMerge !== VMergeType.Continue) {
          const verticalMergeCount = selectTable.getVMergeNum(
            cellIndex,
            rowIndex
          )

          let ret = false
          while (ret !== true) {
            ret = _adjustTableByCellPos(targetTable, {
              startGridCol: curStartGridCol,
              gridSpan: gridSpan,
              startRow: currentRowIndex,
              verticalMergeCount,
            })
          }
        }

        curStartGridCol += gridSpan
      })

      currentRowIndex++
    })

    // apply content
    currentRowIndex = startRowIndex
    ManagedSliceUtil.forEach(selectTable.children, (row) => {
      const targetRow = targetTable.getRow(currentRowIndex)
      let curStartGridCol = startCellInfo.startGridCol

      ManagedSliceUtil.forEach(row.children, (cell) => {
        const targetCell =
          targetRow?.getCellByStartGridColIndex(curStartGridCol)
        const vMerge = cell.getVMerge()
        if (targetCell && vMerge !== VMergeType.Continue) {
          targetCell.textDoc.clearContent()
          targetCell.addTextDoc(cell.textDoc)
        }
        curStartGridCol += cell.getGridSpan()
      })
      currentRowIndex++
    })
    // clear selection
    targetTable.selection.isUse = false
    return true
  }
  return false
}

function _adjustTableByCellPos(
  table: Table,
  adjustInfo: { startGridCol; gridSpan; startRow; verticalMergeCount }
): boolean {
  const { startGridCol, gridSpan, startRow, verticalMergeCount } = adjustInfo
  const endRow = startRow + verticalMergeCount - 1
  const lastGridCol = startGridCol + gridSpan - 1
  const needMergeCells = [] as CellPos[]
  for (
    let rowIndex = 0, rowCount = table.children.length;
    rowIndex < rowCount;
    rowIndex++
  ) {
    const row = table.children.at(rowIndex)!
    const cellsCount = row.cellsCount
    for (let cellIndex = 0; cellIndex < cellsCount; cellIndex++) {
      const curCell = row.getCellByIndex(cellIndex)!
      const curCellInfo = row.getCellInfo(cellIndex)
      const vMerge = curCell.getVMerge()
      if (vMerge === VMergeType.Continue) {
        continue
      }
      const curGridSpan = curCell!.getGridSpan()
      const curVMergeCount = table.getVMergeNum(cellIndex, rowIndex)
      const addColumnCount =
        startGridCol + gridSpan - (curCellInfo.startGridCol + curGridSpan)
      if (cellIndex === cellsCount - 1 && addColumnCount > 0) {
        addColumnToTable(table, false, {
          row: rowIndex,
          cell: cellIndex,
          count: addColumnCount,
        })
        return false
      }
      const isLastRowCell = rowIndex + curVMergeCount === rowCount
      const addRowCount =
        startRow + verticalMergeCount - (rowIndex + curVMergeCount)
      if (isLastRowCell && addRowCount > 0) {
        addRowToTable(table, false, {
          row: rowIndex,
          cell: cellIndex,
          count: addRowCount,
        })
        return false
      }
      if (
        startRow === rowIndex &&
        startGridCol === curCellInfo.startGridCol &&
        verticalMergeCount === curVMergeCount &&
        gridSpan === curGridSpan
      ) {
        return true
      }
      const curEndRow = rowIndex + curVMergeCount - 1
      const curEndGridCol = curCellInfo.startGridCol + curGridSpan - 1
      const isColOverlap =
        (startGridCol >= curCellInfo.startGridCol &&
          startGridCol <= curEndGridCol) ||
        (lastGridCol >= curCellInfo.startGridCol &&
          lastGridCol <= curEndGridCol)
      if (isColOverlap) {
        let splitRow
        if (startRow > rowIndex && startRow <= curEndRow) {
          splitRow = startRow
        } else if (endRow >= rowIndex && endRow < curEndRow) {
          splitRow = endRow + 1
        }
        if (isFiniteNumber(splitRow)) {
          const temRow = table.getRow(splitRow)
          if (temRow) {
            for (
              let temCellIndex = 0;
              temCellIndex < temRow.cellsCount;
              temCellIndex++
            ) {
              const tempCellInfo = temRow.getCellInfo(temCellIndex)
              if (curCellInfo.startGridCol === tempCellInfo.startGridCol) {
                const temCell = temRow.getCellByIndex(temCellIndex)
                temCell?.setVMerge(VMergeType.Restart)
              }
            }
          }
          return false
        }
      }
      const isRowOverlap =
        (startRow >= rowIndex && startRow <= curEndRow) ||
        (endRow >= rowIndex && endRow <= curEndRow)
      if (isRowOverlap) {
        let splitGridCol
        if (
          startGridCol > curCellInfo.startGridCol &&
          startGridCol <= curEndGridCol
        ) {
          splitGridCol = startGridCol
        } else if (
          lastGridCol >= curCellInfo.startGridCol &&
          lastGridCol < curEndGridCol
        ) {
          splitGridCol = lastGridCol + 1
        }
        if (isFiniteNumber(splitGridCol)) {
          const gridColOffset = splitGridCol - curCellInfo.startGridCol
          for (
            let temRowIndex = rowIndex;
            temRowIndex < rowIndex + curVMergeCount;
            temRowIndex++
          ) {
            const temRow = table.getRow(temRowIndex)
            curCell.setGridSpan(gridColOffset)
            if (temRow) {
              const cellInsertCount = curGridSpan - gridColOffset
              for (let i = 1; i <= cellInsertCount; i++) {
                const inserIndex = curCell.index + i
                const newCell = temRow.addCell(inserIndex, null, false)
                newCell.applyPr(curCell.pr)
                newCell.setGridSpan(1)
              }
            }
          }
          return false
        }
      }
      if (
        rowIndex >= startRow &&
        rowIndex + curVMergeCount <= startRow + verticalMergeCount &&
        curCellInfo.startGridCol >= startGridCol &&
        curCellInfo.startGridCol + curGridSpan <= startGridCol + gridSpan
      ) {
        needMergeCells.push({ row: rowIndex, cell: cellIndex })
      }
    }
  }

  if (needMergeCells.length > 1) {
    table.selection.isUse = true
    table.selection.type = TableSelectionType.CELL
    table.selection.cellPositions = needMergeCells
    mergeCellsForTable(table, false)
  }

  return false
}

function insertSlideElements(
  presentation: Presentation,
  contentForCopyPaste: PresentationContentForCopyPaste,
  indexOfContentForCopyPaste: PptSelectedContentsForCopyIndexValues | undefined,
  onExtra?: (sp: SlideElement, extra: DrawingExtra) => void
) {
  let ret = false
  presentation.isNotesFocused = false
  presentation.selectionState.reset()
  const curSlide = presentation.slides[presentation.currentSlideIndex]!

  clearCxnState()
  for (let i = 0; i < contentForCopyPaste.slideElements.length; ++i) {
    let isInsertShape = true
    const copyElm = contentForCopyPaste.slideElements[i]
    const sp = copyElm.slideElement

    if (sp.instanceType === InstanceType.ImageShape) {
      const xfrm = sp.spPr?.xfrm
      const imageUrl = sp.blipFill?.imageSrcId
      // url 是base64的 或者是粘贴成图的image 均上传插入处理
      if (
        imageUrl &&
        (imageUrl.startsWith('data:') ||
          indexOfContentForCopyPaste === PptSelectedContentsForCopyIndex.image)
      ) {
        const options = xfrm
          ? {
              size: {
                width: xfrm.extX * Factor_mm_to_pix,
                height: xfrm.extY * Factor_mm_to_pix,
              },
              offset: {
                x: xfrm.offX * Factor_mm_to_pix,
                y: xfrm.offY * Factor_mm_to_pix,
              },
            }
          : undefined
        EditorKit.uploadImagesAndInsert([imageUrl], options)
        continue
      }
    }

    if (sp.instanceType === InstanceType.GraphicFrame && sp.graphicObject) {
      _fillTableCellByGridCount(sp.graphicObject)
    }

    let slidePh
    let layoutPlaceholder
    let placeholderType
    if (isPlaceholderWithEmptyContent(sp)) {
      const info: {
        isBadMatch?: boolean
      } = {}
      placeholderType = getPlaceholderType(sp)
      const idxOfPh = getIndexOfPlaceholder(sp)
      const layout = curSlide.layout
      layoutPlaceholder =
        layout &&
        getMatchedSlideElementForPh(
          layout,
          placeholderType,
          idxOfPh,
          false,
          info
        )
      if (!layoutPlaceholder || info.isBadMatch) {
        isInsertShape = false
      } else {
        slidePh = getMatchedSlideElementForPh(
          curSlide,
          placeholderType,
          idxOfPh,
          false,
          info
        )
        if (slidePh) {
          isInsertShape = false
        }
      }
    }

    if (isInsertShape) {
      if (sp.isDeleted) {
        setDeletedForSp(sp, false)
      }
      setParentForSlideElement(sp, curSlide)

      if (!sp.spTreeParent) {
        sp.spTreeParent = curSlide.cSld
      }

      if (sp.instanceType === InstanceType.GraphicFrame) {
        presentation.adjustGraphicFrameRowHeight(sp)
      }

      collectCxnStateForSp(sp)

      addToParentSpTree(sp)
      checkExtentsByTextContent(sp)

      if (isPlaceholder(sp) && sp.instanceType === InstanceType.Shape) {
        if (slidePh || !layoutPlaceholder) {
          const nvProps = getNvProps(sp)
          if (nvProps && nvProps.ph) {
            if (sp.txBody) {
              const textListStyle = new TextListStyle()
              const parents = getParentsOfElement(sp)
              if (parents?.master?.txStyles) {
                const textListStyleByPh = getTextStyleByPhType(
                  parents.master,
                  placeholderType
                )

                if (textListStyleByPh) {
                  textListStyle.merge(textListStyleByPh)
                }
              }

              const hsps = getHierarchySps(sp)
              const bodyPr = new BodyPr()
              hsps.reverse().forEach((h) => {
                const txBody =
                  h?.instanceType === InstanceType.Shape ? h.txBody : null
                if (txBody) {
                  textListStyle.merge(txBody.lstStyle)
                  bodyPr.merge(txBody.bodyPr)
                }
              })
              textListStyle.merge(sp.txBody.lstStyle)
              bodyPr.merge(sp.txBody.bodyPr)
              sp.txBody.setTextListStyle(textListStyle)
              sp.txBody.setBodyPr(bodyPr)
            }
            nvProps.setPh(null)
          }
        }
      }
      presentation.selectionState.select(sp)
      if (onExtra && copyElm.extra != null) {
        onExtra(sp, copyElm.extra)
      }
      ret = true
    }
  }

  applyCxnSpToSpMapState()
  if (
    contentForCopyPaste.paraCollection &&
    contentForCopyPaste.paraCollection.elements.length > 0
  ) {
    const shape = _addShapeFromTextContent(
      presentation,
      contentForCopyPaste.paraCollection
    )
    presentation.selectionState.select(shape)
    ret = true
  }

  return ret
}

function _fillTableCellByGridCount(table: Table) {
  const tableGridLength = table.tableGrid.length
  ManagedSliceUtil.forEach(table.children, (row) => {
    let rowGridCount = 0
    ManagedSliceUtil.forEach(row.children, (cell) => {
      rowGridCount += cell.getGridSpan()
    })

    if (rowGridCount < tableGridLength) {
      const newCell = row.addCell(row.cellsCount, null, false)
      newCell.setGridSpan(tableGridLength - rowGridCount)
    }
  })
}

function _ensureSpsXfrmBeforePaste(
  contentForCopyPaste: PresentationContentForCopyPaste,
  sourceContentForCopyPaste: PresentationContentForCopyPaste,
  parent: Slide
) {
  let i, j
  for (i = 0; i < contentForCopyPaste.slideElements.length; ++i) {
    const shape = contentForCopyPaste.slideElements[i].slideElement
    if (
      isPlaceholder(shape) &&
      (!shape.spPr || !shape.spPr.xfrm || !shape.spPr.xfrm.isValid())
    ) {
      const _parent = shape.parent
      shape.parent = parent
      const hsps = getHierarchySps(shape)
      for (j = 0; j < hsps.length; ++j) {
        const sp = hsps[j]
        if (sp?.spPr?.xfrm?.isValid()) {
          break
        }
      }
      if (j === hsps.length) {
        if (
          sourceContentForCopyPaste.slideElements[i] &&
          sourceContentForCopyPaste.slideElements[i].slideElement
        ) {
          const sourceShape =
            sourceContentForCopyPaste.slideElements[i].slideElement
          if (
            sourceShape &&
            sourceShape.spPr &&
            sourceShape.spPr.xfrm &&
            sourceShape.spPr.xfrm.isValid()
          ) {
            shape.x = sourceShape.spPr.xfrm.offX
            shape.y = sourceShape.spPr.xfrm.offY
            shape.extX = sourceShape.spPr.xfrm.extX
            shape.extY = sourceShape.spPr.xfrm.extY
            ensureSpPrXfrm(shape)
          }
        }
      }
      shape.parent = _parent
    }
  }
}

function insertTextContent(
  presentation: Presentation,
  contentForCopyPaste: PresentationContentForCopyPaste,
  isEndFormatting = false,
  calcedTextPr?: TextPr
) {
  let ret = false
  const paraCollection = contentForCopyPaste.paraCollection!
  paraCollection.endCollect()

  let textTarget: Nullable<TableOrTextDocument> =
    presentation.getCurrentTextTarget(true)
  if (!textTarget) {
    const selectedElements = presentation.selectionState.selectedSlideElements
    if (selectedElements.length === 1) {
      const sp = selectedElements[0]
      let textDoc = getTextDocumentOfSp(sp)
      if (!textDoc && sp.instanceType === InstanceType.Shape) {
        sp.createTxBody()
        textDoc = getTextDocumentOfSp(sp)!
      } else if (
        sp.instanceType === InstanceType.GraphicFrame &&
        !presentation.selectionState.selectedTextSp
      ) {
        textDoc = null
      }
      textDoc?.clearContent()
      textTarget = textDoc
    }
  }

  if (textTarget) {
    let textDocArr: TextDocument[]
    if (textTarget.instanceType === InstanceType.Table) {
      const table = textTarget
      const selectionCells = table.getSelectedCellPositions()
      textDocArr = selectionCells.reduce((textDocs, pos) => {
        const { row, cell: ci } = pos
        const cell = table.children.at(row)?.children.at(ci)
        if (cell) {
          textDocs.push(cell.textDoc)
        }
        return textDocs
      }, [] as TextDocument[])
      if (selectionCells.length > 1) {
        presentation.onRemove(1)
      }
    } else {
      textDocArr = [textTarget]
    }
    const isMultiContent = textDocArr.length > 1

    textDocArr.forEach((textDoc) => {
      if (textDoc.selection.isUse) {
        presentation.onRemove(1)
      }

      const _paraCollection = isMultiContent
        ? paraCollection.clone()
        : paraCollection

      const targetParagraph = textDoc.children.at(
        textDoc.cursorPosition.childIndex
      )

      if (isEndFormatting && targetParagraph) {
        _applyPropsToContent(
          targetParagraph,
          _paraCollection,
          isMultiContent,
          calcedTextPr
        )
      }

      if (targetParagraph) {
        const adjPos = {
          paragraph: targetParagraph,
          pos: targetParagraph.currentElementPosition(false, false),
        }
        updateClosestPosForParagraph(targetParagraph, adjPos)

        const paraAdjacentPos = getParaClosestPos(adjPos.paragraph, adjPos)!
        if (null == paraAdjacentPos || paraAdjacentPos.enities.length < 2) {
          return
        }

        const lastEntity =
          paraAdjacentPos.enities[paraAdjacentPos.enities.length - 1]
        if (InstanceType.ParaRun !== lastEntity.instanceType) return

        if (null == targetParagraph.parent) {
          return
        }

        textDoc.insertSelection(_paraCollection, adjPos)
      }
      const selectTextSp = presentation.selectionState.selectedTextSp
      selectTextSp && checkExtentsByTextContent(selectTextSp)
      ret = true
    })
  } else {
    presentation.isNotesFocused = false
    const shape = _addShapeFromTextContent(presentation, paraCollection)
    presentation.selectionState.reset()
    presentation.selectionState.select(shape)

    presentation.calcForEmptyNotes()
    ret = true
  }
  return ret
}

function _applyPropsToContent(
  paragraph: Paragraph,
  paraCollection: ParaCollection,
  isMultiContent: boolean,
  calcedTextPr?: TextPr
) {
  if (paragraph) {
    const textPr =
      paragraph.paraTextPr.textPr ??
      paragraph.getTextStylePr(undefined, true).textPr

    const applyPrToContent = function (
      items: RecyclableArray<ParagraphItem> | RecyclableArray<ParaRun>,
      textPr: TextPr
    ) {
      ManagedSliceUtil.forEach(items, (item: ParaRun | ParaHyperlink) => {
        if (item.instanceType === InstanceType.ParaRun) {
          const run = item
          if (run.pr) {
            run.pr.fillEmptyProps(textPr)
          }
        } else {
          applyPrToContent(item.children, textPr)
        }
      })
    }
    if (!isMultiContent) {
      paraCollection.elements.forEach(({ item: para }) => {
        applyPrToContent(para.children, textPr)
      })
    }
  }

  if (calcedTextPr && isNumber(calcedTextPr.fontSize)) {
    const textPr = {
      fontSize: calcedTextPr.fontSize,
    }
    paraCollection.elements.forEach(({ item: para }) => {
      para.isSetToAll = true
      para.applyTextPr(textPr)
      para.isSetToAll = false
    })
  }
}

function _addShapeFromTextContent(
  presentation: Presentation,
  paraCollection: ParaCollection
) {
  const curSlide = presentation.slides[presentation.currentSlideIndex]
  const shapeEditControl = new AddGeometryControl(
    'textRect',
    0,
    0,
    curSlide?.layout?.master!.theme,
    curSlide?.layout?.master,
    curSlide?.layout,
    curSlide
  )
  shapeEditControl.track(new PointerEventInfo(), 0, 0)
  const shape = shapeEditControl.getShape(curSlide)
  shape.setParent(curSlide)
  const paragraph = shape.txBody!.textDoc!.children.at(0)
  if (paragraph) {
    const adjPos = {
      paragraph: paragraph,
      pos: paragraph.currentElementPosition(false, false),
    }
    updateClosestPosForParagraph(paragraph, adjPos)

    shape.txBody!.textDoc!.insertSelection(paraCollection, adjPos, true)
  }

  const body_pr = shape.getBodyPr()!
  const w =
    getMaxTextContentWidthByLimit(
      shape.txBody!.textDoc!,
      presentation.width / 2
    ) +
    body_pr.lIns! +
    body_pr.rIns!
  const h =
    shape.txBody!.textDoc!.getWholeHeight() + body_pr.tIns! + body_pr.bIns!
  shape.spPr!.xfrm!.setExtX(w)
  shape.spPr!.xfrm!.setExtY(h)
  shape.spPr!.xfrm!.setOffX((presentation.width - w) / 2)
  shape.spPr!.xfrm!.setOffY((presentation.height - h) / 2)
  shape.setParent(curSlide)
  addToParentSpTree(shape)
  return shape
}

// default as paste using source formatting
export function insertPPTContent(
  presentation: Presentation,
  pptContent: PresentationContentForCopyPaste
) {
  const currentMaster =
    presentation.slides[presentation.currentSlideIndex]?.layout?.master ||
    presentation.slideMasters[0]
  if (!currentMaster) {
    return false
  }

  EditorUtil.entityRegistry.ignoreAddCheck = true

  const content = pptContent.clone()

  // preparation before insert
  if (content.slides.length > 0) {
    let kw = 1.0,
      kh = 1.0
    let updateSize = false
    let sizeUpdateInfo: undefined | ShapeSizeUpdateInfo
    if (content.slideWidth !== null && content.slideHeight !== null) {
      if (
        !checkAlmostEqual(presentation.width, content.slideWidth) ||
        !checkAlmostEqual(presentation.height, content.slideHeight)
      ) {
        updateSize = true
        kw = presentation.width / content.slideWidth
        kh = presentation.height / content.slideHeight
        sizeUpdateInfo = getDefaultScaleUpdateInfo({
          from: {
            width: content.slideWidth,
            height: content.slideHeight,
          },
          to: { width: presentation.width, height: presentation.height },
        })
      }
    }
    // update layouts and masters
    for (let i = 0; i < content.layouts.length; ++i) {
      let layout = content.layouts[i]

      const valueOfMasterIndex = content.mastersIndices[i]
      let master = content.masters[valueOfMasterIndex]

      const themeIndex = content.themesIndices[valueOfMasterIndex]
      const theme = content.themes[themeIndex]
      if (!master || (!master.theme && !theme)) {
        // set default
        content.layouts[i] = currentMaster.layouts[0]
        break
      }

      if (master && !master.theme && theme) {
        master.setTheme(theme)
      }

      const matchingMaster = presentation.slideMasters.find((slideMaster) => {
        const themeName = master?.theme?.name
        return themeName && themeName === slideMaster.theme?.name
      })

      if (matchingMaster) {
        master = matchingMaster
        const matchedLayout = matchingMaster.layouts.find(
          (slideLayout) =>
            slideLayout.cSld?.name &&
            slideLayout.cSld.name === layout.cSld?.name
        )

        if (matchedLayout) {
          layout = matchedLayout
        } else {
          layout.setMaster(master)
          master.addLayout(layout)
          if (updateSize) {
            setSizeAndScale(
              layout,
              presentation.width,
              presentation.height,
              kw,
              kh
            )
          }
          presentation.updateSlideMaster(master)
        }
      } else {
        if (master) {
          master.addLayout(layout)
          layout.setMaster(master)
          if (updateSize) {
            setSizeAndScale(
              master,
              presentation.width,
              presentation.height,
              kw,
              kh
            )
            setSizeAndScale(
              layout,
              presentation.width,
              presentation.height,
              kw,
              kh
            )
          }
        }
        presentation.addSlideMaster(presentation.slideMasters.length, master)
      }

      // update layouts in paste content
      content.layouts[i] = layout
    }

    for (let i = 0; i < content.slides.length; ++i) {
      const valueOfLayoutIndex = content.layoutsIndices[i]
      const layout =
        content.layouts[valueOfLayoutIndex] ?? currentMaster.layouts[0]

      const slide = content.slides[i]
      slide.setLayout(layout)

      const notes = content.notes[i] ?? createNotes()
      slide.setNotes(notes)
      slide.notes!.setNotesMaster(presentation.notesMasters[0])
      slide.notes!.setSlide(slide)

      if (updateSize) {
        slide.updateSize(
          presentation.width,
          presentation.height,
          sizeUpdateInfo
        )
      }
    }
  }

  const ret = insertCopyPasteContent(
    presentation,
    content,
    undefined,
    false,
    undefined,
    PptSelectedContentsForCopyIndex.source
  )

  EditorUtil.entityRegistry.ignoreAddCheck = false
  return ret
}

export function pasteContent(
  presentation: Presentation,
  arrOfContents: PresentationContentForCopyPaste[],
  indexOfContentForCopyPaste: PptSelectedContentsForCopyIndexValues
) {
  if (!arrOfContents[indexOfContentForCopyPaste]) {
    return false
  }

  const currentMaster =
    presentation.slides[presentation.currentSlideIndex]?.layout?.master ||
    presentation.slideMasters[0]
  if (!currentMaster) {
    return false
  }

  EditorUtil.entityRegistry.ignoreAddCheck = true

  const isEndFormatting =
    indexOfContentForCopyPaste === PptSelectedContentsForCopyIndex.destination

  const content = arrOfContents[indexOfContentForCopyPaste].clone()

  // preparation before insert
  if (content.slides.length > 0) {
    let kw = 1.0,
      kh = 1.0
    let updateSize = false
    let sizeUpdateInfo: undefined | ShapeSizeUpdateInfo
    if (content.slideWidth !== null && content.slideHeight !== null) {
      if (
        !checkAlmostEqual(presentation.width, content.slideWidth) ||
        !checkAlmostEqual(presentation.height, content.slideHeight)
      ) {
        updateSize = true
        kw = presentation.width / content.slideWidth
        kh = presentation.height / content.slideHeight
        sizeUpdateInfo = getDefaultScaleUpdateInfo({
          from: {
            width: content.slideWidth,
            height: content.slideHeight,
          },
          to: { width: presentation.width, height: presentation.height },
        })
      }
    }

    if (isEndFormatting) {
      const sourceContentForCopyPaste =
        arrOfContents[PptSelectedContentsForCopyIndex.source]

      for (let i = 0; i < content.slides.length; ++i) {
        const slide = content.slides[i]
        const valueOfLayoutIndex = sourceContentForCopyPaste.layoutsIndices[i]
        const layoutOfSrc =
          sourceContentForCopyPaste.layouts[valueOfLayoutIndex]

        if (layoutOfSrc) {
          slide.setLayout(
            currentMaster.getMatchLayout(
              layoutOfSrc.type,
              layoutOfSrc.matchingName,
              layoutOfSrc.cSld.name,
              true
            )
          )
        } else {
          slide.setLayout(currentMaster.layouts[0])
        }

        let notes = content.notes[i]
        if (!notes) {
          notes = createNotes()
        }
        slide.setNotes(notes)
        slide.notes!.setNotesMaster(presentation.notesMasters[0])
        slide.notes!.setSlide(slide)
        if (updateSize) {
          slide.updateSize(
            presentation.width,
            presentation.height,
            sizeUpdateInfo
          )
        }
      }
    } else {
      // update layouts and masters
      for (let i = 0; i < content.layouts.length; ++i) {
        let layout = content.layouts[i]

        const valueOfMasterIndex = content.mastersIndices[i]
        let master = content.masters[valueOfMasterIndex]

        const themeIndex = content.themesIndices[valueOfMasterIndex]
        const theme = content.themes[themeIndex]
        if (!master || (!master.theme && !theme)) {
          // set default
          content.layouts[i] = currentMaster.layouts[0]
          break
        }

        if (master && !master.theme && theme) {
          master.setTheme(theme)
        }

        const matchingMaster = presentation.slideMasters.find((slideMaster) => {
          const themeName = master?.theme?.name
          return themeName && themeName === slideMaster.theme?.name
        })

        if (matchingMaster) {
          master = matchingMaster
          const matchedLayout = matchingMaster.layouts.find(
            (slideLayout) =>
              slideLayout.cSld?.name &&
              slideLayout.cSld.name === layout.cSld?.name
          )

          if (matchedLayout) {
            layout = matchedLayout
          } else {
            layout.setMaster(master)
            master.addLayout(layout)
            if (updateSize) {
              setSizeAndScale(
                layout,
                presentation.width,
                presentation.height,
                kw,
                kh
              )
            }
            presentation.updateSlideMaster(master)
          }
        } else {
          if (master) {
            master.addLayout(layout)
            layout.setMaster(master)
            if (updateSize) {
              setSizeAndScale(
                master,
                presentation.width,
                presentation.height,
                kw,
                kh
              )
              setSizeAndScale(
                layout,
                presentation.width,
                presentation.height,
                kw,
                kh
              )
            }
          }
          presentation.addSlideMaster(presentation.slideMasters.length, master)
        }

        // update layouts in paste content
        content.layouts[i] = layout
      }

      for (let i = 0; i < content.slides.length; ++i) {
        const valueOfLayoutIndex = content.layoutsIndices[i]
        const layout =
          content.layouts[valueOfLayoutIndex] ?? currentMaster.layouts[0]

        const slide = content.slides[i]
        slide.setLayout(layout)

        const notes = content.notes[i] ?? createNotes()
        slide.setNotes(notes)
        slide.notes!.setNotesMaster(presentation.notesMasters[0])
        slide.notes!.setSlide(slide)

        if (updateSize) {
          slide.updateSize(
            presentation.width,
            presentation.height,
            sizeUpdateInfo
          )
        }
      }
    }
  }

  if (content.slideElements.length > 0 && isEndFormatting) {
    const slide = presentation.slides[presentation.currentSlideIndex]
    const sourceContentForCopyPaste =
      arrOfContents[PptSelectedContentsForCopyIndex.source]
    if (slide && !presentation.isNotesFocused && sourceContentForCopyPaste) {
      _ensureSpsXfrmBeforePaste(content, sourceContentForCopyPaste, slide)
    }
  }

  let calcedTextPr: TextPr | undefined
  if (
    isEndFormatting &&
    content.paraCollection?.elements &&
    content.paraCollection?.elements.length > 0
  ) {
    calcedTextPr = presentation.getCalcedTextPr()
  }

  const ret = insertCopyPasteContent(
    presentation,
    content,
    undefined,
    isEndFormatting,
    calcedTextPr,
    indexOfContentForCopyPaste
  )

  EditorUtil.entityRegistry.ignoreAddCheck = false
  return ret
}

export function checkPasteLimit(
  _presentation: Presentation,
  arrOfContents: PresentationContentForCopyPaste[],
  indexOfContentForCopyPaste: PptSelectedContentsForCopyIndexValues
) {
  if (EditorSettings.isLimitPasting === false) return false
  const content = arrOfContents[indexOfContentForCopyPaste]
  if (!content) {
    return false
  }

  const type = content.getCopyPasteContentType()
  switch (type) {
    case ContentTypeForCopyPaste.Slides: {
      if (content.slides.length >= EditorSettings.limitCountForPasting) {
        hookManager.invoke(Hooks.Emit.EmitEvent, {
          eventType: 'Error',
          args: [Errors.PasteReachLimit, { slideCount: content.slides.length }],
        })
        return true
      } else {
        return false
      }
    }
    default: {
      return false
    }
  }
}

function setSizeAndScale(
  target: SlideLayout | SlideMaster,
  sw: number,
  sh: number,
  kw: number,
  kh: number
) {
  target.setSlideSize(sw, sh)
  target.scale(kw, kh)
}
