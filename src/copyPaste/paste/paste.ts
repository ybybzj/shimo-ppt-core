import { Dict, Nullable } from '../../../liber/pervasive'
import { IEditorKit } from '../../editor/type'
import { SpecialPasteKind, SpecialPasteKindValue } from '../const'
import { eachOfUnicodeString, parseJSON } from '../../common/utils'
import { ParaCollection } from '../../core/Paragraph/ParaCollection'
import { ParaSpace } from '../../core/Paragraph/RunElement/ParaSpace'
import { ParaTab } from '../../core/Paragraph/RunElement/ParaTab'
import { ParaText } from '../../core/Paragraph/RunElement/ParaText'

import { Presentation } from '../../core/Slide/Presentation'
import {
  TextOverflowType,
  TextAutoFitType,
  TextVerticalType,
} from '../../core/SlideElement/const'
import { createGeometryOfPrstTxWarp } from '../../core/SlideElement/geometry/creators'

import {
  checkVersionCompatible,
  ClipboardDataFormat,
  ClipboardDataFormatValue,
  ClipboardSettings,
  getIsUpdatingPasteDataFromClipboard,
  InternalDataAttributeKey,
  Rect,
} from '../common'
import { gOffsetStateForCopyShape } from '../gOffsetStateForCopyShape'
import { CopyResult, DataInsideHtmlType } from '../type'

import { convertGlobalInnerMMToPixPos } from '../../ui/editorUI/utils/utils'
import { gSpecialPasteUtil } from '../gSpecialPasteUtil'
import { ClipboardHandler } from '../gClipboard'
import { InstanceType, isInstanceTypeOf } from '../../core/instanceTypes'
import { TextFit } from '../../core/SlideElement/attrs/TextFit'
import { Shape } from '../../core/SlideElement/Shape'
import { calculateForRendering } from '../../core/calculation/calculate/calculateForRendering'
import { HtmlPaster } from './html/htmlPaster'
import { moveCursorToEndPosInTextDocument } from '../../core/utilities/cursor/moveToEdgePos'
import { Paragraph } from '../../core/Paragraph/Paragraph'
import {
  addNewParagraphInTextDocument,
  applyTextPrToParagraphInTextDocument,
} from '../../core/TextDocument/utils/operations'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../core/common/utils'
import { ensureSpPrXfrm } from '../../core/utilities/shape/attrs'
import { moveCursorForward } from '../../core/utilities/presentation/hookInvoker'
import { GraphicFrame } from '../../core/SlideElement/GraphicFrame'
import {
  ContentTypeForCopyPaste,
  CopyWrapperOfSlideElement,
  PresentationContentForCopyPaste,
} from '../copyPasteContent'
import {
  checkPasteLimit,
  insertCopyPasteContent,
  pasteContent,
} from './insertion'
import {
  getLayoutsFromDeltaString,
  getMastersFromDeltaString,
  getParaCollectionFromDeltaString,
  getSlideElementsInfoFromDeltaString,
  getSlidesInfoFromDeltaString,
  getNotesFromDeltaString,
  getThemesFromDeltaString,
} from './fromDeltaString'
import { logger } from '../../lib/debug/log'
import {
  PptSelectedContentsForCopyIndex,
  PptSelectedContentsForCopyIndexValues,
} from '../copy/getContentForCopyPaste'
import { RunContentElement } from '../../core/Paragraph/RunElement/type'
import { Dom, toElement } from '../../common/dom/vdom'
import { createTextBodyWithText } from '../utils'
import { ManagedSliceUtil, RecyclableArray } from '../../common/managedArray'

export interface HTMLPasteData {
  node: HTMLElement
  htmlText: string
}

export function executePaste(
  editorKit: IEditorKit,
  clipboard: ClipboardHandler,
  format: Nullable<ClipboardDataFormatValue>,
  data: Nullable<string | HTMLPasteData>,
  textData: Nullable<string>,
  specialPasteType?: SpecialPasteKindValue
) {
  if (specialPasteType == null) {
    gSpecialPasteUtil.hideUI()
    gSpecialPasteUtil.data.format = format

    gSpecialPasteUtil.data.data = data
    gSpecialPasteUtil.data.textData = textData
  } else {
    gSpecialPasteUtil.kind = specialPasteType

    format = gSpecialPasteUtil.data.format
    data = gSpecialPasteUtil.data.data
    textData = gSpecialPasteUtil.data.textData

    if (
      specialPasteType === SpecialPasteKind.keepTextOnly &&
      format !== ClipboardDataFormat.TEXT &&
      textData
    ) {
      format = ClipboardDataFormat.TEXT
      data = textData
    }
  }

  switch (format) {
    case ClipboardDataFormat.HTML: {
      const htmlText = data as Nullable<string>
      if (htmlText) {
        _handleHtmlPasteWithCallback(htmlText, textData, (newHtmlPasteData) => {
          executePasteHtml(editorKit, clipboard, newHtmlPasteData.node)
        })
      }

      break
    }
    case ClipboardDataFormat.INTERNAL: {
      executePasteInternalData(editorKit, clipboard, data as string)
      break
    }
    case ClipboardDataFormat.TEXT: {
      executePasteText(editorKit, clipboard, data as string)
      break
    }
  }
}

//#region paste executions
function _beforeExePaste(presentation: Presentation) {
  const isUpdatingPasteDataFromClipboard = getIsUpdatingPasteDataFromClipboard()
  const curSlideIndex = presentation.currentSlideIndex
  if (
    !isUpdatingPasteDataFromClipboard
  ) {
    const slidePasteCount =
      gOffsetStateForCopyShape.getSlidePasteCount(curSlideIndex)
    if (!gSpecialPasteUtil.isSpecialPasteRevert) {
      gOffsetStateForCopyShape.setSlidePasteCount(
        curSlideIndex,
        slidePasteCount + 1
      )
    }
  }
}
function executePasteHtml(
  editorKit: IEditorKit,
  clipboard: ClipboardHandler,
  node: Nullable<HTMLElement>
) {
  const presentation = editorKit.presentation
  _beforeExePaste(presentation)

  if (!node) {
    gSpecialPasteUtil.resetUI()
    gSpecialPasteUtil.pasteEnd()
    return
  }

  const dataInsideHtml = _getDataInsideHtml(node)
  let isPasteInternal = false
  if (dataInsideHtml) {
    const { internalData, version, sourceFileId } = dataInsideHtml
    clipboard.updateCopySourceFileId(sourceFileId)
    if (checkVersionCompatible(version)) {
      isPasteInternal = _pasteFromInternalData(editorKit, internalData)
    }
  }

  if (isPasteInternal === false) {
    _pasteFromHtml(editorKit, node)
  }
}

function executePasteInternalData(
  editorKit: IEditorKit,
  _clipboard: ClipboardHandler,
  internalData: string
) {
  const presentation = editorKit.presentation
  _beforeExePaste(presentation)

  if (!internalData) {
    return
  }

  const isPasteInternal = _pasteFromInternalData(editorKit, internalData)

  if (isPasteInternal === false) {
    gSpecialPasteUtil.resetUI()
    gSpecialPasteUtil.pasteEnd()
  }
}

function executePasteText(
  editorKit: IEditorKit,
  _clipboard: ClipboardHandler,
  text: string
) {
  const isUpdatingPasteDataFromClipboard = getIsUpdatingPasteDataFromClipboard()
  _beforeExePaste(editorKit.presentation)

  //PASTE
  if (text) {
    if (isUpdatingPasteDataFromClipboard) {
      updateSpecialPasteShowOptions(editorKit)
    } else {
      editorKit.presentation.removeTargetBeforePaste()
      _pasteFromText(editorKit, text)
    }
  }
}
//#endregion
function _readContentInfosFromStr(editorKit: IEditorKit, dataString: string) {
  const contents = parseJSON(dataString)
  const contentInfos = Array.isArray(contents)
    ? contents.map((content) =>
        _copyPasteContentInfoFromCopyResult(editorKit, content)
      )
    : []
  const { themeName: themeName } = contentInfos[0]?.content ?? {}
  return { content: contentInfos, themeName: themeName }
}

function _copyPasteContentInfoFromCopyResult(
  editorKit: IEditorKit,
  copyResult: CopyResult
) {
  const state = turnOffRecordChanges()

  const contentForCopyPaste: PresentationContentForCopyPaste =
    new PresentationContentForCopyPaste()
  const fonts: string[] = []

  let isContentEmpty = true

  for (const type in copyResult) {
    if (type !== 'docContent') {
      isContentEmpty = false
    }
    switch (type) {
      case 'width': {
        const width = copyResult['width']
        if (width != null) {
          contentForCopyPaste.slideWidth = width / 100000.0
        }
        break
      }
      case 'height': {
        const height = copyResult['height']
        if (height != null) {
          contentForCopyPaste.slideHeight = height / 100000.0
        }
        break
      }
      case 'docContent': {
        const deltaString = copyResult['docContent']
        if (deltaString != null) {
          const fontMap: Dict = {}
          const selectedContent = getParaCollectionFromDeltaString(
            deltaString,
            fontMap
          )
          if (selectedContent) {
            contentForCopyPaste.paraCollection = selectedContent
            for (const i in fontMap) {
              fonts.push(i)
            }
            isContentEmpty = false
          }
        }
        break
      }
      case 'drawings': {
        const deltaString = copyResult['drawings']
        if (deltaString != null) {
          const { shapes, fontMap } = getSlideElementsInfoFromDeltaString(
            editorKit,
            deltaString,
            copyResult['drawingsExtra']
          )
          contentForCopyPaste.slideElements = shapes
          for (const i in fontMap) {
            fonts.push(i)
          }
        }
        break
      }
      case 'slides': {
        const deltaString = copyResult['slides']
        if (deltaString) {
          const { slideCopyObjects, fontMap } = getSlidesInfoFromDeltaString(
            editorKit,
            deltaString,
            copyResult['slidesRefId']
          )
          contentForCopyPaste.slides = slideCopyObjects
          for (const i in fontMap) {
            fonts.push(i)
          }
        }
        break
      }
      case 'layouts': {
        if (copyResult['layouts']) {
          const layouts = getLayoutsFromDeltaString(
            editorKit,
            copyResult['layouts']
          )
          contentForCopyPaste.layouts = restoreOrderByRefID(
            copyResult['layoutsRefId'],
            layouts
          )
          resetRefIds(contentForCopyPaste.layouts)
        }
        break
      }
      case 'masters': {
        if (copyResult['masters']) {
          const masters = getMastersFromDeltaString(
            editorKit,
            copyResult['masters']
          )
          contentForCopyPaste.masters = restoreOrderByRefID(
            copyResult['mastersRefId'],
            masters
          )
        }
        resetRefIds(contentForCopyPaste.masters)
        break
      }
      case 'notes': {
        if (copyResult['notes']) {
          const notes = getNotesFromDeltaString(
            editorKit,
            copyResult['notes'],
            copyResult['notesRefId']!
          )
          contentForCopyPaste.notes = notes
        }
        break
      }
      case 'themes': {
        if (copyResult['themes']) {
          const themes = getThemesFromDeltaString(
            editorKit,
            copyResult['themes']
          )
          contentForCopyPaste.themes = restoreOrderByRefID(
            copyResult['themesRefId'],
            themes
          )
          resetRefIds(contentForCopyPaste.themes)
        }
        break
      }
      case 'themeName':
        contentForCopyPaste.themeName = copyResult['themeName']
        break
    }
  }
  if (isContentEmpty === false) {
    contentForCopyPaste.layoutsIndices = copyResult['layoutsIndexes'] ?? []
    if (copyResult['layoutsRefId']) {
      contentForCopyPaste.layoutsRefId = copyResult['layoutsRefId']
    }
    contentForCopyPaste.mastersIndices = copyResult['mastersIndexes'] ?? []
    if (copyResult['mastersRefId']) {
      contentForCopyPaste.mastersRefId = copyResult['mastersRefId']
    }

    contentForCopyPaste.themesIndices = copyResult['themesIndexes'] ?? []
    if (copyResult['themesRefId']) {
      contentForCopyPaste.themesRefId = copyResult['themesRefId']
    }
  }

  const ret = {
    content: contentForCopyPaste,
    fonts: fonts,
    images: [],
  }

  restoreRecordChangesState(state)
  return ret
}

function _pasteFromInternalData(editorKit: IEditorKit, contentString: string) {
  const isUpdatingPasteDataFromClipboard = getIsUpdatingPasteDataFromClipboard()
  const presentation = editorKit.presentation
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const { themeName, content: contentInfos } = _readContentInfosFromStr(
    editorKit,
    contentString
  )
  const multipleParamsCount = contentInfos ? contentInfos.length : 0
  if (multipleParamsCount > 0) {
    const copyPasteContents: PresentationContentForCopyPaste[] =
      multipleParamsCount > 0
        ? contentInfos.map((curContent) => curContent.content)
        : []

    let specialOptionsArr: SpecialPasteKindValue[] = []
    switch (multipleParamsCount) {
      case 1:
        specialOptionsArr = [SpecialPasteKind.destinationTheme]
        break
      case 2:
        specialOptionsArr = [
          SpecialPasteKind.destinationTheme,
          SpecialPasteKind.sourceFormatting,
        ]
        break
      case 3:
        specialOptionsArr = [
          SpecialPasteKind.destinationTheme,
          SpecialPasteKind.sourceFormatting,
        ]
        if (
          copyPasteContents[PptSelectedContentsForCopyIndex.image].slideElements
            .length > 0
        ) {
          specialOptionsArr.push(SpecialPasteKind.picture)
        }

        break
    }

    let defaultPasteFormatting: PptSelectedContentsForCopyIndexValues =
      PptSelectedContentsForCopyIndex.destination

    const sourceContent =
      copyPasteContents[PptSelectedContentsForCopyIndex.source]
    if (
      !gSpecialPasteUtil.isSpecialPasteStart &&
      !presentation.isNotesFocused &&
      (sourceContent?.slides?.length || sourceContent?.slideElements?.length)
    ) {
      defaultPasteFormatting = PptSelectedContentsForCopyIndex.source
      gSpecialPasteUtil.kind = SpecialPasteKind.sourceFormatting
    }

    let contentForPate = copyPasteContents[defaultPasteFormatting]
    const hasTextContent = contentForPate?.paraCollection
    if (gSpecialPasteUtil.isSpecialPasteStart) {
      const props = gSpecialPasteUtil.kind
      switch (props) {
        case SpecialPasteKind.destinationTheme: {
          break
        }
        case SpecialPasteKind.sourceFormatting: {
          if (copyPasteContents[PptSelectedContentsForCopyIndex.source]) {
            contentForPate =
              copyPasteContents[PptSelectedContentsForCopyIndex.source]
            defaultPasteFormatting = PptSelectedContentsForCopyIndex.source
          }
          break
        }
        case SpecialPasteKind.picture: {
          if (copyPasteContents[PptSelectedContentsForCopyIndex.image]) {
            contentForPate =
              copyPasteContents[PptSelectedContentsForCopyIndex.image]
            defaultPasteFormatting = PptSelectedContentsForCopyIndex.image
          }
          break
        }
        case SpecialPasteKind.keepTextOnly: {
          break
        }
      }
    }

    if (null == contentForPate) {
      return false
    }

    // 很多情况下复制直接取 recalc 后的属性因此不能只用 theme 来判断是否需要 sourceFormatting
    // if (
    //   contentForPate.slideElements &&
    //   contentForPate.slideElements.length > 0
    // ) {
    // const sourceFileId = copySourceFileId
    // const curTheme = getThemeForEditContainer(presentation)
    // if (
    //   sourceFileId === editorKit.sourceFileId &&
    //   curTheme &&
    //   curTheme.name === themeName
    // ) {
    //   specialOptionsArr.splice(1, 1)
    // }
    // }

    const pasteCallback = () => {
      let isPaste: boolean
      if (!isUpdatingPasteDataFromClipboard) {
        isPaste = pasteContent(
          presentation,
          copyPasteContents,
          defaultPasteFormatting
        )

        calculateForRendering()
        moveCursorForward()
        presentation.syncEditorUIState()
      } else {
        isPaste = true
      }

      if (specialOptionsArr.length >= 1 && isPaste) {
        if (hasTextContent) {
          specialOptionsArr.push(SpecialPasteKind.keepTextOnly)
        }

        const slides = copyPasteContents[defaultPasteFormatting].slides
        updateSpecialPasteShowOptions(editorKit, specialOptionsArr, slides)
      } else {
        gSpecialPasteUtil.resetUI()
      }

      gSpecialPasteUtil.pasteEnd()

      switch (defaultPasteFormatting) {
        case PptSelectedContentsForCopyIndex.destination:
          gSpecialPasteUtil.kind = SpecialPasteKind.destinationTheme
          break
        case PptSelectedContentsForCopyIndex.source:
          gSpecialPasteUtil.kind = SpecialPasteKind.sourceFormatting
          break
      }
    }

    if (isUpdatingPasteDataFromClipboard) {
      pasteCallback()
    } else {
      if (
        checkPasteLimit(presentation, copyPasteContents, defaultPasteFormatting)
      ) {
        return false
      }
      editorKit.dataLoader.applyAsyncAssets(pasteCallback, true)
    }
    return true
  } else {
    return false
  }
}

function _pasteFromHtml(editorKit: IEditorKit, node: HTMLElement) {
  const htmlParser = new HtmlPaster(node)
  let collectShapes: Shape[] = []
  const collectImages: string[] = []
  const collectTables: GraphicFrame[] = []
  const resultSps: CopyWrapperOfSlideElement[] = []
  const pasteHtmlToPresentation = (_fonts?) => {
    const _exePaste = () => {
      // 未收集到内容，清空 collectShapes，不插入空形状
      if (
        collectShapes.length === 1 &&
        collectShapes[0].txBody?.textDoc?.children?.length === 1
      ) {
        const paraItems =
          collectShapes[0].txBody.textDoc.children.at(0)?.children
        if (
          paraItems &&
          paraItems.length === 2 &&
          paraItems.elements[0].children?.length === 0
        ) {
          collectShapes = []
        }
      }

      collectShapes.forEach((shape) => {
        const textDocContents = shape.txBody!.textDoc!.children
        if (textDocContents.length > 1) {
          const firstPara = textDocContents.at(0)!
          if (
            firstPara.children.length > 0 &&
            (firstPara.children.length === 1 ||
              (firstPara.children.length === 2 &&
                firstPara.children.at(0)!.children &&
                firstPara.children.at(0)!.children.length === 0))
          ) {
            shape.txBody!.textDoc!.removeContent(0, 1)
          }
        }

        const w = shape.txBody!.getRectWidthByLimit(
          (presentation.width * 2) / 3
        )
        const h = shape.txBody!.textDoc!.getWholeHeight()
        ensureSpPrXfrm(shape)
        shape.spPr!.xfrm!.setExtX(w)
        shape.spPr!.xfrm!.setExtY(h)
        shape.spPr!.xfrm!.setOffX((presentation.width - w) / 2.0)
        shape.spPr!.xfrm!.setOffY((presentation.height - h) / 2.0)

        const bodyPr = shape.getBodyPr().clone()
        bodyPr.rot = 0
        bodyPr.spcFirstLastPara = false
        bodyPr.vertOverflow = TextOverflowType.Overflow
        bodyPr.horzOverflow = TextOverflowType.Overflow
        bodyPr.vert = TextVerticalType.horz
        bodyPr.lIns = 2.54
        bodyPr.tIns = 1.27
        bodyPr.rIns = 2.54
        bodyPr.bIns = 1.27
        bodyPr.numCol = 1
        bodyPr.spcCol = 0
        bodyPr.rtlCol = false
        bodyPr.fromWordArt = false
        bodyPr.anchor = 4
        bodyPr.anchorCtr = false
        bodyPr.forceAA = false
        bodyPr.compatLnSpc = true
        const state = turnOffRecordChanges()
        bodyPr.prstTxWarp = createGeometryOfPrstTxWarp('textNoShape')
        restoreRecordChangesState(state)

        bodyPr.textFit = TextFit.new(TextAutoFitType.SHAPE)
        shape.txBody!.setBodyPr(bodyPr)
        moveCursorToEndPosInTextDocument(shape.txBody!.textDoc!)
        resultSps.push(new CopyWrapperOfSlideElement(shape, 0, 0, w, h))
      })

      collectTables.forEach((shape) => {
        const w = 100
        const h = 100
        ensureSpPrXfrm(shape)
        shape.spPr!.xfrm!.setExtX(w)
        shape.spPr!.xfrm!.setExtY(h)
        shape.spPr!.xfrm!.setOffX((presentation.width - w) / 2.0)
        shape.spPr!.xfrm!.setOffY((presentation.height - h) / 2.0)
        resultSps.push(new CopyWrapperOfSlideElement(shape, 0, 0, w, h))
      })
    }

    const _insertContent = () => {
      const isUpdatingPasteDataFromClipboard =
        getIsUpdatingPasteDataFromClipboard()
      const target = presentation.getCurrentTextTarget()
      if (
        target &&
        collectShapes.length === 1 &&
        collectImages.length === 0 &&
        collectTables.length === 0
      ) {
        if (isUpdatingPasteDataFromClipboard) {
          updateSpecialPasteShowOptions(editorKit)
        } else {
          const shape = resultSps[0].slideElement as Shape
          const content = shape.txBody!.textDoc!.children
          _insertInPresentation(editorKit, content)
        }
      } else {
        const selectedContent = new PresentationContentForCopyPaste()
        selectedContent.slideElements = resultSps

        if (!isUpdatingPasteDataFromClipboard) {
          insertCopyPasteContent(presentation, selectedContent, collectImages)
          calculateForRendering()
          moveCursorForward()
          presentation.syncEditorUIState()
        }

        let isPasteOnlyImage = false
        const copyPasteContentType = selectedContent.getCopyPasteContentType()
        if (
          copyPasteContentType === ContentTypeForCopyPaste.SlideElements ||
          (copyPasteContentType === ContentTypeForCopyPaste.Unknown &&
            collectImages.length)
        ) {
          isPasteOnlyImage = true
          if (
            selectedContent.slideElements.find(
              (sp) => !isInstanceTypeOf(sp, InstanceType.ImageShape)
            )
          ) {
            isPasteOnlyImage = false
          }
        }

        if (isPasteOnlyImage) {
          gSpecialPasteUtil.hideUI()
          gSpecialPasteUtil.resetUI()
        } else {
          updateSpecialPasteShowOptions(editorKit, [
            SpecialPasteKind.sourceFormatting,
            SpecialPasteKind.keepTextOnly,
          ])
        }

        gSpecialPasteUtil.pasteEnd()
      }
    }

    const presentation = editorKit.presentation
    if (presentation.slides.length === 0) {
      presentation.addNextSlide()
    }

    // 默认形状
    const shape = new Shape()
    const txBody = createTextBodyWithText('', shape)
    shape.setTxBody(txBody)
    collectShapes.push(shape)

    htmlParser.parse(
      node,
      {},
      true,
      true,
      collectShapes,
      collectImages,
      collectTables
    )

    collectShapes.forEach((shape) => {
      shape.setParent(presentation.slides[presentation.currentSlideIndex])
    })

    _exePaste()

    _insertContent()
  }

  if (
    gSpecialPasteUtil.isSpecialPasteStart &&
    SpecialPasteKind.keepTextOnly === gSpecialPasteUtil.kind
  ) {
    pasteHtmlToPresentation()
  } else {
    const fonts = htmlParser.preParse(node)
    pasteHtmlToPresentation(fonts)
  }
}

function _pasteFromText(editorKit: IEditorKit, text: string) {
  const presentation = editorKit.presentation
  if (presentation.slides.length === 0) {
    presentation.addNextSlide()
  }
  const shape = new Shape()
  shape.setParent(presentation.slides[presentation.currentSlideIndex])
  shape.setTxBody(createTextBodyWithText('', shape))
  const paras = shape.txBody!.textDoc!.children

  text = text.replace(/^(\r|\t)+|(\r|\t)+$/g, '')
  //text = text.replace(/(\r|\t)/g, ' ');
  if (text.length > 0) {
    // this.document = shape.txBody!.content

    let isAddParagraph = false
    eachOfUnicodeString(text, (code) => {
      if (isAddParagraph) {
        // content里面默认有一个段落，这里判断下是有是空, 避免第一行空行
        if (!shape.txBody!.textDoc!.isEmpty()) {
          addNewParagraphInTextDocument(shape.txBody!.textDoc!)
        }
        isAddParagraph = false
      }

      if (null != code && 13 !== code) {
        let item: RunContentElement
        if (0x0a === code || 0x0d === code) {
          isAddParagraph = true
        } else if (0x09 === code) {
          item = ParaTab.new()
          shape.addParaItem(item, false)
        } else if (0x20 !== code && 0xa0 !== code && 0x2009 !== code) {
          item = ParaText.new(code)
          shape.addParaItem(item, false)
        } else {
          item = ParaSpace.new()
          shape.addParaItem(item, false)
        }
      }
    })
  }

  const textPr = presentation.getCalcedTextPr()
  shape.txBody!.textDoc!.isSetToAll = true
  applyTextPrToParagraphInTextDocument(shape.txBody!.textDoc!, textPr)
  shape.txBody!.textDoc!.isSetToAll = false

  _insertInPresentation(editorKit, paras, true)
}

function _getDataInsideHtml(node: Nullable<Element>) {
  const data = getDataBySearchAttribute(node) as string
  if (data) {
    return deserializeDataInsideHtml(data)
  }
}

function getDataBySearchAttribute(node: Nullable<Element>) {
  if (node && node.children[0]) {
    const child = node.children[0]
    const dataAttr = child ? child.getAttribute(InternalDataAttributeKey) : null
    if (dataAttr) {
      return dataAttr
    } else {
      return getDataBySearchAttribute(node.children[0])
    }
  }
  return null
}

function updateSpecialPasteShowOptions(
  editorKit: IEditorKit,
  options?: SpecialPasteKindValue[],
  slides?
) {
  const uiOptions = gSpecialPasteUtil.uiOptions
  const { presentation, editorUI } = editorKit
  const curSlideIndex = presentation.currentSlideIndex
  options = options ??
    uiOptions.formats ?? [
      SpecialPasteKind.sourceFormatting,
      SpecialPasteKind.keepTextOnly,
    ]
  let x, y, w, h
  let needConvertPosition = true
  const hasSlides = slides?.length
  if (hasSlides) {
    // 粘贴 slide
    const pasteLastSlideIndex = curSlideIndex + slides.length - 1
    gSpecialPasteUtil.uiOptions.setPasteSlideIndex(pasteLastSlideIndex)
    const pos =
      editorUI.thumbnailsManager.getSlideThumbnailPosByIndex(
        pasteLastSlideIndex
      )
    if (pos) {
      ;[x, y] = [pos.x, pos.y]
      needConvertPosition = false
    }
  } else {
    const pos = presentation.getCursorPositionInText()
    if (!pos) {
      // 粘贴为 slideElement
      const _pos = presentation.getSelectionBounds()
      w = _pos.w
      h = _pos.h
      x = _pos.x + w
      y = _pos.y + h
    } else {
      // 粘贴到文本框
      x = pos.x
      y = pos.y
    }
  }

  uiOptions.updateOptions(options)

  const target = presentation.getCurrentTextTarget()
  if (target && target.id) {
    uiOptions.setShapeId(target.id)
  } else {
    uiOptions.setShapeId(null)
  }

  let position
  if (needConvertPosition) {
    const screenPos = convertGlobalInnerMMToPixPos(editorUI, x, y)
    position = new Rect(screenPos.x, screenPos.y, w, h)
  } else {
    position = new Rect(x, y, w, h)
  }

  uiOptions.setScreenPosition(position)
  uiOptions.setPosition({
    x: x,
    y: y,
    slideIndex: curSlideIndex,
    w: w,
    h: h,
    needConvertPosition,
  })
}

function _specialPasteParagraphApplier(
  presentation: Presentation,
  paragraph: Paragraph
) {
  const pasteKind = gSpecialPasteUtil.kind

  const paraPr = presentation.getCalcedParaPr(false)
  const textPr = presentation.getCalcedTextPr(false)

  if (pasteKind === SpecialPasteKind.keepTextOnly) {
    if (paraPr) {
      paragraph.setPr(paraPr)

      if (paragraph.paraTextPr && textPr) {
        paragraph.paraTextPr.setTextPr(textPr)
      }
    }
    const paraItems = paragraph.children
    if (textPr) {
      for (let i = 0; i < paraItems.length; i++) {
        const item = paraItems.at(i)!
        switch (item.instanceType) {
          case InstanceType.ParaRun: {
            item.fromTextPr(textPr)
            break
          }
          case InstanceType.ParaHyperlink: {
            const runs = item.children.remove(0, item.children.length)
            paraItems.recycle(paraItems.remove(i, 1))

            for (let n = 0; n < runs.length; n++) {
              paraItems.insert(i + n, runs[n])
            }
            i--
            break
          }
        }
      }
    }
  }

  return paragraph
}

function _insertInPresentation(
  editorKit: IEditorKit,
  paragraphs: RecyclableArray<Paragraph>,
  isText?: boolean
) {
  const presentation = editorKit.presentation
  const copyPasteContent = new PresentationContentForCopyPaste()
  copyPasteContent.paraCollection = new ParaCollection()
  ManagedSliceUtil.forEach(paragraphs, (para) => {
    if (gSpecialPasteUtil.isSpecialPasteStart && !isText) {
      para = _specialPasteParagraphApplier(presentation, para)
    }
    copyPasteContent.paraCollection!.add(para)
  })

  insertCopyPasteContent(presentation, copyPasteContent)
  calculateForRendering()
  moveCursorForward()
  presentation.syncEditorUIState()

  updateSpecialPasteShowOptions(editorKit)
  if (ClipboardSettings.isPastTextOnly) {
    gSpecialPasteUtil.resetUI()
  }
  gSpecialPasteUtil.pasteEnd()
}

// helpers
function restoreOrderByRefID<T extends { refId?: string }>(
  refIds: Nullable<string[]>,
  items: T[]
): T[] {
  if (refIds == null) {
    return items
  }
  const map = items.reduce((result, item) => {
    if (item.refId) {
      result[item.refId] = item
    }
    return result
  }, {} as Dict<T>)

  const res: T[] = []
  for (const rId of refIds) {
    const item = map[rId]
    if (item) {
      res.push(item)
      delete map[rId]
    }
  }

  return res
}

function resetRefIds<T extends { refId?: string }>(items: T[]) {
  if (items && items.length) {
    items.forEach((item) => {
      item.refId = undefined
    })
  }
}

function deserializeDataInsideHtml(
  data: string
): DataInsideHtmlType | undefined {
  try {
    const res = JSON.parse(decodeURIComponent(data)) as DataInsideHtmlType
    return {
      version: res['version'],
      internalData: res['internalData'],
      sourceFileId: res['sourceFileId'],
    }
  } catch (e) {
    logger.error(`[deserializeDataInsideHtml]`, e)
    return undefined
  }
}

function _handleHtmlPasteWithCallback(
  _html_data: string,
  text_data: Nullable<string>,
  cb: (node: HTMLPasteData, textData: Nullable<string>) => void
) {
  const id = 'sm-xmdea'
  let iframeElement = document.getElementById(id)
  if (!iframeElement) {
    iframeElement = toElement(
      Dom.iframe(`#${id}`, {
        style: {
          ['position']: 'absolute',
          ['top']: '-100px',
          ['left']: '0px',
          ['width']: '10000px',
          ['height']: '100px',
          ['overflow']: 'hidden',
          ['zIndex']: '-1000',
        },
        attrs: { ['sandbox']: 'allow-same-origin allow-scripts' },
      })
    ) as HTMLIFrameElement
    ;(iframeElement as any).name = id
    document.body.appendChild(iframeElement)
  } else {
    iframeElement.style.width = '10000px'
  }
  const iframe = window.frames[id]
  if (iframe) {
    iframe.document.open()
    iframe.document.write(_html_data)
    iframe.document.close()
    if (null != iframe.document && null != iframe.document.body) {
      iframeElement.style.display = 'block'
      cb({ node: iframe.document.body, htmlText: _html_data }, text_data)
    }
  }
  iframeElement.style.width = '100px'

  if (iframeElement && iframeElement.style.display !== 'none') {
    iframeElement.blur()
    iframeElement.style.display = 'none'
  }
}
