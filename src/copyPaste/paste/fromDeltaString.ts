import { Nullable, Dict } from '../../../liber/pervasive'
import { isNumber } from '../../common/utils'
import { Paragraph } from '../../core/Paragraph/Paragraph'
import { paraCollectionFromParagraphs } from '../../core/Paragraph/ParaCollection'
import { CSld } from '../../core/Slide/CSld'
import { Notes } from '../../core/Slide/Notes'
import { Slide } from '../../core/Slide/Slide'
import { SlideLayout } from '../../core/Slide/SlideLayout'
import { SlideMaster } from '../../core/Slide/SlideMaster'
import { Theme } from '../../core/SlideElement/attrs/theme'
import { collectAllFontNamesForSlideElement } from '../../core/utilities/shape/getters'
import { EditorKit } from '../../editor/global'
import { IEditorKit } from '../../editor/type'
import { getRefItemMap } from '../../io/infoData'
import { gOffsetStateForCopyShape } from '../gOffsetStateForCopyShape'
import { CopyWrapperOfSlideElement } from '../copyPasteContent'

import { logger } from '../../lib/debug/log'
import { hookManager, Hooks } from '../../core/hooks'
import { loadParagraphs } from '../../io/loaders/text'
import { StringSlice, stringToSlice } from '../../re/export/modoc'
import {
  dataFromDelta,
  firstItemOfDeltaSlice,
  getIterFromModocItemMap,
  ModocItem,
} from '../../io/loaders/utils'
import { FromJSONParams } from '../../io/defs/pptdoc'
import { loadLayout } from '../../io/loaders/SlideLayout'
import { applySlideMaster } from '../../io/loaders/SlideMaster'
import { genTheme } from '../../io/loaders/Theme'

import { loadSlide } from '../../io/loaders/Slide'
import { genSlideNote } from '../../io/loaders/Notes'
import { DrawingExtra } from '../type'
import { SlideElement } from '../../core/SlideElement/type'
import { isHiddenSp, loadSpTree } from '../../io/loaders/csld/groupShape'
import { CSldDataLayout, CSldDataLayoutTypeMap } from '../../io/defs/csld'
import {
  applyCxnSpToSpMapState,
  clearCxnState,
} from '../../core/utilities/shape/manageCxnSpState'
import { PPT_Default_SIZE } from '../../core/common/const/drawing'

export function getLayoutsFromDeltaString(
  editorKit: IEditorKit,
  deltaString: string,
  ignoreRId = false
): SlideLayout[] {
  const presentation = editorKit.presentation
  const layouts: SlideLayout[] = []

  const op = firstItemOfDeltaSlice(stringToSlice(deltaString))

  const params: FromJSONParams = {
    presentation,
    ignoreRId,
  }
  if (op && op.type === 'map') {
    const iter = getIterFromModocItemMap(op)
    while (iter.hasNext()) {
      const { id: rId, delta } = iter.next()
      const { layout } = loadLayout(delta, params.presentation, params, rId)
      layouts.push(layout)
      hookManager.invoke(Hooks.Attrs.GroupCollectedResources, {
        type: 'layouts',
        refId: rId,
        isApply: true,
      })
    }
  }

  return layouts
}

export function getMastersFromDeltaString(
  editorKit: IEditorKit,
  deltaString: string,
  ignoreRId = false
): SlideMaster[] {
  const presentation = editorKit.presentation
  const masters: SlideMaster[] = []

  const op = firstItemOfDeltaSlice(stringToSlice(deltaString))
  if (op && op.type === 'map') {
    const iter = getIterFromModocItemMap(op)
    while (iter.hasNext()) {
      const { id: rId, delta } = iter.next()
      const master = new SlideMaster(presentation)
      applySlideMaster(delta, rId, master, null, {
        presentation,
        ignoreRId,
      })
      hookManager.invoke(Hooks.Attrs.GroupCollectedResources, {
        type: 'slideMasters',
        refId: rId,
        isApply: true,
      })
      // FIXME: 主题没存大小，这里默认设置 16:9 的大小
      // 复制粘贴保留原格式 这里也应该设置复制源的大小
      master.width = PPT_Default_SIZE.width
      master.height = PPT_Default_SIZE.height 
      masters.push(master)
    }
  }
  return masters
}

export function getThemesFromDeltaString(
  editorKit: IEditorKit,
  deltaString: string,
  ignoreRId = false
): Theme[] {
  const presentation = editorKit.presentation
  const themes: Theme[] = []

  const op = firstItemOfDeltaSlice(stringToSlice(deltaString))
  if (op && op.type === 'map') {
    const iter = getIterFromModocItemMap(op)
    while (iter.hasNext()) {
      const { id: rId, delta } = iter.next()
      const { theme } = genTheme(delta, rId, { presentation, ignoreRId })
      themes.push(theme)
      hookManager.invoke(Hooks.Attrs.GroupCollectedResources, {
        type: 'themes',
        refId: rId,
        isApply: true,
      })
    }
  }

  return themes
}

export function getParaCollectionFromDeltaString(
  delta: Nullable<string>,
  fonts?: Dict
) {
  if (delta == null || delta === '') return undefined

  let paragraphs: undefined | Paragraph[]
  try {
    paragraphs = loadParagraphs(stringToSlice(delta))
  } catch (e) {
    logger.error(`[loadParagraphs] invalid delta`, e)
  }

  return paraCollectionFromParagraphs(paragraphs, fonts)
}

export function getSlideElementsInfoFromDeltaString(
  editorKit: IEditorKit,
  deltaString: string,
  extra?: Dict<DrawingExtra>
) {
  const presentation = editorKit.presentation
  const { currentSlideIndex: curSlideIndex } = presentation
  const mainObject = presentation.slides[curSlideIndex]

  if (mainObject == null) {
    return {
      shapes: [],
      fontMap: {},
    }
  }

  const csld = new CSld(mainObject)

  let onApplySp: undefined | ((sp: SlideElement, rId: Nullable<string>) => void)
  const spExtraMap = new Map<SlideElement, DrawingExtra>()
  if (extra) {
    onApplySp = (sp, rId) => {
      if (rId != null && extra[rId] != null) {
        spExtraMap.set(sp, extra[rId])
      }
    }
  }
  applyCSld(
    stringToSlice(deltaString),
    csld,
    mainObject,
    {
      presentation,
      ignoreRId: true,
    },
    onApplySp
  )
  const shapes = csld.spTree.map((sp) => {
    // 只在同一 slide 内， 复制形状加上偏移量
    if (gOffsetStateForCopyShape.copyShapeOnly && sp?.spPr?.xfrm) {
      const slidePasteTime =
        gOffsetStateForCopyShape.getSlidePasteCount(curSlideIndex)
      const offset = slidePasteTime * 5
      if (isNumber(sp.spPr.xfrm.offX)) {
        sp.spPr.xfrm.offX += offset
      }
      if (isNumber(sp.spPr.xfrm.offY)) {
        sp.spPr.xfrm.offY += offset
      }
    }

    const { xfrm } = (sp as any).spPr || {}
    const { x, y, extX, extY } = xfrm || { x: 0, y: 0, extX: 0, extY: 0 }
    return new CopyWrapperOfSlideElement(
      sp,
      x,
      y,
      extX,
      extY,
      undefined,
      spExtraMap.get(sp)
    )
  })

  spExtraMap.clear()
  const fontMap = {}
  for (let i = 0; i < shapes.length; ++i) {
    collectAllFontNamesForSlideElement(shapes[i].slideElement, fontMap)
  }

  return {
    shapes,
    fontMap,
  }
}

function applyCSld(
  ds: StringSlice,
  csld: CSld,
  mainObject: Nullable<any>,
  params: FromJSONParams,
  onApplySp?: (sp: SlideElement, rId: Nullable<string>) => void
) {
  clearCxnState()

  const data = dataFromDelta(ds, CSldDataLayout, CSldDataLayoutTypeMap)
  let op: ModocItem | undefined = data['attrs']

  op = data['spTree']
  if (op) {
    const updateSp = (sp: SlideElement, rId: Nullable<string>) => {
      if (sp && !isHiddenSp(sp)) {
        sp.spTreeParent = csld
        if (onApplySp) {
          onApplySp(sp, rId)
        }
      }
    }

    loadSpTree(csld, op, mainObject, params, updateSp)

    applyCxnSpToSpMapState()
  }
}

// to improve
export function getSlidesInfoFromDeltaString(
  editorKit: IEditorKit,
  deltaString: string,
  slidesRefId: string[] = []
) {
  const presentation = editorKit.presentation

  const slideMap: Dict<Slide> = {}

  const op = firstItemOfDeltaSlice(stringToSlice(deltaString))
  if (op && op.type === 'map') {
    const iter = getIterFromModocItemMap(op)
    while (iter.hasNext()) {
      const { id: rId, delta } = iter.next()
      const layouts = getRefItemMap(
        EditorKit.collaborationManager.currentInfoData()?.layouts || []
      )
      const res = loadSlide(
        delta,
        presentation,
        {
          presentation,
          // ignoreId: true,
        },
        rId,
        layouts
      )

      if (res?.slide) {
        slideMap[rId] = res.slide
      }
    }
  }

  const slides: Slide[] = []

  for (const rId of slidesRefId) {
    const slide = slideMap[rId]
    if (slide) {
      slides.push(slide)
      delete slideMap[rId]
    }
  }

  //images and fonts
  const fontMap: Dict = {}
  for (let i = 0; i < slides.length; ++i) {
    slides[i].getAllFonts(fontMap)
  }

  return { slideCopyObjects: slides, fontMap }
}

export function getNotesFromDeltaString(
  editorKit: IEditorKit,
  deltaString: string,
  notesRefId: string[]
): Nullable<Notes>[] {
  const presentation = editorKit.presentation
  const notes: Notes[] = []

  const op = firstItemOfDeltaSlice(stringToSlice(deltaString))
  if (op && op.type === 'map') {
    const iter = getIterFromModocItemMap(op)
    while (iter.hasNext()) {
      const { id: rId, delta } = iter.next()
      const { notes: newNote } = genSlideNote(delta, { presentation }, rId)

      notes.push(newNote)
    }
  }

  return notesRefId.map((refId) =>
    notes.find((note) => note && refId !== '' && note.refId === refId)
  )
}
