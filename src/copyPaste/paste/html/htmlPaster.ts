import { Dict, Nullable } from '../../../../liber/pervasive'
import {
  LineHeightRule,
  ShdType,
  VertAlignType,
} from '../../../core/common/const/attrs'
import {
  convertUnitValStrToMMWithUnit,
  eachOfUnicodeString,
  toInt,
} from '../../../common/utils'
import { TableCellBorder } from '../../../core/Table/attrs/CellBorder'
import { CShd } from '../../../core/Table/attrs/CShd'
import { TextPr } from '../../../core/textAttributes/TextPr'
import {
  Slide_Default_Width,
  Default_Left_Margin,
  Default_Right_Margin,
} from '../../../core/common/const/drawing'
import {
  Factor_mm_to_pt,
  Factor_pix_to_mm,
  HightLight_None,
} from '../../../core/common/const/unit'
import { copyDict } from '../../../core/common/helpers'
import { NumberingFormat } from '../../../core/common/numberingFormat'
import { ParaNewLine } from '../../../core/Paragraph/RunElement/ParaNewLine'
import { ParaSpace } from '../../../core/Paragraph/RunElement/ParaSpace'
import { ParaText } from '../../../core/Paragraph/RunElement/ParaText'
import { GraphicFrame } from '../../../core/SlideElement/GraphicFrame'
import { Shape } from '../../../core/SlideElement/Shape'
import { TextBody } from '../../../core/SlideElement/TextBody'
import { Table } from '../../../core/Table/Table'
import { moveCursorToStartPosInTable } from '../../../core/utilities/cursor/moveToEdgePos'
import { TextDocument } from '../../../core/TextDocument/TextDocument'
import {
  createTextBodyWithText,
  getDefaultFontFamily,
  getDefaultFontSize,
  parseHexColorFromString,
  stringValToMM,
} from '../../utils'
import { addNewParagraphInTextDocument } from '../../../core/TextDocument/utils/operations'
import { TableRow } from '../../../core/Table/TableRow'
import { TableCell } from '../../../core/Table/TableCell'
import { CellBorderType, SetCellMarginType } from '../../../core/Table/common'
import { CHexColor } from '../../../core/color/CHexColor'
import { FillEffects } from '../../../core/SlideElement/attrs/fill/fill'
import { CComplexColor } from '../../../core/color/complexColor'
import {
  createTableByGrid,
  getCalcedStyle,
  getStyleValue,
  isBlockHtmlTag,
  parseBorder,
  parseGenericFont,
} from './utils'
import {
  TableLengthUnitType,
  VMergeType,
} from '../../../core/common/const/table'

export class HtmlPaster {
  rootNode: HTMLElement
  borderCache: Dict
  maxWidth: number
  document: TextDocument | null
  fonts: Dict
  constructor(rootNode: HTMLElement) {
    this.rootNode = rootNode
    this.borderCache = {}
    this.maxWidth =
      Slide_Default_Width - Default_Left_Margin - Default_Right_Margin
    this.fonts = {}
    this.document = null
  }

  preParse(node: HTMLElement) {
    return this._recursivePreParse(node, true, true)
  }

  _recursivePreParse(
    node: HTMLElement,
    isIgnoreStyle: boolean,
    isCheckFonts?: boolean
  ) {
    // const nodeName = node.nodeName.toLowerCase()
    const nodeType = node.nodeType
    if (!isIgnoreStyle) {
      if (Node.TEXT_NODE === nodeType) {
        const calcedStyle = getCalcedStyle(node.parentNode as HTMLElement)
        const fontFamily = getDefaultFontFamily(
          getStyleValue(
            node.parentNode! as HTMLElement,
            calcedStyle,
            'font-family'
          )
        )
        if (fontFamily) {
          this.fonts[fontFamily] = {
            name: parseGenericFont(fontFamily),
            index: -1,
          }
        }
      } else {
        // const src = node.getAttribute('src')
        // if (src) this.images[src] = src
      }
    }
    for (let i = 0, length = node.childNodes.length; i < length; i++) {
      const child = node.childNodes[i]
      const childNodeType = child.nodeType
      if (
        !(
          Node.ELEMENT_NODE === childNodeType ||
          Node.TEXT_NODE === childNodeType
        )
      ) {
        continue
      }

      if (Node.TEXT_NODE === child.nodeType) {
        let value = child.nodeValue
        if (!value) continue
        value = value.replace(/(\r|\t|\n)/g, '')
        if ('' === value) continue
      }
      this._recursivePreParse(child as HTMLElement, false)
    }

    if (isCheckFonts) {
      const fonts: string[] = []
      for (const font_family in this.fonts) {
        const fontItem = this.fonts[font_family]
        this.fonts[font_family].index = -1
        fonts.push(fontItem.name)
      }
      return fonts
    }
  }

  parse(
    node: HTMLElement,
    pPr: Dict,
    isRoot: boolean,
    isAddParagraph: boolean | undefined,
    collectShapes: Shape[],
    collectImages: string[],
    collectTables: GraphicFrame[]
  ) {
    const shape = collectShapes[0]
    const txBody = shape.txBody!

    if (!isRoot) {
      const nodeName = node.nodeName.toLowerCase()
      //TEXT NODE
      if (Node.TEXT_NODE === node.nodeType) {
        isAddParagraph = this._parseTextNodeFunc(
          node,
          shape,
          txBody,
          isAddParagraph
        )
        return false
      }

      //TABLE
      if ('table' === nodeName) {
        this._preParseTable(
          node as HTMLTableElement,
          collectShapes,
          collectImages,
          collectTables
        )
        return
      }

      //NUMBERING
      this._parseNumberingFunc(pPr, nodeName)

      //IMAGE
      if ('img' === nodeName) {
        this._parseImageFunc(node, collectImages)
        return isAddParagraph
      }

      //LINEBREAK
      this._parseLineBreakFunc(node, nodeName, shape)
    }

    node.childNodes.forEach((child) => {
      isAddParagraph = this._parseChildNodesFunc(
        node,
        child as HTMLElement,
        pPr,
        txBody,
        isAddParagraph,
        collectShapes,
        collectImages,
        collectTables
      )
    })

    return isAddParagraph
  }

  _parseChildNodesFunc(
    node: HTMLElement,
    child: HTMLElement,
    pPr: Dict,
    txBody: TextBody,
    isAddParagraph: boolean | undefined,
    collectShapes: Shape[],
    collectImages: string[],
    collectTables: GraphicFrame[]
  ) {
    const nodeType = child.nodeType
    let childNodeName = child.nodeName.toLowerCase()
    if (
      !(Node.ELEMENT_NODE === nodeType || Node.TEXT_NODE === nodeType) ||
      childNodeName === 'style' ||
      childNodeName === '#comment' ||
      childNodeName === 'script'
    ) {
      return
    }

    if (Node.TEXT_NODE === child.nodeType) {
      let value = child.nodeValue
      if (!value) {
        return
      }
      value = value.replace(/(\r|\t|\n)/g, '')
      if ('' === value) {
        return
      }
    }
    childNodeName = child.nodeName.toLowerCase()
    const isBlockChild = isBlockHtmlTag(childNodeName)
    if (isBlockChild) {
      isAddParagraph = true
    }

    let isInsertLink: any = null
    if ('a' === childNodeName) {
      const href = (child as HTMLLinkElement).href
      if (href) {
        const title = child.getAttribute('title')

        const textDoc = (this.document = txBody.textDoc)!

        isInsertLink = node.getElementsByTagName('img')

        let text: Nullable<string>
        if (isInsertLink && isInsertLink.length) {
          isInsertLink = null
        } else {
          text = child.innerText ? child.innerText : child.textContent
        }

        if (isInsertLink) {
          if (isAddParagraph) {
            if (!textDoc.isEmpty()) {
              addNewParagraphInTextDocument(textDoc)
            }
          }
          const pos =
            true === textDoc.selection.isUse
              ? textDoc.selection.startIndex
              : textDoc.cursorPosition.childIndex
          const para = textDoc.children.at(pos)
          para &&
            para.insertHyperlink({
              text: !text || text.length <= 0 ? href : text,
              value: href,
              toolTip: title,
            })
          isAddParagraph = false
        }
      }
    }

    if (!isInsertLink) {
      isAddParagraph = this.parse(
        child,
        copyDict(pPr),
        false,
        isAddParagraph,
        collectShapes,
        collectImages,
        collectTables
      )
    }

    if (isBlockChild) {
      isAddParagraph = true
    }
    return isAddParagraph
  }

  _parseTextNodeFunc(
    node: HTMLElement,
    shape: Shape,
    txBody: TextBody,
    isAddParagraph?: boolean
  ) {
    let value = node.nodeValue ?? ''
    let whiteSpacing = false
    if (node.parentNode) {
      const calcedStyle = getCalcedStyle(node.parentNode as HTMLElement)
      const tmpWhiteSpacingStyle = getStyleValue(
        node.parentNode as HTMLElement,
        calcedStyle,
        'white-space'
      )
      whiteSpacing =
        'pre' === tmpWhiteSpacingStyle || 'pre-wrap' === tmpWhiteSpacingStyle
    }

    if (!whiteSpacing) {
      value = value.replace(/^(\r|\t|\n)+|(\r|\t|\n)+$/g, '')
      value = value.replace(/(\r|\t|\n)/g, ' ')

      if (value.charCodeAt(0) !== 160) {
        const checkSpaceCodes = value.replace(/(\s)/g, '')
        if (checkSpaceCodes === '') {
          value = ''
        }
      }
    }

    if (value.length > 0) {
      const content = (this.document = txBody.textDoc)!
      if (isAddParagraph) {
        // content里面默认有一个段落，这里判断下是有是空, 避免第一行空行
        if (!content.isEmpty()) {
          addNewParagraphInTextDocument(content)
        }
        isAddParagraph = false
      }

      const rPr = this._parseTextPr(node.parentNode as HTMLElement)
      shape.applyTextPr(rPr, false)

      eachOfUnicodeString(value, (charCode) => {
        if (null !== charCode) {
          let item: ParaText | ParaSpace
          if (0x20 !== charCode && 0xa0 !== charCode && 0x2009 !== charCode) {
            item = ParaText.new(charCode)
          } else item = ParaSpace.new()

          shape.addParaItem(item, false)
        }
      })
    }
    return isAddParagraph
  }

  _parseNumberingFunc(pPr: Dict, nodeName: string) {
    if ('ul' === nodeName || 'ol' === nodeName || 'li' === nodeName) {
      if ('ul' === nodeName) {
        pPr.numType = NumberingFormat.Char
      } else if ('ol' === nodeName) {
        pPr.numType = NumberingFormat.arabicPeriod
      }
    }
  }

  _parseImageFunc(node: HTMLElement, collectImages: string[]) {
    const imageSrc = node.getAttribute('src')
    if (imageSrc) {
      collectImages.push(imageSrc)
    }
  }

  _parseLineBreakFunc(node: HTMLElement, nodeName: string, shape: Shape) {
    if ('br' === nodeName || 'always' === node.style.pageBreakBefore) {
      if ('always' === node.style.pageBreakBefore) {
        shape.addParaItem(ParaNewLine.new(), false)
      } else {
        shape.addParaItem(ParaNewLine.new(), false)
      }
    }
  }

  _preParseTable(
    tableNode: HTMLTableElement,
    collectShapes: Shape[],
    collectImages: string[],
    collectTables: GraphicFrame[]
  ) {
    const tbodyNode = Array.from(tableNode.childNodes).find(
      (node) => 'tbody' === node.nodeName.toLowerCase()
    ) as HTMLTableSectionElement | undefined

    if (!tbodyNode) {
      return
    }

    let rowCount = 0
    let minColCount = 0
    let maxColCount = 0
    const rowSums: Record<number, number> = {}
    rowSums[0] = 0
    let maxSum = 0
    let curCol = 0
    let curSum = 0
    const rowSpans = {}
    const parseSpans = () => {
      let spans = rowSpans[curCol]
      while (null != spans && spans.row > 0) {
        spans.row--
        curCol += spans.col
        curSum += spans.width
        spans = rowSpans[curCol]
      }
    }

    for (let i = 0, length = tbodyNode.childNodes.length; i < length; ++i) {
      if ('tr' === tbodyNode.childNodes[i].nodeName.toLowerCase()) {
        const tr = tbodyNode.childNodes[i] as HTMLTableRowElement
        curSum = 0
        curCol = 0

        tr.childNodes.forEach((node) => {
          const tcName = node.nodeName.toLowerCase()
          if ('td' === tcName || 'th' === tcName) {
            const tc = node as HTMLTableCellElement
            parseSpans()

            const calcedStyle = getCalcedStyle(tc)
            const calcedWidth = getStyleValue(tc, calcedStyle, 'width')
            const width =
              stringValToMM(calcedWidth) ?? tc.clientWidth * Factor_pix_to_mm

            let colSpan = toInt(tc.getAttribute('colspan'))
            colSpan = !isNaN(colSpan) ? colSpan : 1
            const curRowSpan = toInt(tc.getAttribute('rowspan'))
            if (!isNaN(curRowSpan) && curRowSpan > 1) {
              rowSpans[curCol] = {
                row: curRowSpan - 1,
                col: colSpan,
                width: width,
              }
            }

            curSum += width
            if (null == rowSums[curCol + colSpan]) {
              rowSums[curCol + colSpan] = curSum
            }
            curCol += colSpan
          }
        })
        parseSpans()

        maxSum = Math.max(maxSum, curSum)

        if (0 === curCol) {
          tbodyNode.removeChild(tr)
          length--
          i--
        } else {
          if (0 === minColCount || minColCount > curCol) {
            minColCount = curCol
          }
          maxColCount = Math.max(maxColCount, curCol)
          rowCount++
        }
      }
    }

    if (rowCount > 0 && maxColCount > 0) {
      const tableGrids = this._parseTableGrids(maxSum, rowSums)
      const table = createTableByGrid(tableGrids)
      const graphicFrame = table.parent
      table.setTableStyleId(0)
      collectTables.push(graphicFrame)

      this._parseTable(
        tableNode,
        tbodyNode,
        table,
        collectShapes,
        collectImages
      )
      moveCursorToStartPosInTable(table)
      return
    }
  }

  _parseTableGrids(maxSum: number, rowSums: Record<number, number>) {
    let isUseScaleFactor = false
    let scaleFactor = 1
    if (maxSum * scaleFactor > this.maxWidth) {
      scaleFactor = (scaleFactor * this.maxWidth) / maxSum
      isUseScaleFactor = true
    }

    const tableGrids: number[] = []
    let prevIndex: null | number = null
    let prevVal = 0
    for (const i in rowSums) {
      const curIndex = Number(i) - 0
      const curVal = rowSums[i]
      let curWidth = curVal - prevVal
      if (isUseScaleFactor) {
        curWidth *= scaleFactor
      }
      if (null != prevIndex) {
        const diff = curIndex - prevIndex
        if (1 === diff) {
          tableGrids.push(curWidth)
        } else {
          const pVal = curWidth / diff
          for (let i = 0; i < diff; ++i) {
            tableGrids.push(pVal)
          }
        }
      }
      prevVal = curVal
      prevIndex = curIndex
    }
    return tableGrids
  }

  _parseTable(
    tableNode: HTMLTableElement,
    tbodyNode: HTMLTableSectionElement,
    table: Table,
    collectShapes: Shape[],
    collectImages: string[]
  ) {
    this._parseTablePr(tableNode, table)
    const rowSpans = {}
    tbodyNode.childNodes.forEach((tr) => {
      if (
        'tr' === tr.nodeName.toLowerCase() &&
        (tr as HTMLTableRowElement).children.length !== 0
      ) {
        const row = table.addRow(table.children.length, 0)
        this._parseTableRow(
          tr as HTMLTableRowElement,
          row,
          rowSpans,
          collectShapes,
          collectImages
        )
      }
    })
  }

  _parseTablePr(tableNode: HTMLTableElement, table: Table) {
    table.setInsideHTableBorder(TableCellBorder.new())
    table.setInsideVTableBorder(TableCellBorder.new())
    const calcedStyle = getCalcedStyle(tableNode)
    const bgColor = getStyleValue(tableNode, calcedStyle, 'background-color')
    if (null != bgColor) {
      const color = parseHexColorFromString(bgColor)
      if (color) {
        table.setShd(ShdType.Clear, color.r, color.g, color.b)
      }
    }

    const leftBorder = parseBorder(
      calcedStyle,
      tableNode,
      'left',
      'Left',
      false,
      true
    )
    if (null != leftBorder) table.setLeftTableBorder(leftBorder)
    const topBorder = parseBorder(
      calcedStyle,
      tableNode,
      'top',
      'Top',
      false,
      true
    )
    if (null != topBorder) table.setTopTableBorder(topBorder)
    const rightBorder = parseBorder(
      calcedStyle,
      tableNode,
      'right',
      'Right',
      false,
      true
    )
    if (null != rightBorder) table.setRightTableBorder(rightBorder)
    const bottomBorder = parseBorder(
      calcedStyle,
      tableNode,
      'bottom',
      'Bottom',
      false,
      true
    )
    if (null != bottomBorder) table.setBottomTableBorder(bottomBorder)
  }

  _parseTableRow(
    node: HTMLTableRowElement,
    row: TableRow,
    rowSpans: Dict,
    collectShapes: Shape[],
    collectImages: string[]
  ) {
    this._parseTableRowPr(node, row)

    let cellIndexSpan = 0
    const parseSpans = () => {
      let spans = rowSpans[cellIndexSpan]
      while (null != spans) {
        const curCell = row.addCell(row.cellsCount, null, false)
        curCell.setVMerge(VMergeType.Continue)
        if (spans.col > 1) curCell.setGridSpan(spans.col)
        spans.row--
        if (spans.row <= 0) delete rowSpans[cellIndexSpan]
        cellIndexSpan += spans.col
        spans = rowSpans[cellIndexSpan]
      }
    }
    node.childNodes.forEach((child) => {
      parseSpans()
      const tcName = child.nodeName.toLowerCase()
      if ('td' === tcName || 'th' === tcName) {
        const tc = child as HTMLTableCellElement
        let colSpan = toInt(tc.getAttribute('colspan'))
        colSpan = !isNaN(colSpan) ? colSpan : 1

        const curCell = row.addCell(row.cellsCount, null, false)
        if (colSpan > 1) curCell.setGridSpan(colSpan)
        let rowSpan = toInt(tc.getAttribute('rowspan'))
        rowSpan = !isNaN(rowSpan) ? rowSpan : 1

        if (rowSpan > 1) {
          curCell.setVMerge(VMergeType.Restart)
          rowSpans[cellIndexSpan] = { row: rowSpan - 1, col: colSpan }
        }

        this._parseTableCell(tc, curCell, collectShapes, collectImages)
        cellIndexSpan += colSpan
      }
    })
    parseSpans()
  }

  _parseTableRowPr(node: HTMLTableRowElement, row: TableRow) {
    if (node.style.height) {
      const heightV = node.style.height
      if (
        !(
          'auto' === heightV ||
          'inherit' === heightV ||
          -1 !== heightV.indexOf('%')
        )
      ) {
        const height = stringValToMM(heightV)
        if (height != null) {
          row.setHeight({ value: height, heightRule: LineHeightRule.AtLeast })
        }
      }
    }
  }

  _parseTableCell(
    node: HTMLTableCellElement,
    cell: TableCell,
    collectShapes: Shape[],
    collectImages: string[]
  ) {
    this._parseTableCellPr(node, cell)

    const arrOfShapes: Shape[] = []
    const arrOfImages: string[] = []
    const shape = new Shape()
    shape.setTxBody(createTextBodyWithText('', shape))
    arrOfShapes.push(shape)
    this.parse(node, {}, true, true, arrOfShapes, arrOfImages, [])

    // 插入单元格内容
    if (arrOfShapes.length > 0) {
      const firstSp = arrOfShapes[0]
      const textDoc = firstSp.txBody!.textDoc!

      for (let i = 0, l = textDoc.children.length; i < l; ++i) {
        const paragraph = textDoc.children.at(i)!
        cell.textDoc.insertParagraph(i + 1, paragraph)
      }

      cell.textDoc.removeContent(0, 1)
      arrOfShapes.splice(0, 1)
    }
    for (let i = 0; i < arrOfShapes.length; ++i) {
      collectShapes.push(arrOfShapes[i])
    }
    for (let i = 0; i < arrOfImages.length; ++i) {
      collectImages.push(arrOfImages[i])
    }
  }

  _parseTableCellPr(node: HTMLTableCellElement, cell: TableCell) {
    const isAddIfNull = false
    const calcedStyle = getCalcedStyle(node)
    const bgColor = getStyleValue(node, calcedStyle, 'background-color')
    const hexColor = parseHexColorFromString(bgColor)
    if (hexColor) {
      const shd = CShd.new({
        value: ShdType.Clear,
        fillEffects: FillEffects.SolidRGBA(hexColor.r, hexColor.g, hexColor.b),
      })
      cell.setShd(shd)
    }
    let border = parseBorder(
      calcedStyle,
      node,
      'left',
      'Left',
      isAddIfNull,
      true
    )
    if (null != border) cell.setBorder(border, CellBorderType.Left)

    border = parseBorder(calcedStyle, node, 'top', 'Top', isAddIfNull, true)
    if (null != border) cell.setBorder(border, CellBorderType.Top)

    border = parseBorder(calcedStyle, node, 'right', 'Right', isAddIfNull, true)
    if (null != border) cell.setBorder(border, CellBorderType.Right)

    border = parseBorder(
      calcedStyle,
      node,
      'bottom',
      'Bottom',
      isAddIfNull,
      true
    )
    if (null != border) cell.setBorder(border, CellBorderType.Bottom)
    const topV = getStyleValue(node, calcedStyle, 'padding-top')
    const top = stringValToMM(topV)
    if (null != top) {
      cell.setMargins({
        margin: { w: top, type: TableLengthUnitType.MM },
        type: SetCellMarginType.Top,
      })
    }

    const rightV = getStyleValue(node, calcedStyle, 'padding-right')
    const right = stringValToMM(rightV)
    if (null != right) {
      cell.setMargins({
        margin: { w: right, type: TableLengthUnitType.MM },
        type: SetCellMarginType.Right,
      })
    }

    const bottomV = getStyleValue(node, calcedStyle, 'padding-bottom')
    const bottom = stringValToMM(bottomV)
    if (null != bottom) {
      cell.setMargins({
        margin: { w: bottom, type: TableLengthUnitType.MM },
        type: SetCellMarginType.Bootom,
      })
    }

    const leftV = getStyleValue(node, calcedStyle, 'padding-left')
    const left = stringValToMM(leftV)
    if (null != left) {
      cell.setMargins({
        margin: { w: left, type: TableLengthUnitType.MM },
        type: SetCellMarginType.Left,
      })
    }
  }

  _parseTextPr(node: HTMLElement) {
    const rPr = TextPr.new({
      bold: false,
      italic: false,
      underline: false,
      strikeout: false,
      rFonts: {
        ascii: {
          name: 'Arial',
          index: -1,
        },
        eastAsia: {
          name: 'Arial',
          index: -1,
        },
        hAnsi: {
          name: 'Arial',
          index: -1,
        },
        cs: {
          name: 'Arial',
          index: -1,
        },
      },
      fontSize: 11,
      fillEffects: FillEffects.SolidRGBA(0, 0, 0),
      vertAlign: VertAlignType.Baseline,
      highLight: HightLight_None,
    })
    const calcedStyle = getCalcedStyle(node)
    if (calcedStyle) {
      const fontFamily = getDefaultFontFamily(
        getStyleValue(node, calcedStyle, 'font-family')
      )
      if (fontFamily && '' !== fontFamily) {
        const fontItem = this.fonts[fontFamily]
        if (null != fontItem && null != fontItem.name) {
          const name = fontItem.name
          rPr.rFonts.ascii = { name, index: fontItem.index }
          rPr.rFonts.hAnsi = { name, index: fontItem.index }
          rPr.rFonts.cs = { name, index: fontItem.index }
          rPr.rFonts.eastAsia = { name, index: fontItem.index }
        }
      }
      let fontSizeV: Nullable<string> = node.style ? node.style.fontSize : null
      if (!fontSizeV) {
        fontSizeV = getStyleValue(node, calcedStyle, 'font-size')
      }
      fontSizeV = getDefaultFontSize(fontSizeV)
      if (fontSizeV != null) {
        const obj = convertUnitValStrToMMWithUnit(fontSizeV)
        if (obj && '%' !== obj.type && 'none' !== obj.type) {
          let fontSize = obj.val
          fontSize = Math.round(2 * fontSize * Factor_mm_to_pt) / 2

          //TODO use constant
          if (fontSize > 300) fontSize = 300
          else if (fontSize === 0) fontSize = 1

          rPr.fontSize = fontSize
        }
      }

      const fontWeight = getStyleValue(node, calcedStyle, 'font-weight')
      if (fontWeight) {
        if (
          'bold' === fontWeight ||
          'bolder' === fontWeight ||
          400 < Number(fontWeight)
        ) {
          rPr.bold = true
        }
      }

      const fontStyle = getStyleValue(node, calcedStyle, 'font-style')
      if ('italic' === fontStyle) rPr.italic = true

      const colorV = getStyleValue(node, calcedStyle, 'color')
      const color = parseHexColorFromString(colorV)
      if (color) {
        if (color) {
          rPr.fillEffects = FillEffects.SolidRGBA(color.r, color.g, color.b)
        }
      }

      const spacingV = getStyleValue(node, calcedStyle, 'letter-spacing')
      const spacing = stringValToMM(spacingV)
      if (spacing != null) {
        rPr.spacing = spacing
      }

      let bgColorV: Nullable<CHexColor> = null
      let underlineV: null | boolean = null
      let strikeoutV: null | boolean = null
      let vAlignV: string | null = null
      let _node: Nullable<HTMLElement> = node

      while (_node) {
        const calcedStyle = getCalcedStyle(_node)
        if (null == calcedStyle) break
        if (null == underlineV || null == strikeoutV) {
          const textDecoration = getStyleValue(
            node,
            calcedStyle,
            'text-decoration'
          )
          if (textDecoration) {
            if (-1 !== textDecoration.indexOf('underline')) underlineV = true
            else if (
              -1 !== textDecoration.indexOf('none') &&
              node.parentElement &&
              node.parentElement.nodeName.toLowerCase() === 'a'
            ) {
              underlineV = false
            }

            if (-1 !== textDecoration.indexOf('line-through')) strikeoutV = true
          }
        }

        if (null == bgColorV) {
          const bgColor = getStyleValue(node, calcedStyle, 'background-color')
          bgColorV = parseHexColorFromString(bgColor)
        }

        if (null == vAlignV || 'baseline' === vAlignV) {
          vAlignV = getStyleValue(node, calcedStyle, 'vertical-align')
          if (!vAlignV) vAlignV = null
        }
        if (vAlignV && bgColorV && strikeoutV && underlineV) break

        _node = _node.parentNode as Nullable<HTMLElement>
        if (
          !_node ||
          this.rootNode === _node ||
          'body' === _node.nodeName.toLowerCase() ||
          true === isBlockHtmlTag(_node.nodeName.toLowerCase())
        ) {
          break
        }
      }

      if (bgColorV != null) {
        const complexColor = CComplexColor.RGBA(
          bgColorV.r,
          bgColorV.g,
          bgColorV.b
        )
        rPr.highLight = complexColor
      }
      if (null != underlineV) rPr.underline = underlineV
      if (null != strikeoutV) rPr.strikeout = strikeoutV
      switch (vAlignV) {
        case 'sub':
          rPr.vertAlign = VertAlignType.SubScript
          break
        case 'super':
          rPr.vertAlign = VertAlignType.SuperScript
          break
      }
    }
    return rPr
  }
}
