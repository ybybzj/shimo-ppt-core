import { Nullable } from '../../../../liber/pervasive'
import { FillEffects } from '../../../core/SlideElement/attrs/fill/fill'
import { UniNvPr } from '../../../core/SlideElement/attrs/shapePrs'
import { GraphicFrame } from '../../../core/SlideElement/GraphicFrame'
import { Table } from '../../../core/Table/Table'
import { TableCellBorder } from '../../../core/Table/attrs/CellBorder'
import { stringValToMM, parseHexColorFromString } from '../../utils'
import { TCBorderValue } from '../../../core/common/const/table'

export const parseBorder = (
  calcedStyle: Nullable<CSSStyleDeclaration>,
  node: HTMLTableElement | HTMLTableCellElement,
  borderStyleStr: string,
  widthStyleStr: string,
  isAddIfNull: boolean,
  isSetFillEffects?: boolean
) => {
  let res: TableCellBorder | null = null
  const style = getStyleValue(
    node,
    calcedStyle,
    'border-' + borderStyleStr + '-style'
  )
  if (null != style) {
    res = TableCellBorder.new()
    if ('none' === style || '' === style) res.value = TCBorderValue.None
    else {
      res.value = TCBorderValue.Single
      let width = node.style['border' + widthStyleStr + 'Width']
      if (!width) {
        width = getStyleValue(
          node,
          calcedStyle,
          'border-' + borderStyleStr + '-width'
        )
      }

      if (null != width) {
        const widthVal = stringValToMM(width)
        if (widthVal != null) {
          res.size = widthVal
        }
      }

      const color = getStyleValue(
        node,
        calcedStyle,
        'border-' + borderStyleStr + '-color'
      )

      const hexColor = parseHexColorFromString(color)
      if (hexColor) {
        if (isSetFillEffects) {
          res.fillEffects = FillEffects.SolidRGBA(
            hexColor.r,
            hexColor.g,
            hexColor.b
          )
        } else {
          res.color = hexColor
        }
      }
    }
  }

  if (isAddIfNull && null == res) res = TableCellBorder.new()
  return res
}

export const getCalcedStyle = (
  node: HTMLElement
): Nullable<CSSStyleDeclaration> => {
  if (null != node && Node.ELEMENT_NODE === node.nodeType) {
    const defaultView = node.ownerDocument?.defaultView
    return defaultView?.getComputedStyle(node, null)
  }
}

export const getStyleValue = (
  node: HTMLElement,
  calcedStyle: Nullable<CSSStyleDeclaration>,
  prop: string
) => {
  const getVal = () => {
    let ret: null | string = null
    if (calcedStyle) {
      ret = calcedStyle.getPropertyValue(prop)
    }
    if (!ret) {
      if (node && (node as any).currentStyle) {
        ret = (node as any).currentStyle[prop]
      }
    }
    return ret
  }

  let res = getVal()
  const nodeName = node.nodeName.toLowerCase()
  if (!res) {
    switch (prop) {
      case 'font-family': {
        res = (node as any).fontFamily
        break
      }
      case 'background-color': {
        res = node.style.backgroundColor
        if ('td' === nodeName && !res) {
          res = (node as HTMLTableCellElement).bgColor
        }
        break
      }
      case 'font-weight': {
        res = node.style.fontWeight
      }
    }
  }

  return res
}

export const parseGenericFont = (name: string) => {
  const index = name.indexOf(',')
  let fontName = -1 !== index ? name.substring(0, index) : name
  fontName = fontName.replace(/^[\s'"]+|[\s'"]+$/g, '')
  if (!fontName) {
    fontName = 'Arial'
  } else {
    switch (fontName.toLowerCase()) {
      case 'serif':
        fontName = 'Times New Roman'
        break
      case 'sans-serif':
        fontName = 'Arial'
        break
      case 'cursive':
        fontName = 'Comic Sans MS'
        break
      case 'fantasy':
        fontName = 'Impact'
        break
      case 'monospace':
        fontName = 'Courier New'
        break
    }
  }
  return fontName
}

export const isBlockHtmlTag = (name: string) => {
  if (
    'p' === name ||
    'div' === name ||
    'ul' === name ||
    'ol' === name ||
    'li' === name ||
    'table' === name ||
    'tbody' === name ||
    'tr' === name ||
    'td' === name ||
    'th' === name ||
    'h1' === name ||
    'h2' === name ||
    'h3' === name ||
    'h4' === name ||
    'h5' === name ||
    'h6' === name ||
    'center' === name ||
    'dd' === name ||
    'dt' === name
  ) {
    return true
  }
  return false
}

export const createTableByGrid = (grid: number[]) => {
  const graphicFrame = new GraphicFrame()
  const table = new Table(graphicFrame, 0, 0, grid)
  graphicFrame.setGraphicObject(table)
  graphicFrame.setNvSpPr(new UniNvPr(graphicFrame))
  return table
}
