import {
  ActionHookArg,
  Hook,
  HookArg,
  HookHandler,
  ReducableHookArg,
  ReturnableHookArg,
} from './type'
import { Dict, Nullable } from '../../../liber/pervasive'

export class HookManager {
  private registry: Dict<Array<HookHandler<HookArg<any, any>, any>>> = {}

  public register<P, R, Arg extends HookArg<P, R>>(
    hook: Hook<P, R, Arg>,
    handler: (arg: Arg) => R,
    priority = 5000
  ): void {
    if (!this.registry.hasOwnProperty(hook)) {
      this.registry[hook] = []
    }
    const handlers = this.registry[hook] as Array<HookHandler<Arg, R>>
    const handlerObj = {
      handler,
      priority,
    }
    let idx = 0
    for (let i = 0, l = handlers.length; i < l; i++) {
      idx = i
      if (handlers[i].priority > handlerObj.priority) {
        break
      }
    }

    handlers.splice(idx, 0, handlerObj)
  }

  public unregister<P, R, Arg extends HookArg<P, R>>(
    hook: Hook<P, R, Arg>,
    handler?: (arg: Arg) => R
  ): void {
    if (!this.registry.hasOwnProperty(hook)) {
      return
    }

    const handlers = this.registry[hook] as Array<HookHandler<Arg, R>>

    if (handler) {
      for (let i = handlers.length - 1; i >= 0; --i) {
        if (handlers[i].handler === handler) handlers.splice(i, 1)
      }
      if (handlers.length <= 0) {
        delete this.registry[hook]
      }
    } else {
      delete this.registry[hook]
    }
  }

  public isListening(hookName: string): boolean {
    const handlers = this.registry[hookName]
    return handlers && handlers.length > 0
  }

  public invoke(hook: Hook<void, any, ActionHookArg<void>>): void
  public invoke<P>(hook: Hook<P, any, ActionHookArg<P>>, args: P): void
  public invoke(hook: Hook<any, any, ActionHookArg<any>>, args?: any): void {
    const handlers = this.registry[hook] as Array<
      HookHandler<ActionHookArg<any>, void>
    >
    if (!handlers || handlers.length <= 0) {
      return
    }

    handlers.forEach((handler) => {
      handler.handler({ payload: args })
    })
  }

  public get<R>(hook: Hook<void, R, ReturnableHookArg<void, R>>): Nullable<R>
  public get<P, R>(
    hook: Hook<P, R, ReturnableHookArg<P, R>>,
    args?: P
  ): Nullable<R>
  public get<R>(
    hook: Hook<any, R, ReturnableHookArg<any, R>>,
    args?: any
  ): Nullable<R> {
    const handlers = this.registry[hook] as Array<
      HookHandler<ReturnableHookArg<any, R>, R>
    >
    if (!handlers || handlers.length <= 0) {
      return undefined
    }

    const handlerArgs: ReturnableHookArg<any, R> = {
      handled: false,
      return: undefined,
      payload: args,
    }

    for (let i = 0, l = handlers.length; i < l; i++) {
      const handler = handlers[i]
      handlerArgs.return = undefined
      const result = handler.handler(handlerArgs)
      if (handlerArgs.return === undefined) {
        handlerArgs.return = result
      }

      if (handlerArgs.handled === true) {
        break
      }
    }

    return handlerArgs.return
  }

  public reduce<R, ReduceResult>(
    hook: Hook<void, R, ReducableHookArg<void, R>>,
    reducer: (m: ReduceResult, r: R) => ReduceResult,
    init: ReduceResult
  ): ReduceResult
  public reduce<P, R, ReduceResult>(
    hook: Hook<P, R, ReducableHookArg<P, R>>,
    reducer: (m: ReduceResult, r: R) => ReduceResult,
    init: ReduceResult,
    args: P
  ): ReduceResult
  public reduce<R, ReduceResult>(
    hook: Hook<any, R, ReducableHookArg<any, R>>,
    reducer: (m: ReduceResult, r: R) => ReduceResult,
    init: ReduceResult,
    args?: any
  ): ReduceResult {
    const handlers = this.registry[hook] as Array<
      HookHandler<ReducableHookArg<any, R>, R>
    >
    if (!handlers || handlers.length <= 0) {
      return init
    }
    const hanlderArg: ReducableHookArg<any, R> = {
      return: undefined,
      payload: args,
    }
    return handlers.reduce((m, handler) => {
      hanlderArg.return = undefined
      const r = handler.handler(hanlderArg)
      if (hanlderArg.return === undefined) {
        hanlderArg.return = r
      }

      return reducer(m, hanlderArg.return)
    }, init)
  }
}
