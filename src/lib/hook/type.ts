// tslint:disable: interface-name

import { Nullable } from '../../../liber/pervasive'

/**
 * A hook is comprised of :
 *  - name
 *  - category it's belong to
 *  - arguments that come with
 *  - maybe a reducer function to collect the results of all the handlers
 *
 * A hook handler is comprised of:
 *  - priority that determines the order of invocation
 *  - the handle logic itself with the related args passed in
 *
 *
 * Categories of hooks(distinguished by the behaviour of invocation in terms of its handlers):
 *    action - handlers always be executed according to the priority, the excution result is ignored
 *    returnable - handlers will be executed according to the priority, until the previous execution is marked as handled,
 *          then the result of that executed will be returned after this hook invocation.
 *    reducable - all handlers will be executed according to the priority, and the result of execution will be collected by the reducer of the invoked hook.
 */

export interface ActionHookArg<P> {
  readonly payload: P
}

export interface ReturnableHookArg<P, R> {
  readonly payload: P
  handled: boolean
  return: Nullable<R>
}

export interface ReducableHookArg<P, R> {
  readonly payload: P
  return: Nullable<R>
}

export type HookArg<P, R> =
  | ActionHookArg<P>
  | ReturnableHookArg<P, R>
  | ReducableHookArg<P, R>

export interface HookHandler<Arg, R> {
  priority: number
  handler: (arg: Arg) => R
}

export type Hook<P, R, Arg extends HookArg<P, R>> = string & { _ARG_TYPE_: Arg }

export function createActionHook<P>(
  name: string
): Hook<P, any, ActionHookArg<P>> {
  return name as Hook<P, any, ActionHookArg<P>>
}

export function createReturnableHook<P, R>(
  name: string
): Hook<P, R, ReturnableHookArg<P, R>> {
  return name as Hook<P, R, ReturnableHookArg<P, R>>
}

export function createReducableHook<P, R>(
  name: string
): Hook<P, R, ReducableHookArg<P, R>> {
  return name as Hook<P, R, ReducableHookArg<P, R>>
}
