import { EditorSettings } from '../../core/common/EditorSettings'

const isInWindow =
  typeof window !== 'undefined' && typeof window['location'] !== 'undefined'
const isInDebug = (() => {
  const searchParams = location?.search ?? ''
  return searchParams.startsWith('?dbg=1')
})()

export function checkIsInDebug() {
  if (EditorSettings.isInDebug) return true
  if (!isInWindow) return false
  return isInDebug
}

function log(...args: any) {
  if (checkIsInDebug()) {
    console.log(...args)
  }
}

function warn(...args: any) {
  if (checkIsInDebug()) {
    console.warn(...args)
  }
}
function error(...args: any) {
  if (checkIsInDebug()) {
    console.error(...args)
  }
}
function info(...args: any) {
  if (checkIsInDebug()) {
    console.info(...args)
  }
}
function time(...args: any) {
  if (checkIsInDebug()) {
    console.time(...args)
  }
}
function timeEnd(...args: any) {
  if (checkIsInDebug()) {
    console.timeEnd(...args)
  }
}

export const logger = {
  log,
  warn,
  error,
  info,
  time,
  timeEnd,
}
