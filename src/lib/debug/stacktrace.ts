import { Nullable } from '../../../liber/pervasive'
import { logger } from './log'

function getErrorWithStack(): Error {
  try {
    // Error must be thrown to get stack in IE
    throw new Error()
  } catch (e) {
    return e
  }
}

const maxStackSize = 10
function generateBacktrace(): StackInfo[] {
  const stack: StackInfo[] = []

  // tslint:disable-next-line: no-arg
  let curr = arguments.callee
  // tslint:disable-next-line: no-string-literal
  while (curr && stack.length < maxStackSize && curr['arguments']) {
    // Allow V8 optimizations
    const args = new Array(curr.arguments.length)
    for (let i = 0; i < args.length; ++i) {
      args[i] = curr.arguments[i]
    }
    if (/function(?:\s+([\w$]+))+\s*\(/.test(curr.toString())) {
      stack.push({ functionName: RegExp.$1 || undefined, args })
    } else {
      stack.push({ args })
    }

    try {
      curr = curr.caller
    } catch (e) {
      break
    }
  }
  return stack
}

interface StackInfo {
  functionName?: string
  fileName?: string
  lineNumber?: string
  columnCount?: string
  source?: string
  args?: string[]
}

function extractLocation(urlLike: Nullable<string>): Array<string | undefined> {
  if (urlLike == null) {
    return []
  }
  // Fail-fast but return locations like "(native)"
  if (urlLike.indexOf(':') === -1) {
    return [urlLike]
  }

  const regExp = /(.+?)(?:\:(\d+))?(?:\:(\d+))?$/
  const parts = regExp.exec(urlLike.replace(/[\(\)]/g, ''))!
  return [parts[1], parts[2] || undefined, parts[3] || undefined]
}

const CHROME_IE_STACK_REGEXP = /^\s*at .*(\S+\:\d+|\(native\))/m
function parseStack(error: Error): StackInfo[] {
  const filtered = error
    .stack!.split('\n')
    .filter((line) => !!line.match(CHROME_IE_STACK_REGEXP))

  return filtered.map((line) => {
    if (line.indexOf('(eval ') > -1) {
      // Throw away eval information until we implement stacktrace.js/stackframe#8
      line = line
        .replace(/eval code/g, 'eval')
        .replace(/(\(eval at [^\()]*)|(\)\,.*$)/g, '')
    }
    let sanitizedLine = line.replace(/^\s+/, '').replace(/\(eval code/g, '(')

    // capture and preseve the parenthesized location "(/foo/my bar.js:12:87)" in
    // case it has spaces in it, as the string is split on \s+ later on
    const location = sanitizedLine.match(/ (\((.+):(\d+):(\d+)\)$)/)

    // remove the parenthesized location from the line, if it was matched
    sanitizedLine = location
      ? sanitizedLine.replace(location[0], '')
      : sanitizedLine

    const tokens = sanitizedLine.split(/\s+/).slice(1)
    // if a location was matched, pass it to extractLocation() otherwise pop the last token
    const locationParts = extractLocation(location ? location[1] : tokens.pop())
    const functionName = tokens.join(' ') || undefined
    const fileName =
      ['eval', '<anonymous>'].indexOf(locationParts[0]!) > -1
        ? undefined
        : locationParts[0]

    return {
      functionName,
      fileName,
      lineNumber: locationParts[1],
      columnCount: locationParts[2],
      source: line,
    }
  })
}

function getStackInfos(): StackInfo[] {
  const err = getErrorWithStack()
  if (err && typeof err.stack === 'string') {
    return parseStack(err)
  } else {
    return generateBacktrace()
  }
}

export const TracingStack = {
  get() {
    return getStackInfos().filter(
      (st) => st.fileName!.indexOf('debug/stacktrace') <= -1
    )
  },
  log(stackInfos?: StackInfo[]) {
    if (stackInfos) {
      logger.log('===== stacktrace info ====>')
      stackInfos.forEach((st) => {
        console.info(st.source)
      })
      logger.log('<===== stacktrace info ====\n')
    } else {
      console.trace()
    }
  },
}
