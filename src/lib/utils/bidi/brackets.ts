import { BIDI_Brackets } from './data/bidiBrackets.data'
import { parseCharacterMap } from './util/parseCharacterMap'

let openToClose: Map<string, string>,
  closeToOpen: Map<string, string>,
  canonical: Map<string, string>

function parse() {
  if (!openToClose) {
    //const start = performance.now()
    const { map, reverseMap } = parseCharacterMap(BIDI_Brackets.pairs, true)
    openToClose = map
    closeToOpen = reverseMap!
    canonical = parseCharacterMap(BIDI_Brackets.canonical, false).map
    //console.log(`brackets parsed in ${performance.now() - start}ms`)
  }
}

export function openingToClosingBracket(char: string) {
  parse()
  return openToClose.get(char) || null
}

export function closingToOpeningBracket(char: string) {
  parse()
  return closeToOpen.get(char) || null
}

export function getCanonicalBracket(char: string) {
  parse()
  return canonical.get(char) || null
}
