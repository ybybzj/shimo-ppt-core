import { Opaque } from '../../../../liber/pervasive'

export type BidiItem<T> = Opaque<'BidiItem', T>
export type LetterInfoGetter<T> = (item: BidiItem<T>) => {
  charCode: number
  char: string
}
