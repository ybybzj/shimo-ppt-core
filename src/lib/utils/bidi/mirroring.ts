import { Nullable } from '../../../../liber/pervasive'
import { BIDI_Mirroring } from './data/bidiMirroring.data'
import { EmbeddingLevelsInfo } from './embeddingLevels'
import { BidiItem, LetterInfoGetter } from './type'
import { parseCharacterMap } from './util/parseCharacterMap'

let mirrorMap: Map<string, string>

function parse() {
  if (!mirrorMap) {
    //const start = performance.now()
    const { map, reverseMap } = parseCharacterMap(BIDI_Mirroring, true)
    // Combine both maps into one
    reverseMap!.forEach((value, key) => {
      map.set(key, value)
    })
    mirrorMap = map
    //console.log(`mirrored chars parsed in ${performance.now() - start}ms`)
  }
}

export function getMirroredCharacter(char: string) {
  parse()
  return mirrorMap.get(char) || null
}

/**
 * Given a string and its resolved embedding levels, build a map of indices to replacement chars
 * for any characters in right-to-left segments that have defined mirrored characters.
 * @param input
 * @param embeddingLevels
 * @param [start]
 * @param [end]
 * @return {Map<number, string>}
 */
export function getMirroredCharactersMap<T>(
  input: Array<BidiItem<T>>,
  getter: LetterInfoGetter<T>,
  embeddingLevels: EmbeddingLevelsInfo,
  start: Nullable<number>,
  end: Nullable<number>
) {
  const strLen = input.length
  start = Math.max(0, start == null ? 0 : +start)
  end = Math.min(strLen - 1, end == null ? strLen - 1 : +end)

  const map = new Map<number, string>()
  for (let i = start; i <= end; i++) {
    if (embeddingLevels[i] & 1) {
      //only odd (rtl) levels
      const mirror = getMirroredCharacter(getter(input[i]).char)
      if (mirror !== null) {
        map.set(i, mirror)
      }
    }
  }
  return map
}
