import { Dict } from '../../../../liber/pervasive'
import { BIDI_CharTypes } from './data/bidiCharTypes.data'

export const TYPES: Dict<number> = {}
export const TYPES_TO_NAMES: Dict<string> = {}
TYPES.L = 1 //L is the default
TYPES_TO_NAMES[1] = 'L'

const charTypesKeys = Object.keys(BIDI_CharTypes) as Array<
  keyof typeof BIDI_CharTypes
>
charTypesKeys.forEach((type, i) => {
  TYPES[type] = 1 << (i + 1)
  TYPES_TO_NAMES[TYPES[type]] = type
})
Object.freeze(TYPES)

export const ISOLATE_INIT_TYPES = TYPES.LRI | TYPES.RLI | TYPES.FSI
export const STRONG_TYPES = TYPES.L | TYPES.R | TYPES.AL
export const NEUTRAL_ISOLATE_TYPES =
  TYPES.B |
  TYPES.S |
  TYPES.WS |
  TYPES.ON |
  TYPES.FSI |
  TYPES.LRI |
  TYPES.RLI |
  TYPES.PDI
export const BN_LIKE_TYPES =
  TYPES.BN | TYPES.RLE | TYPES.LRE | TYPES.RLO | TYPES.LRO | TYPES.PDF
export const TRAILING_TYPES =
  TYPES.S | TYPES.WS | TYPES.B | ISOLATE_INIT_TYPES | TYPES.PDI | BN_LIKE_TYPES

let map: Map<number, number>

function parseData() {
  if (!map) {
    //const start = performance.now()
    map = new Map()
    charTypesKeys.forEach((type) => {
      let lastCode = 0
      BIDI_CharTypes[type].split(',').forEach((range) => {
        const [skip, step] = range.split('+')
        const skip_n = parseInt(skip, 36)
        const step_n = step ? parseInt(step, 36) : 0
        map.set((lastCode += skip_n), TYPES[type])
        for (let i = 0; i < step_n; i++) {
          map.set(++lastCode, TYPES[type])
        }
      })
    })

    //console.log(`char types parsed in ${performance.now() - start}ms`)
  }
}

/**
 * @param {string} char
 * @return {number}
 */
export function getBidiCharType(char: string) {
  parseData()
  return map.get(char.codePointAt(0)!) || TYPES.L
}

export function getBidiCharTypeName(char: string) {
  return TYPES_TO_NAMES[getBidiCharType(char)]
}
