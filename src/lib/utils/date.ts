import { format as DateFormatter } from './dateTimeFormat/formatDateTime'
import { locale as enUS } from './dateTimeFormat/locale/en-US/locale'
import { locale as arSA } from './dateTimeFormat/locale/ar-SA/locale'
import { locale as ja } from './dateTimeFormat/locale/ja-JP/locale'
import { locale as zhCN } from './dateTimeFormat/locale/zh-CN/locale'

export const formatDateTimeFormat = (
  date: number | Date,
  format: string,
  locale: string
) => {
  const localeMap = {
    'en-US': enUS,
    'zh-CN': zhCN,
    'ja-JP': ja,
    'ar-SA': arSA,
  }
  return DateFormatter(date, format, {
    locale: localeMap[locale],
  })
}
