type EndExecutable = (cb?: () => void) => boolean
export function endExec(fn: () => void): EndExecutable {
  let isExecute = true
  return (cb?: () => void) => {
    if (cb == null) {
      if (isExecute === true) {
        fn()
        return true
      } else {
        return false
      }
    }
    isExecute = false
    cb()
    isExecute = true
    fn()
    return true
  }
}
