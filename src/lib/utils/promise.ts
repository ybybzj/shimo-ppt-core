interface PRPair<T> {
  p: Promise<T>
  resolved: boolean
  result?: T
}

export function useLatest<T = any>(
  promiseMaker: (...args: any[]) => Promise<T>
): (...args: any[]) => Promise<T> {
  let latest: PRPair<T>

  let resolvedCB

  const finalPromise = new Promise<T>((resolve) => {
    resolvedCB = () => {
      if (latest && latest.resolved === true) {
        resolve(latest.result!)
      }
    }
  })
  return (...args: any[]) => {
    const pair: PRPair<T> = {
      p: promiseMaker(...args),
      resolved: false,
    }

    latest = pair

    pair.p.then((result) => {
      pair.resolved = true
      pair.result = result
      if (typeof resolvedCB === 'function') {
        resolvedCB()
      }
    })

    return finalPromise
  }
}
