import { strHash32 } from '../../../re/Native'

export function strHash(str: string): string {
  const hash = strHash32(str, 5381)
  /* JavaScript does bitwise operations (like XOR, above) on 32-bit signed
   * integers. Since we want the results to be always positive, convert the
   * signed int to an unsigned by doing an unsigned bitshift. */
  return (hash >>> 0).toString(36)
}

class StrHasher {
  private _seed: number = 0
  private _hash: number
  constructor(seed?: number) {
    if (seed != null) {
      this._seed = seed
    }
    this._hash = this._seed | 0
  }
  update(str: string) {
    for (let i = 0, l = str.length; i < l; i++) {
      this._hash = (this._hash << 5) - this._hash + str.charCodeAt(i)
      this._hash |= 0 // Convert to 32bit integer
    }
  }
  digest() {
    const result = this._hash
    this.seed(0)
    return result
  }
  seed(seed?: number) {
    if (seed != null) {
      this._seed = seed
    }

    this._hash = this._seed | 0
  }
}

export const strHasher = new StrHasher()
