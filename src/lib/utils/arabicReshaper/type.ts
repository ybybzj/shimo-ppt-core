import { Opaque } from '../../../../liber/pervasive'
import { LetterStatusCode } from './statusCode'

export type ArabicReshapeItem<T> = Opaque<'ArabicReshapeItem', T>
export type LetterStatusGetter<T> = (item: ArabicReshapeItem<T>) => {
  statusCode: LetterStatusCode
  charCode: number
}

export type LetterStatusUpdater<T> = (
  item: ArabicReshapeItem<T>,
  statusCode: LetterStatusCode,
  formCode?: number
) => void
