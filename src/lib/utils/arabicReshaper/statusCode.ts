import { Nullable, Opaque } from '../../../../liber/pervasive'
import {
  ArabicLetterForms,
  formOf,
  FORM_IDX,
  getCharFormIndex,
  isHARAKAT,
  LetterFormsMap,
} from './letters'

/** type LetterStatusCode
 *
 *  each letter has a status code encoded by these pieces of information below:
 *  1) extra info like is need to be skipped for typesetting
 *  2) position index of LetterFormsMap for lookup form info
 *  3) index of form kind, refer to *type FORM_IDX*
 *
 *  status code is a 12 bit binary, first 2 bits contains extra info(right now  "skipped" & "isharakat"), next 8 bits stands for position index of LetterFormsMap,
 *  and the rest 2 bits is the value of index of form kind, ex.
 *      0      0      |0110,         0111            |11,
 *      ^      ^         ^             ^               ^
 *      |      |         |             |               |
 * skipped isharakat tens digits  single digits     form_index
 *  stands for donot skipped, 67 index in LetterFormsMap, form of "final(3)"
 *
 * if all bits are zero like 000000000000, means to treat it as a regular letter
 */
export type LetterStatusCode = Opaque<'LetterStatusCode', number>

/**
 *   LetterStatusCode utilities
 */

const UNSET_SKIPPED = 0b011111111111
const UNSET_ISHARAKAT = 0b101111111111
const MAP_INDEX_MASK = 0b001111111100
const UNSET_MAP_INDEX_MASK = 0b110000000011
const FORM_MASK = 0b000000000011
const UNSET_FORM_MASK = 0b111111111100

export const INIT_STATUS_CODE = 0 as LetterStatusCode

const INFO_SKIPPED = 0
const INFO_ISHARAKAT = 1
const INFO_MAP_INDEX = 2
const INFO_FORM = 3

function getInfoBits(
  statusCode: LetterStatusCode,
  type: 0 | 1 | 2 | 3
): number {
  const code = (statusCode as number) & 0b111111111111

  switch (type) {
    case INFO_SKIPPED:
      return code >> 11
    case INFO_ISHARAKAT:
      return (code >> 10) & 0b01
    case INFO_MAP_INDEX:
      return (code & MAP_INDEX_MASK) >> 2
    case INFO_FORM:
      return code & FORM_MASK
  }
}

function encodeLetterFormsMapIndex(
  code: LetterStatusCode,
  index: number
): LetterStatusCode {
  const sd = (index | 0) % (10 | 0)
  const td = ((index | 0) / 10) | 0
  const encoded_index = ((td << 4) + sd) << 2
  return ((code & UNSET_MAP_INDEX_MASK) | encoded_index) as LetterStatusCode
}

function decodeLetterFormsMapIndex(code: LetterStatusCode): number {
  const index_bits = getInfoBits(code, INFO_MAP_INDEX)
  const sd = (index_bits | 0) & 0b00001111
  const td = (index_bits | 0) >> 4
  return td * 10 + sd
}

export function isArabicLetter(statusCode: LetterStatusCode) {
  return (statusCode & UNSET_SKIPPED) > INIT_STATUS_CODE
}

export function isSkippedLetter(statusCode: LetterStatusCode) {
  return getInfoBits(statusCode, INFO_SKIPPED) > 0
}

export function isHarakatLetter(statusCode: LetterStatusCode) {
  return getInfoBits(statusCode, INFO_ISHARAKAT) > 0
}

export function getLetterFormByStatusCode(
  statusCode: LetterStatusCode
): Nullable<ArabicLetterForms> {
  if (statusCode <= INIT_STATUS_CODE) {
    return undefined
  }
  const indexOfMap = decodeLetterFormsMapIndex(statusCode)
  // const form_idx = getInfoBits(statusCode, INFO_FORM) as FORM_IDX

  const form = LetterFormsMap[indexOfMap]

  return form
}

export function getLetterFormKindByStatusCode(statusCode: LetterStatusCode) {
  return getInfoBits(statusCode, INFO_FORM) as FORM_IDX
}

export function updateFormIndex(
  statusCode: LetterStatusCode,
  form_idx: FORM_IDX
): LetterStatusInfo {
  const indexOfMap = decodeLetterFormsMapIndex(statusCode)
  const form = LetterFormsMap[indexOfMap]
  const formCode = form ? formOf(form, form_idx) : undefined
  const updatedCode = ((statusCode & UNSET_FORM_MASK) |
    form_idx) as LetterStatusCode
  return {
    statusCode: updatedCode,
    formCode: formCode == null ? undefined : formCode,
  }
}

export function updateSkippedStatus(
  statusCode: LetterStatusCode,
  isSkipped: boolean
): LetterStatusCode {
  return ((statusCode & UNSET_SKIPPED) |
    ((isSkipped ? 0b1 : 0b0) << 11)) as LetterStatusCode
}

export function updateHarakatStatus(
  statusCode: LetterStatusCode,
  isHarakat: boolean
): LetterStatusCode {
  return ((statusCode & UNSET_ISHARAKAT) |
    ((isHarakat ? 0b1 : 0b0) << 10)) as LetterStatusCode
}

export interface LetterStatusInfo {
  statusCode: LetterStatusCode
  formCode?: number
}
export function genLetterStatusInfo(letter: number): LetterStatusInfo {
  let statusCode = INIT_STATUS_CODE
  const isHarakat = isHARAKAT(letter)
  if (isHarakat === true) {
    statusCode = updateHarakatStatus(statusCode, true)
  } else {
    const indexOfMap = getCharFormIndex(letter)
    if (indexOfMap != null) {
      statusCode = encodeLetterFormsMapIndex(statusCode, indexOfMap)
    }
  }

  return {
    statusCode,
  }
}
