import { Nullable } from '../../../../liber/pervasive'
import { ManagedSlice } from '../../../common/managedArray'
import {
  ArabicLetterForms,
  FINAL_IDX,
  formOf,
  FORM_IDX,
  INITIAL_IDX,
  ISOLATED_IDX,
  MEDIAL_IDX,
} from './letters'
import {
  getLetterFormKindByStatusCode,
  // INIT_STATUS_CODE,
  isHarakatLetter,
  updateSkippedStatus,
} from './statusCode'
import {
  ArabicReshapeItem,
  LetterStatusGetter,
  LetterStatusUpdater,
} from './type'

import { bsearch } from './utils'

const ligature_mapping_first_letters = [
  0x0623, 0x0626, 0x0627, 0x0628, 0x062a, 0x062b, 0x062c, 0x062d, 0x062e,
  0x0630, 0x0631, 0x0633, 0x0634, 0x0635, 0x0636, 0x0637, 0x0638, 0x0639,
  0x063a, 0x0640, 0x0641, 0x0642, 0x0643, 0x0644, 0x0645, 0x0646, 0x0647,
  0x0648, 0x0649, 0x064a,
]

type letter = number
type ligature_pattern = letter[]

type ligature_map = [ligature_pattern, ArabicLetterForms]

const ligatures_mapping: ligature_map[][] = [
  [
    [
      [0x0623, 0x0643, 0x0628, 0x0631],
      [0xfdf3, 0, 0, 0],
    ],
  ],
  [
    [
      [0x0626, 0x0649],
      [0xfbf9, 0xfbfb, 0, 0xfbfa],
    ],
    [
      [0x0626, 0x06d5],
      [0xfbec, 0, 0, 0xfbed],
    ],
    [
      [0x0626, 0x0627],
      [0xfbea, 0, 0, 0xfbeb],
    ],
    [
      [0x0626, 0x0649],
      [0xfc03, 0, 0, 0xfc68],
    ],
    [
      [0x0626, 0x06d0],
      [0xfbf6, 0xfbf8, 0, 0xfbf7],
    ],
    [
      [0x0626, 0x062d],
      [0xfc01, 0xfc98, 0, 0],
    ],
    [
      [0x0626, 0x0647],
      [0, 0xfc9b, 0xfce0, 0],
    ],
    [
      [0x0626, 0x062c],
      [0xfc00, 0xfc97, 0, 0],
    ],
    [
      [0x0626, 0x062e],
      [0, 0xfc99, 0, 0],
    ],
    [
      [0x0626, 0x0645],
      [0xfc02, 0xfc9a, 0xfcdf, 0xfc66],
    ],
    [
      [0x0626, 0x0646],
      [0, 0, 0, 0xfc67],
    ],
    [
      [0x0626, 0x06c6],
      [0xfbf2, 0, 0, 0xfbf3],
    ],
    [
      [0x0626, 0x0631],
      [0, 0, 0, 0xfc64],
    ],
    [
      [0x0626, 0x06c7],
      [0xfbf0, 0, 0, 0xfbf1],
    ],
    [
      [0x0626, 0x0648],
      [0xfbee, 0, 0, 0xfbef],
    ],
    [
      [0x0626, 0x064a],
      [0xfc04, 0, 0, 0xfc69],
    ],
    [
      [0x0626, 0x06c8],
      [0xfbf4, 0, 0, 0xfbf5],
    ],
    [
      [0x0626, 0x0632],
      [0, 0, 0, 0xfc65],
    ],
  ],
  [
    [
      [0x0627, 0x0644, 0x0644, 0x0647, 0x0020],
      [0xfdfd, 0, 0, 0],
    ],
    [
      [0x0627, 0x0644, 0x0644, 0x0647, 0x0020],
      [0xfdfa, 0, 0, 0],
    ],
    [
      [0x0627, 0x0644, 0x0644, 0x0647],
      [0xfdf2, 0, 0, 0],
    ],
    [
      [0x0627, 0x0644, 0x0631, 0x062d, 0x0645, 0x0646, 0x0020],
      [0xfdfd, 0, 0, 0],
    ],
    [
      [0x0627, 0x0644, 0x0631, 0x062d, 0x064a, 0x0645],
      [0xfdfd, 0, 0, 0],
    ],
    [
      [0x0627, 0x064b],
      [0xfd3d, 0, 0, 0xfd3c],
    ],
    [
      [0x0627, 0x064b],
      [0xfd3d, 0, 0, 0xfd3c],
    ],
  ],
  [
    [
      [0x0628, 0x0633, 0x0645, 0x0020],
      [0xfdfd, 0, 0, 0],
    ],
    [
      [0x0628, 0x0649],
      [0xfc09, 0, 0, 0xfc6e],
    ],
    [
      [0x0628, 0x062d, 0x064a],
      [0, 0, 0, 0xfdc2],
    ],
    [
      [0x0628, 0x062d],
      [0xfc06, 0xfc9d, 0, 0],
    ],
    [
      [0x0628, 0x0647],
      [0, 0xfca0, 0xfce2, 0],
    ],
    [
      [0x0628, 0x062c],
      [0xfc05, 0xfc9c, 0, 0],
    ],
    [
      [0x0628, 0x062e, 0x064a],
      [0, 0, 0, 0xfd9e],
    ],
    [
      [0x0628, 0x062e],
      [0xfc07, 0xfc9e, 0, 0],
    ],
    [
      [0x0628, 0x0645],
      [0xfc08, 0xfc9f, 0xfce1, 0xfc6c],
    ],
    [
      [0x0628, 0x0646],
      [0, 0, 0, 0xfc6d],
    ],
    [
      [0x0628, 0x0631],
      [0, 0, 0, 0xfc6a],
    ],
    [
      [0x0628, 0x0649],
      [0xfc09, 0, 0, 0xfc6e],
    ],
    [
      [0x0628, 0x062d, 0x064a],
      [0, 0, 0, 0xfdc2],
    ],
    [
      [0x0628, 0x062d],
      [0xfc06, 0xfc9d, 0, 0],
    ],
    [
      [0x0628, 0x0647],
      [0, 0xfca0, 0xfce2, 0],
    ],
    [
      [0x0628, 0x062c],
      [0xfc05, 0xfc9c, 0, 0],
    ],
    [
      [0x0628, 0x062e],
      [0xfc07, 0xfc9e, 0, 0],
    ],
    [
      [0x0628, 0x062e, 0x064a],
      [0, 0, 0, 0xfd9e],
    ],
    [
      [0x0628, 0x0645],
      [0xfc08, 0xfc9f, 0xfce1, 0xfc6c],
    ],
    [
      [0x0628, 0x0646],
      [0, 0, 0, 0xfc6d],
    ],
    [
      [0x0628, 0x0631],
      [0, 0, 0, 0xfc6a],
    ],
    [
      [0x0628, 0x064a],
      [0xfc0a, 0, 0, 0xfc6f],
    ],
    [
      [0x0628, 0x0632],
      [0, 0, 0, 0xfc6b],
    ],
  ],
  [
    [
      [0x062a, 0x0649],
      [0xfc0f, 0, 0, 0xfc74],
    ],
    [
      [0x062a, 0x062d],
      [0xfc0c, 0xfca2, 0, 0],
    ],
    [
      [0x062a, 0x062d, 0x062c],
      [0, 0xfd52, 0, 0xfd51],
    ],
    [
      [0x062a, 0x062d, 0x0645],
      [0, 0xfd53, 0, 0],
    ],
    [
      [0x062a, 0x0647],
      [0, 0xfca5, 0xfce4, 0],
    ],
    [
      [0x062a, 0x062c],
      [0xfc0b, 0xfca1, 0, 0],
    ],
    [
      [0x062a, 0x062c, 0x0649],
      [0, 0, 0, 0xfda0],
    ],
    [
      [0x062a, 0x062c, 0x0645],
      [0, 0xfd50, 0, 0],
    ],
    [
      [0x062a, 0x062c, 0x064a],
      [0, 0, 0, 0xfd9f],
    ],
    [
      [0x062a, 0x062e],
      [0xfc0d, 0xfca3, 0, 0],
    ],
    [
      [0x062a, 0x062e, 0x0649],
      [0, 0, 0, 0xfda2],
    ],
    [
      [0x062a, 0x062e, 0x0645],
      [0, 0xfd54, 0, 0],
    ],
    [
      [0x062a, 0x062e, 0x064a],
      [0, 0, 0, 0xfda1],
    ],
    [
      [0x062a, 0x0645],
      [0xfc0e, 0xfca4, 0xfce3, 0xfc72],
    ],
    [
      [0x062a, 0x0645, 0x0649],
      [0, 0, 0, 0xfda4],
    ],
    [
      [0x062a, 0x0645, 0x062d],
      [0, 0xfd56, 0, 0],
    ],
    [
      [0x062a, 0x0645, 0x062c],
      [0, 0xfd55, 0, 0],
    ],
    [
      [0x062a, 0x0645, 0x062e],
      [0, 0xfd57, 0, 0],
    ],
    [
      [0x062a, 0x0645, 0x064a],
      [0, 0, 0, 0xfda3],
    ],
    [
      [0x062a, 0x0646],
      [0, 0, 0, 0xfc73],
    ],
    [
      [0x062a, 0x0631],
      [0, 0, 0, 0xfc70],
    ],
    [
      [0x062a, 0x064a],
      [0xfc10, 0, 0, 0xfc75],
    ],
    [
      [0x062a, 0x0632],
      [0, 0, 0, 0xfc71],
    ],
  ],
  [
    [
      [0x062b, 0x0649],
      [0xfc13, 0, 0, 0xfc7a],
    ],
    [
      [0x062b, 0x0647],
      [0, 0, 0xfce6, 0],
    ],
    [
      [0x062b, 0x062c],
      [0xfc11, 0, 0, 0],
    ],
    [
      [0x062b, 0x0645],
      [0xfc12, 0xfca6, 0xfce5, 0xfc78],
    ],
    [
      [0x062b, 0x0646],
      [0, 0, 0, 0xfc79],
    ],
    [
      [0x062b, 0x0631],
      [0, 0, 0, 0xfc76],
    ],
    [
      [0x062b, 0x064a],
      [0xfc14, 0, 0, 0xfc7b],
    ],
    [
      [0x062b, 0x0632],
      [0, 0, 0, 0xfc77],
    ],
  ],
  [
    [
      [0x062c, 0x0644, 0x0020, 0x062c, 0x0644, 0x0627, 0x0644, 0x0647],
      [0xfdfb, 0, 0, 0],
    ],
    [
      [0x062c, 0x0649],
      [0xfd01, 0, 0, 0xfd1d],
    ],
    [
      [0x062c, 0x062d],
      [0xfc15, 0xfca7, 0, 0],
    ],
    [
      [0x062c, 0x062d, 0x0649],
      [0, 0, 0, 0xfda6],
    ],
    [
      [0x062c, 0x062d, 0x064a],
      [0, 0, 0, 0xfdbe],
    ],
    [
      [0x062c, 0x0645],
      [0xfc16, 0xfca8, 0, 0],
    ],
    [
      [0x062c, 0x0645, 0x0649],
      [0, 0, 0, 0xfda7],
    ],
    [
      [0x062c, 0x0645, 0x062d],
      [0, 0xfd59, 0, 0xfd58],
    ],
    [
      [0x062c, 0x0645, 0x064a],
      [0, 0, 0, 0xfda5],
    ],
    [
      [0x062c, 0x064a],
      [0xfd02, 0, 0, 0xfd1e],
    ],
  ],
  [
    [
      [0x062d, 0x0649],
      [0xfcff, 0, 0, 0xfd1b],
    ],
    [
      [0x062d, 0x062c],
      [0xfc17, 0xfca9, 0, 0],
    ],
    [
      [0x062d, 0x062c, 0x064a],
      [0, 0, 0, 0xfdbf],
    ],
    [
      [0x062d, 0x0645],
      [0xfc18, 0xfcaa, 0, 0],
    ],
    [
      [0x062d, 0x0645, 0x0649],
      [0, 0, 0, 0xfd5b],
    ],
    [
      [0x062d, 0x0645, 0x064a],
      [0, 0, 0, 0xfd5a],
    ],
    [
      [0x062d, 0x064a],
      [0xfd00, 0, 0, 0xfd1c],
    ],
  ],
  [
    [
      [0x062e, 0x0649],
      [0xfd03, 0, 0, 0xfd1f],
    ],
    [
      [0x062e, 0x062d],
      [0xfc1a, 0, 0, 0],
    ],
    [
      [0x062e, 0x062c],
      [0xfc19, 0xfcab, 0, 0],
    ],
    [
      [0x062e, 0x0645],
      [0xfc1b, 0xfcac, 0, 0],
    ],
    [
      [0x062e, 0x064a],
      [0xfd04, 0, 0, 0xfd20],
    ],
  ],
  [
    [
      [0x0630, 0x0670],
      [0xfc5b, 0, 0, 0],
    ],
  ],
  [
    [
      [0x0631, 0x0633, 0x0648, 0x0644],
      [0xfdf6, 0, 0, 0],
    ],
    [
      [0x0631, 0x06cc, 0x0627, 0x0644],
      [0xfdfc, 0, 0, 0],
    ],
    [
      [0x0631, 0x0670],
      [0xfc5c, 0, 0, 0],
    ],
  ],
  [
    [
      [0x0633, 0x0649],
      [0xfcfb, 0, 0, 0xfd17],
    ],
    [
      [0x0633, 0x062d],
      [0xfc1d, 0xfcae, 0xfd35, 0],
    ],
    [
      [0x0633, 0x062d, 0x062c],
      [0, 0xfd5c, 0, 0],
    ],
    [
      [0x0633, 0x0647],
      [0, 0xfd31, 0xfce8, 0],
    ],
    [
      [0x0633, 0x062c],
      [0xfc1c, 0xfcad, 0xfd34, 0],
    ],
    [
      [0x0633, 0x062c, 0x0649],
      [0, 0, 0, 0xfd5e],
    ],
    [
      [0x0633, 0x062c, 0x062d],
      [0, 0xfd5d, 0, 0],
    ],
    [
      [0x0633, 0x062e],
      [0xfc1e, 0xfcaf, 0xfd36, 0],
    ],
    [
      [0x0633, 0x062e, 0x0649],
      [0, 0, 0, 0xfda8],
    ],
    [
      [0x0633, 0x062e, 0x064a],
      [0, 0, 0, 0xfdc6],
    ],
    // [
    //   [0x0633, 0x0645, 0x062d],
    //   [0, 0xfd60, 0, 0xfd5f],
    // ],
    // [
    //   [0x0633, 0x0645],
    //   [0xfc1f, 0xfcb0, 0xfce7, 0],
    // ],
    [
      [0x0633, 0x0645, 0x062c],
      [0, 0xfd61, 0, 0],
    ],
    [
      [0x0633, 0x0645, 0x0645],
      [0, 0xfd63, 0, 0xfd62],
    ],
    [
      [0x0633, 0x0631],
      [0xfd0e, 0, 0, 0xfd2a],
    ],
    [
      [0x0633, 0x064a],
      [0xfcfc, 0, 0, 0xfd18],
    ],
  ],
  [
    [
      [0x0634, 0x0649],
      [0xfcfd, 0, 0, 0xfd19],
    ],
    [
      [0x0634, 0x062d],
      [0xfd0a, 0xfd2e, 0xfd38, 0xfd26],
    ],
    [
      [0x0634, 0x062d, 0x0645],
      [0, 0xfd68, 0, 0xfd67],
    ],
    [
      [0x0634, 0x062d, 0x064a],
      [0, 0, 0, 0xfdaa],
    ],
    [
      [0x0634, 0x0647],
      [0, 0xfd32, 0xfcea, 0],
    ],
    [
      [0x0634, 0x062c],
      [0xfd09, 0xfd2d, 0xfd37, 0xfd25],
    ],
    [
      [0x0634, 0x062c, 0x064a],
      [0, 0, 0, 0xfd69],
    ],
    [
      [0x0634, 0x062e],
      [0xfd0b, 0xfd2f, 0xfd39, 0xfd27],
    ],
    [
      [0x0634, 0x0645],
      [0xfd0c, 0xfd30, 0xfce9, 0xfd28],
    ],
    [
      [0x0634, 0x0645, 0x062e],
      [0, 0xfd6b, 0, 0xfd6a],
    ],
    [
      [0x0634, 0x0645, 0x0645],
      [0, 0xfd6d, 0, 0xfd6c],
    ],
    [
      [0x0634, 0x0631],
      [0xfd0d, 0, 0, 0xfd29],
    ],
    [
      [0x0634, 0x064a],
      [0xfcfe, 0, 0, 0xfd1a],
    ],
  ],
  [
    [
      [0x0635, 0x0644, 0x0649, 0x0020],
      [0xfdfa, 0, 0, 0],
    ],
    [
      [0x0635, 0x0644, 0x0639, 0x0645],
      [0xfdf5, 0, 0, 0],
    ],
    [
      [0x0635, 0x0644, 0x0649],
      [0xfdf9, 0, 0, 0],
    ],
    [
      [0x0635, 0x0649],
      [0xfd05, 0, 0, 0xfd21],
    ],
    [
      [0x0635, 0x062d],
      [0xfc20, 0xfcb1, 0, 0],
    ],
    [
      [0x0635, 0x062d, 0x062d],
      [0, 0xfd65, 0, 0xfd64],
    ],
    [
      [0x0635, 0x062d, 0x064a],
      [0, 0, 0, 0xfda9],
    ],
    [
      [0x0635, 0x062e],
      [0, 0xfcb2, 0, 0],
    ],
    [
      [0x0635, 0x0645],
      [0xfc21, 0xfcb3, 0, 0],
    ],
    [
      [0x0635, 0x0645, 0x0645],
      [0, 0xfdc5, 0, 0xfd66],
    ],
    [
      [0x0635, 0x0631],
      [0xfd0f, 0, 0, 0xfd2b],
    ],
    [
      [0x0635, 0x064a],
      [0xfd06, 0, 0, 0xfd22],
    ],
  ],
  [
    [
      [0x0636, 0x0649],
      [0xfd07, 0, 0, 0xfd23],
    ],
    [
      [0x0636, 0x062d],
      [0xfc23, 0xfcb5, 0, 0],
    ],
    [
      [0x0636, 0x062d, 0x0649],
      [0, 0, 0, 0xfd6e],
    ],
    [
      [0x0636, 0x062d, 0x064a],
      [0, 0, 0, 0xfdab],
    ],
    [
      [0x0636, 0x062c],
      [0xfc22, 0xfcb4, 0, 0],
    ],
    [
      [0x0636, 0x062e],
      [0xfc24, 0xfcb6, 0, 0],
    ],
    [
      [0x0636, 0x062e, 0x0645],
      [0, 0xfd70, 0, 0xfd6f],
    ],
    [
      [0x0636, 0x0645],
      [0xfc25, 0xfcb7, 0, 0],
    ],
    [
      [0x0636, 0x0631],
      [0xfd10, 0, 0, 0xfd2c],
    ],
    [
      [0x0636, 0x064a],
      [0xfd08, 0, 0, 0xfd24],
    ],
  ],
  [
    [
      [0x0637, 0x0649],
      [0xfcf5, 0, 0, 0xfd11],
    ],
    [
      [0x0637, 0x062d],
      [0xfc26, 0xfcb8, 0, 0],
    ],
    [
      [0x0637, 0x0645],
      [0xfc27, 0xfd33, 0xfd3a, 0],
    ],
    [
      [0x0637, 0x0645, 0x062d],
      [0, 0xfd72, 0, 0xfd71],
    ],
    [
      [0x0637, 0x0645, 0x0645],
      [0, 0xfd73, 0, 0],
    ],
    [
      [0x0637, 0x0645, 0x064a],
      [0, 0, 0, 0xfd74],
    ],
    [
      [0x0637, 0x064a],
      [0xfcf6, 0, 0, 0xfd12],
    ],
  ],
  [
    [
      [0x0638, 0x0645],
      [0xfc28, 0xfcb9, 0xfd3b, 0],
    ],
  ],
  [
    [
      [0x0639, 0x0644, 0x064a, 0x0647, 0x0020],
      [0xfdfa, 0, 0, 0],
    ],
    [
      [0x0639, 0x0644, 0x064a, 0x0647],
      [0xfdf7, 0, 0, 0],
    ],
    [
      [0x0639, 0x0649],
      [0xfcf7, 0, 0, 0xfd13],
    ],
    [
      [0x0639, 0x062c],
      [0xfc29, 0xfcba, 0, 0],
    ],
    [
      [0x0639, 0x062c, 0x0645],
      [0, 0xfdc4, 0, 0xfd75],
    ],
    [
      [0x0639, 0x0645],
      [0xfc2a, 0xfcbb, 0, 0],
    ],
    [
      [0x0639, 0x0645, 0x0649],
      [0, 0, 0, 0xfd78],
    ],
    [
      [0x0639, 0x0645, 0x0645],
      [0, 0xfd77, 0, 0xfd76],
    ],
    [
      [0x0639, 0x0645, 0x064a],
      [0, 0, 0, 0xfdb6],
    ],
    [
      [0x0639, 0x064a],
      [0xfcf8, 0, 0, 0xfd14],
    ],
    [
      [0x0639, 0x0649],
      [0xfcf7, 0, 0, 0xfd13],
    ],
    [
      [0x0639, 0x062c],
      [0xfc29, 0xfcba, 0, 0],
    ],
    [
      [0x0639, 0x062c, 0x0645],
      [0, 0xfdc4, 0, 0xfd75],
    ],
    [
      [0x0639, 0x0645],
      [0xfc2a, 0xfcbb, 0, 0],
    ],
    [
      [0x0639, 0x0645, 0x0649],
      [0, 0, 0, 0xfd78],
    ],
    [
      [0x0639, 0x0645, 0x0645],
      [0, 0xfd77, 0, 0xfd76],
    ],
    [
      [0x0639, 0x0645, 0x064a],
      [0, 0, 0, 0xfdb6],
    ],
    [
      [0x0639, 0x064a],
      [0xfcf8, 0, 0, 0xfd14],
    ],
  ],
  [
    [
      [0x063a, 0x0649],
      [0xfcf9, 0, 0, 0xfd15],
    ],
    [
      [0x063a, 0x062c],
      [0xfc2b, 0xfcbc, 0, 0],
    ],
    [
      [0x063a, 0x0645],
      [0xfc2c, 0xfcbd, 0, 0],
    ],
    [
      [0x063a, 0x0645, 0x0649],
      [0, 0, 0, 0xfd7b],
    ],
    [
      [0x063a, 0x0645, 0x0645],
      [0, 0, 0, 0xfd79],
    ],
    [
      [0x063a, 0x0645, 0x064a],
      [0, 0, 0, 0xfd7a],
    ],
    [
      [0x063a, 0x064a],
      [0xfcfa, 0, 0, 0xfd16],
    ],
  ],
  [
    [
      [0x0640, 0x064f, 0x0651],
      [0, 0, 0xfcf3, 0],
    ],
    [
      [0x0640, 0x064e, 0x0651],
      [0, 0, 0xfcf2, 0],
    ],
    [
      [0x0640, 0x0650, 0x0651],
      [0, 0, 0xfcf4, 0],
    ],
  ],
  [
    [
      [0x0641, 0x0649],
      [0xfc31, 0, 0, 0xfc7c],
    ],
    [
      [0x0641, 0x062d],
      [0xfc2e, 0xfcbf, 0, 0],
    ],
    [
      [0x0641, 0x062c],
      [0xfc2d, 0xfcbe, 0, 0],
    ],
    [
      [0x0641, 0x062e],
      [0xfc2f, 0xfcc0, 0, 0],
    ],
    [
      [0x0641, 0x062e, 0x0645],
      [0, 0xfd7d, 0, 0xfd7c],
    ],
    [
      [0x0641, 0x0645],
      [0xfc30, 0xfcc1, 0, 0],
    ],
    [
      [0x0641, 0x0645, 0x064a],
      [0, 0, 0, 0xfdc1],
    ],
    [
      [0x0641, 0x064a],
      [0xfc32, 0, 0, 0xfc7d],
    ],
  ],
  [
    [
      [0x0642, 0x0649],
      [0xfc35, 0, 0, 0xfc7e],
    ],
    [
      [0x0642, 0x062d],
      [0xfc33, 0xfcc2, 0, 0],
    ],
    [
      [0x0642, 0x0645],
      [0xfc34, 0xfcc3, 0, 0],
    ],
    [
      [0x0642, 0x0645, 0x062d],
      [0, 0xfdb4, 0, 0xfd7e],
    ],
    [
      [0x0642, 0x0645, 0x0645],
      [0, 0, 0, 0xfd7f],
    ],
    [
      [0x0642, 0x0645, 0x064a],
      [0, 0, 0, 0xfdb2],
    ],
    [
      [0x0642, 0x064a],
      [0xfc36, 0, 0, 0xfc7f],
    ],
  ],
  [
    [
      [0x0643, 0x0627],
      [0xfc37, 0, 0, 0xfc80],
    ],
    [
      [0x0643, 0x0649],
      [0xfc3d, 0, 0, 0xfc83],
    ],
    [
      [0x0643, 0x062d],
      [0xfc39, 0xfcc5, 0, 0],
    ],
    [
      [0x0643, 0x062c],
      [0xfc38, 0xfcc4, 0, 0],
    ],
    [
      [0x0643, 0x062e],
      [0xfc3a, 0xfcc6, 0, 0],
    ],
    [
      [0x0643, 0x0644],
      [0xfc3b, 0xfcc7, 0xfceb, 0xfc81],
    ],
    [
      [0x0643, 0x0645],
      [0xfc3c, 0xfcc8, 0xfcec, 0xfc82],
    ],
    [
      [0x0643, 0x0645, 0x0645],
      [0, 0xfdc3, 0, 0xfdbb],
    ],
    [
      [0x0643, 0x0645, 0x064a],
      [0, 0, 0, 0xfdb7],
    ],
    [
      [0x0643, 0x064a],
      [0xfc3e, 0, 0, 0xfc84],
    ],
  ],
  [
    [
      [0x0644, 0x0627],
      [0xfefb, 0, 0, 0xfefc],
    ],
    [
      [0x0644, 0x0649],
      [0xfc43, 0, 0, 0xfc86],
    ],
    [
      [0x0644, 0x0623],
      [0xfef7, 0, 0, 0xfef8],
    ],
    [
      [0x0644, 0x0625],
      [0xfef9, 0, 0, 0xfefa],
    ],
    [
      [0x0644, 0x0622],
      [0xfef5, 0, 0, 0xfef6],
    ],
    [
      [0x0644, 0x062d],
      [0xfc40, 0xfcca, 0, 0],
    ],
    [
      [0x0644, 0x062d, 0x0649],
      [0, 0, 0, 0xfd82],
    ],
    [
      [0x0644, 0x062d, 0x0645],
      [0, 0xfdb5, 0, 0xfd80],
    ],
    [
      [0x0644, 0x062d, 0x064a],
      [0, 0, 0, 0xfd81],
    ],
    [
      [0x0644, 0x0647],
      [0, 0xfccd, 0, 0],
    ],
    [
      [0x0644, 0x062c],
      [0xfc3f, 0xfcc9, 0, 0],
    ],
    [
      [0x0644, 0x062c, 0x062c],
      [0, 0xfd83, 0, 0xfd84],
    ],
    [
      [0x0644, 0x062c, 0x0645],
      [0, 0xfdba, 0, 0xfdbc],
    ],
    [
      [0x0644, 0x062c, 0x064a],
      [0, 0, 0, 0xfdac],
    ],
    [
      [0x0644, 0x062e],
      [0xfc41, 0xfccb, 0, 0],
    ],
    [
      [0x0644, 0x062e, 0x0645],
      [0, 0xfd86, 0, 0xfd85],
    ],
    [
      [0x0644, 0x0645, 0x062e],
      [0, 0xfd88, 0, 0xfd87],
    ],
    [
      [0x0644, 0x0645, 0x062d],
      [0, 0xfd88, 0, 0xfd87],
    ],
    [
      [0x0644, 0x0645, 0x064a],
      [0, 0, 0, 0xfdad],
    ],
    [
      [0x0644, 0x0645],
      [0xfc42, 0xfccc, 0xfced, 0xfc85],
    ],
    [
      [0x0644, 0x064a],
      [0xfc44, 0, 0, 0xfc87],
    ],
  ],
  [
    [
      [0x0645, 0x062d, 0x0645, 0x062f],
      [0xfdf4, 0, 0, 0],
    ],
    [
      [0x0645, 0x0627],
      [0, 0, 0, 0xfc88],
    ],
    [
      [0x0645, 0x0649],
      [0xfc49, 0, 0, 0],
    ],
    [
      [0x0645, 0x062d],
      [0xfc46, 0xfccf, 0, 0],
    ],
    [
      [0x0645, 0x062d, 0x062c],
      [0, 0xfd89, 0, 0],
    ],
    [
      [0x0645, 0x062d, 0x0645],
      [0, 0xfd8a, 0, 0],
    ],
    [
      [0x0645, 0x062d, 0x064a],
      [0, 0, 0, 0xfd8b],
    ],
    [
      [0x0645, 0x062c],
      [0xfc45, 0xfcce, 0, 0],
    ],
    [
      [0x0645, 0x062c, 0x062d],
      [0, 0xfd8c, 0, 0],
    ],
    [
      [0x0645, 0x062c, 0x062e],
      [0, 0xfd92, 0, 0],
    ],
    [
      [0x0645, 0x062c, 0x0645],
      [0, 0xfd8d, 0, 0],
    ],
    [
      [0x0645, 0x062c, 0x064a],
      [0, 0, 0, 0xfdc0],
    ],
    [
      [0x0645, 0x062e],
      [0xfc47, 0xfcd0, 0, 0],
    ],
    [
      [0x0645, 0x062e, 0x062c],
      [0, 0xfd8e, 0, 0],
    ],
    [
      [0x0645, 0x062e, 0x0645],
      [0, 0xfd8f, 0, 0],
    ],
    [
      [0x0645, 0x062e, 0x064a],
      [0, 0, 0, 0xfdb9],
    ],
    [
      [0x0645, 0x0645],
      [0xfc48, 0xfcd1, 0, 0xfc89],
    ],
    [
      [0x0645, 0x0645, 0x064a],
      [0, 0, 0, 0xfdb1],
    ],
    [
      [0x0645, 0x064a],
      [0xfc4a, 0, 0, 0],
    ],
  ],
  [
    [
      [0x0646, 0x0649],
      [0xfc4f, 0, 0, 0xfc8e],
    ],
    [
      [0x0646, 0x062d],
      [0xfc4c, 0xfcd3, 0, 0],
    ],
    [
      [0x0646, 0x062d, 0x0649],
      [0, 0, 0, 0xfd96],
    ],
    [
      [0x0646, 0x062d, 0x0645],
      [0, 0xfd95, 0, 0],
    ],
    [
      [0x0646, 0x062d, 0x064a],
      [0, 0, 0, 0xfdb3],
    ],
    [
      [0x0646, 0x0647],
      [0, 0xfcd6, 0xfcef, 0],
    ],
    [
      [0x0646, 0x062c],
      [0xfc4b, 0xfcd2, 0, 0],
    ],
    [
      [0x0646, 0x062c, 0x0649],
      [0, 0, 0, 0xfd99],
    ],
    [
      [0x0646, 0x062c, 0x062d],
      [0, 0xfdb8, 0, 0xfdbd],
    ],
    [
      [0x0646, 0x062c, 0x0645],
      [0, 0xfd98, 0, 0xfd97],
    ],
    [
      [0x0646, 0x062c, 0x064a],
      [0, 0, 0, 0xfdc7],
    ],
    [
      [0x0646, 0x062e],
      [0xfc4d, 0xfcd4, 0, 0],
    ],
    [
      [0x0646, 0x0645],
      [0xfc4e, 0xfcd5, 0xfcee, 0xfc8c],
    ],
    [
      [0x0646, 0x0645, 0x0649],
      [0, 0, 0, 0xfd9b],
    ],
    [
      [0x0646, 0x0645, 0x064a],
      [0, 0, 0, 0xfd9a],
    ],
    [
      [0x0646, 0x0646],
      [0, 0, 0, 0xfc8d],
    ],
    [
      [0x0646, 0x0631],
      [0, 0, 0, 0xfc8a],
    ],
    [
      [0x0646, 0x064a],
      [0xfc50, 0, 0, 0xfc8f],
    ],
    [
      [0x0646, 0x0632],
      [0, 0, 0, 0xfc8b],
    ],
  ],
  [
    [
      [0x0647, 0x0649],
      [0xfc53, 0, 0, 0],
    ],
    [
      [0x0647, 0x062c],
      [0xfc51, 0xfcd7, 0, 0],
    ],
    [
      [0x0647, 0x0645],
      [0xfc52, 0xfcd8, 0, 0],
    ],
    [
      [0x0647, 0x0645, 0x062c],
      [0, 0xfd93, 0, 0],
    ],
    [
      [0x0647, 0x0645, 0x0645],
      [0, 0xfd94, 0, 0],
    ],
    [
      [0x0647, 0x0670],
      [0, 0xfcd9, 0, 0],
    ],
    [
      [0x0647, 0x064a],
      [0xfc54, 0, 0, 0],
    ],
  ],
  [
    [
      [0x0648, 0x0633, 0x0644, 0x0645],
      [0xfdfa, 0, 0, 0],
    ],
    [
      [0x0648, 0x0633, 0x0644, 0x0645],
      [0xfdf8, 0, 0, 0],
    ],
  ],
  [
    [
      [0x0649, 0x0670],
      [0xfc5d, 0, 0, 0xfc90],
    ],
    [
      [0x0649, 0x0670],
      [0xfc5d, 0, 0, 0xfc90],
    ],
  ],
  [
    [
      [0x064a, 0x0649],
      [0xfc59, 0, 0, 0xfc95],
    ],
    [
      [0x064a, 0x062d],
      [0xfc56, 0xfcdb, 0, 0],
    ],
    [
      [0x064a, 0x062d, 0x064a],
      [0, 0, 0, 0xfdae],
    ],
    [
      [0x064a, 0x0647],
      [0, 0xfcde, 0xfcf1, 0],
    ],
    [
      [0x064a, 0x062c],
      [0xfc55, 0xfcda, 0, 0],
    ],
    [
      [0x064a, 0x062c, 0x064a],
      [0, 0, 0, 0xfdaf],
    ],
    [
      [0x064a, 0x062e],
      [0xfc57, 0xfcdc, 0, 0],
    ],
    [
      [0x064a, 0x0645],
      [0xfc58, 0xfcdd, 0xfcf0, 0xfc93],
    ],
    [
      [0x064a, 0x0645, 0x0645],
      [0, 0xfd9d, 0, 0xfd9c],
    ],
    [
      [0x064a, 0x0645, 0x064a],
      [0, 0, 0, 0xfdb0],
    ],
    [
      [0x064a, 0x0646],
      [0, 0, 0, 0xfc94],
    ],
    [
      [0x064a, 0x0631],
      [0, 0, 0, 0xfc91],
    ],
    [
      [0x064a, 0x064a],
      [0xfc5a, 0, 0, 0xfc96],
    ],
    [
      [0x064a, 0x0632],
      [0, 0, 0, 0xfc92],
    ],
  ],
]

const ligature_simple_mapping_first_letters = [0x0644, 0x064a]
const ligatures_simple_mapping: ligature_map[][] = [
  [
    // 0x0644
    [
      [0x0644, 0x0622],
      [0xfef5, 0, 0, 0xfef6],
    ] /* LAM_ALEF_MADDA */,
    [
      [0x0644, 0x0623],
      [0xfef7, 0, 0, 0xfef8],
    ] /* LAM_ALEF_HAMZA_ABOVE */,
    [
      [0x0644, 0x0625],
      [0xfef9, 0, 0, 0xfefa],
    ] /* LAM_ALEF_HAMZA_BELOW */,
    [
      [0x0644, 0x0627],
      [0xfefb, 0, 0, 0xfefc],
    ] /* LAM_ALEF */,
    // [
    //   [0x0644, 0x0645, 0x062e],
    //   [0, 0xfd88, 0, 0xfd87],
    // ],
    [
      [0x0644, 0x0645, 0x062d],
      [0, 0xfd88, 0, 0xfd87],
    ],
  ],
  [
    // 0x064a
    // [
    //   [0x064a, 0x062c],
    //   [0xfc55, 0xfcda, 0, 0],
    // ],
    [
      [0x064a, 0x062c, 0x064a],
      [0, 0, 0, 0xfdaf],
    ],
    // [
    //   [0x064a, 0x062d],
    //   [0xfc56, 0xfcdb, 0, 0],
    // ],
    [
      [0x064a, 0x062d, 0x064a],
      [0, 0, 0, 0xfdae],
    ],
    // [
    //   [0x064a, 0x062e],
    //   [0xfc57, 0xfcdc, 0, 0],
    // ],
    // [
    //   [0x064a, 0x0631],
    //   [0, 0, 0, 0xfc91],
    // ],
    // [
    //   [0x064a, 0x0632],
    //   [0, 0, 0, 0xfc92],
    // ],
    // [
    //   [0x064a, 0x0645],
    //   [0xfc58, 0xfcdd, 0xfcf0, 0xfc93],
    // ],
    [
      [0x064a, 0x0645, 0x0645],
      [0, 0xfd9d, 0, 0xfd9c],
    ],
    [
      [0x064a, 0x0645, 0x064a],
      [0, 0, 0, 0xfdb0],
    ],
    // [
    //   [0x064a, 0x0646],
    //   [0, 0, 0, 0xfc94],
    // ],
    // [
    //   [0x064a, 0x0647],
    //   [0, 0xfcde, 0xfcf1, 0],
    // ],
    // [
    //   [0x064a, 0x0649],
    //   [0xfc59, 0, 0, 0xfc95],
    // ],
    // [
    //   [0x064a, 0x064a],
    //   [0xfc5a, 0, 0, 0xfc96],
    // ],
  ],
]

export function getLigatureMappings(
  charCode: number,
  useLigature: boolean
): Nullable<ligature_map[]> {
  if (charCode < 0x0623 || charCode > 0x064a) {
    return null
  }

  if (useLigature) {
    const found = bsearch(ligature_mapping_first_letters, charCode)
    if (found != null) {
      return ligatures_mapping[found]
    } else {
      return null
    }
  } else {
    const found = ligature_simple_mapping_first_letters.indexOf(charCode)

    if (found > -1) {
      return ligatures_simple_mapping[found]
    } else {
      return null
    }
  }
}

function matchPatten<T>(
  input: ManagedSlice<ArabicReshapeItem<T>>,
  startIndex: number,
  patten_map: ligature_map,
  getter: LetterStatusGetter<T>
): {
  isMatched: boolean
  matchLength?: number
} {
  const patten = patten_map[0]
  const inputLen = input.length
  const pattenLen = patten.length

  if (startIndex < 0 || inputLen - startIndex < pattenLen) {
    return { isMatched: false }
  }

  let matchLength: number = 0
  for (let i = 0, l = patten.length; i < l; i++) {
    const inputIndex = startIndex + matchLength
    if (inputIndex >= inputLen) {
      return { isMatched: false }
    }

    let info = getter(input.at(inputIndex)!)

    while (isHarakatLetter(info.statusCode)) {
      matchLength += 1
      if (startIndex + matchLength >= inputLen) {
        return { isMatched: false }
      }
      info = getter(input.at(startIndex + matchLength)!)
    }

    if (info.charCode === patten[i]) {
      matchLength += 1
      continue
    } else {
      return { isMatched: false }
    }
  }
  return { isMatched: true, matchLength }
}

/*
          +-----------+----------+---------+---------+----------+
          | start\end | ISOLATED | INITIAL | MEDIAL  | FINAL    |
          +-----------+----------+---------+---------+----------+
          | ISOLATED  | ISOLATED | INITIAL | INITIAL | ISOLATED |
          | INITIAL   | ISOLATED | INITIAL | INITIAL | ISOLATED |
          | MEDIAL    | FINAL    | MEDIAL  | MEDIAL  | FINAL    |
          | FINAL     | FINAL    | MEDIAL  | MEDIAL  | FINAL    |
          +-----------+----------+---------+---------+----------+
*/

function getLigatureFormKind(start: FORM_IDX, end: FORM_IDX): FORM_IDX {
  if (start === ISOLATED_IDX || start === INITIAL_IDX) {
    if (end === ISOLATED_IDX || end === FINAL_IDX) {
      return ISOLATED_IDX
    } else {
      return INITIAL_IDX
    }
  } else {
    if (end === ISOLATED_IDX || end === FINAL_IDX) {
      return FINAL_IDX
    } else {
      return MEDIAL_IDX
    }
  }
}

export function processLigature<T>(
  input: ManagedSlice<ArabicReshapeItem<T>>,
  startIndex: number,
  getter: LetterStatusGetter<T>,
  updater: LetterStatusUpdater<T>,
  useLigature: boolean
) {
  if (startIndex > input.length - 1) {
    return startIndex
  }
  const startItem = input.at(startIndex)!
  const { charCode, statusCode } = getter(startItem)
  const ligture_maps = getLigatureMappings(charCode, useLigature)
  if (ligture_maps == null) {
    return startIndex + 1
  }

  for (let i = 0, l = ligture_maps.length; i < l; i++) {
    const mapping = ligture_maps[i]
    const { isMatched, matchLength } = matchPatten(
      input,
      startIndex,
      mapping,
      getter
    )
    if (isMatched === true) {
      const startForm = getLetterFormKindByStatusCode(statusCode)
      const forms = mapping[1]
      const matchedLength = matchLength!
      const endForm = getLetterFormKindByStatusCode(
        getter(input.at(startIndex + matchedLength - 1)!).statusCode
      )

      const ligatureForm = getLigatureFormKind(startForm, endForm)
      const formCode = formOf(forms, ligatureForm)
      if (formCode == null) {
        return startIndex + 1
      } else {
        // set ligature form for first letter in patten
        updater(startItem, statusCode, formCode)

        // skip the rest letters in patten
        for (let j = 1; j < matchedLength; j++) {
          const item = input.at(j + startIndex)!

          const statusCode = updateSkippedStatus(getter(item).statusCode, true)
          updater(item, statusCode)
        }

        return startIndex + matchedLength
      }
    }
  }
  return startIndex + 1
}
