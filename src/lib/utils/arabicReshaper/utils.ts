export function bsearch<T>(arr: T[], v: T): undefined | number {
  let start = 0
  let end = arr.length - 1
  while (start <= end) {
    const mid = (start + ((end - start) | 0) / 2) | 0
    const mid_v = arr[mid]
    if (mid_v === v) {
      return mid
    }
    if (mid_v > v) {
      end = (mid - 1) | 0
    } else {
      start = (mid + 1) | 0
    }
  }
}
