import { Nullable } from '../../../../liber/pervasive'
import { ManagedSlice } from '../../../common/managedArray'
import { FINAL_IDX, INITIAL_IDX, ISOLATED_IDX, MEDIAL_IDX } from './letters'
import { processLigature } from './ligature'
import {
  LetterStatusCode,
  getLetterFormByStatusCode,
  updateSkippedStatus,
  INIT_STATUS_CODE,
  isArabicLetter,
  updateFormIndex,
  getLetterFormKindByStatusCode,
  isHarakatLetter,
} from './statusCode'
import {
  ArabicReshapeItem,
  LetterStatusGetter,
  LetterStatusUpdater,
} from './type'

function can_connects_with_letter_before(statusCode: LetterStatusCode) {
  const forms = getLetterFormByStatusCode(statusCode)
  if (forms == null) return false
  return forms[MEDIAL_IDX] + forms[FINAL_IDX] > 0
}

function can_connects_with_letter_after(statusCode: LetterStatusCode) {
  const forms = getLetterFormByStatusCode(statusCode)
  if (forms == null) return false
  return forms[MEDIAL_IDX] + forms[INITIAL_IDX] > 0
}

function can_connects_with_letter_before_and_after(
  statusCode: LetterStatusCode
) {
  const forms = getLetterFormByStatusCode(statusCode)
  if (forms == null) return false
  return forms[MEDIAL_IDX] > 0
}

function reshapePair<T>(
  prev: Nullable<ArabicReshapeItem<T>>,
  current: ArabicReshapeItem<T>,
  getter: LetterStatusGetter<T>,
  updater: LetterStatusUpdater<T>,
  isSkipHARAKAT: boolean = false
): {
  hasArabic: boolean
  isHarakat: boolean
} {
  let hasArabic = true
  let isHarakat = false
  const { statusCode: curStatusCode } = getter(current)

  if (isHarakatLetter(curStatusCode)) {
    const statusCode = updateSkippedStatus(curStatusCode, isSkipHARAKAT)
    updater(current, statusCode)
    isHarakat = true
  } else if (!isArabicLetter(curStatusCode)) {
    updater(current, INIT_STATUS_CODE)
    hasArabic = false
  } else if (prev == null) {
    const { statusCode, formCode } = updateFormIndex(
      curStatusCode,
      ISOLATED_IDX
    )
    updater(current, statusCode, formCode)
  } else {
    const { statusCode: prevStatusCode } = getter(prev)

    const prevFormKind = getLetterFormKindByStatusCode(prevStatusCode)
    if (prevStatusCode === INIT_STATUS_CODE) {
      // not Arabic before this one
      const { statusCode, formCode } = updateFormIndex(
        curStatusCode,
        ISOLATED_IDX
      )
      updater(current, statusCode, formCode)
    } else if (!can_connects_with_letter_before(curStatusCode)) {
      // this letter doesn't try to connect with previous
      const { statusCode, formCode } = updateFormIndex(
        curStatusCode,
        ISOLATED_IDX
      )
      updater(current, statusCode, formCode)
    } else if (!can_connects_with_letter_after(prevStatusCode)) {
      // previous letter doesn't try to connect to me
      const { statusCode, formCode } = updateFormIndex(
        curStatusCode,
        ISOLATED_IDX
      )
      updater(current, statusCode, formCode)
    } else if (
      prevFormKind === FINAL_IDX &&
      !can_connects_with_letter_before_and_after(prevStatusCode)
    ) {
      // previous letter was final and cannot be medial to connect to me
      const { statusCode, formCode } = updateFormIndex(
        curStatusCode,
        ISOLATED_IDX
      )
      updater(current, statusCode, formCode)
    } else if (prevFormKind === ISOLATED_IDX) {
      // previous letter was alone - we can change it to be initial of my phrase
      // for now this letter is the final of the phrase
      const { statusCode: prevCode, formCode: prevFormCode } = updateFormIndex(
        prevStatusCode,
        INITIAL_IDX
      )
      updater(prev, prevCode, prevFormCode)

      const { statusCode, formCode } = updateFormIndex(curStatusCode, FINAL_IDX)
      updater(current, statusCode, formCode)
    } else {
      // previous letter can be changed to medial
      // this one can be final
      const { statusCode: prevCode, formCode: prevFormCode } = updateFormIndex(
        prevStatusCode,
        MEDIAL_IDX
      )
      updater(prev, prevCode, prevFormCode)

      const { statusCode, formCode } = updateFormIndex(curStatusCode, FINAL_IDX)
      updater(current, statusCode, formCode)
    }
  }
  return { hasArabic, isHarakat }
}

function reshapeForLigature<T>(
  items: ManagedSlice<ArabicReshapeItem<T>>,
  getter: LetterStatusGetter<T>,
  updater: LetterStatusUpdater<T>,
  useLigature: boolean
) {
  let startIndex = 0
  const length = items.length
  while (startIndex < length) {
    startIndex = processLigature(
      items,
      startIndex,
      getter,
      updater,
      useLigature
    )
  }
}

export interface ReshapeOptions {
  isSkipHARAKAT?: boolean
  useLigature?: boolean
}

export function reshape<T>(
  items: ManagedSlice<ArabicReshapeItem<T>>,
  getter: LetterStatusGetter<T>,
  updater: LetterStatusUpdater<T>,
  options?: ReshapeOptions
) {
  options = options ?? {}
  const isSkipHARAKAT = options.isSkipHARAKAT ?? false
  // 默认不开启ligatrue支持
  const useLigature = options.useLigature ?? false
  let prev: ArabicReshapeItem<T> | null = null
  let hasArabic = false
  for (let i = 0, l = items.length; i < l; i++) {
    const item = items.at(i)!
    const { hasArabic: _hasArabic, isHarakat } = reshapePair(
      prev,
      item,
      getter,
      updater,
      isSkipHARAKAT
    )
    if (_hasArabic === true) {
      hasArabic = true
    }
    if (isHarakat === false) {
      prev = item
    }
  }

  if (hasArabic === true) {
    reshapeForLigature(items, getter, updater, useLigature)
  }

  return hasArabic
}
