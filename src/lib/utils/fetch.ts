export function fetch_(
  url,
  options?
): Promise<{
  ok: boolean
  statusText: string
  status: number
  url: string
  text: () => Promise<string>
  json: () => Promise<any>
  blob: () => Promise<Blob>
  arrayBuffer: () => Promise<ArrayBuffer>
}> {
  options = options || {}

  if (typeof fetch !== 'undefined') {
    // logger.log('using fetch...')
    return fetch(url, options)
  }

  return new Promise((resolve, reject) => {
    const request = new XMLHttpRequest()
    const keys: string[] = []
    const all: string[][] = []
    const headers = {}

    const response = () => ({
      ok: ((request.status / 100) | 0) === 2, // 200-299
      statusText: request.statusText,
      status: request.status,
      url: request.responseURL,
      text: () => Promise.resolve(request.responseText),
      json: () => Promise.resolve(request.responseText).then(JSON.parse),
      blob: () => Promise.resolve(new Blob([request.response])),
      arrayBuffer: () =>
        Promise.resolve(new Blob([request.response])).then((blob) =>
          blob.arrayBuffer()
        ),
      clone: response,
      headers: {
        keys: () => keys,
        entries: () => all,
        get: (n) => headers[n.toLowerCase()],
        has: (n) => n.toLowerCase() in headers,
      },
    })

    request.open(options.method || 'get', url, true)

    request.onload = () => {
      request.getAllResponseHeaders().replace(/^(.*?):[^\S\n]*([\s\S]*?)$/gm, ((
        _m,
        key,
        value
      ) => {
        keys.push((key = key.toLowerCase()))
        all.push([key, value])
        headers[key] = headers[key] ? `${headers[key]},${value}` : value
      }) as any)
      resolve(response())
    }

    request.onerror = reject

    request.withCredentials = options.credentials === 'include'

    for (const i in options.headers) {
      request.setRequestHeader(i, options.headers[i])
    }

    request.send(options.body || null)
  })
}
