import { Opaque } from '../../../../liber/pervasive'

export type Pointer = Opaque<'Pointer', number>
