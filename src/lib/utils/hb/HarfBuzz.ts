import { HarfBuzzNative } from './HarfBuzzNative'
import { Pointer } from './type'

class CString {
  readonly ptr: Pointer
  readonly length: number

  constructor(hb: HarfBuzzNative, text: string) {
    const bytes = hb.utf8Encoder.encode(text)
    this.ptr = hb.malloc(bytes.byteLength)
    hb.heapu8.set(bytes, this.ptr)
    this.length = bytes.byteLength
  }

  destroy(hb: HarfBuzzNative) {
    hb.free(this.ptr)
  }
}
const HB_MEMORY_MODE_WRITABLE: number = 2
export class HarfBuzzBlob {
  readonly ptr: Pointer
  readonly hb: HarfBuzzNative
  constructor(hb: HarfBuzzNative, data: Uint8Array) {
    this.hb = hb
    const blobPtr = hb.malloc(data.length)
    hb.heapu8.set(data, blobPtr)
    this.ptr = hb.hb_blob_create(
      blobPtr,
      data.byteLength,
      HB_MEMORY_MODE_WRITABLE,
      blobPtr,
      hb.free_ptr()
    )
  }

  destroy() {
    this.hb.hb_blob_destroy(this.ptr)
  }
}

export class HarfBuzzFace {
  readonly ptr: Pointer
  readonly hb: HarfBuzzNative
  constructor(hb: HarfBuzzNative, blob: HarfBuzzBlob, index: number) {
    this.hb = hb
    this.ptr = hb.hb_face_create(blob.ptr, index)
  }

  getUnitsPerEM() {
    return this.hb.hb_face_get_upem(this.ptr)
  }

  destroy() {
    this.hb.hb_face_destroy(this.ptr)
  }
}

export class HarfBuzzFont {
  readonly ptr: Pointer
  readonly unitsPerEM: number
  readonly hb: HarfBuzzNative
  constructor(hb: HarfBuzzNative, face: HarfBuzzFace) {
    this.hb = hb
    this.ptr = hb.hb_font_create(face.ptr)
    this.unitsPerEM = face.getUnitsPerEM()
  }

  setScale(xScale: number, yScale: number) {
    this.hb.hb_font_set_scale(this.ptr, xScale, yScale)
  }

  destroy() {
    this.hb.hb_font_destroy(this.ptr)
  }
}

export type HarfBuzzDirection = 'ltr' | 'rtl' | 'ttb' | 'btt'

export class GlyphInformation {
  readonly GlyphId: number
  readonly Cluster: number
  readonly XAdvance: number
  readonly YAdvance: number
  readonly XOffset: number
  readonly YOffset: number

  constructor(
    glyphId: number,
    cluster: number,
    xAdvance: number,
    yAdvance: number,
    xOffset: number,
    yOffset: number
  ) {
    this.GlyphId = glyphId
    this.Cluster = cluster
    this.XAdvance = xAdvance
    this.YAdvance = yAdvance
    this.XOffset = xOffset
    this.YOffset = yOffset
  }
}

export class HarfBuzzBuffer {
  readonly ptr: Pointer
  readonly hb: HarfBuzzNative
  constructor(hb: HarfBuzzNative) {
    this.hb = hb
    this.ptr = hb.hb_buffer_create()
  }

  addText(text: string) {
    const hb = this.hb
    const str = new CString(hb, text)
    hb.hb_buffer_add_utf8(this.ptr, str.ptr, str.length, 0, str.length)
    str.destroy(hb)
  }

  guessSegmentProperties() {
    this.hb.hb_buffer_guess_segment_properties(this.ptr)
  }

  setDirection(direction: HarfBuzzDirection) {
    const d = { ltr: 4, rtl: 5, ttb: 6, btt: 7 }[direction]
    this.hb.hb_buffer_set_direction(this.ptr, d)
  }

  shape(font: HarfBuzzFont) {
    this.hb.hb_buffer_set_cluster_level(this.ptr, 0)
    this.hb.hb_shape(font.ptr, this.ptr, 0, 0)
  }

  json() {
    const hb = this.hb
    const length = hb.hb_buffer_get_length(this.ptr)
    const result = new Array<GlyphInformation>()
    const infosPtr32 = hb.hb_buffer_get_glyph_infos(this.ptr, 0) / 4
    const positionsPtr32 = hb.hb_buffer_get_glyph_positions(this.ptr, 0) / 4
    const infos = hb.heapu32.subarray(infosPtr32, infosPtr32 + 5 * length)
    const positions = hb.heapi32.subarray(
      positionsPtr32,
      positionsPtr32 + 5 * length
    )
    for (let i = 0; i < length; ++i) {
      result.push(
        new GlyphInformation(
          infos[i * 5 + 0],
          infos[i * 5 + 2] >> 1, // is weird that cluster number is doubled
          positions[i * 5 + 0],
          positions[i * 5 + 1],
          positions[i * 5 + 2],
          positions[i * 5 + 3]
        )
      )
    }
    return result
  }

  destroy() {
    this.hb.hb_buffer_destroy(this.ptr)
  }
}
