import { isSupportWasm } from '../isSupportWasm'
import { hb_wasm } from './hb_wasm'
import { Pointer } from './type'

export class HarfBuzzNative {
  readonly heapu8: Uint8Array
  readonly heapu32: Uint32Array
  readonly heapi32: Int32Array
  readonly utf8Encoder: TextEncoder

  //exported HarfBuzz methods
  readonly malloc: (length: number) => Pointer
  readonly free: (ptr: Pointer) => void
  readonly free_ptr: () => Pointer
  readonly hb_blob_create: (
    data: Pointer,
    length: number,
    memoryMode: number,
    useData: Pointer,
    destroyFunction: Pointer
  ) => Pointer
  readonly hb_blob_destroy: (ptr: Pointer) => void
  readonly hb_face_create: (blobPtr: Pointer, index: number) => Pointer
  readonly hb_face_get_upem: (facePtr: Pointer) => number
  readonly hb_face_destroy: (ptr: Pointer) => void
  readonly hb_font_create: (facePtr: Pointer) => Pointer
  readonly hb_font_set_scale: (
    fontPtr: Pointer,
    xScale: number,
    yScale: number
  ) => void
  readonly hb_font_destroy: (ptr: Pointer) => void
  readonly hb_buffer_create: () => Pointer
  readonly hb_buffer_add_utf8: (
    bufferPtr: Pointer,
    stringPtr: Pointer,
    stringLength: number,
    itemOffset: number,
    itemLength: number
  ) => void
  readonly hb_buffer_guess_segment_properties: (bufferPtr: Pointer) => void
  readonly hb_buffer_set_direction: (
    bufferPtr: Pointer,
    direction: number
  ) => void
  readonly hb_shape: (
    fontPtr: Pointer,
    bufferPtr: Pointer,
    features: any,
    numFeatures: number
  ) => void
  readonly hb_buffer_get_length: (bufferPtr: Pointer) => number
  readonly hb_buffer_get_glyph_infos: (
    bufferPtr: Pointer,
    length: number
  ) => any
  readonly hb_buffer_get_glyph_positions: (
    bufferPtr: Pointer,
    length: number
  ) => any
  readonly hb_buffer_set_cluster_level: (
    bufferPtr: Pointer,
    level: number
  ) => any
  readonly hb_buffer_destroy: (bufferPtr: Pointer) => void

  constructor(exports: any) {
    const buffer = exports['memory']['buffer']
    this.heapu8 = new Uint8Array(buffer)
    this.heapu32 = new Uint32Array(buffer)
    this.heapi32 = new Int32Array(buffer)
    this.utf8Encoder = new TextEncoder()

    this.malloc = exports['malloc']
    this.free = exports['free']
    this.free_ptr = exports['free_ptr']
    this.hb_blob_destroy = exports['hb_blob_destroy']
    this.hb_blob_create = exports['hb_blob_create']
    this.hb_face_create = exports['hb_face_create']
    this.hb_face_get_upem = exports['hb_face_get_upem']
    this.hb_face_destroy = exports['hb_face_destroy']
    this.hb_font_create = exports['hb_font_create']
    this.hb_font_set_scale = exports['hb_font_set_scale']
    this.hb_font_destroy = exports['hb_font_destroy']
    this.hb_buffer_create = exports['hb_buffer_create']
    this.hb_buffer_add_utf8 = exports['hb_buffer_add_utf8']
    this.hb_buffer_guess_segment_properties =
      exports['hb_buffer_guess_segment_properties']
    this.hb_buffer_set_direction = exports['hb_buffer_set_direction']
    this.hb_shape = exports['hb_shape']
    this.hb_buffer_get_length = exports['hb_buffer_get_length']
    this.hb_buffer_get_glyph_infos = exports['hb_buffer_get_glyph_infos']
    this.hb_buffer_get_glyph_positions =
      exports['hb_buffer_get_glyph_positions']
    this.hb_buffer_set_cluster_level = exports['hb_buffer_set_cluster_level']
    this.hb_buffer_destroy = exports['hb_buffer_destroy']
  }
}

const HB_WASM_MEMORY_SZIE = 1024 * 5 // 5M

export function loadHarfBuzz(): Promise<undefined | HarfBuzzNative> {
  if (isSupportWasm === false) {
    console.warn('Webassembly is not supported!')
    return Promise.resolve(undefined)
  } else {
    return WebAssembly.instantiate(hb_wasm).then((module) => {
      //@ts-ignore

      const exports = module['instance']['exports']
      try {
        exports['memory']['grow'](HB_WASM_MEMORY_SZIE)
      } catch (error) {
        return undefined
      }
      return new HarfBuzzNative(exports)
    })
  }
}
