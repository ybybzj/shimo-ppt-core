import { getHBFont } from '../../../fonts/HBFontCache'
import { HarfBuzzBuffer, HarfBuzzFont } from './HarfBuzz'
import { HarfBuzzNative } from './HarfBuzzNative'

export function getGlyphInfo(
  hb: HarfBuzzNative,
  text: string,
  font: string | HarfBuzzFont
) {
  const hbFont = typeof font === 'string' ? getHBFont(font) : font
  if (!hbFont) {
    return undefined
  }

  const buffer = new HarfBuzzBuffer(hb)
  buffer.addText(text)
  buffer.guessSegmentProperties()
  buffer.shape(hbFont)
  const result = buffer.json()
  buffer.destroy()
  return result
}
