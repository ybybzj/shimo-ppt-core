import { Dict } from '../../../liber/pervasive'

export const parseQuery = <T = unknown>(search: string): Partial<T> => {
  const params = {} as any
  search
    .replace(/^\?/, '')
    .split('&')
    .map((str) => {
      const [key, value = true] = str.split('=')
      params[decodeURIComponent(key)] =
        value === 'true'
          ? true
          : value === 'false'
          ? false
          : !Number.isNaN(Number(value))
          ? Number(value)
          : typeof value === 'string'
          ? decodeURIComponent(value)
          : value
    })

  return params
}

export const stringifyQuery = <T = unknown>(dict: Partial<T>): string => {
  if (!dict) {
    return ''
  }
  return Object.keys(dict || {})
    .map((key) => {
      return `${encodeURIComponent(key)}=${encodeURIComponent(
        (dict as any)[key]
      )}`
    })
    .join('&')
}

const a = document.createElement('a')
export function getSearchString(url: string) {
  a.href = url
  return a.search
}

export function addSearchParams<P extends Dict>(
  url: string,
  params: Partial<P>
): string {
  if (!params || Object.keys(params).length <= 0) return url
  const oldParams = parseQuery(getSearchString(url))

  params = { ...oldParams, ...params }

  a.href = url
  a.search = stringifyQuery(params)
  return a.href
}
