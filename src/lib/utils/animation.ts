import {
  makeAnimation,
  makeAnimationWithDuration,
  startAnimationRe,
  stopAnimationRe,
  isAnimationPlayingRe,
  animationRe,
} from '../../re/export/Animation'

export type frameCallback = (percentOfDuration?: number) => void

export type animation = animationRe
export function createAnimation(
  frameCallback: frameCallback,
  duration?: number,
  endCallback?: () => void
): animation {
  const anim =
    duration != null
      ? makeAnimationWithDuration(
          frameCallback,
          endCallback,
          duration
        )
      : makeAnimation(frameCallback)

  return anim as unknown as animation
}

export function startAnimation(anim: animation) {
  startAnimationRe(anim as unknown as animation)
}

export function stopAnimation(anim: animation) {
  stopAnimationRe(anim as unknown as animation)
}

export function isAnimationPlaying(anim: animation): boolean {
  return isAnimationPlayingRe(anim as unknown as animation)
}
