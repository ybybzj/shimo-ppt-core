export function toDate<DateType extends Date = Date>(
  argument: DateType | number
): DateType {
  const argStr = Object.prototype.toString.call(argument)

  // Clone the date
  if (
    argument instanceof Date ||
    (typeof argument === 'object' && argStr === '[object Date]')
  ) {
    // Prevent the date to lose the milliseconds when passed to new Date() in IE10
    // eslint-disable-next-line @typescript-eslint/ban-ts-comment
    // @ts-ignore: TODO find a way to make TypeScript happy about this code
    return new argument.constructor(argument.getTime())
    // return new Date(argument.getTime())
  } else if (typeof argument === 'number' || argStr === '[object Number]') {
    return new Date(argument) as DateType
  } else {
    return new Date(NaN) as DateType
  }
}

export default function isDate(value: unknown): value is Date {
  return (
    value instanceof Date ||
    (typeof value === 'object' &&
      Object.prototype.toString.call(value) === '[object Date]')
  )
}

export function isValid(dirtyDate: unknown): boolean {
  if (!isDate(dirtyDate) && typeof dirtyDate !== 'number') {
    return false
  }
  const date = toDate(dirtyDate)
  return !isNaN(Number(date))
}

const protectedDayOfYearTokens = ['D', 'DD']
const protectedWeekYearTokens = ['YY', 'YYYY']

export function isProtectedDayOfYearToken(token: string): boolean {
  return protectedDayOfYearTokens.indexOf(token) !== -1
}

export function isProtectedWeekYearToken(token: string): boolean {
  return protectedWeekYearTokens.indexOf(token) !== -1
}

export function throwProtectedError(
  token: string,
  format: string,
  input: string
): void {
  if (token === 'YYYY') {
    throw new RangeError(
      `Use \`yyyy\` instead of \`YYYY\` (in \`${format}\`) for formatting years to the input \`${input}\`; see: https://github.com/date-fns/date-fns/blob/master/docs/unicodeTokens.md`
    )
  } else if (token === 'YY') {
    throw new RangeError(
      `Use \`yy\` instead of \`YY\` (in \`${format}\`) for formatting years to the input \`${input}\`; see: https://github.com/date-fns/date-fns/blob/master/docs/unicodeTokens.md`
    )
  } else if (token === 'D') {
    throw new RangeError(
      `Use \`d\` instead of \`D\` (in \`${format}\`) for formatting days of the month to the input \`${input}\`; see: https://github.com/date-fns/date-fns/blob/master/docs/unicodeTokens.md`
    )
  } else if (token === 'DD') {
    throw new RangeError(
      `Use \`dd\` instead of \`DD\` (in \`${format}\`) for formatting days of the month to the input \`${input}\`; see: https://github.com/date-fns/date-fns/blob/master/docs/unicodeTokens.md`
    )
  }
}
