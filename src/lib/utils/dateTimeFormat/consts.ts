import { Nullable } from '../../../../liber/pervasive'

// Ecma Office Open XML Part 1 - Fundamentals And Markup Language Reference.pdf
// 21.1.2.2.4  fld (Text Field)
const DateTimeFormatsMap = {
  ['datetime1']: 'M/dd/yyyy',
  ['datetime2']: 'EEEE, MMMM dd, yyyy',
  ['datetime3']: 'dd MMMM yyyy',
  ['datetime4']: 'MMMM dd, yyyy',
  ['datetime5']: 'dd-MMM-yy',
  ['datetime6']: 'MMMM yy',
  ['datetime7']: 'MMM-yy',
  ['datetime8']: 'M/dd/yyyy h:mm a',
  ['datetime9']: 'M/dd/yyyy h:mm:ss a',
  ['datetime10']: 'H:mm',
  ['datetime11']: 'H:mm:ss',
  ['datetime12']: 'h:mm a',
  ['datetime13']: 'h:mm:ss a',
  ['datetimeyyyy']: 'yyyy',
} as const

export function getDateTimeFormatString(
  fldStr: string,
  locale?: Nullable<string>
): string {
  const result = DateTimeFormatsMap[fldStr]

  switch (locale) {
    case 'zh-CN': {
      if (fldStr === 'datetime9') {
        return 'yoMMMdoEEEEHomoso'
      }
    }
  }

  return result ?? DateTimeFormatsMap['datetime1']
}
