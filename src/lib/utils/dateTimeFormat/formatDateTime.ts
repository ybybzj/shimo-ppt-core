import { Locale } from './locale/type'
import {
  isProtectedDayOfYearToken,
  isProtectedWeekYearToken,
  isValid,
  throwProtectedError,
  toDate,
} from './utils'
import { Formatter, formatters } from './formatters'
import { Nullable } from '../../../../liber/pervasive'
import { locale } from './locale/zh-CN/locale'
const defaultLocale = locale

interface FormatOptions {
  locale: Locale
}

// This RegExp consists of three parts separated by `|`:
// - [yYQqMLwIdDecihHKkms]o matches any available ordinal number token
//   (one of the certain letters followed by `o`)
// - (\w)\1* matches any sequences of the same letter
// - . matches any single character unmatched by previous parts of the RegExps
const formattingTokensRegExp = /[yYQqMLwIdDecihHKkms]o|(\w)\1*|./g
const unescapedLatinCharacterRegExp = /[a-zA-Z]/

export function format<DateType extends Date>(
  dirtyDate: DateType | number,
  formatStr: string,
  options?: FormatOptions
): string {
  const localize = options?.locale?.localize ?? defaultLocale.localize

  if (!localize) {
    throw new RangeError('locale must contain localize property')
  }

  const originalDate = toDate(dirtyDate)

  if (!isValid(originalDate)) {
    throw new RangeError('Invalid time value')
  }

  const result = formatStr
    .match(formattingTokensRegExp)!
    .map(function (substring) {
      const firstCharacter = substring[0]
      const formatter = formatters[firstCharacter] as Nullable<Formatter>
      if (formatter) {
        if (isProtectedWeekYearToken(substring)) {
          throwProtectedError(substring, formatStr, String(dirtyDate))
        }
        if (isProtectedDayOfYearToken(substring)) {
          throwProtectedError(substring, formatStr, String(dirtyDate))
        }

        return formatter(originalDate, substring, localize)
      }

      if (firstCharacter.match(unescapedLatinCharacterRegExp)) {
        throw new RangeError(
          'Format string contains an unescaped latin alphabet character `' +
            firstCharacter +
            '`'
        )
      }

      return substring
    })
    .join('')

  return result
}
