import type { Locale } from '../type'
import localize from './fns/localize'

export const locale: Locale = {
  code: 'zh-CN',
  localize: localize,
}
