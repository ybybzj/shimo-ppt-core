export type Era = 0 | 1

export type Quarter = 1 | 2 | 3 | 4

export type Day = 0 | 1 | 2 | 3 | 4 | 5 | 6

export type Month = 0 | 1 | 2 | 3 | 4 | 5 | 6 | 7 | 8 | 9 | 10 | 11

export type Unit =
  | 'second'
  | 'minute'
  | 'hour'
  | 'day'
  | 'dayOfYear'
  | 'date'
  | 'week'
  | 'month'
  | 'quarter'
  | 'year'

export type FormatDistanceToken =
  | 'lessThanXSeconds'
  | 'xSeconds'
  | 'halfAMinute'
  | 'lessThanXMinutes'
  | 'xMinutes'
  | 'aboutXHours'
  | 'xHours'
  | 'xDays'
  | 'aboutXWeeks'
  | 'xWeeks'
  | 'aboutXMonths'
  | 'xMonths'
  | 'aboutXYears'
  | 'xYears'
  | 'overXYears'
  | 'almostXYears'

export type FormatDistanceLocale<Value> = {
  [token in FormatDistanceToken]: Value
}
export interface FormatDistanceFnOptions {
  addSuffix?: boolean
  comparison?: -1 | 0 | 1
}

export type FormatDistanceFn = (
  token: FormatDistanceToken,
  count: number,
  options?: FormatDistanceFnOptions
) => string

export type FormatRelativeToken =
  | 'lastWeek'
  | 'yesterday'
  | 'today'
  | 'tomorrow'
  | 'nextWeek'
  | 'other'

export interface FormatRelativeFnOptions {
  weekStartsOn?: Day
  locale?: Locale
}

export type FormatRelativeFn = <DateType extends Date>(
  token: FormatRelativeToken,
  date: DateType,
  baseDate: DateType,
  options?: FormatRelativeFnOptions
) => string

export type LocaleDayPeriod =
  | 'am'
  | 'pm'
  | 'midnight'
  | 'noon'
  | 'morning'
  | 'afternoon'
  | 'evening'
  | 'night'
export type LocaleUnit = Era | Quarter | Month | Day | LocaleDayPeriod

type LocalizeEraValues = readonly [string, string]
type LocalizeQuarterValues = readonly [string, string, string, string]
type LocalizeDayValues = readonly [
  string,
  string,
  string,
  string,
  string,
  string,
  string
]

type LocalizeMonthValues = readonly [
  string,
  string,
  string,
  string,
  string,
  string,
  string,
  string,
  string,
  string,
  string,
  string
]
export type LocalizeUnitValues<Unit extends LocaleUnit> =
  Unit extends LocaleDayPeriod
    ? Record<LocaleDayPeriod, string>
    : Unit extends Era
    ? LocalizeEraValues
    : Unit extends Quarter
    ? LocalizeQuarterValues
    : Unit extends Day
    ? LocalizeDayValues
    : Unit extends Month
    ? LocalizeMonthValues
    : never

export type LocalizeUnitValuesIndex<Values extends LocalizeUnitValues<any>> =
  Values extends Record<LocaleDayPeriod, string>
    ? string
    : Values extends LocalizeEraValues
    ? Era
    : Values extends LocalizeQuarterValues
    ? Quarter
    : Values extends LocalizeDayValues
    ? Day
    : Values extends LocalizeMonthValues
    ? Month
    : never

export type LocalizeUnitIndex<Unit extends LocaleUnit | number> =
  Unit extends LocaleUnit
    ? LocalizeUnitValuesIndex<LocalizeUnitValues<Unit>>
    : number

export type BuildLocalizeFnArgCallback<Result extends LocaleUnit | number> = (
  value: Result
) => LocalizeUnitIndex<Result>

export type LocalePatternWidth =
  | 'narrow'
  | 'short'
  | 'abbreviated'
  | 'wide'
  | 'any'

export type LocalizeFn<
  Result extends LocaleUnit | number,
  ArgCallback extends BuildLocalizeFnArgCallback<Result> | undefined = undefined
> = (
  value: ArgCallback extends undefined
    ? Result
    : Result extends Quarter
    ? Quarter
    : LocalizeUnitIndex<Result>,
  options?: {
    width?: LocalePatternWidth
    context?: 'formatting' | 'standalone'
    unit?: Unit
  }
) => string

export interface Localize {
  ordinalNumber: LocalizeFn<
    number,
    BuildLocalizeFnArgCallback<number> | undefined
  >
  era: LocalizeFn<Era, undefined>
  quarter: LocalizeFn<Quarter, BuildLocalizeFnArgCallback<Quarter>>
  month: LocalizeFn<Month, undefined>
  day: LocalizeFn<Day, undefined>
  dayPeriod: LocalizeFn<LocaleDayPeriod, undefined>
}

export type FormatLongWidth = 'full' | 'long' | 'medium' | 'short' | 'any'
export interface FormatLongFnOptions {
  width?: FormatLongWidth
}
export type FormatLongFn = (options: FormatLongFnOptions) => string

export interface FormatLong {
  date: FormatLongFn
  time: FormatLongFn
  dateTime: FormatLongFn
}

export type MatchFn<Result, ExtraOptions = Record<string, unknown>> = (
  str: string,
  options?: {
    width?: LocalePatternWidth
    valueCallback?: MatchValueCallback<string, Result>
  } & ExtraOptions
) => { value: Result; rest: string } | null

export type LocaleOrdinalUnit =
  | 'second'
  | 'minute'
  | 'hour'
  | 'day'
  | 'week'
  | 'month'
  | 'quarter'
  | 'year'
  | 'date'
  | 'dayOfYear'

export interface Match {
  ordinalNumber: MatchFn<
    number,
    {
      unit: LocaleOrdinalUnit
    }
  >
  era: MatchFn<Era>
  quarter: MatchFn<Quarter>
  month: MatchFn<Month>
  day: MatchFn<Day>
  dayPeriod: MatchFn<LocaleDayPeriod>
}

/**
 * FirstWeekContainsDate is used to determine which week is the first week of the year, based on what day the January, 1 is in that week.
 *
 * The day in that week can only be 1 (Monday) or 4 (Thursday).
 *
 * Please see https://en.wikipedia.org/wiki/Week#Week_numbering for more information.
 */
export type FirstWeekContainsDate = 1 | 4

export interface LocaleOptions {
  weekStartsOn?: Day
  firstWeekContainsDate?: FirstWeekContainsDate
}

export interface Locale {
  code: string
  localize: Localize

  formatDistance?: FormatDistanceFn
  formatRelative?: FormatRelativeFn
  formatLong?: FormatLong
  match?: Match
  options?: LocaleOptions
}

export type MatchPatterns<DefaultWidth extends LocalePatternWidth> = {
  [pattern in LocalePatternWidth]?: RegExp
} & { [key in DefaultWidth]: RegExp }

export type ParsePatterns<
  Result extends LocaleUnit,
  DefaultWidth extends LocalePatternWidth
> = {
  [pattern in LocalePatternWidth]?: ParsePattern<Result>
} & { [key in DefaultWidth]: ParsePattern<Result> }

export type ParsePattern<Result extends LocaleUnit> =
  Result extends LocaleDayPeriod
    ? Record<LocaleDayPeriod, RegExp>
    : Result extends Quarter
    ? readonly [RegExp, RegExp, RegExp, RegExp]
    : Result extends Era
    ? readonly [RegExp, RegExp]
    : Result extends Day
    ? readonly [RegExp, RegExp, RegExp, RegExp, RegExp, RegExp, RegExp]
    : Result extends Month
    ? readonly [
        RegExp,
        RegExp,
        RegExp,
        RegExp,
        RegExp,
        RegExp,
        RegExp,
        RegExp,
        RegExp,
        RegExp,
        RegExp,
        RegExp
      ]
    : never

export type MatchValueCallback<Arg, Result> = (value: Arg) => Result

export interface BuildMatchFnArgs<
  Result extends LocaleUnit,
  DefaultMatchWidth extends LocalePatternWidth,
  DefaultParseWidth extends LocalePatternWidth
> {
  matchPatterns: MatchPatterns<DefaultMatchWidth>
  defaultMatchWidth: DefaultMatchWidth
  parsePatterns: ParsePatterns<Result, DefaultParseWidth>
  defaultParseWidth: DefaultParseWidth
  valueCallback?: MatchValueCallback<
    Result extends LocaleDayPeriod ? string : number,
    Result
  >
}
