import { Locale } from '../type'
import localize from './fns/localize'

export const locale: Locale = {
  code: 'en-US',
  localize: localize,
}
