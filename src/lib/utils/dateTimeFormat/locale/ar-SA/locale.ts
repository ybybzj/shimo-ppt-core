import { Locale } from '../type'
import localize from './fns/localize'

export const locale: Locale = {
  code: 'ar-SA',
  localize: localize,
}
