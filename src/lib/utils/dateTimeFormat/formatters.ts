import { Day, Localize, Month } from './locale/type'

export type Formatter = (
  date: Date,
  token: string,
  localize: Localize
) => string

// first char of fomatter string:
type formatterKey = 'M' | 'd' | 'y' | 'E' | 'H' | 'h' | 'm' | 's' | 'a'

export const formatters: { [k in formatterKey]: Formatter } = {
  // Year
  ['y']: function (date: Date, token: string, localize: Localize) {
    // Ordinal number
    if (token === 'yo') {
      const signedYear = date.getFullYear()
      // Returns 1 for 1 BC (which is year 0 in JavaScript)
      const year = signedYear > 0 ? signedYear : 1 - signedYear
      return localize.ordinalNumber(year, { unit: 'year' })
    }

    return lightFormatters.y(date, token)
  },

  // Month
  ['M']: function (date, token, localize) {
    const month = date.getMonth() as Month
    switch (token) {
      case 'M':
      case 'MM':
        return lightFormatters.M(date, token)
      // 1st, 2nd, ..., 12th
      case 'Mo':
        return localize.ordinalNumber(month + 1, { unit: 'month' })
      // Jan, Feb, ..., Dec
      case 'MMM':
        return localize.month(month, {
          width: 'abbreviated',
          context: 'formatting',
        })
      // J, F, ..., D
      case 'MMMMM':
        return localize.month(month, { width: 'narrow', context: 'formatting' })
      // January, February, ..., December
      case 'MMMM':
      default:
        return localize.month(month, { width: 'wide', context: 'formatting' })
    }
  },

  // Day of the month
  ['d']: function (date, token, localize) {
    if (token === 'do') {
      return localize.ordinalNumber(date.getDate(), { unit: 'date' })
    }

    return lightFormatters.d(date, token)
  },

  // Day of week
  ['E']: function (date, token, localize) {
    const dayOfWeek = date.getDay() as Day
    switch (token) {
      // Tue
      case 'E':
      case 'EE':
      case 'EEE':
        return localize.day(dayOfWeek, {
          width: 'abbreviated',
          context: 'formatting',
        })
      // T
      case 'EEEEE':
        return localize.day(dayOfWeek, {
          width: 'narrow',
          context: 'formatting',
        })
      // Tu
      case 'EEEEEE':
        return localize.day(dayOfWeek, {
          width: 'short',
          context: 'formatting',
        })
      // Tuesday
      case 'EEEE':
      default:
        return localize.day(dayOfWeek, { width: 'wide', context: 'formatting' })
    }
  },

  // AM or PM
  ['a']: function (date, token, localize) {
    const hours = date.getHours()
    const dayPeriodEnumValue = hours / 12 >= 1 ? 'pm' : 'am'

    switch (token) {
      case 'a':
      case 'aa':
        return localize.dayPeriod(dayPeriodEnumValue, {
          width: 'abbreviated',
          context: 'formatting',
        })
      case 'aaa':
        return localize
          .dayPeriod(dayPeriodEnumValue, {
            width: 'abbreviated',
            context: 'formatting',
          })
          .toLowerCase()
      case 'aaaaa':
        return localize.dayPeriod(dayPeriodEnumValue, {
          width: 'narrow',
          context: 'formatting',
        })
      case 'aaaa':
      default:
        return localize.dayPeriod(dayPeriodEnumValue, {
          width: 'wide',
          context: 'formatting',
        })
    }
  },

  // Hour [1-12]
  ['h']: function (date, token, localize) {
    if (token === 'ho') {
      let hours = date.getHours() % 12
      if (hours === 0) hours = 12
      return localize.ordinalNumber(hours, { unit: 'hour' })
    }

    return lightFormatters.h(date, token)
  },

  // Hour [0-23]
  ['H']: function (date, token, localize) {
    if (token === 'Ho') {
      return localize.ordinalNumber(date.getHours(), { unit: 'hour' })
    }

    return lightFormatters.H(date, token)
  },

  // Minute
  ['m']: function (date, token, localize) {
    if (token === 'mo') {
      return localize.ordinalNumber(date.getMinutes(), { unit: 'minute' })
    }

    return lightFormatters.m(date, token)
  },

  // Second
  ['s']: function (date, token, localize) {
    if (token === 'so') {
      return localize.ordinalNumber(date.getSeconds(), { unit: 'second' })
    }

    return lightFormatters.s(date, token)
  },
}

function addLeadingZeros(number: number, targetLength: number): string {
  const sign = number < 0 ? '-' : ''
  let output = Math.abs(number).toString()
  while (output.length < targetLength) {
    output = '0' + output
  }
  return sign + output
}

const lightFormatters = {
  // Year
  y(date: Date, token: string): string {
    // From http://www.unicode.org/reports/tr35/tr35-31/tr35-dates.html#Date_Format_tokens
    // | Year     |     y | yy |   yyy |  yyyy | yyyyy |
    // |----------|-------|----|-------|-------|-------|
    // | AD 1     |     1 | 01 |   001 |  0001 | 00001 |
    // | AD 12    |    12 | 12 |   012 |  0012 | 00012 |
    // | AD 123   |   123 | 23 |   123 |  0123 | 00123 |
    // | AD 1234  |  1234 | 34 |  1234 |  1234 | 01234 |
    // | AD 12345 | 12345 | 45 | 12345 | 12345 | 12345 |

    const signedYear = date.getFullYear()
    // Returns 1 for 1 BC (which is year 0 in JavaScript)
    const year = signedYear > 0 ? signedYear : 1 - signedYear
    return addLeadingZeros(token === 'yy' ? year % 100 : year, token.length)
  },

  // Month
  M(date: Date, token: string): string {
    const month = date.getMonth()
    return token === 'M' ? String(month + 1) : addLeadingZeros(month + 1, 2)
  },

  // Day of the month
  d(date: Date, token: string): string {
    return addLeadingZeros(date.getDate(), token.length)
  },

  // AM or PM
  a(date: Date, token: string): string {
    const dayPeriodEnumValue = date.getHours() / 12 >= 1 ? 'pm' : 'am'

    switch (token) {
      case 'a':
      case 'aa':
        return dayPeriodEnumValue.toUpperCase()
      case 'aaa':
        return dayPeriodEnumValue
      case 'aaaaa':
        return dayPeriodEnumValue[0]
      case 'aaaa':
      default:
        return dayPeriodEnumValue === 'am' ? 'a.m.' : 'p.m.'
    }
  },

  // Hour [1-12]
  h(date: Date, token: string): string {
    return addLeadingZeros(date.getHours() % 12 || 12, token.length)
  },

  // Hour [0-23]
  H(date: Date, token: string): string {
    return addLeadingZeros(date.getHours(), token.length)
  },

  // Minute
  m(date: Date, token: string): string {
    return addLeadingZeros(date.getMinutes(), token.length)
  },

  // Second
  s(date: Date, token: string): string {
    return addLeadingZeros(date.getSeconds(), token.length)
  },

  // Fraction of second
  S(date: Date, token: string): string {
    const numberOfDigits = token.length
    const milliseconds = date.getMilliseconds()
    const fractionalSeconds = Math.floor(
      milliseconds * Math.pow(10, numberOfDigits - 3)
    )
    return addLeadingZeros(fractionalSeconds, token.length)
  },
}
