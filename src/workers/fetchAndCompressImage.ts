import { MAX_IMAGE_DATA_SIZE } from '../images/const'
import { CompressActionMessage, FetchActinMessage } from '../images/type'
const worker = self as unknown as Worker

function fetchImgData(src: string, imageSrcOrigin: string) {
  return fetch(src, {
    mode: 'cors',
    redirect: 'follow',
    credentials: 'same-origin',
    referrer: `${imageSrcOrigin}`,
  }).then((res) => {
    return res.blob()
  })
}

interface CompressedResult {
  bitmap: ImageBitmap
  isCompressed: boolean
  scale?: number
}

function compressImgData(
  blob: Blob,
  {
    src,
    isDebug,
    needCompress,
    sizeHint,
  }: { src: string; isDebug: boolean; needCompress: boolean; sizeHint?: number }
): Promise<CompressedResult> {
  const bitmapP = createImageBitmap(blob, {
    premultiplyAlpha: 'none',
    colorSpaceConversion: 'none',
  })

  return bitmapP.then((bitmap) => {
    const result = needCompress
      ? _compressBitmap(bitmap, sizeHint)
      : { bitmap, isCompressed: false }

    if (isDebug && result.isCompressed) {
      console.log('[' + src + ']: is compressed')
    }
    return result
  })
}

function _compressBitmap(
  bitmap: ImageBitmap,
  sizeHint?: number
): CompressedResult {
  const width = bitmap.width
  const height = bitmap.height
  const sizeOfBitmap = 4 * width * height
  const sizeLimit = sizeHint
    ? (MAX_IMAGE_DATA_SIZE * sizeHint) >> 0
    : MAX_IMAGE_DATA_SIZE

  if (sizeOfBitmap <= sizeLimit) {
    return { bitmap, isCompressed: false }
  }

  const scale = Math.sqrt(sizeLimit / sizeOfBitmap)

  const w = (width * scale + 0.5) >> 0
  const h = (height * scale + 0.5) >> 0
  const canvas = new OffscreenCanvas(w, h)
  const ctx = canvas.getContext('2d')!
  ctx.drawImage(bitmap, 0, 0, w, h)
  bitmap.close()
  const compressed = canvas.transferToImageBitmap()
  return { bitmap: compressed, isCompressed: true, scale: scale }
}

function onMessage(e) {
  const msg: CompressActionMessage | FetchActinMessage = e.data
  const action = msg['action']
  switch (action) {
    case 'fetch': {
      const src = msg['src']
      const imageSrcOrigin = msg['imageSrcOrigin']
      if (src == null || src.length <= 0) {
        worker.postMessage({
          ['action']: action,
          ['src']: src,
          ['success']: false,
          ['error']: new Error('src is empty'),
        })
        return
      }
      fetchImgData(src, imageSrcOrigin)
        .then((blob) => {
          worker.postMessage({
            ['action']: action,
            ['src']: src,
            ['success']: true,
            ['data']: blob,
          })
        })
        .catch((e) => {
          postMessage({
            ['action']: action,
            ['src']: src,
            ['success']: false,
            ['error']: e,
          })
        })
      break
    }
    case 'compress': {
      const imgData = msg['imgData']
      const src = msg['src']
      const needCompress = msg['needCompress']
      const isDebug = msg['isDebug']
      compressImgData(imgData, { src, needCompress, isDebug })
        .then((result) => {
          worker.postMessage({
            ['action']: action,
            ['src']: src,
            ['success']: true,
            ['isCompressed']: result.isCompressed,
            ['scale']: result.scale ?? 1,
            ['data']: result.bitmap,
          })
        })
        .catch((e) => {
          postMessage({
            ['action']: action,
            ['src']: src,
            ['success']: false,
            ['error']: e,
          })
        })
      break
    }
  }
}

worker.onmessage = onMessage
