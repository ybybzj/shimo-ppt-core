import { Nullable } from '../../../liber/pervasive'
import { SelectedElementType } from '../../core/common/const/attrs'
import { throttle } from '../../common/utils'
import { gSpecialPasteUtil } from '../../copyPaste/gSpecialPasteUtil'
import { ParaPr } from '../../core/textAttributes/ParaPr'
import { TextPr } from '../../core/textAttributes/TextPr'
import { getDocPrsOfSlideElements } from '../../core/utilities/shape/getters'
import { CollaborationShapeInfo } from '../../exports/view'
import { InstanceType } from '../../core/instanceTypes'
import {
  Evt_canDecreaseIndent,
  Evt_canIncreaseIndent,
  Evt_onCanAddHyperlink,
  Evt_onCanGroup,
  Evt_onCanUnGroup,
  Evt_onCountPages,
  Evt_onFocusObject,
  Evt_onPresentationSize,
  Evt_onUpdateCollaborationSelection,
  Evt_onVert,
  Evt_onVerticalTextAlign,
} from '../events/EventNames'
import { IEditorKit } from '../type'
import { emitImageProp } from './image'
import { emitParaProps } from './paraPr'
import {
  clearSelectedElementsInfoStack,
  getSelectedElementsInfoStack,
} from './selectedElementsInfoStack'
import { emitShapeProp } from './shape'
import { emitSlideProp } from './slide'
import { emitTableProp } from './table'
import { emitTextProps } from './textPr'
import { getSpPropsFromSpArr } from './util'

let executionCount = 0
export function syncInterfaceState(editorKit: IEditorKit) {
  executionCount++
  if (executionCount === 1) {
    realSyncInterfaceState(editorKit)
  } else {
    throttleSyncInterfaceState(editorKit)
  }
}

const throttleSyncInterfaceState = throttle(
  (editorKit) => {
    realSyncInterfaceState(editorKit)
    executionCount = 0
  },
  100,
  true
)

function realSyncInterfaceState(editorKit: IEditorKit) {
  editorKit.resourcesLoadManager.turnOffCollectAssets()
  const presentation = editorKit.presentation
  const handler = editorKit.editorUI.editorHandler
  if (presentation.lockInterfaceEvents) {
    editorKit.resourcesLoadManager.turnOnCollectAssets()
    return
  }
  editorKit.emitEvent(Evt_onCountPages, presentation.slides.length)
  clearSelectedElementsInfoStack()
  const currentSlide = presentation.slides[presentation.currentSlideIndex]
  if (currentSlide) {
    emitSlideProp(editorKit, currentSlide)
    if (!handler) {
      emitEndSyncInterface(editorKit)
      editorKit.resourcesLoadManager.turnOnCollectAssets()
      return
    }
    const { selectedElements } = presentation.selectionState.selectionStatusInfo
    const textTarget = presentation.getCurrentTextTarget(true)
    const spProps = getSpPropsFromSpArr(presentation, selectedElements)
    if (spProps.imageShapeProps && !presentation.isNotesFocused) {
      emitImageProp(spProps.imageShapeProps, spProps.imageIdArr)
    }

    if (spProps.shapeProps && !presentation.isNotesFocused) {
      emitShapeProp(editorKit, spProps.shapeProps, spProps.shapeIdArr)
      editorKit.emitEvent(
        Evt_onVerticalTextAlign,
        spProps.shapeProps.verticalTextAlign
      )
      editorKit.emitEvent(Evt_onVert, spProps.shapeProps.verticalTextAlign)
    }

    if (spProps.tableProps && !presentation.isNotesFocused) {
      emitTableProp(spProps.tableProps, spProps.tableIdArr)
    }
    if (textTarget) {
      textTarget.syncEditorUIState()
    } else {
      if (selectedElements && selectedElements.length) {
        const { paraPrs, textPrs } = getDocPrsOfSlideElements(selectedElements)
        const comparedParaPr = paraPrs.reduce((res, cur) => {
          if (!res) {
            return cur
          } else {
            return res.intersect(cur)
          }
        }, null as Nullable<ParaPr>)
        const comparedTextPr = textPrs.reduce((res, cur) => {
          if (!res) {
            return cur
          } else {
            return res.subDiff(cur)
          }
        }, null as Nullable<TextPr>)
        if (comparedParaPr) {
          emitParaProps(editorKit, comparedParaPr)
        }
        if (comparedTextPr) {
          emitTextProps(editorKit, comparedTextPr)
        }
      }
    }
    editorKit.resourcesLoadManager.turnOnCollectAssets()
  }

  emitEndSyncInterface(editorKit)
  editorKit.emitEvent(Evt_onCanAddHyperlink, handler.canInsertHyperlink(false))
  editorKit.emitEvent(
    Evt_onPresentationSize,
    presentation.width,
    presentation.height
  )
  editorKit.emitEvent(
    Evt_canIncreaseIndent,
    presentation.canChangeParagraphIndentLevel(true)
  )
  editorKit.emitEvent(
    Evt_canDecreaseIndent,
    presentation.canChangeParagraphIndentLevel(false)
  )
  editorKit.emitEvent(Evt_onCanGroup, presentation.canGroup())
  editorKit.emitEvent(Evt_onCanUnGroup, presentation.canUnGroup())
  gSpecialPasteUtil.updateUI()
}

export function getSelectedElementsInfo(
  editorKit: IEditorKit,
  isUpdate = false
) {
  if (true === isUpdate) {
    syncInterfaceState(editorKit)
  }
  return getSelectedElementsInfoStack()
}

export function emitEndSyncInterface(editorKit: IEditorKit) {
  const { presentation, editorHandler: handler } = editorKit.editorUI
  let noParagraph = true
  let noText = true
  let noObject = true
  getSelectedElementsInfoStack().forEach(({ type, value }) => {
    const { Paragraph, Image, Shape, Table } = SelectedElementType
    if (type === Paragraph) {
      noParagraph = false
      noText = false
    } else if (type === Image || type === Shape || type === Table) {
      noObject = false
      if (type === Table || (type === Shape && !value.isFromImage)) {
        noText = false
      }
    }
  })

  editorKit.emitEvent(Evt_onFocusObject, {
    noParagraph,
    noText,
    noObject,
  })
  const { slides, currentSlideIndex } = presentation
  const slideRef = slides[currentSlideIndex]?.getRefId()
  const selectedSps = handler.getSelectedSlideElements()
  const shapeInfos: CollaborationShapeInfo[] = selectedSps.map((sp) => {
    const spRef = sp.getRefId()
    if (sp.instanceType === InstanceType.GraphicFrame) {
      const selectInfo = sp.graphicObject?.getSelectionInfo()
      if (selectInfo?.type === 'cell') {
        const tableInfo = {
          ['rowIndex']: selectInfo.cellInfo.rowIndex,
          ['cellIndex']: selectInfo.cellInfo.cellIndex,
        }
        return { ['spRef']: spRef, ['tableInfo']: tableInfo }
      }
    }
    return { spRef }
  })
  const selectionInfo = { ['slideRef']: slideRef, ['shapeInfos']: shapeInfos }
  editorKit.emitEvent(Evt_onUpdateCollaborationSelection, selectionInfo)
}
