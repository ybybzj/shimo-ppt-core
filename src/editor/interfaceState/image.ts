import { SelectedElementType } from '../../core/common/const/attrs'
import { SlideElementOptions } from '../../core/properties/options'
import { ImageProps, ImageSubTypes } from '../../exports/type'
import { addSelectedElementInfo } from './selectedElementsInfoStack'

export const emitImageProp = (pr: SlideElementOptions, ids: string[]) => {
  const imageProps = getImageProps(pr)
  if (imageProps) {
    addSelectedElementInfo({
      type: SelectedElementType.Image,
      value: imageProps,
      ids,
    })
  }
}

export const getImageProps = (
  pr: SlideElementOptions
): ImageProps | undefined => {
  if (!pr) {
    return
  }
  const {
    id,
    subTypes,
    imageUrl,
    encryptImageUrl,
    transparent,
    hyperlink,
    media,
    attachment,
  } = pr
  return {
    ['id']: id!,
    ['subTypes']: subTypes as ImageSubTypes[],
    ['url']: imageUrl!,
    ['encryptUrl']: encryptImageUrl,
    ['linkUrl']: hyperlink!,
    ['transparent']: transparent!,
    ['media']: media
      ? {
          ['type']: media.type,
          ['url']: media.media,
          ['encryptUrl']: media.encryptUrl,
        }
      : undefined,
    ['attachment']: attachment
      ? {
          ['url']: attachment?.url,
          ['encryptUrl']: attachment?.encryptUrl,
        }
      : undefined,
  }
}
