import { SelectedElementInfo } from './type'

let selectedElementsInfoStack: SelectedElementInfo[] = []
export const getSelectedElementsInfoStack = () => {
  return selectedElementsInfoStack
}
export const clearSelectedElementsInfoStack = () => {
  selectedElementsInfoStack = []
}
export const addSelectedElementInfo = (info: SelectedElementInfo) => {
  selectedElementsInfoStack.push(info)
}
export const addSelectedElementInfoWithCheckLast = (
  info: SelectedElementInfo
) => {
  const len = selectedElementsInfoStack.length
  if (len > 0) {
    if (selectedElementsInfoStack[len - 1].type === info.type) {
      selectedElementsInfoStack[len - 1].value = info.value
      return
    }
  }
  addSelectedElementInfo(info)
}
