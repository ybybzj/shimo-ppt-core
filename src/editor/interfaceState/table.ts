import { Nullable } from '../../../liber/pervasive'
import { ShdType, SelectedElementType } from '../../core/common/const/attrs'
import { Factor_pt_to_mm } from '../../core/common/const/unit'
import { TCBorderValue } from '../../core/common/const/table'
import {
  BackgroundInTableOptions,
  BordersPropsOptions,
  TablePropOptions,
} from '../../core/properties/tableProp'
import { TableCellBorderOptions } from '../../core/Table/attrs/CellBorder'
import { getColorProps, getTransparentFromAlphaMod } from './common'
import { addSelectedElementInfo } from './selectedElementsInfoStack'

export const emitTableProp = (
  pr: Nullable<TablePropOptions>,
  ids: string[]
) => {
  const tableProps = getTableProps(pr)
  if (tableProps) {
    addSelectedElementInfo({
      type: SelectedElementType.Table,
      value: tableProps,
      ids,
    })
  }
}

export const getTableProps = (pr: Nullable<TablePropOptions>) => {
  if (!pr) {
    return
  }
  const { cellsBackground, cellBorders, tableLook } = pr
  return {
    ['cellsBackground']: cellsBackground
      ? getCellsBackground(cellsBackground)
      : undefined,
    ['borders']: getBordersProps(cellBorders),
    ['rtl']: tableLook?.rtl,
  }
}

export function getBordersProps(borders: Nullable<BordersPropsOptions>) {
  const ret = {}
  if (borders) {
    ret['left'] = borders.left ? getBorderProps(borders.left) : null
    ret['right'] = borders.right ? getBorderProps(borders.right) : null
    ret['top'] = borders.top ? getBorderProps(borders.top) : null
    ret['bottom'] = borders.bottom ? getBorderProps(borders.bottom) : null
    ret['insideh'] = borders.insideH ? getBorderProps(borders.insideH) : null
    ret['insidev'] = borders.insideV ? getBorderProps(borders.insideV) : null
  }
  return ret
}

export function getBorderProps(border: TableCellBorderOptions) {
  const { color, size, value } = border
  return {
    ['color']: getColorProps(color),
    ['transparent']: getTransparentFromAlphaMod(color),
    ['size']: typeof size === 'number' ? size / Factor_pt_to_mm : size,
    ['visible']: value !== TCBorderValue.None,
  }
}

export function getCellsBackground(cellsBackground: BackgroundInTableOptions) {
  const { color, value, fillEffects } = cellsBackground
  const rgbFromFill = fillEffects ? fillEffects.getRGBAColor() : null
  const transparent = fillEffects ? fillEffects.transparent : null
  return color
    ? {
        ['color']:
          rgbFromFill == null
            ? getColorProps(color)!
            : {
                ['r']: rgbFromFill.r,
                ['g']: rgbFromFill.g,
                ['b']: rgbFromFill.b,
              },
        // in percentage
        ['transparent']:
          transparent != null
            ? (((255 - transparent) * 100) / 255) >> 0
            : getTransparentFromAlphaMod(color)!,
        ['visible']: value !== ShdType.Nil,
      }
    : undefined
}
