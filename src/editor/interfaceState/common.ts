import { Nullable } from '../../../liber/pervasive'
import {
  FillKIND,
  FillGradPathType,
  FillBlipType,
  StrokeType,
  DashStyle,
} from '../../core/common/const/attrs'
import { isNumber } from '../../common/utils'
import { CHexColorOptions } from '../../core/color/CHexColor'
import {
  ComputedRGBAColor,
  computedRGBColorFromComplexColor,
} from '../../core/color/ComputedRGBAColor'
import { SlideTransition } from '../../core/Slide/SlideTransition'
import { ShapeFill, gradType } from '../../exports/type'
import { InstanceType } from '../../core/instanceTypes'
import { FillEffects } from '../../core/SlideElement/attrs/fill/fill'
import { GradFill } from '../../core/SlideElement/attrs/fill/GradFill'
import { Ln } from '../../core/SlideElement/attrs/line'
import { StrokeOptions } from '../../core/properties/options'
import { ScaleOfPPTXSizes } from '../../core/common/const/unit'
import { LineEndType } from '../../core/SlideElement/const'
import { RGBClr } from '../../core/color/type'

export function getFillProps(fillEffect: Nullable<FillEffects>) {
  let fillProps: ShapeFill | null = null
  const fill = fillEffect?.fill
  if (!fill || fill?.type === FillKIND.NOFILL) {
    fillProps = { ['type']: FillKIND.NOFILL, ['fill']: null }
  } else if (fill.type === FillKIND.SOLID) {
    const color = computedRGBColorFromComplexColor(fill.color)
    fillProps = {
      ['type']: FillKIND.SOLID,
      ['fill']: {
        ['color']: getColorProps(color)!,
      },
      ['transparent']: getTransparentFromAlphaMod(color),
    }
  } else if (fill.type === FillKIND.BLIP) {
    const { tile, imageSrcId } = fill
    fillProps = {
      ['type']: FillKIND.BLIP,
      ['fill']: {
        ['type']: tile == null ? FillBlipType.STRETCH : FillBlipType.TILE,
        ['url']: imageSrcId,
      },
      ['transparent']: fill.getTransparent(), // TODO:
    }
  } else if (fill.type === FillKIND.GRAD) {
    const { gsList, ln, path } = fill
    fillProps = {
      ['type']: FillKIND.GRAD,
      ['gradType']: getGradTypeFromFill(fill),
      ['angle']: ln && isNumber(ln?.angle) ? ln.angle / 60000 : undefined,
      ['direction']: path?.getDirection(),
      ['gsLst']: gsList.map((gs) => {
        const color = computedRGBColorFromComplexColor(gs.color)
        return { ['color']: getColorProps(color)!, pos: gs.pos / 1000 }
      }),
    }
  }
  return fillProps
}

export function getStrokeProps(ln: Ln): StrokeOptions | undefined {
  if (!ln || !ln.fillEffects || ln.fillEffects.fill == null) {
    return
  }

  const ret: StrokeOptions = {}
  const _fill = ln.fillEffects.fill
  if (_fill != null) {
    switch (_fill.type) {
      case FillKIND.BLIP: {
        break
      }
      case FillKIND.SOLID: {
        ret.color = computedRGBColorFromComplexColor(_fill.color)
        ret.type = StrokeType.COLOR
        break
      }
      case FillKIND.GRAD: {
        const _c = _fill.gsList
        if (_c.length !== 0) {
          ret.color = computedRGBColorFromComplexColor(_fill.gsList[0].color)
          ret.type = StrokeType.COLOR
        }

        break
      }
      case FillKIND.PATT: {
        ret.color = computedRGBColorFromComplexColor(_fill.fgClr)
        ret.type = StrokeType.COLOR
        break
      }
      case FillKIND.NOFILL: {
        ret.color = null
        ret.type = StrokeType.NONE
        break
      }
      default: {
        break
      }
    }
  }

  ret.width = ln.w == null ? 12700 : ln.w >> 0
  ret.width /= ScaleOfPPTXSizes

  if (ln.cap != null) {
    ret.lineCap = ln.cap
  }

  if (ln.Join != null) {
    ret.lineJoin = ln.Join.type
  }

  if (ln.headEnd != null) {
    ret.beginOfLineEndType =
      ln.headEnd.type == null ? LineEndType.None : ln.headEnd.type

    const _len = null == ln.headEnd.len ? 1 : 2 - ln.headEnd.len
    const _w = null == ln.headEnd.w ? 1 : 2 - ln.headEnd.w

    ret.beginOfLineEndWidth = _w * 3 + _len
  } else {
    ret.beginOfLineEndType = LineEndType.None
  }

  if (ln.tailEnd != null) {
    ret.endOfLineEndType =
      ln.tailEnd.type == null ? LineEndType.None : ln.tailEnd.type

    const _len = null == ln.tailEnd.len ? 1 : 2 - ln.tailEnd.len
    const _w = null == ln.tailEnd.w ? 1 : 2 - ln.tailEnd.w

    ret.endOflineEndWidth = _w * 3 + _len
  } else {
    ret.endOflineEndWidth = LineEndType.None
  }
  if (isNumber(ln.prstDash)) {
    ret.prstDash = ln.prstDash
  } else if (ln.prstDash == null) {
    ret.prstDash = DashStyle.solid
  }
  return ret
}

export function getGradTypeFromFill(fill: GradFill): gradType {
  if (fill.ln) {
    return gradType.linear
  } else if (fill.path) {
    switch (fill.path.path) {
      case FillGradPathType.Rect:
      case FillGradPathType.Shape:
      case FillGradPathType.Circle:
      default:
        return gradType.circle
    }
  } else {
    return gradType.linear
  }
}

export function getColorProps(
  color: Nullable<CHexColorOptions | ComputedRGBAColor>
): RGBClr | null {
  const isSRGBC =
    (color as ComputedRGBAColor)?.instanceType === InstanceType.ComputedRGBColor
  const srgbC = color as ComputedRGBAColor
  const hexColor = color as CHexColorOptions
  return color
    ? {
        ['r']: isSRGBC ? srgbC.r : hexColor.r,
        ['g']: isSRGBC ? srgbC.g : hexColor.g,
        ['b']: isSRGBC ? srgbC.b : hexColor.b,
      }
    : null
}

export function getTransitionProps(timing: Nullable<SlideTransition>) {
  let res = {}
  if (timing) {
    const {
      type,
      option,
      duration,
      advanceOnMouseClick,
      advanceAfter,
      advanceDuration,
    } = timing
    res = {
      ['transitionType']: type,
      ['transitionOption']: option,
      ['transitionDuration']: duration,
      ['slideAdvanceOnMouseClick']: advanceOnMouseClick,
      ['slideAdvanceAfter']: advanceAfter,
      ['slideAdvanceDuration']: advanceDuration,
    }
  }
  return res
}

export function getTransparentFromAlphaMod(
  color: Nullable<ComputedRGBAColor | CHexColorOptions>
): number | undefined {
  const mods = color && (color as ComputedRGBAColor).mods
  if (mods) {
    const alphaMod = mods.find((mod) => mod.name === 'alpha')
    if (alphaMod) {
      return ((100000 - alphaMod.val) * 100) / 100000
    }
  }
}
