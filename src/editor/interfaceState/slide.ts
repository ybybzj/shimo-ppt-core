import { FillKIND, SelectedElementType } from '../../core/common/const/attrs'
import { intersectObjects } from '../../core/common/helpers'
import {
  audioNodesToAnimationSettings,
  timeNodesToAnimationSettings,
} from '../../core/Slide/Animation'
import { Slide } from '../../core/Slide/Slide'
import {
  SlideTransition,
  compareTiming,
} from '../../core/Slide/SlideTransition'
import { SlideProps } from '../../exports/type'
import { IEditorKit } from '../type'
import { getFillProps, getTransitionProps } from './common'
import { addSelectedElementInfoWithCheckLast } from './selectedElementsInfoStack'

export const emitSlideProp = (editorKit: IEditorKit, slide: Slide) => {
  const slideProps = getSlideProps(editorKit, slide)
  if (slideProps) {
    addSelectedElementInfoWithCheckLast({
      type: SelectedElementType.Slide,
      value: slideProps,
    })
  }
}

export const getSlideProps = (
  editorKit: IEditorKit,
  slide: Slide
): SlideProps | undefined => {
  const { editorUI, presentation } = editorKit
  if (!editorUI || !presentation || !slide) return
  const selectedSlides = getSelectedSlides(editorKit, slide)
  const firstSelectedSlide = selectedSlides[0]

  let bgFill = firstSelectedSlide.backgroundFill
    ? firstSelectedSlide.backgroundFill.clone()
    : firstSelectedSlide.backgroundFill

  for (let i = 1; i < selectedSlides.length; ++i) {
    bgFill = intersectObjects(bgFill, selectedSlides[i].backgroundFill)
    if (!bgFill) {
      break
    }
  }

  if (!bgFill) {
    editorUI.uiTextureDrawer.drawSlideImageTextureFill(null)
  } else {
    if (bgFill.fill?.type === FillKIND.BLIP) {
      editorUI.uiTextureDrawer.drawSlideImageTextureFill(bgFill.fill.imageSrcId)
    } else {
      editorUI.uiTextureDrawer.drawSlideImageTextureFill(null)
    }
  }

  let timing: SlideTransition | null = firstSelectedSlide.timing
    ? firstSelectedSlide.timing.clone()
    : firstSelectedSlide.timing
  for (let i = 1; i < selectedSlides.length; ++i) {
    timing = compareTiming(timing, selectedSlides[i].timing)
    if (!timing) {
      break
    }
  }
  timing = timing ? timing.clone() : new SlideTransition()

  // Todo: 考虑 cache
  return {
    ['background']: getFillProps(bgFill),
    ['transition']: getTransitionProps(timing),
    ['animation']: timeNodesToAnimationSettings(
      firstSelectedSlide,
      firstSelectedSlide.animation.cloneTimeNodes()
    ),
    ['audio']: audioNodesToAnimationSettings(
      firstSelectedSlide,
      firstSelectedSlide.animation.cloneAudioNodes()
    ),
  }
}

const getSelectedSlides = (editorKit: IEditorKit, slide) => {
  const { editorUI, presentation } = editorKit
  const arrOfSlides: Slide[] = []
  if (editorUI.thumbnailsManager) {
    const selectedSlideIndices = presentation.getSelectedSlideIndices()
    for (let i = 0; i < selectedSlideIndices.length; ++i) {
      if (presentation.slides[selectedSlideIndices[i]]) {
        arrOfSlides.push(presentation.slides[selectedSlideIndices[i]])
      }
    }
  } else {
    arrOfSlides.push(slide)
  }

  if (arrOfSlides.length === 0) {
    arrOfSlides.push(slide)
  }
  return arrOfSlides
}
