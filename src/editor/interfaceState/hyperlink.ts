import { SelectedElementType } from '../../core/common/const/attrs'
import { HyperlinkOptions } from '../../core/properties/options'
import { addSelectedElementInfo } from './selectedElementsInfoStack'

export function emitHyperlinkProp(pr: HyperlinkOptions) {
  const { text: text, value: value, toolTip: toolTip } = pr
  const linkProps = {
    ['text']: text,
    ['value']: value,
    ['toolTip']: toolTip,
  }

  addSelectedElementInfo({
    type: SelectedElementType.Hyperlink,
    value: linkProps,
  })
}
