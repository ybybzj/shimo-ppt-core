import { FillKIND, SelectedElementType } from '../../core/common/const/attrs'
import { convertMMToPt } from '../../common/utils'
import { FillEffects } from '../../core/SlideElement/attrs/fill/fill'
import { ShapeProps } from '../../exports/type'
import { IEditorKit } from '../type'
import {
  getColorProps,
  getFillProps,
  getStrokeProps,
  getTransparentFromAlphaMod,
} from './common'
import { addSelectedElementInfoWithCheckLast } from './selectedElementsInfoStack'
import { SlideElementOptions } from '../../core/properties/options'
import { Ln } from '../../core/SlideElement/attrs/line'
import { PresetShapeTypes } from '../../exports/graphic'

export const emitShapeProp = (
  editorKit: IEditorKit,
  pr: SlideElementOptions,
  ids: string[]
) => {
  const shapeProps = getShapeProps(editorKit, pr)
  if (shapeProps) {
    addSelectedElementInfoWithCheckLast({
      type: SelectedElementType.Shape,
      value: shapeProps,
      ids,
    })
  }
}

export const getShapeProps = (
  editorKit: IEditorKit,
  options: SlideElementOptions
): ShapeProps | undefined => {
  const { editorUI } = editorKit
  const fill = options.fill as FillEffects
  if (fill && fill.fill != null && fill.fill.type === FillKIND.BLIP) {
    editorUI.uiTextureDrawer.drawShapeImageTextureFill(fill.fill.imageSrcId)
  } else {
    editorUI.uiTextureDrawer.drawShapeImageTextureFill(null)
  }

  const { isEmptyPH, verticalTextAlign, stroke, isFromImage } = options
  const canFill = options.canFill ?? true

  const strokeOptions = getStrokeProps(stroke as Ln)

  const strokeN = strokeOptions
    ? {
        ['type']: strokeOptions.type!,
        ['width']: convertMMToPt(strokeOptions.width!),
        ['prstDash']: strokeOptions.prstDash!,
        ['color']: getColorProps(strokeOptions.color!),
        ['transparent']: getTransparentFromAlphaMod(strokeOptions.color!),
      }
    : null

  return {
    ['type']: options.type as PresetShapeTypes,
    ['stroke']: strokeN,
    ['fill']: getFillProps(fill),
    ['verticalTextAlign']: verticalTextAlign,
    ['canFill']: !!canFill,
    ['isEmptyPlaceholder']: !!isEmptyPH,
    ['isFromImage']: !!isFromImage,
  }
}
