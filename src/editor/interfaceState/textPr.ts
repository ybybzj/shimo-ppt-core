import { HightLight_None } from '../../core/common/const/unit'
import { TextPr } from '../../core/textAttributes/TextPr'
import { getThemeForEditContainer } from '../../core/utilities/presentation/getters'
import { EditorUtil } from '../../globals/editor'
import {
  Evt_onFontFamily,
  Evt_onBold,
  Evt_onItalic,
  Evt_onUnderline,
  Evt_onStrikeout,
  Evt_onFontSize,
} from '../events/EventNames'
import { IEditorKit } from '../type'

export const emitTextProps = (editorKit: IEditorKit, textPr: TextPr) => {
  if (!textPr) return
  textPr.fontFamily = textPr.fontFamily ?? { index: 0, name: '' }
  const theme = getThemeForEditContainer(editorKit.presentation)!
  const fontScheme = theme?.themeElements.fontScheme
  if (textPr.rFonts && fontScheme) {
    if (textPr.rFonts.ascii) {
      textPr.rFonts.ascii.name = fontScheme.checkFont(textPr.rFonts.ascii.name)
    }
    if (textPr.rFonts.eastAsia) {
      textPr.rFonts.eastAsia.name = fontScheme.checkFont(
        textPr.rFonts.eastAsia.name
      )
    }
    if (textPr.rFonts.hAnsi) {
      textPr.rFonts.hAnsi.name = fontScheme.checkFont(textPr.rFonts.hAnsi.name)
    }
    if (textPr.rFonts.cs) {
      textPr.rFonts.cs.name = fontScheme.checkFont(textPr.rFonts.cs.name)
    }
  }
  if (textPr.fontFamily) {
    textPr.fontFamily.name = fontScheme.checkFont(textPr.fontFamily.name)
  }
  const font = EditorUtil.FontKit.getFontFile(textPr.fontFamily.name)
  textPr.fontFamily.name = font?.fullName ?? textPr.fontFamily.name

  editorKit.emitEvent(Evt_onFontFamily, textPr.fontFamily)
  editorKit.emitEvent(Evt_onBold, !!textPr.bold)
  editorKit.emitEvent(Evt_onItalic, !!textPr.italic)
  editorKit.emitEvent(Evt_onUnderline, !!textPr.underline)
  editorKit.emitEvent(Evt_onStrikeout, !!textPr.strikeout)
  editorKit.emitEvent(Evt_onFontSize, textPr.fontSize)

  if (textPr.vertAlign !== undefined) {
    editorKit.emitVerticalAlign(textPr.vertAlign)
  }
  if (textPr.fillEffects !== undefined) {
    editorKit.emitTextColor(textPr.fillEffects)
  }
  if (textPr.highLight || textPr.highLight === HightLight_None) {
    editorKit.emitTextHighLight(textPr.highLight)
  }
}
