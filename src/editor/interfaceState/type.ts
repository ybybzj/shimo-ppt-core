import { SelectedElementType } from '../../core/common/const/attrs'
import {
  HyperlinkProps,
  ImageProps,
  ParagraphProps,
  ShapeProps,
  SlideProps,
  TableProps,
} from '../../exports/type'

export type SelectedElementInfo =
  | {
      readonly type: typeof SelectedElementType.Paragraph
      value: ParagraphProps
    }
  | {
      readonly type: typeof SelectedElementType.Table
      ids: string[]
      value: TableProps
    }
  | {
      readonly type: typeof SelectedElementType.Image
      ids: string[]
      value: ImageProps
    }
  | {
      readonly type: typeof SelectedElementType.Hyperlink
      value: HyperlinkProps
    }
  | {
      readonly type: typeof SelectedElementType.Shape
      ids: string[]
      value: ShapeProps
    }
  | {
      readonly type: typeof SelectedElementType.Slide
      value: SlideProps
    }
  | {
      readonly type:
        | typeof SelectedElementType.Header
        | typeof SelectedElementType.Chart
        | typeof SelectedElementType.Math
      value: any
    }
