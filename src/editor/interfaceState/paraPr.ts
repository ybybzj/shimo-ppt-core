import { SelectedElementType } from '../../core/common/const/attrs'

import { getListTypeFromBullet } from '../../core/SlideElement/attrs/bullet'
import { ParaPr } from '../../core/textAttributes/ParaPr'
import { ParagraphProps } from '../../exports/type'
import { Evt_onLineSpacing } from '../events/EventNames'
import { IEditorKit } from '../type'
import { addSelectedElementInfoWithCheckLast } from './selectedElementsInfoStack'

export const emitParaProps = (editorKit: IEditorKit, paraPr: ParaPr) => {
  const pPr = paraPr

  editorKit.emitEvent(Evt_onLineSpacing, pPr.spacing)
  editorKit.emitPrAlignCallBack(pPr.jcAlign)
  editorKit.emitListType(getListTypeFromBullet(pPr.bullet))

  const spacing = pPr.spacing
  const paraProps: ParagraphProps = {
    ['spacing']: spacing
      ? {
          ['after']: spacing.after ?? null,
          ['before']: spacing.before ?? null,
          ['line']: spacing.line ?? null,
          ['lineRule']: spacing.lineRule ?? null,
        }
      : null,
    ['rtl']: !!pPr.rtl,
  }

  addSelectedElementInfoWithCheckLast({
    type: SelectedElementType.Paragraph,
    value: paraProps,
  })
}
