import { Nullable } from '../../../liber/pervasive'
import { FillKIND, ShdType } from '../../core/common/const/attrs'
import { isDict, isNumber } from '../../common/utils'
import { CHexColor } from '../../core/color/CHexColor'
import { createComputedRBGColorCustom } from '../../core/color/ComputedRGBAColor'
import { intersectObjects } from '../../core/common/helpers'
import { SlideElementOptions } from '../../core/properties/options'
import {
  MarginPaddings,
  TablePropOptions,
} from '../../core/properties/tableProp'
import { Presentation } from '../../core/Slide/Presentation'
import { FillEffects } from '../../core/SlideElement/attrs/fill/fill'
import { Ln } from '../../core/SlideElement/attrs/line'
import { ShapeLocks } from '../../core/SlideElement/const'
import { TableCellBorderOptions } from '../../core/Table/attrs/CellBorder'

import { isPlaceholderWithEmptyContent } from '../../core/utilities/shape/asserts'
import {
  getGeomPreset,
  getTitleForSp,
  getDescriptionForSp,
  getMediaFromShape,
} from '../../core/utilities/shape/getters'
import { checkLock } from '../../core/utilities/shape/locks'
import { getTableProps } from '../../core/utilities/table'
import { ImageSubTypes } from '../../exports/type'
import { isErrorMediaUrl } from '../../images/errorImageSrc'
import { InstanceType } from '../../core/instanceTypes'
import { SlideElement } from '../../core/SlideElement/type'
import { TCBorderValue } from '../../core/common/const/table'

export function getSpPropsFromSpArr(
  presentation: Presentation,
  spArr: SlideElement[]
) {
  let spPrs: Nullable<SlideElementOptions>
  let imgPrs: Nullable<SlideElementOptions>
  let tablePrs: Nullable<TablePropOptions>
  const shapeIdArr: string[] = []
  const imageIdArr: string[] = []
  const tableIdArr: string[] = []

  for (let i = 0; i < spArr.length; ++i) {
    const sp = spArr[i]

    const lockAspect = checkLock(sp, ShapeLocks.noModifyAspect)
    switch (sp.instanceType) {
      case InstanceType.Shape: {
        const newSpPr: SlideElementOptions = {
          canFill: sp.canFill(),
          type: getGeomPreset(sp),
          fill: getSpFill(sp),
          stroke: getSpStroke(sp),
          paddings: sp.getPaddings(),
          verticalTextAlign: sp.getBodyPr().anchor,
          vert: sp.getBodyPr().vert,
          rot: sp.rot,
          flipH: sp.flipH,
          flipV: sp.flipV,
          isFromChart: false,
          lockAspect: lockAspect,
          title: getTitleForSp(sp),
          description: getDescriptionForSp(sp),
          columnCount: sp.getColumnCount(),
          columnSpace: sp.getColumnSpace(),
          isEmptyPH: isPlaceholderWithEmptyContent(sp),
        }
        spPrs = spPrs ? intersectShapeProperties(spPrs, newSpPr) : newSpPr
        if (!sp.group) shapeIdArr.push(sp.getId())
        break
      }
      case InstanceType.ImageShape: {
        const media = getMediaFromShape(sp)
        const { url, encryptUrl } = sp.getImageUrl()
        const newImgPrs: SlideElementOptions = {
          subTypes: [],
          id: sp.id,
          hyperlink: sp.nvPicPr?.cNvPr?.hlinkClick?.id,
          transparent: sp.getTransparent(),
          imageUrl: url,
          encryptImageUrl: encryptUrl,
          rot: sp.rot,
          flipH: sp.flipH,
          flipV: sp.flipV,
          lockAspect: lockAspect,
          title: getTitleForSp(sp),
          description: getDescriptionForSp(sp),
          media: media,
        }

        let imageSubType: ImageSubTypes
        switch (sp.subType) {
          case ImageSubTypes.chart:
            imageSubType = ImageSubTypes.chart
            break
          case ImageSubTypes.custom:
            imageSubType = ImageSubTypes.custom
            break
          default:
            imageSubType = ImageSubTypes.image
        }
        if (media && !isErrorMediaUrl(media?.media)) {
          imageSubType = ImageSubTypes.media
        }
        if (sp.isOleObject()) {
          imageSubType = ImageSubTypes.ole
          newImgPrs.attachment = {
            url: sp.uri,
            encryptUrl: sp.encryptUrl,
          }
        }

        newImgPrs.subTypes?.push(imageSubType)

        if (!imgPrs) {
          imgPrs = newImgPrs
        } else {
          if (imgPrs.id != null && imgPrs.id !== newImgPrs.id) {
            imgPrs.id = null
          }
          if (imgPrs.hyperlink && newImgPrs.hyperlink) {
            imgPrs.hyperlink = null
          } else if (newImgPrs.hyperlink) {
            imgPrs.hyperlink = newImgPrs.hyperlink
          }
          if (
            imgPrs.transparent != null &&
            imgPrs.transparent !== newImgPrs.transparent
          ) {
            imgPrs.transparent = null
          }
          if (
            imgPrs.imageUrl != null &&
            imgPrs.imageUrl !== newImgPrs.imageUrl
          ) {
            imgPrs.imageUrl = null
          }

          if (imgPrs.rot != null && imgPrs.rot !== newImgPrs.rot) {
            imgPrs.rot = null
          }

          if (imgPrs.flipH != null && imgPrs.flipH !== newImgPrs.flipH) {
            imgPrs.flipH = null
          }

          if (imgPrs.flipV != null && imgPrs.flipV !== newImgPrs.flipV) {
            imgPrs.flipV = null
          }

          if (imgPrs.lockAspect || newImgPrs.lockAspect) {
            imgPrs.lockAspect = false
          }
          if (imgPrs.title !== newImgPrs.title) {
            imgPrs.title = undefined
          }
          if (imgPrs.description !== newImgPrs.description) {
            imgPrs.description = undefined
          }
          if (imgPrs.media && newImgPrs.media) {
            imgPrs.media = undefined
          } else if (newImgPrs.media) {
            imgPrs.media = newImgPrs.media
          }
          if (imgPrs.subTypes && newImgPrs.subTypes) {
            imgPrs.subTypes = imgPrs.subTypes.concat(newImgPrs.subTypes)
          } else if (newImgPrs.subTypes) {
            imgPrs.subTypes = newImgPrs.subTypes
          }
        }

        const newSpPr: SlideElementOptions = {
          canFill: false,
          type: getGeomPreset(sp),
          fill: getSpFill(sp),
          stroke: getSpStroke(sp),
          paddings: null,
          verticalTextAlign: null,
          vert: null,
          rot: sp.rot,
          flipH: sp.flipH,
          flipV: sp.flipV,
          isFromChart: false,
          isFromImage: true,
          lockAspect: lockAspect,
          title: getTitleForSp(sp),
          description: getDescriptionForSp(sp),
          columnCount: null,
          columnSpace: null,
        }
        spPrs = spPrs ? intersectShapeProperties(spPrs, newSpPr) : newSpPr
        if (!sp.group) imageIdArr.push(sp.getId())
        break
      }
      case InstanceType.GraphicFrame: {
        if (tablePrs == null) {
          const newTablePrs = getTableProps(sp.graphicObject!)
          tablePrs = newTablePrs
          if (newTablePrs.cellsBackground) {
            if (
              newTablePrs.cellsBackground.fillEffects &&
              newTablePrs.cellsBackground.fillEffects.fill &&
              newTablePrs.cellsBackground.fillEffects.fill!.type !==
                FillKIND.NOFILL
            ) {
              newTablePrs.cellsBackground.fillEffects.check(
                sp.getTheme(),
                sp.getColorMap()
              )
              const rgba =
                newTablePrs.cellsBackground.fillEffects.getRGBAColor()
              newTablePrs.cellsBackground.color = createComputedRBGColorCustom(
                rgba.r,
                rgba.g,
                rgba.b
              )
              newTablePrs.cellsBackground.value = ShdType.Clear
            } else {
              newTablePrs.cellsBackground.color = createComputedRBGColorCustom(
                0,
                0,
                0
              )
              newTablePrs.cellsBackground.value = ShdType.Nil
            }
          }
          if (newTablePrs.cellBorders) {
            const updateTableBorder = (
              border: Nullable<TableCellBorderOptions>
            ) => {
              if (!border) return
              if (
                border.fillEffects &&
                border.fillEffects.fill &&
                border.fillEffects.fill.type !== FillKIND.NOFILL
              ) {
                border.fillEffects.check(sp.getTheme(), sp.getColorMap())
                const rgba = border.fillEffects.getRGBAColor()
                border.color = new CHexColor(rgba.r, rgba.g, rgba.b, false)
                border.value = TCBorderValue.Single
              } else {
                if (!border.color) {
                  border.color = new CHexColor(0, 0, 0, false)
                }
                border.value = TCBorderValue.None
              }
            }
            updateTableBorder(newTablePrs.cellBorders.top)
            updateTableBorder(newTablePrs.cellBorders.bottom)
            updateTableBorder(newTablePrs.cellBorders.right)
            updateTableBorder(newTablePrs.cellBorders.left)
          }
        } else {
          tablePrs = {}
        }
        if (!sp.group) tableIdArr.push(sp.getId())
        break
      }
      case InstanceType.GroupShape: {
        const childPrs = getSpPropsFromSpArr(presentation, sp.spTree)
        const {
          shapeProps: childSpPr,
          imageShapeProps: childImgPr,
          tableProps: childTablePr,
        } = childPrs
        if (childSpPr) {
          if (!spPrs) spPrs = childSpPr
          else {
            spPrs = intersectShapeProperties(spPrs, childSpPr)
          }
        }

        if (childImgPr) {
          if (!imgPrs) imgPrs = childImgPr
          else {
            if (
              imgPrs.transparent != null &&
              imgPrs.transparent !== childImgPr.transparent
            ) {
              imgPrs.transparent = null
            }
            if (
              imgPrs.imageUrl != null &&
              imgPrs.imageUrl !== childImgPr.imageUrl
            ) {
              imgPrs.imageUrl = null
            }

            if (imgPrs.rot != null && imgPrs.rot !== childImgPr.rot) {
              imgPrs.rot = null
            }

            if (imgPrs.flipH != null && imgPrs.flipH !== childImgPr.flipH) {
              imgPrs.flipH = null
            }

            if (imgPrs.flipV != null && imgPrs.flipV !== childImgPr.flipV) {
              imgPrs.flipV = null
            }

            if (!imgPrs.lockAspect || !childImgPr.lockAspect) {
              imgPrs.lockAspect = false
            }
            if (imgPrs.title !== childImgPr.title) {
              imgPrs.title = undefined
            }
            if (imgPrs.description !== childImgPr.description) {
              imgPrs.description = undefined
            }
          }
        }
        if (childTablePr) {
          tablePrs = tablePrs ? childTablePr : null
        }
        if (!sp.group) shapeIdArr.push(sp.getId())
        break
      }
    }
  }
  return {
    imageShapeProps: imgPrs,
    imageIdArr,
    shapeProps: spPrs,
    shapeIdArr,
    tableProps: tablePrs,
    tableIdArr,
  }
}

function intersectShapeProperties(
  spPr1: SlideElementOptions,
  spPr2: SlideElementOptions
) {
  const result: SlideElementOptions = {}
  if (spPr1.type === spPr2.type) {
    result.type = spPr1.type
  } else {
    result.type = null
  }

  if (spPr1.stroke == null || spPr2.stroke == null) {
    result.stroke = null
  } else {
    result.stroke = (spPr1.stroke as Ln).intersect(spPr2.stroke as Ln)
  }

  {
    result.verticalTextAlign = null
    result.vert = null
  }

  result.fill = intersectObjects(spPr1.fill as FillEffects, spPr2.fill)
  if (isDict(spPr1.paddings) && isDict(spPr2.paddings)) {
    result.paddings = new MarginPaddings()
    result.paddings.left = isNumber(spPr1.paddings.left)
      ? spPr1.paddings.left === spPr2.paddings.left
        ? spPr1.paddings.left
        : undefined
      : undefined
    result.paddings.top = isNumber(spPr1.paddings.top)
      ? spPr1.paddings.top === spPr2.paddings.top
        ? spPr1.paddings.top
        : undefined
      : undefined
    result.paddings.right = isNumber(spPr1.paddings.right)
      ? spPr1.paddings.right === spPr2.paddings.right
        ? spPr1.paddings.right
        : undefined
      : undefined
    result.paddings.bottom = isNumber(spPr1.paddings.bottom)
      ? spPr1.paddings.bottom === spPr2.paddings.bottom
        ? spPr1.paddings.bottom
        : undefined
      : undefined
  }
  result.canFill = spPr1.canFill === true || spPr2.canFill === true

  if (spPr1.isFromChart || spPr2.isFromChart) {
    result.isFromChart = true
  } else {
    result.isFromChart = false
  }
  if (!spPr1.isFromImage || !spPr2.isFromImage) {
    result.isFromImage = false
  } else {
    result.isFromImage = true
  }

  result.lockAspect = !!(spPr1.lockAspect && spPr2.lockAspect)
  if (spPr1.title === spPr2.title) {
    result.title = spPr1.title
  }
  if (spPr1.description === spPr2.description) {
    result.description = spPr1.description
  }
  if (spPr1.columnCount === spPr2.columnCount) {
    result.columnCount = spPr1.columnCount
  }
  if (spPr1.columnSpace === spPr2.columnSpace) {
    result.columnSpace = spPr1.columnSpace
  }
  if (spPr1.isEmptyPH || spPr2.isEmptyPH) {
    result.isEmptyPH = true
  } else {
    result.isEmptyPH = false
  }

  return result
}

function getSpFill(sp: SlideElement) {
  if (sp.brush && sp.brush.fill) {
    return sp.brush
  }
  return FillEffects.NoFill()
}

function getSpStroke(sp: SlideElement): Ln {
  if (sp.pen && sp.pen.fillEffects) {
    if (
      sp.instanceType === InstanceType.ImageShape &&
      !sp.isOleObject() &&
      isNumber(sp.pen.w)
    ) {
      const pen = sp.pen.clone()
      pen.w! /= 2.0
      return pen
    }
    return sp.pen
  }
  const ret = Ln.NoFill()
  ret.w = 0
  return ret
}
