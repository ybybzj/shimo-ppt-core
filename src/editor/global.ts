import { EditorSettings } from '../core/common/EditorSettings'
import { IEditorKit } from './type'

export let EditorKit: IEditorKit

export function setEditor(editorKit: any): void {
  EditorKit = editorKit
  ;(EditorKit as any).EditorSettings = EditorSettings
  dev: {
    const _editor = window['editor']
    if (!_editor) {
      console.log() // 触发插件
      window['editor'] = editorKit
    }
  }
}
