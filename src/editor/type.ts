import { Dict } from '../../liber/pervasive'
import * as protos from './proto'
import { EditorKitCtor } from './editorKit'
import { CmContentData } from '../io/dataType/comment'
import { FontProvider } from '../fonts/type'
import { IOAdapter } from '../io/adapter'

export interface UploadUrlResult {
  url: string
  encryptUrl?: string
}

export type EditorKitOptions = Dict<any> & {
  disableAutoSave?: boolean
  translate?: Dict<string>
  insertImageFunc?: () => Promise<UploadUrlResult | undefined>
  uploadImagesFunc?: (
    files: (string | File)[]
  ) => Promise<(UploadUrlResult | File | undefined)[] | undefined>
  uploadAttachmentFunc?: (files: File[]) => Promise<void>
  /** 获取图片的压缩后/原图地址 */
  transformImageSrcFunc?: (
    src: string,
    mode: 'thumbnail' | 'compressed' | 'origin'
  ) => string
  locale?: string
  endShowElement?: HTMLElement
  shouldDisplayComment?: (cmContentData: CmContentData) => boolean
  pasteCallbackFunc?: (params: {
    files: string[]
    sourceFileId: string
  }) => Promise<any>
  detectLinkTyeFunc?: (link: string) => Promise<{ type: string } | undefined>
  getPlaceholderImageByErrorInfo?: (error: string) => string | undefined
  fonts: FontProvider
  sourceFileId?: string
  isEnableVideo?: boolean
  isEnableEavert?: boolean
  dir?: 'ltr' | 'rtl'
  imageSrcOrigin?: string
  skipShortcut?: (e: KeyboardEvent) => boolean // check if skip handle keydown event
  prepareChartObjectImage?: (info: ChartObjectInfo) => Promise<void>
  fetchImage?: (src: string) => Promise<Blob | undefined>
}

export interface ChartObjectInfo {
  link?: string
  data?: any
}
export type IEditorKit = EditorKitCtor &
  typeof protos & {
    new (config: EditorKitOptions, ioAdapter: IOAdapter<any>): IEditorKit
    constructor: IEditorKit
  }
