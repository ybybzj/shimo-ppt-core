import {
  ChartObjectInfo,
  EditorKitOptions,
  IEditorKit,
  UploadUrlResult,
} from './type'

import { extend } from '../../liber/extend'
import * as protos from './proto'
import { EditorUI } from '../ui/editorUI/EditorUI'
import { CollaborationManager } from '../collaboration/collaborationManager'
import { PPTYApplier } from '../io/PPTYApplier'
import { ImageLoadManager } from '../images/ImageLoadManager'
import { SlidePreviewImages } from '../ui/SlidePreviewImages'
import {
  ActionHookName,
  emitEvent,
  getFromEvent,
  ReturnableHookName,
} from './events/registry'
import { EditorSettings } from '../core/common/EditorSettings'
import { EditorKit, setEditor } from './global'
import { translator } from '../globals/translator'
import { EditorUtil } from '../globals/editor'
import { CmContentData } from '../io/dataType/comment'
import { Presentation } from '../core/Slide/Presentation'
import { ResourcesLoadManager } from './utils/resourcesLoadManager'
import { EnUSLangID } from '../lib/locale'
import { LoadableImage } from '../images/utils'
import { initEditorKit } from './proto/init'
import {
  StatusOfFormatPainter,
  StatusOfFormatPainterValues,
} from '../ui/editorUI/const'
import { adaptIOAdapter, IOAdapter } from '../io/adapter'

export class EditorKitCtor {
  locale: string
  isLoadEmptyPPT = true
  presentation!: Presentation
  editorUI!: EditorUI
  isSupportEmptySlides = true
  private _paintFormatState: StatusOfFormatPainterValues =
    StatusOfFormatPainter.OFF
  get paintFormatState(): StatusOfFormatPainterValues {
    return this._paintFormatState
  }
  set paintFormatState(v: StatusOfFormatPainterValues) {
    this._paintFormatState = v
    if (this.presentation) {
      this.presentation.allowPasteFormat = v !== StatusOfFormatPainter.OFF
    }
  }
  isEditorUIInitComplete = false
  endShowElement?: HTMLElement
  imageLoader!: ImageLoadManager
  imageLoadedCallback!: null | ((_image: LoadableImage) => void)

  translator: any
  targetElementId: string
  collaborationManager: CollaborationManager<any>
  ioAdapter: IOAdapter<any>
  dataLoader: PPTYApplier
  resourcesLoadManager: ResourcesLoadManager
  disableAutoSave: boolean
  insertImageFunc?: () => Promise<UploadUrlResult | undefined>
  uploadImagesFunc?: (
    files: (string | File)[]
  ) => Promise<(UploadUrlResult | File | undefined)[] | undefined>
  uploadAttachmentFunc?: (files: File[]) => Promise<void>
  /** 获取图片的缩略图/高保真压缩/原图地址 */
  transformImageSrcFunc?: (
    src: string,
    mode: 'thumbnail' | 'compressed' | 'origin'
  ) => string
  shouldDisplayCommentFunc?: (cmContentData: CmContentData) => boolean
  pasteCallbackFunc?: (params: {
    files: string[]
    sourceFileId: string
  }) => Promise<any>
  detectLinkTypeFunc?: (link: string) => Promise<{ type: string } | undefined>
  getPlaceholderImageByErrorInfo?: (error: string) => string | undefined
  sourceFileId?: string
  slidesPreviewer?: SlidePreviewImages
  skipShortcut?: (e: KeyboardEvent) => boolean
  prepareChartObjectImage?: (info: ChartObjectInfo) => Promise<void>

  fetchImage?: (src: string) => Promise<Blob | undefined>

  constructor(config: EditorKitOptions, ioAdapter: IOAdapter<any>) {
    this.targetElementId = config['id-view'] || ''

    if (config['mobile'] != null) {
      EditorSettings.isMobile = config['mobile']
    }

    this.translator = translator.init(config['translate'])
    this.locale = config['locale'] ?? EnUSLangID
    this.insertImageFunc = config['insertImageFunc']
    this.uploadImagesFunc = config['uploadImagesFunc']
    this.uploadAttachmentFunc = config['uploadAttachmentFunc']
    this.transformImageSrcFunc = config['transformImageSrcFunc']
    this.shouldDisplayCommentFunc = config['shouldDisplayComment']
    this.pasteCallbackFunc = config['pasteCallbackFunc']
    this.detectLinkTypeFunc = config['detectLinkTypeFunc']
    this.getPlaceholderImageByErrorInfo =
      config['getPlaceholderImageByErrorInfo']
    this.sourceFileId = config['sourceFileId']
    if (config['isEnableVideo'] != null) {
      EditorSettings.isSupportVideo = config['isEnableVideo']
    }

    if (config['isEnableEavert'] != null) {
      EditorSettings.isSupportEavert = config['isEnableEavert']
    }

    if (config['dir'] != null) {
      EditorSettings.isRTL = config['dir'] === 'rtl' ? true : false
    }
    if (config['skipShortcut'] != null) {
      this.skipShortcut = config['skipShortcut']
    }

    if (config['prepareChartObjectImage'] != null) {
      this.prepareChartObjectImage = config['prepareChartObjectImage']
    }
    if (config['fetchImage'] != null) {
      this.fetchImage = config['fetchImage']
    }
    this.ioAdapter = adaptIOAdapter(ioAdapter)

    this.paintFormatState = StatusOfFormatPainter.OFF
    this.endShowElement = config['endShowElement']

    if (EditorKit == null) {
      setEditor(this as unknown as IEditorKit)
    }

    EditorUtil.FontKit.init(config['fonts'])

    this.collaborationManager = new CollaborationManager({
      editorKit: this as unknown as IEditorKit,
    })

    this.dataLoader = new PPTYApplier(this as unknown as IEditorKit)

    this.resourcesLoadManager = new ResourcesLoadManager(
      this as unknown as IEditorKit
    )

    this.disableAutoSave = config['disableAutoSave'] ?? false
    initEditorKit(this as unknown as IEditorKit)
  }

  getViewMode() {
    return EditorSettings.isViewMode
  }

  emitEvent(name: ActionHookName<IEditorKit>, ...args: any[]) {
    return emitEvent(this as unknown as IEditorKit, name, ...args)
  }

  fromEvent(name: ReturnableHookName<IEditorKit>, ...args: any[]): any {
    return getFromEvent(this as unknown as IEditorKit, name, ...args)
  }
}

extend(EditorKitCtor.prototype, protos)

export function createEditorKit(
  config: EditorKitOptions,
  ioAdapter: IOAdapter<any>
): IEditorKit {
  return new EditorKitCtor(config, ioAdapter) as unknown as IEditorKit
}
