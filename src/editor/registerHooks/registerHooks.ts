import { HookManager } from '../../lib/hook'
import { IEditorKit } from '../type'
import { registers } from './registers'

export function registerEditorHooks(
  hookManager: HookManager,
  editorKit: IEditorKit
): void {
  registers.forEach((register) => {
    register(hookManager, editorKit)
  })
}
