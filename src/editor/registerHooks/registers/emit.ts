import { Hooks } from '../../../core/hooks'
import { isChartImage } from '../../../core/utilities/shape/asserts'
import { isAudioImage, isVideoImage } from '../../../core/utilities/shape/image'
import { InstanceType } from '../../../core/instanceTypes'
import { HookManager } from '../../../lib/hook'
import { genAudioAction } from '../../../ui/transitions/AudioManager'
import {
  Evt_onContextMenu,
  Evt_onError,
  Evt_onHoveredCommentChange,
  Evt_onHoveredObjectChange,
  Evt_onMouseMove,
  Evt_onSearchEnd,
  Evt_onSelectedCommentChange,
  Evt_onSlideBackgroundChange,
  Evt_onToggleCommentShow,
  Evt_onVert,
  Evt_onVerticalTextAlign,
  Evt_viewAttachment,
} from '../../events/EventNames'
import { emitHyperlinkProp } from '../../interfaceState/hyperlink'
import { ImageLikeInfo } from '../../proto'
import { IEditorKit } from '../../type'

export function register(hookManager: HookManager, editorKit: IEditorKit) {
  hookManager.register(
    Hooks.Emit.EmitEvent,
    ({ payload: { eventType, args } }) => {
      let eventHook
      switch (eventType) {
        case 'Vert':
          eventHook = Evt_onVert
          break
        case 'VerticalTextAlign':
          eventHook = Evt_onVerticalTextAlign
          break
        case 'Error':
          eventHook = Evt_onError
          break
        case 'HoveredCommentChange':
          eventHook = Evt_onHoveredCommentChange
          break
        case 'SelectedCommentChange':
          eventHook = Evt_onSelectedCommentChange
          break
        case 'ToggleCommentShow':
          eventHook = Evt_onToggleCommentShow
          break
        case 'ContextMenu':
          eventHook = Evt_onContextMenu
          break
        case 'MouseMove':
          eventHook = Evt_onMouseMove
          break
        case 'SlideBackgroundChange':
          eventHook = Evt_onSlideBackgroundChange
          break
        case 'HoveredObjectChange':
          eventHook = Evt_onHoveredObjectChange
          break
        case 'FindText':
          eventHook = Evt_onSearchEnd
          break
      }

      if (eventHook != null) {
        editorKit.emitEvent(eventHook, ...args)
      }
    }
  )

  hookManager.register(
    Hooks.Emit.EmitHyperProp,
    ({ payload: { hyperlinkOptions } }) => {
      emitHyperlinkProp(hyperlinkOptions)
    }
  )

  hookManager.register(Hooks.Emit.EmitAddComment, ({ payload: {} }) => {
    editorKit.emitAddComment()
  })
  hookManager.register(Hooks.Emit.EmitChangeComment, () => {
    editorKit.emitChangeComment()
  })
  hookManager.register(Hooks.Emit.EmitChangeCommentAuthor, () => {
    editorKit.onCommentAuthorChange()
  })
  hookManager.register(Hooks.Emit.EmitRemoveComment, ({ payload: {} }) => {
    editorKit.emitRemoveComment()
  })

  hookManager.register(Hooks.Emit.EmitEndAddShape, () => {
    editorKit.emitEndAddShape()
  })

  hookManager.register(Hooks.Emit.EmitDoubleClick, ({ payload: sp }) => {
    switch (sp.instanceType) {
      case InstanceType.ImageShape:
        // 双击裁剪
        // checkCropState(editorKit.editorUI.editorHandler, sp)

        // 双击音频
        if (isAudioImage(sp)) {
          const curSlide = editorKit.editorUI.editorHandler.getCurrentSlide()
          const audioAction = genAudioAction(curSlide, sp, 'dbClick')
          if (audioAction) {
            editorKit.emitAudioAction(audioAction)
          }

          break
        }

        // 双击查看 OLE 附件
        if (sp.isOleObject()) {
          const oleInfo: ImageLikeInfo = {
            ['type']: 4, // 注意这里依赖 enum ImageLikeTypes(循环依赖), 如修改需同步修改
            ['refId']: sp.getRefId(),
            ['id']: sp.getId(),
            ['imgInfo']: sp.getImageUrl(),
            ['attachment']: {
              ['url']: sp.uri,
              ['encryptUrl']: sp.encryptUrl,
            },
          }
          editorKit.emitEvent(Evt_viewAttachment, oleInfo)
          break
        }

        // 双击查看大图
        if (!isChartImage(sp) && !isVideoImage(sp)) {
          const imgInfo: ImageLikeInfo = {
            ['type']: 0, // Todo, circular dependency, ImageLikeTypes.image,
            ['refId']: sp.getRefId(),
            ['id']: sp.getId(),
            ['imgInfo']: sp.getImageUrl(),
          }
          editorKit.emitViewSourceImage(imgInfo)
        }
        break
      case InstanceType.GroupShape:
        // checkCropState(editorKit.editorUI.editorHandler)
        break
      default:
        break
    }
  })
}
