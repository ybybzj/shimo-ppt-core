import { Hooks } from '../../../core/hooks'
import { HookManager } from '../../../lib/hook'
import { SelectionCursorColorStyle } from '../../../ui/rendering/Drawer/const'
import { IEditorKit } from '../../type'

export function register(hookManager: HookManager, editorKit: IEditorKit) {
  hookManager.register(
    Hooks.Cursor.GetHoveredStatus,
    ({ payload: { gTableId } }) => {
      const handler = editorKit.editorUI.editorHandler
      return handler.hoveredObject && handler.hoveredObject.getId() === gTableId
    }
  )

  hookManager.register(Hooks.Cursor.GetIsInTouchMode, () => {
    // TODO: to be implemented
    return editorKit.editorUI.isInTouchMode()
  })

  hookManager.register(
    Hooks.Cursor.DrawSelectionCursor,
    ({ payload: { edge, x, y, size } }) => {
      const textCursor = editorKit.editorUI.textCursor

      const selectionCursorInfo = {
        hitX: -1,
        hitY: -1,
        targetX: x,
        targetY: y,
        size,
      }
      const { mmX, mmY } = textCursor.onRenderTextCursor(
        x,
        y,
        size,
        SelectionCursorColorStyle
      )
      selectionCursorInfo.hitX = mmX
      selectionCursorInfo.hitY = mmY
      if (edge === 'start') {
        textCursor.onUpdateSelectionStart(selectionCursorInfo)
      } else {
        textCursor.onUpdateSelectionEnd(selectionCursorInfo)
      }
    }
  )
}
