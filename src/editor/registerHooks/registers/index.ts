import { register as Editor } from './editor'
import { register as Aniamtion } from './animation'
import { register as Attrs } from './attrs'
import { register as UndoRedo } from './document'
import { register as EditorUI } from './editorUI'
import { register as Emit } from './emit'
import { register as Comments } from './comments'
import { register as Cursor } from './cursor'

export const registers = [
  Editor,
  Aniamtion,
  Attrs,
  UndoRedo,
  EditorUI,
  Emit,
  Comments,
  Cursor,
] as const
