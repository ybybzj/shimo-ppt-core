import { Hooks } from '../../../core/hooks'
import { HookManager } from '../../../lib/hook'
import { IEditorKit } from '../../type'

export function register(hookManager: HookManager, editorKit: IEditorKit) {
  hookManager.register(Hooks.Comments.OnAddComment, ({ payload: {} }) => {
    editorKit.emitAddComment()
  })

  hookManager.register(Hooks.Comments.OnRemoveComment, ({ payload: {} }) => {
    editorKit.emitRemoveComment()
  })
}
