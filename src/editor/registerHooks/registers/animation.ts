import { Hooks } from '../../../core/hooks'
import { HookManager } from '../../../lib/hook'
import { IEditorKit } from '../../type'

export function register(hookManager: HookManager, _api: IEditorKit) {
  hookManager.register(
    Hooks.Animation.OnReplaceShape,
    ({ payload: { slide, oldRefId, newRefId } }) => {
      slide.animation.onReplaceShape(oldRefId, newRefId)
    }
  )
  hookManager.register(
    Hooks.Animation.OnRemoveShape,
    ({ payload: { slide, refId } }) => {
      slide.animation.onDeleteShape(refId)
    }
  )
}
