import { checkImageSrc } from '../../../common/utils'
import { Hooks } from '../../../core/hooks'
import { HookManager } from '../../../lib/hook'
import {
  convertGlobalInnerMMToPixPos,
  isInShowMode,
} from '../../../ui/editorUI/utils/utils'
import { IEditorKit } from '../../type'
import { syncInterfaceState } from '../../interfaceState/syncInterfaceState'
import { SlideElement } from '../../../core/SlideElement/type'
import { InstanceType } from '../../../core/instanceTypes'
import { EditorSettings } from '../../../core/common/EditorSettings'
import { ShapeDrawerContext } from '../../../core/graphic/type'
import { syncSelectionState } from '../../../ui/editorUI/utils/updates'
import { FocusTarget } from '../../../core/common/const/drawing'
import { getUniNvPr } from '../../../core/utilities/shape/getters'
import { emitParaProps } from '../../interfaceState/paraPr'
import { emitTextProps } from '../../interfaceState/textPr'

export function register(hookManager: HookManager, editorKit: IEditorKit) {
  hookManager.register(
    Hooks.EditorUI.OnUpdateLayouts,
    ({ payload: isForce }) => {
      editorKit.editorUI.updateLayouts(isForce)
    }
  )

  hookManager.register(
    Hooks.EditorUI.OnUpdateParagraphProp,
    ({ payload: { paraPr } }) => {
      emitParaProps(editorKit, paraPr)
    }
  )

  hookManager.register(
    Hooks.EditorUI.UpdateTextPr,
    ({ payload: { textPr: textPr } }) => {
      emitTextProps(editorKit, textPr)
    }
  )

  hookManager.register(
    Hooks.EditorUI.HyperlinkClick,
    ({ payload: { value } }) => {
      editorKit.emitHyperlinkClick(value)
    }
  )

  hookManager.register(
    Hooks.EditorUI.AddSlideSelection,
    ({ payload: { slideIndex, x, y, width, height, matrix } }) => {
      editorKit.editorUI.textCursor.onSelectionDraw(
        slideIndex,
        x,
        y,
        width,
        height,
        matrix
      )
    }
  )

  hookManager.register(
    Hooks.EditorUI.setCursorSize,
    ({ payload: { size, ascent } }) => {
      editorKit.editorUI.textCursor.setSize(size, ascent)
    }
  )

  hookManager.register(
    Hooks.EditorUI.setCursorColor,
    ({ payload: { r, g, b } }) => {
      editorKit.editorUI.textCursor.setColor(r, g, b)
    }
  )

  hookManager.register(
    Hooks.EditorUI.UpdateCursor,
    ({ payload: { x, y, slideIndex } }) => {
      editorKit.editorUI.textCursor.onUpdate(x, y, slideIndex)
    }
  )

  hookManager.register(Hooks.EditorUI.OnCursorShow, () => {
    editorKit.editorUI.textCursor.show()
  })

  hookManager.register(
    Hooks.EditorUI.OnCalculateSlide,
    ({ payload: { index } }) => {
      editorKit.editorUI.onCalculateSlide(index)
    }
  )

  hookManager.register(
    Hooks.EditorUI.OnGoToSlide,
    ({ payload: { slideIndex, isForce } }) => {
      if (isForce != null) {
        editorKit.editorUI.goToSlide(slideIndex, isForce)
      } else {
        editorKit.editorUI.goToSlide(slideIndex)
      }
    }
  )

  hookManager.register(
    Hooks.EditorUI.OnScrollToCenter,
    ({ payload: { slideIndex } }) => {
      editorKit.editorUI.scrolls.onGoToSlide(slideIndex)
    }
  )

  hookManager.register(Hooks.EditorUI.OnLockCursor, () => {
    editorKit.editorUI.mouseCursor.lock()
  })

  hookManager.register(
    Hooks.EditorUI.OnSetInTextSelection,
    ({ payload: { isEnabled: isEnable } }) => {
      editorKit.editorUI.renderingController.setInTextSelection(isEnable)
    }
  )

  hookManager.register(
    Hooks.EditorUI.OnMultiplyTargetTransform,
    ({ payload: { transform, callback } }) => {
      editorKit.editorUI.textCursor.onMultiplyTargetTransform(
        callback,
        transform
      )
    }
  )

  hookManager.register(
    Hooks.EditorUI.OnUpdateCursorTransform,
    ({ payload: { transform } }) => {
      editorKit.editorUI.textCursor.updateTransform(transform)
    }
  )

  hookManager.register(
    Hooks.EditorUI.OnCalculateNotes,
    ({ payload: { slideIndex, notesWidth, notesHeight } }) => {
      editorKit.editorUI.calculateStateForNotes(
        slideIndex,
        notesWidth,
        notesHeight
      )
    }
  )

  hookManager.register(Hooks.EditorUI.OnUpdateEditLayer, () => {
    editorKit.editorUI.renderingController.onUpdateEditLayer()
  })

  hookManager.register(Hooks.EditorUI.OnFirePaint, () => {
    editorKit.editorUI.triggerRender()
  })

  hookManager.register(Hooks.EditorUI.OnFireNotePaint, () => {
    editorKit.editorUI.notesView.prepareRender()
  })

  hookManager.register(Hooks.EditorUI.OnCalculatePreviews, () => {
    editorKit.editorUI.calculatePreviews()
  })

  hookManager.register(
    Hooks.EditorUI.OnUpdatePlaceholders,
    ({ payload: { slideIndex } }) => {
      editorKit.editorUI.renderingController.onUpdatePlaceholder(slideIndex)
    }
  )

  hookManager.register(Hooks.EditorUI.OnSyncInterfaceState, () => {
    syncInterfaceState(editorKit)
  })

  hookManager.register(Hooks.EditorUI.OnUpdateSelectionState, () => {
    syncSelectionState(editorKit.editorUI)
  })

  hookManager.register(
    Hooks.EditorUI.OnComponentFocus,
    ({ payload: { focusType } }) => {
      if (focusType !== FocusTarget.Thumbnails) {
        editorKit.editorUI.thumbnailsManager.onBlur()
      }
    }
  )

  hookManager.register(Hooks.EditorUI.OnMoveCursorForward, () => {
    const presentation = editorKit.presentation
    const handler = editorKit.editorUI.editorHandler
    const textDoc = presentation.getCurrentTextTarget(false)
    if (textDoc) {
      handler.moveCursorRight(false, false, true, true)
    }
  })

  hookManager.register(Hooks.EditorUI.OnApplyPropsWithImageSrcId, () => {
    const handler = editorKit.editorUI.editorHandler
    if (handler.curState.instanceType === InstanceType.CropState) {
      handler.curState.refreshState()
    }
  })
  // getters
  hookManager.register(
    Hooks.EditorUI.ConvertCoordsToCursorPos,
    ({ payload: { x, y, transform } }) => {
      return convertGlobalInnerMMToPixPos(editorKit.editorUI, x, y, transform)
    }
  )

  hookManager.register(
    Hooks.EditorUI.GetImageFromLoader,
    ({ payload: imageSrc }) => {
      imageSrc = checkImageSrc(imageSrc)
      const img = editorKit.imageLoader.imagesMap[imageSrc!]
      return img
    }
  )

  hookManager.register(Hooks.EditorUI.GetWidthOfNotes, () => {
    return editorKit.editorUI.getNotesWidth()
  })

  hookManager.register(
    Hooks.EditorUI.GetSlideElementNeedDraw,
    ({ payload: { sp, graphics } }) =>
      needDrawSlideElement(editorKit, sp, graphics)
  )

  hookManager.register(Hooks.EditorUI.GetIsInShowMod, () =>
    isInShowMode(editorKit.editorUI)
  )

  hookManager.register(
    Hooks.EditorUI.OnUpdateImageSubType,
    ({ payload: { image } }) => {
      if (editorKit.detectLinkTypeFunc) {
        const hyperlink = getUniNvPr(image)?.cNvPr.hlinkClick?.id
        if (hyperlink) {
          editorKit.detectLinkTypeFunc(hyperlink).then((res) => {
            if (res) {
              image.subType = res.type
            }
          })
        }
      }
    }
  )
  hookManager.register(
    Hooks.EditorUI.OnAddGuideline,
    ({ payload: { line } }) => {
      if (line) {
        editorKit.editorUI.guidelinesManager.addGuideLine(line)
      }
    }
  )
}

function needDrawSlideElement(
  editorKit: IEditorKit,
  sp: SlideElement,
  graphics: ShapeDrawerContext
) {
  const handler = editorKit.editorUI.editorHandler

  switch (sp.instanceType) {
    case InstanceType.ImageShape:
      if (
        EditorSettings.isSupportCrop &&
        handler.curState.instanceType === InstanceType.CropState &&
        !handler.curState.needDrawOnSlide(sp) &&
        graphics.instanceType === InstanceType.CanvasDrawer &&
        graphics.isRenderThumbnail !== true
      ) {
        return false
      }
      return true
    case InstanceType.Shape:
    case InstanceType.GroupShape:
    case InstanceType.GraphicFrame:
      return true
  }
}
