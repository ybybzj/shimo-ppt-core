import { Hooks } from '../../../core/hooks'
import { HookManager } from '../../../lib/hook'
import { invalidAllMastersThumbnails } from '../../../ui/editorUI/utils/utils'
import { Evt_onRedrawDocument } from '../../events/EventNames'
import { IEditorKit } from '../../type'
import { refreshPresentation } from '../../utils/editor'

export function register(hookManager: HookManager, editorKit: IEditorKit) {
  hookManager.register(
    Hooks.Editor.OnRefreshPresentation,
    ({ payload: { isDirty } }) => {
      refreshPresentation(editorKit, isDirty)
      if (isDirty) {
        invalidAllMastersThumbnails(editorKit.editorUI)
      }
    }
  )

  hookManager.register(Hooks.Editor.OnAsyncSlideLoaded, ({ payload: {} }) => {
    editorKit.emitEvent(Evt_onRedrawDocument)
  })
}
