import { breadcrumbTrail } from '../../../collaboration/breadcrumbTrail'
import { changeLockChecker } from '../../../collaboration/changeLockChecker'
import { Hooks } from '../../../core/hooks'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { logger } from '../../../lib/debug/log'
import { HookManager } from '../../../lib/hook'
import { IEditorKit } from '../../type'

let firstLockedEditType: EditActionFlag | undefined

export function register(hookManager: HookManager, editorKit: IEditorKit) {
  hookManager.register(Hooks.Document.GetViewMode, () => {
    return editorKit.getViewMode()
  })
  hookManager.register(Hooks.Document.CheckChangeStackLocked, ({ payload }) => {
    if (firstLockedEditType == null) firstLockedEditType = payload.editType
    if (changeLockChecker.checkInvalid()) {
      editorKit.trackError(
        'LOCKING_CHANGE',
        new Error('Core changestack locked too long'),
        {
          ['actionTrails']: breadcrumbTrail.get(),
          ['stopRecordingCouter']: payload.stopRecordingCouter,
          ['editType']: firstLockedEditType,
        }
      )
      changeLockChecker.reset()
    }
  })
  hookManager.register(Hooks.Document.ResetChangeStackLocked, () => {
    changeLockChecker.reset()
    firstLockedEditType = undefined
  })
  hookManager.register(Hooks.Document.InvalidChangeWithNoAction, () => {
    const error = new Error('Invalid change with no action')
    logger.error(error)
    editorKit.trackError('NO_ACTION_CHANGE', error, {
      ['actionTrails']: breadcrumbTrail.get(),
    })
  })

  hookManager.register(
    Hooks.Document.TrackPerf,
    ({ payload: { type, timing } }) => {
      editorKit.trackPerf(type, timing)
    }
  )
}
