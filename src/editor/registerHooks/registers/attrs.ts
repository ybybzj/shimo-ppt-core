import { Nullable } from '../../../../liber/pervasive'
import { breadcrumbTrail } from '../../../collaboration/breadcrumbTrail'
import { gClipboard } from '../../../copyPaste/gClipboard'
import { Hooks } from '../../../core/hooks'
import { ImageShape } from '../../../core/SlideElement/Image'
import {
  ImageWidgetType,
  isVideoImage,
} from '../../../core/utilities/shape/image'
import {
  ErrorInfoToImageUrlMap,
  ImageUrlToErrorInfoMap,
  isCustomAssetsUrl,
  isErrorPosterFrameUrl,
} from '../../../images/errorImageSrc'
import { gLoadImageExtraUrlParamsMap } from '../../../images/utils'
import { HookManager } from '../../../lib/hook'
import { invalidAllMastersThumbnails } from '../../../ui/editorUI/utils/utils'
import { EditorKit } from '../../global'
import { IEditorKit } from '../../type'
import { refreshPresentation } from '../../utils/editor'

export function register(hookManager: HookManager, editorKit: IEditorKit) {
  hookManager.register(
    Hooks.Attrs.CollectImageFromDataChange,
    ({ payload: { imageUrl } }) => {
      if (imageUrl) {
        editorKit.resourcesLoadManager.addImage(imageUrl)
      }
    }
  )

  hookManager.register(
    Hooks.Attrs.CollectBase64ImageFromDataChange,
    ({ payload: { dataUrl, sp } }) => {
      if (dataUrl && gClipboard.checkIsProcessing()) {
        editorKit.imageLoader.collectBase64ImageShape(dataUrl, sp)
      }
    }
  )

  hookManager.register(
    Hooks.Attrs.CollectFontFromDataChange,
    ({ payload: { font } }) => {
      if (font) {
        editorKit.resourcesLoadManager.addFont(font)
      }
    }
  )

  hookManager.register(
    Hooks.Attrs.CollectImageUrlParamsFromDataChange,
    ({ payload: { imgSrc, type, extX, extY } }) => {
      if (type === 'chart') {
        gLoadImageExtraUrlParamsMap.set(imgSrc, { type, extX, extY })
      } else if (imgSrc.indexOf('http') === 0) {
        gLoadImageExtraUrlParamsMap.set(imgSrc, { type: 'image', extX, extY })
      }
    }
  )
  hookManager.register(
    Hooks.Attrs.CollectAssetsForPasting,
    ({ payload: { assetUrl, assetObject } }) => {
      if (isCustomAssetsUrl(assetUrl) && gClipboard.checkIsProcessing()) {
        // console.info(`assetUrl for collect =>`, assetUrl, assetObject)
        editorKit.resourcesLoadManager.addAssetForPasting(assetUrl, assetObject)
      }
    }
  )

  hookManager.register(
    Hooks.Attrs.GroupCollectedResources,
    ({ payload: { type, refId, isApply } }) => {
      editorKit.resourcesLoadManager.groupCollectedResources(
        type,
        refId,
        isApply
      )
    }
  )
  hookManager.register(
    Hooks.Attrs.GetImageForChangeOfficalTheme,
    ({ payload: { url } }) => {
      const dataLoader = editorKit.dataLoader
      return dataLoader.themeReplaceImagesMap
        ? dataLoader.themeReplaceImagesMap[url]
        : undefined
    }
  )
  hookManager.register(
    Hooks.Attrs.CheckKnownErrorImageUrl,
    ({ payload: { url, media } }) => {
      if (media && isErrorPosterFrameUrl(media)) {
        ImageUrlToErrorInfoMap[url] = media
        checkLoadErrorPlaceholderImage(media)
      } else if (isErrorPosterFrameUrl(url)) {
        ImageUrlToErrorInfoMap[url] = url
        checkLoadErrorPlaceholderImage(url)
      }
    }
  )
  hookManager.register(
    Hooks.Attrs.getImageOriginUrl,
    ({ payload: { url } }) => {
      if (
        !gClipboard.isGettingCopyData &&
        typeof url === 'string' &&
        url.startsWith('data:')
      ) {
        const err = new Error('err base64')
        editorKit.trackError('CORE_TRACK', err, {
          ['actionTrails']: breadcrumbTrail.get(),
        })
      }

      if (editorKit.transformImageSrcFunc) {
        try {
          return editorKit.transformImageSrcFunc(url, 'origin')
        } catch {
          return url
        }
      }
      return url
    }
  )

  hookManager.register(
    Hooks.Attrs.GetImageWidgetType,
    ({ payload: { sp } }) => {
      return getImageWidgetType(sp)
    }
  )

  hookManager.register(
    Hooks.Attrs.GetLoadableImageByImageSrc,
    ({ payload: { src } }) => {
      return getCImageBySrc(src)
    }
  )
}

function getCImageBySrc(imgSrc: string) {
  let img = EditorKit.imageLoader.imagesMap[imgSrc]

  // 检测是否需要替换为指定占位图片
  const errInfo = ImageUrlToErrorInfoMap[imgSrc]
  if (errInfo) {
    const errPlaceholderImageUrl = ErrorInfoToImageUrlMap[errInfo]
    if (
      errPlaceholderImageUrl &&
      EditorKit.imageLoader.imagesMap[errPlaceholderImageUrl]
    ) {
      img = EditorKit.imageLoader.imagesMap[errPlaceholderImageUrl]
    }
  }
  return img
}

function getImageWidgetType(sp: ImageShape): Nullable<number> {
  const imageSrc = sp.blipFill?.imageSrcId
  if (isVideoImage(sp)) {
    return ImageWidgetType.Video
  } else if (EditorKit.imageLoader.imagesMap[imageSrc]?.isGif) {
    return ImageWidgetType.Gif
  }
}

function checkLoadErrorPlaceholderImage(errorInfo?: string) {
  if (
    errorInfo &&
    EditorKit.getPlaceholderImageByErrorInfo &&
    isErrorPosterFrameUrl(errorInfo) &&
    !ErrorInfoToImageUrlMap[errorInfo]
  ) {
    const image = EditorKit.getPlaceholderImageByErrorInfo(errorInfo)
    if (image) {
      EditorKit.imageLoader.loadImages([{ url: image }]).then(() => {
        refreshPresentation(EditorKit)
        invalidAllMastersThumbnails(EditorKit.editorUI, {
          images: [image],
        })
      })
      ErrorInfoToImageUrlMap[errorInfo] = image
    }
  }
}
