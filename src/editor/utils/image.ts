import { EditorUtil } from '../../globals/editor'

export function loadImages(images: string[]): Promise<any> {
  return Promise.all(
    images.map((img) => EditorUtil.imageLoader.loadImages([{ url: img }]))
  )
}
