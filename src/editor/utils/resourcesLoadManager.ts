import { Dict, Nullable } from '../../../liber/pervasive'
import { SlideMaster } from '../../core/Slide/SlideMaster'
import { AttrWithAssets } from '../../core/SlideElement/attrs/type'
import { EditorUtil } from '../../globals/editor'
import { StatusOfLoadableImage } from '../../images/utils'
import { invalidAllMastersThumbnails } from '../../ui/editorUI/utils/utils'
import { IEditorKit } from '../type'
import { refreshPresentation } from './editor'

type DataMapType = {
  ['presentation']?: Dict
  ['layouts']?: Dict<Dict>
  ['slideMasters']?: Dict<Dict>
  ['slides']?: Dict<Dict>
  ['themes']?: Dict<Dict>
  ['notesMasters']?: Dict<Dict>
  ['notes']?: Dict<Dict>
}

export type ResourcesContainerType = keyof DataMapType
export class ResourcesLoadManager {
  editorKit: IEditorKit
  loadingTasksMap: Dict = {}
  _tempFontsCollector: Dict = {}
  _tempImagesCollector: Dict = {}
  groupedImagesMap: DataMapType = {}
  groupedFontsMap: DataMapType = {}
  applyImagesMap = {}
  applyFontsMap = {}
  _isBaseResourcesLoaed = false
  _baseLoadTask: Nullable<Promise<any>>
  _fontsLoadingCounter = 0
  _slidesLoadingQueue: string[] = []
  _assetsForPasting: Record<string, AttrWithAssets[]> = {}
  _allowCollectAssets: boolean = true

  constructor(editor: IEditorKit) {
    this.editorKit = editor
  }

  addImage(url: string) {
    this._tempImagesCollector[url] = true
  }

  addFont(font: string) {
    const fontFile = EditorUtil.FontKit.getFontFile(font.toLowerCase())
    if (fontFile && fontFile.isUsable) {
      return
    }
    this._tempFontsCollector[font] = true
  }

  turnOffCollectAssets() {
    this._allowCollectAssets = false
  }

  turnOnCollectAssets() {
    this._allowCollectAssets = true
  }

  addAssetForPasting(url: string, attr: AttrWithAssets) {
    if (!this._allowCollectAssets) return
    const attrs = this._assetsForPasting[url] || []
    if (attrs.indexOf(attr) === -1) {
      attrs.push(attr)
    }
    this._assetsForPasting[url] = attrs
  }

  collectImages() {
    Object.assign(this.applyImagesMap, this._tempImagesCollector)
    this._tempImagesCollector = {}
  }

  resetApplyCollector() {
    this.applyImagesMap = {}
    this.applyFontsMap = {}
    this._assetsForPasting = {}
  }

  groupCollectedResources(
    elementsType: string,
    refId?: string,
    isApply?: boolean
  ) {
    const isApplyingChanges =
      isApply ?? this.editorKit.collaborationManager.isApplyingChanges()
    if (isApplyingChanges) {
      Object.assign(this.applyImagesMap, this._tempImagesCollector)
      Object.assign(this.applyFontsMap, this._tempFontsCollector)
    }

    let imagesGroup = (this.groupedImagesMap[elementsType] =
      this.groupedImagesMap[elementsType] || {})
    if (refId) {
      imagesGroup = imagesGroup[refId] = imagesGroup[refId] || {}
    }
    Object.assign(imagesGroup, this._tempImagesCollector)

    let fontsGroup = (this.groupedFontsMap[elementsType] =
      this.groupedFontsMap[elementsType] || {})
    if (refId) {
      fontsGroup = fontsGroup[refId] = fontsGroup[refId] || {}
    }
    Object.assign(fontsGroup, this._tempFontsCollector)

    this._tempImagesCollector = {}
    this._tempFontsCollector = {}
  }

  updateSlideLoadingQueue(startIndex: number = 0, endIndex: number = 0) {
    const slides = this.editorKit.presentation.slides
    const refIds: string[] = []
    for (let i = startIndex; i <= endIndex; i++) {
      const slide = slides[i]
      if (slide) {
        refIds.push(slide.getRefId())
      }
    }

    this._slidesLoadingQueue.forEach((refId) => {
      if (!refIds.includes(refId)) {
        refIds.push(refId)
      }
    })

    this._slidesLoadingQueue = refIds
  }

  async startLoading(): Promise<any> {
    const slides = this.editorKit.presentation.slides
    this._slidesLoadingQueue = slides.map((slide) => slide.getRefId())

    const loadBase = () => {
      const baseImageTasks = this.loadImages(
        Object.keys(this.groupedImagesMap['presentation'] ?? {})
      )
      this.loadFonts(Object.keys(this.groupedFontsMap['presentation'] ?? {}))

      return Promise.all(baseImageTasks)
    }

    const loadSlides = () => {
      if (!this._slidesLoadingQueue.length) {
        return
      }
      const refId = this._slidesLoadingQueue.shift()!
      return this._loadSlideResources(refId, false)
        .then(loadSlides)
        .catch(loadSlides)
    }
    const loadRemains = () => {
      const [images, fonts] = [this.groupedImagesMap, this.groupedFontsMap].map(
        (data) => {
          return Object.assign(
            {},
            ...Object.values(data['themes'] ?? {}),
            ...Object.values(data['slideMasters'] ?? {}),
            ...Object.values(data['layouts'] ?? {}),
            ...Object.values(data['notesMasters'] ?? {}),
            ...Object.values(data['notes'] ?? {})
          )
        }
      )
      const imageTasks = this.loadImages(Object.keys(images))
      this.loadFonts(Object.keys(fonts))
      return Promise.all(imageTasks)
    }

    this._baseLoadTask = loadBase()
    this._baseLoadTask.finally(() => {
      this._isBaseResourcesLoaed = true
    })

    return this._baseLoadTask
      .then(loadSlides)
      .then(loadRemains)
      .then(() => {
        if (this._fontsLoadingCounter !== 0) {
          refreshPresentation(this.editorKit, true)
          invalidAllMastersThumbnails(this.editorKit.editorUI)
        }
      })
  }

  _loadSlideResources(refId: string, isWaitFont = true): Promise<any> {
    const { images, fonts } = this._getSlideResources(refId)
    const imageTasks = this.loadImages(Object.keys(images))
    const fontTasks = this.loadFonts(Object.keys(fonts))
    return Promise.all(isWaitFont ? imageTasks.concat([fontTasks]) : imageTasks)
  }

  checkSlideLoaded(refId: string, isWait = false): boolean | Promise<any> {
    if (isWait) {
      return Promise.all([this._baseLoadTask, this._loadSlideResources(refId)])
    }
    if (!this._isBaseResourcesLoaed) {
      return false
    }
    const imageLoader = this.editorKit.imageLoader
    const FontKit = EditorUtil.FontKit
    const { images, fonts } = this._getSlideResources(refId)
    const hasUnloadResource =
      Object.keys(images).find((image) => {
        if (image == null) return false
        const imageResource = imageLoader.getImage(image)
        if (imageResource == null) return true
        if (
          imageResource.Status === StatusOfLoadableImage.Loading ||
          StatusOfLoadableImage.Init
        ) {
          return true
        }
      }) ||
      Object.keys(fonts).find((font) => {
        if (font && FontKit.getFontFile(font)?.isLoading === true) {
          return true
        }
      })
    return !hasUnloadResource
  }

  loadImages(images: string[] = []) {
    return images.map((image) => {
      return (this.loadingTasksMap[image] =
        this.loadingTasksMap[image] ?? this._loadImage(image))
    })
  }
  private async _loadImage(image: string) {
    return this.editorKit.imageLoader.loadImages([{ url: image }], () => {
      refreshPresentation(this.editorKit)
      invalidAllMastersThumbnails(this.editorKit.editorUI, {
        images: [image],
      })
    })
  }

  loadFonts(fonts: string[] = []) {
    const newFonts: string[] = []
    for (const font of fonts) {
      if (this.loadingTasksMap[font] !== true) {
        newFonts.push(font)
        this.loadingTasksMap[font] = true
      }
    }

    if (newFonts.length > 0) {
      return this._loadFont(newFonts)
    }
    return Promise.resolve(false)
  }
  private async _loadFont(fonts: string[]) {
    return EditorUtil.FontLoader.loadFonts(fonts, 4, (isFinished: boolean) => {
      this._fontsLoadingCounter++
      if (isFinished || this._fontsLoadingCounter > 4) {
        refreshPresentation(this.editorKit, true)
        invalidAllMastersThumbnails(this.editorKit.editorUI)
        this._fontsLoadingCounter = 0
      }
    })
  }

  getResourceOfSlideMaster(
    master: SlideMaster,
    resourceType: 'image' | 'font',
    pred: (src: string) => boolean,
    isFindAll = true
  ): string[] {
    if (master == null || master.refId == null) return []

    const resources = _findMasterResources(
      resourceType === 'image' ? this.groupedImagesMap : this.groupedFontsMap,
      master,
      pred,
      isFindAll
    )
    return resources
  }

  _getSlideResources(slideRefId: string): {
    images: Dict
    fonts: Dict
  } {
    const slide = this.editorKit.getSlideByRefId(slideRefId)
    const layout = slide?.layout
    const layoutRefId = layout?.refId
    const master = layout?.master
    const masterRefId = master?.refId
    const theme = master?.theme
    const themeRefId = theme?.refId
    const notes = slide?.notes
    const noteRefId = notes?.refId
    const notesMaster = notes?.master
    const notesMasterRefid = notesMaster?.refId

    const [images, fonts] = [this.groupedImagesMap, this.groupedFontsMap].map(
      (data) => {
        return Object.assign(
          {},
          data['slides'] && slideRefId ? data['slides'][slideRefId] : undefined,
          data['layouts'] && layoutRefId
            ? data['layouts'][layoutRefId]
            : undefined,
          data['slideMasters'] && masterRefId
            ? data['slideMasters'][masterRefId]
            : undefined,
          data['themes'] && themeRefId ? data['themes'][themeRefId] : undefined,
          data['notes'] && noteRefId ? data['notes'][noteRefId] : undefined,
          data['notesMasters'] && notesMasterRefid
            ? data['notesMasters'][notesMasterRefid]
            : undefined
        )
      }
    )

    return {
      images,
      fonts,
    }
  }

  // loadImageForChartObject(chartObject: ChartObject) {
  //   if (
  //     chartObject.isValid() &&
  //     typeof this.editorKit.prepareChartObjectImage === 'function'
  //   ) {
  //     return this._loadImageForChartObject(chartObject)
  //   }
  // }
}

// helpers
// function _collectMasterResources(resources: DataMapType, master: SlideMaster): Dict {
//   const masterRefId = master.refId!
//   const theme = master.Theme
//   const themeRefId = theme?.refId
//   const layoutIds = master.layouts
//     .map((layout) => layout.refId)
//     .filter(Boolean) as string[]
//   let ret: Dict = {}
//   const layoutRes = resources['layouts']
//   if (layoutRes) {
//     for (const lId of layoutIds) {
//       if (layoutRes[lId]) {
//         ret = Object.assign(ret, layoutRes[lId])
//       }
//     }
//   }
//
//   const masterRes = resources['slideMasters']
//   if (masterRes && masterRes[masterRefId]) {
//     ret = Object.assign(ret, masterRes[masterRefId])
//   }
//
//   const themeRes = resources['themes']
//   if (themeRes && themeRefId != null && themeRes[themeRefId]) {
//     ret = Object.assign(ret, themeRes[themeRefId])
//   }
//
//   return ret
// }

function _findMasterResources(
  resources: DataMapType,
  master: SlideMaster,
  pred: (src: string) => boolean,
  isFindAll = true
): string[] {
  const masterRefId = master.refId!
  const theme = master.theme
  const themeRefId = theme?.refId
  const layoutIds = master.layouts
    .map((layout) => layout.refId)
    .filter(Boolean) as string[]

  const ret: string[] = []
  const layoutRes = resources['layouts']

  if (layoutRes) {
    for (const lId of layoutIds) {
      if (layoutRes[lId]) {
        const res = _findInKeys(layoutRes[lId], pred)
        if (res != null) {
          ret.push(res)
          if (isFindAll === false) {
            return ret
          }
        }
      }
    }
  }

  const masterRes = resources['slideMasters']
  if (masterRes && masterRes[masterRefId]) {
    const res = _findInKeys(masterRes[masterRefId], pred)

    if (res != null) {
      ret.push(res)
      if (isFindAll === false) {
        return ret
      }
    }
  }

  const themeRes = resources['themes']
  if (themeRes && themeRefId != null && themeRes[themeRefId]) {
    const res = _findInKeys(themeRes[themeRefId], pred)
    if (res != null) {
      ret.push(res)
      if (isFindAll === false) {
        return ret
      }
    }
  }

  return ret
}

function _findInKeys(
  obj: Dict,
  pred: (key: string) => boolean
): string | undefined {
  for (const key of Object.keys(obj)) {
    if (pred(key)) {
      return key
    }
  }

  return undefined
}
