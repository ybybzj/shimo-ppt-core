import { debounce } from '../../common/utils'
import { IEditorKit } from '../type'

let _isCalc = false

export const refreshPresentation: (
  editorKit: IEditorKit,
  isCalc?: boolean
) => void = (editorKit, isCalc) => {
  if (!_isCalc) {
    _isCalc = !!isCalc
  }
  refreshPresentationDebounce(editorKit)
}

const refreshPresentationDebounce: (editorKit: IEditorKit) => void = debounce(
  (editorKit: IEditorKit) => {
    if (editorKit.isEditorUIInitComplete) {
      editorKit.refreshPPT(_isCalc)
      _isCalc = false
    }
  },
  200
)
