import { IEditorKit } from '../type'
import { MouseMoveData } from '../../core/properties/MouseMoveData'
import {
  Evt_onCurrentPage,
  Evt_onMouseMove,
  Evt_onThumbnailsShow,
  Evt_onEndAddShape,
  Evt_viewSourceImage,
} from '../events/EventNames'
import { EditorSettings } from '../../core/common/EditorSettings'
import { splitterStates } from '../../ui/editorUI/splitterStates'
import { isGroup } from '../../core/utilities/shape/asserts'

import { handleBeginCompositeInput } from '../../ui/editorUI/utils/keys'
import { getSelectedSlideElementsByTypes } from '../../core/utilities/presentation/getters'
import { ImageLikeInfo } from './slide'
import { LoadableImage } from '../../images/utils'

// 切换幻灯片时
export function emitCurrentSlide(this: IEditorKit, number) {
  this.emitEvent(Evt_onCurrentPage, number)
}

export function onImageLoaded(this: IEditorKit, image: LoadableImage) {
  if (this.imageLoadedCallback) this.imageLoadedCallback(image)
  else {
    this.presentation.addImageShape(50, 50, {
      url: image.src,
      encryptUrl: image.encryptSrc,
    })
  }
}

export function emitMouseMove(this: IEditorKit, mouseData?: MouseMoveData) {
  this.emitEvent(Evt_onMouseMove, mouseData)
}

export function emitThumbnailsShow(this: IEditorKit) {
  let isShow = true
  if (0 === splitterStates.thumbnailSplitterPos) isShow = false
  this.emitEvent(Evt_onThumbnailsShow, isShow)
}

let viewModeInLoad: boolean | undefined

export function setViewMode(this: IEditorKit, isViewMode?: boolean) {
  if (EditorSettings.asyncLoadingSlides !== true) {
    if (isViewMode != null) {
      _setViewMode(this, isViewMode)
    }
    return
  }

  const isLoading = this.presentation.isLoadComplete !== true

  if (isLoading) {
    viewModeInLoad = isViewMode
    return
  } else if (isViewMode != null) {
    viewModeInLoad = undefined
  }

  if (viewModeInLoad != null && isViewMode == null) {
    isViewMode = viewModeInLoad
    viewModeInLoad = undefined
  }

  if (isViewMode == null) return
  _setViewMode(this, isViewMode)
}

function _setViewMode(editorKit: IEditorKit, isViewMode: boolean) {
  const isViewModeChanged = EditorSettings.isViewMode !== !!isViewMode
  EditorSettings.isViewMode = !!isViewMode

  if (isViewModeChanged) {
    const { slideRenderer, renderingController } = editorKit.editorUI
    slideRenderer.updateRenderInfo(renderingController.curSlideIndex)
  } else {
    editorKit.editorUI.onResize(true)
  }
  editorKit.clearEditLayer()
}

export function setLoopShow(this: IEditorKit, isLoop) {
  this.presentation.setLoopShow(isLoop)
}

export function emitEndAddShape(this: IEditorKit) {
  this.emitEvent(Evt_onEndAddShape)
  this.editorUI.mouseCursor.onEndAddShape()
}

export function emitViewSourceImage(this: IEditorKit, imgInfo: ImageLikeInfo) {
  this.emitEvent(Evt_viewSourceImage, imgInfo)
}

// input
export function startCompositeInput(this: IEditorKit) {
  if (this.presentation) {
    return handleBeginCompositeInput(this.editorUI)
  }
  return null
}

export function replaceCompositeInput(this: IEditorKit, charCodes) {
  if (this.presentation) {
    return this.presentation.replaceCompositeInput(charCodes)
  }
  return null
}

export function endComposite(this: IEditorKit) {
  if (this.presentation) {
    return this.presentation.endComposite()
  }
  return null
}

export function updateInputPos(this: IEditorKit) {
  if (this.presentation) {
    this.editorUI.textCursor.updateWithInputContext()
  }
}

export function onKeyPress(this: IEditorKit, e: KeyboardEvent) {
  return this.editorUI.onKeyPress(e)
}

// Todo:rename with ui
export function getSelectedSlideElementsCount(this: IEditorKit) {
  if (!this.editorUI) {
    return 0
  }
  if (!this.presentation) {
    return 0
  }
  return this.presentation.getSelectedSpsCount()
}

export function getSelectedElementsNumberByTypes(this: IEditorKit): {
  shapes: number
  images: number
  groups: number
  tables: number
} {
  const ret = {
    ['shapes']: 0,
    ['images']: 0,
    ['groups']: 0,
    ['tables']: 0,
  }
  const presentation = this.presentation
  const slide = presentation.slides[presentation.currentSlideIndex]
  if (slide) {
    const selectedElementsByTypes = getSelectedSlideElementsByTypes(
      presentation,
      true
    )
    const getTopGroup = (item) => {
      if (item.group) {
        const group = getTopGroup(item.group)
        if (group) {
          return group
        }
      }
      return isGroup(item) ? item : null
    }
    const groups = selectedElementsByTypes['groups']
    for (const type in selectedElementsByTypes) {
      selectedElementsByTypes[type] = selectedElementsByTypes[type].filter(
        (item) => {
          const group = getTopGroup(item)
          return !(group !== item && groups.indexOf(group) !== -1)
        }
      )
      const counts = selectedElementsByTypes[type].length
      switch (type) {
        case 'shapes':
          ret['shapes'] += counts
          break
        case 'images':
          ret['images'] += counts
          break
        case 'groups':
          ret['groups'] += counts
          break
        case 'tables':
          ret['tables'] += counts
          break
      }
    }
  }
  return ret
}

export function isFocusOnText(this: IEditorKit): boolean {
  const selectionState = this.presentation.selectionState
  if (selectionState) {
    return !!selectionState.rootTextSelection
  }
  return false
}

export function setViewModeCopyable(this: IEditorKit, copyable = false) {
  EditorSettings.isViewModeCopyable = copyable
  this.clearEditLayer()
}
