import { Nullable } from '../../../liber/pervasive'
import { EditorSettings } from '../../core/common/EditorSettings'
import { Comment } from '../../core/Slide/Comments/Comment'

import {
  startAddComment as _startAddComment,
  endAddComment as _endAddComment,
  onAddComment,
} from '../../ui/features/comment/utils'
import { CmContentData } from '../../io/dataType/comment'
import {
  Evt_onCommentChange,
  Evt_onEndAddComment,
  Evt_onHoveredCommentChange,
  Evt_onSelectedCommentChange,
  Evt_onStartAddComment,
  Evt_onToggleCommentShow,
  Evt_onUpdateCommentPosition,
} from '../events/EventNames'
import { IEditorKit } from '../type'
import { InputCommentData } from '../../core/utilities/comments'

/** 支持添加原生评论和扩展评论, 扩展评论信息均透明存储于 textExt 内, 返回 uid*/
export function addComment(
  this: IEditorKit,
  data: InputCommentData
): string | undefined {
  if (!EditorSettings.isSupportComment) return

  const handler = this.editorUI.editorHandler
  const comment: Nullable<Comment> = onAddComment(handler, data)
  if (comment) return comment.getUid()
  return
}

// ------------------------- callbacks -------------------------
export function emitHoveredCommentChangeCallback(
  this: IEditorKit,
  cmContentData?: CmContentData
) {
  this.emitEvent(Evt_onHoveredCommentChange, cmContentData)
}

export function emitSelectedCommentChangeCallback(
  this: IEditorKit,
  cmContentData?: CmContentData
) {
  this.emitEvent(Evt_onSelectedCommentChange, cmContentData)
}

/** has not tmp comment icon, emit to call comment ui */
export function emitToggleShowComment(this: IEditorKit) {
  this.emitEvent(Evt_onToggleCommentShow)
}

/** has add tmp comment icon, emit ui to input */
export function emitStartAddComment(
  this: IEditorKit,
  afterIndex: number,
  slideRefId: string
) {
  this.emitEvent(Evt_onStartAddComment, {
    ['afterIndex']: afterIndex,
    ['slideRefId']: slideRefId,
  })
}

/** enter addCommentState */
export function startAddComment(this: IEditorKit) {
  if (!EditorSettings.isSupportComment) return
  const handler = this.editorUI.editorHandler
  _startAddComment(handler)
}

/** end addCommentState */
export function endAddComment(this: IEditorKit) {
  if (!EditorSettings.isSupportComment) return
  const handler = this.editorUI.editorHandler
  _endAddComment(handler)
}

/** has add tmp comment icon, click other area, emit ui to cancel input */
export function emitEndAddComment(this: IEditorKit) {
  this.emitEvent(Evt_onEndAddComment)
}

export function emitRemoveComment(this: IEditorKit) {
  this.emitEvent(Evt_onCommentChange)
  this.emitEvent(Evt_onSelectedCommentChange)
}

export function emitAddComment(this: IEditorKit) {
  this.emitEvent(Evt_onCommentChange)
}

/**
 * 发送后 ui 全量查询变更
 * @发送场景
 * -  发起者: 直接操作评论/删除含评论的Slide/删除不含评论的Slide, 但即将跳转的Slide含评论
 * -  协作者: Applying Delta 中 PPTDataLayout 层级含有 comment 变更/ 删除 Slide 变更
 */
export function emitChangeComment(this: IEditorKit) {
  this.emitEvent(Evt_onCommentChange)
}

export function onCommentAuthorChange(this: IEditorKit) {
  this.presentation.onCommentAuthorChange()
}

export function emitUpdateCommentPosition(this: IEditorKit, id, afterIndex) {
  this.emitEvent(Evt_onUpdateCommentPosition, {
    ['id']: id,
    ['afterIndex']: afterIndex,
  })
}
