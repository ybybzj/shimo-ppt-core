/*functions for working with zoom & navigation*/
import { IEditorKit } from '../type'
import {
  Evt_onNoteAreaHeightChange,
  Evt_onZoomChange,
} from '../events/EventNames'
import { toInt } from '../../common/utils'
import { ZoomValue } from '../../core/common/zoomValue'
import { executeWithCenterKeep } from '../../ui/editorUI/utils/utils'

export function zoomIn(this: IEditorKit) {
  if (!this.editorUI.canZoom()) return
  this.editorUI.zoomIn()
}

export function zoomOut(this: IEditorKit) {
  if (!this.editorUI.canZoom()) return
  this.editorUI.zoomOut()
}

export function zoomFitToPage(this: IEditorKit) {
  if (!this.editorUI.canZoom()) return
  executeWithCenterKeep(this.editorUI, () => {
    this.editorUI.zoomFitPage()
  })
}

export function zoomFitToWidth(this: IEditorKit) {
  if (!this.editorUI.canZoom()) return
  executeWithCenterKeep(this.editorUI, () => {
    this.editorUI.zoomFitWidth()
  })
}

export function zoom(this: IEditorKit, percent) {
  if (!this.editorUI.canZoom()) return
  executeWithCenterKeep(this.editorUI, () => {
    ZoomValue.value = percent
    this.editorUI.exeZoom(0)
  })
}

export function getZoomValue(this: IEditorKit) {
  return ZoomValue.value || 0
}

export function goToSlide(this: IEditorKit, number) {
  this.editorUI.goToSlide(number)
}

export function goToSlideByRefId(this: IEditorKit, refId) {
  const slides = this.presentation.slides
  const index = slides.findIndex((slide) => slide.getRefId() === refId)
  if (index !== -1) {
    this.editorUI.goToSlide(index)
  }
}

export function getAmountOfSlides(this: IEditorKit) {
  return this.editorUI.renderingController.countOfSlides
}

/*callbacks*/
export function emitZoomChange(this: IEditorKit, percent, type) {
  this.emitEvent(Evt_onZoomChange, percent, type)
}

export function emitChangeNotesHeight(this: IEditorKit, noteAreaHeight) {
  this.emitEvent(Evt_onNoteAreaHeightChange, toInt(noteAreaHeight))
}

export function clearEditLayer(this: IEditorKit) {
  this.editorUI.renderingController.editLayer.clear()
}
