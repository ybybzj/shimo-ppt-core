import {
  getLayoutsFromDeltaString,
  getMastersFromDeltaString,
  getThemesFromDeltaString,
} from '../../copyPaste/paste/fromDeltaString'
import { SlideMaster } from '../../core/Slide/SlideMaster'
import { DEFAULT_THEME_NAME } from '../../core/SlideElement/attrs/creators'
import { translator } from '../../globals/translator'
import { EditActionFlag } from '../../core/EditActionFlag'
import { generateMasterThumbnails } from '../../ui/editorUI/utils/utils'
import { IEditorKit } from '../type'
import { startEditAction } from '../../core/calculation/informChanges/startAction'
import { Theme } from '../../core/SlideElement/attrs/theme'
import { SlideLayout } from '../../core/Slide/SlideLayout'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../core/common/utils'
import { convertRGBToHexString } from '../../common/utils/color'
import { defaultRGBAColor } from '../../core/color/complexColor'

type BuiltInThemeInfo = {
  id?: string
  refId: string
  name: string
  coverImage: string
  colors: string[]
  layoutCount: number
  titleFont: string
  bodyFont: string
}

export type ApplyType = 'all' | 'selected' | 'matching'

export type ThemeInfo = {
  id: string
  theme: string
  images: Record<string, string>
  type?: ApplyType
}

const _rgba = defaultRGBAColor()

export function getBuiltInThemes(this: IEditorKit): BuiltInThemeInfo[] {
  const { slideMasters, preservedSlideMasters } = this.presentation
  const masters = slideMasters.concat(
    preservedSlideMasters.filter(
      (preservedSlideMaster) =>
        !slideMasters.find(
          (master) => master?.refId === preservedSlideMaster.refId
        )
    )
  )
  return masters.map((master) => {
    const accentColors: string[] = []
    let titleFont = ''
    let bodyFont = ''
    if (master.theme) {
      const { clrScheme, fontScheme } = master.theme.themeElements
      if (clrScheme.colors) {
        // color from Accent1 to Accent6
        for (let i = 0; i < 6; i++) {
          clrScheme.colors[i]?.check(null, null, _rgba)
          accentColors.push(
            `#${convertRGBToHexString(_rgba.r, _rgba.g, _rgba.b)}`
          )
        }
      }
      titleFont = fontScheme.majorFont.latin ?? ''
      bodyFont = fontScheme.minorFont.latin ?? ''
    }

    let themeName = master.theme?.name || ''
    if (themeName === DEFAULT_THEME_NAME) {
      themeName = 'default_theme'
    }

    if (!master.imgBase64) {
      generateMasterThumbnails(this.editorUI, master)
    }

    return {
      ['id']: master.themeId,
      ['refId']: master.getRefId(),
      ['name']: translator.getValue(themeName),
      ['coverImage']: master.imgBase64,
      ['colors']: accentColors,
      ['layoutCount']: master.layouts.length,
      ['titleFont']: titleFont,
      ['bodyFont']: bodyFont,
    }
  })
}

export function changeBuiltInTheme(
  this: IEditorKit,
  refId: string,
  type?: ApplyType
) {
  const { slideMasters, preservedSlideMasters } = this.presentation
  const master =
    slideMasters.find((master) => master?.refId === refId) ??
    preservedSlideMasters.find((master) => master.refId === refId)
  if (master) {
    const isSelectedSlides = type === 'selected'
    applyMaster(this, master, isSelectedSlides)
  }
}

export function applyOfficalTheme(this: IEditorKit, themeInfo: ThemeInfo) {
  const themeId = themeInfo['id']
  const themeData = themeInfo['theme']
  const imagesMap = themeInfo['images']
  const applyType = themeInfo['type']

  const { slideMasters } = this.presentation
  const master =
    slideMasters.find((master) => themeId && master.themeId === themeId) ??
    parseThemeInfo(this, themeId, themeData, imagesMap)
  if (master) {
    const isSelectedSlides = applyType === 'selected'
    applyMaster(this, master, isSelectedSlides)
  }
}

function applyMaster(
  editorKit: IEditorKit,
  master: SlideMaster,
  isSelectedSlides?: boolean
) {
  const presentation = editorKit.presentation
  startEditAction(EditActionFlag.action_Presentation_ChangeTheme)
  const selectedSlideIndices = presentation.getSelectedSlideIndices()
  presentation.applySlideMaster(
    master,
    isSelectedSlides ? selectedSlideIndices : null
  )

  generateMasterThumbnails(editorKit.editorUI, master)
  editorKit.editorUI.updateLayouts()

  !isSelectedSlides && editorKit.editorUI.thumbnailsManager.updateAll()

  // 加载图片字体
  editorKit.dataLoader.applyAsyncAssets()
}

function parseThemeInfo(
  editorKit: IEditorKit,
  themeId: string,
  themeData: string,
  imagesMap: Record<string, string> = {}
): SlideMaster {
  const s = turnOffRecordChanges()
  const content = JSON.parse(themeData)
  let master: SlideMaster
  let theme: Theme
  let layouts: SlideLayout[]
  editorKit.dataLoader.applyWithThemeImageReplacement(() => {
    for (const type in content) {
      const deltaString = content[type]
      switch (type) {
        case 'themes':
          theme = getThemesFromDeltaString(editorKit, deltaString, true)[0]
          break
        case 'masters':
          master = getMastersFromDeltaString(editorKit, deltaString, true)[0]
          break
        case 'layouts':
          layouts = getLayoutsFromDeltaString(editorKit, deltaString, true)
          break
      }
    }
  }, imagesMap)
  master!.themeId = themeId
  master!.theme = theme!
  layouts!.forEach((layout) => {
    if (layout) {
      master.addLayout(layout)
      layout.setMaster(master)
    }
  })
  restoreRecordChangesState(s)
  return master!
}
