import { IEditorKit } from '../type'
import { Evt_onScriptError } from '../events/EventNames'
import { EditorUI } from '../../ui/editorUI/EditorUI'
import { SlidePreviewImages } from '../../ui/SlidePreviewImages'
import { registerEditorHooks } from '../registerHooks/registerHooks'
import { hookManager as editorHookManager } from '../../core/hooks'
import { EditorUtil } from '../../globals/editor'
import { Presentation } from '../../core/Slide/Presentation'
import { EditorSettings } from '../../core/common/EditorSettings'
import { Errors } from '../../exports/type'
import { EventNames } from '../events'

export function initEditorKit(editorKit: IEditorKit): void {
  editorKit.imageLoader = EditorUtil.imageLoader
  editorKit.imageLoader.init(editorKit)

  editorKit.presentation = new Presentation()
  EditorUtil.ChangesStack.setPresentation(editorKit.presentation)

  // slidesPreviewer init
  editorKit.slidesPreviewer = new SlidePreviewImages(editorKit)

  editorKit.editorUI = new EditorUI(editorKit)
  // ----- from setViewMode start----
  editorKit.editorUI.setNotesEnable(EditorSettings.isMobile ? false : true)

  editorKit.editorUI.onResize(true)

  _onScriptError(editorKit)
  registerEditorHooks(editorHookManager, editorKit)
}

function _onScriptError(editorKit: IEditorKit): void {
  const orgOnError = window.onerror
  window.onerror = (errorMsg, url, lineNumber, column, error) => {
    const msg = `Error: ${errorMsg} Script: ${url} Line: ${lineNumber}:${column} userAgent: ${
      navigator.userAgent || navigator.vendor || (window as any).opera
    } platform: ${navigator.platform} isPPTLoaded: ${
      editorKit.presentation.isLoadComplete
    } StackTrace: ${error ? error.stack : ''}`
    editorKit.emitEvent(Evt_onScriptError, msg)
    if (!EditorUtil.ChangesStack.isOn()) {
      editorKit.trackError('ERROR_BETWEEN_CHANGE', error!, {
        ['stopRecordingCouter']:
          EditorUtil.ChangesStack.getStopRecordingCouter(),
      })
      this.emitEvent(EventNames.Evt_onError, Errors.ErrorChange)
    }

    //send only first error to reduce number of requests. also following error may be consequences of first
    window.onerror = orgOnError
    if (orgOnError) {
      return orgOnError.apply(window, [msg, url, lineNumber, column, error])
    } else {
      return false
    }
  }
}
