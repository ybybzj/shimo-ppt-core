import { Factor_pix_to_mm } from '../../../core/common/const/unit'
import { Offset, Size } from '../../../core/common/types'
import { Slide } from '../../../core/Slide/Slide'
import { TextWarpType } from '../../../core/SlideElement/const'
import { createGeometry } from '../../../core/SlideElement/geometry/creators'

import { checkExtentsByTextContent } from '../../../core/utilities/shape/extents'
import {
  addToParentSpTree,
  resetTxBodyForShape,
  setDeletedForSp,
} from '../../../core/utilities/shape/updates'
import { EditActionFlag } from '../../../core/EditActionFlag'
import { InstanceType } from '../../../core/instanceTypes'
import { IEditorKit } from '../../type'
import { BodyPr } from '../../../core/SlideElement/attrs/bodyPr'
import { FillEffects } from '../../../core/SlideElement/attrs/fill/fill'
import { SpPr, Xfrm } from '../../../core/SlideElement/attrs/shapePrs'
import { TextFit } from '../../../core/SlideElement/attrs/TextFit'
import { calculateShapeContent } from '../../../core/calculation/calculate/shapeContent'
import { Shape } from '../../../core/SlideElement/Shape'
import { startEditAction } from '../../../core/calculation/informChanges/startAction'
import { getExtents } from '../../../ui/features/addShape/utils'
import { ShapeStyle } from '../../../core/SlideElement/attrs/ShapeStyle'
import { ParaPr } from '../../../core/textAttributes/ParaPr'

export interface AddShapeOptions {
  offset?: Offset
  size?: Size
}

export type AddedShapeInfo = Promise<
  {
    id: string
    refId: string
    slideRefId?: string
  }[]
>

export function addTextBox(
  this: IEditorKit,
  options: AddShapeOptions = {}
): AddedShapeInfo {
  return new Promise((resolve) => {
    const presentation = this.presentation
    const controller = this.editorUI.editorHandler
    const preset = 'textRect'
    presentation.beforeStartAddShape()

    const slide = controller.editFocusContainer
    if (slide && slide.instanceType === InstanceType.Slide) {
      startEditAction(EditActionFlag.action_Slide_AddNewShape)
      const shape = this.getShape(slide, preset, options)
      if (slide.cSld) {
        shape.setParent(slide)
        shape.resetCalcStatus()
      }
      addToParentSpTree(shape)
      presentation.removeSelection()
      controller.startCalculate()
      resolve([{ id: shape.getId(), refId: shape.getRefId() }])
    }
  })
}

export function getShape(
  this: IEditorKit,
  slide: Slide,
  preset: string,
  options: AddShapeOptions
) {
  const theme = slide.getTheme()
  const { offset, size } = options
  const { extX, extY } = size
    ? {
        extX: size.width * Factor_pix_to_mm,
        extY: size.height * Factor_pix_to_mm,
      }
    : getExtents(preset)
  const { x: offX, y: offY } = offset
    ? {
        x: offset.x * Factor_pix_to_mm,
        y: offset.y * Factor_pix_to_mm,
      }
    : { x: 0, y: 0 }

  const shape = new Shape()
  shape.spTreeParent = slide.cSld

  shape.setSpPr(new SpPr())
  shape.spPr!.setParent(shape)
  const x = offX
  const y = offY
  const xfrm = new Xfrm(shape.spPr)
  shape.spPr!.setXfrm(xfrm)
  xfrm.setParent(shape.spPr)
  xfrm.setOffX(x)
  xfrm.setOffY(y)
  xfrm.setExtX(extX)
  xfrm.setExtY(extY)
  xfrm.setFlipH(false)
  xfrm.setFlipV(false)

  setDeletedForSp(shape, false)

  if (preset === 'textRect') {
    shape.spPr!.setGeometry(createGeometry('rect'))
    const fill = FillEffects.NoFill()
    shape.spPr!.setFill(fill)

    const body_pr = BodyPr.Default()
    let paraPr: ParaPr | undefined
    const txDef = theme?.txDef
    if (txDef) {
      const { bodyPr, lstStyle, spPr } = txDef
      if (bodyPr) {
        body_pr.merge(bodyPr.clone())
      }
      const defParaPr = lstStyle?.levels[9]
      if (defParaPr) {
        paraPr = defParaPr
      }
      if (spPr.fillEffects) {
        shape.spPr!.setFill(spPr.fillEffects.clone())
      }
      if (spPr.ln) {
        if (shape.spPr!.ln) {
          shape.spPr!.ln.merge(spPr.ln)
        } else {
          shape.spPr!.setLn(spPr.ln.clone())
        }
      }
    }

    body_pr.textFit = TextFit.new(TextWarpType.NONE)
    body_pr.wrap = TextWarpType.SQUARE

    resetTxBodyForShape(shape, body_pr, paraPr)

    const xfrm = shape.spPr!.xfrm
    if (xfrm) {
      const orgX = xfrm.offX
      const orgY = xfrm.offY
      shape.setParent(slide)
      calculateShapeContent(shape)
      checkExtentsByTextContent(shape)
      xfrm.setExtX(extX)
      xfrm.setOffX(orgX)
      xfrm.setOffY(orgY)
      shape.extX = extX
      shape.extY = extY
    }
  } else {
    if (!shape.spPr!.geometry) {
      shape.spPr!.setGeometry(createGeometry(preset))
    }
    shape.setStyle(ShapeStyle.Default(preset))
    const spDef = theme?.spDef
    if (spDef) {
      if (spDef.style) {
        shape.setStyle(spDef.style.clone())
      }
      if (spDef.spPr) {
        if (spDef.spPr.fillEffects) {
          shape.spPr!.setFill(spDef.spPr.fillEffects.clone())
        }
        if (spDef.spPr.ln) {
          if (shape.spPr!.ln) {
            shape.spPr!.ln.merge(spDef.spPr.ln)
          } else {
            shape.spPr!.setLn(spDef.spPr.ln.clone())
          }
        }
      }
    }
  }
  shape.x = x
  shape.y = y

  return shape
}
