import { IEditorKit } from '../type'
import { Evt_onPaintFormatChanged } from '../events/EventNames'
import { FocusTarget } from '../../core/common/const/drawing'
import { StatusOfFormatPainterValues } from '../../ui/editorUI/const'

export function canEdit(this: IEditorKit) {
  return (
    !this.editorUI.showManager.isInShowMode &&
    this.editorUI.presentation.canEdit()
  )
}

export function remove(this: IEditorKit) {
  this.editorUI.editorHandler.remove(-1, true)
}

export function selectAll(this: IEditorKit, isSelectByFocus = false) {
  const presentation = this.presentation
  const focusType = presentation.getFocusType()
  if (isSelectByFocus && focusType === FocusTarget.Thumbnails) {
    this.selectAllSlides()
  } else {
    this.editorUI.editorHandler.selectAll()
  }
}

export function setPaintFormat(
  this: IEditorKit,
  value: StatusOfFormatPainterValues
) {
  this.paintFormatState = value
  this.editorUI.editorHandler.copyFormat()
  this.emitEvent(Evt_onPaintFormatChanged, value)
}

export function emitPaintFormat(
  this: IEditorKit,
  value: StatusOfFormatPainterValues
) {
  this.paintFormatState = value
  return this.emitEvent(Evt_onPaintFormatChanged, value)
}

export function pastePaintFormat(this: IEditorKit) {
  this.editorUI.editorHandler.pastePaintFormat()
}

export function IsFocus(this: IEditorKit) {
  let ret = false
  if (this.editorUI.isFocus) ret = true

  return ret
}

export function Resize(this: IEditorKit) {
  if (false === this.isEditorUIInitComplete) return
  this.editorUI.onResize(false)
}
