import { IEditorKit } from '../type'
import {
  Evt_onStartPPTShow,
  Evt_onEndPPTShow,
  Evt_onPPTShowSlideChanged,
  Evt_onPPTShowAction,
} from '../events/EventNames'
import { EditActionFlag } from '../../core/EditActionFlag'
import { gShowSetting } from '../../ui/transitions/PPTShowSettings'
import {
  ShowAction,
  ShowState,
  ShowStartOption,
} from '../../ui/transitions/ShowManager'
import {
  SlideTransition,
  TransitionTypes,
} from '../../core/Slide/SlideTransition'
import { syncInterfaceState } from '../interfaceState/syncInterfaceState'
import { startEditAction } from '../../core/calculation/informChanges/startAction'
import { BrowserInfo } from '../../common/browserInfo'

export function emitEndShow(this: IEditorKit) {
  this.emitEvent(Evt_onEndPPTShow)
  this.collaborationManager.applyPendingChange()
}

export function emitSlideChangedInShow(this: IEditorKit, slideIndex) {
  this.emitEvent(Evt_onPPTShowSlideChanged, slideIndex)
}

export function startPPTShow(
  this: IEditorKit,
  divId: string,
  startSlideIndex: number,
  options?: ShowStartOption
) {
  this.emitEvent(Evt_onStartPPTShow)
  const showState: ShowState = {
    ['slideNum']: startSlideIndex,
    ['action']: ShowAction.goto(startSlideIndex),
  }
  this.emitEvent(Evt_onPPTShowAction, showState)
  this.editorUI.showManager.start(divId, startSlideIndex, options)
  if (!BrowserInfo.isTablet && !this.editorUI.isInTouchMode()) {
    this.editorUI.focusInput()
  } else {
    this.editorUI.checkMobileInput()
  }
}

export function getCurSlideIndexInPPTShow(this: IEditorKit) {
  return this.editorUI.showManager.getCurSlideIndex()
}

export function endPPTShow(this: IEditorKit) {
  this.editorUI.showManager.end()
}

export function enableEventsInShowMode(
  this: IEditorKit,
  enabled: boolean,
  keyCode?: number
) {
  if (this.editorUI.showManager) {
    this.editorUI.showManager.setEventEnabled(enabled, keyCode)
  }
}

export function playPPTShow(this: IEditorKit) {
  this.editorUI.showManager.play()
}

export function pausePPTShow(this: IEditorKit) {
  this.editorUI.showManager.pause()
}

export function goToNextSlideInPPTShow(
  this: IEditorKit,
  isStopTransition?: boolean
) {
  this.editorUI.showManager.next(isStopTransition)
}

export function goToPrevSlideInPPTShow(
  this: IEditorKit,
  isStopTransition?: boolean
) {
  this.editorUI.showManager.prev(isStopTransition)
}

export function goToSlideInPPTShow(this: IEditorKit, slideIndex: number) {
  this.editorUI.showManager.goToSlide(slideIndex)
}

export function ApplySlideTiming(this: IEditorKit, timing) {
  startEditAction(EditActionFlag.action_Presentation_ApplyTiming)
  const {
    presentation,
    thumbnailsManager: Thumbnails,
    renderingController,
  } = this.editorUI
  const { countOfSlides: count, curSlideIndex: curIndex } = renderingController
  if (curIndex < 0 || curIndex >= count) return

  const selectedSlideIndices = presentation.getSelectedSlideIndices()
  for (const index of selectedSlideIndices) {
    presentation.slides[index].applyTiming(timing)
  }

  syncInterfaceState(this)
  Thumbnails.updateSlideTransitionStatus()
}

export function SlideTimingApplyToAll(this: IEditorKit) {
  startEditAction(EditActionFlag.action_Presentation_ApplyTimingToAll)
  const countOfSlides = this.editorUI.renderingController.countOfSlides
  const curSlideIndex = this.editorUI.renderingController.curSlideIndex
  const slides = this.presentation.slides

  if (curSlideIndex < 0 || curSlideIndex >= countOfSlides) return
  const curSlide = slides[curSlideIndex]

  const currentSlideTiming = new SlideTransition()
  curSlide.timing.formatDuplicate(currentSlideTiming)

  for (let i = 0; i < countOfSlides; i++) {
    if (i === curSlideIndex) continue

    slides[i].applyTiming(currentSlideTiming)
  }

  const masters = this.presentation.slideMasters
  for (const master of masters) {
    master.applyTransition(
      currentSlideTiming.type === TransitionTypes.None
        ? undefined
        : currentSlideTiming
    )
    master.applyTransitionToAllLayouts()
  }

  this.editorUI.thumbnailsManager.updateSlideTransitionStatus()
}

export function PlaySlideTransition(this: IEditorKit) {
  const count = this.editorUI.renderingController.countOfSlides
  const cur = this.editorUI.renderingController.curSlideIndex
  if (cur < 0 || cur >= count) return
  const timing = this.presentation.slides[cur].timing

  const tr = this.editorUI.transitionManager
  if (timing.type != null) {
    tr.type = timing.type
  }

  if (timing.option != null) {
    tr.option = timing.option
  }

  if (timing.duration != null) {
    tr.duration = timing.duration
  }

  tr.start()
}

export function StopSlideTransitionPlay(this: IEditorKit) {
  if (this.editorUI.transitionManager.isPlaying()) {
    this.editorUI.transitionManager.end()
  }
}

export function setUseEndSlideInPPTShow(this: IEditorKit, val: boolean) {
  gShowSetting.setUseEndSlide(val)
}

export function setTextOfEndingSlideForPPTShow(this: IEditorKit, val: string) {
  gShowSetting.setTextOfEndingSlide(val)
}

export function setEndPPTShowOnlyByEsc(this: IEditorKit, val: boolean) {
  gShowSetting.setEndOnlyByEsc(val)
}
