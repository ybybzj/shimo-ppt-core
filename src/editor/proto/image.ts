import { FillKIND } from '../../core/common/const/attrs'
import { checkRemoteImageUrl } from '../../common/utils'
import {
  Slide_Default_Width,
  Default_Left_Margin,
  Default_Right_Margin,
  Slide_Default_Height,
  Default_Top_Margin,
  Default_Bottom_Margin,
} from '../../core/common/const/drawing'
import {
  Factor_mm_to_pix,
  Factor_pix_to_mm,
} from '../../core/common/const/unit'
import { Offset, Size } from '../../core/common/types'
import { ZoomValue } from '../../core/common/zoomValue'
import { FillBlipProp } from '../../core/properties/props'
import {
  UniNvPr,
  CNvPr,
  UniMedia,
  UniMediaOptions,
} from '../../core/SlideElement/attrs/shapePrs'
import { HyperlinkProps } from '../../core/SlideElement/attrs/HyperlinkProps'

import { ImageShape } from '../../core/SlideElement/Image'
import { getSelectedSlideElementsByTypes } from '../../core/utilities/presentation/getters'
import { isChartImage, isCustomImage } from '../../core/utilities/shape/asserts'
import {
  getHyperlinkFromShape,
  getMediaFromShape,
} from '../../core/utilities/shape/getters'
import { getShapeFromSlideByRefId } from '../../core/utilities/slide'
import { EditorUtil } from '../../globals/editor'
import { LoadableImage } from '../../images/utils'
import { getTrackRectPosition } from '../../ui/rendering/Drawer/EditLayerDrawer'
import { cropByOption } from '../../ui/features/crop/utils'
import { IEditorKit, UploadUrlResult } from '../type'
import { AddedShapeInfo } from './advanced'
import { SlideElement } from '../../core/SlideElement/type'
import {
  ImageShapeSubTypeMap,
  ImageShapeSubTypes,
} from '../../core/utilities/shape/creators'
import { AudioAction, SlideElementPos } from '../../exports/type'
import { Evt_onToggleAudio } from '../events/EventNames'
import { EditorSettings } from '../../core/common/EditorSettings'

export async function uploadImages(
  this: IEditorKit,
  images: Array<File | string>
): Promise<UploadUrlResult[] | undefined> {
  const processUrls = (
    input: Array<UploadUrlResult | File | undefined> | undefined
  ) => {
    const urlsData = input ?? []
    return urlsData.reduce((acc: UploadUrlResult[], result) => {
      if ((result as any)?.url) {
        acc.push({
          url: (result as UploadUrlResult).url,
          encryptUrl: (result as UploadUrlResult).encryptUrl,
        })
      }
      return acc
    }, [])
  }
  if (this.uploadImagesFunc) {
    return this.uploadImagesFunc(images)
      .then(processUrls)
      .catch(() => [])
  }
}

export function uploadImagesAndInsert(
  this: IEditorKit,
  images: Array<File | string>,
  options?: {
    slideRefId?: string
    callback?
    offset?: Offset
    size?: Size
  }
) {
  this.uploadImages(images).then((urls) => {
    if (urls && urls.length) {
      this.addImageShapesByUrl(urls, { ...options, type: 'image' })
    }
  })
}

export function uploadAttachmentAndInsertOle(
  this: IEditorKit,
  files: Array<File>
) {
  if (this.uploadAttachmentFunc) {
    this.uploadAttachmentFunc(files)
  }
}

export function replaceImageWithUrl(
  this: IEditorKit,
  url: UploadUrlResult,
  shapeId?: string
) {
  this.addImageUrlWithAction(url, null, ImageChangeActions.ImageChangeUrl, {
    shapeId,
  })
}

export type MediaInfoData = Omit<Required<UniMediaOptions>, 'encryptUrl'> & {
  encryptUrl?: string
}

export function addOleObject(
  this: IEditorKit,
  /** 缩略图链接 */
  urls: Array<UploadUrlResult>,
  options: {
    slideRefId?: string
    offset?: Offset
    size?: Size
    /** 附件链接 */
    fileInfo: UploadUrlResult & {
      fileType?: string
      name?: string
    }
  }
): AddedShapeInfo {
  return this.addImageShapesByUrl(urls, {
    type: 'ole',
    ...options,
    callback: function (oleObject) {
      if (oleObject) {
        const { fileType, url, encryptUrl, name } = options.fileInfo
        oleObject.setFileType(fileType)
        oleObject.setUri(url)
        oleObject.encryptUrl = encryptUrl
        oleObject.defaultSizeX = oleObject.spPr?.xfrm?.extX
        oleObject.defaultSizeY = oleObject.spPr?.xfrm?.extY
        if (name) {
          const nvPicPr = oleObject.nvPicPr ?? new UniNvPr(oleObject)
          const cNvPr = new CNvPr()
          if (name) {
            cNvPr.setName(name)
          }
          nvPicPr.setCNvPr(cNvPr)
          oleObject.setNvPicPr(nvPicPr)
        }
      }
    },
  })
}

export function addImages(
  this: IEditorKit,
  urls: Array<UploadUrlResult>,
  options: {
    slideRefId?: string
    offset?: Offset
    size?: Size
    link?: string
    subType?: string
    mediaInfo?: MediaInfoData
    name?: string
  } = {}
): AddedShapeInfo {
  return this.addImageShapesByUrl(urls, {
    type: 'image',
    ...options,
    callback: function (image) {
      if (image) {
        const { link, name, subType, mediaInfo } = options
        if (link || name) {
          const nvPicPr = image.nvPicPr ?? new UniNvPr(image)
          const cNvPr = new CNvPr()
          if (link) {
            const hyperlink = HyperlinkProps.new({
              id: link,
            })
            cNvPr.setHlinkClick(hyperlink)
          }
          if (name) {
            cNvPr.setName(name)
          }
          nvPicPr.setCNvPr(cNvPr)
          image.setNvPicPr(nvPicPr)
        }
        if (mediaInfo) {
          const nvPicPr = image.nvPicPr ?? new UniNvPr(image)
          const uniMedia = new UniMedia()
          uniMedia.type = mediaInfo.type
          uniMedia.media = mediaInfo.media
          uniMedia.encryptUrl = mediaInfo.encryptUrl
          nvPicPr.nvPr.setUniMedia(uniMedia)
          image.setNvPicPr(nvPicPr)
        }
        if (subType) {
          image.setSubType(subType)
        }
      }
    },
  })
}

/** 替换图片 */
export function updateImage(
  this: IEditorKit,
  id: string,
  options: {
    url?: UploadUrlResult
    link?: string
    subType?: string
  }
) {
  if (id) {
    const { url, link, subType } = options
    const image = EditorUtil.entityRegistry.getEntityById<ImageShape>(id)
    if (image == null) return

    if (subType) {
      image.setSubType(subType)
    }
    if (url) {
      if (isCustomImage(image)) {
        cropByOption(this.editorUI.editorHandler, 'fill', image, false)
      }
      this.replaceImageWithUrl(url, id)
    }
    if (link) {
      if (image) {
        const nvPicPr = image.nvPicPr ?? new UniNvPr(image)
        const cNvPr = new CNvPr()
        const hyperlink = HyperlinkProps.new({
          id: link,
        })
        cNvPr.setHlinkClick(hyperlink)
        nvPicPr.setCNvPr(cNvPr)
        image.setNvPicPr(nvPicPr)
      }
    }
  }
}

type SelectedImageInfo = {
  id: string
  url?: string
  encryptUrl?: string
  linkUrl?: string
  trackPosition: {
    x1: number
    x2: number
    x3: number
    x4: number
    y1: number
    y2: number
    y3: number
    y4: number
  }
  w: number
  h: number
}

export function getSelectedCharts(this: IEditorKit): SelectedImageInfo[] {
  const infos = this.getSelectedImagesInfo()
  return infos.filter(({ id }) => {
    const shape = EditorUtil.entityRegistry.getEntityById<SlideElement>(id)
    return isChartImage(shape)
  })
}

export function getSelectedImagesInfo(this: IEditorKit): SelectedImageInfo[] {
  const selectedElements = getSelectedSlideElementsByTypes(
    this.presentation,
    true
  )
  const images = selectedElements?.images ?? []
  const zoomValue = ZoomValue.value
  return images.map((image) => {
    const { pos, width, height } = getTrackRectPosition(
      image.getTransform(),
      0,
      0,
      image.extX,
      image.extY,
      this.editorUI.getSlideRenderingInfo()
    )
    const { L, T } = this.editorUI.mainContainer.PositionInPx
    const offsetL = L * Factor_mm_to_pix
    const offsetT = T * Factor_mm_to_pix
    const { url, encryptUrl } = image.getImageUrl()
    return {
      ['id']: image.id,
      ['url']: url,
      ['encryptUrl']: encryptUrl,
      ['linkUrl']: getHyperlinkFromShape(image)?.id,
      ['trackPosition']: {
        ['x1']: Math.ceil(pos.x1 + offsetL),
        ['x2']: Math.ceil(pos.x2 + offsetL),
        ['x3']: Math.ceil(pos.x3 + offsetL),
        ['x4']: Math.ceil(pos.x4 + offsetL),
        ['y1']: Math.ceil(pos.y1 + offsetT),
        ['y2']: Math.ceil(pos.y2 + offsetT),
        ['y3']: Math.ceil(pos.y3 + offsetT),
        ['y4']: Math.ceil(pos.y4 + offsetT),
      },
      ['w']: Math.ceil((width / zoomValue) * 100),
      ['h']: Math.ceil((height / zoomValue) * 100),
    }
  })
}

export function addImageShapesByUrl<T extends ImageShapeSubTypes>(
  this: IEditorKit,
  urls: Array<UploadUrlResult>,
  options?: {
    type: T
    slideRefId?: string
    callback?: (image: ImageShapeSubTypeMap[T]) => void
    offset?: Offset
    size?: Size
  }
): AddedShapeInfo {
  return new Promise((resolve) => {
    this.imageLoader.loadImages(urls).then((images) => {
      const shapes = this.presentation.addImages(images, options)
      resolve(
        shapes.map((sp) => {
          return {
            id: sp.getId(),
            refId: sp.getRefId(),
            slideRefId: sp.parent?.getRefId(),
          }
        })
      )
    })
  })
}

export async function onInsertPlaceholderImage(
  this: IEditorKit,
  placeholderShapeId: string
) {
  if (this.insertImageFunc) {
    const presentation = this.presentation
    const refId = presentation.slides[presentation.currentSlideIndex].getRefId()
    const url = await this.insertImageFunc()
    if (url) {
      const images = await this.imageLoader.loadImages([url])
      if (images[0]) {
        this.presentation.addImageByPlaceholder(
          images[0],
          refId,
          placeholderShapeId
        )
      }
    }
  }
}

export enum ImageChangeActions {
  ImageChangeUrl = 1,
  ShapeImageChangeUr = 2,
  SlideImageChangeUrl = 3,
}

export function addImageUrlWithAction(
  this: IEditorKit,
  urlInfo: UploadUrlResult,
  blipType: number | null,
  actionType: ImageChangeActions,
  options?: {
    shapeId?: string
  }
) {
  const image = this.imageLoader.getImage(urlInfo.url)
  if (!image) {
    this.imageLoader.loadImages([urlInfo], (image) => {
      this.onImageLoaded(image)
    })
  }
  if (image) {
    _addImageUrlWithAction(this, image, blipType, actionType, options)
  } else {
    this.imageLoadedCallback = (image) => {
      _addImageUrlWithAction(this, image, blipType, actionType, options)
      this.imageLoadedCallback = null
    }
  }
}

function _addImageUrlWithAction(
  editorKit: IEditorKit,
  image: LoadableImage,
  blipType: number | null,
  actionType: ImageChangeActions,
  options?: {
    shapeId?: string
  }
) {
  let w = Slide_Default_Width - (Default_Left_Margin + Default_Right_Margin)
  let h = Slide_Default_Height - (Default_Top_Margin + Default_Bottom_Margin)
  if (image.Image != null) {
    const _w = Math.max(image.Image.width * Factor_pix_to_mm, 1)
    const _h = Math.max(image.Image.height * Factor_pix_to_mm, 1)
    w = Math.max(5, Math.min(w, _w))
    h = Math.max(5, Math.min((w * _h) / _w))
  }

  let src = image.src
  let fill: FillBlipProp
  switch (actionType) {
    case ImageChangeActions.ShapeImageChangeUr:
      fill = new FillBlipProp()
      fill.setUrl(src)
      if (blipType != null) {
        fill.setType(blipType)
      }
      editorKit.applyPropsForSp({
        fill: {
          type: FillKIND.BLIP,
          fill,
        },
      })
      break
    case ImageChangeActions.SlideImageChangeUrl:
      fill = new FillBlipProp()
      fill.setUrl(src)
      if (blipType != null) {
        fill.setType(blipType)
      }
      editorKit.applySlideOptions({
        background: {
          type: FillKIND.BLIP,
          fill,
        },
      })
      break
    case ImageChangeActions.ImageChangeUrl:
      editorKit.applyPropsForImage(
        {
          imageUrl: src,
          encryptImageUrl: image.encryptSrc,
        },
        options
      )
      break
    default:
      const imgSrcUrl = checkRemoteImageUrl(src)
      if (imgSrcUrl) {
        src = imgSrcUrl
      }
      editorKit.presentation.addImageShape(w, h, {
        url: src,
        encryptUrl: image.encryptSrc,
      })
      break
  }
}

export function getMediaByRefId(
  this: IEditorKit,
  slideRefId: string,
  shapeRefId: string
) {
  const slide = this.getSlideByRefId(slideRefId)
  if (slide) {
    const shape = getShapeFromSlideByRefId(slide, shapeRefId)
    if (shape) {
      const media = getMediaFromShape(shape)
      if (media) {
        return {
          ['type']: media.type,
          ['media']: media.media,
          ['encryptUrl']: media.encryptUrl,
        }
      }
    }
  }
}

/** 演示模式需要传入位置, 编辑态由 ui 计算位置 */
export function emitAudioAction(
  this: IEditorKit,
  audioStatus: AudioAction,
  pos?: SlideElementPos
) {
  if (!EditorSettings.isSupportAudio) return
  this.emitEvent(
    Evt_onToggleAudio,
    pos
      ? {
          ...audioStatus,
          ['pos']: {
            ['absX']: pos.absX,
            ['absY']: pos.absY,
            ['width']: pos.width,
            ['height']: pos.height,
          },
        }
      : audioStatus
  )
}
