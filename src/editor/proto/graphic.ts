import { IEditorKit } from '../type'
import { Nullable } from '../../../liber/pervasive'

import { FillKIND, StrokeType } from '../../core/common/const/attrs'
import {
  checkRemoteImageUrl,
  isNullString,
  convertPtToMM,
} from '../../common/utils'
import { Evt_onRedrawDocument } from '../events/EventNames'
import { FillSolidProp, FillBlipProp } from '../../core/properties/props'
import { computedRGBColorFromStr } from '../../core/color/ComputedRGBAColor'
import { USER_TEXTURE_PRESETS } from '../../core/common/const/texture'

import {
  calculateRenderingState,
  invalidAllSlidesTextCalcStatus,
} from '../../core/calculation/calculate/presentation'
import { PresetLineDashStyleValues } from '../../io/dataType/spAttrs'
import { AlphaModFix } from '../../core/SlideElement/attrs/fill/Effects'
import {
  ShapeFillOptions,
  SlideElementOptions,
  StrokeOptions,
} from '../../core/properties/options'
import { invalidAllMastersThumbnails } from '../../ui/editorUI/utils/utils'

export function applyPropsForSp(
  this: IEditorKit,
  options: SlideElementOptions
) {
  if (!this.canEdit()) {
    return
  }

  let imageUrl = ''
  const fillProp = options.fill as Nullable<ShapeFillOptions>
  if (fillProp != null) {
    if (fillProp.fill != null && fillProp.type === FillKIND.BLIP) {
      imageUrl = (fillProp.fill as FillBlipProp).getUrl()

      const textureId = (fillProp.fill as FillBlipProp).getTextureId()
      if (
        null != textureId &&
        0 <= textureId &&
        textureId < USER_TEXTURE_PRESETS.length
      ) {
        imageUrl = USER_TEXTURE_PRESETS[textureId]
      }
    }
  }
  if (!isNullString(imageUrl)) {
    let _imageUrl: null | string = null
    if (!checkRemoteImageUrl(imageUrl)) {
      _imageUrl = imageUrl
    }
    const editorKit = this
    const applyFill = function () {
      const image = editorKit.imageLoader.getImage(imageUrl)
      if (!image) {
        editorKit.imageLoader.loadImages([{ url: imageUrl }], (image) => {
          editorKit.onImageLoaded(image)
        })
      }
      const imgSrcUrl = checkRemoteImageUrl(imageUrl)
      if (imgSrcUrl) {
        imageUrl = imgSrcUrl
      }
      ;(options.fill?.fill as FillBlipProp).setUrl(imageUrl)
      if (null != image) {
        editorKit.presentation.applyShapeProps(options)
        editorKit.editorUI.uiTextureDrawer.drawShapeImageTextureFill(imageUrl)
      } else {
        editorKit.imageLoadedCallback = function (_image) {
          editorKit.presentation.applyShapeProps(options)
          editorKit.editorUI.uiTextureDrawer.drawShapeImageTextureFill(imageUrl)

          editorKit.imageLoadedCallback = null
        }
      }
    }
    if (!_imageUrl) {
      applyFill()
    } else {
      this.uploadImages([_imageUrl]).then((urls) => {
        if (urls && urls.length) {
          imageUrl = urls[0].url // 图片填充暂不考虑加密
          applyFill()
        }
      })
    }
  } else {
    this.presentation.applyShapeProps(options)
  }
}

export function modifyShapeType(this: IEditorKit, shapetype) {
  this.presentation.modifyShapeType(shapetype)
}

export function setShapeborderProp(
  this: IEditorKit,
  borderWidth: number,
  dashStyle: PresetLineDashStyleValues,
  borderColor: string
) {
  const stroke: StrokeOptions =
    borderWidth < 0.00001
      ? {
          type: StrokeType.NONE,
        }
      : {
          type: StrokeType.COLOR,
          width: borderWidth,
          prstDash: dashStyle,
          color: computedRGBColorFromStr(borderColor),
        }
  this.presentation.applyShapeProps({
    stroke,
  })
}

export function applyPropsForImage(
  this: IEditorKit,
  imageOptions: SlideElementOptions,
  options?: { shapeId?: string }
) {
  if (!this.canEdit()) {
    return
  }
  let { imageUrl, encryptImageUrl } = imageOptions
  if (!isNullString(imageUrl)) {
    const editorKit = this
    const applyProps = function () {
      const image = editorKit.imageLoader.getImage(imageUrl)
      if (!image && imageUrl) {
        editorKit.imageLoader.loadImages(
          [{ url: imageUrl, encryptUrl: encryptImageUrl }],
          (image) => {
            editorKit.onImageLoaded(image)
          }
        )
      }
      const imgSrcUrl = checkRemoteImageUrl(imageUrl)
      if (imgSrcUrl) {
        imageUrl = imgSrcUrl
      }
      if (null != image) {
        editorKit.presentation.applyImageProps(imageOptions, options)
      } else {
        editorKit.imageLoadedCallback = function (_image) {
          editorKit.presentation.applyImageProps(imageOptions, options)
          editorKit.imageLoadedCallback = null
        }
      }
    }

    // 非远程地址(如 base64)无需下载可直接应用
    const remoteImgUrl = !checkRemoteImageUrl(imageUrl) ? imageUrl : null
    if (!remoteImgUrl) {
      applyProps()
    } else {
      this.uploadImages([remoteImgUrl]).then((urls) => {
        if (urls && urls.length) {
          imageUrl = urls[0].url
          encryptImageUrl = urls[0].encryptUrl
          applyProps()
        }
      })
    }
  } else {
    imageUrl = null
    this.presentation.applyImageProps(imageOptions, options)
  }
}

export function setBlipFillTransparency(this: IEditorKit, val: number) {
  const effect = new AlphaModFix()
  effect.amt = 100 - val
  this.applyPropsForImage({
    effects: [effect],
  })
}

// shape arrange
export function bringToFront(this: IEditorKit) {
  if (!this.canEdit() || this.presentation.isNotesFocused) {
    return
  }
  this.editorUI.editorHandler.onBringToFront()
}

export function bringForward(this: IEditorKit) {
  if (!this.canEdit() || this.presentation.isNotesFocused) {
    return
  }
  this.editorUI.editorHandler.onBringForward()
}

export function bringToBack(this: IEditorKit) {
  if (!this.canEdit() || this.presentation.isNotesFocused) {
    return
  }
  this.editorUI.editorHandler.onSendToBack()
}

export function bringBackward(this: IEditorKit) {
  if (!this.canEdit() || this.presentation.isNotesFocused) {
    return
  }
  this.editorUI.editorHandler.onBringBackward()
}

export function groupShapes(this: IEditorKit) {
  if (!this.canEdit()) {
    return
  }
  this.presentation.groupShapes()
}

export function unGroupShapes(this: IEditorKit) {
  if (!this.canEdit()) {
    return
  }
  this.presentation.unGroupShapes()
}

export function canGroup(this: IEditorKit) {
  return this.presentation.canGroup()
}

export function canUnGroup(this: IEditorKit) {
  return this.presentation.canUnGroup()
}

// shape rotate
export function rotate(this: IEditorKit, angle: number) {
  this.applyPropsForSp({
    rotToIncr: (angle * 3.14159265358979) / 180,
  })
}

// shape flip
export function flipHorizontal(this: IEditorKit) {
  if (!this.canEdit()) {
    return
  }
  this.applyPropsForSp({
    InvertFlipH: true,
  })
}

export function flipVertical(this: IEditorKit) {
  if (!this.canEdit()) {
    return
  }
  this.applyPropsForSp({
    InvertFlipV: true,
  })
}

// shape stroke
export function setShapeStroke(
  this: IEditorKit,
  size: number,
  color: string,
  type: PresetLineDashStyleValues
) {
  let value = parseFloat(size + '')
  value = isNaN(value) ? 0 : Math.max(0, Math.min(1584, value))

  const stroke: StrokeOptions =
    value < 0.00001
      ? {
          type: StrokeType.NONE,
        }
      : {
          type: StrokeType.COLOR,
          color: computedRGBColorFromStr(color),
          prstDash: type,
          width: convertPtToMM(value),
        }
  this.applyPropsForSp({
    stroke,
  })
}

export function setShapeStrokeSize(this: IEditorKit, size: number) {
  let value = parseFloat(size + '')
  value = isNaN(value) ? 0 : Math.max(0, Math.min(1584, value))

  const stroke: StrokeOptions =
    value < 0.00001
      ? {
          type: StrokeType.NONE,
        }
      : {
          type: StrokeType.COLOR,
          width: convertPtToMM(value),
        }
  this.applyPropsForSp({
    stroke,
  })
}

export function setShapeStrokeColor(
  this: IEditorKit,
  color: string,
  transparency?: number
) {
  const strokeColor = computedRGBColorFromStr(color)
  if (transparency != null) {
    const colorMod = {
      name: 'alpha',
      val: ((100 - transparency) * 100000) / 100,
    }

    strokeColor!.mods.push(colorMod)
  }

  const stroke: StrokeOptions = {
    type: StrokeType.COLOR,
    color: strokeColor,
  }
  this.applyPropsForSp({
    stroke,
  })
}

export function setShapeStrokeDash(this: IEditorKit, type: number) {
  const stroke: StrokeOptions = {
    type: StrokeType.COLOR,
    prstDash: type as PresetLineDashStyleValues,
  }
  this.applyPropsForSp({
    stroke,
  })
}

// shape fill
export function setShapeFillNoFill(this: IEditorKit) {
  this.applyPropsForSp({
    fill: {
      type: FillKIND.NOFILL,
      fill: null,
    },
  })
}

export function setShapeFillTransparency(
  this: IEditorKit,
  transparency: number
) {
  this.applyPropsForSp({
    fill: {
      type: FillKIND.NONE,
      fill: null,
      transparent: (((100 - transparency) * 255) / 100) >> 0,
    },
  })
}

export function setShapeFillSolid(
  this: IEditorKit,
  color: string,
  transparency?: number
) {
  const fillSolid = new FillSolidProp()
  fillSolid.setColor(computedRGBColorFromStr(color))

  if (transparency != null) {
    const colorMod = {
      name: 'alpha',
      val: ((100 - transparency) * 100000) / 100,
    }
    fillSolid.color.mods.push(colorMod)
  }

  const fill = {
    type: FillKIND.SOLID,
    fill: fillSolid,
  }

  this.applyPropsForSp({
    fill,
  })
}

export function refreshPPT(this: IEditorKit, isDirty: boolean = false) {
  const editorKit = this
  const currentSlideIndex = editorKit.editorUI.renderingController.curSlideIndex
  const currentSlide = editorKit.presentation.slides[currentSlideIndex]
  if (isDirty === true) {
    invalidAllSlidesTextCalcStatus(this.presentation)
    if (currentSlide) {
      calculateRenderingState(editorKit.presentation, [currentSlideIndex])
    }
  }
  editorKit.editorUI.thumbnailsManager.updateAll()
  editorKit.editorUI.slideRenderer.updateRenderInfo(currentSlideIndex)

  if (currentSlideIndex >= 0) {
    editorKit.editorUI.notesView.calculateState(
      currentSlideIndex,
      currentSlide.notesWidth,
      currentSlide.getNotesShapeHeight()
    )
  }
  editorKit.emitEvent(Evt_onRedrawDocument)
  editorKit.editorUI.prepareRender()

  if (editorKit.editorUI.showManager.isInShowMode) {
    editorKit.editorUI.showManager.clearCache()
    editorKit.editorUI.showManager.redrawCurrentSlide()
  }
}

export function invalidAllThumbnailsOfMasters(
  this: IEditorKit,
  resources?: {
    images?: string[]
    fonts?: string[]
  }
) {
  invalidAllMastersThumbnails(this.editorUI, resources)
}
