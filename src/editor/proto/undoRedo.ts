import { IEditorKit } from '../type'

export function canUndo(this: IEditorKit) {
  return this.collaborationManager.canUndo()
}

export function canRedo(this: IEditorKit) {
  return this.collaborationManager.canRedo()
}

export function undo(this: IEditorKit) {
  this.collaborationManager.undo()
}
export function redo(this: IEditorKit) {
  this.collaborationManager.redo()
}

export function resetUndoRedo(this: IEditorKit) {
  this.collaborationManager.clearRedoUndo()
}
