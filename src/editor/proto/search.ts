import { findTextInPPT } from '../../core/search/findText'
import { Evt_onSearchEnd } from '../events/EventNames'
import { IEditorKit } from '../type'

/**
 * 查找高亮, 选中上/下一条, 选中对应对象
 * @notes 应用协作时不直接调用该接口避免跳页
 * @params noJump 仅查找, 不执行选中对应 object (跳页) 等操作
 * */
export function findText(
  this: IEditorKit,
  text: string,
  isNext: boolean,
  isCaseSensitive = false,
  noJump = false
) {
  const presentation = this.presentation
  const { count, currentIndex } = findTextInPPT(
    presentation,
    text,
    isNext,
    isCaseSensitive,
    noJump
  )
  if (noJump) {
    return
  }

  this.emitSearchEnd(count, currentIndex)
}

export function clearSearchResult(this: IEditorKit) {
  this.presentation.clearSearchResult()
}

export function emitSearchEnd(this: IEditorKit, count, curIndex) {
  this.emitEvent(Evt_onSearchEnd, { ['count']: count, ['curIndex']: curIndex })
}
