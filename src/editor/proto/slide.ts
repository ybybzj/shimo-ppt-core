import { IEditorKit, UploadUrlResult } from '../type'
import { translator } from '../../globals/translator'

import {
  FillKIND,
  FillBlipType,
  SelectedElementType,
} from '../../core/common/const/attrs'
import { checkRemoteImageUrl } from '../../common/utils'
import { DEFAULT_THEME_NAME } from '../../core/SlideElement/attrs/creators'
import { LayoutId } from '../../exports/type'
import { computedRGBColorFromStr } from '../../core/color/ComputedRGBAColor'
import { EditActionFlag } from '../../core/EditActionFlag'
import { InstanceType } from '../../core/instanceTypes'
import { Op, Operation } from '../../io/operation'
import {
  getTextDocumentOfSp,
  getHyperlinkFromShape,
  getMediaFromShape,
  getShapeContentText,
  getSpImgInfo,
} from '../../core/utilities/shape/getters'
import { ImageChangeActions } from './image'
import { Slide } from '../../core/Slide/Slide'
import { Nullable, Optional } from '../../../liber/pervasive'
import {
  convertPixToInnerMMPos,
  convertPctToInnerMMPos,
  generateMasterThumbnails,
} from '../../ui/editorUI/utils/utils'
import { SlideElement } from '../../core/SlideElement/type'
import { isChartImage, isOleImage } from '../../core/utilities/shape/asserts'
import { isAudioImage, isVideoImage } from '../../core/utilities/shape/image'
import {
  checkHitBounds,
  hitInnerArea,
  hitPath,
  hitTextRect,
} from '../../ui/states/hittests'
import { getHyperlinkFromSp } from '../../ui/rendering/helpers'
import { Bg } from '../../core/SlideElement/attrs/bg'
import { BlipFill } from '../../core/SlideElement/attrs/fill/blipFill'
import { calculateForRendering } from '../../core/calculation/calculate/calculateForRendering'
import { startEditAction } from '../../core/calculation/informChanges/startAction'
import { updateClosestPosForParagraph } from '../../core/Paragraph/utils/position'
import { updateFillEffectsByProp } from '../../core/utilities/presentation/propsAppliers'
import { StatusOfLoadableImage } from '../../images/utils'
import { getSelectedElementsInfo } from '../interfaceState/syncInterfaceState'
import {
  FillBlipProp,
  FillHatch,
  FillSolidProp,
} from '../../core/properties/props'
import { SlideOptions } from '../../core/properties/options'
import { applyParagraphs } from '../../io/appliers/csld/txBody'
import { paraCollectionFromParagraphs } from '../../core/Paragraph/ParaCollection'

export function addSlide(this: IEditorKit, layoutId?: string) {
  this.presentation.addNextSlide(layoutId)
}

export function duplicateSlide(this: IEditorKit) {
  this.presentation.duplicateSlide()
}

export function selectAllSlides(this: IEditorKit, _layoutType?) {
  this.editorUI.thumbnailsManager.selectAll()
}

export function hideSlides(this: IEditorKit, isHide) {
  this.presentation.hideSlides(isHide)
}

export function getCurrentSlideIndex(this: IEditorKit): number {
  return this.presentation.currentSlideIndex
}

export function getCurrentSlide(this: IEditorKit): Slide | undefined {
  return this.presentation.slides[this.getCurrentSlideIndex()]
}

export function isSlideVisible(this: IEditorKit, index: number): boolean {
  if (index < 0) return false
  const slide = this.presentation.slides[index]
  return slide.isVisible()
}

export function changeLayout(this: IEditorKit, layoutId: string) {
  const { presentation, editorHandler: handler } = this.editorUI
  const thumbnailSelected = presentation.getSelectedSlideIndices()
  const selectedSlideIndices =
    thumbnailSelected.length > 0
      ? thumbnailSelected
      : [presentation.currentSlideIndex]
  presentation.slideMasters.forEach((master) =>
    master.layouts.forEach((layout, index) => {
      if (layout.id === layoutId) {
        handler.onChangeLayout(selectedSlideIndices, layout.master!, index)
      }
    })
  )
}

export function applySlideOptions(
  this: IEditorKit,
  options: SlideOptions,
  indicesOfSlides?: number[]
) {
  if (null == options) return
  const { presentation, editorHandler: handler } = this.editorUI

  const thumbnailSelected = presentation.getSelectedSlideIndices()
  const slideIndices: number[] = indicesOfSlides
    ? indicesOfSlides
    : thumbnailSelected.length > 0
    ? thumbnailSelected
    : [presentation.currentSlideIndex]
  const bgFillProp = options.background
  if (bgFillProp) {
    if (bgFillProp.type === FillKIND.NOFILL) {
      const bg = Bg.Pr(updateFillEffectsByProp(bgFillProp, null))

      handler.onChangeBackground(bg, slideIndices)
      return
    }

    const curSlide = presentation.slides[presentation.currentSlideIndex]
    const oldBgFill = curSlide?.backgroundFill
    const bg = Bg.Pr(updateFillEffectsByProp(bgFillProp, oldBgFill))

    let imageUrl = ''
    if (
      bgFillProp.type === FillKIND.BLIP &&
      bgFillProp.fill &&
      typeof (bgFillProp.fill as FillBlipProp).url === 'string' &&
      (bgFillProp.fill as FillBlipProp).url.length > 0
    ) {
      imageUrl = (bgFillProp.fill as FillBlipProp).url
    }
    if (imageUrl !== '') {
      let _imageUrl: any = null
      if (!checkRemoteImageUrl(imageUrl)) {
        _imageUrl = imageUrl
      }
      const editorKit = this
      const bgFill = bg.bgPr!.fill!
      const applyFill = function () {
        const _image = editorKit.imageLoader.getImage(imageUrl)
        if (!_image) {
          editorKit.imageLoader.loadImages([{ url: imageUrl }], (image) => {
            editorKit.onImageLoaded(image)
          })
        }
        const imgSrcUrl = checkRemoteImageUrl(imageUrl)
        if (imgSrcUrl) {
          imageUrl = imgSrcUrl
          ;(bgFill.fill as BlipFill).imageSrcId = imageUrl
        }

        if (null != _image) {
          if (
            bgFill != null &&
            bgFill.fill != null &&
            bgFill.fill.type === FillKIND.BLIP
          ) {
            editorKit.editorUI.uiTextureDrawer.drawSlideImageTextureFill(
              bgFill.fill.imageSrcId
            )
          }

          handler.onChangeBackground(bg, slideIndices)
        } else {
          editorKit.imageLoadedCallback = function (_image) {
            if (
              bgFill != null &&
              bgFill.fill != null &&
              bgFill.fill.type === FillKIND.BLIP
            ) {
              editorKit.editorUI.uiTextureDrawer.drawSlideImageTextureFill(
                bgFill.fill.imageSrcId
              )
            }

            handler.onChangeBackground(bg, slideIndices)
            editorKit.imageLoadedCallback = null
          }
        }
      }
      if (!_imageUrl) {
        applyFill()
      } else {
        this.uploadImages([_imageUrl]).then((urls) => {
          if (urls && urls.length) {
            imageUrl = urls[0].url
            applyFill()
          }
        })
      }
    } else {
      if (
        bg.bgPr!.fill != null &&
        bg.bgPr!.fill.fill != null &&
        bg.bgPr!.fill.fill.type === FillKIND.BLIP
      ) {
        this.editorUI.uiTextureDrawer.drawSlideImageTextureFill(
          bg.bgPr!.fill.fill.imageSrcId
        )
      }

      handler.onChangeBackground(bg, slideIndices)
    }
  }

  if (options.transition) {
    this.ApplySlideTiming(options.transition)
  }
}

export function setSlideFillNoFill(this: IEditorKit) {
  const fill = {
    type: FillKIND.NOFILL,
    fill: null,
  }
  this.applySlideOptions({
    background: fill,
  })
}

// color---eg: E7E6E6（灰色）  70AD47(绿色)  FFC000（黄色）4472C4（蓝色）
export function setSlideFillSolid(this: IEditorKit, color: string) {
  const solidFill = new FillSolidProp()
  solidFill.setColor(computedRGBColorFromStr(color))
  const fill = {
    type: FillKIND.SOLID,
    fill: solidFill,
  }
  this.applySlideOptions({
    background: fill,
  })
}

// 图案  global_hatch_offsets （0---53）
export function setSlideFillPattern(
  this: IEditorKit,
  patternFillType: number,
  fgColor?: string,
  bgColor?: string
) {
  const hatchFill = new FillHatch()
  hatchFill.setPatternType(patternFillType)
  hatchFill.setColorBg(
    computedRGBColorFromStr(bgColor == null ? 'FFFFFF' : bgColor)
  )
  hatchFill.setColorFg(
    computedRGBColorFromStr(fgColor == null ? '778899' : fgColor)
  )
  const fill = {
    type: FillKIND.PATT,
    fill: hatchFill,
  }
  this.applySlideOptions({
    background: fill,
  })
}

export function setSlideFillPatternForegroundColor(
  this: IEditorKit,
  color: string
) {
  const hatchFill = new FillHatch()
  hatchFill.setColorFg(computedRGBColorFromStr(color))
  const fill = {
    type: FillKIND.PATT,
    fill: hatchFill,
  }
  this.applySlideOptions({
    background: fill,
  })
}

export function setSlideFillPatternBackgroundColor(
  this: IEditorKit,
  color: string
) {
  const hatchFill = new FillHatch()
  hatchFill.setColorBg(computedRGBColorFromStr(color))
  const fill = {
    type: FillKIND.PATT,
    fill: hatchFill,
  }
  this.applySlideOptions({
    background: fill,
  })
}

export function setSlideFillBlipTexture(
  this: IEditorKit,
  textureId: number,
  blipType = 2
) {
  const blipFill = new FillBlipProp()
  blipFill.setType(blipType === 2 ? FillBlipType.TILE : FillBlipType.STRETCH)
  blipFill.setTextureId(textureId)
  const fill = {
    type: FillKIND.BLIP,
    fill: blipFill,
  }
  this.applySlideOptions({
    background: fill,
  })
}

export function setSlideFillBlipImage(
  this: IEditorKit,
  url: string,
  blipType = 1 // 1: stretch 2: tile
) {
  this.addImageUrlWithAction(
    { url },
    blipType,
    ImageChangeActions.SlideImageChangeUrl
  )
}

export function setSlideFillBlipType(this: IEditorKit, blipType: number) {
  const blipFill = new FillBlipProp()
  blipFill.setType(blipType)
  const fill = {
    type: FillKIND.BLIP,
    fill: blipFill,
  }
  this.applySlideOptions({
    background: fill,
  })
}

export function applySlideFillToAll(this: IEditorKit) {
  const selectedElements = getSelectedElementsInfo(this)
  const slideProps = selectedElements.find(
    (object) => object.type === SelectedElementType.Slide
  )
  if (slideProps) {
    const slidesArr = this.presentation.slides.map((_s, index) => index)
    this.applySlideOptions(
      {
        background: slideProps.value.background,
      },
      slidesArr
    )
  }
}

// layout
export function getSlideMasters(this: IEditorKit) {
  const { slideMasters } = this.presentation
  const currentSlideIndex = this.getCurrentSlideIndex()
  const currentSlide = this.presentation.slides[currentSlideIndex]

  return slideMasters
    .filter((slideMaster) => !!slideMaster.layouts.length)
    .map((slideMaster) => {
      let themeName = slideMaster.theme?.name || ''
      const isDefaultTheme = themeName === DEFAULT_THEME_NAME
      if (isDefaultTheme) {
        themeName = 'default_theme'
      }
      if (!slideMaster.imgBase64) {
        generateMasterThumbnails(this.editorUI, slideMaster)
      }

      return {
        masterId: slideMaster.id,
        theme: translator.getValue(themeName),
        image: slideMaster.imgBase64,
        isSelected: !!(
          currentSlide &&
          currentSlide.layout &&
          currentSlide.layout.master &&
          currentSlide.layout.master.id === slideMaster.id
        ),
        layouts: slideMaster.layouts.map((layout, index) => {
          return {
            index,
            layoutId: layout.id as LayoutId,
            isSelected: !!(
              currentSlide &&
              currentSlide.layout &&
              currentSlide.layout.id === layout.id
            ),
            name: layout.cSld
              ? isDefaultTheme
                ? translator.getValue(layout.cSld.name)
                : layout.cSld.name
              : '',
            image: layout.imgBase64,
          }
        }),
      }
    })
}

export function getSlideLayout(this: IEditorKit, index: number) {
  const { layout } = this.presentation.slides[index]
  return {
    masterId: layout?.master?.id,
    layoutId: layout?.id,
  }
}

export function getSlideId(this: IEditorKit, index: number) {
  return this.presentation.slides[index]?.getRefId()
}

export function getSlideByRefId(this: IEditorKit, slideRefId: string) {
  return slideRefId
    ? this.presentation.slides.find((slide) => slide.refId === slideRefId)
    : null
}

export function getSlideMasterByRefId(this: IEditorKit, masterRefId: string) {
  return masterRefId
    ? this.presentation.slideMasters.find(
        (master) => master.refId === masterRefId
      )
    : undefined
}

export function getSlideNotesContent(this: IEditorKit, slideIndex: number) {
  const document = this.presentation
  const { shapeOfNotes } = document.slides[slideIndex]
  return getShapeContentText(shapeOfNotes!)
}

export function setSlideNotesContent(
  this: IEditorKit,
  slideIndex: number,
  text: string
) {
  const document = this.presentation
  startEditAction(EditActionFlag.action_Slide_Remove)
  const { shapeOfNotes } = document.slides[slideIndex]
  if (shapeOfNotes) {
    let textDoc = getTextDocumentOfSp(shapeOfNotes)!
    textDoc.isSetToAll = true
    textDoc.remove(-1)
    textDoc.isSetToAll = false

    const textOps = operationsFromText(text)
    const paras = applyParagraphs(textOps)
    const paraCollection = paraCollectionFromParagraphs(paras)
    const paragraph = textDoc.children.at(0)
    if (paragraph && paraCollection) {
      textDoc = getTextDocumentOfSp(shapeOfNotes)!
      const adjPos = {
        paragraph: paragraph,
        pos: paragraph.currentElementPosition(false, false),
      }
      updateClosestPosForParagraph(paragraph, adjPos)

      textDoc.insertSelection(paraCollection, adjPos)
      calculateForRendering()
    }
  }
}

function operationsFromText(text: string): Operation[] {
  const ops: Operation[] = []
  const textArr = text.split('\n')
  textArr.forEach((text) => {
    ops.push(Op.Insert(text))
    ops.push(Op.Insert(1, { paraEnd: true }))
  })

  return ops
}

/** 同步修改 export 中 */
export enum ImageLikeTypes {
  image,
  video,
  audio,
  chart,
  attachment,
}

export type ImageLikeInfo = (
  | {
      type: typeof ImageLikeTypes.image
    }
  | {
      type: typeof ImageLikeTypes.video
      video: UploadUrlResult
    }
  | {
      type: typeof ImageLikeTypes.audio
      audio: UploadUrlResult
    }
  | {
      type: typeof ImageLikeTypes.chart
      chart: Nullable<string>
    }
  | {
      type: typeof ImageLikeTypes.attachment
      /** 若 url 不存在则说明上传失败或者为 link 型 ole */
      attachment: Optional<UploadUrlResult>
    }
) & {
  /** 用于直接获取实例的 id */
  id: string
  /** modoc 中的 id */
  refId: string
  imgInfo: UploadUrlResult
  name?: string
}
export type HyperlinkInfo = {
  url: string
}

/** 供外部 ui 查询一张 slide 内所有的图片信息以展示大图列表 */
export function getSlideImageLikes(
  this: IEditorKit,
  slideIndex: number
): Nullable<ImageLikeInfo[]> {
  const slide = this.presentation.slides[slideIndex]
  return getImageLikeInfoFromSpTree(slide?.cSld.spTree)
}

export function getSelectedImageLikes(this: IEditorKit): ImageLikeInfo[] {
  const sps = this.editorUI.editorHandler.getSelectedSlideElements()
  return getImageLikeInfoFromSpTree(sps, true)
}

function getImageLikeInfoFromSpTree(
  spTree: SlideElement[],
  selectedOnly = false
): ImageLikeInfo[] {
  const images: ImageLikeInfo[] = []
  if (!spTree || !spTree.length) {
    return images
  }
  for (let i = spTree.length - 1; i >= 0; i--) {
    const sp = spTree[i]
    switch (sp.instanceType) {
      case InstanceType.GroupShape:
        const sps = selectedOnly
          ? sp.selection && sp.selection.selectedElements.length > 0
            ? sp.selection.selectedElements
            : sp.spTree
          : sp.spTree
        images.push(...getImageLikeInfoFromSpTree(sps))
        break
      default:
        const firstImgInfo = getSpImgInfo(sp)[0]
        if (firstImgInfo) {
          images.push(getImageLikeInfoFromShape(sp, firstImgInfo))
        }
        break
    }
  }
  return images
}

function getImageLikeInfoFromShape(
  sp: SlideElement,
  url: UploadUrlResult
): ImageLikeInfo {
  let info: ImageLikeInfo
  const commonInfo = {
    ['refId']: sp.getRefId(),
    ['id']: sp.getId(),
  }
  const name =
    sp.instanceType === InstanceType.ImageShape ? sp.getName() : undefined
  if (name != null) {
    commonInfo['name'] = name
  }

  if (isVideoImage(sp)) {
    const mediaInfo = getMediaFromShape(sp)
    info = {
      ['type']: ImageLikeTypes.video,
      ['imgInfo']: url,
      ['video']: { url: mediaInfo!.media!, encryptUrl: mediaInfo?.encryptUrl },
      ...commonInfo,
    }
  } else if (isAudioImage(sp)) {
    const mediaInfo = getMediaFromShape(sp)
    info = {
      ['type']: ImageLikeTypes.audio,
      ['imgInfo']: url,
      ['audio']: { url: mediaInfo!.media!, encryptUrl: mediaInfo?.encryptUrl },
      ...commonInfo,
    }
  } else if (isChartImage(sp)) {
    info = {
      ['type']: ImageLikeTypes.chart,
      ['imgInfo']: url,
      ['chart']: getHyperlinkFromShape(sp)?.id,
      ...commonInfo,
    }
  } else if (isOleImage(sp)) {
    info = {
      ['type']: ImageLikeTypes.attachment,
      ['imgInfo']: url,
      ['attachment']: {
        ['url']: sp.uri,
        ['encryptUrl']: sp.encryptUrl,
      },
      ...commonInfo,
    }
  } else {
    info = {
      ['type']: ImageLikeTypes.image,
      ['imgInfo']: url,
      ...commonInfo,
    }
  }
  return info
}

function getHyperlinkInfoFromShape(
  info: ShapeHitInfo,
  x: number,
  y: number
): Nullable<HyperlinkInfo> {
  const { sp, isHitInPath, isHitInInnerArea, isHitInTextRect } = info
  let linkUrl
  if (isHitInTextRect) {
    linkUrl = getHyperlinkFromSp(sp, x, y)?.value
  } else if (isHitInPath || isHitInInnerArea) {
    linkUrl = getHyperlinkFromShape(sp)?.id
  }
  if (linkUrl) {
    return {
      ['url']: linkUrl,
    }
  }
}

export type ShapeHitInfo = {
  sp: SlideElement
  isHitInPath: boolean
  isHitInInnerArea: boolean
  isHitInTextRect: boolean
}

export function checkSlideShapeHitByPosition(
  this: IEditorKit,
  x: number,
  y: number
): Nullable<ShapeHitInfo> {
  const slide = this.getCurrentSlide()
  if (!slide) return

  const checkBySpTree = (spTree) => {
    for (let i = spTree.length - 1; i >= 0; i--) {
      const sp = spTree[i]
      if (checkHitBounds(sp, x, y) === false) {
        continue
      }
      switch (sp.instanceType) {
        case InstanceType.GroupShape:
          const info = checkBySpTree(sp.spTree)
          if (info) {
            return info
          }
          break
        default:
          const isHitInPath = hitPath(sp, x, y, false)
          let isHitInInnerArea: boolean = false
          let isHitInTextRect: boolean = false

          if (isHitInPath !== true) {
            isHitInTextRect = hitTextRect(sp, x, y)
          }
          if (isHitInTextRect !== true) {
            isHitInInnerArea = hitInnerArea(sp, x, y)
          }
          if (isHitInPath || isHitInInnerArea || isHitInTextRect) {
            return {
              ['sp']: sp,
              ['isHitInPath']: isHitInPath,
              ['isHitInInnerArea']: isHitInInnerArea,
              ['isHitInTextRect']: isHitInTextRect,
            }
          }
          break
      }
    }
  }

  return checkBySpTree(slide.cSld.spTree)
}

/** 使用处注意混淆 */
export type HittestInfo = {
  image: Nullable<ImageLikeInfo>
  hyperlink: Nullable<HyperlinkInfo>
}

/** 使用处注意使用 ['key'] 写法避免混淆(目前外部 ui 和 core 内部均会使用)  */
export function getHittestInfoByMousePosition(
  this: IEditorKit,
  x: number,
  y: number,
  needConvertCoords = true
): Nullable<HittestInfo> {
  const info = {} as HittestInfo
  const pos = needConvertCoords
    ? convertPixToInnerMMPos(this.editorUI, x, y)
    : { x, y }
  const spInfo = this.checkSlideShapeHitByPosition(pos.x, pos.y)
  if (spInfo) {
    const sp = spInfo['sp']
    const firstImgInfo = getSpImgInfo(sp)[0]
    if (firstImgInfo) {
      const image = this.imageLoader.getImage(firstImgInfo.url)
      if (image?.Status === StatusOfLoadableImage.Error) {
        this.imageLoader.reloadErrorImages()
      }
      info['image'] = getImageLikeInfoFromShape(sp, firstImgInfo)
    }
    const hyperlinkInfo = getHyperlinkInfoFromShape(spInfo, pos.x, pos.y)
    if (hyperlinkInfo) {
      info['hyperlink'] = hyperlinkInfo
    }
  }
  return info
}

/** 使用处注意使用 ['key'] 写法避免混淆(目前仅外部 ui 使用)  */
export function getHittestInfoByPagePercentagePosition(
  this: IEditorKit,
  x: number,
  y: number,
  slideIndex: number
): Nullable<HittestInfo> {
  const drawingController = this.editorUI.renderingController
  if (drawingController.curSlideIndex !== -1) {
    if (drawingController.curSlideIndex !== slideIndex) {
      this.goToSlide(slideIndex)
    }
    const pos = convertPctToInnerMMPos(this.editorUI, x, y)
    return this.getHittestInfoByMousePosition(pos.x, pos.y, false)
  }
}
