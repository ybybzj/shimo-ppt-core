import { IEditorKit } from '../type'
import { AlignKindValue } from '../../core/common/const/attrs'
import { EditActionFlag } from '../../core/EditActionFlag'
import {
  Evt_onVerticalAlign,
  Evt_onPrAlign,
  Evt_onListType,
  Evt_onTextColor,
  Evt_onTextHighLight,
} from '../events/EventNames'
import { HightLight_None } from '../../core/common/const/unit'
import { startEditAction } from '../../core/calculation/informChanges/startAction'
import { FillEffects } from '../../core/SlideElement/attrs/fill/fill'
import { BulletListType } from '../../core/SlideElement/attrs/bullet'
import { CComplexColor } from '../../core/color/complexColor'
import { TextPr } from '../../core/textAttributes/TextPr'
import { FontsProps, RFonts } from '../../core/textAttributes/RFonts'

/* 	Bulleted List Type = 0
      none         - SubType = -1
      black dot - SubType = 1
      circle         - SubType = 2
      square      - SubType = 3
      picture     - SubType = -1
      4 diamonds      - SubType = 4
      arrow  - SubType = 5
      check        - SubType = 6
      diamond        - SubType = 7
      dash        - SubType = 7

    Type numbered list = 1
      none - SubType = -1
      1.  - SubType = 1
      1)  - SubType = 2
      I.  - SubType = 3
      A.  - SubType = 4
      a)  - SubType = 5
      a.  - SubType = 6
      i.  - SubType = 7

    Layered Type List = 2
      none            - SubType = -1
      1)a)i)        - SubType = 1
      1.1.1         - SubType = 2
      marked - SubType = 3
    */
export function setNumberingType(
  this: IEditorKit,
  type: number,
  subtype: number
) {
  if (!this.canEdit()) {
    return
  }
  this.presentation.setParagraphNumbering({
    type: type,
    subType: subtype,
  })
}

export function setTextDirection(this: IEditorKit, isRTL: boolean) {
  if (!this.canEdit()) {
    return
  }
  startEditAction(EditActionFlag.action_Presentation_SetTextPrAlign)
  this.presentation.setParagraphTextDirection(isRTL)
}

export function setAlign(this: IEditorKit, value: AlignKindValue) {
  if (!this.canEdit()) {
    return
  }
  startEditAction(EditActionFlag.action_Presentation_SetTextPrAlign)
  this.presentation.setAlignForParagraph(value)
}

export function setVerticalAlign(this: IEditorKit, align) {
  if (!this.canEdit()) {
    return
  }
  this.presentation.setVerticalAlign(align)
}

export function increaseIndent(this: IEditorKit) {
  if (!this.canEdit()) {
    return
  }
  this.presentation.changeIndentLevel(true)
}

export function decreaseIndent(this: IEditorKit) {
  if (!this.canEdit()) {
    return
  }
  this.presentation.changeIndentLevel(false)
}

export function setPrLineSpacing(
  this: IEditorKit,
  type: number,
  value: number
) {
  if (!this.canEdit()) {
    return
  }
  this.presentation.setSpacingForParagraph({
    lineRule: type,
    line: value,
  })
}

/*callbacks*/
export function emitVerticalAlign(this: IEditorKit, baselineType) {
  this.emitEvent(Evt_onVerticalAlign, baselineType)
}

export function emitPrAlignCallBack(this: IEditorKit, value) {
  this.emitEvent(Evt_onPrAlign, value)
}

export function emitListType(this: IEditorKit, bulletListType: BulletListType) {
  this.emitEvent(Evt_onListType, bulletListType)
}

export function emitTextColor(this: IEditorKit, fillEffects: FillEffects) {
  if (fillEffects?.fill) {
    const rgbColor = fillEffects.getRGBAColor()
    this.emitEvent(Evt_onTextColor, {
      r: rgbColor.r,
      g: rgbColor.g,
      b: rgbColor.b,
    })
  }
}

export function emitTextHighLight(
  this: IEditorKit,
  highlight: typeof HightLight_None | CComplexColor
) {
  if (highlight === HightLight_None) {
    this.emitEvent(Evt_onTextHighLight, null)
  } else {
    const rgba = highlight.rgba
    if (rgba) {
      this.emitEvent(Evt_onTextHighLight, { r: rgba.r, g: rgba.g, b: rgba.b })
    }
  }
}

export function addText(
  this: IEditorKit,
  text: string,
  options?: {
    font?: string
  }
) {
  if (!this.canEdit()) {
    return
  }
  let textPr: TextPr | undefined
  if (options) {
    textPr = new TextPr()
    if (options.font) {
      const props: FontsProps = { name: options.font, index: -1 }
      textPr.fontFamily = props
      textPr.rFonts = RFonts.new({
        ascii: props,
        eastAsia: props,
        hAnsi: props,
        cs: props,
      })
    }
  }
  this.editorUI.editorHandler.addTextWithPr(text, { textPr })
}
