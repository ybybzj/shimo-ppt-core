import { SpecialPasteKind, SpecialPasteKindValue } from '../../copyPaste/const'
import { isFiniteNumber, isNumber } from '../../common/utils'
import { gClipboard } from '../../copyPaste/gClipboard'
import {
  checkClipboardModificationSupport,
  queryClipboardPermission,
  Rect,
  SpecialPasteUIOptions,
} from '../../copyPaste/common'
import { executePaste } from '../../copyPaste/paste/paste'
import { gSpecialPasteUtil } from '../../copyPaste/gSpecialPasteUtil'
import { Factor_mm_to_pix } from '../../core/common/const/unit'
import { ZoomValue } from '../../core/common/zoomValue'
import { startEditAction } from '../../core/calculation/informChanges/startAction'
import { EditActionFlag } from '../../core/EditActionFlag'
import { convertGlobalInnerMMToPixPos } from '../../ui/editorUI/utils/utils'
import {
  Evt_onShowSpecialPaste,
  Evt_onHideSpecialPaste,
} from '../events/EventNames'
import { IEditorKit } from '../type'

export function syncSpecialPasteUI(
  this: IEditorKit,
  uiOptions: SpecialPasteUIOptions
) {
  const { presentation, editorUI } = this
  const isNotesFocused = presentation.isNotesFocused

  const htmlElement = editorUI.editorView.element
  const position = uiOptions.position
  let screenPosition = uiOptions.screenPosition!
  let startSpPos:
    | undefined
    | {
        x: number
        y: number
      }

  if (position && position.needConvertPosition && position.h && position.w) {
    startSpPos = convertGlobalInnerMMToPixPos(
      editorUI,
      position.x - position.w,
      position.y - position.h
    )
  }

  if (
    uiOptions.pasteSlideIndex == null &&
    !isNotesFocused &&
    screenPosition.y > htmlElement.height
  ) {
    if (startSpPos && startSpPos.y < htmlElement.height) {
      screenPosition.y = htmlElement.height
    } else {
      screenPosition = new Rect(-1, -1, 0, 0)
    }
  }

  const pixThumbnailsOffL =
    editorUI.mainContainer.PositionInPx.L * Factor_mm_to_pix
  if (
    uiOptions.pasteSlideIndex == null &&
    !isNotesFocused &&
    screenPosition.x > htmlElement.width + pixThumbnailsOffL
  ) {
    if (startSpPos && startSpPos.x < htmlElement.width + pixThumbnailsOffL) {
      screenPosition.x = htmlElement.width + pixThumbnailsOffL
    } else {
      screenPosition = new Rect(-1, -1, 0, 0)
    }
  }

  if (screenPosition) {
    uiOptions.setScreenPosition(screenPosition)
  }

  let currentSpecialPasteKind = gSpecialPasteUtil.kind
  if (!isFiniteNumber(currentSpecialPasteKind)) {
    currentSpecialPasteKind = SpecialPasteKind.destinationTheme
    if (!uiOptions.formats!.includes(currentSpecialPasteKind)) {
      currentSpecialPasteKind = SpecialPasteKind.sourceFormatting
    }
  }
  const factor = (ZoomValue.value * Factor_mm_to_pix) / 100
  const w = (screenPosition.width ?? 0) * factor
  this.emitEvent(Evt_onShowSpecialPaste, {
    ['left']: screenPosition.x + editorUI.x,
    ['right']: editorUI.width - screenPosition.x + w,
    ['top']: screenPosition.y + editorUI.y,
    ['options']: uiOptions.formats,
    ['currentSpecialPasteType']: currentSpecialPasteKind,
  })
  this.editorUI.checkMobileInput() // 选择性粘贴 button 会抢占焦点
}

export function hideSpecialPasteUI(this: IEditorKit) {
  this.emitEvent(Evt_onHideSpecialPaste)
}

export function updateSpecialPasteUI(this: IEditorKit) {
  const presentation = this.presentation
  const { uiOptions } = gSpecialPasteUtil
  const { position, shapeId, pasteSlideIndex } = uiOptions
  const { w, h } = position!
  let _coord: {
      x: number
      y: number
    },
    screenPosition

  if (isNumber(pasteSlideIndex)) {
    const thumbnail = this.editorUI.thumbnailsManager
    const pos = thumbnail.getSlideThumbnailPosByIndex(pasteSlideIndex)
    if (pos) {
      screenPosition = new Rect(pos.x, pos.y, w, h)
    }
  } else {
    const isNotesFocused = presentation.isNotesFocused
    if (shapeId) {
      // when switching between shapes, hide the special insert icon
      const textTarget = presentation
        ? presentation.getCurrentTextTarget()
        : null
      if (textTarget && textTarget.id === shapeId) {
        if (position) {
          const { x, y, needConvertPosition } = position
          _coord = needConvertPosition
            ? convertGlobalInnerMMToPixPos(this.editorUI, x, y)
            : { x: x, y: y }
          screenPosition = new Rect(_coord.x, _coord.y, w, h)
        }
      } else {
        screenPosition = new Rect(-1, -1, w, h)
      }
    } else {
      if (true === isNotesFocused) {
        screenPosition = new Rect(-1, -1, w, h)
      } else if (
        position &&
        position.slideIndex === presentation.currentSlideIndex
      ) {
        const { x, y, needConvertPosition } = position
        _coord = needConvertPosition
          ? convertGlobalInnerMMToPixPos(this.editorUI, x, y)
          : { x: x, y: y }
        screenPosition = new Rect(_coord.x, _coord.y, w, h)
      } else {
        screenPosition = new Rect(-1, -1, w, h)
      }
    }
  }

  if (screenPosition) {
    uiOptions.setScreenPosition(screenPosition)
  }

  this.syncSpecialPasteUI(uiOptions)
}

export function specialPaste(this: IEditorKit, type: number, isRevert = true) {
  const presentation = this.presentation
  if (!presentation) return

  gSpecialPasteUtil.pasteStart()
  gSpecialPasteUtil.startSpecialPaste(isRevert)

  if (isRevert) {
    this.presentation.lockInterfaceEvents = true
    this.presentation.updateDocumentState()
    this.collaborationManager.undo(true)
    this.presentation.lockInterfaceEvents = false
  }
  startEditAction(EditActionFlag.action_Document_PasteByHotKey)

  executePaste(
    this,
    gClipboard,
    null,
    null,
    null,
    type as SpecialPasteKindValue
  )
}

export enum ContextmenuSpecialPasteStatusType {
  succeed = 0,
  unsupported = 1,
  denied = 2,
  empty = 3,
}

export type QuerySpecialPasteOptionsType =
  | {
      status: ContextmenuSpecialPasteStatusType.succeed
      specialPasteTypes: number[]
    }
  | {
      status: ContextmenuSpecialPasteStatusType.unsupported
    }
  | {
      status: ContextmenuSpecialPasteStatusType.denied
    }
  | {
      status: ContextmenuSpecialPasteStatusType.empty
    }

export async function querySpecialPasteOptions(
  this: IEditorKit
): Promise<QuerySpecialPasteOptionsType> {
  if (!checkClipboardModificationSupport()) {
    return Promise.resolve({
      ['status']: ContextmenuSpecialPasteStatusType.unsupported,
    })
  }
  const hasPermission = await queryClipboardPermission()
  if (hasPermission) {
    try {
      await gClipboard.updatePasteDataFromClipboard()
    } catch (e) {
      return Promise.resolve({
        ['status']: ContextmenuSpecialPasteStatusType.empty,
      })
    }
    const options = gSpecialPasteUtil.uiOptions.formats ?? []
    if (options.length) {
      return Promise.resolve({
        ['status']: ContextmenuSpecialPasteStatusType.succeed,
        ['specialPasteTypes']: options,
      })
    }
    return Promise.resolve({
      ['status']: ContextmenuSpecialPasteStatusType.empty,
    })
  } else {
    return Promise.resolve({
      ['status']: ContextmenuSpecialPasteStatusType.denied,
    })
  }
}
