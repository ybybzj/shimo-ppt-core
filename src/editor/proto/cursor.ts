import { IEditorKit } from '../type'
import {
  Evt_onContextMenu,
  Evt_onHoveredObjectChange,
  Evt_toggleContextMenu,
} from '../events/EventNames'
import { ContextMenuData } from '../../core/properties/props'
import { HoveredObjectInfo } from '../../core/properties/SelectedObject'

/** 缩略图区域双击由 core 捕获 */
export function emitContextMenu(this: IEditorKit, cxtData: ContextMenuData) {
  this.emitEvent(Evt_onContextMenu, cxtData)
}

export function emitToggleContextMenu(
  this: IEditorKit,
  cxtData: ContextMenuData
) {
  this.emitEvent(Evt_toggleContextMenu, cxtData)
}

export function emitOnHoveredChange(hoveredInfo?: HoveredObjectInfo) {
  this.emitEvent(Evt_onHoveredObjectChange, hoveredInfo)
}
