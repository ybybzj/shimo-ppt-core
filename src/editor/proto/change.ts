import { EditorUtil } from '../../globals/editor'

export function turnOnChanges() {
  EditorUtil.ChangesStack.enableRecording()
}

export function turnOffChanges() {
  EditorUtil.ChangesStack.stopRecording()
}
