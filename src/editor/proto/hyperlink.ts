import { IEditorKit } from '../type'
import { Evt_onHyperlinkClick } from '../events/EventNames'

export function canInsertHyperlink(this: IEditorKit) {
  if (!this.canEdit()) {
    return false
  }
  return this.editorUI.editorHandler.canInsertHyperlink()
}

export interface HyperlinkProps {
  text?: string
  value?: string
  toolTip?: string
}

export function insertHyperlink(this: IEditorKit, props: HyperlinkProps) {
  if (!this.canEdit()) {
    return
  }

  this.editorUI.editorHandler.insertHyperlink({
    text: props.text,
    value: props.value,
    toolTip: props.toolTip,
  })
}

export function changeHyperlink(this: IEditorKit, props: HyperlinkProps) {
  this.editorUI.editorHandler.updateHyperlink({
    text: props.text,
    value: props.value,
    toolTip: props.toolTip,
  })
}

export function removeHyperlink(this: IEditorKit) {
  this.editorUI.editorHandler.removeHyperlink()
}

export function emitHyperlinkClick(this: IEditorKit, url: string) {
  const indAction = url.indexOf('ppaction://hlink')
  const countOfSlides = this.editorUI.renderingController.countOfSlides
  if (0 === indAction) {
    if (url === 'ppaction://hlinkshowjump?jump=firstslide') {
      this.editorUI.goToSlide(0)
    } else if (url === 'ppaction://hlinkshowjump?jump=lastslide') {
      this.editorUI.goToSlide(countOfSlides - 1)
    } else if (url === 'ppaction://hlinkshowjump?jump=nextslide') {
      this.editorUI.toNextSlide()
    } else if (url === 'ppaction://hlinkshowjump?jump=previousslide') {
      this.editorUI.toPrevSlide()
    } else {
      const prefix = 'ppaction://hlinksldjumpslide'
      const idx = url.indexOf(prefix)
      if (0 === idx) {
        const slideIndex = parseInt(url.substring(prefix.length))
        if (slideIndex >= 0 && slideIndex < countOfSlides) {
          this.editorUI.goToSlide(slideIndex)
        }
      }
    }
    return
  }
  this.emitEvent(Evt_onHyperlinkClick, url)
}
