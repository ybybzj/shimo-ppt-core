import { BrowserInfo } from '../../common/browserInfo'
import { EditorSettings } from '../../core/common/EditorSettings'
import { EditorUtil } from '../../globals/editor'
// import { idGenerator } from '../../globals/IdGenerator'
import {
  generateAllMastersThumbnails,
  invalidAllMastersThumbnails,
} from '../../ui/editorUI/utils/utils'
import {
  Evt_onDocumentContentReady,
  Evt_onLoadPPTCompleted,
  Evt_onPresentationSize,
} from '../events/EventNames'
import { IEditorKit } from '../type'
import { syncSelectionState } from '../../ui/editorUI/utils/updates'
import { syncInterfaceState } from '../interfaceState/syncInterfaceState'
// import { calculateRenderingState } from '../../core/calculation/calculate/presentation'
import { calculateForRendering } from '../../core/calculation/calculate/calculateForRendering'
import { startPerfTiming } from '../../common/utils'
import { logger } from '../../lib/debug/log'
import { ModocDelta } from '../../types/globals/io'
import { resetAttrParseCache } from '../../re/io/ModocAttr'

export async function loadData(this: IEditorKit, inputData: ModocDelta) {
  logger.info('init delta ==>', inputData)

  this.isLoadEmptyPPT = false
  this.presentation.isLoadComplete = false
  await EditorUtil.FontKit.useHB()

  const loader = this.dataLoader
  loader.reset()

  const getLoadTiming = startPerfTiming()

  const appliedDataP = loader.load(inputData)

  const loadTiming = getLoadTiming()
  this.trackPerf('IO_LOAD', loadTiming)
  logger.log(`load modoc timing:`, loadTiming)

  if (EditorSettings.isMobile) {
    BrowserInfo.isSafariInMacOs = false
  }

  afterLoad(this)

  const appliedData = await appliedDataP
  resetAttrParseCache()
  this.resourcesLoadManager.startLoading().finally(() => {
    invalidAllMastersThumbnails(this.editorUI)
    EditorUtil.FontLoader.loadStorageFonts()
  })
  logger.info('loaded ops ==>', appliedData)
  this.collaborationManager.initData(appliedData)

  if (!this.disableAutoSave) {
    ;(this as unknown as IEditorKit).collaborationManager.start() // 启动定时收集 changes 并发送
  }
  generateAllMastersThumbnails(this.editorUI)
  onLoadComplete(this)
}

function afterLoad(editorKit: IEditorKit) {
  if (!editorKit.editorUI || !editorKit.presentation) {
    return
  }

  let needRerender = false
  const { presentation, editorUI } = editorKit
  if (editorKit.isLoadEmptyPPT) {
    editorKit.editorUI.textCursor.show()
    calculateForRendering()
  } else {
    const { width, height } = presentation
    editorUI.layoutDrawer.widthInMM = width
    editorUI.layoutDrawer.heightInMM = height
    editorUI.masterDrawer.widthInMM = width
    editorUI.masterDrawer.heightInMM = height

    editorKit.emitEvent(Evt_onPresentationSize, width, height)
    needRerender = true
  }

  editorKit.slidesPreviewer?.calculateState()
  editorKit.isEditorUIInitComplete = true

  syncInterfaceState(editorKit)
  syncSelectionState(editorKit.editorUI)

  editorUI.initUIControl()
  if (needRerender) {
    editorUI.goToSlide(0, true)
  }
  editorKit.emitEvent(Evt_onDocumentContentReady)
  return needRerender
}

function onLoadComplete(editorKit: IEditorKit) {
  const presentation = editorKit.presentation
  presentation.isLoadComplete = true
  editorKit.emitEvent(Evt_onLoadPPTCompleted)
  editorKit.setViewMode()
}
