import { IEditorKit } from '../type'
import { JSONFromPPTDoc } from '../../io/readers/PPTDoc'
import { PPTDocData } from '../../io/dataType/pptDoc'

export function toJSON(this: IEditorKit): PPTDocData {
  const pptdoc = this.presentation
  return JSONFromPPTDoc(pptdoc)
}
