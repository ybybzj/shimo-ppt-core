import { IEditorKit } from '../type'
import { ShdType } from '../../core/common/const/attrs'
import { EditActionFlag } from '../../core/EditActionFlag'

import { EditorUtil } from '../../globals/editor'
import { computedRGBColorFromStr } from '../../core/color/ComputedRGBAColor'
import { Table } from '../../core/Table/Table'
import { getSelectedSlideElementsByTypes } from '../../core/utilities/presentation/getters'
import { startEditAction } from '../../core/calculation/informChanges/startAction'
import {
  TableCellBorder,
  TableCellBorderOptions,
} from '../../core/Table/attrs/CellBorder'
import { FillEffects } from '../../core/SlideElement/attrs/fill/fill'
import {
  TableProp,
  BordersPropsOptions,
  TablePropOptions,
} from '../../core/properties/tableProp'
import { TCBorderValue } from '../../core/common/const/table'

export function addTable(this: IEditorKit, col, row) {
  if (!this.canEdit()) {
    return
  }
  this.presentation.addTable(col, row)
}

export function addRowAbove(this: IEditorKit, _count) {
  if (!this.canEdit()) {
    return
  }
  startEditAction(EditActionFlag.action_Presentation_AddRowAbove)
  this.editorUI.editorHandler.onAddTableRow(true)
}

export function addRowBelow(this: IEditorKit, _count) {
  if (!this.canEdit()) {
    return
  }
  startEditAction(EditActionFlag.action_Presentation_AddRowBelow)
  this.editorUI.editorHandler.onAddTableRow(false)
}

export function addColumnLeft(this: IEditorKit, _count) {
  if (!this.canEdit()) {
    return
  }
  startEditAction(EditActionFlag.action_Presentation_AddColLeft)
  this.editorUI.editorHandler.onAddTableColumn(true)
}

export function addColumnRight(this: IEditorKit, _count) {
  if (!this.canEdit()) {
    return
  }
  startEditAction(EditActionFlag.action_Presentation_AddColRight)
  this.editorUI.editorHandler.onAddTableColumn(false)
}

export function removeRow(this: IEditorKit) {
  if (!this.canEdit()) {
    return
  }
  startEditAction(EditActionFlag.action_Presentation_RemoveRow)
  this.editorUI.editorHandler.onRemoveTableRow()
}

export function removeColumn(this: IEditorKit) {
  if (!this.canEdit()) {
    return
  }
  startEditAction(EditActionFlag.action_Presentation_RemoveCol)
  this.editorUI.editorHandler.onRemoveTableColumn()
}

export function removeTable(this: IEditorKit) {
  if (!this.canEdit()) {
    return
  }
  startEditAction(EditActionFlag.action_Presentation_RemoveTable)
  this.editorUI.editorHandler.onRemoveTable()
}

export function distributeTableCells(this: IEditorKit, isHorizontal) {
  if (!this.canEdit()) {
    return
  }
  const presentation = this.presentation
  if (!presentation) return

  startEditAction(EditActionFlag.action_Document_DistributeTableCells)
  if (!this.editorUI.editorHandler.onDistributeTableCells(isHorizontal)) {
    EditorUtil.ChangesStack.removeLastAction()
    return false
  }

  return true
}

export function canMergeTableCells(this: IEditorKit) {
  return this.presentation.canMergeTableCells()
}

export function canSplitTableCells(this: IEditorKit) {
  return this.presentation.canSplitTableCells()
}

export function mergeCells(this: IEditorKit) {
  if (!this.canEdit()) {
    return
  }
  startEditAction(EditActionFlag.action_Presentation_MergeCells)
  this.editorUI.editorHandler.onMergeTableCells()
}

export function splitCell(this: IEditorKit, cols, rows) {
  if (!this.canEdit()) {
    return
  }
  startEditAction(EditActionFlag.action_Presentation_SplitCells)
  this.editorUI.editorHandler.onSplitTableCells(cols, rows)
}

export function applyTableProps(this: IEditorKit, obj: TableProp) {
  if (!this.canEdit()) {
    return
  }
  startEditAction(EditActionFlag.action_Presentation_ApplyTable)

  if (obj.cellsBackground && obj.cellsBackground.color) {
    obj.cellsBackground.fillEffects = FillEffects.SolidComputed(
      obj.cellsBackground.color
    )
  }
  this.presentation.setTableProps(obj)
}

export interface CellBordersOptions {
  size?: number
  color?: string
  transparent?: number
  visible?: boolean
}

export function setCellBorders(
  this: IEditorKit,
  border: string,
  options: CellBordersOptions
) {
  if (!this.canEdit()) {
    return
  }
  const cellBorders = updateBordersStyle(border, options)
  const props = new TableProp({
    cellBorders,
  })

  this.applyTableProps(props)
}

function updateBordersStyle(border: string, options: CellBordersOptions) {
  const cellBorders: BordersPropsOptions = {}
  if (border.indexOf('l') > -1) {
    cellBorders.left = updateBorderStyle(TableCellBorder.new(), options)
  }
  if (border.indexOf('t') > -1) {
    cellBorders.top = updateBorderStyle(TableCellBorder.new(), options)
  }
  if (border.indexOf('r') > -1) {
    cellBorders.right = updateBorderStyle(TableCellBorder.new(), options)
  }
  if (border.indexOf('b') > -1) {
    cellBorders.bottom = updateBorderStyle(TableCellBorder.new(), options)
  }
  if (border.indexOf('v') > -1) {
    cellBorders.insideV = updateBorderStyle(TableCellBorder.new(), options)
  }
  if (border.indexOf('h') > -1) {
    cellBorders.insideH = updateBorderStyle(TableCellBorder.new(), options)
  }

  return cellBorders
}

function updateBorderStyle(
  border: TableCellBorder,
  options: CellBordersOptions
) {
  const { size, color, transparent } = options
  let { visible } = options
  if (visible == null) {
    visible = true
  }
  const borderOpts: TableCellBorderOptions = {}

  if (visible) {
    borderOpts.value = TCBorderValue.Single
    if (size != null) {
      borderOpts.size = (parseFloat(String(size)) * 25.4) / 72.0
    }
    if (color != null) {
      const srgbColor = computedRGBColorFromStr(color)

      if (transparent != null) {
        const colorMod = {
          name: 'alpha',
          val: ((100 - transparent) * 100000) / 100,
        }
        srgbColor.mods.push(colorMod)
      }

      const fill = FillEffects.SolidComputed(srgbColor)
      borderOpts.color = {
        r: srgbColor.r,
        g: srgbColor.g,
        b: srgbColor.b,
      }
      border.fromOptions(borderOpts)
      border.fillEffects = fill
    }
  } else {
    border.value = 0
  }
  return border
}

export function setCellsBackground(
  this: IEditorKit,
  color: string,
  transparent?: number
) {
  if (!this.canEdit()) {
    return
  }

  const bgColor = computedRGBColorFromStr(color)
  if (transparent != null) {
    const colorMod = {
      name: 'alpha',
      val: ((100 - transparent) * 100000) / 100,
    }

    bgColor.mods.push(colorMod)
  }

  const opts: TablePropOptions = {
    cellsBackground: {
      value: ShdType.Clear,
      color: bgColor,
    },
  }

  this.applyTableProps(new TableProp(opts))
}

export function clearCellsBackground(this: IEditorKit) {
  const opts: TablePropOptions = {
    cellsBackground: {
      value: ShdType.Nil,
    },
  }

  this.applyTableProps(new TableProp(opts))
}

// 用来区分选中了整个表格还是单元格
export function isFocusOnTableCell(this: IEditorKit): boolean {
  const presentation = this.presentation
  const slide = presentation.slides[presentation.currentSlideIndex]
  if (slide) {
    const { tables } = getSelectedSlideElementsByTypes(presentation, true) || {}
    if (tables && tables.length === 1) {
      const table = tables[0].graphicObject as Table
      if (table.curCell!.isCurrentSelected() || table.hasTextSelection()) {
        return true
      }
    }
  }
  return false
}

export function setTableDirection(this: IEditorKit, isRTL = false) {
  if (!this.canEdit()) {
    return
  }
  const props = new TableProp({ rtl: isRTL })
  this.applyTableProps(props)
}
