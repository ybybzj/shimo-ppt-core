import { IEditorKit } from '../type'
import * as textApi from '../../core/utilities/presentation/textPr'
import { EditorUtil } from '../../globals/editor'

export function setFont(this: IEditorKit, name: string = ''): void {
  if (!this.canEdit()) {
    return
  }
  const fontFile = EditorUtil.FontKit.getFontFile(name)
  if (fontFile && fontFile.isSystem && !fontFile.isUsable) {
    EditorUtil.FontLoader.loadFont(name).then(() => {
      textApi.setFont(this.presentation, name)
    })
  } else {
    textApi.setFont(this.presentation, name)
  }
}

export function setFontSize(this: IEditorKit, size: number): void {
  if (!this.canEdit()) {
    return
  }
  textApi.setFontSize(this.presentation, size)
}

export function setBold(this: IEditorKit, value: boolean) {
  if (!this.canEdit()) {
    return
  }
  textApi.setBold(this.presentation, value)
}

export function setItalic(this: IEditorKit, value: boolean) {
  if (!this.canEdit()) {
    return
  }
  textApi.setItalic(this.presentation, value)
}

export function setUnderline(this: IEditorKit, value: boolean) {
  if (!this.canEdit()) {
    return
  }
  textApi.setUnderline(this.presentation, value)
}

export function setStrikeout(this: IEditorKit, value: boolean) {
  if (!this.canEdit()) {
    return
  }
  textApi.setStrikeout(this.presentation, value)
}

export function setLineSpacing(this: IEditorKit, type: number, value: number) {
  if (!this.canEdit()) {
    return
  }
  textApi.setLineSpacing(this.presentation, type, value)
}

export function setLineSpacingBeforeAfter(
  this: IEditorKit,
  type: number, //"type == 0" means "Before", "type == 1" means "After"
  value: number
) {
  if (!this.canEdit()) {
    return
  }
  textApi.setLineSpacingBeforeAfter(this.presentation, type, value)
}

export function setFontSizeIn(this: IEditorKit) {
  if (!this.canEdit()) {
    return
  }
  textApi.setFontSizeIn(this.presentation)
}

export function setFontSizeOut(this: IEditorKit) {
  if (!this.canEdit()) {
    return
  }
  textApi.setFontSizeOut(this.presentation)
}

export function setHighlight(
  this: IEditorKit,
  isHighlight: boolean,
  color?: string
) {
  if (!this.canEdit()) {
    return
  }
  textApi.setHighlight(this.presentation, isHighlight, color)
}

export function setTextColor(this: IEditorKit, color: string) {
  if (!this.canEdit()) {
    return
  }
  textApi.setTextColor(this.presentation, color)
}
