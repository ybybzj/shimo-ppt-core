import { descriptionFromIdentityFlag } from '../../core/FlagDescriptions'
import { TrackErrorType, TrackPerfType } from '../../exports/type'
import { EditorUtil } from '../../globals/editor'
import { EventNames } from '../events'
import { IEditorKit } from '../type'

export function trackError(
  this: IEditorKit,
  errorType: TrackErrorType,
  error: Error,
  extra: any
) {
  this.emitEvent(EventNames.Evt_onTrack, {
    ['trackType']: 'error',
    ['errorType']: errorType,
    ['error']: error,
    ['extra']: extra,
  })
}

export function trackPerf(
  this: IEditorKit,
  type: TrackPerfType,
  timing: number
) {
  if (checkTimingLimit(type, timing)) {
    const emitArgs = {
      ['trackType']: 'perf',
      ['type']: type,
      ['timing']: timing,
    }
    if (type === 'CALC_STATUS' || type === 'RENDER_SLIDE') {
      const actionChangeType =
        EditorUtil.ChangesStack.peekLastActionChangeType()
      if (actionChangeType) {
        const desc = `${descriptionFromIdentityFlag(
          actionChangeType.action
        )} - ${descriptionFromIdentityFlag(actionChangeType.change)}`
        emitArgs['description'] = desc
      }
    }
    this.emitEvent(EventNames.Evt_onTrack, emitArgs)
  }
}
function checkTimingLimit(type: TrackPerfType, timing: number): boolean {
  switch (type) {
    case 'IO_LOAD':
      return timing > 1000
    case 'IO_SEND':
      return timing > 200
    case 'IO_APPLY':
      return timing > 200
    case 'CALC_STATUS':
      return timing > 100
    case 'RENDER_SLIDE':
      return timing > 100
  }
  return false
}
