import { IEditorKit } from '../type'
import {
  onCanUndoRedoConverter,
  onColorConverter,
  onContextMenuConverter,
  onFocusObjectConverter,
  onLineSpacingConverter,
  onListTypeConverter,
  onMediaConverter,
  onMouseMoveConverter,
} from './converters'
import { ActionHookName, addArgConverter, ArgsConverter } from './registry'

function actionEvtName<P = any>(
  name: string,
  converter?: ArgsConverter<P>
): ActionHookName<IEditorKit> {
  const evtName = name as ActionHookName<IEditorKit>
  if (converter) {
    addArgConverter(evtName, converter)
  }
  return evtName
}

export const Evt_onMediaPlay = actionEvtName(
  'Evt_onMediaPlay',
  onMediaConverter
)
export const Evt_onToggleAudio = actionEvtName('Evt_onToggleAudio')

export const Evt_onAnimationStart = actionEvtName('Evt_onAnimationStart')
export const Evt_onAnimationEnd = actionEvtName('Evt_onAnimationEnd')
export const Evt_onContentChange = actionEvtName('onContentChange')

export const Evt_canDecreaseIndent = actionEvtName('Evt_canDecreaseIndent')
export const Evt_canIncreaseIndent = actionEvtName('Evt_canIncreaseIndent')

export const Evt_onBold = actionEvtName('Evt_onBold')
export const Evt_onCanAddHyperlink = actionEvtName('Evt_onCanAddHyperlink')
export const Evt_onCanGroup = actionEvtName('Evt_onCanGroup')
export const Evt_onCanRedo = actionEvtName('Evt_onCanRedo')
export const Evt_onCanUndo = actionEvtName('Evt_onCanUndo')
export const Evt_onCanUndoRedo = actionEvtName(
  'Evt_onCanUndoRedo',
  onCanUndoRedoConverter
)

export const Evt_onCanUnGroup = actionEvtName('Evt_onCanUnGroup')

export const Evt_onCommentChange = actionEvtName('Evt_onCommentChange')

export const Evt_onSlideBackgroundChange = actionEvtName(
  'Evt_onSlideBackgroundChange'
)

export const Evt_onContextMenu = actionEvtName(
  'Evt_onContextMenu',
  onContextMenuConverter
)
export const Evt_toggleContextMenu = actionEvtName(
  'Evt_toggleContextMenu',
  onContextMenuConverter
)
export const Evt_hideContextMenu = actionEvtName('Evt_hideContextMenu')
export const Evt_onCountPages = actionEvtName('Evt_onCountPages')
export const Evt_onCurrentPage = actionEvtName('Evt_onCurrentPage')

export const Evt_onPPTShowSlideChanged = actionEvtName(
  'Evt_onPPTShowSlideChanged'
)
export const Evt_onPPTShowAction = actionEvtName('Evt_onPPTShowAction')
export const Evt_onPPTShowPlayMode = actionEvtName('Evt_onPPTShowPlayMode')

export const Evt_onPPTShowForceAutoPlay = actionEvtName(
  'Evt_onPPTShowForceAutoPlay'
)
export const Evt_onTrack = actionEvtName('Evt_onTrack')

export const Evt_onRedrawDocument = actionEvtName('Evt_onRedrawDocument')
export const Evt_onDocumentContentReady = actionEvtName(
  'Evt_onDocumentContentReady'
)
export const Evt_onLoadPPTCompleted = actionEvtName('Evt_onLoadPPTCompleted')
export const Evt_onEndAddComment = actionEvtName('Evt_onEndAddComment')
export const Evt_onEndAddShape = actionEvtName('Evt_onEndAddShape')
export const Evt_onEndPPTShow = actionEvtName('Evt_onEndPPTShow')
export const Evt_onError = actionEvtName('Evt_onError')
export const Evt_onFocusObject = actionEvtName(
  'Evt_onFocusObject',
  onFocusObjectConverter
)
export const Evt_onReadOnlyModeFocusEmpty = actionEvtName(
  'Evt_onReadOnlyModeFocusEmpty'
)
export const Evt_onFontFamily = actionEvtName('Evt_onFontFamily')
export const Evt_onFontSize = actionEvtName('Evt_onFontSize')

export const Evt_onHideSpecialPaste = actionEvtName('Evt_onHideSpecialPaste')
export const Evt_onHoveredCommentChange = actionEvtName(
  'Evt_onHoveredCommentChange'
)
export const Evt_onHoveredObjectChange = actionEvtName(
  'Evt_onHoveredObjectChange'
)
export const Evt_onHyperlinkClick = actionEvtName('Evt_onHyperlinkClick')
export const Evt_onItalic = actionEvtName('Evt_onItalic')
export const Evt_onLineSpacing = actionEvtName(
  'Evt_onLineSpacing',
  onLineSpacingConverter
)
export const Evt_onListType = actionEvtName(
  'Evt_onListType',
  onListTypeConverter
)

export const Evt_onMouseMove = actionEvtName(
  'Evt_onMouseMove',
  onMouseMoveConverter
)
export const Evt_onNoteAreaHeightChange = actionEvtName(
  'Evt_onNoteAreaHeightChange'
)
export const Evt_onPaintFormatChanged = actionEvtName(
  'Evt_onPaintFormatChanged'
)

export const Evt_onPrAlign = actionEvtName('Evt_onPrAlign')
export const Evt_onPresentationSize = actionEvtName('Evt_onPresentationSize')

export const Evt_onSearchEnd = actionEvtName('Evt_onSearchEnd')
export const Evt_onSelectedCommentChange = actionEvtName(
  'Evt_onSelectedCommentChange'
)
export const Evt_onShowSpecialPaste = actionEvtName('Evt_onShowSpecialPaste')

export const Evt_onStartAddComment = actionEvtName('Evt_onStartAddComment')
export const Evt_onStartPPTShow = actionEvtName('Evt_onStartPPTShow')
export const Evt_onStrikeout = actionEvtName('Evt_onStrikeout')

export const Evt_onTextColor = actionEvtName(
  'Evt_onTextColor',
  onColorConverter
)
export const Evt_onTextHighLight = actionEvtName(
  'Evt_onTextHighLight',
  onColorConverter
)
export const Evt_onThumbnailsShow = actionEvtName('Evt_onThumbnailsShow')
export const Evt_onNotesShow = actionEvtName('Evt_onNoteShow')
export const Evt_onThumbnailSlideDoubleClick = actionEvtName(
  'Evt_onThumbnailSlideDoubleClick'
)
export const Evt_onToggleCommentShow = actionEvtName('Evt_onToggleCommentShow')
export const Evt_onUnderline = actionEvtName('Evt_onUnderline')
export const Evt_onUpdateCommentPosition = actionEvtName(
  'Evt_onUpdateCommentPosition'
)
export const Evt_onUpdateLayout = actionEvtName('Evt_onUpdateLayout')
export const Evt_onUpdateTheme = actionEvtName('Evt_onUpdateTheme')
export const Evt_onUpdateScrolls = actionEvtName('Evt_onUpdateScrolls')
export const Evt_onUpdateCollaborationSelection = actionEvtName(
  'Evt_onUpdateCollaborationSelection'
)

export const Evt_onVert = actionEvtName('Evt_onVert')
export const Evt_onVerticalAlign = actionEvtName('Evt_onVerticalAlign')
export const Evt_onVerticalTextAlign = actionEvtName('Evt_onVerticalTextAlign')
export const Evt_onZoomChange = actionEvtName('Evt_onZoomChange')

export const Evt_onScriptError = actionEvtName('Evt_onScriptError')

// collab
export const Evt_Collab_readyToHandleChanges = actionEvtName(
  'Collab_readyToHandleChanges'
)

// slide transition
export const Evt_onSlideTransitionStart = actionEvtName(
  'Evt_onSlideTransitionStart'
)
export const Evt_onSlideTransitionEnd = actionEvtName(
  'Evt_onSlideTransitionEnd'
)
export const Evt_toggleCollaboratorStatus = actionEvtName(
  'Evt_toggleCollaboratorStatus'
)

/** {width: number, height: number} */
export const Evt_onStartCropElement = actionEvtName('Evt_onStartCropElement')
export const Evt_onCropSizeChange = actionEvtName('Evt_onCropSizeChange')
export const Evt_onEndCropElement = actionEvtName('Evt_onEndCropElement')

/** imageLikeInfo */
export const Evt_viewSourceImage = actionEvtName('Evt_viewSourceImage')
export const Evt_viewAttachment = actionEvtName('Evt_viewAttachment')
export const Evt_copyFailedWithoutPermission = actionEvtName(
  'Evt_copyFailedWithoutPermission'
)
