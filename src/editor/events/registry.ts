import { Nullable, Dict } from '../../../liber/pervasive'
import {
  HookManager,
  Hook,
  ActionHookArg,
  ReturnableHookArg,
} from '../../lib/hook'

export type ArgsConverter<R> = (...args: any[]) => R

const apiHooksManager = new HookManager()
const apiHooksArgConverters: Dict<ArgsConverter<any>> = {}

export interface PayLoad<Context> {
  context: Context
  args: any[]
}

export type ActionHookName<Context> = Hook<
  PayLoad<Context>,
  void,
  ActionHookArg<PayLoad<Context>>
>

export type ReturnableHookName<Context, R = any> = Hook<
  PayLoad<Context>,
  R,
  ReturnableHookArg<PayLoad<Context>, R>
>

export type HookName<Context, R = any> =
  | ActionHookName<Context>
  | ReturnableHookName<Context, R>

export type CallbackHandler<Context> = (this: Context, ...args: any[]) => any

export function addArgConverter<Context, R = any, P = any>(
  name: HookName<Context, R>,
  converter: ArgsConverter<P>
) {
  apiHooksArgConverters[name] = converter
}

export function registerCallback<Context, R = any>(
  name: HookName<Context, R>,
  callback: CallbackHandler<Context>
) {
  const argsConverter = apiHooksArgConverters[name]
  const hookHandler = ({ payload: { context, args } }) => {
    if (argsConverter != null) {
      args = [argsConverter(...args)]
    }
    return callback.apply(context, args)
  }

  apiHooksManager.register(name, hookHandler)

  return function unregisterCallback() {
    apiHooksManager.unregister(name, hookHandler)
  }
}

export function unregister(name: HookName<any, any>): void {
  apiHooksManager.unregister(name)
}

export function emitEvent<Context>(
  ctx: Context,
  name: ActionHookName<Context>,
  ...args: any[]
) {
  if (!apiHooksManager.isListening(name)) {
    return false
  }
  apiHooksManager.invoke(name, {
    context: ctx,
    args,
  })

  return true
}

export function getFromEvent<Context, R = any>(
  ctx: Context,
  name: ReturnableHookName<Context, R>,
  ...args: any[]
): Nullable<R> {
  return apiHooksManager.get(name, {
    context: ctx,
    args,
  })
}

export function hasCallback(name: string) {
  return apiHooksManager.isListening(name)
}
