import { ContextMenuData, ContextMenuTypes } from '../../core/properties/props'
import { EditorKit } from '../global'
import { UniMedia } from '../../core/SlideElement/attrs/shapePrs'
import {
  MouseMoveData,
  MouseMoveDataTypes,
} from '../../core/properties/MouseMoveData'
import { Nullable } from '../../../liber/pervasive'
import { ParaSpacingOptions } from '../../core/textAttributes/ParaSpacing'
import { BulletListType } from '../../core/SlideElement/attrs/bullet'

export function onLineSpacingConverter(lineSpacing: ParaSpacingOptions) {
  return lineSpacing
    ? {
        ['Line']: lineSpacing.line,
        ['LineRule']: lineSpacing.lineRule,
        ['Before']: lineSpacing.before,
        ['BeforePct']: lineSpacing.beforePercent,
        ['BeforeAutoSpacing']: lineSpacing.beforeAuto,
        ['After']: lineSpacing.after,
        ['AfterPct']: lineSpacing.afterPercent,
        ['AfterAutoSpacing']: lineSpacing.afterAuto,
      }
    : lineSpacing
}

export function onListTypeConverter(listType: BulletListType) {
  return listType
    ? {
        ['Type']: listType.type,
        ['SubType']: listType.subType,
      }
    : listType
}

export function onColorConverter(
  color: Nullable<{ r: number; g: number; b: number; a?: number }>
) {
  return color
    ? {
        ['r']: color.r,
        ['g']: color.g,
        ['b']: color.b,
        ['a']: color.a ?? 255,
      }
    : null
}

export function onFocusObjectConverter(param: {
  noParagraph: boolean
  noText: boolean
  noObject: boolean
}) {
  return {
    ['noParagraph']: param.noParagraph,
    ['noText']: param.noText,
    ['noObject']: param.noObject,
  }
}

export function onContextMenuConverter(data: ContextMenuData) {
  if (!data) return data
  const { absX, absY, type } = data
  let props
  const x = absX + EditorKit.editorUI.x
  const y = absY + EditorKit.editorUI.y
  if (type === ContextMenuTypes.Thumbnails) {
    props = {
      ['isThumbnails']: true,
      ['isSlideSelect']: data.isSlideSelected,
      ['x']: x,
      ['y']: y,
    }
  } else {
    props = {
      ['isThumbnails']: false,
      ['x']: x,
      ['y']: y,
    }
  }
  return props
}

export interface CanUndoRedoInfo {
  canUndo: boolean
  canRedo: boolean
}

export function onCanUndoRedoConverter(info: CanUndoRedoInfo): CanUndoRedoInfo {
  return {
    ['canUndo']: info.canUndo,
    ['canRedo']: info.canRedo,
  }
}

export function onMouseMoveConverter(data: MouseMoveData) {
  if (data) {
    const ret = {
      ['type']: data.type,
      ['x']: data.absX + EditorKit.editorUI.x, // convert to global position
      ['y']: data.absY + EditorKit.editorUI.y,
    }
    if (data.type === MouseMoveDataTypes.Hyperlink) {
      ret['hyperlink'] = {
        ['text']: data.hyperlink?.text,
        ['value']: data.hyperlink?.value,
        ['toolTip']: data.hyperlink?.toolTip,
      }
    }
    return ret
  }
}

export function onMediaConverter(media: UniMedia) {
  if (UniMedia) {
    return {
      ['type']: media.type,
      ['media']: media.media,
    }
  }
}
