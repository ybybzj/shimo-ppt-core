export class SyncSemaphoreLock {
  private lockCount = 0
  lock() {
    this.lockCount += 1
  }
  unlock() {
    this.lockCount -= 1
    if (this.lockCount < 0) this.lockCount = 0
  }

  isLocked() {
    return this.lockCount !== 0
  }

  reset() {
    this.lockCount = 0
  }
}
