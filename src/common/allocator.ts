import { BrowserInfo } from './browserInfo'

type RecycleNode<T> = Recyclable<T> | null
export abstract class Recyclable<T> {
  nextRecycled?: RecycleNode<T>
}

export type PoolAllocatorCreator<
  T extends Recyclable<T>,
  Args extends any[]
> = (...args: Args) => T
export type PoolAllocatorResetor<
  T extends Recyclable<T>,
  Args extends any[]
> = (item: T, ...args: Args) => void

export type PoolAllocatorConstructor<
  T extends { reset(this: T, ...args: Args): void } & Recyclable<T>,
  Args extends any[]
> = {
  new (...args: Args): T
}

const MAX_CACHE_COUNT =
  BrowserInfo.isMobile || BrowserInfo.isTablet ? 5000 : 20000

export class PoolAllocator<T extends Recyclable<T>, Args extends any[]> {
  private firstFreeNode: RecycleNode<T> = null
  private allocateCount: number = 0
  private creator: PoolAllocatorCreator<T, Args>
  private resetor: PoolAllocatorResetor<T, Args>
  private static allocators: Array<PoolAllocator<Recyclable<any>, any[]>> = []
  static getAllocator<T extends Recyclable<T>, Args extends any[]>(
    creator: PoolAllocatorCreator<T, Args>,
    resetor: PoolAllocatorResetor<T, Args>
  ): PoolAllocator<T, Args>
  static getAllocator<T extends Recyclable<T>, Args extends any[]>(
    ctor: PoolAllocatorConstructor<
      T & { reset(this: T, ...args: Args): void },
      Args
    >
  ): PoolAllocator<T, Args>
  static getAllocator<T extends Recyclable<T>, Args extends any[]>(
    ...args:
      | [
          PoolAllocatorConstructor<
            T & { reset(this: T, ...args: Args): void },
            Args
          >
        ]
      | [
          creator: PoolAllocatorCreator<T, Args>,
          resetor: PoolAllocatorResetor<T, Args>
        ]
  ): PoolAllocator<T, Args> {
    const allocator =
      args.length === 1
        ? new PoolAllocator(args[0])
        : new PoolAllocator(args[0], args[1])
    PoolAllocator.allocators.push(allocator as any)
    return allocator
  }
  static disposeAll() {
    for (let i = 0, l = PoolAllocator.allocators.length; i < l; i++) {
      const allocator = PoolAllocator.allocators[i]
      allocator.dispose()
    }

    PoolAllocator.allocators.length = 0
  }
  constructor(
    creator: PoolAllocatorCreator<T, Args>,
    resetor: PoolAllocatorResetor<T, Args>
  )
  constructor(
    ctor: PoolAllocatorConstructor<
      T & { reset(this: T, ...args: Args): void },
      Args
    >
  )
  constructor(...args: any[]) {
    if (args.length === 1) {
      const Ctor = args[0] as PoolAllocatorConstructor<
        T & { reset(this: T, ...args: Args): void },
        Args
      >
      this.creator = (...args: Args) => new Ctor(...args)
      this.resetor = (item: T, ...args: Args) =>
        (item as T & { reset(this: T, ...args: Args): void }).reset(...args)
    } else {
      this.creator = args[0]
      this.resetor = args[1]
    }
  }

  allocate(...args: Args) {
    if (this.firstFreeNode !== null) {
      const item = this.firstFreeNode

      const next = item.nextRecycled
      this.firstFreeNode = next ?? null

      // reset, then set nextRef to undefined to allocate
      this.resetor(item as T, ...args)
      item.nextRecycled = undefined
      this.allocateCount -= 1
      return item as T
    } else {
      const newItem = this.creator(...args)
      return newItem
    }
  }

  recycle(item: T) {
    // cannot recycle twice
    if (
      item.nextRecycled !== undefined ||
      this.allocateCount > MAX_CACHE_COUNT
    ) {
      return
    }

    this.allocateCount += 1
    // insert to head of recycle queue
    item.nextRecycled = this.firstFreeNode
    this.firstFreeNode = item
  }

  dispose() {
    this.firstFreeNode = undefined as any
    this.allocateCount = 0
  }
}
