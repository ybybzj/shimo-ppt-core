export const BrowserInfo = {
  userAgent: '',
  opera: '',
  ie: false,
  inMacOs: false,
  inWindows: false,
  isSafariInMacOs: false,
  appleDevices: false,
  android: false,
  canTouch: false,
  isMobile: false,
  isPhone: false,
  isTablet: false,
  gecko: false,
  chrome: false,
  weChat: false,
  isOpera: false,
  webkit: false,
  safari: false,
  arm: false,
  mozilla: false,
  edge: false,
  ie9: false,
  ie10: false,
  isRetina: false,
  linux: false,
  originPixelRatio: 1, // 原始 dpr, 用于判断浏览器缩放是否有变化
  PixelRatio: 1, // 校准后 dpr，用于修正非整数 dpr 等
  vivaldiAndLinux: false,
  deviceSmallerSide: -1,
  /** 是否外接键盘, 这一项可能被外部运行时修改 */
  hasExternalKeyBoard: false,
}

export const TabletMiniMumSize = 640

// user agent lower case
BrowserInfo.userAgent = navigator.userAgent.toLowerCase()

if (typeof screen !== 'undefined') {
  BrowserInfo.deviceSmallerSide =
    screen.width < screen.height ? screen.width : screen.height
}

// ie detect
BrowserInfo.ie =
  BrowserInfo.userAgent.indexOf('msie') > -1 ||
  BrowserInfo.userAgent.indexOf('trident') > -1 ||
  BrowserInfo.userAgent.indexOf('edge') > -1

BrowserInfo.edge = BrowserInfo.userAgent.indexOf('edge/') > -1

BrowserInfo.ie9 =
  BrowserInfo.userAgent.indexOf('msie9') > -1 ||
  BrowserInfo.userAgent.indexOf('msie 9') > -1
BrowserInfo.ie10 =
  BrowserInfo.userAgent.indexOf('msie10') > -1 ||
  BrowserInfo.userAgent.indexOf('msie 10') > -1

// macOs detect
BrowserInfo.inMacOs = BrowserInfo.userAgent.indexOf('mac') > -1

BrowserInfo.inWindows = BrowserInfo.userAgent.indexOf('windows') > -1

// chrome detect
BrowserInfo.chrome =
  !BrowserInfo.ie && BrowserInfo.userAgent.indexOf('chrome') > -1

// safari detect
BrowserInfo.safari =
  !BrowserInfo.ie &&
  !BrowserInfo.chrome &&
  BrowserInfo.userAgent.indexOf('safari') > -1

// macOs safari detect
BrowserInfo.isSafariInMacOs = BrowserInfo.safari && BrowserInfo.inMacOs

BrowserInfo.weChat = BrowserInfo.userAgent.indexOf('wechat') > -1
// apple devices detect
BrowserInfo.appleDevices =
  BrowserInfo.userAgent.indexOf('ipad') > -1 ||
  BrowserInfo.userAgent.indexOf('iphone') > -1 ||
  BrowserInfo.userAgent.indexOf('ipod') > -1

// android devices detect
BrowserInfo.android = BrowserInfo.userAgent.indexOf('android') > -1

const operaStr = (window as any).opera

BrowserInfo.opera = operaStr
BrowserInfo.canTouch = typeof (window as any).ontouchmove !== 'undefined'
// mobile detect
BrowserInfo.isMobile =
  /android|avantgo|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od|ad)|iris|kindle|lge |maemo|midp|mmp|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(
    navigator.userAgent || navigator.vendor || operaStr
  )
BrowserInfo.isTablet =
  BrowserInfo.canTouch && BrowserInfo.deviceSmallerSide >= TabletMiniMumSize
BrowserInfo.isPhone =
  BrowserInfo.isMobile &&
  BrowserInfo.deviceSmallerSide !== -1 &&
  BrowserInfo.deviceSmallerSide < TabletMiniMumSize
// gecko detect
BrowserInfo.gecko = BrowserInfo.userAgent.indexOf('gecko/') > -1

// opera detect
BrowserInfo.isOpera = !!operaStr || BrowserInfo.userAgent.indexOf('opr/') > -1

// webkit detect
BrowserInfo.webkit =
  !BrowserInfo.ie && BrowserInfo.userAgent.indexOf('webkit') > -1

// arm detect
BrowserInfo.arm = BrowserInfo.userAgent.indexOf('arm') > -1

BrowserInfo.mozilla =
  !BrowserInfo.ie && BrowserInfo.userAgent.indexOf('firefox') > -1

BrowserInfo.linux = BrowserInfo.userAgent.indexOf(' linux ') > -1

BrowserInfo.vivaldiAndLinux =
  BrowserInfo.linux && BrowserInfo.userAgent.indexOf('vivaldi') > -1

BrowserInfo.originPixelRatio = window.devicePixelRatio

export function updatePixelRatio(): boolean {
  const oldIsRetina = BrowserInfo.isRetina
  if (BrowserInfo.android) {
    BrowserInfo.isRetina = window.devicePixelRatio >= 1.9
    BrowserInfo.PixelRatio = window.devicePixelRatio
    return oldIsRetina !== BrowserInfo.isRetina
  }

  BrowserInfo.isRetina = false
  BrowserInfo.PixelRatio = 1

  if (
    BrowserInfo.chrome &&
    !BrowserInfo.isOpera &&
    !BrowserInfo.isMobile &&
    document &&
    document.firstElementChild &&
    document.body
  ) {
    if (window.devicePixelRatio <= 1) {
      BrowserInfo.PixelRatio = 1
    } else {
      BrowserInfo.PixelRatio = window.devicePixelRatio
    }
    if (window.devicePixelRatio > 1) {
      BrowserInfo.isRetina = true
    }
  } else {
    BrowserInfo.isRetina = Math.abs(2 - window.devicePixelRatio) < 0.01
    if (BrowserInfo.isRetina) BrowserInfo.PixelRatio = 2

    if (BrowserInfo.isMobile) {
      BrowserInfo.isRetina = window.devicePixelRatio >= 1.9
      BrowserInfo.PixelRatio = window.devicePixelRatio
    }
  }

  return oldIsRetina !== BrowserInfo.isRetina
}

export function enlargeByRetinaRatio(value: number): number {
  return (value * BrowserInfo.PixelRatio + 0.5) >> 0
}
export function shrinkByRetinaRatio(value: number): number {
  return (value / BrowserInfo.PixelRatio + 0.5) >> 0
}
updatePixelRatio()
