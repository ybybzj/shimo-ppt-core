import { Dict } from '../../liber/pervasive'
import { GUID } from './guid'
import {
  Factor_in_to_mm,
  Factor_pc_to_mm,
  Factor_pix_to_mm,
  Factor_pt_to_mm,
} from '../core/common/const/unit'

export { GUID } from './guid'

export function createGUID() {
  let val = '{' + GUID() + '}'
  val = val.toUpperCase()
  return val
}

export function checkImageSrc(src) {
  return src ?? ''
}

export function checkRemoteImageUrl(url) {
  if (url && 0 === url.indexOf('data:image')) {
    return null
  }

  return url
}

export function isDict(obj): obj is Dict {
  return obj != null && typeof obj === 'object'
}

export function isFiniteNumber(n) {
  return !isNaN(parseFloat(n)) && isFinite(n)
}

export function isNumber(n): n is number {
  return typeof n === 'number' && !isNaN(n)
}

export function isBool(b): b is boolean {
  return b === true || b === false
}

export function toInt(v: any, radix = 10): number {
  return parseInt(v + '', radix)
}

export function round(x: number) {
  const y = x + (x >= 0 ? 0.5 : -0.5)
  return y | y
  //return Math.round(x);
}
const precisionRefinement = 0.999999999999999
export function floor(x: number) {
  let y = x | x
  y -= x < 0 && y > x ? 1 : 0
  return y + (x - y > precisionRefinement ? 1 : 0) // to fix float number precision caused by binary presentation
  //return Math.floor(x);
}

export function ceil(x: number) {
  let y = x | x
  y += x > 0 && y < x ? 1 : 0
  return y - (y - x > precisionRefinement ? 1 : 0) // to fix float number precision caused by binary presentation
  //return Math.ceil(x);
}

export function copyArr(arr: any[], isDeepCopy = false): any[] {
  const result: any[] = []
  const len = arr.length
  for (let i = 0; i < len; i++) {
    const item = arr[i]
    result[i] = Array.isArray(item) && isDeepCopy ? copyArr(item) : item
  }
  return result
}

export function normalizeMMToTwips(mm: number) {
  return ((((mm * 20 * 72) / 25.4 + 0.5) | 0) * 25.4) / 20 / 72
}

export function TwipsToMM(valueOfTwips) {
  return (
    ((null != valueOfTwips && null != valueOfTwips ? valueOfTwips : 1) * 25.4) /
    20 /
    72
  )
}

export function convertMMToTwips(mm) {
  return ((mm * 20 * 72) / 25.4 + 0.5) | 0
}

export function isNullString(str) {
  return str == null || str === ''
}

export function isEastAsianCharCode(charCode: number) {
  // Bopomofo (3100–312F)
  // Bopomofo Extended (31A0–31BF)
  // CJK Unified Ideographs (4E00–9FEA)
  // CJK Unified Ideographs Extension A (3400–4DB5)
  // CJK Unified Ideographs Extension B (20000–2A6D6)
  // CJK Unified Ideographs Extension C (2A700–2B734)
  // CJK Unified Ideographs Extension D (2B740–2B81D)
  // CJK Unified Ideographs Extension E (2B820–2CEA1)
  // CJK Unified Ideographs Extension F (2CEB0–2EBE0)
  // CJK Compatibility Ideographs (F900–FAFF)
  // CJK Compatibility Ideographs Supplement (2F800–2FA1F)
  // Kangxi Radicals (2F00–2FDF)
  // CJK Radicals Supplement (2E80–2EFF)
  // CJK Strokes (31C0–31EF)
  // Ideographic Description Characters (2FF0–2FFF)
  // Hangul Jamo (1100–11FF)
  // Hangul Jamo Extended-A (A960–A97F)
  // Hangul Jamo Extended-B (D7B0–D7FF)
  // Hangul Compatibility Jamo (3130–318F)
  // Halfwidth and Fullwidth Forms (FF00–FFEF)
  // Hangul Syllables (AC00–D7AF)
  // Hiragana (3040–309F)
  // Kana Extended-A (1B100–1B12F)
  // Kana Supplement (1B000–1B0FF)
  // Kanbun (3190–319F)
  // Katakana (30A0–30FF)
  // Katakana Phonetic Extensions (31F0–31FF)
  // Lisu (A4D0–A4FF)
  // Miao (16F00–16F9F)
  // Nushu (1B170–1B2FF)
  // Tangut (17000–187EC)
  // Tangut Components (18800–18AFF)
  // Yi Syllables (A000–A48F)
  // Yi Radicals (A490–A4CF)

  return (
    (0x3100 <= charCode && charCode <= 0x312f) ||
    (0x31a0 <= charCode && charCode <= 0x31bf) ||
    (0x4e00 <= charCode && charCode <= 0x9fea) ||
    (0x3400 <= charCode && charCode <= 0x4db5) ||
    (0x20000 <= charCode && charCode <= 0x2a6d6) ||
    (0x2a700 <= charCode && charCode <= 0x2b734) ||
    (0x2b740 <= charCode && charCode <= 0x2b81d) ||
    (0x2b820 <= charCode && charCode <= 0x2cea1) ||
    (0x2ceb0 <= charCode && charCode <= 0x2ebe0) ||
    (0xf900 <= charCode && charCode <= 0xfaff) ||
    (0x2f800 <= charCode && charCode <= 0x2fa1f) ||
    (0x2f00 <= charCode && charCode <= 0x2fdf) ||
    (0x2e80 <= charCode && charCode <= 0x2eff) ||
    (0x31c0 <= charCode && charCode <= 0x31ef) ||
    (0x2ff0 <= charCode && charCode <= 0x2fff) ||
    (0x1100 <= charCode && charCode <= 0x11ff) ||
    (0xa960 <= charCode && charCode <= 0xa97f) ||
    (0xd7b0 <= charCode && charCode <= 0xd7ff) ||
    (0x3130 <= charCode && charCode <= 0x318f) ||
    (0xff00 <= charCode && charCode <= 0xffef) ||
    (0xac00 <= charCode && charCode <= 0xd7af) ||
    (0x3040 <= charCode && charCode <= 0x309f) ||
    (0x1b100 <= charCode && charCode <= 0x1b12f) ||
    (0x1b000 <= charCode && charCode <= 0x1b0ff) ||
    (0x3190 <= charCode && charCode <= 0x319f) ||
    (0x30a0 <= charCode && charCode <= 0x30ff) ||
    (0x31f0 <= charCode && charCode <= 0x31ff) ||
    (0xa4d0 <= charCode && charCode <= 0xa4ff) ||
    (0x16f00 <= charCode && charCode <= 0x16f9f) ||
    (0x1b170 <= charCode && charCode <= 0x1b2ff) ||
    (0x17000 <= charCode && charCode <= 0x187ec) ||
    (0x18800 <= charCode && charCode <= 0x18aff) ||
    (0xa000 <= charCode && charCode <= 0xa48f) ||
    (0xa490 <= charCode && charCode <= 0xa4cf)
  )
}

export function sortAscending(a, b) {
  return a - b
}

export function sortDescending(a, b) {
  return b - a
}

export function isSurrogateCharCode(valueOfCharCode: number) {
  return valueOfCharCode >= 0xd800 && valueOfCharCode <= 0xdfff
}

export function mergeCharCodesForSurrogate(
  valueOfLeadingChar,
  valueOfTrailingChar
) {
  if (
    valueOfLeadingChar < 0xdc00 &&
    valueOfTrailingChar >= 0xdc00 &&
    valueOfTrailingChar <= 0xdfff
  ) {
    return (
      (0x10000 + ((valueOfLeadingChar & 0x3ff) << 10)) |
      (valueOfTrailingChar & 0x3ff)
    )
  } else return null
}

export function eachOfUnicodeString(
  s: string,
  handler: (charCode: null | number, iter: UnicodeStringIterator) => void
) {
  for (let iter = new UnicodeStringIterator(s); iter.hasNext(); iter.next()) {
    handler(iter.value(), iter)
  }
}
class UnicodeStringIterator {
  private _position: number
  private index: number
  private str: string
  constructor(str) {
    this._position = 0
    this.index = 0
    this.str = str
  }

  isOutside() {
    return this.index >= this.str.length
  }

  isInside() {
    return this.index < this.str.length
  }

  hasNext() {
    return this.index < this.str.length
  }

  value() {
    if (this.index >= this.str.length) return 0

    const valueOfCharCode = this.str.charCodeAt(this.index)
    if (!isSurrogateCharCode(valueOfCharCode)) return valueOfCharCode

    if (this.str.length - 1 === this.index) return valueOfCharCode // error

    const valueOfTrailingChar = this.str.charCodeAt(this.index + 1)
    return mergeCharCodesForSurrogate(valueOfCharCode, valueOfTrailingChar)
  }

  next() {
    if (this.index >= this.str.length) return

    this._position++
    if (!isSurrogateCharCode(this.str.charCodeAt(this.index))) {
      ++this.index
      return
    }

    if (this.index === this.str.length - 1) {
      ++this.index
      return
    }

    this.index += 2
  }

  position() {
    return this._position
  }
}

export function stringFromCharCode(charCode: number) {
  if (charCode < 0x10000) {
    return String.fromCharCode(charCode)
  } else {
    charCode = charCode - 0x10000
    const valueOfLeadingChar = 0xd800 | (charCode >> 10)
    const valueOfTrailingChar = 0xdc00 | (charCode & 0x3ff)
    return (
      String.fromCharCode(valueOfLeadingChar) +
      String.fromCharCode(valueOfTrailingChar)
    )
  }
}

export function convertUnicodeToUTF16(unicodes: number[]) {
  let utf16 = ''
  const valueOfLength = unicodes.length
  for (let valueOfPos = 0; valueOfPos < valueOfLength; valueOfPos++) {
    utf16 += stringFromCharCode(unicodes[valueOfPos])
  }

  return utf16
}

export function convertUTF16toUnicode(utf16: string) {
  const unicodes: number[] = []
  const valueOfLength = utf16.length
  for (let valueOfPos = 0; valueOfPos < valueOfLength; valueOfPos++) {
    let valueOfUnicode: null | number = null
    const valueOfCharCode = utf16.charCodeAt(valueOfPos)
    if (isSurrogateCharCode(valueOfCharCode)) {
      if (valueOfPos + 1 < valueOfLength) {
        valueOfPos++
        const valueOfTrailingChar = utf16.charCodeAt(valueOfPos)
        valueOfUnicode = mergeCharCodesForSurrogate(
          valueOfCharCode,
          valueOfTrailingChar
        )
      }
    } else valueOfUnicode = valueOfCharCode

    if (null != valueOfUnicode) unicodes.push(valueOfUnicode)
  }

  return unicodes
}

export function parseJSON(v: string): any {
  let r = v
  try {
    r = JSON.parse(v)
  } catch (e) {
    console.warn(`[JSON.parse] parse failed with "${v}"`)
  }

  return r
}

export function convertPtToMM(value: number): number {
  return (value * 25.4) / 72.0
}

export function convertMMToPt(value: number): number {
  return (value * 72.0) / 25.4
}

export function convertUnitValStrToMMWithUnit(str: string) {
  let val = parseFloat(str)
  let type
  if (!isNaN(val)) {
    if (-1 !== str.indexOf('%')) {
      type = '%'
      val /= 100
    } else if (-1 !== str.indexOf('px')) {
      type = 'px'
      val *= Factor_pix_to_mm
    } else if (-1 !== str.indexOf('in')) {
      type = 'in'
      val *= Factor_in_to_mm
    } else if (-1 !== str.indexOf('cm')) {
      type = 'cm'
      val *= 10
    } else if (-1 !== str.indexOf('mm')) {
      type = 'mm'
    } else if (-1 !== str.indexOf('pt')) {
      type = 'pt'
      val *= Factor_pt_to_mm
    } else if (-1 !== str.indexOf('pc')) {
      type = 'pc'
      val *= Factor_pc_to_mm
    } else {
      type = 'none'
    }
    return { val: val, type: type }
  }
  return null
}

export function checkImageByUrl(url: string) {
  if (typeof url !== 'string') {
    return false
  }
  const [, format = ''] = url.match(/https?:\/\/.*(jpe?g|gif|png)/i) || []
  return !!format
}

export function appendArrays<T>(target: T[], arr: T[]) {
  const appendLen = arr.length
  const targetLen = target.length
  const len = targetLen + appendLen
  target.length = len
  for (let i = 0; i < appendLen; i++) {
    target[targetLen + i] = arr[i]
  }
}

export const throttle = (
  func: any,
  limit: number = 100,
  trailing?: boolean
) => {
  let inThrottle
  return (...args) => {
    if (!inThrottle) {
      const next = () => func(...args)
      if (!trailing) {
        next()
      }
      inThrottle = true
      setTimeout(() => {
        if (trailing) {
          next()
        }
        inThrottle = false
      }, limit)
    }
  }
}

// from lodash.js
export function memoize(
  func: (...args: any[]) => void,
  resolver?: (...args: any[]) => any
) {
  if (
    typeof func != 'function' ||
    (resolver != null && typeof resolver != 'function')
  ) {
    throw new Error()
  }
  const memoized = function (...args) {
    const key = resolver ? resolver.apply(this, args) : args[0]
    const cache = memoized.cache

    if (cache.has(key)) {
      return cache.get(key)
    }
    const result = func.apply(this, args)
    memoized.cache = cache.set(key, result) || cache
    return result
  }
  memoized.cache = new Map()
  return memoized
}

// https://github.com/lodash/lodash/issues/2403#issuecomment-290734839
export const memoizeDebounce = (
  func: (...args: any[]) => void,
  limit = 0,
  options: { resolver?: (...args: any[]) => any } = {}
) => {
  const mem = memoize(function () {
    return debounce(func, limit)
  }, options.resolver)
  return function (...args) {
    mem(...args)(...args)
  }
}

export const debounce = (func: any, limit: number = 100) => {
  let timeout
  return (...args) => {
    const next = () => func(...args)
    clearTimeout(timeout)
    timeout = setTimeout(next, limit)
  }
}

export function padNumber(n: number | string, fill: number): string {
  const len = ('' + n).length
  return Array(fill > len ? fill - len + 1 || 0 : 0).join('0') + n
}

export function isGifImage(url: string): boolean {
  if (url.startsWith('data:image')) {
    return url.startsWith('data:image/gif')
  }
  const exp = /([/|.|\w|\s|-])*\.(?:gif)/i
  return exp.test(url)
}

export function limitInvoke<Args extends any[]>(
  fn: (...args: Args) => void,
  threshold: number
): (...args: Args) => void {
  let invokeTimestamp
  return (...args: Args) => {
    const currentTimestamp = Date.now()
    if (
      invokeTimestamp == null ||
      currentTimestamp - invokeTimestamp >= threshold
    ) {
      fn(...args)
      const afterInvoke = Date.now()
      invokeTimestamp =
        afterInvoke - currentTimestamp < threshold
          ? currentTimestamp
          : afterInvoke
    }
  }
}

export const nowFn =
  typeof window !== 'undefined' && typeof window['performance'] !== 'undefined'
    ? () => performance.now()
    : () => Date.now()

export function startPerfTiming(): () => number {
  const t = nowFn()
  return () => nowFn() - t
}

export function isBase64Url(url: any) {
  return typeof url === 'string' && url.startsWith('data:')
}
