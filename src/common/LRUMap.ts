export class LRUMap<K, V> {
  private capacity: number
  private cache: Map<K, V> = new Map<K, V>()
  private cleanup?: (v: V) => void
  constructor(cap: number, cleanup?: (v: V) => void) {
    this.capacity = cap
    this.cleanup = cleanup
  }
  set(key: K, value: V) {
    if (this.capacity < 1) return

    if (this.cache.size >= this.capacity) {
      const lruKey = this.cache.keys().next().value
      const removedV = this.cache.get(lruKey)!
      this.cache.delete(lruKey)
      if (this.cleanup) {
        this.cleanup(removedV)
      }
    }
    this.cache.delete(key)
    this.cache.set(key, value)
  }
  delete(key: K) {
    if (this.capacity < 1) return

    const removedV = this.cache.get(key)
    this.cache.delete(key)
    if (removedV !== undefined && this.cleanup) {
      this.cleanup(removedV)
    }
  }
  get(key: K): undefined | V {
    if (this.capacity < 1) return

    let result: undefined | V
    if (this.cache.has(key)) {
      result = this.cache.get(key)!
      this.cache.delete(key)
      this.cache.set(key, result)
    }
    return result
  }
  getCache(): Map<K, V> {
    return this.cache
  }

  isFull() {
    return this.cache.size >= this.capacity
  }
}
