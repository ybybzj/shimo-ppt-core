import { convertUnitValStrToMMWithUnit, floor } from '../utils'

export function convertHexStringToRGB(hex: string) {
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
      }
    : null
}

export function convertRGBToHexString(
  r: number = 0,
  g: number = 0,
  b: number = 0
) {
  return [r, g, b]
    .map((x) => {
      const hex = x.toString(16)
      return hex.length === 1 ? '0' + hex : hex
    })
    .join('')
}

export function convertRGBStringToRGB(color: string) {
  const startIndex = color.indexOf('(')
  const endIndex = color.indexOf(')')
  if (-1 !== startIndex && -1 !== endIndex && startIndex < endIndex) {
    const rgbStr = color.substring(startIndex + 1, endIndex)
    const rgbs = rgbStr.split(',')
    if (rgbs.length >= 3) {
      if (rgbs.length >= 4) {
        const a = convertUnitValStrToMMWithUnit(rgbs[3])!
        if (0 === a.val) {
          return null
        }
      }
      const rObj = convertUnitValStrToMMWithUnit(rgbs[0])!
      const gObj = convertUnitValStrToMMWithUnit(rgbs[1])!
      const bObj = convertUnitValStrToMMWithUnit(rgbs[2])!
      let r, g, b
      if (rObj && '%' === rObj.type) {
        r = floor((255 * rObj.val) / 100)
      } else {
        r = rObj.val
      }
      if (gObj && '%' === gObj.type) {
        g = floor((255 * gObj.val) / 100)
      } else {
        g = gObj.val
      }
      if (bObj && '%' === bObj.type) {
        b = floor((255 * bObj.val) / 100)
      } else {
        b = bObj.val
      }
      return { r, g, b }
    }
  }
}
