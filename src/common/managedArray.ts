import { Nullable } from '../../liber/pervasive'

export interface ManagedSlice<T> {
  at(index: number): Nullable<T>
  length: number
  elements: readonly T[]
}

export class ManagedArray<T> {
  readonly __$ManagedSlice$__ = true
  private array: T[]
  private endIndex: number = 0
  private maxCapacity: number

  // return readonly original array
  get elements() {
    return this.array
  }
  get length() {
    return this.endIndex
  }

  get arraySlice(){
    return this.array.slice(0, this.endIndex)
  }
  constructor(maxCapacity?: number) {
    this.maxCapacity = maxCapacity ?? 1000
    this.array = []
  }

  reset() {
    if (this.array.length > this.maxCapacity) {
      this.array.length = this.maxCapacity
    }
    this.endIndex = 0
  }

  dispose(cb?: (item: T, index: number) => void) {
    if (cb) {
      forEach(this, cb)
    }
    this.array.length = 0
    this.endIndex = 0
  }

  at(index: number): Nullable<T> {
    if (index < 0 || index >= this.endIndex) return undefined
    return this.array[index]
  }

  set(index: number, v: T) {
    if (index < 0 || index >= this.endIndex) return false
    this.array[index] = v
    return true
  }

  push(item: T) {
    if (this.array.length > this.endIndex) {
      this.array[this.endIndex] = item
    } else {
      this.array.push(item)
    }
    this.endIndex += 1
  }

  insert(index: number, ...items: T[]) {
    index = index < 0 ? 0 : index > this.endIndex ? this.endIndex : index

    if (index === this.endIndex) {
      for (let i = 0, l = items.length; i < l; i++) {
        this.push(items[i])
      }
      return
    }

    if (this.array.length !== this.endIndex) {
      this.array.length = this.endIndex
    }
    this.array.splice(index, 0, ...items)
    this.endIndex += items.length
  }

  remove(index: number, count: number) {
    index = index < 0 ? 0 : index > this.endIndex ? this.endIndex : index
    const result = this.array.splice(index, count)
    this.endIndex -= result.length
    return result
  }
}

export class RecyclableArray<T> {
  readonly __$ManagedSlice$__ = true
  private array: T[]
  private recycleFn: (item: T) => void
  get elements() {
    return this.array
  }
  get length() {
    return this.array.length
  }
  constructor(recycleFn: (item: T) => void) {
    this.array = []
    this.recycleFn = recycleFn
  }
  recycle(items: T[]) {
    for (let i = 0, l = items.length; i < l; i++) {
      const item = items[i]
      this.recycleFn(item)
    }
  }

  private hasIndex(index: number) {
    return index >= 0 && index < this.array.length
  }
  recycleIndexOf(index: number) {
    if (this.hasIndex(index)) {
      this.recycleFn(this.array[index])
    }
  }

  clear() {
    this.array.forEach((item) => this.recycleFn(item))
    this.array.length = 0
  }
  // reset length to {len}, which is less than the current {this.array.length}
  resetLength(len: number) {
    const l = this.length
    len = Math.min(l, len)
    if (len < l) {
      for (let i = len; i < l; i++) {
        this.recycleFn(this.array[i])
      }
      this.array.length = len
    }
  }

  at(index: number): Nullable<T> {
    return this.hasIndex(index) ? this.array[index] : undefined
  }

  push(item: T) {
    this.array.push(item)
  }

  insert(index: number, ...items: T[]) {
    index = index < 0 ? 0 : index > this.length ? this.length : index
    this.array.splice(index, 0, ...items)
  }

  append(items: T[]) {
    for (let i = 0, l = items.length; i < l; i++) {
      this.array.push(items[i])
    }
  }

  remove(index: number, count: number): T[] {
    index = index < 0 ? 0 : index > this.length ? this.length : index
    const result = this.array.splice(index, count)
    return result
  }
}

class SlicedManagedArray<T> {
  readonly __$ManagedSlice$__ = true
  private slice!: ManagedSlice<T>
  private sliceStart!: number
  private sliceEnd!: number
  get elements() {
    const array = this.slice.elements.slice(this.sliceStart, this.sliceEnd)
    return array
  }
  static new<T>(managedArray: ManagedSlice<T>, start?: number, end?: number) {
    return new SlicedManagedArray<T>(managedArray, start, end)
  }
  constructor(managedArray: ManagedSlice<T>, start?: number, end?: number) {
    this.reset(managedArray, start, end)
  }
  reset(managedArray: ManagedSlice<T>, start?: number, end?: number) {
    this.slice = managedArray
    const l = managedArray.length
    start = start ?? 0
    end = end ?? l

    if (start > end) {
      const t = end
      end = start
      start = t
    }

    this.sliceStart = Math.max(0, start)
    this.sliceEnd = Math.max(0, Math.min(end, l))
  }

  get length() {
    return this.sliceEnd - this.sliceStart
  }

  at(index: number): Nullable<T> {
    return this.slice.at(this.sliceStart + index)
  }
}

class MappedManagedArray<T, O> {
  readonly __$ManagedSlice$__ = true
  private slice!: ManagedSlice<O>
  private mapper: (item: O, index: number) => T
  get elements() {
    return this.slice.elements.map((item, index) => this.mapper(item, index))
  }
  static new<T, O>(managedArray: ManagedSlice<O>, converter: (item: O) => T) {
    return new MappedManagedArray<T, O>(managedArray, converter)
  }
  constructor(managaedArray: ManagedSlice<O>, converter: (item: O) => T) {
    this.slice = managaedArray
    this.mapper = converter
  }
  reset(managedArray: ManagedSlice<O>, converter: (item: O) => T) {
    this.slice = managedArray
    this.mapper = converter
  }

  get length() {
    return this.slice.length
  }

  at(index: number): Nullable<T> {
    const item = this.slice.at(index)
    return item != null ? this.mapper(item, index) : undefined
  }
}

function isManagedSlice(o: any): o is ManagedSlice<any> {
  return Object(o) === o && o.__$ManagedSlice$__ === true
}

function forEach<T>(
  slice: ManagedSlice<T>,
  cb: (item: T, index: number) => void
): void {
  for (let i = 0, l = slice.length; i < l; i++) {
    cb(slice.at(i)!, i)
  }
}

function reverseEach<T>(
  slice: ManagedSlice<T>,
  cb: (item: T, index: number) => void
): void {
  for (let l = slice.length, i = l - 1; i >= 0; i--) {
    cb(slice.at(i)!, i)
  }
}

function findIndex<T>(
  slice: ManagedSlice<T>,
  pred: (item: T, index: number) => boolean
): number {
  let found: number = -1
  for (let i = 0, l = slice.length; i < l; i++) {
    const item = slice.at(i)!
    if (pred(item, i) === true) {
      found = i
      break
    }
  }
  return found
}

function find<T>(
  slice: ManagedSlice<T>,
  pred: (item: T, index: number) => boolean
): Nullable<T> {
  let found: Nullable<T>
  for (let i = 0, l = slice.length; i < l; i++) {
    const item = slice.at(i)!
    if (pred(item, i) === true) {
      found = item
      break
    }
  }
  return found
}

function slice<T>(managedArray: ManagedSlice<T>, start?: number, end?: number) {
  return SlicedManagedArray.new<T>(managedArray, start, end)
}

function map<T, O>(managedArray: ManagedSlice<O>, mapper: (item: O) => T) {
  return MappedManagedArray.new<T, O>(managedArray, mapper)
}

export const ManagedSliceUtil = {
  isManagedSlice,
  forEach,
  reverseEach,
  findIndex,
  find,
  slice,
  map,
} as const
