import { escapeHtml } from '../../lib/escapeHtml'

export function createElement(tagName: any): HTMLElement {
  return document.createElement(tagName)
}

export function createElementNS(
  namespaceURI: string,
  qualifiedName: string
): Element {
  return document.createElementNS(namespaceURI, qualifiedName)
}

export function createTextNode(text: string): Text {
  return document.createTextNode(escapeHtml(text))
}

export function createComment(text: string): Comment {
  return document.createComment(text)
}

export function insertBefore(
  parentNode: Node,
  newNode: Node,
  referenceNode: Node | null
): void {
  parentNode.insertBefore(newNode, referenceNode)
}

export function removeChild(node: Node, child: Node): void {
  node.removeChild(child)
}

export function appendChild(node: Node, child: Node): void {
  node.appendChild(child)
}
export function appendChildren(node: Node, children: Node[]): void {
  for (const child of children) {
    node.appendChild(child)
  }
}

export function parentNode(node: Node): Node | null {
  return node.parentNode
}

export function nextSibling(node: Node): Node | null {
  return node.nextSibling
}

export function tagName(elm: Element): string {
  return elm.tagName
}

export function getOffset(
  el: HTMLElement | string
): undefined | { top: number; left: number } {
  const element = typeof el === 'string' ? document.getElementById(el) : el
  if (!element) return
  const rect = element.getBoundingClientRect()

  return {
    top: rect.top + document.body.scrollTop,
    left: rect.left + document.body.scrollLeft,
  }
}

export function setImageSmoothingForCanvasCtx(ctx, enable: boolean) {
  if (ctx && enable != null) {
    ctx.imageSmoothingEnabled = enable
    ctx.mozImageSmoothingEnabled = enable
    ctx.oImageSmoothingEnabled = enable
    ctx.webkitImageSmoothingEnabled = enable
  }
}

export function replaceNodeWith(
  oldNode: Node,
  ...newNodes: Array<Node | string>
) {
  const parent = oldNode.parentNode
  let i = newNodes.length
  let currentNode
  if (!parent) return
  if (i <= 0) {
    // if there are no arguments
    parent.removeChild(oldNode)
  }
  while (i--) {
    // i-- decrements i and returns the value of i before the decrement
    currentNode = newNodes[i]
    if (typeof currentNode === 'string') {
      currentNode = oldNode.ownerDocument!.createTextNode(currentNode)
    } else if (currentNode.parentNode) {
      currentNode.parentNode.removeChild(currentNode)
    }
    // the value of "i" below is after the decrement
    if (i <= 0) {
      // if currentNode is the first argument (currentNode === arguments[0])
      parent.replaceChild(currentNode, oldNode)
    }
    // if currentNode isn't the first
    else parent.insertBefore(currentNode, oldNode.nextSibling)
  }
}

export function appendWith(
  parent: Element,
  ...nodes: Array<Node | string>
): void {
  const docFrag = document.createDocumentFragment()

  nodes.forEach((node) => {
    const isNode = node instanceof Node
    docFrag.appendChild(
      isNode ? (node as Node) : document.createTextNode(String(node))
    )
  })

  parent.appendChild(docFrag)
}

export function removeAllChildren(parent: Element) {
  if (parent.textContent != null) {
    parent.textContent = ''
  } else {
    while (parent.firstChild) {
      parent.removeChild(parent.lastChild!)
    }
  }
}
