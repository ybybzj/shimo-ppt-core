import { BrowserInfo } from '../browserInfo'
import { EditorKit } from '../../editor/global'
import { Nullable, valuesOfDict } from '../../../liber/pervasive'
import { DomWheelEvt, handleWheelEvt, wheelEventName } from './wheelEvent'

export const POINTER_EVENT_TYPE = {
  Down: 0,
  Move: 1,
  Up: 2,
  Wheel: 3,
} as const
export type PointerEvtTypeValue = valuesOfDict<typeof POINTER_EVENT_TYPE>

export const POINTER_BUTTON = {
  Left: 0,
  Center: 1,
  Right: 2,
} as const

export type PointerTypes = 'mouse' | 'touch' | 'pencil'

export function stopEvent(e?: Event) {
  if (!e) return
  if (e.preventDefault) e.preventDefault()
  if (e.stopPropagation) e.stopPropagation()
}

export function preventDefault({ e }: { e: PointerEvent }) {
  if (!BrowserInfo.ie) {
    if (e.preventDefault) e.preventDefault()
    else e.returnValue = false
  }
}

export const DoubleClickDuration = BrowserInfo.isTablet ? 300 : 500 // ms
export const TouchEPS = 20 * BrowserInfo.PixelRatio
export const MouseEPS = 3 * BrowserInfo.PixelRatio

export function getPointerEventPos(e: PointerEvent) {
  if (e.pageX || e.pageY) {
    return [e.pageX >> 0, e.pageY >> 0] as const
  } else if (e.clientX || e.clientY) {
    return [e.clientX >> 0, e.clientY >> 0] as const
  }
}

export class PointerEventInfo {
  x: number
  y: number
  button: number
  type: PointerEvtTypeValue
  altKey: boolean
  ctrlKey: boolean
  shiftKey: boolean
  target: Nullable<Element>
  lastClickTime: number
  clicks: number
  isPressed: boolean
  lastX: number
  lastY: number
  isLocked: boolean
  isMove: boolean
  private isDeviceTypeLocked = false
  /** 一次完整的操作是由鼠标还是触屏引起的, 该操作后续都响应对应的逻辑 */
  private lockedDeviceType: PointerTypes = 'mouse'

  constructor() {
    this.x = 0
    this.y = 0

    this.button = POINTER_BUTTON.Left
    this.type = POINTER_EVENT_TYPE.Move

    this.altKey = false
    this.ctrlKey = false
    this.shiftKey = false

    this.target = null

    this.lastClickTime = -1
    this.clicks = 0

    this.isPressed = false
    this.lastX = 0
    this.lastY = 0

    this.isLocked = false

    this.isMove = false
  }

  lock() {
    if (!this.isLocked) {
      this.isLocked = true

      return true
    }
    return false
  }

  unLock() {
    if (this.isLocked) {
      this.isLocked = false

      EditorKit.collaborationManager.informUndoRedo()
      return true
    }
    return false
  }

  isTouchMode() {
    return (
      this.lockedDeviceType === 'touch' || this.lockedDeviceType === 'pencil'
    )
  }

  /** 直至本次 pointerUp 所有逻辑执行完毕，都保持 mouse | touch 模式 */
  lockPointerType(e: PointerEvent) {
    if (!this.isDeviceTypeLocked) {
      this.lockedDeviceType = e.pointerType as PointerTypes
      this.isDeviceTypeLocked = true
    }
  }

  unLockPointerType() {
    this.isDeviceTypeLocked = false
  }

  onDown(e: PointerEvent, isLockPointer: boolean, isCapture = false) {
    const pos = getPointerEventPos(e)
    if (pos) {
      ;[this.x, this.y] = pos
    }

    const eps = e.pointerType !== 'mouse' ? TouchEPS : MouseEPS
    if (
      Math.abs(this.x - this.lastX) > eps ||
      Math.abs(this.y - this.lastY) > eps
    ) {
      // not only move!!! (touch - fast click in different places)
      this.lastClickTime = -1
      this.clicks = 0
    }

    this.lastX = this.x
    this.lastY = this.y

    this.altKey = e.altKey
    this.shiftKey = e.shiftKey
    this.ctrlKey = e.ctrlKey || e.metaKey

    GlobalEvents.keyboard.altKey = this.altKey
    GlobalEvents.keyboard.shiftKey = this.shiftKey
    GlobalEvents.keyboard.ctrlKey = this.ctrlKey
    this.type = POINTER_EVENT_TYPE.Down
    this.button = e.button !== undefined ? e.button : 0
    if (this.button === -1) {
      this.button = 0
    }

    if (!this.isLocked || !this.target) {
      this.target = (e.target ? e.target : e.srcElement) as Element
    }
    if (isLockPointer === true) {
      this.lock()
      if (isCapture && e.pointerId != null) {
        this.target.setPointerCapture(e.pointerId)
      }
    }

    if (isLockPointer && this.button === POINTER_BUTTON.Left) {
      const curTime = new Date().getTime()
      if (0 === this.clicks) {
        this.clicks = 1
        this.lastClickTime = curTime
      } else {
        if (DoubleClickDuration > curTime - this.lastClickTime) {
          this.lastClickTime = curTime
          this.clicks++
        } else {
          this.clicks = 1
          this.lastClickTime = curTime
        }
      }
    } else {
      this.lastClickTime = -1
      this.clicks = 1
    }
  }
  onMove(e: PointerEvent) {
    const pos = getPointerEventPos(e)
    if (pos) {
      ;[this.x, this.y] = pos
    }

    this.altKey = e.altKey
    this.shiftKey = e.shiftKey
    this.ctrlKey = e.ctrlKey || e.metaKey

    this.type = POINTER_EVENT_TYPE.Move

    if (!this.isLocked) {
      this.target = (e.target ? e.target : e.srcElement) as Element
    }

    const eps = 5
    if (
      Math.abs(this.x - this.lastX) > eps ||
      Math.abs(this.y - this.lastY) > eps
    ) {
      this.lastClickTime = -1
      this.clicks = 0
      this.isMove = true
    } else {
      this.isMove = false
    }
  }
  onUp(e: PointerEvent) {
    const pos = getPointerEventPos(e)
    if (pos) {
      ;[this.x, this.y] = pos
    }

    this.altKey = e.altKey
    this.shiftKey = e.shiftKey
    this.ctrlKey = e.ctrlKey || e.metaKey

    GlobalEvents.keyboard.altKey = this.altKey
    GlobalEvents.keyboard.shiftKey = this.shiftKey
    GlobalEvents.keyboard.ctrlKey = this.ctrlKey

    this.type = POINTER_EVENT_TYPE.Up
    this.button = e.button !== undefined ? e.button : 0

    if (this.isLocked === true && e.pointerId != null) {
      this.target?.releasePointerCapture(e.pointerId)
    }
    const newTarget = (e.target ? e.target : e.srcElement) as Element
    this.target = newTarget

    this.unLock()

    this.isPressed = false
  }
}

export class KeyboardEventInfo {
  altKey: boolean = false
  ctrlKey: boolean = false
  shiftKey: boolean = false
  altGr: boolean = false
  target: EventTarget | null = null
  charCode: number = 0
  keyCode: number = 0
  which: number = -1

  onKeyUp() {
    this.altKey = false
    this.ctrlKey = false
    this.shiftKey = false
    this.altGr = false
  }
  onKeyEvent(e: KeyboardEvent) {
    this.altKey = e.altKey
    this.altGr = getAltGr(e)
    this.ctrlKey = !this.altGr && (e.metaKey || e.ctrlKey)

    this.shiftKey = e.shiftKey

    this.target = e.srcElement ? e.srcElement : e.target

    this.charCode = e.charCode
    this.keyCode = e.keyCode
    this.which = e.which
  }
  updateModifyKeys(e: KeyboardEvent) {
    this.altKey = e.altKey

    if (e.metaKey != null) {
      this.ctrlKey = e.ctrlKey || e.metaKey
    } else this.ctrlKey = e.ctrlKey

    this.shiftKey = e.shiftKey

    this.altGr = this.ctrlKey && this.altKey ? true : false

    if (this.altGr === true) {
      this.ctrlKey = false
    }
  }
}

export const GlobalEvents: {
  keyboard: KeyboardEventInfo
} = {
  keyboard: new KeyboardEventInfo(),
}

function getAltGr(e: KeyboardEvent) {
  const ctrlKey = e.metaKey || e.ctrlKey
  const altKey = e.altKey
  return altKey && (BrowserInfo.inMacOs ? !ctrlKey : ctrlKey)
}

export type PointerEventType =
  | 'pointerdown'
  | 'pointermove'
  | 'pointerup'
  | 'wheel'
  | 'pointerleave'
  | 'pointerout'
export type PointerEventArg<T extends PointerEventType> = {
  pointerdown: PointerEvent
  pointerup: PointerEvent
  pointermove: PointerEvent
  pointerleave: PointerEvent
  pointerout: PointerEvent
  wheel: DomWheelEvt
}[T]

export type PointerHandler =
  | {
      handlePointerEvent<T extends PointerEventType>(
        evtType: T,
        e: PointerEventArg<T>
      ): void
    }
  | {
      <T extends PointerEventType>(evtType: T, e: PointerEventArg<T>): void
    }

export function listenPointerEvents(
  el: Element,
  evts: PointerEventType[],
  handler: PointerHandler,
  isCapture = false
): () => void {
  const handlerWrapper = {
    handleEvent(e: PointerEvent | WheelEvent) {
      switch (e.type) {
        case 'pointerdown':
        case 'pointerup':
        case 'pointercancel':
        case 'pointermove':
        case 'pointerleave':
        case 'pointerout': {
          const etype = e.type === 'pointercancel' ? 'pointerup' : e.type
          if (typeof handler === 'function') {
            handler(etype, e as PointerEvent)
          } else {
            handler.handlePointerEvent(etype, e as PointerEvent)
          }
          break
        }
        case 'wheel':
        case 'mousewheel': {
          if (typeof handler === 'function') {
            handleWheelEvt(e, (evt) => handler('wheel', evt))
          } else {
            handleWheelEvt(e, (evt) => handler.handlePointerEvent('wheel', evt))
          }
          break
        }
      }
    },
  }
  for (const evtName of evts) {
    if (evtName === 'wheel') {
      el.addEventListener(`${wheelEventName}`, handlerWrapper, isCapture)
    } else {
      el.addEventListener(evtName, handlerWrapper, isCapture)
    }
  }

  return function unlistenEvts() {
    if (el == null) return
    for (const evtName of evts) {
      if (evtName === 'wheel') {
        el.removeEventListener(`${wheelEventName}`, handlerWrapper, isCapture)
      } else {
        el.removeEventListener(evtName, handlerWrapper, isCapture)
      }
    }
  }
}
