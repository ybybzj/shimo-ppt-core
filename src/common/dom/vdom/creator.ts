import { createPlaceholderVnode } from './utils'
import { makeCreator } from './vnode'

export const Dom = {
  a: makeCreator('a'),
  abbr: makeCreator('abbr'),
  address: makeCreator('address'),
  area: makeCreator('area'),
  article: makeCreator('article'),
  aside: makeCreator('aside'),
  audio: makeCreator('audio'),
  b: makeCreator('b'),
  base: makeCreator('base'),
  bdi: makeCreator('bdi'),
  bdo: makeCreator('bdo'),
  blockquote: makeCreator('blockquote'),
  body: makeCreator('body'),
  br: makeCreator('br'),
  button: makeCreator('button'),
  canvas: makeCreator<HTMLCanvasElement>('canvas'),
  caption: makeCreator('caption'),
  cite: makeCreator('cite'),
  code: makeCreator('code'),
  col: makeCreator('col'),
  colgroup: makeCreator('colgroup'),
  data: makeCreator('data'),
  datalist: makeCreator('datalist'),
  dd: makeCreator('dd'),
  del: makeCreator('del'),
  details: makeCreator('details'),
  dfn: makeCreator('dfn'),
  dialog: makeCreator('dialog'),
  div: makeCreator<HTMLDivElement>('div'),
  dl: makeCreator('dl'),
  dt: makeCreator('dt'),
  em: makeCreator('em'),
  embed: makeCreator('embed'),
  fieldset: makeCreator('fieldset'),
  figure: makeCreator('figure'),
  footer: makeCreator('footer'),
  form: makeCreator('form'),
  h1: makeCreator('h1'),
  h2: makeCreator('h2'),
  h3: makeCreator('h3'),
  h4: makeCreator('h4'),
  h5: makeCreator('h5'),
  h6: makeCreator('h6'),
  head: makeCreator('head'),
  header: makeCreator('header'),
  hgroup: makeCreator('hgroup'),
  hr: makeCreator('hr'),
  html: makeCreator('html'),
  i: makeCreator('i'),
  iframe: makeCreator('iframe'),
  img: makeCreator('img'),
  input: makeCreator('input'),
  ins: makeCreator('ins'),
  kbd: makeCreator('kbd'),
  keygen: makeCreator('keygen'),
  label: makeCreator('label'),
  legend: makeCreator('legend'),
  li: makeCreator('li'),
  link: makeCreator('link'),
  main: makeCreator('main'),
  map: makeCreator('map'),
  mark: makeCreator('mark'),
  menu: makeCreator('menu'),
  menuitem: makeCreator('menuitem'),
  meta: makeCreator('meta'),
  meter: makeCreator('meter'),
  nav: makeCreator('nav'),
  noscript: makeCreator('noscript'),
  object: makeCreator('object'),
  ol: makeCreator('ol'),
  optgroup: makeCreator('optgroup'),
  option: makeCreator('option'),
  output: makeCreator('output'),
  p: makeCreator('p'),
  param: makeCreator('param'),
  pre: makeCreator('pre'),
  progress: makeCreator('progress'),
  q: makeCreator('q'),
  rb: makeCreator('rb'),
  rp: makeCreator('rp'),
  rt: makeCreator('rt'),
  rtc: makeCreator('rtc'),
  ruby: makeCreator('ruby'),
  s: makeCreator('s'),
  samp: makeCreator('samp'),
  script: makeCreator('script'),
  section: makeCreator('section'),
  select: makeCreator('select'),
  small: makeCreator('small'),
  source: makeCreator('source'),
  span: makeCreator('span'),
  strong: makeCreator('strong'),
  style: makeCreator('style'),
  sub: makeCreator('sub'),
  summary: makeCreator('summary'),
  sup: makeCreator('sup'),
  table: makeCreator('table'),
  tbody: makeCreator('tbody'),
  td: makeCreator('td'),
  template: makeCreator('template'),
  textarea: makeCreator('textarea'),
  tfoot: makeCreator('tfoot'),
  th: makeCreator('th'),
  thead: makeCreator('thead'),
  time: makeCreator('time'),
  title: makeCreator('title'),
  tr: makeCreator('tr'),
  track: makeCreator('track'),
  u: makeCreator('u'),
  ul: makeCreator('ul'),
  var: makeCreator('var'),
  video: makeCreator('video'),
  wbr: makeCreator('wbr'),
  comment: makeCreator<Comment>('!'),
  placeholder: createPlaceholderVnode,
} as const
