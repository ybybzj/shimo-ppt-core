/* eslint-disable no-redeclare */
import { Dict } from '../../../../liber/pervasive'

export type Key = string | number

export type Attrs = Dict<string | number | boolean>

export type Dataset = Dict<string>

export type Style = Dict<string>

export type ElmNode = Text | Element | Comment

export interface VNodeData {
  attrs?: Attrs
  style?: Style
  dataset?: Dataset
  key?: Key
  ns?: string // for SVGs
}

export interface VNode<Elm extends ElmNode = ElmNode> {
  kind: '__$VNode$__'
  sel: string | undefined
  data: VNodeData | undefined
  children: Array<VNode | string> | undefined
  text: string | undefined
  elm: Elm | undefined
  key: Key | undefined
}
export function vnode<Elm extends ElmNode = ElmNode>(
  sel: string | undefined,
  data: VNodeData | undefined,
  children: Array<VNode | string> | undefined,
  text: string | undefined,
  elm: Elm | undefined
): VNode {
  const key = data === undefined ? undefined : data.key
  return { kind: '__$VNode$__', sel, data, children, text, elm, key }
}

export function isVNode(vnode: any): vnode is VNode {
  return !!vnode && vnode.kind === '__$VNode$__'
}

export function isTextVNode(vnode: VNode): boolean {
  return isVNode(vnode) && vnode.sel == null
}

export type VNodes = VNode[]
export type VNodeChildElement = VNode | string | number | undefined | null
export type ArrayOrElement<T> = T | T[]
export type VNodeChildren = ArrayOrElement<VNodeChildElement>

function addNS(
  data: VNodeData,
  children: VNodes | undefined,
  sel: string | undefined
): void {
  data.ns = 'http://www.w3.org/2000/svg'
  if (sel !== 'foreignObject' && children != null) {
    for (let i = 0; i < children.length; ++i) {
      const childData = children[i].data
      if (childData != null) {
        addNS(
          childData,
          (children[i] as VNode).children as VNodes,
          children[i].sel
        )
      }
    }
  }
}

export function h(tag: string): VNode
export function h(tag: string, data: VNodeData | null): VNode
export function h(tag: string, children: VNodeChildren): VNode
export function h(
  tag: string,
  data: VNodeData | null,
  children: VNodeChildren
): VNode
export function h(tag: any, b?: any, c?: any): VNode {
  let data: VNodeData = {}
  let children: any
  let text: any
  let i: number
  if (c != null) {
    if (b != null) {
      data = b
    }
    if (Array.isArray(c)) {
      children = c
    } else if (typeof c === 'string') {
      text = c
    } else if (isVNode(c)) {
      children = [c]
    }
  } else if (b != null) {
    if (Array.isArray(b)) {
      children = b
    } else if (typeof b === 'string') {
      text = b
    } else if (isVNode(b)) {
      children = [b]
    } else {
      data = b
    }
  }

  if (children != null) {
    for (i = 0; i < children.length; ++i) {
      if (typeof children[i] === 'string') {
        children[i] = vnode(
          undefined,
          undefined,
          undefined,
          children[i],
          undefined
        )
      }
    }
  }

  if (
    tag[0] === 's' &&
    tag[1] === 'v' &&
    tag[2] === 'g' &&
    (tag.length === 3 || tag[3] === '.' || tag[3] === '#')
  ) {
    addNS(data, children, tag)
  }
  return vnode(tag, data, children, text, undefined)
}

export type VNodeCreator<Elm extends ElmNode> = {
  (): VNode<Elm>
  (sel: string, data: VNodeData | null, children?: VNodeChildren): VNode<Elm>
}

export function makeCreator<Elm extends ElmNode = ElmNode>(
  creatorName: string
): VNodeCreator<Elm> {
  return (...args: any[]) => {
    if (args.length <= 0) {
      return h(creatorName) as VNode<Elm>
    } else {
      const [sel, data, children] = args
      return h(creatorName + sel, data, children) as VNode<Elm>
    }
  }
}
