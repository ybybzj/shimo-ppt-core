import { VNode, isVNode, ElmNode } from './vnode'
import { Dict } from '../../../../liber/pervasive'
import { parseSel, applyAttrs, applyDataSet, applyStyles } from './utils'
import {
  createElementNS,
  createElement,
  appendChild,
  createTextNode,
  createComment,
  appendChildren,
  removeAllChildren,
  appendWith,
} from '../utils'
export * from './creator'

export type ElementRefs = Dict<Node>

export function toElement<Elm extends ElmNode = ElmNode>(
  vnode: VNode<Elm> | string,
  elmRefs?: ElementRefs,
  placeholders?: ElementRefs
): Elm {
  if (typeof vnode === 'string') {
    return createTextNode(vnode) as Elm
  }

  const children = vnode.children
  const sel = parseSel(vnode.sel)
  switch (sel.type) {
    case 'tag': {
      const data = vnode.data
      const tag = sel.tag!
      const elm = (
        data && data.ns ? createElementNS(data.ns, tag) : createElement(tag)
      ) as Elm
      vnode.elm = elm
      if (sel.id) {
        ;(elm as Element).setAttribute('id', sel.id)
        if (elmRefs) {
          elmRefs[sel.id] = elm
        }
      }

      if (sel.class) {
        ;(elm as Element).setAttribute('class', sel.class)
      }

      applyAttrs(vnode)
      applyDataSet(vnode, placeholders)
      applyStyles(vnode)

      if (Array.isArray(children)) {
        for (const child of children) {
          if (child != null) {
            if (Array.isArray(child)) {
              appendChildren(elm, toElements(child, elmRefs, placeholders))
            } else {
              const childElm = isVNode(child)
                ? toElement(child, elmRefs, placeholders)
                : createTextNode('' + child)
              appendChild(elm, childElm)
            }
          }
        }
      }

      break
    }
    case 'comment': {
      vnode.elm = createComment(sel.text ?? '') as Elm
      break
    }
    case 'text': {
      vnode.elm = createTextNode(vnode.text ?? '') as Elm
    }
  }

  return vnode.elm!
}

export function toElements(
  vnodes: Array<VNode | string>,
  elmRefs?: ElementRefs,
  placeholders?: ElementRefs
): Node[] {
  return vnodes.map((vnode) => toElement(vnode, elmRefs, placeholders))
}

export function fillElement<Elm extends Element>(
  parent: Elm,
  children: VNode[],
  elmRefs?: ElementRefs,
  placeholders?: ElementRefs
): Elm {
  const nodes = toElements(children, elmRefs, placeholders)
  removeAllChildren(parent)
  appendWith(parent, ...nodes)
  return parent
}
