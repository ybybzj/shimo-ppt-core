import { Dict } from '../../../../liber/pervasive'
import { h, VNode } from './vnode'

export type SelParseResult =
  | SelParseResultTag
  | SelParseResultComment
  | SelParseResultText

export interface SelParseResultTag {
  type: 'tag'
  tag: string
  id?: string
  class?: string
}

export interface SelParseResultComment {
  type: 'comment'
  text: string
}

export interface SelParseResultText {
  type: 'text'
}
export function parseSel(sel: string | undefined): SelParseResult {
  if (sel?.charAt[0] === '!') {
    return {
      type: 'comment',
      text: sel!.slice(1),
    }
  } else if (sel != null) {
    sel = sel.replace(/ /g, '')
    const hashIdx = sel.indexOf('#')
    const dotIdx = sel.indexOf('.', hashIdx)
    const hash = hashIdx > 0 ? hashIdx : sel.length
    const dot = dotIdx > 0 ? dotIdx : sel.length
    const tag =
      hashIdx !== -1 || dotIdx !== -1 ? sel.slice(0, Math.min(hash, dot)) : sel
    const result: SelParseResult = {
      type: 'tag',
      tag: tag !== '' ? tag : 'div',
    }
    if (hash < dot) {
      result.id = sel.slice(hash + 1, dot)
    }

    if (dotIdx > 0) {
      result.class = sel.slice(dot + 1).replace(/\./g, ' ')
    }
    return result
  } else {
    return { type: 'text' }
  }
}

const xlinkNS = 'http://www.w3.org/1999/xlink'
const xmlNS = 'http://www.w3.org/XML/1998/namespace'
const colonChar = 58
const xChar = 120

export function applyAttrs(vnode: VNode): void {
  const elm: Element = vnode.elm as Element
  if (!elm) {
    return
  }
  const attrs = vnode.data?.attrs ?? {}
  Object.keys(attrs).forEach((key) => {
    const attrVal = attrs[key]
    if (attrVal === true) {
      elm.setAttribute(key, '')
    } else if (attrVal === false) {
      elm.removeAttribute(key)
    } else {
      if (key.charCodeAt(0) !== xChar) {
        elm.setAttribute(key, attrVal as any)
      } else if (key.charCodeAt(3) === colonChar) {
        // Assume xml namespace
        elm.setAttributeNS(xmlNS, key, attrVal as any)
      } else if (key.charCodeAt(5) === colonChar) {
        // Assume xlink namespace
        elm.setAttributeNS(xlinkNS, key, attrVal as any)
      } else {
        elm.setAttribute(key, attrVal as any)
      }
    }
  })
}

const CAPS_REGEX = /[A-Z]/g
const PLACEHOLDER_KEY = `__$placeholder$__`
export function applyDataSet(vnode: VNode, placeholders?: Dict<Node>): void {
  const elm: HTMLElement = vnode.elm as HTMLElement
  if (!elm) {
    return
  }

  const dataset = vnode.data?.dataset
  if (dataset) {
    const d = elm.dataset
    Object.keys(dataset).forEach((key) => {
      if (key === PLACEHOLDER_KEY) {
        // collect placeholder elms
        if (placeholders) {
          placeholders[dataset[key]] = elm
        }
        return
      }
      if (d) {
        d[key] = dataset[key]
      } else {
        elm.setAttribute(
          'data-' + key.replace(CAPS_REGEX, '-$&').toLowerCase(),
          dataset[key]
        )
      }
    })
  }
}

export function createPlaceholderVnode(name: string): VNode<HTMLDivElement> {
  return h('div', {
    dataset: { ['__$placeholder$__']: name },
    style: {
      ['display']: 'none',
    },
  }) as VNode<HTMLDivElement>
}

export function applyStyles(vnode: VNode): void {
  const elm: HTMLElement = vnode.elm as HTMLElement

  if (!elm) {
    return
  }

  const style = vnode.data?.style

  if (style) {
    Object.keys(style).forEach((key) => {
      elm.style[key] = style[key]
    })
  }
}
