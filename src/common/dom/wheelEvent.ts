export const wheelEventName =
  'onwheel' in document.createElement('div')
    ? 'wheel' // Modern Browsers
    : (document as any).onmousewheel !== undefined
    ? 'mousewheel' // Webkit and IE
    : 'DOMMouseScroll' // old FireFOx

type converter = (org: any) => WheelEventArg

type checker = (evt: any) => boolean

type detecter = [number, checker, converter, string]

function cloneDetect(detecter: detecter): detecter {
  return [detecter[0], detecter[1], detecter[2], detecter[3]]
}

interface DetectResult {
  detected?: boolean
  converter: converter
  type?: string
}

function makeDetectFn(detecters: detecter[]): (org: any) => DetectResult {
  const result: DetectResult = {
    converter: (e) => e,
  }

  detecters = detecters.map(cloneDetect)
  return (orgEvt) => {
    if (result.detected != null) {
      return result
    }

    let finished = false
    let i = 0
    const l = detecters.length
    for (; i < l; i++) {
      const detecter = detecters[i]
      // decrease checkCount by 1 for each detecter
      detecter[0] -= 1
      const [count, checker, converter, type] = detecter
      const checkResult = checker(orgEvt)
      if (checkResult === true) {
        if (count <= 0) {
          result.detected = true
          result.converter = converter
          result.type = type
          finished = true
        }
        break
      } else if (i === l - 1) {
        finished = true
      }
    }

    if (finished === true && result.detected == null) {
      result.detected = false
    }

    return result
  }
}

export interface WheelEventArg {
  originEvent: any
  target: HTMLElement
  type: 'wheel'
  deltaMode: 0
  deltaX: number
  deltaY: number
  deltaZ: 0
  timeStamp: number
  preventDefault: () => void
}

function getWheelEvent(
  converter: (evt: WheelEventArg, org: any) => WheelEventArg
): converter {
  return (originEvent) => {
    const event: WheelEventArg = {
      originEvent: originEvent,
      target: (originEvent.target || originEvent.srcElement) as HTMLElement,
      type: 'wheel',
      deltaMode: 0,
      deltaX: 0,
      deltaY: 0,
      deltaZ: 0,
      timeStamp: originEvent.timeStamp || new Date().getTime(),
      preventDefault: () => {
        // eslint-disable-next-line no-unused-expressions
        originEvent.preventDefault
          ? originEvent.preventDefault()
          : (originEvent.returnValue = false)
      },
    }
    return converter(event, originEvent)
  }
}

const mousewheelAndSmoothScroll: detecter = [
  2,
  (e) => !!e.wheelDelta && Math.abs(e.wheelDelta) % 40 !== 0,
  getWheelEvent((evt, org) => {
    const wheelDelta =
      org.wheelDeltaY != null ? org.wheelDeltaY : org.wheelDelta
    const adjustWheelDelta = (delta) => {
      const wheel = 10
      const absDelta = Math.abs(delta)
      if (absDelta > 0 && absDelta <= wheel) {
        delta = delta > 0 ? -1 : 1
      } else {
        delta = -1 * (delta / wheel)
      }

      return delta
    }
    evt.deltaY = adjustWheelDelta(wheelDelta)
    if (org.wheelDeltaX != null) {
      evt.deltaX = adjustWheelDelta(org.wheelDeltaX)
    }
    return evt
  }),
  'mousewheelAndSmoothScroll',
]

const mousewheelAndLineScroll: detecter = [
  2,
  (e) => !!e.wheelDelta && Math.abs(e.wheelDelta) % 40 === 0,
  getWheelEvent((evt, org) => {
    const mousewheelAndLineScrollRate = 3
    const wheelDelta =
      org.wheelDeltaY != null ? org.wheelDeltaY : org.wheelDelta
    evt.deltaY = mousewheelAndLineScrollRate * (-1 / 20) * wheelDelta
    if (org.wheelDeltaX != null) {
      evt.deltaX = mousewheelAndLineScrollRate * (-1 / 20) * org.wheelDeltaX
    }
    return evt
  }),
  'mousewheelAndLineScroll',
]

const wheelAnd3LScroll: detecter = [
  1,
  (e) =>
    e.deltaMode === 1 &&
    (Math.abs(e.deltaY) % 3 === 0 || Math.abs(e.deltaX) % 3 === 0),
  getWheelEvent((evt, org) => {
    const wheelAnd3LScrollRate = 3
    if (org.deltaX) evt.deltaX = ((wheelAnd3LScrollRate * org.deltaX) / 3) * 10
    if (org.deltaY) evt.deltaY = ((wheelAnd3LScrollRate * org.deltaY) / 3) * 10

    return evt
  }),
  'wheelAnd3LScroll',
]

const wheelAndLineScroll: detecter = [
  1,
  (e) => e.deltaMode === 1,
  getWheelEvent((evt, org) => {
    const wheelAndLineScrollRate = 3
    if (org.deltaX) evt.deltaX = wheelAndLineScrollRate * org.deltaX * 10
    if (org.deltaY) evt.deltaY = wheelAndLineScrollRate * org.deltaY * 10

    return evt
  }),
  'wheelAndLineScroll',
]

const wheelAndSmoothScroll: detecter = [
  1,
  (e) => !e.wheelDelta && e.deltaMode === 0,
  getWheelEvent((evt, org) => {
    if (org.deltaX) evt.deltaX = org.deltaX
    if (org.deltaY) evt.deltaY = org.deltaY

    return evt
  }),
  'wheelAndSmoothScroll',
]

const makeDetector = () =>
  makeDetectFn([
    mousewheelAndSmoothScroll,
    mousewheelAndLineScroll,
    wheelAnd3LScroll,
    wheelAndLineScroll,
    wheelAndSmoothScroll,
  ])

let detect = makeDetector()

let timer: any = null

let result

export function handleWheelEvt(
  orgEvt: any,
  handler: (e: { eventObj: WheelEventArg; origin: WheelEvent }) => void
): void {
  if (timer != null) {
    clearTimeout(timer!)
  }

  if (result == null || result.detected == null) {
    timer = setTimeout(() => {
      detect = makeDetector()
      timer = null
    }, 500)
    result = detect(orgEvt)
  }

  if (result.detected != null) {
    handler({ eventObj: result.converter(orgEvt), origin: orgEvt })
  }
}

export interface DomWheelEvt {
  eventObj: WheelEventArg
  origin: WheelEvent
}

export function onDomWheelEvent(
  elm: HTMLElement,
  cb: (e: DomWheelEvt) => void
) {
  elm[`on${wheelEventName}`] = (e: WheelEvent) => {
    handleWheelEvt(e, cb)
  }
}

export function removeDomEvent_Wheel(elm: HTMLElement) {
  elm[`on${wheelEventName}`] = null
}
