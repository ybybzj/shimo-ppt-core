// export const isSupportOffscreenCanvas = false

import { BrowserInfo } from '../browserInfo'
export const isSupportOffscreenCanvas =
  BrowserInfo.isSafariInMacOs === false &&
  typeof document.createElement('canvas').transferControlToOffscreen ===
    'function'
// export const isSupportOffscreenCanvas = false

const offscreenCanvasMap = new WeakMap<HTMLCanvasElement, OffscreenCanvas>()
const mirrorCanvasMap = new WeakMap<HTMLCanvasElement, HTMLCanvasElement>()

export function getOffscreenCanvas(canvas: HTMLCanvasElement) {
  if (isSupportOffscreenCanvas) {
    let offCanvas = offscreenCanvasMap.get(canvas)
    if (offCanvas == null) {
      offCanvas = canvas.transferControlToOffscreen()
      offscreenCanvasMap.set(canvas, offCanvas)
    }
    return offCanvas
  } else {
    let mirrorCanvas = mirrorCanvasMap.get(canvas)
    if (mirrorCanvas == null) {
      mirrorCanvas = document.createElement('canvas')
      mirrorCanvas.width = canvas.width
      mirrorCanvas.height = canvas.height
      mirrorCanvasMap.set(canvas, mirrorCanvas)
    }
    return mirrorCanvas
  }
}

export function updateWidtAndHeightForCanvasWithMirror(
  canvas: HTMLCanvasElement,
  width: number,
  height: number
) {
  const mirrorCanvas = isSupportOffscreenCanvas
    ? offscreenCanvasMap.get(canvas)
    : mirrorCanvasMap.get(canvas)
  if (isSupportOffscreenCanvas && mirrorCanvas != null) {
    mirrorCanvas.width = width
    mirrorCanvas.height = height
    return
  }

  canvas.width = width
  canvas.height = height

  if (mirrorCanvas) {
    mirrorCanvas.width = width
    mirrorCanvas.height = height
  }
}
