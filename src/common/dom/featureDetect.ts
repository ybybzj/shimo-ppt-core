let img: undefined | HTMLImageElement = new Image()
export const isSupportImageDecode = typeof img?.decode === 'function'
img = undefined

// export const isSupportFetch = typeof window['fetch'] === 'function'

export const isSupportImageBitmap =
  typeof window['createImageBitmap'] === 'function' &&
  typeof window['ImageBitmap'] === 'function'
