import { find } from '../../../liber/l/find'
import { findIndex } from '../../../liber/l/findIndex'
import { Nullable, valuesOfDict } from '../../../liber/pervasive'
import { isNumber } from '../utils'

export const MULTI_TOUCH_EVT_TYPE = {
  None: 0,
  PinchZoomIn: 1,
  PinchZoomOut: 2,
  Panning: 3,
} as const

interface TouchEvtInfo {
  id: number
  x: number
  y: number
}

function toTouchEvtInfo(e: PointerEvent): Nullable<TouchEvtInfo> {
  let x: number | undefined
  let y: number | undefined
  if (e.pageX || e.pageY) {
    x = e.pageX
    y = e.pageY
  } else if (e.clientX || e.clientY) {
    x = e.clientX
    y = e.clientY
  }

  return isNumber(x) && isNumber(y)
    ? {
        id: e.pointerId,
        x,
        y,
      }
    : undefined
}

function getDist(p1: { x: number; y: number }, p2: { x: number; y: number }) {
  const dx = p1.x - p2.x
  const dy = p1.y - p2.y
  return Math.sqrt(dx * dx + dy * dy)
}

function getCenterPos(
  p1: { x: number; y: number },
  p2: { x: number; y: number }
): { x: number; y: number } {
  const cx = Math.min(p1.x, p2.x) + Math.abs((p1.x - p2.x) / 2)
  const cy = Math.min(p1.y, p2.y) + Math.abs((p1.y - p2.y) / 2)
  return {
    x: cx,
    y: cy,
  }
}
export type TOUCH_MOVE_EVT_INFO =
  | {
      type:
        | typeof MULTI_TOUCH_EVT_TYPE.PinchZoomIn
        | typeof MULTI_TOUCH_EVT_TYPE.PinchZoomOut
        | typeof MULTI_TOUCH_EVT_TYPE.None
    }
  | { type: typeof MULTI_TOUCH_EVT_TYPE.Panning; dx: number; dy: number }

export class MultiTouchEvtHandler {
  private evCache: TouchEvtInfo[] = []
  private lastDistOfPointers = -1
  private lastCenterOfPointers: { x: number; y: number } = { x: 0, y: 0 }
  type: valuesOfDict<typeof MULTI_TOUCH_EVT_TYPE> = MULTI_TOUCH_EVT_TYPE.None
  private isInMultiTouchMode = false

  onDown(e: PointerEvent): boolean {
    if (e.pointerType !== 'touch') {
      return false
    }
    const count = this.addToCache(e)
    if (count < 2) {
      return false
    }

    this.isInMultiTouchMode = true
    this.type = MULTI_TOUCH_EVT_TYPE.None
    const [p0, p1] = this.evCache
    this.lastDistOfPointers = getDist(p0, p1)
    this.lastCenterOfPointers = getCenterPos(p0, p1)
    return true
  }

  onMove(e: PointerEvent):
    | { handled: false }
    | {
        handled: true
        evtInfo: TOUCH_MOVE_EVT_INFO
      } {
    if (e.pointerType !== 'touch') {
      return { handled: false }
    }

    this.updateCache(e)

    if (this.evCache.length < 2) {
      return this.isInMultiTouchMode
        ? { handled: true, evtInfo: { type: MULTI_TOUCH_EVT_TYPE.None } }
        : { handled: false }
    }

    const eps = 5

    const [p0, p1] = this.evCache
    const pointerDist = getDist(p0, p1)
    const diff = pointerDist - this.lastDistOfPointers
    const isTriggerPinch = Math.abs(diff) > 30
    const { x: cx, y: cy } = getCenterPos(p0, p1)

    const { x: lastX, y: lastY } = this.lastCenterOfPointers

    const dx = lastX - cx
    const dy = lastY - cy
    const isScrollX = Math.abs(dx) > eps
    const isScrollY = Math.abs(dy) > eps
    const isTriggerPanning = isScrollX || isScrollY

    if (isTriggerPinch) {
      this.lastDistOfPointers = pointerDist
    }

    if (isTriggerPanning) {
      this.lastCenterOfPointers = { x: cx, y: cy }
    }

    if (isTriggerPinch) {
      if (diff > 0) {
        this.type = MULTI_TOUCH_EVT_TYPE.PinchZoomIn
        return {
          handled: true,
          evtInfo: { type: MULTI_TOUCH_EVT_TYPE.PinchZoomIn },
        }
      } else {
        this.type = MULTI_TOUCH_EVT_TYPE.PinchZoomOut
        return {
          handled: true,
          evtInfo: { type: MULTI_TOUCH_EVT_TYPE.PinchZoomOut },
        }
      }
    } else if (isTriggerPanning) {
      this.type = MULTI_TOUCH_EVT_TYPE.Panning
      return {
        handled: true,
        evtInfo: {
          type: MULTI_TOUCH_EVT_TYPE.Panning,
          dx: isScrollX ? lastX - cx : 0,
          dy: isScrollY ? lastY - cy : 0,
        },
      }
    } else {
      this.type = MULTI_TOUCH_EVT_TYPE.None
      return {
        handled: true,
        evtInfo: {
          type: MULTI_TOUCH_EVT_TYPE.None,
        },
      }
    }
  }

  onUp(e: PointerEvent): boolean {
    if (e.pointerType !== 'touch') {
      return false
    }
    const isInMultiTouchMode = this.isInMultiTouchMode

    this.type = MULTI_TOUCH_EVT_TYPE.None

    const count = this.removeFromCache(e)

    // until no finger is on the device, then stop handle multi touch
    if (count <= 0) {
      this.isInMultiTouchMode = false
    }

    return isInMultiTouchMode
  }

  private addToCache(e: PointerEvent) {
    const id = e.pointerId
    if (id == null) return this.evCache.length

    const index = findIndex((ev) => ev.id === id, this.evCache)
    const newEv = toTouchEvtInfo(e)

    if (newEv != null) {
      if (index <= -1) {
        this.evCache.push(newEv)
      } else {
        const ev = this.evCache[index]
        ev.x = newEv.x
        ev.y = newEv.y
      }
    }

    return this.evCache.length
  }

  private updateCache(e: PointerEvent): boolean {
    const id = e.pointerId
    if (id == null) return false
    const ev = find((ev) => ev.id === id, this.evCache)
    const newEv = toTouchEvtInfo(e)
    if (newEv != null && ev != null) {
      ev.x = newEv.x
      ev.y = newEv.y
      return true
    } else {
      return false
    }
  }

  private removeFromCache(e: PointerEvent) {
    const id = e.pointerId
    if (id == null) return this.evCache.length
    const index = findIndex((ev) => ev.id === id, this.evCache)
    if (index > -1) {
      this.evCache.splice(index, 1)
    }

    return this.evCache.length
  }
}
