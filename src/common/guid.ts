const getRandomValues =
  typeof Uint8Array !== 'undefined' &&
  ((typeof crypto !== 'undefined' &&
    crypto['getRandomValues'] &&
    crypto['getRandomValues'].bind(crypto)) ||
    (typeof window !== 'undefined' && typeof window['msCrypto'] !== 'undefined' &&
      typeof window['msCrypto']['getRandomValues'] === 'function' &&
      window['msCrypto']['getRandomValues'].bind(window['msCrypto'])))

// for crypto implementation
const hexCharMap: string[] = []

for (let i = 0; i < 256; ++i) {
  hexCharMap.push((i + 0x100).toString(16).substr(1))
}
function bufToUuid(buf: Uint8Array, needHyphen: boolean) {
  // Note: Be careful editing this code!  It's been tuned for performance
  // and works in ways you may not expect. See https://github.com/uuidjs/uuid/pull/434
  return (
    hexCharMap[buf[0]] +
    hexCharMap[buf[1]] +
    hexCharMap[buf[2]] +
    hexCharMap[buf[3]] +
    (needHyphen ? '-' : '') +
    hexCharMap[buf[4]] +
    hexCharMap[buf[5]] +
    (needHyphen ? '-' : '') +
    hexCharMap[buf[6]] +
    hexCharMap[buf[7]] +
    (needHyphen ? '-' : '') +
    hexCharMap[buf[8]] +
    hexCharMap[buf[9]] +
    (needHyphen ? '-' : '') +
    hexCharMap[buf[10]] +
    hexCharMap[buf[11]] +
    hexCharMap[buf[12]] +
    hexCharMap[buf[13]] +
    hexCharMap[buf[14]] +
    hexCharMap[buf[15]]
  ).toUpperCase()
}

// for Math.random implementation
function S4(): string {
  let ret = (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1)
  ret = ret.toUpperCase()
  return ret
}
export function GUID(needHyphen = true) {
  if (typeof getRandomValues === 'function') {
    const rnds8 = new Uint8Array(16)
    const randomValBuf = getRandomValues(rnds8)
    return bufToUuid(randomValBuf, needHyphen)
  } else {
    return (
      S4() +
      S4() +
      (needHyphen ? '-' : '') +
      S4() +
      (needHyphen ? '-' : '') +
      S4() +
      (needHyphen ? '-' : '') +
      S4() +
      (needHyphen ? '-' : '') +
      S4() +
      S4() +
      S4()
    )
  }
}

export function randomStr(n: number): string {
  let str = ''
  if (typeof getRandomValues === 'function') {
    const len = Math.ceil(n / 2)
    const rnds8 = new Uint8Array(len)
    const randomValBuf = getRandomValues(rnds8)
    for (const buf of randomValBuf) {
      str += hexCharMap[buf]
    }

    return str.substr(0, n).toLocaleLowerCase()
  } else {
    const len = Math.ceil(n / 4)
    for (let i = 0; i < len; i++) {
      str += S4()
    }
  }
  return str.substr(0, n).toLocaleLowerCase()
}
