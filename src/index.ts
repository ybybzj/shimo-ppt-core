import { initGlobals } from './globals/initGlobals'

initGlobals()
import * as IOType from './io/exports'
export * from './exports'

export { IOType }
