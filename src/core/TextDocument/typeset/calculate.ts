import { isParentOfTextDocChanged } from '../../calculation/updateCalcStatus/textContent'
import { ParaCalcStatus, ParaCalcStatusValues } from '../../common/const/doc'
import { calculateParagraphByColumn } from '../../Paragraph/typeset/calculate'
import { TextDocument } from '../TextDocument'

export function calculateTextDocumentByBounds(
  textDocument: TextDocument,
  x: number,
  y: number,
  xLimit: number,
  yLimit: number,
  infoOfClip?: { x0: number; x1: number },
  widthLimitInSp?: number
) {
  const forceCalc = isParentOfTextDocChanged(textDocument)
  let hasCalced = !forceCalc && textDocument.hasCalculated === true
  if (hasCalced) {
    hasCalced = textDocument.renderingState.isSameDimension(
      x,
      y,
      xLimit,
      yLimit,
      infoOfClip
    )
  }
  if (hasCalced) return

  resetRenderingState(textDocument, x, y, xLimit, yLimit)

  if (infoOfClip != null) {
    textDocument.renderingState.setInfoForClip(infoOfClip.x0, infoOfClip.x1)
  }

  const count = textDocument.children.length

  let paraY = y

  for (let i = 0; i < count; i++) {
    const para = textDocument.children.at(i)!
    let needRecalc = forceCalc || para.hasCalculated === false

    para.setIndexInDocument(i)
    if (needRecalc === false) {
      needRecalc = !para.renderingState.isSameDimension(
        x,
        paraY,
        xLimit,
        yLimit,
        0,
        widthLimitInSp
      )
    }
    if (needRecalc) {
      para.renderingState.resetDimension(
        x,
        paraY,
        xLimit,
        yLimit,
        0,
        widthLimitInSp
      )

      calculateParagraphByColumn(para, 0)
    }

    paraY = para.getDocumentBounds(0).bottom
  }

  textDocument.renderingState.bounds.left = x
  textDocument.renderingState.bounds.top = y
  textDocument.renderingState.bounds.right = xLimit
  textDocument.renderingState.bounds.bottom = paraY

  textDocument.renderingState.endIndex = count - 1
  textDocument.hasCalculated = true
}

export function calculateTextDocumentBySize(
  textDocument: TextDocument,
  width: number,
  height: number,
  widthLimitInSp?: number
) {
  if (textDocument.getColumnsCount() === 1) {
    calculateTextDocumentByBounds(
      textDocument,
      0,
      0,
      width,
      20000,
      undefined,
      widthLimitInSp
    )
  } else {
    calculateTextDocument(textDocument, width, height)

    // 对于超出指定height的情况，需要修正最小高度重新计算，收敛得到合适的高度
    let totalHeight = textDocument.getWholeHeight()

    if (totalHeight <= height) {
      return
    }
    let low = height
    let high = totalHeight
    while (high - low > 0.1) {
      const checkHieght = low + (high - low) / 2
      calculateTextDocument(textDocument, width, checkHieght)

      totalHeight = textDocument.getWholeHeight()
      if (totalHeight > checkHieght) {
        low = checkHieght
      } else {
        high = checkHieght
      }
    }
    if (height < high + 0.01) {
      calculateTextDocument(textDocument, width, high + 0.01)
    }
  }
}
interface DocumentCalculateState {
  columnIndex: number
  startIndex: number
}

const gDocCalculateState: DocumentCalculateState = {
  columnIndex: 0,
  startIndex: 0,
}

export function calculateTextDocument(
  textDocument: TextDocument,
  width,
  height
) {
  // reset calc state
  gDocCalculateState.columnIndex = 0
  gDocCalculateState.startIndex = 0

  const valueOfColumnsCount = textDocument.getColumnsCount()
  resetRenderingState(textDocument, 0, 0, width, height, valueOfColumnsCount)

  calculateContentColumn(textDocument, valueOfColumnsCount)
}

function calculateContentColumn(
  textDocument: TextDocument,
  valueOfColumnsCount: number
) {
  const forceCalc = isParentOfTextDocChanged(textDocument)

  const valueOfColumnIndex = gDocCalculateState.columnIndex
  const valueOfStartIndex = gDocCalculateState.startIndex

  const startIndex =
    textDocument.renderingState.getStartColumnDimensionInfo(valueOfColumnIndex)
  const x = startIndex.x
  let y = startIndex.y
  const xLimit = startIndex.xLimit
  const yLimit = startIndex.yLimit

  let valueOfCalcResult: ParaCalcStatusValues = ParaCalcStatus.NextColumn

  const dimension = textDocument.renderingState!
  const section = dimension?.section!
  const column = section.columns.at(valueOfColumnIndex)
  if (column == null) return

  column.x = x
  column.xLimit = xLimit
  column.y = y
  column.yLimit = yLimit
  column.index = valueOfStartIndex
  column.empty = false
  column.spaceBefore = startIndex.ColumnSpaceBefore
  column.spaceAfter = startIndex.ColumnSpaceAfter

  let i
  let isContinue = false
  const paragraphs = textDocument.children
  const valueOfCount = paragraphs.length
  for (i = valueOfStartIndex; i < valueOfCount; ++i) {
    const paragraph = paragraphs.at(i)!
    let needRecalc = forceCalc || paragraph.hasCalculated === false

    if ((0 === i && 0 === valueOfColumnIndex) || i !== valueOfStartIndex) {
      paragraph.setIndexInDocument(i)
      if (needRecalc === false) {
        needRecalc = !paragraph.renderingState.isSameDimension(
          x,
          y,
          xLimit,
          yLimit,
          valueOfColumnIndex
        )
      }
      if (needRecalc) {
        paragraph.renderingState.resetDimension(
          x,
          y,
          xLimit,
          yLimit,
          valueOfColumnIndex
        )
      }
    }
    const calcColIndex = textDocument.getRelativeColIndexForParaElem(
      i,
      valueOfColumnIndex
    )

    if (needRecalc) {
      valueOfCalcResult = calculateParagraphByColumn(paragraph, calcColIndex)
    } else {
      valueOfCalcResult =
        paragraph.renderingState.getCalcResultByColumn(calcColIndex) ??
        valueOfCalcResult
    }
    // if (valueOfCalcResult & ParaCalcStatus.NextElement) {
    y = paragraph.getDocumentBounds(calcColIndex).bottom
    // }
    column.bounds.bottom = y
    if (valueOfCalcResult & ParaCalcStatus.NextColumn) {
      column.endIndex = i
      section.endIndex = i
      dimension.endIndex = i

      isContinue = true
      paragraph.hasCalculated = false

      gDocCalculateState.startIndex = i

      if (column.endIndex === column.index) {
        const para = paragraphs.at(column.index)
        const colIndex = textDocument.getRelativeColIndexForParaElem(
          i,
          valueOfColumnIndex
        )
        if (
          para == null ||
          true === para.renderingState.isEmptyColumn(colIndex)
        ) {
          column.empty = true
        }
      }

      gDocCalculateState.columnIndex = valueOfColumnIndex + 1
      if (gDocCalculateState.columnIndex >= valueOfColumnsCount) {
        gDocCalculateState.columnIndex = 0
        isContinue = false
        paragraph.hasCalculated = true
      }
      break
    }
  }
  if (i === valueOfCount) {
    dimension.endIndex = valueOfCount - 1
    section.endIndex = valueOfCount - 1
    column.endIndex = valueOfCount - 1
  }
  if (isContinue) {
    calculateContentColumn(textDocument, valueOfColumnsCount)
  }

  textDocument.hasCalculated = true
}

function resetRenderingState(
  doc: TextDocument,
  x: number,
  y: number,
  xLimit: number,
  yLimit: number,
  valueOfCols?: number
) {
  doc.renderingState.reset(x, y, xLimit, yLimit, valueOfCols)

  if (0 === doc.cursorPosition.x && 0 === doc.cursorPosition.y) {
    doc.cursorPosition.x = x
    doc.cursorPosition.y = y

    doc.cursorPosition.cursorX = x
    doc.cursorPosition.cursorY = y
  }
}
