import { Nullable } from '../../../../liber/pervasive'
import { PoolAllocator, Recyclable } from '../../../common/allocator'
import { RecyclableArray } from '../../../common/managedArray'
import { isNumber } from '../../../common/utils'
import { InstanceType } from '../../instanceTypes'
import { checkAlmostEqual } from '../../common/utils'
import { TextDocBounds } from '../DocumentBounds'
import { TextDocument } from '../TextDocument'

export class TextDocRenderingState {
  width: number
  height: number
  margins: { left: number; right: number; top: number; bottom: number }
  bounds: TextDocBounds
  startParaIndex: number
  endIndex: number
  x: number
  y: number
  xLimit: number
  yLimit: number
  section: TextContentSection
  infoOfClip: Nullable<{ x0: number; x1: number }>
  private textDocument: TextDocument
  constructor(
    doc: TextDocument,
    x: number,
    y: number,
    xLimit: number,
    yLimit: number
  ) {
    this.textDocument = doc
    this.width = 0
    this.height = 0
    this.margins = {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
    }

    this.bounds = new TextDocBounds(0, 0, 0, 0)
    this.startParaIndex = 0
    this.endIndex = -1

    this.x = x
    this.y = y
    this.xLimit = xLimit
    this.yLimit = yLimit
    this.section = new TextContentSection()
    this.infoOfClip = null
  }

  reset(
    x: number,
    y: number,
    xLimit: number,
    yLimit: number,
    valueOfCols?: number
  ) {
    this.width = 0
    this.height = 0

    this.margins.left = 0
    this.margins.right = 0
    this.margins.top = 0
    this.margins.bottom = 0

    this.bounds.reset()
    this.startParaIndex = 0
    this.endIndex = -1

    this.x = x
    this.y = y
    this.xLimit = xLimit
    this.yLimit = yLimit
    this.section.reset(valueOfCols)
    this.infoOfClip = null
  }

  isSameDimension(
    x: number,
    y: number,
    xLimit: number,
    yLimit: number,
    infoOfClip?: { x0: number; x1: number },
    valueOfCols: number = 0
  ) {
    return (
      checkAlmostEqual(this.x, x, 0.01) &&
      checkAlmostEqual(this.y, y, 0.01) &&
      checkAlmostEqual(this.xLimit, xLimit, 0.01) &&
      checkAlmostEqual(this.yLimit, yLimit, 0.01) &&
      this.section.columns.length === valueOfCols &&
      ((this.infoOfClip == null && infoOfClip == null) ||
        (this.infoOfClip != null &&
          infoOfClip != null &&
          checkAlmostEqual(this.infoOfClip!.x0, infoOfClip!.x0, 0.01) &&
          checkAlmostEqual(this.infoOfClip!.x1, infoOfClip!.x1, 0.01)))
    )
  }

  translateBy(dx: number, dy: number) {
    this.x += dx
    this.xLimit += dx
    this.y += dy
    this.yLimit += dy

    this.bounds.translateBy(dx, dy)

    if (this.infoOfClip) {
      this.infoOfClip.x0 += dx
      this.infoOfClip.x1 += dx
    }
    const startIndex = this.startParaIndex
    const endIndex = this.endIndex
    for (let i = startIndex; i <= endIndex; i++) {
      const para = this.textDocument.children.at(i)
      para && para.renderingState.translateBy(0, dx, dy)
    }
  }

  setInfoForClip(x0: number, x1: number) {
    this.infoOfClip = { x0: x0, x1: x1 }
  }

  getDocumentBounds() {
    return this.bounds ?? new TextDocBounds(0, 0, 0, 0)
  }
  getStartColumnDimensionInfo(valueOfColumnIndex: number) {
    const textDocument = this.textDocument
    let x = this.x
    const y = this.y
    let xLimit = this.xLimit
    const yLimit = this.yLimit

    let colSpaceBefore = 0
    let colSpaceAfter = 0

    const valueOfNumCol = textDocument.getColumnsCount()

    const bodyPr =
      textDocument.parent.instanceType === InstanceType.TextBody &&
      textDocument.parent.getBodyPr()
    if (valueOfNumCol > 1 && bodyPr) {
      let space = isNumber(bodyPr.spcCol) ? bodyPr.spcCol : 0
      space = Math.min(space, this.xLimit / (valueOfNumCol - 1))
      const columnWidth = Math.max(
        (this.xLimit - this.x - (valueOfNumCol - 1) * space) / valueOfNumCol,
        0
      )
      x += valueOfColumnIndex * (columnWidth + space)
      xLimit = x + columnWidth
      if (valueOfColumnIndex > 0) {
        colSpaceBefore = space
      }
      if (valueOfColumnIndex < valueOfNumCol - 1) {
        colSpaceAfter = space
      }
    }

    return {
      x: x,
      y: y,
      xLimit: xLimit,
      yLimit: yLimit,
      ColumnSpaceBefore: colSpaceBefore,
      ColumnSpaceAfter: colSpaceAfter,
    }
  }
}

class TextContentColumn extends Recyclable<TextContentColumn> {
  bounds: TextDocBounds
  index: number
  endIndex: number
  empty: boolean
  x: number
  y: number
  xLimit: number
  yLimit: number
  spaceBefore: number
  spaceAfter: number
  private static allocator: PoolAllocator<TextContentColumn, []> =
    PoolAllocator.getAllocator<TextContentColumn, []>(TextContentColumn)
  static new() {
    return TextContentColumn.allocator.allocate()
  }
  static recycle(txtColumn: TextContentColumn) {
    TextContentColumn.allocator.recycle(txtColumn)
  }
  constructor() {
    super()
    this.bounds = new TextDocBounds(0, 0, 0, 0)
    this.index = 0
    this.endIndex = -1
    this.empty = true

    this.x = 0
    this.y = 0
    this.xLimit = 0
    this.yLimit = 0

    this.spaceBefore = 0
    this.spaceAfter = 0
  }

  translateBy(dx: number, dy: number) {
    this.x += dx
    this.xLimit += dx
    this.y += dy
    this.yLimit += dy

    this.bounds.translateBy(dx, dy)
  }

  reset() {
    this.bounds.reset()
    this.index = 0
    this.endIndex = -1
    this.empty = true

    this.x = 0
    this.y = 0
    this.xLimit = 0
    this.yLimit = 0

    this.spaceBefore = 0
    this.spaceAfter = 0
  }

  isEmpty() {
    return this.empty
  }
}

class TextContentSection {
  index: number
  endIndex: number
  y: number
  yLimit: number
  columns: RecyclableArray<TextContentColumn>
  constructor() {
    this.index = 0
    this.endIndex = -1

    this.y = 0
    this.yLimit = 0

    this.columns = new RecyclableArray(TextContentColumn.recycle)
  }

  reset(valueOfCols?: number) {
    this.index = 0
    this.endIndex = -1

    this.y = 0
    this.yLimit = 0

    this.resetColumns(valueOfCols ?? 0)
  }

  resetColumns(valueOfCols: number) {
    if (valueOfCols === 0) {
      this.columns.clear()
      return
    }

    let i = 0
    const orgCount = this.columns.length
    const l = Math.min(orgCount, valueOfCols)
    while (i < l) {
      this.columns.at(i)!.reset()
      i += 1
    }
    if (orgCount < valueOfCols) {
      while (i < valueOfCols) {
        this.columns.insert(i, TextContentColumn.new())
        i += 1
      }
    } else if (orgCount > valueOfCols) {
      while (i < orgCount) {
        TextContentColumn.recycle(this.columns.at(i)!)
        i += 1
      }
    }
  }

  isEmpty() {
    return this.columns.length <= 0
  }

  translateBy(Dx, Dy) {
    this.y += Dy
    this.yLimit += Dy

    for (let i = 0, l = this.columns.length; i < l; ++i) {
      this.columns.at(i)!.translateBy(Dx, Dy)
    }
  }

  getY() {
    return this.y
  }
}
