import { ManagedSliceUtil } from '../../../common/managedArray'
import { TextContentDrawContext } from '../../common/types'
import { renderParagraph } from '../../Paragraph/typeset/render'
import { mmsPerPt } from '../../utilities/helpers'
import { TextDocument } from '../TextDocument'

export function drawTextDocument(
  textDocument: TextDocument,
  drawContext: TextContentDrawContext
) {
  const renderingState = textDocument.renderingState
  if (renderingState != null) {
    const section = renderingState.section
    // draw columns
    if (section && !section.isEmpty()) {
      ManagedSliceUtil.forEach(section.columns, (Column, colIndex) => {
        const { index: elmIndex, endIndex } = Column
        for (let i = elmIndex; i <= endIndex; ++i) {
          const drawColIndex = textDocument.getRelativeColIndexForParaElem(
            i,
            colIndex
          )
          const para = textDocument.children.at(i)
          para && renderParagraph(para, drawColIndex, drawContext)
        }
      })
    } else {
      const info = textDocument.renderingState.infoOfClip
      if (info) {
        const correction = mmsPerPt(1)

        const bounds = renderingState.bounds
        drawContext.saveState()
        drawContext.addClipRect(
          info.x0,
          bounds.top - correction,
          Math.abs(info.x1 - info.x0),
          bounds.bottom - bounds.top + correction
        )
      }

      const paraStartPos = renderingState.startParaIndex
      const paraEndPos = renderingState.endIndex
      for (let i = paraStartPos; i <= paraEndPos; i++) {
        const para = textDocument.children.at(i)
        para && renderParagraph(para, 0, drawContext)
      }

      if (info) {
        drawContext.restoreState()
      }
    }
  }
}
