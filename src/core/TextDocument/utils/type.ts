import { RunContentElement } from '../../Paragraph/RunElement/type'
import { ParagraphItem } from '../../Paragraph/type'

export type ItemOfAddToParagraph = ParagraphItem | RunContentElement
