import { TextDocument } from '../TextDocument'
import { calculateTextDocumentByBounds } from '../typeset/calculate'

export function getMaxTextContentWidthByLimit(
  textDoc: TextDocument,
  widthLimit: number
): number {
  calculateTextDocumentByBounds(textDoc, 0, 0, widthLimit - 0.01, 20000)

  let maxWidth = 0

  const paras = textDoc.children
  for (let i = 0, l = paras.length; i < l; ++i) {
    const para = paras.at(i)!
    const maxSegWidth = para.renderingState.getMaxSegmentWidth()

    if (maxSegWidth > maxWidth) {
      maxWidth = maxSegWidth
    }
  }
  return maxWidth + 0.01
}
