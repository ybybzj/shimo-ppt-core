import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import { TableCell } from '../../Table/TableCell'
import { getPresentation } from '../../utilities/finders'
import { TextDocument } from '../TextDocument'

function getTextDocStartPosInfoInTable(cell: TableCell) {
  const table = cell.parent.parent
  const row = cell.parent
  const cellIndex = cell.index
  const cellMar = cell.getMargins()
  const cellInfo = row.getCellInfo(cellIndex)

  const presentation = getPresentation(table)
  const y: number = cellMar.top!.w
  const x = table.dimension.x

  const yLimit = presentation ? presentation.height : 0

  return {
    x: x + cellInfo.xOfTextDocStart,
    xLimit: x + cellInfo.xOfTextDocEnd,
    y: y,
    yLimit: yLimit,
  }
}

export function getTextDocStartPosInfo(
  textDoc: Nullable<TextDocument>,
  columnIndex: number
) {
  const parentType = textDoc?.parent.instanceType
  switch (parentType) {
    case InstanceType.TableCell: {
      const cell = textDoc!.parent
      return getTextDocStartPosInfoInTable(cell)
    }
    case InstanceType.TextBody: {
      return textDoc!.renderingState.getStartColumnDimensionInfo(columnIndex)
    }
    default: {
      return { x: 0, xLimit: 0, y: 0, yLimit: 0 }
    }
  }
}
