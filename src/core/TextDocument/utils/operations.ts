import { InstanceType } from '../../instanceTypes'
import { ParaPr } from '../../textAttributes/ParaPr'
import { TextPr, TextPrOptions } from '../../textAttributes/TextPr'
import { EditorSettings } from '../../common/EditorSettings'
import { isArabicElement } from '../../Paragraph/ParaContent/utils/typeset'
import { Paragraph } from '../../Paragraph/Paragraph'
import { RunContentElement } from '../../Paragraph/RunElement/type'
import { isCursorAtEndInParagraph } from '../../Paragraph/utils/cursor'
import { getAndUpdateCursorPositionForParagraph } from '../../utilities/cursor/cursorPosition'
import { moveCursorToStartPosInParagraph } from '../../utilities/cursor/moveToEdgePos'
import { TextDocument } from '../TextDocument'
import { JCAlignTypeValues } from '../../../io/dataType/style'
import { ParaSpacingOptions } from '../../textAttributes/ParaSpacing'
import { ParaIndOptions } from '../../textAttributes/ParaInd'

export function addNewParagraphInTextDocument(textDoc: TextDocument) {
  if (textDoc.cursorPosition.childIndex < 0) return

  if (true === textDoc.selection.isUse) {
    textDoc.remove(1, false, true)
  }

  const item = textDoc.children.at(textDoc.cursorPosition.childIndex)

  if (item) {
    const newParagraph = Paragraph.new(textDoc)

    if (true === isCursorAtEndInParagraph(item)) {
      item.applyPrTo(newParagraph)

      const lastRun = item.children.at(item.children.length - 1)
      if (
        lastRun &&
        lastRun.instanceType === InstanceType.ParaRun &&
        lastRun.pr.lang &&
        lastRun.pr.lang.val
      ) {
        newParagraph.selectAll()
        newParagraph.applyTextPr({ lang: lastRun.pr.lang.clone() })
        newParagraph.removeSelection()
      }
    } else {
      item.splitRestInto(newParagraph)
    }

    newParagraph.correctChildren()
    moveCursorToStartPosInParagraph(newParagraph)

    textDoc.insertParagraph(textDoc.cursorPosition.childIndex + 1, newParagraph)
    textDoc.cursorPosition.childIndex++
  }
}

export function applyTextPrToParagraphInTextDocument(
  textDoc: TextDocument,
  textPr: TextPrOptions,
  isDirty?: boolean
) {
  if (true === textDoc.isSetToAll) {
    for (let i = 0, l = textDoc.children.length; i < l; i++) {
      const item = textDoc.children.at(i)!
      item.isSetToAll = true
      item.applyTextPr(textPr)
      item.isSetToAll = false
    }
  } else if (true === textDoc.selection.isUse) {
    let startIndex = textDoc.selection.startIndex
    let endIndex = textDoc.selection.endIndex
    if (endIndex < startIndex) {
      const t = startIndex
      startIndex = endIndex
      endIndex = t
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const para = textDoc.children.at(i)
      para && para.applyTextPr(textPr)
    }

    if (false !== isDirty) {
      textDoc._calculateState()
    }
  } else {
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)

    if (para) {
      para.applyTextPr(textPr)

      if (false !== isDirty) {
        textDoc._calculateState()

        getAndUpdateCursorPositionForParagraph(para)
        para.cursorPosition.cursorX = para.cursorPosition.x
        para.cursorPosition.cursorY = para.cursorPosition.y
      }
    }
  }
}

export function addToParagraphInTextDocument(
  textDoc: TextDocument,
  paraItem: RunContentElement,
  isDirty?: boolean
) {
  if (true === textDoc.selection.isUse) {
    const instanceType = paraItem.instanceType
    switch (instanceType) {
      case InstanceType.ParaNewLine:
      case InstanceType.ParaText:
      case InstanceType.ParaSpace:
      case InstanceType.ParaTab: {
        textDoc.remove(1, false, true)

        break
      }
    }
  }

  const para = textDoc.children.at(textDoc.cursorPosition.childIndex)

  if (para == null) return

  if (EditorSettings.isRTL && isArabicElement(paraItem)) {
    const text = (para.getText() ?? '').trim()
    if (!text) {
      para.setParagraphTextDirection(true)
    }
  }
  para.add(paraItem)

  if (false !== isDirty) {
    textDoc._calculateState()

    getAndUpdateCursorPositionForParagraph(para)
    para.cursorPosition.cursorX = para.cursorPosition.x
    para.cursorPosition.cursorY = para.cursorPosition.y
  }
}

export function setParagraphTextDirectionForTextDocument(
  textDoc: TextDocument,
  isRTL: boolean
) {
  if (true === textDoc.isSetToAll) {
    for (let i = 0, l = textDoc.children.length; i < l; i++) {
      const item = textDoc.children.at(i)!
      item.isSetToAll = true
      item.setParagraphTextDirection(isRTL)
      item.isSetToAll = false
    }
    return
  }

  if (textDoc.cursorPosition.childIndex < 0) return

  if (true === textDoc.selection.isUse) {
    let startIndex = textDoc.selection.startIndex
    let endIndex = textDoc.selection.endIndex
    if (endIndex < startIndex) {
      const t = startIndex
      startIndex = endIndex
      endIndex = t
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const para = textDoc.children.at(i)
      para && para.setParagraphTextDirection(isRTL)
    }
  } else {
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
    para && para.setParagraphTextDirection(isRTL)
  }
}

export function setParagraphAlignForTextDocument(
  textDoc: TextDocument,
  align: undefined | JCAlignTypeValues
) {
  if (true === textDoc.isSetToAll) {
    for (let i = 0, l = textDoc.children.length; i < l; i++) {
      const para = textDoc.children.at(i)!
      para.isSetToAll = true
      para.setAlign(align)
      para.isSetToAll = false
    }

    return
  }

  if (textDoc.cursorPosition.childIndex < 0) return

  if (true === textDoc.selection.isUse) {
    let startIndex = textDoc.selection.startIndex
    let endIndex = textDoc.selection.endIndex
    if (endIndex < startIndex) {
      const t = startIndex
      startIndex = endIndex
      endIndex = t
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const para = textDoc.children.at(i)
      para && para.setAlign(align)
    }
  } else {
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
    para && para.setAlign(align)
  }
}

export function clearParagraphFormattingForTextDocument(
  textDoc: TextDocument,
  isFormatParaPr: boolean,
  isFormatTextPr: boolean
) {
  if (false !== isFormatParaPr) isFormatParaPr = true

  if (false !== isFormatTextPr) isFormatTextPr = true

  if (true === textDoc.isSetToAll) {
    for (let i = 0, l = textDoc.children.length; i < l; i++) {
      const para = textDoc.children.at(i)!
      para.isSetToAll = true
      para.clearFormatting(isFormatParaPr, isFormatTextPr)
      para.isSetToAll = false
    }

    return
  }

  if (true === textDoc.selection.isUse) {
    let startIndex = textDoc.selection.startIndex
    let endIndex = textDoc.selection.endIndex
    if (startIndex > endIndex) {
      const t = startIndex
      startIndex = endIndex
      endIndex = t
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const para = textDoc.children.at(i)
      para && para.clearFormatting(isFormatParaPr, isFormatTextPr)
    }
  } else {
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
    para && para.clearFormatting(isFormatParaPr, isFormatTextPr)
  }
}

export function setParagraphSpacingForTextDocument(
  textDoc: TextDocument,
  spacing: ParaSpacingOptions
) {
  if (true === textDoc.isSetToAll) {
    for (let i = 0, l = textDoc.children.length; i < l; i++) {
      const para = textDoc.children.at(i)!
      para.isSetToAll = true
      para.setSpacing(spacing)
      para.isSetToAll = false
    }

    return
  }

  if (textDoc.cursorPosition.childIndex < 0) return

  if (true === textDoc.selection.isUse) {
    let startIndex = textDoc.selection.startIndex
    let endIndex = textDoc.selection.endIndex
    if (endIndex < startIndex) {
      const t = startIndex
      startIndex = endIndex
      endIndex = t
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const para = textDoc.children.at(i)
      para && para.setSpacing(spacing)
    }
  } else {
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
    para && para.setSpacing(spacing)
  }
}

export function setParagraphNumberingForTextDocument(
  textDoc: TextDocument,
  bullet
) {
  if (true === textDoc.isSetToAll) {
    for (let i = 0, l = textDoc.children.length; i < l; i++) {
      textDoc.children.at(i)!.setNumbering(bullet)
    }
    return
  }

  if (textDoc.cursorPosition.childIndex < 0) return

  if (true === textDoc.selection.isUse) {
    let startIndex = textDoc.selection.startIndex
    let endIndex = textDoc.selection.endIndex
    if (endIndex < startIndex) {
      const t = startIndex
      startIndex = endIndex
      endIndex = t
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const para = textDoc.children.at(i)
      para && para.setNumbering(bullet)
    }
    textDoc._calculateState()
    return
  }
  const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
  para && para.setNumbering(bullet)
  textDoc._calculateState()
}

export default function setParagraphIndentForTextDocument(
  textDoc: TextDocument,
  ind: ParaIndOptions
) {
  if (true === textDoc.isSetToAll) {
    for (let i = 0, l = textDoc.children.length; i < l; i++) {
      const para = textDoc.children.at(i)!
      para.isSetToAll = true
      para.setIndent(ind)
      para.isSetToAll = false
    }

    return
  }

  if (textDoc.cursorPosition.childIndex < 0) return

  if (true === textDoc.selection.isUse) {
    let startIndex = textDoc.selection.startIndex
    let endIndex = textDoc.selection.endIndex
    if (endIndex < startIndex) {
      const t = startIndex
      startIndex = endIndex
      endIndex = t
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const para = textDoc.children.at(i)
      para && para.setIndent(ind)
    }
  } else {
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
    para && para.setIndent(ind)
  }
}

export function changeFontSizeForTextDocument(
  textDoc: TextDocument,
  isIncrease: boolean
) {
  if (true === textDoc.isSetToAll) {
    for (let i = 0, l = textDoc.children.length; i < l; i++) {
      const para = textDoc.children.at(i)!
      para.isSetToAll = true
      para.changeFontSize(isIncrease)
      para.isSetToAll = false
    }

    return
  }

  if (textDoc.cursorPosition.childIndex < 0) return

  if (true === textDoc.selection.isUse) {
    let startIndex = textDoc.selection.startIndex
    let endIndex = textDoc.selection.endIndex
    if (endIndex < startIndex) {
      const t = startIndex
      startIndex = endIndex
      endIndex = t
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const para = textDoc.children.at(i)
      para && para.changeFontSize(isIncrease)
    }
  } else {
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
    para && para.changeFontSize(isIncrease)
  }
}

export function changeParagraphIndentLevelForTextDocument(
  textDoc: TextDocument,
  isIncrease: boolean
) {
  if (!textDoc.canChangeParagraphIndentLevel(isIncrease)) return
  if (true === textDoc.isSetToAll) {
    for (let i = 0, l = textDoc.children.length; i < l; i++) {
      textDoc.children.at(i)!.changeIndentLevel(isIncrease)
    }
    return
  }

  if (textDoc.cursorPosition.childIndex < 0) return

  if (true === textDoc.selection.isUse) {
    let startIndex = textDoc.selection.startIndex
    let endIndex = textDoc.selection.endIndex
    if (endIndex < startIndex) {
      const t = startIndex
      startIndex = endIndex
      endIndex = t
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const para = textDoc.children.at(i)
      para && para.changeIndentLevel(isIncrease)
    }
    textDoc._calculateState()
    return
  }
  const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
  para && para.changeIndentLevel(isIncrease)
  textDoc._calculateState()
}

export function setParagraphDefaultTabSizeForTextDocument(
  textDoc: TextDocument,
  tabSize
) {
  if (true === textDoc.isSetToAll) {
    for (let i = 0, l = textDoc.children.length; i < l; i++) {
      const para = textDoc.children.at(i)!
      para.isSetToAll = true
      para.setDefaultTab(tabSize)
      para.isSetToAll = false
    }

    return
  }

  if (textDoc.cursorPosition.childIndex < 0) return

  if (true === textDoc.selection.isUse) {
    let startIndex = textDoc.selection.startIndex
    let endIndex = textDoc.selection.endIndex
    if (endIndex < startIndex) {
      const t = startIndex
      startIndex = endIndex
      endIndex = t
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const para = textDoc.children.at(i)
      para && para.setDefaultTab(tabSize)
    }
  } else {
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
    para && para.setDefaultTab(tabSize)
  }
}

export function pasteFormattingForTextDocument(
  textDoc: TextDocument,
  textPr: TextPr,
  paraPr: ParaPr
) {
  if (true === textDoc.isSetToAll) {
    for (let i = 0, l = textDoc.children.length; i < l; i++) {
      const para = textDoc.children.at(i)!
      para.isSetToAll = true
      para.pasteFormatting(textPr, paraPr)
      para.isSetToAll = false
    }

    return
  }

  if (true === textDoc.selection.isUse) {
    let start = textDoc.selection.startIndex
    let end = textDoc.selection.endIndex
    if (start > end) {
      start = textDoc.selection.endIndex
      end = textDoc.selection.startIndex
    }

    for (let i = start; i <= end; i++) {
      const para = textDoc.children.at(i)
      para && para.pasteFormatting(textPr, paraPr)
    }
  } else {
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
    para && para.pasteFormatting(textPr, paraPr)
  }
}

export type TextDocumentOperationFns = (
  textDoc: TextDocument,
  ...args: any[]
) => void
export type TextDocumentOperationArgs<T extends TextDocumentOperationFns> =
  T extends (textDoc: TextDocument, ...args: infer U) => void ? U : never
