import { Dict, Nullable } from '../../../liber/pervasive'

import { idGenerator } from '../../globals/IdGenerator'
import { ParaSpace } from '../Paragraph/RunElement/ParaSpace'
import { Paragraph } from '../Paragraph/Paragraph'
import { ParaPr } from '../textAttributes/ParaPr'

import { InstanceType } from '../instanceTypes'
import { Presentation } from '../Slide/Presentation'
import { ParaTab } from '../Paragraph/RunElement/ParaTab'
import { ParaNewLine } from '../Paragraph/RunElement/ParaNewLine'
import { ParaText } from '../Paragraph/RunElement/ParaText'

import { HoverInfo } from '../../ui/states/type'
import {
  updateParagraphProps,
  updateParagraphTab,
  updateTextPr,
} from '../utilities/shape/hookInvokers'
import { getPresentation } from '../utilities/finders'
import { TextDocRenderingState } from './typeset/DocumentRenderingState'
import { TextBody } from '../SlideElement/TextBody'
import { TableCell } from '../Table/TableCell'
import { PresentationField } from '../Paragraph/ParaContent/PresentationField'
import { isNumber, eachOfUnicodeString } from '../../common/utils'
import { ParaCollection } from '../Paragraph/ParaCollection'
import { calculateTextDocument } from './typeset/calculate'
import { EditorSettings } from '../common/EditorSettings'
import {
  genLetterStatusInfo,
  isArabicLetter,
} from '../../lib/utils/arabicReshaper/statusCode'
import { informDocChange } from '../calculation/informChanges/textContent'
import { calculateShapeContent } from '../calculation/calculate/shapeContent'
import { calculateForRendering } from '../calculation/calculate/calculateForRendering'
import { CHexColor } from '../color/CHexColor'

import {
  moveCursorToEndPosInParagraph,
  moveCursorToStartPosInParagraph,
} from '../utilities/cursor/moveToEdgePos'
import { AdjacentPosition } from '../Paragraph/common'
import {
  isCursorAtBeginInParagraph,
  isCursorAtEndInParagraph,
  moveCursorToAdjacentPosInParagraph,
} from '../Paragraph/utils/cursor'

import { updateSelectionBeginAndEndForParagraph } from '../Paragraph/utils/selection'
import { getParaClosestPos } from '../Paragraph/utils/position'
import { addToParagraphInTextDocument } from './utils/operations'
import { TextPr } from '../textAttributes/TextPr'
import { TextOperationDirection } from '../utilities/type'
import {
  updateCalcStatusForTextContentItem,
  updateCalcStatusForTextDocument,
} from '../calculation/updateCalcStatus/textContent'
import { HyperlinkOptions } from '../properties/options'
import { ManagedSliceUtil, RecyclableArray } from '../../common/managedArray'
import { PoolAllocator, Recyclable } from '../../common/allocator'
import { CheckEmptyRunOptions } from '../Paragraph/ParaContent/ParaRun'
import { DefaultTabStop } from '../textAttributes/ParaTab'

const UnknownValue = null
export class TextDocument extends Recyclable<TextDocument> {
  readonly instanceType = InstanceType.TextDocument
  id: string

  children: RecyclableArray<Paragraph>
  cursorPosition: DocumentCursorPosition
  selection: DocumentSelection
  parent: Presentation | TextBody | TableCell

  hasCalculated: boolean
  renderingState: TextDocRenderingState
  private startIndexFrom: number
  isSetToAll: boolean
  private static allocator: PoolAllocator<
    TextDocument,
    [
      parent: Presentation | TextBody | TableCell,
      x: number,
      y: number,
      xLimit: number,
      yLimit: number,
    ]
  > = PoolAllocator.getAllocator<
    TextDocument,
    [
      parent: Presentation | TextBody | TableCell,
      x: number,
      y: number,
      xLimit: number,
      yLimit: number,
    ]
  >(TextDocument)
  static new(
    parent: Presentation | TextBody | TableCell,
    x: number,
    y: number,
    xLimit: number,
    yLimit: number
  ) {
    return TextDocument.allocator.allocate(parent, x, y, xLimit, yLimit)
  }

  static recycle(textDoc: TextDocument) {
    TextDocument.allocator.recycle(textDoc)
  }
  constructor(
    parent: Presentation | TextBody | TableCell,
    x: number,
    y: number,
    xLimit: number,
    yLimit: number
  ) {
    super()
    this.parent = parent
    this.id = idGenerator.newId()

    this.startIndexFrom = 0

    this.hasCalculated = false
    this.renderingState = new TextDocRenderingState(this, x, y, xLimit, yLimit)

    // init contents
    this.children = new RecyclableArray(Paragraph.recycle)
    const defaultFirstParagraph = Paragraph.new(this)
    defaultFirstParagraph.correctChildren()
    defaultFirstParagraph.setNextInDocument(null)
    defaultFirstParagraph.setPrevInDocument(null)

    this.children.insert(0, defaultFirstParagraph)

    this.cursorPosition = {
      x: 0,
      y: 0,
      childIndex: 0,
      cursorX: 0,
      cursorY: 0,
    }

    this.selection = {
      start: false,
      isUse: false,
      startIndex: 0,
      endIndex: 0,
    }

    this.isSetToAll = false

    this.startIndexFrom = 0
  }

  reset(
    parent: Presentation | TextBody | TableCell,
    x: number,
    y: number,
    xLimit: number,
    yLimit: number
  ) {
    this.startIndexFrom = 0

    this.parent = parent

    this.hasCalculated = false
    this.renderingState.reset(x, y, xLimit, yLimit)
    this.children.clear()
    const firstPara = Paragraph.new(this)

    firstPara.correctChildren()
    firstPara.setNextInDocument(null)
    firstPara.setPrevInDocument(null)

    this.children.insert(0, firstPara)

    this.cursorPosition.x = 0
    this.cursorPosition.y = 0
    this.cursorPosition.childIndex = 0
    this.cursorPosition.cursorX = 0
    this.cursorPosition.cursorY = 0

    this.selection.start = false
    this.selection.isUse = false
    this.selection.startIndex = 0
    this.selection.endIndex = 0

    this.isSetToAll = false

    this.startIndexFrom = 0
  }
  getId() {
    return this.id
  }

  updateChildrenIndexing() {
    if (-1 < this.startIndexFrom) {
      for (let i = this.startIndexFrom, l = this.children.length; i < l; i++) {
        this.children.at(i)!.index = i
      }

      this.startIndexFrom = -1
    }
  }

  getPresentationFields() {
    const result: PresentationField[] = []
    ManagedSliceUtil.forEach(this.children, (paragraph) => {
      const children = paragraph.children
      ManagedSliceUtil.forEach(children, (item) => {
        if (item instanceof PresentationField) {
          result.push(item)
        }
      })
    })
    return result
  }

  getWholeWidth() {
    let totalWidth = 0
    const section = this.renderingState?.section
    if (section && section.columns.length > 0) {
      const columns = section.columns
      ManagedSliceUtil.forEach(columns, (column, columnIndex) => {
        for (let k = column.index; k <= column.endIndex; ++k) {
          const para = this.children.at(k)
          if (para) {
            const elementColIndex = this.getRelativeColIndexForParaElem(
              k,
              columnIndex
            )
            const { right } = para.getDocumentBounds(elementColIndex)
            if (totalWidth < right) {
              totalWidth = right
            }
          }
        }
      })
    } else {
      const bounds = this.getDocumentBounds()
      const boundsW = bounds.right - bounds.left
      let maxWidth = 0
      for (let i = 0, l = this.children.length; i < l; i++) {
        maxWidth = Math.max(this.children.at(i)!.getWholeWidth(), maxWidth)
      }
      totalWidth = Math.max(boundsW, maxWidth)
    }
    return totalWidth
  }

  getWholeHeight() {
    let totalHeight = 0
    const section = this.renderingState?.section
    if (section && section.columns.length > 0) {
      const columns = section.columns
      ManagedSliceUtil.forEach(columns, (column, columnIndex) => {
        for (let k = column.index; k <= column.endIndex; ++k) {
          const para = this.children.at(k)
          if (para) {
            const elementColIndex = this.getRelativeColIndexForParaElem(
              k,
              columnIndex
            )
            const { bottom } = para.getDocumentBounds(elementColIndex)
            if (totalHeight < bottom) {
              totalHeight = bottom
            }
          }
        }
      })
    } else {
      const bounds = this.getDocumentBounds()
      totalHeight = bounds.bottom - bounds.top
    }
    return totalHeight
  }

  private reindexParas(startIndex: number) {
    if (-1 === this.startIndexFrom || this.startIndexFrom > startIndex) {
      this.startIndexFrom = startIndex
    }
  }

  private createNewParagraph() {
    const para = Paragraph.new(this)
    para.correctChildren()
    moveCursorToStartPosInParagraph(para, false)
    return para
  }

  private _remove(
    dir: TextOperationDirection,
    isRemoveOnlySelection: boolean,
    isOnTextAdd: boolean
  ) {
    if (this.cursorPosition.childIndex < 0) return false

    let result = true
    let para: Nullable<Paragraph>
    if (true === this.selection.isUse) {
      let startIndex = this.selection.startIndex
      let endIndex = this.selection.endIndex
      if (endIndex < startIndex) {
        const t = startIndex
        startIndex = endIndex
        endIndex = t
      }

      para = this.children.at(endIndex)
      if (
        startIndex !== endIndex &&
        (para == null || true === para.hasNoSelection(true))
      ) {
        endIndex--
      }

      this.selection.isUse = false
      this.selection.startIndex = 0
      this.selection.endIndex = 0

      if (startIndex < endIndex) {
        const startPara = this.children.at(startIndex)
        const endPara = this.children.at(endIndex)
        let isStartEmpty = startPara == null
        let isEndEmpty = endPara == null
        if (startPara) {
          startPara.remove(1)
          isStartEmpty = startPara.isEmpty()
        }

        if (endPara) {
          endPara.remove(1)
          isEndEmpty = endPara.isEmpty()
        }

        if (!isStartEmpty && !isEndEmpty) {
          this.children.recycle(
            this.removeContent(startIndex + 1, endIndex - startIndex - 1)
          )

          this.cursorPosition.childIndex = startIndex

          if (true === isOnTextAdd) {
            moveCursorToEndPosInParagraph(startPara!, false, false)
            this.remove(1, true)
          } else {
            this.cursorPosition.childIndex = startIndex
            const toAppend = this.removeContent(startIndex + 1, 1)[0]
            toAppend && startPara!.appendParagraph(toAppend)
          }
        } else if (!isStartEmpty) {
          this.children.recycle(
            this.removeContent(startIndex + 1, endIndex - startIndex)
          )
          this.cursorPosition.childIndex = startIndex
          moveCursorToEndPosInParagraph(startPara!, false, false)
        } else if (!isEndEmpty) {
          this.children.recycle(
            this.removeContent(startIndex, endIndex - startIndex)
          )
          this.cursorPosition.childIndex = startIndex
          moveCursorToStartPosInParagraph(this.children.at(startIndex)!, false)
        } else {
          if (true === isOnTextAdd) {
            this.children.recycle(
              this.removeContent(startIndex, endIndex - startIndex)
            )

            this.cursorPosition.childIndex = startIndex
            moveCursorToStartPosInParagraph(this.children.at(startIndex)!)
          } else {
            if (
              0 === startIndex &&
              endIndex - startIndex + 1 >= this.children.length
            ) {
              if (startPara && endPara) {
                const toAppend = this.removeContent(
                  this.children.length - 1,
                  1
                )[0]
                toAppend && startPara.appendParagraph(toAppend)
              }
              this.children.recycle(
                this.removeContent(1, this.children.length - 1)
              )
              result = false
            } else {
              this.children.recycle(
                this.removeContent(startIndex, endIndex - startIndex + 1)
              )
            }

            let para: Nullable<Paragraph>
            if (startIndex >= this.children.length) {
              this.cursorPosition.childIndex = this.children.length - 1
              para = this.children.at(this.cursorPosition.childIndex)
              if (para) {
                moveCursorToEndPosInParagraph(para, false, false)
              }
            } else {
              this.cursorPosition.childIndex = startIndex
              para = this.children.at(startIndex)
              para && moveCursorToStartPosInParagraph(para, false)
            }
          }
        }
      } else {
        this.cursorPosition.childIndex = startIndex
        let para = this.children.at(startIndex)

        if (para && false === para.remove(dir, isOnTextAdd)) {
          if (true !== isOnTextAdd) {
            if (true === para.isEmpty() && this.children.length > 1) {
              this.children.recycle(this.removeContent(startIndex, 1))

              if (startIndex >= this.children.length) {
                this.cursorPosition.childIndex = this.children.length - 1
                para = this.children.at(this.cursorPosition.childIndex)
                para && moveCursorToEndPosInParagraph(para, false, false)
              } else {
                this.cursorPosition.childIndex = startIndex
                para = this.children.at(startIndex)
                para && moveCursorToStartPosInParagraph(para, false)
              }
            } else if (
              this.cursorPosition.childIndex <
              this.children.length - 1
            ) {
              const toAppend = this.removeContent(startIndex + 1, 1)[0]
              toAppend && para.appendParagraph(toAppend)
            } else if (
              this.children.length === 1 &&
              true === this.children.at(0)!.isEmpty()
            ) {
              if (dir > 0) {
                this.insertParagraph(0, this.createNewParagraph())
                this.children.recycle(
                  this.removeContent(1, this.children.length - 1)
                )
              }

              result = false
            }
          }
        }
      }
    } else {
      if (true === isRemoveOnlySelection || true === isOnTextAdd) return true

      let para = this.children.at(this.cursorPosition.childIndex)

      if (para && false === para.remove(dir)) {
        if (dir < 0) {
          if (this.cursorPosition.childIndex > 0) {
            para = this.children.at(this.cursorPosition.childIndex - 1)
            if (para) {
              if (true === para.isEmpty()) {
                this.children.recycle(
                  this.removeContent(this.cursorPosition.childIndex - 1, 1)
                )
                this.cursorPosition.childIndex--
                para = this.children.at(this.cursorPosition.childIndex)
                para && moveCursorToStartPosInParagraph(para, false)
              } else {
                moveCursorToEndPosInParagraph(para, false, false)

                const toAppend = this.removeContent(
                  this.cursorPosition.childIndex,
                  1
                )[0]
                toAppend && para.appendParagraph(toAppend)
                this.cursorPosition.childIndex--
              }
            }
          } else if (0 === this.cursorPosition.childIndex) {
            result = false
          }
        } else if (dir > 0) {
          if (this.cursorPosition.childIndex < this.children.length - 1) {
            if (true === para.isEmpty()) {
              this.children.recycle(
                this.removeContent(this.cursorPosition.childIndex, 1)
              )
              para = this.children.at(this.cursorPosition.childIndex)
              para && moveCursorToStartPosInParagraph(para, false)
            } else {
              const toAppend = this.removeContent(
                this.cursorPosition.childIndex + 1,
                1
              )[0]
              toAppend && para.appendParagraph(toAppend)
            }
          } else if (
            true === para.isEmpty() &&
            this.cursorPosition.childIndex === this.children.length - 1 &&
            this.cursorPosition.childIndex !== 0
          ) {
            this.children.recycle(
              this.removeContent(this.cursorPosition.childIndex, 1)
            )
            this.cursorPosition.childIndex--
            para = this.children.at(this.cursorPosition.childIndex)
            para && moveCursorToEndPosInParagraph(para, false, false)
          } else if (
            this.cursorPosition.childIndex ===
            this.children.length - 1
          ) {
            result = false
          }
        }
      }

      const item = this.children.at(this.cursorPosition.childIndex)
      if (item) {
        item.cursorPosition.cursorX = item.cursorPosition.x
        item.cursorPosition.cursorY = item.cursorPosition.y
      }
    }

    return result
  }

  isOnlyOneElementSelected() {
    return (
      true === this.selection.isUse &&
      this.selection.startIndex === this.selection.endIndex
    )
  }

  childrenCount() {
    return this.children.length
  }

  getChild(index: number) {
    return this.children.at(index)
  }

  insertText(str: string) {
    eachOfUnicodeString(str, (charCode) => {
      if (9 === charCode) {
        // \t
        addToParagraphInTextDocument(this, ParaTab.new(), false)
      }
      if (10 === charCode) {
        // \n
        addToParagraphInTextDocument(this, ParaNewLine.new(), false)
      } else if (13 === charCode) {
        // \r
        return
      } else if (32 === charCode) {
        // space
        addToParagraphInTextDocument(this, ParaSpace.new(), false)
      } else addToParagraphInTextDocument(this, ParaText.new(charCode!), false)
    })
  }

  lastParagraph() {
    if (this.children.length <= 0) return null

    return this.children.at(this.children.length - 1)
  }

  getTheme() {
    if (this.parent) return this.parent.getTheme()

    return null
  }

  getColorMap() {
    if (this.parent) return this.parent.getColorMap()

    return null
  }

  lastParaEndTextHeight() {
    const count = this.children.length
    if (count <= 0) return 0

    const element = this.children.at(count - 1)!

    if (InstanceType.Paragraph === element.instanceType) {
      return element.paraEndTextHeight()
    } else return 0
  }

  getTextStyles(lvl: number) {
    return this.parent.getTextStyles(lvl)
  }

  getTableStyleForParagraph() {
    return this.parent.instanceType === InstanceType.TableCell
      ? this.parent.getTableStyleForParagraph()
      : undefined
  }

  getTextBgColor() {
    switch (this.parent.instanceType) {
      case InstanceType.Presentation: {
        return new CHexColor(255, 255, 255, false)
      }
      case InstanceType.TableCell: {
        return this.parent.getTextBgColor()
      }
      default:
        return undefined
    }
  }

  invalidateAllParagraphsCalcedPrs(bubbleUp = true) {
    const count = this.children.length
    for (let i = 0; i < count; i++) {
      const item = this.children.at(i)
      if (item) {
        item.invalidateCalcedPrs(false)
        item.invalidateAllItemsCalcedPrs(false)
      }
    }
    if (bubbleUp && count > 0) {
      updateCalcStatusForTextDocument(this)
    }
  }

  selectSelf(paraIndex: number) {
    const index = Math.max(0, Math.min(this.children.length - 1, paraIndex))

    this.selection.isUse = false
    this.selection.start = false
    this.selection.startIndex = index
    this.selection.endIndex = index
    this.cursorPosition.childIndex = index

    const para = this.children.at(index)
    if (para && true === para.hasTextSelection()) {
      this.selection.isUse = true
    }
    if (this.parent.instanceType !== InstanceType.Presentation) {
      this.parent.selectSelf()
    }
  }

  isCurrentSelected() {
    return this.parent.isCurrentSelected()
  }

  isInTableCell() {
    return this.parent && this.parent.instanceType === InstanceType.TableCell
  }

  getTableCell() {
    return this.parent && this.parent.instanceType === InstanceType.TableCell
      ? this.parent
      : null
  }

  isInLastCellOfRow() {
    if (this.parent.instanceType !== InstanceType.TableCell) return false

    return this.parent.isLastInRow()
  }

  isInPPT(id?: string) {
    let isUse = false
    if (null != id) {
      isUse =
        ManagedSliceUtil.findIndex(
          this.children,
          (item) => item.getId() === id
        ) !== -1
    } else {
      isUse = true
    }

    if (
      true === isUse &&
      this.parent &&
      this.parent.instanceType !== InstanceType.Presentation
    ) {
      return this.parent.isInPPT(this.getId())
    }

    return false
  }

  getParentShape() {
    if (this.parent && this.parent.instanceType === InstanceType.TextBody) {
      return this.parent.parent
    } else return null
  }

  getDocumentBounds() {
    return this.renderingState.getDocumentBounds()
  }

  getFirstParagraph() {
    if (this.children.length > 0) return this.children.at(0)

    return null
  }

  clearContent(keepEmptyPara = true) {
    this.removeSelection()

    this.cursorPosition.x = 0
    this.cursorPosition.y = 0
    this.cursorPosition.childIndex = 0
    this.cursorPosition.cursorX = 0
    this.cursorPosition.cursorY = 0

    this.removeAllContent()

    if (false !== keepEmptyPara) {
      this.insertParagraph(0, Paragraph.new(this))
    }
  }

  addContent(textDoc: TextDocument) {
    if (!textDoc || textDoc.children.length <= 0) return

    const paragraphs = textDoc.children.remove(0, textDoc.children.length)
    if (this.children.length <= 0 || true === this.isEmpty()) {
      if (this.children.length > 0) this.removeAllContent()

      for (let i = 0, l = paragraphs.length; i < l; ++i) {
        this.insertParagraph(i, paragraphs[i])
      }
    } else {
      const lastPara = this.children.at(this.children.length - 1)!

      lastPara.setNextInDocument(paragraphs[0])
      paragraphs[0].setPrevInDocument(lastPara)

      for (let i = 0, l = paragraphs.length; i < l; ++i) {
        this.insertParagraph(this.children.length, paragraphs[i])
      }
    }
  }

  isEmpty(props?: CheckEmptyRunOptions) {
    if (this.children.length > 1) {
      return false
    }

    return this.children.length <= 0 || this.children.at(0)!.isEmpty(props)
  }

  getTextTransformOfParent() {
    if (this.parent && this.parent.instanceType !== InstanceType.Presentation) {
      return this.parent.getTextTransformOfParent()
    }

    return null
  }

  isHitInText(x: number, y: number) {
    const elmIndex = this.getElementIndexByXY(x, y)
    const item = this.children.at(elmIndex)
    if (item == null) return false

    const elementColIndex = this.getElementColIndexByXY(elmIndex, x)
    return item.isHitInText(x, y, elementColIndex)
  }

  collectAllFontNames(allFonts: Dict<boolean>) {
    const l = this.children.length
    for (let i = 0; i < l; i++) {
      const para = this.children.at(i)!
      para.collectAllFontNames(allFonts)
    }
  }

  syncEditorUIState() {
    this.onParaPrUpdate()
    this.onTextPrUpdate()

    if (
      (true === this.selection.isUse &&
        this.selection.startIndex === this.selection.endIndex) ||
      false === this.selection.isUse
    ) {
      const index =
        true === this.selection.isUse
          ? this.selection.startIndex
          : this.cursorPosition.childIndex
      const para = this.children.at(index)
      para && para.syncEditorUIState()
    }
  }

  /** 统一 return cursorType 到最上级更新 */
  getCursorInfoByPos(x: number, y: number): Nullable<HoverInfo> {
    const elmIndex = this.getElementIndexByXY(x, y)
    const item = this.children.at(elmIndex)
    if (item == null) return undefined

    const elementColIndex = this.getElementColIndexByXY(elmIndex, x)
    return item.getCursorInfoByPos(x, y, elementColIndex)
  }

  remove(
    dir: TextOperationDirection,
    isRemoveOnlySelection?: boolean,
    isOnTextAdd?: boolean
  ) {
    if (true === this.isSetToAll) {
      this.removeAllContent()
      this.insertParagraph(0, this.createNewParagraph())

      this.cursorPosition.x = 0
      this.cursorPosition.y = 0
      this.cursorPosition.childIndex = 0
      this.cursorPosition.cursorX = 0
      this.cursorPosition.cursorY = 0

      this.selection.start = false
      this.selection.isUse = false
      this.selection.startIndex = 0
      this.selection.endIndex = 0

      return false
    }

    if (undefined === isRemoveOnlySelection) isRemoveOnlySelection = false

    if (undefined === isOnTextAdd) isOnTextAdd = false

    return this._remove(dir, isRemoveOnlySelection, isOnTextAdd)
  }

  hasTextSelection() {
    if (true === this.selection.isUse) return true

    return false
  }

  insertSelection(
    paraCollection: ParaCollection,
    adjPos: AdjacentPosition,
    isInsertToNewContent?: boolean,
    selfSelect?: boolean
  ) {
    const para = adjPos.paragraph
    const paraAdjPos = getParaClosestPos(para, adjPos)!
    const lastEntity = paraAdjPos?.enities[paraAdjPos.enities.length - 1]
    if (lastEntity && InstanceType.ParaRun === lastEntity.instanceType) {
      let destIndex = -1
      const count = this.children.length
      for (let i = 0; i < count; i++) {
        if (this.children.at(i) === para) {
          destIndex = i
          break
        }
      }

      if (-1 === destIndex) return

      const elements = paraCollection.elements
      const elementsCount = elements.length
      const firstElement = elements[0]
      if (1 === elementsCount && true !== firstElement.selectedAll) {
        const newPara = firstElement.item
        const countOfNewChildren = newPara.children.length - 1

        const newElement = lastEntity.splitAtContentPos(
          paraAdjPos.adjacentPos!.pos,
          paraAdjPos.enities.length - 1
        )
        const prevEntity: Paragraph = paraAdjPos.enities[
          paraAdjPos.enities.length - 2
        ] as Paragraph
        const prevPos =
          paraAdjPos.adjacentPos!.pos.positonIndices[
            paraAdjPos.enities.length - 2
          ]

        let text = ''
        if (EditorSettings.isRTL) {
          text = (prevEntity.getText() ?? '').trim()
        }

        prevEntity.addItemAt(prevPos + 1, newElement)

        for (let i = 0; i < countOfNewChildren; i++) {
          const item = newPara.children.at(i)!
          prevEntity.addItemAt(prevPos + 1 + i, item)

          item.selectAll()
        }

        if (isInsertToNewContent) {
          prevEntity.pr.reset()
          prevEntity.pr.fromOptions(newPara.pr)
        }

        prevEntity.selection.isUse = true
        prevEntity.selection.startIndex = prevPos + 1
        prevEntity.selection.endIndex = prevPos + 1 + countOfNewChildren - 1

        for (let i = 0; i < paraAdjPos.enities.length - 2; i++) {
          const entity = paraAdjPos.enities[i]
          const entityPos = paraAdjPos.adjacentPos!.pos.positonIndices[i]

          entity.selection.isUse = true
          entity.selection.startIndex = entityPos
          entity.selection.endIndex = entityPos
        }

        this.selection.isUse = true
        this.selection.startIndex = destIndex
        this.selection.endIndex = destIndex

        if (prevEntity.correctChildren) {
          prevEntity.correctChildren()
        }

        if (EditorSettings.isRTL && !text) {
          const currentText = (prevEntity.getText() ?? '').trim()
          if (
            isArabicLetter(
              genLetterStatusInfo(currentText.charCodeAt(0)).statusCode
            )
          ) {
            prevEntity.setParagraphTextDirection(true)
          }
        }
      } else {
        let isConcatStart =
          InstanceType.Paragraph !== elements[0].item?.instanceType
            ? false
            : true
        let isConcatEnd =
          InstanceType.Paragraph !==
            elements[elementsCount - 1].item?.instanceType ||
          true === elements[elementsCount - 1].selectedAll
            ? false
            : true
        const startParagraph = para
        let lastParagraph = para
        let paraEndIndex = destIndex

        moveCursorToAdjacentPosInParagraph(para, adjPos)
        para.removeSelection()

        let keepEmptyPara = false

        if (true === isCursorAtEndInParagraph(para)) {
          isConcatEnd = false

          if (
            1 === elementsCount &&
            InstanceType.Paragraph === firstElement.item?.instanceType &&
            (true === firstElement.item.isEmpty() ||
              true === firstElement.selectedAll)
          ) {
            isConcatStart = false

            const childPara = this.children.at(destIndex)
            if (childPara && true !== childPara.isEmpty()) {
              destIndex++
            }
          } else if (
            true === elements[elementsCount - 1].selectedAll &&
            true === isConcatStart
          ) {
            keepEmptyPara = true
          }
        } else if (true === isCursorAtBeginInParagraph(para)) {
          isConcatStart = false
        } else {
          const newParagraph = Paragraph.new(this)
          para.splitRestInto(newParagraph)
          this.insertParagraph(destIndex + 1, newParagraph)

          lastParagraph = newParagraph
          paraEndIndex = destIndex + 1
        }

        let emptyParagraph: Nullable<Paragraph>
        if (true === keepEmptyPara) {
          emptyParagraph = Paragraph.new(this)
          emptyParagraph.setPr(startParagraph.pr)
          emptyParagraph.paraTextPr.fromTextPrData(
            startParagraph.paraTextPr.textPr
          )
          this.insertParagraph(destIndex + 1, emptyParagraph)
        }

        let startIndex = 0
        if (true === isConcatStart) {
          const startPara = elements[0].item as Paragraph
          startPara.selectAll()
          const lenOfStartPara = startPara.children.length

          startParagraph.setPr(startPara.pr)
          startParagraph.paraTextPr.resetTextPr()
          startParagraph.paraTextPr.fromTextPrData(startPara.paraTextPr.textPr)

          startParagraph.appendParagraph(startPara)
          startIndex++

          startParagraph.selection.isUse = true
          startParagraph.selection.startIndex =
            startParagraph.children.length - lenOfStartPara
          startParagraph.selection.endIndex = startParagraph.children.length - 1
        }

        let endIndex = elementsCount - 1
        if (true === isConcatEnd) {
          const endPara = elements[elementsCount - 1].item as Paragraph
          const tmCount = endPara.children.length - 1

          endPara.selectAll()
          if (lastParagraph) {
            endPara.appendParagraph(lastParagraph)
            endPara.setPr(lastParagraph.pr)
          }
          this.children.recycle(this.removeContent(paraEndIndex + 1, 1))

          this.insertParagraph(paraEndIndex, endPara)

          endPara.selection.isUse = true
          endPara.selection.startIndex = 0
          endPara.selection.endIndex = tmCount

          endIndex--
        }

        for (let i = startIndex; i <= endIndex; i++) {
          this.insertParagraph(destIndex + i, elements[i].item as Paragraph)
          this.children.at(destIndex + i)!.selectAll()
        }

        let lastIndex = destIndex + elementsCount - 1

        if (
          emptyParagraph &&
          emptyParagraph === this.children.at(lastIndex + 1)
        ) {
          lastIndex++
          const child = this.children.at(lastIndex)
          child && child.selectAll()
        } else if (
          lastIndex + 1 < this.children.length &&
          false === isConcatEnd
        ) {
          lastIndex++
          const childPara = this.children.at(lastIndex)
          if (childPara) {
            childPara.selection.isUse = true
            updateSelectionBeginAndEndForParagraph(childPara, true, true)
            updateSelectionBeginAndEndForParagraph(childPara, false, true)
          }
        }

        this.selection.start = false
        this.selection.isUse = true
        this.selection.startIndex = destIndex
        this.selection.endIndex = lastIndex
        this.cursorPosition.childIndex = lastIndex
      }

      if (
        this.parent &&
        this.parent.instanceType !== InstanceType.Presentation &&
        selfSelect !== false
      ) {
        this.parent.selectSelf()
      }
    }
  }

  canChangeParagraphIndentLevel(isIncrease: boolean) {
    if (true === this.isSetToAll) {
      for (let i = 0, l = this.children.length; i < l; i++) {
        if (!this.children.at(i)!.canChangeIndentLevel(isIncrease)) return false
      }
      return true
    }

    if (this.cursorPosition.childIndex < 0) return false

    let para: Nullable<Paragraph>
    if (true === this.selection.isUse) {
      let startIndex = this.selection.startIndex
      let endIndex = this.selection.endIndex
      if (endIndex < startIndex) {
        const t = startIndex
        startIndex = endIndex
        endIndex = t
      }

      for (let i = startIndex; i <= endIndex; i++) {
        para = this.children.at(i)
        if (para && !para.canChangeIndentLevel(isIncrease)) {
          return false
        }
      }

      return true
    }
    para = this.children.at(this.cursorPosition.childIndex)
    return para ? para.canChangeIndentLevel(isIncrease) : false
  }

  getFirstParaPr() {
    let paraPr: Nullable<ParaPr>
    if (this.children.length <= 0) return paraPr

    if (true === this.isSetToAll) {
      const startPr = this.children.at(0)!.getCalcedParaPr()
      paraPr = startPr.clone()

      return paraPr
    }

    if (true === this.selection.isUse) {
      let startIndex = this.selection.startIndex
      let endIndex = this.selection.endIndex
      if (endIndex < startIndex) {
        const t = startIndex
        startIndex = endIndex
        endIndex = t
      }

      let startPr: Nullable<ParaPr> = null

      for (let i = startIndex; i <= endIndex; i++) {
        const para = this.children.at(i)
        if (para?.isSelectedAll()) {
          startPr = para.getCalcedParaPr()
          break
        }
      }

      paraPr = startPr ? startPr.clone() : paraPr
    } else {
      const item = this.children.at(this.cursorPosition.childIndex)
      const prs = item?.getCalcedPrs(false)

      paraPr = prs ? prs.paraPr.clone() : paraPr
    }

    return paraPr
  }

  getCalcedParaPr() {
    let paraPr = ParaPr.new()
    if (this.children.length <= 0) return paraPr

    if (true === this.isSetToAll) {
      const startPr = this.children.at(0)!.getCalcedParaPr()
      let pr = startPr.clone()

      for (let i = 1; i < this.children.length; i++) {
        const childPr = this.children.at(i)!.getCalcedParaPr()
        pr = pr.intersect(childPr) ?? pr
      }

      if (pr.ind.left === UnknownValue) pr.ind.left = startPr.ind.left

      if (pr.ind.right === UnknownValue) pr.ind.right = startPr.ind.right

      if (pr.ind.firstLine === UnknownValue) {
        pr.ind.firstLine = startPr.ind.firstLine
      }

      paraPr = pr

      return paraPr
    }

    if (true === this.selection.isUse) {
      let startIndex = this.selection.startIndex
      let endIndex = this.selection.endIndex
      if (endIndex < startIndex) {
        const t = startIndex
        startIndex = endIndex
        endIndex = t
      }

      const startPr = this.children.at(startIndex)?.getCalcedParaPr()
      let pr = startPr ? startPr.clone() : paraPr

      for (let i = startIndex + 1; i <= endIndex; i++) {
        const cPr = this.children.at(i)?.getCalcedParaPr()
        pr = cPr ? (pr.intersect(cPr) ?? pr) : pr
      }

      if (startPr) {
        if (undefined === pr.ind.left) pr.ind.left = startPr.ind.left

        if (undefined === pr.ind.right) pr.ind.right = startPr.ind.right

        if (undefined === pr.ind.firstLine) {
          pr.ind.firstLine = startPr.ind.firstLine
        }
      }
      paraPr = pr
    } else {
      const item = this.children.at(this.cursorPosition.childIndex)
      const prs = item?.getCalcedPrs(false)

      paraPr = prs ? prs.paraPr.clone() : paraPr
    }

    return paraPr
  }

  getFirstParaTextPr() {
    let textPr: Nullable<TextPr> = null

    if (this.children.length <= 0) return textPr

    if (true === this.isSetToAll) {
      const para = this.children.at(0)!
      para.isSetToAll = true
      textPr = para.getFirstRunTextPr()
      para.isSetToAll = false

      return textPr!
    }

    if (true === this.selection.isUse) {
      let startIndex = this.selection.startIndex
      let endIndex = this.selection.endIndex
      if (endIndex < startIndex) {
        const t = startIndex
        startIndex = endIndex
        endIndex = t
      }

      textPr = this.children.at(startIndex)?.getFirstRunTextPr()
    } else {
      textPr = this.children
        .at(this.cursorPosition.childIndex)
        ?.getFirstRunTextPr()
    }

    return textPr
  }

  getCalcedTextPr() {
    let calcedTextPr: Nullable<TextPr> = null

    if (this.children.length <= 0) return calcedTextPr

    if (true === this.isSetToAll) {
      let textPr: TextPr
      let para = this.children.at(0)!
      para.isSetToAll = true
      textPr = para.getCalcedTextPr()
      para.isSetToAll = false

      const l = this.children.length
      for (let i = 1; i < l; i++) {
        para = this.children.at(i)!
        para.isSetToAll = true
        const curPr = para.getCalcedTextPr()
        textPr = textPr.subDiff(curPr)
        para.isSetToAll = false
      }

      calcedTextPr = textPr

      return calcedTextPr
    }

    if (true === this.selection.isUse) {
      let startIndex = this.selection.startIndex
      let endIndex = this.selection.endIndex
      if (endIndex < startIndex) {
        const t = startIndex
        startIndex = endIndex
        endIndex = t
      }

      let textPr = this.children.at(startIndex)?.getCalcedTextPr()
      if (textPr) {
        for (let i = startIndex + 1; i <= endIndex; i++) {
          const curPr = this.children.at(i)?.getCalcedTextPr()
          textPr = curPr ? textPr.subDiff(curPr) : textPr
        }
      }

      calcedTextPr = textPr
    } else {
      calcedTextPr = this.children
        .at(this.cursorPosition.childIndex)
        ?.getCalcedTextPr()
    }

    return calcedTextPr
  }

  onParaPrUpdate() {
    const paraPr = this.getCalcedParaPr()

    if (null != paraPr) {
      if (null != paraPr.tabs) {
        const defaultTab =
          paraPr.defaultTab != null ? paraPr.defaultTab : DefaultTabStop
        updateParagraphTab(defaultTab, paraPr.tabs)
      }

      updateParagraphProps(paraPr)
    }
  }

  onTextPrUpdate() {
    const textPr = this.getCalcedTextPr()

    if (null != textPr) {
      const theme = this.getTheme()
      if (theme && theme.themeElements && theme.themeElements.fontScheme) {
        const fontScheme = theme.themeElements.fontScheme
        if (textPr.fontFamily) {
          textPr.fontFamily.name = fontScheme.checkFont(textPr.fontFamily.name)
        }
        if (textPr.rFonts) {
          if (textPr.rFonts.ascii) {
            textPr.rFonts.ascii.name = fontScheme.checkFont(
              textPr.rFonts.ascii.name
            )
          }
          if (textPr.rFonts.eastAsia) {
            textPr.rFonts.eastAsia.name = fontScheme.checkFont(
              textPr.rFonts.eastAsia.name
            )
          }
          if (textPr.rFonts.hAnsi) {
            textPr.rFonts.hAnsi.name = fontScheme.checkFont(
              textPr.rFonts.hAnsi.name
            )
          }
          if (textPr.rFonts.cs) {
            textPr.rFonts.cs.name = fontScheme.checkFont(textPr.rFonts.cs.name)
          }
        }
      }
      updateTextPr(textPr)
    }
  }

  removeSelection() {
    if (true === this.selection.isUse) {
      let startIndex = this.selection.startIndex
      let ednIndex = this.selection.endIndex

      if (startIndex > ednIndex) {
        const t = startIndex
        startIndex = ednIndex
        ednIndex = t
      }

      startIndex = Math.max(0, startIndex)
      ednIndex = Math.min(this.children.length - 1, ednIndex)

      for (let i = startIndex; i <= ednIndex; i++) {
        this.children.at(i)!.removeSelection()
      }
    }

    this.selection.startIndex = 0
    this.selection.endIndex = 0

    this.selection.isUse = false
    this.selection.start = false
  }

  getRelativeColIndexForParaElem(paraPos: number, colIndex: number) {
    const paragraph = this.children.at(paraPos)
    if (!paragraph || paragraph.getColumnsCount() <= 0) return 0

    const startColumn = paragraph.getIndexOfStartColumn()

    return colIndex - startColumn
  }

  hasNoSelection(isCheckEnd?: boolean) {
    if (true === this.selection.isUse) {
      if (this.selection.startIndex === this.selection.endIndex) {
        const para = this.children.at(this.selection.startIndex)
        return para ? para.hasNoSelection(isCheckEnd) : true
      } else {
        return false
      }
    }

    return true
  }

  selectAll() {
    if (true === this.selection.isUse) this.removeSelection()

    this.selection.isUse = true
    this.selection.start = false

    this.selection.startIndex = 0
    this.selection.endIndex = this.children.length - 1

    for (let i = 0, l = this.children.length; i < l; i++) {
      this.children.at(i)!.selectAll()
    }
  }

  insertParagraph(index: number, newParagraph: Paragraph) {
    if (index < 0 || index > this.children.length) return

    const prevPara = this.children.at(index - 1)
    const curPara = this.children.at(index)

    informDocChange(this)

    this.children.insert(index, newParagraph)
    newParagraph.setParent(this)
    newParagraph.setNextInDocument(curPara)
    newParagraph.setPrevInDocument(prevPara)

    if (null != prevPara) prevPara.setNextInDocument(newParagraph)

    if (null != curPara) curPara.setPrevInDocument(newParagraph)

    this.reindexParas(index)
    updateCalcStatusForTextContentItem(this)
  }

  removeContent(index: number, count: number, isCheckLastElement?: boolean) {
    if (count <= 0) return []

    index = Math.max(0, Math.min(index, this.children.length - 1))
    const prevChild = this.children.at(index - 1)
    const nextChild = this.children.at(index + count)

    for (let i = 0; i < count; i++) {
      const child = this.children.at(index + i)
      child && child.beforeDelete()
    }

    informDocChange(this)

    const removed = this.children.remove(index, count)

    if (null != prevChild) prevChild.setNextInDocument(nextChild)

    if (null != nextChild) nextChild.setPrevInDocument(prevChild)

    if (false !== isCheckLastElement && this.children.length <= 0) {
      this.insertParagraph(this.children.length, Paragraph.new(this))
    }

    this.reindexParas(index)
    updateCalcStatusForTextContentItem(this)
    return removed
  }

  removeAllContent() {
    for (let i = 0, l = this.children.length; i < l; i++) {
      this.children.at(i)!.beforeDelete()
    }
    informDocChange(this)

    this.children.clear()
    updateCalcStatusForTextContentItem(this)
  }

  getParentSlideIndex() {
    return this.parent && this.parent.instanceType !== InstanceType.Presentation
      ? this.parent.getParentSlideIndex()
      : 0
  }

  insertHyperlink(hyperlinkOptions: HyperlinkOptions) {
    if (
      false === this.selection.isUse ||
      this.selection.startIndex === this.selection.endIndex
    ) {
      const elmIndex =
        true === this.selection.isUse
          ? this.selection.startIndex
          : this.cursorPosition.childIndex
      const para = this.children.at(elmIndex)
      para && para.insertHyperlink(hyperlinkOptions)
    }
  }

  updateHyperlink(hyperlinkOptions: HyperlinkOptions) {
    if (
      false === this.selection.isUse ||
      this.selection.startIndex === this.selection.endIndex
    ) {
      const elmIndex =
        true === this.selection.isUse
          ? this.selection.startIndex
          : this.cursorPosition.childIndex
      const para = this.children.at(elmIndex)
      para && para.updateHyperlink(hyperlinkOptions)
    }
  }

  removeHyperlink() {
    if (
      false === this.selection.isUse ||
      this.selection.startIndex === this.selection.endIndex
    ) {
      const elmIndex =
        true === this.selection.isUse
          ? this.selection.startIndex
          : this.cursorPosition.childIndex
      const para = this.children.at(elmIndex)
      para && para.removeHyperlink()
    }
  }

  canInsertHyperlink(isCheckInHyperlink?: boolean) {
    if (
      true === this.selection.isUse &&
      this.selection.startIndex !== this.selection.endIndex
    ) {
      return false
    }
    const index =
      true === this.selection.isUse
        ? this.selection.startIndex
        : this.cursorPosition.childIndex

    const para = this.children.at(index)
    return para ? para.canInsertHyperlink(isCheckInHyperlink) : false
  }

  startSelectFromCurrentPosition() {
    this.selection.isUse = true
    this.selection.start = false
    this.selection.startIndex = this.cursorPosition.childIndex
    this.selection.endIndex = this.cursorPosition.childIndex
    const para = this.children.at(this.cursorPosition.childIndex)
    para && para.startSelectFromCurrentPosition()
  }

  isSelectedAll() {
    if (
      this.children.length > 0 &&
      true === this.selection.isUse &&
      ((0 === this.selection.startIndex &&
        this.children.length - 1 === this.selection.endIndex) ||
        (0 === this.selection.endIndex &&
          this.children.length - 1 === this.selection.startIndex)) &&
      true === this.children.at(0)!.isSelectedAll() &&
      true === this.children.at(this.children.length - 1)!.isSelectedAll()
    ) {
      return true
    }

    return false
  }

  getParent() {
    return this.parent
  }

  getColumnsCount() {
    let valueOfColumnCount = 1
    if (this.parent.instanceType === InstanceType.TextBody) {
      const bodyPr = this.parent.getBodyPr()
      valueOfColumnCount = isNumber(bodyPr.numCol) ? bodyPr.numCol : 1
    }
    return valueOfColumnCount > 0 ? valueOfColumnCount : 1
  }

  /** 根据 innerMM 坐标判断位于第几个 paragraph */
  getElementIndexByXY(x: number, y: number) {
    const renderingState = this.renderingState
    // Now check for empty paragraphs with section endings
    if (renderingState.section == null || renderingState.section.isEmpty()) {
      const startIndex = Math.min(
        renderingState.startParaIndex,
        this.children.length - 1
      )
      const endIndex = this.children.length - 1

      const inlineIndexes: number[] = []
      for (let i = startIndex; i <= endIndex; i++) {
        inlineIndexes.push(i)
      }

      const count = inlineIndexes.length
      if (count <= 0) return startIndex

      for (let i = 0; i < count - 1; i++) {
        const item = this.children.at(inlineIndexes[i + 1])!

        if (item.getColumnsCount() <= 0 || y < item.getDocumentBounds(0).top) {
          return inlineIndexes[i]
        }

        if (i === count - 2) {
          return inlineIndexes[count - 1]
        }
      }

      return inlineIndexes[0]
    }

    const columns = renderingState.section.columns
    const colsCount = columns.length
    let columnIndex = 0
    for (; columnIndex < colsCount - 1; ++columnIndex) {
      if (
        x <
        (columns.at(columnIndex)!.xLimit + columns.at(columnIndex + 1)!.x) / 2
      ) {
        break
      }
    }

    while (columnIndex > 0 && true === columns.at(columnIndex)!.empty) {
      columnIndex--
    }

    const column = columns.at(columnIndex)!
    const startIndex = column.index
    const endIndex = column.endIndex
    let index: number
    for (index = startIndex; index < endIndex; ++index) {
      const item = this.children.at(index + 1)
      if (item) {
        const bounds = item.getDocumentBounds(0)
        if (y < bounds.top) return index
      }
    }

    if (index === endIndex) {
      return endIndex
    }
    return 0
  }

  getElementColIndexByXY(elementIndex: number, x: number) {
    const section = this.renderingState?.section
    if (section && section.columns.length > 0) {
      return _getElementColIndexByXYFromDocument(this, elementIndex, x)
    }
    return 0
  }

  clone(parent: Presentation | TextBody | TableCell) {
    const textDoc = TextDocument.new(parent, 0, 0, 0, 0)

    textDoc.removeAllContent()

    const count = this.children.length
    for (let i = 0; i < count; i++) {
      textDoc.insertParagraph(i, this.children.at(i)!.clone(textDoc))
    }

    return textDoc
  }

  populateBy(other: TextDocument) {
    this.removeAllContent()

    const count = other.children.length
    for (let index = 0; index < count; index++) {
      this.insertParagraph(index, other.children.at(index)!.clone(this))
    }
  }

  _calculateState() {
    const presentation = getPresentation(this)
    if (presentation) {
      calculateForRendering()
    } else {
      if (
        this.parent &&
        this.parent.instanceType === InstanceType.TextBody &&
        this.parent.parent?.instanceType === InstanceType.Shape
      ) {
        calculateShapeContent(this.parent.parent)
        return
      }
      if (this.renderingState.xLimit > 0) {
        calculateTextDocument(
          this,
          this.renderingState.xLimit,
          this.renderingState.yLimit
        )
      }
    }
  }
}

type DocumentCursorPosition = {
  x: number
  y: number
  childIndex: number
  cursorX: number
  cursorY: number
}

type DocumentSelection = {
  start: boolean
  isUse: boolean
  startIndex: number
  endIndex: number
}

function _getElementColIndexByXYFromDocument(
  textDoc: TextDocument,
  paraIndex: number,
  x: number
) {
  const paragraph = textDoc.children.at(paraIndex)
  if (!paragraph) return 0

  const renderingState = textDoc.renderingState
  if (!renderingState) return 0

  const section = renderingState.section

  if (!section || section.isEmpty()) return 0

  const paraStartColumn = paragraph.getIndexOfStartColumn()
  const paraColumnsCount = paragraph.getColumnsCount()

  const colsCount = section.columns.length
  let startColumn = 0
  let endColumnIndex = colsCount - 1

  startColumn = paragraph.getIndexOfStartColumn()
  endColumnIndex = Math.min(
    paraStartColumn + paraColumnsCount - 1,
    colsCount - 1
  )

  while (endColumnIndex > startColumn) {
    const col = section.columns.at(endColumnIndex)
    if (col == null || true === col.empty) {
      endColumnIndex--
    } else break
  }

  let colIndex = endColumnIndex
  for (let i = startColumn; i < endColumnIndex; ++i) {
    const para = section.columns.at(i)
    const paraNext = section.columns.at(i + 1)

    if (para && paraNext && x < (para.xLimit + paraNext.x) / 2) {
      colIndex = i
      break
    }
  }

  return textDoc.getRelativeColIndexForParaElem(paraIndex, colIndex)
}
