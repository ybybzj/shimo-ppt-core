import { Nullable } from '../../../liber/pervasive'

export class TextDocBounds {
  bottom: number
  left: number
  right: number
  top: number
  constructor(left: number, top: number, right: number, bottom: number) {
    this.bottom = bottom
    this.left = left
    this.right = right
    this.top = top
  }

  fromBounds(
    bounds: Nullable<{
      bottom: number
      left: number
      right: number
      top: number
    }>
  ) {
    if (!bounds) return

    this.bottom = bounds.bottom
    this.left = bounds.left
    this.right = bounds.right
    this.top = bounds.top
  }

  translateBy(dx: number, dy: number) {
    this.bottom += dy
    this.top += dy
    this.left += dx
    this.right += dx
  }

  reset(left?: number, top?: number, right?: number, bottom?: number) {
    this.bottom = bottom ?? 0
    this.left = left ?? 0
    this.right = right ?? 0
    this.top = top ?? 0
  }

  isEmpty() {
    return (
      this.bottom === 0 && this.left === 0 && this.right === 0 && this.top === 0
    )
  }

  clone() {
    return new TextDocBounds(this.left, this.top, this.right, this.bottom)
  }
}
