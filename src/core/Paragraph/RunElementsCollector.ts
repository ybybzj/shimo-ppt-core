import { InstanceType, InstanceTypeIDS } from '../instanceTypes'
import { ParagraphElementPosition } from './common'
import { ParaHyperlink } from './ParaContent/ParaHyperlink'
import { ParaRun } from './ParaContent/ParaRun'
import { Paragraph } from './Paragraph'
import { RunContentElement } from './RunElement/type'

export class RunElementsCollector<
  Elm extends RunContentElement = RunContentElement,
> {
  elementPosition: ParagraphElementPosition
  elements: Elm[]
  count: number
  types: InstanceTypeIDS[]
  isEnd: boolean
  reverse: boolean
  currentChildPosition: ParagraphElementPosition
  constructor(
    elementPosition: ParagraphElementPosition,
    count: number,
    types: InstanceTypeIDS[] = [],
    isReverse: boolean = false
  ) {
    this.elementPosition = elementPosition
    this.elements = []
    this.count = count + 1
    this.types = types
    this.isEnd = false
    this.reverse = isReverse
    this.currentChildPosition = new ParagraphElementPosition()
  }

  private updateChildPosition(indexOfPosition: number, valueOfDepth: number) {
    this.currentChildPosition.updatePosByLevel(indexOfPosition, valueOfDepth)
  }

  private containType(valueOfType: InstanceTypeIDS) {
    if (this.types.length <= 0) return true

    for (
      let valueOfIndex = 0, valueOfCount = this.types.length;
      valueOfIndex < valueOfCount;
      ++valueOfIndex
    ) {
      if (valueOfType === this.types[valueOfIndex]) return true
    }

    return false
  }

  add(element: RunContentElement) {
    if (this.containType(element.instanceType)) {
      if (this.reverse) {
        this.elements.splice(0, 0, element as Elm)
      } else {
        this.elements.push(element as Elm)
      }

      this.count--
    }
  }

  isEmpty() {
    return this.count <= 0
  }

  amendEnd() {
    if (this.count <= 0) {
      this.isEnd = false
      if (this.reverse) this.elements.splice(0, 1)
      else this.elements.splice(this.elements.length - 1, 1)
    } else if (this.count >= 1) {
      this.isEnd = true
    }
  }

  getElements() {
    return this.elements
  }

  private updatePrevElementsPosInParaItem(
    paraItem: ParaRun | ParaHyperlink,
    fromElmPosition: boolean,
    lvl: number
  ) {
    if (paraItem.instanceType === InstanceType.ParaRun) {
      const startIndex =
        true === fromElmPosition
          ? this.elementPosition.get(lvl) - 1
          : paraItem.children.length - 1

      for (let i = startIndex; i >= 0; --i) {
        if (this.isEmpty()) return

        const item = paraItem.children.at(i)
        if (item) {
          this.updateChildPosition(i, lvl)
          this.add(item)
        }
      }
    } else {
      if (this.isEmpty()) return

      const childrenCount = paraItem.children.length
      let curIndex =
        true === fromElmPosition
          ? this.elementPosition.get(lvl)
          : childrenCount - 1

      while (curIndex >= 0 && curIndex <= childrenCount) {
        this.updateChildPosition(curIndex, lvl)
        this.updatePrevElementsPosInParaItem(
          paraItem.children.at(curIndex)!,
          fromElmPosition,
          lvl + 1
        )

        curIndex--
      }
    }
  }
  updatePrevRunElementsPosInParagraph(paragraph: Paragraph) {
    const elementPosition = this.elementPosition
    let index = elementPosition.get(0)

    let paraItem = paragraph.children.at(index)
    if (paraItem) {
      this.updateChildPosition(index, 0)
      this.updatePrevElementsPosInParaItem(paraItem, true, 1)
    }
    index--

    while (index >= 0) {
      if (this.isEmpty()) break
      paraItem = paragraph.children.at(index)
      if (paraItem) {
        this.updateChildPosition(index, 0)
        this.updatePrevElementsPosInParaItem(paraItem, false, 1)
      }
      index--
    }

    this.amendEnd()
  }

  private updateNextElementsPosInParaItem(
    paraItem: ParaRun | ParaHyperlink,
    fromElmPosition: boolean,
    lvl: number
  ) {
    if (paraItem.instanceType === InstanceType.ParaRun) {
      const startIndex =
        true === fromElmPosition ? this.elementPosition.get(lvl) : 0

      for (let i = startIndex, l = paraItem.children.length; i < l; ++i) {
        if (this.isEmpty()) return

        const item = paraItem.children.at(i)
        if (item) {
          this.updateChildPosition(i, lvl)
          this.add(item)
        }
      }
    } else {
      if (this.isEmpty()) return

      let curIndex =
        true === fromElmPosition ? this.elementPosition.get(lvl) : 0
      const contentLen = paraItem.children.length

      let run = paraItem.children.at(curIndex)
      if (run) {
        this.updateChildPosition(curIndex, lvl)
        this.updateNextElementsPosInParaItem(run, fromElmPosition, lvl + 1)
      }
      curIndex++

      while (curIndex < contentLen) {
        if (this.isEmpty()) return
        run = paraItem.children.at(curIndex)
        if (run) {
          this.updateChildPosition(curIndex, lvl)
          this.updateNextElementsPosInParaItem(run, false, lvl + 1)
        }

        curIndex++
      }
    }
  }

  updateNextRunElementsPosInParagraph(paragraph: Paragraph) {
    const elementPosition = this.elementPosition
    let curIndex = elementPosition.get(0)
    const contentLen = paragraph.children.length

    let paraItem = paragraph.children.at(curIndex)

    if (paraItem) {
      this.updateChildPosition(curIndex, 0)
      this.updateNextElementsPosInParaItem(paraItem, true, 1)
    }
    curIndex++
    while (curIndex < contentLen) {
      if (this.isEmpty()) break

      paraItem = paragraph.children.at(curIndex)
      if (paraItem) {
        this.updateChildPosition(curIndex, 0)
        this.updateNextElementsPosInParaItem(paraItem, false, 1)
      }
      if (this.count <= 0) break

      curIndex++
    }

    this.amendEnd()
  }
}
