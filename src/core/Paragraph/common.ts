import { Nullable } from '../../../liber/pervasive'
import { InstanceType } from '../instanceTypes'
import { ParaHyperlink } from './ParaContent/ParaHyperlink'
import { ParaRun } from './ParaContent/ParaRun'
import { Paragraph } from './Paragraph'
import { ParagraphItem } from './type'

export const LEVEL_PARAGRAPH = 0
export const LEVEL_PARAHYPERLINK = 1

export class ParaItemPosInfo {
  line: number
  index: number
  colIndex: number
  constructor(line: number, index: number, colIndex: number) {
    this.line = line
    this.index = index
    this.colIndex = colIndex
  }
}

export interface AdjacentPosition {
  paragraph: Paragraph
  pos: ParagraphElementPosition
}
export interface ParagraphAdjacentPosition {
  adjacentPos?: Nullable<AdjacentPosition>
  enities: Array<Paragraph | ParaHyperlink | ParaRun>
}

export interface ParagraphElementAdjacentPosition {
  adjacentPos: Nullable<AdjacentPosition>
  level: number
}

export class ParagraphElementPosition {
  positonIndices: number[] // [run/hyper, runItem/run, runItem]
  level: number
  constructor() {
    this.positonIndices = [0, 0, 0]
    this.level = 0
  }

  reset() {
    this.positonIndices = [0, 0, 0]
    this.level = 0
  }

  add(pos: number) {
    this.positonIndices[this.level] = pos
    this.level++
  }

  updatePosByLevel(index: number, l: number) {
    this.positonIndices[l] = index
    this.level = l + 1
  }

  increaseByOne() {
    const lastDepthPos = this.positonIndices[this.level - 1]
    if (lastDepthPos != null) {
      this.positonIndices[this.level - 1] += 1
    }
  }

  setPosByLevel(indexOfPosition: number, lvl: number) {
    this.positonIndices[lvl] = indexOfPosition
  }

  setByPosition(position: ParagraphElementPosition) {
    const l = position.level
    for (let i = 0; i < l; i++) {
      this.positonIndices[i] = position.positonIndices[i]
    }

    this.level = position.level

    // if (this.positonIndices.length > this.level) {
    //   this.positonIndices.length = this.level
    // }
  }

  get(lvl: number) {
    return this.positonIndices[lvl]
  }

  getLevel() {
    return this.level - 1
  }

  getlastLevelData() {
    return this.positonIndices[this.level - 1]
  }

  decreaseLevel(valueOfCount) {
    this.level = Math.max(0, this.level - valueOfCount)
  }

  clone() {
    const elementPosition = new ParagraphElementPosition()

    const count = this.positonIndices.length
    for (let i = 0; i < count; i++) {
      elementPosition.add(this.positonIndices[i])
    }

    elementPosition.level = this.level

    return elementPosition
  }

  compare(pos: ParagraphElementPosition) {
    let lvl = 0

    const len1 = this.positonIndices.length
    const len2 = pos.positonIndices.length
    const minLen = Math.min(len1, len2)

    while (lvl < minLen) {
      if (this.positonIndices[lvl] === pos.positonIndices[lvl]) {
        lvl++
        continue
      } else if (this.positonIndices[lvl] > pos.positonIndices[lvl]) return 1
      else return -1
    }

    if (len1 !== len2) return -1

    return 0
  }
}

export interface ParaRunStateSelection {
  isUse: boolean
  startIndex: number
  endIndex: number
}

export interface ParaRunState {
  selection: ParaRunStateSelection
  elementIndex: number
}

class ParagraphTextGetter {
  private text: string
  private isFinished: boolean = false
  private breakOnParaEnd: boolean = true
  private paraEndWithSpace: boolean = false
  constructor() {
    this.text = ''
  }

  reset() {
    this.text = ''
    this.isFinished = false
    this.breakOnParaEnd = true
    this.paraEndWithSpace = false
  }

  setSettings(opts: { breakOnParaEnd?: boolean; paraEndWithSpace?: boolean }) {
    if (opts.breakOnParaEnd != null) {
      this.breakOnParaEnd = opts.breakOnParaEnd
    }
    if (opts.paraEndWithSpace != null) {
      this.paraEndWithSpace = opts.paraEndWithSpace
    }
  }

  collectFromParaItem(paraItem: ParagraphItem) {
    if (paraItem.instanceType === InstanceType.ParaRun) {
      if (this.isFinished) {
        return
      }
      const contentLen = paraItem.children.length

      for (let i = 0; i < contentLen; i++) {
        const item = paraItem.children.at(i)!

        let isBreak = false

        switch (item.instanceType) {
          case InstanceType.ParaEnd: {
            if (true === this.breakOnParaEnd) {
              this.isFinished = true
              isBreak = true
            }

            if (true === this.paraEndWithSpace) this.text += ' '

            break
          }

          case InstanceType.ParaText:
            this.text += String.fromCodePoint(item.value)
            break
          case InstanceType.ParaSpace:
          case InstanceType.ParaTab:
            this.text += ' '
            break
        }

        if (true === isBreak) break
      }
    } else {
      const l = paraItem.children.length
      for (let i = 0; i < l; i++) {
        this.collectFromParaItem(paraItem.children.at(i)!)
      }
    }
  }
  getText() {
    return this.text
  }
}

const gParagraphTextGetter = new ParagraphTextGetter()

export function getParagraphTextGetter(opts?: {
  breakOnParaEnd?: boolean
  paraEndWithSpace?: boolean
}): ParagraphTextGetter {
  gParagraphTextGetter.reset()
  if (opts) {
    gParagraphTextGetter.setSettings(opts)
  }
  return gParagraphTextGetter
}
export class ParagraphLocationInfo {
  pos: ParagraphElementPosition
  found: boolean
  line: number
  stage: number
  isShift: boolean
  punctuation: boolean
  first: boolean
  needUpdatePositon: boolean
  constructor() {
    this.pos = new ParagraphElementPosition()
    this.found = false

    this.line = -1

    this.stage = 0
    this.isShift = false
    this.punctuation = false
    this.first = true
    this.needUpdatePositon = false
  }
}
