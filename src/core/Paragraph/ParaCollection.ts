import { InstanceType } from '../instanceTypes'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../common/utils'
import { Paragraph } from './Paragraph'
import { Dict, Nullable } from '../../../liber/pervasive'

interface CollectItem {
  item: Paragraph
  selectedAll: boolean
}
export class ParaCollection {
  elements: CollectItem[]

  constructor() {
    this.elements = []
  }

  reset() {
    this.elements.length = 0
  }

  isEmpty() {
    return this.elements.length <= 0
  }

  add(para: Paragraph, selectedAll = false) {
    const selectedElement = { item: para, selectedAll }
    this.elements.push(selectedElement)
  }

  endCollect() {
    const count = this.elements.length
    this.elements.forEach(({ item: para }) => {
      if (para?.instanceType === InstanceType.Paragraph && count > 1) {
        para.correctChildren()
      }
    })
  }

  clone() {
    const collection = new ParaCollection()
    const s = turnOffRecordChanges()
    for (let i = 0, l = this.elements.length; i < l; i++) {
      const { item: para, selectedAll } = this.elements[i]
      collection.add(para.clone(para.parent), selectedAll)
    }
    restoreRecordChangesState(s)
    return collection
  }
}

export function paraCollectionFromParagraphs(
  paragraphs: Nullable<Paragraph[]>,
  fonts?: Dict
) {
  if (paragraphs && paragraphs.length > 0) {
    const paraCollection = new ParaCollection()
    for (const para of paragraphs) {
      if (fonts) {
        para.collectAllFontNames(fonts) //FONTS
      }
      paraCollection.add(para)
    }
    return paraCollection
  } else {
    return undefined
  }
}
