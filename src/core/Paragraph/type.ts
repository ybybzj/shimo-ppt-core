import { Nullable } from '../../../liber/pervasive'
import { RunElementIndices } from '../../re/export/textContent'
import { Matrix2D } from '../graphic/Matrix'
import { ParaHyperlink } from './ParaContent/ParaHyperlink'
import { ParaRun } from './ParaContent/ParaRun'

export type ParagraphItem = ParaHyperlink | ParaRun

export interface RunElemnetOrderIndicesInfo {
  indices: RunElementIndices
  dir: 'L' | 'R'
  mirrorCode?: number
  isStartInLine?: boolean
  isEndInLine?: boolean
}

export interface CursorPosition {
  x: number
  y: number
  height?: number
  lineAndCol?: {
    line: number
    colIndex: number
  }
  transform?: Nullable<Matrix2D>
}
