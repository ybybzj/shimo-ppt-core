import {
  ParaTextForm,
  TEXTWIDTH_PER_UNIT,
  CharCode_NBSP,
} from '../../common/const/paraContent'
import {
  smallcaps_and_script_factor,
  VertAlignFactor,
  smallcaps_Factor,
} from '../../common/const/unit'
import { VertAlignType } from '../../common/const/attrs'
import { isEastAsianCharCode } from '../../../common/utils'
import { InstanceType } from '../../instanceTypes'
import { FontKit } from '../../../fonts/FontKit'
import { GraphicsBoundsChecker } from '../../graphic/Bounds'
import { CanvasDrawer } from '../../graphic/CanvasDrawer'
import { getFontClass } from '../../../fonts/FontClassification'
import { TextPr } from '../../textAttributes/TextPr'
import {
  genLetterStatusInfo,
  isArabicLetter,
  isSkippedLetter,
  LetterStatusCode,
  updateSkippedStatus,
} from '../../../lib/utils/arabicReshaper/statusCode'
import { FontClassValues } from '../../../fonts/const'
import { PoolAllocator, Recyclable } from '../../../common/allocator'

const numberCodes = new Set([
  0x0030, 0x0031, 0x0032, 0x0033, 0x0034, 0x0035, 0x0036, 0x0037, 0x0038,
  0x0039,
])

const letterReg = /[a-zA-Z]/
export class ParaText extends Recyclable<ParaText> {
  readonly instanceType = InstanceType.ParaText

  private _value: number
  private _isComplex = false
  width: number
  renderWidth: number
  actualWidth: number
  form: number
  spaceAfter!: boolean
  arabicFormValue?: number
  arabicStatusCode: LetterStatusCode

  private _char: string

  get value(): number {
    return this._value
  }
  get char() {
    return this._char
  }

  get isLetter() {
    return letterReg.test(this._char)
  }
  get isComplex() {
    return this._isComplex
  }

  private static allocator: PoolAllocator<ParaText, [number]> =
    PoolAllocator.getAllocator<ParaText, [number]>(ParaText)
  static new(charCode = 0x00) {
    return ParaText.allocator.allocate(charCode)
  }
  static recycle(paraText: ParaText) {
    return ParaText.allocator.recycle(paraText)
  }
  constructor(charCode = 0x00) {
    super()
    this.width = 0x00000000 | 0
    this.renderWidth = 0x00000000 | 0
    this.actualWidth = 0x00000000 | 0

    this._value = charCode
    this._char = String.fromCodePoint(charCode)

    this.form = 0x00000000 | 0

    this.setSpaceAfter(this._canAddSpaceAfter())
    const { statusCode } = genLetterStatusInfo(charCode)
    this.arabicStatusCode = statusCode
  }

  reset(charCode = 0x00) {
    this.width = 0x00000000 | 0
    this.renderWidth = 0x00000000 | 0
    this.actualWidth = 0x00000000 | 0

    this._value = charCode
    this._char = String.fromCodePoint(charCode)

    this.form = 0x00000000 | 0

    this.setSpaceAfter(this._canAddSpaceAfter())
    const { statusCode } = genLetterStatusInfo(charCode)
    this.arabicStatusCode = statusCode
    this.arabicFormValue = undefined
  }

  public getDrawCharCode() {
    return this.arabicFormValue ?? this.value
  }
  draw(
    x: number,
    y: number,
    context: GraphicsBoundsChecker | CanvasDrawer,
    mirrorCode?: number
  ) {
    if (this.isSkipped()) {
      return
    }
    if (context.instanceType === InstanceType.GraphicsBoundsChecker) {
      context.fillString(x, y)
      return
    }
    const charCode = mirrorCode ?? this.getDrawCharCode()

    let fontFactor = 1
    if (
      this.form & ParaTextForm.FONTKOEF_SCRIPT &&
      this.form & ParaTextForm.FONTKOEF_SMALLCAPS
    ) {
      fontFactor = smallcaps_and_script_factor
    } else if (this.form & ParaTextForm.FONTKOEF_SCRIPT) {
      fontFactor = VertAlignFactor.Size
    } else if (this.form & ParaTextForm.FONTKOEF_SMALLCAPS) {
      fontFactor = smallcaps_Factor
    }

    context.setFontClass(
      ((this.form >> 8) & 0xff) as FontClassValues,
      fontFactor
    )

    const result =
      this.form & ParaTextForm.CAPITALS
        ? String.fromCodePoint(charCode).toUpperCase().codePointAt(0)!
        : charCode

    if (true !== this.isNBSP()) {
      context.fillString(x, y, String.fromCodePoint(result))
    }
  }

  measure(
    fontKit: FontKit,
    textPr: TextPr,
    onWidthChanged?: (item: ParaText) => void
  ) {
    if (this.isSkipped()) {
      this.width = 0
      this.renderWidth = 0
      return true
    }
    const isArabic = this.isArabic()
    const charCode = isArabic ? this.getDrawCharCode() : this.value

    let codeForMeasure = charCode
    let isCapitals = false

    if (true === textPr.caps || true === textPr.smallCaps) {
      this.form |= ParaTextForm.CAPITALS
      codeForMeasure = String.fromCodePoint(charCode)
        .toUpperCase()
        .codePointAt(0)!
      isCapitals = codeForMeasure === charCode
    } else {
      this.form &= ParaTextForm.NON_CAPITALS
      isCapitals = false
    }

    if (textPr.vertAlign !== VertAlignType.Baseline) {
      this.form |= ParaTextForm.FONTKOEF_SCRIPT
    } else {
      this.form &= ParaTextForm.NON_FONTKOEF_SCRIPT
    }

    if (
      true !== textPr.caps &&
      true === textPr.smallCaps &&
      false === isCapitals
    ) {
      this.form |= ParaTextForm.FONTKOEF_SMALLCAPS
    } else {
      this.form &= ParaTextForm.NON_FONTKOEF_SMALLCAPS
    }

    const hint = textPr.rFonts.hint
    const isCS = textPr.cs || isArabic
    const ea = textPr.lang?.eastAsia

    const fontClass = getFontClass(codeForMeasure, hint, ea, isCS)

    const _0Byte = (this.form >> 0) & 0xff
    const _2Byte = (this.form >> 16) & 0xff

    this.form = _0Byte | ((fontClass & 0xff) << 8) | (_2Byte << 16)

    let fontFactor = 1
    this._isComplex = false
    if (
      this.form & ParaTextForm.FONTKOEF_SCRIPT &&
      this.form & ParaTextForm.FONTKOEF_SMALLCAPS
    ) {
      fontFactor = smallcaps_and_script_factor
    } else if (this.form & ParaTextForm.FONTKOEF_SCRIPT) {
      fontFactor = VertAlignFactor.Size
    } else if (this.form & ParaTextForm.FONTKOEF_SMALLCAPS) {
      fontFactor = smallcaps_Factor
    }

    const fontSize = textPr.fontSize!
    if (1 !== fontFactor) {
      fontFactor = ((fontSize * fontFactor * 2 + 0.5) | 0) / 2 / fontSize
      this._isComplex = true
    }

    fontKit.setTextPr(textPr)
    fontKit.setFontClass(fontClass, fontFactor)
    const { width, isFallback } = fontKit.MeasureCode(
      codeForMeasure,
      undefined,
      undefined,
      undefined
    )

    this.actualWidth = (Math.max(width, 0) * TEXTWIDTH_PER_UNIT) | 0

    if (textPr.spacing != null && textPr.spacing !== 0) {
      this._isComplex = true
    }
    const result =
      (Math.max(width + textPr.spacing!, 0) * TEXTWIDTH_PER_UNIT) | 0

    if (this.width !== result) {
      this.width = result
      this.renderWidth = result

      if (onWidthChanged) {
        onWidthChanged(this)
      }
    }

    return !isFallback
  }

  clone() {
    return ParaText.new(this.value)
  }

  isNBSP() {
    return this.value === CharCode_NBSP
  }

  isPunctuation() {
    if (undefined !== punctuationFlagMap[this.value]) return true

    return false
  }

  isNumber() {
    return numberCodes.has(this.value)
  }

  needSpaceAfter() {
    return this.form & ParaTextForm.SPACEAFTER ? true : false
  }

  setSpaceAfter(needSpaceAfter: boolean) {
    if (needSpaceAfter) this.form |= ParaTextForm.SPACEAFTER
    else this.form &= ParaTextForm.NON_SPACEAFTER
  }

  private _canAddSpaceAfter() {
    if (0x002d === this.value || 0x2014 === this.value) return true

    if (isEastAsianCharCode(this.value) && this.canBeLineEnd()) {
      return true
    }

    return false
  }

  canBeLineStart() {
    if (this.isNBSP()) return false

    const flag = punctuationFlagMap[this.value] ?? 0
    return !(flag & PFLAG_CANT_BE_AT_BEGIN)
  }

  canBeLineEnd() {
    if (this.isNBSP()) return false

    const flag = punctuationFlagMap[this.value] ?? 0
    return !(flag & PFLAG_CANT_BE_AT_END)
  }

  canStartAutoCorrect() {
    // 34 "
    // 39 '
    // 45 -

    return 34 === this.value || 39 === this.value || 45 === this.value
  }
  isSkipped() {
    return isSkippedLetter(this.arabicStatusCode)
  }

  setSkipStatus() {
    this.arabicStatusCode = updateSkippedStatus(this.arabicStatusCode, true)
    this.width = 0
    this.renderWidth = 0
    this.actualWidth = 0
  }

  updateWidthAndUnsetSkip(width: number, actualWidth: number) {
    this.arabicStatusCode = updateSkippedStatus(this.arabicStatusCode, false)
    this.width = width
    this.renderWidth = width
    this.actualWidth = actualWidth
  }

  getWidth() {
    return this.width / TEXTWIDTH_PER_UNIT
  }

  getVisibleWidth() {
    return this.renderWidth / TEXTWIDTH_PER_UNIT
  }

  getActualWidth() {
    return this.actualWidth / TEXTWIDTH_PER_UNIT
  }

  canMerge() {
    return this.actualWidth === this.renderWidth
  }

  isArabic() {
    return isArabicLetter(this.arabicStatusCode)
  }
}

const PFLAG_BASE = 0x0001
const PFLAG_CANT_BE_AT_BEGIN = 0x0010
const PFLAG_CANT_BE_AT_END = 0x0020
const PFLAG_EAST_ASIAN = 0x0100
const PFLAG_CANT_BE_AT_BEGIN_E = 0x0002
const PFLAG_CANT_BE_AT_END_E = 0x0004

const punctuationFlagMap: number[] = []
punctuationFlagMap[0x0021] =
  PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN | PFLAG_CANT_BE_AT_BEGIN_E // !
punctuationFlagMap[0x0022] = PFLAG_BASE // "
punctuationFlagMap[0x0023] = PFLAG_BASE // #
punctuationFlagMap[0x0024] = PFLAG_BASE | PFLAG_CANT_BE_AT_END // $
punctuationFlagMap[0x0025] =
  PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN | PFLAG_CANT_BE_AT_BEGIN_E // %
punctuationFlagMap[0x0026] = PFLAG_BASE // &
punctuationFlagMap[0x0027] = PFLAG_BASE // '
punctuationFlagMap[0x0028] =
  PFLAG_BASE | PFLAG_CANT_BE_AT_END | PFLAG_CANT_BE_AT_END_E // (
punctuationFlagMap[0x0029] =
  PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN | PFLAG_CANT_BE_AT_BEGIN_E // )
punctuationFlagMap[0x002a] = PFLAG_BASE // *
punctuationFlagMap[0x002b] = PFLAG_BASE // +
punctuationFlagMap[0x002c] =
  PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN | PFLAG_CANT_BE_AT_BEGIN_E // ,
punctuationFlagMap[0x002d] = PFLAG_BASE // -
punctuationFlagMap[0x002e] =
  PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN | PFLAG_CANT_BE_AT_BEGIN_E // .
punctuationFlagMap[0x002f] = PFLAG_BASE // /
punctuationFlagMap[0x003a] =
  PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN | PFLAG_CANT_BE_AT_BEGIN_E // :
punctuationFlagMap[0x003b] =
  PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN | PFLAG_CANT_BE_AT_BEGIN_E // ;
punctuationFlagMap[0x003c] = PFLAG_BASE // <
punctuationFlagMap[0x003d] = PFLAG_BASE // =
punctuationFlagMap[0x003e] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // >
punctuationFlagMap[0x003f] =
  PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN | PFLAG_CANT_BE_AT_BEGIN_E // ?
punctuationFlagMap[0x0040] = PFLAG_BASE // @
punctuationFlagMap[0x005b] =
  PFLAG_BASE | PFLAG_CANT_BE_AT_END | PFLAG_CANT_BE_AT_END_E // [
punctuationFlagMap[0x005c] = PFLAG_BASE // \
punctuationFlagMap[0x005d] =
  PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN | PFLAG_CANT_BE_AT_BEGIN_E // ]
punctuationFlagMap[0x005e] = PFLAG_BASE // ^
punctuationFlagMap[0x005f] = PFLAG_BASE // _
punctuationFlagMap[0x0060] = PFLAG_BASE // `
punctuationFlagMap[0x007b] =
  PFLAG_BASE | PFLAG_CANT_BE_AT_END | PFLAG_CANT_BE_AT_END_E // {
punctuationFlagMap[0x007c] = PFLAG_BASE // |
punctuationFlagMap[0x007d] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // }
punctuationFlagMap[0x007e] = PFLAG_BASE // ~

punctuationFlagMap[0x00a1] = PFLAG_BASE // ¡
punctuationFlagMap[0x00a2] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // ¢
punctuationFlagMap[0x00a3] = PFLAG_BASE | PFLAG_CANT_BE_AT_END // £
punctuationFlagMap[0x00a4] = PFLAG_BASE // ¤
punctuationFlagMap[0x00a5] = PFLAG_BASE | PFLAG_CANT_BE_AT_END // ¥
punctuationFlagMap[0x00a6] = PFLAG_BASE // ¦
punctuationFlagMap[0x00a7] = PFLAG_BASE // §
punctuationFlagMap[0x00a8] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // ¨
punctuationFlagMap[0x00a9] = PFLAG_BASE // ©
punctuationFlagMap[0x00aa] = PFLAG_BASE // ª
punctuationFlagMap[0x00ab] = PFLAG_BASE // «
punctuationFlagMap[0x00ac] = PFLAG_BASE // ¬
punctuationFlagMap[0x00ad] = PFLAG_BASE // ­
punctuationFlagMap[0x00ae] = PFLAG_BASE // ®
punctuationFlagMap[0x00af] = PFLAG_BASE // ¯
punctuationFlagMap[0x00b0] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // °
punctuationFlagMap[0x00b1] = PFLAG_BASE // ±
punctuationFlagMap[0x00b4] = PFLAG_BASE // ´
punctuationFlagMap[0x00b6] = PFLAG_BASE // ¶
punctuationFlagMap[0x00b7] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // ·
punctuationFlagMap[0x00b8] = PFLAG_BASE // ¸
punctuationFlagMap[0x00ba] = PFLAG_BASE // º
punctuationFlagMap[0x00bb] = PFLAG_BASE // »
punctuationFlagMap[0x00bb] = PFLAG_BASE // »
punctuationFlagMap[0x00bf] = PFLAG_BASE // ¿

punctuationFlagMap[0x2010] = PFLAG_BASE // ‐
punctuationFlagMap[0x2011] = PFLAG_BASE // ‑
punctuationFlagMap[0x2012] = PFLAG_BASE // ‒
punctuationFlagMap[0x2013] = PFLAG_BASE // –
punctuationFlagMap[0x2014] = PFLAG_BASE // —
punctuationFlagMap[0x2015] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // ―
punctuationFlagMap[0x2016] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // ‖
punctuationFlagMap[0x2017] = PFLAG_BASE // ‗
punctuationFlagMap[0x2018] = PFLAG_BASE | PFLAG_CANT_BE_AT_END // ‘
punctuationFlagMap[0x2019] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // ’
punctuationFlagMap[0x201a] = PFLAG_BASE // ‚
punctuationFlagMap[0x201b] = PFLAG_BASE // ‛
punctuationFlagMap[0x201c] = PFLAG_BASE | PFLAG_CANT_BE_AT_END // “
punctuationFlagMap[0x201d] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // ”
punctuationFlagMap[0x201e] = PFLAG_BASE // „
punctuationFlagMap[0x201f] = PFLAG_BASE // ‟
punctuationFlagMap[0x2020] = PFLAG_BASE // †
punctuationFlagMap[0x2021] = PFLAG_BASE // ‡
punctuationFlagMap[0x2022] = PFLAG_BASE // •
punctuationFlagMap[0x2023] = PFLAG_BASE // ‣
punctuationFlagMap[0x2024] = PFLAG_BASE // ․
punctuationFlagMap[0x2025] = PFLAG_BASE // ‥
punctuationFlagMap[0x2026] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // …
punctuationFlagMap[0x2027] = PFLAG_BASE // ‧
punctuationFlagMap[0x2030] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // ‰
punctuationFlagMap[0x2031] = PFLAG_BASE // ‱
punctuationFlagMap[0x2032] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // ′
punctuationFlagMap[0x2033] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // ″
punctuationFlagMap[0x2034] = PFLAG_BASE // ‴
punctuationFlagMap[0x2035] = PFLAG_BASE // ‵
punctuationFlagMap[0x2036] = PFLAG_BASE // ‶
punctuationFlagMap[0x2037] = PFLAG_BASE // ‷
punctuationFlagMap[0x2038] = PFLAG_BASE // ‸
punctuationFlagMap[0x2039] = PFLAG_BASE // ‹
punctuationFlagMap[0x203a] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // ›
punctuationFlagMap[0x203b] = PFLAG_BASE // ※
punctuationFlagMap[0x203c] = PFLAG_BASE // ‼
punctuationFlagMap[0x203d] = PFLAG_BASE // ‽
punctuationFlagMap[0x203e] = PFLAG_BASE // ‾
punctuationFlagMap[0x203f] = PFLAG_BASE // ‿
punctuationFlagMap[0x2040] = PFLAG_BASE // ⁀
punctuationFlagMap[0x2041] = PFLAG_BASE // ⁁
punctuationFlagMap[0x2042] = PFLAG_BASE // ⁂
punctuationFlagMap[0x2043] = PFLAG_BASE // ⁃
punctuationFlagMap[0x2044] = PFLAG_BASE // ⁄
punctuationFlagMap[0x2045] = PFLAG_BASE // ⁅
punctuationFlagMap[0x2046] = PFLAG_BASE // ⁆
punctuationFlagMap[0x2047] = PFLAG_BASE // ⁇
punctuationFlagMap[0x2048] = PFLAG_BASE // ⁈
punctuationFlagMap[0x2049] = PFLAG_BASE // ⁉
punctuationFlagMap[0x204a] = PFLAG_BASE // ⁊
punctuationFlagMap[0x204b] = PFLAG_BASE // ⁋
punctuationFlagMap[0x204c] = PFLAG_BASE // ⁌
punctuationFlagMap[0x204d] = PFLAG_BASE // ⁍
punctuationFlagMap[0x204e] = PFLAG_BASE // ⁎
punctuationFlagMap[0x204f] = PFLAG_BASE // ⁏
punctuationFlagMap[0x2050] = PFLAG_BASE // ⁐
punctuationFlagMap[0x2051] = PFLAG_BASE // ⁑
punctuationFlagMap[0x2052] = PFLAG_BASE // ⁒
punctuationFlagMap[0x2053] = PFLAG_BASE // ⁓
punctuationFlagMap[0x2054] = PFLAG_BASE // ⁔
punctuationFlagMap[0x2055] = PFLAG_BASE // ⁕
punctuationFlagMap[0x2056] = PFLAG_BASE // ⁖
punctuationFlagMap[0x2057] = PFLAG_BASE // ⁗
punctuationFlagMap[0x2058] = PFLAG_BASE // ⁘
punctuationFlagMap[0x2059] = PFLAG_BASE // ⁙
punctuationFlagMap[0x205a] = PFLAG_BASE // ⁚
punctuationFlagMap[0x205b] = PFLAG_BASE // ⁛
punctuationFlagMap[0x205c] = PFLAG_BASE // ⁜
punctuationFlagMap[0x205d] = PFLAG_BASE // ⁝
punctuationFlagMap[0x205e] = PFLAG_BASE // ⁞

punctuationFlagMap[0x3001] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // 、
punctuationFlagMap[0x3002] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // 。
punctuationFlagMap[0x3003] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // 〃
punctuationFlagMap[0x3004] = PFLAG_BASE // 〄
punctuationFlagMap[0x3005] = PFLAG_BASE // 々
punctuationFlagMap[0x3006] = PFLAG_BASE // 〆
punctuationFlagMap[0x3007] = PFLAG_BASE // 〇
punctuationFlagMap[0x3008] = PFLAG_BASE | PFLAG_CANT_BE_AT_END // 〈
punctuationFlagMap[0x3009] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // 〉
punctuationFlagMap[0x300a] = PFLAG_BASE | PFLAG_CANT_BE_AT_END // 《
punctuationFlagMap[0x300b] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // 》
punctuationFlagMap[0x300c] = PFLAG_BASE | PFLAG_CANT_BE_AT_END // 「
punctuationFlagMap[0x300d] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // 」
punctuationFlagMap[0x300e] = PFLAG_BASE | PFLAG_CANT_BE_AT_END // 『
punctuationFlagMap[0x300f] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // 』
punctuationFlagMap[0x3010] = PFLAG_BASE | PFLAG_CANT_BE_AT_END // 【
punctuationFlagMap[0x3011] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // 】
punctuationFlagMap[0x3012] = PFLAG_BASE // 〒
punctuationFlagMap[0x3013] = PFLAG_BASE // 〓
punctuationFlagMap[0x3014] = PFLAG_BASE | PFLAG_CANT_BE_AT_END //〔
punctuationFlagMap[0x3015] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // 〕
punctuationFlagMap[0x3016] = PFLAG_BASE | PFLAG_CANT_BE_AT_END //〖
punctuationFlagMap[0x3017] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // 〗
punctuationFlagMap[0x3018] = PFLAG_BASE // 〘
punctuationFlagMap[0x3019] = PFLAG_BASE // 〙
punctuationFlagMap[0x301a] = PFLAG_BASE // 〚
punctuationFlagMap[0x301b] = PFLAG_BASE // 〛
punctuationFlagMap[0x301c] = PFLAG_BASE // 〜
punctuationFlagMap[0x301d] = PFLAG_BASE | PFLAG_CANT_BE_AT_END // 〝
punctuationFlagMap[0x301e] = PFLAG_BASE | PFLAG_CANT_BE_AT_BEGIN // 〞
punctuationFlagMap[0x301f] = PFLAG_BASE // 〟

punctuationFlagMap[0xff01] =
  PFLAG_BASE |
  PFLAG_EAST_ASIAN |
  PFLAG_CANT_BE_AT_BEGIN |
  PFLAG_CANT_BE_AT_BEGIN_E // ！
punctuationFlagMap[0xff02] =
  PFLAG_BASE | PFLAG_EAST_ASIAN | PFLAG_CANT_BE_AT_BEGIN // ＂
punctuationFlagMap[0xff03] = PFLAG_BASE | PFLAG_EAST_ASIAN // ＃
punctuationFlagMap[0xff04] =
  PFLAG_BASE | PFLAG_EAST_ASIAN | PFLAG_CANT_BE_AT_END // ＄
punctuationFlagMap[0xff05] =
  PFLAG_BASE |
  PFLAG_EAST_ASIAN |
  PFLAG_CANT_BE_AT_BEGIN |
  PFLAG_CANT_BE_AT_BEGIN_E // ％
punctuationFlagMap[0xff06] = PFLAG_BASE | PFLAG_EAST_ASIAN // ＆
punctuationFlagMap[0xff07] =
  PFLAG_BASE | PFLAG_EAST_ASIAN | PFLAG_CANT_BE_AT_BEGIN // ＇
punctuationFlagMap[0xff08] =
  PFLAG_BASE | PFLAG_EAST_ASIAN | PFLAG_CANT_BE_AT_END | PFLAG_CANT_BE_AT_END_E // （
punctuationFlagMap[0xff09] =
  PFLAG_BASE |
  PFLAG_EAST_ASIAN |
  PFLAG_CANT_BE_AT_BEGIN |
  PFLAG_CANT_BE_AT_BEGIN_E // )
punctuationFlagMap[0xff0a] = PFLAG_BASE | PFLAG_EAST_ASIAN // ＊
punctuationFlagMap[0xff0b] = PFLAG_BASE | PFLAG_EAST_ASIAN // ＋
punctuationFlagMap[0xff0c] =
  PFLAG_BASE |
  PFLAG_EAST_ASIAN |
  PFLAG_CANT_BE_AT_BEGIN |
  PFLAG_CANT_BE_AT_BEGIN_E // ，
punctuationFlagMap[0xff0d] = PFLAG_BASE | PFLAG_EAST_ASIAN // －
punctuationFlagMap[0xff0e] =
  PFLAG_BASE |
  PFLAG_EAST_ASIAN |
  PFLAG_CANT_BE_AT_BEGIN |
  PFLAG_CANT_BE_AT_BEGIN_E // ．
punctuationFlagMap[0xff0f] = PFLAG_BASE | PFLAG_EAST_ASIAN // ／
punctuationFlagMap[0xff1a] =
  PFLAG_BASE |
  PFLAG_EAST_ASIAN |
  PFLAG_CANT_BE_AT_BEGIN |
  PFLAG_CANT_BE_AT_BEGIN_E // ：
punctuationFlagMap[0xff1b] =
  PFLAG_BASE |
  PFLAG_EAST_ASIAN |
  PFLAG_CANT_BE_AT_BEGIN |
  PFLAG_CANT_BE_AT_BEGIN_E // ；
punctuationFlagMap[0xff1c] = PFLAG_BASE | PFLAG_EAST_ASIAN // ＜
punctuationFlagMap[0xff1d] = PFLAG_BASE | PFLAG_EAST_ASIAN // ＝
punctuationFlagMap[0xff1e] = PFLAG_BASE | PFLAG_EAST_ASIAN // ＞
punctuationFlagMap[0xff1f] =
  PFLAG_BASE |
  PFLAG_EAST_ASIAN |
  PFLAG_CANT_BE_AT_BEGIN |
  PFLAG_CANT_BE_AT_BEGIN_E // ？
punctuationFlagMap[0xff20] = PFLAG_BASE | PFLAG_EAST_ASIAN // ＠
punctuationFlagMap[0xff3b] =
  PFLAG_BASE | PFLAG_EAST_ASIAN | PFLAG_CANT_BE_AT_END | PFLAG_CANT_BE_AT_END_E // ［
punctuationFlagMap[0xff3c] = PFLAG_BASE | PFLAG_EAST_ASIAN // ＼
punctuationFlagMap[0xff3d] =
  PFLAG_BASE |
  PFLAG_EAST_ASIAN |
  PFLAG_CANT_BE_AT_BEGIN |
  PFLAG_CANT_BE_AT_BEGIN_E // ］
punctuationFlagMap[0xff3e] = PFLAG_BASE | PFLAG_EAST_ASIAN // ＾
punctuationFlagMap[0xff3f] = PFLAG_BASE | PFLAG_EAST_ASIAN // ＿
punctuationFlagMap[0xff40] =
  PFLAG_BASE | PFLAG_EAST_ASIAN | PFLAG_CANT_BE_AT_BEGIN // ｀
punctuationFlagMap[0xff5b] =
  PFLAG_BASE | PFLAG_EAST_ASIAN | PFLAG_CANT_BE_AT_END | PFLAG_CANT_BE_AT_END_E // ｛
punctuationFlagMap[0xff5c] =
  PFLAG_BASE | PFLAG_EAST_ASIAN | PFLAG_CANT_BE_AT_BEGIN // ｜
punctuationFlagMap[0xff5d] =
  PFLAG_BASE |
  PFLAG_EAST_ASIAN |
  PFLAG_CANT_BE_AT_BEGIN |
  PFLAG_CANT_BE_AT_BEGIN_E // ｝
punctuationFlagMap[0xff5e] =
  PFLAG_BASE | PFLAG_EAST_ASIAN | PFLAG_CANT_BE_AT_BEGIN // ～
punctuationFlagMap[0xff5f] = PFLAG_BASE | PFLAG_EAST_ASIAN // ｟
punctuationFlagMap[0xff60] = PFLAG_BASE | PFLAG_EAST_ASIAN // ｠
punctuationFlagMap[0xff61] = PFLAG_BASE | PFLAG_EAST_ASIAN // ｡
punctuationFlagMap[0xff62] = PFLAG_BASE | PFLAG_EAST_ASIAN // ｢
punctuationFlagMap[0xff63] = PFLAG_BASE | PFLAG_EAST_ASIAN // ｣
punctuationFlagMap[0xff64] = PFLAG_BASE | PFLAG_EAST_ASIAN // ､
punctuationFlagMap[0xff65] = PFLAG_BASE | PFLAG_EAST_ASIAN // ･
punctuationFlagMap[0xffe0] =
  PFLAG_BASE | PFLAG_EAST_ASIAN | PFLAG_CANT_BE_AT_BEGIN // ￠
punctuationFlagMap[0xffe1] =
  PFLAG_BASE | PFLAG_EAST_ASIAN | PFLAG_CANT_BE_AT_END // ￡
punctuationFlagMap[0xffe2] = PFLAG_BASE | PFLAG_EAST_ASIAN // ￢
punctuationFlagMap[0xffe3] = PFLAG_BASE | PFLAG_EAST_ASIAN // ￣
punctuationFlagMap[0xffe4] = PFLAG_BASE | PFLAG_EAST_ASIAN // ￤
punctuationFlagMap[0xffe5] =
  PFLAG_BASE | PFLAG_EAST_ASIAN | PFLAG_CANT_BE_AT_END // ￥
punctuationFlagMap[0xffe6] = PFLAG_BASE | PFLAG_EAST_ASIAN // ￦
punctuationFlagMap[0xffe8] = PFLAG_BASE | PFLAG_EAST_ASIAN // ￨
punctuationFlagMap[0xffe9] = PFLAG_BASE | PFLAG_EAST_ASIAN // ￩
punctuationFlagMap[0xffea] = PFLAG_BASE | PFLAG_EAST_ASIAN // ￪
punctuationFlagMap[0xffeb] = PFLAG_BASE | PFLAG_EAST_ASIAN // ￫
punctuationFlagMap[0xffec] = PFLAG_BASE | PFLAG_EAST_ASIAN // ￬
punctuationFlagMap[0xffed] = PFLAG_BASE | PFLAG_EAST_ASIAN // ￭
punctuationFlagMap[0xffee] = PFLAG_BASE | PFLAG_EAST_ASIAN // ￮
