import { FontClass } from '../../../fonts/const'
import { TEXTWIDTH_PER_UNIT } from '../../common/const/paraContent'
import { InstanceType } from '../../instanceTypes'
import { FontKit } from '../../../fonts/FontKit'
import { TextContentDrawContext } from '../../common/types'
import { PoolAllocator, Recyclable } from '../../../common/allocator'

export class ParaEnd extends Recyclable<ParaEnd> {
  readonly instanceType = InstanceType.ParaEnd
  width: number
  renderWidth: number
  actualWidth: number

  private static allocator: PoolAllocator<ParaEnd, []> =
    PoolAllocator.getAllocator<ParaEnd, []>(ParaEnd)
  static new() {
    return ParaEnd.allocator.allocate()
  }
  static recycle(paraEnd: ParaEnd) {
    return ParaEnd.allocator.recycle(paraEnd)
  }
  constructor() {
    super()
    this.width = 0x00000000 | 0
    this.renderWidth = 0x00000000 | 0
    this.actualWidth = 0x00000000 | 0
  }

  reset() {
    this.width = 0x00000000 | 0
    this.renderWidth = 0x00000000 | 0
    this.actualWidth = 0x00000000 | 0
  }

  draw(
    x: number,
    y: number,
    drawContext: TextContentDrawContext,
    isEndCell: boolean,
    isForceDraw: boolean
  ) {
    if (true === isForceDraw) {
      if (drawContext.instanceType === InstanceType.GraphicsBoundsChecker) {
        drawContext.fillString(x, y)
        return
      }

      drawContext.setFontClass(FontClass.ASCII)

      if (true === isEndCell) {
        drawContext.fillString(x, y, String.fromCodePoint(0x00a4))
      } else {
        drawContext.fillString(x, y, String.fromCodePoint(0x00b6))
      }
    }
  }

  measure(fontKit: FontKit, isEndCell: boolean = false) {
    fontKit.setFontClass(FontClass.ASCII)

    const measureResult = fontKit.MeasureCode(
      String.fromCodePoint(true === isEndCell ? 0x00a4 : 0x00b6)
    )
    this.renderWidth = (measureResult.width * TEXTWIDTH_PER_UNIT) | 0

    return !measureResult.isFallback
  }

  getWidth() {
    return 0
  }

  getVisibleWidth() {
    return this.renderWidth / TEXTWIDTH_PER_UNIT
  }

  getActualWidth() {
    return this.actualWidth / TEXTWIDTH_PER_UNIT
  }
  clone() {
    return ParaEnd.new()
  }
  canStartAutoCorrect() {
    return false
  }
}
