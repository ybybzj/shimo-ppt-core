import { InstanceType } from '../../instanceTypes'
import {
  INIT_STATUS_CODE,
  LetterStatusCode,
} from '../../../lib/utils/arabicReshaper/statusCode'
import { ArabicReshapeItem } from '../../../lib/utils/arabicReshaper/type'
import { ParaEnd } from './ParaEnd'
import { ParaSpace } from './ParaSpace'
import { ParaText } from './ParaText'
import { ParaRunElement, RunContentElement } from './type'

export function getRunElementStatusInfo(
  runItem: ArabicReshapeItem<RunContentElement>
): {
  statusCode: LetterStatusCode
  charCode: number
} {
  switch (runItem.instanceType) {
    case InstanceType.ParaText: {
      const statusCode = runItem.arabicStatusCode
      const charCode = runItem.value
      return { statusCode, charCode }
    }
    case InstanceType.ParaSpace: {
      return {
        statusCode: INIT_STATUS_CODE,
        charCode: runItem.value,
      }
    }
    case InstanceType.ParaTab: {
      return {
        statusCode: INIT_STATUS_CODE,
        charCode: runItem.value,
      }
    }
    case InstanceType.ParaNewLine:
      return {
        statusCode: INIT_STATUS_CODE,
        charCode: 0x2028,
      }
    case InstanceType.ParaEnd: {
      return {
        statusCode: INIT_STATUS_CODE,
        charCode: 0x2029,
      }
    }
  }

  return {
    statusCode: INIT_STATUS_CODE,
    charCode: 0,
  }
}

export function updateRunElementStatusInfo(
  runItem: ArabicReshapeItem<RunContentElement>,
  statusCode: LetterStatusCode,
  formCode?: number
) {
  if (runItem.instanceType === InstanceType.ParaText) {
    runItem.arabicStatusCode = statusCode
    if (formCode != null) {
      runItem.arabicFormValue = formCode
    }
  }
}

export function canPrependNumbering(runItem: ParaRunElement) {
  switch (runItem.instanceType) {
    case InstanceType.ParaNumbering:
      return false
    case InstanceType.ParaText:
    case InstanceType.ParaSpace:
    case InstanceType.ParaTab:
    case InstanceType.ParaNewLine:
    case InstanceType.ParaEnd:
      return true
    default:
      return false
  }
}

export function recycleRunElement(item: RunContentElement) {
  switch (item.instanceType) {
    case InstanceType.ParaEnd: {
      ParaEnd.recycle(item)
      break
    }
    case InstanceType.ParaSpace: {
      ParaSpace.recycle(item)
      break
    }
    case InstanceType.ParaText: {
      ParaText.recycle(item)
      break
    }
  }
}
