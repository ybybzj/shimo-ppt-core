import { InstanceType } from '../../instanceTypes'
import { FontKit } from '../../../fonts/FontKit'
import { TextContentDrawContext } from '../../common/types'
import { TEXTWIDTH_PER_UNIT } from '../../common/const/paraContent'
import { PoolAllocator, Recyclable } from '../../../common/allocator'
const TabAlign_Symbol = 0x0022
export class ParaTab extends Recyclable<ParaTab> {
  readonly instanceType = InstanceType.ParaTab
  width: number
  renderWidth: number
  actualWidth: number
  value = 9
  private static allocator: PoolAllocator<ParaTab, []> =
    PoolAllocator.getAllocator<ParaTab, []>(ParaTab)
  static new() {
    return ParaTab.allocator.allocate()
  }
  static recycle(item: ParaTab) {
    return ParaTab.allocator.recycle(item)
  }

  constructor() {
    super()
    this.width = 0
    this.renderWidth = 0
    this.actualWidth = 0
  }

  reset() {
    this.width = 0
    this.renderWidth = 0
    this.actualWidth = 0
  }

  draw(_X: number, _Y: number, _Context: TextContentDrawContext) {}

  measure(fontkit: FontKit) {
    if (this.actualWidth <= 0) {
      fontkit.setFont({
        fontFamily: { name: 'Arial', index: -1 },
        fontSize: 10,
        italic: false,
        bold: false,
      })
      this.actualWidth = fontkit.MeasureCode(TabAlign_Symbol).width
    }
    // TODO need to consider that desired font is not loaded yet
    return true
  }

  getWidth() {
    return this.width
  }

  getVisibleWidth() {
    return this.renderWidth
  }
  getActualWidth() {
    return this.actualWidth / TEXTWIDTH_PER_UNIT
  }
  clone() {
    return this
  }
  canStartAutoCorrect() {
    return false
  }
}
