import { InstanceType } from '../../instanceTypes'
import { TEXTWIDTH_PER_UNIT } from '../../common/const/paraContent'

export class ParaNewLine {
  readonly instanceType = InstanceType.ParaNewLine
  width: number
  renderWidth: number
  actualWidth: number
  height: number
  private static instance?: ParaNewLine
  static new() {
    if (ParaNewLine.instance == null) {
      ParaNewLine.instance = new ParaNewLine()
    }
    return ParaNewLine.instance
  }
  static recycle(_item: ParaNewLine) {
    return true
  }
  constructor() {
    this.width = 0x00000000 | 0
    this.actualWidth = 0x00000000 | 0
    this.renderWidth = 2.5

    this.height = 0
  }
  reset() {
    this.width = 0x00000000 | 0
    this.actualWidth = 0x00000000 | 0
    this.renderWidth = 2.5

    this.height = 0
  }
  draw(_X, _Y, _Context) {}

  measure(_Context) {
    return true
  }

  getWidth() {
    return this.width
  }

  getVisibleWidth() {
    return this.renderWidth
  }
  getActualWidth() {
    return this.actualWidth / TEXTWIDTH_PER_UNIT
  }
  clone() {
    return this
  }
  canStartAutoCorrect() {
    return false
  }
}
