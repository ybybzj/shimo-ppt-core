import { ParaEnd } from './ParaEnd'
import { ParaNewLine } from './ParaNewLine'
import { ParaNumbering } from './ParaNumbering'
import { ParaSpace } from './ParaSpace'
import { ParaTab } from './ParaTab'
import { ParaText } from './ParaText'

export type RunContentElement =
  | ParaEnd
  | ParaNewLine
  | ParaSpace
  | ParaTab
  | ParaText

export type ParaRunElement = RunContentElement | ParaNumbering
