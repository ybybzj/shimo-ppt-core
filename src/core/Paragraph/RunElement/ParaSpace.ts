import { FontClass } from '../../../fonts/const'
import {
  ParaTextForm,
  TEXTWIDTH_PER_UNIT,
} from '../../common/const/paraContent'
import {
  smallcaps_and_script_factor,
  VertAlignFactor,
  smallcaps_Factor,
} from '../../common/const/unit'
import { VertAlignType } from '../../common/const/attrs'
import { InstanceType } from '../../instanceTypes'
import { FontKit } from '../../../fonts/FontKit'
import { PoolAllocator, Recyclable } from '../../../common/allocator'
import { TextPr } from '../../textAttributes/TextPr'

export class ParaSpace extends Recyclable<ParaSpace> {
  readonly instanceType = InstanceType.ParaSpace
  form: number
  value: number
  width: number
  renderWidth: number
  actualWidth: number
  private static allocator: PoolAllocator<ParaSpace, []> =
    PoolAllocator.getAllocator<ParaSpace, []>(ParaSpace)
  static new() {
    return ParaSpace.allocator.allocate()
  }
  static recycle(item: ParaSpace) {
    return ParaSpace.allocator.recycle(item)
  }

  constructor() {
    super()
    this.width = 0x00000000 | 0
    this.renderWidth = 0x00000000 | 0
    this.actualWidth = 0x00000000 | 0
    this.value = 32
    this.form = 0x00000000 | 0
  }
  reset() {
    this.width = 0x00000000 | 0
    this.renderWidth = 0x00000000 | 0
    this.actualWidth = 0x00000000 | 0
    this.value = 32
    this.form = 0x00000000 | 0
  }
  getWidth() {
    return this.width / TEXTWIDTH_PER_UNIT
  }

  getVisibleWidth() {
    return this.renderWidth / TEXTWIDTH_PER_UNIT
  }

  getActualWidth() {
    return this.actualWidth / TEXTWIDTH_PER_UNIT
  }
  draw(_X, _Y, _Context) {}

  measure(fontKit: FontKit, textPr: TextPr) {
    this.setFontFactor_Script(
      textPr.vertAlign !== VertAlignType.Baseline ? true : false
    )
    this.setFontFactor_SmallCaps(
      true !== textPr.caps && true === textPr.smallCaps ? true : false
    )

    let fontFactor = this.getFontFactor()
    const fontSize = textPr.fontSize!
    if (1 !== fontFactor) {
      fontFactor = ((fontSize * fontFactor * 2 + 0.5) | 0) / 2 / fontSize
    }

    fontKit.setFontClass(textPr.lang.fontClass ?? FontClass.ASCII, fontFactor)

    const { width, isFallback } = fontKit.MeasureCode(0x20)

    const result = (Math.max(width + textPr.spacing!, 0) * 16384) | 0
    this.width = result
    return !isFallback
  }

  getFontFactor() {
    if (
      this.form & ParaTextForm.FONTKOEF_SCRIPT &&
      this.form & ParaTextForm.FONTKOEF_SMALLCAPS
    ) {
      return smallcaps_and_script_factor
    } else if (this.form & ParaTextForm.FONTKOEF_SCRIPT) {
      return VertAlignFactor.Size
    } else if (this.form & ParaTextForm.FONTKOEF_SMALLCAPS) {
      return smallcaps_Factor
    } else return 1
  }

  setFontFactor_Script(isScript) {
    if (isScript) this.form |= ParaTextForm.FONTKOEF_SCRIPT
    else this.form &= ParaTextForm.NON_FONTKOEF_SCRIPT
  }

  setFontFactor_SmallCaps(isSmallCaps) {
    if (isSmallCaps) this.form |= ParaTextForm.FONTKOEF_SMALLCAPS
    else this.form &= ParaTextForm.NON_FONTKOEF_SMALLCAPS
  }

  clone() {
    return ParaSpace.new()
  }

  canStartAutoCorrect() {
    return true
  }
}
