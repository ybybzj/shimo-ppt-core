import {
  NumberingFormat,
  numberToAlpha,
  numberToString,
  numberToRoman,
  NumberingFormatArrMapping,
  numberToChinese,
} from '../../common/numberingFormat'
import { InstanceType } from '../../instanceTypes'
import { Nullable } from '../../../../liber/pervasive'
import { RunContentElement } from './type'
import { ParaRun } from '../ParaContent/ParaRun'
import { TextContentDrawContext } from '../../common/types'
import { ParagraphRenderStateElements } from '../typeset/render'
import { FontKit } from '../../../fonts/FontKit'
import { isCJKChar } from '../../../fonts/utils'
import { TextPr } from '../../textAttributes/TextPr'
import { Bullet } from '../../SlideElement/attrs/bullet'
import { Theme } from '../../SlideElement/attrs/theme'
import { ClrMap } from '../../color/clrMap'
import {
  BulletTypeValue,
  BulletColorType,
  BulletSizeType,
  BulletTypefaceType,
} from '../../SlideElement/const'
import { EditorUtil } from '../../../globals/editor'
import { getFontClass } from '../../../fonts/FontClassification'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import {
  checkBackupBulletShimoText,
  checkBackupBulletText,
  getRTLMirrorChar,
} from '../../utilities/helpers'
import { TEXTWIDTH_PER_UNIT } from '../../common/const/paraContent'

type BulletInfo = {
  type: number
  startAt: number
  textPr?: TextPr | null
  color: { r: number; g: number; b: number; a: number }
  str: Nullable<string>
  num?: any
  size?: any
  font: string
  char: Nullable<string>
  fillEffects?: FillEffects | null
  isSizeTx: boolean
  isSizePct: boolean
  isColorTx: boolean
  isFontTx: boolean
}

const getDefaultBulletInfo = (): BulletInfo => {
  return {
    type: NumberingFormat.None,
    startAt: 0,
    textPr: null,
    color: { r: 0, g: 0, b: 0, a: 255 },
    num: null,
    str: null,
    size: 1,
    font: 'Arial',
    char: null,
    fillEffects: null,
    isSizeTx: false,
    isSizePct: true,
    isColorTx: true,
    isFontTx: true,
  }
}

function resetBulletInfo(info: BulletInfo) {
  info.type = NumberingFormat.None
  info.startAt = 0
  info.textPr = null
  info.color = { r: 0, g: 0, b: 0, a: 255 }
  info.num = null
  info.str = null
  info.size = 1
  info.font = 'Arial'
  info.char = null
  info.fillEffects = null
  info.isSizeTx = false
  info.isSizePct = true
  info.isColorTx = true
  info.isFontTx = true
}

export class ParaNumbering {
  readonly instanceType = InstanceType.ParaNumbering
  bulletInfo: BulletInfo
  bulletNum: number
  height!: number
  item: Nullable<RunContentElement>
  run: Nullable<ParaRun>
  width: number
  renderWidth: number
  actualWidth: number
  line?: number
  lineAscent?: number
  constructor() {
    this.width = 0x00000000 | 0
    this.renderWidth = 0x00000000 | 0
    this.actualWidth = 0x00000000 | 0
    this.bulletInfo = getDefaultBulletInfo()
    this.bulletNum = 0
  }
  reset() {
    this.width = 0x00000000 | 0
    this.renderWidth = 0x00000000 | 0
    this.actualWidth = 0x00000000 | 0
    resetBulletInfo(this.bulletInfo)
    this.bulletNum = 0
  }
  getWidth() {
    return this.width / TEXTWIDTH_PER_UNIT
  }

  getVisibleWidth() {
    return this.renderWidth / TEXTWIDTH_PER_UNIT
  }

  getActualWidth() {
    return this.actualWidth / TEXTWIDTH_PER_UNIT
  }

  canStartAutoCorrect() {
    return false
  }

  getType() {
    return this.bulletInfo.type
  }

  getStartAt() {
    return this.bulletInfo.startAt
  }

  measure(fontkit: FontKit, textPrOfFirstRun: TextPr) {
    let fontSize = textPrOfFirstRun.fontSize!
    if (false === this.bulletInfo.isSizeTx) {
      if (true === this.bulletInfo.isSizePct) {
        fontSize *= this.bulletInfo.size
      } else {
        fontSize = this.bulletInfo.size
      }
    }

    const needReplace =
      this.bulletInfo.font === 'Wingdings' &&
      !fontkit.getFontFile('Wingdings')?.isUsable

    if (needReplace) {
      // 新增石墨自有版权字体 ShimoUnorderedList 作为无序列表渲染备用字体
      if (fontkit.getFontFile('ShimoUnorderedList')?.isUsable) {
        this.bulletInfo.font = 'ShimoUnorderedList'
        this.bulletInfo.char = checkBackupBulletShimoText(this.bulletInfo.char)
      } else {
        this.bulletInfo.char = checkBackupBulletText(this.bulletInfo.char)
        this.bulletInfo.font = 'Arial'
      }
    }

    const rFonts = this.bulletInfo.isFontTx
      ? textPrOfFirstRun.rFonts
      : {
          ascii: {
            name: this.bulletInfo.font,
            index: -1,
          },
          eastAsia: {
            name: this.bulletInfo.font,
            index: -1,
          },
          cs: {
            name: this.bulletInfo.font,
            index: -1,
          },
          hAnsi: {
            name: this.bulletInfo.font,
            index: -1,
          },
        }

    const _textPrOfFirstRun = textPrOfFirstRun.clone()
    if (_textPrOfFirstRun.underline) {
      _textPrOfFirstRun.underline = false
    }

    if (true === this.bulletInfo.isColorTx || !this.bulletInfo.fillEffects) {
      if (textPrOfFirstRun.fillEffects) {
        this.bulletInfo.fillEffects = _textPrOfFirstRun.fillEffects
      } else {
        this.bulletInfo.fillEffects = FillEffects.SolidRGBA(0, 0, 0)
      }
    }

    const textPr = {
      rFonts: rFonts,
      fillEffects: this.bulletInfo.fillEffects,
      fontSize: fontSize,
      bold:
        this.bulletInfo.type >= NumberingFormat.arabicPeriod
          ? textPrOfFirstRun.bold
          : false,
      italic:
        this.bulletInfo.type >= NumberingFormat.arabicPeriod
          ? textPrOfFirstRun.italic
          : false,
    }
    _textPrOfFirstRun.merge(textPr)
    this.bulletInfo.textPr = _textPrOfFirstRun

    const num = this.bulletNum + this.bulletInfo.startAt - 1
    this.bulletInfo.num = num
    const oldTextPr = fontkit.getTextPr()
    let str = ''
    let width = 0
    // 有序列表
    switch (this.bulletInfo.type) {
      case NumberingFormat.Char: {
        if (null != this.bulletInfo.char) str = this.bulletInfo.char
        break
      }

      case NumberingFormat.alphaLcParenR: {
        str = numberToAlpha(num, true) + ')'
        break
      }

      case NumberingFormat.alphaLcPeriod: {
        str = numberToAlpha(num, true) + '.'
        break
      }

      case NumberingFormat.alphaUcParenR: {
        str = numberToAlpha(num, false) + ')'
        break
      }

      case NumberingFormat.alphaUcPeriod: {
        str = numberToAlpha(num, false) + '.'
        break
      }

      case NumberingFormat.arabicParenR: {
        str += numberToString(num) + ')'
        break
      }

      case NumberingFormat.arabicParenBoth: {
        str += '(' + numberToString(num) + ')'
        break
      }

      case NumberingFormat.ea1ChsPeriod: {
        str = numberToChinese(num) + '、'
        break
      }

      case NumberingFormat.chineseNumberParenBoth: {
        str = '(' + numberToChinese(num) + ')'
        break
      }

      case NumberingFormat.arabicPeriod: {
        str += numberToString(num) + '.'
        break
      }

      case NumberingFormat.romanLcPeriod: {
        str += numberToRoman(num, true) + '.'
        break
      }

      case NumberingFormat.romanUcPeriod: {
        str += numberToRoman(num, false) + '.'
        break
      }
    }

    this.bulletInfo.str = str

    const hint = this.bulletInfo.textPr.rFonts.hint
    const isCS = this.bulletInfo.textPr.cs
    const ea = this.bulletInfo.textPr.lang?.eastAsia

    const fontClass = getFontClass(str.charCodeAt(0), hint, ea, isCS)
    fontkit.setTextPr(this.bulletInfo.textPr)
    fontkit.setFontClass(fontClass)
    for (let i = 0; i < str.length; i++) {
      const c = str.charCodeAt(i)
      width += fontkit.MeasureCode(c).width
    }

    if (oldTextPr) {
      fontkit.setTextPr(oldTextPr)
    }

    this.height = 0
    this.width = width
    this.renderWidth = width
  }

  draw(
    x: number,
    y: number,
    drawContext: TextContentDrawContext,
    isDrawPlaceholder: boolean,
    paraRenderStateElements: ParagraphRenderStateElements
  ) {
    const { color, str, num: valueOfNum } = this.bulletInfo
    const textPr = this.bulletInfo.textPr!
    if (
      null === textPr ||
      null === valueOfNum ||
      null == str ||
      str.length === 0
    ) {
      return
    }

    const oldTextPr = EditorUtil.FontKit.getTextPr()

    const hint = textPr.rFonts.hint
    const isCS = textPr.cs
    const ea = textPr.lang?.eastAsia
    const { rtl } = paraRenderStateElements.paragraph!.getCalcedParaPr()

    const fontClass = getFontClass(str.charCodeAt(0), hint, ea, isCS)

    if (textPr.fillEffects) {
      textPr.fillEffects.check(
        paraRenderStateElements.theme,
        paraRenderStateElements.colorMap
      )
    }
    if (textPr.fillEffects) {
      const rgba = textPr.fillEffects.getRGBAColor()
      color.r = rgba.r
      color.g = rgba.g
      color.b = rgba.b
    }
    drawContext.setPenColor(
      color.r,
      color.g,
      color.b,
      (255 * (isDrawPlaceholder ? 0.5 : 1)) >> 0
    )
    drawContext.setBrushColor(
      color.r,
      color.g,
      color.b,
      (255 * (isDrawPlaceholder ? 0.5 : 1)) >> 0
    )

    if (drawContext.instanceType === InstanceType.CanvasDrawer) {
      drawContext.setTextPr(textPr)
      drawContext.setFontClass(fontClass)
    } else {
      EditorUtil.FontKit.setTextPr(textPr)
      EditorUtil.FontKit.setFontClass(fontClass)
    }

    const oldY = y
    const { descender } = EditorUtil.FontKit.getMetrics()
    const chars = Array.from(str as String)
    if (rtl) {
      const sumWidth = chars.reduce((sum, nextChar) => {
        return sum + EditorUtil.FontKit.MeasureCode(nextChar).width
      }, 0)
      x -= sumWidth
    }
    if (rtl) {
      chars.reverse()
    }
    chars.forEach((char) => {
      char = rtl ? getRTLMirrorChar(char) : char
      const { width } = EditorUtil.FontKit.MeasureCode(char)
      if (EditorUtil.FontKit.isEaVertText) {
        const isCJK = isCJKChar(char)
        if (isCJK) {
          x += width / 2
          y += Math.abs(descender)
        }
        drawContext.fillString(x, y, char)
        if (isCJK) {
          x += width / 2
        } else {
          x += width
        }
        y = oldY
      } else {
        drawContext.fillString(x, y, char)
        x += width
      }
    })

    if (oldTextPr) {
      if (drawContext.instanceType === InstanceType.CanvasDrawer) {
        drawContext.setTextPr(oldTextPr)
      } else {
        EditorUtil.FontKit.setTextPr(oldTextPr)
      }
    }
  }

  clone() {
    return new ParaNumbering()
  }

  isSegmentValid(line: number) {
    if (null != this.item && null != this.run && line === this.line) {
      return true
    }

    return false
  }

  updateBulletInfo(info) {
    this.bulletInfo = info
  }
}

export function getBulletInfo(
  bullet: Bullet,
  theme?: Nullable<Theme>,
  colorMap?: Nullable<ClrMap>
) {
  const bulletInfo = getDefaultBulletInfo()
  if (bullet && bullet.isBullet()) {
    switch (bullet.bulletType!.type) {
      case BulletTypeValue.CHAR: {
        bulletInfo.type = NumberingFormat.Char
        const char = bullet.bulletType!.char
        if (typeof char === 'string' && char.length > 0) {
          bulletInfo.char = char.substring(0, 1)
        } else {
          bulletInfo.char = '•'
        }
        break
      }

      case BulletTypeValue.AUTONUM: {
        bulletInfo.type =
          NumberingFormatArrMapping[bullet.bulletType!.textAutoNum!]
        bulletInfo.startAt = bullet.bulletType!.startAt
        break
      }
      case BulletTypeValue.NONE: {
        break
      }
      case BulletTypeValue.BLIP: {
        bulletInfo.type = NumberingFormat.Char
        bulletInfo.char = '•'
        break
      }
    }

    if (bullet.bulletColor) {
      if (bullet.bulletColor.type === BulletColorType.NONE) {
        bulletInfo.isColorTx = false
        bulletInfo.color.a = 0
      }
      if (bullet.bulletColor.type === BulletColorType.CLR) {
        if (bullet.bulletColor.complexColor && theme && colorMap) {
          bulletInfo.isColorTx = false
          bulletInfo.fillEffects = FillEffects.SolidCColor(
            bullet.bulletColor.complexColor
          )
        }
      }
    }
    if (bullet.bulletTypeface) {
      if (bullet.bulletTypeface.type === BulletTypefaceType.BUFONT) {
        bulletInfo.isFontTx = false
        bulletInfo.font = bullet.bulletTypeface.typeface
      }
    }
    if (bullet.bulletSize) {
      switch (bullet.bulletSize.type) {
        case BulletSizeType.TX: {
          bulletInfo.isSizeTx = true
          break
        }
        case BulletSizeType.PCT: {
          bulletInfo.isSizeTx = false
          bulletInfo.isSizePct = true
          bulletInfo.size = bullet.bulletSize.val! / 100000.0
          break
        }
        case BulletSizeType.PTS: {
          bulletInfo.isSizeTx = false
          bulletInfo.isSizePct = false
          bulletInfo.size = bullet.bulletSize.val! / 100.0
          break
        }
      }
    }
  }
  return bulletInfo
}
