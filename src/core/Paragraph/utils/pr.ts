import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import { TextPrOptions } from '../../textAttributes/TextPr'
import { getFontSizeForIncreaseDecreaseByInput } from '../../common/utils'
import { ParaRun } from '../ParaContent/ParaRun'
import { applyTextPrToParaItem } from '../ParaContent/utils/pr'
import { Paragraph } from '../Paragraph'
import { isCursorAtEndInParagraph } from './cursor'
import { ParagraphItem } from '../type'

export function applyTextPrToParagraph(
  paragraph: Paragraph,
  textPr: Nullable<TextPrOptions>,
  isIncreaseFontSize?: boolean
) {
  let item: Nullable<ParagraphItem>
  if (true === paragraph.selection.isUse) {
    let startPosIndex = paragraph.selection.startIndex
    let endPosIndex = paragraph.selection.endIndex

    if (startPosIndex === endPosIndex) {
      item = paragraph.children.at(endPosIndex)
      if (item == null) return
      const newElements = applyTextPrToParaItem(
        item,
        textPr,
        isIncreaseFontSize,
        false
      )!

      if (InstanceType.ParaRun === item.instanceType) {
        _replaceRunInPara(paragraph, endPosIndex, newElements)
      }
    } else {
      if (startPosIndex > endPosIndex) {
        const temp = startPosIndex
        startPosIndex = endPosIndex
        endPosIndex = temp
      }

      for (let i = startPosIndex + 1; i < endPosIndex; i++) {
        item = paragraph.children.at(i)
        item && applyTextPrToParaItem(item, textPr, isIncreaseFontSize, false)
      }

      let needCorrect = false

      item = paragraph.children.at(endPosIndex)
      if (item) {
        const newElements = applyTextPrToParaItem(
          item,
          textPr,
          isIncreaseFontSize,
          false
        )!
        if (InstanceType.ParaRun === item.instanceType) {
          _replaceRunInPara(paragraph, endPosIndex, newElements)
          needCorrect = true
        }
      }

      item = paragraph.children.at(startPosIndex)
      if (item) {
        const newElements = applyTextPrToParaItem(
          item,
          textPr,
          isIncreaseFontSize,
          false
        )!
        if (InstanceType.ParaRun === item.instanceType) {
          _replaceRunInPara(paragraph, startPosIndex, newElements)
          needCorrect = true
        }
      }

      if (true === needCorrect) paragraph.correctChildren()
    }
  } else {
    const pos = paragraph.cursorPosition.elementIndex
    const element = paragraph.children.at(pos)
    if (element) {
      const newElements = applyTextPrToParaItem(
        element,
        textPr,
        isIncreaseFontSize,
        false
      )!

      if (InstanceType.ParaRun === element.instanceType) {
        _replaceRunInPara(paragraph, pos, newElements)
      }
    }

    if (true === isCursorAtEndInParagraph(paragraph)) {
      if (undefined === isIncreaseFontSize && textPr) {
        paragraph.paraTextPr.fromTextPrData(textPr)
      } else {
        const textPrOfParaEnd = paragraph.getCalcedPrs(false)!.textPr.clone()
        textPrOfParaEnd.merge(paragraph.paraTextPr.textPr)

        paragraph.paraTextPr.setFontSize(
          getFontSizeForIncreaseDecreaseByInput(
            !!isIncreaseFontSize,
            textPrOfParaEnd.fontSize!
          )
        )
      }

      const lastItem = paragraph.children.at(paragraph.children.length - 1)
      if (lastItem && lastItem.instanceType === InstanceType.ParaRun) {
        lastItem.fromTextPr(paragraph.paraTextPr.textPr)
      }
    }
  }
}

function _replaceRunInPara(
  paragraph: Paragraph,
  index: number,
  newRuns: Array<Nullable<ParaRun>>
) {
  const runOfLeft = newRuns[0]
  const runOfCenter = newRuns[1]!
  const runOfRight = newRuns[2]

  let newRunIndex = index

  const oldStartIndex = paragraph.selection.startIndex
  const oldEndIndex = paragraph.selection.endIndex
  const oldElementIndex = paragraph.cursorPosition.elementIndex

  if (null != runOfLeft) {
    paragraph.addItemAt(index + 1, runOfCenter)
    newRunIndex = index + 1
  }

  if (null != runOfRight) {
    paragraph.addItemAt(newRunIndex + 1, runOfRight)
  }

  if (oldElementIndex > index) {
    if (null != runOfLeft && null != runOfRight) {
      paragraph.cursorPosition.elementIndex = oldElementIndex + 2
    } else if (null != runOfRight || null != runOfRight) {
      paragraph.cursorPosition.elementIndex = oldElementIndex + 1
    }
  } else if (oldElementIndex === index) {
    paragraph.cursorPosition.elementIndex = newRunIndex
  }
  paragraph.cursorPosition.line = -1

  if (oldStartIndex > index) {
    if (null != runOfLeft && null != runOfRight) {
      paragraph.selection.startIndex = oldStartIndex + 2
    } else if (null != runOfRight || null != runOfRight) {
      paragraph.selection.startIndex = oldStartIndex + 1
    }
  } else if (oldStartIndex === index) {
    if (oldEndIndex > oldStartIndex) {
      paragraph.selection.startIndex = index
    } else if (oldEndIndex < oldStartIndex) {
      if (null != runOfLeft && null != runOfRight) {
        paragraph.selection.startIndex = index + 2
      } else if (null != runOfLeft || null != runOfRight) {
        paragraph.selection.startIndex = index + 1
      } else paragraph.selection.startIndex = index
    } else {
      paragraph.selection.startIndex = index

      if (null != runOfLeft && null != runOfRight) {
        paragraph.selection.endIndex = index + 2
      } else if (null != runOfLeft || null != runOfRight) {
        paragraph.selection.endIndex = index + 1
      } else paragraph.selection.endIndex = index
    }
  }

  if (oldEndIndex > index) {
    if (null != runOfLeft && null != runOfRight) {
      paragraph.selection.endIndex = oldEndIndex + 2
    } else if (null != runOfRight || null != runOfRight) {
      paragraph.selection.endIndex = oldEndIndex + 1
    }
  } else if (oldEndIndex === index) {
    if (oldEndIndex > oldStartIndex) {
      if (null != runOfLeft && null != runOfRight) {
        paragraph.selection.endIndex = index + 2
      } else if (null != runOfLeft || null != runOfRight) {
        paragraph.selection.endIndex = index + 1
      } else paragraph.selection.endIndex = index
    } else if (oldEndIndex < oldStartIndex) {
      paragraph.selection.endIndex = index
    }
  }

  return newRunIndex
}
