import { InstanceType } from '../../instanceTypes'
import { ParagraphElementPosition } from '../common'
import { Paragraph } from '../Paragraph'
import { ParaRun } from '../ParaContent/ParaRun'
import { Nullable } from '../../../../liber/pervasive'
import {
  compareIndices,
  eachRunElementBetween,
  getRunElementByIndices,
  RunElementIndices,
  runElementIndicesFromPos,
} from '../../../re/export/textContent'
import { getRunElementStatusInfo } from '../RunElement/utils'
import { ArabicReshapeItem } from '../../../lib/utils/arabicReshaper/type'
import { RunContentElement } from '../RunElement/type'
import { BidiItem } from '../../../lib/utils/bidi/type'
import { getEmbeddingLevels } from '../../../lib/utils/bidi/embeddingLevels'
import { getReorderedIndices } from '../../../lib/utils/bidi/reordering'
import { getMirroredCharacter } from '../../../lib/utils/bidi/mirroring'
import { ParagraphItem, RunElemnetOrderIndicesInfo } from '../type'
import { getWidthAtIndices } from '../ParaContent/utils/typeset'
import { LineSegmentIndexesUtil } from '../ParaContent/LineSegmentIndexes'
import { ManagedArray, ManagedSlice } from '../../../common/managedArray'

function getItemSegmentFromLineAndSegment(
  item: ParagraphItem,
  lineIndex: number
): Nullable<[number, /*start */ number /*end */]> {
  const curLine = lineIndex - item.startParaLine

  const lineSegmentIndexes = item.lineSegmentIndexes
  const startindex = LineSegmentIndexesUtil.getStartIndexOfLine(
    lineSegmentIndexes,
    curLine
  )
  let endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
    lineSegmentIndexes,
    curLine
  )
  const contentLen = item.children.length
  const isValidPos = (pos) => pos >= 0 && pos < contentLen
  if (!isValidPos(startindex)) {
    return undefined
  }
  // hyperlink 的endPos是包含的, 参考方法`calculateParaHyperlinkRange`
  if (item.instanceType === InstanceType.ParaHyperlink) {
    endIndex += 1
    endIndex = endIndex > contentLen ? contentLen : endIndex
  }
  return startindex < endIndex ? [startindex, endIndex] : undefined
}

export function getPosByLineAndSegment(
  paragraph: Paragraph,
  startIndex: number,
  endIndex: number,
  line: number
): Nullable<{
  startPos: ParagraphElementPosition
  endPos: ParagraphElementPosition
}> {
  const startPos = new ParagraphElementPosition()
  const endPos = new ParagraphElementPosition()

  let startPositions = _getPosByLineAndSegment(
    paragraph,
    startIndex,
    line,
    startPos,
    'start'
  )
  let eendPositions = _getPosByLineAndSegment(
    paragraph,
    endIndex,
    line,
    endPos,
    'end'
  )
  let sIndex = startPositions == null ? startIndex + 1 : startIndex
  let eIndex = eendPositions == null ? endIndex - 1 : endIndex
  while (
    sIndex <= eIndex &&
    (startPositions == null || eendPositions == null)
  ) {
    if (startPositions == null) {
      startPositions = _getPosByLineAndSegment(
        paragraph,
        sIndex,
        line,
        startPos,
        'start'
      )
    }
    if (eendPositions == null) {
      eendPositions = _getPosByLineAndSegment(
        paragraph,
        eIndex,
        line,
        endPos,
        'end'
      )
    }
    sIndex = startPositions == null ? sIndex + 1 : sIndex
    eIndex = eendPositions == null ? eIndex - 1 : eIndex
  }

  if (startPositions && eendPositions) {
    return {
      startPos,
      endPos,
    }
  } else {
    return undefined
  }
}

function _getPosByLineAndSegment(
  paragraph: Paragraph,
  index: number,
  line: number,
  elementPosition: ParagraphElementPosition,
  edge: 'start' | 'end'
): Nullable<ParagraphElementPosition> {
  index = index < 0 ? 0 : index
  elementPosition.reset()

  elementPosition.updatePosByLevel(index, 0)

  const paraItem = paragraph.children.at(index)
  if (paraItem == null) return undefined

  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    const hyperLinkPoses = getItemSegmentFromLineAndSegment(paraItem, line)
    if (hyperLinkPoses) {
      const [s, e] = hyperLinkPoses

      const pos = edge === 'start' ? s : e - 1
      elementPosition.updatePosByLevel(pos, 1)

      const run = paraItem.children.at(pos)

      if (run == null || run.instanceType !== InstanceType.ParaRun) {
        return undefined
      }
      const runPoses = getItemSegmentFromLineAndSegment(run, line)
      if (runPoses == null) {
        return undefined
      }
      const [rs, re] = runPoses
      const rpos = edge === 'start' ? rs : re - 1
      elementPosition.updatePosByLevel(rpos, 2)
    } else {
      return undefined
    }
  } else if (paraItem.instanceType === InstanceType.ParaRun) {
    const runPoses = getItemSegmentFromLineAndSegment(paraItem, line)
    if (runPoses == null) {
      return undefined
    }
    const [s, e] = runPoses
    const pos = edge === 'start' ? s : e - 1
    elementPosition.updatePosByLevel(pos, 1)
  } else {
    return undefined
  }
  return elementPosition
}

const rElmDices = new ManagedArray<RunElementIndices>()

export function getRunElementReorderIndicesInfoForLine(
  paragraph: Paragraph,
  lineIndex: number
): Nullable<ManagedArray<RunElemnetOrderIndicesInfo>> {
  const line = paragraph.renderingState.getLine(lineIndex)
  if (line == null) {
    return undefined
  }

  const hasArabic = paragraph.renderingState.hasArabicAtLine(lineIndex)

  if (paragraph.isRTL === false && !hasArabic) {
    return undefined
  }

  const reorderPosInfoArray = line.getReorderPosInfoArray()
  if (reorderPosInfoArray != null) {
    return reorderPosInfoArray
  }

  const { startIndex, endIndex } = line.segment
  rElmDices.reset()
  getRunElementDicesForLine(
    paragraph,
    startIndex,
    endIndex,
    lineIndex,
    rElmDices
  )

  const reorderedInfoArray = reorderDicesArray(paragraph, rElmDices)
  line.resetReorderPosInfo()
  for (let i = 0, l = reorderedInfoArray.length; i < l; i++) {
    const { index, dir, mirrorCode } = reorderedInfoArray[i]
    const indices = rElmDices.at(index)!
    const info: RunElemnetOrderIndicesInfo = {
      indices,
      dir,
      mirrorCode,
    }
    if (index === 0) {
      info.isStartInLine = true
    }
    if (index === l - 1) {
      info.isEndInLine = true
    }
    line.updateReorderPosInfo(info)
  }

  return line.getReorderPosInfoArray()
}

function getRunElementDicesForLine(
  paragraph: Paragraph,
  startIndex: number,
  endIndex: number,
  line: number,
  rElmDices: ManagedArray<RunElementIndices>
) {
  const contentPoses = getPosByLineAndSegment(
    paragraph,
    startIndex,
    endIndex,
    line
  )

  if (contentPoses) {
    const { startPos, endPos } = contentPoses
    const startDices = runElementIndicesFromPos(paragraph, startPos)
    const endDices = runElementIndicesFromPos(paragraph, endPos)

    eachRunElementBetween(paragraph, startDices!, endDices!, (_, dices) => {
      rElmDices.push(dices)
    })
  }
  return rElmDices
}

// to be improved
function reorderDicesArray(
  paragraph: Paragraph,
  indicesArray: ManagedSlice<RunElementIndices>
): Array<{ index: number; dir: 'R' | 'L'; mirrorCode?: number }> {
  const charInfoGetter = (indice: RunElementIndices) => {
    const runItem = getRunElementByIndices(paragraph, indice)!
    const info = getRunElementStatusInfo(
      runItem as ArabicReshapeItem<RunContentElement>
    )
    return {
      charCode: info.charCode,
      char: String.fromCodePoint(info.charCode),
    }
  }

  const input = indicesArray as ManagedSlice<BidiItem<RunElementIndices>>
  const embedLevelsResult = getEmbeddingLevels(
    input,
    charInfoGetter,
    paragraph.isRTL ? 'rtl' : 'ltr'
  )
  const indices = getReorderedIndices(
    input,
    charInfoGetter,
    embedLevelsResult,
    null,
    null
  )
  const result: Array<{ index: number; dir: 'R' | 'L'; mirrorCode?: number }> =
    new Array(indices.length)
  indices.forEach(({ index: indexOfChar, dir }, i) => {
    result[i] = {
      index: indexOfChar,
      dir,
    }

    if (embedLevelsResult.levels[indexOfChar] & 1) {
      const mirrorChar = getMirroredCharacter(
        charInfoGetter(input.at(indexOfChar)!).char
      )
      if (mirrorChar != null) {
        result[i].mirrorCode = mirrorChar.codePointAt(0)
      }
    }
  })
  return result
}

export function getXForCursorAtBiDiLine(
  paragraph: Paragraph,
  curIndices: Nullable<RunElementIndices>,
  startX: number,
  lineIndex: number,
  isDrawAfter: boolean
): Nullable<number> {
  if (curIndices == null) {
    return undefined
  }
  const renderingState = paragraph.renderingState

  const line = renderingState.getLine(lineIndex)
  if (line == null) {
    return undefined
  }

  const reorderPosInfoArray = getRunElementReorderIndicesInfoForLine(
    paragraph,
    lineIndex
  )
  if (reorderPosInfoArray == null) {
    return undefined
  }

  let x = startX
  let found: Nullable<{ indices: RunElementIndices; dir: 'R' | 'L' }>

  for (let i = 0, l = reorderPosInfoArray.length; i < l; i++) {
    const { indices, dir } = reorderPosInfoArray.at(i)!
    if (compareIndices(indices, curIndices) === 0) {
      found = {
        dir,
        indices,
      }
      break
    } else {
      x += getWidthAtIndices(paragraph, indices)
    }
  }
  if (found != null) {
    const { indices, dir } = found
    // const curRunElement = getRunElementByIndices(paragraph, indices)
    const isRTL = dir === 'R'
    const curWidth = getWidthAtIndices(paragraph, indices)
    const isDrawLeft = (isRTL && isDrawAfter) || (!isRTL && !isDrawAfter)
    return isDrawLeft ? x : x + curWidth
  }
  return undefined
}

export function getParagraphSelectionIndices(
  paragraph: Paragraph,
  startIndex: number,
  endIndex: number
): Nullable<{ startDices: RunElementIndices; endDices: RunElementIndices }> {
  const segmentPoses = getSelectionSegmentPositions(
    paragraph,
    startIndex,
    endIndex
  )
  if (segmentPoses) {
    const { startPos, endPos } = segmentPoses
    const startDices = runElementIndicesFromPos(paragraph, startPos)!
    const endDices = runElementIndicesFromPos(paragraph, endPos)!
    return {
      startDices,
      endDices,
    }
  }
  return undefined
}

function getSelectionSegmentForParaItem(
  item: ParagraphItem
): Nullable<[number, /*start */ number /*end */]> {
  const contentLen = item.children.length
  if (contentLen <= 0) {
    return undefined
  }
  const selection = item.state.selection
  if (selection.isUse !== true) {
    return undefined
  }
  let [start, end] = [selection.startIndex, selection.endIndex]
  if (start > end) {
    start = selection.endIndex
    end = selection.startIndex
  }

  // hyperlink 的endPos是包含的, 参考方法`calculateParaHyperlinkRange`
  if (item.instanceType === InstanceType.ParaHyperlink) {
    end += 1
    end = end > contentLen ? contentLen : end
  }
  return start === end ? undefined : [start, end]
}

function _getSelectionPosForParagraph(
  paragraph: Paragraph,
  startIndex: number,
  endIndex: number,
  elementPosition: ParagraphElementPosition,
  edge: 'start' | 'end'
): Nullable<ParagraphElementPosition> {
  elementPosition.reset()
  const index = edge === 'start' ? startIndex : endIndex
  elementPosition.updatePosByLevel(index, 0)

  const paraItem = paragraph.children.at(index)

  if (paraItem == null) return undefined

  const updateContenPosForRun = (
    run: ParaRun,
    edge: 'start' | 'end',
    elementPosition: ParagraphElementPosition,
    level: number
  ) => {
    if (run.instanceType !== InstanceType.ParaRun) {
      return false
    }
    const runSelectionSegment = getSelectionSegmentForParaItem(run)
    if (runSelectionSegment == null) {
      return false
    }
    const [s, e] = runSelectionSegment
    const pos = edge === 'start' ? s : e - 1
    elementPosition.updatePosByLevel(pos, level)
    return true
  }

  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    const hyperLinkSelectionSegment = getSelectionSegmentForParaItem(paraItem)
    if (hyperLinkSelectionSegment) {
      const [s, e] = hyperLinkSelectionSegment

      let pos = edge === 'start' ? s : e - 1
      const nextPos = (pos, edge) => (edge === 'start' ? pos + 1 : pos - 1)
      while (pos >= s && pos <= e - 1) {
        elementPosition.updatePosByLevel(pos, 1)
        const run = paraItem.children[pos]
        if (updateContenPosForRun(run, edge, elementPosition, 2)) {
          break
        } else {
          pos = nextPos(pos, edge)
        }
      }
      if (!(pos >= s && pos <= e - 1)) {
        return undefined
      }
    } else {
      return undefined
    }
  } else if (paraItem.instanceType === InstanceType.ParaRun) {
    if (updateContenPosForRun(paraItem, edge, elementPosition, 1) === false) {
      return undefined
    }
  } else {
    return undefined
  }
  return elementPosition
}

function getSelectionSegmentPositions(
  paragraph: Paragraph,
  startIndex: number,
  endIndex: number
): Nullable<{
  startPos: ParagraphElementPosition
  endPos: ParagraphElementPosition
}> {
  const startPos = new ParagraphElementPosition()
  const endPos = new ParagraphElementPosition()

  let startPosition = _getSelectionPosForParagraph(
    paragraph,
    startIndex,
    endIndex,
    startPos,
    'start'
  )
  let endPosition = _getSelectionPosForParagraph(
    paragraph,
    startIndex,
    endIndex,
    endPos,
    'end'
  )
  let sIndex = startPosition == null ? startIndex + 1 : startIndex
  let eIndex = endPosition == null ? endIndex - 1 : endIndex
  while (sIndex <= eIndex && (startPosition == null || endPosition == null)) {
    if (startPosition == null) {
      startPosition = _getSelectionPosForParagraph(
        paragraph,
        sIndex,
        eIndex,
        startPos,
        'start'
      )
    }
    if (endPosition == null) {
      endPosition = _getSelectionPosForParagraph(
        paragraph,
        sIndex,
        eIndex,
        endPos,
        'end'
      )
    }
    sIndex = startPosition == null ? sIndex + 1 : sIndex
    eIndex = endPosition == null ? eIndex - 1 : eIndex
  }

  if (startPosition && endPosition) {
    return {
      startPos,
      endPos,
    }
  } else {
    return undefined
  }
}

interface ParaElementPosInfo_BehindLastInRun {
  kind: 'behindLastInRun'
  run: ParaRun
  pos: ParagraphElementPosition
}

interface ParaElementPosInfo_InEmptyRun {
  kind: 'inEmptyRun'
  run: ParaRun
  pos: ParagraphElementPosition
}

interface ParaElementPosInfo_InRunContent {
  kind: 'inRunContent'
  run: ParaRun
  pos: ParagraphElementPosition
}

type ParaElementPosInfo =
  | ParaElementPosInfo_BehindLastInRun
  | ParaElementPosInfo_InEmptyRun
  | ParaElementPosInfo_InRunContent

export function getCurrentParaElementPosInfo(
  paragraph: Paragraph
): Nullable<ParaElementPosInfo> {
  const paraItemPos = paragraph.cursorPosition.elementIndex
  const curElementPos = paragraph.currentElementPosition(false)

  const getPosInfo = (run: ParaRun): Nullable<ParaElementPosInfo> => {
    const runItemPos = run.state.elementIndex
    const contentLen = run.children.length
    if (runItemPos >= 0 && runItemPos < contentLen) {
      return {
        kind: 'inRunContent',
        pos: curElementPos,
        run: run,
      }
    } else if (runItemPos >= contentLen && contentLen > 0) {
      return {
        kind: 'behindLastInRun',
        pos: curElementPos,
        run: run,
      }
    } else if (contentLen === 0 && runItemPos === 0) {
      return {
        kind: 'inEmptyRun',
        pos: curElementPos,
        run: run,
      }
    }
    return undefined
  }

  if (paraItemPos >= 0 && paraItemPos < paragraph.children.length) {
    const paraItem = paragraph.children.at(paraItemPos)!
    if (paraItem.instanceType === InstanceType.ParaHyperlink) {
      const hyperItemPos = paraItem.state.elementIndex
      if (hyperItemPos >= 0 && hyperItemPos < paraItem.children.length) {
        const hyperItem = paraItem.children.at(hyperItemPos)!
        if (hyperItem.instanceType === InstanceType.ParaRun) {
          return getPosInfo(hyperItem)
        }
      }
    } else if (paraItem.instanceType === InstanceType.ParaRun) {
      return getPosInfo(paraItem)
    }
  }

  return undefined
}
