import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import {
  getRunElementByIndices,
  runElementIndicesFromPos,
} from '../../../re/export/textContent'
import {
  ParagraphLocationInfo,
  ParagraphElementPosition,
  ParaItemPosInfo,
  ParagraphAdjacentPosition,
  ParagraphElementAdjacentPosition,
  AdjacentPosition,
} from '../common'
import { LineSegmentIndexesUtil } from '../ParaContent/LineSegmentIndexes'
import { ParaHyperlink } from '../ParaContent/ParaHyperlink'
import { ParaRun } from '../ParaContent/ParaRun'
import { stopCursorOnEntryExit } from '../ParaContent/utils/cursor'
import {
  searchPositionInfoForRunElement,
  updateSearchPInfoByXPosInfo,
} from '../ParaContent/utils/typeset'
import { Paragraph } from '../Paragraph'
import { ParaText } from '../RunElement/ParaText'
import { ParagraphItem, RunElemnetOrderIndicesInfo } from '../type'
import { ParagraphSearchPositionInfo } from '../typeset/ParaRenderingState'
export function updateStartPosForParaItem(
  paraItem: ParaRun | ParaHyperlink,
  pos: ParagraphElementPosition,
  lvl: number
) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    pos.updatePosByLevel(0, lvl)
  } else {
    if (paraItem.children.length > 0) {
      pos.updatePosByLevel(0, lvl)
      pos.updatePosByLevel(0, lvl + 1)
    }
  }
}

export function updateEndPosForParaItem(
  paraItem: ParaRun | ParaHyperlink,
  isBehindEnd: boolean,
  pos: ParagraphElementPosition,
  lvl: number
) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    const contentLen = paraItem.children.length

    if (true === isBehindEnd) pos.updatePosByLevel(contentLen, lvl)
    else {
      for (let i = 0; i < contentLen; i++) {
        if (InstanceType.ParaEnd === paraItem.children.at(i)!.instanceType) {
          pos.updatePosByLevel(i, lvl)
          return
        }
      }

      pos.updatePosByLevel(contentLen, lvl)
    }
  } else {
    const contentLen = paraItem.children.length
    if (contentLen > 0) {
      pos.updatePosByLevel(contentLen - 1, lvl)
      updateEndPosForParaItem(
        paraItem.children.at(contentLen - 1)!,
        isBehindEnd,
        pos,
        lvl + 1
      )
    }
  }
}

export function findLeftContentPosition(
  paragraph: Paragraph,
  locInfo: ParagraphLocationInfo,
  elementPosition: ParagraphElementPosition
) {
  const lvl = 0
  let index = elementPosition.get(lvl)

  let item = paragraph.children.at(index)
  if (item) {
    findLeftContentPositionInParaItem(
      item,
      locInfo,
      elementPosition,
      lvl + 1,
      true
    )
    locInfo.pos.updatePosByLevel(index, lvl)
  }
  if (true === locInfo.found) return true

  index--

  if (index >= 0 && stopCursorOnEntryExit(paragraph.children.at(index + 1))) {
    item = paragraph.children.at(index)
    if (item) {
      updateEndPosForParaItem(item, false, locInfo.pos, lvl + 1)
      locInfo.pos.updatePosByLevel(index, lvl)
      locInfo.found = true
      return true
    }
  }

  while (index >= 0) {
    item = paragraph.children.at(index)
    if (item) {
      findLeftContentPositionInParaItem(
        item,
        locInfo,
        elementPosition,
        lvl + 1,
        false
      )
      locInfo.pos.updatePosByLevel(index, lvl)
    }
    if (locInfo.found) return true

    index--
  }

  return false
}

function findLeftContentPositionInParaItem(
  paraItem: ParaRun | ParaHyperlink,
  locInfo: ParagraphLocationInfo,
  elementPosition: ParagraphElementPosition,
  lvl: number,
  fromElmPosition: boolean
) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    if (paraItem.isPresentationField()) {
      if (false === fromElmPosition && paraItem.children.length > 0) {
        locInfo.found = true
        locInfo.pos.updatePosByLevel(0, lvl)
        return true
      }

      return false
    } else {
      let index =
        true === fromElmPosition
          ? elementPosition.get(lvl)
          : paraItem.children.length

      index--

      if (index >= 0) {
        locInfo.found = true
        locInfo.pos.updatePosByLevel(index, lvl)
      }
    }
  } else {
    const childrenCount = paraItem.children.length
    if (childrenCount <= 0) return false

    let index =
      true === fromElmPosition ? elementPosition.get(lvl) : childrenCount - 1

    if (index < 0 || index > childrenCount - 1) return false

    findLeftContentPositionInParaItem(
      paraItem.children.at(index)!,
      locInfo,
      elementPosition,
      lvl + 1,
      fromElmPosition
    )
    locInfo.pos.updatePosByLevel(index, lvl)

    if (true === locInfo.found) return true

    index--

    if (index >= 0 && stopCursorOnEntryExit(paraItem.children.at(index + 1)!)) {
      updateEndPosForParaItem(
        paraItem.children.at(index)!,
        false,
        locInfo.pos,
        lvl + 1
      )
      locInfo.pos.updatePosByLevel(index, lvl)
      locInfo.found = true
      return true
    }

    while (index >= 0) {
      const run = paraItem.children.at(index)
      if (run) {
        findLeftContentPositionInParaItem(
          run,
          locInfo,
          elementPosition,
          lvl + 1,
          false
        )
        locInfo.pos.updatePosByLevel(index, lvl)
      }

      if (locInfo.found) return true

      index--
    }

    return false
  }
}

export function findRightContentPosition(
  paragraph: Paragraph,
  locInfo: ParagraphLocationInfo,
  elementPosition: ParagraphElementPosition,
  includeEnd: boolean = false
) {
  const lvl = 0
  let index = elementPosition.get(lvl)
  let item = paragraph.children.at(index)
  if (item) {
    findRightContentPositionInParaItem(
      item,
      locInfo,
      elementPosition,
      lvl + 1,
      true,
      includeEnd
    )
    locInfo.pos.updatePosByLevel(index, lvl)
  }
  if (true === locInfo.found) return true

  index++

  const childrenCount = paragraph.children.length
  if (
    index < childrenCount &&
    stopCursorOnEntryExit(paragraph.children.at(index - 1))
  ) {
    item = paragraph.children.at(index)
    if (item) {
      updateStartPosForParaItem(item, locInfo.pos, lvl + 1)
      locInfo.pos.updatePosByLevel(index, lvl)
      locInfo.found = true
      return true
    }
  }

  while (index < childrenCount) {
    item = paragraph.children.at(index)
    if (item) {
      findRightContentPositionInParaItem(
        item,
        locInfo,
        elementPosition,
        lvl + 1,
        false,
        includeEnd
      )
      locInfo.pos.updatePosByLevel(index, lvl)
    }
    if (locInfo.found) return true

    index++
  }

  return false
}

function findRightContentPositionInParaItem(
  paraItem: ParaRun | ParaHyperlink,
  locInfo: ParagraphLocationInfo,
  elementPosition: ParagraphElementPosition,
  lvl: number,
  fromElmPosition: boolean,
  stepEnd: boolean
) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    if (paraItem.isPresentationField()) {
      if (false === fromElmPosition && paraItem.children.length > 0) {
        locInfo.found = true
        locInfo.pos.updatePosByLevel(paraItem.children.length, lvl)
        return true
      }

      return false
    } else {
      let index = true === fromElmPosition ? elementPosition.get(lvl) : 0

      const count = paraItem.children.length
      if (count === 0) return false

      while (index <= count) {
        index++

        if (count === index) {
          const prevItem = paraItem.children.at(index - 1)!

          if (
            true !== stepEnd &&
            InstanceType.ParaEnd === prevItem.instanceType
          ) {
            return false
          }

          break
        }

        const item = paraItem.children.at(index - 1)

        if (
          item &&
          !(true !== stepEnd && InstanceType.ParaEnd === item.instanceType)
        ) {
          break
        }
      }

      if (index <= count) {
        locInfo.found = true
        locInfo.pos.updatePosByLevel(index, lvl)
      }
    }
  } else {
    const childrenCount = paraItem.children.length
    if (childrenCount <= 0) return false

    let index = true === fromElmPosition ? elementPosition.get(lvl) : 0

    if (index < 0 || index > childrenCount - 1) return false

    findRightContentPositionInParaItem(
      paraItem.children.at(index)!,
      locInfo,
      elementPosition,
      lvl + 1,
      fromElmPosition,
      stepEnd
    )
    locInfo.pos.updatePosByLevel(index, lvl)

    if (true === locInfo.found) return true

    index++

    if (
      index < childrenCount &&
      stopCursorOnEntryExit(paraItem.children.at(index - 1)!)
    ) {
      updateStartPosForParaItem(
        paraItem.children.at(index)!,
        locInfo.pos,
        lvl + 1
      )
      locInfo.pos.updatePosByLevel(index, lvl)
      locInfo.found = true
      return true
    }

    while (index < childrenCount) {
      const run = paraItem.children.at(index)
      if (run) {
        findRightContentPositionInParaItem(
          run,
          locInfo,
          elementPosition,
          lvl + 1,
          false,
          stepEnd
        )
        locInfo.pos.updatePosByLevel(index, lvl)
      }
      if (locInfo.found) return true

      index++
    }

    return false
  }
}

export function findWordStartPos(
  paragraph: Paragraph,
  locInfo: ParagraphLocationInfo,
  elementPosition: ParagraphElementPosition
) {
  const lvl = 0
  let index = elementPosition.get(lvl)
  let paraItem = paragraph.children.at(index)
  if (paraItem) {
    findWordStartPosInParaItem(
      paraItem,
      locInfo,
      elementPosition,
      lvl + 1,
      true
    )

    if (true === locInfo.needUpdatePositon) {
      locInfo.pos.updatePosByLevel(index, lvl)
    }

    if (true === locInfo.found) return
  }
  index--

  while (index >= 0) {
    paraItem = paragraph.children.at(index)
    if (paraItem) {
      findWordStartPosInParaItem(
        paraItem,
        locInfo,
        elementPosition,
        lvl + 1,
        false
      )

      if (true === locInfo.needUpdatePositon) {
        locInfo.pos.updatePosByLevel(index, lvl)
      }
    }
    if (locInfo.found) return

    index--
  }

  if (true === locInfo.isShift) {
    locInfo.found = true
  }
}

function findWordStartPosInParaItem(
  paraItem: ParaRun | ParaHyperlink,
  locInfo: ParagraphLocationInfo,
  elementPosition: ParagraphElementPosition,
  lvl: number,
  fromElmPosition: boolean
) {
  if (
    paraItem.instanceType === InstanceType.ParaRun &&
    paraItem.isPresentationField() === false
  ) {
    let index =
      true === fromElmPosition
        ? Math.min(elementPosition.get(lvl) - 1, paraItem.children.length - 1)
        : paraItem.children.length - 1

    if (index < 0) return

    locInfo.isShift = true

    let needUpdate = false

    if (0 === locInfo.stage) {
      while (true) {
        const item = paraItem.children.at(index)!
        const instanceType = item.instanceType

        let isSpace = false

        if (
          InstanceType.ParaSpace === instanceType ||
          InstanceType.ParaTab === instanceType ||
          (InstanceType.ParaText === instanceType && true === item.isNBSP())
        ) {
          isSpace = true
        }

        if (true === isSpace) {
          index--

          if (index < 0) return
        } else {
          if (InstanceType.ParaText !== item.instanceType) {
            locInfo.pos.updatePosByLevel(index, lvl)
            locInfo.found = true
            locInfo.needUpdatePositon = true
            return
          }

          locInfo.pos.updatePosByLevel(index, lvl)
          locInfo.stage = 1
          locInfo.punctuation = item.isPunctuation()
          needUpdate = true

          break
        }
      }
    } else {
      index =
        true === fromElmPosition
          ? Math.min(elementPosition.get(lvl), paraItem.children.length)
          : paraItem.children.length
    }

    while (index > 0) {
      index--
      const item = paraItem.children.at(index)!
      const instanceType = item.instanceType

      if (
        InstanceType.ParaText !== instanceType ||
        true === item.isNBSP() ||
        (true === locInfo.punctuation && true !== item.isPunctuation()) ||
        (false === locInfo.punctuation && false !== item.isPunctuation())
      ) {
        locInfo.found = true
        break
      } else {
        locInfo.pos.updatePosByLevel(index, lvl)
        needUpdate = true
      }
    }

    locInfo.needUpdatePositon = needUpdate
  } else if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    let index =
      true === fromElmPosition
        ? elementPosition.get(lvl)
        : paraItem.children.length - 1

    let run = paraItem.children.at(index)
    if (run) {
      findWordStartPosInParaItem(
        run,
        locInfo,
        elementPosition,
        lvl + 1,
        fromElmPosition
      )

      if (true === locInfo.needUpdatePositon) {
        locInfo.pos.updatePosByLevel(index, lvl)
      }
    }
    if (true === locInfo.found) return

    index--

    while (index >= 0) {
      const orgUpdatePos = locInfo.needUpdatePositon
      run = paraItem.children.at(index)
      if (run) {
        findWordStartPosInParaItem(
          run,
          locInfo,
          elementPosition,
          lvl + 1,
          false
        )

        if (true === locInfo.needUpdatePositon) {
          locInfo.pos.updatePosByLevel(index, lvl)
        } else locInfo.needUpdatePositon = orgUpdatePos
      }
      if (locInfo.found) return

      index--
    }
  }
}

export function findWordEndPos(
  paragraph: Paragraph,
  locInfo: ParagraphLocationInfo,
  elementPosition: ParagraphElementPosition,
  isIncludeEnd: boolean = false
) {
  const lvl = 0
  let index = elementPosition.get(lvl)
  let paraItem = paragraph.children.at(index)
  if (paraItem) {
    findWordEndPosInParaItem(
      paraItem,
      locInfo,
      elementPosition,
      lvl + 1,
      true,
      isIncludeEnd
    )

    if (true === locInfo.needUpdatePositon) {
      locInfo.pos.updatePosByLevel(index, lvl)
    }
  }
  if (true === locInfo.found) return

  index++

  const count = paragraph.children.length
  while (index < count) {
    paraItem = paragraph.children.at(index)
    if (paraItem) {
      findWordEndPosInParaItem(
        paraItem,
        locInfo,
        elementPosition,
        lvl + 1,
        false,
        isIncludeEnd
      )

      if (true === locInfo.needUpdatePositon) {
        locInfo.pos.updatePosByLevel(index, lvl)
      }
    }
    if (locInfo.found) return

    index++
  }

  if (true === locInfo.isShift) {
    locInfo.found = true
  }
}

function findWordEndPosInParaItem(
  paraItem: ParaRun | ParaHyperlink,
  locInfo: ParagraphLocationInfo,
  elementPosition: ParagraphElementPosition,
  lvl: number,
  fromElmPosition: boolean,
  isIncludeEnd: boolean
) {
  if (
    paraItem.instanceType === InstanceType.ParaRun &&
    paraItem.isPresentationField() === false
  ) {
    let index =
      true === fromElmPosition ? Math.max(elementPosition.get(lvl), 0) : 0

    const contentLen = paraItem.children.length
    if (index >= contentLen) return

    // const NeedUpdate = false

    if (0 === locInfo.stage) {
      while (true) {
        const item = paraItem.children.at(index)!
        const instanceType = item.instanceType
        let isText = false

        if (
          InstanceType.ParaText === instanceType &&
          true !== item.isNBSP() &&
          (true === locInfo.first ||
            locInfo.punctuation === item.isPunctuation())
        ) {
          isText = true
        }

        if (true === isText) {
          if (true === locInfo.first) {
            locInfo.first = false
            locInfo.punctuation = (item as ParaText).isPunctuation()
          }

          locInfo.isShift = true

          index++

          if (index >= contentLen) return
        } else {
          locInfo.stage = 1

          if (true === locInfo.first) {
            if (InstanceType.ParaEnd === instanceType) {
              if (true === isIncludeEnd) {
                locInfo.pos.updatePosByLevel(index + 1, lvl)
                locInfo.found = true
                locInfo.needUpdatePositon = true
              }

              return
            }

            index++

            locInfo.isShift = true
          }

          break
        }
      }
    }

    if (index >= contentLen) return

    const item = paraItem.children.at(index)!
    const instanceType = item.instanceType
    if (
      !(
        InstanceType.ParaSpace === instanceType ||
        (InstanceType.ParaText === instanceType && true === item.isNBSP())
      )
    ) {
      locInfo.pos.updatePosByLevel(index, lvl)
      locInfo.found = true
      locInfo.needUpdatePositon = true
    } else {
      while (index < contentLen - 1) {
        index++
        const item = paraItem.children.at(index)!
        const instanceType = item.instanceType

        if (
          (true !== isIncludeEnd && InstanceType.ParaEnd === instanceType) ||
          !(
            InstanceType.ParaSpace === instanceType ||
            (InstanceType.ParaText === instanceType && true === item.isNBSP())
          )
        ) {
          locInfo.found = true
          break
        }
      }

      locInfo.pos.updatePosByLevel(index, lvl)
      locInfo.needUpdatePositon = true
    }
  } else if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    let index = true === fromElmPosition ? elementPosition.get(lvl) : 0
    let run = paraItem.children.at(index)
    if (run) {
      findWordEndPosInParaItem(
        run,
        locInfo,
        elementPosition,
        lvl + 1,
        fromElmPosition,
        isIncludeEnd
      )

      if (true === locInfo.needUpdatePositon) {
        locInfo.pos.updatePosByLevel(index, lvl)
      }
    }
    if (true === locInfo.found) return

    index++

    const count = paraItem.children.length
    while (index < count) {
      const oldPos = locInfo.needUpdatePositon
      run = paraItem.children.at(index)
      if (run) {
        findWordEndPosInParaItem(
          run,
          locInfo,
          elementPosition,
          lvl + 1,
          false,
          isIncludeEnd
        )

        if (true === locInfo.needUpdatePositon) {
          locInfo.pos.updatePosByLevel(index, lvl)
        } else locInfo.needUpdatePositon = oldPos
      }

      if (locInfo.found) return

      index++
    }
  }
}

export function findEndSegmentPos(
  paragraph: Paragraph,
  locInfo: ParagraphLocationInfo,
  elementPosition: ParagraphElementPosition
) {
  const posInfo = paragraph.paraItemPosInfoByContentPos(elementPosition)

  if (posInfo == null) return
  const curLine = posInfo.line

  const pos = paragraph.renderingState.getContentPos(curLine)
  if (pos == null) return

  const [startIndex, endIndex] = pos

  locInfo.line = curLine

  for (let i = startIndex; i <= endIndex; i++) {
    const item = paragraph.children.at(i)

    if (
      item &&
      true === findEndSegmentPosInParaItem(item, curLine, locInfo, 1)
    ) {
      locInfo.pos.updatePosByLevel(i, 0)
    }
  }
}

function findEndSegmentPosInParaItem(
  paraItem: ParaRun | ParaHyperlink,
  line: number,
  locInfo: ParagraphLocationInfo,
  lvl: number
): boolean {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    const curLine = line - paraItem.startParaLine

    const lineSegmentIndexes = paraItem.lineSegmentIndexes
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      curLine
    )
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      curLine
    )

    let lastIndex = -1
    for (let i = startIndex; i < endIndex; i++) {
      const item = paraItem.children.at(i)
      if (
        item &&
        !(
          InstanceType.ParaEnd === item.instanceType ||
          InstanceType.ParaNewLine === item.instanceType
        )
      ) {
        lastIndex = i + 1
      }
    }

    if (-1 !== lastIndex) {
      locInfo.pos.updatePosByLevel(lastIndex, lvl)
      return true
    } else return false
  } else {
    const curLine = line - paraItem.startParaLine
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      paraItem.lineSegmentIndexes,
      curLine
    )

    if (endIndex >= paraItem.children.length || endIndex < 0) return false

    const run = paraItem.children.at(endIndex)!

    const result = findEndSegmentPosInParaItem(run, line, locInfo, lvl + 1)

    if (true === result) locInfo.pos.updatePosByLevel(endIndex, lvl)

    return result
  }
}

export function findStartSegmentPos(
  paragraph: Paragraph,
  locInfo: ParagraphLocationInfo,
  elementPosition: ParagraphElementPosition
) {
  const posInfo = paragraph.paraItemPosInfoByContentPos(elementPosition)

  if (posInfo == null) return

  const curLine = posInfo.line

  const indices = paragraph.renderingState.getContentPos(curLine)
  if (indices == null) return

  const [startIndex, endIndex] = indices

  locInfo.line = curLine

  for (let i = endIndex; i >= startIndex; i--) {
    const item = paragraph.children.at(i)

    if (
      item &&
      true === findStartSegmentPosInParaItem(item, curLine, locInfo, 1)
    ) {
      locInfo.pos.updatePosByLevel(i, 0)
    }
  }
}

function findStartSegmentPosInParaItem(
  paraItem: ParaRun | ParaHyperlink,
  line: number,
  locInfo: ParagraphLocationInfo,
  lvl: number
): boolean {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    const curLine = line - paraItem.startParaLine
    const lineSegmentIndexes = paraItem.lineSegmentIndexes
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      curLine
    )
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      curLine
    )

    let firstIndex = -1
    for (let i = endIndex - 1; i >= startIndex; i--) {
      firstIndex = i
    }

    if (-1 !== firstIndex) {
      locInfo.pos.updatePosByLevel(firstIndex, lvl)
      return true
    } else return false
  } else {
    const curLine = line - paraItem.startParaLine
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      paraItem.lineSegmentIndexes,
      curLine
    )

    if (startIndex >= paraItem.children.length || startIndex < 0) return false

    const result = findStartSegmentPosInParaItem(
      paraItem.children.at(startIndex)!,
      line,
      locInfo,
      lvl + 1
    )

    if (true === result) locInfo.pos.updatePosByLevel(startIndex, lvl)

    return result
  }
}

export function getCurrentParaItemPosInfo(para: Paragraph) {
  const curItem = para.children.at(para.cursorPosition.elementIndex)
  if (curItem == null) return undefined
  let paraPos: ParaItemPosInfo
  if (curItem.instanceType === InstanceType.ParaHyperlink) {
    const elementPosition = curItem.state.elementIndex

    if (elementPosition >= 0 && elementPosition < curItem.children.length) {
      const curRun = curItem.children.at(elementPosition)!
      paraPos = getParaItemPosInfo(curRun, curRun.state.elementIndex)
    } else {
      paraPos = new ParaItemPosInfo(curItem.startParaLine, 0, 0)
    }
  } else {
    paraPos = getParaItemPosInfo(curItem, curItem.state.elementIndex)
  }

  if (-1 !== para.cursorPosition.line) {
    paraPos.line = para.cursorPosition.line
    paraPos.colIndex = para.renderingState.getColIndexByLine(paraPos.line)
  }

  return paraPos
}

export function getParaItemPosInfo(paraRun: ParaRun, index: number) {
  if (-1 === paraRun.startParaLine) return new ParaItemPosInfo(-1, -1, -1)

  let curLine = 0
  let startIndex, endIndex
  const lineSegmentIndexes = paraRun.lineSegmentIndexes
  const linesCount = LineSegmentIndexesUtil.getLinesCount(lineSegmentIndexes)
  for (; curLine < linesCount; curLine++) {
    startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      curLine
    )
    endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      curLine
    )
    if (index < endIndex && index >= startIndex) {
      return new ParaItemPosInfo(curLine + paraRun.startParaLine, 0, 0)
    }
  }

  return new ParaItemPosInfo(linesCount - 1 + paraRun.startParaLine, 0, 0)
}

export function getStartPosOfParagraph(para: Paragraph) {
  const startPosition = new ParagraphElementPosition()
  const lvl = 0

  startPosition.updatePosByLevel(0, lvl)

  const firstChild = para.children.at(0)
  if (firstChild) {
    updateStartPosForParaItem(firstChild, startPosition, lvl + 1)
  }
  return startPosition
}

export function getEndPosOfParagraph(
  para: Paragraph,
  isBehindEnd: boolean,
  isSkipLast = false
) {
  const elementPosition = new ParagraphElementPosition()
  const lvl = 0

  const contentLen = para.children.length
  let indexOfPosition = contentLen - 1
  if (isSkipLast === true && para.children.length > 1) {
    indexOfPosition = contentLen - 2
  }
  elementPosition.updatePosByLevel(indexOfPosition, lvl)

  const paraItem = para.children.at(indexOfPosition)
  if (paraItem) {
    updateEndPosForParaItem(paraItem, isBehindEnd, elementPosition, lvl + 1)
  }
  return elementPosition
}

export function updateClosestPosForParagraph(
  paragraph: Paragraph,
  adjPos: AdjacentPosition
) {
  const l = paragraph.adjacentPositions.length
  for (let i = 0; i < l; i++) {
    if (paragraph.adjacentPositions[i].adjacentPos === adjPos) return
  }

  const paraAdjPos: ParagraphAdjacentPosition = { enities: [] }
  paraAdjPos.adjacentPos = adjPos

  paragraph.adjacentPositions.push(paraAdjPos)
  paraAdjPos.enities.push(paragraph)

  const curIndex = adjPos.pos.get(0)
  const paraItem = paragraph.children.at(curIndex)
  paraItem && updateClosestPosForParaItem(paraItem, paraAdjPos, 1)
}

function updateClosestPosForParaItem(
  paraItem: ParagraphItem,
  paraAdjPos: ParagraphAdjacentPosition,
  lvl: number
) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    const runAdjPos: ParagraphElementAdjacentPosition = {
      adjacentPos: paraAdjPos.adjacentPos,
      level: lvl,
    }

    paraItem.adjacentPositions.push(runAdjPos)
    paraAdjPos.enities.push(paraItem)
  } else {
    const hyperAdjPos: ParagraphElementAdjacentPosition = {
      adjacentPos: paraAdjPos.adjacentPos,
      level: lvl,
    }

    paraItem.adjacentPositions.push(hyperAdjPos)
    paraAdjPos.enities.push(paraItem)

    if (paraAdjPos.adjacentPos) {
      const curIndex = paraAdjPos.adjacentPos.pos.get(lvl)
      const run = paraItem.children.at(curIndex)
      run && updateClosestPosForParaItem(run, paraAdjPos, lvl + 1)
    }
  }
}

/**
 * 根据 Shape 内的相对坐标获取 paragraph 对应的 pos, level 信息
 * @param isYLine  === true? Y指代 lineNumber : Y指代坐标
 * */
export function getSearchPositionInfoFromParagraph(
  paragraph: Paragraph,
  x: number,
  y: number,
  colIndex: Nullable<number>,
  isYLine: boolean,
  isIncludeEnd: boolean,
  allowInText = true
) {
  const renderingState = paragraph.renderingState

  const searchInfo = new ParagraphSearchPositionInfo()
  searchInfo.allowInText = allowInText
  const lineCount = renderingState.getLineCount()
  if (lineCount <= 0) return searchInfo

  let col = -1 === colIndex || null == colIndex ? 0 : colIndex

  const columnLength = renderingState.columns.length
  if (columnLength <= 0) return searchInfo

  // 正常情况下幻灯片不会进入此分支
  if (col >= columnLength) {
    col = columnLength - 1
    isYLine = true
    y = lineCount - 1
  } else if (col < 0) {
    col = 0
    isYLine = true
    y = 0
  }

  // 首先根据 Y 坐标定位所处 line
  let curLine = 0
  const paraColumn = renderingState.columns.at(col)!
  if (true === isYLine) {
    curLine = y
  } else {
    curLine = paraColumn.firstLine
    const fisrtParaLine = renderingState.getLine(curLine)
    if (fisrtParaLine == null) return searchInfo

    const fisrtParaLineH =
      fisrtParaLine.y +
      fisrtParaLine.metrics.descent +
      fisrtParaLine.metrics.LineGap
    let yOfCurrentLine = paraColumn.y + fisrtParaLineH
    const lastLine =
      col >= columnLength - 1
        ? lineCount - 1
        : renderingState.columns.at(col + 1)!.firstLine - 1

    let isFindY = false
    while (!isFindY) {
      if (y < yOfCurrentLine || curLine >= lastLine) {
        isFindY = true
        continue
      }
      curLine++
      const nextParaLine = renderingState.getLine(curLine)
      if (nextParaLine == null) break
      const nextParaLineH =
        nextParaLine.y +
        nextParaLine.metrics.descent +
        nextParaLine.metrics.LineGap
      yOfCurrentLine = paraColumn.y + nextParaLineH
    }
  }
  const curParaLine = renderingState.getLine(curLine)
  if (curParaLine == null) {
    return searchInfo
  }

  // Define the segment in which we fall
  // 正常情况下幻灯片有且仅有一个 segment
  const curParaLineSegment = curParaLine.segment

  // 根据 curParaLine 和 X 坐标确定具体的位置
  const para = paragraph
  const startIndex = curParaLineSegment.startIndex
  let endIndex = curParaLineSegment.endIndex
  searchInfo.cursorX = curParaLineSegment.XForRender
  ;[searchInfo.x, searchInfo.y] = [x, y]

  // Check the hit in the numbering
  if (!para.isRTL && true === para.paraNumbering.isSegmentValid(curLine)) {
    searchInfo.cursorX += para.paraNumbering.renderWidth
  }

  // 最后一个 run 可能不止有 ParaEnd
  const childrenCount = para.children.length
  const lastChildLength =
    para.children.at(childrenCount - 1)?.children?.length ?? 0
  if (lastChildLength > 1) {
    isIncludeEnd = true
  }
  // isIncludeEnd 是否检查到最后一个
  if (
    !isIncludeEnd &&
    endIndex === childrenCount - 1 &&
    endIndex > startIndex
  ) {
    endIndex--
  }

  const hasArabic = renderingState.hasArabicAtLine(curLine)
  const indicesArray = curParaLine.getReorderPosInfoArray()
  if (hasArabic === true && indicesArray != null) {
    let prevIndicesInfo: Nullable<RunElemnetOrderIndicesInfo> = null
    for (let i = 0, l = indicesArray.length; i < l; i++) {
      const indicesInfo = indicesArray.at(i)!
      const xPosInfo = searchPositionInfoForRunElement(
        paragraph,
        searchInfo,
        indicesInfo,
        prevIndicesInfo,
        i === 0 ? 'start' : i === l - 1 ? 'end' : 'between'
      )
      if (xPosInfo != null) {
        updateSearchPInfoByXPosInfo(paragraph, searchInfo, xPosInfo)
        break
      } else {
        prevIndicesInfo = indicesInfo
      }
    }

    if (isIncludeEnd === true) {
      const searchIndices = runElementIndicesFromPos(paragraph, searchInfo.pos)

      const searchedElement =
        searchIndices && getRunElementByIndices(paragraph, searchIndices)
      if (
        searchedElement &&
        searchedElement.instanceType === InstanceType.ParaEnd
      ) {
        searchInfo.pos.increaseByOne()
      }
    }
  } else if (childrenCount > 0) {
    for (
      let i = startIndex < 0 ? 0 : startIndex,
        _endIndex = Math.min(endIndex, childrenCount - 1);
      i <= _endIndex;
      i++
    ) {
      const item = para.children.at(i)!

      if (false === searchInfo.hitTextContent) {
        searchInfo.positionInText.setPosByLevel(i, 0)
      }

      if (
        true ===
        getParaContentPosByXYInParaIem(
          item,
          searchInfo,
          1,
          curLine,
          isIncludeEnd
        )
      ) {
        searchInfo.pos.setPosByLevel(i, 0)
      }
    }
  }

  searchInfo.isInTextContent = searchInfo.hitTextContent

  // We hit some element by X, check by Y
  if (
    true === searchInfo.hitTextContent &&
    y >= paraColumn.y + curParaLine.y - curParaLine.metrics.ascent - 0.01 &&
    y <=
      paraColumn.y +
        curParaLine.y +
        curParaLine.metrics.descent +
        curParaLine.metrics.LineGap +
        0.01
  ) {
    searchInfo.hitTextContent = true
  } else searchInfo.hitTextContent = false

  // This is possible if all the wounds before (including this one) were empty,
  // then, in order not to return the wrong position, we will return the position of the beginning of this empty wound.
  if (searchInfo.diffX > 1000000 - 1) {
    searchInfo.line = -1
  } else {
    searchInfo.line = curLine
  }

  return searchInfo
}

function getParaContentPosByXYInParaIem(
  paraItem: ParagraphItem,
  searchInfo: ParagraphSearchPositionInfo,
  lvl: number,
  lineIndex: number,
  isIncludeEnd: boolean
) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    let result = false

    const curLine = lineIndex - paraItem.startParaLine

    const lineSegmentIndexes = paraItem.lineSegmentIndexes
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      curLine
    )
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      curLine
    )

    for (let i = startIndex; i <= endIndex; i++) {
      const item = paraItem.children.at(i)

      if (false === searchInfo.hitTextContent) {
        searchInfo.positionInText.setPosByLevel(i, lvl)
      }

      if (
        item &&
        true ===
          getParaContentPosByXYInParaRun(
            item,
            searchInfo,
            lvl + 1,
            lineIndex,
            isIncludeEnd
          )
      ) {
        searchInfo.pos.setPosByLevel(i, lvl)
        result = true
      }
    }

    return result
  } else {
    return getParaContentPosByXYInParaRun(
      paraItem,
      searchInfo,
      lvl,
      lineIndex,
      isIncludeEnd
    )
  }
}

function getParaContentPosByXYInParaRun(
  paraRun: ParaRun,
  searchInfo: ParagraphSearchPositionInfo,
  lvl: number,
  paraLine: number,
  isIncludeEnd: boolean
) {
  let result = false

  const curLine = paraLine - paraRun.startParaLine

  const lineSegmentIndexes = paraRun.lineSegmentIndexes
  const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
    lineSegmentIndexes,
    curLine
  )
  const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
    lineSegmentIndexes,
    curLine
  )

  let diff
  let curIndex = startIndex

  // 正常情况下幻灯片不会进入此分支
  if (curIndex >= endIndex) {
    // A stub so that we poke to the right to fall into the rightmost empty wound
    // Check if we hit the given element
    diff = searchInfo.x - searchInfo.cursorX

    if (
      ((diff <= 0 && Math.abs(diff) < searchInfo.diffX - 0.001) ||
        (diff > 0 && diff < searchInfo.diffX + 0.001)) &&
      (searchInfo.allowInText || searchInfo.x > searchInfo.cursorX)
    ) {
      searchInfo.diffX = Math.abs(diff)
      searchInfo.pos.updatePosByLevel(curIndex, lvl)
      result = true
    }
  } else {
    for (; curIndex < endIndex; curIndex++) {
      const item = paraRun.children.at(curIndex)
      if (item == null) continue
      const itemType = item.instanceType
      const curVWidth = item.getVisibleWidth()
      // Check if we hit the given element
      diff = searchInfo.x - searchInfo.cursorX

      if (
        ((diff <= 0 && Math.abs(diff) < searchInfo.diffX - 0.001) ||
          (diff > 0 && diff < searchInfo.diffX + 0.001)) &&
        (searchInfo.allowInText || searchInfo.x > searchInfo.cursorX)
      ) {
        searchInfo.diffX = Math.abs(diff)
        searchInfo.pos.updatePosByLevel(curIndex, lvl)
        result = true

        if (diff >= -0.001 && diff <= curVWidth + 0.001) {
          searchInfo.positionInText.updatePosByLevel(curIndex, lvl)
          searchInfo.hitTextContent = true
        }
      }

      searchInfo.cursorX += curVWidth

      // Placeholder for paragraph mark and end of line
      diff = searchInfo.x - searchInfo.cursorX
      if (
        Math.abs(diff) < searchInfo.diffX + 0.001 &&
        (searchInfo.allowInText || searchInfo.x > searchInfo.cursorX)
      ) {
        if (InstanceType.ParaEnd === itemType) {
          searchInfo.isParaEnd = true

          // If we are looking for a position for select, then we need to look behind the paragraph mark
          if (true === isIncludeEnd) {
            searchInfo.diffX = Math.abs(diff)
            searchInfo.pos.updatePosByLevel(paraRun.children.length, lvl)
            result = true
          }
        } else if (
          curIndex === endIndex - 1 &&
          InstanceType.ParaNewLine !== itemType
        ) {
          searchInfo.diffX = Math.abs(diff)
          searchInfo.pos.updatePosByLevel(endIndex, lvl)
          result = true
        }
      }
    }
  }

  if (searchInfo.diffX > 1000000 - 1) {
    searchInfo.diffX = searchInfo.x - searchInfo.cursorX
    searchInfo.pos.updatePosByLevel(startIndex, lvl)
    result = true
  }

  return result
}

export function clearClosestPositions(para: Paragraph) {
  const count = para.adjacentPositions.length

  for (let i = 0; i < count; i++) {
    const paraAdjPos = para.adjacentPositions[i]

    const entityCount = paraAdjPos.enities.length

    for (let j = 1; j < entityCount; j++) {
      const entity = paraAdjPos.enities[j]
      entity.adjacentPositions = []
    }
  }

  para.adjacentPositions = []
}

export function getParaClosestPos(para: Paragraph, adjPos: AdjacentPosition) {
  const count = para.adjacentPositions.length

  for (let i = 0; i < count; i++) {
    const paraAdjPos = para.adjacentPositions[i]

    if (adjPos === paraAdjPos.adjacentPos) return paraAdjPos
  }

  return null
}
