import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import { CurrentFocusElementsInfo } from '../../Slide/CurrentFocusElementsInfo'
import { TextOperationDirection } from '../../utilities/type'
import { ParagraphElementPosition } from '../common'
import { ParaHyperlink } from '../ParaContent/ParaHyperlink'
import { ParaRun } from '../ParaContent/ParaRun'
import { PresentationField } from '../ParaContent/PresentationField'
import { getPosIndexByContentPosition } from '../ParaContent/utils/selection'
import { Paragraph } from '../Paragraph'
import { ParagraphItem } from '../type'
import { getEndPosOfParagraph, getStartPosOfParagraph } from './position'

type PositionFlag = 0 | 1 | -1
export function updateSelectionStateForParaItem(
  paraItem: ParaRun | ParaHyperlink,
  startElmPosition: Nullable<ParagraphElementPosition>,
  endElmPosition: Nullable<ParagraphElementPosition>,
  lvl: number,
  startFlag: PositionFlag,
  endFlag: PositionFlag
) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    const startIndex = getPosIndexByContentPosition(
      paraItem,
      startFlag,
      startElmPosition,
      lvl
    )

    const endIndex = getPosIndexByContentPosition(
      paraItem,
      endFlag,
      endElmPosition,
      lvl
    )

    const selection = paraItem.state.selection
    selection.startIndex = startIndex
    selection.endIndex = endIndex
    selection.isUse = true
  } else {
    if (paraItem.children.length <= 0) return

    const selection = paraItem.selection

    let oldStartIndex = selection.startIndex
    let oldEndIndex = selection.endIndex

    if (oldStartIndex > oldEndIndex) {
      oldStartIndex = selection.endIndex
      oldEndIndex = selection.startIndex
    }

    const startIndex = getPosIndexByContentPosition(
      paraItem,
      startFlag,
      startElmPosition,
      lvl
    )

    const endIndex = getPosIndexByContentPosition(
      paraItem,
      endFlag,
      endElmPosition,
      lvl
    )

    if (oldStartIndex < startIndex && oldStartIndex < endIndex) {
      const startIndex = Math.max(0, oldStartIndex)
      const eIndex = Math.min(
        paraItem.children.length - 1,
        Math.min(startIndex, endIndex) - 1
      )
      for (let i = startIndex; i <= eIndex; ++i) {
        paraItem.children.at(i)!.removeSelection()
      }
    }

    if (oldEndIndex > startIndex && oldEndIndex > endIndex) {
      const sIndex = Math.max(0, Math.max(startIndex, endIndex) + 1)
      const eIndex = Math.min(oldEndIndex, paraItem.children.length - 1)
      for (let i = sIndex; i <= eIndex; ++i) {
        paraItem.children.at(i)!.removeSelection()
      }
    }

    selection.isUse = true
    selection.startIndex = startIndex
    selection.endIndex = endIndex

    let run: Nullable<ParaRun>
    if (startIndex !== endIndex) {
      run = paraItem.children.at(startIndex)

      run &&
        updateSelectionStateForParaItem(
          run,
          startElmPosition,
          null,
          lvl + 1,
          startFlag,
          startIndex > endIndex ? 1 : -1
        )

      run = paraItem.children.at(endIndex)
      run &&
        updateSelectionStateForParaItem(
          run,
          null,
          endElmPosition,
          lvl + 1,
          startIndex > endIndex ? -1 : 1,
          endFlag
        )

      let _startIndex = startIndex
      let _endIndex = endIndex
      let direction: TextOperationDirection = 1

      if (_startIndex > _endIndex) {
        _startIndex = endIndex
        _endIndex = startIndex
        direction = -1
      }

      for (let i = _startIndex + 1; i < _endIndex; i++) {
        run = paraItem.children.at(i)
        run && run.selectAll(direction)
      }
    } else {
      run = paraItem.children.at(startIndex)
      run &&
        updateSelectionStateForParaItem(
          run,
          startElmPosition,
          endElmPosition,
          lvl + 1,
          startFlag,
          endFlag
        )
    }
  }
}

export function updateSelectionStateForParagraph(
  paragraph: Paragraph,
  startElmPosition: ParagraphElementPosition,
  endElmPosition: ParagraphElementPosition,
  needCorrectAnchor?: boolean
) {
  const lvl = 0

  let direction: TextOperationDirection = 1
  if (startElmPosition.compare(endElmPosition) > 0) direction = -1

  let oldStartIndex = Math.max(
    0,
    Math.min(paragraph.selection.startIndex, paragraph.children.length - 1)
  )
  let oldEndIndex = Math.max(
    0,
    Math.min(paragraph.selection.endIndex, paragraph.children.length - 1)
  )
  // 倒序选中文本则纠正起点终点
  if (oldStartIndex > oldEndIndex) {
    oldStartIndex = paragraph.selection.endIndex
    oldEndIndex = paragraph.selection.startIndex
  }

  const startIndex = startElmPosition.get(lvl)
  const endIndex = endElmPosition.get(lvl)

  paragraph.selection.startIndex = startIndex
  paragraph.selection.endIndex = endIndex
  correctParaSelectionState(paragraph)

  let paraItem: Nullable<ParagraphItem>
  if (oldStartIndex < startIndex && oldStartIndex < endIndex) {
    const _endIndex = Math.min(startIndex, endIndex)
    for (let i = oldStartIndex; i < _endIndex; i++) {
      paraItem = paragraph.children.at(i)
      paraItem && paraItem.removeSelection()
    }
  }

  if (oldEndIndex > startIndex && oldEndIndex > endIndex) {
    const _startIndex = Math.max(startIndex, endIndex)
    for (let i = _startIndex + 1; i <= oldEndIndex; i++) {
      paraItem = paragraph.children.at(i)
      paraItem && paraItem.removeSelection()
    }
  }

  if (startIndex === endIndex) {
    paraItem = paragraph.children.at(startIndex)
    paraItem &&
      updateSelectionStateForParaItem(
        paraItem,
        startElmPosition,
        endElmPosition,
        lvl + 1,
        0,
        0
      )
  } else {
    if (startIndex > endIndex) {
      paraItem = paragraph.children.at(startIndex)
      paraItem &&
        updateSelectionStateForParaItem(
          paraItem,
          startElmPosition,
          null,
          lvl + 1,
          0,
          1
        )
      paraItem = paragraph.children.at(endIndex)
      paraItem &&
        updateSelectionStateForParaItem(
          paraItem,
          null,
          endElmPosition,
          lvl + 1,
          -1,
          0
        )

      for (let i = endIndex + 1; i < startIndex; i++) {
        paraItem = paragraph.children.at(i)
        paraItem && paraItem.selectAll(-1)
      }
    } else {
      paraItem = paragraph.children.at(startIndex)
      paraItem &&
        updateSelectionStateForParaItem(
          paraItem,
          startElmPosition,
          null,
          lvl + 1,
          0,
          -1
        )
      paraItem = paragraph.children.at(endIndex)
      paraItem &&
        updateSelectionStateForParaItem(
          paraItem,
          null,
          endElmPosition,
          lvl + 1,
          1,
          0
        )

      for (let i = startIndex + 1; i < endIndex; i++) {
        paraItem = paragraph.children.at(i)
        paraItem && paraItem.selectAll(1)
      }
    }
  }

  if (false !== needCorrectAnchor) {
    if (true === paragraph.isParaEndSelected()) {
      let isNeedSelectAll = true
      const startIndex = Math.min(
        paragraph.selection.startIndex,
        paragraph.selection.endIndex
      )
      for (let i = 0; i <= startIndex; i++) {
        paraItem = paragraph.children.at(i)
        if (paraItem && false === paraItem.isSelectedAll()) {
          isNeedSelectAll = false
          break
        }
      }

      if (true === isNeedSelectAll) {
        if (1 === direction) paragraph.selection.startIndex = 0
        else paragraph.selection.endIndex = 0

        for (let i = 0; i <= startIndex; i++) {
          paraItem = paragraph.children.at(i)
          paraItem && paraItem.selectAll(direction)
        }
      }
    } else if (
      true !== paragraph.hasNoSelection(true) &&
      ((1 === direction && true === paragraph.selection.startBySet) ||
        (1 !== direction && true === paragraph.selection.endBySet))
    ) {
      let isNeedCorrectLeftPos = true
      const minIndex = Math.min(startIndex, endIndex)
      for (let i = 0; i < startIndex; i++) {
        paraItem = paragraph.children.at(i)
        if (paraItem && true !== paraItem.isEmpty()) {
          isNeedCorrectLeftPos = false
          break
        }
      }

      if (true === isNeedCorrectLeftPos) {
        for (let i = minIndex; i <= endIndex; i++) {
          paraItem = paragraph.children.at(i)
          if (
            paraItem &&
            true === _correctLeftPosOfSelection(paraItem, direction)
          ) {
            if (1 === direction) {
              if (i + 1 > paragraph.selection.endIndex) break

              paragraph.selection.startIndex = i + 1
            } else {
              if (i + 1 > paragraph.selection.startIndex) break

              paragraph.selection.endIndex = i + 1
            }

            paraItem.removeSelection()
          } else break
        }
      }
    }
  }
}

export function correctParaSelectionState(para: Paragraph) {
  para.selection.startIndex = Math.max(
    0,
    Math.min(para.children.length - 1, para.selection.startIndex)
  )
  para.selection.endIndex = Math.max(
    0,
    Math.min(para.children.length - 1, para.selection.endIndex)
  )
  para.cursorPosition.elementIndex = Math.max(
    0,
    Math.min(para.children.length - 1, para.cursorPosition.elementIndex)
  )
}

export function updateSelectionPositionOnRemove(
  para: Paragraph,
  position: number,
  count: number
) {
  if (para.cursorPosition.elementIndex >= position + count) {
    para.cursorPosition.elementIndex -= count
  } else if (para.cursorPosition.elementIndex >= position) {
    if (position < para.children.length) {
      para.cursorPosition.elementIndex = position
    } else if (position > 0) {
      para.cursorPosition.elementIndex = position - 1
    } else para.cursorPosition.elementIndex = 0
  }

  if (para.selection.startIndex <= para.selection.endIndex) {
    if (para.selection.startIndex >= position + count) {
      para.selection.startIndex -= count
    } else if (para.selection.startIndex >= position) {
      para.selection.startIndex = position
    }

    if (para.selection.endIndex >= position + count) {
      para.selection.endIndex -= count
    } else if (para.selection.endIndex >= position) {
      para.selection.startIndex = position - 1
    }

    if (para.selection.startIndex > para.selection.endIndex) {
      para.selection.isUse = false
      para.selection.startIndex = 0
      para.selection.endIndex = 0
    }
  } else {
    if (para.selection.endIndex >= position + count) {
      para.selection.endIndex -= count
    } else if (para.selection.endIndex >= position) {
      para.selection.endIndex = position
    }

    if (para.selection.startIndex >= position + count) {
      para.selection.startIndex -= count
    } else if (para.selection.startIndex >= position) {
      para.selection.startIndex = position - 1
    }

    if (para.selection.endIndex > para.selection.startIndex) {
      para.selection.isUse = false
      para.selection.startIndex = 0
      para.selection.endIndex = 0
    }
  }
  correctParaSelectionState(para)
}

export function updateSelectionBeginAndEndForParagraph(
  para: Paragraph,
  hasStartSelection: boolean,
  isStartPara: boolean
) {
  const elementPosition =
    true === isStartPara
      ? getStartPosOfParagraph(para)
      : getEndPosOfParagraph(para, true)

  if (true === hasStartSelection) {
    para.selection.startBySet = false
    updateSelectionStateForParagraph(
      para,
      elementPosition,
      para.currentElementPosition(true, false)
    )
  } else {
    para.selection.endBySet = false
    updateSelectionStateForParagraph(
      para,
      para.currentElementPosition(true, true),
      elementPosition
    )
  }
}

//helper
function _correctLeftPosOfSelection(
  paraItem: ParagraphItem,
  dir: TextOperationDirection
) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    if (false === paraItem.selection.isUse || true === paraItem.isEmpty()) {
      return true
    }

    return false
  } else {
    if (false === paraItem.selection.isUse || true === paraItem.isEmpty()) {
      return true
    }

    const selection = paraItem.state.selection
    const startIndex = Math.min(selection.startIndex, selection.endIndex)
    const endIndex = Math.max(selection.startIndex, selection.endIndex)

    for (let i = 0; i < startIndex; i++) {
      const run = paraItem.children.at(i)
      if (run && true !== run.isEmpty()) {
        return false
      }
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const run = paraItem.children.at(i)
      if (run && true === _correctLeftPosOfSelection(run, dir)) {
        if (1 === dir) paraItem.selection.startIndex = i + 1
        else paraItem.selection.endIndex = i + 1

        run.removeSelection()
      } else return false
    }

    return true
  }
}

export function getCurrentFocusElementsInfoForParagraph(
  paragraph: Paragraph,
  focusInfo: CurrentFocusElementsInfo,
  elementPosition?: ParagraphElementPosition,
  lvl?: number
) {
  focusInfo.setParagraph(paragraph)
  let item: Nullable<ParagraphItem>
  if (elementPosition) {
    const indexOfPosition = elementPosition.get(lvl!)
    item = paragraph.children.at(indexOfPosition)
  } else {
    if (
      true === paragraph.selection.isUse &&
      paragraph.selection.startIndex === paragraph.selection.endIndex
    ) {
      item = paragraph.children.at(paragraph.selection.endIndex)
    } else if (false === paragraph.selection.isUse) {
      item = paragraph.children.at(paragraph.cursorPosition.elementIndex)
    }
  }

  if (item) {
    if (item.instanceType === InstanceType.ParaHyperlink) {
      focusInfo.setHyperlink(item)
    } else if (item.isPresentationField()) {
      focusInfo.setPresentationField(item as PresentationField)
    }
  }
}
