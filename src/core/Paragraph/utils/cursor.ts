import {
  AdjacentPosition,
  ParagraphElementPosition,
  ParagraphLocationInfo,
} from '../common'
import { Paragraph } from '../Paragraph'
import { findLeftContentPosition, findRightContentPosition } from './position'
import { updateSelectionStateForParagraph } from './selection'

export function moveCursorToAdjacentPosInParagraph(
  para: Paragraph,
  adjPos: AdjacentPosition
) {
  para.updateCursorPosByPosition(adjPos.pos, true, -1)

  para.selection.isUse = true
  updateSelectionStateForParagraph(para, adjPos.pos, adjPos.pos)

  const startPosition = para.currentElementPosition(true, true)
  const endPosition = para.currentElementPosition(true, false)

  if (0 === startPosition.compare(endPosition)) para.removeSelection()
}

export function isCursorAtEndInParagraph(para: Paragraph) {
  const elementPosition = para.currentElementPosition(false, false)
  const locInfo = new ParagraphLocationInfo()

  findRightContentPosition(para, locInfo, elementPosition, false)

  if (true === locInfo.found) return false
  else return true
}

export function isCursorAtBeginInParagraph(
  para: Paragraph,
  elementPosition?: ParagraphElementPosition
) {
  elementPosition =
    null == elementPosition
      ? para.currentElementPosition(false, false)
      : elementPosition
  const locInfo = new ParagraphLocationInfo()

  findLeftContentPosition(para, locInfo, elementPosition)

  if (true === locInfo.found) return false
  else return true
}
