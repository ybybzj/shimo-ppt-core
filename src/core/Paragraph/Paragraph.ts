/* eslint-disable @typescript-eslint/no-unused-vars */
import { ParaPr } from '../textAttributes/ParaPr'

import { FontClass } from '../../fonts/const'
import { Factor_pt_to_mm } from '../common/const/unit'
import { TextPr, TextPrOptions } from '../textAttributes/TextPr'
import { LineHeightRule, AlignKind } from '../common/const/attrs'
import { NumberingFormat } from '../common/numberingFormat'
import { RFontsOptions } from '../textAttributes/RFonts'
import { ParaTablist } from '../textAttributes/ParaTabs'
import { ParaInd, ParaIndOptions } from '../textAttributes/ParaInd'
import { ParaSpacing, ParaSpacingOptions } from '../textAttributes/ParaSpacing'
import { idGenerator } from '../../globals/IdGenerator'
import { GlobalEvents } from '../../common/dom/events'
import { normalizeMMToTwips, isNumber } from '../../common/utils'
import { BulletTypeValue } from '../SlideElement/const'
import { EditorUtil } from '../../globals/editor'
import { ParaTextPr } from './ParaContent/ParaTextPr'
import { getBulletInfo, ParaNumbering } from './RunElement/ParaNumbering'
import { CheckEmptyRunOptions, ParaRun } from './ParaContent/ParaRun'
import { ParaEnd } from './RunElement/ParaEnd'

import {
  ParagraphElementPosition,
  ParagraphAdjacentPosition,
  ParagraphLocationInfo,
  ParaItemPosInfo,
  getParagraphTextGetter,
  AdjacentPosition,
} from './common'
import { Dict, Nullable } from '../../../liber/pervasive'
import { CurrentFocusElementsInfo } from '../Slide/CurrentFocusElementsInfo'
import { ParaHyperlink } from './ParaContent/ParaHyperlink'
import { InstanceType } from '../instanceTypes'

import { TextDocument } from '../TextDocument/TextDocument'

import { hookManager, Hooks } from '../hooks'
import { isEavertTextContent } from '../utilities/shape/getters'
import { HoverInfo } from '../../ui/states/type'
import { cursorExtType } from '../../ui/editorUI/uiCursor'
import { RunContentElement } from './RunElement/type'
import { ParaRenderingState } from './typeset/ParaRenderingState'

import { getTextHeight } from '../../fonts/FontKit'
import { informDocChange } from '../calculation/informChanges/textContent'
import { Bullet, BulletType } from '../SlideElement/attrs/bullet'
import { ParagraphSearchElement } from '../search/searchStates'
import { ParagraphItem } from './type'

import { updateTextPrFonts } from '../utilities/style'
import {
  findLeftContentPosition,
  findWordEndPos,
  findWordStartPos,
  getStartPosOfParagraph,
  getEndPosOfParagraph,
  updateClosestPosForParagraph,
  getSearchPositionInfoFromParagraph,
} from './utils/position'

import {
  moveCursorToEndPosForParaItem,
  moveCursorToEndPosInParagraph,
  moveCursorToStartPosForParaItem,
  moveCursorToStartPosInParagraph,
} from '../utilities/cursor/moveToEdgePos'
import {
  hasAdjacentPosInfoOfParaItem,
  updateParaContentPosition,
} from './ParaContent/utils/position'
import {
  correctParaSelectionState,
  getCurrentFocusElementsInfoForParagraph,
  updateSelectionPositionOnRemove,
  updateSelectionStateForParagraph,
} from './utils/selection'
import { beforeDeleteParaItem } from './ParaContent/utils/onDelete'
import {
  isCursorNeededCorrectPos,
  isCursorAtEndInParaItem,
  isCursorAtStartInParaItem,
} from './ParaContent/utils/cursor'
import { isCursorAtBeginInParagraph } from './utils/cursor'
import { applyTextPrToParaItem } from './ParaContent/utils/pr'
import { applyTextPrToParagraph } from './utils/pr'
import { TextOperationDirection } from '../utilities/type'
import { updateCalcStatusForTextContentItem } from '../calculation/updateCalcStatus/textContent'
import { FillEffects } from '../SlideElement/attrs/fill/fill'
import { ThemeColor } from '../../io/dataType/spAttrs'
import { JCAlignTypeValues } from '../../io/dataType/style'
import {
  defaultContentStylePrs,
  recylceTextContentStylePrs,
  TextContentStylePrs,
} from '../textAttributes/ContentStyle'
import { MouseMoveData, MouseMoveDataTypes } from '../properties/MouseMoveData'
import { HyperlinkOptions } from '../properties/options'
import { LineSegmentIndexesUtil } from './ParaContent/LineSegmentIndexes'
import { ManagedSliceUtil, RecyclableArray } from '../../common/managedArray'
import { PoolAllocator, Recyclable } from '../../common/allocator'
function recylceParagraphItem(item: ParagraphItem) {
  switch (item.instanceType) {
    case InstanceType.ParaHyperlink:
      ParaHyperlink.recycle(item)
      break
    case InstanceType.ParaRun:
      ParaRun.recycle(item)
      break
  }
}
export class Paragraph extends Recyclable<Paragraph> {
  readonly instanceType = InstanceType.Paragraph
  id: string
  prev: Nullable<Paragraph>
  next: Nullable<Paragraph>
  index: number
  hasCalculated: boolean
  renderingState: ParaRenderingState
  children: RecyclableArray<ParagraphItem>
  calcedPrs: {
    pr: Nullable<TextContentStylePrs>
    isDirty: boolean
  }
  pr: ParaPr
  parent?: TextDocument
  paraTextPr: ParaTextPr

  paraNumbering: ParaNumbering
  cursorPosition: ParagraphCursorPosition
  selection: ParagraphSelection
  isSetToAll: boolean
  searchResultElements: Dict<ParagraphSearchElement>
  adjacentPositions: ParagraphAdjacentPosition[]
  private static allocator: PoolAllocator<Paragraph, [parent?: TextDocument]> =
    PoolAllocator.getAllocator<Paragraph, [parent?: TextDocument]>(Paragraph)
  static new(parent?: TextDocument) {
    return Paragraph.allocator.allocate(parent)
  }
  static recycle(para: Paragraph) {
    Paragraph.allocator.recycle(para)
  }

  constructor(parent?: TextDocument) {
    super()
    this.id = idGenerator.newId()

    this.prev = null
    this.next = null
    this.index = -1

    this.hasCalculated = false

    this.renderingState = ParaRenderingState.new(this)

    this.parent = parent
    this.calcedPrs = {
      pr: null,
      isDirty: true,
    }
    this.pr = ParaPr.new()
    this.paraTextPr = new ParaTextPr(this)

    this.paraNumbering = new ParaNumbering()

    this.cursorPosition = {
      x: 0,
      y: 0,
      elementIndex: 0,
      line: -1,
      cursorX: 0,
      cursorY: 0,
      colIndex: 0,
    }

    this.selection = new ParagraphSelection()

    this.isSetToAll = false

    this.searchResultElements = {}

    this.adjacentPositions = []

    this.children = new RecyclableArray(recylceParagraphItem)
    const endRun = ParaRun.new(this)
    endRun.addItemAt(0, ParaEnd.new())

    this.children.insert(0, endRun)
  }

  get isRTL() {
    return this.pr.rtl === true || this.calcedPrs?.pr?.paraPr?.rtl === true
  }

  reset(parent?: TextDocument) {
    this.prev = null
    this.next = null
    this.index = -1

    this.hasCalculated = false

    this.parent = parent ?? this.parent
    this.calcedPrs.pr = null
    this.calcedPrs.isDirty = true

    this.pr.reset()
    this.paraTextPr.reset(this)

    this.paraNumbering.reset()

    this.cursorPosition.x = 0
    this.cursorPosition.y = 0
    this.cursorPosition.elementIndex = 0
    this.cursorPosition.line = -1
    this.cursorPosition.cursorX = 0
    this.cursorPosition.cursorY = 0
    this.cursorPosition.colIndex = 0

    this.selection.reset()
    this.isSetToAll = false

    this.searchResultElements = {}

    this.adjacentPositions.length = 0

    this.children.clear()

    const endRun = ParaRun.new(this)
    endRun.addItemAt(0, ParaEnd.new())

    this.children.insert(0, endRun)

    this.renderingState.reset(this)
  }

  setIndexInDocument(valueOfIndex: number) {
    this.index = valueOfIndex
  }

  setNextInDocument(element: Nullable<Paragraph>) {
    this.next = element
  }
  setPrevInDocument(element: Nullable<Paragraph>) {
    this.prev = element
  }

  getNextPara() {
    return this.next
  }
  getPrevPara() {
    return this.prev
  }
  setParent(parent: TextDocument | undefined) {
    this.parent = parent
  }

  getParent() {
    return this.parent
  }

  getId() {
    return this.id
  }

  getIndex() {
    if (!this.parent) return -1

    this.parent.updateChildrenIndexing()

    return this.index
  }

  getIndexOfStartColumn() {
    return this.renderingState.docStartColumnIndex
  }

  getParentSlideIndex() {
    return this.parent ? this.parent.getParentSlideIndex() : 0
  }

  isOnlyOneElementSelected() {
    if (this.parent) return this.parent.isOnlyOneElementSelected()

    return false
  }

  setPr(paraPr: ParaPr) {
    if (paraPr === this.pr) return
    informDocChange(this)

    this.pr.reset()

    this.pr.fromOptions(paraPr)

    this.invalidateCalcedPrs()
    this.invalidateAllItemsCalcedPrs()
    this.hasCalculated = false
  }

  setTextPr(textPr: TextPr) {
    for (let i = 0, l = this.children.length; i < l; ++i) {
      const item = this.children.at(i)!
      if (item.instanceType === InstanceType.ParaRun) {
        item.fromTextPr(textPr)

        if (i === l - 1) {
          this.paraTextPr.setTextPr(textPr)
        }
      }
    }
  }

  clone(parent: undefined | TextDocument) {
    const para = Paragraph.new(parent)

    const extra = { paragraph: para }

    para.setPr(this.pr)

    para.paraTextPr.setTextPr(this.paraTextPr.textPr)

    para.children.recycle(para.removeContent(0, para.children.length))

    const count = this.children.length
    for (let index = 0; index < count; index++) {
      const item = this.children.at(index)!

      para.addItemAt(para.children.length, item.clone(false, extra))
    }

    const endRun = ParaRun.new(para)
    endRun.addItemAt(0, ParaEnd.new())
    para.addItemAt(para.children.length, endRun)
    endRun.fromTextPr(this.paraTextPr.textPr)

    para.removeSelection()
    moveCursorToStartPosInParagraph(para, false)

    return para
  }

  getDocumentBounds(colIndex: number) {
    return this.renderingState.getColumnBounds(colIndex)
  }

  paraEndTextHeight() {
    const prs = this.getSpacingAdjustedPrs()
    const textPr = prs.textPr.clone()
    textPr.merge(this.paraTextPr.textPr)

    return getTextHeight(
      EditorUtil.FontKit,
      textPr,
      textPr.lang.fontClass ?? FontClass.ASCII
    )
  }

  getTheme() {
    if (this.parent) return this.parent.getTheme()

    return undefined
  }
  getColorMap() {
    if (this.parent) return this.parent.getColorMap()

    return null
  }

  private assignPrTo(targetParagraph: Paragraph) {
    targetParagraph.renderingState.x = this.renderingState.x
    targetParagraph.renderingState.xLimit = this.renderingState.xLimit

    targetParagraph.setPr(this.pr)

    targetParagraph.paraTextPr.fromTextPrData(this.paraTextPr.textPr)
  }

  addItemAt(index: number, item: ParagraphItem) {
    informDocChange(this)
    this.children.insert(index, item)

    if (this.cursorPosition.elementIndex >= index) {
      this.cursorPosition.elementIndex++
    }

    if (this.selection.startIndex >= index) {
      this.selection.startIndex++
    }

    if (this.selection.endIndex >= index) {
      this.selection.endIndex++
    }

    correctParaSelectionState(this)

    const lLen = this.adjacentPositions.length
    for (let index = 0; index < lLen; index++) {
      const paraAdjPos = this.adjacentPositions[index]
      const paraElementPos = paraAdjPos.adjacentPos!.pos

      if (paraElementPos.positonIndices[0] >= index) {
        paraElementPos.positonIndices[0]++
      }
    }

    for (const id in this.searchResultElements) {
      let elementPosition = this.searchResultElements[id].startPosition

      if (elementPosition.positonIndices[0] >= index) {
        elementPosition.positonIndices[0]++
      }

      elementPosition = this.searchResultElements[id].endPosition

      if (elementPosition.positonIndices[0] >= index) {
        elementPosition.positonIndices[0]++
      }
    }

    item.setParagraph(this)

    updateCalcStatusForTextContentItem(this)
  }

  private appendContent(items: ParagraphItem[]) {
    const startIndex = this.children.length
    this.children.append(items)

    informDocChange(this)

    for (let i = startIndex; i < this.children.length; i++) {
      const item = this.children.at(i)!
      item.setParagraph(this)
      invalidateItemCalcedPrs(item)
    }
    updateCalcStatusForTextContentItem(this)
  }

  removeChildAt(index: number) {
    const item = this.children.at(index)
    if (item == null) return []
    informDocChange(this)
    const removed = this.children.remove(index, 1)

    beforeDeleteParaItem(item)

    if (this.selection.startIndex > index) {
      this.selection.startIndex--
    }

    if (this.selection.endIndex >= index) {
      this.selection.endIndex--
    }

    if (this.cursorPosition.elementIndex > index) {
      this.cursorPosition.elementIndex--
    }

    correctParaSelectionState(this)

    const l = this.adjacentPositions.length
    for (let index = 0; index < l; index++) {
      const paraAdjPos = this.adjacentPositions[index]
      const paraElementPos = paraAdjPos.adjacentPos!.pos

      if (paraElementPos.positonIndices[0] > index) {
        paraElementPos.positonIndices[0]--
      }
    }

    for (const id in this.searchResultElements) {
      let elementPosition = this.searchResultElements[id].startPosition

      if (elementPosition.positonIndices[0] > index) {
        elementPosition.positonIndices[0]--
      }

      elementPosition = this.searchResultElements[id].endPosition

      if (elementPosition.positonIndices[0] > index) {
        elementPosition.positonIndices[0]--
      }
    }
    updateCalcStatusForTextContentItem(this)
    return removed
  }

  removeContent(index: number, count: number) {
    if (0 === index && this.children.length === count) {
      return this.clearContent()
    }

    for (
      let i = index, l = Math.min(index + count, this.children.length);
      i < l;
      ++i
    ) {
      beforeDeleteParaItem(this.children.at(i)!)
    }

    informDocChange(this)
    if (this.selection.startIndex > index + count) {
      this.selection.startIndex -= count
    } else if (this.selection.startIndex > index) {
      this.selection.startIndex = index
    }

    if (this.selection.endIndex > index + count) {
      this.selection.endIndex -= count
    }
    if (this.selection.endIndex > index) this.selection.endIndex = index

    if (this.cursorPosition.elementIndex > index + count) {
      this.cursorPosition.elementIndex -= count
    } else if (this.cursorPosition.elementIndex > index) {
      this.cursorPosition.elementIndex = index
    }

    correctParaSelectionState(this)

    const l = this.adjacentPositions.length
    for (let index = 0; index < l; index++) {
      const paraAdjPos = this.adjacentPositions[index]
      const paraElementPos = paraAdjPos.adjacentPos!.pos

      if (paraElementPos.positonIndices[0] > index + count) {
        paraElementPos.positonIndices[0] -= count
      } else if (paraElementPos.positonIndices[0] > index) {
        paraElementPos.positonIndices[0] = Math.max(0, index)
      }
    }

    const removed = this.children.remove(index, count)
    updateSelectionPositionOnRemove(this, index, count)
    updateCalcStatusForTextContentItem(this)
    return removed
  }

  private clearContent() {
    if (this.children.length <= 0) return []
    ManagedSliceUtil.forEach(this.children, (item) => {
      beforeDeleteParaItem(item)
    })

    informDocChange(this)

    this.selection.startIndex = 0
    this.selection.endIndex = 0
    this.cursorPosition.elementIndex = 0

    this.adjacentPositions = []

    updateCalcStatusForTextContentItem(this)
    return this.children.remove(0, this.children.length)
  }

  paraItemPosInfoByContentPos(elementPosition: ParagraphElementPosition) {
    const curItem = this.children.at(elementPosition.get(0))
    if (curItem == null) return undefined
    let paraPos: ParaItemPosInfo
    if (curItem.instanceType === InstanceType.ParaHyperlink) {
      const indexOfPosition = elementPosition.get(1)
      const curRun = curItem.children.at(indexOfPosition)
      if (curRun == null) return undefined
      paraPos = curRun.getParaItemPosInfoByContentPos(elementPosition, 2)
    } else {
      paraPos = curItem.getParaItemPosInfoByContentPos(elementPosition, 1)
    }

    const curLine = paraPos.line

    const colIndex = this.renderingState.getColIndexByLine(curLine)
    paraPos.colIndex = colIndex
    return paraPos
  }

  remove(dir: TextOperationDirection, isOnAddText?: boolean) {
    if (this.children.length <= 0) return false
    let result = true

    if (true === this.selection.isUse) {
      let { startIndex, endIndex } = this.selection

      if (startIndex > endIndex) {
        const temp = startIndex
        startIndex = endIndex
        endIndex = temp
      }

      let paraItem: Nullable<ParagraphItem>
      if (endIndex === this.children.length - 1) {
        paraItem = this.children.at(endIndex)!
        if (
          paraItem.instanceType === InstanceType.ParaRun &&
          true === paraItem.isParaEndSelected()
        ) {
          result = false
        }
      }

      if (startIndex === endIndex) {
        paraItem = this.children.at(startIndex)
        if (paraItem && paraItem.isPresentationField()) {
          this.children.recycle(this.removeContent(startIndex, 1))
          if (this.children.length <= 1) {
            this.addItemAt(0, ParaRun.new(this))
            this.cursorPosition.elementIndex = 0
          } else if (startIndex > 0) {
            this.cursorPosition.elementIndex = startIndex - 1
            paraItem = this.children.at(startIndex - 1)
            paraItem && moveCursorToEndPosForParaItem(paraItem)
          } else {
            this.cursorPosition.elementIndex = startIndex
            paraItem = this.children.at(startIndex)
            paraItem && moveCursorToStartPosForParaItem(paraItem)
          }
          this.correctElementIndex()
        } else if (paraItem) {
          paraItem.remove(dir, isOnAddText)

          if (
            startIndex < this.children.length - 2 &&
            true === paraItem.isEmpty() &&
            true !== hasAdjacentPosInfoOfParaItem(paraItem) &&
            !isOnAddText
          ) {
            if (this.selection.startIndex === this.selection.endIndex) {
              this.selection.isUse = false
            }

            this.children.recycle(this.removeChildAt(startIndex))

            this.cursorPosition.elementIndex = startIndex
            paraItem = this.children.at(startIndex)
            paraItem && moveCursorToStartPosForParaItem(paraItem)
            this.correctElementIndex()
          }
        }
      } else {
        let isDeleteStart = false
        let isDeleteEnd = false
        paraItem = this.children.at(endIndex)
        if (paraItem && paraItem.isPresentationField()) {
          this.children.recycle(this.removeContent(endIndex, 1))
          isDeleteEnd = true

          if (this.children.length <= 1) {
            this.addItemAt(0, ParaRun.new(this))
            this.cursorPosition.elementIndex = 0
          } else if (endIndex > 0) {
            this.cursorPosition.elementIndex = endIndex - 1
            paraItem = this.children.at(endIndex - 1)
            paraItem && moveCursorToEndPosForParaItem(paraItem)
          } else {
            this.cursorPosition.elementIndex = endIndex
            paraItem = this.children.at(endIndex)
            paraItem && moveCursorToStartPosForParaItem(paraItem)
          }
          this.correctElementIndex()
        } else if (paraItem) {
          paraItem.remove(dir, isOnAddText)

          if (
            endIndex < this.children.length - 2 &&
            true === paraItem.isEmpty() &&
            true !== hasAdjacentPosInfoOfParaItem(paraItem)
          ) {
            this.children.recycle(this.removeContent(endIndex, 1))
            isDeleteEnd = true

            this.cursorPosition.elementIndex = endIndex
            paraItem = this.children.at(endIndex)
            paraItem && moveCursorToStartPosForParaItem(paraItem)
          }
        }

        this.children.recycle(
          this.removeContent(startIndex + 1, endIndex - startIndex - 1)
        )

        paraItem = this.children.at(startIndex)
        if (paraItem && paraItem.isPresentationField()) {
          this.children.recycle(this.removeContent(startIndex, 1))
          isDeleteStart = true

          if (this.children.length <= 1) {
            this.addItemAt(0, ParaRun.new(this))
            this.cursorPosition.elementIndex = 0
          }
        } else if (paraItem) {
          paraItem.remove(dir, isOnAddText)

          if (
            startIndex <= this.children.length - 2 &&
            true === paraItem.isEmpty() &&
            true !== hasAdjacentPosInfoOfParaItem(paraItem) &&
            ((dir > -1 && true !== isOnAddText) ||
              InstanceType.ParaRun !== paraItem.instanceType)
          ) {
            this.children.recycle(this.removeContent(startIndex, 1))
            isDeleteStart = true
          }
        }

        if (isDeleteStart && isDeleteEnd) {
          this.selection.isUse = false
        }

        if (dir > -1 && true !== isOnAddText) {
          this.correctElementIndex()
        } else {
          this.cursorPosition.elementIndex = startIndex
          this.selection.startIndex = startIndex
          this.selection.endIndex = startIndex
          if (!this.children.at(startIndex)) {
            this.correctElementIndex()
          }
        }
      }

      paraItem = this.children.at(this.cursorPosition.elementIndex)
      if (!paraItem || true !== paraItem.hasTextSelection()) {
        this.removeSelection()

        if (dir > -1 && true !== isOnAddText) this.correctChildren()
      } else {
        this.selection.isUse = true
        this.selection.start = false
        this.selection.startIndex = this.cursorPosition.elementIndex
        this.selection.endIndex = this.cursorPosition.elementIndex

        if (dir > -1 && true !== isOnAddText) this.correctChildren()

        this.selectSelf()

        return true
      }
    } else {
      let elmentIndex = this.cursorPosition.elementIndex
      if (elmentIndex < 0 || elmentIndex >= this.children.length) {
        result = false
      }

      let paraItem = this.children.at(elmentIndex)!
      while (false === paraItem.remove(dir, isOnAddText)) {
        if (dir < 0) elmentIndex--
        else elmentIndex++

        if (elmentIndex < 0 || elmentIndex >= this.children.length) {
          break
        }

        paraItem = this.children.at(elmentIndex)!
        if (dir < 0) {
          moveCursorToEndPosForParaItem(paraItem, false)
        } else moveCursorToStartPosForParaItem(paraItem)
      }

      if (elmentIndex < 0 || elmentIndex >= this.children.length) {
        result = false
      } else {
        if (true === paraItem.hasTextSelection()) {
          this.selection.isUse = true
          this.selection.start = false

          return true
        }

        if (
          elmentIndex < this.children.length - 2 &&
          true === paraItem.isEmpty()
        ) {
          this.removeChildAt(elmentIndex)

          this.cursorPosition.elementIndex = elmentIndex
          moveCursorToStartPosForParaItem(paraItem)
          this.correctElementIndex()
        } else {
          this.cursorPosition.elementIndex = elmentIndex
        }
      }

      this.correctChildren(elmentIndex, elmentIndex)

      if (dir < 0 && false === result) {
        result = true

        if (NumberingFormat.None !== this.paraNumbering.getType()) {
          this.removeNumbering()
        } else {
          result = false
        }
      }
    }

    return result
  }

  private removeParaEnd() {
    const contentLen = this.children.length
    for (let i = contentLen - 1; i >= 0; i--) {
      const item = this.children.at(i)

      if (
        item &&
        item.instanceType === InstanceType.ParaRun &&
        true === item.removeParaEnd()
      ) {
        return
      }
    }
  }

  private runTextPrAt(elementPosition: ParagraphElementPosition) {
    const index = elementPosition.get(0)
    const curItem = this.children.at(index)
    if (curItem == null) return undefined
    if (curItem.instanceType === InstanceType.ParaHyperlink) {
      return curItem.textPrAt(elementPosition, 1)
    } else {
      return curItem.getTextPr()
    }
  }

  applyTextPr(textPrOpts: TextPrOptions) {
    let textPr = textPrOpts
    if (null != textPrOpts.fontFamily) {
      textPr = TextPr.new(textPrOpts)
      const fontName = textPrOpts.fontFamily.name
      const index = textPrOpts.fontFamily.index

      const rFonts: RFontsOptions = {}
      rFonts.ascii = { name: fontName, index: index }
      rFonts.eastAsia = { name: fontName, index: index }
      rFonts.hAnsi = { name: fontName, index: index }
      rFonts.cs = { name: fontName, index: index }
      ;(textPr as TextPr).rFonts.fromOptions(rFonts)
    }

    if (true === this.isSetToAll) {
      const contentLen = this.children.length

      for (let i = 0; i < contentLen; i++) {
        applyTextPrToParaItem(this.children.at(i)!, textPr, undefined, true)
      }

      this.paraTextPr.fromTextPrData(textPr)
    } else {
      if (true === this.selection.isUse) {
        applyTextPrToParagraph(this, textPr)
      } else {
        const curElemPosition = this.currentElementPosition(false, false)

        const leftLocInfo = new ParagraphLocationInfo()
        findLeftContentPosition(this, leftLocInfo, curElemPosition)

        const runItem = this.runElementAtPos(curElemPosition)
        const runItemOnLeft =
          false === leftLocInfo.found
            ? null
            : this.runElementAtPos(leftLocInfo.pos)

        if (null == runItem || InstanceType.ParaEnd === runItem.instanceType) {
          applyTextPrToParagraph(this, textPr)

          this.paraTextPr.fromTextPrData(textPr)
        } else if (
          null != runItem &&
          null != runItemOnLeft &&
          InstanceType.ParaText === runItem.instanceType &&
          InstanceType.ParaText === runItemOnLeft.instanceType &&
          false === runItem.isPunctuation() &&
          false === runItemOnLeft.isPunctuation()
        ) {
          const startLocInfo = new ParagraphLocationInfo()
          const endLocInfo = new ParagraphLocationInfo()

          findWordStartPos(this, startLocInfo, curElemPosition)
          findWordEndPos(this, endLocInfo, curElemPosition)

          if (true !== startLocInfo.found || true !== endLocInfo.found) {
            return
          }

          this.selection.isUse = true
          updateSelectionStateForParagraph(
            this,
            startLocInfo.pos,
            endLocInfo.pos
          )

          applyTextPrToParagraph(this, textPr)

          this.removeSelection()
        } else {
          applyTextPrToParagraph(this, textPr)
        }
      }
    }
  }

  add(item: RunContentElement | ParagraphItem) {
    if (item.instanceType === InstanceType.ParaRun) {
      item.parent = this
    }

    if (
      item.instanceType === InstanceType.ParaHyperlink ||
      item.instanceType === InstanceType.ParaRun
    ) {
      item.setParagraph(this)
    }

    switch (item.instanceType) {
      case InstanceType.ParaHyperlink: {
        const elementPosition = this.currentElementPosition(false, false)
        const curIndex = elementPosition.get(0)

        const curItem = this.children.at(curIndex)
        if (curItem) {
          if (InstanceType.ParaRun === curItem.instanceType) {
            const newElement = curItem.splitAtContentPos(elementPosition, 1)

            if (null !== newElement) {
              this.addItemAt(curIndex + 1, newElement)
            }

            this.addItemAt(curIndex + 1, item)

            this.cursorPosition.elementIndex = curIndex + 2
            moveCursorToStartPosForParaItem(
              this.children.at(this.cursorPosition.elementIndex)!
            )
          } else {
            curItem.add(item)
          }
        }

        break
      }
      case InstanceType.ParaRun: {
        const elementPosition = this.currentElementPosition(false, false)
        const curIndex = elementPosition.get(0)

        const curItem = this.children.at(curIndex)

        if (curItem) {
          switch (curItem.instanceType) {
            case InstanceType.ParaRun: {
              const newRun = curItem.splitAtContentPos(elementPosition, 1)

              this.addItemAt(curIndex + 1, item)
              this.addItemAt(curIndex + 2, newRun)
              this.cursorPosition.elementIndex = curIndex + 1
              break
            }

            case InstanceType.ParaHyperlink: {
              curItem.add(item)
              break
            }

            default: {
              this.addItemAt(curIndex + 1, item)
              this.cursorPosition.elementIndex = curIndex + 1
              break
            }
          }

          moveCursorToEndPosForParaItem(item, false)
        }
        break
      }
      default: {
        const paraItem = this.children.at(this.cursorPosition.elementIndex)
        if (paraItem) {
          // 在超链接两端输入的内容不加入到超链接中
          if (InstanceType.ParaHyperlink === paraItem.instanceType) {
            const { elementIndex, selection } = paraItem.state
            if (this.cursorPosition.elementIndex !== 0 && !selection.isUse) {
              const run = paraItem.children.at(elementIndex)
              if (run) {
                if (elementIndex === 0 && run.state.elementIndex <= 0) {
                  // cursor 在开头的情况
                  const newRun = ParaRun.new(this)
                  newRun.addItemAt(0, item)
                  const preRunElement = this.children.at(
                    this.cursorPosition.elementIndex - 1
                  )
                  if (
                    preRunElement &&
                    preRunElement.instanceType === InstanceType.ParaRun &&
                    preRunElement.pr
                  ) {
                    newRun.fromTextPr(preRunElement.pr)
                  }
                  this.addItemAt(this.cursorPosition.elementIndex, newRun)
                  break
                } else if (
                  elementIndex === paraItem.children.length - 1 &&
                  isCursorAtEndInParaItem(run)
                ) {
                  // cursor 在结尾的情况
                  const newRun = ParaRun.new(this)
                  newRun.addItemAt(0, item)
                  const preRunElement = paraItem.children.at(
                    paraItem.children.length - 1
                  )
                  if (preRunElement && preRunElement.pr) {
                    newRun.fromTextPr(preRunElement.pr)
                  }
                  this.addItemAt(this.cursorPosition.elementIndex + 1, newRun)
                  break
                }
              }
            }
          }

          paraItem.add(item)
        }
        break
      }
    }
  }

  changeFontSize(isIncrease: boolean) {
    if (true === this.isSetToAll) {
      const contentLen = this.children.length

      for (let i = 0; i < contentLen; i++) {
        applyTextPrToParaItem(this.children.at(i)!, undefined, isIncrease, true)
      }
    } else {
      if (true === this.selection.isUse) {
        applyTextPrToParagraph(this, undefined, isIncrease)
      } else {
        const curElemPosition = this.currentElementPosition(false, false)

        const leftLocInfo = new ParagraphLocationInfo()
        findLeftContentPosition(this, leftLocInfo, curElemPosition)

        const runItem = this.runElementAtPos(curElemPosition)
        const runItemOnLeft =
          false === leftLocInfo.found
            ? null
            : this.runElementAtPos(leftLocInfo.pos)

        if (null == runItem || InstanceType.ParaEnd === runItem.instanceType) {
          applyTextPrToParagraph(this, undefined, isIncrease)
        } else if (
          null != runItem &&
          null != runItemOnLeft &&
          InstanceType.ParaText === runItem.instanceType &&
          InstanceType.ParaText === runItemOnLeft.instanceType &&
          false === runItem.isPunctuation() &&
          false === runItemOnLeft.isPunctuation()
        ) {
          const startLocInfo = new ParagraphLocationInfo()
          const endLocInfo = new ParagraphLocationInfo()

          findWordStartPos(this, startLocInfo, curElemPosition)
          findWordEndPos(this, endLocInfo, curElemPosition)

          if (true !== startLocInfo.found || true !== endLocInfo.found) {
            return
          }

          this.selection.isUse = true
          updateSelectionStateForParagraph(
            this,
            startLocInfo.pos,
            endLocInfo.pos
          )

          applyTextPrToParagraph(this, undefined, isIncrease)

          this.removeSelection()
        } else {
          applyTextPrToParagraph(this, undefined, isIncrease)
        }
      }
    }

    return true
  }

  canChangeIndentLevel(isIncrease: boolean) {
    const curLevel = isNumber(this.pr.lvl) ? this.pr.lvl : 0
    const oldPr = this.getCalcedPrs(false)!.textPr
    let newPr: TextPr
    if (isIncrease) {
      if (curLevel >= 8) {
        return false
      }
      newPr = this.getTextStylePr(curLevel + 1).textPr
    } else {
      if (curLevel <= 0) {
        return false
      }
      newPr = this.getTextStylePr(curLevel - 1).textPr
    }
    const fontSizeDiff = newPr.fontSize! - oldPr.fontSize!
    if (this.pr.defaultRunPr && isNumber(this.pr.defaultRunPr.fontSize)) {
      if (this.pr.defaultRunPr.fontSize + fontSizeDiff < 1) {
        return false
      }
    }
    if (isNumber(this.paraTextPr.textPr.fontSize)) {
      if (this.paraTextPr.textPr.fontSize + fontSizeDiff < 1) {
        return false
      }
    }
    for (let i = 0, l = this.children.length; i < l; ++i) {
      const item = this.children.at(i)!
      if (item.instanceType === InstanceType.ParaRun) {
        const runPr = item.getCalcedPr()
        if (runPr.fontSize! + fontSizeDiff < 1) {
          return false
        }
      } else if (item.instanceType === InstanceType.ParaHyperlink) {
        for (let j = 0, l = item.children.length; j < l; ++j) {
          const paraRun = item.children.at(j)!
          if (paraRun.instanceType === InstanceType.ParaRun) {
            const runPr = paraRun.getCalcedPr()
            if (runPr.fontSize! + fontSizeDiff < 1) {
              return false
            }
          }
        }
      }
    }
    return true
  }
  changeIndentLevel(isIncrease: boolean) {
    const curLevel = isNumber(this.pr.lvl) ? this.pr.lvl : 0,
      oldPr = this.getCalcedPrs(false)!.textPr
    let newPr: TextPr
    if (isIncrease) {
      newPr = this.getTextStylePr(curLevel + 1).textPr
      if (this.pr.ind && this.pr.ind.left != null) {
        this.setIndent(
          {
            left: this.pr.ind.left + 11.1125,
            firstLine: this.pr.ind.firstLine,
          },
          false
        )
      }
      this.setPrLevel(curLevel + 1)
    } else {
      newPr = this.getTextStylePr(curLevel - 1).textPr
      if (this.pr.ind && this.pr.ind.left != null) {
        this.setIndent(
          {
            firstLine: this.pr.ind.firstLine,
            left: this.pr.ind.left - 11.1125,
          },
          false
        )
      }
      this.setPrLevel(curLevel - 1)
    }
    const fontSizeDiff = newPr.fontSize! - oldPr.fontSize!
    if (fontSizeDiff !== 0) {
      if (this.pr.defaultRunPr && isNumber(this.pr.defaultRunPr.fontSize)) {
        const newParaPr = this.pr.clone()
        newParaPr.defaultRunPr!.fontSize! += fontSizeDiff
        this.setPr(newParaPr)
      }
      if (isNumber(this.paraTextPr.textPr.fontSize)) {
        this.paraTextPr.setFontSize(
          this.paraTextPr.textPr.fontSize + fontSizeDiff
        )
      }
      for (let i = 0, l = this.children.length; i < l; ++i) {
        const item = this.children.at(i)!
        if (item.instanceType === InstanceType.ParaRun) {
          if (isNumber(item.pr.fontSize)) {
            item.setFontSize(item.pr.fontSize + fontSizeDiff)
          }
        } else if (item.instanceType === InstanceType.ParaHyperlink) {
          for (let j = 0; j < item.children.length; ++j) {
            const hyperItem = item.children.at(j)
            if (hyperItem && hyperItem.instanceType === InstanceType.ParaRun) {
              if (isNumber(hyperItem.pr.fontSize)) {
                hyperItem.setFontSize(hyperItem.pr.fontSize! + fontSizeDiff)
              }
            }
          }
        }
      }
    }
  }

  correctElementIndex(isCorrectEndLine?: boolean) {
    if (this.hasTextSelection()) {
      return
    }

    const count = this.children.length
    let elmIndex = this.cursorPosition.elementIndex
    let item: Nullable<ParagraphItem>
    if (isCorrectEndLine != null) {
      item = this.children.at(elmIndex)
      if (
        item &&
        true === isCorrectEndLine &&
        true === isCursorAtEndInParaItem(item)
      ) {
        let curIndex = elmIndex + 1
        while (curIndex < count) {
          item = this.children.at(curIndex)
          if (item == null || item.isEmpty() === true) {
            curIndex++
          } else {
            break
          }
        }
        item = this.children.at(curIndex)
        if (item && true === isAtBeginOfNewLine(item)) {
          elmIndex = curIndex
          moveCursorToStartPosForParaItem(item)
        }
      }

      while (elmIndex > 0) {
        item = this.children.at(elmIndex)
        const itemPrev = this.children.at(elmIndex - 1)
        if (
          item &&
          true === isCursorNeededCorrectPos(item) &&
          itemPrev &&
          itemPrev.instanceType === InstanceType.ParaRun &&
          !itemPrev.isPresentationField()
        ) {
          elmIndex--
          item = this.children.at(elmIndex)
          item && moveCursorToEndPosForParaItem(item)
        } else {
          break
        }
      }

      this.cursorPosition.elementIndex = elmIndex
    }

    elmIndex = Math.min(
      Math.max(0, this.cursorPosition.elementIndex),
      count - 1
    )

    while (elmIndex > 0) {
      item = this.children.at(elmIndex)
      if (
        item &&
        InstanceType.ParaRun !== item.instanceType &&
        true === isCursorAtStartInParaItem(item)
      ) {
        elmIndex--
        item = this.children.at(elmIndex)
        item && moveCursorToEndPosForParaItem(item)
      } else {
        break
      }
    }

    while (elmIndex < count) {
      item = this.children.at(elmIndex)
      if (
        item &&
        InstanceType.ParaRun !== item.instanceType &&
        true === isCursorAtEndInParaItem(item)
      ) {
        elmIndex++
        item = this.children.at(elmIndex)
        item && moveCursorToStartPosForParaItem(item)
      } else {
        break
      }
    }

    this._correctCursorPosSegmentLine()

    this.cursorPosition.elementIndex = elmIndex
  }

  currentElementPosition(isSelection: boolean, isStart?: boolean) {
    const elementPosition = new ParagraphElementPosition()

    const pos =
      true !== isSelection
        ? this.cursorPosition.elementIndex
        : false !== isStart
          ? this.selection.startIndex
          : this.selection.endIndex

    elementPosition.add(pos)

    if (pos < 0 || pos >= this.children.length) return elementPosition

    updateParaContentPosition(
      this.children.at(pos)!,
      isSelection,
      isStart === true,
      elementPosition
    )
    return elementPosition
  }

  updateCursorPosByPosition(
    elementPosition: ParagraphElementPosition,
    isCorrectEndLine: boolean,
    line: number,
    correctPos?: boolean
  ) {
    let pos = elementPosition.get(0)

    if (pos >= this.children.length) pos = this.children.length - 1

    if (pos < 0) pos = 0

    this.cursorPosition.elementIndex = pos
    const item = this.children.at(pos)
    item && item.updateStateContentPos(elementPosition, 1)

    if (false !== correctPos) {
      this.correctElementIndex(isCorrectEndLine)
    }

    this.cursorPosition.line = line
  }

  getContentPositionByElement(element: ParaHyperlink | ParaRun) {
    const elementPosition = new ParagraphElementPosition()

    const curLine = element.startParaLine

    const pos = this.renderingState.getContentPos(curLine)
    if (null != pos) {
      const [startIndex, endIndex] = pos

      if (
        0 <= startIndex &&
        startIndex < this.children.length &&
        0 <= endIndex &&
        endIndex < this.children.length
      ) {
        for (let i = startIndex; i <= endIndex; i++) {
          const item = this.children.at(i)!
          elementPosition.updatePosByLevel(i, 0)

          if (
            (item.instanceType === InstanceType.ParaRun && item === element) ||
            (item.instanceType === InstanceType.ParaHyperlink &&
              true ===
                item.updatePosByRun(element, elementPosition, 1, true, curLine))
          ) {
            return elementPosition
          }
        }
      }
    }

    for (let i = 0, l = this.children.length; i < l; i++) {
      const item = this.children.at(i)!

      elementPosition.updatePosByLevel(i, 0)

      if (
        (item.instanceType === InstanceType.ParaRun && item === element) ||
        (item.instanceType === InstanceType.ParaHyperlink &&
          true === item.updatePosByRun(element, elementPosition, 1, false, -1))
      ) {
        return elementPosition
      }
    }

    return null
  }

  getParaItemsByContentPosition(elementPosition: ParagraphElementPosition) {
    const items: Array<ParagraphItem> = []

    if (elementPosition == null) {
      return items
    }
    const indexOfPosition = elementPosition.get(0)
    if (0 <= indexOfPosition && indexOfPosition <= this.children.length - 1) {
      const item = this.children.at(indexOfPosition)!
      if (item.instanceType === InstanceType.ParaRun) {
        items.push(item)
      } else {
        items.push(item)
        const index = elementPosition.get(1)
        if (0 <= index && index <= item.children.length - 1) {
          items.push(item.children.at(index)!)
        }
      }
    }

    return items
  }

  private runElementAtPos(elementPosition: ParagraphElementPosition) {
    let index = elementPosition.get(0)
    const contentLen = this.children.length

    let item = this.children.at(index)
    let element = item ? item.runElementAtPos(elementPosition, 1) : null

    while (null == element) {
      index++

      if (index >= contentLen) break
      item = this.children.at(index)
      element = item ? item.runElementAtPos() : null
    }

    return element
  }

  hasTextSelection() {
    return this.selection.isUse
  }

  correctChildren(
    startPosIndex?: number,
    endPosIndex?: number,
    keepEmptyRuns?: boolean
  ) {
    if (this.adjacentPositions.length >= 1) return

    startPosIndex = startPosIndex == null ? 0 : Math.max(startPosIndex - 1, 0)
    endPosIndex =
      endPosIndex == null
        ? this.children.length - 1
        : Math.min(endPosIndex + 1, this.children.length - 1)

    for (let i = endPosIndex; i >= startPosIndex; i--) {
      const paraItem = this.children.at(i)!

      if (
        (InstanceType.ParaHyperlink === paraItem.instanceType ||
          paraItem.isPresentationField()) &&
        true === paraItem.isEmpty() &&
        true !== hasAdjacentPosInfoOfParaItem(paraItem)
      ) {
        this.children.recycle(this.removeChildAt(i))
      } else if (InstanceType.ParaRun !== paraItem.instanceType) {
        if (
          i === this.children.length - 1 ||
          InstanceType.ParaRun !== this.children.at(i + 1)!.instanceType ||
          i === this.children.length - 2
        ) {
          const newRun = ParaRun.new(this)
          this.addItemAt(i + 1, newRun)
        }

        if (
          startPosIndex === i &&
          (0 === i ||
            InstanceType.ParaRun !== this.children.at(i - 1)!.instanceType)
        ) {
          const newRun = ParaRun.new(this)
          this.addItemAt(i, newRun)
        }
      } else {
        if (true !== keepEmptyRuns) {
          if (
            true === paraItem.isEmpty() &&
            0 < i &&
            i < this.children.length - 2 &&
            InstanceType.ParaRun === this.children.at(i + 1)!.instanceType
          ) {
            this.children.recycle(this.removeChildAt(i))
          }
        }
      }
    }

    if (
      1 === this.children.length ||
      (this.children.length > 1 &&
        InstanceType.ParaRun !==
          this.children.at(this.children.length - 2)!.instanceType)
    ) {
      const newRun = ParaRun.new(this)
      newRun.fromTextPr(this.paraTextPr.textPr)
      this.addItemAt(this.children.length - 1, newRun)
    }

    this.correctElementIndex()
  }

  getHyperlinkByXY(x: number, y: number, colIndex: number) {
    const info = new CurrentFocusElementsInfo()
    const { pos: index } = getSearchPositionInfoFromParagraph(
      this,
      x,
      y,
      colIndex,
      false,
      true
    )
    getCurrentFocusElementsInfoForParagraph(this, info, index, 0)
    return info.getHyperlink()
  }

  insertHyperlink(hyperlinkOptions: HyperlinkOptions) {
    const paragraph = this
    if (true === this.selection.isUse) {
      const hyperlink = ParaHyperlink.new(paragraph)

      if (null != hyperlinkOptions.value) {
        hyperlink.setValue(hyperlinkOptions.value)
      }

      if (null != hyperlinkOptions.toolTip) {
        hyperlink.setToolTip(hyperlinkOptions.toolTip)
      }

      let startElmPosition = this.currentElementPosition(true, true)
      let endElmPosition = this.currentElementPosition(true, false)

      if (startElmPosition.compare(endElmPosition) > 0) {
        const t = startElmPosition
        startElmPosition = endElmPosition
        endElmPosition = t
      }

      let startIndex = startElmPosition.get(0)
      let endIndex = endElmPosition.get(0)

      if (startIndex > endIndex) {
        const t = startIndex
        startIndex = endIndex
        endIndex = t
      }

      if (
        this.children.length - 1 === endIndex &&
        true === this.isParaEndSelected()
      ) {
        endElmPosition = getEndPosOfParagraph(this, false)
        endIndex = endElmPosition.get(0)
      }

      const newEndElement = this.children
        .at(endIndex)
        ?.splitAtContentPos(endElmPosition, 1)
      const newStartElement = this.children
        .at(startIndex)
        ?.splitAtContentPos(startElmPosition, 1)

      let hyperPosIndex = 0
      const itemsToHyperlink = this.removeContent(
        startIndex + 1,
        endIndex - startIndex
      )
      if (null != newStartElement) {
        hyperlink.addItemAt(hyperPosIndex++, newStartElement)
      }

      for (let i = 0, l = itemsToHyperlink.length; i < l; i++) {
        hyperlink.addItemAt(hyperPosIndex++, itemsToHyperlink[i])
      }

      if (0 === hyperlink.children.length) {
        hyperlink.addItemAt(0, ParaRun.new(paragraph))
      }

      this.addItemAt(startIndex + 1, hyperlink)

      if (null != newEndElement) {
        this.addItemAt(startIndex + 2, newEndElement)
      }

      this.removeSelection()
      this.selection.isUse = true
      this.selection.startIndex = startIndex + 1
      this.selection.endIndex = startIndex + 1

      moveCursorToStartPosForParaItem(hyperlink)
      hyperlink.selectAll()

      const textPr = TextPr.new()
      textPr.underline = true
      applyTextPrToParaItem(hyperlink, textPr, undefined, false)
    } else if (null != hyperlinkOptions.text && '' !== hyperlinkOptions.text) {
      const elementPosition = this.currentElementPosition(false, false)
      const index = elementPosition.get(0)

      const textPr = this.runTextPrAt(elementPosition)

      const hyperlink = ParaHyperlink.new(paragraph)

      if (null != hyperlinkOptions.value) {
        hyperlink.setValue(hyperlinkOptions.value)
      }

      if (null != hyperlinkOptions.toolTip) {
        hyperlink.setToolTip(hyperlinkOptions.toolTip)
      }

      const run = ParaRun.new(paragraph, textPr)

      run.setUnderline(true)

      run.insertText(hyperlinkOptions.text ?? '')

      hyperlink.addItemAt(0, run, false)

      const newElement = this.children
        .at(index)
        ?.splitAtContentPos(elementPosition, 1)

      if (null != newElement) this.addItemAt(index + 1, newElement)

      this.addItemAt(index + 1, hyperlink)

      this.cursorPosition.elementIndex = index + 1
      moveCursorToEndPosForParaItem(hyperlink, false)
    }

    this.correctChildren()
  }

  updateHyperlink(hyperlinkOptions: HyperlinkOptions) {
    let hyperPosIndex = -1

    let item: Nullable<ParagraphItem>
    if (true === this.selection.isUse) {
      let statIndex = this.selection.startIndex
      let endIndex = this.selection.endIndex
      if (statIndex > endIndex) {
        statIndex = this.selection.endIndex
        endIndex = this.selection.startIndex
      }

      for (let i = statIndex; i <= endIndex; i++) {
        item = this.children.at(i)

        if (
          item &&
          true !== item.hasNoSelection() &&
          item.instanceType !== InstanceType.ParaHyperlink
        ) {
          break
        } else if (
          item &&
          true !== item.hasNoSelection() &&
          item.instanceType === InstanceType.ParaHyperlink
        ) {
          if (-1 === hyperPosIndex) hyperPosIndex = i
          else break
        }
      }

      if (this.selection.startIndex === this.selection.endIndex) {
        item = this.children.at(this.selection.startIndex)
        if (item && InstanceType.ParaHyperlink === item.instanceType) {
          hyperPosIndex = this.selection.startIndex
        }
      }
    } else {
      item = this.children.at(this.cursorPosition.elementIndex)
      if (item && InstanceType.ParaHyperlink === item.instanceType) {
        hyperPosIndex = this.cursorPosition.elementIndex
      }
    }

    if (-1 !== hyperPosIndex) {
      const hyperlink = this.children.at(hyperPosIndex) as ParaHyperlink

      if (null != hyperlinkOptions.value) {
        hyperlink.setValue(hyperlinkOptions.value)
      }

      if (
        null != hyperlinkOptions.toolTip &&
        null != hyperlinkOptions.toolTip
      ) {
        hyperlink.setToolTip(hyperlinkOptions.toolTip)
      }

      if (null != hyperlinkOptions.text) {
        const textPr = hyperlink.textPrAt()

        hyperlink.removeContent(0, hyperlink.children.length)

        const hyperRun = ParaRun.new(this, textPr)

        hyperRun.setFillEffects(
          FillEffects.SolidScheme(ThemeColor.Hyperlink, 0)
        )
        hyperRun.setUnderline(true)
        hyperRun.insertText(hyperlinkOptions.text)

        hyperlink.addItemAt(0, hyperRun, false)

        if (true === this.selection.isUse) {
          this.selection.startIndex = hyperPosIndex
          this.selection.endIndex = hyperPosIndex

          hyperlink.selectAll()
        } else {
          this.cursorPosition.elementIndex = hyperPosIndex
          moveCursorToEndPosForParaItem(hyperlink, false)
        }

        return true
      }

      return false
    }

    return false
  }
  removeHyperlink() {
    // First, find the hyperlink you want to remove
    let hyperlinkIndex = -1
    const childrenCount = this.children.length
    if (childrenCount <= 0) return false

    if (true === this.selection.isUse) {
      let startIndex = this.selection.startIndex
      let endIndex = this.selection.endIndex
      if (startIndex > endIndex) {
        startIndex = this.selection.endIndex
        endIndex = this.selection.startIndex
      }

      startIndex = Math.max(0, startIndex)
      endIndex = Math.min(endIndex, childrenCount - 1)

      for (let i = startIndex; i <= endIndex; i++) {
        const elem = this.children.at(i)!

        if (
          true !== elem.hasNoSelection() &&
          InstanceType.ParaHyperlink !== elem.instanceType
        ) {
          break
        } else if (
          true !== elem.hasNoSelection() &&
          InstanceType.ParaHyperlink === elem.instanceType
        ) {
          if (-1 === hyperlinkIndex) hyperlinkIndex = i
          else break
        }
      }
      if (this.selection.startIndex === this.selection.endIndex) {
        const item = this.children.at(this.selection.startIndex)
        if (item && InstanceType.ParaHyperlink === item.instanceType) {
          hyperlinkIndex = this.selection.startIndex
        }
      }
    } else {
      if (
        InstanceType.ParaHyperlink ===
        this.children.at(this.cursorPosition.elementIndex)?.instanceType
      ) {
        hyperlinkIndex = this.cursorPosition.elementIndex
      }
    }

    if (-1 !== hyperlinkIndex) {
      const hyperlink = this.removeChildAt(hyperlinkIndex)[0] as ParaHyperlink

      const hyperlinkStartIndex = hyperlink.state.selection.startIndex
      const hyperlinkEndIndex = hyperlink.state.selection.endIndex
      const hyperlinkElementIndex = hyperlink.state.elementIndex

      const runs = hyperlink.removeContent(0, hyperlink.children.length)
      ParaHyperlink.recycle(hyperlink)

      const textPr = TextPr.new()
      textPr.underline = undefined
      textPr.fillEffects = undefined

      for (let i = 0, l = runs.length; i < l; i++) {
        const elem = runs[i]
        this.addItemAt(hyperlinkIndex + i, elem)
        applyTextPrToParaItem(elem, textPr, undefined, true)
      }
      if (true === this.selection.isUse) {
        this.selection.startIndex = hyperlinkIndex + hyperlinkStartIndex
        this.selection.endIndex = hyperlinkIndex + hyperlinkEndIndex
      } else {
        this.cursorPosition.elementIndex =
          hyperlinkIndex + hyperlinkElementIndex
      }

      return true
    }

    return false
  }

  canInsertHyperlink(isCheckInHyperlink?: boolean) {
    if (true === isCheckInHyperlink) {
      if (true === this.selection.isUse) {
        // If the beginning or end of a hyperlink, or the end of a paragraph,
        // or the entire selection is inside a hyperlink, then we cannot add a new one.
        // In all other cases, let us add.
        // Also, if the beginning or end of the selection falls into
        //an element that cannot be split, then we also prohibit adding a hyperlink.

        let startIndex = this.selection.startIndex
        let endIndex = this.selection.endIndex
        if (endIndex < startIndex) {
          startIndex = this.selection.endIndex
          endIndex = this.selection.startIndex
        }

        for (let i = startIndex; i <= endIndex; i++) {
          const item = this.children.at(i)
          if (item && InstanceType.ParaHyperlink === item.instanceType) {
            return false
          }
        }

        return true
      } else {
        const instanceType = this.children.at(
          this.cursorPosition.elementIndex
        )?.instanceType
        if (InstanceType.ParaHyperlink === instanceType) return false
        else return true
      }
    } else {
      if (true === this.selection.isUse) {
        // If we have several hyperlinks or the end of a paragraph in the selection,
        // then we return false, in all other cases true.
        // Also, if the beginning or end of the selection falls into an element that
        // cannot be split, then we also prohibit adding a hyperlink.

        let startIndex = this.selection.startIndex
        let endIndex = this.selection.endIndex
        if (endIndex < startIndex) {
          startIndex = this.selection.endIndex
          endIndex = this.selection.startIndex
        }

        return true
      } else {
        return true
      }
    }
  }

  //#region para selection
  removeSelection() {
    if (true === this.selection.isUse) {
      let startIndex = this.selection.startIndex
      let endIndex = this.selection.endIndex

      if (startIndex > endIndex) {
        startIndex = this.selection.endIndex
        endIndex = this.selection.startIndex
      }

      startIndex = Math.max(0, startIndex)
      endIndex = Math.min(this.children.length - 1, endIndex)

      for (let i = startIndex; i <= endIndex; i++) {
        this.children.at(i)!.removeSelection()
      }
    }

    this.selection.isUse = false
    this.selection.start = false
    this.selection.startIndex = 0
    this.selection.endIndex = 0
  }

  isParaEndSelected() {
    if (true !== this.selection.isUse) return false

    const endIndex =
      this.selection.startIndex > this.selection.endIndex
        ? this.selection.startIndex
        : this.selection.endIndex

    const endItem = this.children.at(endIndex)
    return endItem && endItem.instanceType === InstanceType.ParaRun
      ? endItem.isParaEndSelected()
      : false
  }

  isContentPosSelected(elementPosition: ParagraphElementPosition) {
    const indexOfPosition = elementPosition.get(0)

    let item: Nullable<ParagraphItem>
    if (
      this.selection.startIndex <= indexOfPosition &&
      indexOfPosition <= this.selection.endIndex
    ) {
      item = this.children.at(indexOfPosition)
      return item
        ? item.isParaContentPosSelected(
            elementPosition,
            1,
            indexOfPosition === this.selection.endIndex
          )
        : false
    } else if (
      this.selection.endIndex <= indexOfPosition &&
      indexOfPosition <= this.selection.startIndex
    ) {
      item = this.children.at(indexOfPosition)
      return item
        ? item.isParaContentPosSelected(
            elementPosition,
            1,
            indexOfPosition === this.selection.startIndex
          )
        : false
    }

    return false
  }

  selectAll(dir?: TextOperationDirection) {
    this.selection.isUse = true

    let startPosition: ParagraphElementPosition,
      endPosition: ParagraphElementPosition
    if (-1 === dir) {
      startPosition = getEndPosOfParagraph(this, true)
      endPosition = getStartPosOfParagraph(this)
    } else {
      startPosition = getStartPosOfParagraph(this)
      endPosition = getEndPosOfParagraph(this, true)
    }

    this.selection.startBySet = false
    this.selection.endBySet = false

    updateSelectionStateForParagraph(this, startPosition, endPosition)
  }

  hasNoSelection(isCheckEnd = true) {
    if (true === this.selection.isUse) {
      let startIndex = this.selection.startIndex
      let endIndex = this.selection.endIndex

      if (startIndex > endIndex) {
        endIndex = this.selection.startIndex
        startIndex = this.selection.endIndex
      }

      for (let i = startIndex; i <= endIndex; i++) {
        const item = this.children.at(i)
        if (item && true !== item.hasNoSelection(isCheckEnd)) {
          return false
        }
      }
    }

    return true
  }

  isSelectFromStart() {
    if (true === this.hasTextSelection()) {
      let startPosition = this.currentElementPosition(true, true)
      const endPosition = this.currentElementPosition(true, false)

      if (startPosition.compare(endPosition) > 0) startPosition = endPosition

      if (true !== isCursorAtBeginInParagraph(this, startPosition)) return false

      return true
    }

    return false
  }
  startSelectFromCurrentPosition() {
    const elementPosition = this.currentElementPosition(false, false)

    this.selection.isUse = true
    this.selection.start = false
    this.selection.startBySet = true
    this.selection.endBySet = true
    updateSelectionStateForParagraph(this, elementPosition, elementPosition)
  }

  //#endregion

  isEmpty(props?: CheckEmptyRunOptions) {
    const pr: CheckEmptyRunOptions = { ignoreEnd: true }

    if (undefined !== props) {
      if (props.ignoreEnd != null) pr.ignoreEnd = props.ignoreEnd
      if (props.ignoreNewLine === true) pr.ignoreNewLine = true
    }

    const l = this.children.length
    for (let i = 0; i < l; i++) {
      if (false === this.children.at(i)!.isEmpty(pr)) return false
    }

    return true
  }

  isHitInText(x: number, y: number, colIndex: number) {
    if (colIndex < 0 || colIndex >= this.renderingState.columns.length) {
      return null
    }
    const searchInfo = getSearchPositionInfoFromParagraph(
      this,
      x,
      y,
      colIndex,
      false,
      false
    )
    if (true === searchInfo.hitTextContent) return this

    return null
  }
  isInPPT() {
    return this.parent ? this.parent.isInPPT(this.getId()) : false
  }
  getTextPrForFirstRun() {
    for (let i = 0, l = this.children.length; i < l; ++i) {
      const item = this.children.at(i)!
      if (!item.isEmpty()) {
        if (item.instanceType === InstanceType.ParaRun) {
          return item.getCalcedPr()
        } else if (item.instanceType === InstanceType.ParaHyperlink) {
          let hyperlinkPr: TextPr | undefined
          for (let i = 0, l = item.children.length; i < l; ++i) {
            const run = item.children.at(i)!
            if (run && !run.isEmpty()) {
              hyperlinkPr = run.getCalcedPr()
              break
            }
          }

          if (hyperlinkPr) {
            return hyperlinkPr
          }
        }
      }
    }
    return this.getCalcedPrs(false)!.textPr
  }

  setNumbering(newBullet: Bullet) {
    const orgBullet = this.pr.bullet
    this.pr.bullet = undefined
    this.calcedPrs.isDirty = true

    if (!newBullet) {
      newBullet = new Bullet()
      newBullet.bulletType = new BulletType()
      newBullet.bulletType.type = BulletTypeValue.NONE
    }
    const theme = this.getTheme()
    const colorMap = this.getColorMap()
    const paraPr = this.getCalcedPrs(false)!.paraPr
    const newBulletInfo = getBulletInfo(newBullet, theme, colorMap)
    const newType = newBulletInfo.type
    const currentBulletInfo = paraPr.bullet
      ? getBulletInfo(paraPr.bullet, theme, colorMap)
      : null
    const currentType = paraPr.bullet
      ? currentBulletInfo!.type
      : NumberingFormat.None
    let leftInd: number
    if (newType === currentType) {
      if (newType === NumberingFormat.Char) {
        if (currentBulletInfo?.char === newBulletInfo.char) {
          this.pr.bullet = orgBullet
          this.setBullet(undefined)
        } else {
          this.pr.bullet = orgBullet
          this.setBullet(newBullet.clone())
        }
      } else {
        this.pr.bullet = orgBullet
        this.setBullet(undefined)
      }
      this.setIndent({ left: undefined, firstLine: undefined }, true)
    } else {
      this.pr.bullet = orgBullet
      this.setBullet(newBullet.clone())
      leftInd = Math.min(
        paraPr.ind.left!,
        paraPr.ind.left! + paraPr.ind.getCalcFirstLine()!
      )
      const firstRunPr = this.getTextPrForFirstRun()
      const indent = firstRunPr.fontSize! * 0.305954545 + 2.378363636
      if (newType === NumberingFormat.None) {
        this.setIndent({ firstLine: 0, left: leftInd }, false)
      } else {
        this.setIndent({ left: leftInd + indent, firstLine: -indent }, false)
      }
    }
  }
  getNumbering() {
    this.getCalcedPrs(false)
    return this.paraNumbering
  }
  private removeNumbering() {
    const bullet = new Bullet()
    bullet.bulletType = new BulletType()
    bullet.bulletType.type = BulletTypeValue.NONE
    this.setNumbering(bullet)
  }
  private setPrLevel(lvl: number | undefined) {
    if (this.pr.lvl !== lvl) {
      informDocChange(this)
      this.pr.lvl = lvl
      this.invalidateCalcedPrs()
      this.invalidateAllItemsCalcedPrs()
      this.hasCalculated = false
    }
  }
  getFirstRunTextPr() {
    let textPr: Nullable<TextPr>
    if (this.children.length > 0) {
      if (true === this.isSetToAll) {
        textPr = this.children.at(0)?.getCalcedTextPr(true)
      } else {
        if (true === this.selection.isUse) {
          let startIndex = this.selection.startIndex
          let endIndex = this.selection.endIndex

          if (startIndex > endIndex) {
            startIndex = this.selection.endIndex
            endIndex = this.selection.startIndex
          }

          startIndex = Math.max(0, startIndex)
          endIndex = Math.min(this.children.length - 1, endIndex)

          if (
            startIndex === endIndex &&
            this.children.length - 1 === endIndex
          ) {
            textPr = this.getCalcedPrs(false)!.textPr.clone()
            textPr.merge(this.paraTextPr.textPr)
          } else {
            let isCheckParaEnd = false
            if (
              this.children.length - 1 === endIndex &&
              true !== this.children.at(endIndex)!.hasNoSelection(true)
            ) {
              endIndex--
              isCheckParaEnd = true
            }

            while (
              startIndex < endIndex &&
              true === this.children.at(startIndex)!.hasNoSelection()
            ) {
              startIndex++
            }

            if (
              isCheckParaEnd &&
              startIndex === endIndex &&
              this.children.at(startIndex)!.hasNoSelection()
            ) {
              textPr = null
            } else textPr = this.children.at(startIndex)!.getCalcedTextPr(true)

            if (null == textPr) {
              textPr = this.getCalcedPrs(false)!.textPr.clone()
              textPr.merge(this.paraTextPr.textPr)
            }
          }
        } else {
          textPr = this.children
            .at(this.cursorPosition.elementIndex)
            ?.getCalcedTextPr(true)
        }
      }
    }

    if (textPr == null) {
      textPr = this.paraTextPr.textPr.clone()
    }

    return textPr
  }
  getCalcedTextPr() {
    let textPr: Nullable<TextPr>
    if (this.children.length > 0) {
      if (true === this.isSetToAll) {
        this.selectAll(1)

        const startIndex = 0
        let count = this.children.length

        textPr = this.children.at(startIndex)?.getCalcedTextPr(true)
        count = this.children.length

        for (let i = startIndex + 1; i < count; i++) {
          const item = this.children.at(i)
          if (item == null) continue
          const childTextPr = item.getCalcedTextPr(false)
          if (textPr == null) textPr = childTextPr.clone()
          else if (true !== item.hasNoSelection()) {
            textPr = textPr.subDiff(childTextPr)
          }
        }

        this.removeSelection()
      } else {
        if (true === this.selection.isUse) {
          let startIndex = this.selection.startIndex
          let endIndex = this.selection.endIndex

          if (startIndex > endIndex) {
            startIndex = this.selection.endIndex
            endIndex = this.selection.startIndex
          }

          startIndex = Math.max(0, startIndex)
          endIndex = Math.min(this.children.length - 1, endIndex)

          if (
            startIndex === endIndex &&
            this.children.length - 1 === endIndex
          ) {
            textPr = this.getCalcedPrs(false)!.textPr.clone()
            textPr.merge(this.paraTextPr.textPr)
          } else {
            let isCheckParaEnd = false
            if (
              this.children.length - 1 === endIndex &&
              true !== this.children.at(endIndex)!.hasNoSelection(true)
            ) {
              endIndex--
              isCheckParaEnd = true
            }

            while (
              startIndex < endIndex &&
              true === this.children.at(startIndex)!.hasNoSelection()
            ) {
              startIndex++
            }

            if (
              isCheckParaEnd &&
              startIndex === endIndex &&
              this.children.at(startIndex)!.hasNoSelection()
            ) {
              textPr = null
            } else textPr = this.children.at(startIndex)!.getCalcedTextPr(true)

            if (null == textPr) {
              textPr = this.getCalcedPrs(false)!.textPr.clone()
              textPr.merge(this.paraTextPr.textPr)
            }

            for (let i = startIndex + 1; i <= endIndex; i++) {
              const item = this.children.at(i)
              if (item == null) continue
              const tTextPr = item.getCalcedTextPr(false)

              if (textPr == null) textPr = tTextPr.clone()
              else if (true !== item.hasNoSelection()) {
                textPr = textPr.subDiff(tTextPr)
              }
            }

            if (true === isCheckParaEnd) {
              const endTextPr = this.getCalcedPrs(false)!.textPr.clone()
              endTextPr.merge(this.paraTextPr.textPr)
              textPr = textPr.subDiff(endTextPr)
            }
          }
        } else {
          textPr = this.children
            .at(this.cursorPosition.elementIndex)
            ?.getCalcedTextPr(true)
        }
      }
    }

    if (textPr == null) {
      textPr = this.paraTextPr.textPr.clone()
    }

    // FIXME
    if (undefined !== textPr.rFonts && null !== textPr.rFonts) {
      textPr.fontFamily = textPr.rFonts.ascii
    }

    return textPr
  }
  getCalcedParaPr() {
    const paraPr = this.getCalcedPrs(false)!.paraPr
    // paraPr.locked = this.Lock.isLocked()
    return paraPr
  }

  getSpacingAdjustedPrs() {
    const pr = this.getCalcedPrs()!

    const prevElement = this.getPrevPara()
    const nextElement = this.getNextPara()

    if (null != prevElement) {
      const prevPr = prevElement.getCalcedPrs(false)!.paraPr
      let spcAfterOfPrev = prevPr.spacing.after!
      const spcAfterAutoOfPrev = prevPr.spacing.afterAuto!
      let spcBefore = pr.paraPr.spacing.before!
      const spcBeforeAuto = pr.paraPr.spacing.beforeAuto!

      spcBefore = calculateAutoSpacing(spcBefore, spcBeforeAuto)
      spcAfterOfPrev = calculateAutoSpacing(spcAfterOfPrev, spcAfterAutoOfPrev)

      pr.paraPr.spacing.before =
        Math.max(spcAfterOfPrev, spcBefore) - spcAfterOfPrev
    } else if (null == prevElement) {
      pr.paraPr.spacing.before = 0
    }

    if (null != nextElement) {
      const curAfter = pr.paraPr.spacing.after!
      const curAfterAuto = pr.paraPr.spacing.afterAuto!

      pr.paraPr.spacing.after = calculateAutoSpacing(curAfter, curAfterAuto)
    } else {
      if (
        true === this.isInTableCell() &&
        true === pr.paraPr.spacing.afterAuto
      ) {
        pr.paraPr.spacing.after = 0
      } else {
        pr.paraPr.spacing.after = 0
      }
    }

    return pr
  }

  invalidateCalcedPrs(bubbleUp = true) {
    this.calcedPrs.isDirty = true
    if (bubbleUp) {
      updateCalcStatusForTextContentItem(this)
    }
  }

  invalidateAllItemsCalcedPrs(bubbleUp = true) {
    const count = this.children.length
    for (let i = 0; i < count; i++) {
      const element = this.children.at(i)!
      invalidateItemCalcedPrs(element, bubbleUp)
    }
  }

  getCalcedPrs(isCopy?: boolean) {
    this._calcParaPr()
    return isCopy
      ? {
          textPr: this.calcedPrs.pr!.textPr.clone(),
          paraPr: this.calcedPrs.pr!.paraPr.clone(),
        }
      : this.calcedPrs.pr
  }

  _calcParaPr() {
    if (true === this.calcedPrs.isDirty) {
      if (null != this.parent) {
        recylceTextContentStylePrs(this.calcedPrs.pr)
        this.calcedPrs.pr = this.getTextStylePr()
        this.paraNumbering.updateBulletInfo(
          getBulletInfo(
            this.calcedPrs.pr!.paraPr.bullet!,
            this.getTheme(),
            this.getColorMap()
          )
        )

        this.calcedPrs.isDirty = false
      } else {
        if (null == this.calcedPrs.pr) {
          this.calcedPrs.pr = defaultContentStylePrs()

          this.calcedPrs.pr.paraPr.tabs = new ParaTablist()
        }

        this.calcedPrs.isDirty = true
      }
    }
  }
  /**
   * We form the final properties of the paragraph based on the style, possible numbering and direct settings.
   */

  getTextStylePr(lvl?: number, isNoMergeDefault?: boolean) {
    let pr: TextContentStylePrs
    if (this.parent) {
      const _Lvl = isNumber(lvl) ? lvl : isNumber(this.pr.lvl) ? this.pr.lvl : 0
      // Reading properties for the current style
      const styleInfo = this.parent.getTextStyles(_Lvl)
      pr = styleInfo.style
    } else {
      pr = {
        textPr: TextPr.new(),
        paraPr: ParaPr.new(),
      }
    }
    const tableStyle = this.parent?.getTableStyleForParagraph()
    if (tableStyle && tableStyle.textPr) {
      pr.textPr.merge(tableStyle.textPr)
    }

    pr.paraPr.tabs =
      null != pr.paraPr.tabs ? pr.paraPr.tabs.clone() : new ParaTablist()

    if (isNoMergeDefault !== true) {
      // copy the direct settings of the paragraph.
      pr.paraPr.merge(this.pr)
      if (this.pr.defaultRunPr) pr.textPr.merge(this.pr.defaultRunPr)
    }
    updateTextPrFonts(pr.textPr, this.getTheme()!)
    return pr
  }

  pasteFormatting(textPr: Nullable<TextPrOptions>, paraPr: Nullable<ParaPr>) {
    if (textPr) {
      this.applyTextPr(TextPr.nulledOptions(textPr, true))
    }

    if (paraPr && this.isSelectedAll()) {
      if (paraPr.ind) this.setIndent(paraPr.ind, true)
      else this.setIndent(new ParaInd(), true)

      this.setAlign(paraPr.jcAlign)

      if (paraPr.rtl != null && paraPr.rtl !== this.pr.rtl) {
        this.pr.rtl = paraPr.rtl
        this.invalidateCalcedPrs()
      }

      if (paraPr.spacing) this.setSpacing(paraPr.spacing, true)
      else this.setSpacing(new ParaSpacing(), true)

      if (paraPr.tabs) this.setTabs(paraPr.tabs.clone())
      else this.setTabs(new ParaTablist())

      this.setBullet(paraPr.bullet)
    }
  }

  private _clearFormatting() {
    this.setIndent(new ParaInd(), true)
    this.setAlign(undefined)
    this.setSpacing(new ParaSpacing(), true)
    this.setTabs(new ParaTablist())
    this.setBullet(undefined)

    this.calcedPrs.isDirty = true
  }

  setIndent(ind: ParaIndOptions, isDeleteUndefined: boolean = false) {
    if (undefined === this.pr.ind) this.pr.ind = new ParaInd()

    if (
      (null != ind.firstLine || true === isDeleteUndefined) &&
      this.pr.ind.firstLine !== ind.firstLine
    ) {
      this.pr.ind.firstLine = ind.firstLine
    }

    if (
      (null != ind.left || true === isDeleteUndefined) &&
      this.pr.ind.left !== ind.left
    ) {
      this.pr.ind.left = ind.left
    }

    if (
      (null != ind.right || true === isDeleteUndefined) &&
      this.pr.ind.right !== ind.right
    ) {
      informDocChange(this)
      this.pr.ind.right = ind.right
    }

    this.invalidateCalcedPrs()
  }
  setSpacing(spacing: ParaSpacingOptions, isDeleteUndefined?: boolean) {
    if (undefined === this.pr.spacing) this.pr.spacing = new ParaSpacing()

    if (
      (null != spacing.line || true === isDeleteUndefined) &&
      this.pr.spacing.line !== spacing.line
    ) {
      let spcOfLine = spacing.line

      if (undefined !== spacing.line) {
        if (
          (undefined !== spacing.lineRule &&
            spacing.lineRule !== LineHeightRule.Auto) ||
          (undefined === spacing.lineRule &&
            (LineHeightRule.Exact === this.pr.spacing.lineRule ||
              LineHeightRule.AtLeast === this.pr.spacing.lineRule))
        ) {
          spcOfLine = normalizeMMToTwips(
            ((((spacing.line / 25.4) * 72 * 20) | 0) * 25.4) / 20 / 72
          )
        }
      }

      informDocChange(this)
      this.pr.spacing.line = spcOfLine
    }

    if (
      (null != spacing.lineRule || true === isDeleteUndefined) &&
      this.pr.spacing.lineRule !== spacing.lineRule
    ) {
      informDocChange(this)
      this.pr.spacing.lineRule = spacing.lineRule
    }

    if (
      (null != spacing.before || true === isDeleteUndefined) &&
      this.pr.spacing.before !== spacing.before
    ) {
      informDocChange(this)
      this.pr.spacing.before = spacing.before
    }

    if (
      (null != spacing.after || true === isDeleteUndefined) &&
      this.pr.spacing.after !== spacing.after
    ) {
      informDocChange(this)
      this.pr.spacing.after = spacing.after
    }

    if (
      (null != spacing.afterAuto || true === isDeleteUndefined) &&
      this.pr.spacing.afterAuto !== spacing.afterAuto
    ) {
      informDocChange(this)
      this.pr.spacing.afterAuto = spacing.afterAuto
    }

    if (
      (null != spacing.beforeAuto || true === isDeleteUndefined) &&
      this.pr.spacing.beforeAuto !== spacing.beforeAuto
    ) {
      informDocChange(this)
      this.pr.spacing.beforeAuto = spacing.beforeAuto
    }

    this.invalidateCalcedPrs()
  }
  setAlign(align: JCAlignTypeValues | undefined) {
    if (this.pr.jcAlign !== align) {
      informDocChange(this)
      this.pr.jcAlign = align

      this.invalidateCalcedPrs()
    }
  }

  setDefaultTab(tabSize: number | undefined) {
    if (this.pr.defaultTab !== tabSize) {
      informDocChange(this)
      this.pr.defaultTab = tabSize

      this.invalidateCalcedPrs()
    }
  }

  setTabs(tabs: ParaTablist) {
    informDocChange(this)
    this.pr.tabs = tabs

    this.invalidateCalcedPrs()
  }

  setBullet(bullet: Bullet | undefined) {
    informDocChange(this)
    this.pr.bullet = bullet
    this.invalidateCalcedPrs()
    this.invalidateAllItemsCalcedPrs()
    this.hasCalculated = false
  }

  getTextTransformOfParent() {
    return this.parent?.getTextTransformOfParent()
  }
  /** 计算 cursorType 统一 return 至最上级处理*/
  getCursorInfoByPos(
    x: number,
    y: number,
    colIndex: number
  ): Nullable<HoverInfo> {
    const columns = this.renderingState.columns
    colIndex = Math.max(0, Math.min(colIndex, columns.length - 1))
    const column = columns.at(colIndex)
    const parentTextTransform = this.getTextTransformOfParent()
    const mouseMoveData = new MouseMoveData()
    const coords = hookManager.get(Hooks.EditorUI.ConvertCoordsToCursorPos, {
      x: x,
      y: y,
      transform: parentTextTransform,
    })

    if (coords) {
      mouseMoveData.absX = coords.x
      mouseMoveData.absY = coords.y
    }

    const hyperlink = this.getHyperlinkByXY(x, y, colIndex)
    if (
      column &&
      hyperlink &&
      y <= column.bounds.bottom &&
      y >= column.bounds.top
    ) {
      mouseMoveData.type = MouseMoveDataTypes.Hyperlink
      mouseMoveData.hyperlink = {
        text: null,
        value: hyperlink.value,
        toolTip: hyperlink.toolTip,
      }

      const textGetter = getParagraphTextGetter()
      textGetter.collectFromParaItem(hyperlink)
      mouseMoveData.hyperlink.text = textGetter.getText()
      mouseMoveData.hyperlink.toolTip = hyperlink.getToolTip()
    } else {
      mouseMoveData.type = MouseMoveDataTypes.Common
    }
    const canOpenHyperlink = hyperlink && true === GlobalEvents.keyboard.ctrlKey
    const type = canOpenHyperlink
      ? cursorExtType.Pointer
      : this.parent && isEavertTextContent(this.parent.parent)
        ? cursorExtType.VerticalText
        : cursorExtType.Text
    return {
      objectId: this.getId(),
      cursorType: type,
      mouseData: mouseMoveData,
    }
  }

  collectAllFontNames(allFonts: Dict<boolean>) {
    this.paraTextPr.textPr.collectAllFontNames(allFonts)
    if (this.pr.bullet) {
      this.pr.bullet.collectAllFontNames(allFonts)
    }
    if (this.pr.defaultRunPr) {
      this.pr.defaultRunPr.collectAllFontNames(allFonts)
    }

    const l = this.children.length
    for (let i = 0; i < l; i++) {
      this.children.at(i)!.collectAllFontNames(allFonts)
    }
  }

  syncEditorUIState() {
    if (true === this.selection.isUse) {
      let startIndex = this.selection.startIndex
      let endIndex = this.selection.endIndex
      if (startIndex > endIndex) {
        startIndex = this.selection.endIndex
        endIndex = this.selection.startIndex
      }

      for (let i = startIndex; i <= endIndex; i++) {
        const item = this.children.at(i)
        if (
          item &&
          true !== item.hasNoSelection() &&
          item.instanceType === InstanceType.ParaHyperlink
        ) {
          item.syncEditorUIState()
        }
      }
    } else {
      const item = this.children.at(this.cursorPosition.elementIndex)
      if (item && item.instanceType === InstanceType.ParaHyperlink) {
        item.syncEditorUIState()
      }
    }
  }

  beforeDelete() {
    for (let i = 0, l = this.children.length; i < l; i++) {
      const item = this.children.at(i)!
      beforeDeleteParaItem(item)
    }
  }

  selectSelf() {
    if (this.parent) {
      this.parent.updateChildrenIndexing()
      this.parent.selectSelf(this.index)
    }
  }

  isCurrentSelected() {
    const parent = this.parent
    if (parent == null) return false
    parent.updateChildrenIndexing()
    if (
      false === parent.selection.isUse &&
      this.index === parent.cursorPosition.childIndex &&
      parent.children.at(this.index) === this
    ) {
      return parent.isCurrentSelected()
    }

    return false
  }

  splitRestInto(newParagraph: Paragraph) {
    this.removeSelection()
    newParagraph.removeSelection()

    const elementPosition = this.currentElementPosition(false, false)
    const index = elementPosition.get(0)

    const textPr = this.runTextPrAt(elementPosition)

    let newElement = this.children
      .at(index)
      ?.splitAtContentPos(elementPosition, 1)

    if (null == newElement) {
      newElement = ParaRun.new(newParagraph, textPr)
    }

    const newContent = this.removeContent(
      index + 1,
      this.children.length - index - 1
    )

    const endRun = ParaRun.new(this)
    endRun.addItemAt(0, ParaEnd.new())

    this.addItemAt(this.children.length, endRun)

    newParagraph.children.recycle(
      newParagraph.removeContent(0, newParagraph.children.length)
    )
    newParagraph.appendContent(newContent)
    newParagraph.addItemAt(0, newElement)
    newParagraph.correctChildren()

    this.assignPrTo(newParagraph)
    this.paraTextPr.resetTextPr()
    this.paraTextPr.fromTextPrData(textPr)

    moveCursorToEndPosInParagraph(this, false, false)
    moveCursorToStartPosInParagraph(newParagraph, false)
  }

  appendParagraph(para: Paragraph) {
    this.removeParaEnd()

    const l = para.adjacentPositions.length
    for (let i = 0; i < l; i++) {
      const paraAdjPos = para.adjacentPositions[i]

      paraAdjPos.enities[0] = this
      paraAdjPos.adjacentPos!.paragraph = this
      paraAdjPos.adjacentPos!.pos.positonIndices[0] += this.children.length

      this.adjacentPositions.push(paraAdjPos)
    }

    const newContent = para.removeContent(0, para.children.length)
    this.appendContent(newContent)
    Paragraph.recycle(para)
  }

  applyPrTo(newParagraph: Paragraph) {
    let textPr: TextPr | undefined
    if (this.isEmpty()) {
      textPr = this.runTextPrAt(getEndPosOfParagraph(this, false))
    } else {
      const endPosition = getEndPosOfParagraph(this, false, true)
      const curPosition = this.currentElementPosition(false, false)
      this.updateCursorPosByPosition(endPosition, true, -1)
      textPr = this.runTextPrAt(this.currentElementPosition(false, false))
      this.updateCursorPosByPosition(curPosition, false, -1, false)
    }

    this.assignPrTo(newParagraph)

    this.paraTextPr.resetTextPr()
    this.paraTextPr.fromTextPrData(textPr)

    newParagraph.addItemAt(0, ParaRun.new(newParagraph))
    newParagraph.correctChildren()
    moveCursorToStartPosInParagraph(newParagraph, false)

    for (let i = 0, l = newParagraph.children.length; i < l; i++) {
      const item = newParagraph.children.at(i)!
      if (InstanceType.ParaRun === item.instanceType && textPr) {
        item.fromTextPr(textPr)
      }
    }
  }

  //----------------------------------------------------------------------------------------------------------------------

  private _correctCursorPosSegmentLine() {
    if (-1 !== this.cursorPosition.line) return

    const pos = this.currentElementPosition(false, false)

    const segments = this.getSegmentsByPos(pos)

    this.cursorPosition.line = -1

    for (let i = 0, l = segments.length; i < l; i++) {
      const lineIndex = segments[i]

      const paraLineSegment = this.renderingState.getSegment(lineIndex)
      if (paraLineSegment != null && paraLineSegment.w > 0) {
        this.cursorPosition.line = lineIndex
        break
      }
    }
  }

  private getSegmentsByPos(elementPosition: ParagraphElementPosition) {
    const run = this.elementAtElementPos(elementPosition)

    if (null == run || InstanceType.ParaRun !== run.instanceType) return []

    return run.getLineSegmentsContains(
      elementPosition.get(elementPosition.level - 1)
    )
  }

  elementAtElementPos(elementPosition: ParagraphElementPosition) {
    if (elementPosition.level < 1) return this

    const index = elementPosition.get(0)
    const item = this.children.at(index)
    if (item == null) return null

    if (item.instanceType === InstanceType.ParaRun) {
      return item
    } else {
      if (1 >= elementPosition.level) return item

      const runPos = elementPosition.get(1)
      const run = item.children.at(runPos)
      if (run == null) return null

      return run
    }
  }

  isSelectedAll() {
    const isStart = this.isSelectFromStart()
    const isEnd = this.isParaEndSelected()

    return (true === isStart && true === isEnd) || true === this.isSetToAll
      ? true
      : false
  }

  getColumnsCount() {
    return this.renderingState.columns.length
  }

  updateParaPropsForVerticalText(isTextVertical: boolean) {
    if (true === isTextVertical) {
      const left =
        undefined === this.pr.ind.left || this.pr.ind.left < 0.001
          ? 2
          : undefined
      const right =
        undefined === this.pr.ind.right || this.pr.ind.right < 0.001
          ? 2
          : undefined

      this.setIndent({ left: left, right: right }, false)
    } else {
      const left =
        undefined !== this.pr.ind.left && Math.abs(this.pr.ind.left - 2) < 0.01
          ? undefined
          : this.pr.ind.left
      const right =
        undefined !== this.pr.ind.right &&
        Math.abs(this.pr.ind.right - 2) < 0.01
          ? undefined
          : this.pr.ind.right
      const firstLine = this.pr.ind.firstLine

      this.setIndent({ left: left, right: right, firstLine: firstLine }, true)
    }
  }

  getText() {
    const textGetter = getParagraphTextGetter({
      breakOnParaEnd: false,
      paraEndWithSpace: true,
    })

    for (let i = 0, l = this.children.length; i < l; ++i) {
      textGetter.collectFromParaItem(this.children.at(i)!)
    }

    return textGetter.getText()
  }

  clearFormatting(isFormatParaPr: boolean, isFormatTextPr: boolean) {
    if (
      isFormatParaPr &&
      (!this.hasTextSelection() ||
        this.isSelectedAll() ||
        !(this.parent && this.parent.isOnlyOneElementSelected()))
    ) {
      this._clearFormatting()
    }

    if (isFormatTextPr) {
      const textPr = TextPr.nulledOptions(
        {
          highLight: -1,
        },
        true
      )
      this.applyTextPr(textPr)
    }
  }
  setParagraphTextDirection(isRTL = false) {
    if (this.pr.rtl !== isRTL) {
      informDocChange(this)
      this.pr.rtl = isRTL
      const { jcAlign } = this.getCalcedParaPr()
      if (jcAlign === undefined || jcAlign === AlignKind.Left) {
        this.pr.jcAlign = AlignKind.Right
      } else if (jcAlign === AlignKind.Right) {
        this.pr.jcAlign = AlignKind.Left
      }
      this.invalidateCalcedPrs()
    }
  }

  isInTableCell() {
    return this.parent ? this.parent.isInTableCell() : false
  }

  getChild(index: number) {
    return this.children.at(index)
  }

  childrenCount() {
    return this.children.length - 1
  }

  getParaEndCalcedPr() {
    const textPr = this.getCalcedPrs(false)!.textPr.clone()

    textPr.merge(this.paraTextPr.textPr)
    return textPr
  }

  getPresentationField(elementPosition?: ParagraphElementPosition) {
    const info = new CurrentFocusElementsInfo()
    getCurrentFocusElementsInfoForParagraph(this, info, elementPosition, 0)

    const presentationField = info.getPresentationField()
    if (presentationField) return presentationField

    return null
  }

  getCurrentAdjacentPosition() {
    const adjPos: AdjacentPosition = {
      paragraph: this,
      pos: this.currentElementPosition(false, false),
    }

    updateClosestPosForParagraph(this, adjPos)
    return adjPos
  }

  getWholeWidth() {
    const colmns = this.renderingState.columns
    let totalWidth = 0
    if (colmns) {
      for (let i = 0, l = colmns.length; i < l; i++) {
        const col = colmns.at(i)!
        const right = Math.max(col.bounds.right, col.renderingXLimit ?? 0)
        const width = right - col.bounds.left
        totalWidth += width
      }
    }
    return totalWidth
  }
}

//helpers
function invalidateItemCalcedPrs(item: ParagraphItem, bubbleUp = true) {
  if (item) {
    if (item.instanceType === InstanceType.ParaRun) {
      item.invalidateCalcedPrs(true, bubbleUp)
    } else {
      const count = item.children.length
      for (let i = 0; i < count; i++) {
        const run = item.children.at(i)
        run && run.invalidateCalcedPrs(true, bubbleUp)
      }
    }
  }
}

function isAtBeginOfNewLine(paraItem: ParagraphItem) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    if (
      LineSegmentIndexesUtil.getLinesCount(paraItem.lineSegmentIndexes) < 2 ||
      0 !==
        LineSegmentIndexesUtil.getStartIndexOfLine(
          paraItem.lineSegmentIndexes,
          1
        )
    ) {
      return false
    }

    return true
  } else {
    if (paraItem.children.length <= 0) return false

    return isAtBeginOfNewLine(paraItem.children.at(0)!)
  }
}

interface ParagraphCursorPosition {
  x: number
  y: number
  elementIndex: number
  line: number
  cursorX: number
  cursorY: number
  colIndex: number
  runElementPos?: number
}

class ParagraphSelection {
  start: boolean
  isUse: boolean
  startIndex: number
  endIndex: number

  startBySet: boolean
  endBySet: boolean
  constructor() {
    this.start = false
    this.isUse = false
    this.startIndex = 0
    this.endIndex = 0

    this.startBySet = true
    this.endBySet = true
  }
  reset() {
    this.start = false
    this.isUse = false
    this.startIndex = 0
    this.endIndex = 0

    this.startBySet = true
    this.endBySet = true
  }
}

export interface ParagraphSelectionDrawInfo {
  startX: number
  startY: number
  width: number
  height: number
  searchingStartX: boolean
}

function calculateAutoSpacing(v: number, isAuto: boolean) {
  let res = v
  if (true === isAuto) res = 14 * Factor_pt_to_mm

  return res
}
