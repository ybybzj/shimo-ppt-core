import {
  ParagraphElementPosition,
  ParagraphElementAdjacentPosition,
  ParaRunState,
  ParaRunStateSelection,
  getParagraphTextGetter,
} from '../common'
import { idGenerator } from '../../../globals/IdGenerator'
import { InstanceType } from '../../instanceTypes'
import { hookManager, Hooks } from '../../hooks'
import { CheckEmptyRunOptions, ParaRun } from './ParaRun'
import { Paragraph } from '../Paragraph'
import { TextPr } from '../../textAttributes/TextPr'

import {
  LineSegmentIndexes,
  LineSegmentIndexesUtil,
} from './LineSegmentIndexes'
import { informDocChange } from '../../calculation/informChanges/textContent'
import {
  HyperlinkProps,
  HyperlinkPropsOptions,
} from '../../SlideElement/attrs/HyperlinkProps'
import { ParagraphItem } from '../type'

import {
  updateEndPosForParaItem,
  updateStartPosForParaItem,
} from '../utils/position'
import {
  moveCursorToEndPosForParaItem,
  moveCursorToStartPosForParaItem,
} from '../../utilities/cursor/moveToEdgePos'
import { updateSelectionStateForParagraph } from '../utils/selection'
import {
  isCursorAtEndInParaItem,
  isCursorAtStartInParaItem,
} from './utils/cursor'
import { SearchResultOfParagraph } from '../../search/searchStates'
import { Dict, Nullable } from '../../../../liber/pervasive'
import { TextOperationDirection } from '../../utilities/type'
import { updateCalcStatusForTextContentItem } from '../../calculation/updateCalcStatus/textContent'
import { HyperlinkOptions } from '../../properties/options'
import { RecyclableArray } from '../../../common/managedArray'
import { PoolAllocator, Recyclable } from '../../../common/allocator'
import { RunContentElement } from '../RunElement/type'
import { applyTextPrToParaRun } from './utils/pr'
import { updateParaContentPosition } from './utils/position'

export class ParaHyperlink extends Recyclable<ParaHyperlink> {
  readonly instanceType = InstanceType.ParaHyperlink
  // base properties
  paragraph: Paragraph
  startParaLine: number
  lineSegmentIndexes: LineSegmentIndexes

  state: ParaRunState
  selection: ParaRunStateSelection
  adjacentPositions: ParagraphElementAdjacentPosition[]
  searchResults: SearchResultOfParagraph[]
  cursorIndex: Nullable<number>

  id: string
  value: string
  visited: boolean
  toolTip: string
  children: RecyclableArray<ParaRun>
  private static allocator: PoolAllocator<ParaHyperlink, [Paragraph]> =
    PoolAllocator.getAllocator<ParaHyperlink, [Paragraph]>(ParaHyperlink)

  static new(paragraph: Paragraph) {
    return ParaHyperlink.allocator.allocate(paragraph)
  }
  static recycle(hyperlink: ParaHyperlink) {
    ParaHyperlink.allocator.recycle(hyperlink)
  }
  constructor(paragraph: Paragraph) {
    super()
    this.paragraph = paragraph

    this.lineSegmentIndexes = LineSegmentIndexesUtil.resetOrCreate()

    this.startParaLine = -1

    this.children = new RecyclableArray(ParaRun.recycle)

    this.selection = {
      isUse: false,
      startIndex: 0,
      endIndex: 0,
    }
    this.state = {
      selection: this.selection,
      elementIndex: 0,
    }

    this.adjacentPositions = []
    this.searchResults = []
    this.id = idGenerator.newId()

    this.value = ''
    this.visited = false
    this.toolTip = ''
  }

  reset(paragraph: Paragraph) {
    this.paragraph = paragraph

    LineSegmentIndexesUtil.resetOrCreate(this.lineSegmentIndexes)

    this.startParaLine = -1

    this.children.clear()

    this.selection.isUse = false
    this.selection.startIndex = 0
    this.selection.endIndex = 0

    this.state.elementIndex = 0

    this.adjacentPositions = []
    this.searchResults = []
    this.id = idGenerator.newId()

    this.value = ''
    this.visited = false
    this.toolTip = ''
  }

  // base methods

  getParent() {
    if (!this.paragraph) return null

    const elementPosition = this.paragraph.getContentPositionByElement(this)
    if (!elementPosition || elementPosition.getLevel() < 0) return null

    elementPosition.decreaseLevel(1)
    return this.paragraph.elementAtElementPos(elementPosition)
  }

  isPresentationField() {
    return false
  }

  setParagraph(paragraph: Paragraph) {
    this.paragraph = paragraph

    const l = this.children.length
    for (let i = 0; i < l; i++) {
      this.children.at(i)!.setParagraph(paragraph)
    }
  }

  isEmpty(..._args: any[]) {
    for (let i = 0, l = this.children.length; i < l; i++) {
      if (false === this.children.at(i)!.isEmpty()) return false
    }

    return true
  }

  textPrAt(elementPosition?: ParagraphElementPosition, lvl?: number) {
    if (null == elementPosition) return this.children.at(0)?.getTextPr()
    else if (lvl != null) {
      return this.children.at(elementPosition.get(lvl!))?.getTextPr()
    }
  }

  getFirstContentTextPr() {
    let element: ParaRun | null = null
    if (this.children.length > 0) {
      element = this.children.at(0)!
    }

    if (element != null) {
      return element.getTextPr()
    } else return TextPr.new()
  }

  getCalcedTextPr(isCopy: boolean) {
    let textPr: TextPr | undefined

    if (this.state.selection != null && this.state.selection.isUse) {
      let startIndex = this.state.selection.startIndex
      let endIndex = this.state.selection.endIndex

      if (startIndex > endIndex) {
        startIndex = this.state.selection.endIndex
        endIndex = this.state.selection.startIndex
      }

      textPr = this.children.at(startIndex)?.getCalcedTextPr(isCopy)

      while (null == textPr && startIndex < endIndex) {
        startIndex++
        textPr = this.children.at(startIndex)?.getCalcedTextPr(isCopy)
      }

      for (let i = startIndex + 1; i <= endIndex; i++) {
        const curTextPr = this.children.at(i)?.getCalcedPr(false)

        if (null != curTextPr) textPr!.subDiff(curTextPr)
      }
    } else {
      const elmIndex = this.state.elementIndex

      if (elmIndex >= 0 && elmIndex < this.children.length) {
        textPr = this.children.at(elmIndex)!.getCalcedTextPr(isCopy)
      }
    }

    return textPr ?? TextPr.new()
  }

  remove(dir: TextOperationDirection, isOnAddText?: boolean) {
    const selection = this.state.selection
    const children = this.children
    if (children.length <= 0) return false

    if (true === selection.isUse) {
      let startIndex = selection.startIndex
      let endIndex = selection.endIndex

      if (startIndex > endIndex) {
        startIndex = selection.endIndex
        endIndex = selection.startIndex
      }

      startIndex = Math.max(0, startIndex)
      endIndex = Math.min(endIndex, children.length - 1)

      if (startIndex === endIndex) {
        if (children.at(startIndex)!.isPresentationField()) {
          children.recycle(this.removeContent(startIndex, 1, true))
        } else {
          const startItem = this.children.at(startIndex)!
          startItem.remove(dir)

          if (
            startIndex !== this.children.length - 1 &&
            true === startItem.isEmpty() &&
            true !== isOnAddText
          ) {
            children.recycle(this.removeContent(startIndex, 1, true))
          }
        }
      } else {
        const endItem = this.children.at(endIndex)!
        if (endItem.isPresentationField()) {
          children.recycle(this.removeContent(endIndex, 1, true))
        } else {
          endItem.remove(dir)

          if (
            endIndex !== this.children.length - 1 &&
            true === endItem.isEmpty() &&
            true !== isOnAddText
          ) {
            children.recycle(this.removeContent(endIndex, 1, true))
          }
        }

        for (let i = endIndex - 1; i > startIndex; i--) {
          this.removeContent(i, 1, true)
        }

        const startItem = this.children.at(startIndex)!
        if (startItem.isPresentationField()) {
          this.removeContent(startIndex, 1, true)
        } else {
          startItem.remove(dir)

          if (true === startItem.isEmpty()) {
            this.removeContent(startIndex, 1, true)
          }
        }
      }
      this.removeSelection()

      this.state.elementIndex = startIndex
    } else {
      let elmIndex = this.state.elementIndex
      if (elmIndex < 0 || elmIndex >= this.children.length) return false

      if (
        true === isCursorAtStartInParaItem(this) ||
        true === isCursorAtEndInParaItem(this)
      ) {
        this.selectAll()
        this.selectSelf(1)
      } else {
        let item = this.children.at(elmIndex)!
        while (false === item.remove(dir)) {
          if (dir < 0) elmIndex--
          else elmIndex++

          if (elmIndex < 0 || elmIndex >= this.children.length) break

          item = this.children.at(elmIndex)!

          if (dir < 0) {
            moveCursorToEndPosForParaItem(item, false)
          } else moveCursorToStartPosForParaItem(item)
        }

        if (elmIndex < 0 || elmIndex >= this.children.length) return false
        else {
          if (
            elmIndex !== this.children.length - 1 &&
            true === this.children.at(elmIndex)!.isEmpty() &&
            true !== isOnAddText
          ) {
            children.recycle(this.removeContent(elmIndex, 1, true))
          }

          this.state.elementIndex = elmIndex
        }
      }
    }
    return true
  }

  collectAllFontNames(allFonts: Dict<boolean>) {
    const l = this.children.length
    for (let i = 0; i < l; i++) {
      this.children.at(i)!.collectAllFontNames(allFonts)
    }
  }

  updateStateContentPos(
    elementPosition: ParagraphElementPosition,
    lvl: number
  ) {
    let pos = elementPosition.get(lvl)

    if (pos >= this.children.length) pos = this.children.length - 1

    if (pos < 0) pos = 0

    this.state.elementIndex = pos

    this.children.at(pos)!.updateStateContentPos(elementPosition, lvl + 1)
  }

  updatePosByRun(
    element: ParagraphItem,
    elementPosition: ParagraphElementPosition,
    lvl: number,
    useSegment: boolean,
    line: number
  ) {
    if (this === element) return true

    if (this.children.length <= 0) return false

    let startIndex = 0
    let endIndex = this.children.length - 1
    let curLine: number
    if (true === useSegment) {
      curLine = line - this.startParaLine

      const lineSegmentIndexes = this.lineSegmentIndexes
      if (
        curLine >= 0 &&
        curLine < LineSegmentIndexesUtil.getLinesCount(lineSegmentIndexes)
      ) {
        startIndex = Math.min(
          this.children.length - 1,
          Math.max(
            0,
            LineSegmentIndexesUtil.getStartIndexOfLine(
              lineSegmentIndexes,
              curLine
            )
          )
        )
        endIndex = Math.min(
          this.children.length - 1,
          Math.max(
            0,
            LineSegmentIndexesUtil.getEndIndexOfLine(
              lineSegmentIndexes,
              curLine
            )
          )
        )
      }
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const item = this.children.at(i)!

      elementPosition.updatePosByLevel(i, lvl)

      if (item === element) {
        return true
      }
    }

    return false
  }

  runElementAtPos(elementPosition?: ParagraphElementPosition, lvl?: number) {
    if (null != elementPosition && lvl != null) {
      const pos = elementPosition.get(lvl)

      return this.children.at(pos)?.runElementAtPos(elementPosition, lvl + 1)
    } else {
      const count = this.children.length
      if (count <= 0) return null

      let pos = 0
      let element = this.children.at(pos)!

      while (null == element && pos < count - 1) {
        element = this.children.at(++pos)!
      }

      return element.runElementAtPos()
    }
  }

  removeSelection() {
    const selection = this.selection

    if (true === selection.isUse) {
      let startIndex = selection.startIndex
      let endIndex = selection.endIndex

      if (startIndex > endIndex) {
        startIndex = selection.endIndex
        endIndex = selection.startIndex
      }

      startIndex = Math.max(0, startIndex)
      endIndex = Math.min(this.children.length - 1, endIndex)

      for (let i = startIndex; i <= endIndex; i++) {
        this.children.at(i)!.removeSelection()
      }
    }

    selection.isUse = false
    selection.startIndex = 0
    selection.endIndex = 0
  }

  selectAll(dir?: TextOperationDirection) {
    const contentLen = this.children.length

    const selection = this.selection

    selection.isUse = true

    if (-1 === dir) {
      selection.startIndex = contentLen - 1
      selection.endIndex = 0
    } else {
      selection.startIndex = 0
      selection.endIndex = contentLen - 1
    }

    for (let i = 0; i < contentLen; i++) {
      this.children.at(i)!.selectAll(dir)
    }
  }

  hasNoSelection(isCheckEnd?: boolean) {
    if (this.children.length <= 0) return true

    let startIndex = this.state.selection.startIndex
    let endIndex = this.state.selection.endIndex

    if (startIndex > endIndex) {
      startIndex = this.state.selection.endIndex
      endIndex = this.state.selection.startIndex
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const item = this.children.at(i)
      if (item && false === item.hasNoSelection(isCheckEnd)) {
        return false
      }
    }

    return true
  }

  isParaContentPosSelected(
    elementPosition: ParagraphElementPosition,
    lvl: number,
    isEnd: boolean
  ) {
    const indexOfPosition = elementPosition.get(lvl)

    const item = this.children.at(indexOfPosition)
    if (item == null) return false

    let startIndex = this.state.selection.startIndex
    let endIndex = this.state.selection.endIndex

    if (startIndex > endIndex) {
      startIndex = this.state.selection.endIndex
      endIndex = this.state.selection.startIndex
    }

    if (startIndex <= indexOfPosition && indexOfPosition <= endIndex) {
      return item.isParaContentPosSelected(
        elementPosition,
        lvl + 1,
        isEnd && indexOfPosition === endIndex
      )
    }

    return false
  }

  isSelectedAll(props?: CheckEmptyRunOptions) {
    const selection = this.state.selection

    if (false === selection.isUse && true !== this.isEmpty(props)) return false

    let startIndex = selection.startIndex
    let endIndex = selection.endIndex

    if (endIndex < startIndex) {
      startIndex = selection.endIndex
      endIndex = selection.startIndex
    }

    for (let i = 0; i <= startIndex; i++) {
      const item = this.children.at(i)
      if (item && false === item.isSelectedAll(props)) return false
    }

    const l = this.children.length
    for (let i = endIndex; i < l; i++) {
      if (false === this.children.at(i)!.isSelectedAll(props)) return false
    }

    return true
  }

  hasTextSelection() {
    return this.state.selection.isUse
  }

  isInPPT() {
    return (
      this.paragraph &&
      true === this.paragraph.isInPPT() &&
      true === this.isInParagraph()
    )
  }

  isInParagraph() {
    return !!this.paragraph?.getContentPositionByElement(this)
  }

  private selectSelf(dir: number) {
    if (!this.paragraph) return false
    const elementPosition = this.paragraph.getContentPositionByElement(this)
    if (!elementPosition) return false

    const startPosition = elementPosition.clone()
    const endPosition = elementPosition.clone()

    if (dir > 0) {
      updateStartPosForParaItem(
        this,
        startPosition,
        startPosition.getLevel() + 1
      )
      updateEndPosForParaItem(
        this,
        true,
        endPosition,
        endPosition.getLevel() + 1
      )
    } else {
      updateStartPosForParaItem(this, endPosition, endPosition.getLevel() + 1)
      updateEndPosForParaItem(
        this,
        true,
        startPosition,
        startPosition.getLevel() + 1
      )
    }

    this.paragraph.selection.isUse = true
    this.paragraph.selection.start = false
    this.paragraph.updateCursorPosByPosition(startPosition, true, -1)
    updateSelectionStateForParagraph(
      this.paragraph,
      startPosition,
      endPosition,
      false
    )
    this.paragraph.selectSelf()

    return true
  }

  getChild(index: number) {
    return this.children.at(index)
  }

  childrenCount() {
    return this.children.length
  }

  //ParaHyperlink methods

  toCTHyperlink(): HyperlinkProps {
    const linkOpts: HyperlinkPropsOptions = {}
    let linkStr = this.value
    let action: Nullable<string>
    const tooltip = this.getToolTip()
    if (linkStr === 'ppaction://hlinkshowjump?jump=firstslide') {
      action = linkStr
      linkStr = ''
    } else if (linkStr === 'ppaction://hlinkshowjump?jump=lastslide') {
      action = linkStr
      linkStr = ''
    } else if (linkStr === 'ppaction://hlinkshowjump?jump=nextslide') {
      action = linkStr
      linkStr = ''
    } else if (linkStr === 'ppaction://hlinkshowjump?jump=previousslide') {
      action = linkStr
      linkStr = ''
    } else {
      const prefix = 'ppaction://hlinksldjumpslide'
      const idx = linkStr.indexOf(prefix)
      if (0 === idx) {
        const slideIndex = parseInt(linkStr.substring(prefix.length))
        linkStr = 'slide' + (slideIndex + 1) + '.xml'
        action = 'ppaction://hlinksldjump'
      }
    }
    linkOpts.id = linkStr
    if (action != null) {
      linkOpts.action = action
    }

    if (tooltip != null) {
      linkOpts.tooltip = tooltip
    }

    return HyperlinkProps.new(linkOpts)
  }

  fromCTHyperlink(link: HyperlinkProps) {
    if (link == null) {
      return
    }
    // correct hyperlink
    if (link.action != null && link.action !== '') {
      if (link.action === 'ppaction://hlinkshowjump?jump=firstslide') {
        link.id = 'ppaction://hlinkshowjump?jump=firstslide'
      } else if (link.action === 'ppaction://hlinkshowjump?jump=lastslide') {
        link.id = 'ppaction://hlinkshowjump?jump=lastslide'
      } else if (link.action === 'ppaction://hlinkshowjump?jump=nextslide') {
        link.id = 'ppaction://hlinkshowjump?jump=nextslide'
      } else if (
        link.action === 'ppaction://hlinkshowjump?jump=previousslide'
      ) {
        link.id = 'ppaction://hlinkshowjump?jump=previousslide'
      } else if (link.action === 'ppaction://hlinksldjump') {
        if (link.id != null && link.id.indexOf('slide') === 0) {
          let url = link.id.substring(5)
          const idxOfDot = url.indexOf('.')
          if (-1 !== idxOfDot) url = url.substring(0, idxOfDot)

          let slideIndex = parseInt(url)
          if (isNaN(slideIndex)) slideIndex = 1

          --slideIndex

          link.id = link.action + 'slide' + slideIndex
        } else {
          link.id = undefined
        }
      } /* else {
        hlink.id = null
      } */
    }

    if (link.id != null) {
      this.setValue(link.id)
      if (link.tooltip != null) {
        this.setToolTip(link.tooltip)
      }
    }
  }

  getId() {
    return this.id
  }

  private _copy(isSelected?: boolean, pr?: { paragraph: Paragraph }) {
    const newElement = ParaHyperlink.new(this.paragraph)

    let startIndex = 0
    let endIndex = this.children.length - 1

    if (true === isSelected && true === this.state.selection.isUse) {
      startIndex = this.state.selection.startIndex
      endIndex = this.state.selection.endIndex

      if (startIndex > endIndex) {
        startIndex = this.state.selection.endIndex
        endIndex = this.state.selection.startIndex
      }
    }

    startIndex = Math.max(0, startIndex)
    endIndex = Math.min(endIndex, this.children.length - 1)

    for (let i = startIndex; i <= endIndex; i++) {
      const run = this.children.at(i)!

      let insertRun: ParaRun
      if (startIndex === i || endIndex === i) {
        insertRun = run.clone(!!isSelected, pr)
      } else {
        insertRun = run.clone(false, pr)
      }
      // clone should not set parent dirty
      newElement._addRun(i - startIndex, insertRun)
    }

    return newElement
  }

  clone(isSelected?: boolean, pr?: { paragraph: Paragraph }) {
    const newParaHyperlink = this._copy(isSelected, pr)

    // clone should not set parent dirty
    newParaHyperlink.value = this.value
    newParaHyperlink.toolTip = this.toolTip
    newParaHyperlink.visited = this.visited
    return newParaHyperlink
  }

  private _addRun(index: number, item: ParaRun, updateIndex?: boolean) {
    this.children.insert(index, item)

    if (false !== updateIndex) {
      if (this.state.elementIndex >= index) this.state.elementIndex++

      if (true === this.state.selection.isUse) {
        if (this.state.selection.startIndex >= index) {
          this.state.selection.startIndex++
        }

        if (this.state.selection.endIndex >= index) {
          this.state.selection.endIndex++
        }
      }

      const lineSegmentIndexes = this.lineSegmentIndexes

      const linesCount =
        LineSegmentIndexesUtil.getLinesCount(lineSegmentIndexes)
      for (let curLine = 0; curLine < linesCount; curLine++) {
        let startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
          lineSegmentIndexes,
          curLine
        )
        let endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
          lineSegmentIndexes,
          curLine
        )

        if (startIndex > index) startIndex++

        if (endIndex > index) endIndex++

        LineSegmentIndexesUtil.setIndexesOfLine(
          lineSegmentIndexes,
          curLine,
          startIndex,
          endIndex
        )

        if (index === this.children.length - 1 && linesCount - 1 === curLine) {
          LineSegmentIndexesUtil.setEndIndexOfLine(
            lineSegmentIndexes,
            curLine,
            LineSegmentIndexesUtil.getEndIndexOfLine(
              lineSegmentIndexes,
              curLine
            ) + 1
          )
        }
      }
    }

    for (let i = 0, l = this.adjacentPositions.length; i < l; i++) {
      const hyperAdjPos = this.adjacentPositions[i]
      const elementPosition = hyperAdjPos.adjacentPos!.pos
      const lvl = hyperAdjPos.level

      if (elementPosition.positonIndices[lvl] >= index) {
        elementPosition.positonIndices[lvl]++
      }
    }

    for (let i = 0, l = this.searchResults.length; i < l; i++) {
      const mark = this.searchResults[i]
      const elementPosition =
        true === mark.isStart
          ? mark.searchResult.startPosition
          : mark.searchResult.endPosition
      const lvl = mark.level

      if (elementPosition.positonIndices[lvl] >= index) {
        elementPosition.positonIndices[lvl]++
      }
    }
  }
  addItemAt(pos: number, item: ParagraphItem, updateIndex?: boolean) {
    if (item.instanceType === InstanceType.ParaHyperlink) {
      const itemsToAdd = item.removeContent(0, item.children.length)
      ParaHyperlink.recycle(item)
      for (let itemPos = 0, l = itemsToAdd.length; itemPos < l; itemPos++) {
        this.addItemAt(pos + itemPos, itemsToAdd[itemPos], updateIndex)
      }

      return
    }
    informDocChange(this)

    this._addRun(pos, item, updateIndex)
    updateCalcStatusForTextContentItem(this)
  }

  removeContent(index: number, count: number, needUpdatePosition?: boolean) {
    informDocChange(this)
    const removedRuns = this.children.remove(index, count)

    if (false !== needUpdatePosition) {
      if (this.state.elementIndex > index + count) {
        this.state.elementIndex -= count
      } else if (this.state.elementIndex > index) {
        this.state.elementIndex = index
      }

      if (true === this.state.selection.isUse) {
        if (this.state.selection.startIndex <= this.state.selection.endIndex) {
          if (this.state.selection.startIndex > index + count) {
            this.state.selection.startIndex -= count
          } else if (this.state.selection.startIndex > index) {
            this.state.selection.startIndex = index
          }

          if (this.state.selection.endIndex >= index + count) {
            this.state.selection.endIndex -= count
          } else if (this.state.selection.endIndex >= index) {
            this.state.selection.endIndex = Math.max(0, index - 1)
          }
        } else {
          if (this.state.selection.startIndex >= index + count) {
            this.state.selection.startIndex -= count
          } else if (this.state.selection.startIndex >= index) {
            this.state.selection.startIndex = Math.max(0, index - 1)
          }

          if (this.state.selection.endIndex > index + count) {
            this.state.selection.endIndex -= count
          } else if (this.state.selection.endIndex > index) {
            this.state.selection.endIndex = index
          }
        }

        this.selection.startIndex = Math.max(
          0,
          Math.min(this.children.length - 1, this.selection.startIndex)
        )
        this.selection.endIndex = Math.max(
          0,
          Math.min(this.children.length - 1, this.selection.endIndex)
        )
      }

      const lineSegmentIndexes = this.lineSegmentIndexes
      const linesCount =
        LineSegmentIndexesUtil.getLinesCount(lineSegmentIndexes)
      for (let lineIndex = 0; lineIndex < linesCount; lineIndex++) {
        let startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
          lineSegmentIndexes,
          lineIndex
        )
        let endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
          lineSegmentIndexes,
          lineIndex
        )

        if (startIndex > index + count) startIndex -= count
        else if (startIndex > index) startIndex = Math.max(0, index)

        if (endIndex >= index + count) endIndex -= count
        else if (endIndex >= index) endIndex = Math.max(0, index)

        LineSegmentIndexesUtil.setIndexesOfLine(
          lineSegmentIndexes,
          lineIndex,
          startIndex,
          endIndex
        )
      }
    }

    for (let i = 0, l = this.adjacentPositions.length; i < l; i++) {
      const hyperAdjPos = this.adjacentPositions[i]
      const elementPosition = hyperAdjPos.adjacentPos!.pos
      const lvl = hyperAdjPos.level

      if (elementPosition.positonIndices[lvl] > index + count) {
        elementPosition.positonIndices[lvl] -= count
      } else if (elementPosition.positonIndices[lvl] > index) {
        elementPosition.positonIndices[lvl] = Math.max(0, index)
      }
    }

    for (let i = 0, l = this.searchResults.length; i < l; i++) {
      const mark = this.searchResults[i]
      const elementPosition =
        true === mark.isStart
          ? mark.searchResult.startPosition
          : mark.searchResult.endPosition
      const lvl = mark.level

      if (elementPosition.positonIndices[lvl] > index + count) {
        elementPosition.positonIndices[lvl] -= count
      } else if (elementPosition.positonIndices[lvl] > index) {
        elementPosition.positonIndices[lvl] = Math.max(0, index)
      }
    }
    updateCalcStatusForTextContentItem(this)
    return removedRuns
  }
  private _add(item: RunContentElement | ParaRun) {
    if (item.instanceType === InstanceType.ParaRun) item.parent = this
    const elmIndex = this.state.elementIndex
    const curItem = this.children.at(elmIndex)
    if (curItem) {
      switch (item.instanceType) {
        case InstanceType.ParaRun: {
          const textPr = this.getFirstContentTextPr()
          item.selectAll()
          applyTextPrToParaRun(item, textPr)
          item.removeSelection()

          const newRun = curItem.splitAtIndex(curItem.state.elementIndex)
          this.addItemAt(elmIndex + 1, item)
          this.addItemAt(elmIndex + 2, newRun)

          this.state.elementIndex = elmIndex + 2
          moveCursorToStartPosForParaItem(
            this.children.at(this.state.elementIndex)!
          )

          break
        }

        default: {
          curItem.add(item)
          break
        }
      }
    }
  }
  add(item: RunContentElement | ParaRun | ParaHyperlink) {
    if (InstanceType.ParaHyperlink === item.instanceType) {
      const elmIndex = this.state.elementIndex
      const curItem = this.children.at(elmIndex)
      if (item.children.length > 0 && curItem) {
        const curElementPos = new ParagraphElementPosition()
        updateParaContentPosition(curItem, false, false, curElementPos)

        const newItem = curItem.splitAtContentPos(curElementPos, 0)
        const paraRunsInItem = item.removeContent(0, item.children.length)
        ParaHyperlink.recycle(item)
        const count = paraRunsInItem.length
        for (let i = 0; i < count; i++) {
          this.addItemAt(elmIndex + i + 1, paraRunsInItem[i], false)
        }
        this.addItemAt(elmIndex + count + 1, newItem, false)
        this.state.elementIndex = elmIndex + count
        moveCursorToEndPosForParaItem(
          this.children.at(this.state.elementIndex)!
        )
      }
    } else {
      this._add(item)
    }
  }
  splitAtContentPos(elementPosition: ParagraphElementPosition, lvl: number) {
    const newParaHyperlink = ParaHyperlink.new(this.paragraph)

    const index = elementPosition.get(lvl)

    const textPr = this.textPrAt(elementPosition, lvl)

    let newElement = this.children
      .at(index)
      ?.splitAtContentPos(elementPosition, lvl + 1)

    if (null == newElement) {
      newElement = ParaRun.new(this.paragraph, textPr)
    }

    const newContent = this.removeContent(
      index + 1,
      this.children.length - index - 1,
      false
    )

    const l = newContent.length
    for (let i = 0; i < l; i++) {
      newParaHyperlink.addItemAt(i, newContent[i], false)
    }

    newParaHyperlink.addItemAt(0, newElement, false)
    newParaHyperlink.setValue(this.value)
    newParaHyperlink.setToolTip(this.toolTip)
    return newParaHyperlink
  }

  setVisited(isVisited: boolean) {
    this.visited = isVisited
  }

  setToolTip(toolTip: string) {
    informDocChange(this)

    this.toolTip = toolTip
    updateCalcStatusForTextContentItem(this)
  }

  getToolTip() {
    if (null == this.toolTip) {
      if ('string' === typeof this.value) return this.value
      else return ''
    } else return this.toolTip
  }

  setValue(value: string) {
    informDocChange(this)

    this.value = value
    updateCalcStatusForTextContentItem(this)
  }

  getValue() {
    return this.value
  }

  syncEditorUIState() {
    const hyperText = getParagraphTextGetter()
    hyperText.collectFromParaItem(this)

    const hyperlinkOptions: HyperlinkOptions = {
      text: hyperText.getText(),
      value: this.value,
      toolTip: this.toolTip,
    }

    hookManager.invoke(Hooks.Emit.EmitHyperProp, {
      hyperlinkOptions,
    })
  }
}
