import { FontClass } from '../../../../fonts/const'
import { EditorUtil } from '../../../../globals/editor'
import { InstanceType } from '../../../instanceTypes'

import { RunContentElement } from '../../RunElement/type'
import { ParagraphCalcState } from '../../typeset/ParagraphCalcState'
import { ParaRun, ParaRunCalcState } from '../ParaRun'
import {
  assignRef,
  deref,
  Dict,
  Nullable,
  ref,
  Ref,
} from '../../../../../liber/pervasive'
import { reshape } from '../../../../lib/utils/arabicReshaper/reshaper'
import {
  getRunElementStatusInfo,
  updateRunElementStatusInfo,
} from '../../RunElement/utils'
import { ArabicReshapeItem } from '../../../../lib/utils/arabicReshaper/type'
import { PresentationField } from '../PresentationField'
import { ParaText } from '../../RunElement/ParaText'
import {
  compareIndices,
  decreaseRunElementIndices,
  eachRunElementBetween,
  getRunAndHyperlinkInfo,
  getRunElementIndicesByElement,
  RunAndHyperlinkInfo,
  RunElementIndices,
} from '../../../../re/export/textContent'
import { EditorSettings } from '../../../common/EditorSettings'
import { TextPr } from '../../../textAttributes/TextPr'
import { Paragraph } from '../../Paragraph'
import {
  canUseHB,
  makeTextContentProcessor,
  ParaTextProcessItem,
  TextElementFallbackHandler,
  TextFragmentProcessor,
} from '../utils/typeset'
import { getGlyphInfo } from '../../../../lib/utils/hb/hb'
import { VertAlignType } from '../../../common/const/attrs'
import { getFontClass } from '../../../../fonts/FontClassification'
import {
  ParaTextForm,
  TEXTWIDTH_PER_UNIT,
} from '../../../common/const/paraContent'
import {
  smallcaps_and_script_factor,
  VertAlignFactor,
  smallcaps_Factor,
} from '../../../common/const/unit'
import {
  ManagedArray,
  ManagedSlice,
  ManagedSliceUtil,
} from '../../../../common/managedArray'

export function reshapeRunItems(reshapeItems: ManagedSlice<RunContentElement>) {
  return reshape(
    reshapeItems as ManagedSlice<ArabicReshapeItem<RunContentElement>>,
    getRunElementStatusInfo,
    updateRunElementStatusInfo,
    {
      useLigature: EditorSettings.isSupportArabicLigature,
    }
  )
}
const firstPartElements = new ManagedArray<RunContentElement>()
const secondPartElements = new ManagedArray<RunContentElement>()
const parentInfoArray = new ManagedArray<Nullable<RunAndHyperlinkInfo>>()

function reshapeContent(
  run: ParaRun,
  paraCalcState: ParagraphCalcState,
  startIndex: number,
  endIndex: number | undefined,
  refHasArabic: Ref<boolean>
): Array<{
  elements: ManagedSlice<RunContentElement>
  textPr: TextPr
  parentInfo:
    | ManagedSlice<Nullable<RunAndHyperlinkInfo>>
    | Nullable<RunAndHyperlinkInfo>
}> {
  const reshapeItems = getRunElementsForReshape(
    run,
    paraCalcState,
    startIndex,
    endIndex
  )

  for (let i = 0, l = reshapeItems.length; i < l; i++) {
    if (reshapeRunItems(reshapeItems[i].elements) === true) {
      assignRef(refHasArabic, true)
    }
  }

  return reshapeItems
}

function getRunElementsForReshape(
  run: ParaRun,
  paraCalcState: ParagraphCalcState,
  startIndex: number,
  endIndex: number | undefined
): Array<{
  elements: ManagedSlice<RunContentElement>
  textPr: TextPr
  parentInfo:
    | ManagedSlice<Nullable<RunAndHyperlinkInfo>>
    | Nullable<RunAndHyperlinkInfo>
}> {
  // reset managed array
  firstPartElements.reset()
  secondPartElements.reset()
  parentInfoArray.reset()
  //
  const { reshapeStartPos, paragraph: paragraph } = paraCalcState

  const curRunPos: Nullable<RunElementIndices> = getRunElementIndicesByElement(
    paragraph,
    run
  )
  let reshapeStartRun: Nullable<ParaRun>
  let reshapeStartPosParentInfo: Nullable<RunAndHyperlinkInfo>
  const isOnlyHandleCurrentRun = (() => {
    // first  letter is not arabic
    const firstItem = run.children.at(0)
    if (
      firstItem == null ||
      firstItem.instanceType !== InstanceType.ParaText ||
      !firstItem.isArabic()
    ) {
      return true
    }

    // empty startPos
    if (reshapeStartPos == null) {
      return true
    }

    // start within current run
    if (startIndex > 0) {
      return true
    }

    // invalid startPos

    if (curRunPos == null || compareIndices(curRunPos, reshapeStartPos) <= 0) {
      return true
    }

    // invalid element at startPos
    reshapeStartPosParentInfo = getRunAndHyperlinkInfo(
      paragraph,
      reshapeStartPos
    )
    if (reshapeStartPosParentInfo == null) {
      return true
    } else {
      reshapeStartRun = reshapeStartPosParentInfo.run
    }
    return false
  })()
  const curTextPr = run.getCalcedPr(false)
  const curParentInfo =
    curRunPos && getRunAndHyperlinkInfo(paragraph, curRunPos)

  if (isOnlyHandleCurrentRun === true) {
    getReshapeItemsForRun(run, startIndex, endIndex, firstPartElements)
    return [
      {
        elements: firstPartElements,
        textPr: curTextPr,
        parentInfo: curParentInfo,
      },
    ]
  }

  const reshapeStartRunTextPr = reshapeStartRun!.getCalcedPr(false)

  const endIndices = decreaseRunElementIndices(paragraph, curRunPos!)

  if (endIndices) {
    collectRunElementsBetweenPoses(
      paragraph,
      reshapeStartPos!,
      endIndices,
      firstPartElements,
      parentInfoArray
    )
  }

  //the run at startPos is not compatible with current run
  if (!curTextPr.isCompatible(reshapeStartRunTextPr)) {
    paraCalcState.updateReshapeStartPos(
      startIndex,
      paraCalcState.getReshapeState()
    )
    getReshapeItemsForRun(run, startIndex, endIndex, secondPartElements)
    return [
      {
        elements: firstPartElements,
        textPr: reshapeStartRunTextPr,
        parentInfo: reshapeStartPosParentInfo,
      },
      {
        elements: secondPartElements,
        textPr: curTextPr,
        parentInfo: curParentInfo,
      },
    ]
  } else {
    _collectItemsForRun(
      run,
      startIndex,
      endIndex,
      firstPartElements,
      curParentInfo,
      parentInfoArray
    )

    return [
      {
        elements: firstPartElements,
        textPr: curTextPr,
        parentInfo: parentInfoArray,
      },
    ]
  }
}

/**
 *
 * 按照 run的粒度来控制是否需要measure运算，
 * 规则是：
 *  不包含阿语字符的情况下，如果没有输入或者删除操作，或者影响width变化的操作，比如字体字形大小变化，只会measure一遍，
 *  包含阿语的情况，每次都会measure，因为阿语可能需要reshaping
 */
export function reMeasureContent(
  run: ParaRun,
  paraCalcState: ParagraphCalcState,
  segmentStartPos: number
) {
  const calculateInfo = run.calculateInfo

  if (calculateInfo.reMeasure !== true) {
    return calculateInfo.hasArabic
  }

  calculateInfo.reMeasure = false

  if ((run as PresentationField).paraField === true) {
    const paraField = run as PresentationField
    paraField.calcContent()
  }

  const refHasArabic = ref(false)
  const reshapeItems = reshapeContent(
    run,
    paraCalcState,
    segmentStartPos,
    undefined,
    refHasArabic
  )
  calculateInfo.hasArabic = deref(refHasArabic)

  const textPr = run.getCalcedPr(false)

  EditorUtil.FontKit.setTextPr(textPr)
  EditorUtil.FontKit.setFontClass(textPr.lang.fontClass ?? FontClass.ASCII)

  const { textHeight, ascender, descender } = EditorUtil.FontKit.getMetrics()
  run.metrics.textHeight = textHeight
  run.metrics.textDescent = Math.abs(descender)
  run.metrics.textAscent = textHeight - run.metrics.textDescent
  run.metrics.textAscentOfMetrics = ascender

  const paragraph = run.paragraph
  reshapeItems.forEach(({ elements, textPr, parentInfo }) =>
    measureElementsByRun(
      elements,
      textPr,
      paragraph,
      run.calculateInfo,
      parentInfo
    )
  )

  return calculateInfo.hasArabic
}
const gRunElementMeasureProcessor = makeTextContentProcessor()

const _sliceMapper = ({ paraText }) => paraText
export function measureElementsByRun(
  items: ManagedSlice<RunContentElement>,
  textPr: TextPr,
  paragraph: Paragraph,
  calculateInfo: ParaRunCalcState,
  parentInfoArr:
    | ManagedSlice<Nullable<RunAndHyperlinkInfo>>
    | Nullable<RunAndHyperlinkInfo>,
  onWidthChanged?: (item: ParaText) => void
) {
  const textPrOfParaEnd = paragraph?.getParaEndCalcedPr()
  const useHB = calculateInfo.hasArabic && canUseHB(textPr)
  const processor: TextFragmentProcessor = (
    textElmArr: ManagedSlice<ParaText>
  ) => {
    measureFragmentByPr(textElmArr, textPr, onWidthChanged)
  }
  const fallback: TextElementFallbackHandler = (
    items: ManagedSlice<ParaTextProcessItem>
  ) => {
    const fallbackMappedSlice = ManagedSliceUtil.map(
      ManagedSliceUtil.slice(items, 0, items.length),
      _sliceMapper
    )

    reshapeRunItems(fallbackMappedSlice)
    ManagedSliceUtil.forEach(fallbackMappedSlice, (paraText) =>
      measureRunContentElement(
        paraText,
        textPr,
        textPrOfParaEnd,
        onWidthChanged
      )
    )
  }

  for (let i = 0, l = items.length; i < l; i++) {
    const runElement = items.at(i)!
    const parentInfo = ManagedSliceUtil.isManagedSlice(parentInfoArr)
      ? parentInfoArr.at(i)!
      : parentInfoArr

    const _runElement = useHB
      ? gRunElementMeasureProcessor.process(
          runElement,
          parentInfo,
          processor,
          fallback
        )
      : runElement
    if (_runElement) {
      const isActualMeasured = measureRunContentElement(
        items.at(i)!,
        textPr,
        textPrOfParaEnd,
        onWidthChanged
      )
      if (isActualMeasured === false) {
        calculateInfo.reMeasure = true
      }
    }
  }
  if (items.length > 0) {
    gRunElementMeasureProcessor.process(null, null, processor, fallback)
  }
}

function measureRunContentElement(
  item: RunContentElement,
  textPr: TextPr,
  textPrOfParaEnd: TextPr,
  onFormCodeChanged?: (item: ParaText) => void
) {
  switch (item.instanceType) {
    case InstanceType.ParaText: {
      return item.measure(EditorUtil.FontKit, textPr, onFormCodeChanged)
    }
    case InstanceType.ParaEnd: {
      if (textPrOfParaEnd) {
        EditorUtil.FontKit.setTextPr(textPrOfParaEnd)
      }
      return item.measure(EditorUtil.FontKit)
    }
    default:
      return item.measure(EditorUtil.FontKit, textPr)
  }
}
// helpers

function _collectItemsForRun(
  run: ParaRun,
  startIndex: Nullable<number>,
  endIndex: Nullable<number>,
  collection: ManagedArray<RunContentElement>,
  parentInfo?: Nullable<RunAndHyperlinkInfo>,
  infoCollection?: ManagedArray<Nullable<RunAndHyperlinkInfo>>
) {
  if (run == null) {
    return
  }

  const l = run.children.length
  if (l <= 0) {
    return
  }
  startIndex = startIndex == null || startIndex < 0 ? 0 : startIndex

  endIndex = endIndex == null || endIndex > l - 1 ? l - 1 : endIndex
  endIndex = endIndex < 0 ? l - 1 + endIndex : endIndex
  endIndex = endIndex < 0 ? 0 : endIndex

  for (let i = startIndex; i <= endIndex; i++) {
    const item = run.children.at(i)
    if (item != null) {
      collection.push(item)
      if (infoCollection != null) {
        infoCollection.push(parentInfo)
      }
    }
  }
}

function getReshapeItemsForRun(
  run: ParaRun,
  startIndex: Nullable<number>,
  endIndex: Nullable<number>,
  collection: ManagedArray<RunContentElement>
): ManagedArray<RunContentElement> {
  _collectItemsForRun(run, startIndex, endIndex, collection)
  return collection
}

export function collectRunElementsBetweenPoses(
  paragraph: Paragraph,
  startIndices: RunElementIndices,
  endIndices: RunElementIndices,
  collection: ManagedArray<RunContentElement>,
  parentInfoCollection?: ManagedArray<Nullable<RunAndHyperlinkInfo>>
): ManagedArray<RunContentElement> {
  eachRunElementBetween(
    paragraph,
    startIndices,
    endIndices,
    (item, indices) => {
      collection.push(item)
      if (parentInfoCollection != null) {
        const parentInfo = indices && getRunAndHyperlinkInfo(paragraph, indices)
        parentInfoCollection.push(parentInfo)
      }
    }
  )
  return collection
}

function measureFragmentByPr(
  textElmArr: ManagedSlice<ParaText>,
  textPr: TextPr,
  onWidthChanged?: (item: ParaText) => void
) {
  let text = ''
  ManagedSliceUtil.forEach(textElmArr, (t) => {
    const c = String.fromCodePoint(t.value)
    text += c
  })
  const hbFont = EditorUtil.FontKit.getHBFont(textPr)!
  const glyphArr = getGlyphInfo(EditorUtil.FontKit.hb!, text, hbFont)!
  const glyphInfos = glyphArr
    .reduce(
      (result, { Cluster, XAdvance }, index) => {
        const lastItem = result[index - 1]
        if (lastItem == null || lastItem.Cluster !== Cluster) {
          result.push({
            Cluster,
            advanceWidth: XAdvance,
          })
        } else {
          lastItem.advanceWidth += XAdvance
        }
        return result
      },
      [] as Array<{ Cluster: number; advanceWidth: number }>
    )
    .reverse()
    .reduce((result, { advanceWidth, Cluster }) => {
      result[Cluster] = advanceWidth
      return result
    }, {} as Dict<number>)
  // console.log(`measuring text:`, text)
  // console.log(`glyphArr`, glyphArr)
  // console.log(`glyphInfos`, glyphInfos)
  for (let i = 0, l = textElmArr.length; i < l; i++) {
    const advanceWidth = glyphInfos[i]

    const paraText = textElmArr.at(i)!
    if (advanceWidth != null) {
      _adjustMeasureForParaText(
        paraText,
        advanceWidth,
        hbFont.unitsPerEM,
        textPr,
        onWidthChanged
      )
    } else {
      paraText.setSkipStatus()
    }
  }
}

function _adjustMeasureForParaText(
  paraText: ParaText,
  advanceWidth: number,
  unitsPerEM: number,
  textPr: TextPr,
  onWidthChanged?: (item: ParaText) => void
) {
  let isCapitals = false

  const charCode = paraText.getDrawCharCode()
  let formatCharCode = charCode
  if (true === textPr.caps || true === textPr.smallCaps) {
    paraText.form |= ParaTextForm.CAPITALS
    formatCharCode = String.fromCodePoint(charCode)
      .toUpperCase()
      .codePointAt(0)!
    isCapitals = formatCharCode === charCode
  } else {
    paraText.form &= ParaTextForm.NON_CAPITALS
    isCapitals = false
  }

  if (textPr.vertAlign !== VertAlignType.Baseline) {
    paraText.form |= ParaTextForm.FONTKOEF_SCRIPT
  } else {
    paraText.form &= ParaTextForm.NON_FONTKOEF_SCRIPT
  }

  if (
    true !== textPr.caps &&
    true === textPr.smallCaps &&
    false === isCapitals
  ) {
    paraText.form |= ParaTextForm.FONTKOEF_SMALLCAPS
  } else {
    paraText.form &= ParaTextForm.NON_FONTKOEF_SMALLCAPS
  }

  const hint = textPr.rFonts.hint
  const isCS = true
  const ea = textPr.lang?.eastAsia

  const fontClass = getFontClass(formatCharCode, hint, ea, isCS)

  const form_0Byte = (paraText.form >> 0) & 0xff
  const form_2Byte = (paraText.form >> 16) & 0xff

  paraText.form = form_0Byte | ((fontClass & 0xff) << 8) | (form_2Byte << 16)

  let fontFactor = 1
  if (
    paraText.form & ParaTextForm.FONTKOEF_SCRIPT &&
    paraText.form & ParaTextForm.FONTKOEF_SMALLCAPS
  ) {
    fontFactor = smallcaps_and_script_factor
  } else if (paraText.form & ParaTextForm.FONTKOEF_SCRIPT) {
    fontFactor = VertAlignFactor.Size
  } else if (paraText.form & ParaTextForm.FONTKOEF_SMALLCAPS) {
    fontFactor = smallcaps_Factor
  }

  const fontSize = textPr.fontSize!
  if (1 !== fontFactor) {
    fontFactor = ((fontSize * fontFactor * 2 + 0.5) | 0) / 2 / fontSize
  }

  EditorUtil.FontKit.setTextPr(textPr)
  EditorUtil.FontKit.setFontClass(fontClass, fontFactor)
  const measuredWidth = EditorUtil.FontKit.AdjustMeasure(
    advanceWidth,
    unitsPerEM
  )

  const actualWidth = (Math.max(measuredWidth, 0) * TEXTWIDTH_PER_UNIT) | 0
  const width =
    (Math.max(measuredWidth + textPr.spacing!, 0) * TEXTWIDTH_PER_UNIT) | 0

  if (paraText.width !== width && onWidthChanged) {
    onWidthChanged(this)
  }
  paraText.updateWidthAndUnsetSkip(width, actualWidth)
}
