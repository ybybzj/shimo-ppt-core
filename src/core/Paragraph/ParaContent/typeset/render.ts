import { Nullable } from '../../../../../liber/pervasive'
import { VertAlignType } from '../../../common/const/attrs'
import { isCJKChar } from '../../../../fonts/utils'
import { EditorUtil } from '../../../../globals/editor'
import { InstanceType } from '../../../instanceTypes'
import { logger } from '../../../../lib/debug/log'

import {
  getRunAndHyperlinkInfo,
  getRunElementByIndices,
  getRunElementIndexFromIndices,
  RunAndHyperlinkInfo,
} from '../../../../re/export/textContent'
import { ParaTextForm } from '../../../common/const/paraContent'
import {
  Factor_mm_to_pix,
  Factor_pt_to_mm,
  HightLight_None,
  smallcaps_and_script_factor,
  smallcaps_Factor,
  VertAlignFactor,
} from '../../../common/const/unit'
import { TextContentDrawContext } from '../../../common/types'
import { GraphicsBoundsChecker } from '../../../graphic/Bounds'
import { CanvasDrawer } from '../../../graphic/CanvasDrawer'
import {
  CComplexColor,
  complexColorToCHexColor,
} from '../../../color/complexColor'
import {
  gHLinkColor,
  gVisitedHLinkColor,
} from '../../../SlideElement/attrs/creators'
import { CHexColor } from '../../../color/CHexColor'
import { TextPr } from '../../../textAttributes/TextPr'
import { ParaText } from '../../RunElement/ParaText'
import { RunContentElement } from '../../RunElement/type'
import { ParagraphItem, RunElemnetOrderIndicesInfo } from '../../type'
import {
  ParagraphRenderStateElements,
  ParagraphRenderStateHighlights,
  ParagraphRenderStateLines,
} from '../../typeset/render'
import {
  canUseHB,
  getWidthOfRunElement,
  gTextBrushGetter,
  makeTextContentProcessor,
  ParaTextProcessItem,
  TextDrawingColorInfo,
  TextElementFallbackHandler,
  TextFragmentProcessor,
} from '../utils/typeset'
import { ParaRun } from '../ParaRun'
import { FontClassValues } from '../../../../fonts/const'
import { FillEffects } from '../../../SlideElement/attrs/fill/fill'
import {
  BrowserInfo,
  enlargeByRetinaRatio,
} from '../../../../common/browserInfo'
import { setPenDash } from '../../../graphic/drawUtils'
import { LineJoinType } from '../../../SlideElement/const'
import { getLineBounds } from '../utils/render'
import { ShapeRenderingInfo } from '../../../render/drawShape/type'
import { drawShapeByInfo } from '../../../render/drawShape/drawShape'
import { LineSegmentIndexesUtil } from '../LineSegmentIndexes'
import {
  ManagedArray,
  ManagedSlice,
  ManagedSliceUtil,
} from '../../../../common/managedArray'
import { EditorSettings } from '../../../common/EditorSettings'
import { ColorRGBA } from '../../../color/type'
const gRunElementDrawProcessor = makeTextContentProcessor()

export function renderLineOfRunElement(
  indicesArray: ManagedArray<RunElemnetOrderIndicesInfo>,
  paraRenderStateElements: ParagraphRenderStateElements
) {
  const paragraph = paraRenderStateElements.paragraph!

  const processor: TextFragmentProcessor = (
    textElmArr: ManagedSlice<ParaText>,
    parentInfo: RunAndHyperlinkInfo
  ) =>
    renderFragmentByParams(
      textElmArr,
      paraRenderStateElements,
      parentInfo,
      true
    )

  let parentInfo: Nullable<RunAndHyperlinkInfo>
  let curTextPr: Nullable<TextPr>
  let isHBAvailable: boolean = false
  const fallback: TextElementFallbackHandler = (
    textArr: ManagedSlice<ParaTextProcessItem>
  ) =>
    ManagedSliceUtil.forEach(textArr, ({ paraText, parentInfo }) =>
      renderRunElement(paraText, parentInfo, paraRenderStateElements)
    )

  for (let i = 0, l = indicesArray.length; i < l; i++) {
    const { indices, mirrorCode } = indicesArray.at(i)!

    const runElement = getRunElementByIndices(paragraph, indices)
    parentInfo = getRunAndHyperlinkInfo(paragraph, indices)

    if (runElement && parentInfo) {
      const textPr = parentInfo.run.getCalcedPr(false)
      const isOldHBAvailable = isHBAvailable
      if (curTextPr !== textPr) {
        curTextPr = textPr
        isHBAvailable = canUseHB(curTextPr)
      }

      const _runElement = isHBAvailable
        ? gRunElementDrawProcessor.process(
            runElement,
            parentInfo,
            processor,
            fallback
          )
        : runElement
      if (isOldHBAvailable === true && isHBAvailable === false) {
        gRunElementDrawProcessor.process(null, null, processor, fallback)
      }
      if (_runElement) {
        renderRunElement(
          _runElement,
          parentInfo,
          paraRenderStateElements,
          mirrorCode
        )
      }
    } else {
      logger.warn(
        `[drawLineOfRunElement]invalid dices for paragraph:`,
        indices,
        paragraph
      )
    }
  }

  if (indicesArray.length > 0) {
    gRunElementDrawProcessor.process(null, null, processor, fallback)
  }
}

const _mergedParaTextsArr = new ManagedArray<ParaText>(100)
export function renderRunElementOfParaItem(
  paraItem: ParagraphItem,
  paraRenderStateElements: ParagraphRenderStateElements
) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    paraRenderStateElements.isHyperlinkVisited = paraItem.visited
    paraRenderStateElements.isHyperlink = true
    const lineIndex = paraRenderStateElements.line - paraItem.startParaLine

    const lineSegmentIndexes = paraItem.lineSegmentIndexes
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      lineIndex
    )
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      lineIndex
    )

    for (let i = startIndex; i <= endIndex; i++) {
      const run = paraItem.children.at(i)
      if (run) renderRunElementOfParaItem(run, paraRenderStateElements)
    }
    paraRenderStateElements.isHyperlinkVisited = false
    paraRenderStateElements.isHyperlink = false
  } else {
    const {
      line,
      drawContext: drawContext,
      isHyperlink,
      isHyperlinkVisited,
    } = paraRenderStateElements
    if (drawContext == null) {
      return
    }
    const lineIndex = line - paraItem.startParaLine
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      paraItem.lineSegmentIndexes,
      lineIndex
    )
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      paraItem.lineSegmentIndexes,
      lineIndex
    )
    const runAndHyperlinkInfo: RunAndHyperlinkInfo = {
      run: paraItem,
      isInLink: isHyperlink,
      isVisited: isHyperlinkVisited,
    }

    const canNotMerge =
      EditorUtil.FontKit.isEaVertText ||
      drawContext.instanceType !== InstanceType.CanvasDrawer

    let textForm: undefined | number
    let isLastPunctuation: boolean = false
    for (let i = startIndex; i < endIndex; i++) {
      const item = paraItem.children.at(i)
      if (item) {
        const needRenderAlone =
          EditorSettings.isBatchRenderText !== true ||
          canNotMerge ||
          item.instanceType !== InstanceType.ParaText ||
          !item.canMerge()

        const isPunctuationItem =
          item.instanceType === InstanceType.ParaText && item.isPunctuation()

        if (
          needRenderAlone ||
          item.form !== textForm ||
          (isPunctuationItem && isLastPunctuation) // consecutive punctuation should render separately
        ) {
          if (_mergedParaTextsArr.length > 0) {
            renderFragmentByParams(
              _mergedParaTextsArr,
              paraRenderStateElements,
              runAndHyperlinkInfo,
              false
            )

            _mergedParaTextsArr.reset()
          }

          if (!needRenderAlone) {
            textForm = item.form
            _mergedParaTextsArr.push(item)
          } else {
            renderRunElement(
              item,
              runAndHyperlinkInfo,
              paraRenderStateElements,
              undefined
            )
            textForm = undefined
          }
        } else {
          _mergedParaTextsArr.push(item)
        }

        isLastPunctuation = isPunctuationItem
      }
    }

    if (_mergedParaTextsArr.length > 0) {
      renderFragmentByParams(
        _mergedParaTextsArr,
        paraRenderStateElements,
        runAndHyperlinkInfo,
        false
      )
    }

    _mergedParaTextsArr.reset()
    textForm = undefined
  }
}

function renderRunElement(
  runElement: RunContentElement,
  parentInfo: RunAndHyperlinkInfo,
  paraRenderStateElements: ParagraphRenderStateElements,
  mirrorCode?: number
) {
  const { paragraph: paragraph, drawContext: drawContext } =
    paraRenderStateElements
  if (drawContext == null) {
    return
  }

  let { x, y } = paraRenderStateElements

  const { run } = parentInfo

  const curTextPr = run.getCalcedPr(false)

  const state = setTextDrawingColor(
    drawContext,
    curTextPr,
    parentInfo,
    paraRenderStateElements
  )
  const brushColor = state.brushColor
  const adjustXY = (item: RunContentElement, x, y) => {
    let isEaVertItem = false
    if (EditorUtil.FontKit.isEaVertText) {
      switch (item.instanceType) {
        case InstanceType.ParaText:
        case InstanceType.ParaSpace:
        case InstanceType.ParaTab: {
          if (item.value !== undefined && isCJKChar(item.value)) {
            x += item.getWidth() / 2
            y += run.metrics.textDescent
            isEaVertItem = true
          }
          break
        }
      }
    }
    return [x, y, isEaVertItem]
  }
  const item = runElement

  const instanceType = item.instanceType

  const tmpY = y

  switch (curTextPr.vertAlign) {
    case VertAlignType.SubScript: {
      y -= VertAlignFactor.Sub * curTextPr.fontSize! * Factor_pt_to_mm
      break
    }
    case VertAlignType.SuperScript: {
      y -= VertAlignFactor.Super * curTextPr.fontSize! * Factor_pt_to_mm
      break
    }
  }

  switch (instanceType) {
    case InstanceType.ParaTab: {
      const [ax, ay] = adjustXY(item, x, y)
      item.draw(ax, ay, drawContext)
      x += item.getVisibleWidth()
      if (
        InstanceType.ParaTab === instanceType &&
        drawContext.instanceType !== InstanceType.GraphicsBoundsChecker
      ) {
        drawContext.setTextPr(curTextPr)
        if (brushColor) {
          drawContext.setBrushColor(
            brushColor.r,
            brushColor.g,
            brushColor.b,
            255
          )
          drawContext.setPenColor(brushColor.r, brushColor.g, brushColor.b, 255)
        }
      }
      break
    }
    case InstanceType.ParaText: {
      const [ax, ay, isEaVertItem] = adjustXY(item, x, y)
      const letterW = item.getActualWidth()
      const fill = curTextPr.fillEffects

      if (
        fill?.isComplexFill() &&
        drawContext.instanceType === InstanceType.CanvasDrawer
      ) {
        const g = drawContext
        const isTextStrokeSet = g.isTextStrokeSet
        const isTextFillSet = g.isTextFillSet
        const { l, t, w, h } = getLineBounds(paraRenderStateElements)
        const isBgTransparent =
          g.isBgTransparent === true && state.highlight == null
        const isEaVert = EditorUtil.FontKit.isEaVertText === true
        const ctx = g.context
        if (BrowserInfo.isMobile) {
          if (isBgTransparent === false) {
            g.isTextFillSet = false
            item.draw(ax, ay, g, mirrorCode)
            g.isTextFillSet = isTextFillSet
          }
        }

        g.save()
        g.saveState()

        g.setCxtTransformReset(false)
        g._begin()

        if (isEaVert) {
          ctx.rect(isEaVertItem ? x - 2 : x, t, (letterW + 0.5) >> 0, h)
        } else {
          ctx.rect(x, t, letterW, h)
        }
        // ctx.save()
        // ctx.lineWidth = 0.5
        // ctx.strokeStyle = '#00ff00'
        // ctx.stroke()
        // ctx.restore()

        ctx.clip()
        //
        ctx.fillStyle = '#ffffff'
        ctx.globalCompositeOperation = isBgTransparent ? 'source-over' : 'xor'
        //
        g.isTextStrokeSet = false
        g.isTextFillSet = true
        //
        item.draw(ax, ay, g, mirrorCode)
        const fontStyle = ctx.font
        //
        ctx.globalCompositeOperation = isBgTransparent
          ? 'source-in'
          : 'destination-over'

        let tx, ty
        if (isEaVert) {
          tx = (isEaVertItem ? letterW - h : 0) - t
          ty = l - (isEaVertItem ? 2 : 0)
        } else {
          tx = l
          ty = t
        }
        const drawInfo: ShapeRenderingInfo = {
          brush: fill,
          l: tx,
          t: ty,
          extX: w + 2,
          extY: isEaVertItem ? letterW : h,
        }

        drawShapeByInfo(drawInfo, g)

        ctx.globalCompositeOperation = 'source-over'

        g.restoreState()
        g.restore()
        ctx.font = fontStyle

        g.isTextStrokeSet = isTextStrokeSet
        if (BrowserInfo.isMobile) {
          if (isBgTransparent) {
            ctx.globalCompositeOperation = 'destination-over'

            g.isTextFillSet = false
            item.draw(ax, ay, g, mirrorCode)
            g.isTextFillSet = isTextFillSet
            ctx.globalCompositeOperation = 'source-over'
          }
        } else {
          g.isTextFillSet = false
          item.draw(ax, ay, g, mirrorCode)
          g.isTextFillSet = isTextFillSet
        }
      } else {
        item.draw(ax, ay, drawContext, mirrorCode)
      }

      x += item.getVisibleWidth()
      break
    }
    case InstanceType.ParaSpace: {
      const [ax, ay] = adjustXY(item, x, y)
      item.draw(ax, ay, drawContext)
      x += item.getVisibleWidth()

      break
    }
    case InstanceType.ParaEnd: {
      const textPrOfParaEnd = paragraph!.getParaEndCalcedPr()
      let isEndCell = false

      if (drawContext.instanceType !== InstanceType.GraphicsBoundsChecker) {
        if (textPrOfParaEnd.fillEffects) {
          textPrOfParaEnd.fillEffects.check(
            paraRenderStateElements.theme,
            paraRenderStateElements.colorMap
          )

          const rgbaOfParaEnd = textPrOfParaEnd.fillEffects.getRGBAColor()
          drawContext.setTextPr(textPrOfParaEnd)
          drawContext.setBrushColor(
            rgbaOfParaEnd.r,
            rgbaOfParaEnd.g,
            rgbaOfParaEnd.b,
            255
          )
        } else {
          drawContext.setBrushColor(0, 0, 0, 255)
        }
        const textDoc = paragraph!.parent
        const cell =
          textDoc && textDoc.isInTableCell() ? textDoc.getTableCell() : null
        if (cell) {
          const textDoc = cell.getTextDoc()
          isEndCell = paragraph === textDoc.lastParagraph()
        }
      }
      item.draw(x, y, drawContext, isEndCell, false)
      x += item.getWidth()

      break
    }
    case InstanceType.ParaNewLine: {
      item.draw(x, y, drawContext)
      x += item.renderWidth
      break
    }
  }

  y = tmpY

  paraRenderStateElements.x = x
  restoreDrawContextStateAfterSetTextDrawingColor(drawContext, state)
}

export function updateHightLightsDrawInfoForLine(
  indicesArray: ManagedArray<RunElemnetOrderIndicesInfo>,
  paraRenderStateHLights: ParagraphRenderStateHighlights,
  theme
) {
  const { paragraph: paragraph } = paraRenderStateHLights

  for (let i = 0, l = indicesArray.length; i < l; i++) {
    const { indices } = indicesArray.at(i)!
    const runElement = getRunElementByIndices(paragraph!, indices)
    const parentInfo = getRunAndHyperlinkInfo(paragraph!, indices)
    const runElementIndex = getRunElementIndexFromIndices(indices)
    if (runElement && parentInfo) {
      updateHightLightsDrawInfoForRunElement(
        runElement,
        parentInfo.run,
        runElementIndex,
        paraRenderStateHLights,
        theme
      )
    } else {
      logger.warn(
        `[updateHightLightsDrawInfoForLine]invalid dices for paragraph:`,
        indices,
        paragraph
      )
    }
  }
}

export function updateHightLightsDrawInfoForParaItem(
  paraItem: ParagraphItem,
  paraRenderStateHLights: ParagraphRenderStateHighlights,
  theme
) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    const lineIndexs = paraRenderStateHLights.line - paraItem.startParaLine

    const lineSegmentIndexes = paraItem.lineSegmentIndexes
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      lineIndexs
    )
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      lineIndexs
    )

    for (let i = startIndex; i <= endIndex; i++) {
      const run = paraItem.children.at(i)
      if (run) {
        updateHightLightsDrawInfoForParaItem(run, paraRenderStateHLights, theme)
      }
    }
  } else {
    const curLine = paraRenderStateHLights.line - paraItem.startParaLine

    const lineSegmentIndexes = paraItem.lineSegmentIndexes
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      curLine
    )
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      curLine
    )

    for (let i = startIndex; i < endIndex; i++) {
      const item = paraItem.children.at(i)
      if (item) {
        updateHightLightsDrawInfoForRunElement(
          item,
          paraItem,
          i,
          paraRenderStateHLights,
          theme
        )
      }
    }
  }
}

function updateHightLightsDrawInfoForRunElement(
  runElement: RunContentElement,
  run: ParaRun,
  elementIndex: number,
  paraRenderStateHLights: ParagraphRenderStateHighlights,
  theme
) {
  const arrOfHigh = paraRenderStateHLights.highLight
  const arrOfFind = paraRenderStateHLights.searchFound

  const isDrawFind = paraRenderStateHLights.drawSearchFound

  const calcedPr = run.getCalcedPr(false)

  let x = paraRenderStateHLights.x
  const y0 = paraRenderStateHLights.y0
  const y1 = paraRenderStateHLights.y1

  const highLight = calcedPr.highLight

  const count = run.searchResults.length

  const item = runElement
  const instanceType = item.instanceType
  const widthOfItem = getWidthOfRunElement(item)

  for (let i = 0; i < count; i++) {
    const res = run.searchResults[i]
    const resultIndex = res.searchResult.startPosition.get(res.level)
    if (elementIndex === resultIndex && true === res.isStart) {
      paraRenderStateHLights.numOfSearch++
    }
  }

  const drawSearch =
    paraRenderStateHLights.numOfSearch > 0 && true === isDrawFind

  switch (instanceType) {
    case InstanceType.ParaTab:
    case InstanceType.ParaText: {
      if (highLight && HightLight_None !== highLight) {
        const rgb = complexColorToCHexColor(highLight, theme)
        arrOfHigh.add(
          y0,
          y1,
          x,
          x + widthOfItem,
          0,
          rgb.r,
          rgb.g,
          rgb.b,
          highLight
        )
      }

      if (true === drawSearch) {
        arrOfFind.add(y0, y1, x, x + widthOfItem, 0, 0, 0, 0)
      }
      x += widthOfItem

      break
    }
    case InstanceType.ParaSpace: {
      if (HightLight_None !== highLight) {
        const rgb = complexColorToCHexColor(highLight!, theme)
        arrOfHigh.add(
          y0,
          y1,
          x,
          x + widthOfItem,
          0,
          rgb.r,
          rgb.g,
          rgb.b,
          highLight
        )
      }

      if (true === drawSearch) {
        arrOfFind.add(y0, y1, x, x + widthOfItem, 0, 0, 0, 0)
      }

      x += widthOfItem

      break
    }
    case InstanceType.ParaEnd: {
      x += item.getWidth()
      break
    }
    case InstanceType.ParaNewLine: {
      x += widthOfItem
      break
    }
  }

  for (let i = 0; i < count; i++) {
    const res = run.searchResults[i]
    const resultIndex = res.searchResult.endPosition.get(res.level)

    if (elementIndex + 1 === resultIndex && true !== res.isStart) {
      paraRenderStateHLights.numOfSearch--
    }
  }

  paraRenderStateHLights.x = x
}

export function renderUnderOrStrikeLinesForLine(
  indicesArray: ManagedArray<RunElemnetOrderIndicesInfo>,
  paraRenderStateLns: ParagraphRenderStateLines
) {
  const { paragraph: paragraph } = paraRenderStateLns

  for (let i = 0, l = indicesArray.length; i < l; i++) {
    const { indices } = indicesArray.at(i)!
    const runElement = getRunElementByIndices(paragraph!, indices)
    const parentInfo = getRunAndHyperlinkInfo(paragraph!, indices)
    const runElementIndex = getRunElementIndexFromIndices(indices)
    if (runElement && parentInfo) {
      renderUnderOrStrikeLinesForRunElement(
        runElement,
        parentInfo,
        runElementIndex,
        paraRenderStateLns
      )
    } else {
      logger.warn(
        `[drawUnderOrStrikeLinesForLine]invalid dices for paragraph:`,
        indices,
        paragraph
      )
    }
  }
}

export function renderUnderOrStrikeLinesForParaItem(
  paraItem: ParagraphItem,
  paraRenderStateLns: ParagraphRenderStateLines
) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    paraRenderStateLns.isHyperlinkVisited = paraItem.visited
    paraRenderStateLns.isHyperlink = true
    const lineIndex = paraRenderStateLns.line - paraItem.startParaLine

    const lineSegmentIndexes = paraItem.lineSegmentIndexes
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      lineIndex
    )
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      lineIndex
    )

    const curDepth = paraRenderStateLns.curLevel
    for (let i = startIndex; i <= endIndex; i++) {
      paraRenderStateLns.currentPosition.updatePosByLevel(i, curDepth)
      paraRenderStateLns.curLevel = curDepth + 1

      const run = paraItem.children.at(i)
      if (run) renderUnderOrStrikeLinesForParaRun(run, paraRenderStateLns)
    }
    paraRenderStateLns.isHyperlinkVisited = false
    paraRenderStateLns.isHyperlink = false
  } else {
    renderUnderOrStrikeLinesForParaRun(paraItem, paraRenderStateLns)
  }
}

function renderUnderOrStrikeLinesForParaRun(
  run: ParaRun,
  paraRenderStateLns: ParagraphRenderStateLines
) {
  const curLine = paraRenderStateLns.line - run.startParaLine

  const lineSegmentIndexes = run.lineSegmentIndexes
  const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
    lineSegmentIndexes,
    curLine
  )
  const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
    lineSegmentIndexes,
    curLine
  )

  const curTextPr = run.getCalcedPr(false)
  const { yForStrikeout, yForUnderline } = _renderUnderOrStrikeLines_computeYs(
    curTextPr,
    paraRenderStateLns
  )

  const curColor = _renderUnderOrStrikeLines_computeColor(
    run,
    curTextPr,
    paraRenderStateLns,
    paraRenderStateLns.isHyperlinkVisited,
    paraRenderStateLns.isHyperlink
  )

  paraRenderStateLns.currentPosition.updatePosByLevel(
    startIndex,
    paraRenderStateLns.curLevel
  )

  for (let i = startIndex; i < endIndex; i++) {
    const item = run.children.at(i)
    if (item) {
      _renderUnderOrStrikeLinesForRunElem(
        item,
        paraRenderStateLns,
        run,
        curTextPr,
        i,
        yForUnderline,
        yForStrikeout,
        curColor
      )
    }
  }
}

function renderUnderOrStrikeLinesForRunElement(
  runElement: RunContentElement,
  parentInfo: RunAndHyperlinkInfo,
  runElementIndex: number,
  paraRenderStateLns: ParagraphRenderStateLines
) {
  const { run, isInLink, isVisited } = parentInfo
  const curTextPr = run.getCalcedPr(false)
  const { yForStrikeout, yForUnderline } = _renderUnderOrStrikeLines_computeYs(
    curTextPr,
    paraRenderStateLns
  )

  // TO BE IMPROV_ED
  const curColor = _renderUnderOrStrikeLines_computeColor(
    run,
    curTextPr,
    paraRenderStateLns,
    isVisited === true,
    isInLink
  )
  _renderUnderOrStrikeLinesForRunElem(
    runElement,
    paraRenderStateLns,
    run,
    curTextPr,
    runElementIndex,
    yForUnderline,
    yForStrikeout,
    curColor
  )
}

function _renderUnderOrStrikeLines_computeYs(
  curTextPr: TextPr,
  paraRenderStateLns: ParagraphRenderStateLines
): { yForStrikeout: number; yForUnderline: number } {
  const y = paraRenderStateLns.baseLine
  const underlineOffset = paraRenderStateLns.underlineOffset
  let yForStrikeout = y

  const fontFactor = 1

  let yForUnderline = y + underlineOffset

  switch (curTextPr.vertAlign) {
    case VertAlignType.Baseline: {
      yForStrikeout +=
        -curTextPr.fontSize! *
        fontFactor *
        Factor_pt_to_mm *
        (EditorUtil.FontKit.isEaVertText ? 0 : 0.27)
      break
    }
    case VertAlignType.SubScript: {
      yForStrikeout +=
        -curTextPr.fontSize! *
          fontFactor *
          VertAlignFactor.Size *
          Factor_pt_to_mm *
          0.27 -
        VertAlignFactor.Sub * curTextPr.fontSize! * fontFactor * Factor_pt_to_mm
      yForUnderline -=
        VertAlignFactor.Sub * curTextPr.fontSize! * fontFactor * Factor_pt_to_mm
      break
    }
    case VertAlignType.SuperScript: {
      yForStrikeout +=
        -curTextPr.fontSize! *
          fontFactor *
          VertAlignFactor.Size *
          Factor_pt_to_mm *
          0.27 -
        VertAlignFactor.Super *
          curTextPr.fontSize! *
          fontFactor *
          Factor_pt_to_mm
      break
    }
  }
  return {
    yForStrikeout: yForStrikeout,
    yForUnderline: yForUnderline,
  }
}
function _renderUnderOrStrikeLines_computeColor(
  run: ParaRun,
  curTextPr: TextPr,
  paraRenderStateLns: ParagraphRenderStateLines,
  isVisited: boolean,
  isInLink: boolean
): CHexColor {
  let curColor = new CHexColor(0, 0, 0, false)

  if (
    paraRenderStateLns.drawContext &&
    paraRenderStateLns.drawContext.instanceType !==
      InstanceType.GraphicsBoundsChecker
  ) {
    const bgColor = paraRenderStateLns.bgColor
    const theme = run.paragraph.getTheme()
    const colorMap = run.paragraph.getColorMap()

    let rgba: ColorRGBA
    if (
      (bgColor == null || bgColor.auto) &&
      curTextPr.fontRef &&
      curTextPr.fontRef.color
    ) {
      curTextPr.fontRef.color.check(theme, colorMap)
      rgba = curTextPr.fontRef.color.rgba
    }

    // Setting the stroke color
    if (true === isVisited) {
      gVisitedHLinkColor().check(theme, colorMap)
      rgba = gVisitedHLinkColor().getRGBAColor()
      curColor = new CHexColor(rgba.r, rgba.g, rgba.b, true)
    } else {
      if (isInLink === true) {
        gHLinkColor().check(theme, colorMap)
        rgba = gHLinkColor().getRGBAColor()
        curColor = new CHexColor(rgba.r, rgba.g, rgba.b, true)
      } else if (curTextPr.fillEffects) {
        curTextPr.fillEffects.check(theme, colorMap)
        rgba = curTextPr.fillEffects.getRGBAColor()
        curColor = new CHexColor(rgba.r, rgba.g, rgba.b)
      } else {
        curColor = new CHexColor(0, 0, 0)
      }
    }
  }
  return curColor
}

function _renderUnderOrStrikeLinesForRunElem(
  runElement: RunContentElement,
  paraRenderStateLns: ParagraphRenderStateLines,
  run: ParaRun,
  curTextPr: TextPr,
  runElementIndex: number,
  yForUnderline: number,
  yForStrikeout: number,
  curColor: CHexColor
) {
  let x = paraRenderStateLns.x
  const strikeout = paraRenderStateLns.strikeout
  const underline = paraRenderStateLns.underline
  const drawWidth = (curTextPr.fontSize! / 18) * Factor_pt_to_mm
  const instanceType = runElement.instanceType
  const widthOfItem = getWidthOfRunElement(runElement)
  switch (instanceType) {
    case InstanceType.ParaEnd:
    case InstanceType.ParaNewLine: {
      x += widthOfItem
      break
    }

    case InstanceType.ParaTab:
    case InstanceType.ParaText: {
      if (
        InstanceType.ParaText === instanceType &&
        null != run.compositeInputInfo &&
        runElementIndex >= run.compositeInputInfo.index &&
        runElementIndex <
          run.compositeInputInfo.index + run.compositeInputInfo.length
      ) {
        underline.add(
          yForUnderline,
          yForUnderline,
          x,
          x + widthOfItem,
          drawWidth,
          curColor.r,
          curColor.g,
          curColor.b,
          curTextPr
        )
      }

      if (true === curTextPr.strikeout) {
        strikeout.add(
          yForStrikeout,
          yForStrikeout,
          x,
          x + widthOfItem,
          drawWidth,
          curColor.r,
          curColor.g,
          curColor.b,
          curTextPr
        )
      }

      if (true === curTextPr.underline) {
        underline.add(
          yForUnderline,
          yForUnderline,
          x,
          x + widthOfItem,
          drawWidth,
          curColor.r,
          curColor.g,
          curColor.b,
          curTextPr
        )
      }

      x += widthOfItem

      break
    }
    case InstanceType.ParaSpace: {
      if (true === curTextPr.strikeout) {
        strikeout.add(
          yForStrikeout,
          yForStrikeout,
          x,
          x + widthOfItem,
          drawWidth,
          curColor.r,
          curColor.g,
          curColor.b,
          curTextPr
        )
      }

      if (true === curTextPr.underline) {
        underline.add(
          yForUnderline,
          yForUnderline,
          x,
          x + widthOfItem,
          drawWidth,
          curColor.r,
          curColor.g,
          curColor.b,
          curTextPr
        )
      }

      x += widthOfItem

      break
    }
  }

  paraRenderStateLns.x = x
}

function renderFragmentByParams(
  textElemArr: ManagedSlice<ParaText>,
  paraRenderStateElements: ParagraphRenderStateElements,
  parentInfo: RunAndHyperlinkInfo,
  isReverseChars: boolean
) {
  if (textElemArr.length <= 0) return
  const textPr = parentInfo.run.getCalcedTextPr(false)
  const paraText = textElemArr.at(0)!

  let text = ''
  let drawWidth = 0

  const reducer = (paraText: ParaText) => {
    // compute text
    const c = String.fromCodePoint(paraText.value)
    text += c
    // compute drawWidth
    drawWidth += paraText.getVisibleWidth()
  }

  if (isReverseChars) {
    ManagedSliceUtil.reverseEach(textElemArr, reducer)
  } else {
    ManagedSliceUtil.forEach(textElemArr, reducer)
  }

  // console.log('drawFragment:', text)
  // console.log('drawFragmentWidth:', drawWidth)
  const { drawContext: drawContext } = paraRenderStateElements
  if (drawContext == null) {
    return
  }

  let { x, y } = paraRenderStateElements

  switch (textPr.vertAlign) {
    case VertAlignType.SubScript: {
      y -= VertAlignFactor.Sub * textPr.fontSize! * Factor_pt_to_mm
      break
    }
    case VertAlignType.SuperScript: {
      y -= VertAlignFactor.Super * textPr.fontSize! * Factor_pt_to_mm
      break
    }
  }
  const s = setTextDrawingColor(
    drawContext,
    textPr,
    parentInfo,
    paraRenderStateElements
  )

  const fill = textPr.fillEffects

  if (
    fill?.isComplexFill() &&
    drawContext.instanceType === InstanceType.CanvasDrawer
  ) {
    const g = drawContext
    const isTextStrokeSet = g.isTextStrokeSet
    const isTextFillSet = g.isTextFillSet
    const { l, t, w, h } = getLineBounds(paraRenderStateElements)
    const isBgTransparent = g.isBgTransparent === true && s.highlight == null

    g.save()
    g.saveState()
    const ctx = g.context

    g.setCxtTransformReset(false)
    ctx.beginPath()
    ctx.rect(x, t, drawWidth + 2, h)

    // ctx.save()
    // ctx.lineWidth = 0.5
    // ctx.strokeStyle = '#00ff00'
    // ctx.stroke()
    // ctx.restore()

    ctx.clip()
    //
    ctx.fillStyle = '#ffffff'
    ctx.globalCompositeOperation = isBgTransparent ? 'source-over' : 'xor'
    //
    g.isTextStrokeSet = false
    g.isTextFillSet = true
    //
    renderText(text, x, y, drawContext, paraText)
    const fontStyle = ctx.font
    //
    ctx.globalCompositeOperation = isBgTransparent
      ? 'source-in'
      : 'destination-over'

    const drawInfo: ShapeRenderingInfo = {
      brush: fill,
      extX: w + 2,
      extY: h,
      l,
      t,
    }

    drawShapeByInfo(drawInfo, g)
    ctx.globalCompositeOperation = 'source-over'
    g.restoreState()
    g.restore()
    ctx.font = fontStyle

    g.isTextStrokeSet = isTextStrokeSet
    g.isTextFillSet = false
    renderText(text, x, y, drawContext, paraText)
    g.isTextFillSet = isTextFillSet
  } else {
    renderText(text, x, y, drawContext, paraText)
  }
  paraRenderStateElements.x += drawWidth
  restoreDrawContextStateAfterSetTextDrawingColor(drawContext, s)
}

interface SetTextDrawingColorState {
  isPenSet: boolean
  brushColor?: {
    r: number
    g: number
    b: number
    a: number
    auto: CHexColor
  }
  fill?: FillEffects
  lineJoin?: CanvasLineJoin
  highlight?: CComplexColor
}

function setTextDrawingColor(
  drawContext: TextContentDrawContext,
  textPr: TextPr,
  parentInfo: RunAndHyperlinkInfo,
  paraRenderStateElements: ParagraphRenderStateElements
): SetTextDrawingColorState {
  let isPenSet = false
  let brushColor: undefined | TextDrawingColorInfo['brushColor'],
    fill: undefined | FillEffects,
    lineJoin: undefined | CanvasLineJoin,
    highlight: undefined | CComplexColor
  if (drawContext.instanceType !== InstanceType.GraphicsBoundsChecker) {
    lineJoin = drawContext.context.lineJoin
    isPenSet = drawContext.isTextStrokeSet
    drawContext.setTextPr(textPr)
    const { bgColor, colorMap, theme } = paraRenderStateElements
    const textColorInfo = gTextBrushGetter.get(
      textPr,
      parentInfo,
      bgColor,
      colorMap,
      theme
    )

    if (textColorInfo) {
      brushColor = textColorInfo.brushColor
      fill = textColorInfo.fill
      highlight = textColorInfo.highlight
      drawContext.setBrushColor(
        brushColor.r,
        brushColor.g,
        brushColor.b,
        brushColor.a
      )

      const ln = textColorInfo.ln
      if (ln != null) {
        drawContext.setPenColor(ln.r, ln.g, ln.b, ln.a)
        const lineWidth = enlargeByRetinaRatio(ln.w * Factor_mm_to_pix)

        drawContext.context.lineWidth = lineWidth
        setPenDash(ln.prstDash, drawContext, lineWidth)

        if (ln.joinType != null) {
          switch (ln.joinType) {
            case LineJoinType.Round: {
              drawContext.setLineJoin('round')
              break
            }
            case LineJoinType.Bevel: {
              drawContext.setLineJoin('bevel')
              break
            }
            case LineJoinType.Empty: {
              drawContext.setLineJoin('miter')
              break
            }
            case LineJoinType.Miter: {
              drawContext.setLineJoin('miter')
              break
            }
          }
        }

        drawContext.isTextStrokeSet = true
      }
    }
  }
  return {
    isPenSet,
    brushColor,
    highlight,
    fill,
    lineJoin,
  }
}

function restoreDrawContextStateAfterSetTextDrawingColor(
  drawContext: TextContentDrawContext,
  state: SetTextDrawingColorState
) {
  if (drawContext.instanceType === InstanceType.CanvasDrawer) {
    drawContext.isTextStrokeSet = state.isPenSet
    if (state.lineJoin != null) {
      drawContext.context.lineJoin = state.lineJoin
    }
  }
  state = null as any
}

function renderText(
  text: string,
  x: number,
  y: number,
  context: GraphicsBoundsChecker | CanvasDrawer,
  paraText: ParaText
) {
  if (context.instanceType === InstanceType.GraphicsBoundsChecker) {
    context.fillString(x, y)
    return
  }

  let fontFactor = 1
  if (
    paraText.form & ParaTextForm.FONTKOEF_SCRIPT &&
    paraText.form & ParaTextForm.FONTKOEF_SMALLCAPS
  ) {
    fontFactor = smallcaps_and_script_factor
  } else if (paraText.form & ParaTextForm.FONTKOEF_SCRIPT) {
    fontFactor = VertAlignFactor.Size
  } else if (paraText.form & ParaTextForm.FONTKOEF_SMALLCAPS) {
    fontFactor = smallcaps_Factor
  }

  EditorUtil.FontKit.setFontClass(
    ((paraText.form >> 8) & 0xff) as FontClassValues,
    fontFactor
  )

  const textToRender =
    paraText.form & ParaTextForm.CAPITALS ? text.toUpperCase() : text

  context.fillString(x, y, textToRender)
}
