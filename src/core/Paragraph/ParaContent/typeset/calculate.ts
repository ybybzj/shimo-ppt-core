import { Nullable } from '../../../../../liber/pervasive'
import { LineHeightRule } from '../../../common/const/attrs'
import { convertMMToTwips } from '../../../../common/utils'
import { EditorUtil } from '../../../../globals/editor'
import { InstanceType } from '../../../instanceTypes'
import {
  compareIndices,
  decreaseRunElementIndices,
  eachRunElementBetween,
  getRunAndHyperlinkInfo,
  getRunElementIndexFromIndices,
  getRunElementIndicesByElement,
  RunAndHyperlinkInfo,
} from '../../../../re/export/textContent'
import {
  ParaTextForm,
  TEXTWIDTH_PER_UNIT,
} from '../../../common/const/paraContent'
import { TextTabAlign } from '../../../common/const/paragraph'
import { NumberingFormat } from '../../../common/numberingFormat'
import { ParaPr } from '../../../textAttributes/ParaPr'
import { DefaultTabStop, ParaTabStop } from '../../../textAttributes/ParaTab'
import { LEVEL_PARAHYPERLINK, ParagraphElementPosition } from '../../common'
import { Paragraph } from '../../Paragraph'
import { RunContentElement } from '../../RunElement/type'
import {
  ParagraphCalcState,
  ParagraphCalcAlignState,
  ParagraphCalcCounterState,
  ParagraphCalcTabInfo,
} from '../../typeset/ParagraphCalcState'
import { ParaHyperlink } from '../ParaHyperlink'
import { ParaRun } from '../ParaRun'
import {
  collectRunElementsBetweenPoses,
  measureElementsByRun,
  reMeasureContent,
  reshapeRunItems,
} from './measure'
import { ParagraphItem } from '../../type'
import { canPrependNumbering } from '../../RunElement/utils'
import { getTextDocStartPosInfo } from '../../../TextDocument/utils/typeset'
import { LineSegmentIndexesUtil } from '../LineSegmentIndexes'
import { ManagedArray } from '../../../../common/managedArray'
import { WordWidthCalc } from './utils'

export function calcNumbering(
  paraCalcState: ParagraphCalcState,
  item: RunContentElement,
  run: ParaRun,
  paraPr: ParaPr,
  x: number
) {
  const curLine = paraCalcState.curLineIndex
  const colIndex = paraCalcState.curColumnIndex
  const paragraph = paraCalcState.paragraph!
  const lineAscent = paraCalcState.lineAscent
  const numbering = paragraph.paraNumbering
  const level = paragraph.pr.lvl ?? 0

  let bulletNum = 0
  if (numbering.getType() >= NumberingFormat.arabicPeriod) {
    let prev = paragraph.prev
    while (null != prev) {
      const orgLevel = prev.pr.lvl ?? 0
      const prevNumbering = prev.getNumbering()

      if (level < orgLevel) {
        prev = prev.prev
        continue
      } else if (level > orgLevel) break
      else if (
        prevNumbering.getType() === numbering.getType() &&
        prevNumbering.getStartAt() === prevNumbering.getStartAt()
      ) {
        if (true !== prev.isEmpty()) bulletNum++

        prev = prev.prev
      } else break
    }
  }

  const textPrOfFirstRun = paragraph.getTextPrForFirstRun()

  numbering.bulletNum = bulletNum + 1
  numbering.measure(EditorUtil.FontKit, textPrOfFirstRun)

  if (NumberingFormat.None !== numbering.getType()) {
    const column = paragraph.renderingState.columns.at(colIndex)
    if (column) {
      if (paraPr.ind.firstLine! < 0) {
        numbering.renderWidth = Math.max(
          numbering.width,
          column.x + paraPr.ind.left! + paraPr.ind.firstLine! - x,
          column.x + paraPr.ind.left! - x
        )
      } else {
        numbering.renderWidth = Math.max(
          column.x + paraPr.ind.left! + numbering.width - x,
          column.x + paraPr.ind.left! + paraPr.ind.firstLine! - x,
          column.x + paraPr.ind.left! - x
        )
      }
    }
  }

  x += numbering.renderWidth

  numbering.item = item
  numbering.run = run
  numbering.line = curLine
  numbering.lineAscent = lineAscent
  return x
}

export function resetParaItemCalcStatus(
  paraItem: ParagraphItem,
  startLine: number
) {
  LineSegmentIndexesUtil.resetOrCreate(paraItem.lineSegmentIndexes)
  paraItem.startParaLine = startLine
}

export function calcParaItemSegment(
  paraItem: ParagraphItem,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr
) {
  switch (paraItem.instanceType) {
    case InstanceType.ParaHyperlink: {
      calcParaHyperlinkSegment(paraItem, paraCalcState, paraPr)
      break
    }
    case InstanceType.ParaRun: {
      calcParaRunSegment(paraItem, paraCalcState, paraPr)
      break
    }
  }
}

function calcParaHyperlinkSegment(
  paraHyperlink: ParaHyperlink,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr
) {
  if (paraHyperlink.paragraph !== paraCalcState.paragraph) {
    paraHyperlink.paragraph = paraCalcState.paragraph
  }

  const curLine = paraCalcState.curLineIndex - paraHyperlink.startParaLine
  const segmentStartPos = LineSegmentIndexesUtil.resetToLine(
    paraHyperlink.lineSegmentIndexes,
    curLine
  )
  let segmentEndIndex = 0

  const contentLen = paraHyperlink.children.length
  let pos = Math.max(segmentStartPos, 0)
  for (; pos < contentLen; pos++) {
    const item = paraHyperlink.children.at(pos)!
    if ((0 === pos && 0 === curLine) || pos !== segmentStartPos) {
      resetParaItemCalcStatus(item, paraCalcState.curLineIndex)
    }

    paraCalcState.updateCursorPos(pos, LEVEL_PARAHYPERLINK)
    calcParaRunSegment(item, paraCalcState, paraPr)

    if (true === paraCalcState.isSegmentEnd) {
      segmentEndIndex = pos
      break
    }
  }

  if (pos >= contentLen) {
    segmentEndIndex = pos - 1
  }

  LineSegmentIndexesUtil.setIndexesOfLine(
    paraHyperlink.lineSegmentIndexes,
    curLine,
    segmentStartPos,
    segmentEndIndex
  )
}

const _reshapeItemsBefore = new ManagedArray<RunContentElement>()
const _parentInfoArr = new ManagedArray<Nullable<RunAndHyperlinkInfo>>()

function calcParaRunSegment(
  paraRun: ParaRun,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr
) {
  if (paraRun.paragraph !== paraCalcState.paragraph) {
    paraRun.paragraph = paraCalcState.paragraph
    paraRun.calculateInfo.isTextPr = true
  }

  const curLine = paraCalcState.curLineIndex - paraRun.startParaLine

  // at para beginning
  if (0 === curLine) {
    const preCalcInfo = paraCalcState.lastRunCalcInfo

    if (null == preCalcInfo) paraRun.calculateInfo.isAddingNumbering = true
    else {
      paraRun.calculateInfo.isAddingNumbering = preCalcInfo.isAddingNumbering
    }

    paraRun.calculateInfo.numbering = null
  }

  paraCalcState.lastRunCalcInfo = paraRun.calculateInfo

  const segmentStartPos = LineSegmentIndexesUtil.resetToLine(
    paraRun.lineSegmentIndexes,
    curLine
  )

  const hasArabic = reMeasureContent(paraRun, paraCalcState, segmentStartPos)
  let segmentCalcResult = _calcParaRunSegment(
    paraRun,
    paraCalcState,
    paraPr,
    segmentStartPos
  )

  // 是否换行的状态值应该不会变化
  let isSegmentEnd = segmentCalcResult.isSegmentEnd
  let calcCount = 1

  const para = paraRun.paragraph
  while (
    calcCount < 20 &&
    hasArabic &&
    segmentCalcResult.arabicLetterBreakSegmentIndex != null
  ) {
    // 到了这里一定换行，保险起见强制设为true
    isSegmentEnd = true
    //只要有在阿语字符处换行，当前run在计算下一行的时候就得重新measure
    paraRun.calculateInfo.reMeasure = true
    let isFormCodeChanged = false
    // 这里取换行位置的前一个位置作为结束位，重新进行reshape&measure，并重新迭代计算行信息
    let startIndices = paraCalcState.reshapeStartPos
    if (startIndices == null) {
      startIndices = getRunElementIndicesByElement(
        para,
        paraRun,
        segmentStartPos
      )!
    }

    let nextSegmentStartPos = segmentStartPos
    if (startIndices) {
      const endIndices = decreaseRunElementIndices(
        para,
        getRunElementIndicesByElement(
          para,
          paraRun,
          segmentCalcResult.arabicLetterBreakSegmentIndex
        )!
      )

      if (endIndices && compareIndices(endIndices, startIndices) >= 0) {
        _reshapeItemsBefore.reset()
        _parentInfoArr.reset()
        collectRunElementsBetweenPoses(
          para,
          startIndices,
          endIndices,
          _reshapeItemsBefore,
          _parentInfoArr
        )
        reshapeRunItems(_reshapeItemsBefore)
        const textPr = paraRun.getCalcedPr(false)

        measureElementsByRun(
          _reshapeItemsBefore,
          textPr,
          para,
          paraRun.calculateInfo,
          _parentInfoArr,
          (_item) => {
            // console.log(_item)
            isFormCodeChanged = true
          }
        )
        nextSegmentStartPos = getRunElementIndexFromIndices(startIndices)
      }
    }

    if (isFormCodeChanged === false) {
      break
    }
    segmentCalcResult = _calcParaRunSegment(
      paraRun,
      paraCalcState,
      paraPr,
      nextSegmentStartPos
    )
    calcCount += 1
  }

  const {
    needMoveToLineBreak: needMoveToLineBreak,
    isParaEnd: isParaEnd,
    isWordLetter: isWordLetter,
    isStartHandleLetter: isStartHandleLetter,
    isFirstOnLine: isFirstOnLine,
    isEmptyLine: isEmptyLine,
    hasTextOnLine: hasTextOnLine,
    spaceCount: spaceCount,
    wordWidth: wordWidth,
    x,
    xEnd,
    segmentEndIndex,
  } = segmentCalcResult

  paraCalcState.needMoveToLineBreak = needMoveToLineBreak
  paraCalcState.isSegmentEnd = isSegmentEnd
  paraCalcState.isParaEnd = isParaEnd

  paraCalcState.isWordLetter = isWordLetter
  paraCalcState.isStartHandleLetter = isStartHandleLetter
  paraCalcState.isFirstOnLine = isFirstOnLine
  paraCalcState.isEmptyLine = isEmptyLine
  paraCalcState.hasTextOnLine = hasTextOnLine

  paraCalcState.spaceCount = spaceCount
  paraCalcState.wordWidth = wordWidth

  paraCalcState.x = x
  paraCalcState.xEnd = xEnd

  LineSegmentIndexesUtil.setIndexesOfLine(
    paraRun.lineSegmentIndexes,
    curLine,
    segmentStartPos,
    segmentEndIndex
  )
}

const wordWidthCalc = new WordWidthCalc()
function _calcParaRunSegment(
  paraRun: ParaRun,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr,
  segmentStartPos: number
): {
  needMoveToLineBreak: boolean
  isSegmentEnd: boolean
  isParaEnd: boolean
  isWordLetter: boolean
  isStartHandleLetter: boolean
  isFirstOnLine: boolean
  isEmptyLine: boolean
  hasTextOnLine: boolean
  spaceCount: number
  wordWidth: number
  x: number
  xEnd: number
  segmentEndIndex: number
  arabicLetterBreakSegmentIndex: Nullable<number>
} {
  let segmentEndIndex = 0

  let arabicLetterBreakSegmentIndex: Nullable<number> = null
  const para = paraCalcState.paragraph

  let needMoveToLineBreak = paraCalcState.needMoveToLineBreak
  let isSegmentEnd = paraCalcState.isSegmentEnd
  let isParaEnd = paraCalcState.isParaEnd

  let isWordLetter = paraCalcState.isWordLetter
  let isStartHandleLetter = paraCalcState.isStartHandleLetter
  let isFirstOnLine = paraCalcState.isFirstOnLine
  let isEmptyLine = paraCalcState.isEmptyLine
  let hasTextOnLine = paraCalcState.hasTextOnLine

  let spaceCount = paraCalcState.spaceCount
  let wordWidth = paraCalcState.wordWidth

  let x = paraCalcState.x
  const xEnd = paraCalcState.xEnd

  let pos = segmentStartPos

  const contentLength = paraRun.children.length

  const startIndices = paraCalcState.reshapeStartPos

  // 因为reshape的缘故，measure数据有变化，需要重新从标记位置开始测量换行信息
  if (paraCalcState.reshapeStartState != null) {
    x = paraCalcState.reshapeStartState.x
    isWordLetter = paraCalcState.reshapeStartState.isWordLetter
    wordWidth = paraCalcState.reshapeStartState.wordWidth
    isSegmentEnd = paraCalcState.reshapeStartState.isSegmentEnd
    needMoveToLineBreak = paraCalcState.reshapeStartState.needMoveToLineBreak
    isStartHandleLetter = paraCalcState.reshapeStartState.isStartHandleLetter
    isFirstOnLine = paraCalcState.reshapeStartState.isFirstOnLine
    isEmptyLine = paraCalcState.reshapeStartState.isEmptyLine
    hasTextOnLine = paraCalcState.reshapeStartState.hasTextOnLine
    spaceCount = paraCalcState.reshapeStartState.spaceCount
    isParaEnd = paraCalcState.reshapeStartState.isParaEnd
  }

  // 仅仅当reshape的文本范围包括当前run的前面的run的内容时，快速把之前的measure宽度计算进去
  if (pos === 0 && startIndices != null) {
    const startRun = getRunAndHyperlinkInfo(para, startIndices)
    if (startRun != null && startRun.run !== paraRun) {
      const endIndices = decreaseRunElementIndices(
        para,
        getRunElementIndicesByElement(para, paraRun, 0)!
      )
      if (endIndices && compareIndices(endIndices, startIndices) >= 0) {
        eachRunElementBetween(para, startIndices, endIndices, (item) => {
          if (item.instanceType === InstanceType.ParaText) {
            isStartHandleLetter = true
            const letterWidth = item.width / TEXTWIDTH_PER_UNIT

            if (true !== isWordLetter) {
              if (true !== isSegmentEnd) {
                isWordLetter = true
                wordWidth = letterWidth
              }
            } else {
              if (true !== isSegmentEnd) {
                wordWidth += letterWidth
              }
            }
          }
        })
      }
    }
  }

  let preWordWidth: number = 0
  for (; pos < contentLength; pos++) {
    const item = paraRun.children.at(pos)!
    const instanceType = item.instanceType

    if (
      true === paraRun.calculateInfo.isAddingNumbering &&
      true === canPrependNumbering(item)
    ) {
      x = calcNumberingAndUpdateCalcInfo(
        paraRun,
        paraCalcState,
        item,
        paraPr,
        x
      )
    }
    switch (instanceType) {
      case InstanceType.ParaText: {
        if (item.isSkipped()) {
          break
        }
        const isArabic = item.isArabic()
        if (!isArabic) {
          paraCalcState.clearReshapeStartPos()
        }
        if (isArabic) {
          if (paraCalcState.reshapeStartPos == null) {
            paraCalcState.updateReshapeStartPos(pos, {
              x: x,
              isSegmentEnd: isSegmentEnd,
              isWordLetter: isWordLetter,
              wordWidth: wordWidth,
              needMoveToLineBreak: needMoveToLineBreak,
              isStartHandleLetter: isStartHandleLetter,
              isFirstOnLine: isFirstOnLine,
              isEmptyLine: isEmptyLine,
              hasTextOnLine: hasTextOnLine,
              spaceCount: spaceCount,
              isParaEnd: isParaEnd,
            })
          }
          if (paraCalcState.lineArabicIndices == null) {
            paraCalcState.setArabicIndicesByRunElementPos(pos)
          }
        }

        isStartHandleLetter = true
        const canBeAtStart = item.canBeLineStart()
        let letterWidth = item.width / TEXTWIDTH_PER_UNIT

        if (true !== isWordLetter) {
          if (true !== isFirstOnLine) {
            // 行尾是标点符号(不能出现在行首)的，且符号首部在行范围内的，不换行
            const xEndDelta =
              x + spaceCount <= xEnd && x + spaceCount + letterWidth > xEnd
                ? canBeAtStart === false
                  ? letterWidth
                  : 0
                : 0
            if (x + spaceCount + letterWidth > xEnd + xEndDelta) {
              if (!canBeAtStart && !paraCalcState.isLineBreakFirst) {
                needMoveToLineBreak = true
                isSegmentEnd = true
              } else {
                isSegmentEnd = true
                segmentEndIndex = pos
              }
              arabicLetterBreakSegmentIndex = pos
            }
          }

          if (true !== isSegmentEnd) {
            if (paraCalcState.isLineBreakFirst && !canBeAtStart) {
              isFirstOnLine = true
              letterWidth = letterWidth + spaceCount
              spaceCount = 0
            } else if (canBeAtStart) {
              paraCalcState.updateLineBreakPosition(pos, isFirstOnLine)
            }

            if (item.form & ParaTextForm.SPACEAFTER || item.isPunctuation()) {
              x += spaceCount + letterWidth

              isWordLetter = false
              isFirstOnLine = false
              isEmptyLine = false
              hasTextOnLine = true
              spaceCount = 0

              wordWidth = wordWidthCalc.reset()
            } else {
              isWordLetter = true
              wordWidth = wordWidthCalc.assign(item, paraRun)
            }
          }
        } else {
          // 行尾是标点符号(不能出现在行首)的，且符号首部在行范围内的，不换行
          const xEndDelta =
            x + spaceCount + wordWidth <= xEnd &&
            x + spaceCount + wordWidth + letterWidth > xEnd
              ? canBeAtStart === false
                ? letterWidth
                : 0
              : 0
          let needEndSegment =
            x + spaceCount + wordWidth + letterWidth > xEnd + xEndDelta
          if (needEndSegment) {
            const amendWidth = wordWidthCalc.peekAmendWidth(item, paraRun)
            needEndSegment = x + spaceCount + amendWidth > xEnd + xEndDelta
          }
          // word结尾处的符号后面紧接着的字符出发换行时，需要连接着之前的符号一起换行
          if (xEndDelta > 0) preWordWidth = wordWidth
          if (needEndSegment) {
            isSegmentEnd = true
            const isEndWithPreLetter = preWordWidth > 0

            let breakPos = pos

            if (true === isFirstOnLine) {
              isEmptyLine = false
              hasTextOnLine = true
              x += isEndWithPreLetter ? preWordWidth : wordWidth

              isSegmentEnd = true
              breakPos = isEndWithPreLetter ? pos - 1 : pos
              segmentEndIndex = breakPos
            } else {
              needMoveToLineBreak = true
            }
            arabicLetterBreakSegmentIndex = breakPos
            // reset preWordWidth when segment break
            preWordWidth = 0
          }

          if (true !== isSegmentEnd) {
            wordWidth = wordWidthCalc.update(item, paraRun)

            if (item.form & ParaTextForm.SPACEAFTER) {
              x += spaceCount + wordWidthCalc.amendWidth

              isWordLetter = false
              isFirstOnLine = false
              isEmptyLine = false
              hasTextOnLine = true
              spaceCount = 0
              wordWidth = wordWidthCalc.reset()
            }
          }
        }

        break
      }

      case InstanceType.ParaSpace: {
        const letterWidth = item.width / TEXTWIDTH_PER_UNIT
        if (
          isWordLetter &&
          paraCalcState.lastParaItem &&
          InstanceType.ParaText === paraCalcState.lastParaItem.instanceType &&
          !paraCalcState.lastParaItem.canBeLineEnd()
        ) {
          wordWidth = wordWidthCalc.updateWidth(letterWidth)
          break
        }

        isFirstOnLine = false

        if (true === isWordLetter) {
          x += spaceCount + wordWidthCalc.amendWidth

          isWordLetter = false
          isEmptyLine = false
          hasTextOnLine = true
          spaceCount = 0
          wordWidth = wordWidthCalc.reset()
        }

        spaceCount += letterWidth
        break
      }

      case InstanceType.ParaTab: {
        x = calcLastTab(
          paraCalcState.lastTabInfo,
          x,
          xEnd,
          isWordLetter,
          wordWidth,
          spaceCount
        )

        x += spaceCount + wordWidthCalc.amendWidth
        isWordLetter = false
        spaceCount = 0
        wordWidth = wordWidthCalc.reset()

        const tabPos = calcTabPos(para, x, paraPr)
        const newX = tabPos.newX
        const tabAlign = tabPos.tabAlign

        paraCalcState.lastTabInfo.tabPosIndex = newX
        paraCalcState.lastTabInfo.value = tabAlign
        paraCalcState.lastTabInfo.x = x
        paraCalcState.lastTabInfo.tab = item
        paraCalcState.lastTabInfo.isAtRightEdge = tabPos.isTabAtRightEdge
        paraCalcState.lastTabInfo.xLimit = tabPos.xLimit

        if (TextTabAlign.Left !== tabAlign) {
          item.width = 0
          item.renderWidth = 0
        } else {
          const twipXEnd = convertMMToTwips(xEnd)
          const twipNewX = convertMMToTwips(newX)

          if (
            false === isFirstOnLine &&
            ((tabPos.defaultTab && twipNewX > twipXEnd) ||
              (!tabPos.defaultTab &&
                twipNewX > convertMMToTwips(tabPos.xLimit)))
          ) {
            wordWidth = newX - x
            segmentEndIndex = pos
            isSegmentEnd = true
          } else {
            item.width = newX - x
            item.renderWidth = newX - x

            x = newX
          }
        }

        paraCalcState.updateLineBreakPosition(pos, isFirstOnLine)

        // if (0 === curSegIndex) {
        if (true === isStartHandleLetter) {
          isFirstOnLine = false
          isEmptyLine = false
          hasTextOnLine = true
        }
        // }

        isStartHandleLetter = true
        isWordLetter = true

        break
      }
      case InstanceType.ParaNewLine: {
        x = calcLastTab(
          paraCalcState.lastTabInfo,
          x,
          xEnd,
          isWordLetter,
          wordWidth,
          spaceCount
        )

        x += wordWidthCalc.amendWidth

        if (true === isWordLetter) {
          isEmptyLine = false
          hasTextOnLine = true
          isWordLetter = false
          x += spaceCount
          spaceCount = 0
        }

        isSegmentEnd = true
        isEmptyLine = false
        hasTextOnLine = true

        segmentEndIndex = pos + 1

        break
      }
      case InstanceType.ParaEnd: {
        if (true === isWordLetter) {
          isFirstOnLine = false
          isEmptyLine = false
          hasTextOnLine = true
        }

        x += wordWidthCalc.amendWidth

        if (true === isWordLetter) {
          x += spaceCount
          spaceCount = 0
          wordWidth = wordWidthCalc.reset()
        }

        x = calcLastTab(
          paraCalcState.lastTabInfo,
          x,
          xEnd,
          isWordLetter,
          wordWidth,
          spaceCount
        )

        isSegmentEnd = true
        isParaEnd = true

        segmentEndIndex = pos + 1

        break
      }
    }

    if (InstanceType.ParaSpace !== instanceType) {
      paraCalcState.lastParaItem = item
    }

    if (InstanceType.ParaText !== instanceType) {
      paraCalcState.clearReshapeStartPos()
    }

    if (true === isSegmentEnd) {
      wordWidthCalc.reset()
      break
    }
  }

  if (pos >= contentLength) {
    segmentEndIndex = contentLength
  }

  return {
    needMoveToLineBreak: needMoveToLineBreak,
    isSegmentEnd: isSegmentEnd,
    isParaEnd: isParaEnd,
    isWordLetter: isWordLetter,
    isStartHandleLetter: isStartHandleLetter,
    isFirstOnLine: isFirstOnLine,
    isEmptyLine: isEmptyLine,
    hasTextOnLine: hasTextOnLine,
    spaceCount: spaceCount,
    wordWidth: wordWidth,
    x: x,
    xEnd: xEnd,
    segmentEndIndex,
    arabicLetterBreakSegmentIndex: arabicLetterBreakSegmentIndex,
  }
}

function calcNumberingAndUpdateCalcInfo(
  paraRun: ParaRun,
  paraCalcState: ParagraphCalcState,
  item: RunContentElement,
  paraPr: ParaPr,
  x: number
) {
  x = calcNumbering(paraCalcState, item, paraRun, paraPr, x)

  paraRun.calculateInfo.isAddingNumbering = false
  paraRun.calculateInfo.numbering = paraCalcState.paragraph.paraNumbering

  return x
}
function calcLastTab(
  lastTab: ParagraphCalcTabInfo,
  x: number,
  xEnd: number,
  word: boolean,
  wordLen: number,
  spaceLen: number
) {
  if (TextTabAlign.Left === lastTab.value) {
    lastTab.reset()
  } else if (-1 !== lastTab.value) {
    let tmpX = x

    if (true === word || wordLen > 0) tmpX += spaceLen + wordLen

    const tabItem = lastTab.tab!
    const startX = lastTab.x
    const tabSegmentW = tmpX - startX
    let tabAlign = lastTab.value
    let tabPos = lastTab.tabPosIndex

    if (convertMMToTwips(tabPos) > convertMMToTwips(xEnd)) {
      tabAlign = TextTabAlign.Right
      tabPos = xEnd
    }

    let calcWidth = 0
    if (TextTabAlign.Right === tabAlign) {
      calcWidth = Math.max(tabPos - (startX + tabSegmentW), 0)
    } else if (TextTabAlign.Center === tabAlign) {
      calcWidth = Math.max(tabPos - (startX + tabSegmentW / 2), 0)
    }

    if (lastTab.xLimit != null && x + calcWidth > lastTab.xLimit) {
      calcWidth = lastTab.xLimit - x
    }

    tabItem.width = calcWidth
    tabItem.renderWidth = calcWidth

    lastTab.reset()

    return x + calcWidth
  }

  return x
}

function calcTabPos(para: Paragraph, x: number, paraPr: ParaPr) {
  const absColumnIndex = para.getIndexOfStartColumn() + 0
  const startPosInfo = getTextDocStartPosInfo(para.parent, absColumnIndex)

  let tabsCount = paraPr.tabs!.getCount()

  const tabStyles: ParaTabStop[] = []
  let isCheckLeft = true
  for (let index = 0; index < tabsCount; index++) {
    const tab = paraPr.tabs!.get(index)
    // Todo, handle !
    const tabPos = tab.index! + startPosInfo.x

    if (true === isCheckLeft && tabPos > startPosInfo.x + paraPr.ind.left!) {
      tabStyles.push(new ParaTabStop(TextTabAlign.Left, paraPr.ind.left))
      isCheckLeft = false
    }

    if (TextTabAlign.Clear !== tab.value) tabStyles.push(tab)
  }

  if (true === isCheckLeft) {
    tabStyles.push(new ParaTabStop(TextTabAlign.Left, paraPr.ind.left))
  }

  tabsCount = tabStyles.length

  let tab: Nullable<ParaTabStop>
  for (let index = 0; index < tabsCount; index++) {
    const _tab = tabStyles[index]

    const xTwips = convertMMToTwips(x)
    const tabPosTwips = convertMMToTwips(_tab.index! + startPosInfo.x)

    if (xTwips < tabPosTwips) {
      tab = _tab
      break
    }
  }

  let tabToRight = false

  let newX = 0

  const defaultTab =
    paraPr.defaultTab != null ? paraPr.defaultTab : DefaultTabStop
  if (null == tab) {
    // Todo, handle !
    if (x < startPosInfo.x + paraPr.ind.left!) {
      newX = startPosInfo.x + paraPr.ind.left!
    } else if (defaultTab < 0.001) {
      newX = x
    } else {
      newX = startPosInfo.x
      while (x >= newX - 0.001) newX += defaultTab
    }

    const xTwips = convertMMToTwips(x)
    const endPosTwips = convertMMToTwips(startPosInfo.xLimit)
    const newXTwips = convertMMToTwips(newX)

    if (
      xTwips < endPosTwips &&
      newXTwips >= endPosTwips &&
      convertMMToTwips(paraPr.ind.right) <= 0
    ) {
      newX = startPosInfo.xLimit
      tabToRight = true
    }
  } else {
    newX = tab.index! + startPosInfo.x
  }

  return {
    newX: newX,
    tabAlign: tab ? tab.value! : TextTabAlign.Left,
    defaultTab: tab ? false : true,
    isTabAtRightEdge: tabToRight,
    xLimit: startPosInfo.xLimit,
  }
}

export function calcParaItemSegmentWidth(
  paraItem: ParagraphItem,
  paraCalcCounterState: ParagraphCalcCounterState,
  lineIndex: number
) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    const curLine = lineIndex - paraItem.startParaLine

    const lineSegmentIndexes = paraItem.lineSegmentIndexes
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      curLine
    )
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      curLine
    )

    for (let i = startIndex; i <= endIndex; i++) {
      const run = paraItem.children.at(i)
      if (run) calcParaRunSegmentWidth(run, paraCalcCounterState, lineIndex)
    }
  } else {
    calcParaRunSegmentWidth(paraItem, paraCalcCounterState, lineIndex)
  }
}
const wordWidthCalcForSegmentWidth = new WordWidthCalc()
function calcParaRunSegmentWidth(
  paraRun: ParaRun,
  paraCalcCounterState: ParagraphCalcCounterState,
  curLine: number
) {
  curLine = curLine - paraRun.startParaLine
  const lineSegmentIndexes = paraRun.lineSegmentIndexes
  const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
    lineSegmentIndexes,
    curLine
  )
  const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
    lineSegmentIndexes,
    curLine
  )

  for (let i = startIndex; i < endIndex; i++) {
    const item = paraRun.children.at(i)
    if (item == null) continue
    const instanceType = item.instanceType

    switch (instanceType) {
      case InstanceType.ParaText: {
        paraCalcCounterState.letterCount++

        if (true !== paraCalcCounterState.isInWord) {
          paraCalcCounterState.isInWord = true
          paraCalcCounterState.amountOfWords++
        }

        wordWidthCalcForSegmentWidth.update(item, paraRun)
        paraCalcCounterState.lineSegment!.w += paraCalcCounterState.spaceWidth

        paraCalcCounterState.spaceWidth = 0

        if (paraCalcCounterState.amountOfWords > 1) {
          paraCalcCounterState.spaceTotalCount +=
            paraCalcCounterState.amountOfSpaces
        } else {
          paraCalcCounterState.skipSpaceCount +=
            paraCalcCounterState.amountOfSpaces
        }

        paraCalcCounterState.amountOfSpaces = 0

        if (item.form & ParaTextForm.SPACEAFTER) {
          paraCalcCounterState.lineSegment!.w +=
            wordWidthCalcForSegmentWidth.amendWidth
          wordWidthCalcForSegmentWidth.reset()
          paraCalcCounterState.isInWord = false
        }

        break
      }
      case InstanceType.ParaSpace: {
        if (true === paraCalcCounterState.isInWord) {
          paraCalcCounterState.isInWord = false
          paraCalcCounterState.amountOfSpaces = 1
          paraCalcCounterState.spaceWidth = item.width / TEXTWIDTH_PER_UNIT
          paraCalcCounterState.lineSegment!.w +=
            wordWidthCalcForSegmentWidth.amendWidth
          wordWidthCalcForSegmentWidth.reset()
        } else {
          paraCalcCounterState.amountOfSpaces++
          paraCalcCounterState.spaceWidth += item.width / TEXTWIDTH_PER_UNIT
        }

        break
      }
      case InstanceType.ParaTab: {
        if (true === paraCalcCounterState.isInWord) {
          paraCalcCounterState.lineSegment!.w +=
            wordWidthCalcForSegmentWidth.amendWidth
          wordWidthCalcForSegmentWidth.reset()
        }
        paraCalcCounterState.lineSegment!.w += item.getWidth()
        paraCalcCounterState.lineSegment!.w += paraCalcCounterState.spaceWidth

        paraCalcCounterState.skipLetterCount += paraCalcCounterState.letterCount
        paraCalcCounterState.skipSpaceCount +=
          paraCalcCounterState.spaceTotalCount

        paraCalcCounterState.amountOfWords = 0
        paraCalcCounterState.spaceTotalCount = 0
        paraCalcCounterState.letterCount = 0

        paraCalcCounterState.spaceWidth = 0
        paraCalcCounterState.amountOfSpaces = 0
        paraCalcCounterState.isInWord = false

        break
      }

      case InstanceType.ParaNewLine: {
        if (true === paraCalcCounterState.isInWord) {
          paraCalcCounterState.lineSegment!.w +=
            wordWidthCalcForSegmentWidth.amendWidth
          wordWidthCalcForSegmentWidth.reset()

          if (paraCalcCounterState.amountOfWords > 1) {
            paraCalcCounterState.spaceTotalCount +=
              paraCalcCounterState.amountOfSpaces
          }
        }

        paraCalcCounterState.amountOfSpaces = 0
        paraCalcCounterState.isInWord = false

        paraCalcCounterState.lineSegment!.lineBreakWidth =
          item.getVisibleWidth()

        paraCalcCounterState.lineSegment!.w += paraCalcCounterState.spaceWidth
        paraCalcCounterState.spaceWidth = 0

        break
      }
      case InstanceType.ParaEnd: {
        if (true === paraCalcCounterState.isInWord) {
          paraCalcCounterState.lineSegment!.w +=
            wordWidthCalcForSegmentWidth.amendWidth
          wordWidthCalcForSegmentWidth.reset()

          paraCalcCounterState.spaceTotalCount +=
            paraCalcCounterState.amountOfSpaces
        }

        paraCalcCounterState.lineSegment!.paraEndWidth = item.getVisibleWidth()

        paraCalcCounterState.lineSegment!.w += paraCalcCounterState.spaceWidth
        paraCalcCounterState.spaceWidth = 0

        break
      }
    }
  }

  if (true === paraCalcCounterState.isInWord) {
    paraCalcCounterState.lineSegment!.w +=
      wordWidthCalcForSegmentWidth.amendWidth
    wordWidthCalcForSegmentWidth.reset()
  }
}

export function calcParaItemSegmentSpaces(
  paraItem: ParagraphItem,
  paragraphCalcAlignState: ParagraphCalcAlignState,
  lineIndex: number
) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    const curLine = lineIndex - paraItem.startParaLine
    const lineSegmentIndexes = paraItem.lineSegmentIndexes
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      curLine
    )
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      curLine
    )

    for (let i = startIndex; i <= endIndex; i++) {
      const run = paraItem.children.at(i)
      if (run) calcParaRunSegmentSpaces(run, paragraphCalcAlignState, lineIndex)
    }
  } else {
    calcParaRunSegmentSpaces(paraItem, paragraphCalcAlignState, lineIndex)
  }
}

function calcParaRunSegmentSpaces(
  run: ParaRun,
  paragraphCalcAlignState: ParagraphCalcAlignState,
  curLine: number
) {
  curLine = curLine - run.startParaLine
  const lineSegmentIndexes = run.lineSegmentIndexes
  const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
    lineSegmentIndexes,
    curLine
  )
  const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
    lineSegmentIndexes,
    curLine
  )

  for (let i = startIndex; i < endIndex; i++) {
    const item = run.children.at(i)
    if (item == null) continue
    const instanceType = item.instanceType

    switch (instanceType) {
      case InstanceType.ParaText: {
        let renderWidth = 0

        if (0 !== paragraphCalcAlignState.skipLetterCount) {
          renderWidth = item.width / TEXTWIDTH_PER_UNIT
          paragraphCalcAlignState.skipLetterCount--
        } else {
          renderWidth =
            item.width / TEXTWIDTH_PER_UNIT +
            paragraphCalcAlignState.justifyWordWidth
        }

        item.renderWidth = (renderWidth * TEXTWIDTH_PER_UNIT) | 0

        paragraphCalcAlignState.x += renderWidth
        paragraphCalcAlignState.lastWidth = renderWidth

        break
      }
      case InstanceType.ParaSpace: {
        let renderWidth = item.width / TEXTWIDTH_PER_UNIT

        if (0 !== paragraphCalcAlignState.skipSpaceCount) {
          paragraphCalcAlignState.skipSpaceCount--
        } else if (0 !== paragraphCalcAlignState.spaceCount) {
          renderWidth += paragraphCalcAlignState.justifySpaceWidth
          paragraphCalcAlignState.spaceCount--
        }

        item.renderWidth = (renderWidth * TEXTWIDTH_PER_UNIT) | 0

        paragraphCalcAlignState.x += renderWidth
        paragraphCalcAlignState.lastWidth = renderWidth

        break
      }

      case InstanceType.ParaTab: {
        paragraphCalcAlignState.x += item.renderWidth

        break
      }
      case InstanceType.ParaEnd: {
        paragraphCalcAlignState.x += item.getWidth()

        break
      }
      case InstanceType.ParaNewLine: {
        paragraphCalcAlignState.x += item.renderWidth

        break
      }
    }
  }
}

export function calcAndSetParaItemSegmentEndPos(
  paraItem: ParagraphItem,
  paraCalcState: ParagraphCalcState,
  paraElementPos: ParagraphElementPosition,
  lvl: number
) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    const curLine = paraCalcState.curLineIndex - paraItem.startParaLine

    const index = paraElementPos.get(lvl)

    LineSegmentIndexesUtil.setEndIndexOfLine(
      paraItem.lineSegmentIndexes,
      curLine,
      index
    )

    const run = paraItem.children.at(index)
    if (run) {
      calcAndSetParaRunSegmentEndPos(
        run,
        paraCalcState,
        paraElementPos,
        lvl + 1
      )
    }
  } else {
    calcAndSetParaRunSegmentEndPos(paraItem, paraCalcState, paraElementPos, lvl)
  }
}

function calcAndSetParaRunSegmentEndPos(
  run: ParaRun,
  paraCalcState: ParagraphCalcState,
  paraElementPos: ParagraphElementPosition,
  lvl: number
) {
  const curLine = paraCalcState.curLineIndex - run.startParaLine

  const index = paraElementPos.get(lvl)

  LineSegmentIndexesUtil.setEndIndexOfLine(
    run.lineSegmentIndexes,
    curLine,
    index
  )
}

export function calcLineMetricsForParaItem(
  paraItem: ParagraphItem,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr,
  curLine: number
) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    const curLineForHyperlink = curLine - paraItem.startParaLine

    const lineSegmentIndexes = paraItem.lineSegmentIndexes
    const startPosIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      curLineForHyperlink
    )
    const endPosIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      curLineForHyperlink
    )

    for (let i = startPosIndex; i <= endPosIndex; i++) {
      const run = paraItem.children.at(i)
      if (run) calcLineMetricsForParaRun(run, paraCalcState, paraPr, curLine)
    }
  } else {
    calcLineMetricsForParaRun(paraItem, paraCalcState, paraPr, curLine)
  }
}

function calcLineMetricsForParaRun(
  run: ParaRun,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr,
  line: number
) {
  const paragraph = paraCalcState.paragraph

  const curLine = line - run.startParaLine
  const lineSegmentIndexes = run.lineSegmentIndexes
  const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
    lineSegmentIndexes,
    curLine
  )
  const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
    lineSegmentIndexes,
    curLine
  )

  let updateMetrics = false
  const lineRule = paraPr.spacing.lineRule

  for (let i = startIndex; i < endIndex; i++) {
    const item = run.children.at(i)
    if (item == null) continue

    if (item === paragraph.paraNumbering.item) {
      paraCalcState.lineAscent = paragraph.paraNumbering.lineAscent!
    }
    if (item.instanceType === InstanceType.ParaText) {
      updateMetrics = true
    }
  }

  if (true === updateMetrics) {
    if (paraCalcState.textAscentOfLine < run.metrics.textAscent) {
      paraCalcState.textAscentOfLine = run.metrics.textAscent
    }

    if (
      paraCalcState.textAscentFromMetricsForLine <
      run.metrics.textAscentOfMetrics
    ) {
      paraCalcState.textAscentFromMetricsForLine =
        run.metrics.textAscentOfMetrics
    }

    if (paraCalcState.textDescentOfLine < run.metrics.textDescent) {
      paraCalcState.textDescentOfLine = run.metrics.textDescent
    }

    if (LineHeightRule.Exact === lineRule) {
      if (paraCalcState.lineAscent < run.metrics.textAscent) {
        paraCalcState.lineAscent = run.metrics.textAscent
      }

      if (paraCalcState.lineDescent < run.metrics.textDescent) {
        paraCalcState.lineDescent = run.metrics.textDescent
      }
    } else {
      if (paraCalcState.lineAscent < run.metrics.textAscent) {
        paraCalcState.lineAscent = run.metrics.textAscent
      }

      if (paraCalcState.lineDescent < run.metrics.textDescent) {
        paraCalcState.lineDescent = run.metrics.textDescent
      }
    }
  }
}
