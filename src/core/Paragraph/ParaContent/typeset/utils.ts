import { FontClass } from '../../../../fonts/const'
import { EditorUtil } from '../../../../globals/editor'
import { TEXTWIDTH_PER_UNIT } from '../../../common/const/paraContent'
import { ParaText } from '../../RunElement/ParaText'
import { ParaRun } from '../ParaRun'

export class WordWidthCalc {
  private _widthToAmend: number = 0
  private _width: number = 0
  private _letterWord: string = ''
  private lastRun?: ParaRun
  get width() {
    return this._width + this._widthToAmend
  }
  get amendWidth() {
    this._amend()
    return this.width
  }

  update(item: ParaText, run: ParaRun) {
    const letterWidth = item.width / TEXTWIDTH_PER_UNIT
    if (item.isLetter && !item.isSkipped() && !item.isComplex) {
      if (run !== this.lastRun) {
        this._amend()
        this.lastRun = run
        this._letterWord = item.char
      } else {
        this._letterWord += item.char
      }

      this._widthToAmend += letterWidth
    } else {
      this._amend()
      this._width += letterWidth
    }

    return this.width
  }

  assign(item: ParaText, run: ParaRun) {
    this.reset()
    return this.update(item, run)
  }

  peekAmendWidth(item: ParaText, run: ParaRun) {
    const letterWidth = item.width / TEXTWIDTH_PER_UNIT
    if (this._letterWord.length > 0 && item.isLetter) {
      if (run !== this.lastRun) {
        const amendWidth = measureLetters(this._letterWord, this.lastRun!)
        return this._width + amendWidth + letterWidth
      } else {
        const amendWidth = measureLetters(this._letterWord + item.char, run)
        return this._width + amendWidth
      }
    } else {
      return this.width + letterWidth
    }
  }
  private _amend() {
    if (this._letterWord.length > 0) {
      const amendWidth = measureLetters(this._letterWord, this.lastRun!)
      this._widthToAmend = 0
      this._width += amendWidth
      this.lastRun = undefined
      this._letterWord = ''
    }
  }

  updateWidth(w: number) {
    this._width += w
    return this.width
  }

  reset() {
    this.lastRun = undefined
    this._widthToAmend = 0
    this._width = 0
    this._letterWord = ''
    return 0
  }
}

function measureLetters(letters: string, run: ParaRun) {
  const textPr = run.getCalcedTextPr(false)
  const fontKit = EditorUtil.FontKit
  fontKit.setTextPr(textPr)
  fontKit.setFontClass(FontClass.ASCII, 1)
  return fontKit.measure(letters)
}
