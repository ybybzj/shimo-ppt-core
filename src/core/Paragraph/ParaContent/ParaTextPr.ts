import { TextPr, TextPrOptions } from '../../textAttributes/TextPr'

import { InstanceType } from '../../instanceTypes'
import { Paragraph } from '../Paragraph'
import { informDocChange } from '../../calculation/informChanges/textContent'
import { idGenerator } from '../../../globals/IdGenerator'
import { updateCalcStatusForTextContentItem } from '../../calculation/updateCalcStatus/textContent'
import { OptionsApplyResult } from '../../textAttributes/type'
import { isObjectsTheSame } from '../../common/helpers'
import { Nullable } from '../../../../liber/pervasive'

export class ParaTextPr {
  readonly instanceType = InstanceType.ParaTextPr
  id: string
  textPr: TextPr
  parent: Paragraph
  constructor(parent: Paragraph, opts?: TextPrOptions) {
    this.id = idGenerator.newId()

    this.textPr = TextPr.new(opts)
    this.parent = parent
  }
  reset(parent?: Paragraph) {
    this.textPr.reset()
    this.parent = parent ?? this.parent
  }

  fromTextPrData(textPr: Nullable<TextPrOptions>) {
    const applyResult = { isApplied: false }
    this.textPr.fromOptions(textPr, applyResult)
    if (applyResult.isApplied) {
      informDocChange(this)
      updateCalcStatusForTextContentItem(this)
    }
  }

  resetTextPr() {
    const applyResult: OptionsApplyResult = { isApplied: false }
    this.textPr.reset(applyResult)
    if (applyResult.isApplied) {
      informDocChange(this)
      updateCalcStatusForTextContentItem(this)
    }
  }

  setFontSize(value: Nullable<number>) {
    if (this.textPr.setFontSize(value)) {
      informDocChange(this)
      updateCalcStatusForTextContentItem(this)
    }
  }

  setTextPr(value: TextPrOptions) {
    if (isObjectsTheSame(this.textPr, value)) {
      return
    }
    this.textPr.reset()
    this.textPr.fromOptions(value)
    informDocChange(this)
    updateCalcStatusForTextContentItem(this)
  }
}
