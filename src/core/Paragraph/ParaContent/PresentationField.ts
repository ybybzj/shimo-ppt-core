import { createGUID, stringFromCharCode, isNumber } from '../../../common/utils'
import { EditorUtil } from '../../../globals/editor'
import { ParaRun } from './ParaRun'
import { InstanceType } from '../../instanceTypes'
import { informDocChange } from '../../calculation/informChanges/textContent'
import { Nullable } from '../../../../liber/pervasive'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../common/utils'
import { RunContentElement } from '../RunElement/type'
import { SlideLayout } from '../../Slide/SlideLayout'
import { SlideMaster } from '../../Slide/SlideMaster'
import { Slide } from '../../Slide/Slide'
import { TextStylesInfo } from '../../utilities/shape/style'
import { updateCalcStatusForTextContentItem } from '../../calculation/updateCalcStatus/textContent'
import { formatDateTimeFormat } from '../../../lib/utils/date'
import { ParaPr, ParaPrOptions } from '../../textAttributes/ParaPr'
import { EnUSLangID } from '../../../lib/locale'
import { fromOptions } from '../../common/helpers'
import { ManagedSliceUtil } from '../../../common/managedArray'
import { getDateTimeFormatString } from '../../../lib/utils/dateTimeFormat/consts'
import { Paragraph } from '../Paragraph'

export class PresentationField extends ParaRun {
  Guid: Nullable<string>
  fieldType: string
  pPr: Nullable<ParaPr>
  slide: Slide | null
  slideIndex: number | null
  paraField = true
  constructor(para: Paragraph) {
    super(para)
    this.Guid = null
    this.fieldType = ''
    this.pPr = null

    this.slide = null
    this.slideIndex = null
  }
  isPresentationField() {
    return true
  }

  clone(
    _Selected?: boolean,
    _oPr?: { paragraph: Paragraph } | undefined
  ): PresentationField {
    const field = new PresentationField(this.paragraph)
    field.fromTextPr(this.pr)
    field.setGuid(createGUID())
    field.setFieldType(this.fieldType)
    if (this.pPr) {
      field.setParaPr(this.pPr)
    }
    return field
  }

  setGuid(guid: string) {
    informDocChange(this)

    this.Guid = guid
    updateCalcStatusForTextContentItem(this)
  }

  setFieldType(type: string) {
    informDocChange(this)
    this.fieldType = type
    updateCalcStatusForTextContentItem(this)
  }

  setParaPr(pr: Nullable<ParaPrOptions>) {
    informDocChange(this)
    this.pPr?.reset()
    this.pPr = fromOptions(this.pPr, pr, ParaPr)
    updateCalcStatusForTextContentItem(this)
  }

  addItemAt(index: number, item: RunContentElement, updatePosition?: boolean) {
    if (EditorUtil.ChangesStack.isOn()) {
      return
    }
    super.addItemAt.call(this, index, item, updatePosition)
  }

  removeContent(index: number, count: number, updatePosition?: boolean) {
    if (EditorUtil.ChangesStack.isOn()) {
      return []
    }

    return super.removeContent.call(this, index, count, updatePosition)
  }
  isEmpty() {
    return false
  }

  calcContent() {
    const s = turnOffRecordChanges()
    this.children.clear()
    const str = this.getContentString()
    if (typeof str === 'string') {
      this.insertText(str, -1)
    }
    restoreRecordChangesState(s)
  }

  GetFieldType() {
    if (typeof this.fieldType === 'string') {
      return this.fieldType.toLowerCase()
    }
    return ''
  }

  private getContentString() {
    let result = ''
    let textStyleInfo: TextStylesInfo
    if (typeof this.fieldType === 'string') {
      const fieldType = this.fieldType.toLowerCase()
      result = fieldType
      switch (fieldType) {
        case 'slidenum':
          if (this.paragraph && this.paragraph.parent) {
            textStyleInfo = this.paragraph.parent.getTextStyles(0)
            let firstSlideIndex = 1
            if (textStyleInfo.presentation) {
              if (isNumber(textStyleInfo.presentation?.pres?.firstSlideNum)) {
                firstSlideIndex = textStyleInfo.presentation.pres!.firstSlideNum
              }
            }
            if (textStyleInfo.slide) {
              const slide = textStyleInfo.slide as Slide
              this.slide = slide
              if (isNumber(this.slide.index)) {
                this.slideIndex = this.slide.index
                result = '' + (this.slide.index + firstSlideIndex)
              }
            } else if (textStyleInfo.notes) {
              if (textStyleInfo.notes.slide) {
                const slide = textStyleInfo.notes.slide as Slide
                this.slide = slide
                if (isNumber(this.slide.index)) {
                  this.slideIndex = this.slide.index
                  result = '' + (this.slide.index + firstSlideIndex)
                }
              }
            } else if (textStyleInfo.layout) {
              const layout = textStyleInfo.layout as SlideLayout
              this.slideIndex = layout.lastCalcSlideIndex
              result = '' + (layout.lastCalcSlideIndex + firstSlideIndex)
            } else if (textStyleInfo.master) {
              const master = textStyleInfo.master as SlideMaster
              this.slideIndex = master.lastCalcSlideIndex
              result = '' + (master.lastCalcSlideIndex + firstSlideIndex)
            }
          }
          break
        case 'value':
        case 'percentage':
          break
        default:
          if (fieldType.indexOf('datetime') === 0) {
            if (fieldType) {
              const lang = this.getCalcedPr().lang!.val! ?? EnUSLangID
              result = formatDateTimeFormat(
                new Date(),
                getDateTimeFormatString(fieldType, lang),
                lang
              )
            } else {
              result = fieldType.toUpperCase()
            }
          } else {
            result = fieldType.toUpperCase()
          }
          break
      }
    }
    return result || null
  }

  getText(): string {
    let res = ''
    ManagedSliceUtil.forEach(this.children, (item) => {
      switch (item.instanceType) {
        case InstanceType.ParaText: {
          res += stringFromCharCode(item.value)
          break
        }
        case InstanceType.ParaSpace: {
          res += ' '
          break
        }
        case InstanceType.ParaTab: {
          res += '\t'
          break
        }
        case InstanceType.ParaNewLine: {
          if ('' !== res) {
            res = ''
          }
          break
        }
      }
    })
    return res
  }
}
