import {
  ParaRunState,
  ParaItemPosInfo,
  ParagraphElementAdjacentPosition,
  ParagraphElementPosition,
  ParaRunStateSelection,
} from '../common'
import { idGenerator } from '../../../globals/IdGenerator'
import { TextPr, TextPrOptions } from '../../textAttributes/TextPr'

import { ParaSpace } from '../RunElement/ParaSpace'

import { ParaTab } from '../RunElement/ParaTab'
import { ParaNewLine } from '../RunElement/ParaNewLine'
import { ParaText } from '../RunElement/ParaText'

import { eachOfUnicodeString } from '../../../common/utils'
import { EditorUtil } from '../../../globals/editor'
import { RunElementsCollector } from '../RunElementsCollector'
import { Paragraph } from '../Paragraph'
import { InstanceType, isInstanceTypeOf } from '../../instanceTypes'
import { getPresentation } from '../../utilities/finders'
import { RunContentElement } from '../RunElement/type'
import {
  LineSegmentIndexes,
  LineSegmentIndexesUtil,
} from './LineSegmentIndexes'
import { informDocChange } from '../../calculation/informChanges/textContent'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { getParaItemPosInfo } from '../utils/position'
import { Dict, Nullable } from '../../../../liber/pervasive'
import { ParaHyperlink } from './ParaHyperlink'
import { SearchResultOfParagraph } from '../../search/searchStates'
import { TextOperationDirection } from '../../utilities/type'
import { ParaNumbering } from '../RunElement/ParaNumbering'
import { getFontClass } from '../../../fonts/FontClassification'
import { FontClass } from '../../../fonts/const'
import { updateCalcStatusForTextContentItem } from '../../calculation/updateCalcStatus/textContent'
import { updateTextPrFonts } from '../../utilities/style'
import { OptionsApplyResult } from '../../textAttributes/type'
import { ManagedSliceUtil, RecyclableArray } from '../../../common/managedArray'
import { PoolAllocator, Recyclable } from '../../../common/allocator'
import { recycleRunElement } from '../RunElement/utils'
import { fromOptions } from '../../common/helpers'
import { FillKIND } from '../../common/const/attrs'

export interface CheckEmptyRunOptions {
  ignoreEnd?: boolean
  ignoreNewLine?: boolean
}

interface RunMetrics {
  textAscent: number
  textDescent: number
  textHeight: number
  textAscentOfMetrics: number
  ascent: number
  descent: number
}

export interface ParaRunCalcState {
  isTextPr: boolean
  reMeasure: boolean
  numbering: Nullable<ParaNumbering>
  isAddingNumbering: boolean
  hasArabic: boolean
}

export class ParaRun extends Recyclable<ParaRun> {
  readonly instanceType = InstanceType.ParaRun
  id: string
  paragraph: Paragraph
  pr: TextPr
  children: RecyclableArray<RunContentElement>
  state: ParaRunState
  selection: ParaRunStateSelection
  calcedPrs: TextPr

  calculateInfo: ParaRunCalcState
  startParaLine: number
  lineSegmentIndexes: LineSegmentIndexes
  metrics: RunMetrics

  adjacentPositions: ParagraphElementAdjacentPosition[]
  searchResults: SearchResultOfParagraph[]
  parent: Nullable<Paragraph | ParaHyperlink>
  compositeInputInfo: Nullable<{
    run: ParaRun
    index: number
    length: number
  }>
  private static allocator: PoolAllocator<
    ParaRun,
    [paragraph: Paragraph, pr?: TextPr]
  > = PoolAllocator.getAllocator<ParaRun, [paragraph: Paragraph, pr?: TextPr]>(
    ParaRun
  )

  static new(paragraph: Paragraph, pr?: TextPr) {
    return ParaRun.allocator.allocate(paragraph, pr)
  }
  static recycle(run: ParaRun) {
    ParaRun.allocator.recycle(run)
  }
  constructor(paragraph: Paragraph, pr?: TextPr) {
    super()
    this.lineSegmentIndexes = LineSegmentIndexesUtil.resetOrCreate()

    this.startParaLine = -1

    this.id = idGenerator.newId()
    this.paragraph = paragraph
    this.pr = TextPr.new()
    if (pr != null) {
      this.pr.fromOptions(pr)
    }
    this.children = new RecyclableArray(recycleRunElement)

    this.selection = {
      isUse: false,
      startIndex: 0,
      endIndex: 0,
    }
    this.state = {
      selection: this.selection,
      elementIndex: 0,
    }
    this.calcedPrs = TextPr.new()
    this.calculateInfo = {
      isTextPr: true,
      reMeasure: true,
      numbering: null,
      isAddingNumbering: true,
      hasArabic: false,
    }

    this.metrics = {
      textAscent: 0,
      textDescent: 0,
      textHeight: 0,
      textAscentOfMetrics: 0,
      ascent: 0,
      descent: 0,
    }

    this.adjacentPositions = []
    this.searchResults = []

    this.compositeInputInfo = null
  }

  reset(paragraph: Paragraph, pr?: TextPr) {
    LineSegmentIndexesUtil.resetOrCreate(this.lineSegmentIndexes)

    this.startParaLine = -1

    this.id = idGenerator.newId()
    this.paragraph = paragraph
    this.pr?.reset()
    if (pr) {
      this.pr = fromOptions<TextPrOptions, undefined, TextPr>(
        this.pr,
        pr,
        TextPr
      )!
    }

    this.children.clear()

    this.selection.isUse = false
    this.selection.startIndex = 0
    this.selection.endIndex = 0

    this.state.elementIndex = 0

    this.calcedPrs.reset()

    this.calculateInfo.isTextPr = true
    this.calculateInfo.reMeasure = true
    this.calculateInfo.numbering = null
    this.calculateInfo.isAddingNumbering = true
    this.calculateInfo.hasArabic = false

    this.metrics.textAscent = 0
    this.metrics.textDescent = 0
    this.metrics.textHeight = 0
    this.metrics.textAscentOfMetrics = 0
    this.metrics.ascent = 0
    this.metrics.descent = 0

    this.adjacentPositions = []
    this.searchResults = []

    this.compositeInputInfo = null
  }

  isPresentationField() {
    return false
  }

  setParagraph(paragraph: Paragraph) {
    this.paragraph = paragraph
  }

  getParent() {
    if (!this.paragraph) return null

    const elementPosition = this.paragraph.getContentPositionByElement(this)
    if (!elementPosition || elementPosition.getLevel() < 0) return null

    elementPosition.decreaseLevel(1)
    return this.paragraph.elementAtElementPos(elementPosition)
  }

  // run methods

  getId() {
    return this.id
  }

  getTheme() {
    if (this.paragraph) {
      return this.paragraph.getTheme()
    }
    return undefined
  }

  clone(isSelected: boolean, pr?: { paragraph: Paragraph }) {
    const newRun = ParaRun.new(this.paragraph, this.pr)

    let startIndex = 0
    let endIndex = this.children.length
    if (endIndex <= 0) return newRun

    if (true === isSelected && true === this.state.selection.isUse) {
      startIndex = this.state.selection.startIndex
      endIndex = this.state.selection.endIndex

      if (startIndex > endIndex) {
        startIndex = this.state.selection.endIndex
        endIndex = this.state.selection.startIndex
      }
    } else if (true === isSelected && true !== this.state.selection.isUse) {
      endIndex = -1
    }

    for (let i = startIndex, addedIndex = 0; i < endIndex; i++) {
      const item = this.children.at(i)
      if (item) {
        if (InstanceType.ParaNewLine === item.instanceType && pr) {
          if (
            true !== pr.paragraph.isEmpty() ||
            newRun.isEmpty({ ignoreEnd: true }) !== true
          ) {
            // clone 不应该设置原 para dirty
            newRun.insertItemAt(addedIndex, ParaNewLine.new(), false)
            addedIndex++
          }
        } else {
          if (InstanceType.ParaEnd !== item.instanceType) {
            newRun.insertItemAt(addedIndex, item.clone(), false)
            addedIndex++
          }
        }
      }
    }

    return newRun
  }

  isEmpty(props?: CheckEmptyRunOptions) {
    const ignoreEnd = null != props?.ignoreEnd ? props.ignoreEnd : false
    const ignoreNewLine =
      null != props?.ignoreNewLine ? props.ignoreNewLine : false

    const l = this.children.length

    if (true !== ignoreEnd && true !== ignoreNewLine) {
      if (l > 0) return false
      else return true
    } else {
      for (let i = 0; i < l; ++i) {
        const item = this.children.at(i)!
        const valueOfType = item.instanceType

        if (
          (true !== ignoreEnd || InstanceType.ParaEnd !== valueOfType) &&
          (true !== ignoreNewLine || InstanceType.ParaNewLine !== valueOfType)
        ) {
          return false
        }
      }

      return true
    }
  }
  add(item: RunContentElement) {
    this.addItemAt(this.state.elementIndex, item, true)

    if (!this.isPresentationField() && item.canStartAutoCorrect()) {
      this.correctPositionsFrom(this.state.elementIndex - 1)
    }
  }

  remove(dir: TextOperationDirection) {
    if (this.children.length <= 0) return false

    const selection = this.state.selection

    if (true === selection.isUse) {
      let startIndex = selection.startIndex
      let endIndex = selection.endIndex

      if (startIndex > endIndex) {
        startIndex = selection.endIndex
        endIndex = selection.startIndex
      }
      startIndex = startIndex < 0 ? 0 : startIndex
      endIndex = Math.min(endIndex, this.children.length)
      if (startIndex === endIndex) return false

      if (true === this.isParaEndSelected()) {
        for (let i = endIndex - 1; i >= startIndex; i--) {
          const item = this.children.at(i)
          if (item && InstanceType.ParaEnd !== item.instanceType) {
            this.children.recycle(this.removeContent(i, 1, true))
          }
        }
      } else {
        this.children.recycle(
          this.removeContent(startIndex, endIndex - startIndex, true)
        )
      }

      this.removeSelection()
      this.state.elementIndex = startIndex
    } else {
      const curIndex = this.state.elementIndex

      if (dir < 0) {
        if (curIndex <= 0) return false

        this.children.recycle(this.removeContent(curIndex - 1, 1, true))

        this.state.elementIndex = curIndex - 1
      } else {
        if (
          curIndex >= this.children.length ||
          InstanceType.ParaEnd === this.children.at(curIndex)?.instanceType
        ) {
          return false
        }

        this.children.recycle(this.removeContent(curIndex, 1, true))

        this.state.elementIndex = curIndex
      }
    }
    return true
  }

  removeParaEnd() {
    let index = -1

    const contentLen = this.children.length
    for (let i = 0; i < contentLen; i++) {
      if (InstanceType.ParaEnd === this.children.at(i)?.instanceType) {
        index = i
        break
      }
    }

    if (-1 === index) return false

    this.children.recycle(this.removeContent(index, contentLen - index, true))

    return true
  }

  private _updateStateOnAdd(index: number) {
    if (this.state.elementIndex >= index) this.state.elementIndex++

    if (true === this.state.selection.isUse) {
      if (this.state.selection.startIndex >= index) {
        this.state.selection.startIndex++
      }

      if (this.state.selection.endIndex >= index) {
        this.state.selection.endIndex++
      }
    }
  }

  private updateStateOnRemove(index: number, count: number) {
    if (this.state.elementIndex > index + count) {
      this.state.elementIndex -= count
    } else if (this.state.elementIndex > index) {
      this.state.elementIndex = index
    }

    if (true === this.state.selection.isUse) {
      if (this.state.selection.startIndex <= this.state.selection.endIndex) {
        if (this.state.selection.startIndex > index + count) {
          this.state.selection.startIndex -= count
        } else if (this.state.selection.startIndex > index) {
          this.state.selection.startIndex = index
        }

        if (this.state.selection.endIndex >= index + count) {
          this.state.selection.endIndex -= count
        } else if (this.state.selection.endIndex > index) {
          this.state.selection.endIndex = Math.max(0, index - 1)
        }
      } else {
        if (this.state.selection.startIndex >= index + count) {
          this.state.selection.startIndex -= count
        } else if (this.state.selection.startIndex > index) {
          this.state.selection.startIndex = Math.max(0, index - 1)
        }

        if (this.state.selection.endIndex > index + count) {
          this.state.selection.endIndex -= count
        } else if (this.state.selection.endIndex > index) {
          this.state.selection.endIndex = index
        }
      }
    }
  }

  /** 目前仅输入文本时 updatePosition === true */
  addItemAt(index: number, item: RunContentElement, updatePosition?: boolean) {
    if (-1 === index) index = this.children.length

    informDocChange(this)
    this.insertItemAt(index, item, updatePosition)
    updateCalcStatusForTextContentItem(this)
  }

  insertItemAt(
    index: number,
    item: RunContentElement,
    updatePosition?: boolean
  ) {
    this.children.insert(index, item)

    if (true === updatePosition) this._updateStateOnAdd(index)

    for (let i = 0, l = this.adjacentPositions.length; i < l; i++) {
      const runAdjPos = this.adjacentPositions[i]
      if (runAdjPos.adjacentPos) {
        const elmPosInAdjacent = runAdjPos.adjacentPos.pos
        const lvl = runAdjPos.level

        if (elmPosInAdjacent.positonIndices[lvl] >= index) {
          elmPosInAdjacent.positonIndices[lvl]++
        }
      }
    }

    for (let i = 0, l = this.searchResults.length; i < l; i++) {
      const mark = this.searchResults[i]
      const searchResultPos =
        true === mark.isStart
          ? mark.searchResult.startPosition
          : mark.searchResult.endPosition
      const lvl = mark.level

      if (
        searchResultPos.positonIndices[lvl] != null &&
        (searchResultPos.positonIndices[lvl] > index ||
          (searchResultPos.positonIndices[lvl] === index &&
            true === mark.isStart))
      ) {
        searchResultPos.positonIndices[lvl]++
      }
    }

    this.calculateInfo.reMeasure = true
  }

  removeContent(index: number, count: number, needUpdatePosition?: boolean) {
    informDocChange(this)

    const removedItems = this.children.remove(index, count)

    if (true === needUpdatePosition) {
      this.updateStateOnRemove(index, count)
    }

    for (let i = 0, l = this.adjacentPositions.length; i < l; i++) {
      const runAdjPos = this.adjacentPositions[i]
      const elementPosition = runAdjPos.adjacentPos!.pos
      const lvl = runAdjPos.level

      if (elementPosition.positonIndices[lvl] > index + count) {
        elementPosition.positonIndices[lvl] -= count
      } else if (elementPosition.positonIndices[lvl] > index) {
        elementPosition.positonIndices[lvl] = Math.max(0, index)
      }
    }

    for (let i = 0, l = this.searchResults.length; i < l; i++) {
      const mark = this.searchResults[i]
      const elementPosition =
        true === mark.isStart
          ? mark.searchResult.startPosition
          : mark.searchResult.endPosition
      const lvl = mark.level

      if (elementPosition.positonIndices[lvl] > index + count) {
        elementPosition.positonIndices[lvl] -= count
      } else if (elementPosition.positonIndices[lvl] > index) {
        elementPosition.positonIndices[lvl] = Math.max(0, index)
      }
    }

    this.calculateInfo.reMeasure = true
    updateCalcStatusForTextContentItem(this)
    return removedItems
  }

  private appendContent(items: RunContentElement[]) {
    this.children.append(items)

    informDocChange(this)

    this.calculateInfo.reMeasure = true
    updateCalcStatusForTextContentItem(this)
  }

  private _insertCharCode(charPos: number, charCode: number) {
    switch (charCode) {
      case 9: {
        // \t
        this.addItemAt(charPos, ParaTab.new())
        break
      }
      case 10: {
        // \n
        this.addItemAt(charPos, ParaNewLine.new())
        break
      }
      case 13: {
        // \r
        break
      }
      case 32: {
        // space
        this.addItemAt(charPos, ParaSpace.new())
        break
      }
      default: {
        this.addItemAt(charPos, ParaText.new(charCode))
        break
      }
    }
  }
  insertText(str: string, pos?: number) {
    let charPos = pos != null && -1 !== pos ? pos : this.children.length
    eachOfUnicodeString(str, (charCode) => {
      this._insertCharCode(charPos++, charCode!)
    })
  }

  getParaItemPosInfoByContentPos(
    elementPosition: ParagraphElementPosition,
    lvl: number
  ) {
    if (this.startParaLine < 0) {
      return new ParaItemPosInfo(0, 0, 0)
    }

    const indexOfPosition = elementPosition.get(lvl)

    return getParaItemPosInfo(this, indexOfPosition)
  }

  splitAtContentPos(elementPosition: ParagraphElementPosition, lvl: number) {
    const index = elementPosition.get(lvl)
    return this.splitAtIndex(index)
  }

  splitAtIndex(index: number) {
    informDocChange(this)
    EditorUtil.pptPosition.onSplitRunStart(this, index)

    const newRun = ParaRun.new(this.paragraph, this.pr)

    let endPosIndex = -1
    const endIndex = Math.min(index, this.children.length)
    for (let i = 0; i < endIndex; i++) {
      if (InstanceType.ParaEnd === this.children.at(i)?.instanceType) {
        endPosIndex = i
        break
      }
    }

    if (-1 !== endPosIndex) index = endPosIndex

    const appendItems = this.removeContent(
      index,
      this.children.length - index,
      true
    )
    newRun.appendContent(appendItems)

    updateCalcStatusForTextContentItem(this)
    EditorUtil.pptPosition.onSplitRunEnd(newRun)
    return newRun
  }

  collectAllFontNames(allFonts: Dict<boolean>) {
    this.pr.collectAllFontNames(allFonts)
  }

  updateStateContentPos(
    elementPosition: ParagraphElementPosition,
    lvl: number
  ) {
    let indexOfPosition = elementPosition.get(lvl)

    const contentLength = this.children.length
    if (indexOfPosition > contentLength) indexOfPosition = contentLength

    for (let i = 0; i < indexOfPosition; i++) {
      if (InstanceType.ParaEnd === this.children.at(i)?.instanceType) {
        indexOfPosition = i
        break
      }
    }

    if (indexOfPosition < 0) indexOfPosition = 0
    this.state.elementIndex = indexOfPosition
  }

  runElementAtPos(elementPosition?: ParagraphElementPosition, lvl?: number) {
    if (null != elementPosition && lvl != null) {
      const indexOfPosition = elementPosition.get(lvl)

      if (indexOfPosition >= this.children.length || indexOfPosition < 0) {
        return null
      }

      return this.children.at(indexOfPosition)
    } else {
      if (this.children.length > 0) return this.children.at(0)
      else return null
    }
  }

  hasTextSelection() {
    return this.state.selection.isUse
  }

  isSelectedAll(props?: CheckEmptyRunOptions) {
    const selection = this.state.selection
    if (false === selection.isUse && true !== this.isEmpty(props)) return false

    const skipEnd = props ? props.ignoreEnd : false

    let startIndex = selection.startIndex
    let endIndex = selection.endIndex

    if (endIndex < startIndex) {
      startIndex = selection.endIndex
      endIndex = selection.startIndex
    }

    for (let i = 0; i < startIndex; i++) {
      const item = this.children.at(i)

      if (
        item &&
        !(true === skipEnd && InstanceType.ParaEnd === item.instanceType)
      ) {
        return false
      }
    }

    const count = this.children.length
    for (let i = endIndex; i < count; i++) {
      const item = this.children.at(i)

      if (
        item &&
        !(true === skipEnd && InstanceType.ParaEnd === item.instanceType)
      ) {
        return false
      }
    }

    return true
  }

  removeSelection() {
    const selection = this.state.selection

    selection.isUse = false
    selection.startIndex = 0
    selection.endIndex = 0
  }

  selectAll(dir?: TextOperationDirection) {
    const selection = this.state.selection

    selection.isUse = true

    if (-1 === dir) {
      selection.startIndex = this.children.length
      selection.endIndex = 0
    } else {
      selection.startIndex = 0
      selection.endIndex = this.children.length
    }
  }

  hasNoSelection(isCheckEnd?: boolean) {
    const selection = this.state.selection
    if (true !== selection.isUse) return true

    if (this.children.length <= 0) return true

    let startIndex = selection.startIndex
    let endIndex = selection.endIndex

    if (startIndex > endIndex) {
      startIndex = selection.endIndex
      endIndex = selection.startIndex
    }

    if (true === isCheckEnd) {
      return endIndex > startIndex ? false : true
    } else {
      for (let i = startIndex; i < endIndex; i++) {
        const item = this.children.at(i)
        const itemType = item?.instanceType
        if (InstanceType.ParaEnd !== itemType) return false
      }
    }

    return true
  }

  isParaEndSelected() {
    const selection = this.state.selection
    if (true !== selection.isUse) return false
    if (this.children.length <= 0) return false

    let startIndex = selection.startIndex
    let endIndex = selection.endIndex

    if (startIndex > endIndex) {
      startIndex = selection.endIndex
      endIndex = selection.startIndex
    }

    for (let i = startIndex; i < endIndex; i++) {
      const item = this.children.at(i)

      if (InstanceType.ParaEnd === item?.instanceType) return true
    }

    return false
  }

  isParaContentPosSelected(
    elementPosition: ParagraphElementPosition,
    lvl: number,
    isEnd: boolean
  ) {
    const indexOfPosition = elementPosition.get(lvl)

    if (
      this.selection.startIndex <= this.selection.endIndex &&
      this.selection.startIndex <= indexOfPosition &&
      indexOfPosition <= this.selection.endIndex
    ) {
      if (
        true !== isEnd ||
        (true === isEnd && indexOfPosition !== this.selection.endIndex)
      ) {
        return true
      }
    } else if (
      this.selection.startIndex > this.selection.endIndex &&
      this.selection.endIndex <= indexOfPosition &&
      indexOfPosition <= this.selection.startIndex
    ) {
      if (
        true !== isEnd ||
        (true === isEnd && indexOfPosition !== this.selection.startIndex)
      ) {
        return true
      }
    }

    return false
  }

  clearTextFormat(noInformDocChange = false) {
    const applyResult: OptionsApplyResult = { isApplied: false }
    this.pr.clearTextFormat(applyResult)
    if (applyResult.isApplied && noInformDocChange !== true) {
      informDocChange(this)
      this.invalidateCalcedPrs(true)
    }
  }

  getTextPr() {
    return this.pr
  }

  getCalcedTextPr(copy?: boolean) {
    if (
      true === this.state.selection.isUse &&
      true === this.isParaEndSelected()
    ) {
      const textPr = this.getCalcedPr(true)
      const textPrOfParaEnd = this.paragraph.getParaEndCalcedPr()

      textPr.subDiff(textPrOfParaEnd)

      return textPr
    } else {
      return this.getCalcedPr(copy)
    }
  }

  invalidateCalcedPrs(needReMeasure: boolean, bubbleUp = true) {
    this.calculateInfo.isTextPr = true

    if (true === needReMeasure) this.calculateInfo.reMeasure = true
    if (bubbleUp) {
      updateCalcStatusForTextContentItem(this)
    }
  }

  getCalcedPr(isCopy?: boolean) {
    if (true === this.calculateInfo.isTextPr) {
      TextPr.recycle(this.calcedPrs)
      this.calcedPrs = this.calcPr()
      this.calculateInfo.isTextPr = false
    }

    if (false === isCopy) return this.calcedPrs
    else return this.calcedPrs.clone()
  }

  private calcPr() {
    if (null == this.paragraph) {
      const textPr = TextPr.new()
      textPr.setDefault()
      this.calculateInfo.isTextPr = true
      return textPr
    }

    const textPr = this.paragraph.getCalcedPrs(false)!.textPr.clone()

    textPr.merge(this.pr)

    const theme = this.getTheme()
    if (theme) {
      updateTextPrFonts(textPr, theme)
    }

    const firstText = ManagedSliceUtil.find(this.children, (c) =>
      isInstanceTypeOf(c, InstanceType.ParaText)
    ) as ParaText
    if (firstText) {
      const hint = textPr.rFonts.hint
      const isCS = textPr.cs
      const ea = textPr.lang?.eastAsia
      const fontClass = getFontClass(firstText.value, hint, ea, isCS)
      let font = textPr.rFonts.ascii
      switch (fontClass) {
        case FontClass.ASCII:
        case FontClass.HAnsi:
          font = textPr.rFonts.ascii
          break
        case FontClass.EastAsia:
          font = textPr.rFonts.eastAsia
          break
        case FontClass.CS:
          font = textPr.rFonts.cs
      }
      textPr.fontFamily!.name = font!.name
      textPr.fontFamily!.index = font!.index
      textPr.lang.fontClass = fontClass
    } else {
      textPr.fontFamily!.name = textPr.rFonts.ascii!.name
      textPr.fontFamily!.index = textPr.rFonts.ascii!.index
    }

    return textPr
  }

  fromTextPr(textPr: TextPrOptions) {
    if (textPr === this.pr) return
    this.pr.reset()
    this.pr.fromOptions(textPr)
    informDocChange(this)
    this.invalidateCalcedPrs(true)
  }

  splitAt(index: number) {
    informDocChange(this)
    EditorUtil.pptPosition.onSplitRunStart(this, index)

    const newRun = ParaRun.new(this.paragraph, this.pr)

    const oldCurElmIndex = this.state.elementIndex
    const oldStartIndex = this.state.selection.startIndex
    const oldEndIndex = this.state.selection.endIndex

    const appendItems = this.removeContent(
      index,
      this.children.length - index,
      true
    )
    newRun.appendContent(appendItems)
    updateCalcStatusForTextContentItem(this)

    if (oldCurElmIndex >= index) {
      newRun.state.elementIndex = oldCurElmIndex - index
      this.state.elementIndex = this.children.length
    } else {
      newRun.state.elementIndex = 0
    }

    if (oldStartIndex >= index) {
      newRun.state.selection.startIndex = oldStartIndex - index
      this.state.selection.startIndex = this.children.length
    } else {
      newRun.state.selection.startIndex = 0
    }

    if (oldEndIndex >= index) {
      newRun.state.selection.endIndex = oldEndIndex - index
      this.state.selection.endIndex = this.children.length
    } else {
      newRun.state.selection.endIndex = 0
    }

    EditorUtil.pptPosition.onSplitRunEnd(newRun)
    return newRun
  }

  applyPr(textPr: TextPrOptions) {
    const applyResult = { needReMeasure: false, isApplied: false }
    this.pr.fromOptions(textPr, applyResult)
    if (this.pr.fillEffects?.fill?.type === FillKIND.SOLID) {
      this.pr.fillEffects.check(null, null)
    }

    if (applyResult.isApplied) {
      informDocChange(this)
      this.invalidateCalcedPrs(applyResult.needReMeasure)
    }

    for (let i = 0, l = this.children.length; i < l; ++i) {
      if (InstanceType.ParaEnd === this.children.at(i)?.instanceType) {
        this.paragraph.paraTextPr.fromTextPrData(textPr)
        break
      }
    }
  }

  setUnderline(v: Nullable<boolean>) {
    const isApplied = this.pr.setUnderline(v)
    if (isApplied) {
      informDocChange(this)

      this.invalidateCalcedPrs(false)
    }
  }

  setFontSize(v: Nullable<number>) {
    if (this.pr.setFontSize(v)) {
      informDocChange(this)

      this.invalidateCalcedPrs(true)
    }
  }

  setFillEffects(fill: Nullable<FillEffects>) {
    if (this.pr.setFillEffects(fill)) {
      informDocChange(this)

      this.invalidateCalcedPrs(false)
    }
  }

  setSpacing(v: Nullable<number>) {
    if (this.pr.setSpacing(v)) {
      informDocChange(this)

      this.invalidateCalcedPrs(true)
    }
  }

  getLineSegmentsContains(index: number) {
    const segments: number[] = []
    const lineSegmentIndexes = this.lineSegmentIndexes
    const linesCount = LineSegmentIndexesUtil.getLinesCount(lineSegmentIndexes)
    for (let lineIndex = 0; lineIndex < linesCount; lineIndex++) {
      const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
        lineSegmentIndexes,
        lineIndex
      )
      const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
        lineSegmentIndexes,
        lineIndex
      )

      if (startIndex <= index && index <= endIndex) {
        segments.push(lineIndex + this.startParaLine)
      }
    }

    return segments
  }

  isInHyperlink() {
    if (!this.paragraph) return false

    const elementPosition = this.paragraph.getContentPositionByElement(this)!
    const items = this.paragraph.getParaItemsByContentPosition(elementPosition)

    let isHyper = false
    let isRun = false

    for (let index = 0, count = items.length; index < count; index++) {
      const item = items[index]
      if (item === this) {
        isRun = true
        break
      } else if (item.instanceType === InstanceType.ParaHyperlink) {
        isHyper = true
      }
    }

    return isHyper && isRun
  }

  isInParagraph() {
    return !!this.paragraph?.getContentPositionByElement(this)
  }

  setCompositeInputInfo(
    compositeInput: Nullable<{
      run: ParaRun
      index: number
      length: number
    }>
  ) {
    this.compositeInputInfo = compositeInput
  }

  isInPPT() {
    return (
      this.paragraph &&
      true === this.paragraph.isInPPT() &&
      true === this.isInParagraph()
    )
  }

  childrenCount() {
    return this.children.length
  }

  private correctPositionsFrom(indexOfPosition: number) {
    const maxElements = 10

    const paragraph = this.paragraph
    if (!paragraph) return false

    const document = getPresentation(this)
    if (!document) return false

    const elementPosition = paragraph.getContentPositionByElement(this)
    if (!elementPosition) return false

    elementPosition.updatePosByLevel(
      indexOfPosition,
      elementPosition.getLevel() + 1
    )

    const item = this.children.at(indexOfPosition)
    if (
      item &&
      InstanceType.ParaText === item.instanceType &&
      (34 === item.value || 39 === item.value)
    ) {
      return false
    } else if (
      item &&
      InstanceType.ParaText === item.instanceType &&
      45 === item.value
    ) {
      return false
    }

    const runElmsBefore = new RunElementsCollector(
      elementPosition,
      maxElements,
      undefined,
      true
    )
    runElmsBefore.updatePrevRunElementsPosInParagraph(paragraph)
    const elements = runElmsBefore.getElements()
    if (elements.length <= 0) return false

    for (
      let valueOfIndex = 0, valueOfCount = elements.length;
      valueOfIndex < valueOfCount;
      ++valueOfIndex
    ) {
      if (InstanceType.ParaText !== elements[valueOfIndex].instanceType) {
        return false
      }
    }

    return false
  }
}
