/**
 		// LineSegmentIndexes array is divided into two parts
    // 1. Consists of one element, meaning the number of lines
    
    // 2. The very information about the beginning and end of the line segment. Each segment is represented by a pair of startIndex, endIndex.
    //
    // Example. 2 lines, the first line segment pair is at index 1, 2 and the second line segment pair is at 3, 4
    [
			2, (line count) 
			0, 15, 15, 17(start/end positon pairs for segment)
		//^			 ^
		//|      |
		//L0     L1
		];
 */
export type LineSegmentIndexes = [number, ...number[]]
function resetOrCreate(lsi?: LineSegmentIndexes): LineSegmentIndexes {
  if (lsi == null) return [0]
  lsi[0] = 0
  return lsi
}

function isValidLineIndex(lsi: LineSegmentIndexes, index: number) {
  return index >= 0 && lsi[0] >= index + 1
}

function getSegIndexOffset(lineIndex: number): number {
  return 1 + lineIndex * 2
}

function getStartIndexOfLine(lsi: LineSegmentIndexes, lineIndex: number) {
  return isValidLineIndex(lsi, lineIndex)
    ? lsi[getSegIndexOffset(lineIndex)]
    : -1
}

function getEndIndexOfLine(lsi: LineSegmentIndexes, lineIndex: number) {
  return isValidLineIndex(lsi, lineIndex)
    ? lsi[getSegIndexOffset(lineIndex) + 1]
    : -1
}

function getLinesCount(lsi: LineSegmentIndexes) {
  return lsi[0]
}

function resetToLine(lsi: LineSegmentIndexes, lineIndex: number) {
  lineIndex = lineIndex < 0 ? 0 : lineIndex
  const segOffset = getSegIndexOffset(lineIndex)

  lsi[0] = lineIndex + 1

  lsi[segOffset + 0] = 0
  lsi[segOffset + 1] = 0

  if (0 !== lineIndex) {
    return lsi[segOffset - 1] ?? 0
  } else return 0
}

function setIndexesOfLine(
  lsi: LineSegmentIndexes,
  lineIndex: number,
  startIndex: number,
  endIndex: number
) {
  if (!isValidLineIndex(lsi, lineIndex)) return
  const segOffset = getSegIndexOffset(lineIndex)
  lsi[segOffset + 0] = startIndex
  lsi[segOffset + 1] = endIndex
}

function setEndIndexOfLine(
  lsi: LineSegmentIndexes,
  lineIndex: number,
  endIndex: number
) {
  if (!isValidLineIndex(lsi, lineIndex)) return
  const segOffset = getSegIndexOffset(lineIndex)
  lsi[segOffset + 1] = endIndex
}

export const LineSegmentIndexesUtil = {
  resetOrCreate,
  getStartIndexOfLine,
  getEndIndexOfLine,
  getLinesCount,
  resetToLine,
  setIndexesOfLine,
  setEndIndexOfLine,
} as const
