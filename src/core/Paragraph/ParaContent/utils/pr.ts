import { Nullable } from '../../../../../liber/pervasive'
import { InstanceType } from '../../../instanceTypes'
import { TextPrOptions } from '../../../textAttributes/TextPr'
import { getFontSizeForIncreaseDecreaseByInput } from '../../../common/utils'
import { moveCursorToStartPosForParaItem } from '../../../utilities/cursor/moveToEdgePos'
import { ParagraphItem } from '../../type'
import { ParaHyperlink } from '../ParaHyperlink'
import { ParaRun } from '../ParaRun'
import { TextOperationDirection } from '../../../utilities/type'
export function getCurrentTextPrInParaItem(paraItem: Nullable<ParagraphItem>) {
  if (paraItem == null) return undefined
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    if (paraItem.children.length <= 0) return undefined
    if (true === paraItem.selection.isUse) {
      let startPosIndex = paraItem.selection.startIndex
      let endPosIndex = paraItem.selection.endIndex

      if (startPosIndex > endPosIndex) {
        startPosIndex = paraItem.selection.endIndex
        endPosIndex = paraItem.selection.startIndex
      }

      startPosIndex = Math.max(0, startPosIndex)
      while (
        startPosIndex < endPosIndex &&
        true === paraItem.children.at(startPosIndex)!.hasNoSelection()
      ) {
        startPosIndex++
      }

      return paraItem.children.at(startPosIndex)?.pr
    } else {
      return paraItem.children.at(paraItem.state.elementIndex)?.pr
    }
  } else {
    return paraItem.pr
  }
}

export function applyTextPrToParaItem(
  paraItem: ParagraphItem,
  textPr: Nullable<TextPrOptions>,
  isIncreaseFontSize?: boolean,
  isSetToAll?: boolean
) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    return applyTextPrToParaRun(
      paraItem,
      textPr,
      isIncreaseFontSize,
      isSetToAll
    )
  } else {
    return applyTextPrToHyperlink(
      paraItem,
      textPr,
      isIncreaseFontSize,
      isSetToAll
    )
  }
}

export function applyTextPrToParaRun(
  run: ParaRun,
  textPr: Nullable<TextPrOptions>,
  isIncreaseFontSize?: boolean,
  isSetToAll?: boolean
) {
  if (true === isSetToAll) {
    if (null == isIncreaseFontSize) {
      run.applyPr(textPr!)
    } else {
      const curTextPr = run.getCalcedPr(false)

      run.setFontSize(
        getFontSizeForIncreaseDecreaseByInput(
          isIncreaseFontSize,
          curTextPr.fontSize!
        )
      )
    }

    let isEnd = false
    const count = run.children.length
    for (let i = 0; i < count; i++) {
      const item = run.children.at(i)
      if (item && InstanceType.ParaEnd === item.instanceType) {
        isEnd = true
        break
      }
    }

    if (true === isEnd) {
      if (undefined === isIncreaseFontSize && textPr) {
        run.paragraph.paraTextPr.fromTextPrData(textPr)
      } else {
        const textPrOfParaEnd = run.paragraph.getParaEndCalcedPr()

        run.paragraph.paraTextPr.setFontSize(
          getFontSizeForIncreaseDecreaseByInput(
            !!isIncreaseFontSize,
            textPrOfParaEnd.fontSize!
          )
        )
      }
    }
  } else {
    const result: Array<Nullable<ParaRun>> = []
    let runOfLeft: Nullable<ParaRun> = run,
      runOfCenter: Nullable<ParaRun>,
      runOfRight: Nullable<ParaRun>

    if (true === run.state.selection.isUse) {
      let startPosIndex = run.state.selection.startIndex
      let endPosIndex = run.state.selection.endIndex

      if (startPosIndex === endPosIndex && 0 !== run.children.length) {
        runOfCenter = run
        runOfLeft = null
        runOfRight = null
      } else {
        let direction: TextOperationDirection = 1
        if (startPosIndex > endPosIndex) {
          const temp = startPosIndex
          startPosIndex = endPosIndex
          endPosIndex = temp
          direction = -1
        }

        if (endPosIndex < run.children.length) {
          runOfRight = runOfLeft.splitAt(endPosIndex)
        }

        if (startPosIndex > 0) {
          runOfCenter = runOfLeft.splitAt(startPosIndex)
        } else {
          runOfCenter = runOfLeft
          runOfLeft = null
        }

        if (null != runOfLeft) {
          runOfLeft.selection.isUse = true
          runOfLeft.selection.startIndex = runOfLeft.children.length
          runOfLeft.selection.endIndex = runOfLeft.children.length
        }

        runOfCenter.selectAll(direction)

        if (null == isIncreaseFontSize) runOfCenter.applyPr(textPr!)
        else {
          const curTextPr = run.getCalcedPr(false)

          runOfCenter.setFontSize(
            getFontSizeForIncreaseDecreaseByInput(
              isIncreaseFontSize,
              curTextPr.fontSize!
            )
          )
        }

        if (null != runOfRight) {
          runOfRight.selection.isUse = true
          runOfRight.selection.startIndex = 0
          runOfRight.selection.endIndex = 0
        }

        if (true === run.isParaEndSelected()) {
          if (undefined === isIncreaseFontSize && textPr) {
            run.paragraph.paraTextPr.fromTextPrData(textPr)
          } else {
            const textPrOfParaEnd = run.paragraph.getParaEndCalcedPr()

            run.paragraph.paraTextPr.setFontSize(
              getFontSizeForIncreaseDecreaseByInput(
                !!isIncreaseFontSize,
                textPrOfParaEnd.fontSize!
              )
            )
          }
        }
      }
    } else {
      const curIndex = run.state.elementIndex

      if (curIndex < run.children.length) {
        runOfRight = runOfLeft.splitAt(curIndex)
      }

      if (curIndex > 0) {
        runOfCenter = runOfLeft.splitAt(curIndex)
      } else {
        runOfCenter = runOfLeft
        runOfLeft = null
      }

      if (null != runOfLeft) runOfLeft.removeSelection()

      runOfCenter.removeSelection()
      moveCursorToStartPosForParaItem(runOfCenter)

      if (null == isIncreaseFontSize) {
        runOfCenter.applyPr(textPr!)
      } else {
        const curTextPr = run.getCalcedPr(false)
        runOfCenter.setFontSize(
          getFontSizeForIncreaseDecreaseByInput(
            isIncreaseFontSize,
            curTextPr.fontSize!
          )
        )
      }

      if (null != runOfRight) runOfRight.removeSelection()
    }

    result.push(runOfLeft)
    result.push(runOfCenter)
    result.push(runOfRight)

    return result
  }
}
function applyTextPrToHyperlink(
  hyperlink: ParaHyperlink,
  textPr: Nullable<TextPrOptions>,
  isIncreaseFontSize?: boolean,
  isSetToAll?: boolean
) {
  const childrenLength = hyperlink.children.length
  if (childrenLength <= 0) return

  if (true === isSetToAll) {
    for (let i = 0; i < childrenLength; i++) {
      applyTextPrToParaRun(
        hyperlink.children.at(i)!,
        textPr,
        isIncreaseFontSize,
        true
      )
    }
  } else {
    const selection = hyperlink.state.selection

    if (true === selection.isUse) {
      let startIndex = selection.startIndex
      let endIndex = selection.endIndex

      if (startIndex === endIndex) {
        const run = hyperlink.children.at(endIndex)
        if (run) {
          const newElements = applyTextPrToParaRun(
            run,
            textPr,
            isIncreaseFontSize,
            false
          )!

          const centerRunIndex = _replaceRunInHyperlink(
            hyperlink,
            endIndex,
            newElements
          )

          if (startIndex === hyperlink.state.elementIndex) {
            hyperlink.state.elementIndex = centerRunIndex
          }

          selection.startIndex = centerRunIndex
          selection.endIndex = centerRunIndex
        }
      } else {
        if (startIndex > endIndex) {
          const t = startIndex
          startIndex = endIndex
          endIndex = t
        }

        startIndex = Math.max(0, startIndex)
        endIndex = Math.min(endIndex, childrenLength - 1)

        for (let i = startIndex + 1; i < endIndex; i++) {
          applyTextPrToParaRun(
            hyperlink.children.at(i)!,
            textPr,
            isIncreaseFontSize,
            true
          )
        }

        let newElements = applyTextPrToParaRun(
          hyperlink.children.at(endIndex)!,
          textPr,
          isIncreaseFontSize,
          false
        )!
        _replaceRunInHyperlink(hyperlink, endIndex, newElements)

        newElements = applyTextPrToParaRun(
          hyperlink.children.at(startIndex)!,
          textPr,
          isIncreaseFontSize,
          false
        )!

        _replaceRunInHyperlink(hyperlink, startIndex, newElements)

        let startItem = hyperlink.children.at(selection.startIndex)
        let endItem = hyperlink.children.at(selection.endIndex)
        if (
          selection.startIndex < selection.endIndex &&
          (startItem == null || true === startItem.hasNoSelection())
        ) {
          selection.startIndex++
        } else if (
          selection.endIndex < selection.startIndex &&
          (endItem == null || true === endItem.hasNoSelection())
        ) {
          selection.endIndex++
        }

        startItem = hyperlink.children.at(selection.startIndex)
        endItem = hyperlink.children.at(selection.endIndex)
        if (
          selection.startIndex < selection.endIndex &&
          (endItem == null || true === endItem.hasNoSelection())
        ) {
          selection.endIndex--
        } else if (
          selection.endIndex < selection.startIndex &&
          (startItem == null || true === startItem.hasNoSelection())
        ) {
          selection.startIndex--
        }
      }
    } else {
      const curRunIndex = hyperlink.state.elementIndex
      const run = hyperlink.children.at(curRunIndex)
      if (run) {
        const newElements = applyTextPrToParaRun(
          run,
          textPr,
          isIncreaseFontSize,
          false
        )!

        const newRunIndex = _replaceRunInHyperlink(
          hyperlink,
          curRunIndex,
          newElements
        )
        hyperlink.state.elementIndex = newRunIndex
      }
    }
  }
}
function _replaceRunInHyperlink(
  hyperlink: ParaHyperlink,
  index: number,
  newRuns: Array<Nullable<ParaRun>>
) {
  const runOfLeft = newRuns[0]
  const runOfCenter = newRuns[1]
  const runOfRight = newRuns[2]

  let runIndex = index

  // most left nonempty run is already parent's child, and the rest need to be added
  if (null != runOfLeft) {
    hyperlink.addItemAt(index + 1, runOfCenter!, true)
    runIndex = index + 1
  }

  if (null != runOfRight) hyperlink.addItemAt(runIndex + 1, runOfRight, true)

  return runIndex
}
