import { Dict } from '../../../../../liber/pervasive'
import { enlargeByRetinaRatio } from '../../../../common/browserInfo'
import { Factor_mm_to_pix } from '../../../common/const/unit'
import { CanvasDrawer, getCanvasDrawer } from '../../../graphic/CanvasDrawer'
import { drawShapeByInfo } from '../../../render/drawShape/drawShape'
import { FillEffects } from '../../../SlideElement/attrs/fill/fill'
import { ParagraphRenderStateElements } from '../../typeset/render'

export function getLineBounds(
  paraRenderStateElements: ParagraphRenderStateElements
): {
  l: number
  t: number
  w: number
  h: number
} {
  const l = paraRenderStateElements.xTextStart
  const t = paraRenderStateElements.lineTop
  const w = paraRenderStateElements.lineWidth
  const h = paraRenderStateElements.lineBottom - t
  return { l, t, w, h }
}

let patCache: Dict<CanvasRenderingContext2D> = {}
export function clearTextFillPattCache() {
  if (Object.keys(patCache).length > 20) {
    patCache = {}
  }
}
export function getPattenCanvas(
  g: CanvasDrawer,
  w: number,
  h: number,
  zoom: number,
  fill: FillEffects
) {
  if (w <= 0 || h <= 0) {
    return
  }
  const pw = enlargeByRetinaRatio(w * Factor_mm_to_pix)
  const ph = enlargeByRetinaRatio(h * Factor_mm_to_pix)
  const k = `w${pw}h${ph}`
  let ctx = patCache[k]
  if (ctx) {
    ctx.clearRect(0, 0, pw, ph)
  } else {
    const canvas = document.createElement('canvas')
    canvas.width = pw
    canvas.height = ph
    ctx = canvas.getContext('2d')!
    if (ctx) {
      patCache[k] = ctx
    }
  }
  if (ctx) {
    drawPatten(ctx, pw, ph, fill)
  }
  return ctx
}

function drawPatten(
  ctx: CanvasRenderingContext2D,
  pw: number,
  ph: number,
  fill: FillEffects
) {
  const g = getCanvasDrawer(ctx, pw, ph)
  const drawInfo = {
    brush: fill,
    extX: pw,
    extY: ph,
  }

  drawShapeByInfo(drawInfo, g)
}
