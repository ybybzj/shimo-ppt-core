import { Nullable } from '../../../../../liber/pervasive'
import { VertAlignType } from '../../../common/const/attrs'
import { FontClass } from '../../../../fonts/const'
import { getFontMetrics } from '../../../../fonts/FontKit'
import { EditorUtil } from '../../../../globals/editor'
import { InstanceType } from '../../../instanceTypes'
import { getLastRunElementAtLine } from '../../../../re/export/textContent'
import { ColorRGBA } from '../../../color/type'
import { Factor_pt_to_mm, VertAlignFactor } from '../../../common/const/unit'
import { hookManager, Hooks } from '../../../hooks'
import { isEavertTextContent } from '../../../utilities/shape/getters'
import { CursorPosition, ParagraphItem } from '../../type'
import { LineSegmentIndexesUtil } from '../LineSegmentIndexes'
import { ParaRun } from '../ParaRun'

export function isCursorAtEndInParaItem(paraItem: ParagraphItem) {
  if (paraItem.children.length <= 0) return paraItem.state.elementIndex >= 0
  if (paraItem.instanceType === InstanceType.ParaRun) {
    if (paraItem.state.elementIndex >= paraItem.children.length) return true

    return false
  } else {
    let index = paraItem.children.length - 1
    while (index > paraItem.state.elementIndex && index > 0) {
      if (true === paraItem.children.at(index)!.isEmpty()) index--
      else return false
    }

    return isCursorAtEndInParaItem(paraItem.children.at(index)!)
  }
}

export function isCursorAtStartInParaItem(paraItem: ParagraphItem) {
  if (paraItem.children.length <= 0) return paraItem.state.elementIndex <= 0

  if (paraItem.instanceType === InstanceType.ParaRun) {
    if (paraItem.state.elementIndex <= 0) return true
    return false
  } else {
    let i = 0
    while (
      i < paraItem.state.elementIndex &&
      i < paraItem.children.length - 1
    ) {
      if (true === paraItem.children.at(i)!.isEmpty()) i++
      else return false
    }

    return paraItem.children.at(i)!.state.elementIndex <= 0
  }
}

export function isCursorNeededCorrectPos(paraItem: ParagraphItem) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    if (paraItem.isPresentationField()) {
      return false
    } else {
      if (true === paraItem.isEmpty()) return true

      let newSegmentStart = false
      let segmentEnd = false

      const index = paraItem.state.elementIndex

      const lineSegmentIndexes = paraItem.lineSegmentIndexes
      const linesCount =
        LineSegmentIndexesUtil.getLinesCount(lineSegmentIndexes)
      for (let lineIndex = 0; lineIndex < linesCount; lineIndex++) {
        const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
          lineSegmentIndexes,
          lineIndex
        )
        const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
          lineSegmentIndexes,
          lineIndex
        )

        if (0 !== lineIndex) {
          if (index === startIndex) {
            newSegmentStart = true
          }
        }

        if (index === endIndex) {
          segmentEnd = true
        }

        if (true === newSegmentStart) break
      }

      if (
        true !== newSegmentStart &&
        true !== segmentEnd &&
        paraItem.state.elementIndex <= 0
      ) {
        return true
      }

      return false
    }
  } else {
    return false
  }
}

function calculateCursorPosForParaRun(
  run: ParaRun,
  x: number,
  y: number,
  isCurrentRun: boolean,
  lineIndex: number,
  colIndex: number,
  isUpdateCursorPos: boolean,
  isUpdateCursor: boolean,
  isReturnTarget: boolean
): CursorPosition {
  const para = run.paragraph

  const isEaVert = isEavertTextContent(para.parent?.parent)

  // 在该 segment 位于第几行
  const curLine = lineIndex - run.startParaLine
  const startPosIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
    run.lineSegmentIndexes,
    curLine
  )
  let endPosIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
    run.lineSegmentIndexes,
    curLine
  )

  let index = startPosIndex
  endPosIndex =
    true === isCurrentRun
      ? Math.min(endPosIndex, run.state.elementIndex)
      : endPosIndex

  const lastRunItemAtLine = getLastRunElementAtLine(para, lineIndex)

  for (; index < endPosIndex; index++) {
    const item = run.children.at(index)
    if (item) {
      x +=
        item === lastRunItemAtLine || item.instanceType === InstanceType.ParaEnd
          ? item.getActualWidth() // in case the letter has surrounding space
          : item.getVisibleWidth()
    }
  }

  if (true === isCurrentRun && index === run.state.elementIndex) {
    if (true === isUpdateCursorPos) {
      para.cursorPosition.x = x
      para.cursorPosition.y = y
      para.cursorPosition.colIndex = colIndex
      // update cursor
      if (true === isUpdateCursor) {
        const curTextPr = run.getCalcedPr(false)
        const fontFactor = curTextPr.getFontFactor()

        const {
          textHeight: height,
          ascender: ascender,
          vertTextYOffset,
        } = getFontMetrics(
          EditorUtil.FontKit,
          curTextPr,
          curTextPr.lang.fontClass ?? FontClass.ASCII,
          fontFactor
        )

        hookManager.invoke(Hooks.EditorUI.setCursorSize, {
          size: height,
          ascent: ascender,
        })

        let rgba: ColorRGBA
        hookManager.invoke(Hooks.EditorUI.OnUpdateCursorTransform, {
          transform: para.getTextTransformOfParent(),
        })
        if (curTextPr.fillEffects) {
          curTextPr.fillEffects.check(para.getTheme(), para.getColorMap())
          rgba = curTextPr.fillEffects.getRGBAColor()
          hookManager.invoke(Hooks.EditorUI.setCursorColor, {
            r: rgba.r,
            g: rgba.g,
            b: rgba.b,
          })
        } else {
          hookManager.invoke(Hooks.EditorUI.setCursorColor, {
            r: 0,
            g: 0,
            b: 0,
          })
        }

        let cursorY = y - ascender
        if (isEaVert) {
          cursorY += vertTextYOffset
        }
        switch (curTextPr.vertAlign) {
          case VertAlignType.SubScript: {
            cursorY -=
              curTextPr.fontSize! * Factor_pt_to_mm * VertAlignFactor.Sub
            break
          }
          case VertAlignType.SuperScript: {
            cursorY -=
              curTextPr.fontSize! * Factor_pt_to_mm * VertAlignFactor.Super
            break
          }
        }

        const slideIndex = para.getParentSlideIndex()

        hookManager.invoke(Hooks.EditorUI.UpdateCursor, {
          x: x,
          y: cursorY,
          slideIndex: slideIndex,
        })
      }
    }

    if (true === isReturnTarget) {
      const curTextPr = run.getCalcedPr(false)
      const fontFactor = curTextPr.getFontFactor()

      const {
        textHeight: height,
        ascender: ascender,
        vertTextYOffset,
      } = getFontMetrics(
        EditorUtil.FontKit,
        curTextPr,
        curTextPr.lang.fontClass ?? FontClass.ASCII,
        fontFactor
      )

      let cursorY = y - ascender
      if (isEaVert) {
        cursorY += vertTextYOffset
      }
      switch (curTextPr.vertAlign) {
        case VertAlignType.SubScript: {
          cursorY -= curTextPr.fontSize! * Factor_pt_to_mm * VertAlignFactor.Sub
          break
        }
        case VertAlignType.SuperScript: {
          cursorY -=
            curTextPr.fontSize! * Factor_pt_to_mm * VertAlignFactor.Super
          break
        }
      }

      return {
        x: x,
        y: cursorY,
        height: height,
        lineAndCol: { line: curLine, colIndex },
      }
    } else {
      return {
        x: x,
        y: y,
        lineAndCol: { line: curLine, colIndex },
      }
    }
  }

  return {
    x: x,
    y: y,
    lineAndCol: { line: curLine, colIndex },
  }
}

export function calculateCursorPosForParaItem(
  paraItem: ParagraphItem,
  x: number,
  y: number,
  isCurrentRun: boolean,
  lineIndex: number,
  colIndex: number,
  isUpdateCursorPos: boolean,
  isUpdateCursor: boolean,
  isReturnTarget: boolean
): CursorPosition {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    const curLine = lineIndex - paraItem.startParaLine

    const lineSegmentIndexes = paraItem.lineSegmentIndexes
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      curLine
    )
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      curLine
    )

    for (let i = startIndex; i <= endIndex; i++) {
      const item = paraItem.children.at(i)
      if (item) {
        const _isCurrentRun =
          true === isCurrentRun && i === paraItem.state.elementIndex
        const result = calculateCursorPosForParaRun(
          item,
          x,
          y,
          _isCurrentRun,
          lineIndex,
          colIndex,
          isUpdateCursorPos,
          isUpdateCursor,
          isReturnTarget
        )

        if (_isCurrentRun) {
          return result
        } else {
          x = result.x
        }
      }
    }

    return { x: x, y: y }
  } else {
    return calculateCursorPosForParaRun(
      paraItem,
      x,
      y,
      isCurrentRun,
      lineIndex,
      colIndex,
      isUpdateCursorPos,
      isUpdateCursor,
      isReturnTarget
    )
  }
}

export function stopCursorOnEntryExit(paraItem: Nullable<ParagraphItem>) {
  if (paraItem && paraItem.instanceType === InstanceType.ParaRun) {
    return paraItem.isPresentationField()
  } else {
    return false
  }
}
