import { Nullable } from '../../../../../liber/pervasive'
import { VertAlignType } from '../../../common/const/attrs'
import { FontClass } from '../../../../fonts/const'
import { getFontMetrics } from '../../../../fonts/FontKit'
import { EditorUtil } from '../../../../globals/editor'
import { InstanceType } from '../../../instanceTypes'
import {
  LineJoinTypeOO,
  PresetLineDashStyleValues,
} from '../../../../io/dataType/spAttrs'
import {
  contentPosFromIndices,
  getRunElementByIndices,
  increaseRunElementIndices,
  RunAndHyperlinkInfo,
  updateContentPosByIndices,
} from '../../../../re/export/textContent'
import { RunElementIndices } from '../../../../re/TextContent/ParagraphContentPosition.gen'
import {
  Factor_pt_to_mm,
  HightLight_None,
  VertAlignFactor,
} from '../../../common/const/unit'
import { EditorSettings } from '../../../common/EditorSettings'
import { getLnInfo } from '../../../graphic/ShapeRenderingState'
import { hookManager, Hooks } from '../../../hooks'
import { CComplexColor } from '../../../color/complexColor'
import {
  gHLinkColor,
  gVisitedHLinkColor,
} from '../../../SlideElement/attrs/creators'
import { FillEffects } from '../../../SlideElement/attrs/fill/fill'
import { CHexColor } from '../../../color/CHexColor'
import { TextPr } from '../../../textAttributes/TextPr'
import { isEavertTextContent } from '../../../utilities/shape/getters'
import { Paragraph } from '../../Paragraph'
import { ParaText } from '../../RunElement/ParaText'
import { RunContentElement } from '../../RunElement/type'
import { CursorPosition, RunElemnetOrderIndicesInfo } from '../../type'
import { ParagraphSearchPositionInfo } from '../../typeset/ParaRenderingState'
import { ParaRun } from '../ParaRun'
import {
  ManagedArray,
  ManagedSlice,
  ManagedSliceUtil,
} from '../../../../common/managedArray'
import { Theme } from '../../../SlideElement/attrs/theme'
import { ClrMap } from '../../../color/clrMap'
import { ColorRGBA } from '../../../color/type'

export function getWidthAtIndices(
  paragragh: Paragraph,
  indices: RunElementIndices
) {
  const rElm = getRunElementByIndices(paragragh, indices)
  return getWidthOfRunElement(rElm)
}

export function getWidthOfRunElement(rElm: Nullable<RunContentElement>) {
  if (rElm == null) return 0
  return rElm.instanceType === InstanceType.ParaEnd
    ? rElm.getWidth()
    : rElm.getVisibleWidth()
}

export function searchPositionInfoForRunElement(
  paragraph: Paragraph,
  searchInfo: ParagraphSearchPositionInfo,
  indicesInfo: RunElemnetOrderIndicesInfo,
  prevIndicesInfo: Nullable<RunElemnetOrderIndicesInfo>,
  drawPosStatus: 'end' | 'start' | 'between'
): Nullable<X_PosInfoInLine> {
  // Check if we hit the given element
  const curRunElm = getRunElementByIndices(paragraph, indicesInfo.indices)
  const curWidth = getWidthOfRunElement(curRunElm)
  if (curRunElm && curRunElm.instanceType === InstanceType.ParaEnd) {
    searchInfo.paraEndIndices = indicesInfo.indices
  }

  const diff = searchInfo.x - searchInfo.cursorX
  if (diff < -0.001 && drawPosStatus === 'start') {
    return {
      state: 'leftOfTexts',
      leftIndices: indicesInfo,
    }
  }

  if (diff > curWidth + 0.001 && drawPosStatus === 'end') {
    return {
      state: 'rightOfTexts',
      rightIndices: indicesInfo,
    }
  }

  if (searchInfo.allowInText || searchInfo.x > searchInfo.cursorX) {
    if (Math.abs(diff) < 0.001) {
      // between
      return prevIndicesInfo
        ? {
            state: 'betweenLetters',
            leftIndices: prevIndicesInfo,
            rightIndices: indicesInfo,
          }
        : {
            state: 'inTextLetter',
            whichHalf: 'leftHalf',
            selfIndices: indicesInfo,
          }
    } else if (diff <= curWidth - 0.001) {
      // inTextLetter
      const whichHalf = diff > curWidth / 2 ? 'rightHalf' : 'leftHalf'
      return {
        state: 'inTextLetter',
        selfIndices: indicesInfo,
        whichHalf,
      }
    }
  }
  searchInfo.cursorX += curWidth

  return undefined
}

interface X_leftOfTexts {
  state: 'leftOfTexts'
  leftIndices: RunElemnetOrderIndicesInfo
}
interface X_inTextLetter {
  state: 'inTextLetter'
  whichHalf: 'leftHalf' | 'rightHalf'
  selfIndices: RunElemnetOrderIndicesInfo
}

interface X_betweenLetters {
  state: 'betweenLetters'
  leftIndices: RunElemnetOrderIndicesInfo
  rightIndices: RunElemnetOrderIndicesInfo
}

interface X_rightOfTexts {
  state: 'rightOfTexts'
  rightIndices: RunElemnetOrderIndicesInfo
}

type X_PosInfoInLine =
  | X_leftOfTexts
  | X_inTextLetter
  | X_betweenLetters
  | X_rightOfTexts

// type X_PosStateType = X_PosInfoInLine['state']

export function updateSearchPInfoByXPosInfo(
  paragragh: Paragraph,
  searchInfo: ParagraphSearchPositionInfo,
  xInfo: X_PosInfoInLine
) {
  const isRTL = paragragh.isRTL
  const hasParaEnd = searchInfo.paraEndIndices != null
  switch (xInfo.state) {
    case 'leftOfTexts': {
      searchInfo.hitTextContent = false
      if (isRTL === true && hasParaEnd === true) {
        // pos should be at paraEnd
        updateContentPosByIndices(searchInfo.pos, searchInfo.paraEndIndices!)
      } else {
        const { indices: leftIndices, dir } = xInfo.leftIndices
        const isR = dir === 'R'
        updateContentPosByIndices(
          searchInfo.pos,
          isR
            ? increaseRunElementIndices(paragragh, leftIndices) ?? leftIndices
            : leftIndices
        )
      }
      break
    }
    case 'rightOfTexts': {
      searchInfo.hitTextContent = false
      if (isRTL !== true && hasParaEnd === true) {
        // pos should be at paraEnd
        updateContentPosByIndices(searchInfo.pos, searchInfo.paraEndIndices!)
      } else {
        const { indices: rightIndices, dir } = xInfo.rightIndices
        const isR = dir === 'R'
        updateContentPosByIndices(
          searchInfo.pos,
          isR
            ? rightIndices
            : increaseRunElementIndices(paragragh, rightIndices) ?? rightIndices
        )
      }
      break
    }
    case 'inTextLetter': {
      searchInfo.hitTextContent = true
      const { indices: selfIndices, dir } = xInfo.selfIndices
      updateContentPosByIndices(searchInfo.positionInText, selfIndices)
      const isR = dir === 'R'

      const nextPos = _getPosByInceaseFromIndices(selfIndices)

      if (xInfo.whichHalf === 'leftHalf') {
        if (isR) {
          searchInfo.pos.setByPosition(nextPos)
        } else {
          updateContentPosByIndices(searchInfo.pos, selfIndices)
        }
      } else {
        if (isR) {
          updateContentPosByIndices(searchInfo.pos, selfIndices)
        } else {
          searchInfo.pos.setByPosition(nextPos)
        }
      }

      break
    }
    case 'betweenLetters': {
      searchInfo.hitTextContent = true

      const { indices: leftIndices, dir: leftDir } = xInfo.leftIndices
      const { indices: rightIndices, dir: rightDir } = xInfo.rightIndices
      const isLeft_R = leftDir === 'R'
      const isRight_R = rightDir === 'R'

      switch (true) {
        case !isLeft_R && !isRight_R: {
          updateContentPosByIndices(searchInfo.pos, rightIndices)
          break
        }
        case isLeft_R && isRight_R: {
          updateContentPosByIndices(searchInfo.pos, leftIndices)
          break
        }
        case !isLeft_R && isRight_R: {
          const nextPos = _getPosByInceaseFromIndices(leftIndices)

          searchInfo.pos.setByPosition(nextPos)

          break
        }
        case isLeft_R && !isRight_R: {
          if (isRTL) {
            const nextPos = _getPosByInceaseFromIndices(rightIndices)
            searchInfo.pos.setByPosition(nextPos)
          } else {
            updateContentPosByIndices(searchInfo.pos, rightIndices)
          }
          break
        }
      }

      searchInfo.positionInText.setByPosition(searchInfo.pos)

      break
    }
  }
}

function _getPosByInceaseFromIndices(indices: RunElementIndices) {
  const nextPos = contentPosFromIndices(indices)
  nextPos.increaseByOne()
  return nextPos
}

export function isArabicElement(elm: RunContentElement) {
  return elm.instanceType === InstanceType.ParaText && elm.isArabic()
}

export function updateTextCursor(
  run: ParaRun,
  paragraph: Paragraph,
  x: number,
  y: number,
  colIndex: number,
  curLine: number,
  isUpdateCursorPos: boolean,
  isUpdateCursor: boolean,
  isReturnTarget: boolean
): CursorPosition {
  const isEaVert = isEavertTextContent(paragraph.parent?.parent)
  if (true === isUpdateCursorPos) {
    paragraph.cursorPosition.x = x
    paragraph.cursorPosition.y = y
    paragraph.cursorPosition.colIndex = colIndex
    if (true === isUpdateCursor) {
      const curTextPr = run.getCalcedPr(false)
      const dFontFactor = curTextPr.getFontFactor()

      const {
        textHeight: height,
        ascender: ascender,
        vertTextYOffset,
      } = getFontMetrics(
        EditorUtil.FontKit,
        curTextPr,
        curTextPr.lang.fontClass ?? FontClass.ASCII,
        dFontFactor
      )

      hookManager.invoke(Hooks.EditorUI.setCursorSize, {
        size: height,
        ascent: ascender,
      })

      hookManager.invoke(Hooks.EditorUI.OnUpdateCursorTransform, {
        transform: paragraph.getTextTransformOfParent(),
      })
      if (curTextPr.fillEffects) {
        curTextPr.fillEffects.check(
          paragraph.getTheme(),
          paragraph.getColorMap()
        )
        const rgba = curTextPr.fillEffects.getRGBAColor()
        hookManager.invoke(Hooks.EditorUI.setCursorColor, {
          r: rgba.r,
          g: rgba.g,
          b: rgba.b,
        })
      } else {
        const bgColor = paragraph.parent?.getTextBgColor()
        let autoColor =
          null != bgColor && false === bgColor.isDark()
            ? new CHexColor(255, 255, 255, false)
            : new CHexColor(0, 0, 0, false)
        const theme = paragraph.getTheme(),
          colorMap = paragraph.getColorMap()
        if (
          (bgColor == null || bgColor.auto) &&
          curTextPr.fontRef &&
          curTextPr.fontRef.color
        ) {
          curTextPr.fontRef.color.check(theme, colorMap)
          const rgba = curTextPr.fontRef.color.rgba
          autoColor = new CHexColor(rgba.r, rgba.g, rgba.b, true)
        }

        hookManager.invoke(Hooks.EditorUI.setCursorColor, {
          r: autoColor.r,
          g: autoColor.g,
          b: autoColor.b,
        })
      }

      let yOfCursor = y - ascender
      if (isEaVert) {
        yOfCursor += vertTextYOffset
      }
      switch (curTextPr.vertAlign) {
        case VertAlignType.SubScript: {
          yOfCursor -=
            curTextPr.fontSize! * Factor_pt_to_mm * VertAlignFactor.Sub
          break
        }
        case VertAlignType.SuperScript: {
          yOfCursor -=
            curTextPr.fontSize! * Factor_pt_to_mm * VertAlignFactor.Super
          break
        }
      }

      const slideIndex = paragraph.getParentSlideIndex()

      hookManager.invoke(Hooks.EditorUI.UpdateCursor, {
        x: x,
        y: yOfCursor,
        slideIndex: slideIndex,
      })
    }
  }

  if (true === isReturnTarget) {
    const curTextPr = run.getCalcedPr(false)
    const fontFactor = curTextPr.getFontFactor()

    const {
      textHeight: height,
      ascender: ascender,
      vertTextYOffset,
    } = getFontMetrics(
      EditorUtil.FontKit,
      curTextPr,
      curTextPr.lang.fontClass ?? FontClass.ASCII,
      fontFactor
    )

    let yOfCursor = y - ascender
    if (isEaVert) {
      yOfCursor += vertTextYOffset
    }
    switch (curTextPr.vertAlign) {
      case VertAlignType.SubScript: {
        yOfCursor -= curTextPr.fontSize! * Factor_pt_to_mm * VertAlignFactor.Sub
        break
      }
      case VertAlignType.SuperScript: {
        yOfCursor -=
          curTextPr.fontSize! * Factor_pt_to_mm * VertAlignFactor.Super
        break
      }
    }

    return {
      x: x,
      y: yOfCursor,
      height: height,
      lineAndCol: { line: curLine, colIndex },
    }
  } else {
    return {
      x: x,
      y: y,
      lineAndCol: { line: curLine, colIndex },
    }
  }
}

export function getFontNameForRun(run: ParaRun) {
  return run && run.getCalcedPr(false).fontFamily?.name
}

export interface ParaTextProcessItem {
  paraText: ParaText
  parentInfo: RunAndHyperlinkInfo
}

export type TextFragmentProcessor = (
  textElemArr: ManagedSlice<ParaText>,
  parentInfo: RunAndHyperlinkInfo
) => void
export type TextElementFallbackHandler = (
  textElmArr: ManagedSlice<ParaTextProcessItem>
) => void

const sliceMapper = ({ paraText }) => paraText
export function makeTextContentProcessor() {
  const processItems: ManagedArray<ParaTextProcessItem> =
    new ManagedArray<ParaTextProcessItem>()
  let lastParentInfo: Nullable<RunAndHyperlinkInfo>
  let lastItem: Nullable<ParaText>
  let needFallback = false
  const reset = () => {
    processItems.reset()
    lastParentInfo = null
    lastItem = undefined
    needFallback = false
  }

  const collect = (paraText: ParaText, parentInfo: RunAndHyperlinkInfo) => {
    if (
      needFallback === false &&
      lastParentInfo != null &&
      !isColorCompatibleForRunAndLinkInfo(lastParentInfo, parentInfo)
    ) {
      needFallback = true
    }
    lastParentInfo = parentInfo
    lastItem = paraText
    processItems.push({ paraText, parentInfo })
  }

  const _process = (
    processor: TextFragmentProcessor,
    fallback: TextElementFallbackHandler
  ) => {
    const len = processItems.length
    if (len > 0 && lastParentInfo) {
      const slicedCachedArray = ManagedSliceUtil.slice(processItems, 0, len)
      if (needFallback) {
        fallback(slicedCachedArray)
      } else {
        const _slicedCachedArray = ManagedSliceUtil.map(
          slicedCachedArray,
          sliceMapper
        )
        processor(_slicedCachedArray, lastParentInfo)
      }
    }
    reset()
  }

  const process = (
    runElement: Nullable<RunContentElement>,
    parentInfo: Nullable<RunAndHyperlinkInfo>,
    processor: TextFragmentProcessor,
    fallback: TextElementFallbackHandler
  ) => {
    if (
      runElement?.instanceType === InstanceType.ParaText &&
      parentInfo != null &&
      runElement.isArabic()
    ) {
      if (lastParentInfo == null) {
        reset()
        collect(runElement, parentInfo)
      } else {
        const run = parentInfo.run
        const curTextPr = run.getCalcedTextPr(false)

        const lastRun = lastParentInfo.run
        const lastTextPr = lastRun.getCalcedTextPr(false)

        if (
          !lastTextPr.isCompatible(curTextPr) ||
          runElement.form !== lastItem?.form
        ) {
          _process(processor, fallback)
        }
        collect(runElement, parentInfo)
      }
    } else {
      _process(processor, fallback)
      return runElement
    }
  }

  return {
    process,
    reset,
  }
}

export function canUseHB(textPr: TextPr) {
  if (EditorUtil.FontKit.hb == null || EditorSettings.isUseHarfBuzz === false) {
    return false
  }
  const hbFont = EditorUtil.FontKit.getHBFont(textPr)
  return !!hbFont
}

export interface TextDrawingColorInfo {
  brushColor: {
    r: number
    g: number
    b: number
    a: number
    auto: CHexColor
  }
  ln?: {
    r: number
    g: number
    b: number
    a: number
    w: number
    prstDash?: PresetLineDashStyleValues
    joinType?: LineJoinTypeOO
  }
  fill?: FillEffects
  highlight?: CComplexColor
}

function computeTextDrawingColorInfo(
  textPr: TextPr,
  { isInLink, isVisited }: RunAndHyperlinkInfo,
  bgColor: Nullable<CHexColor>,
  colorMap?: Nullable<ClrMap>,
  theme?: Nullable<Theme>
): TextDrawingColorInfo {
  let rgba: ColorRGBA,
    brushColor: TextDrawingColorInfo['brushColor'],
    fill: TextDrawingColorInfo['fill']

  let autoColor =
    null != bgColor && false === bgColor.isDark()
      ? new CHexColor(255, 255, 255, false)
      : new CHexColor(0, 0, 0, false)
  if (
    (bgColor == null || bgColor.auto) &&
    textPr.fontRef &&
    textPr.fontRef.color
  ) {
    textPr.fontRef.color.check(theme, colorMap)
    rgba = textPr.fontRef.color.rgba
    autoColor = new CHexColor(rgba.r, rgba.g, rgba.b, true)
  }

  if (textPr.fillEffects) {
    textPr.fillEffects.check(theme, colorMap)
    fill = textPr.fillEffects
    rgba = textPr.fillEffects.getRGBAColor()

    if (true === isVisited) {
      gVisitedHLinkColor().check(theme, colorMap)
      rgba = gVisitedHLinkColor().getRGBAColor()
    } else {
      if (isInLink === true) {
        gHLinkColor().check(theme, colorMap)
        rgba = gHLinkColor().getRGBAColor()
      }
    }
    brushColor = {
      r: rgba.r,
      g: rgba.g,
      b: rgba.b,
      a: rgba.a,
      auto: autoColor,
    }
  } else {
    // Todo, previous error this.textPr
    if (true === isVisited && null == textPr.fillEffects) {
      gVisitedHLinkColor().check(theme, colorMap)
      rgba = gVisitedHLinkColor().getRGBAColor()
      brushColor = {
        r: rgba.r,
        g: rgba.g,
        b: rgba.b,
        a: rgba.a,
        auto: autoColor,
      }
    } else {
      brushColor = {
        r: 0,
        g: 0,
        b: 0,
        a: 255,
        auto: autoColor,
      }
    }
  }

  const ln = textPr.textStroke
  if (ln?.fillEffects) {
    ln.fillEffects.check(theme, colorMap)
  }
  const lnInfo = getLnInfo(ln)

  const highlight =
    textPr.highLight && textPr.highLight !== HightLight_None
      ? textPr.highLight
      : undefined
  return {
    brushColor,
    ln:
      lnInfo && lnInfo.noStroke === false
        ? {
            r: lnInfo.StrokeRGBAColor!.r,
            g: lnInfo.StrokeRGBAColor!.g,
            b: lnInfo.StrokeRGBAColor!.b,
            a: lnInfo.StrokeRGBAColor!.a,
            w: lnInfo.strokeWidth!,
            prstDash: ln?.prstDash,
            joinType: ln?.Join?.type,
          }
        : undefined,
    fill,
    highlight,
  }
}

export function makeTextBrushGetter() {
  const TextColorInfoCache: Map<string, TextDrawingColorInfo> = new Map()
  function getBrushColorFunc(
    textPr: TextPr,
    runInfo: RunAndHyperlinkInfo,
    bgColor: any,
    colorMap: any,
    theme: any
  ): TextDrawingColorInfo {
    const { run } = runInfo
    const rId = run.id
    let textColorInfo = TextColorInfoCache.get(rId)
    if (textColorInfo != null) {
      return textColorInfo
    }
    textColorInfo = computeTextDrawingColorInfo(
      textPr,
      runInfo,
      bgColor,
      colorMap,
      theme
    )
    TextColorInfoCache.set(rId, textColorInfo)
    return textColorInfo
  }

  return {
    get: getBrushColorFunc,
    clear() {
      TextColorInfoCache.clear()
    },
  }
}
export const gTextBrushGetter = makeTextBrushGetter()
function isColorCompatibleForRunAndLinkInfo(
  info1: RunAndHyperlinkInfo,
  info2: RunAndHyperlinkInfo
) {
  if (info1 === info2) {
    return true
  }
  const run1 = info1.run
  const run2 = info2.run
  if (run1 === run2) {
    return true
  }

  const textPr1 = run1.getCalcedTextPr(false)
  const textPr2 = run2.getCalcedTextPr(false)

  const para1 = run1.paragraph
  const para2 = run2.paragraph

  const theme1 = para1.getTheme()
  const colorMap1 = para1.getColorMap()
  const bgColor1 = para1.parent?.getTextBgColor()!

  const theme2 = para2.getTheme()
  const colorMap2 = para2.getColorMap()
  const bgColor2 = para2.parent?.getTextBgColor()!

  const {
    brushColor: color1,
    ln: ln1,
    highlight: highlight1,
  } = computeTextDrawingColorInfo(textPr1, info1!, bgColor1, colorMap1, theme1)
  const {
    brushColor: color2,
    ln: ln2,
    highlight: highlight2,
  } = computeTextDrawingColorInfo(textPr2, info2!, bgColor2, colorMap2, theme2)

  const hasHighlight1 = !!highlight1
  const hasHighlight2 = !!highlight2

  const isHightlightEqual = hasHighlight1 === hasHighlight2

  const isBrushEqual =
    color1.a === color2.a &&
    color1.b === color2.b &&
    color1.r === color2.r &&
    color1.g === color2.g

  const isLnEqual =
    (ln1 == null && ln2 == null) ||
    (ln1 != null &&
      ln2 != null &&
      ln1.r === ln2.r &&
      ln1.g === ln2.g &&
      ln1.b === ln2.b &&
      ln1.a === ln2.a &&
      ln1.w === ln2.w)
  return isBrushEqual && isLnEqual && isHightlightEqual
}
