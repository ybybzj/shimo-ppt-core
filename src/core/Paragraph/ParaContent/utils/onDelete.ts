import { InstanceType } from '../../../instanceTypes'
import { ParaHyperlink } from '../ParaHyperlink'
import { ParaRun } from '../ParaRun'

export function beforeDeleteParaItem(paraItem: ParaHyperlink | ParaRun) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    for (let i = 0, l = paraItem.children.length; i < l; i++) {
      const run = paraItem.children.at(i)
      if (run) {
        delete run.pr?.hlink
      }
    }
  }
}
