import { Nullable } from '../../../../../liber/pervasive'
import { InstanceType } from '../../../instanceTypes'
import { ParagraphElementPosition } from '../../common'
import { ParaHyperlink } from '../ParaHyperlink'
import { ParaRun } from '../ParaRun'

export function getPosIndexByContentPosition(
  entity: ParaHyperlink | ParaRun,
  flag: 0 | 1 | -1,
  elementPosition: Nullable<ParagraphElementPosition>,
  lvl: number
): number {
  let index = 0
  switch (flag) {
    case 0:
      index = elementPosition!.get(lvl)
      break
    case 1:
      index = 0
      break
    case -1:
      index =
        entity.instanceType === InstanceType.ParaRun
          ? entity.children.length
          : entity.children.length - 1
      break
  }
  return index
}
