import { InstanceType } from '../../../instanceTypes'
import { ParagraphElementPosition } from '../../common'
import { ParaHyperlink } from '../ParaHyperlink'
import { ParaRun } from '../ParaRun'

export function hasAdjacentPosInfoOfParaItem(
  paraItem: ParaRun | ParaHyperlink
) {
  if (paraItem.adjacentPositions.length > 0) return true

  return false
}
export function updateParaContentPosition(
  paraItem: ParaRun | ParaHyperlink,
  isSelection: boolean,
  isStart: boolean,
  elementPosition: ParagraphElementPosition
) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    let pos =
      true !== isSelection
        ? paraItem.state.elementIndex
        : false !== isStart
        ? paraItem.state.selection.startIndex
        : paraItem.state.selection.endIndex

    if (pos < 0) pos = 0

    if (pos > paraItem.children.length) pos = paraItem.children.length

    elementPosition.add(pos)
  } else {
    const pos =
      true === isSelection
        ? true === isStart
          ? paraItem.state.selection.startIndex
          : paraItem.state.selection.endIndex
        : paraItem.state.elementIndex
    elementPosition.add(pos)

    if (pos < 0 || pos >= paraItem.children.length) return

    updateParaContentPosition(
      paraItem.children.at(pos)!,
      isSelection,
      isStart,
      elementPosition
    )
  }
}
