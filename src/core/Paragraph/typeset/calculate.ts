import { Paragraph } from '../Paragraph'
import { ParaLineInfo } from '../../common/const/paragraph'
import {
  MaxWidthLimitForTextWarpNone,
  ParaCalcStatus,
  ParaCalcStatusValues,
} from '../../common/const/doc'
import {
  gParagraphCalcState,
  gParagraphCalcAlignState,
  gParagraphCalcCounterState,
  ParagraphCalcState,
} from './ParagraphCalcState'
import { AlignKind } from '../../common/const/attrs'
import { FontClass } from '../../../fonts/const'
import { normalizeMMToTwips, isNumber, nowFn } from '../../../common/utils'
import { EditorUtil } from '../../../globals/editor'
import { InstanceType, isInstanceTypeOf } from '../../instanceTypes'
import { ParaPr } from '../../textAttributes/ParaPr'
import { ParaColumn } from './ParaRenderingState'
import { checkIsInDebug, logger } from '../../../lib/debug/log'
import {
  calcAndSetParaItemSegmentEndPos,
  calcLineMetricsForParaItem,
  calcParaItemSegment,
  calcParaItemSegmentSpaces,
  calcParaItemSegmentWidth,
  resetParaItemCalcStatus,
} from '../ParaContent/typeset/calculate'
import { LEVEL_PARAGRAPH } from '../common'
import { getPosByLineAndSegment } from '../utils/typeset'
import {
  compareIndices,
  runElementIndicesFromPos,
} from '../../../re/export/textContent'
import { getFontMetrics } from '../../../fonts/FontKit'
import { ParagraphItem } from '../type'
import { clearClosestPositions } from '../utils/position'
import { LineSegmentIndexesUtil } from '../ParaContent/LineSegmentIndexes'
import { ParaSpacing } from '../../textAttributes/ParaSpacing'

export function calculateParagraphByColumn(
  paragraph: Paragraph,
  colIndex: number
) {
  if (paragraph.parent == null) return ParaCalcStatus.End
  clearClosestPositions(paragraph)

  paragraph.cursorPosition.line = -1

  const status = _calcColumn(paragraph, colIndex)

  paragraph.renderingState.setCalcResultByColumn(colIndex, status)
  paragraph.hasCalculated = true

  return status
}

const CalcTimeBudget = 3000
const EnsureLineCount = 200
function _isInCalcBudget(curLine: number, timeElapsed: number) {
  if (checkIsInDebug() || timeElapsed <= CalcTimeBudget) return true
  return curLine <= EnsureLineCount
}

function _calcColumn(
  paragraph: Paragraph,
  colIndex: number
): ParaCalcStatusValues {
  const paraCalcState = gParagraphCalcState
  paraCalcState.resetParaColumn(paragraph, colIndex)

  const prs = paragraph.getSpacingAdjustedPrs()
  const paraPr = prs.paraPr

  let curLine = paragraph.renderingState.getStartLineForCalc(colIndex)

  calcAndAddColumnRenderingState(
    paragraph,
    curLine,
    colIndex,
    paraCalcState,
    paraPr
  )

  let calcStatus: ParaCalcStatusValues = ParaCalcStatus.NextLine

  const timingStart = nowFn()
  let timeElapsed = 0

  // while (true) { // for debug
  while (_isInCalcBudget(curLine, timeElapsed)) {
    paraCalcState.curLineIndex = curLine
    paraCalcState.calcStatus = ParaCalcStatus.NextLine

    calcAndAddLine(paragraph, curLine, colIndex, paraCalcState, paraPr)

    // for debug
    // paragraph.renderingState.printLineContent(curLine)

    calcStatus = paraCalcState.calcStatus

    timeElapsed = nowFn() - timingStart
    if (calcStatus & ParaCalcStatus.NextLine) {
      curLine++
    } else {
      break
    }
  }

  if (calcStatus === ParaCalcStatus.NextLine) {
    const column = paragraph.renderingState.columns.at(colIndex)
    column?.setEndLine(curLine - 1)
    calcStatus = ParaCalcStatus.NextElement
  }
  return calcStatus
}

function calcAndAddColumnRenderingState(
  paragraph: Paragraph,
  curLine: number,
  colIndex: number,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr
) {
  if (paragraph.parent == null) return

  let x
  let y
  let xLimit
  let yLimit
  const renderingState = paragraph.renderingState
  if (colIndex === 0) {
    x = renderingState.x
    y = renderingState.y
    xLimit = renderingState.xLimit
    yLimit = renderingState.yLimit
  } else {
    const absColumnIndex = paragraph.getIndexOfStartColumn() + colIndex
    const starColInfo =
      paragraph.parent.renderingState.getStartColumnDimensionInfo(
        absColumnIndex
      )

    x = starColInfo.x
    y = starColInfo.y
    xLimit = starColInfo.xLimit
    yLimit = starColInfo.yLimit
  }
  paraCalcState.xOfStart = x
  paraCalcState.xLimit = xLimit - paraPr.ind.right!
  paraCalcState.yLimit = yLimit
  paraCalcState.y = y
  renderingState.columns.resetLength(colIndex)
  renderingState.columns.insert(
    colIndex,
    ParaColumn.new(x, y, xLimit, yLimit, curLine)
  )
}

function calcAndAddLine(
  paragraph: Paragraph,
  curLine: number,
  colIndex: number,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr
) {
  // [paraCalcState.calcStatus] => ParacalcStatus.NextLine

  //  prepare the line with flow segment (segments) in current calc state
  resetLineAndFillSegments(paragraph, curLine, paraCalcState, paraPr)

  // We calculate the segments of this line

  calcLineSegments(paragraph, curLine, paraCalcState, paraPr)

  // Fill in the information about the line
  updateLineInfo(paragraph, curLine, paraCalcState)

  // We calculate the metrics of this line
  calcMetricsForLine(paragraph, curLine, paraCalcState, paraPr)

  // Calculate the line height, as well as the position of the upper and lower borders
  calcLinePosition(paragraph, curLine, colIndex, paraCalcState, paraPr)

  //  Check if the given line has reached the end of the column
  if (
    false ===
    checkBoundForLineBottom(paragraph, curLine, colIndex, paraCalcState)
  ) {
    // [paraCalcState.calcStatus] => ParacalcStatus.NextColumn
    return
  }

  // Set the vertical offset of this line
  calcLineBaseLine(paragraph, curLine, colIndex, paraCalcState)

  // calculate the offsets of elements within the paragraph and the visible widths of the spaces, depending on align.
  calcLineAlign(paragraph, curLine, colIndex, paraCalcState, paraPr)

  calcLineEnd(paragraph, curLine, colIndex, paraCalcState)
  //maybe [paraCalcState.calcStatus] => ParacalcStatus.NextElement
}

function resetLineAndFillSegments(
  paragraph: Paragraph,
  curLine: number,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr
) {
  const renderingState = paragraph.renderingState
  renderingState.resetParaEnd()

  paraCalcState.resetLineMetricsState()

  paraCalcState.isFirstLine = curLine === 0

  // fill segments according to paraCalcState

  const indX =
    paraPr.ind.left! +
    (paraCalcState.isFirstLine ? paraPr.ind.getCalcFirstLine()! : 0)

  // Add a new line to the paragraph
  renderingState.addSegmentAtLine(
    curLine,
    paraCalcState.xOfStart + indX,
    paraCalcState.xLimit
  )
}

function calcLineSegments(
  paragraph: Paragraph,
  curLine: number,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr
) {
  const renderingState = paragraph.renderingState
  _calcSegment(paragraph, curLine, paraCalcState, paraPr)

  if (
    -1 === renderingState.paraEndInfo.line &&
    true === paraCalcState.isParaEnd
  ) {
    renderingState.paraEndInfo.line = curLine
  }

  return
}

function _calcSegment(
  paragraph: Paragraph,
  curLine: number,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr
) {
  // segment index always start at 0

  const renderingState = paragraph.renderingState

  const startIndex = renderingState.getStartItemIndex(curLine)

  renderingState.setSegmentStartPos(curLine, startIndex)

  const segIndex = renderingState.getSegment(curLine)
  if (segIndex == null) {
    logger.warn(`[para calc(_calcSegment) ]invalid "curLine, curSegIndex"!`)
    return
  }

  const x = segIndex.x
  const xEnd = paraCalcState.xLimit

  // reset current segment state for each segment calculation
  paraCalcState.resetLineSegmentState(x, xEnd)

  const contentLen = paragraph.children.length

  let pos = Math.max(startIndex, 0)
  for (; pos < contentLen; pos++) {
    const item = paragraph.children.at(pos)!
    if ((0 === pos && 0 === curLine) || pos !== startIndex) {
      resetParaItemCalcStatus(item, curLine)
    }

    paraCalcState.updateCursorPos(pos, LEVEL_PARAGRAPH)
    calcParaItemSegment(item, paraCalcState, paraPr)

    if (true === paraCalcState.isSegmentEnd) {
      break
    }
  }

  if (pos >= contentLen) pos = contentLen - 1

  if (paraCalcState.calcStatus & ParaCalcStatus.NextLine) {
    // always be true
    // 注意如果是完整的过长英文单词被压入下一行,  X 仍然已经累加
    _calcAndSetSegmentEndPos(paragraph, paraCalcState, curLine, pos)
  }
}

function _calcAndSetSegmentEndPos(
  paragraph: Paragraph,
  paraCalcState: ParagraphCalcState,
  curLine: number,
  index: number
) {
  let curIndex = index
  const positions = getPosByLineAndSegment(
    paragraph,
    curIndex,
    curIndex,
    curLine
  )
  let endPosition = positions?.endPos

  if (true === paraCalcState.needMoveToLineBreak) {
    curIndex = paraCalcState.lineBreakElmPosition.get(0)

    const item = paragraph.children.at(curIndex)
    if (item) {
      calcAndSetParaItemSegmentEndPos(
        item,
        paraCalcState,
        paraCalcState.lineBreakElmPosition,
        1
      )
      endPosition = paraCalcState.lineBreakElmPosition
    }
  }
  paragraph.renderingState.setSegmentEndPos(curLine, curIndex)

  const lineArabicIndices = paraCalcState.lineArabicIndices
  if (lineArabicIndices && endPosition) {
    const lineBreakIndices = runElementIndicesFromPos(paragraph, endPosition)
    if (
      lineBreakIndices &&
      compareIndices(lineArabicIndices, lineBreakIndices) <= 0
    ) {
      paragraph.renderingState.setLineInfo(curLine, ParaLineInfo.HasArabic)
    }
  }
}

function updateLineInfo(
  paragraph: Paragraph,
  curLine: number,
  paraCalcState: ParagraphCalcState
) {
  const renderingState = paragraph.renderingState

  if (true === paraCalcState.isEmptyLine) {
    renderingState.setLineInfo(curLine, ParaLineInfo.IsEmpty)
  }

  if (true === paraCalcState.isParaEnd) {
    renderingState.setLineInfo(curLine, ParaLineInfo.HasParaEnd)
  }

  if (true === paraCalcState.hasTextOnLine) {
    renderingState.setLineInfo(curLine, ParaLineInfo.HasTextOnLine)
  }
}

function calcMetricsForLine(
  paragraph: Paragraph,
  curLine: number,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr
) {
  const renderingState = paragraph.renderingState
  const line = renderingState.getLine(curLine)
  if (line == null) return
  const { startIndex, endIndex } = line.segment
  for (let i = startIndex; i <= endIndex; i++) {
    const item = paragraph.children.at(i)
    item && calcLineMetricsForParaItem(item, paraCalcState, paraPr, curLine)
  }

  if (
    true === paraCalcState.isEmptyLine ||
    paraCalcState.lineAscent < 0.001 ||
    (true === paraCalcState.isParaEnd && true !== paraCalcState.hasTextOnLine)
  ) {
    const lastItem =
      true === paraCalcState.isParaEnd
        ? paragraph.children.at(paragraph.children.length - 1)
        : paragraph.children.at(line.segment.endIndex)

    if (true === paraCalcState.isParaEnd) {
      const textPrOfParaEnd = paragraph.getCalcedPrs(false)!.textPr.clone()
      textPrOfParaEnd.merge(paragraph.paraTextPr.textPr)

      const metrics = getFontMetrics(
        EditorUtil.FontKit,
        textPrOfParaEnd,
        textPrOfParaEnd.lang.fontClass ?? FontClass.ASCII
      )
      const textHeightOfParaEnd = metrics.textHeight
      const textDescentOfParaEnd = Math.abs(metrics.descender)
      const textAscentOfParaEnd = textHeightOfParaEnd - textDescentOfParaEnd
      const ascentOfMetricsOfParaEnd = metrics.ascender

      paraCalcState.textAscentOfLine = textAscentOfParaEnd
      paraCalcState.textAscentFromMetricsForLine = ascentOfMetricsOfParaEnd
      paraCalcState.textDescentOfLine = textDescentOfParaEnd

      if (paraCalcState.lineAscent < textAscentOfParaEnd) {
        paraCalcState.lineAscent = textAscentOfParaEnd
      }

      if (paraCalcState.lineDescent < textDescentOfParaEnd) {
        paraCalcState.lineDescent = textDescentOfParaEnd
      }
    } else if (null != lastItem) {
      const lastRun = getLastRunInSegment(lastItem, paraCalcState.curLineIndex)
      if (lastRun != null) {
        const { textAscent, textAscentOfMetrics, textDescent } = lastRun.metrics
        if (paraCalcState.textAscentOfLine < textAscent) {
          paraCalcState.textAscentOfLine = textAscent
        }

        if (paraCalcState.textAscentFromMetricsForLine < textAscentOfMetrics) {
          paraCalcState.textAscentFromMetricsForLine = textAscentOfMetrics
        }

        if (paraCalcState.textDescentOfLine < textDescent) {
          paraCalcState.textDescentOfLine = textDescent
        }

        if (paraCalcState.lineAscent < textAscent) {
          paraCalcState.lineAscent = textAscent
        }

        if (paraCalcState.lineDescent < textDescent) {
          paraCalcState.lineDescent = textDescent
        }
      }
    }
  }

  line.metrics.Update(
    paraCalcState.textAscentOfLine,
    paraCalcState.textAscentFromMetricsForLine,
    paraCalcState.textDescentOfLine,
    paraCalcState.lineAscent,
    paraCalcState.lineDescent,
    paraPr
  )

  if (
    true === paraCalcState.isParaEnd &&
    true !== paraCalcState.isEmptyLine &&
    true !== paraCalcState.hasTextOnLine &&
    Math.abs(line.metrics.descent - line.metrics.textDescent) < 0.001
  ) {
    line.metrics.descent = 0
  }
}

function calcLinePosition(
  paragraph: Paragraph,
  curLineIndex: number,
  colIndex: number,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr
) {
  if (paragraph.parent == null) return

  const renderingState = paragraph.renderingState
  const column = renderingState.columns.at(colIndex)

  if (column == null) return

  const checkFirstColResult = isFirstColumn(paragraph, colIndex)
  const needBeforeSpacing = _isNeedBeforeSpacing(
    paragraph,
    colIndex,
    paraCalcState
  )
  const curLine = renderingState.getLine(curLineIndex)
  if (curLine == null) return

  // spacing support percentage value
  const textHeight = curLine.metrics.textAscent + curLine.metrics.textDescent
  const spcBefore = ParaSpacing.computeSpcVal(
    textHeight,
    paraPr.spacing.before,
    paraPr.spacing.beforePercent
  )

  const spcAfter = ParaSpacing.computeSpcVal(
    textHeight,
    paraPr.spacing.after,
    paraPr.spacing.afterPercent
  )

  let baseLineOffY = 0
  if (curLineIndex === column.firstLine) {
    baseLineOffY = curLine.metrics.ascent

    if (checkFirstColResult) {
      if (needBeforeSpacing) {
        baseLineOffY += spcBefore
      }
    }

    paraCalcState.baseLineOffY = baseLineOffY
  } else {
    baseLineOffY = paraCalcState.baseLineOffY
  }

  let top, totalBottom
  let bottom

  const prevBottom = column.bounds.bottom

  if (curLineIndex !== column.firstLine || !checkFirstColResult) {
    if (curLineIndex !== column.firstLine) {
      const prevLine = renderingState.getLine(curLineIndex - 1)

      top = prevLine
        ? paraCalcState.y +
          baseLineOffY +
          prevLine.metrics.descent +
          prevLine.metrics.LineGap
        : column.y
      bottom = top + curLine.metrics.ascent + curLine.metrics.descent
    } else {
      top = column.y
      bottom = top + curLine.metrics.ascent + curLine.metrics.descent
    }
  } else {
    top = paraCalcState.y

    if (needBeforeSpacing) {
      bottom =
        top + spcBefore + curLine.metrics.ascent + curLine.metrics.descent
    } else {
      bottom = top + curLine.metrics.ascent + curLine.metrics.descent
    }
  }

  totalBottom = bottom
  totalBottom += curLine.metrics.LineGap

  const yLimit = renderingState.yLimit
  if (true === paraCalcState.isParaEnd) {
    totalBottom += spcAfter

    if (
      false === paragraph.parent.isInTableCell() &&
      totalBottom > yLimit &&
      totalBottom - yLimit <= spcAfter
    ) {
      totalBottom = yLimit
    }
  }

  if (curLineIndex === column.firstLine) {
    column.bounds.top = top
  }

  column.bounds.bottom = totalBottom

  curLine.top = top - column.y
  curLine.bottom = totalBottom - column.y

  paraCalcState.lineTop = normalizeMMToTwips(top)
  paraCalcState.lineBottomTotal = normalizeMMToTwips(totalBottom)
  paraCalcState.lineBottom = normalizeMMToTwips(bottom)
  paraCalcState.linePreBottom = normalizeMMToTwips(prevBottom)
}
function _isNeedBeforeSpacing(
  paragraph: Paragraph,
  colIndex: number,
  paraCalcState: ParagraphCalcState
) {
  if (colIndex <= 0) return true

  if (!isFirstColumn(paragraph, colIndex)) {
    return false
  }

  if (
    isInstanceTypeOf(paraCalcState.parent, InstanceType.TextDocument) &&
    0 !== colIndex
  ) {
    return false
  }

  return true
}

function checkBoundForLineBottom(
  paragraph: Paragraph,
  curLine: number,
  colIndex: number,
  paraCalcState: ParagraphCalcState
) {
  if (paragraph.parent == null) return false
  const top = paraCalcState.lineTop
  let bottom = paraCalcState.lineBottom

  if (true === paragraph.parent.isInTableCell()) {
    bottom = paraCalcState.lineBottomTotal
  }

  const { columns } = paragraph.renderingState

  const yLimit = paraCalcState.yLimit

  const absColumnIndex = colIndex + paragraph.getIndexOfStartColumn()
  const absColumnCount = paragraph.parent.getColumnsCount()
  // do not move to next column when the current column is the last one
  const column = columns.at(colIndex)
  if (
    column &&
    absColumnIndex < absColumnCount - 1 &&
    (top > yLimit || bottom > yLimit) &&
    (curLine !== column.firstLine ||
      null != paragraph.getPrevPara() ||
      true === paragraph.parent.isInTableCell())
  ) {
    column.bounds.bottom = paraCalcState.linePreBottom
    column.setEndLine(curLine - 1)

    if (0 === curLine) paragraph.renderingState.resetLineAt(1)

    paraCalcState.calcStatus = ParaCalcStatus.NextColumn

    return false
  }

  return true
}

function calcLineBaseLine(
  paragraph: Paragraph,
  curLineIndex: number,
  colIndex: number,
  paraCalcState: ParagraphCalcState
) {
  const renderingState = paragraph.renderingState
  const { columns } = renderingState
  const column = columns.at(colIndex)

  const curLine = renderingState.getLine(curLineIndex)
  if (column == null || curLine == null) return

  if (curLineIndex > 0) {
    if (curLineIndex !== column.firstLine) {
      const prevLine = renderingState.getLine(curLineIndex - 1)
      if (prevLine) {
        paraCalcState.y +=
          prevLine.metrics.descent +
          prevLine.metrics.LineGap +
          curLine.metrics.ascent
      }
    }

    curLine.y = paraCalcState.y - column.y
  } else {
    curLine.y = 0
  }

  curLine.y += paraCalcState.baseLineOffY
  curLine.y += curLine.metrics.LineGap
}

function calcLineAlign(
  paragraph: Paragraph,
  curLine: number,
  colIndex: number,
  paraCalcState: ParagraphCalcState,
  paraPr: ParaPr
) {
  const renderingState = paragraph.renderingState
  const paraCalcCounterState = gParagraphCalcCounterState
  const paraCalcAlignState = gParagraphCalcAlignState
  paraCalcAlignState.reset(paragraph)
  paraCalcAlignState.paragraph = paragraph
  paraCalcAlignState.lastWidth = 0

  const column = renderingState.columns.at(colIndex)
  const line = renderingState.getLine(curLine)
  if (column == null || line == null) return

  const segment = line.segment
  const startIndex = segment.startIndex
  const endIndex = segment.endIndex

  paraCalcCounterState.reset(paragraph, segment)

  paraCalcCounterState.lineSegment!.w = 0
  paraCalcCounterState.lineSegment!.paraEndWidth = 0
  paraCalcCounterState.lineSegment!.lineBreakWidth = 0
  if (true === paragraph.paraNumbering.isSegmentValid(curLine)) {
    paraCalcCounterState.lineSegment!.w += paragraph.paraNumbering.renderWidth
  }

  for (let pos = startIndex; pos <= endIndex; pos++) {
    const item = paragraph.children.at(pos)
    item && calcParaItemSegmentWidth(item, paraCalcCounterState, curLine)
  }

  let widthForWordJustify = 0
  let widthForSpaceJustify = 0
  const segmentWidth = isNumber(renderingState.widthLimitInSp)
    ? renderingState.widthLimitInSp
    : segment.xEnd - segment.x
  const indLeftOffset =
    (paraPr.ind.left ?? 0) +
    (paraCalcState.isFirstLine ? paraPr.ind.getCalcFirstLine() ?? 0 : 0)

  let x = 0

  switch (paraPr.jcAlign) {
    case AlignKind.Left: {
      if (paragraph.isRTL) {
        x = segment.x - indLeftOffset - paraCalcCounterState.spaceWidth
      } else {
        x = segment.x
      }
      break
    }
    case AlignKind.Right: {
      if (paragraph.isRTL) {
        x =
          segment.x +
          segmentWidth -
          segment.w -
          indLeftOffset -
          paraCalcCounterState.spaceWidth
      } else {
        x = segment.x + segmentWidth - segment.w
      }
      break
    }
    case AlignKind.Center: {
      x = segment.x + (segmentWidth - segment.w) / 2
      if (paragraph.isRTL) {
        x -= paraCalcCounterState.spaceWidth
      }
      break
    }
    case AlignKind.Justify: {
      x = segment.x
      if (!(line.info & ParaLineInfo.HasParaEnd)) {
        if (paraCalcCounterState.spaceTotalCount > 0) {
          widthForSpaceJustify =
            (segmentWidth - segment.w) / paraCalcCounterState.spaceTotalCount
        } else if (paraCalcCounterState.amountOfWords > 1) {
          const sepCount = paraCalcCounterState.amountOfWords - 1
          widthForWordJustify = (segmentWidth - segment.w) / sepCount
        }

        segment.w = segmentWidth
      }
      if (paragraph.isRTL) {
        x =
          segment.x +
          segmentWidth -
          segment.w -
          indLeftOffset -
          paraCalcCounterState.spaceWidth
      }

      break
    }
    case AlignKind.Dist: {
      if (segmentWidth >= MaxWidthLimitForTextWarpNone) {
        if (paragraph.isRTL) {
          x = segment.x - indLeftOffset - paraCalcCounterState.spaceWidth
        } else {
          x = segment.x
        }
        break
      }
      if (paraCalcCounterState.letterCount === 1) {
        // one letter should be centered
        x = segment.x + (segmentWidth - segment.w) / 2
        if (paragraph.isRTL) {
          x -= paraCalcCounterState.spaceWidth
        }
        break
      }
      // x = segment.x
      if (paraCalcCounterState.letterCount > 1 && segmentWidth > segment.w) {
        const sepCount = paraCalcCounterState.letterCount - 1
        widthForWordJustify = (segmentWidth - segment.w) / sepCount
        segment.w = segmentWidth
      }

      /* } */
      x = segment.x + (segmentWidth - segment.w) / 2
      if (paragraph.isRTL) {
        x =
          segment.x +
          (segmentWidth - segment.w) / 2 -
          indLeftOffset -
          paraCalcCounterState.spaceWidth
      }

      break
    }

    default: {
      x = segment.x
      break
    }
  }

  if (
    curLine === renderingState.paraEndInfo.line &&
    paraPr.jcAlign !== AlignKind.Dist
  ) {
    widthForWordJustify = 0
    widthForSpaceJustify = 0
  }

  segment.spaceCount =
    paraCalcCounterState.spaceTotalCount + paraCalcCounterState.skipSpaceCount

  paraCalcAlignState.x = x
  paraCalcAlignState.y = column.y + line.y
  paraCalcAlignState.xEnd = segment.xEnd
  paraCalcAlignState.justifyWordWidth = widthForWordJustify
  paraCalcAlignState.justifySpaceWidth = widthForSpaceJustify
  paraCalcAlignState.spaceCount = paraCalcCounterState.spaceTotalCount
  paraCalcAlignState.skipSpaceCount = paraCalcCounterState.skipSpaceCount
  paraCalcAlignState.skipLetterCount = paraCalcCounterState.skipLetterCount

  const lineMetrics = line.metrics
  paraCalcAlignState.yTop = column.y + line.y - lineMetrics.ascent
  paraCalcAlignState.yBottom = column.y + line.y + lineMetrics.descent
  if (lineMetrics.LineGap < 0) paraCalcAlignState.yBottom += lineMetrics.LineGap

  segment.XForRender = x

  if (
    !paragraph.isRTL &&
    true === paragraph.paraNumbering.isSegmentValid(curLine)
  ) {
    paraCalcAlignState.x += paragraph.paraNumbering.renderWidth
  }

  for (let pos = startIndex; pos <= endIndex; pos++) {
    const item = paragraph.children.at(pos)
    item && calcParaItemSegmentSpaces(item, paraCalcAlignState, curLine)
  }
}

function calcLineEnd(
  paragraph: Paragraph,
  curLine: number,
  colIndex: number,
  paraCalcState: ParagraphCalcState
) {
  const renderingState = paragraph.renderingState

  if (paraCalcState.isParaEnd === true) {
    const column = renderingState.columns.at(colIndex)
    column && column.setEndLine(curLine)
    paraCalcState.calcStatus = ParaCalcStatus.NextElement
  }
}

// helper
function checkEmptyCols(para: Paragraph, colIndex: number) {
  for (let col = colIndex; col >= 0; --col) {
    if (true !== para.renderingState.isEmptyColumn(col)) {
      return false
    }
  }

  return true
}
function isFirstColumn(para: Paragraph, colIndex: number) {
  if (true === para.renderingState.isEmptyColumn(colIndex)) {
    return false
  }

  return checkEmptyCols(para, colIndex - 1)
}

function getLastRunInSegment(paraItem: ParagraphItem, lineIndex: number) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    lineIndex = lineIndex - paraItem.startParaLine

    if (
      lineIndex <
      LineSegmentIndexesUtil.getLinesCount(paraItem.lineSegmentIndexes)
    ) {
      const lastItem = paraItem.children.at(
        LineSegmentIndexesUtil.getEndIndexOfLine(
          paraItem.lineSegmentIndexes,
          lineIndex
        )
      )
      if (null != lastItem) {
        return lastItem
      }
    }

    return null
  } else {
    return paraItem
  }
}
