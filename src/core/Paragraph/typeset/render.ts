import { Nullable } from '../../../../liber/pervasive'
import { LineDrawingRule } from '../../common/const/attrs'
import { InstanceType } from '../../instanceTypes'

import { TextContentDrawContext } from '../../common/types'

import { hookManager, Hooks } from '../../hooks'
import { SelectionState } from '../../Slide/SelectionState'
import { ClrMap } from '../../color/clrMap'
import { Theme } from '../../SlideElement/attrs/theme'
import { ParaPr } from '../../textAttributes/ParaPr'
import { TextPr } from '../../textAttributes/TextPr'
import { getPresentation } from '../../utilities/finders'
import {
  getTextDocumentOfSp,
  isEavertTextContent,
} from '../../utilities/shape/getters'
import { ParagraphElementPosition } from '../common'

import { getRunElementReorderIndicesInfoForLine } from '../utils/typeset'
import { Paragraph } from '../Paragraph'

import {
  renderLineOfRunElement,
  renderRunElementOfParaItem,
  renderUnderOrStrikeLinesForLine,
  renderUnderOrStrikeLinesForParaItem,
  updateHightLightsDrawInfoForLine,
  updateHightLightsDrawInfoForParaItem,
} from '../ParaContent/typeset/render'
import { renderSearchResult } from '../../render/search'
import { searchManager } from '../../search/SearchManager'
import { getCurrentParagraph } from '../../utilities/tableOrTextDocContent/getter'
import { CComplexColor } from '../../color/complexColor'
import { gTextBrushGetter } from '../ParaContent/utils/typeset'
import { CHexColor } from '../../color/CHexColor'

class ParaRenderingSegmentLinesElement {
  y0: number
  y1: number
  x0: number
  x1: number
  w: number
  r: number
  g: number
  b: number
  extraDrawInfo?: Nullable<TextPr | CComplexColor>
  constructor(y0, y1, x0, x1, w, r, g, b, extraDrawInfo) {
    this.y0 = y0
    this.y1 = y1
    this.x0 = x0
    this.x1 = x1
    this.w = w
    this.r = r
    this.g = g
    this.b = b

    this.extraDrawInfo = extraDrawInfo
  }
}

class ParaRenderingSegmentLines {
  elements: ParaRenderingSegmentLinesElement[]
  constructor() {
    this.elements = []
  }

  clear() {
    this.elements = []
  }

  add(y0, y1, x0, x1, w, r, g, b, extraDrawInfo?) {
    this.elements.push(
      new ParaRenderingSegmentLinesElement(
        y0,
        y1,
        x0,
        x1,
        w,
        r,
        g,
        b,
        extraDrawInfo
      )
    )
  }

  next() {
    let count = this.elements.length
    if (count <= 0) return null

    const element = this.elements[count - 1]
    count--

    while (count > 0) {
      const prevElement = this.elements[count - 1]

      if (_canMergeElements(prevElement, element)) {
        element.x0 = prevElement.x0
        count--
      } else break
    }

    this.elements.length = count

    return element
  }

  nextForward() {
    const l = this.elements.length
    if (l <= 0) return null

    const element = this.elements[0]
    let index = 1

    while (index < l) {
      const nextElement = this.elements[index]

      if (_canMergeElements(nextElement, element)) {
        element.x1 = nextElement.x1
        index++
      } else break
    }

    this.elements.splice(0, index)
    return element
  }

  correctUnderlineWidth() {
    const count = this.elements.length
    if (count <= 0) return

    const elements: ParaRenderingSegmentLinesElement[] = []
    for (let i = 0; i < count; i++) {
      const element = this.elements[i]
      const count = elements.length

      if (0 === count) elements.push(element)
      else {
        const prevElement = elements[count - 1]

        if (
          Math.abs(prevElement.y0 - element.y0) < 0.001 &&
          Math.abs(prevElement.y1 - element.y1) < 0.001 &&
          Math.abs(prevElement.x1 - element.x0) < 0.001
        ) {
          if (element.w > prevElement.w) {
            for (let j = 0; j < count; j++) {
              elements[j].w = element.w
            }
          } else element.w = prevElement.w

          elements.push(element)
        } else {
          elements.length = 0
          elements.push(element)
        }
      }
    }
  }
}

function _canMergeElements(
  prevElement: ParaRenderingSegmentLinesElement,
  element: ParaRenderingSegmentLinesElement
) {
  if (
    Math.abs(prevElement.y0 - element.y0) < 0.001 &&
    Math.abs(prevElement.y1 - element.y1) < 0.001 &&
    Math.abs(prevElement.x1 - element.x0) < 0.001 &&
    Math.abs(prevElement.w - element.w) < 0.001 &&
    prevElement.r === element.r &&
    prevElement.g === element.g &&
    prevElement.b === element.b
  ) {
    if (null == prevElement.extraDrawInfo && null == element.extraDrawInfo) {
      return true
    }

    if (null == prevElement.extraDrawInfo || null == element.extraDrawInfo) {
      return false
    }

    const prevInfo = prevElement.extraDrawInfo
    const curInfo = element.extraDrawInfo

    if (
      prevInfo?.instanceType === InstanceType.ComplexColor &&
      curInfo?.instanceType === InstanceType.ComplexColor
    ) {
      return curInfo.sameAs(prevInfo)
    } else if (
      prevInfo?.instanceType === InstanceType.TextPr &&
      curInfo?.instanceType === InstanceType.TextPr
    ) {
      return curInfo.sameAs(prevInfo)
    }

    return false
  }

  return false
}

export class ParagraphRenderStateHighlights {
  highLight: ParaRenderingSegmentLines
  x: number
  searchFound: ParaRenderingSegmentLines
  line: number
  currentPosition: ParagraphElementPosition
  numOfSearch: number
  paragraph?: Paragraph
  y0: number
  y1: number
  spaceCount: number
  drawSearchFound: boolean
  colIndex: number
  constructor() {
    this.line = 0
    this.colIndex = 0
    this.currentPosition = new ParagraphElementPosition()

    this.highLight = new ParaRenderingSegmentLines()
    this.searchFound = new ParaRenderingSegmentLines()

    this.numOfSearch = 0

    this.paragraph = undefined

    this.x = 0
    this.y0 = 0
    this.y1 = 0

    this.spaceCount = 0

    this.drawSearchFound = false
  }

  reset(paragraph, isDrawFound) {
    this.paragraph = paragraph

    this.drawSearchFound = isDrawFound

    this.currentPosition.reset()

    this.numOfSearch = 0
    this.highLight.clear()
    this.searchFound.clear()
  }

  resetSegment(colIndex, line, x, y0, y1, amountOfSpaces) {
    this.line = line
    this.colIndex = colIndex

    this.x = x
    this.y0 = y0
    this.y1 = y1

    this.spaceCount = amountOfSpaces
  }
}

const gParaRenderStateHLights = new ParagraphRenderStateHighlights()

export class ParagraphRenderStateElements {
  paragraph: Nullable<Paragraph>
  drawContext: Nullable<TextContentDrawContext>
  bgColor: any
  theme: any
  colorMap: any
  currentPosition: ParagraphElementPosition
  isHyperlinkVisited: boolean
  isHyperlink: boolean
  line: number
  x: number
  y: number
  xTextStart: number
  lineTop: number
  lineBottom: number
  baseLine: number
  colIndex: number
  lineWidth: number
  constructor() {
    this.paragraph = undefined
    this.drawContext = undefined
    this.bgColor = undefined

    this.theme = undefined
    this.colorMap = undefined

    this.currentPosition = new ParagraphElementPosition()

    this.isHyperlinkVisited = false
    this.isHyperlink = false

    this.line = 0

    this.x = 0
    this.y = 0
    this.xTextStart = 0
    this.lineWidth = 0
    this.colIndex = 0

    this.lineTop = 0
    this.lineBottom = 0
    this.baseLine = 0
  }

  reset(
    paragraph: Paragraph,
    drawContext: TextContentDrawContext,
    bgColor: Nullable<CHexColor>,
    theme: Nullable<Theme>,
    colorMap: ClrMap
  ) {
    this.paragraph = paragraph
    this.drawContext = drawContext
    this.bgColor = bgColor
    this.theme = theme
    this.colorMap = colorMap

    this.isHyperlinkVisited = false
    this.isHyperlink = false

    this.currentPosition.reset()
  }

  resetSegment(colIndex, line, x, y, w) {
    this.line = line
    this.colIndex = colIndex
    this.x = x
    this.y = y
    this.xTextStart = x
    this.lineWidth = w
  }

  setLineMetrics(baseLine, top, bottom) {
    this.lineTop = top
    this.lineBottom = bottom
    this.baseLine = baseLine
  }
}
const gParagraphRenderStateElements = new ParagraphRenderStateElements()
export class ParagraphRenderStateLines {
  x: any
  currentPosition: ParagraphElementPosition
  curLevel: number
  strikeout: ParaRenderingSegmentLines
  underline: ParaRenderingSegmentLines
  paragraph: Nullable<Paragraph>
  drawContext?: TextContentDrawContext
  bgColor: CHexColor | undefined
  isHyperlinkVisited: boolean
  isHyperlink: boolean
  line: number
  baseLine: number
  underlineOffset: number
  spaceCount: number
  colIndex: number
  constructor() {
    this.paragraph = undefined
    this.drawContext = undefined
    this.bgColor = undefined

    this.currentPosition = new ParagraphElementPosition()
    this.curLevel = 0

    this.isHyperlinkVisited = false
    this.isHyperlink = false

    this.strikeout = new ParaRenderingSegmentLines()
    this.underline = new ParaRenderingSegmentLines()

    this.line = 0

    this.colIndex = 0
    this.x = 0
    this.baseLine = 0
    this.underlineOffset = 0
    this.spaceCount = 0
  }

  reset(
    paragraph: Paragraph,
    drawContext: TextContentDrawContext,
    bgColor: CHexColor | undefined
  ) {
    this.paragraph = paragraph
    this.drawContext = drawContext
    this.bgColor = bgColor

    this.isHyperlinkVisited = false
    this.isHyperlink = false

    this.currentPosition.reset()
    this.curLevel = 0
  }

  resetLine(colIndex, line, baseline, underlineOffset) {
    this.colIndex = colIndex
    this.line = line

    this.baseLine = baseline
    this.underlineOffset = underlineOffset

    this.strikeout.clear()
    this.underline.clear()
  }

  resetSegment(x, spaceCount) {
    this.x = x
    this.spaceCount = spaceCount
  }
}

const gParaRenderStateLns = new ParagraphRenderStateLines()

/**
 * TODO: fix 选中 Slide 重复绘制两次 / 选中文本重复绘制三次
 */
export function renderParagraph(
  para: Paragraph,
  colIndex: number,
  drawContext: TextContentDrawContext
) {
  const renderingState = para.renderingState
  // only cache for one paragraph
  gTextBrushGetter.clear()
  if (para.parent == null || renderingState.isEmptyColumn(colIndex)) {
    return
  }

  const prs = para.getSpacingAdjustedPrs()
  const theme = para.getTheme()
  const colorMap = para.getColorMap()!
  const bgColor = para.parent.getTextBgColor()

  const presentation = getPresentation(para)

  //    We draw a paragraph fill and various text highlights (highlight, search, co-editing).
  //    In addition, draw the side lines of the paragraph stroke.
  const renderSearch = render_highlights(
    para,
    colIndex,
    drawContext,
    !!searchManager.selection &&
      drawContext.instanceType === InstanceType.CanvasDrawer &&
      drawContext.isInPreview !== true,
    theme
  )

  //    Drawing the paragraph elements themselves
  render_content(
    para,
    colIndex,
    drawContext,
    presentation?.selectionState,
    prs,
    bgColor,
    theme,
    colorMap
  )

  renderSearch && renderSearch()
  //    We draw various underscores and strikethroughs.
  render_above(para, colIndex, drawContext, bgColor)
}
function render_highlights(
  paragraph: Paragraph,
  colIndex: number,
  drawContext: TextContentDrawContext,
  isDrawFind: boolean,
  theme
) {
  const paraRenderStateHLights = gParaRenderStateHLights
  const renderingState = paragraph.renderingState
  const { columns } = renderingState
  const column = columns.at(colIndex)
  if (column == null) return
  const startLine = column.startLine
  const endLine = column.endLine

  paraRenderStateHLights.reset(paragraph, isDrawFind)

  for (let curLine = startLine; curLine <= endLine; curLine++) {
    const curParaLine = renderingState.getLine(curLine)
    if (curParaLine == null) continue
    const lineMetrics = curParaLine.metrics
    const y0 = column.y + curParaLine.y - lineMetrics.ascent
    let y1 = column.y + curParaLine.y + lineMetrics.descent
    if (lineMetrics.LineGap < 0) {
      y1 += lineMetrics.LineGap
    }

    const segment = curParaLine.segment
    const x = segment.XForRender
    const startIndex = segment.startIndex
    const endIndex = segment.endIndex

    paraRenderStateHLights.resetSegment(
      colIndex,
      curLine,
      x,
      y0,
      y1,
      segment.spaceCount
    )

    if (
      !paragraph.isRTL &&
      true === paragraph.paraNumbering.isSegmentValid(curLine)
    ) {
      paraRenderStateHLights.x += paragraph.paraNumbering.renderWidth
    }

    const indicesArray = getRunElementReorderIndicesInfoForLine(
      paragraph,
      curLine
    )
    if (indicesArray) {
      updateHightLightsDrawInfoForLine(
        indicesArray,
        paraRenderStateHLights,
        theme
      )
    } else {
      for (let pos = startIndex; pos <= endIndex; pos++) {
        const item = paragraph.children.at(pos)
        item &&
          updateHightLightsDrawInfoForParaItem(
            item,
            paraRenderStateHLights,
            theme
          )
      }
    }
  }

  /* ----------------------------- 绘制 Highlight ----------------------------- */
  const arrOfHigh = paraRenderStateHLights.highLight
  let element = arrOfHigh.next()
  while (null != element) {
    const highlight = element.extraDrawInfo as Nullable<CComplexColor>
    const alpha = highlight?.rgba.a == null ? 255 : highlight.rgba.a
    drawContext.setBrushColor(element.r, element.g, element.b, alpha)

    drawContext.rect(
      element.x0,
      element.y0,
      element.x1 - element.x0,
      element.y1 - element.y0
    )
    drawContext.drawFill()
    element = arrOfHigh.next()
  }

  /* ----------------------------- 绘制查找结果背景 ----------------------------- */
  return () => {
    if (isDrawFind && drawContext.instanceType === InstanceType.CanvasDrawer) {
      const arrOfFind = paraRenderStateHLights.searchFound
      element = arrOfFind.next()
      while (null != element) {
        renderSearchResult(
          drawContext,
          element.x0,
          element.y0,
          element.x1 - element.x0,
          element.y1 - element.y0
        )
        element = arrOfFind.next()
      }
    }
  }
}
function render_content(
  paragraph: Paragraph,
  colIndex: number,
  drawContext: TextContentDrawContext,
  selectionState: SelectionState | undefined,
  pr: {
    paraPr: ParaPr
    textPr: TextPr
  },
  bgColor: Nullable<CHexColor>,
  theme: Nullable<Theme>,
  colorMap: ClrMap
) {
  const paraRenderStateElements = gParagraphRenderStateElements
  paraRenderStateElements.reset(
    paragraph,
    drawContext,
    bgColor,
    theme,
    colorMap
  )

  const renderingState = paragraph.renderingState
  const { columns } = renderingState

  const column = columns.at(colIndex)
  if (column == null) return

  const { startLine, endLine } = column

  for (let curLine = startLine; curLine <= endLine; curLine++) {
    const line = renderingState.getLine(curLine)
    if (line == null) continue
    const lineSegment = line.segment
    const y = column.y + line.y
    const x = lineSegment.XForRender

    paraRenderStateElements.setLineMetrics(
      y,
      y - line.metrics.ascent,
      y + line.metrics.descent
    )

    paraRenderStateElements.resetSegment(colIndex, curLine, x, y, lineSegment.w)

    const { startIndex, endIndex } = lineSegment
    const numberingItem = paragraph.paraNumbering
    if (true === numberingItem.isSegmentValid(curLine)) {
      const textSelectionShape = selectionState?.rootTextSelection
      const textDoc = getTextDocumentOfSp(textSelectionShape!)
      const isEmpty = paragraph.isEmpty()
      const isDrawPlaceholder =
        !hookManager.get(Hooks.Document.GetViewMode) &&
        !!textDoc &&
        getCurrentParagraph(textDoc) === paragraph &&
        drawContext.instanceType === InstanceType.CanvasDrawer &&
        drawContext.isRenderThumbnail !== true &&
        isEmpty === true

      if (isDrawPlaceholder || true !== isEmpty) {
        let _x =
          pr.paraPr.ind.firstLine! < 0 ? x : column.x + pr.paraPr.ind.left!
        if (paragraph.isRTL) {
          _x =
            lineSegment.XForRender +
            lineSegment.w +
            (pr.paraPr.ind.firstLine! < 0
              ? 0
              : pr.paraPr.ind.getCalcFirstLine() ?? 0)
        }
        numberingItem.draw(
          _x,
          y,
          drawContext,
          isDrawPlaceholder && isEmpty === true,
          paraRenderStateElements
        )
      }

      if (!paragraph.isRTL) {
        paraRenderStateElements.x += numberingItem.renderWidth
      }
    }

    paraRenderStateElements.xTextStart = paraRenderStateElements.x

    const dicesOfLine = getRunElementReorderIndicesInfoForLine(
      paragraph,
      curLine
    )
    if (dicesOfLine) {
      renderLineOfRunElement(dicesOfLine, paraRenderStateElements)
    } else {
      for (let pos = startIndex; pos <= endIndex; pos++) {
        paraRenderStateElements.currentPosition.updatePosByLevel(pos, 0)

        const item = paragraph.children.at(pos)
        item && renderRunElementOfParaItem(item, paraRenderStateElements)
      }
    }
  }
  // 实际绘制的宽度
  column.renderingXLimit = paraRenderStateElements.x
}

function render_above(
  paragraph: Paragraph,
  colIndex: number,
  drawContext: TextContentDrawContext,
  bgColor
) {
  const paraRenderStateLns = gParaRenderStateLns
  paraRenderStateLns.reset(paragraph, drawContext, bgColor)

  const isEaVert = isEavertTextContent(paragraph.parent?.parent)
  const renderingState = paragraph.renderingState
  const { columns } = renderingState
  const column = columns.at(colIndex)
  if (column == null) return

  const { startLine: startLine, endLine: endLine } = column

  for (let curLine = startLine; curLine <= endLine; curLine++) {
    const line = renderingState.getLine(curLine)
    if (line == null) continue
    const { textDescent: textDescent } = line.metrics

    const baseline = column.y + line.y
    const underlineOffset = isEaVert ? textDescent : textDescent * 0.4

    paraRenderStateLns.resetLine(colIndex, curLine, baseline, underlineOffset)

    const { XForRender, spaceCount, startIndex, endIndex } = line.segment

    paraRenderStateLns.resetSegment(XForRender, spaceCount)

    if (
      !paragraph.isRTL &&
      true === paragraph.paraNumbering.isSegmentValid(curLine)
    ) {
      paraRenderStateLns.x += paragraph.paraNumbering.renderWidth
    }

    const indicesArray = getRunElementReorderIndicesInfoForLine(
      paragraph,
      curLine
    )
    if (indicesArray) {
      renderUnderOrStrikeLinesForLine(indicesArray, paraRenderStateLns)
    } else {
      for (let pos = startIndex; pos <= endIndex; pos++) {
        paraRenderStateLns.currentPosition.updatePosByLevel(pos, 0)
        paraRenderStateLns.curLevel = 1

        const item = paragraph.children.at(pos)
        item && renderUnderOrStrikeLinesForParaItem(item, paraRenderStateLns)
      }
    }

    const { strikeout, underline } = paraRenderStateLns

    let drawElemnet = strikeout.next()
    while (null != drawElemnet) {
      // fix safari oddness
      if (drawContext.instanceType === InstanceType.CanvasDrawer) {
        drawContext.context.save()
        drawContext.context.setLineDash([])
      }
      drawContext.setPenColor(drawElemnet.r, drawElemnet.g, drawElemnet.b, 255)

      drawContext.drawHorizontalLine(
        LineDrawingRule.Top,
        drawElemnet.y0,
        drawElemnet.x0,
        drawElemnet.x1,
        drawElemnet.w
      )
      drawElemnet = strikeout.next()
      if (drawContext.instanceType === InstanceType.CanvasDrawer) {
        drawContext.context.restore()
      }
    }

    underline.correctUnderlineWidth()
    drawElemnet = underline.next()
    while (null != drawElemnet) {
      if (drawContext.instanceType === InstanceType.CanvasDrawer) {
        drawContext.context.save()
        drawContext.context.setLineDash([])
      }
      drawContext.setPenColor(drawElemnet.r, drawElemnet.g, drawElemnet.b, 255)

      drawContext.drawHorizontalLine(
        LineDrawingRule.Top,
        drawElemnet.y0,
        drawElemnet.x0,
        drawElemnet.x1,
        drawElemnet.w
      )
      drawElemnet = underline.next()
      if (drawContext.instanceType === InstanceType.CanvasDrawer) {
        drawContext.context.restore()
      }
    }
  }
}
