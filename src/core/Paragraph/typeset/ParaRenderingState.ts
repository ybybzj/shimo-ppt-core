import { Nullable } from '../../../../liber/pervasive'
import { LineHeightRule } from '../../common/const/attrs'
import { ParaLineInfo } from '../../common/const/paragraph'
import { TextDocBounds } from '../../TextDocument/DocumentBounds'
import { ParaPr } from '../../textAttributes/ParaPr'
import { ParagraphElementPosition } from '../common'
import { Paragraph } from '../Paragraph'
import { RunElemnetOrderIndicesInfo } from '../type'
import { RunElementIndices } from '../../../re/export/textContent'
import { PoolAllocator, Recyclable } from '../../../common/allocator'
import { RecyclableArray, ManagedArray } from '../../../common/managedArray'
import { ParaCalcStatusValues } from '../../common/const/doc'
import { checkAlmostEqual } from '../../common/utils'
export class ParaLine extends Recyclable<ParaLine> {
  y: number // baseline position
  top: number
  bottom: number
  metrics: ParaLineMetrics
  segment: ParaLineSegment
  info: number
  hasArabic?: boolean
  private reorderPosInfoArray?: ManagedArray<RunElemnetOrderIndicesInfo>

  private static allocator: PoolAllocator<ParaLine, []> =
    PoolAllocator.getAllocator<ParaLine, []>(ParaLine)
  static new() {
    return ParaLine.allocator.allocate()
  }
  static recycle(paraLine: ParaLine) {
    ParaLine.allocator.recycle(paraLine)
  }

  constructor() {
    super()
    this.y = 0
    this.top = 0
    this.bottom = 0
    this.segment = new ParaLineSegment()
    this.metrics = new ParaLineMetrics()
    this.info = 0
  }
  reset() {
    this.y = 0
    this.top = 0
    this.bottom = 0
    this.metrics?.reset()
    this.segment?.reset()
    this.info = 0
    this.hasArabic = undefined
    this.resetReorderPosInfo()
  }

  setSegment(x: number, xEnd: number) {
    if (this.segment) {
      this.segment.reset(x, xEnd)
    } else {
      this.segment = new ParaLineSegment(x, xEnd)
    }
  }

  translateBy(Dx: number, Dy: number) {
    this.segment?.translateBy(Dx, Dy)
  }

  getStartPosIndex() {
    if (this.segment == null) return 0

    return this.segment.startIndex
  }

  getEndPosIndex() {
    if (this.segment == null) return 0

    return this.segment.endIndex
  }

  setSegmentStartPos(startIndex: number) {
    if (this.segment != null) {
      this.segment.startIndex = startIndex
    }
  }

  setSegmentEndPos(endIndex: number) {
    if (this.segment != null) {
      this.segment.endIndex = endIndex
    }
  }

  getReorderPosInfoArray(): Nullable<ManagedArray<RunElemnetOrderIndicesInfo>> {
    return this.reorderPosInfoArray != null &&
      this.reorderPosInfoArray.length > 0
      ? this.reorderPosInfoArray
      : undefined
  }
  updateReorderPosInfo(item: RunElemnetOrderIndicesInfo) {
    if (this.reorderPosInfoArray == null) {
      this.reorderPosInfoArray = new ManagedArray<RunElemnetOrderIndicesInfo>()
    }
    this.reorderPosInfoArray.push(item)
  }
  resetReorderPosInfo() {
    this.reorderPosInfoArray?.reset()
  }
}

class ParaLineMetrics {
  ascent: number
  descent: number
  textAscent: number
  textAscentOfMetrics: number
  textDescent: number
  LineGap: number
  constructor() {
    this.ascent = 0
    this.descent = 0
    this.textAscent = 0
    this.textAscentOfMetrics = 0
    this.textDescent = 0
    this.LineGap = 0
  }

  reset() {
    this.ascent = 0
    this.descent = 0
    this.textAscent = 0
    this.textAscentOfMetrics = 0
    this.textDescent = 0
    this.LineGap = 0
  }

  Update(
    textAscent: number,
    textAscentofMetrics: number,
    textDescent: number,
    ascent: number,
    descent: number,
    paraPr: ParaPr
  ) {
    if (textAscent > this.textAscent) this.textAscent = textAscent

    if (textAscentofMetrics > this.textAscentOfMetrics) {
      this.textAscentOfMetrics = textAscentofMetrics
    }

    if (textDescent > this.textDescent) this.textDescent = textDescent

    if (ascent > this.ascent) this.ascent = ascent

    if (descent > this.descent) this.descent = descent

    if (this.ascent < this.textAscent) this.ascent = this.textAscent

    if (this.descent < this.textDescent) this.descent = this.textDescent

    this.LineGap = calcLineGap(this, paraPr, this.textAscent, this.textDescent)

    if (
      LineHeightRule.AtLeast === paraPr.spacing.lineRule &&
      this.ascent + this.descent + this.LineGap >
        this.textAscent + this.textDescent
    ) {
      this.ascent = this.ascent + this.LineGap
      this.LineGap = 0
    }
  }
}

function calcLineGap(
  paraLineMetrics: ParaLineMetrics,
  paraPr: ParaPr,
  textAscent: number,
  textDescent: number
) {
  let lineGap = 0
  switch (paraPr.spacing.lineRule) {
    case LineHeightRule.Auto: {
      lineGap = (textAscent + textDescent) * (paraPr.spacing.line! - 1)
      break
    }
    case LineHeightRule.Exact: {
      const exactSpcVal = Math.max(25.4 / 72, paraPr.spacing.line!)
      lineGap = exactSpcVal - (textAscent + textDescent)

      let gap = paraLineMetrics.ascent + paraLineMetrics.descent - exactSpcVal

      if (gap > 0) {
        const diffOfDescent =
          paraLineMetrics.descent - paraLineMetrics.textDescent

        if (diffOfDescent > 0) {
          if (diffOfDescent < gap) {
            paraLineMetrics.descent = paraLineMetrics.textDescent
            gap -= diffOfDescent
          } else {
            paraLineMetrics.descent -= gap
            gap = 0
          }
        }

        const diffOfAscent = paraLineMetrics.ascent - paraLineMetrics.textAscent

        if (diffOfAscent > 0) {
          if (diffOfAscent < gap) {
            paraLineMetrics.ascent = paraLineMetrics.textAscent
            gap -= diffOfAscent
          } else {
            paraLineMetrics.ascent -= gap
            gap = 0
          }
        }

        if (gap > 0) {
          const orgTextAscent = paraLineMetrics.textAscent
          const orgTextDescent = paraLineMetrics.textDescent

          const sum = orgTextAscent + orgTextDescent

          paraLineMetrics.ascent = (orgTextAscent * (sum - gap)) / sum
          paraLineMetrics.descent = (orgTextDescent * (sum - gap)) / sum
        }
      } else {
        paraLineMetrics.ascent -= gap
      }

      lineGap = 0

      break
    }
    case LineHeightRule.AtLeast: {
      const lineGapOfPr = paraPr.spacing.line!
      const lineGapOfText = textAscent + textDescent
      const actualLineGap = paraLineMetrics.ascent + paraLineMetrics.descent

      if (Math.abs(lineGapOfText) < 0.001 || actualLineGap >= lineGapOfPr) {
        lineGap = 0
      } else lineGap = lineGapOfPr - actualLineGap

      break
    }
  }
  return lineGap
}

export class ParaLineSegment {
  x: number
  XForRender: number
  xEnd: number
  startIndex: number
  endIndex: number
  w: number
  spaceCount: number
  paraEndWidth: number
  lineBreakWidth: number
  constructor(x?: number, xEnd?: number) {
    this.x = x ?? 0
    this.XForRender = 0
    this.xEnd = xEnd ?? 0
    this.startIndex = 0
    this.endIndex = 0
    this.w = 0
    this.spaceCount = 0
    this.paraEndWidth = 0
    this.lineBreakWidth = 0
  }

  reset(x?: number, xEnd?: number) {
    this.x = x ?? 0
    this.XForRender = 0
    this.xEnd = xEnd ?? 0
    this.startIndex = 0
    this.endIndex = 0
    this.w = 0
    this.spaceCount = 0
    this.paraEndWidth = 0
    this.lineBreakWidth = 0
  }
  translateBy(dx: number, _dy: number) {
    this.x += dx
    this.xEnd += dx
    this.XForRender += dx
  }
}

export class ParaColumn extends Recyclable<ParaColumn> {
  x: number
  y: number
  xLimit: number
  yLimit: number
  firstLine: number
  bounds: TextDocBounds
  startLine: number
  endLine: number
  renderingXLimit?: number
  calcResult?: ParaCalcStatusValues
  private static allocator: PoolAllocator<
    ParaColumn,
    [X: number, Y: number, XLimit: number, YLimit: number, FirstLine: number]
  > = PoolAllocator.getAllocator<
    ParaColumn,
    [X: number, Y: number, XLimit: number, YLimit: number, FirstLine: number]
  >(ParaColumn)
  static new(
    x: number,
    y: number,
    xLimit: number,
    yLimit: number,
    firstLine: number
  ) {
    return ParaColumn.allocator.allocate(x, y, xLimit, yLimit, firstLine)
  }
  static recycle(paraColumn: ParaColumn) {
    ParaColumn.allocator.recycle(paraColumn)
  }

  constructor(
    x: number,
    y: number,
    xLimit: number,
    yLimit: number,
    firstLine: number
  ) {
    super()
    this.x = x
    this.y = y
    this.xLimit = xLimit
    this.yLimit = yLimit
    this.firstLine = firstLine
    this.bounds = new TextDocBounds(x, y, xLimit, y)
    this.startLine = firstLine
    this.endLine = firstLine
  }

  reset(
    x: number,
    y: number,
    xLimit: number,
    yLimit: number,
    firstLine: number
  ) {
    this.x = x
    this.y = y
    this.xLimit = xLimit
    this.yLimit = yLimit
    this.firstLine = firstLine
    this.bounds.reset(x, y, xLimit, y)
    this.startLine = firstLine
    this.renderingXLimit = undefined
    this.calcResult = undefined
  }

  translateBy(Dx: number, Dy: number) {
    this.x += Dx
    this.y += Dy
    this.xLimit += Dx
    this.yLimit += Dy
    this.bounds.translateBy(Dx, Dy)
  }

  setEndLine(endLine: number) {
    this.endLine = endLine
  }
}

export class ParaRenderingState extends Recyclable<ParaRenderingState> {
  x: number
  y: number
  xLimit: number
  yLimit: number
  docStartColumnIndex: number
  columns: RecyclableArray<ParaColumn>
  private lines: RecyclableArray<ParaLine>
  paraEndInfo: { line: number }
  widthLimitInSp?: number
  private paragraph: Paragraph

  private static allocator: PoolAllocator<
    ParaRenderingState,
    [paragraph: Paragraph]
  > = PoolAllocator.getAllocator<ParaRenderingState, [paragraph: Paragraph]>(
    ParaRenderingState
  )
  static new(para: Paragraph) {
    return ParaRenderingState.allocator.allocate(para)
  }
  static recycle(paraRenderState: ParaRenderingState) {
    ParaRenderingState.allocator.recycle(paraRenderState)
  }

  constructor(paragraph: Paragraph) {
    super()
    this.paragraph = paragraph

    this.x = 0
    this.y = 0
    this.xLimit = 0
    this.yLimit = 0
    this.docStartColumnIndex = 0
    this.columns = new RecyclableArray<ParaColumn>(ParaColumn.recycle)
    this.paraEndInfo = {
      line: 0,
    }
    this.lines = new RecyclableArray<ParaLine>(ParaLine.recycle)
  }
  reset(paragraph: Paragraph) {
    this.paragraph = paragraph

    this.x = 0
    this.y = 0
    this.xLimit = 0
    this.yLimit = 0
    this.docStartColumnIndex = 0
    this.columns.clear()
    this.paraEndInfo.line = 0
    this.lines.clear()
    this.widthLimitInSp = undefined
  }

  resetDimension(
    x: number,
    y: number,
    xLimit: number,
    yLimit: number,
    columnNum?: number,
    widthLimitInSp?: number
  ) {
    this.x = x
    this.y = y
    this.xLimit = xLimit
    this.yLimit = yLimit
    this.lines.clear()
    this.docStartColumnIndex = columnNum ? columnNum : 0
    this.widthLimitInSp = widthLimitInSp
  }

  isSameDimension(
    x: number,
    y: number,
    xLimit: number,
    yLimit: number,
    columnNum?: number,
    widthLimitInSp?: number
  ) {
    return (
      checkAlmostEqual(this.x, x, 0.01) &&
      checkAlmostEqual(this.y, x, 0.01) &&
      checkAlmostEqual(this.xLimit, xLimit, 0.01) &&
      checkAlmostEqual(this.yLimit, yLimit, 0.01) &&
      ((this.docStartColumnIndex == null && columnNum == null) ||
        this.docStartColumnIndex === columnNum) &&
      ((this.widthLimitInSp == null && widthLimitInSp == null) ||
        this.widthLimitInSp === widthLimitInSp)
    )
  }

  resetParaEnd() {
    this.paraEndInfo.line = -1
  }

  resetLineAt(lineIndex: number) {
    this.lines.resetLength(lineIndex)
    this.lines.insert(lineIndex, ParaLine.new())
  }

  getLineCount() {
    return this.lines.length
  }

  getLine(index: number) {
    return this.lines.at(index)
  }

  // printLineContent(index: number) {
  //   const line = this.getLine(index)
  //   if (line) {
  //     const { startIndex, endIndex } = line.segment
  //     const runs = this.paragraph.children
  //     console.log(`line(${index}) ---->`)
  //     for (let i = startIndex; i <= endIndex; i++) {
  //       const run = runs[i]
  //       if (run) {
  //         console.log(run)
  //       }
  //     }
  //     console.log(`<---- line(${index})`)
  //   }
  // }

  addSegmentAtLine(lineIndex: number, x: number, xEnd: number) {
    this.resetLineAt(lineIndex)
    const line = this.lines.at(lineIndex)
    if (line) {
      line.setSegment(x, xEnd)
    }
  }

  setLineInfo(lineIndex: number, info: number) {
    const line = this.lines.at(lineIndex)
    if (line) {
      line.info |= info
    }
  }

  hasArabicAtLine(lineIndex: number) {
    const line = this.lines.at(lineIndex)
    if (line && line.info) {
      return (line.info & ParaLineInfo.HasArabic) === ParaLineInfo.HasArabic
    }
    return false
  }

  setSegmentStartPos(lineIndex: number, startIndex: number) {
    const line = this.lines.at(lineIndex)
    if (line) {
      line.setSegmentStartPos(startIndex)
    }
  }
  setSegmentEndPos(lineIndex: number, endIndex: number) {
    const line = this.lines.at(lineIndex)
    if (line) {
      line.setSegmentEndPos(endIndex)
    }
  }
  translateBy(colIndex, Dx, Dy) {
    const column = this.columns.at(colIndex)
    if (column == null) return

    if (0 === colIndex) {
      this.x += Dx
      this.y += Dy
      this.xLimit += Dx
      this.yLimit += Dy
    }

    column.translateBy(Dx, Dy)

    const startLine = column.startLine
    const endLine = column.endLine

    for (let curLine = startLine; curLine <= endLine; curLine++) {
      const line = this.lines.at(curLine)
      if (line) line.translateBy(Dx, Dy)
    }
  }

  getColumnBounds(colIndex: number) {
    const column = this.columns.at(colIndex)
    if (!column) {
      return new TextDocBounds(0, 0, 0, 0)
    }

    return column.bounds
  }

  getCalcResultByColumn(colIndex: number) {
    const column = this.columns.at(colIndex)
    if (!column) {
      return undefined
    }
    return column.calcResult
  }

  setCalcResultByColumn(colIndex: number, calcResult: ParaCalcStatusValues) {
    const column = this.columns.at(colIndex)
    if (column) {
      column.calcResult = calcResult
    }
  }

  getColIndexByLine(lineIndex: number): number {
    for (let colIndex = this.columns.length - 1; colIndex >= 0; colIndex--) {
      const col = this.columns.at(colIndex)!
      if (lineIndex >= col.startLine && lineIndex <= col.endLine) {
        return colIndex
      }
    }

    return 0
  }

  hasLine(lineIndex: number): boolean {
    const lineCount = this.lines.length
    return lineCount > 0 && lineIndex >= 0 && lineIndex < lineCount
  }

  getSegmentsByLine(lineIndex: number): Nullable<ParaLineSegment> {
    return this.lines.at(lineIndex)?.segment
  }

  getSegment(lineIndex: number): Nullable<ParaLineSegment> {
    return this.lines.at(lineIndex)?.segment
  }

  getContentPos(lineIndex: number): Nullable<[number, number]> {
    const segment = this.getSegment(lineIndex)

    if (segment == null) {
      return null
    }

    return segment != null ? [segment.startIndex, segment.endIndex] : null
  }

  getYByLineAndCol(lineIndex: number, colIndex?: number) {
    if (colIndex == null) {
      colIndex = this.getColIndexByLine(lineIndex)
    }
    const col = this.columns.at(colIndex)
    const line = this.lines.at(lineIndex)
    if (col != null && line != null) {
      return col.y + line.y
    } else {
      return 0
    }
  }

  getMaxSegmentWidth() {
    let maxWidth = 0
    const lineCount = this.lines.length
    for (let i = 0; i < lineCount; ++i) {
      const segW = this.lines.at(i)!.segment.w ?? 0
      if (segW > maxWidth) {
        maxWidth = segW
      }
    }

    return maxWidth
  }

  getStartLineForCalc(colIndex: number): number {
    const col = colIndex > 0 ? this.columns.at(colIndex - 1) : undefined
    return col != null ? col.endLine + 1 : 0
  }

  getStartItemIndex(lineIndex: number): number {
    let startIndex = 0

    const segment = this.lines.at(lineIndex - 1)?.segment
    if (segment != null) {
      startIndex = segment.endIndex
    }
    return startIndex
  }

  isLastLine(line: number): boolean {
    return this.getLineCount() - 1 === line
  }

  isEmptyColumn(colIndex: number) {
    const column = this.columns.at(colIndex)
    if (!column || column.endLine < column.startLine) {
      return true
    }
    return false
  }
}

export class ParagraphSearchPositionInfo {
  allowInText: boolean
  cursorX: number
  cursorY: number
  /** 点击的相对于 element 的原始坐标 */
  x: number
  y: number
  isNumbering: boolean
  hitTextContent: boolean
  positionInText: ParagraphElementPosition
  pos: ParagraphElementPosition
  isInTextContent: boolean
  diffX: number
  line: number
  isParaEnd: boolean
  paraEndIndices: Nullable<RunElementIndices>
  constructor() {
    this.pos = new ParagraphElementPosition()
    this.positionInText = new ParagraphElementPosition()

    this.allowInText = true
    this.cursorX = 0
    this.cursorY = 0
    this.x = 0
    this.y = 0
    this.diffX = 1000000

    this.line = 0

    this.isInTextContent = false
    this.hitTextContent = false
    this.isNumbering = false
    this.isParaEnd = false
  }
}
