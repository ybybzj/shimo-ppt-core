import { ParagraphElementPosition } from '../common'

import { Paragraph } from '../Paragraph'
import { Nullable } from '../../../../liber/pervasive'
import { TextDocument } from '../../TextDocument/TextDocument'
import { RunContentElement } from '../RunElement/type'
import { ParaLineSegment } from './ParaRenderingState'
import { ParaCalcStatus, ParaCalcStatusValues } from '../../common/const/doc'
import { ParaRunCalcState } from '../ParaContent/ParaRun'
import {
  RunElementIndices,
  updateElementLevelIndexFromPos,
} from '../../../re/export/textContent'
import { ParaTab } from '../RunElement/ParaTab'

interface ReshapeStartState {
  x: number
  isWordLetter: boolean
  isSegmentEnd: boolean
  wordWidth: number
  needMoveToLineBreak: boolean
  isStartHandleLetter: boolean
  isFirstOnLine: boolean
  isEmptyLine: boolean
  hasTextOnLine: boolean
  spaceCount: number
  isParaEnd: boolean
}

export class ParagraphCalcTabInfo {
  tabPosIndex: number
  x: number
  value: number
  tab: Nullable<ParaTab>
  isAtRightEdge?: boolean
  xLimit?: number
  constructor() {
    this.tabPosIndex = 0
    this.x = 0
    this.value = -1
    this.tab = null
  }

  reset() {
    this.tabPosIndex = 0
    this.x = 0
    this.value = -1
    this.tab = null
  }
}

export class ParagraphCalcState {
  paragraph!: Paragraph
  parent: Nullable<TextDocument>
  // state of current Indexes
  curLineIndex: number
  curColumnIndex: number

  lastTabInfo: ParagraphCalcTabInfo
  spaceCount: number
  wordWidth: number
  isWordLetter: boolean
  isFirstOnLine: boolean
  isStartHandleLetter: boolean
  isSegmentEnd: boolean
  x: number
  xEnd: number
  xSegment: number
  needMoveToLineBreak: boolean
  lineBreakElmPosition: ParagraphElementPosition
  isLineBreakFirst: boolean
  lastParaItem: Nullable<RunContentElement>
  reshapeStartPos: Nullable<RunElementIndices>
  reshapeStartState: Nullable<ReshapeStartState>

  lineArabicIndices: Nullable<RunElementIndices>

  // state of current line
  isEmptyLine: boolean
  isParaEnd: boolean
  isFirstLine: boolean
  hasTextOnLine: boolean
  textAscentOfLine: number
  textAscentFromMetricsForLine: number
  textDescentOfLine: number
  lineAscent: number
  lineDescent: number
  lineTop: number
  lineBottomTotal: number
  lineBottom: number
  linePreBottom: number

  y: number
  xOfStart: number
  xLimit: number
  yLimit: number
  baseLineOffY: number

  currentPosition: ParagraphElementPosition
  lastRunCalcInfo: Nullable<ParaRunCalcState>
  calcStatus: ParaCalcStatusValues

  constructor() {
    this.parent = null

    // state of current Indexes
    this.curLineIndex = 0
    this.curColumnIndex = 0

    this.lastTabInfo = new ParagraphCalcTabInfo()
    this.spaceCount = 0
    this.wordWidth = 0
    this.isWordLetter = false
    this.isFirstOnLine = true
    this.isStartHandleLetter = false
    this.isSegmentEnd = false
    this.x = 0
    this.xEnd = 0
    this.xSegment = 0
    this.needMoveToLineBreak = false
    this.lineBreakElmPosition = new ParagraphElementPosition()
    this.isLineBreakFirst = true
    this.lastParaItem = null
    this.reshapeStartPos = null

    // state of current line
    this.isEmptyLine = true
    this.isParaEnd = false
    this.isFirstLine = false
    this.hasTextOnLine = false
    this.textAscentOfLine = 0
    this.textAscentFromMetricsForLine = 0
    this.textDescentOfLine = 0
    this.lineAscent = 0
    this.lineDescent = 0
    this.lineTop = 0
    this.lineBottomTotal = 0
    this.lineBottom = 0
    this.linePreBottom = 0

    this.y = 0
    this.xOfStart = 0
    this.xLimit = 0
    this.yLimit = 0
    this.baseLineOffY = 0

    //-----------------------------//
    this.currentPosition = new ParagraphElementPosition()
    this.lastRunCalcInfo = null
    this.calcStatus = ParaCalcStatus.End
  }

  resetParaColumn(paragraph: Paragraph, colIndex) {
    this.paragraph = paragraph
    this.parent = paragraph.parent as Nullable<TextDocument>

    this.curColumnIndex = colIndex
    this.lastRunCalcInfo = null
  }

  resetLineMetricsState() {
    this.isEmptyLine = true
    this.isParaEnd = false
    this.isFirstLine = false
    this.hasTextOnLine = false

    this.textAscentOfLine = 0
    this.textAscentFromMetricsForLine = 0
    this.textDescentOfLine = 0
    this.lineAscent = 0
    this.lineDescent = 0
  }

  resetLineSegmentState(x, xEnd) {
    this.lastTabInfo.reset()

    this.spaceCount = 0
    this.wordWidth = 0
    this.isWordLetter = false
    this.isFirstOnLine = true
    this.isStartHandleLetter = false
    this.isSegmentEnd = false
    this.x = x
    this.xEnd = xEnd
    this.xSegment = x

    this.needMoveToLineBreak = false
    this.lineBreakElmPosition.reset()
    this.isLineBreakFirst = true
    this.lastParaItem = null
    this.clearReshapeStartPos()
    this.lineArabicIndices = null
  }

  updateLineBreakPosition(index: number, isFirstItem: boolean) {
    this.lineBreakElmPosition.setByPosition(this.currentPosition)
    this.lineBreakElmPosition.add(index)
    this.isLineBreakFirst = isFirstItem
  }

  setArabicIndicesByRunElementPos(pos: number) {
    const lineArabicIndices = updateElementLevelIndexFromPos(
      this.paragraph,
      this.currentPosition,
      pos
    )
    if (lineArabicIndices != null) {
      this.lineArabicIndices = lineArabicIndices
    }
  }

  updateReshapeStartPos(pos: number, reshapeStartState?: ReshapeStartState) {
    const runElmIndices = updateElementLevelIndexFromPos(
      this.paragraph,
      this.currentPosition,
      pos
    )
    if (runElmIndices != null) {
      this.reshapeStartPos = runElmIndices
    }

    if (reshapeStartState != null) {
      this.reshapeStartState = reshapeStartState
    }
  }
  setReshapeStartPos(
    runElmIndices: RunElementIndices,
    reshapeStartState?: ReshapeStartState
  ) {
    if (runElmIndices != null) {
      this.reshapeStartPos = runElmIndices
    }

    if (reshapeStartState != null) {
      this.reshapeStartState = reshapeStartState
    }
  }

  clearReshapeStartPos() {
    this.reshapeStartPos = null
    this.reshapeStartState = null
  }

  updateCursorPos(pos: number, lvl: number) {
    this.currentPosition.updatePosByLevel(pos, lvl)
  }

  getReshapeState(): ReshapeStartState {
    return {
      x: this.x,
      isWordLetter: this.isWordLetter,
      isSegmentEnd: this.isSegmentEnd,
      wordWidth: this.wordWidth,
      needMoveToLineBreak: this.needMoveToLineBreak,
      isStartHandleLetter: this.isStartHandleLetter,
      isFirstOnLine: this.isFirstOnLine,
      isEmptyLine: this.isEmptyLine,
      hasTextOnLine: this.hasTextOnLine,
      spaceCount: this.spaceCount,
      isParaEnd: this.isParaEnd,
    }
  }
}
export const gParagraphCalcState = new ParagraphCalcState()

export class ParagraphCalcCounterState {
  paragraph?: Paragraph
  lineSegment?: ParaLineSegment
  isInWord: boolean
  spaceWidth: number
  amountOfSpaces: number
  amountOfWords: number
  spaceTotalCount: number
  letterCount: number
  skipSpaceCount: number
  skipLetterCount: number
  constructor() {
    this.paragraph = undefined
    this.lineSegment = undefined
    this.isInWord = false
    this.spaceWidth = 0
    this.amountOfSpaces = 0

    this.amountOfWords = 0
    this.spaceTotalCount = 0
    this.letterCount = 0
    this.skipSpaceCount = 0
    this.skipLetterCount = 0
  }

  reset(paragraph: Paragraph, segment: ParaLineSegment) {
    this.paragraph = paragraph
    this.lineSegment = segment
    this.isInWord = false
    this.spaceWidth = 0
    this.amountOfSpaces = 0

    this.amountOfWords = 0
    this.spaceTotalCount = 0
    this.letterCount = 0
    this.skipSpaceCount = 0
    this.skipLetterCount = 0
  }
}
export const gParagraphCalcCounterState = new ParagraphCalcCounterState()

export class ParagraphCalcAlignState {
  x: number
  y: number
  xEnd: number
  justifyWordWidth: number
  justifySpaceWidth: number
  spaceCount: number
  skipSpaceCount: number
  skipLetterCount: number
  lastWidth: number
  paragraph?: Paragraph
  yTop: number
  yBottom: number
  currentSlideIndex: number
  constructor() {
    this.x = 0
    this.y = 0
    this.xEnd = 0
    this.justifyWordWidth = 0
    this.justifySpaceWidth = 0
    this.spaceCount = 0
    this.skipSpaceCount = 0
    this.skipLetterCount = 0
    this.lastWidth = 0
    this.paragraph = undefined

    this.yTop = 0
    this.yBottom = 0

    this.currentSlideIndex = 0
  }
  reset(para: Paragraph) {
    this.paragraph = para
    this.x = 0
    this.y = 0
    this.xEnd = 0
    this.justifyWordWidth = 0
    this.justifySpaceWidth = 0
    this.spaceCount = 0
    this.skipSpaceCount = 0
    this.skipLetterCount = 0
    this.lastWidth = 0

    this.yTop = 0
    this.yBottom = 0

    this.currentSlideIndex = 0
  }
}
export const gParagraphCalcAlignState = new ParagraphCalcAlignState()
