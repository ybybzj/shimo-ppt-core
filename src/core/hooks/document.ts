import { EditActionFlag } from '../EditActionFlag'
import { TrackPerfType } from '../../exports/type'
import { createActionHook, createReturnableHook } from '../../lib/hook'

// getters
export const GetViewMode = createReturnableHook<void, boolean>('GetViewMode')

export const CheckChangeStackLocked = createActionHook<{
  stopRecordingCouter: number
  editType: EditActionFlag
}>('CheckChangeStackLocked')

export const ResetChangeStackLocked = createActionHook<void>(
  'ResetChangeStackLocked'
)

export const InvalidChangeWithNoAction = createActionHook<void>(
  'InvalidChangeWithNoAction'
)

export const TrackPerf = createActionHook<{
  type: TrackPerfType
  timing: number
}>('TrackPerf')
