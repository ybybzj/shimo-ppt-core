import { Nullable } from '../../../liber/pervasive'
import { ResourcesContainerType } from '../../editor/utils/resourcesLoadManager'
import { LoadableImage } from '../../images/utils'
import { createActionHook, createReturnableHook } from '../../lib/hook'
import { ImageShape } from '../SlideElement/Image'
import { AttrWithAssets } from '../SlideElement/attrs/type'
import { ImageWidgetType } from '../utilities/shape/image'

export const CollectImageFromDataChange = createActionHook<{
  imageUrl: string
}>('CollectImageFromDataChange')

export const CollectBase64ImageFromDataChange = createActionHook<{
  dataUrl: string
  sp: ImageShape
}>('CollectBase64ImageFromDataChange')

export const CollectFontFromDataChange = createActionHook<{ font: string }>(
  'CollectFontFromDataChange'
)

export const CollectImageUrlParamsFromDataChange = createActionHook<{
  imgSrc: string
  type: 'chart' | 'image'
  extX: number
  extY: number
}>('CollectImageUrlParamsFromDataChange')

export const CollectAssetsForPasting = createActionHook<{
  assetUrl: string
  assetObject: AttrWithAssets
}>('CollectAssetsForPasting')

export const GroupCollectedResources = createActionHook<{
  type: ResourcesContainerType
  refId?: string
  isApply?: boolean
}>('GroupCollectedResources')

export const GetImageForChangeOfficalTheme = createReturnableHook<
  { url: string },
  string
>('GetImageForChangeOfficalTheme')

export const CheckKnownErrorImageUrl = createActionHook<{
  media?: string
  url: string
}>('CheckUpdateImageUrlToErrorInfoMap')

export const getImageOriginUrl = createReturnableHook<{ url: string }, string>(
  'getImageOriginUrl'
)

export const GetImageWidgetType = createReturnableHook<
  { sp: ImageShape },
  ImageWidgetType
>('GetImageWidgetType')
export const GetLoadableImageByImageSrc = createReturnableHook<
  { src: string },
  Nullable<LoadableImage>
>('GetLoadableImageByImageSrc')
