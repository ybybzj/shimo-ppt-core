import { createActionHook } from '../../lib/hook'
import { SlideElement } from '../SlideElement/type'

export type EditorEvents =
  | 'Error'
  | 'VerticalTextAlign'
  | 'Vert'
  | 'HideComment'
  | 'HoveredCommentChange'
  | 'SelectedCommentChange'
  | 'ToggleCommentShow'
  | 'ContextMenu'
  | 'MouseMoveStart'
  | 'MouseMoveEnd'
  | 'MouseMove'
  | 'SelectionEnd'
  | 'SlideBackgroundChange'
  | 'Print'
  | 'HoveredObjectChange'
  | 'FindText'

interface EditorEventHookParams {
  eventType: EditorEvents
  args: any[]
}
export const EmitEvent = createActionHook<EditorEventHookParams>('EmitEvent')

export const EmitHyperProp = createActionHook<{ hyperlinkOptions }>(
  'EmitHyperProp'
)

export const EmitAddComment = createActionHook<{ id; CommentData }>(
  'EmitAddComment'
)

export const EmitChangeComment = createActionHook<void>('EmitChangeComment')

export const EmitChangeCommentAuthor = createActionHook<void>(
  'EmitChangeCommentAuthor'
)

export const EmitRemoveComment = createActionHook<{ id }>('EmitRemoveComment')

export const EmitEndAddShape = createActionHook<void>('EmitEndAddShape')

export const EmitDoubleClick = createActionHook<SlideElement>('EmitDoubleClick')
