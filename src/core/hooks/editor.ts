import { createActionHook } from '../../lib/hook'
import { Slide } from '../Slide/Slide'

export const OnRefreshPresentation = createActionHook<{
  isDirty: boolean
}>('OnRefreshPresentation')

export const OnAsyncSlideLoaded = createActionHook<{
  slide: Slide
}>('OnAsyncSlideLoaded')
