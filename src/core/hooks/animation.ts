import { createActionHook } from '../../lib/hook'
import { Slide } from '../Slide/Slide'

export const OnReplaceShape = createActionHook<{
  slide: Slide
  oldRefId
  newRefId
}>('OnReplaceShape')

export const OnRemoveShape = createActionHook<{
  slide: Slide
  refId
}>('OnRemoveShape')
