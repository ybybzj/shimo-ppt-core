import { createActionHook } from '../../lib/hook'

export const OnAddComment = createActionHook<{ id; data }>('OnAddComment')
export const OnRemoveComment = createActionHook<{ id }>('OnRemoveComment')
