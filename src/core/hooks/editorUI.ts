import { Nullable } from '../../../liber/pervasive'
import { LoadableImage } from '../../images/utils'
import { createActionHook, createReturnableHook } from '../../lib/hook'
import { SlideElement } from '../SlideElement/type'
import { SnapPos } from '../../ui/features/guideLines/type'
import { FocusTarget } from '../common/const/drawing'
import { ShapeDrawerContext } from '../graphic/type'
import { ImageShape } from '../SlideElement/Image'
import { ParaPr } from '../textAttributes/ParaPr'

export const OnUpdateLayouts = createActionHook<boolean>('OnUpdateLayouts')

interface UpdateParaTabParams {
  defaultTab: any
  paraTabs: any
}
export const UpdateParaTab =
  createActionHook<UpdateParaTabParams>('UpdateParaTab')

export const OnUpdateParagraphProp = createActionHook<{ paraPr: ParaPr }>(
  'OnUpdateParagraphProp'
)

export const UpdateTextPr = createActionHook<{ textPr }>('UpdateTextPr')

export const HyperlinkClick = createActionHook<{ value }>('HyperlinkClick')

export const AddSlideSelection = createActionHook<{
  slideIndex
  x
  y
  width
  height
  matrix?
}>('AddSlideSelection')

export const setCursorColor = createActionHook<{ r; g; b }>('setCursorColor')

export const setCursorSize = createActionHook<{ size; ascent }>('setCursorSize')

export const UpdateCursor = createActionHook<{ x; y; slideIndex }>(
  'UpdateCursor'
)

export const OnCursorShow = createActionHook<void>('OnCursorShow')

export const OnCalculateSlide = createActionHook<{ index }>('OnCalculateSlide')

export const OnGoToSlide = createActionHook<{
  slideIndex: number
  isForce?: boolean
}>('OnGoToSlide')

export const OnScrollToCenter = createActionHook<{
  slideIndex: number
}>('OnScrollToCenter')

export const OnScrollTo = createActionHook<{ mmX: number; mmY: number }>(
  'OnScrollTo'
)

export const OnLockCursor = createActionHook<void>('OnLockCursor')

export const OnSetInTextSelection = createActionHook<{ isEnabled: boolean }>(
  'OnSetInTextSelection'
)

export const OnMultiplyTargetTransform = createActionHook<{
  transform: any
  callback: () => void
}>('OnMultiplyTargetTransform')

export const OnUpdateCursorTransform = createActionHook<{ transform }>(
  'OnUpdateCursorTransform'
)

export const OnCalculateNotes = createActionHook<{
  slideIndex
  notesWidth
  notesHeight
}>('OnCalculateNotes')

export const OnUpdateEditLayer = createActionHook<void>('OnUpdateEditLayer')

export const OnFirePaint = createActionHook<void>('OnFirePaint')
export const OnFireNotePaint = createActionHook<void>('OnFireNotePaint')
export const OnCalculatePreviews = createActionHook<void>('OnCalculatePreviews')

export const OnUpdatePlaceholders = createActionHook<{
  slideIndex: number
}>('OnUpdatePlaceholders')

export const OnSyncInterfaceState = createActionHook<void>(
  'OnSyncInterfaceState'
)
export const OnUpdateSelectionState = createActionHook<void>(
  'OnUpdateSelectionState'
)

export const OnComponentFocus = createActionHook<{
  focusType: FocusTarget
}>('OnComponentFocus')

export const OnMoveCursorForward = createActionHook<void>('OnMoveCursorForward')

export const OnApplyPropsWithImageSrcId = createActionHook<void>(
  'OnApplyPropsWithImageSrcId'
)

// getters

export const ConvertCoordsToCursorPos = createReturnableHook<
  { x; y; transform? },
  {
    x: number
    y: number
    Error: boolean
  }
>('ConvertCoordsToCursorPos')

export const GetImageFromLoader = createReturnableHook<
  string,
  Nullable<LoadableImage>
>('GetImageFromLoader')

export const GetWidthOfNotes = createReturnableHook<void, number>(
  'GetWidthOfNotes'
)

export const GetFocusElementType = createReturnableHook<void, FocusTarget>(
  'GetFocusElementType'
)

export const GetSlideElementNeedDraw = createReturnableHook<
  { sp: SlideElement; graphics: ShapeDrawerContext },
  boolean
>('GetSlideElementNeedDraw')

export const GetIsInShowMod = createReturnableHook<void, boolean>(
  'GetIsInShowMod'
)

export const OnUpdateScrolls = createActionHook<void>('OnUpdateScrolls')

export const OnUpdateImageSubType = createActionHook<{
  image: ImageShape
}>('OnUpdateImageSubType')

export const OnAddGuideline = createActionHook<{ line: SnapPos }>(
  'OnAddGuideline'
)
