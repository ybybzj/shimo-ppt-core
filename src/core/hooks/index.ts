import * as Editor from './editor'
import * as EditorUI from './editorUI'
import * as Attrs from './attrs'
import * as Document from './document'
import * as Emit from './emit'
import * as Comments from './comments'
import * as Cursor from './cursor'
import * as Animation from './animation'
export * from './manager'

export const Hooks = {
  Editor,
  EditorUI,
  Attrs,
  Document,
  Emit,
  Comments,
  Cursor,
  Animation,
} as const
