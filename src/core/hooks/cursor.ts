import { createActionHook, createReturnableHook } from '../../lib/hook'

export const GetHoveredStatus = createReturnableHook<
  { gTableId: string },
  boolean
>('GetHoveredStatus')

export const GetIsInTouchMode = createReturnableHook<void, boolean>(
  'GetIsInTouchMode'
)

export const DrawSelectionCursor = createActionHook<{
  edge: 'start' | 'end'
  x: number
  y: number
  size: number
}>('DrawSelectionCursor')
