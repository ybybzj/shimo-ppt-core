import { Nullable } from '../../liber/pervasive'
import { SlideElement } from './SlideElement/type'
import { Comment } from './Slide/Comments/Comment'

type RegistryEntity = SlideElement | Comment

export class EntityRegistry {
  store: Map<string, any>
  tempStore: Map<string, any> // 用于在有连接线的场景保存临时的id引用
  disallowAdd: boolean
  ignoreAddCheck: boolean = false
  constructor() {
    this.store = new Map()
    this.tempStore = new Map()
    this.disallowAdd = false
  }

  // eslint-disable-next-line @typescript-eslint/naming-convention
  add(entity: RegistryEntity, id: string) {
    if (this.ignoreAddCheck === true || false === this.disallowAdd) {
      entity.id = id
      this.store.set(id, entity)
    } else {
      this.tempStore.set(id, entity)
    }
  }

  getEntityById<T extends RegistryEntity = RegistryEntity>(
    id: Nullable<string | number>,
    needPromote = false
  ): Nullable<T> {
    if (id == null) return undefined
    id = '' + id
    if ('' === id) return undefined

    let entity = this.store.get(id)
    if (entity != null) {
      return entity
    }

    entity = this.tempStore.get(id)
    if (entity != null) {
      if (needPromote && entity) {
        this.store.set(id, entity)
      }
      return entity
    }

    return undefined
  }

  removeEntityById(id: string) {
    if (id == null || '' === id) return
    if (this.store && this.store.has(id)) {
      this.store.delete(id)
    }
  }

  reset() {
    this.store.clear()
    this.tempStore.clear()
    this.disallowAdd = false
  }

  resetTemPairs() {
    this.tempStore.clear()
  }
}

export const gEntityRegistry = new EntityRegistry()
