import { EditActionFlag } from '../EditActionFlag'
import { EditorUtil } from '../../globals/editor'
import { SlideElementBase } from './SlideElementBase'
import { idGenerator } from '../../globals/IdGenerator'

import { Nullable } from '../../../liber/pervasive'
import { InstanceType } from '../instanceTypes'
import { calcTransform } from '../utilities/shape/calcs'

import { EffectType, ImageSubTypes } from '../../io/dataType/spAttrs'
import { BlipFill } from './attrs/fill/blipFill'
import { SpPr, UniNvPr } from './attrs/shapePrs'
import { ShapeStyle } from './attrs/ShapeStyle'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { Geometry } from './geometry/Geometry'
import { SlideElementParent } from '../Slide/type'
import { Ln } from './attrs/line'
import {
  CalcStatus,
  ImageShapeCalcStatusCode,
  ImageShapeCalcStatusFlags,
  initCalcStatusCodeForImageShape,
} from '../calculation/updateCalcStatus/calcStatus'
import { updateCalcStatusForImageShape } from '../calculation/updateCalcStatus/image'
import { OleObject } from './OleObject'
import { ChartObject } from './ChartObject'

export class ImageShape extends SlideElementBase {
  readonly instanceType = InstanceType.ImageShape
  style: Nullable<ShapeStyle>
  calcStatusCode!: ImageShapeCalcStatusCode
  calcedLine: Nullable<Ln>

  nvPicPr: Nullable<UniNvPr>
  base64Img: Nullable<string>
  base64ImgHeight: Nullable<number>
  base64ImgWidth: Nullable<number>
  blipFill!: BlipFill
  subType?: ImageSubTypes
  calcedGeometry: Nullable<Geometry>
  constructor() {
    super()
    this.nvPicPr = null
    this.style = null

    this.id = idGenerator.newId()
    EditorUtil.entityRegistry.add(this, this.id)
    this.resetCalcStatus()
  }
  resetCalcStatus() {
    this.calcStatusCode =
      initCalcStatusCodeForImageShape as ImageShapeCalcStatusCode
  }

  setCalcStatusOf(flag: ImageShapeCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode |
      flag) as ImageShapeCalcStatusCode
  }

  unsetCalcStatusOf(flag: ImageShapeCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode ^
      flag) as ImageShapeCalcStatusCode
  }

  isCalcStatusSet(flag: ImageShapeCalcStatusFlags) {
    return (this.calcStatusCode & flag) > 0
  }

  isOleObject(): this is OleObject {
    return false
  }
  isChartObject(): this is ChartObject {
    return false
  }
  isValid() {
    if (!this.blipFill) {
      return false
    }
    if (!this.spPr) {
      return false
    }
    return true
  }

  setSubType(type?: ImageSubTypes) {
    this.subType = type
  }
  setNvPicPr(pr: Nullable<UniNvPr>) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_ImageShapeSetNvPicPr,
      this.nvPicPr,
      pr
    )
    this.nvPicPr = pr
  }
  setSpPr(pr: Nullable<SpPr>) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_ImageShapeSetSpPr,
      this.spPr,
      pr
    )
    this.spPr = pr
    updateCalcStatusForImageShape(this, EditActionFlag.edit_ImageShapeSetSpPr)
  }
  setBlipFill(pr: BlipFill) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_ImageShapeSetBlipFill,
      this.blipFill,
      pr
    )
    this.blipFill = pr
    updateCalcStatusForImageShape(
      this,
      EditActionFlag.edit_ImageShapeSetBlipFill
    )
  }
  setParent(pr: Nullable<SlideElementParent>) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_ImageShapeSetParent,
      this.parent,
      pr
    )
    this.parent = pr
  }

  setStyle(pr: Nullable<ShapeStyle>) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_ImageShapeSetStyle,
      this.style,
      pr
    )
    this.style = pr
  }

  getImageUrl() {
    return {
      url: this.blipFill?.imageSrcId,
      encryptUrl: this.blipFill?.encryptUrl,
    }
  }

  getName() {
    return this.nvPicPr?.cNvPr?.name
  }

  isShape() {
    return false
  }
  isImage() {
    return true
  }

  getTransform() {
    if (this.isCalcStatusSet(CalcStatus.ImageShape.Transform)) {
      calcTransform(this)
      this.unsetCalcStatusOf(CalcStatus.ImageShape.Transform)
    }
    return this.transform
  }

  setNvSpPr(pr: Nullable<UniNvPr>) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_ImageShapeSetNvPicPr,
      this.nvPicPr,
      pr
    )
    this.nvPicPr = pr
  }

  getTransparent(): number | undefined {
    let amt: number | undefined
    if (this.blipFill?.effects) {
      this.blipFill.effects.find((effect) => {
        if (effect.Type === EffectType.ALPHAMODFIX) {
          amt = 100 - effect.amt
        }
      })
    }
    return amt
  }
}
