import { Matrix2D } from '../graphic/Matrix'
import { SlideElementBase } from './SlideElementBase'
import { idGenerator } from '../../globals/IdGenerator'
import { EditorUtil } from '../../globals/editor'

import { Nullable } from '../../../liber/pervasive'
import { TextBody } from './TextBody'
import { InstanceType, isInstanceTypeOf } from '../instanceTypes'
import { Geometry } from './geometry/Geometry'
import { ClipRect } from '../graphic/type'
import { GraphicBounds } from '../graphic/Bounds'
import { BlipFill } from './attrs/fill/blipFill'
import { UniNvPr } from './attrs/shapePrs'
import { ShapeStyle } from './attrs/ShapeStyle'
import { Theme } from './attrs/theme'
import { Ln } from './attrs/line'
import { isNumber } from '../../common/utils'
import { EditActionFlag } from '../EditActionFlag'
import { translator } from '../../globals/translator'
import { gDefaultColorMap } from '../color/clrMap'

import { TextDocument } from '../TextDocument/TextDocument'
import { hookManager, Hooks } from '../hooks'
import { MarginPaddings } from '../properties/tableProp'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { createTextDocumentFromString } from '../utilities/tableOrTextDocContent/creators'
import { getPresentation } from '../utilities/finders'
import { isLineShape } from '../utilities/shape/asserts'
import { checkExtentsByTextContent } from '../utilities/shape/extents'
import {
  getTextDocumentOfSp,
  getParentsOfElement,
} from '../utilities/shape/getters'
import { getTextStylesForSp } from '../utilities/shape/style'
import {
  TextAutoFitType,
  TextWarpType,
  PlaceholderTexts,
  PlaceholderType,
} from './const'
import { BodyPr } from './attrs/bodyPr'
import { moveCursorToStartPosInTextDocument } from '../utilities/cursor/moveToEdgePos'
import {
  addToParagraphInTextDocument,
  applyTextPrToParagraphInTextDocument,
} from '../TextDocument/utils/operations'
import { SlideElementParent } from '../Slide/type'
import { ContentMetrics } from '../calculation/calculate/shapeContent'
import {
  CalcStatus,
  initCalcStatusCodeForShape,
  ShapeCalcStatusCode,
  ShapeCalcStatusFlags,
} from '../calculation/updateCalcStatus/calcStatus'
import { updateCalcStatusForShape } from '../calculation/updateCalcStatus/shape'
import { updateCalcStatusForTextBody } from '../calculation/updateCalcStatus/textContent'
import { RunContentElement } from '../Paragraph/RunElement/type'
import { TextPrOptions } from '../textAttributes/TextPr'

export class Shape extends SlideElementBase {
  instanceType = InstanceType.Shape
  attrUseBgFill: boolean = false

  nvSpPr: Nullable<UniNvPr>
  style: Nullable<ShapeStyle>
  txBody!: TextBody | null
  textTransform: Matrix2D
  invertTextTransform: Nullable<Matrix2D>
  calcedTextTransform: Matrix2D
  id: string
  calcStatusCode!: ShapeCalcStatusCode
  calcInfo: {
    metricsOfContent: Nullable<ContentMetrics>
  }

  calcedLine: Nullable<Ln>
  clipRect?: ClipRect
  textTransformForPh: any // for phContent
  clipRectForPh?: ClipRect
  invertTextTransformForPh: any
  contentHeight: any
  base64Img: Nullable<string>
  base64ImgHeight: Nullable<number>
  base64ImgWidth: Nullable<number>

  rot!: number

  contentWidth?: number
  isRightButtonPressed: boolean = false
  bounds!: GraphicBounds
  calcedGeometry: Nullable<Geometry>
  blipFill?: BlipFill
  themeOverride?: Theme
  constructor() {
    super()
    this.nvSpPr = null
    this.style = null
    this.txBody = null

    this.textTransform = new Matrix2D()
    this.invertTextTransform = null

    this.calcedTextTransform = new Matrix2D()
    this.base64Img = null

    this.id = idGenerator.newId()
    EditorUtil.entityRegistry.add(this, this.id)
    this.calcInfo = {
      metricsOfContent: null,
    }
    this.calcStatusCode = initCalcStatusCodeForShape as ShapeCalcStatusCode
  }

  resetCalcStatus() {
    this.calcStatusCode = initCalcStatusCodeForShape as ShapeCalcStatusCode
    this.calcInfo.metricsOfContent = null
  }

  setCalcStatusOf(flag: ShapeCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode | flag) as ShapeCalcStatusCode
  }

  unsetCalcStatusOf(flag: ShapeCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode ^ flag) as ShapeCalcStatusCode
  }

  isCalcStatusSet(flag: ShapeCalcStatusFlags) {
    return (this.calcStatusCode & flag) > 0
  }

  isConnectionShape() {
    return false
  }

  setNvSpPr(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_ShapeSetNvSpPr,
      this.nvSpPr,
      pr
    )
    this.nvSpPr = pr
    updateCalcStatusForShape(this, EditActionFlag.edit_ShapeSetNvSpPr)
  }

  setSpPr(spPr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_ShapeSetSpPr,
      this.spPr,
      spPr
    )
    this.spPr = spPr
    updateCalcStatusForShape(this, EditActionFlag.edit_ShapeSetSpPr)
  }

  setStyle(style) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_ShapeSetStyle,
      this.style,
      style
    )
    this.style = style
    const textDoc = getTextDocumentOfSp(this)

    if (textDoc) {
      textDoc.invalidateAllParagraphsCalcedPrs()
    }
    updateCalcStatusForShape(this, EditActionFlag.edit_ShapeSetStyle)
  }

  setTxBody(txBody: TextBody) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_ShapeSetTxBody,
      this.txBody,
      txBody
    )
    this.txBody = txBody
    updateCalcStatusForShape(this, EditActionFlag.edit_ShapeSetTxBody)
  }

  addParaItem(paraItem: RunContentElement, isDirty: boolean) {
    let textDoc = getTextDocumentOfSp(this)
    if (!textDoc) {
      this.createTxBody()
      textDoc = getTextDocumentOfSp(this)
    }
    if (textDoc) {
      addToParagraphInTextDocument(textDoc, paraItem, isDirty)
    }
  }

  applyTextPr(textPr: TextPrOptions, isDirty: boolean) {
    let textDoc = getTextDocumentOfSp(this)
    if (!textDoc) {
      this.createTxBody()
      textDoc = getTextDocumentOfSp(this)
    }
    if (textDoc) {
      applyTextPrToParagraphInTextDocument(textDoc, textPr, isDirty)
    }
  }
  clearContent() {
    const textDoc = getTextDocumentOfSp(this)
    if (textDoc) {
      textDoc.isSetToAll = true
      textDoc.remove(-1)
      applyTextPrToParagraphInTextDocument(
        textDoc,
        { lang: { val: undefined } },
        false
      )
      textDoc.isSetToAll = false
    }
  }

  getBodyPr(): BodyPr {
    if (this.txBody && this.txBody.bodyPr) {
      return this.txBody.getCalcedBodyPr()!
    }
    const ret = BodyPr.Default()
    return ret
  }

  getGeometry(): Nullable<Geometry> {
    return this.calcedGeometry || this.spPr?.geometry
  }

  setParent(parent: Nullable<SlideElementParent>) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_ShapeSetParent,
      this.parent,
      parent
    )
    this.parent = parent
  }

  canFill() {
    if (this.spPr && this.spPr.geometry) {
      return this.spPr.geometry.canFill()
    }
    return true
  }

  isShape() {
    return true
  }

  isImage() {
    return false
  }

  getPaddings() {
    let paddings: undefined | MarginPaddings
    const shape = this
    let bodyPr
    if (shape.txBody) {
      bodyPr = shape.txBody.bodyPr
    }
    if (bodyPr) {
      paddings = new MarginPaddings()
      if (typeof bodyPr.lIns === 'number') {
        paddings.left = bodyPr.lIns
      } else paddings.left = 2.54

      if (typeof bodyPr.tIns === 'number') {
        paddings.top = bodyPr.tIns
      } else paddings.top = 1.27

      if (typeof bodyPr.rIns === 'number') {
        paddings.right = bodyPr.rIns
      } else paddings.right = 2.54

      if (typeof bodyPr.bIns === 'number') {
        paddings.bottom = bodyPr.bIns
      } else paddings.bottom = 1.27
    }
    return paddings
  }

  setVerticalAlign(align) {
    let bodyPr = this.getBodyPr()
    if (bodyPr) {
      bodyPr = bodyPr.clone()
      bodyPr.anchor = align
      if (this.txBody) {
        this.txBody.setBodyPr(bodyPr)
        updateCalcStatusForTextBody(this.txBody)
      }
    }
  }

  setVert(vert) {
    let bodyPr = this.getBodyPr()
    if (bodyPr) {
      bodyPr = bodyPr.clone()
      bodyPr.vert = vert
      if (this.txBody) {
        this.txBody.setBodyPr(bodyPr)
      }
    }
    checkExtentsByTextContent(this)
  }

  setPaddings(paddings: MarginPaddings) {
    if (paddings) {
      let bodyPr = this.getBodyPr()
      if (bodyPr) {
        bodyPr = bodyPr.clone()
        if (isNumber(paddings.left)) {
          bodyPr.lIns = paddings.left
        }

        if (isNumber(paddings.top)) {
          bodyPr.tIns = paddings.top
        }

        if (isNumber(paddings.right)) {
          bodyPr.rIns = paddings.right
        }
        if (isNumber(paddings.bottom)) {
          bodyPr.bIns = paddings.bottom
        }

        if (this.txBody) {
          this.txBody.setBodyPr(bodyPr)
        }
      }
    }
  }

  getTextRect() {
    return (
      this.spPr?.geometry?.rect ?? {
        l: 0,
        t: 0,
        r: this.extX,
        b: this.extY,
      }
    )
  }

  getTextStyles(level: number) {
    return getTextStylesForSp(this, level)
  }

  getTextTransformOfParent() {
    return this.textTransform.clone()
  }

  needAutofit() {
    const textDoc = getTextDocumentOfSp(this)
    if (textDoc && !textDoc.isEmpty({ ignoreEnd: true })) {
      const bodyPr = this.getBodyPr()
      if (bodyPr.textFit && bodyPr.textFit.type === TextAutoFitType.SHAPE) {
        return true
      }
    }

    return false
  }

  getTransform() {
    return this.transform
  }

  getAngle(x, y) {
    const px = this.invertTransform.XFromPoint(x, y)
    const py = this.invertTransform.YFromPoint(x, y)
    return (
      Math.PI * 0.5 + Math.atan2(px - this.extX * 0.5, py - this.extY * 0.5)
    )
  }

  getTheme() {
    return getParentsOfElement(this).theme
  }

  getCalcedParaPr() {
    if (this.txBody && this.txBody.textDoc) {
      this.txBody.textDoc.isSetToAll = true
      const result = this.txBody.textDoc.getCalcedParaPr()
      this.txBody.textDoc.isSetToAll = false
      return result
    }
    return null
  }

  getCalcedTextPr() {
    if (this.txBody && this.txBody.textDoc) {
      this.txBody.textDoc.isSetToAll = true
      const result = this.txBody.textDoc.getCalcedTextPr()
      this.txBody.textDoc.isSetToAll = false
      return result
    }
    return null
  }

  setFill(fill) {
    this.spPr!.setFill(fill)
  }

  remove(count, isOnlyText, isRemoveOnlySelection) {
    if (this.txBody) {
      this.txBody!.textDoc!.remove(count, isOnlyText, isRemoveOnlySelection)
      this.setCalcStatusOf(CalcStatus.Shape.Content)
      this.setCalcStatusOf(CalcStatus.Shape.TextTransform)
    }
  }

  isValid() {
    if (!this.spPr) {
      return false
    }
    return true
  }

  getColumnCount() {
    const bodyPr = this.getBodyPr()
    if (isNumber(bodyPr.numCol)) {
      return bodyPr.numCol
    }
    return 1
  }

  getColumnSpace() {
    const bodyPr = this.getBodyPr()
    if (isNumber(bodyPr.spcCol)) {
      return bodyPr.spcCol
    }
    return 0
  }

  normalizeWrapForBodyPr() {
    if (!this.txBody) {
      return
    }
    let bodyPr = this.getBodyPr()
    const { numCol, wrap } = bodyPr
    if (isNumber(numCol) && numCol > 1) {
      if (wrap === TextWarpType.NONE) {
        bodyPr = bodyPr.clone()
        bodyPr.wrap = TextWarpType.SQUARE
        this.txBody.setBodyPr(bodyPr)
      }
    }
  }

  setColumnCount(count) {
    if (!isLineShape(this)) {
      let bodyPr = this.getBodyPr()
      if (bodyPr) {
        bodyPr = bodyPr.clone()
        bodyPr.numCol = count >> 0

        if (!this.txBody) {
          this.createTxBody()
        }
        if (this.txBody) {
          this.txBody.setBodyPr(bodyPr)
        }
        this.normalizeWrapForBodyPr()
      }
    }
  }

  setSpaceColumn(spcCol) {
    if (!isLineShape(this)) {
      let bodyPr = this.getBodyPr()
      if (bodyPr) {
        bodyPr = bodyPr.clone()
        bodyPr.spcCol = spcCol
        if (!this.txBody) {
          this.createTxBody()
        }
        if (this.txBody) {
          this.txBody.setBodyPr(bodyPr)
        }
        this.normalizeWrapForBodyPr()
      }
    }
  }

  getColorMap() {
    const parents = getParentsOfElement(this)
    if (parents.slide && parents.slide.clrMap) {
      return parents.slide.clrMap
    } else if (parents.layout && parents.layout.clrMap) {
      return parents.layout.clrMap
    } else if (parents.master && parents.master.clrMap) {
      return parents.master.clrMap
    }
    return gDefaultColorMap
  }

  selectSelf() {
    const presentation = getPresentation(this)
    if (this.parent && presentation) {
      const selectionState = presentation.selectionState
      selectionState.reset(true)
      const parent = this.parent
      let slideIndex: Nullable<number>
      let focusOnNotes = false
      if (parent.instanceType === InstanceType.Slide) {
        slideIndex = parent.index
      } else if (parent.instanceType === InstanceType.Notes) {
        focusOnNotes = true
        slideIndex = parent.slide?.index
      }
      if (slideIndex != null && presentation.currentSlideIndex !== slideIndex) {
        hookManager.invoke(Hooks.EditorUI.OnGoToSlide, {
          slideIndex: slideIndex,
          isForce: true,
        })
      }
      presentation.isNotesFocused = focusOnNotes
      selectionState.setTextSelection(this)
    }
  }

  isCurrentSelected() {
    const selectionState = getPresentation(this)?.selectionState
    if (selectionState) {
      return selectionState.selectedTextSp === this
    }
    return false
  }

  makePlaceholderContent(): TextDocument {
    const ph = this.nvSpPr!.nvPr.ph!
    const phType = ph.type! as number

    let textForPlaceholder
    if (
      isInstanceTypeOf(this.parent, InstanceType.Notes) &&
      phType === PlaceholderType.body
    ) {
      textForPlaceholder = 'Click to add notes'
    } else {
      textForPlaceholder =
        typeof PlaceholderTexts[phType] === 'string' &&
        PlaceholderTexts[phType].length > 0
          ? PlaceholderTexts[phType]
          : PlaceholderTexts[PlaceholderType.body]

      if (phType === PlaceholderType.pic) {
        textForPlaceholder = ''
      }
    }

    return createTextDocumentFromString(
      translator.getValue(textForPlaceholder),
      this.txBody!
    )
  }

  createTxBody() {
    const tx_body = new TextBody(this)
    const textDoc = TextDocument.new(tx_body, 0, 0, 0, 20000)
    textDoc.children.at(0)!.setIndexInDocument(0)

    tx_body.textDoc = textDoc
    tx_body.bodyPr = new BodyPr()

    moveCursorToStartPosInTextDocument(tx_body.textDoc!, false)
    this.setTxBody(tx_body)
  }
}
