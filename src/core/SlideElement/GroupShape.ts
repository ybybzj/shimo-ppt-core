import { EditActionFlag } from '../EditActionFlag'
import { idGenerator } from '../../globals/IdGenerator'
import { EditorUtil } from '../../globals/editor'
import { SlideElementBase } from './SlideElementBase'
import { isNumber } from '../../common/utils'
import { Nullable } from '../../../liber/pervasive'

import { SlideElement } from './type'
import { InstanceType } from '../instanceTypes'

import { isGroup } from '../utilities/shape/asserts'
import { calcTransform } from '../utilities/shape/calcs'
import { onSlideElemntRemoved } from '../utilities/shape/updates'
import { updateCalcStatusForGroup } from '../calculation/updateCalcStatus/group'
import { findClosestParent, getPresentation } from '../utilities/finders'
import { UniNvPr } from './attrs/shapePrs'
import { informEntityChildrenChange } from '../calculation/informChanges/contentChange'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { SlideElementParent } from '../Slide/type'
import {
  CalcStatus,
  GroupShapeCalcStatusCode,
  GroupShapeCalcStatusFlags,
  initCalcStatusCodeForGroupShape,
} from '../calculation/updateCalcStatus/calcStatus'
import { Matrix2D } from '../graphic/Matrix'
import { CSld } from '../Slide/CSld'
import { adjustXfrmOfGroup } from '../utilities/shape/group'

export class GroupShape extends SlideElementBase {
  readonly instanceType = InstanceType.GroupShape
  nvGrpSpPr: Nullable<UniNvPr>
  spTree: SlideElement[]
  scaleFactors: { cx: number; cy: number }
  slideElementsInGroup: SlideElement[]
  id: string
  calcStatusCode!: GroupShapeCalcStatusCode
  base64Img: Nullable<string>
  base64ImgHeight: Nullable<number>
  base64ImgWidth: Nullable<number>

  rot: any

  get selectionState() {
    const presentation = getPresentation(this)
    if (presentation) {
      return presentation.selectionState
    }
    return null
  }
  get selection() {
    return this.selectionState?.groupSelection
  }
  constructor() {
    super()
    this.nvGrpSpPr = null
    this.spTree = []

    this.scaleFactors = { cx: 1, cy: 1 }

    this.slideElementsInGroup = []

    this.id = idGenerator.newId()
    EditorUtil.entityRegistry.add(this, this.id)
    this.resetCalcStatus()
  }

  resetCalcStatus() {
    this.calcStatusCode =
      initCalcStatusCodeForGroupShape as GroupShapeCalcStatusCode
    this.calcedTransform = new Matrix2D()
  }

  setCalcStatusOf(flag: GroupShapeCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode |
      flag) as GroupShapeCalcStatusCode
  }

  unsetCalcStatusOf(flag: GroupShapeCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode ^
      flag) as GroupShapeCalcStatusCode
  }

  isCalcStatusSet(flag: GroupShapeCalcStatusFlags) {
    return (this.calcStatusCode & flag) > 0
  }

  checkRemoveCache() {
    for (let i = 0; i < this.spTree.length; ++i) {
      const spTree: any = this.spTree[i]
      spTree.checkRemoveCache && spTree.checkRemoveCache()
    }
  }

  setNvGrpSpPr(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_GroupShapeSetNvGrpSpPr,
      this.nvGrpSpPr,
      pr
    )
    this.nvGrpSpPr = pr
  }
  setSpPr(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_GroupShapeSetSpPr,
      this.spPr,
      pr
    )
    this.spPr = pr
    updateCalcStatusForGroup(this, EditActionFlag.edit_GroupShapeSetSpPr)
  }
  addToSpTree(index, sp: SlideElement) {
    if (!isNumber(index)) index = this.spTree.length
    informEntityChildrenChange(
      this,
      EditActionFlag.edit_GroupShapeAddToSpTree,
      index,
      [sp],
      true
    )
    updateCalcStatusForGroup(this, EditActionFlag.edit_GroupShapeAddToSpTree)
    sp.spTreeParent = this
    const cSld = findClosestParent(
      sp,
      (parent) => parent.instanceType === InstanceType.CSld
    ) as CSld | undefined
    if (cSld) {
      cSld.checkUniNvPrForSps([sp])
    }
    EditorUtil.entityRegistry.add(sp, sp.id)
    this.spTree.splice(index, 0, sp)
  }
  setParent(pr: Nullable<SlideElementParent>) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_GroupShapeSetParent,
      this.parent,
      pr
    )
    this.parent = pr
  }

  removeSpById(id) {
    for (let i = this.spTree.length - 1; i > -1; --i) {
      if (this.spTree[i].getId() === id) {
        return this.removeFromSpTreeAtIndex(i)
      }
    }
    return null
  }
  removeFromSpTreeAtIndex(index: number) {
    const arrOfSplicedShape = this.spTree.splice(index, 1)
    onSlideElemntRemoved(arrOfSplicedShape[0])
    informEntityChildrenChange(
      this,
      EditActionFlag.edit_GroupShapeRemoveFromSpTree,
      index,
      arrOfSplicedShape,
      false
    )
    updateCalcStatusForGroup(
      this,
      EditActionFlag.edit_GroupShapeRemoveFromSpTree
    )
    return arrOfSplicedShape[0]
  }

  isShape() {
    return false
  }
  isImage() {
    return false
  }

  getCalcedTransform() {
    if (this.isCalcStatusSet(CalcStatus.GroupShape.Transform)) {
      adjustXfrmOfGroup(this)
      calcTransform(this)
      this.unsetCalcStatusOf(CalcStatus.GroupShape.Transform)
    }
    return this.calcedTransform
  }
  getSlideElementsInGrp() {
    if (this.isCalcStatusSet(CalcStatus.GroupShape.ChildrenObjects)) {
      this.refreshSlideElementsInGroup()
    }
    return this.slideElementsInGroup
  }

  refreshSlideElementsInGroup() {
    this.slideElementsInGroup.length = 0
    for (let i = 0; i < this.spTree.length; ++i) {
      const sp = this.spTree[i]
      if (!isGroup(sp)) this.slideElementsInGroup.push(sp)
      else {
        const sps = sp.getSlideElementsInGrp()
        for (let j = 0; j < sps.length; ++j) {
          this.slideElementsInGroup.push(sps[j])
        }
      }
    }
  }

  getTransform() {
    if (this.isCalcStatusSet(CalcStatus.GroupShape.Transform)) {
      adjustXfrmOfGroup(this)
      calcTransform(this)
      this.unsetCalcStatusOf(CalcStatus.GroupShape.Transform)
    }
    return this.transform
  }

  setVerticalAlign(align) {
    for (let i = 0; i < this.spTree.length; ++i) {
      const sp = this.spTree[i]
      if (sp.instanceType === InstanceType.Shape) {
        sp.setVerticalAlign(align)
      }
    }
  }
  setVert(vert) {
    for (let i = 0; i < this.spTree.length; ++i) {
      const sp = this.spTree[i]
      if (sp.instanceType === InstanceType.Shape) {
        sp.setVert(vert)
      }
    }
  }
  setPaddings(paddings) {
    for (let i = 0; i < this.spTree.length; ++i) {
      const sp = this.spTree[i]
      if (sp.instanceType === InstanceType.Shape) {
        sp.setPaddings(paddings)
      }
    }
  }
  setColumnCount(count) {
    for (let i = 0; i < this.spTree.length; ++i) {
      const sp = this.spTree[i]
      if (sp.instanceType === InstanceType.Shape) {
        sp.setColumnCount(count)
      }
    }
  }
  setSpaceColumn(colSpace) {
    for (let i = 0; i < this.spTree.length; ++i) {
      const sp = this.spTree[i]
      if (sp.instanceType === InstanceType.Shape) {
        sp.setSpaceColumn(colSpace)
      }
    }
  }

  canUnGroup() {
    return true
  }

  setNvSpPr(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_GroupShapeSetNvGrpSpPr,
      this.nvGrpSpPr,
      pr
    )
    this.nvGrpSpPr = pr
  }

  bringToFront() {
    const sps: SlideElement[] = []
    for (let i = this.spTree.length - 1; i > -1; --i) {
      const sp = this.spTree[i]
      if (sp.instanceType === InstanceType.GroupShape) {
        sp.bringToFront()
      } else if (sp.selected) {
        sps.push(this.removeFromSpTreeAtIndex(i))
      }
    }
    for (let i = sps.length - 1; i > -1; --i) {
      this.addToSpTree(null, sps[i])
    }
  }
  bringForward() {
    for (let i = this.spTree.length - 1; i > -1; --i) {
      const sp = this.spTree[i]
      if (sp.instanceType === InstanceType.GroupShape) {
        sp.bringForward()
      } else if (
        i < this.spTree.length - 1 &&
        sp.selected &&
        !this.spTree[i + 1].selected
      ) {
        const item = this.removeFromSpTreeAtIndex(i)
        this.addToSpTree(i + 1, item)
      }
    }
  }
  sendToBack() {
    const sps: SlideElement[] = []
    for (let i = this.spTree.length - 1; i > -1; --i) {
      const sp = this.spTree[i]
      if (sp.instanceType === InstanceType.GroupShape) {
        sp.sendToBack()
      } else if (sp.selected) {
        sps.push(this.removeFromSpTreeAtIndex(i))
      }
    }
    sps.reverse()
    for (let i = 0; i < sps.length; ++i) {
      this.addToSpTree(i, sps[i])
    }
  }
  bringBackward() {
    for (let i = 0; i < this.spTree.length; ++i) {
      const sp = this.spTree[i]
      if (sp.instanceType === InstanceType.GroupShape) {
        sp.bringBackward()
      } else if (i > 0 && sp.selected && !this.spTree[i - 1].selected) {
        this.addToSpTree(i - 1, this.removeFromSpTreeAtIndex(i))
      }
    }
  }

  isValid() {
    if (!this.spPr) {
      return false
    }
    if (this.spTree.length === 0) {
      return false
    }
    return true
  }
}
