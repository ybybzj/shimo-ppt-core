import { isNumber } from '../../../common/utils'
import { idGenerator } from '../../../globals/IdGenerator'

import { DefaultShapeAttributes } from './DefaultShapeAttributes'
import { gRefIdCounter, RefIdPrefixes } from '../../../globals/RefIdCounter'
import { Nullable, valuesOfDict } from '../../../../liber/pervasive'
import { ClrMap } from '../../color/clrMap'
import {
  ThemeElementsData,
  FmtSchemeData,
  FontCollectionData,
  FontSchemeData,
  ClrSchemeData,
  ExtraClrSchemeData,
} from '../../../io/dataType/theme'
import {
  assignVal,
  assignJSON,
  assignJSONArray,
  readFromJSONArray,
  readFromJSON,
  toJSON,
  fromJSON,
} from '../../../io/utils'
import { TextPrFont } from '../../../io/dataType/style'
import {
  toSchemeColorType,
  ColorSchemeIndex,
  fromSchemeColorType,
} from '../../../io/dataType/spAttrs'
import { gColorIndexMax, gColorIndexMin } from './const'
import { CComplexColor } from '../../color/complexColor'
import { FillEffects } from './fill/fill'
import { Ln } from './line'
import { Presentation } from '../../Slide/Presentation'
import { hookManager, Hooks } from '../../hooks'

const gThemeFontsName = {
  ['+mj-cs']: true,
  ['+mj-ea']: true,
  ['+mj-lt']: true,
  ['+mn-cs']: true,
  ['+mn-ea']: true,
  ['+mn-lt']: true,
} as const
export class ClrScheme {
  name: string
  colors: Array<CComplexColor | null>
  constructor() {
    this.name = ''
    this.colors = []

    for (let i = gColorIndexMin; i <= gColorIndexMax; i++) this.colors[i] = null
  }

  sameAs(clrScheme: Nullable<ClrScheme>) {
    if (clrScheme == null) {
      return false
    }
    if (!(clrScheme instanceof ClrScheme)) {
      return false
    }
    if (clrScheme.name !== this.name) {
      return false
    }
    for (let i = gColorIndexMin; i <= gColorIndexMax; ++i) {
      if (this.colors[i] !== clrScheme.colors[i]) {
        return false
      }
    }
    return true
  }

  clone() {
    const clrScheme = new ClrScheme()
    clrScheme.name = this.name
    for (let i = 0; i <= this.colors.length; ++i) {
      clrScheme.colors[i] = this.colors[i]
    }
    return clrScheme
  }

  setName(name) {
    this.name = name
  }

  addColor(index, color) {
    this.colors[index] = color
  }

  toJSON(): ClrSchemeData {
    const data: ClrSchemeData = {}

    this.colors.forEach((clr, index) => {
      if (clr != null) {
        const key = toSchemeColorType(index)
        data[key] = toJSON(clr)
      }
    })

    assignVal(data, 'name', this.name)

    return data
  }

  fromJSON(data: ClrSchemeData) {
    if (data['name'] != null) {
      this.name = data['name']
    }

    ColorSchemeIndex.forEach((clrSchemeIdx) => {
      const clrData = data[clrSchemeIdx]
      if (clrData != null) {
        const index = fromSchemeColorType(clrSchemeIdx)
        this.colors[index] = readFromJSON(clrData, CComplexColor)!
      }
    })
  }
}

export class ExtraClrScheme {
  clrScheme: ClrScheme | null
  clrMap: ClrMap | null
  id: string
  constructor() {
    this.clrScheme = null
    this.clrMap = null

    this.id = idGenerator.newId()
  }

  getId() {
    return this.id
  }

  setClrScheme(pr) {
    this.clrScheme = pr
  }

  setClrMap(pr) {
    this.clrMap = pr
  }

  clone() {
    const ret = new ExtraClrScheme()
    if (this.clrScheme) {
      ret.setClrScheme(this.clrScheme.clone())
    }
    if (this.clrMap) {
      ret.setClrMap(this.clrMap.clone())
    }
    return ret
  }

  toJSON(): ExtraClrSchemeData {
    const data: ExtraClrSchemeData = {}
    assignJSON(data, 'clrScheme', this.clrScheme)
    assignJSON(data, 'clrMap', this.clrMap)
    return data
  }

  fromJSON(data: ExtraClrSchemeData) {
    if (data['clrScheme'] != null) {
      const clrScheme = readFromJSON(data['clrScheme'], ClrScheme)!
      this.clrScheme = clrScheme
    }

    if (data['clrMap'] != null) {
      const clrMap = readFromJSON(data['clrMap'], ClrMap)!
      this.clrMap = clrMap
    }
  }
}

export class FontCollection {
  latin: string | null
  ea: string | null
  cs: string | null
  fontScheme?: FontScheme
  constructor(fontScheme?: FontScheme) {
    this.latin = null
    this.ea = null
    this.cs = null
    if (fontScheme) {
      this.setFontScheme(fontScheme)
    }
  }

  toJSON(): FontCollectionData {
    const data: FontCollectionData = {}
    if (this.latin != null) {
      assignVal(data, 'latin', {
        ['typeface']: this.latin,
      } as TextPrFont)
    }
    if (this.ea != null) {
      assignVal(data, 'ea', {
        ['typeface']: this.ea,
      } as TextPrFont)
    }
    if (this.cs != null) {
      assignVal(data, 'cs', {
        ['typeface']: this.cs,
      } as TextPrFont)
    }
    return data
  }

  fromJSON(data: FontCollectionData) {
    if (data['latin'] && data['latin']['typeface'] != null) {
      this.setLatin(data['latin']['typeface'])
    }

    if (data['ea'] && data['ea']['typeface'] != null) {
      this.setEA(data['ea']['typeface'])
    }

    if (data['cs'] && data['cs']['typeface'] != null) {
      this.setCS(data['cs']['typeface'])
    }
    this.collectFonts()
  }

  setFontScheme(fontScheme: FontScheme) {
    this.fontScheme = fontScheme
  }

  setLatin(pr: string) {
    this.latin = pr
    if (this.fontScheme) {
      this.fontScheme.addToMapByFontCollection(pr, this, FontRegion.Latin)
    }
  }

  setEA(pr: string) {
    this.ea = pr
    if (this.fontScheme) {
      this.fontScheme.addToMapByFontCollection(pr, this, FontRegion.EastAsia)
    }
  }

  setCS(pr: string) {
    this.cs = pr
    if (this.fontScheme) {
      this.fontScheme.addToMapByFontCollection(pr, this, FontRegion.CS)
    }
  }

  collectFonts() {
    const fonts = [this.ea, this.cs, this.latin]
    fonts.forEach((font) => {
      if (font) {
        hookManager.invoke(Hooks.Attrs.CollectFontFromDataChange, {
          font,
        })
      }
    })
  }
}

const FontRegion = { Latin: 0, EastAsia: 1, CS: 2 } as const
export class FontScheme {
  name: string
  majorFont: FontCollection
  minorFont: FontCollection
  fontMap: {
    '+mj-lt': Nullable<string>
    '+mj-ea': Nullable<string>
    '+mj-cs': Nullable<string>
    '+mn-lt': Nullable<string>
    '+mn-ea': Nullable<string>
    '+mn-cs': Nullable<string>
  }
  constructor() {
    this.name = ''

    this.majorFont = new FontCollection(this)
    this.minorFont = new FontCollection(this)
    this.fontMap = {
      '+mj-lt': undefined,
      '+mj-ea': undefined,
      '+mj-cs': undefined,
      '+mn-lt': undefined,
      '+mn-ea': undefined,
      '+mn-cs': undefined,
    }
  }

  clone() {
    const dup = new FontScheme()
    if (this.majorFont.latin != null) {
      dup.majorFont.setLatin(this.majorFont.latin)
    }
    if (this.majorFont.ea != null) {
      dup.majorFont.setEA(this.majorFont.ea)
    }
    if (this.majorFont.cs != null) {
      dup.majorFont.setCS(this.majorFont.cs)
    }
    if (this.minorFont.latin != null) {
      dup.minorFont.setLatin(this.minorFont.latin)
    }
    if (this.minorFont.ea != null) {
      dup.minorFont.setEA(this.minorFont.ea)
    }
    if (this.minorFont.cs != null) {
      dup.minorFont.setCS(this.minorFont.cs)
    }
    return dup
  }

  toJSON(): FontSchemeData {
    const data: FontSchemeData = {}
    assignVal(data, 'name', this.name)
    assignJSON(data, 'majorFont', this.majorFont)
    assignJSON(data, 'minorFont', this.minorFont)
    return data
  }

  fromJSON(data: FontSchemeData) {
    if (data['name'] != null) {
      this.name = data['name']
    }

    if (data['majorFont'] != null) {
      this.majorFont = new FontCollection(this)
      fromJSON(this.majorFont, data['majorFont'])
    }

    if (data['minorFont'] != null) {
      this.minorFont = new FontCollection(this)
      fromJSON(this.minorFont, data['minorFont'])
    }
  }

  addToMapByFontCollection(
    font: string,
    fontCollection: FontCollection,
    region: valuesOfDict<typeof FontRegion>
  ) {
    if (fontCollection === this.majorFont) {
      switch (region) {
        case FontRegion.Latin: {
          this.fontMap['+mj-lt'] = font
          break
        }
        case FontRegion.EastAsia: {
          this.fontMap['+mj-ea'] = font
          break
        }
        case FontRegion.CS: {
          this.fontMap['+mj-cs'] = font
          break
        }
      }
    } else if (fontCollection === this.minorFont) {
      switch (region) {
        case FontRegion.Latin: {
          this.fontMap['+mn-lt'] = font
          break
        }
        case FontRegion.EastAsia: {
          this.fontMap['+mn-ea'] = font
          break
        }
        case FontRegion.CS: {
          this.fontMap['+mn-cs'] = font
          break
        }
      }
    }
  }

  checkFont(font: string): string {
    if (gThemeFontsName[font]) {
      return this.fontMap[font] ?? this.fontMap['+mn-lt'] ?? 'Arial'
    }
    return font
  }

  setName(pr: string) {
    this.name = pr
  }

  setMajorFont(pr: FontCollection) {
    this.majorFont = pr
  }

  setMinorFont(pr: FontCollection) {
    this.minorFont = pr
  }
}

export class FmtScheme {
  name: string
  fillStyleLst: FillEffects[]
  lnStyleLst: Ln[]
  effectStyleLst: any
  bgFillStyleLst: FillEffects[]
  constructor() {
    this.name = ''
    this.fillStyleLst = []
    this.lnStyleLst = []
    this.effectStyleLst = null
    this.bgFillStyleLst = []
  }

  toJSON(): FmtSchemeData {
    const data: FmtSchemeData = {}
    assignVal(data, 'name', this.name)
    assignJSONArray(data, 'fillStyleLst', this.fillStyleLst)
    assignJSONArray(data, 'lnStyleLst', this.lnStyleLst)
    assignJSONArray(data, 'bgFillStyleLst', this.bgFillStyleLst)
    return data
  }

  fromJSON(data: FmtSchemeData) {
    if (data['name'] != null) {
      this.name = data['name']
    }

    if (data['fillStyleLst'] != null) {
      const fillStyleLst = readFromJSONArray(data['fillStyleLst'], FillEffects)
      if (fillStyleLst != null) {
        this.fillStyleLst = fillStyleLst as FillEffects[]
      }
    }

    if (data['bgFillStyleLst'] != null) {
      const bgFillStyleLst = readFromJSONArray(
        data['bgFillStyleLst'],
        FillEffects
      )
      if (bgFillStyleLst != null) {
        this.bgFillStyleLst = bgFillStyleLst as FillEffects[]
      }
    }

    if (data['lnStyleLst'] != null) {
      const lnStyleLst = readFromJSONArray(data['lnStyleLst'], Ln)
      if (lnStyleLst != null) {
        this.lnStyleLst = lnStyleLst as Ln[]
      }
    }
  }

  getFillStyle(number, ccolor?: CComplexColor) {
    if (number >= 1 && number <= 999) {
      let ret = this.fillStyleLst[number - 1]
      if (!ret) return null
      ret = ret.clone()
      ret.updateColorForPh(ccolor)
      return ret
    } else if (number >= 1001) {
      let ret = this.bgFillStyleLst[number - 1001]
      if (!ret) return null
      ret = ret.clone()
      ret.updateColorForPh(ccolor)
      return ret
    }
    return null
  }

  clone() {
    const dup = new FmtScheme()
    dup.name = this.name
    let i
    for (i = 0; i < this.fillStyleLst.length; ++i) {
      dup.fillStyleLst[i] = this.fillStyleLst[i].clone()
    }
    for (i = 0; i < this.lnStyleLst.length; ++i) {
      dup.lnStyleLst[i] = this.lnStyleLst[i].clone()
    }

    for (i = 0; i < this.bgFillStyleLst.length; ++i) {
      dup.bgFillStyleLst[i] = this.bgFillStyleLst[i].clone()
    }
    return dup
  }

  setName(pr) {
    this.name = pr
  }
}

export class ThemeElements {
  clrScheme: ClrScheme
  fontScheme: FontScheme
  fmtScheme: FmtScheme
  constructor() {
    this.clrScheme = new ClrScheme()
    this.fontScheme = new FontScheme()
    this.fmtScheme = new FmtScheme()
  }
  toJSON(): ThemeElementsData {
    return {
      clrScheme: toJSON(this.clrScheme),
      fontScheme: toJSON(this.fontScheme),
      fmtScheme: toJSON(this.fmtScheme),
    }
  }
  fromJSON(data: ThemeElementsData) {
    const clrScheme = readFromJSON(data['clrScheme'], ClrScheme)
    if (clrScheme != null) {
      this.clrScheme = clrScheme
    }

    const fontScheme = readFromJSON(data['fontScheme'], FontScheme)
    if (fontScheme != null) {
      this.fontScheme = fontScheme
    }

    const fmtScheme = readFromJSON(data['fmtScheme'], FmtScheme)
    if (fmtScheme != null) {
      this.fmtScheme = fmtScheme
    }
  }
}
export class Theme {
  name: string
  themeElements: ThemeElements
  spDef: DefaultShapeAttributes | null
  lnDef: DefaultShapeAttributes | null
  txDef: DefaultShapeAttributes | null
  extraClrSchemeLst: ExtraClrScheme[]
  presentation: null | Presentation
  clrMap: any
  id: string
  refId?: string
  constructor() {
    this.name = ''
    this.themeElements = new ThemeElements()
    this.spDef = null
    this.lnDef = null
    this.txDef = null

    this.extraClrSchemeLst = []

    // pointers
    this.presentation = null
    this.clrMap = null

    this.id = idGenerator.newId()
  }

  getId() {
    return this.id
  }

  getRefId(): string {
    const refId = this.refId ?? gRefIdCounter.newIDBy(RefIdPrefixes.Theme)
    this.refId = refId
    return refId
  }
  setRefId(rId: string) {
    this.refId = rId
  }
  clone(name?: string) {
    const theme = new Theme()
    theme.setName(name ?? this.name)
    theme.changeColorScheme(this.themeElements.clrScheme.clone())
    theme.setFontScheme(this.themeElements.fontScheme.clone())
    theme.setFormatScheme(this.themeElements.fmtScheme.clone())
    if (this.spDef) {
      theme.setSpDef(this.spDef.clone())
    }
    if (this.lnDef) {
      theme.setLnDef(this.lnDef.clone())
    }
    if (this.txDef) {
      theme.setTxDef(this.txDef.clone())
    }
    for (let i = 0; i < this.extraClrSchemeLst.length; ++i) {
      theme.insertExtraClrSceme(this.extraClrSchemeLst[i].clone())
    }
    return theme
  }

  collectAllFontNames(allFonts) {
    const font_scheme = this.themeElements.fontScheme
    const major_font = font_scheme.majorFont
    typeof major_font.latin === 'string' &&
      major_font.latin.length > 0 &&
      (allFonts[major_font.latin] = 1)
    typeof major_font.ea === 'string' &&
      major_font.ea.length > 0 &&
      (allFonts[major_font.ea] = 1)
    typeof major_font.cs === 'string' &&
      major_font.cs!.length > 0 &&
      (allFonts[major_font.cs] = 1)
    const minor_font = font_scheme.minorFont
    typeof minor_font.latin === 'string' &&
      minor_font.latin.length > 0 &&
      (allFonts[minor_font.latin] = 1)
    typeof minor_font.ea === 'string' &&
      minor_font.ea.length > 0 &&
      (allFonts[minor_font.ea] = 1)
    typeof minor_font.cs === 'string' &&
      minor_font.cs!.length > 0 &&
      (allFonts[minor_font.cs] = 1)
  }

  getFillStyle(idx, ccolor: Nullable<CComplexColor>) {
    if (idx === 0 || idx === 1000) {
      const ret = FillEffects.NoFill()
      return ret
    }
    let ret: undefined | FillEffects
    if (idx >= 1 && idx <= 999) {
      if (this.themeElements.fmtScheme.fillStyleLst[idx - 1]) {
        ret = this.themeElements.fmtScheme.fillStyleLst[idx - 1].clone()
        if (ret) {
          ret.updateColorForPh(ccolor)
          return ret
        }
      }
    } else if (idx >= 1001) {
      if (this.themeElements.fmtScheme.bgFillStyleLst[idx - 1001]) {
        ret = this.themeElements.fmtScheme.bgFillStyleLst[idx - 1001].clone()
        if (ret) {
          ret.updateColorForPh(ccolor)
          return ret
        }
      }
    }
    return FillEffects.SolidRGBA(0, 0, 0, 255)
  }

  getLnStyle(idx: Nullable<number>, ccolor: Nullable<CComplexColor>) {
    if (idx != null && this.themeElements.fmtScheme.lnStyleLst[idx - 1]) {
      const ret = this.themeElements.fmtScheme.lnStyleLst[idx - 1].clone()
      if (ret.fillEffects) {
        ret.fillEffects.updateColorForPh(ccolor)
      }
      return ret
    }
    return new Ln()
  }

  changeColorScheme(clrScheme: ClrScheme) {
    this.themeElements.clrScheme = clrScheme
  }

  setFontScheme(fontScheme: FontScheme) {
    this.themeElements.fontScheme = fontScheme
  }

  setFormatScheme(fmtScheme: FmtScheme) {
    this.themeElements.fmtScheme = fmtScheme
  }

  setName(name: string) {
    this.name = name
  }

  setSpDef(pr) {
    this.spDef = pr
  }

  setLnDef(pr) {
    this.lnDef = pr
  }

  setTxDef(pr) {
    this.txDef = pr
  }

  insertExtraClrSceme(pr, idx?) {
    let pos
    if (isNumber(idx)) pos = idx
    else pos = this.extraClrSchemeLst.length

    this.extraClrSchemeLst.splice(pos, 0, pr)
  }
}
