import { HFData } from '../../../io/dataType/spAttrs'

export class HF {
  dt: boolean
  ftr: boolean
  hdr: boolean
  sldNum: boolean
  constructor() {
    this.dt = false
    this.ftr = false
    this.hdr = false
    this.sldNum = false
  }

  clone() {
    const ret = new HF()
    if (ret.dt !== this.dt) {
      ret.dt = this.dt
    }
    if (ret.ftr !== this.ftr) {
      ret.ftr = this.ftr
    }
    if (ret.hdr !== this.hdr) {
      ret.hdr = this.hdr
    }
    if (ret.sldNum !== this.sldNum) {
      ret.sldNum = this.sldNum
    }
    return ret
  }

  toJSON(): HFData {
    return {
      ['dt']: this.dt,
      ['ftr']: this.ftr,
      ['hdr']: this.hdr,
      ['sldNum']: this.sldNum,
    }
  }
  fromJSON(data: HFData) {
    if (data['dt'] != null) {
      this.dt = data['dt']
    } else {
      this.dt = true
    }
    if (data['ftr'] != null) {
      this.ftr = data['ftr']
    } else {
      this.ftr = true
    }
    if (data['hdr'] != null) {
      this.hdr = data['hdr']
    } else {
      this.hdr = true
    }
    if (data['sldNum'] != null) {
      this.sldNum = data['sldNum']
    } else {
      this.sldNum = true
    }
  }
}
