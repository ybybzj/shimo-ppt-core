import { assignJSON, readFromJSON } from '../../../io/utils'
import { CShapeStyleData, ThemeColor } from '../../../io/dataType/spAttrs'
import { FontRef, StyleRef } from './refs'
import { Nullable } from '../../../../liber/pervasive'
import { CComplexColor } from '../../color/complexColor'
import { FontStyleInd } from '../const'
import { gLineStylePresets } from './creators'
export interface ShapeStyleOptions {
  lnRef?: StyleRef
  fillRef?: StyleRef
  effectRef?: StyleRef
  fontRef?: FontRef
}

export class ShapeStyle {
  lnRef?: StyleRef
  fillRef?: StyleRef
  effectRef?: StyleRef
  fontRef?: FontRef
  static new(opts?: ShapeStyleOptions): ShapeStyle {
    const ret = new ShapeStyle()
    if (opts != null) {
      ret.fromOptions(opts)
    }
    return ret
  }
  static Default(preset?: string): ShapeStyle {
    const islinePreset = typeof preset === 'string' && gLineStylePresets[preset]
    const lnRef = StyleRef.new(
      islinePreset ? 1 : 2,
      CComplexColor.Scheme(ThemeColor.Accent1)
    )
    lnRef.addColorMod({
      name: 'shade',
      val: 50000,
    })

    const fillRef = StyleRef.new(
      islinePreset ? 0 : 1,
      CComplexColor.Scheme(ThemeColor.Accent1)
    )

    const effectRef = StyleRef.new(0, CComplexColor.Scheme(ThemeColor.Accent1))

    const fontRef = FontRef.new(
      FontStyleInd.minor,
      CComplexColor.Scheme(islinePreset ? ThemeColor.Text1 : ThemeColor.Light1)
    )
    return ShapeStyle.new({
      lnRef,
      fillRef,
      effectRef,
      fontRef,
    })
  }
  static TextRectDefault(): ShapeStyle {
    const lnRef = StyleRef.new(0, CComplexColor.Scheme(ThemeColor.Accent1))
    const mod = {
      name: 'shade',
      val: 50000,
    }
    lnRef.addColorMod(mod)

    const fillRef = StyleRef.new(0, CComplexColor.Scheme(ThemeColor.Accent1))

    const effectRef = StyleRef.new(0, CComplexColor.Scheme(ThemeColor.Accent1))

    const fontRef = FontRef.new(
      FontStyleInd.minor,
      CComplexColor.Scheme(ThemeColor.Dark1)
    )

    return ShapeStyle.new({
      lnRef,
      fillRef,
      effectRef,
      fontRef,
    })
  }
  constructor() {
    this.lnRef = undefined
    this.fillRef = undefined
    this.effectRef = undefined
    this.fontRef = undefined
  }
  fromOptions(opts: Nullable<ShapeStyleOptions>) {
    if (opts == null) return
    if (opts.lnRef != null) this.lnRef = opts.lnRef
    if (opts.fillRef != null) this.fillRef = opts.fillRef
    if (opts.effectRef != null) this.effectRef = opts.effectRef
    if (opts.fontRef != null) this.fontRef = opts.fontRef
  }

  merge(style: Nullable<ShapeStyle>) {
    if (style != null) {
      if (style.lnRef != null) {
        this.lnRef = style.lnRef.clone()
      }

      if (style.fillRef != null) {
        this.fillRef = style.fillRef.clone()
      }

      if (style.effectRef != null) {
        this.effectRef = style.effectRef.clone()
      }

      if (style.fontRef != null) {
        this.fontRef = style.fontRef.clone()
      }
    }
  }

  clone() {
    const duplicate = new ShapeStyle()
    if (this.lnRef != null) {
      duplicate.lnRef = this.lnRef.clone()
    }
    if (this.fillRef != null) {
      duplicate.fillRef = this.fillRef.clone()
    }
    if (this.effectRef != null) {
      duplicate.effectRef = this.effectRef.clone()
    }
    if (this.fontRef != null) {
      duplicate.fontRef = this.fontRef.clone()
    }
    return duplicate
  }

  toJSON(): CShapeStyleData {
    const data: CShapeStyleData = {}
    assignJSON(data, 'lnRef', this.lnRef)
    assignJSON(data, 'fillRef', this.fillRef)
    assignJSON(data, 'effectRef', this.effectRef)
    assignJSON(data, 'fontRef', this.fontRef)
    return data
  }

  fromJSON(data: CShapeStyleData) {
    if (data['lnRef'] != null) {
      this.lnRef = readFromJSON(data['lnRef'], StyleRef)
    }
    if (data['fillRef'] != null) {
      this.fillRef = readFromJSON(data['fillRef'], StyleRef)
    }
    if (data['effectRef'] != null) {
      this.effectRef = readFromJSON(data['effectRef'], StyleRef)
    }
    if (data['fontRef'] != null) {
      this.fontRef = readFromJSON(data['fontRef'], FontRef)
    }
  }
}
