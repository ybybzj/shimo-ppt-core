import { createGeometryOfPrstTxWarp } from '../geometry/creators'
import { TextOverflowType, TextVerticalType, TextWarpType } from '../const'

import { Geometry } from '../geometry/Geometry'
import { TextFit } from './TextFit'
import {
  assignVal,
  assignJSON,
  readFromJSON,
  scaleValue,
  fromJSON,
} from '../../../io/utils'
import {
  CBodyPrData,
  TextAnchorTypeOO,
  toTextAnchorType,
  fromTextAnchorType,
  TextVertOverflowTypeOO,
  toTextVertOverflowType,
  fromTextVertOverflowType,
  fromTextHorzOverflowType,
  TextHorzOverflowTypeOO,
  toTextHorzOverflowType,
  TextVerticalTypeOO,
  toTextVerticalType,
  fromTextVerticalType,
  fromTextWrappingType,
  TextWrappingTypeOO,
  toTextWrappingType,
} from '../../../io/dataType/spAttrs'
import { ScaleOfPPTXSizes } from '../../common/const/unit'

import { Nullable } from '../../../../liber/pervasive'

const INS_SCALE = ScaleOfPPTXSizes

export class BodyPr {
  flatTx?: number
  anchor?: TextAnchorTypeOO
  anchorCtr?: boolean
  compatLnSpc?: boolean
  forceAA?: boolean
  fromWordArt?: boolean
  horzOverflow?: TextHorzOverflowTypeOO
  lIns?: number
  tIns?: number
  rIns?: number
  bIns?: number
  numCol?: number
  rot?: number
  rtlCol?: boolean
  spcCol?: number
  spcFirstLastPara?: boolean
  upright?: boolean
  vert?: TextVerticalTypeOO
  vertOverflow?: TextVertOverflowTypeOO
  wrap?: TextWrappingTypeOO
  textFit?: TextFit
  prstTxWarp?: Geometry
  static Default(): BodyPr {
    const ret = new BodyPr()
    ret.setDefault()
    return ret
  }
  constructor() {
    this.flatTx = undefined
    this.anchor = undefined
    this.anchorCtr = undefined
    this.bIns = undefined
    this.compatLnSpc = undefined
    this.forceAA = undefined
    this.fromWordArt = undefined
    this.horzOverflow = undefined
    this.lIns = undefined
    this.numCol = undefined
    this.rIns = undefined
    this.rot = undefined
    this.rtlCol = undefined
    this.spcCol = undefined
    this.spcFirstLastPara = undefined
    this.tIns = undefined
    this.upright = undefined
    this.vert = undefined
    this.vertOverflow = undefined
    this.wrap = undefined
    this.textFit = undefined
    this.prstTxWarp = undefined
  }

  isValid() {
    return (
      this.flatTx != null ||
      this.anchor != null ||
      this.anchorCtr != null ||
      this.bIns != null ||
      this.compatLnSpc != null ||
      this.forceAA != null ||
      this.fromWordArt != null ||
      this.horzOverflow != null ||
      this.lIns != null ||
      this.numCol != null ||
      this.rIns != null ||
      this.rot != null ||
      this.rtlCol != null ||
      this.spcCol != null ||
      this.spcFirstLastPara != null ||
      this.tIns != null ||
      this.upright != null ||
      this.vert != null ||
      this.vertOverflow != null ||
      this.wrap != null ||
      this.textFit != null ||
      this.prstTxWarp != null
    )
  }

  reset() {
    this.flatTx = undefined
    this.anchor = undefined
    this.anchorCtr = undefined
    this.bIns = undefined
    this.compatLnSpc = undefined
    this.forceAA = undefined
    this.fromWordArt = undefined
    this.horzOverflow = undefined
    this.lIns = undefined
    this.numCol = undefined
    this.rIns = undefined
    this.rot = undefined
    this.rtlCol = undefined
    this.spcCol = undefined
    this.spcFirstLastPara = undefined
    this.tIns = undefined
    this.upright = undefined
    this.vert = undefined
    this.vertOverflow = undefined
    this.wrap = undefined
    this.textFit = undefined
    this.prstTxWarp = undefined
  }

  setDefault() {
    this.flatTx = undefined
    this.anchor = 4
    this.anchorCtr = false
    this.bIns = 45720 / ScaleOfPPTXSizes
    // this.compatLnSpc = false
    this.forceAA = false
    this.fromWordArt = false
    this.horzOverflow = TextOverflowType.Overflow
    this.lIns = 91440 / ScaleOfPPTXSizes
    this.numCol = 1
    this.rIns = 91440 / ScaleOfPPTXSizes
    this.rot = undefined
    this.rtlCol = false
    this.spcCol = undefined
    this.spcFirstLastPara = undefined
    this.tIns = 45720 / ScaleOfPPTXSizes
    this.upright = false
    this.vert = TextVerticalType.horz
    this.vertOverflow = TextOverflowType.Overflow
    this.wrap = TextWarpType.SQUARE
    this.prstTxWarp = undefined
    if (this.textFit) this.textFit.reset()
  }

  clone() {
    const duplicate = new BodyPr()
    duplicate.flatTx = this.flatTx
    duplicate.anchor = this.anchor
    duplicate.anchorCtr = this.anchorCtr
    duplicate.bIns = this.bIns
    duplicate.compatLnSpc = this.compatLnSpc
    duplicate.forceAA = this.forceAA
    duplicate.fromWordArt = this.fromWordArt
    duplicate.horzOverflow = this.horzOverflow
    duplicate.lIns = this.lIns
    duplicate.numCol = this.numCol
    duplicate.rIns = this.rIns
    duplicate.rot = this.rot
    duplicate.rtlCol = this.rtlCol
    duplicate.spcCol = this.spcCol
    duplicate.spcFirstLastPara = this.spcFirstLastPara
    duplicate.tIns = this.tIns
    duplicate.upright = this.upright
    duplicate.vert = this.vert
    duplicate.vertOverflow = this.vertOverflow
    duplicate.wrap = this.wrap
    if (this.prstTxWarp) {
      duplicate.prstTxWarp = this.prstTxWarp.clone()
    }
    if (this.textFit) {
      duplicate.textFit = this.textFit.clone()
    }
    return duplicate
  }

  merge(bodyPr: Nullable<BodyPr>) {
    if (!bodyPr) return
    if (bodyPr.flatTx != null) {
      this.flatTx = bodyPr.flatTx
    }
    if (bodyPr.anchor != null) {
      this.anchor = bodyPr.anchor
    }
    if (bodyPr.anchorCtr != null) {
      this.anchorCtr = bodyPr.anchorCtr
    }
    if (bodyPr.bIns != null) {
      this.bIns = bodyPr.bIns
    }
    if (bodyPr.compatLnSpc != null) {
      this.compatLnSpc = bodyPr.compatLnSpc
    }
    if (bodyPr.forceAA != null) {
      this.forceAA = bodyPr.forceAA
    }
    if (bodyPr.fromWordArt != null) {
      this.fromWordArt = bodyPr.fromWordArt
    }

    if (bodyPr.horzOverflow != null) {
      this.horzOverflow = bodyPr.horzOverflow
    }

    if (bodyPr.lIns != null) {
      this.lIns = bodyPr.lIns
    }

    if (bodyPr.numCol != null) {
      this.numCol = bodyPr.numCol
    }
    if (bodyPr.rIns != null) {
      this.rIns = bodyPr.rIns
    }

    if (bodyPr.rot != null) {
      this.rot = bodyPr.rot
    }

    if (bodyPr.rtlCol != null) {
      this.rtlCol = bodyPr.rtlCol
    }

    if (bodyPr.spcCol != null) {
      this.spcCol = bodyPr.spcCol
    }
    if (bodyPr.spcFirstLastPara != null) {
      this.spcFirstLastPara = bodyPr.spcFirstLastPara
    }

    if (bodyPr.tIns != null) {
      this.tIns = bodyPr.tIns
    }
    if (bodyPr.upright != null) {
      this.upright = bodyPr.upright
    }

    if (bodyPr.vert != null) {
      this.vert = bodyPr.vert
    }
    if (bodyPr.vertOverflow != null) {
      this.vertOverflow = bodyPr.vertOverflow
    }
    if (bodyPr.wrap != null) {
      this.wrap = bodyPr.wrap
    }
    if (bodyPr.prstTxWarp) {
      this.prstTxWarp = bodyPr.prstTxWarp.clone()
    }
    if (bodyPr.textFit) {
      this.textFit = bodyPr.textFit.clone()
    }
    if (bodyPr.numCol != null) {
      this.numCol = bodyPr.numCol
    }
    return this
  }

  toJSON(): CBodyPrData {
    const data: CBodyPrData = {}
    assignVal(data, 'flatTx', this.flatTx)
    if (this.anchor != null) {
      assignVal(data, 'anchor', toTextAnchorType(this.anchor))
    }
    assignVal(data, 'anchorCtr', this.anchorCtr)

    assignVal(data, 'bIns', scaleValue(this.bIns, INS_SCALE))
    assignVal(data, 'lIns', scaleValue(this.lIns, INS_SCALE))
    assignVal(data, 'rIns', scaleValue(this.rIns, INS_SCALE))
    assignVal(data, 'spcCol', scaleValue(this.spcCol, INS_SCALE))
    assignVal(data, 'tIns', scaleValue(this.tIns, INS_SCALE))

    assignVal(data, 'compatLnSpc', this.compatLnSpc)
    assignVal(data, 'forceAA', this.forceAA)
    assignVal(data, 'fromWordArt', this.fromWordArt)
    if (Number(this.numCol) > 0) {
      assignVal(data, 'numCol', this.numCol)
    }
    assignVal(data, 'rot', this.rot)
    assignVal(data, 'rtlCol', this.rtlCol)
    assignVal(data, 'spcFirstLastPara', this.spcFirstLastPara)
    assignVal(data, 'upright', this.upright)
    if (this.vert != null) {
      assignVal(data, 'vert', toTextVerticalType(this.vert))
    }
    if (this.horzOverflow != null) {
      assignVal(data, 'horzOverflow', toTextHorzOverflowType(this.horzOverflow))
    }
    if (this.vertOverflow != null) {
      assignVal(data, 'vertOverflow', toTextVertOverflowType(this.vertOverflow))
    }
    if (this.wrap != null) {
      assignVal(data, 'wrap', toTextWrappingType(this.wrap))
    }

    assignJSON(data, 'prstTxWarp', this.prstTxWarp)
    assignJSON(data, 'textFit', this.textFit)

    return data
  }

  fromJSON(data: CBodyPrData) {
    if (data['flatTx'] != null) {
      this.flatTx = data['flatTx']
    }
    if (data['anchor'] != null) {
      this.anchor = fromTextAnchorType(data['anchor'])
    }
    if (data['anchorCtr'] != null) {
      this.anchorCtr = data['anchorCtr']
    }
    if (data['bIns'] != null) {
      this.bIns = data['bIns'] / INS_SCALE
    }

    if (data['lIns'] != null) {
      this.lIns = data['lIns'] / INS_SCALE
    }

    if (data['rIns'] != null) {
      this.rIns = data['rIns'] / INS_SCALE
    }

    if (data['spcCol'] != null) {
      this.spcCol = data['spcCol'] / INS_SCALE
    }

    if (data['tIns'] != null) {
      this.tIns = data['tIns'] / INS_SCALE
    }

    if (data['compatLnSpc'] != null) {
      this.compatLnSpc = data['compatLnSpc']
    }
    if (data['forceAA'] != null) {
      this.forceAA = data['forceAA']
    }
    if (data['fromWordArt'] != null) {
      this.fromWordArt = data['fromWordArt']
    }
    if (data['horzOverflow'] != null) {
      this.horzOverflow = fromTextHorzOverflowType(data['horzOverflow'])
    }

    if (data['numCol'] != null) {
      this.numCol = data['numCol']
    }

    if (data['rot'] != null) {
      this.rot = data['rot']
    }
    if (data['rtlCol'] != null) {
      this.rtlCol = data['rtlCol']
    }

    if (data['spcFirstLastPara'] != null) {
      this.spcFirstLastPara = data['spcFirstLastPara']
    }

    if (data['upright'] != null) {
      this.upright = data['upright']
    }
    if (data['vert'] != null) {
      this.vert = fromTextVerticalType(data['vert'])
    }
    if (data['vertOverflow'] != null) {
      this.vertOverflow = fromTextVertOverflowType(data['vertOverflow'])
    }
    if (data['wrap'] != null) {
      this.wrap = fromTextWrappingType(data['wrap'])
    }

    if (data['prstTxWarp'] != null) {
      const { preset } = data['prstTxWarp']

      this.prstTxWarp = createGeometryOfPrstTxWarp(preset)
      fromJSON(this.prstTxWarp, data['prstTxWarp'])
    }

    if (data['textFit'] != null) {
      this.textFit = readFromJSON(data['textFit'], TextFit)
    }
  }
}
