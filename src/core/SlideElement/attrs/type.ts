import { OleObject } from '../OleObject'
import { BlipFill } from './fill/blipFill'
import { UniMedia } from './shapePrs'

export type AttrWithAssets = BlipFill | OleObject | UniMedia
