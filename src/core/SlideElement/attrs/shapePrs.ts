import { Nullable } from '../../../../liber/pervasive'
import { idGenerator } from '../../../globals/IdGenerator'
import { isNumber, isDict } from '../../../common/utils'

import { EditorUtil } from '../../../globals/editor'
import { EditActionFlag } from '../../EditActionFlag'
import {
  CNvPrData,
  UniMediaData,
  toMediaType,
  fromMediaType,
  NvPrData,
  SpTypes,
  CNvUniSpPrData,
  toNvSpLocks,
  fromNvSpLocks,
  UniNvPrData,
  CXfrmData,
  BwModeKindValue,
  CSpPrData,
  toBwMode,
  fromBwMode,
} from '../../../io/dataType/spAttrs'
import {
  assignVal,
  assignJSON,
  readFromJSON,
  scaleValue,
} from '../../../io/utils'
import { Geometry } from '../geometry/Geometry'
import { HyperlinkProps } from './HyperlinkProps'
import { FillEffects } from './fill/fill'
import { Ln } from './line'
import { Ph } from './ph'
import { InstanceType } from '../../instanceTypes'
import { ScaleOfPPTXSizes } from '../../common/const/unit'

import { SlideElement } from '../type'
import { MediaType } from '../../common/const/attrs'
import { hookManager, Hooks } from '../../hooks'
import { informEntityPropertyChange } from '../../calculation/informChanges/propertyChange'
import { getCNvProps } from '../../utilities/shape/getters'
import { getGeometryForSp } from '../../utilities/shape/geometry'
import { Shape } from '../Shape'
import {
  updateCalcStatusForSpPr,
  updateCalcStatusForXfrm,
} from '../../calculation/updateCalcStatus/spProperties'

export class CNvPr {
  nid: number
  name: string
  isHidden: boolean
  descr?: string
  title?: string
  hlinkClick?: HyperlinkProps
  hlinkHover?: HyperlinkProps

  constructor() {
    this.nid = 1
    this.name = ''
    this.isHidden = false
    this.descr = undefined
    this.title = undefined

    this.hlinkClick = undefined
    this.hlinkHover = undefined
  }

  clone() {
    const duplicate = new CNvPr()
    duplicate.setName(this.name)
    duplicate.setIsHidden(this.isHidden)
    if (this.nid != null) {
      duplicate.setId(this.nid)
    }
    if (this.descr != null) {
      duplicate.setDescr(this.descr)
    }
    if (this.title != null) {
      duplicate.setTitle(this.title)
    }
    if (this.hlinkClick) {
      duplicate.setHlinkClick(this.hlinkClick.clone())
    }
    if (this.hlinkHover) {
      duplicate.setHlinkClick(this.hlinkHover.clone())
    }
    return duplicate
  }

  setId(id: number) {
    this.nid = id
  }

  setName(name: string) {
    this.name = name
  }

  setIsHidden(isHidden: boolean) {
    this.isHidden = isHidden
  }

  setDescr(descr: string) {
    this.descr = descr
  }

  setHlinkClick(pr: HyperlinkProps) {
    this.hlinkClick = pr
  }

  setTitle(title: string) {
    this.title = title
  }

  toJSON(): CNvPrData {
    const data: CNvPrData = {}
    assignVal(data, 'id', this.nid)
    assignVal(data, 'name', this.name)
    assignVal(data, 'isHidden', this.isHidden)
    assignVal(data, 'descr', this.descr)
    assignVal(data, 'title', this.title)
    assignJSON(data, 'hlinkClick', this.hlinkClick)
    assignJSON(data, 'hlinkHover', this.hlinkHover)
    return data
  }

  fromJSON(data: CNvPrData) {
    if (data['id'] != null) {
      this.nid = data['id']
    }
    if (data['name'] != null) {
      this.name = data['name']
    }
    if (data['isHidden'] != null) {
      this.isHidden = data['isHidden']
    }
    if (data['descr'] != null) {
      this.descr = data['descr']
    }
    if (data['title'] != null) {
      this.title = data['title']
    }
    if (data['hlinkClick'] != null) {
      const hlinkClick = readFromJSON(data['hlinkClick'], HyperlinkProps)
      if (hlinkClick != null) this.hlinkClick = hlinkClick
    }
    if (data['hlinkHover'] != null) {
      const hlinkHover = readFromJSON(data['hlinkHover'], HyperlinkProps)
      if (hlinkHover) this.hlinkHover = hlinkHover
    }
  }
}

export interface UniMediaOptions {
  type?: MediaType
  media?: string
  encryptUrl?: string
}

export class UniMedia {
  type?: MediaType
  media?: string
  encryptUrl?: string

  constructor() {
    this.type = undefined
    this.media = undefined
  }

  toJSON(): UniMediaData {
    const data: UniMediaData = {}
    const type = toMediaType(this.type!)
    if (type != null) {
      assignVal(data, 'type', type)
      assignVal(data, 'media', this.media)
      assignVal(data, 'encryptUrl', this.encryptUrl)
      if (this.media) {
        hookManager.invoke(Hooks.Attrs.CheckKnownErrorImageUrl, {
          url: this.media,
        })
      }
    }
    return data
  }

  fromJSON(data: UniMediaData) {
    if (data['type'] != null) {
      this.type = fromMediaType(data['type'])
    }
    if (data['media'] != null) {
      this.media = data['media']
      hookManager.invoke(Hooks.Attrs.CheckKnownErrorImageUrl, {
        url: this.media,
      })
      hookManager.invoke(Hooks.Attrs.CollectAssetsForPasting, {
        assetUrl: this.media,
        assetObject: this,
      })
    }
    if (data['encryptUrl'] != null) {
      this.encryptUrl = data['encryptUrl']
    }
  }

  clone() {
    const _ret = new UniMedia()
    _ret.type = this.type
    _ret.media = this.media
    if (_ret.media) {
      hookManager.invoke(Hooks.Attrs.CollectAssetsForPasting, {
        assetUrl: _ret.media,
        assetObject: _ret,
      })
    }
    _ret.encryptUrl = this.encryptUrl
    return _ret
  }

  updateAsset(url: string) {
    this.media = url
  }
}

export class NvPr {
  isPhoto: boolean
  userDrawn: boolean
  ph: Nullable<Ph>
  unimedia?: UniMedia
  id: string

  constructor() {
    this.isPhoto = false
    this.userDrawn = false
    this.ph = undefined
    this.unimedia = undefined

    this.id = idGenerator.newId()
  }

  getId(): string {
    return this.id
  }

  setIsPhoto(isPhoto: boolean) {
    this.isPhoto = isPhoto
  }

  setUserDrawn(userDrawn: boolean) {
    this.userDrawn = userDrawn
  }

  setPh(ph: Nullable<Ph>) {
    this.ph = ph
  }

  setUniMedia(pr: any) {
    this.unimedia = pr
  }

  clone(): NvPr {
    const duplicate = new NvPr()
    duplicate.setIsPhoto(this.isPhoto)
    duplicate.setUserDrawn(this.userDrawn)
    if (this.ph != null) {
      duplicate.setPh(this.ph.clone())
    }
    if (this.unimedia) {
      duplicate.setUniMedia(this.unimedia.clone())
    }
    return duplicate
  }

  toJSON(): NvPrData {
    const data: NvPrData = {}
    assignVal(data, 'isPhoto', this.isPhoto)
    assignVal(data, 'userDrawn', this.userDrawn)
    assignJSON(data, 'unimedia', this.unimedia)
    assignJSON(data, 'ph', this.ph)

    return data
  }

  fromJSON(data: NvPrData) {
    if (data['isPhoto'] != null) {
      this.isPhoto = data['isPhoto']
    }
    if (data['userDrawn'] != null) {
      this.userDrawn = data['userDrawn']
    }
    if (data['unimedia'] != null) {
      this.unimedia = readFromJSON(data['unimedia'], UniMedia) as UniMedia
    }
    if (data['ph'] != null) {
      this.ph = readFromJSON(data['ph'], Ph) as Ph
    }
  }
}

// Mark 应该就是协议中的 cNvSpPr,  但是后面的属性(stXXX endXXX)可能来源 CT_OfficeArtExtensionList
export class CNvUniSpPr {
  locks?: number
  stCxnIdx?: Nullable<number>
  stCxnId?: Nullable<number | string>
  endCxnIdx?: Nullable<number>
  endCxnId?: Nullable<number | string>

  constructor() {
    this.locks = undefined
    this.stCxnIdx = undefined
    this.stCxnId = undefined
    this.endCxnIdx = undefined
    this.endCxnId = undefined
  }

  toJSON(spType?: SpTypes): CNvUniSpPrData {
    const data: CNvUniSpPrData = {}
    if (spType != null && this.locks != null) {
      assignVal(data, 'locks', toNvSpLocks(this.locks, spType))
    }
    assignVal(data, _keyAdapter('st', 'Idx'), this.stCxnIdx)
    assignVal(data, _keyAdapter('end', 'Idx'), this.endCxnIdx)

    const startSp = EditorUtil.entityRegistry.getEntityById<Shape>(this.stCxnId)
    const endSp = EditorUtil.entityRegistry.getEntityById<Shape>(this.endCxnId)
    const sid = getCNvProps(startSp)?.nid
    if (sid != null) {
      assignVal(data, _keyAdapter('st', 'Id'), sid)
    }

    const eid = getCNvProps(endSp)?.nid
    if (eid != null) {
      assignVal(data, _keyAdapter('end', 'Id'), eid)
    }

    return data
  }

  fromJSON(data: CNvUniSpPrData, spType?: SpTypes) {
    if (data['locks'] != null && spType != null) {
      this.locks = fromNvSpLocks(data['locks'], spType)
    }

    {
      const k = _keyAdapter('st', 'Idx')
      if (data[k] != null) {
        this.stCxnIdx = data[k]
      }
    }
    {
      const k = _keyAdapter('st', 'Id')
      if (data[k] != null) {
        this.stCxnId = data[k]
      }
    }
    {
      const k = _keyAdapter('end', 'Idx')
      if (data[k] != null) {
        this.endCxnIdx = data[k]
      }
    }

    {
      const k = _keyAdapter('end', 'Id')
      if (data[k] != null) {
        this.endCxnId = data[k]
      }
    }
  }

  clone() {
    const ret = new CNvUniSpPr()
    ret.locks = this.locks
    ret.stCxnId = this.stCxnId
    ret.stCxnIdx = this.stCxnIdx
    ret.endCxnId = this.endCxnId
    ret.endCxnIdx = this.endCxnIdx
    return ret
  }
}
function _keyAdapter<B extends string, A extends string>(
  b: B,
  a: A
): `${B}Cnx${A}` {
  return `${b}Cnx${a}`
}

export class UniNvPr {
  readonly instanceType = InstanceType.UniNvPr
  cNvPr: CNvPr
  nvPr: NvPr
  nvUniSpPr: CNvUniSpPr
  id: string
  locks?: number

  parent: SlideElement

  constructor(parent: SlideElement) {
    this.cNvPr = new CNvPr()
    this.nvPr = new NvPr()
    this.nvUniSpPr = new CNvUniSpPr()

    this.parent = parent
    this.id = idGenerator.newId()
  }

  getId() {
    return this.id
  }

  setCNvPr(cNvPr: CNvPr) {
    this.cNvPr = cNvPr
  }

  setUniSpPr(pr: CNvUniSpPr) {
    this.nvUniSpPr = pr
  }

  setNvPr(nvPr: NvPr) {
    this.nvPr = nvPr
  }

  clone(parent?) {
    const duplicate: any = new UniNvPr(parent ?? this.parent)

    this.cNvPr && duplicate.setCNvPr(this.cNvPr.clone())
    this.nvPr && duplicate.setNvPr(this.nvPr.clone())
    this.nvUniSpPr && duplicate.setUniSpPr(this.nvUniSpPr.clone())
    return duplicate as UniNvPr
  }

  toJSON(spType?: SpTypes): UniNvPrData {
    const data: UniNvPrData = {}
    assignJSON(data, 'cNvPr', this.cNvPr)
    assignJSON(data, 'nvPr', this.nvPr)
    assignJSON(data, 'nvUniSpPr', this.nvUniSpPr, spType)
    return data
  }

  fromJSON(data: UniNvPrData, spType?: SpTypes) {
    if (isDict(data['cNvPr'])) {
      this.cNvPr = readFromJSON(data['cNvPr'], CNvPr) as CNvPr
    }
    if (isDict(data['nvPr'])) {
      this.nvPr = readFromJSON(data['nvPr'], NvPr) as NvPr
    }
    if (isDict(data['nvUniSpPr'])) {
      this.nvUniSpPr = readFromJSON(data['nvUniSpPr'], CNvUniSpPr, undefined, [
        spType,
      ]) as CNvUniSpPr
    }
  }
}

export class Xfrm {
  readonly instanceType = InstanceType.Xfrm
  offX: number
  offY: number
  extX: number
  extY: number
  chOffX: number
  chOffY: number
  chExtX: number
  chExtY: number
  flipH: boolean
  flipV: boolean
  rot: number

  parent?: Nullable<SpPr>
  id: string

  constructor(parent: Nullable<SpPr>) {
    this.offX = 0
    this.offY = 0
    this.extX = 0
    this.extY = 0
    this.chOffX = 0
    this.chOffY = 0
    this.chExtX = 0
    this.chExtY = 0
    this.flipH = false
    this.flipV = false
    this.rot = 0
    this.parent = parent
    this.id = idGenerator.newId()
  }

  getId() {
    return this.id
  }

  toJSON(): CXfrmData {
    const data: CXfrmData = {}
    assignVal(data, 'offX', scaleValue(this.offX, ScaleOfPPTXSizes))
    assignVal(data, 'offY', scaleValue(this.offY, ScaleOfPPTXSizes))
    assignVal(data, 'extX', scaleValue(this.extX, ScaleOfPPTXSizes))
    assignVal(data, 'extY', scaleValue(this.extY, ScaleOfPPTXSizes))
    assignVal(data, 'chOffX', scaleValue(this.chOffX, ScaleOfPPTXSizes))
    assignVal(data, 'chOffY', scaleValue(this.chOffY, ScaleOfPPTXSizes))
    assignVal(data, 'chExtX', scaleValue(this.chExtX, ScaleOfPPTXSizes))
    assignVal(data, 'chExtY', scaleValue(this.chExtY, ScaleOfPPTXSizes))
    assignVal(data, 'flipH', this.flipH)
    assignVal(data, 'flipV', this.flipV)
    assignVal(data, 'rot', scaleValue(this.rot, (180 * 60000) / Math.PI))

    return data
  }

  fromJSON(data: CXfrmData) {
    if (data['offX'] != null) {
      const offX = data['offX'] / ScaleOfPPTXSizes

      this.offX = offX
    }
    if (data['offY'] != null) {
      const offY = data['offY'] / ScaleOfPPTXSizes

      this.offY = offY
    }
    if (data['extX'] != null) {
      const extX = data['extX'] / ScaleOfPPTXSizes

      this.extX = extX
    }
    if (data['extY'] != null) {
      const extY = data['extY'] / ScaleOfPPTXSizes

      this.extY = extY
    }

    if (data['chOffX'] != null) {
      const chOffX = data['chOffX'] / ScaleOfPPTXSizes

      this.chOffX = chOffX
    }
    if (data['chOffY'] != null) {
      const chOffY = data['chOffY'] / ScaleOfPPTXSizes

      this.chOffY = chOffY
    }
    if (data['chExtX'] != null) {
      const chExtX = data['chExtX'] / ScaleOfPPTXSizes

      this.chExtX = chExtX
    }
    if (data['chExtY'] != null) {
      const chExtY = data['chExtY'] / ScaleOfPPTXSizes

      this.chExtY = chExtY
    }

    if (data['flipH'] != null) {
      this.flipH = data['flipH']
    }
    if (data['flipV'] != null) {
      this.flipV = data['flipV']
    }
    if (data['rot'] != null) {
      const rot = ((data['rot'] / 60000) * Math.PI) / 180

      this.rot = rot
    }
  }

  isValid() {
    return (
      isNumber(this.offX) &&
      isNumber(this.offY) &&
      isNumber(this.extX) &&
      isNumber(this.extY)
    )
  }

  isNotEmptyForGroup() {
    return (
      isNumber(this.offX) &&
      isNumber(this.offY) &&
      isNumber(this.chOffX) &&
      isNumber(this.chOffY) &&
      isNumber(this.extX) &&
      isNumber(this.extY) &&
      isNumber(this.chExtX) &&
      isNumber(this.chExtY)
    )
  }

  sameAs(xfrm: Nullable<Xfrm>) {
    return (
      xfrm &&
      this.offX === xfrm.offX &&
      this.offY === xfrm.offY &&
      this.extX === xfrm.extX &&
      this.extY === xfrm.extY &&
      this.chOffX === xfrm.chOffX &&
      this.chOffY === xfrm.chOffY &&
      this.chExtX === xfrm.chExtX &&
      this.chExtY === xfrm.chExtY
    )
  }

  merge(xfrm) {
    if (xfrm.offX != null) {
      this.offX = xfrm.offX
    }
    if (xfrm.offY != null) {
      this.offY = xfrm.offY
    }

    if (xfrm.extX != null) {
      this.extX = xfrm.extX
    }
    if (xfrm.extY != null) {
      this.extY = xfrm.extY
    }

    if (xfrm.chOffX != null) {
      this.chOffX = xfrm.chOffX
    }
    if (xfrm.chOffY != null) {
      this.chOffY = xfrm.chOffY
    }

    if (xfrm.chExtX != null) {
      this.chExtX = xfrm.chExtX
    }
    if (xfrm.chExtY != null) {
      this.chExtY = xfrm.chExtY
    }

    if (xfrm.flipH != null) {
      this.flipH = xfrm.flipH
    }
    if (xfrm.flipV != null) {
      this.flipV = xfrm.flipV
    }

    if (xfrm.rot != null) {
      this.rot = xfrm.rot
    }
  }

  clone() {
    const duplicate = new Xfrm(this.parent)
    duplicate.setOffX(this.offX)
    duplicate.setOffY(this.offY)
    duplicate.setExtX(this.extX)
    duplicate.setExtY(this.extY)
    duplicate.setChOffX(this.chOffX)
    duplicate.setChOffY(this.chOffY)
    duplicate.setChExtX(this.chExtX)
    duplicate.setChExtY(this.chExtY)

    duplicate.setFlipH(this.flipH)
    duplicate.setFlipV(this.flipV)
    duplicate.setRot(this.rot)
    return duplicate as Xfrm
  }

  setParent(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Xfrm_SetParent,
      this.parent,
      pr
    )
    this.parent = pr
  }

  private onModify(editType: EditActionFlag) {
    updateCalcStatusForXfrm(this, editType)
  }

  setOffX(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Xfrm_SetOffX,
      this.offX,
      pr
    )
    this.offX = pr
    this.onModify(EditActionFlag.edit_Xfrm_SetOffX)
  }

  setOffY(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Xfrm_SetOffY,
      this.offY,
      pr
    )
    this.offY = pr
    this.onModify(EditActionFlag.edit_Xfrm_SetOffY)
  }

  setExtX(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Xfrm_SetExtX,
      this.extX,
      pr
    )
    this.extX = pr
    this.onModify(EditActionFlag.edit_Xfrm_SetExtX)
  }

  setExtY(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Xfrm_SetExtY,
      this.extY,
      pr
    )
    this.extY = pr
    this.onModify(EditActionFlag.edit_Xfrm_SetExtY)
  }

  setChOffX(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Xfrm_SetChOffX,
      this.chOffX,
      pr
    )
    this.chOffX = pr
    this.onModify(EditActionFlag.edit_Xfrm_SetChOffX)
  }

  setChOffY(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Xfrm_SetChOffY,
      this.chOffY,
      pr
    )
    this.chOffY = pr
    this.onModify(EditActionFlag.edit_Xfrm_SetChOffY)
  }

  setChExtX(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Xfrm_SetChExtX,
      this.chExtX,
      pr
    )
    this.chExtX = pr
    this.onModify(EditActionFlag.edit_Xfrm_SetChExtX)
  }

  setChExtY(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Xfrm_SetChExtY,
      this.chExtY,
      pr
    )
    this.chExtY = pr
    this.onModify(EditActionFlag.edit_Xfrm_SetChExtY)
  }

  setFlipH(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Xfrm_SetFlipH,
      this.flipH,
      pr
    )
    this.flipH = pr
    this.onModify(EditActionFlag.edit_Xfrm_SetFlipH)
  }

  setFlipV(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Xfrm_SetFlipV,
      this.flipV,
      pr
    )
    this.flipV = pr
    this.onModify(EditActionFlag.edit_Xfrm_SetFlipV)
  }

  setRot(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Xfrm_SetRot,
      this.rot,
      pr
    )
    this.rot = pr
    this.onModify(EditActionFlag.edit_Xfrm_SetRot)
  }
}

export class SpPr {
  readonly instanceType = InstanceType.SpPr
  bwMode: BwModeKindValue
  xfrm?: Xfrm
  geometry?: Geometry
  fillEffects?: FillEffects
  ln?: Ln
  parent: Nullable<SlideElement>
  id: string

  constructor() {
    this.bwMode = 0

    this.xfrm = undefined
    this.geometry = undefined
    this.fillEffects = undefined
    this.ln = undefined
    this.parent = undefined

    this.id = idGenerator.newId()
  }

  getId() {
    return this.id
  }

  clone() {
    const duplicate = new SpPr()
    duplicate.setBwMode(this.bwMode)
    if (this.xfrm) {
      duplicate.setXfrm(this.xfrm.clone())
      duplicate.xfrm && duplicate.xfrm.setParent(duplicate)
    }
    if (this.geometry != null) {
      duplicate.setGeometry(this.geometry.clone())
    }
    if (this.fillEffects != null) {
      duplicate.setFill(this.fillEffects.clone())
    }
    if (this.ln != null) {
      duplicate.setLn(this.ln.clone())
    }
    return duplicate
  }

  toJSON(): CSpPrData {
    const data: CSpPrData = {}
    if (this.bwMode != null) {
      assignVal(data, 'bwMode', toBwMode(this.bwMode))
    }
    assignJSON(data, 'xfrm', this.xfrm)
    assignJSON(
      data,
      'geometry',
      this.geometry || (this.parent && getGeometryForSp(this.parent))
    )
    assignJSON(data, 'fill', this.fillEffects)
    assignJSON(data, 'ln', this.ln)
    return data
  }

  fromJSON(data: CSpPrData) {
    if (data['bwMode'] != null) {
      const bwMode = fromBwMode(data['bwMode'])
      this.bwMode = bwMode
    }
    if (data['xfrm'] != null) {
      const pr = readFromJSON(data['xfrm'], Xfrm, [this]) as Xfrm
      this.xfrm = pr
    }
    if (data['geometry'] != null) {
      const pr = readFromJSON(data['geometry'], Geometry, undefined, [
        this.xfrm,
      ]) as Geometry

      this.geometry = pr
      if (this.geometry) {
        this.geometry.setParent(this)
      }
    }
    if (data['fill'] != null) {
      const pr = readFromJSON(data['fill'], FillEffects) as FillEffects

      this.fillEffects = pr
    }
    if (data['ln'] != null) {
      const pr = readFromJSON(data['ln'], Ln) as Ln
      this.ln = pr
    }
  }
  setParent(pr: SlideElement) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SpPr_SetParent,
      this.parent,
      pr
    )
    this.parent = pr
  }

  private onModify(editType: EditActionFlag) {
    updateCalcStatusForSpPr(this, editType)
  }
  setBwMode(pr: BwModeKindValue) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SpPr_SetBwMode,
      this.bwMode,
      pr
    )
    this.bwMode = pr
  }

  setXfrm(pr: Xfrm) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SpPr_SetXfrm,
      this.xfrm,
      pr
    )
    this.xfrm = pr
    if (this.xfrm) {
      this.xfrm.parent = this
    }
    this.onModify(EditActionFlag.edit_SpPr_SetXfrm)
  }

  setGeometry(pr: Geometry | undefined) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SpPr_SetGeometry,
      this.geometry,
      pr
    )
    this.geometry = pr
    if (this.geometry) {
      this.geometry.setParent(this)
    }
    this.onModify(EditActionFlag.edit_SpPr_SetGeometry)
  }

  setFill(pr: undefined | FillEffects) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SpPr_SetFill,
      this.fillEffects,
      pr
    )
    this.fillEffects = pr
    if (this.parent) {
      this.onModify(EditActionFlag.edit_SpPr_SetFill)
    }
  }

  setLn(pr: Ln | undefined) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SpPr_SetLn,
      this.ln,
      pr
    )
    this.ln = pr
    if (this.parent) {
      this.onModify(EditActionFlag.edit_SpPr_SetLn)
    }
  }
}
