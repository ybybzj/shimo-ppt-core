import { Dict, Nullable } from '../../../../liber/pervasive'
import { isNumber } from '../../../common/utils'
import {
  CBulletData,
  BulletColorTypeValues,
  BulletColorData,
  toBulletColorType,
  fromBulletColorType,
  BulletSizeTypeValues,
  CBulletSizeData,
  toBulletSizeType,
  fromBulletSizeType,
  BulletTypefaceTypeValues,
  CBulletTypefaceData,
  toBulletTypefaceType,
  fromBulletTypefaceType,
  BulletTypeValues,
  CBulletTypeData,
  toBulleType,
  toAutoNumScheme,
  fromBulleType,
  fromAutoNumScheme,
} from '../../../io/dataType/spAttrs'
import { assignJSON, readFromJSON, assignVal } from '../../../io/utils'
import {
  NumberingFormatToNumberMap,
  NumberingFormatArrMapping,
} from '../../common/numberingFormat'
import { hookManager, Hooks } from '../../hooks'
import {
  BulletTypeValue,
  BulletColorType,
  BulletSizeType,
  BulletTypefaceType,
} from '../const'
import { CComplexColor } from '../../color/complexColor'
import { fromOptions } from '../../common/helpers'
export interface BulletOptions {
  bulletType?: BulletTypeOptions
  bulletColor?: BulletColorOptions
  bulletSize?: BulletSizeOptions
  bulletTypeface?: BulletTypefaceOptions
}
export class Bullet {
  bulletType?: BulletType
  bulletColor?: BulletColor
  bulletSize?: BulletSize
  bulletTypeface?: BulletTypeface
  static new(opts?: BulletOptions) {
    const ret = new Bullet()
    if (opts != null) ret.fromOptions(opts)
    return ret
  }
  constructor() {
    this.reset()
  }

  reset() {
    this.bulletColor = undefined
    this.bulletSize = undefined
    this.bulletTypeface = undefined
    this.bulletType = undefined
  }

  fromOptions(opts: Nullable<BulletOptions>) {
    if (opts) {
      this.bulletType = fromOptions(
        this.bulletType,
        opts.bulletType,
        BulletType
      )
      this.bulletSize = fromOptions(
        this.bulletSize,
        opts.bulletSize,
        BulletSize
      )
      this.bulletTypeface = fromOptions(
        this.bulletTypeface,
        opts.bulletTypeface,
        BulletTypeface
      )
      this.bulletColor = fromOptions(
        this.bulletColor,
        opts.bulletColor,
        BulletColor
      )
    }
  }

  clone() {
    return Bullet.new(this)
  }

  isBullet() {
    return this.bulletType != null && this.bulletType.type != null
  }

  toJSON(): CBulletData {
    const data: CBulletData = {}
    assignJSON(data, 'bulletColor', this.bulletColor)
    assignJSON(data, 'bulletSize', this.bulletSize)
    assignJSON(data, 'bulletTypeface', this.bulletTypeface)
    assignJSON(data, 'bulletType', this.bulletType)
    return data
  }

  fromJSON(data: CBulletData) {
    if (data['bulletColor']) {
      this.bulletColor = readFromJSON(data['bulletColor'], BulletColor)
    }
    if (data['bulletSize']) {
      this.bulletSize = readFromJSON(data['bulletSize'], BulletSize)
    }
    if (data['bulletType']) {
      this.bulletType = readFromJSON(data['bulletType'], BulletType)
    }

    if (data['bulletTypeface']) {
      this.bulletTypeface = readFromJSON(data['bulletTypeface'], BulletTypeface)
    }

    this.collectFonts()
  }

  collectFonts() {
    if (
      this.bulletTypeface &&
      typeof this.bulletTypeface.typeface === 'string' &&
      this.bulletTypeface.typeface.length > 0
    ) {
      hookManager.invoke(Hooks.Attrs.CollectFontFromDataChange, {
        font: this.bulletTypeface.typeface,
      })
    }
  }

  collectAllFontNames(allFonts: Dict<boolean>) {
    if (
      this.bulletTypeface &&
      typeof this.bulletTypeface.typeface === 'string' &&
      this.bulletTypeface.typeface.length > 0
    ) {
      allFonts[this.bulletTypeface.typeface] = true
    }
  }
}

interface BulletColorOptions {
  type?: BulletColorTypeValues
  complexColor?: CComplexColor | null
}
class BulletColor {
  type: BulletColorTypeValues
  complexColor?: CComplexColor
  static new(opts?: BulletColorOptions) {
    const ret = new BulletColor()
    if (opts != null) ret.fromOptions(opts)
    return ret
  }
  constructor() {
    this.type = BulletColorType.CLRTX
    this.complexColor = undefined
  }

  fromOptions(opts: Nullable<BulletColorOptions>) {
    if (opts == null) return
    if (opts.type != null) this.type = opts.type
    if (opts.complexColor !== undefined) {
      const v = opts.complexColor ?? undefined
      this.complexColor = v
    }
  }

  clone() {
    const duplicate = new BulletColor()
    duplicate.type = this.type
    if (this.complexColor != null) {
      duplicate.complexColor = this.complexColor.clone()
    }
    return duplicate
  }

  toJSON(): BulletColorData {
    const data: BulletColorData = {}
    assignVal(data, 'type', toBulletColorType(this.type))
    assignJSON(data, 'uniColor', this.complexColor)
    return data
  }

  fromJSON(data: BulletColorData) {
    if (data['type'] != null) {
      this.type = fromBulletColorType(data['type'])
    }
    if (data['uniColor'] != null) {
      this.complexColor = readFromJSON(data['uniColor'], CComplexColor)
    }
  }
}

interface BulletSizeOptions {
  type?: BulletSizeTypeValues
  val?: number
}
class BulletSize {
  type: BulletSizeTypeValues
  val?: number
  static new(opts?: BulletSizeOptions) {
    const ret = new BulletSize()
    if (opts != null) ret.fromOptions(opts)
    return ret
  }
  constructor() {
    this.type = BulletSizeType.NONE
    this.val = 0
  }
  fromOptions(opts: Nullable<BulletSizeOptions>) {
    if (opts == null) return
    if (opts.type != null) this.type = opts.type
    if (opts.val != null) this.val = opts.val
  }

  clone() {
    return BulletSize.new(this)
  }

  toJSON(): CBulletSizeData {
    const data: CBulletSizeData = {}
    if (this.type != null) {
      assignVal(data, 'type', toBulletSizeType(this.type))
    }
    assignVal(data, 'val', this.val)
    return data
  }

  fromJSON(data: CBulletSizeData) {
    if (data['type'] != null) {
      this.type = fromBulletSizeType(data['type'])
    }
    if (data['val'] != null) {
      this.val = data['val']
    }
  }
}

interface BulletTypefaceOptions {
  type?: BulletTypefaceTypeValues
  typeface?: string
}
class BulletTypeface {
  type: BulletTypefaceTypeValues
  typeface: string
  static new(opts?: BulletTypefaceOptions) {
    const ret = new BulletTypeface()
    if (opts != null) ret.fromOptions(opts)
    return ret
  }
  constructor() {
    this.type = BulletTypefaceType.NONE
    this.typeface = ''
  }

  fromOptions(opts: Nullable<BulletTypefaceOptions>) {
    if (opts == null) return
    if (opts.type != null) this.type = opts.type
    if (opts.typeface != null) this.typeface = opts.typeface
  }

  clone() {
    return BulletTypeface.new(this)
  }

  toJSON(): CBulletTypefaceData {
    const data: CBulletTypefaceData = {}
    assignVal(data, 'type', toBulletTypefaceType(this.type))
    assignVal(data, 'typeface', this.typeface)
    return data
  }

  fromJSON(data: CBulletTypefaceData) {
    if (data['type'] != null) {
      this.type = fromBulletTypefaceType(data['type'])
    }
    if (data['typeface'] != null) {
      this.typeface = data['typeface']
    }
  }
}

export interface BulletTypeOptions {
  type?: BulletTypeValues
  char?: string
  textAutoNum?: number
  startAt?: number
}

export class BulletType {
  type: BulletTypeValues
  char: string
  textAutoNum: number
  startAt: number
  static new(opts?: BulletTypeOptions) {
    const ret = new BulletType()
    if (opts != null) ret.fromOptions(opts)
    return ret
  }

  constructor() {
    this.type = BulletTypeValue.NONE //BulletTypeValue.NONE;
    this.char = String.fromCodePoint(0x2022) // "•"
    this.textAutoNum = 0
    this.startAt = 1
  }

  reset() {
    this.type = BulletTypeValue.NONE //BulletTypeValue.NONE;
    this.char = String.fromCodePoint(0x2022) // "•"
    this.textAutoNum = 0
    this.startAt = 1
  }

  fromOptions(opts: Nullable<BulletTypeOptions>) {
    if (opts == null) return
    if (opts.type != null) this.type = opts.type
    if (opts.char != null) this.char = opts.char
    if (opts.textAutoNum != null) this.textAutoNum = opts.textAutoNum
    if (opts.startAt != null) this.startAt = opts.startAt
  }

  clone() {
    const d = new BulletType()
    d.type = this.type
    d.char = this.char
    d.textAutoNum = this.textAutoNum
    d.startAt = this.startAt
    return d
  }

  toJSON(): CBulletTypeData {
    const data: CBulletTypeData = {}
    if (this.type != null) {
      assignVal(data, 'type', toBulleType(this.type))
    }
    assignVal(data, 'char', this.char)
    if (this.textAutoNum != null) {
      assignVal(data, 'autoNumType', toAutoNumScheme(this.textAutoNum))
    }
    assignVal(data, 'startAt', this.startAt)
    return data
  }

  fromJSON(data: CBulletTypeData) {
    if (data['type'] != null) {
      this.type = fromBulleType(data['type'])
    }
    if (data['char'] != null) {
      this.char = data['char']
    }
    if (data['autoNumType'] != null) {
      this.textAutoNum =
        fromAutoNumScheme(data['autoNumType']) ??
        NumberingFormatToNumberMap['arabicPeriod']
    }
    if (data['startAt'] != null) {
      this.startAt = data['startAt']
    }
  }
}

export function getBulletByListInfo(listInfo: BulletListType) {
  const bullet = new Bullet()
  if (listInfo.subType != null && listInfo.subType < 0) {
    bullet.bulletType = new BulletType()
    bullet.bulletType.type = BulletTypeValue.NONE
  } else {
    let bulletText: string | undefined
    switch (listInfo.type) {
      case 0 /*bulletChar*/: {
        switch (listInfo.subType) {
          case 0:
          case 1: {
            bulletText = String.fromCodePoint(0x2022)
            bullet.bulletTypeface = new BulletTypeface()
            bullet.bulletTypeface.type = BulletTypefaceType.BUFONT
            break
          }
          case 2: {
            bulletText = String.fromCodePoint(0xa7)
            bullet.bulletTypeface = new BulletTypeface()
            bullet.bulletTypeface.type = BulletTypefaceType.BUFONT
            // bullet.bulletTypeface.typeface = 'Courier New'
            break
          }
          case 3: {
            bulletText = String.fromCodePoint(0x77)
            bullet.bulletTypeface = new BulletTypeface()
            bullet.bulletTypeface.type = BulletTypefaceType.BUFONT
            // bullet.bulletTypeface.typeface = 'Wingdings'
            break
          }
          case 4: {
            bulletText = String.fromCodePoint(0xb2)
            bullet.bulletTypeface = new BulletTypeface()
            bullet.bulletTypeface.type = BulletTypefaceType.BUFONT
            // bullet.bulletTypeface.typeface = 'Wingdings'
            break
          }
          case 5: {
            bulletText = String.fromCodePoint(0xfc)
            bullet.bulletTypeface = new BulletTypeface()
            bullet.bulletTypeface.type = BulletTypefaceType.BUFONT
            // bullet.bulletTypeface.typeface = 'Wingdings'
            break
          }
          case 6: {
            bulletText = String.fromCodePoint(0xd8)
            bullet.bulletTypeface = new BulletTypeface()
            bullet.bulletTypeface.type = BulletTypefaceType.BUFONT
            // bullet.bulletTypeface.typeface = 'Wingdings'
            break
          }
          case 7: {
            bulletText = String.fromCodePoint(0xe0)
            bullet.bulletTypeface = new BulletTypeface()
            bullet.bulletTypeface.type = BulletTypefaceType.BUFONT
            // bullet.bulletTypeface.typeface = 'Wingdings'
            break
          }
          case 8: {
            bulletText = String.fromCodePoint(0x2013)
            bullet.bulletTypeface = new BulletTypeface()
            bullet.bulletTypeface.type = BulletTypefaceType.BUFONT
            // bullet.bulletTypeface.typeface = 'Arial'
            break
          }
        }
        bullet.bulletType = new BulletType()
        bullet.bulletType.type = BulletTypeValue.CHAR
        if (bulletText != null) {
          bullet.bulletType.char = bulletText
        }
        if (bullet.bulletTypeface && !bullet.bulletTypeface.typeface) {
          bullet.bulletTypeface.typeface = 'Wingdings'
        }
        break
      }
      case 1 /*autonum*/: {
        let numberingType: number | undefined
        switch (listInfo.subType) {
          case 0:
          case 1: {
            numberingType = 12 //numbering_numfmt_arabicPeriod;
            break
          }
          case 2: {
            // numberingType = 11 //numbering_numfmt_arabicParenR;
            numberingType = 41 //numbering_numfmt_arabicParenBoth;
            break
          }
          case 3: {
            // numberingType = 34 //numbering_numfmt_romanUcPeriod;
            numberingType = 42 //NumberingFormat.chineseNumberComma
            break
          }
          case 4: {
            // numberingType = 5 //numbering_numfmt_alphaUcPeriod;
            numberingType = 43 //NumberingFormat.chineseNumberComma
            break
          }
          case 5: {
            numberingType = 1
            break
          }
          case 6: {
            // numberingType = 2
            numberingType = 31 //numbering_numfmt_romanLcPeriod;
            break
          }
          case 7: {
            // numberingType = 31 //numbering_numfmt_romanLcPeriod;
            numberingType = 5 //numbering_numfmt_alphaUcPeriod;
            break
          }
        }
        bullet.bulletType = new BulletType()
        bullet.bulletType.type = BulletTypeValue.AUTONUM
        if (numberingType != null) {
          bullet.bulletType.textAutoNum = numberingType
        }
        bullet.bulletTypeface = new BulletTypeface()

        break
      }
      default: {
        break
      }
    }
  }
  return bullet
}

export interface BulletListType {
  type: number
  subType?: number
}

export function getListTypeFromBullet(
  bullet: Nullable<Bullet>
): BulletListType {
  const listType: BulletListType = {
    type: -1,
    subType: -1,
  }
  if (bullet && bullet.bulletType) {
    switch (bullet.bulletType.type) {
      case BulletTypeValue.CHAR: {
        listType.type = 0
        listType.subType = undefined
        switch (bullet.bulletType.char) {
          case String.fromCodePoint(0x2022): {
            listType.subType = 1
            break
          }
          case String.fromCodePoint(0xa7): {
            listType.subType = 2
            break
          }
          case String.fromCodePoint(0x77): {
            listType.subType = 3
            break
          }
          case String.fromCodePoint(0xb2): {
            listType.subType = 4
            break
          }
          case String.fromCodePoint(0xfc): {
            listType.subType = 5
            break
          }
          case String.fromCodePoint(0xd8): {
            listType.subType = 6
            break
          }
          case String.fromCodePoint(0xe0): {
            listType.subType = 7
            break
          }
          case String.fromCodePoint(0x2013): {
            listType.subType = 8
            break
          }
        }
        break
      }
      case BulletTypeValue.BLIP: {
        listType.type = 0
        listType.subType = undefined
        break
      }
      case BulletTypeValue.AUTONUM: {
        listType.type = 1
        listType.subType = undefined
        if (isNumber(bullet.bulletType.textAutoNum)) {
          let textAutoNum =
            NumberingFormatArrMapping[bullet.bulletType.textAutoNum] - 99
          switch (bullet.bulletType.textAutoNum) {
            case 1: {
              textAutoNum = 5
              break
            }
            case 31: {
              textAutoNum = 6
              break
            }
            case 43: {
              textAutoNum = 4
              break
            }
            case 41: {
              textAutoNum = 2
              break
            }
            case 12: {
              textAutoNum = 1
              break
            }
            case 5: {
              textAutoNum = 7
              break
            }
            case 42: {
              textAutoNum = 3
              break
            }
          }
          if (isNumber(textAutoNum) && textAutoNum > 0 && textAutoNum < 9) {
            listType.subType = textAutoNum
          }
        }
        break
      }
    }
  }
  return listType
}
