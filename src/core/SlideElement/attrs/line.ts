import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import {
  LineEndTypeValues,
  EndArrowData,
  toLineEndType,
  fromLineEndType,
  LineJoinTypeOO,
  LineJoinData,
  toLineJoinType,
  fromLineJoinType,
  PresetLineDashStyleValues,
  CLnData,
  toPresetLineDashVal,
  toPenAlignment,
  toLineCap,
  toCompoundLine,
  fromPresetLineDashVal,
  fromPenAlignment,
  fromLineCap,
  fromCompoundLine,
} from '../../../io/dataType/spAttrs'
import { assignVal, assignJSON, readFromJSON } from '../../../io/utils'
import { ClrMap } from '../../color/clrMap'
import { ColorMod } from '../../color/ColorModifiers'
import { ColorRGBA } from '../../color/type'
import {
  fromOptions,
  intersectObjects,
  isObjectsTheSame,
} from '../../common/helpers'
import { NotesMaster } from '../../Slide/NotesMaster'
import { Slide } from '../../Slide/Slide'
import { SlideLayout } from '../../Slide/SlideLayout'
import { SlideMaster } from '../../Slide/SlideMaster'
import { LineEndWidthTypes } from '../const'
import { FillEffects } from './fill/fill'
import { Theme } from './theme'
interface EndArrowOptions {
  type?: LineEndTypeValues
  len?: number
  w?: number
}
class EndArrow {
  type?: LineEndTypeValues
  len?: number
  w?: number
  static new(opts?: EndArrowOptions): EndArrow {
    const ret = new EndArrow()
    if (opts != null) {
      ret.fromOptions(opts)
    }
    return ret
  }

  static fromOptions(
    endArrow: Nullable<EndArrow>,
    opts?: EndArrowOptions
  ): EndArrow {
    if (endArrow == null) {
      return EndArrow.new(opts)
    } else {
      endArrow.fromOptions(opts)
      return endArrow
    }
  }
  fromOptions(opts: Nullable<EndArrowOptions>) {
    if (opts == null) return
    if (opts.type != null) this.type = opts.type
    if (opts.len != null) this.len = opts.len
    if (opts.w != null) this.w = opts.w
  }
  constructor() {
    this.type = undefined
    this.len = undefined
    this.w = undefined
  }

  intersect(endArrow: Nullable<EndArrowOptions>) {
    if (endArrow == null) {
      return undefined
    }
    const ret = new EndArrow()
    if (this.type === endArrow.type) {
      ret.type = this.type
    }
    if (this.len === endArrow.len) {
      ret.len = this.len
    }
    if (this.w === endArrow.w) {
      ret.w = this.w
    }
    return ret
  }

  clone() {
    const duplicate = new EndArrow()
    duplicate.type = this.type
    duplicate.len = this.len
    duplicate.w = this.w
    return duplicate
  }

  sameAs(arrow: Nullable<EndArrowOptions>) {
    return (
      !!arrow &&
      arrow.type === this.type &&
      arrow.len === this.len &&
      arrow.w === this.w
    )
  }

  getWidth(size: number, max?: number) {
    let ret = 3 * size
    if (null != this.w) {
      switch (this.w) {
        case LineEndWidthTypes.Large:
          ret = 5 * size
          break
        case LineEndWidthTypes.Small:
          ret = 2 * size
          break
        default:
          break
      }
    }
    return Math.max(ret, max ? max : 7)
  }

  getLen(size: number, max?: number) {
    let ret = 3 * size
    if (null != this.len) {
      switch (this.len) {
        case LineEndWidthTypes.Large:
          ret = 5 * size
          break
        case LineEndWidthTypes.Small:
          ret = 2 * size
          break
        default:
          break
      }
    }
    return Math.max(ret, max ? max : 7)
  }

  setType(type) {
    this.type = type
  }

  setLen(len) {
    this.len = len
  }

  setW(w) {
    this.w = w
  }

  toJSON(): EndArrowData {
    const data: EndArrowData = {}
    if (this.type != null) {
      assignVal(data, 'type', toLineEndType(this.type))
    }
    assignVal(data, 'len', this.len)
    assignVal(data, 'w', this.w)
    return data
  }

  fromJSON(data: EndArrowData) {
    if (data['type'] != null) {
      this.type = fromLineEndType(data['type'])
    }
    this.len = data['len']
    this.w = data['w']
  }
}

export interface LineJoinOptions {
  type?: LineJoinTypeOO
  limit?: boolean
}
export class LineJoin {
  type?: LineJoinTypeOO
  limit?: boolean
  static new(type: LineJoinTypeOO, limit?: boolean): LineJoin {
    const ret = new LineJoin()
    ret.type = type
    if (limit != null) {
      ret.limit = limit
    }
    return ret
  }
  fromOptions(opts: Nullable<LineJoinOptions>) {
    if (opts == null) return
    if (opts.limit != null) this.limit = opts.limit
    if (opts.type != null) this.type = opts.type
  }
  constructor() {
    this.type = undefined
    this.limit = undefined
  }

  sameAs(join: Nullable<LineJoinOptions>) {
    if (!join) return false
    if (this.type !== join.type) {
      return false
    }
    if (this.limit !== join.limit) return false
    return true
  }

  clone() {
    const duplicate = new LineJoin()
    duplicate.type = this.type
    duplicate.limit = this.limit
    return duplicate
  }

  setType(type) {
    this.type = type
  }

  setLimit(limit) {
    this.limit = limit
  }

  toJSON(): LineJoinData {
    const data: LineJoinData = {}
    if (this.type != null) {
      assignVal(data, 'type', toLineJoinType(this.type))
    }
    assignVal(data, 'limit', this.limit)
    return data
  }

  fromJSON(data: LineJoinData) {
    if (data['type'] != null) {
      this.type = fromLineJoinType(data['type'])
    }
    this.limit = data['limit']
  }
}

export interface LnOptions {
  fillEffects?: FillEffects
  prstDash?: PresetLineDashStyleValues
  Join?: LineJoinOptions
  headEnd?: EndArrowOptions | null
  tailEnd?: EndArrowOptions | null
  algn?: number
  cap?: number
  cmpd?: number
  w?: number
}

export class Ln {
  readonly instanceType = InstanceType.Ln
  fillEffects?: FillEffects
  prstDash?: PresetLineDashStyleValues
  Join?: LineJoin
  headEnd?: EndArrow
  tailEnd?: EndArrow
  algn?: number
  cap?: number
  cmpd?: number
  w?: number
  static new(opts?: LnOptions): Ln {
    const ret = new Ln()
    if (opts != null) {
      ret.fromOptions(opts)
    }
    return ret
  }

  static fromOptions(ln: Nullable<Ln>, opts?: LnOptions): Ln {
    ln = ln ?? new Ln()
    if (opts != null) {
      ln.fromOptions(opts)
    }
    return ln
  }

  static NoFill(): Ln {
    const ret = new Ln()
    ret.fillEffects = FillEffects.NoFill()
    return ret
  }

  constructor() {
    this.fillEffects = undefined
    this.prstDash = undefined

    this.Join = undefined
    this.headEnd = undefined
    this.tailEnd = undefined

    this.algn = undefined
    this.cap = undefined
    this.cmpd = undefined
    this.w = undefined
  }

  fromOptions(opts: Nullable<LnOptions>) {
    if (opts == null) return
    if (opts.fillEffects) this.fillEffects = opts.fillEffects.clone()
    this.Join = fromOptions(this.Join, opts.Join, LineJoin)
    this.headEnd =
      opts.headEnd === null
        ? undefined
        : fromOptions(this.headEnd, opts.headEnd, EndArrow)
    this.tailEnd =
      opts.tailEnd === null
        ? undefined
        : fromOptions(this.tailEnd, opts.tailEnd, EndArrow)
    if (opts.prstDash != null) this.prstDash = opts.prstDash
    if (opts.algn != null) this.algn = opts.algn
    if (opts.cap != null) this.cap = opts.cap
    if (opts.cmpd != null) this.cmpd = opts.cmpd
    if (opts.w != null) this.w = opts.w
  }

  addColorMod(mod: ColorMod) {
    if (this.fillEffects) {
      this.fillEffects.addColorMod(mod)
    }
  }
  intersect(line: Nullable<LnOptions>) {
    if (line == null) {
      return undefined
    }
    const ret = new Ln()
    if (this.fillEffects != null) {
      ret.fillEffects =
        intersectObjects(this.fillEffects, line.fillEffects) ?? undefined
    }
    if (this.prstDash === line.prstDash) {
      ret.prstDash = this.prstDash
    } else {
      ret.prstDash = undefined
    }
    if (this.Join === line.Join) {
      ret.Join = this.Join
    }
    if (this.tailEnd != null) {
      ret.tailEnd = this.tailEnd.intersect(line.tailEnd)
    }
    if (this.headEnd != null) {
      ret.headEnd = this.headEnd.intersect(line.headEnd)
    }

    if (this.algn === line.algn) {
      ret.algn = this.algn
    }
    if (this.cap === line.cap) {
      ret.cap = this.cap
    }
    if (this.cmpd === line.cmpd) {
      ret.cmpd = this.cmpd
    }
    if (this.w === line.w) {
      ret.w = this.w
    }
    return ret
  }

  merge(opts: LnOptions) {
    this.fromOptions(opts)
  }

  calculate(
    theme: Nullable<Theme>,
    slide: Nullable<Slide>,
    layout: Nullable<SlideLayout>,
    master: Nullable<SlideMaster | NotesMaster>,
    rgba?: ColorRGBA,
    colorMap?: ClrMap
  ) {
    if (this.fillEffects) {
      this.fillEffects.calculate(theme, slide, layout, master, rgba, colorMap)
    }
  }

  clone(opts: { cloneFill?: boolean; cloneTail?: boolean } = {}) {
    opts = Object.assign({ cloneFill: true, cloneTail: true }, opts)
    const duplicate = new Ln()

    if (null != this.fillEffects && opts.cloneFill === true) {
      duplicate.fillEffects = this.fillEffects.clone()
    }

    duplicate.prstDash = this.prstDash
    duplicate.Join = this.Join
    if (opts.cloneTail === true) {
      if (this.headEnd != null) {
        duplicate.headEnd = this.headEnd.clone()
      }

      if (this.tailEnd != null) {
        duplicate.tailEnd = this.tailEnd.clone()
      }
    }
    duplicate.algn = this.algn
    duplicate.cap = this.cap
    duplicate.cmpd = this.cmpd
    duplicate.w = this.w
    return duplicate
  }

  sameAs(ln: Nullable<LnOptions>) {
    return (
      !!ln &&
      isObjectsTheSame(this.fillEffects, ln.fillEffects) &&
      isObjectsTheSame(this.Join, ln.Join) &&
      isObjectsTheSame(this.headEnd, ln.headEnd) &&
      isObjectsTheSame(this.tailEnd, ln.tailEnd) &&
      this.algn === ln.algn &&
      this.cap === ln.cap &&
      this.cmpd === ln.cmpd &&
      this.w === ln.w &&
      this.prstDash === ln.prstDash
    )
  }

  toJSON(): CLnData {
    const data: CLnData = {}
    assignJSON(data, 'fill', this.fillEffects)
    if (this.prstDash != null) {
      assignVal(data, 'prstDash', toPresetLineDashVal(this.prstDash))
    }
    assignJSON(data, 'join', this.Join)
    assignJSON(data, 'headEnd', this.headEnd)
    assignJSON(data, 'tailEnd', this.tailEnd)
    if (this.algn != null) {
      assignVal(data, 'algn', toPenAlignment(this.algn))
    }
    if (this.cap != null) {
      assignVal(data, 'cap', toLineCap(this.cap))
    }
    if (this.cmpd != null) {
      assignVal(data, 'cmpd', toCompoundLine(this.cmpd))
    }
    assignVal(data, 'w', this.w)

    return data
  }

  fromJSON(data: CLnData) {
    if (data['prstDash'] != null) {
      this.prstDash = fromPresetLineDashVal(data['prstDash'])
    }
    if (data['algn'] != null) {
      this.algn = fromPenAlignment(data['algn'])
    }

    if (data['cap'] != null) {
      this.cap = fromLineCap(data['cap'])
    }
    if (data['cmpd'] != null) {
      this.cmpd = fromCompoundLine(data['cmpd'])
    }
    this.w = data['w']
    this.fillEffects = readFromJSON(data['fill'], FillEffects)
    this.headEnd = readFromJSON(data['headEnd'], EndArrow)
    this.tailEnd = readFromJSON(data['tailEnd'], EndArrow)
    this.Join = readFromJSON(data['join'], LineJoin)
  }
}
