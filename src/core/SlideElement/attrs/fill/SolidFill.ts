import { Nullable } from '../../../../../liber/pervasive'
import { FillKIND } from '../../../common/const/attrs'
import { InstanceType } from '../../../instanceTypes'
import { CSolidFillData } from '../../../../io/dataType/spAttrs'
import { assignJSON, readFromJSON } from '../../../../io/utils'
import { CComplexColor } from '../../../color/complexColor'
import { isObjectsTheSame } from '../../../common/helpers'

export class SolidFill {
  readonly instanceType = InstanceType.SolidFill
  readonly type = FillKIND.SOLID
  color?: Nullable<CComplexColor>
  constructor(color?: Nullable<CComplexColor>) {
    this.color = color
  }

  toJSON(): CSolidFillData {
    const data: CSolidFillData = {}
    assignJSON(data, 'color', this.color)

    return data
  }

  fromJSON(data: CSolidFillData) {
    if (data && data['color'] != null) {
      this.color = readFromJSON(data['color'], CComplexColor)
    }
  }

  sameAs(fill: SolidFill) {
    return isObjectsTheSame(this.color, fill.color)
  }

  clone() {
    const duplicate = new SolidFill()
    if (this.color) {
      duplicate.color = this.color.clone()
    }
    return duplicate
  }
}
