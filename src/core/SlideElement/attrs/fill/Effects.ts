import { Nullable } from '../../../../../liber/pervasive'
import {
  EffectType,
  EffectTypeValues,
  toEffectType,
} from '../../../../io/dataType/spAttrs'
import { scaleValue } from '../../../../io/utils'

function createEffectByType(type: Nullable<EffectTypeValues>) {
  let ret: Effects | null = null
  switch (type) {
    case EffectType.NONE: {
      break
    }

    case EffectType.ALPHAMODFIX: {
      ret = new AlphaModFix()
      break
    }
  }
  return ret
}

export function readEffect(type: Nullable<EffectTypeValues>, val: number) {
  const ret = createEffectByType(type)
  if (ret) {
    ret.fromJSON(val)
  }
  return ret
}

export interface EffectOptions {
  amt: number
}
export class AlphaModFix {
  readonly Type = EffectType.ALPHAMODFIX
  amt: number // percentage 0 - 100
  constructor() {
    this.amt = 100
  }
  toJSON() {
    return {
      ['name']: toEffectType(this.Type),
      ['val']: scaleValue(this.amt, 1000),
    }
  }
  fromJSON(val: number) {
    this.amt = val / 1000
  }
  clone() {
    const dup = new AlphaModFix()
    dup.amt = this.amt
    return dup
  }
  fromOptions({ amt }: EffectOptions) {
    this.amt = amt
  }
}

export type Effects = AlphaModFix
