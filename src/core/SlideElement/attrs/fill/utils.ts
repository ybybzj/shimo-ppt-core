import { Nullable } from '../../../../../liber/pervasive'
import { FillKIND } from '../../../common/const/attrs'
import { FillTypeData } from '../../../../io/dataType/spAttrs'
import { fromJSON, toJSON } from '../../../../io/utils'
import { ClrMap } from '../../../color/clrMap'
import { ColorMod } from '../../../color/ColorModifiers'
import { CComplexColor } from '../../../color/complexColor'
import { ColorRGBA } from '../../../color/type'
import { intersectObjects } from '../../../common/helpers'
import { NotesMaster } from '../../../Slide/NotesMaster'
import { Slide } from '../../../Slide/Slide'
import { SlideLayout } from '../../../Slide/SlideLayout'
import { SlideMaster } from '../../../Slide/SlideMaster'
import { FillEffectType } from '../../../utilities/shape/attrs'
import { Theme } from '../theme'
import { BlipFill } from './blipFill'
import { GradFill, GradLin, GradPath } from './GradFill'
import { GrpFill, NoFill } from './otherFills'
import { PatternFill } from './PatternFill'
import { SolidFill } from './SolidFill'

export function isEqualFills(
  f1: Nullable<FillEffectType>,
  f2: Nullable<FillEffectType>
): boolean {
  if (f1 == null && f2 == null) return true
  if (f1 == null || f2 == null) return false
  if (f1.type !== f2.type) return false
  switch (f1.type) {
    case FillKIND.BLIP:
      return f1.sameAs(f2 as BlipFill)
    case FillKIND.SOLID:
      return f1.sameAs(f2 as SolidFill)
    case FillKIND.PATT:
      return f1.sameAs(f2 as PatternFill)
    case FillKIND.GRAD:
      return f1.sameAs(f2 as GradFill)
    case FillKIND.NOFILL:
    case FillKIND.GRP:
      return true
  }
}

export function checkFills(
  fill: FillEffectType,
  theme: Nullable<Theme>,
  clrMap: Nullable<ClrMap>
) {
  switch (fill.type) {
    case FillKIND.BLIP:
    case FillKIND.NOFILL:
    case FillKIND.GRP:
      return
    case FillKIND.SOLID: {
      if (fill.color) {
        fill.color.check(theme, clrMap)
      }
      break
    }
    case FillKIND.PATT: {
      fill.check(theme, clrMap)
      break
    }
    case FillKIND.GRAD: {
      fill.check(theme, clrMap)
      break
    }
  }
}

export function addColorModToFill(fill: FillEffectType, mod: ColorMod) {
  switch (fill.type) {
    case FillKIND.BLIP:
    case FillKIND.NOFILL:
    case FillKIND.GRP: {
      break
    }
    case FillKIND.SOLID: {
      if (fill.color) {
        fill.color.addColorMod(mod)
      }
      break
    }
    case FillKIND.GRAD: {
      for (let i = 0; i < fill.gsList.length; ++i) {
        const ccolor = fill.gsList[i].color
        if (ccolor) {
          ccolor.addColorMod(mod)
        }
      }
      break
    }
    case FillKIND.PATT: {
      if (fill.bgClr) {
        fill.bgClr.addColorMod(mod)
      }
      if (fill.fgClr) {
        fill.fgClr.addColorMod(mod)
      }
      break
    }
  }
}

export function updateAlphaToFill(
  fill: FillEffectType,
  alpha: Nullable<number>
) {
  switch (fill.type) {
    case FillKIND.BLIP:
    case FillKIND.NOFILL:
    case FillKIND.GRP: {
      break
    }
    case FillKIND.SOLID: {
      if (fill.color) {
        fill.color.updateAlphaMod(alpha)
      }
      break
    }
    case FillKIND.GRAD: {
      for (let i = 0; i < fill.gsList.length; ++i) {
        const ccolor = fill.gsList[i].color
        if (ccolor) {
          ccolor.updateAlphaMod(alpha)
        }
      }
      break
    }
    case FillKIND.PATT: {
      if (fill.bgClr) {
        fill.bgClr.updateAlphaMod(alpha)
      }
      if (fill.fgClr) {
        fill.fgClr.updateAlphaMod(alpha)
      }
      break
    }
  }
}

export function updateColorForPh(
  fill: FillEffectType,
  ccolor: Nullable<CComplexColor>
) {
  switch (fill.type) {
    case FillKIND.BLIP:
    case FillKIND.NOFILL:
    case FillKIND.GRP: {
      break
    }
    case FillKIND.SOLID: {
      if (fill.color) {
        fill.color.updateColorForPh(ccolor)
      }
      break
    }
    case FillKIND.GRAD: {
      for (let i = 0; i < fill.gsList.length; ++i) {
        const gs_clr = fill.gsList[i].color
        if (gs_clr) {
          gs_clr.updateColorForPh(ccolor)
        }
      }
      break
    }
    case FillKIND.PATT: {
      if (fill.bgClr) {
        fill.bgClr.updateColorForPh(ccolor)
      }
      if (fill.fgClr) {
        fill.fgClr.updateColorForPh(ccolor)
      }
      break
    }
  }
}

export function fillToJSON(
  fill: FillEffectType,
  trans: Nullable<number>
): FillTypeData {
  let data: FillTypeData
  switch (fill.type) {
    case FillKIND.GRP:
    case FillKIND.NOFILL: {
      data = { ['type']: fill.type }
      break
    }
    case FillKIND.BLIP: {
      data = {
        ['type']: fill.type,
        ['data']: toJSON(fill),
      }
      break
    }
    case FillKIND.SOLID: {
      if (fill.color) {
        fill.color.updateAlphaMod(trans)
      }
      data = {
        ['type']: fill.type,
        ['data']: toJSON(fill),
      }
      break
    }
    case FillKIND.GRAD: {
      fill.gsList.forEach((gs) => {
        if (gs.color) {
          gs.color.updateAlphaMod(trans)
        }
      })
      data = {
        ['type']: fill.type,
        ['data']: toJSON(fill),
      }
      break
    }
    case FillKIND.PATT: {
      if (fill.fgClr) {
        fill.fgClr.updateAlphaMod(trans)
      }

      if (fill.bgClr) {
        fill.bgClr.updateAlphaMod(trans)
      }
      data = {
        ['type']: fill.type,
        ['data']: toJSON(fill),
      }
      break
    }
  }
  return data
}

export function fillFromJSON(data: FillTypeData): Nullable<FillEffectType> {
  const type = data['type']
  let fill: FillEffectType | undefined
  switch (type) {
    case FillKIND.BLIP: {
      fill = new BlipFill()
      fromJSON(fill, data['data'])
      break
    }
    case FillKIND.NOFILL: {
      fill = new NoFill()
      break
    }
    case FillKIND.SOLID: {
      fill = new SolidFill()
      fromJSON(fill, data['data'])
      break
    }
    case FillKIND.GRAD: {
      fill = new GradFill()
      fromJSON(fill, data['data'])
      break
    }
    case FillKIND.PATT: {
      fill = new PatternFill()
      fromJSON(fill, data['data'])
      break
    }
    case FillKIND.GRP: {
      fill = new GrpFill()
      break
    }
  }

  return fill
}

export function calulateFillCColor(
  fill: FillEffectType,
  theme: Nullable<Theme>,
  slide: Nullable<Slide>,
  layout: Nullable<SlideLayout>,
  master: Nullable<SlideMaster | NotesMaster>,
  rgba?: ColorRGBA,
  colorMap?: ClrMap
) {
  switch (fill.type) {
    case FillKIND.BLIP:
    case FillKIND.NOFILL:
    case FillKIND.GRP: {
      break
    }
    case FillKIND.SOLID: {
      if (fill.color) {
        fill.color.updateBy(theme, slide, layout, master, rgba, colorMap)
      }
      break
    }
    case FillKIND.GRAD: {
      for (const gs of fill.gsList) {
        if (gs.color) {
          gs.color.updateBy(theme, slide, layout, master, rgba, colorMap)
        }
      }
      break
    }
    case FillKIND.PATT: {
      if (fill.fgClr) {
        fill.fgClr.updateBy(theme, slide, layout, master, rgba, colorMap)
      }
      if (fill.bgClr) {
        fill.bgClr.updateBy(theme, slide, layout, master, rgba, colorMap)
      }
      break
    }
  }
}

export function intersectFills(
  f1: Nullable<FillEffectType>,
  f2: Nullable<FillEffectType>
): Nullable<FillEffectType> {
  if (f1 == null || f2 == null) return undefined
  if (f1.type !== f2.type) return undefined
  switch (f1.type) {
    case FillKIND.BLIP: {
      return undefined
    }
    case FillKIND.NOFILL: {
      return new NoFill()
    }
    case FillKIND.GRP: {
      return new GrpFill()
    }
    case FillKIND.SOLID: {
      const ret = new SolidFill()
      if (f1.color) {
        ret.color = f1.color.intersect((f2 as SolidFill).color)
      }
      return ret
    }
    case FillKIND.GRAD: {
      const ret = new GradFill()
      const _f2 = f2 as GradFill

      if (f1.ln == null || _f2.ln == null) ret.ln = undefined
      else {
        ret.ln = new GradLin()
        ret.ln.angle = f1.ln.angle === _f2.ln.angle ? _f2.ln.angle : 5400000
        ret.ln.scale = f1.ln.scale === _f2.ln.scale ? _f2.ln.scale : true
      }

      if (f1.path == null || _f2.path == null) {
        ret.path = undefined
      } else {
        const gpath = f1.path ?? _f2.path
        ret.path = new GradPath(gpath.path)
      }

      const gsList = ret.gsList

      if (f1.gsList.length === _f2.gsList.length) {
        const l = f1.gsList.length
        if (l > 0) {
          for (let i = 0; i < l; ++i) {
            const gs = intersectObjects(f1.gsList[i], _f2.gsList[i])
            if (!gs) {
              break
            }
            gsList.push(gs)
          }
        }

        if (gsList.length !== l) {
          ret.gsList.length = 0
        }
      }
      return ret
    }
    case FillKIND.PATT: {
      const _f2 = f2 as PatternFill
      const ret = new PatternFill()
      if (_f2.patternType === f1.patternType) {
        ret.patternType = f1.patternType
      }
      ret.fgClr = f1.fgClr!.intersect(_f2.fgClr)
      ret.bgClr = f1.bgClr!.intersect(_f2.bgClr)
      return ret
    }
  }
}
