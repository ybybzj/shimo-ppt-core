import { findIndex } from '../../../../../liber/l/findIndex'
import { Nullable } from '../../../../../liber/pervasive'
import { FillKIND } from '../../../common/const/attrs'
import { checkImageSrc, isBase64Url, isDict } from '../../../../common/utils'
import { InstanceType } from '../../../instanceTypes'
import {
  CBlipFillTileData,
  toTileFlipMode,
  toRectAlignment,
  fromTileFlipMode,
  fromRectAlignment,
  CBlipFillData,
  fromEffectType,
  EffectType,
} from '../../../../io/dataType/spAttrs'
import {
  assignVal,
  assignJSON,
  toJSON,
  readFromJSON,
  isEmptyPropValue,
  scaleValue,
} from '../../../../io/utils'
import { ScaleOfPPTXSizes } from '../../../common/const/unit'
import { fromOptions, isObjectsTheSame } from '../../../common/helpers'
import { hookManager, Hooks } from '../../../hooks'
import { AlphaModFix, EffectOptions, Effects, readEffect } from './Effects'
import { RelativeRect, RelativeRectOptions } from './RelativeRect'

// not implement
export interface BlipTileInfoOptions {
  tx?: number
  ty?: number
  sx?: number
  sy?: number
  flip?: number
  algn?: number
}
export class BlipTileInfo {
  tx: number
  ty: number
  sx: number
  sy: number
  flip?: number
  algn?: number
  constructor() {
    this.tx = 0
    this.ty = 0
    this.sx = 100
    this.sy = 100
    this.flip = undefined
    this.algn = 7 // topleft
  }

  reset() {
    this.tx = 0
    this.ty = 0
    this.sx = 100
    this.sy = 100
    this.flip = undefined
    this.algn = 7 // topleft
  }

  isInitail() {
    return this.tx === 0 && this.ty === 0 && this.sx === 100 && this.sy === 100
  }

  fromOptions(opts: BlipTileInfoOptions) {
    if (!opts) return
    if (opts.sx != null) {
      this.sx = opts.sx
    }

    if (opts.tx != null) {
      this.tx = opts.tx
    }
    if (opts.sy != null) {
      this.sy = opts.sy
    }
    if (opts.ty != null) {
      this.ty = opts.ty
    }
    if (opts.flip != null) {
      this.flip = opts.flip
    }
    if (opts.algn != null) {
      this.algn = opts.algn
    }
  }

  toJSON(): CBlipFillTileData {
    const data: CBlipFillTileData = {}
    assignVal(data, 'tx', scaleValue(this.tx, ScaleOfPPTXSizes))
    assignVal(data, 'ty', scaleValue(this.ty, ScaleOfPPTXSizes))
    assignVal(data, 'sx', scaleValue(this.sx, 1000))
    assignVal(data, 'sy', scaleValue(this.sy, 1000))
    if (this.flip != null) {
      assignVal(data, 'flip', toTileFlipMode(this.flip))
    }
    if (this.algn != null) {
      assignVal(data, 'algn', toRectAlignment(this.algn))
    }
    return data
  }

  fromJSON(data: CBlipFillTileData) {
    this.tx = (data['tx'] ?? 0) / ScaleOfPPTXSizes
    this.ty = (data['ty'] ?? 0) / ScaleOfPPTXSizes
    this.sx = (data['sx'] ?? 100000) / 1000
    this.sy = (data['sy'] ?? 100000) / 1000
    if (data['flip'] != null) {
      this.flip = fromTileFlipMode(data['flip'])
    }

    if (data['algn'] != null) {
      this.algn = fromRectAlignment(data['algn'])
    }
  }

  clone() {
    const ret = new BlipTileInfo()
    ret.tx = this.tx
    ret.ty = this.ty
    ret.sx = this.sx
    ret.sy = this.sy
    ret.flip = this.flip
    ret.algn = this.algn
    return ret
  }

  sameAs(o) {
    if (!o) {
      return false
    }
    return (
      o.tx === this.tx &&
      o.ty === this.ty &&
      o.sx === this.sx &&
      o.sy === this.sy &&
      o.flip === this.flip &&
      o.algn === this.algn
    )
  }
}

export interface BlipFillOptions {
  imageSrcId?: string
  srcRect?: RelativeRectOptions
  stretch?: boolean
  fillRect?: RelativeRectOptions
  tile?: BlipTileInfoOptions
  rotWithShape?: boolean
  effects?: EffectOptions[]
}

export class BlipFill {
  readonly instanceType = InstanceType.BlipFill
  readonly type = FillKIND.BLIP
  imageSrcId: string
  encryptUrl?: string // 非 OOXML 标准, 石墨内部透传
  srcRect?: RelativeRect
  stretch?: boolean
  fillRect?: RelativeRect
  tile?: BlipTileInfo
  rotWithShape?: boolean
  effects: Effects[]
  constructor(srcId?: string, encryptUrl?: string) {
    this.imageSrcId = srcId ?? ''
    this.encryptUrl = encryptUrl
    this.srcRect = undefined
    this.fillRect = undefined
    this.stretch = undefined
    this.tile = undefined

    this.rotWithShape = undefined

    this.effects = []
  }

  toJSON(): CBlipFillData {
    const data: CBlipFillData = {
      ['rasterImageId']:
        hookManager.get(Hooks.Attrs.getImageOriginUrl, {
          url: this.imageSrcId,
        }) ?? this.imageSrcId,
    }
    assignVal(data, 'encryptUrl', this.encryptUrl)
    hookManager.invoke(Hooks.Attrs.CheckKnownErrorImageUrl, {
      url: this.imageSrcId,
    })
    assignJSON(data, 'srcRect', this.srcRect)
    assignJSON(data, 'fillRect', this.fillRect)

    if (this.tile != null) {
      // need keep empty object value
      data['tile'] = toJSON(this.tile)
    } else {
      const stretch = this.stretch == null ? true : this.stretch
      assignVal(data, 'stretch', stretch)
    }

    assignVal(data, 'rotWithShape', this.rotWithShape)

    if (this.effects.length) {
      const effects = this.effects
        .filter((effect) => effect.Type === EffectType.ALPHAMODFIX)
        .map((effect) => toJSON(effect))
      assignVal(data, 'effects', effects)
    }

    return data
  }

  getTransparent() {
    let transparent
    this.effects.forEach((effect) => {
      if (effect.Type === EffectType.ALPHAMODFIX) {
        transparent = (255 * effect.amt) / 100
      }
    })
    return transparent
  }

  updateTransparent(trans: Nullable<number>) {
    const alphaEffectIndex = findIndex(
      (effect) => effect.Type === EffectType.ALPHAMODFIX,
      this.effects
    )
    let effect = this.effects.splice(alphaEffectIndex, 1)[0]
    if (trans != null) {
      effect = effect ?? new AlphaModFix()
      effect.amt = ((trans * 100) / 255) >> 0
      this.effects.unshift(effect)
    }
  }

  reset() {
    this.imageSrcId = ''
    this.encryptUrl = undefined
    this.srcRect = undefined
    this.fillRect = undefined
    this.stretch = undefined
    this.tile = undefined

    this.rotWithShape = undefined

    this.effects.length = 0
  }

  fromOptions(opts: BlipFillOptions) {
    if (!opts) return
    if (opts.imageSrcId != null) {
      this.imageSrcId = opts.imageSrcId
    }
    if (opts.rotWithShape != null) {
      this.rotWithShape = opts.rotWithShape
    }

    this.fillRect = fromOptions(this.fillRect, opts.fillRect, RelativeRect)
    this.srcRect = fromOptions(this.srcRect, opts.srcRect, RelativeRect)
    if (opts.stretch != null) {
      this.stretch = opts.stretch
    }
    this.tile = fromOptions(this.tile, opts.tile, BlipTileInfo)
    if (opts.effects) {
      this.effects = opts.effects.map((effect) => {
        return fromOptions(undefined, effect, AlphaModFix)!
      })
    }
  }

  fromJSON(data: CBlipFillData) {
    const oldImageId = this.imageSrcId
    const url = data['rasterImageId']
    this.imageSrcId =
      hookManager.get(Hooks.Attrs.GetImageForChangeOfficalTheme, { url }) ?? url

    if (data['encryptUrl']) {
      this.encryptUrl = data['encryptUrl']
    }
    if (
      this.imageSrcId != null &&
      !isBase64Url(this.imageSrcId) &&
      this.imageSrcId !== oldImageId
    ) {
      hookManager.invoke(Hooks.Attrs.CollectImageFromDataChange, {
        imageUrl: this.imageSrcId,
      })
      hookManager.invoke(Hooks.Attrs.CollectAssetsForPasting, {
        assetUrl: this.imageSrcId,
        assetObject: this,
      })
      hookManager.invoke(Hooks.Attrs.CheckKnownErrorImageUrl, {
        url: this.imageSrcId,
      })
    }
    this.srcRect = readFromJSON(data['srcRect'], RelativeRect)
    this.fillRect = readFromJSON(data['fillRect'], RelativeRect)
    this.stretch = data['stretch'] ?? true
    if (!isEmptyPropValue(data['tile'])) {
      this.tile = readFromJSON(data['tile'], BlipTileInfo)
    }
    this.rotWithShape = data['rotWithShape']

    if (data['effects']) {
      const effects = data['effects']
        .map((effectData) => {
          const { name, val } = effectData
          return readEffect(fromEffectType(name), val)
        })
        .filter(Boolean) as Effects[]
      this.effects = effects
    }
  }

  updateAsset(url: string) {
    this.setImageSrcId(url)
  }

  setImageSrcId(imageSrcId: string) {
    this.imageSrcId = imageSrcId
  }

  setEncryptUrl(encryptUrl?: string) {
    this.encryptUrl = encryptUrl
  }

  setSrcRect(srcRect: RelativeRect) {
    this.srcRect = srcRect
  }

  setFillRect(srcRect: RelativeRect) {
    this.fillRect = srcRect
  }

  setStretch(stretch: boolean) {
    this.stretch = stretch
    if (stretch && !this.fillRect) {
      this.fillRect = new RelativeRect()
    }
  }

  setTile(tile: BlipTileInfo) {
    this.tile = tile
  }

  clone() {
    const duplicate = new BlipFill()
    duplicate.imageSrcId = this.imageSrcId
    if (duplicate.imageSrcId) {
      hookManager.invoke(Hooks.Attrs.CollectAssetsForPasting, {
        assetUrl: duplicate.imageSrcId,
        assetObject: duplicate,
      })
    }
    duplicate.encryptUrl = this.encryptUrl
    duplicate.stretch = this.stretch
    if (isDict(this.tile)) {
      duplicate.tile = this.tile!.clone()
    }

    if (null != this.srcRect) duplicate.srcRect = this.srcRect.clone()
    if (null != this.fillRect) {
      duplicate.fillRect = this.fillRect.clone()
    }

    duplicate.rotWithShape = this.rotWithShape
    if (Array.isArray(this.effects)) {
      for (let i = 0; i < this.effects.length; ++i) {
        const eff = this.effects[i]
        if (eff && eff.clone) {
          duplicate.effects.push(eff.clone())
        }
      }
    }
    return duplicate
  }

  sameAs(fill: BlipFill) {
    if (
      (this.imageSrcId && !fill.imageSrcId) ||
      (fill.imageSrcId && !this.imageSrcId) ||
      (typeof this.imageSrcId === 'string' &&
        typeof fill.imageSrcId === 'string' &&
        checkImageSrc(this.imageSrcId) !== checkImageSrc(fill.imageSrcId))
    ) {
      return false
    }
    if (fill.imageSrcId !== this.imageSrcId) {
      return false
    }

    if (fill.stretch !== this.stretch) {
      return false
    }

    if (fill.rotWithShape !== this.rotWithShape) {
      return false
    }

    return (
      isObjectsTheSame(this.tile, fill.tile) &&
      isObjectsTheSame(this.srcRect, fill.srcRect) &&
      isObjectsTheSame(this.fillRect, fill.fillRect)
    )
  }
}
