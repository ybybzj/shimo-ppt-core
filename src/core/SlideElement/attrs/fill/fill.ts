import { Nullable } from '../../../../../liber/pervasive'

import { FillKIND } from '../../../common/const/attrs'
import { isNumber } from '../../../../common/utils'
import { InstanceType } from '../../../instanceTypes'
import {
  CFillEffectsData,
  ThemeColorValue,
} from '../../../../io/dataType/spAttrs'

import {
  CComplexColor,
  defaultRGBAColor,
  updateRGBA,
} from '../../../color/complexColor'

import { Theme } from '../theme'
import { Slide } from '../../../Slide/Slide'
import { SlideLayout } from '../../../Slide/SlideLayout'
import { SlideMaster } from '../../../Slide/SlideMaster'
import { ColorRGBA } from '../../../color/type'
import { ClrMap } from '../../../color/clrMap'
import { NotesMaster } from '../../../Slide/NotesMaster'
import { BlipFill, BlipFillOptions } from './blipFill'
import { SolidFill } from './SolidFill'
import { GradFill, GradFillOptions } from './GradFill'
import { PatternFill, PatternFillOptions } from './PatternFill'
import {
  addColorModToFill,
  calulateFillCColor,
  checkFills,
  fillFromJSON,
  fillToJSON,
  intersectFills,
  isEqualFills,
  updateAlphaToFill,
  updateColorForPh,
} from './utils'
import { GrpFill, NoFill } from './otherFills'
import { ColorMod } from '../../../color/ColorModifiers'
import { FillEffectType } from '../../../utilities/shape/attrs'
import { PresetColorValues } from '../../const'
import { ComputedRGBAColor } from '../../../color/ComputedRGBAColor'

export class FillEffects {
  readonly instanceType = InstanceType.FillEffects
  fill?: FillEffectType
  transparent?: number // 0 - 255
  static NoFill(): FillEffects {
    const ret = new FillEffects()
    ret.NoFill()
    return ret
  }

  static Grp(): FillEffects {
    const ret = new FillEffects()
    ret.Grp()
    return ret
  }

  static Blip(opts: BlipFillOptions): FillEffects {
    const ret = new FillEffects()
    ret.Blip(opts)
    return ret
  }

  static SolidRGBA(r: number, g: number, b: number, a?: number): FillEffects {
    const ret = new FillEffects()
    ret.SolidRGBA(r, g, b, a)
    return ret
  }
  static SolidScheme(
    colorId: ThemeColorValue,
    tintOrShade?: number
  ): FillEffects {
    const ret = new FillEffects()
    ret.SolidScheme(colorId)
    if (tintOrShade != null) {
      if (tintOrShade !== 0) {
        tintOrShade *= 100000.0
        let mod: ColorMod
        if (tintOrShade > 0) {
          mod = { name: 'tint', val: tintOrShade }
        } else {
          mod = { name: 'shade', val: Math.abs(tintOrShade) }
        }
        ret.addColorMod(mod)
      }
    }
    return ret
  }

  static SolidPrst(id: Nullable<PresetColorValues>): FillEffects {
    const ret = new FillEffects()
    ret.SolidPrst(id)
    return ret
  }

  static SolidComputed(color: Nullable<ComputedRGBAColor>): FillEffects {
    const ret = new FillEffects()
    ret.SolidComputed(color)
    return ret
  }

  static SolidCColor(ccolor: Nullable<CComplexColor>): FillEffects {
    const ret = new FillEffects()
    ret.SolidCColor(ccolor)
    return ret
  }

  static Grad(opts: GradFillOptions): FillEffects {
    const ret = new FillEffects()
    ret.Grad(opts)
    return ret
  }

  static Pattern(opts: PatternFillOptions): FillEffects {
    const ret = new FillEffects()
    ret.Pattern(opts)
    return ret
  }

  NoFill() {
    if (this.fill?.type !== FillKIND.NOFILL) {
      this.fill = new NoFill()
    }
  }

  Grp() {
    if (this.fill?.type !== FillKIND.GRP) {
      const fill = new GrpFill()
      this.fill = fill
    }
  }

  Blip(opts: BlipFillOptions) {
    let fill = this.fill
    if (fill?.type !== FillKIND.BLIP) {
      fill = new BlipFill()
    } else {
      fill.reset()
    }
    fill.fromOptions(opts)

    this.fill = fill
  }

  SolidRGBA(r: number, g: number, b: number, a?: number) {
    const ccolor = CComplexColor.RGBA(r, g, b, a)
    updateSolidFillEffects(this, ccolor)
  }
  SolidScheme(colorId: ThemeColorValue) {
    const ccolor = CComplexColor.Scheme(colorId)
    updateSolidFillEffects(this, ccolor)
  }

  SolidPrst(id: Nullable<PresetColorValues>) {
    const ccolor = CComplexColor.Prst(id)
    updateSolidFillEffects(this, ccolor)
  }

  SolidComputed(color: Nullable<ComputedRGBAColor>) {
    const ccolor = CComplexColor.Computed(color)
    updateSolidFillEffects(this, ccolor)
  }

  SolidCColor(ccolor: Nullable<CComplexColor>) {
    updateSolidFillEffects(this, ccolor ? ccolor.clone() : undefined)
  }

  Grad(opts: GradFillOptions) {
    let fill = this.fill
    if (fill?.type !== FillKIND.GRAD) {
      fill = new GradFill()
    } else {
      fill.reset()
    }
    fill.fromOptions(opts)

    this.fill = fill
  }

  Pattern(opts: PatternFillOptions) {
    let fill = this.fill
    if (fill?.type !== FillKIND.PATT) {
      fill = new PatternFill()
    } else {
      fill.reset()
    }
    fill.fromOptions(opts)

    this.fill = fill
  }

  check(theme: Nullable<Theme>, colorMap: Nullable<ClrMap>) {
    if (this.fill) {
      checkFills(this.fill, theme, colorMap)
    }
  }

  addColorMod(mod: ColorMod) {
    const fill = this.fill
    if (fill) {
      addColorModToFill(fill, mod)
    }
  }

  updateAlpha(alpha: Nullable<number>) {
    const fill = this.fill
    if (fill) {
      updateAlphaToFill(fill, alpha)
    }
  }

  updateColorForPh(ccolor: Nullable<CComplexColor>) {
    const fill = this.fill
    if (fill) {
      updateColorForPh(fill, ccolor)
    }
  }

  isComplexFill() {
    const filltype = this.fill?.type
    return (
      filltype === FillKIND.PATT ||
      filltype === FillKIND.BLIP ||
      filltype === FillKIND.GRAD
    )
  }

  setTransparent(transparent: number) {
    this.transparent = transparent
  }

  toJSON(): CFillEffectsData {
    const data: CFillEffectsData = {}
    const trans =
      this.transparent != null && this.transparent !== 255
        ? this.transparent
        : null
    const fill = this.fill
    if (fill) {
      data['fill'] = fillToJSON(fill, trans)
    }
    return data
  }

  fromJSON(data: CFillEffectsData) {
    if (data['fill']) {
      const fill = fillFromJSON(data['fill'])
      if (fill) {
        this.fill = fill
      }
    }
    // update transparent
    if (this.fill?.type === FillKIND.SOLID) {
      const transparent = this.fill.color?.getAlphaFromMod()

      if (transparent) {
        this.transparent = transparent
      }
    }
  }

  calculate(
    theme: Nullable<Theme>,
    slide: Nullable<Slide>,
    layout: Nullable<SlideLayout>,
    master: Nullable<SlideMaster | NotesMaster>,
    rgba?: ColorRGBA,
    colorMap?: ClrMap
  ) {
    if (this.fill) {
      calulateFillCColor(
        this.fill,
        theme,
        slide,
        layout,
        master,
        rgba,
        colorMap
      )
    }
  }

  getRGBAColor(rgba_?: ColorRGBA) {
    const rgba = rgba_ ?? defaultRGBAColor()
    const fill = this.fill
    if (fill) {
      switch (fill.type) {
        case FillKIND.SOLID: {
          if (fill.color) {
            updateRGBA(rgba, fill.color.rgba)
            return rgba
          }
          break
        }
        case FillKIND.GRAD: {
          defaultRGBAColor(rgba)

          const colors = fill.gsList
          const len = colors.length

          if (0 === len) return rgba

          for (let i = 0; i < len; i++) {
            rgba.r += colors[i].color?.rgba.r ?? 0
            rgba.g += colors[i].color?.rgba.g ?? 0
            rgba.b += colors[i].color?.rgba.b ?? 0
          }

          rgba.r = (rgba.r / len) >> 0
          rgba.g = (rgba.g / len) >> 0
          rgba.b = (rgba.b / len) >> 0

          return rgba
        }
        case FillKIND.PATT: {
          if (fill.fgClr) {
            updateRGBA(rgba, fill.fgClr.rgba)
            return rgba
          }
          break
        }
      }
    }
    return defaultRGBAColor(rgba)
  }

  clone() {
    const fill = this.fill
    if (fill) {
      switch (fill.type) {
        case FillKIND.NOFILL:
        case FillKIND.GRP: {
          return this
        }
        case FillKIND.BLIP:
        case FillKIND.SOLID:
        case FillKIND.GRAD:
        case FillKIND.PATT: {
          const ret = new FillEffects()
          ret.fill = this.fill?.clone()
          ret.transparent = this.transparent
          return ret
        }
      }
    }
    return this
  }

  merge(fillEffects: Nullable<FillEffects>) {
    if (fillEffects) {
      if (fillEffects.fill != null) {
        this.fill = fillEffects.fill.clone()
        if (this.fill.type === FillKIND.PATT) {
          const patternFill = this.fill
          if (!patternFill.fgClr) {
            patternFill.fgClr = CComplexColor.RGBA(0, 0, 0)
          }
          if (!patternFill.bgClr) {
            patternFill.bgClr = CComplexColor.RGBA(255, 255, 255)
          }
        }
      }
      if (fillEffects.transparent != null) {
        this.transparent = fillEffects.transparent
      }
    }
  }

  sameAs(fillEffects: Nullable<FillEffects>) {
    if (fillEffects == null) {
      return false
    }

    if (
      isNumber(this.transparent) !== isNumber(fillEffects.transparent) ||
      (isNumber(this.transparent) &&
        this.transparent !== fillEffects.transparent)
    ) {
      return false
    }

    return isEqualFills(this.fill, fillEffects.fill)
  }

  intersect(fillEffects: FillEffects) {
    if (fillEffects == null) {
      return undefined
    }
    const ret = new FillEffects()

    if (
      !(
        this.transparent == null ||
        fillEffects.transparent == null ||
        this.transparent !== fillEffects.transparent
      )
    ) {
      ret.transparent = fillEffects.transparent
    }

    ret.fill = intersectFills(this.fill, fillEffects.fill) ?? undefined
    return ret
  }

  isNoFill() {
    return this.fill == null || this.fill.type === FillKIND.NOFILL
  }
}

export function validateFillMods(fillEffect: FillEffects) {
  const fill = fillEffect.fill
  if (fill) {
    switch (fill.type) {
      case FillKIND.SOLID: {
        fill.color && fill.color.validateMods()
        break
      }
      case FillKIND.GRAD: {
        for (let i = 0, l = fill.gsList.length; i < l; i++) {
          const gs = fill.gsList[i]
          if (gs && gs.color) {
            gs.color.validateMods()
          }
        }

        break
      }
      case FillKIND.PATT: {
        fill.fgClr && fill.fgClr.validateMods()
        fill.bgClr && fill.bgClr.validateMods()
        break
      }
    }
  }
}

function updateSolidFillEffects(
  fillEffects: FillEffects,
  ccolor: Nullable<CComplexColor>
) {
  let fill = fillEffects.fill
  if (fill?.type !== FillKIND.SOLID) {
    fill = new SolidFill(ccolor)
  } else {
    fill.color = ccolor
  }
  const transparent = ccolor?.getAlphaFromMod()

  if (transparent != null) {
    fillEffects.transparent = transparent
  }

  fillEffects.fill = fill
}
