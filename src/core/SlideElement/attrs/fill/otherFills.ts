import { FillKIND } from '../../../common/const/attrs'
import { InstanceType } from '../../../instanceTypes'

export class NoFill {
  readonly instanceType = InstanceType.NoFill
  readonly type = FillKIND.NOFILL
  constructor() {}

  check() {}

  clone() {
    return new NoFill()
  }
}

export class GrpFill {
  readonly instanceType = InstanceType.GrpFill
  readonly type = FillKIND.GRP
  constructor() {}

  check() {}

  clone() {
    return new GrpFill()
  }
}
