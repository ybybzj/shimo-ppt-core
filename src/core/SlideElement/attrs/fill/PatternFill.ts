import { Nullable } from '../../../../../liber/pervasive'
import { FillKIND, PresetPatternValToNumMap } from '../../../common/const/attrs'
import { InstanceType } from '../../../instanceTypes'
import {
  PresetPatternNumVal,
  CPatternFillData,
  toPresetPatternVal,
  fromPresetPatternVal,
} from '../../../../io/dataType/spAttrs'
import { assignJSON, readFromJSON } from '../../../../io/utils'
import { ClrMap } from '../../../color/clrMap'
import { CComplexColor } from '../../../color/complexColor'
import { isObjectsTheSame } from '../../../common/helpers'
import { Theme } from '../theme'
export interface PatternFillOptions {
  patternType?: PresetPatternNumVal
  fgClr?: Nullable<CComplexColor>
  bgClr?: Nullable<CComplexColor>
}
export class PatternFill {
  readonly instanceType = InstanceType.PatternFill
  readonly type = FillKIND.PATT
  patternType: PresetPatternNumVal
  fgClr?: Nullable<CComplexColor>
  bgClr?: Nullable<CComplexColor>
  constructor(patternType?: PresetPatternNumVal) {
    this.patternType = patternType ?? PresetPatternValToNumMap.cross
    this.fgClr = undefined
    this.bgClr = undefined
  }

  reset() {
    this.patternType = PresetPatternValToNumMap.cross
    this.fgClr = undefined
    this.bgClr = undefined
  }

  fromOptions(opts: PatternFillOptions) {
    if (!opts) return
    if (opts.bgClr != null) {
      this.bgClr = opts.bgClr
    }

    if (opts.fgClr != null) {
      this.fgClr = opts.fgClr
    }
    if (opts.patternType != null) {
      this.patternType = opts.patternType
    }
  }
  check(theme: Nullable<Theme>, colorMap: Nullable<ClrMap>) {
    if (this.fgClr) this.fgClr.check(theme, colorMap)
    if (this.bgClr) this.bgClr.check(theme, colorMap)
  }

  toJSON(): CPatternFillData {
    const data: CPatternFillData = {
      ['ftype']: toPresetPatternVal(this.patternType),
    }
    assignJSON(data, 'fgClr', this.fgClr)
    assignJSON(data, 'bgClr', this.bgClr)
    return data
  }

  fromJSON(data: CPatternFillData) {
    this.patternType = fromPresetPatternVal(data['ftype'])
    if (data['fgClr']) {
      this.fgClr = readFromJSON(data['fgClr'], CComplexColor)
    }
    if (data['bgClr']) {
      this.bgClr = readFromJSON(data['bgClr'], CComplexColor)
    }
  }

  sameAs(fill: PatternFill) {
    return (
      isObjectsTheSame(this.fgClr, fill.fgClr) &&
      isObjectsTheSame(this.bgClr, fill.bgClr) &&
      this.patternType === fill.patternType
    )
  }

  clone() {
    const duplicate = new PatternFill()
    duplicate.patternType = this.patternType
    if (this.fgClr) {
      duplicate.fgClr = this.fgClr.clone()
    }
    if (this.bgClr) {
      duplicate.bgClr = this.bgClr.clone()
    }
    return duplicate
  }
}
