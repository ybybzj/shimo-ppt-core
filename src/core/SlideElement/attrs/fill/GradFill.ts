import { Nullable } from '../../../../../liber/pervasive'
import { FillGradPathType, FillKIND } from '../../../common/const/attrs'
import { isNumber } from '../../../../common/utils'
import { GradientDirections } from '../../../../exports/type'
import { InstanceType } from '../../../instanceTypes'
import {
  CGsData,
  GradLinData,
  GradPathType,
  GradPathData,
  CGradFillData,
  ST_TileFlipMode,
} from '../../../../io/dataType/spAttrs'
import {
  assignJSON,
  readFromJSON,
  assignJSONArray,
  assignVal,
} from '../../../../io/utils'
import { ClrMap } from '../../../color/clrMap'
import { CComplexColor } from '../../../color/complexColor'
import { fromOptions, isObjectsTheSame } from '../../../common/helpers'
import { Theme } from '../theme'
import { RelativeRect, RelativeRectOptions } from './RelativeRect'

export interface CGsOptions {
  color?: CComplexColor
  pos: number
}

export class CGs {
  color?: CComplexColor
  pos: number
  constructor(pos: number, color?: CComplexColor) {
    this.color = color
    this.pos = pos
  }

  setColor(color: CComplexColor) {
    this.color = color
  }

  setPos(pos) {
    this.pos = pos
  }

  toJSON(): CGsData {
    const data: CGsData = { ['pos']: this.pos }
    assignJSON(data, 'color', this.color)
    return data
  }

  fromJSON(data: CGsData) {
    this.pos = data['pos']
    this.color = readFromJSON(data['color'], CComplexColor)
  }

  sameAs(gs: CGs) {
    return this.pos === gs.pos && isObjectsTheSame(this.color, gs.color)
  }

  clone() {
    const duplicate = new CGs(this.pos, this.color?.clone())

    return duplicate
  }

  intersect(gs: CGs) {
    if (gs.pos !== this.pos) {
      return undefined
    }
    const ccolor = this.color?.intersect(gs.color)
    if (!ccolor) {
      return undefined
    }
    const ret = new CGs(gs.pos === this.pos ? this.pos : 0, ccolor)
    return ret
  }
}

export interface GradLinOptions {
  angle: number
  scale: boolean
}

export class GradLin {
  angle: number
  scale: boolean
  constructor(angle?: number, scale?: boolean) {
    this.angle = angle ?? 5400000
    this.scale = scale ?? true
  }

  reset() {
    this.angle = 5400000
    this.scale = true
  }

  fromOptions(opts: GradLinOptions) {
    if (!opts) return
    if (opts.angle != null) {
      this.angle = opts.angle
    }
    if (opts.scale != null) {
      this.scale = opts.scale
    }
  }

  toJSON(): GradLinData {
    return {
      ['angle']: this.angle,
      ['scale']: this.scale,
    }
  }

  fromJSON(data: GradLinData) {
    this.angle = data['angle']
    this.scale = data['scale']
  }

  sameAs(lin: GradLinOptions) {
    if (this.angle !== lin.angle) return false
    if (this.scale !== lin.scale) return false

    return true
  }

  clone() {
    const duplicate = new GradLin(this.angle, this.scale)
    return duplicate
  }
}

export interface GradPathOptions {
  path?: GradPathType
  fillRect?: RelativeRectOptions
}

export class GradPath {
  path: GradPathType
  fillRect?: RelativeRect
  constructor(pathType: GradPathType, fillRect?: RelativeRect) {
    this.path = pathType
    this.fillRect = fillRect
  }

  reset() {
    this.fillRect = undefined
  }

  fromOptions(opts: GradPathOptions) {
    if (opts == null) return
    if (opts.path != null) {
      this.path = opts.path
    }

    this.fillRect = fromOptions(this.fillRect, opts.fillRect, RelativeRect)
  }

  setFillRect(rect: RelativeRect) {
    this.fillRect = rect
  }

  toJSON(): GradPathData {
    const data: GradPathData = {
      ['path']: this.path,
    }
    assignJSON(data, 'rect', this.fillRect)
    return data
  }

  fromJSON(data: GradPathData) {
    this.path = data['path']
    if (data['rect'] != null) {
      this.fillRect = readFromJSON(data['rect'], RelativeRect)
    }
  }

  getDirection(): GradientDirections | undefined {
    if (!this.fillRect) return
    const { l, t, r } = this.fillRect
    switch (this.path) {
      case FillGradPathType.Circle:
        if (isNumber(l) && l === 100) {
          return isNumber(t) && t === 100
            ? GradientDirections.FromBottomRightCorner
            : GradientDirections.FromTopRightCorner
        } else if (isNumber(r) && 100 - r === 100) {
          return isNumber(t) && t === 100
            ? GradientDirections.FromBottomLeftCorner
            : GradientDirections.FromTopLeftCorner
        } else {
          return GradientDirections.FromCenter
        }
      case FillGradPathType.Rect:
      case FillGradPathType.Shape:
        return
    }
  }

  sameAs(path: GradPath) {
    if (this.path !== path.path) return false
    return true
  }

  clone() {
    const duplicate = new GradPath(this.path, this.fillRect)
    return duplicate
  }
}

export interface GradFillOptions {
  ln?: GradLinOptions
  path?: GradPathOptions
  tileRect?: RelativeRectOptions
  gsList?: CGsOptions[]
}

export class GradFill {
  readonly instanceType = InstanceType.GradFill
  readonly type = FillKIND.GRAD
  gsList: CGs[]
  ln?: GradLin
  path?: GradPath
  rotateWithShape: boolean = true
  tileRect?: RelativeRect
  flipMode?: ST_TileFlipMode
  constructor() {
    this.gsList = []

    this.ln = undefined
    this.path = undefined
  }

  reset() {
    this.gsList.length = 0

    this.ln?.reset()
    this.path?.reset()
    this.rotateWithShape = true
    this.tileRect?.reset()
    this.flipMode = undefined
  }

  fromOptions(opts: GradFillOptions) {
    if (!opts) return
    this.ln = fromOptions(this.ln, opts.ln, GradLin)
    this.path = fromOptions(this.path, opts.path, GradPath)
    this.tileRect = fromOptions(this.tileRect, opts.tileRect, RelativeRect)
    if (opts.gsList && opts.gsList.length > 0) {
      const gsList = opts.gsList.map(({ pos, color }) => new CGs(pos, color))
      this.gsList = gsList
    }
  }

  check(theme: Nullable<Theme>, colorMap: Nullable<ClrMap>) {
    for (let i = 0; i < this.gsList.length; ++i) {
      const ccolor = this.gsList[i].color
      if (ccolor) {
        ccolor.check(theme, colorMap)
      }
    }
  }

  addColor(gs: CGs) {
    this.gsList.push(gs)
  }

  toJSON(): CGradFillData {
    const data: CGradFillData = {}
    assignJSONArray(data, 'colors', this.gsList)
    assignJSON(data, 'lin', this.ln)
    assignJSON(data, 'path', this.path)
    assignJSON(data, 'tileRect', this.tileRect)
    assignVal(data, 'flip', this.flipMode)
    assignVal(data, 'rotWithShape', this.rotateWithShape)
    return data
  }

  fromJSON(data: CGradFillData) {
    if (data['colors'] != null) {
      const gsListData = data['colors']

      this.gsList = gsListData.map(
        ({ pos, color }) => new CGs(pos, readFromJSON(color, CComplexColor))
      )
      this.gsList.sort(function (a, b) {
        return a.pos - b.pos
      })
    }
    this.ln = readFromJSON(data['lin'], GradLin)
    this.path = readFromJSON(data['path'], GradPath)
    this.tileRect = readFromJSON(data['tileRect'], RelativeRect)
    if (data['flip'] != null) {
      this.flipMode = data['flip']
    }
    if (data['rotWithShape'] != null) {
      this.rotateWithShape = data['rotWithShape']
    }
  }

  sameAs(fill: GradFill) {
    if (fill.gsList.length !== this.gsList.length) {
      return false
    }
    for (let i = 0; i < this.gsList.length; ++i) {
      if (!this.gsList[i].sameAs(fill.gsList[i])) {
        return false
      }
    }

    return (
      isObjectsTheSame(this.path, fill.path) &&
      isObjectsTheSame(this.ln, fill.ln)
    )
  }

  clone() {
    const duplicate = new GradFill()
    for (let i = 0; i < this.gsList.length; ++i) {
      duplicate.gsList[i] = this.gsList[i].clone()
    }

    if (this.ln) duplicate.ln = this.ln.clone()
    if (this.path) duplicate.path = this.path.clone()
    if (this.tileRect) duplicate.tileRect = this.tileRect.clone()

    if (this.rotateWithShape != null) {
      duplicate.rotateWithShape = this.rotateWithShape
    }

    if (this.flipMode != null) {
      duplicate.flipMode = this.flipMode
    }

    return duplicate
  }
}
