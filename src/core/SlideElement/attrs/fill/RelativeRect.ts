import { isNumber } from '../../../../common/utils'
import { InstanceType } from '../../../instanceTypes'
import { CSrcRectData } from '../../../../io/dataType/spAttrs'
import { assignVal, scaleValue } from '../../../../io/utils'

export interface RelativeRectOptions {
  l: number
  t: number
  r: number
  b: number
}

export class RelativeRect {
  readonly instanceType = InstanceType.RelativeRect
  l!: number
  t!: number
  r!: number
  b!: number

  constructor(l?: number, t?: number, r?: number, b?: number) {
    this.set(l ?? 0, t ?? 0, r ?? 100, b ?? 100)
  }

  reset() {
    this.l = 0
    this.t = 0
    this.r = 100
    this.b = 100
  }

  isInitial() {
    return this.l === 0 && this.t === 0 && this.r === 100 && this.b === 100
  }

  sameAs(rect: RelativeRect) {
    return (
      this.l === rect.l &&
      this.t === rect.t &&
      this.r === rect.r &&
      this.b === rect.b
    )
  }

  fromOptions(opts: RelativeRectOptions) {
    if (!opts) return
    this.set(opts.l, opts.t, opts.r, opts.b)
  }

  set(l: number, t: number, r: number, b: number) {
    this.l = l
    this.t = t
    this.r = r
    this.b = b
    if (this.l > this.r) {
      const t = this.l
      this.l = this.r
      this.r = t
    }
    if (this.t > this.b) {
      const t = this.t
      this.t = this.b
      this.b = t
    }
  }

  toJSON(): CSrcRectData {
    const data: CSrcRectData = {}
    assignVal(data, 'l', scaleValue(this.l, 1000))
    assignVal(data, 't', scaleValue(this.t, 1000))
    if (isNumber(this.l) && isNumber(this.r)) {
      const n = 100 - this.r
      assignVal(data, 'r', scaleValue(n, 1000))
    }
    if (isNumber(this.l) && isNumber(this.b)) {
      const n = 100 - this.b
      assignVal(data, 'b', scaleValue(n, 1000))
    }
    return data
  }

  fromJSON(data: CSrcRectData) {
    if (
      null == data['l'] &&
      null == data['t'] &&
      null == data['r'] &&
      null == data['b']
    ) {
      this.l = this.l ?? 0
      this.t = this.t ?? 0
      this.r = this.r ?? 100
      this.b = this.b ?? 100
      return
    }
    const res = {
      l: (data['l'] ?? 0) / 1000,
      t: (data['t'] ?? 0) / 1000,
      r: (data['r'] ?? 0) / 1000,
      b: (data['b'] ?? 0) / 1000,
    }

    res.r = 100 - res.r
    res.b = 100 - res.b

    this.set(res.l, res.t, res.r, res.b)
  }

  clone() {
    const ret = new RelativeRect(this.l, this.t, this.r, this.b)

    return ret
  }

  isValid() {
    return this.l != null || this.t != null || this.r != null || this.b != null
  }
}
