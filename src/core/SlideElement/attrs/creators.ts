import { FillEffects } from './fill/fill'
import { FontCollection, FontScheme, Theme } from './theme'
import { CComplexColor } from '../../color/complexColor'
import { Ln } from './line'

import { ThemeColor } from '../../../io/dataType/spAttrs'
import { Presentation } from '../../Slide/Presentation'

export const DEFAULT_THEME_NAME = 'SM_Theme_D7EA2X'

export function createDefaultTheme(presentation: Presentation): Theme {
  const defaultFontName = 'Arial'

  const theme = new Theme()
  theme.name = DEFAULT_THEME_NAME
  theme.presentation = presentation
  theme.setFontScheme(new FontScheme())
  const fontScheme = theme.themeElements.fontScheme
  fontScheme.setMajorFont(new FontCollection(fontScheme))
  fontScheme.setMinorFont(new FontCollection(fontScheme))
  fontScheme.majorFont.setLatin(defaultFontName)
  fontScheme.minorFont.setLatin(defaultFontName)

  const scheme = theme.themeElements.clrScheme

  scheme.colors[8] = CComplexColor.RGBA(43, 48, 51)
  scheme.colors[12] = CComplexColor.RGBA(255, 255, 255)
  scheme.colors[9] = CComplexColor.RGBA(131, 135, 140)
  scheme.colors[13] = CComplexColor.RGBA(236, 237, 237)
  scheme.colors[0] = CComplexColor.RGBA(230, 151, 96)
  scheme.colors[1] = CComplexColor.RGBA(210, 113, 104)
  scheme.colors[2] = CComplexColor.RGBA(121, 157, 222)
  scheme.colors[3] = CComplexColor.RGBA(238, 200, 105)
  scheme.colors[4] = CComplexColor.RGBA(146, 192, 199)
  scheme.colors[5] = CComplexColor.RGBA(200, 198, 135)
  scheme.colors[11] = CComplexColor.RGBA(91, 160, 231)
  scheme.colors[10] = CComplexColor.RGBA(122, 102, 204)

  // -------------- fill styles -------------------------
  let brush = FillEffects.SolidScheme(ThemeColor.None)

  theme.themeElements.fmtScheme.fillStyleLst.push(brush)

  brush = FillEffects.SolidRGBA(0, 0, 0)

  theme.themeElements.fmtScheme.fillStyleLst.push(brush)

  brush = FillEffects.SolidRGBA(0, 0, 0)

  theme.themeElements.fmtScheme.fillStyleLst.push(brush)
  // ----------------------------------------------------

  // -------------- back styles -------------------------
  brush = FillEffects.SolidScheme(ThemeColor.None)

  theme.themeElements.fmtScheme.bgFillStyleLst.push(brush)

  brush = FillEffects.SolidRGBA(0, 0, 0)

  theme.themeElements.fmtScheme.bgFillStyleLst.push(brush)

  brush = FillEffects.SolidRGBA(0, 0, 0)

  theme.themeElements.fmtScheme.bgFillStyleLst.push(brush)
  // ----------------------------------------------------

  let pen = Ln.new({
    w: 9525,
    fillEffects: FillEffects.SolidScheme(ThemeColor.None),
  })
  let mod = {
    name: 'shade',
    val: 95000,
  }
  pen.addColorMod(mod)

  mod = { name: 'satMod', val: 105000 }
  pen.addColorMod(mod)

  theme.themeElements.fmtScheme.lnStyleLst.push(pen)

  pen = Ln.new({
    w: 25400,
    fillEffects: FillEffects.SolidScheme(ThemeColor.None),
  })

  theme.themeElements.fmtScheme.lnStyleLst.push(pen)

  pen = Ln.new({
    w: 38100,
    fillEffects: FillEffects.SolidScheme(ThemeColor.None),
  })

  theme.themeElements.fmtScheme.lnStyleLst.push(pen)
  theme.extraClrSchemeLst = []
  return theme
}

export const gLineStylePresets = {
  ['line']: true,
  ['bracePair']: true,
  ['leftBrace']: true,
  ['rightBrace']: true,
  ['bracketPair']: true,
  ['leftBracket']: true,
  ['rightBracket']: true,
  ['bentConnector2']: true,
  ['bentConnector3']: true,
  ['bentConnector4']: true,
  ['bentConnector5']: true,
  ['curvedConnector2']: true,
  ['curvedConnector3']: true,
  ['curvedConnector4']: true,
  ['curvedConnector5']: true,
  ['straightConnector1']: true,
  ['arc']: true,
} as const

let _gVisitedHLinkColor
export const gVisitedHLinkColor = (): FillEffects => {
  if (_gVisitedHLinkColor == null) {
    _gVisitedHLinkColor = FillEffects.SolidScheme(
      ThemeColor.FollowedHyperlink,
      0
    )
  }
  return _gVisitedHLinkColor
}

let _gHLinkColor
export const gHLinkColor = (): FillEffects => {
  if (_gHLinkColor == null) {
    _gHLinkColor = FillEffects.SolidScheme(ThemeColor.Hyperlink, 0)
  }
  return _gHLinkColor
}
