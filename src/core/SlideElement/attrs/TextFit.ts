import { assignVal } from '../../../io/utils'
import {
  CTextFitData,
  toTextFitType,
  TextFitTypeOO,
  fromTextFitType,
} from '../../../io/dataType/spAttrs'
import { TextAutoFitType } from '../const'

export class TextFit {
  type: TextFitTypeOO = TextAutoFitType.NO
  fontScale?: number
  lnSpcReduction?: number
  static new(
    type: TextFitTypeOO,
    fontScale?: number,
    lnSpcReduction?: number
  ): TextFit {
    const ret = new TextFit()
    ret.type = type
    if (fontScale != null) ret.fontScale = fontScale
    if (lnSpcReduction != null) ret.lnSpcReduction = lnSpcReduction
    return ret
  }
  constructor() {
    this.fontScale = undefined
    this.lnSpcReduction = undefined
  }
  reset() {
    this.fontScale = undefined
    this.lnSpcReduction = undefined
    this.type = TextAutoFitType.NO
  }

  clone() {
    return TextFit.new(this.type, this.fontScale, this.lnSpcReduction)
  }

  toJSON(): CTextFitData {
    const data: CTextFitData = {}
    if (this.type != null && this.type !== -1) {
      assignVal(data, 'type', toTextFitType(this.type))
    }
    assignVal(data, 'fontScale', this.fontScale)
    assignVal(data, 'lnSpcReduction', this.lnSpcReduction)
    return data
  }

  fromJSON(data: CTextFitData) {
    if (data['type'] != null) {
      this.type = fromTextFitType(data['type'])
    } else {
      this.type = -1
    }

    if (data['fontScale'] != null) {
      this.fontScale = data['fontScale']
    }

    if (data['lnSpcReduction'] != null) {
      this.lnSpcReduction = data['lnSpcReduction']
    }
  }
}
