import { CComplexColor } from '../../color/complexColor'
import { isDict } from '../../../common/utils'
import {
  StyleRefData,
  FontRefData,
  toFontRefIdx,
  fromFontRefIdx,
} from '../../../io/dataType/spAttrs'
import { readFromJSON, assignJSON, toJSON } from '../../../io/utils'
import { FontStyleInd, FontStyleIndValues } from '../const'
import { Nullable } from '../../../../liber/pervasive'
import { ColorMod } from '../../color/ColorModifiers'
import { isObjectsTheSame } from '../../common/helpers'

export class StyleRef {
  idx: number
  color: CComplexColor
  static new(idx: number, ccolor?: CComplexColor): StyleRef {
    const ret = new StyleRef()
    ret.idx = idx
    if (ccolor) {
      ret.color = ccolor
    }
    return ret
  }
  constructor() {
    this.idx = 0
    this.color = new CComplexColor()
  }

  reset() {
    this.idx = 0
    this.color.reset()
  }

  sameAs(styleRef: Nullable<StyleRef>) {
    if (styleRef == null) {
      return false
    }
    if (this.idx !== styleRef.idx) {
      return false
    }

    if (this.color.sameAs(styleRef.color) === false) {
      return false
    }

    return true
  }

  addColorMod(mod: ColorMod) {
    this.color.addColorMod(mod)
  }

  clone() {
    const duplicate = new StyleRef()
    duplicate.idx = this.idx
    if (this.color) duplicate.color = this.color.clone()
    return duplicate
  }

  toJSON(): StyleRefData {
    const data: StyleRefData = { ['idx']: this.idx }
    if (isDict(this.color)) {
      data['color'] = toJSON(this.color)
    }
    return data
  }

  fromJSON(data: StyleRefData) {
    if (null != data['idx']) {
      this.idx = data['idx']
    }
    if (isDict(data['color'])) {
      const ccolor = readFromJSON(data['color'], CComplexColor)
      if (ccolor) {
        this.color = ccolor
      }
    }
  }
}

export class FontRef {
  idx: FontStyleIndValues
  color?: CComplexColor
  static new(idx: FontStyleIndValues, ccolor?: CComplexColor): FontRef {
    const ret = new FontRef()
    ret.idx = idx
    ret.color = ccolor
    return ret
  }
  constructor() {
    this.idx = FontStyleInd.none
    this.color = undefined
  }

  clone() {
    const duplicate = new FontRef()
    duplicate.idx = this.idx
    if (this.color) duplicate.color = this.color.clone()
    return duplicate
  }

  sameAs(ref: Nullable<FontRef>) {
    if (ref == null) return false

    return isObjectsTheSame(this.color, ref.color) && this.idx === ref.idx
  }

  toJSON(): FontRefData {
    const data: FontRefData = { ['idx']: toFontRefIdx(this.idx) }
    if (this.color != null) {
      assignJSON(data, 'color', this.color)
    }
    return data
  }

  fromJSON(data: FontRefData) {
    this.idx = fromFontRefIdx(data['idx'])
    if (data['color']) {
      this.color = readFromJSON(data['color'], CComplexColor)
    }
  }
}
