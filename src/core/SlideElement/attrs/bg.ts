import { assignJSON, readFromJSON, assignVal } from '../../../io/utils'
import {
  CBgPrData,
  CBgData,
  BwModeKindValue,
  toBwMode,
  fromBwMode,
} from '../../../io/dataType/spAttrs'
import { FillEffects } from './fill/fill'
import { StyleRef } from './refs'
import { CComplexColor } from '../../color/complexColor'

export class BgPr {
  fill?: FillEffects
  shadeToTitle: boolean
  static new(fill: FillEffects, shadeToTitle?: boolean): BgPr {
    const ret = new BgPr()
    ret.fill = fill
    if (shadeToTitle != null) {
      ret.shadeToTitle = shadeToTitle
    }
    return ret
  }
  constructor() {
    this.fill = undefined
    this.shadeToTitle = false
  }

  clone() {
    const dup = new BgPr()
    if (this.fill != null) {
      dup.fill = this.fill.clone()
    }
    dup.shadeToTitle = this.shadeToTitle
    return dup
  }

  toJSON(): CBgPrData {
    const data: CBgPrData = {
      ['shadeToTitle']: this.shadeToTitle,
    }
    assignJSON(data, 'fill', this.fill)
    return data
  }

  fromJSON(data: CBgPrData) {
    this.shadeToTitle = data['shadeToTitle']
    if (data['fill']) {
      this.fill = readFromJSON(data['fill'], FillEffects)
    }
    return data
  }
}

export class Bg {
  bwMode?: BwModeKindValue
  bgPr?: BgPr
  bgRef?: StyleRef
  static Pr(
    fill: FillEffects,
    shadeToTitle?: boolean,
    bwMode?: BwModeKindValue
  ): Bg {
    const ret = new Bg()
    const pr = BgPr.new(fill, shadeToTitle)
    ret.bgPr = pr
    if (bwMode != null) {
      ret.bwMode = bwMode
    }
    return ret
  }

  static Ref(idx: number, ccolor?: CComplexColor, bwMode?: BwModeKindValue) {
    const ret = new Bg()
    const ref = StyleRef.new(idx, ccolor)
    ret.bgRef = ref
    if (bwMode != null) {
      ret.bwMode = bwMode
    }
    return ret
  }

  constructor() {
    this.bwMode = undefined
    this.bgPr = undefined
    this.bgRef = undefined
  }

  clone() {
    const dup = new Bg()
    dup.bwMode = this.bwMode
    if (this.bgPr != null) {
      dup.bgPr = this.bgPr.clone()
    }
    if (this.bgRef != null) {
      dup.bgRef = this.bgRef.clone()
    }
    return dup
  }

  toJSON(): CBgData {
    const data: CBgData = {}
    if (this.bwMode != null) {
      assignVal(data, 'bwMode', toBwMode(this.bwMode))
    }

    assignJSON(data, 'bgPr', this.bgPr)
    assignJSON(data, 'bgRef', this.bgRef)
    return data
  }

  fromJSON(data: CBgData) {
    if (data['bwMode'] != null) {
      this.bwMode = fromBwMode(data['bwMode'])
    }

    if (data['bgPr'] != null) {
      this.bgPr = readFromJSON(data['bgPr'], BgPr)
    }

    if (data['bgRef'] != null) {
      this.bgRef = readFromJSON(data['bgRef'], StyleRef)
    }
  }
}
