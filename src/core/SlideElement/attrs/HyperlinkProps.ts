import { Nullable } from '../../../../liber/pervasive'
import { HyperlinkPropsData } from '../../../io/dataType/spAttrs'
import { assignVal } from '../../../io/utils'
export interface HyperlinkPropsOptions {
  snd?: string
  id?: string
  invalidUrl?: string
  action?: string
  tgtFrame?: string
  tooltip?: string
  history?: boolean
  highlightClick?: boolean
  endSnd?: boolean
}
export class HyperlinkProps {
  snd?: string
  id?: string
  invalidUrl?: string
  action?: string
  tgtFrame?: string
  tooltip?: string
  history?: boolean
  highlightClick?: boolean
  endSnd?: boolean
  static new(opts?: HyperlinkPropsOptions) {
    const ret = new HyperlinkProps()
    if (opts != null) {
      ret.fromOptions(opts)
    }
    return ret
  }
  fromOptions(opts: Nullable<HyperlinkPropsOptions>) {
    if (opts == null) return
    if (opts.snd != null) this.snd = opts.snd
    if (opts.id != null) this.id = opts.id
    if (opts.invalidUrl != null) this.invalidUrl = opts.invalidUrl
    if (opts.action != null) this.action = opts.action
    if (opts.tgtFrame != null) this.tgtFrame = opts.tgtFrame
    if (opts.tooltip != null) this.tooltip = opts.tooltip
    if (opts.history != null) this.history = opts.history
    if (opts.highlightClick != null) this.highlightClick = opts.highlightClick
    if (opts.endSnd != null) this.endSnd = opts.endSnd
  }
  constructor() {
    this.snd = undefined
    this.id = undefined
    this.invalidUrl = undefined
    this.action = undefined
    this.tgtFrame = undefined
    this.tooltip = undefined
    this.history = undefined
    this.highlightClick = undefined
    this.endSnd = undefined
  }

  toJSON(): HyperlinkPropsData {
    const data: HyperlinkPropsData = {}
    assignVal(data, 'snd', this.snd)
    assignVal(data, 'id', this.id)
    assignVal(data, 'invalidUrl', this.invalidUrl)
    assignVal(data, 'action', this.action)
    assignVal(data, 'tgtFrame', this.tgtFrame)
    assignVal(data, 'tooltip', this.tooltip)
    assignVal(data, 'history', this.history)
    assignVal(data, 'highlightClick', this.highlightClick)
    assignVal(data, 'endSnd', this.endSnd)

    return data
  }

  fromJSON(data: HyperlinkPropsData) {
    if (data['snd'] != null) {
      this.snd = data['snd']
    }
    if (data['id'] != null) {
      this.id = data['id']
    }
    if (data['invalidUrl'] != null) {
      this.invalidUrl = data['invalidUrl']
    }
    if (data['action'] != null) {
      this.action = data['action']
    }
    if (data['tgtFrame'] != null) {
      this.tgtFrame = data['tgtFrame']
    }
    if (data['tooltip'] != null) {
      this.tooltip = data['tooltip']
    }
    if (data['history'] != null) {
      this.history = data['history']
    }
    if (data['highlightClick'] != null) {
      this.highlightClick = data['highlightClick']
    }
    if (data['endSnd'] != null) {
      this.endSnd = data['endSnd']
    }
  }

  clone() {
    const ret = new HyperlinkProps()
    ret.snd = this.snd
    ret.id = this.id
    ret.invalidUrl = this.invalidUrl
    ret.action = this.action
    ret.tgtFrame = this.tgtFrame
    ret.tooltip = this.tooltip
    ret.history = this.history
    ret.highlightClick = this.highlightClick
    ret.endSnd = this.endSnd
    return ret
  }
}
