import { Nullable } from '../../../../liber/pervasive'
import {
  PhSZTypeOO,
  PhTypeOO,
  PhData,
  toPhSZType,
  toPhType,
  fromPhSZType,
  fromPhType,
} from '../../../io/dataType/spAttrs'
import { assignVal } from '../../../io/utils'
export interface PhOptions {
  idx?: string
  orient?: number
  sz?: PhSZTypeOO
  type?: PhTypeOO
}

export class Ph {
  hasCustomPrompt: boolean = false
  idx?: string
  orient?: number
  sz?: PhSZTypeOO
  type?: PhTypeOO
  static new(opts?: PhOptions): Ph {
    const ret = new Ph()
    if (opts != null) {
      ret.fromOptions(opts)
    }
    return ret
  }
  constructor() {
    this.idx = undefined
    this.orient = undefined
    this.sz = undefined
    this.type = undefined
  }

  fromOptions(opts: Nullable<PhOptions>) {
    if (opts == null) return
    if (opts.idx != null) this.idx = opts.idx
    if (opts.orient != null) this.orient = opts.orient
    if (opts.sz != null) this.sz = opts.sz
    if (opts.type != null) this.type = opts.type
  }

  clone(): Ph {
    const duplicate = new Ph()
    duplicate.hasCustomPrompt = this.hasCustomPrompt
    duplicate.idx = this.idx!
    duplicate.orient = this.orient
    duplicate.sz = this.sz
    duplicate.type = this.type
    return duplicate
  }

  toJSON(): PhData {
    const data: PhData = {}
    assignVal(data, 'hasCustomPrompt', this.hasCustomPrompt)
    assignVal(data, 'idx', this.idx)
    assignVal(data, 'orient', this.orient)
    if (this.sz != null) {
      assignVal(data, 'sz', toPhSZType(this.sz))
    }
    if (this.type != null) {
      assignVal(data, 'type', toPhType(this.type))
    }
    return data
  }

  fromJSON(data: PhData) {
    if (data['hasCustomPrompt'] != null) {
      this.hasCustomPrompt = data['hasCustomPrompt']
    }
    if (data['idx'] != null) {
      this.idx = '' + data['idx']
    }
    if (data['orient'] != null) {
      this.orient = data['orient']
    }
    if (data['sz'] != null) {
      this.sz = fromPhSZType(data['sz'])
    }
    if (data['type'] != null) {
      this.type = fromPhType(data['type'])
    }
  }
}
