import { TextListStyle } from '../../textAttributes/TextListStyle'

import { DefaultShapeAttributesData } from '../../../io/dataType/theme'
import { assignJSON, readFromJSON } from '../../../io/utils'
import { BodyPr } from './bodyPr'
import { SpPr } from './shapePrs'
import { ShapeStyle } from './ShapeStyle'

export class DefaultShapeAttributes {
  spPr: SpPr
  bodyPr: BodyPr
  lstStyle: TextListStyle
  style: ShapeStyle | null
  constructor() {
    this.spPr = new SpPr()
    this.bodyPr = new BodyPr()
    this.lstStyle = new TextListStyle()
    this.style = null
  }

  toJSON(): DefaultShapeAttributesData {
    const data: DefaultShapeAttributesData = {}

    assignJSON(data, 'bodyPr', this.bodyPr)
    assignJSON(data, 'spPr', this.spPr)
    assignJSON(data, 'lstStyle', this.lstStyle)
    assignJSON(data, 'style', this.style)

    return data
  }

  fromJSON(data: DefaultShapeAttributesData) {
    const spPr = readFromJSON(data['spPr'], SpPr)
    if (spPr != null) {
      this.spPr = spPr
    }

    const bodyPr = readFromJSON(data['bodyPr'], BodyPr)
    if (bodyPr != null) {
      this.bodyPr = bodyPr
    }

    const lstStyle = readFromJSON(data['lstStyle'], TextListStyle)
    if (lstStyle != null) {
      this.lstStyle = lstStyle
    }

    const style = readFromJSON(data['style'], ShapeStyle)
    if (style != null) {
      this.style = style
    }
  }

  clone() {
    const ret = new DefaultShapeAttributes()
    if (this.spPr) {
      ret.spPr = this.spPr.clone()
    }
    if (this.bodyPr) {
      ret.bodyPr = this.bodyPr.clone()
    }
    if (this.lstStyle) {
      ret.lstStyle = this.lstStyle.clone()
    }
    if (this.style) {
      ret.style = this.style.clone()
    }
    return ret
  }
}
