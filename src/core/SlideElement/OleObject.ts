import { ImageShape } from './Image'
import { Nullable } from '../../../liber/pervasive'
import { OLEObjectAttrs } from '../../io/dataType/csld'
import { assignVal, scaleValue } from '../../io/utils'
import { ScaleOfPPTXSizes } from '../common/const/unit'
import { Hooks, hookManager } from '../hooks'

export class OleObject extends ImageShape {
  progId: Nullable<string>
  defaultSizeX: Nullable<number>
  defaultSizeY: Nullable<number>
  objectName: Nullable<string>
  uri?: string
  encryptUrl?: string
  showAsIcon: boolean = true
  objectControl: Nullable<string> = 'Embed'

  constructor() {
    super()
  }

  isOleObject(): this is OleObject {
    return true
  }

  setApplicationId(applicationId) {
    this.progId = applicationId
  }

  setFileType(fileType?: string) {
    switch (fileType) {
      case 'doc':
        this.objectName = 'Document'
        this.progId = 'Word.Document.8'
        break
      case 'docx':
        this.objectName = 'Document'
        this.progId = 'Word.Document.12'
        break
      case 'xls':
        this.objectName = 'Worksheet'
        this.progId = 'Excel.Sheet.8'
        break
      case 'xlsx':
        this.objectName = 'Worksheet'
        this.progId = 'Excel.Sheet.12'
        break
      case 'ppt':
        this.objectName = 'Presentation'
        this.progId = 'PowerPoint.Show.8'
        break
      case 'pptx':
        this.objectName = 'Presentation'
        this.progId = 'PowerPoint.Show.12'
        break
      default:
        this.objectName = 'Packager Shell Object'
        this.progId = 'package'
    }
  }

  setUri(uri) {
    this.uri = uri
  }

  updateAsset(url: string) {
    this.setUri(url)
  }

  attrsToJSON(): OLEObjectAttrs {
    const data: OLEObjectAttrs = {}
    assignVal(data, 'link', this.uri)
    assignVal(data, 'encryptUrl', this.encryptUrl)
    const extra: OLEObjectAttrs['extra'] = {}
    assignVal(extra, 'img_w', scaleValue(this.defaultSizeX, ScaleOfPPTXSizes))
    assignVal(extra, 'img_h', scaleValue(this.defaultSizeY, ScaleOfPPTXSizes))
    assignVal(extra, 'name', this.objectName)
    assignVal(extra, 'ole_obj_control', this.objectControl)
    assignVal(extra, 'prog_id', this.progId)
    assignVal(extra, 'show_as_icon', this.showAsIcon)
    data['extra'] = extra
    return data
  }

  attrsFromJSON(data: OLEObjectAttrs) {
    if (data['link'] != null) {
      this.uri = data['link']
      hookManager.invoke(Hooks.Attrs.CollectAssetsForPasting, {
        assetUrl: this.uri,
        assetObject: this,
      })
    }

    if (data['encryptUrl'] != null) {
      this.encryptUrl = data['encryptUrl']
    }

    const extra = data['extra']
    if (extra != null) {
      if (extra['name'] != null) {
        this.objectName = extra['name']
      }
      if (extra['prog_id'] != null) {
        this.progId = extra['prog_id']
      }
      if (extra['show_as_icon'] != null) {
        this.showAsIcon = extra['show_as_icon']
      }
      if (extra['ole_obj_control'] != null) {
        this.objectControl = extra['ole_obj_control']
      }
    }
  }

  copyTo(target: OleObject) {
    target.defaultSizeY = this.defaultSizeY
    target.defaultSizeX = this.defaultSizeX
    target.objectName = this.objectName
    target.uri = this.uri
    if (target.uri) {
      hookManager.invoke(Hooks.Attrs.CollectAssetsForPasting, {
        assetUrl: target.uri,
        assetObject: target,
      })
    }
    target.progId = this.progId
    target.showAsIcon = this.showAsIcon
    target.objectControl = this.objectControl
  }

  isValid() {
    if (super.isValid() === false) {
      return false
    }

    if (!this.uri) {
      return false
    }

    if (this.defaultSizeX == null || this.defaultSizeY == null) {
      return false
    }
    return true
  }
}
