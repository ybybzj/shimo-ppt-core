import { Geometry, operateGeometry, setGeometryPrst } from './Geometry'
import { GeometryOperation, GeomAddOp, PathAddCmd } from './operations'

export function createGeometry(prst) {
  const geom = new Geometry()
  setGeometryPrst(geom, prst)
  return geom
}

export function createGeometryOfPrstTxWarp(prst) {
  const f = new Geometry()
  let ops: GeometryOperation[] = []
  switch (prst) {
    case 'textArchDown': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '0']],
        [GeomAddOp.Guide, ['adval', 10, '0', 'adj', '21599999']],
        [GeomAddOp.Guide, ['v1', 1, '10800000', '0', 'adval']],
        [GeomAddOp.Guide, ['v2', 1, '32400000', '0', 'adval']],
        [GeomAddOp.Guide, ['nv1', 1, '0', '0', 'v1']],
        [GeomAddOp.Guide, ['stAng', 3, 'nv1', 'v2', 'v1']],
        [GeomAddOp.Guide, ['w1', 1, '5400000', '0', 'adval']],
        [GeomAddOp.Guide, ['w2', 1, '16200000', '0', 'adval']],
        [GeomAddOp.Guide, ['d1', 1, 'adval', '0', 'stAng']],
        [GeomAddOp.Guide, ['d2', 1, 'd1', '0', '21600000']],
        [GeomAddOp.Guide, ['v3', 1, '0', '0', '10800000']],
        [GeomAddOp.Guide, ['c2', 3, 'w2', 'd1', 'd2']],
        [GeomAddOp.Guide, ['c1', 3, 'v1', 'd2', 'c2']],
        [GeomAddOp.Guide, ['c0', 3, 'w1', 'd1', 'c1']],
        [GeomAddOp.Guide, ['swAng', 3, 'stAng', 'c0', 'v3']],
        [GeomAddOp.Guide, ['wt1', 12, 'wd2', 'adj']],
        [GeomAddOp.Guide, ['ht1', 7, 'hd2', 'adj']],
        [GeomAddOp.Guide, ['dx1', 6, 'wd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['dy1', 11, 'hd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['x1', 1, 'hc', 'dx1', '0']],
        [GeomAddOp.Guide, ['y1', 1, 'vc', 'dy1', '0']],
        [GeomAddOp.Guide, ['wt2', 12, 'wd2', 'stAng']],
        [GeomAddOp.Guide, ['ht2', 7, 'hd2', 'stAng']],
        [GeomAddOp.Guide, ['dx2', 6, 'wd2', 'ht2', 'wt2']],
        [GeomAddOp.Guide, ['dy2', 11, 'hd2', 'ht2', 'wt2']],
        [GeomAddOp.Guide, ['x2', 1, 'hc', 'dx2', '0']],
        [GeomAddOp.Guide, ['y2', 1, 'vc', 'dy2', '0']],
        [
          GeomAddOp.HandlePolar,
          ['adj', '0', '21599999', undefined, '0', '0', 'x1', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x2', 'y2']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'hd2', 'stAng', 'swAng']],
      ]
      break
    }
    case 'textArchDownPour': {
      ops = [
        [GeomAddOp.Adj, ['adj1', 15, '0']],
        [GeomAddOp.Adj, ['adj2', 15, '25000']],
        [GeomAddOp.Guide, ['adval', 10, '0', 'adj1', '21599999']],
        [GeomAddOp.Guide, ['v1', 1, '10800000', '0', 'adval']],
        [GeomAddOp.Guide, ['v2', 1, '32400000', '0', 'adval']],
        [GeomAddOp.Guide, ['nv1', 1, '0', '0', 'v1']],
        [GeomAddOp.Guide, ['stAng', 3, 'nv1', 'v2', 'v1']],
        [GeomAddOp.Guide, ['w1', 1, '5400000', '0', 'adval']],
        [GeomAddOp.Guide, ['w2', 1, '16200000', '0', 'adval']],
        [GeomAddOp.Guide, ['d1', 1, 'adval', '0', 'stAng']],
        [GeomAddOp.Guide, ['d2', 1, 'd1', '0', '21600000']],
        [GeomAddOp.Guide, ['v3', 1, '0', '0', '10800000']],
        [GeomAddOp.Guide, ['c2', 3, 'w2', 'd1', 'd2']],
        [GeomAddOp.Guide, ['c1', 3, 'v1', 'd2', 'c2']],
        [GeomAddOp.Guide, ['c0', 3, 'w1', 'd1', 'c1']],
        [GeomAddOp.Guide, ['swAng', 3, 'stAng', 'c0', 'v3']],
        [GeomAddOp.Guide, ['wt1', 12, 'wd2', 'stAng']],
        [GeomAddOp.Guide, ['ht1', 7, 'hd2', 'stAng']],
        [GeomAddOp.Guide, ['dx1', 6, 'wd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['dy1', 11, 'hd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['x1', 1, 'hc', 'dx1', '0']],
        [GeomAddOp.Guide, ['y1', 1, 'vc', 'dy1', '0']],
        [GeomAddOp.Guide, ['adval2', 10, '0', 'adj2', '99000']],
        [GeomAddOp.Guide, ['ratio', 0, 'adval2', '1', '100000']],
        [GeomAddOp.Guide, ['iwd2', 0, 'wd2', 'ratio', '1']],
        [GeomAddOp.Guide, ['ihd2', 0, 'hd2', 'ratio', '1']],
        [GeomAddOp.Guide, ['wt2', 12, 'iwd2', 'adval']],
        [GeomAddOp.Guide, ['ht2', 7, 'ihd2', 'adval']],
        [GeomAddOp.Guide, ['dx2', 6, 'iwd2', 'ht2', 'wt2']],
        [GeomAddOp.Guide, ['dy2', 11, 'ihd2', 'ht2', 'wt2']],
        [GeomAddOp.Guide, ['x2', 1, 'hc', 'dx2', '0']],
        [GeomAddOp.Guide, ['y2', 1, 'vc', 'dy2', '0']],
        [GeomAddOp.Guide, ['wt3', 12, 'iwd2', 'stAng']],
        [GeomAddOp.Guide, ['ht3', 7, 'ihd2', 'stAng']],
        [GeomAddOp.Guide, ['dx3', 6, 'iwd2', 'ht3', 'wt3']],
        [GeomAddOp.Guide, ['dy3', 11, 'ihd2', 'ht3', 'wt3']],
        [GeomAddOp.Guide, ['x3', 1, 'hc', 'dx3', '0']],
        [GeomAddOp.Guide, ['y3', 1, 'vc', 'dy3', '0']],
        [
          GeomAddOp.HandlePolar,
          ['adj1', '0', '21599999', 'adj2', '0', '100000', 'x2', 'y2'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x3', 'y3']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'iwd2', 'ihd2', 'stAng', 'swAng']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x1', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'hd2', 'stAng', 'swAng']],
      ]
      break
    }
    case 'textArchUp': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, 'cd2']],
        [GeomAddOp.Guide, ['adval', 10, '0', 'adj', '21599999']],
        [GeomAddOp.Guide, ['v1', 1, '10800000', '0', 'adval']],
        [GeomAddOp.Guide, ['v2', 1, '32400000', '0', 'adval']],
        [GeomAddOp.Guide, ['end', 3, 'v1', 'v1', 'v2']],
        [GeomAddOp.Guide, ['w1', 1, '5400000', '0', 'adval']],
        [GeomAddOp.Guide, ['w2', 1, '16200000', '0', 'adval']],
        [GeomAddOp.Guide, ['d1', 1, 'end', '0', 'adval']],
        [GeomAddOp.Guide, ['d2', 1, '21600000', 'd1', '0']],
        [GeomAddOp.Guide, ['c2', 3, 'w2', 'd1', 'd2']],
        [GeomAddOp.Guide, ['c1', 3, 'v1', 'd2', 'c2']],
        [GeomAddOp.Guide, ['swAng', 3, 'w1', 'd1', 'c1']],
        [GeomAddOp.Guide, ['wt1', 12, 'wd2', 'adj']],
        [GeomAddOp.Guide, ['ht1', 7, 'hd2', 'adj']],
        [GeomAddOp.Guide, ['dx1', 6, 'wd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['dy1', 11, 'hd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['x1', 1, 'hc', 'dx1', '0']],
        [GeomAddOp.Guide, ['y1', 1, 'vc', 'dy1', '0']],
        [
          GeomAddOp.HandlePolar,
          ['adj', '0', '21599999', undefined, '0', '0', 'x1', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x1', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'hd2', 'adval', 'swAng']],
      ]
      break
    }
    case 'textArchUpPour': {
      ops = [
        [GeomAddOp.Adj, ['adj1', 15, 'cd2']],
        [GeomAddOp.Adj, ['adj2', 15, '50000']],
        [GeomAddOp.Guide, ['adval', 10, '0', 'adj1', '21599999']],
        [GeomAddOp.Guide, ['v1', 1, '10800000', '0', 'adval']],
        [GeomAddOp.Guide, ['v2', 1, '32400000', '0', 'adval']],
        [GeomAddOp.Guide, ['end', 3, 'v1', 'v1', 'v2']],
        [GeomAddOp.Guide, ['w1', 1, '5400000', '0', 'adval']],
        [GeomAddOp.Guide, ['w2', 1, '16200000', '0', 'adval']],
        [GeomAddOp.Guide, ['d1', 1, 'end', '0', 'adval']],
        [GeomAddOp.Guide, ['d2', 1, '21600000', 'd1', '0']],
        [GeomAddOp.Guide, ['c2', 3, 'w2', 'd1', 'd2']],
        [GeomAddOp.Guide, ['c1', 3, 'v1', 'd2', 'c2']],
        [GeomAddOp.Guide, ['swAng', 3, 'w1', 'd1', 'c1']],
        [GeomAddOp.Guide, ['wt1', 12, 'wd2', 'adval']],
        [GeomAddOp.Guide, ['ht1', 7, 'hd2', 'adval']],
        [GeomAddOp.Guide, ['dx1', 6, 'wd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['dy1', 11, 'hd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['x1', 1, 'hc', 'dx1', '0']],
        [GeomAddOp.Guide, ['y1', 1, 'vc', 'dy1', '0']],
        [GeomAddOp.Guide, ['adval2', 10, '0', 'adj2', '99000']],
        [GeomAddOp.Guide, ['ratio', 0, 'adval2', '1', '100000']],
        [GeomAddOp.Guide, ['iwd2', 0, 'wd2', 'ratio', '1']],
        [GeomAddOp.Guide, ['ihd2', 0, 'hd2', 'ratio', '1']],
        [GeomAddOp.Guide, ['wt2', 12, 'iwd2', 'adval']],
        [GeomAddOp.Guide, ['ht2', 7, 'ihd2', 'adval']],
        [GeomAddOp.Guide, ['dx2', 6, 'iwd2', 'ht2', 'wt2']],
        [GeomAddOp.Guide, ['dy2', 11, 'ihd2', 'ht2', 'wt2']],
        [GeomAddOp.Guide, ['x2', 1, 'hc', 'dx2', '0']],
        [GeomAddOp.Guide, ['y2', 1, 'vc', 'dy2', '0']],
        [
          GeomAddOp.HandlePolar,
          ['adj1', '0', '21599999', 'adj2', '0', '100000', 'x2', 'y2'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x1', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'hd2', 'adval', 'swAng']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x2', 'y2']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'iwd2', 'ihd2', 'adval', 'swAng']],
      ]
      break
    }
    case 'textButton': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '10800000']],
        [GeomAddOp.Guide, ['adval', 10, '0', 'adj', '21599999']],
        [GeomAddOp.Guide, ['bot', 1, '5400000', '0', 'adval']],
        [GeomAddOp.Guide, ['lef', 1, '10800000', '0', 'adval']],
        [GeomAddOp.Guide, ['top', 1, '16200000', '0', 'adval']],
        [GeomAddOp.Guide, ['rig', 1, '21600000', '0', 'adval']],
        [GeomAddOp.Guide, ['c3', 3, 'top', 'adval', '0']],
        [GeomAddOp.Guide, ['c2', 3, 'lef', '10800000', 'c3']],
        [GeomAddOp.Guide, ['c1', 3, 'bot', 'rig', 'c2']],
        [GeomAddOp.Guide, ['stAng', 3, 'adval', 'c1', '0']],
        [GeomAddOp.Guide, ['w1', 1, '21600000', '0', 'stAng']],
        [GeomAddOp.Guide, ['stAngB', 3, 'stAng', 'w1', '0']],
        [GeomAddOp.Guide, ['td1', 0, 'bot', '2', '1']],
        [GeomAddOp.Guide, ['td2', 0, 'top', '2', '1']],
        [GeomAddOp.Guide, ['ntd2', 1, '0', '0', 'td2']],
        [GeomAddOp.Guide, ['w2', 1, '0', '0', '10800000']],
        [GeomAddOp.Guide, ['c6', 3, 'top', 'ntd2', 'w2']],
        [GeomAddOp.Guide, ['c5', 3, 'lef', '10800000', 'c6']],
        [GeomAddOp.Guide, ['c4', 3, 'bot', 'td1', 'c5']],
        [GeomAddOp.Guide, ['v1', 3, 'adval', 'c4', '10800000']],
        [GeomAddOp.Guide, ['swAngT', 1, '0', '0', 'v1']],
        [GeomAddOp.Guide, ['stT', 3, 'lef', 'stAngB', 'stAng']],
        [GeomAddOp.Guide, ['stB', 3, 'lef', 'stAng', 'stAngB']],
        [GeomAddOp.Guide, ['swT', 3, 'lef', 'v1', 'swAngT']],
        [GeomAddOp.Guide, ['swB', 3, 'lef', 'swAngT', 'v1']],
        [GeomAddOp.Guide, ['wt1', 12, 'wd2', 'stT']],
        [GeomAddOp.Guide, ['ht1', 7, 'hd2', 'stT']],
        [GeomAddOp.Guide, ['dx1', 6, 'wd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['dy1', 11, 'hd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['x1', 1, 'hc', 'dx1', '0']],
        [GeomAddOp.Guide, ['y1', 1, 'vc', 'dy1', '0']],
        [GeomAddOp.Guide, ['wt2', 12, 'wd2', 'stB']],
        [GeomAddOp.Guide, ['ht2', 7, 'hd2', 'stB']],
        [GeomAddOp.Guide, ['dx2', 6, 'wd2', 'ht2', 'wt2']],
        [GeomAddOp.Guide, ['dy2', 11, 'hd2', 'ht2', 'wt2']],
        [GeomAddOp.Guide, ['x2', 1, 'hc', 'dx2', '0']],
        [GeomAddOp.Guide, ['y2', 1, 'vc', 'dy2', '0']],
        [GeomAddOp.Guide, ['wt3', 12, 'wd2', 'adj']],
        [GeomAddOp.Guide, ['ht3', 7, 'hd2', 'adj']],
        [GeomAddOp.Guide, ['dx3', 6, 'wd2', 'ht3', 'wt3']],
        [GeomAddOp.Guide, ['dy3', 11, 'hd2', 'ht3', 'wt3']],
        [GeomAddOp.Guide, ['x3', 1, 'hc', 'dx3', '0']],
        [GeomAddOp.Guide, ['y3', 1, 'vc', 'dy3', '0']],
        [
          GeomAddOp.HandlePolar,
          ['adj', '0', '21599999', undefined, '0', '0', 'x3', 'y3'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x1', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'hd2', 'stT', 'swT']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'vc']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'vc']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x2', 'y2']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'hd2', 'stB', 'swB']],
      ]
      break
    }
    case 'textButtonPour': {
      ops = [
        [GeomAddOp.Adj, ['adj1', 15, 'cd2']],
        [GeomAddOp.Adj, ['adj2', 15, '50000']],
        [GeomAddOp.Guide, ['adval', 10, '0', 'adj1', '21599999']],
        [GeomAddOp.Guide, ['bot', 1, '5400000', '0', 'adval']],
        [GeomAddOp.Guide, ['lef', 1, '10800000', '0', 'adval']],
        [GeomAddOp.Guide, ['top', 1, '16200000', '0', 'adval']],
        [GeomAddOp.Guide, ['rig', 1, '21600000', '0', 'adval']],
        [GeomAddOp.Guide, ['c3', 3, 'top', 'adval', '0']],
        [GeomAddOp.Guide, ['c2', 3, 'lef', '10800000', 'c3']],
        [GeomAddOp.Guide, ['c1', 3, 'bot', 'rig', 'c2']],
        [GeomAddOp.Guide, ['stAng', 3, 'adval', 'c1', '0']],
        [GeomAddOp.Guide, ['w1', 1, '21600000', '0', 'stAng']],
        [GeomAddOp.Guide, ['stAngB', 3, 'stAng', 'w1', '0']],
        [GeomAddOp.Guide, ['td1', 0, 'bot', '2', '1']],
        [GeomAddOp.Guide, ['td2', 0, 'top', '2', '1']],
        [GeomAddOp.Guide, ['ntd2', 1, '0', '0', 'td2']],
        [GeomAddOp.Guide, ['w2', 1, '0', '0', '10800000']],
        [GeomAddOp.Guide, ['c6', 3, 'top', 'ntd2', 'w2']],
        [GeomAddOp.Guide, ['c5', 3, 'lef', '10800000', 'c6']],
        [GeomAddOp.Guide, ['c4', 3, 'bot', 'td1', 'c5']],
        [GeomAddOp.Guide, ['v1', 3, 'adval', 'c4', '10800000']],
        [GeomAddOp.Guide, ['swAngT', 1, '0', '0', 'v1']],
        [GeomAddOp.Guide, ['stT', 3, 'lef', 'stAngB', 'stAng']],
        [GeomAddOp.Guide, ['stB', 3, 'lef', 'stAng', 'stAngB']],
        [GeomAddOp.Guide, ['swT', 3, 'lef', 'v1', 'swAngT']],
        [GeomAddOp.Guide, ['swB', 3, 'lef', 'swAngT', 'v1']],
        [GeomAddOp.Guide, ['wt1', 12, 'wd2', 'stT']],
        [GeomAddOp.Guide, ['ht1', 7, 'hd2', 'stT']],
        [GeomAddOp.Guide, ['dx1', 6, 'wd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['dy1', 11, 'hd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['x1', 1, 'hc', 'dx1', '0']],
        [GeomAddOp.Guide, ['y1', 1, 'vc', 'dy1', '0']],
        [GeomAddOp.Guide, ['wt6', 12, 'wd2', 'stB']],
        [GeomAddOp.Guide, ['ht6', 7, 'hd2', 'stB']],
        [GeomAddOp.Guide, ['dx6', 6, 'wd2', 'ht6', 'wt6']],
        [GeomAddOp.Guide, ['dy6', 11, 'hd2', 'ht6', 'wt6']],
        [GeomAddOp.Guide, ['x6', 1, 'hc', 'dx6', '0']],
        [GeomAddOp.Guide, ['y6', 1, 'vc', 'dy6', '0']],
        [GeomAddOp.Guide, ['adval2', 10, '40000', 'adj2', '99000']],
        [GeomAddOp.Guide, ['ratio', 0, 'adval2', '1', '100000']],
        [GeomAddOp.Guide, ['iwd2', 0, 'wd2', 'ratio', '1']],
        [GeomAddOp.Guide, ['ihd2', 0, 'hd2', 'ratio', '1']],
        [GeomAddOp.Guide, ['wt2', 12, 'iwd2', 'stT']],
        [GeomAddOp.Guide, ['ht2', 7, 'ihd2', 'stT']],
        [GeomAddOp.Guide, ['dx2', 6, 'iwd2', 'ht2', 'wt2']],
        [GeomAddOp.Guide, ['dy2', 11, 'ihd2', 'ht2', 'wt2']],
        [GeomAddOp.Guide, ['x2', 1, 'hc', 'dx2', '0']],
        [GeomAddOp.Guide, ['y2', 1, 'vc', 'dy2', '0']],
        [GeomAddOp.Guide, ['wt5', 12, 'iwd2', 'stB']],
        [GeomAddOp.Guide, ['ht5', 7, 'ihd2', 'stB']],
        [GeomAddOp.Guide, ['dx5', 6, 'iwd2', 'ht5', 'wt5']],
        [GeomAddOp.Guide, ['dy5', 11, 'ihd2', 'ht5', 'wt5']],
        [GeomAddOp.Guide, ['x5', 1, 'hc', 'dx5', '0']],
        [GeomAddOp.Guide, ['y5', 1, 'vc', 'dy5', '0']],
        [GeomAddOp.Guide, ['d1', 1, 'hd2', '0', 'ihd2']],
        [GeomAddOp.Guide, ['d12', 0, 'd1', '1', '2']],
        [GeomAddOp.Guide, ['yu', 1, 'vc', '0', 'd12']],
        [GeomAddOp.Guide, ['yd', 1, 'vc', 'd12', '0']],
        [GeomAddOp.Guide, ['v1', 0, 'd12', 'd12', '1']],
        [GeomAddOp.Guide, ['v2', 0, 'ihd2', 'ihd2', '1']],
        [GeomAddOp.Guide, ['v3', 0, 'v1', '1', 'v2']],
        [GeomAddOp.Guide, ['v4', 1, '1', '0', 'v3']],
        [GeomAddOp.Guide, ['v5', 0, 'iwd2', 'iwd2', '1']],
        [GeomAddOp.Guide, ['v6', 0, 'v4', 'v5', '1']],
        [GeomAddOp.Guide, ['v7', 13, 'v6']],
        [GeomAddOp.Guide, ['xl', 1, 'hc', '0', 'v7']],
        [GeomAddOp.Guide, ['xr', 1, 'hc', 'v7', '0']],
        [GeomAddOp.Guide, ['wtadj', 12, 'iwd2', 'adj1']],
        [GeomAddOp.Guide, ['htadj', 7, 'ihd2', 'adj1']],
        [GeomAddOp.Guide, ['dxadj', 6, 'iwd2', 'htadj', 'wtadj']],
        [GeomAddOp.Guide, ['dyadj', 11, 'ihd2', 'htadj', 'wtadj']],
        [GeomAddOp.Guide, ['xadj', 1, 'hc', 'dxadj', '0']],
        [GeomAddOp.Guide, ['yadj', 1, 'vc', 'dyadj', '0']],
        [
          GeomAddOp.HandlePolar,
          ['adj1', '0', '21599999', 'adj2', '0', '100000', 'xadj', 'yadj'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x1', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'hd2', 'stT', 'swT']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x2', 'y2']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'iwd2', 'ihd2', 'stT', 'swT']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'xl', 'yu']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'xr', 'yu']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'xl', 'yd']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'xr', 'yd']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x5', 'y5']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'iwd2', 'ihd2', 'stB', 'swB']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x6', 'y6']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'hd2', 'stB', 'swB']],
      ]
      break
    }
    case 'textCanDown': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '14286']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '33333']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['y0', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['y1', 1, 'b', '0', 'dy']],
        [GeomAddOp.Guide, ['ncd2', 0, 'cd2', '-1', '1']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '0', '33333', 'hc', 'y0'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'y0', 'cd2', 'ncd2']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'y0', 'cd2', 'ncd2']],
      ]
      break
    }
    case 'textCanUp': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '85714']],
        [GeomAddOp.Guide, ['a', 10, '66667', 'adj', '100000']],
        [GeomAddOp.Guide, ['dy1', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['dy', 1, 'h', '0', 'dy1']],
        [GeomAddOp.Guide, ['y0', 1, 't', 'dy1', '0']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'dy', '0']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '66667', '100000', 'hc', 'y0'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'y1', 'cd2', 'cd2']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'b']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'y1', 'cd2', 'cd2']],
      ]
      break
    }
    case 'textCascadeDown': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '44444']],
        [GeomAddOp.Guide, ['a', 10, '28570', 'adj', '100000']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['dy2', 1, 'h', '0', 'dy']],
        [GeomAddOp.Guide, ['dy3', 0, 'dy2', '1', '4']],
        [GeomAddOp.Guide, ['y2', 1, 't', 'dy3', '0']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '28570', '100000', 'l', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'y2']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'b']],
      ]
      break
    }
    case 'textCascadeUp': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '44444']],
        [GeomAddOp.Guide, ['a', 10, '28570', 'adj', '100000']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['dy2', 1, 'h', '0', 'dy']],
        [GeomAddOp.Guide, ['dy3', 0, 'dy2', '1', '4']],
        [GeomAddOp.Guide, ['y2', 1, 't', 'dy3', '0']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '28570', '100000', 'r', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y2']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'y1']],
      ]
      break
    }
    case 'textChevron': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '25000']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '50000']],
        [GeomAddOp.Guide, ['y', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'b', 'y']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '0', '50000', 'l', 'y'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'hc', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'y']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'hc', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'b']],
      ]
      break
    }
    case 'textChevronInverted': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '75000']],
        [GeomAddOp.Guide, ['a', 10, '50000', 'adj', '100000']],
        [GeomAddOp.Guide, ['y', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['y1', 1, 'b', '0', 'y']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '50000', '100000', 'l', 'y'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'hc', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'hc', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'y']],
      ]
      break
    }
    case 'textCircle': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '10800000']],
        [GeomAddOp.Guide, ['adval', 10, '0', 'adj', '21599999']],
        [GeomAddOp.Guide, ['d0', 1, 'adval', '0', '10800000']],
        [GeomAddOp.Guide, ['d1', 1, '10800000', '0', 'adval']],
        [GeomAddOp.Guide, ['d2', 1, '21600000', '0', 'adval']],
        [GeomAddOp.Guide, ['d3', 3, 'd1', 'd1', '10799999']],
        [GeomAddOp.Guide, ['d4', 3, 'd0', 'd2', 'd3']],
        [GeomAddOp.Guide, ['swAng', 0, 'd4', '2', '1']],
        [GeomAddOp.Guide, ['wt1', 12, 'wd2', 'adj']],
        [GeomAddOp.Guide, ['ht1', 7, 'hd2', 'adj']],
        [GeomAddOp.Guide, ['dx1', 6, 'wd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['dy1', 11, 'hd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['x1', 1, 'hc', 'dx1', '0']],
        [GeomAddOp.Guide, ['y1', 1, 'vc', 'dy1', '0']],
        [
          GeomAddOp.HandlePolar,
          ['adj', '0', '21599999', undefined, '0', '0', 'x1', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x1', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'hd2', 'adval', 'swAng']],
      ]
      break
    }
    case 'textCirclePour': {
      ops = [
        [GeomAddOp.Adj, ['adj1', 15, 'cd2']],
        [GeomAddOp.Adj, ['adj2', 15, '50000']],
        [GeomAddOp.Guide, ['adval', 10, '0', 'adj1', '21599999']],
        [GeomAddOp.Guide, ['d0', 1, 'adval', '0', '10800000']],
        [GeomAddOp.Guide, ['d1', 1, '10800000', '0', 'adval']],
        [GeomAddOp.Guide, ['d2', 1, '21600000', '0', 'adval']],
        [GeomAddOp.Guide, ['d3', 3, 'd1', 'd1', '10799999']],
        [GeomAddOp.Guide, ['d4', 3, 'd0', 'd2', 'd3']],
        [GeomAddOp.Guide, ['swAng', 0, 'd4', '2', '1']],
        [GeomAddOp.Guide, ['wt1', 12, 'wd2', 'adval']],
        [GeomAddOp.Guide, ['ht1', 7, 'hd2', 'adval']],
        [GeomAddOp.Guide, ['dx1', 6, 'wd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['dy1', 11, 'hd2', 'ht1', 'wt1']],
        [GeomAddOp.Guide, ['x1', 1, 'hc', 'dx1', '0']],
        [GeomAddOp.Guide, ['y1', 1, 'vc', 'dy1', '0']],
        [GeomAddOp.Guide, ['adval2', 10, '0', 'adj2', '99000']],
        [GeomAddOp.Guide, ['ratio', 0, 'adval2', '1', '100000']],
        [GeomAddOp.Guide, ['iwd2', 0, 'wd2', 'ratio', '1']],
        [GeomAddOp.Guide, ['ihd2', 0, 'hd2', 'ratio', '1']],
        [GeomAddOp.Guide, ['wt2', 12, 'iwd2', 'adval']],
        [GeomAddOp.Guide, ['ht2', 7, 'ihd2', 'adval']],
        [GeomAddOp.Guide, ['dx2', 6, 'iwd2', 'ht2', 'wt2']],
        [GeomAddOp.Guide, ['dy2', 11, 'ihd2', 'ht2', 'wt2']],
        [GeomAddOp.Guide, ['x2', 1, 'hc', 'dx2', '0']],
        [GeomAddOp.Guide, ['y2', 1, 'vc', 'dy2', '0']],
        [
          GeomAddOp.HandlePolar,
          ['adj1', '0', '21599999', 'adj2', '0', '100000', 'x2', 'y2'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x1', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'wd2', 'hd2', 'adval', 'swAng']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x2', 'y2']],
        [GeomAddOp.Path, [PathAddCmd.arcTo, 'iwd2', 'ihd2', 'adval', 'swAng']],
      ]
      break
    }
    case 'textCurveDown': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '45977']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '56338']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['gd1', 0, 'dy', '3', '4']],
        [GeomAddOp.Guide, ['gd2', 0, 'dy', '5', '4']],
        [GeomAddOp.Guide, ['gd3', 0, 'dy', '3', '8']],
        [GeomAddOp.Guide, ['gd4', 0, 'dy', '1', '8']],
        [GeomAddOp.Guide, ['gd5', 1, 'h', '0', 'gd3']],
        [GeomAddOp.Guide, ['gd6', 1, 'gd4', 'h', '0']],
        [GeomAddOp.Guide, ['y0', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'gd1', '0']],
        [GeomAddOp.Guide, ['y2', 1, 't', 'gd2', '0']],
        [GeomAddOp.Guide, ['y3', 1, 't', 'gd3', '0']],
        [GeomAddOp.Guide, ['y4', 1, 't', 'gd4', '0']],
        [GeomAddOp.Guide, ['y5', 1, 't', 'gd5', '0']],
        [GeomAddOp.Guide, ['y6', 1, 't', 'gd6', '0']],
        [GeomAddOp.Guide, ['x1', 1, 'l', 'wd3', '0']],
        [GeomAddOp.Guide, ['x2', 1, 'r', '0', 'wd3']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '0', '56338', 'r', 'y0'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x1', 'y1', 'x2', 'y2', 'r', 'y0'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y5']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x1', 'y6', 'x2', 'y6', 'r', 'y5'],
        ],
      ]
      break
    }
    case 'textCurveUp': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '45977']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '56338']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['gd1', 0, 'dy', '3', '4']],
        [GeomAddOp.Guide, ['gd2', 0, 'dy', '5', '4']],
        [GeomAddOp.Guide, ['gd3', 0, 'dy', '3', '8']],
        [GeomAddOp.Guide, ['gd4', 0, 'dy', '1', '8']],
        [GeomAddOp.Guide, ['gd5', 1, 'h', '0', 'gd3']],
        [GeomAddOp.Guide, ['gd6', 1, 'gd4', 'h', '0']],
        [GeomAddOp.Guide, ['y0', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'gd1', '0']],
        [GeomAddOp.Guide, ['y2', 1, 't', 'gd2', '0']],
        [GeomAddOp.Guide, ['y3', 1, 't', 'gd3', '0']],
        [GeomAddOp.Guide, ['y4', 1, 't', 'gd4', '0']],
        [GeomAddOp.Guide, ['y5', 1, 't', 'gd5', '0']],
        [GeomAddOp.Guide, ['y6', 1, 't', 'gd6', '0']],
        [GeomAddOp.Guide, ['x1', 1, 'l', 'wd3', '0']],
        [GeomAddOp.Guide, ['x2', 1, 'r', '0', 'wd3']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '0', '56338', 'l', 'y0'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y0']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x1', 'y2', 'x2', 'y1', 'r', 't'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y5']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x1', 'y6', 'x2', 'y6', 'r', 'y5'],
        ],
      ]
      break
    }
    case 'textDeflate': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '18750']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '37500']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'ss', '100000']],
        [GeomAddOp.Guide, ['gd0', 0, 'dy', '4', '3']],
        [GeomAddOp.Guide, ['gd1', 1, 'h', '0', 'gd0']],
        [GeomAddOp.Guide, ['adjY', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['y0', 1, 't', 'gd0', '0']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'gd1', '0']],
        [GeomAddOp.Guide, ['x0', 1, 'l', 'wd3', '0']],
        [GeomAddOp.Guide, ['x1', 1, 'r', '0', 'wd3']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '0', '37500', 'hc', 'adjY'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x0', 'y0', 'x1', 'y0', 'r', 't'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'b']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x0', 'y1', 'x1', 'y1', 'r', 'b'],
        ],
      ]
      break
    }
    case 'textDeflateBottom': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '50000']],
        [GeomAddOp.Guide, ['a', 10, '6250', 'adj', '100000']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'ss', '100000']],
        [GeomAddOp.Guide, ['dy2', 1, 'h', '0', 'dy']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['cp', 1, 'y1', '0', 'dy2']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '6250', '100000', 'hc', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'b']],
        [GeomAddOp.Path, [PathAddCmd.quadBezTo, 'hc', 'cp', 'r', 'b']],
      ]
      break
    }
    case 'textDeflateInflate': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '35000']],
        [GeomAddOp.Guide, ['a', 10, '5000', 'adj', '95000']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['del', 0, 'h', '5', '100']],
        [GeomAddOp.Guide, ['dh1', 0, 'h', '45', '100']],
        [GeomAddOp.Guide, ['dh2', 0, 'h', '55', '100']],
        [GeomAddOp.Guide, ['yh', 1, 'dy', '0', 'del']],
        [GeomAddOp.Guide, ['yl', 1, 'dy', 'del', '0']],
        [GeomAddOp.Guide, ['y3', 1, 'yh', 'yh', 'dh1']],
        [GeomAddOp.Guide, ['y4', 1, 'yl', 'yl', 'dh2']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '5000', '95000', 'hc', 'dy'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'dh1']],
        [GeomAddOp.Path, [PathAddCmd.quadBezTo, 'hc', 'y3', 'r', 'dh1']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'dh2']],
        [GeomAddOp.Path, [PathAddCmd.quadBezTo, 'hc', 'y4', 'r', 'dh2']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'b']],
      ]
      break
    }
    case 'textDeflateInflateDeflate': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '25000']],
        [GeomAddOp.Guide, ['a', 10, '3000', 'adj', '47000']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['del', 0, 'h', '3', '100']],
        [GeomAddOp.Guide, ['ey1', 0, 'h', '30', '100']],
        [GeomAddOp.Guide, ['ey2', 0, 'h', '36', '100']],
        [GeomAddOp.Guide, ['ey3', 0, 'h', '63', '100']],
        [GeomAddOp.Guide, ['ey4', 0, 'h', '70', '100']],
        [GeomAddOp.Guide, ['by', 1, 'b', '0', 'dy']],
        [GeomAddOp.Guide, ['yh1', 1, 'dy', '0', 'del']],
        [GeomAddOp.Guide, ['yl1', 1, 'dy', 'del', '0']],
        [GeomAddOp.Guide, ['yh2', 1, 'by', '0', 'del']],
        [GeomAddOp.Guide, ['yl2', 1, 'by', 'del', '0']],
        [GeomAddOp.Guide, ['y1', 1, 'yh1', 'yh1', 'ey1']],
        [GeomAddOp.Guide, ['y2', 1, 'yl1', 'yl1', 'ey2']],
        [GeomAddOp.Guide, ['y3', 1, 'yh2', 'yh2', 'ey3']],
        [GeomAddOp.Guide, ['y4', 1, 'yl2', 'yl2', 'ey4']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '3000', '47000', 'hc', 'dy'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'ey1']],
        [GeomAddOp.Path, [PathAddCmd.quadBezTo, 'hc', 'y1', 'r', 'ey1']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'ey2']],
        [GeomAddOp.Path, [PathAddCmd.quadBezTo, 'hc', 'y2', 'r', 'ey2']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'ey3']],
        [GeomAddOp.Path, [PathAddCmd.quadBezTo, 'hc', 'y3', 'r', 'ey3']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'ey4']],
        [GeomAddOp.Path, [PathAddCmd.quadBezTo, 'hc', 'y4', 'r', 'ey4']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'b']],
      ]
      break
    }
    case 'textDeflateTop': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '50000']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '93750']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['cp', 1, 'y1', 'dy', '0']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '0', '93750', 'hc', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [GeomAddOp.Path, [PathAddCmd.quadBezTo, 'hc', 'cp', 'r', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'b']],
      ]
      break
    }
    case 'textDoubleWave1': {
      ops = [
        [GeomAddOp.Adj, ['adj1', 15, '6250']],
        [GeomAddOp.Adj, ['adj2', 15, '0']],
        [GeomAddOp.Guide, ['a1', 10, '0', 'adj1', '12500']],
        [GeomAddOp.Guide, ['a2', 10, '-10000', 'adj2', '10000']],
        [GeomAddOp.Guide, ['y1', 0, 'h', 'a1', '100000']],
        [GeomAddOp.Guide, ['dy2', 0, 'y1', '10', '3']],
        [GeomAddOp.Guide, ['y2', 1, 'y1', '0', 'dy2']],
        [GeomAddOp.Guide, ['y3', 1, 'y1', 'dy2', '0']],
        [GeomAddOp.Guide, ['y4', 1, 'b', '0', 'y1']],
        [GeomAddOp.Guide, ['y5', 1, 'y4', '0', 'dy2']],
        [GeomAddOp.Guide, ['y6', 1, 'y4', 'dy2', '0']],
        [GeomAddOp.Guide, ['of', 0, 'w', 'a2', '100000']],
        [GeomAddOp.Guide, ['of2', 0, 'w', 'a2', '50000']],
        [GeomAddOp.Guide, ['x1', 4, 'of']],
        [GeomAddOp.Guide, ['dx2', 3, 'of2', '0', 'of2']],
        [GeomAddOp.Guide, ['x2', 1, 'l', '0', 'dx2']],
        [GeomAddOp.Guide, ['dx8', 3, 'of2', 'of2', '0']],
        [GeomAddOp.Guide, ['x8', 1, 'r', '0', 'dx8']],
        [GeomAddOp.Guide, ['dx3', 2, 'dx2', 'x8', '6']],
        [GeomAddOp.Guide, ['x3', 1, 'x2', 'dx3', '0']],
        [GeomAddOp.Guide, ['dx4', 2, 'dx2', 'x8', '3']],
        [GeomAddOp.Guide, ['x4', 1, 'x2', 'dx4', '0']],
        [GeomAddOp.Guide, ['x5', 2, 'x2', 'x8', '2']],
        [GeomAddOp.Guide, ['x6', 1, 'x5', 'dx3', '0']],
        [GeomAddOp.Guide, ['x7', 2, 'x6', 'x8', '2']],
        [GeomAddOp.Guide, ['x9', 1, 'l', 'dx8', '0']],
        [GeomAddOp.Guide, ['x15', 1, 'r', 'dx2', '0']],
        [GeomAddOp.Guide, ['x10', 1, 'x9', 'dx3', '0']],
        [GeomAddOp.Guide, ['x11', 1, 'x9', 'dx4', '0']],
        [GeomAddOp.Guide, ['x12', 2, 'x9', 'x15', '2']],
        [GeomAddOp.Guide, ['x13', 1, 'x12', 'dx3', '0']],
        [GeomAddOp.Guide, ['x14', 2, 'x13', 'x15', '2']],
        [GeomAddOp.Guide, ['x16', 1, 'r', '0', 'x1']],
        [GeomAddOp.Guide, ['xAdj', 1, 'hc', 'of', '0']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj1', '0', '12500', 'l', 'y1'],
        ],
        [
          GeomAddOp.HandleXY,
          ['adj2', '-10000', '10000', undefined, '0', '0', 'xAdj', 'b'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x2', 'y1']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x3', 'y2', 'x4', 'y3', 'x5', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x6', 'y2', 'x7', 'y3', 'x8', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x9', 'y4']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x10', 'y5', 'x11', 'y6', 'x12', 'y4'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x13', 'y5', 'x14', 'y6', 'x15', 'y4'],
        ],
      ]
      break
    }
    case 'textFadeDown': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '33333']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '49999']],
        [GeomAddOp.Guide, ['dx', 0, 'a', 'w', '100000']],
        [GeomAddOp.Guide, ['x1', 1, 'l', 'dx', '0']],
        [GeomAddOp.Guide, ['x2', 1, 'r', '0', 'dx']],
        [
          GeomAddOp.HandleXY,
          ['adj', '0', '49999', undefined, '0', '0', 'x1', 'b'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x1', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'x2', 'b']],
      ]
      break
    }
    case 'textFadeLeft': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '33333']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '49999']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['y2', 1, 'b', '0', 'dy']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '0', '49999', 'l', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y2']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'b']],
      ]
      break
    }
    case 'textFadeRight': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '33333']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '49999']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['y2', 1, 'b', '0', 'dy']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '0', '49999', 'r', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'y1']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'y2']],
      ]
      break
    }
    case 'textFadeUp': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '33333']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '49999']],
        [GeomAddOp.Guide, ['dx', 0, 'a', 'w', '100000']],
        [GeomAddOp.Guide, ['x1', 1, 'l', 'dx', '0']],
        [GeomAddOp.Guide, ['x2', 1, 'r', '0', 'dx']],
        [
          GeomAddOp.HandleXY,
          ['adj', '0', '49999', undefined, '0', '0', 'x1', 't'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x1', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'x2', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'b']],
      ]
      break
    }
    case 'textInflate': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '18750']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '20000']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['gd', 0, 'dy', '1', '3']],
        [GeomAddOp.Guide, ['gd0', 1, '0', '0', 'gd']],
        [GeomAddOp.Guide, ['gd1', 1, 'h', '0', 'gd0']],
        [GeomAddOp.Guide, ['ty', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['by', 1, 'b', '0', 'dy']],
        [GeomAddOp.Guide, ['y0', 1, 't', 'gd0', '0']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'gd1', '0']],
        [GeomAddOp.Guide, ['x0', 1, 'l', 'wd3', '0']],
        [GeomAddOp.Guide, ['x1', 1, 'r', '0', 'wd3']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '0', '20000', 'l', 'ty'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'ty']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x0', 'y0', 'x1', 'y0', 'r', 'ty'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'by']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x0', 'y1', 'x1', 'y1', 'r', 'by'],
        ],
      ]
      break
    }
    case 'textInflateBottom': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '60000']],
        [GeomAddOp.Guide, ['a', 10, '60000', 'adj', '100000']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['ty', 1, 't', 'dy', '0']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '60000', '100000', 'l', 'ty'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'ty']],
        [GeomAddOp.Path, [PathAddCmd.quadBezTo, 'hc', 'b', 'r', 'ty']],
      ]
      break
    }
    case 'textInflateTop': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '40000']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '50000']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['ty', 1, 't', 'dy', '0']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '0', '50000', 'l', 'ty'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'ty']],
        [GeomAddOp.Path, [PathAddCmd.quadBezTo, 'hc', 't', 'r', 'ty']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'b']],
      ]
      break
    }
    case 'textPlain': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '50000']],
        [GeomAddOp.Guide, ['a', 10, '30000', 'adj', '70000']],
        [GeomAddOp.Guide, ['mid', 0, 'a', 'w', '100000']],
        [GeomAddOp.Guide, ['midDir', 1, 'mid', '0', 'hc']],
        [GeomAddOp.Guide, ['dl', 1, 'mid', '0', 'l']],
        [GeomAddOp.Guide, ['dr', 1, 'r', '0', 'mid']],
        [GeomAddOp.Guide, ['dl2', 0, 'dl', '2', '1']],
        [GeomAddOp.Guide, ['dr2', 0, 'dr', '2', '1']],
        [GeomAddOp.Guide, ['dx', 3, 'midDir', 'dr2', 'dl2']],
        [GeomAddOp.Guide, ['xr', 1, 'l', 'dx', '0']],
        [GeomAddOp.Guide, ['xl', 1, 'r', '0', 'dx']],
        [GeomAddOp.Guide, ['tlx', 3, 'midDir', 'l', 'xl']],
        [GeomAddOp.Guide, ['trx', 3, 'midDir', 'xr', 'r']],
        [GeomAddOp.Guide, ['blx', 3, 'midDir', 'xl', 'l']],
        [GeomAddOp.Guide, ['brx', 3, 'midDir', 'r', 'xr']],
        [
          GeomAddOp.HandleXY,
          ['adj', '30000', '70000', undefined, '0', '0', 'mid', 'b'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'tlx', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'trx', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'blx', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'brx', 'b']],
      ]
      break
    }
    case 'textRingInside': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '60000']],
        [GeomAddOp.Guide, ['a', 10, '50000', 'adj', '99000']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['y', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['r', 0, 'dy', '1', '2']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'r', '0']],
        [GeomAddOp.Guide, ['y2', 1, 'b', '0', 'r']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '50000', '99000', 'hc', 'y'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y1']],
        [
          GeomAddOp.Path,
          [PathAddCmd.arcTo, 'wd2', 'y1', '10800000', '21599999'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y2']],
        [
          GeomAddOp.Path,
          [PathAddCmd.arcTo, 'wd2', 'r', '10800000', '21599999'],
        ],
      ]
      break
    }
    case 'textRingOutside': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '60000']],
        [GeomAddOp.Guide, ['a', 10, '50000', 'adj', '99000']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['y', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['r', 0, 'dy', '1', '2']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'r', '0']],
        [GeomAddOp.Guide, ['y2', 1, 'b', '0', 'r']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '50000', '99000', 'hc', 'y'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y1']],
        [
          GeomAddOp.Path,
          [PathAddCmd.arcTo, 'wd2', 'y1', '10800000', '-21599999'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y2']],
        [
          GeomAddOp.Path,
          [PathAddCmd.arcTo, 'wd2', 'r', '10800000', '-21599999'],
        ],
      ]
      break
    }
    case 'textSlantDown': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '44445']],
        [GeomAddOp.Guide, ['a', 10, '28569', 'adj', '100000']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['y2', 1, 'b', '0', 'dy']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '28569', '100000', 'l', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'y2']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'b']],
      ]
      break
    }
    case 'textSlantUp': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '55555']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '71431']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['y2', 1, 'b', '0', 'dy']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '0', '71431', 'l', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'y2']],
      ]
      break
    }
    case 'textStop': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '25000']],
        [GeomAddOp.Guide, ['a', 10, '14286', 'adj', '50000']],
        [GeomAddOp.Guide, ['dx', 0, 'w', '1', '3']],
        [GeomAddOp.Guide, ['dy', 0, 'a', 'h', '100000']],
        [GeomAddOp.Guide, ['x1', 1, 'l', 'dx', '0']],
        [GeomAddOp.Guide, ['x2', 1, 'r', '0', 'dx']],
        [GeomAddOp.Guide, ['y1', 1, 't', 'dy', '0']],
        [GeomAddOp.Guide, ['y2', 1, 'b', '0', 'dy']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '14286', '50000', 'l', 'dy'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y1']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'x1', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'x2', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'y1']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y2']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'x1', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'x2', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'y2']],
      ]
      break
    }
    case 'textTriangle': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '50000']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '100000']],
        [GeomAddOp.Guide, ['y', 0, 'a', 'h', '100000']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '0', '100000', 'l', 'y'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'hc', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'y']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'b']],
      ]
      break
    }
    case 'textTriangleInverted': {
      ops = [
        [GeomAddOp.Adj, ['adj', 15, '50000']],
        [GeomAddOp.Guide, ['a', 10, '0', 'adj', '100000']],
        [GeomAddOp.Guide, ['y', 0, 'a', 'h', '100000']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj', '0', '100000', 'l', 'y'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 't']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 't']],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'l', 'y']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'hc', 'b']],
        [GeomAddOp.Path, [PathAddCmd.lineTo, 'r', 'y']],
      ]
      break
    }
    case 'textWave1': {
      ops = [
        [GeomAddOp.Adj, ['adj1', 15, '12500']],
        [GeomAddOp.Adj, ['adj2', 15, '0']],
        [GeomAddOp.Guide, ['a1', 10, '0', 'adj1', '20000']],
        [GeomAddOp.Guide, ['a2', 10, '-10000', 'adj2', '10000']],
        [GeomAddOp.Guide, ['y1', 0, 'h', 'a1', '100000']],
        [GeomAddOp.Guide, ['dy2', 0, 'y1', '10', '3']],
        [GeomAddOp.Guide, ['y2', 1, 'y1', '0', 'dy2']],
        [GeomAddOp.Guide, ['y3', 1, 'y1', 'dy2', '0']],
        [GeomAddOp.Guide, ['y4', 1, 'b', '0', 'y1']],
        [GeomAddOp.Guide, ['y5', 1, 'y4', '0', 'dy2']],
        [GeomAddOp.Guide, ['y6', 1, 'y4', 'dy2', '0']],
        [GeomAddOp.Guide, ['of', 0, 'w', 'a2', '100000']],
        [GeomAddOp.Guide, ['of2', 0, 'w', 'a2', '50000']],
        [GeomAddOp.Guide, ['x1', 4, 'of']],
        [GeomAddOp.Guide, ['dx2', 3, 'of2', '0', 'of2']],
        [GeomAddOp.Guide, ['x2', 1, 'l', '0', 'dx2']],
        [GeomAddOp.Guide, ['dx5', 3, 'of2', 'of2', '0']],
        [GeomAddOp.Guide, ['x5', 1, 'r', '0', 'dx5']],
        [GeomAddOp.Guide, ['dx3', 2, 'dx2', 'x5', '3']],
        [GeomAddOp.Guide, ['x3', 1, 'x2', 'dx3', '0']],
        [GeomAddOp.Guide, ['x4', 2, 'x3', 'x5', '2']],
        [GeomAddOp.Guide, ['x6', 1, 'l', 'dx5', '0']],
        [GeomAddOp.Guide, ['x10', 1, 'r', 'dx2', '0']],
        [GeomAddOp.Guide, ['x7', 1, 'x6', 'dx3', '0']],
        [GeomAddOp.Guide, ['x8', 2, 'x7', 'x10', '2']],
        [GeomAddOp.Guide, ['x9', 1, 'r', '0', 'x1']],
        [GeomAddOp.Guide, ['xAdj', 1, 'hc', 'of', '0']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj1', '0', '20000', 'l', 'y1'],
        ],
        [
          GeomAddOp.HandleXY,
          ['adj2', '-10000', '10000', undefined, '0', '0', 'xAdj', 'b'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x2', 'y1']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x3', 'y2', 'x4', 'y3', 'x5', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x6', 'y4']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x7', 'y5', 'x8', 'y6', 'x10', 'y4'],
        ],
      ]
      break
    }
    case 'textWave2': {
      ops = [
        [GeomAddOp.Adj, ['adj1', 15, '12500']],
        [GeomAddOp.Adj, ['adj2', 15, '0']],
        [GeomAddOp.Guide, ['a1', 10, '0', 'adj1', '20000']],
        [GeomAddOp.Guide, ['a2', 10, '-10000', 'adj2', '10000']],
        [GeomAddOp.Guide, ['y1', 0, 'h', 'a1', '100000']],
        [GeomAddOp.Guide, ['dy2', 0, 'y1', '10', '3']],
        [GeomAddOp.Guide, ['y2', 1, 'y1', '0', 'dy2']],
        [GeomAddOp.Guide, ['y3', 1, 'y1', 'dy2', '0']],
        [GeomAddOp.Guide, ['y4', 1, 'b', '0', 'y1']],
        [GeomAddOp.Guide, ['y5', 1, 'y4', '0', 'dy2']],
        [GeomAddOp.Guide, ['y6', 1, 'y4', 'dy2', '0']],
        [GeomAddOp.Guide, ['of', 0, 'w', 'a2', '100000']],
        [GeomAddOp.Guide, ['of2', 0, 'w', 'a2', '50000']],
        [GeomAddOp.Guide, ['x1', 4, 'of']],
        [GeomAddOp.Guide, ['dx2', 3, 'of2', '0', 'of2']],
        [GeomAddOp.Guide, ['x2', 1, 'l', '0', 'dx2']],
        [GeomAddOp.Guide, ['dx5', 3, 'of2', 'of2', '0']],
        [GeomAddOp.Guide, ['x5', 1, 'r', '0', 'dx5']],
        [GeomAddOp.Guide, ['dx3', 2, 'dx2', 'x5', '3']],
        [GeomAddOp.Guide, ['x3', 1, 'x2', 'dx3', '0']],
        [GeomAddOp.Guide, ['x4', 2, 'x3', 'x5', '2']],
        [GeomAddOp.Guide, ['x6', 1, 'l', 'dx5', '0']],
        [GeomAddOp.Guide, ['x10', 1, 'r', 'dx2', '0']],
        [GeomAddOp.Guide, ['x7', 1, 'x6', 'dx3', '0']],
        [GeomAddOp.Guide, ['x8', 2, 'x7', 'x10', '2']],
        [GeomAddOp.Guide, ['x9', 1, 'r', '0', 'x1']],
        [GeomAddOp.Guide, ['xAdj', 1, 'hc', 'of', '0']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj1', '0', '20000', 'l', 'y1'],
        ],
        [
          GeomAddOp.HandleXY,
          ['adj2', '-10000', '10000', undefined, '0', '0', 'xAdj', 'b'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x2', 'y1']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x3', 'y3', 'x4', 'y2', 'x5', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x6', 'y4']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x7', 'y6', 'x8', 'y5', 'x10', 'y4'],
        ],
      ]
      break
    }
    case 'textWave4': {
      ops = [
        [GeomAddOp.Adj, ['adj1', 15, '6250']],
        [GeomAddOp.Adj, ['adj2', 15, '0']],
        [GeomAddOp.Guide, ['a1', 10, '0', 'adj1', '12500']],
        [GeomAddOp.Guide, ['a2', 10, '-10000', 'adj2', '10000']],
        [GeomAddOp.Guide, ['y1', 0, 'h', 'a1', '100000']],
        [GeomAddOp.Guide, ['dy2', 0, 'y1', '10', '3']],
        [GeomAddOp.Guide, ['y2', 1, 'y1', '0', 'dy2']],
        [GeomAddOp.Guide, ['y3', 1, 'y1', 'dy2', '0']],
        [GeomAddOp.Guide, ['y4', 1, 'b', '0', 'y1']],
        [GeomAddOp.Guide, ['y5', 1, 'y4', '0', 'dy2']],
        [GeomAddOp.Guide, ['y6', 1, 'y4', 'dy2', '0']],
        [GeomAddOp.Guide, ['of', 0, 'w', 'a2', '100000']],
        [GeomAddOp.Guide, ['of2', 0, 'w', 'a2', '50000']],
        [GeomAddOp.Guide, ['x1', 4, 'of']],
        [GeomAddOp.Guide, ['dx2', 3, 'of2', '0', 'of2']],
        [GeomAddOp.Guide, ['x2', 1, 'l', '0', 'dx2']],
        [GeomAddOp.Guide, ['dx8', 3, 'of2', 'of2', '0']],
        [GeomAddOp.Guide, ['x8', 1, 'r', '0', 'dx8']],
        [GeomAddOp.Guide, ['dx3', 2, 'dx2', 'x8', '6']],
        [GeomAddOp.Guide, ['x3', 1, 'x2', 'dx3', '0']],
        [GeomAddOp.Guide, ['dx4', 2, 'dx2', 'x8', '3']],
        [GeomAddOp.Guide, ['x4', 1, 'x2', 'dx4', '0']],
        [GeomAddOp.Guide, ['x5', 2, 'x2', 'x8', '2']],
        [GeomAddOp.Guide, ['x6', 1, 'x5', 'dx3', '0']],
        [GeomAddOp.Guide, ['x7', 2, 'x6', 'x8', '2']],
        [GeomAddOp.Guide, ['x9', 1, 'l', 'dx8', '0']],
        [GeomAddOp.Guide, ['x15', 1, 'r', 'dx2', '0']],
        [GeomAddOp.Guide, ['x10', 1, 'x9', 'dx3', '0']],
        [GeomAddOp.Guide, ['x11', 1, 'x9', 'dx4', '0']],
        [GeomAddOp.Guide, ['x12', 2, 'x9', 'x15', '2']],
        [GeomAddOp.Guide, ['x13', 1, 'x12', 'dx3', '0']],
        [GeomAddOp.Guide, ['x14', 2, 'x13', 'x15', '2']],
        [GeomAddOp.Guide, ['x16', 1, 'r', '0', 'x1']],
        [GeomAddOp.Guide, ['xAdj', 1, 'hc', 'of', '0']],
        [
          GeomAddOp.HandleXY,
          [undefined, '0', '0', 'adj1', '0', '12500', 'l', 'y1'],
        ],
        [
          GeomAddOp.HandleXY,
          ['adj2', '-10000', '10000', undefined, '0', '0', 'xAdj', 'b'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x2', 'y1']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x3', 'y3', 'x4', 'y2', 'x5', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x6', 'y3', 'x7', 'y2', 'x8', 'y1'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.create, false, 'none', undefined, undefined, undefined],
        ],
        [GeomAddOp.Path, [PathAddCmd.moveTo, 'x9', 'y4']],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x10', 'y6', 'x11', 'y5', 'x12', 'y4'],
        ],
        [
          GeomAddOp.Path,
          [PathAddCmd.cubicBezTo, 'x13', 'y6', 'x14', 'y5', 'x15', 'y4'],
        ],
      ]
      break
    }
  }
  operateGeometry(ops, f)
  if (typeof prst === 'string' && prst.length > 0) {
    f.preset = prst
  }
  return f
}
