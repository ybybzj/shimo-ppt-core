import { Dict } from '../../../../liber/pervasive'
import { isNumber, toInt } from '../../../common/utils'
import {
  AdjAngle,
  AdjCoordinate,
  GDFormulaTypeValue,
  GeomRect,
} from '../../../io/dataType/geometry'
import { Geometry } from './Geometry'
import { Path } from './Path'

export const GeomAddOp = {
  Adj: 1,
  Guide: 2,
  HandleXY: 3,
  HandlePolar: 4,
  Cnx: 5,
  Rect: 6,
  Path: 7,
} as const
type OpParameters<F> = F extends (geom: Geometry, ...args: infer R) => void
  ? R
  : never

export type GeometryOperation =
  | [typeof GeomAddOp.Adj, OpParameters<typeof addAdjToGeom>]
  | [typeof GeomAddOp.Guide, OpParameters<typeof addGuideToGeom>]
  | [typeof GeomAddOp.HandleXY, OpParameters<typeof addHandleXYToGeom>]
  | [typeof GeomAddOp.HandlePolar, OpParameters<typeof addHandlePolarGeom>]
  | [typeof GeomAddOp.Cnx, OpParameters<typeof addCnxToGeom>]
  | [typeof GeomAddOp.Rect, OpParameters<typeof setRectToGeom>]
  | [typeof GeomAddOp.Path, OpParameters<typeof addPathOperationToGeom>]

const gdPresets: Dict<number> = {
  ['_3cd4']: 16200000,
  ['_3cd8']: 8100000,
  ['_5cd8']: 13500000,
  ['_7cd8']: 18900000,
  ['cd2']: 10800000,
  ['cd4']: 5400000,
  ['cd8']: 2700000,
  ['l']: 0,
  ['t']: 0,
} as const

export function addAdjToGeom(
  geom: Geometry,
  name: string,
  _formula: number,
  x: AdjCoordinate | undefined
) {
  let val = toInt(x)
  if (isNaN(val) && x != null) {
    if (isNumber(gdPresets[x])) {
      val = gdPresets[x]
    } else {
      val = 0
    }
  }
  geom.gdLst[name] = val
  geom.avLst[name] = true
  // updateCalcStatusForGeometry(geom)
}

export function addGuideToGeom(
  geom: Geometry,
  name: string,
  formula: GDFormulaTypeValue,
  x: string,
  y?: string,
  z?: string,
  _n?
) {
  geom.gdLstInfo.push({
    name: name,
    formula: formula,
    x: x,
    y: y,
    z: z,
  })
}

export function addHandleXYToGeom(
  geom: Geometry,
  gdRefX: string | undefined,
  minX: AdjCoordinate | undefined,
  maxX: AdjCoordinate | undefined,
  gdRefY: string | undefined,
  minY: AdjCoordinate | undefined,
  maxY: AdjCoordinate | undefined,
  posX: AdjCoordinate,
  posY: AdjCoordinate
) {
  geom.ahXYInfoLst.push({
    gdRefX: gdRefX,
    minX: minX,
    maxX: maxX,

    gdRefY: gdRefY,
    minY: minY,
    maxY: maxY,

    posX: posX,
    posY: posY,
  })
}

export function addHandlePolarGeom(
  geom: Geometry,
  gdRefAng: string | undefined,
  minAng: AdjCoordinate | undefined,
  maxAng: AdjCoordinate | undefined,
  gdRefR: string | undefined,
  minR: AdjCoordinate | undefined,
  maxR: AdjCoordinate | undefined,
  posX: AdjCoordinate,
  posY: AdjCoordinate
) {
  geom.ahPolarInfoLst.push({
    gdRefAng: gdRefAng,
    minAng: minAng,
    maxAng: maxAng,

    gdRefR: gdRefR,
    minR: minR,
    maxR: maxR,

    posX: posX,
    posY: posY,
  })
}

export function addCnxToGeom(
  geom: Geometry,
  ang: AdjAngle | undefined,
  x: AdjCoordinate,
  y: AdjCoordinate
) {
  geom.cxnLstInfo.push({
    ang: ang,
    x: x,
    y: y,
  })
}

/** 记得触发 calc (如果有必要) */
export function setRectToGeom(
  geom: Geometry,
  l: AdjCoordinate,
  t: AdjCoordinate,
  r: AdjCoordinate,
  b: AdjCoordinate
) {
  const geomRect: GeomRect = geom.geomRect ?? { l, t, r, b }
  if (geom.geomRect != null) {
    geomRect.l = l
    geomRect.t = t
    geomRect.r = r
    geomRect.b = b
  }
  geom.geomRect = geomRect
}

export const PathAddCmd = {
  create: 0,
  moveTo: 1,
  lineTo: 2,
  arcTo: 3,
  quadBezTo: 4,
  cubicBezTo: 5,
  close: 6,
} as const

type PathAddCmdValue = (typeof PathAddCmd)[keyof typeof PathAddCmd]

export function addPathOperationToGeom(
  geom: Geometry,
  command: PathAddCmdValue,
  x1?,
  y1?,
  x2?,
  y2?,
  x3?,
  y3?
) {
  switch (command) {
    case PathAddCmd.create: {
      /* extrusionOk, fill, stroke, w, h*/
      const path = new Path()
      path.setExtrusionOk(x1 || false)
      path.setFill(y1 || 'norm')
      path.setStroke(x2 != null ? x2 : true)
      path.setPathWidth(y2)
      path.setPathHeight(x3)
      geom.addPath(path)
      break
    }
    case PathAddCmd.moveTo: {
      geom.pathLst[geom.pathLst.length - 1].moveTo(x1, y1)
      break
    }
    case PathAddCmd.lineTo: {
      geom.pathLst[geom.pathLst.length - 1].lineTo(x1, y1)
      break
    }
    case PathAddCmd.arcTo: {
      geom.pathLst[geom.pathLst.length - 1].arcTo(
        x1 /*wR*/,
        y1 /*hR*/,
        x2 /*stAng*/,
        y2 /*swAng*/
      )
      break
    }
    case PathAddCmd.quadBezTo: {
      geom.pathLst[geom.pathLst.length - 1].quadBezTo(x1, y1, x2, y2)
      break
    }
    case PathAddCmd.cubicBezTo: {
      geom.pathLst[geom.pathLst.length - 1].cubicBezTo(x1, y1, x2, y2, x3, y3)
      break
    }
    case PathAddCmd.close: {
      geom.pathLst[geom.pathLst.length - 1].close()
      break
    }
  }
}
