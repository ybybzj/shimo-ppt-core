import { idGenerator } from '../../../globals/IdGenerator'
import { drawArcOnCanvas } from '../../graphic/arc'
import {
  PathFillMode,
  Path2DOperation,
  Path2DData,
  AdjCoordinate,
} from '../../../io/dataType/geometry'
import { assignVal } from '../../../io/utils'
import {
  hitLine,
  hitInQuadraticCurve,
  hitInBezierCurve,
  hitInArc,
} from '../../graphic/hittest'
import { InstanceType } from '../../instanceTypes'
import { Nullable } from '../../../../liber/pervasive'

// path
export const PathOperationType = {
  moveTo: 0,
  lineTo: 1,
  arcTo: 2,
  quadBezTo: 3,
  cubicBezTo: 4,
  close: 5,
} as const

export class Path {
  readonly instanceType = InstanceType.Path
  stroke: boolean | null
  extrusionOk: boolean | null
  fillMode: PathFillMode | null
  pathHeight: number | null
  pathWidth: number | null
  pathCommandsInfos: PathOperationData[]
  pathCommands: PathOperation[]
  path2DForHitTestArea?: Path2D
  id: string
  constructor() {
    this.stroke = null
    this.extrusionOk = null
    this.fillMode = null
    this.pathHeight = null
    this.pathWidth = null

    this.pathCommandsInfos = []
    this.pathCommands = []

    this.id = idGenerator.newId()
  }

  getId() {
    return this.id
  }

  clone() {
    const p = new Path()
    p.setStroke(this.stroke)
    p.setExtrusionOk(this.extrusionOk)
    p.setFill(this.fillMode)
    p.setPathHeight(this.pathHeight)
    p.setPathWidth(this.pathWidth)
    for (let i = 0; i < this.pathCommandsInfos.length; ++i) {
      const command = this.pathCommandsInfos[i]
      switch (command.id) {
        case PathOperationType.moveTo:
        case PathOperationType.lineTo: {
          const x = command.x
          const y = command.y
          p.addPathOp({ id: command.id, x: x, y: y })
          break
        }
        case PathOperationType.quadBezTo: {
          const x0 = command.x0
          const y0 = command.y0
          const x1 = command.x1
          const y1 = command.y1
          p.addPathOp({
            id: PathOperationType.quadBezTo,
            x0: x0,
            y0: y0,
            x1: x1,
            y1: y1,
          })
          break
        }
        case PathOperationType.cubicBezTo: {
          const x0 = command.x0
          const y0 = command.y0
          const x1 = command.x1
          const y1 = command.y1
          const x2 = command.x2
          const y2 = command.y2
          p.addPathOp({
            id: PathOperationType.cubicBezTo,
            x0: x0,
            y0: y0,
            x1: x1,
            y1: y1,
            x2: x2,
            y2: y2,
          })
          break
        }
        case PathOperationType.arcTo: {
          const hR = command.hR
          const wR = command.wR
          const stAng = command.stAng
          const swAng = command.swAng
          p.addPathOp({
            id: PathOperationType.arcTo,
            hR: hR,
            wR: wR,
            stAng: stAng,
            swAng: swAng,
          })
          break
        }
        case PathOperationType.close: {
          p.addPathOp({ id: PathOperationType.close })
          break
        }
      }
    }
    return p
  }

  setStroke(pr) {
    this.stroke = pr
  }

  setExtrusionOk(pr) {
    this.extrusionOk = pr
  }

  setFill(pr: PathFillMode | null) {
    this.fillMode = pr
  }

  setPathHeight(pr) {
    this.pathHeight = pr
  }

  setPathWidth(pr) {
    this.pathWidth = pr
  }

  addPathOp(cmd: PathOperationData) {
    this.pathCommandsInfos.push(cmd)
  }

  moveTo(x, y) {
    this.addPathOp({ id: PathOperationType.moveTo, x: x, y: y })
  }

  lineTo(x, y) {
    this.addPathOp({ id: PathOperationType.lineTo, x: x, y: y })
  }

  arcTo(wR, hR, stAng, swAng) {
    this.addPathOp({
      id: PathOperationType.arcTo,
      wR: wR,
      hR: hR,
      stAng: stAng,
      swAng: swAng,
    })
  }

  quadBezTo(x0, y0, x1, y1) {
    this.addPathOp({
      id: PathOperationType.quadBezTo,
      x0: x0,
      y0: y0,
      x1: x1,
      y1: y1,
    })
  }

  cubicBezTo(x0, y0, x1, y1, x2, y2) {
    this.addPathOp({
      id: PathOperationType.cubicBezTo,
      x0: x0,
      y0: y0,
      x1: x1,
      y1: y1,
      x2: x2,
      y2: y2,
    })
  }

  close() {
    this.addPathOp({ id: PathOperationType.close })
  }

  resetHittestPath2D() {
    this.path2DForHitTestArea = undefined
  }

  getPath2D(): Nullable<Path2D> {
    const pathCommands = this.pathCommands
    const l = pathCommands.length
    if (l <= 0) return undefined
    if (this.path2DForHitTestArea != null) return this.path2DForHitTestArea

    const path2D = new Path2D()
    for (let i = 0; i < l; ++i) {
      const command = pathCommands[i]
      switch (command.id) {
        case PathOperationType.moveTo: {
          path2D.moveTo(command.x, command.y)
          break
        }
        case PathOperationType.lineTo: {
          path2D.lineTo(command.x, command.y)
          break
        }
        case PathOperationType.arcTo: {
          drawArcOnCanvas(
            path2D,
            command.stX,
            command.stY,
            command.wR,
            command.hR,
            command.stAng,
            command.swAng
          )
          break
        }
        case PathOperationType.quadBezTo: {
          path2D.quadraticCurveTo(
            command.x0,
            command.y0,
            command.x1,
            command.y1
          )
          break
        }
        case PathOperationType.cubicBezTo: {
          path2D.bezierCurveTo(
            command.x0,
            command.y0,
            command.x1,
            command.y1,
            command.x2,
            command.y2
          )
          break
        }
        case PathOperationType.close: {
          path2D.closePath()
        }
      }
    }
    this.path2DForHitTestArea = path2D
    return path2D
  }

  hitPath(
    cxt: CanvasRenderingContext2D,
    x: number,
    y: number,
    isTouch: boolean
  ) {
    const commands = this.pathCommands
    let lastX, lastY
    let beginX, beginY
    for (let i = 0, l = commands.length; i < l; ++i) {
      const command = commands[i]
      switch (command.id) {
        case PathOperationType.moveTo: {
          lastX = command.x
          lastY = command.y
          beginX = command.x
          beginY = command.y
          break
        }
        case PathOperationType.lineTo: {
          if (hitLine(cxt, x, y, lastX, lastY, command.x, command.y, isTouch)) {
            return true
          }
          lastX = command.x
          lastY = command.y
          break
        }
        case PathOperationType.arcTo: {
          if (
            hitInArc(
              cxt,
              x,
              y,
              command.stX,
              command.stY,
              command.wR,
              command.hR,
              command.stAng,
              command.swAng
            )
          ) {
            return true
          }
          lastX =
            command.stX -
            command.wR * Math.cos(command.stAng) +
            command.wR * Math.cos(command.swAng)
          lastY =
            command.stY -
            command.hR * Math.sin(command.stAng) +
            command.hR * Math.sin(command.swAng)
          break
        }
        case PathOperationType.quadBezTo: {
          if (
            hitInQuadraticCurve(
              cxt,
              x,
              y,
              lastX,
              lastY,
              command.x0,
              command.y0,
              command.x1,
              command.y1
            )
          ) {
            return true
          }
          lastX = command.x1
          lastY = command.y1
          break
        }
        case PathOperationType.cubicBezTo: {
          if (
            hitInBezierCurve(
              cxt,
              x,
              y,
              lastX,
              lastY,
              command.x0,
              command.y0,
              command.x1,
              command.y1,
              command.x2,
              command.y2
            )
          ) {
            return true
          }
          lastX = command.x2
          lastY = command.y2
          break
        }
        case PathOperationType.close: {
          if (hitLine(cxt, x, y, lastX, lastY, beginX, beginY, isTouch)) {
            return true
          }
        }
      }
    }
    return false
  }

  toJSON(): Path2DData {
    const data: Path2DData = {}
    assignVal(data, 'extrusionOk', this.extrusionOk)
    assignVal(data, 'fill', this.fillMode)
    assignVal(data, 'h', this.pathHeight)
    assignVal(data, 'w', this.pathWidth)
    assignVal(data, 'stroke', this.stroke)
    const cmds = this.pathCommandsInfos.map(toPath2DOperation)
    if (cmds.length > 0) {
      assignVal(data, 'cmds', cmds)
    }
    return data
  }

  fromJSON(data: Path2DData) {
    // if (data['extrusionOk'] != null) {
    this.extrusionOk = data['extrusionOk'] ?? false
    // }

    // if (data['fill'] != null) {
    this.fillMode = data['fill'] === 'none' ? 'none' : 'norm'
    // }

    // if (data['stroke'] != null) {
    this.stroke = data['stroke'] ?? true
    // }

    if (data['w'] != null) {
      this.pathWidth = data['w']
    }

    if (data['h'] != null) {
      this.pathHeight = data['h']
    }

    const cmds = data['cmds'] ?? []
    cmds.forEach((cmd) => addFromPath2DOperation(this, cmd))
  }
  hasCoords(): boolean {
    const cmds = this.pathCommandsInfos
    return cmds.some(hasCoords)
  }
}

interface PathOpClose {
  id: typeof PathOperationType.close
}
interface PathOpMoveTo {
  id: typeof PathOperationType.moveTo
  x: number
  y: number
}
interface PathOpLineTo {
  id: typeof PathOperationType.lineTo
  x: number
  y: number
}

interface PathOpArcTo {
  id: typeof PathOperationType.arcTo
  stX: number
  stY: number
  wR: number
  hR: number
  stAng: number
  swAng: number
}

interface PathOpQuadBezierTo {
  id: typeof PathOperationType.quadBezTo
  x0: number
  y0: number
  x1: number
  y1: number
}

interface PathOpCubicBezierTo {
  id: typeof PathOperationType.cubicBezTo
  x0: number
  y0: number
  x1: number
  y1: number
  x2: number
  y2: number
}

type PathOperation =
  | PathOpClose
  | PathOpMoveTo
  | PathOpLineTo
  | PathOpArcTo
  | PathOpQuadBezierTo
  | PathOpCubicBezierTo

interface PathOpDataClose {
  id: typeof PathOperationType.close
}
interface PathOpDataMoveTo {
  id: typeof PathOperationType.moveTo
  x: AdjCoordinate
  y: AdjCoordinate
}
interface PathOpDataLineTo {
  id: typeof PathOperationType.lineTo
  x: AdjCoordinate
  y: AdjCoordinate
}

interface PathOpDataArcTo {
  id: typeof PathOperationType.arcTo
  wR: AdjCoordinate
  hR: AdjCoordinate
  stAng: AdjCoordinate
  swAng: AdjCoordinate
}

interface PathOpDataQuadBezierTo {
  id: typeof PathOperationType.quadBezTo
  x0: AdjCoordinate
  y0: AdjCoordinate
  x1: AdjCoordinate
  y1: AdjCoordinate
}

interface PathOpDataCubicBezierTo {
  id: typeof PathOperationType.cubicBezTo
  x0: AdjCoordinate
  y0: AdjCoordinate
  x1: AdjCoordinate
  y1: AdjCoordinate
  x2: AdjCoordinate
  y2: AdjCoordinate
}

type PathOperationData =
  | PathOpDataClose
  | PathOpDataMoveTo
  | PathOpDataLineTo
  | PathOpDataArcTo
  | PathOpDataQuadBezierTo
  | PathOpDataCubicBezierTo

function addFromPath2DOperation(p: Path, pathOp: Path2DOperation) {
  switch (pathOp['type']) {
    case 'close':
      p.addPathOp({ id: PathOperationType.close })
      break
    case 'moveTo':
      p.addPathOp({
        id: PathOperationType.moveTo,
        x: pathOp['x'],
        y: pathOp['y'],
      })
      break
    case 'lnTo':
      p.addPathOp({
        id: PathOperationType.lineTo,
        x: pathOp['x'],
        y: pathOp['y'],
      })
      break
    case 'arcTo':
      p.addPathOp({
        id: PathOperationType.arcTo,
        wR: pathOp['wR'],
        hR: pathOp['hR'],
        stAng: pathOp['stAng'],
        swAng: pathOp['swAng'],
      })
      break
    case 'quadBezTo': {
      const [p0, p1] = pathOp['pts']
      if (p0 && p1) {
        p.addPathOp({
          id: PathOperationType.quadBezTo,
          x0: p0['x'] ?? 0,
          y0: p0['y'] ?? 0,
          x1: p1['x'] ?? 0,
          y1: p1['y'] ?? 0,
        })
      }
      break
    }
    case 'cubicBezTo': {
      const [p0, p1, p2] = pathOp['pts']
      if (p0 && p1 && p2) {
        p.addPathOp({
          id: PathOperationType.cubicBezTo,
          x0: p0['x'] ?? 0,
          y0: p0['y'] ?? 0,
          x1: p1['x'] ?? 0,
          y1: p1['y'] ?? 0,
          x2: p2['x'] ?? 0,
          y2: p2['y'] ?? 0,
        })
      }
      break
    }
  }
}

function toPath2DOperation(
  pathOpData: PathOperationData
): Path2DOperation | undefined {
  switch (pathOpData.id) {
    case PathOperationType.close:
      return {
        ['type']: 'close',
      }
    case PathOperationType.moveTo:
      return {
        ['type']: 'moveTo',
        ['x']: pathOpData.x,
        ['y']: pathOpData.y,
      }
    case PathOperationType.lineTo:
      return {
        ['type']: 'lnTo',
        ['x']: pathOpData.x,
        ['y']: pathOpData.y,
      }
    case PathOperationType.arcTo:
      return {
        ['type']: 'arcTo',
        ['wR']: pathOpData.wR,
        ['hR']: pathOpData.hR,
        ['stAng']: pathOpData.stAng,
        ['swAng']: pathOpData.swAng,
      }
    case PathOperationType.quadBezTo:
      return {
        ['type']: 'quadBezTo',
        ['pts']: [
          { ['x']: pathOpData.x0, ['y']: pathOpData.y0 },
          { ['x']: pathOpData.x1, ['y']: pathOpData.y1 },
        ],
      }
    case PathOperationType.cubicBezTo:
      return {
        ['type']: 'cubicBezTo',
        ['pts']: [
          { ['x']: pathOpData.x0, ['y']: pathOpData.y0 },
          { ['x']: pathOpData.x1, ['y']: pathOpData.y1 },
          { ['x']: pathOpData.x2, ['y']: pathOpData.y2 },
        ],
      }
  }
}

function hasCoords(cmd: PathOperationData): boolean {
  const keys = Object.keys(cmd)
  for (let i = 0, l = keys.length; i < l; i++) {
    const k = keys[i]
    if (k !== 'id' && !isNaN(parseInt(cmd[k]))) {
      return true
    }
  }
  return false
}
