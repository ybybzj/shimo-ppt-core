import { Matrix2D } from '../graphic/Matrix'

import { RefIdPrefixes, gRefIdCounter } from '../../globals/RefIdCounter'
import { Nullable } from '../../../liber/pervasive'

import { GroupShape } from './GroupShape'

import { SlideElementParent, SpTreeParent } from '../Slide/type'
import { GraphicBounds } from '../graphic/Bounds'

import { FillEffects } from './attrs/fill/fill'
import { SpPr } from './attrs/shapePrs'
import { Ln } from './attrs/line'

export type ShapeAnimationDrawInfo =
  | { action: 'NoDraw' }
  | {
      action: 'Draw'
      transform: Matrix2D
      rot?: number
    }
/**
 * Base class for all graphic objects
 * @constructor
 */
export class SlideElementBase {
  // [x: string]: any
  spPr: Nullable<SpPr>
  group: Nullable<GroupShape>
  parent: Nullable<SlideElementParent>
  spTreeParent: Nullable<SpTreeParent>
  isDeleted: boolean
  locks: number
  id: string
  x: number
  y: number
  extX: number
  extY: number
  rot: number
  flipH: boolean
  flipV: boolean
  bounds: GraphicBounds
  calcedTransform: Matrix2D
  transform: Matrix2D
  invertTransform: Matrix2D
  _tmpInfoData: {
    animationDrawInfo?: Nullable<ShapeAnimationDrawInfo>
  } = {}
  pen: Nullable<Ln>
  brush?: FillEffects
  snapXs: number[]
  snapYs: number[]
  selected: boolean
  hideWhenShow?: boolean

  refId?: string
  constructor() {
    /*Format fields*/
    this.spPr = null
    this.group = null
    this.parent = null
    this.isDeleted = true
    this.locks = 0
    this.id = ''

    /*Calculated fields*/
    this.x = 0
    this.y = 0
    this.extX = 0
    this.extY = 0
    this.rot = 0
    this.flipH = false
    this.flipV = false
    this.bounds = new GraphicBounds(0, 0, 0, 0)
    this.calcedTransform = new Matrix2D()
    this.transform = new Matrix2D()
    this.invertTransform = new Matrix2D()
    this.pen = null
    this.snapXs = []
    this.snapYs = []

    this.selected = false
  }

  getRefId(): string {
    const refId = this.refId ?? gRefIdCounter.newIDBy(RefIdPrefixes.Shape)
    this.refId = refId
    return refId
  }
  setRefId(rId: string) {
    this.refId = rId
  }
  updateAnimationDrawInfo(info: Nullable<ShapeAnimationDrawInfo>) {
    this._tmpInfoData.animationDrawInfo = info
  }

  getAnimationDrawInfo(): Nullable<ShapeAnimationDrawInfo> {
    return this._tmpInfoData?.animationDrawInfo
  }

  getId() {
    return this.id
  }
}
