import { EditActionFlag } from '../EditActionFlag'
import { idGenerator } from '../../globals/IdGenerator'

import { TextAutoFitType } from './const'
import { isNumber, isDict } from '../../common/utils'

import { TextListStyle } from '../textAttributes/TextListStyle'
import { TextDocument } from '../TextDocument/TextDocument'
import { InstanceType, isInstanceTypeOf } from '../instanceTypes'
import { isPlaceholder, isSlideElementInPPT } from '../utilities/shape/asserts'
import {
  getHierarchySps,
  getParentsOfElement,
} from '../utilities/shape/getters'
import { checkTextFit } from '../utilities/shape/calcs'
import { getPresentation } from '../utilities/finders'
import { Shape } from './Shape'
import { Nullable } from '../../../liber/pervasive'
import { BodyPr } from './attrs/bodyPr'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'

import { updateCalcStatusForTextBody } from '../calculation/updateCalcStatus/textContent'
import { defaultContentStylePrs } from '../textAttributes/ContentStyle'
import { getMaxTextContentWidthByLimit } from '../TextDocument/utils/getter'

export class TextBody {
  readonly instanceType = InstanceType.TextBody
  bodyPr?: BodyPr
  lstStyle?: TextListStyle
  textDoc: TextDocument | null
  parent: Shape
  phContent: TextDocument | null
  calcedBodyPr: Nullable<BodyPr>
  calcStatus: {
    isBodyPrDirty: boolean
    isPlaceholderContentDirty: boolean
  }
  id: string
  contentWidth: any
  contentWidthOfPh?: number
  contentHeightPh?: number
  constructor(parent: Shape) {
    this.bodyPr = undefined
    this.lstStyle = undefined
    this.textDoc = null
    this.parent = parent

    this.phContent = null
    this.calcedBodyPr = null
    this.calcStatus = {
      isBodyPrDirty: true,
      isPlaceholderContentDirty: true,
    }
    this.id = idGenerator.newId()
  }

  clone() {
    const ret = new TextBody(this.parent)
    if (this.bodyPr) ret.setBodyPr(this.bodyPr.clone())
    if (this.lstStyle) ret.setTextListStyle(this.lstStyle.clone())
    if (this.textDoc) {
      ret.setContent(this.textDoc.clone(ret))
    }
    return ret
  }

  getId() {
    return this.id
  }

  getTheme() {
    if (this.parent) {
      return this.parent.getTheme()
    }
    return null
  }

  getColorMap() {
    if (this.parent) {
      return this.parent.getColorMap()
    }
    return null
  }

  setParent(pr: Shape) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_TextBodySetParent,
      this.parent,
      pr
    )
    this.parent = pr
  }

  setBodyPr(pr: BodyPr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_TextBodySetBodyPr,
      this.bodyPr,
      pr
    )
    this.bodyPr = pr
    this.calcStatus.isBodyPrDirty = true
    updateCalcStatusForTextBody(this, EditActionFlag.edit_TextBodySetBodyPr)
  }

  setContent(textDoc: TextDocument) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_TextBodySetContent,
      this.textDoc,
      textDoc
    )
    if (this.textDoc) {
      TextDocument.recycle(this.textDoc)
    }
    this.textDoc = textDoc
    updateCalcStatusForTextBody(this, EditActionFlag.edit_TextBodySetContent)
  }

  setTextListStyle(lstStyle: TextListStyle | undefined) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_TextBodySetLstStyle,
      this.lstStyle,
      lstStyle
    )
    this.lstStyle = lstStyle
    updateCalcStatusForTextBody(this, EditActionFlag.edit_TextBodySetLstStyle)
  }

  private calculateBodyPr() {
    if (!this.calcedBodyPr) this.calcedBodyPr = BodyPr.Default()
    else {
      this.calcedBodyPr.setDefault()
    }
    if (this.parent && isPlaceholder(this.parent)) {
      const hsps = getHierarchySps(this.parent)
      for (let i = hsps.length - 1; i > -1; --i) {
        const sp = hsps[i]
        if (sp?.instanceType === InstanceType.Shape && sp.txBody?.bodyPr) {
          this.calcedBodyPr.merge(sp.txBody.bodyPr)
        }
      }
    }
    if (this.bodyPr) {
      this.calcedBodyPr.merge(this.bodyPr)
    }
  }

  checkTextFit() {
    if (
      isInstanceTypeOf(this.parent?.parent, InstanceType.Slide) &&
      this.bodyPr
    ) {
      const { textFit } = this.bodyPr
      if (isDict(textFit)) {
        if (textFit.type === TextAutoFitType.NORMAL) {
          let fontScale, spacingScale
          if (isNumber(textFit.fontScale)) {
            fontScale = textFit.fontScale / 100000
          }
          if (isNumber(textFit.lnSpcReduction)) {
            spacingScale = textFit.lnSpcReduction / 100000
          }

          checkTextFit(this.parent, fontScale, spacingScale)

          this.bodyPr.textFit = undefined
        }
      }
    }
  }

  isEmpty() {
    return this.textDoc ? this.textDoc.isEmpty() : true
  }

  selectSelf() {
    if (this.parent) {
      this.parent.selectSelf()
    }
  }

  getBodyPr() {
    if (this.calcStatus.isBodyPrDirty) {
      this.calculateBodyPr()
      this.calcStatus.isBodyPrDirty = false
    }
    return this.calcedBodyPr!
  }

  getWholeHeight() {
    return this.textDoc!.getWholeHeight()
  }

  getWholeHeightForPhContent() {
    return this.phContent ? this.phContent.getWholeHeight() : 0
  }

  getCalcedBodyPr() {
    this.calculateBodyPr()
    return this.calcedBodyPr
  }

  getTextStyles(level: number) {
    if (this.parent) {
      return this.parent.getTextStyles(level)
    }

    return {
      style: defaultContentStylePrs(),
    }
  }

  getRectWidthByLimit(widthLimit: number) {
    const bodyPr = this.getBodyPr()
    const r_ins = bodyPr.rIns!
    const l_ins = bodyPr.lIns!
    widthLimit = widthLimit - r_ins - l_ins
    widthLimit = getMaxTextContentWidthByLimit(this.textDoc!, widthLimit)

    return widthLimit + 2 + r_ins + l_ins
  }

  isInPPT(id?: string) {
    if (id != null) {
      if (!this.textDoc || this.textDoc.getId() !== id) {
        return false
      }
    }
    if (this.parent) {
      return isSlideElementInPPT(this.parent)
    }
    return false
  }

  getTextTransformOfParent() {
    if (this.parent && this.parent.textTransform) {
      return this.parent.textTransform.clone()
    }
    return null
  }

  isCurrentSelected() {
    if (this.parent && this.parent.isCurrentSelected) {
      return this.parent.isCurrentSelected()
    }
    return false
  }

  getParentSlideIndex() {
    if (this.parent) {
      const parents = getParentsOfElement(this.parent)
      if (parents.slide) {
        return parents.slide.index
      }
      if (parents.notes && parents.notes.slide) {
        return parents.notes.slide.index
      }
    }
    return 0
  }

  isCurrentPlaceholder() {
    const presentation = getPresentation(this)
    const textTarget = presentation?.getCurrentTextTarget()
    if (textTarget) {
      return textTarget === this.textDoc
    }
    return false
  }
}
