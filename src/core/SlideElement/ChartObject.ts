import { ImageShape } from './Image'
import { Dict } from '../../../liber/pervasive'
import { ChartAttrs, OLEObjectAttrs } from '../../io/dataType/csld'
import { assignVal } from '../../io/utils'

export class ChartObject extends ImageShape {
  uri?: string
  data?: Dict<any>

  constructor() {
    super()
  }

  isChartObject(): this is ChartObject {
    return true
  }

  setUri(uri) {
    this.uri = uri
  }

  attrsToJSON(): ChartAttrs {
    const data: ChartAttrs = {}
    assignVal(data, 'link', this.uri)
    assignVal(data, 'data', this.data)
    return data
  }

  attrsFromJSON(data: OLEObjectAttrs) {
    if (data['link'] != null) {
      this.uri = data['link']
    }

    const chart_data = data['data']
    if (chart_data != null) {
      this.data = chart_data
    }
  }

  copyTo(target: ChartObject) {
    target.uri = this.uri
    target.data = this.data
  }

  isValid() {
    if (super.isValid() === false) {
      return false
    }

    if (!this.uri) {
      return false
    }

    return true
  }
}
