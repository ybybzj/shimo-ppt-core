import { ABS_DIRECTION } from '../common/const/attrs'
import { normalizeRotValue } from '../common/utils'
import { GraphicBounds } from '../graphic/Bounds'
import { Matrix2D } from '../graphic/Matrix'
import { radFactor } from './const'

export class ConnectorInfo {
  bounds: GraphicBounds
  dir: number
  x: number
  y: number
  idx: number
  constructor() {
    this.bounds = new GraphicBounds(0, 0, 0, 0)
    this.dir = ABS_DIRECTION.E
    this.x = 0
    this.y = 0
    this.idx = 0
  }

  clone() {
    const cp = new ConnectorInfo()
    cp.bounds.fromBounds(this.bounds)
    cp.dir = this.dir
    cp.x = this.x
    cp.y = this.y
    cp.idx = this.idx
    return cp
  }

  transform(transform: Matrix2D) {
    this.bounds.transform(transform)
    //this.dir = CARD_DIRECTION_E;
    const x = transform.XFromPoint(this.x, this.y)
    const y = transform.YFromPoint(this.x, this.y)
    this.x = x
    this.y = y
  }
}

export function convertToConnectorInfo(
  rot: number,
  flipH: boolean,
  flipV: boolean,
  transform: Matrix2D,
  bounds: GraphicBounds,
  infos: {
    idx: number
    ang: number
    x: number
    y: number
  }
) {
  rot = normalizeRotValue(infos.ang * radFactor + rot)

  const ret = new ConnectorInfo()
  ret.dir = ABS_DIRECTION.E
  if (
    (rot >= 0 && rot < Math.PI * 0.25) ||
    (rot >= 7 * Math.PI * 0.25 && rot < 2 * Math.PI)
  ) {
    ret.dir = ABS_DIRECTION.E
    if (flipH) {
      ret.dir = ABS_DIRECTION.W
    }
  } else if (rot >= Math.PI * 0.25 && rot < 3 * Math.PI * 0.25) {
    ret.dir = ABS_DIRECTION.S
    if (flipV) {
      ret.dir = ABS_DIRECTION.N
    }
  } else if (rot >= 3 * Math.PI * 0.25 && rot < 5 * Math.PI * 0.25) {
    ret.dir = ABS_DIRECTION.W
    if (flipH) {
      ret.dir = ABS_DIRECTION.E
    }
  } else if (rot >= 5 * Math.PI * 0.25 && rot < 7 * Math.PI * 0.25) {
    ret.dir = ABS_DIRECTION.N
    if (flipV) {
      ret.dir = ABS_DIRECTION.S
    }
  }
  ret.x = transform.XFromPoint(infos.x, infos.y)
  ret.y = transform.YFromPoint(infos.x, infos.y)
  ret.bounds.fromBounds(bounds)
  ret.idx = infos.idx
  return ret
}
