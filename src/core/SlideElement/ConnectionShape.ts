import { EditorUtil } from '../../globals/editor'
import {
  normalizeRotValue,
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../common/utils'
import { isGroup, isLineConnector } from '../utilities/shape/asserts'
import { ABS_DIRECTION } from '../common/const/attrs'
import { Matrix2D, MatrixUtils } from '../graphic/Matrix'
import { createGeometry } from './geometry/creators'
import { GraphicBounds } from '../graphic/Bounds'
import { getConnectionInfo } from '../utilities/shape/calcs'
import {
  getFinalFlipH,
  getFinalFlipV,
  getFinalRotate,
  getTopMostGroupShape,
} from '../utilities/shape/getters'
import { SpPr, Xfrm } from './attrs/shapePrs'
import { calculateForShape } from '../calculation/calculate/shape'
import { Shape } from './Shape'
import { calculateForGroup } from '../calculation/calculate/group'
import { calculateSlideElement } from '../calculation/calculate/slideElement'
import { getGeometryForSp } from '../utilities/shape/geometry'
import { ConnectorInfo } from './ConnectorInfo'
import { CxnProps } from './geometry/Geometry'
import { SlideElement } from './type'
import { Nullable } from '../../../liber/pervasive'

/**
 * @note flip 仅与生成时的方向有关，左上方为正, 调整 rot 导致的方向变化不影响 flip
 * @note 直线默认无 rot，不可旋转，可通过多选 shape 旋转或设置数值
 * resize 会重置 rot 为 0, 同时重新计算 flip， move 和 rotate 保持
 * @note 弓形连接符等计算得出 rot
 */
export class ConnectionShape extends Shape {
  constructor() {
    super()
  }
  isConnectionShape() {
    return true
  }

  resetWithShape(shape: SlideElement) {
    if (!this.nvSpPr) return

    const nvUniSpPr = this.nvSpPr.nvUniSpPr

    // 组合图形需递归判断
    const getResetSp = (sp: SlideElement) => {
      if (nvUniSpPr.stCxnId === sp.id || nvUniSpPr.endCxnId === sp.id) {
        return sp
      }
      if (isGroup(sp)) {
        for (let i = 0; i < sp.spTree.length; i++) {
          const innerSp = getResetSp(sp.spTree[i])
          if (innerSp) {
            return innerSp
          }
        }
      }
    }

    shape = getResetSp(shape)
    if (shape) {
      const newPr = nvUniSpPr.clone()
      if (nvUniSpPr.stCxnId === shape.id) {
        newPr.stCxnId = null
        newPr.stCxnIdx = null
      }
      if (nvUniSpPr.endCxnId === shape.id) {
        newPr.endCxnId = null
        newPr.endCxnIdx = null
      }
      this.nvSpPr.setUniSpPr(newPr)
    }
  }

  getStCxnId() {
    return this.nvSpPr!.nvUniSpPr.stCxnId
  }

  getEndCxnId() {
    return this.nvSpPr!.nvUniSpPr.endCxnId
  }

  getStCxnIdx() {
    return this.nvSpPr!.nvUniSpPr.stCxnIdx
  }

  getEndCxnIdx() {
    return this.nvSpPr!.nvUniSpPr.endCxnIdx
  }

  calcTransform(isMove?: boolean) {
    const stId = this.getStCxnId()
    const endId = this.getEndCxnId()

    const calcStartOrEndSpById = (cxnId): Nullable<Shape> => {
      let sp = EditorUtil.entityRegistry.getEntityById<Shape>(cxnId)
      if (sp && sp.isDeleted) {
        sp = null
      }
      if (sp) {
        const group = getTopMostGroupShape(sp)
        if (group) {
          calculateForGroup(group)
        } else {
          calculateSlideElement(sp)
        }
      }
      return sp
    }

    const startSp = calcStartOrEndSpById(stId)
    const endSp = calcStartOrEndSpById(endId)
    let startCxnInfo: Nullable<ConnectorInfo>
    let endCxnInfo: Nullable<ConnectorInfo>
    const xfrm = this.spPr!.xfrm!
    if (startSp) {
      startCxnInfo = getConnectionInfo(startSp, this.getStCxnIdx())
    }
    if (endSp) {
      endCxnInfo = getConnectionInfo(endSp, this.getEndCxnIdx())
    }

    if (startCxnInfo && endCxnInfo) {
      const spPr = calculateCxnSpPr(
        startCxnInfo!,
        endCxnInfo!,
        this.spPr!.geometry!.preset,
        this.pen?.w
      )
      const _xfrm = spPr.xfrm!
      xfrm.setExtX(_xfrm.extX)
      xfrm.setExtY(_xfrm.extY)
      if (!this.group) {
        xfrm.setOffX(_xfrm.offX)
        xfrm.setOffY(_xfrm.offY)
        xfrm.setFlipH(_xfrm.flipH)
        xfrm.setFlipV(_xfrm.flipV)
        xfrm.setRot(_xfrm.rot)
      } else {
        const _xc = _xfrm.offX + _xfrm.extX / 2.0
        const _yc = _xfrm.offY + _xfrm.extY / 2.0
        const xc = this.group.invertTransform.XFromPoint(_xc, _yc)
        const yc = this.group.invertTransform.YFromPoint(_xc, _yc)
        xfrm.setOffX(xc - _xfrm.extX / 2.0)
        xfrm.setOffY(yc - _xfrm.extY / 2.0)
        xfrm.setFlipH(getFinalFlipH(this.group) ? !_xfrm.flipH : _xfrm.flipH)
        xfrm.setFlipV(getFinalFlipV(this.group) ? !_xfrm.flipV : _xfrm.flipV)
        xfrm.setRot(normalizeRotValue(_xfrm.rot - getFinalRotate(this.group)))
      }
      this.spPr!.setGeometry(spPr.geometry!.clone())
      calculateForShape(this)
    } else if (startCxnInfo || endCxnInfo) {
      if (isMove) {
        let _x, _y
        let spX,
          spY,
          diffX,
          diffY,
          isChecked = false
        let cxnProps: CxnProps
        if (startSp) {
          cxnProps = getGeometryForSp(startSp).cxnLst[this.getStCxnIdx()!]
          if (cxnProps) {
            spX = startSp.transform.XFromPoint(cxnProps.x, cxnProps.y)
            spY = startSp.transform.YFromPoint(cxnProps.x, cxnProps.y)
            _x = this.transform.XFromPoint(0, 0)
            _y = this.transform.YFromPoint(0, 0)
            isChecked = true
          }
        } else {
          cxnProps = getGeometryForSp(endSp!).cxnLst[this.getEndCxnIdx()!]
          if (cxnProps) {
            spX = endSp!.transform.XFromPoint(cxnProps.x, cxnProps.y)
            spY = endSp!.transform.YFromPoint(cxnProps.x, cxnProps.y)
            _x = this.transform.XFromPoint(this.extX, this.extY)
            _y = this.transform.YFromPoint(this.extX, this.extY)
            isChecked = true
          }
        }

        if (isChecked) {
          if (this.group) {
            const groupMatrix = this.group.invertTransform.clone()
            groupMatrix.tx = 0
            groupMatrix.ty = 0
            diffX = groupMatrix.XFromPoint(spX - _x, spY - _y)
            diffY = groupMatrix.YFromPoint(spX - _x, spY - _y)
          } else {
            diffX = spX - _x
            diffY = spY - _y
          }
          this.spPr!.xfrm!.setOffX(this.spPr!.xfrm!.offX + diffX)
          this.spPr!.xfrm!.setOffY(this.spPr!.xfrm!.offY + diffY)
          calculateForShape(this)
        }
      } else {
        if (!startCxnInfo) {
          const tx = this.transform.XFromPoint(0, 0)
          const ty = this.transform.YFromPoint(0, 0)
          startCxnInfo = calcConnectionInfoByXY(endCxnInfo!, tx, ty)
        }
        if (!endCxnInfo) {
          const tx = this.transform.XFromPoint(this.extX, this.extY)
          const ty = this.transform.YFromPoint(this.extX, this.extY)
          endCxnInfo = calcConnectionInfoByXY(startCxnInfo, tx, ty)
        }
        const spPr = calculateCxnSpPr(
          startCxnInfo,
          endCxnInfo,
          this.spPr!.geometry!.preset,
          this.pen && this.pen.w
        )
        const _xfrm = spPr.xfrm!
        xfrm.setExtX(_xfrm.extX)
        xfrm.setExtY(_xfrm.extY)
        if (!this.group) {
          xfrm.setOffX(_xfrm.offX)
          xfrm.setOffY(_xfrm.offY)
          xfrm.setFlipH(_xfrm.flipH)
          xfrm.setFlipV(_xfrm.flipV)
          xfrm.setRot(_xfrm.rot)
        } else {
          const _xc = _xfrm.offX + _xfrm.extX / 2.0
          const _yc = _xfrm.offY + _xfrm.extY / 2.0
          const xc = this.group.invertTransform.XFromPoint(_xc, _yc)
          const yc = this.group.invertTransform.YFromPoint(_xc, _yc)
          xfrm.setOffX(xc - _xfrm.extX / 2.0)
          xfrm.setOffY(yc - _xfrm.extY / 2.0)
          xfrm.setFlipH(getFinalFlipH(this.group) ? !_xfrm.flipH : _xfrm.flipH)
          xfrm.setFlipV(getFinalFlipV(this.group) ? !_xfrm.flipV : _xfrm.flipV)
          xfrm.setRot(normalizeRotValue(_xfrm.rot - getFinalRotate(this.group)))
        }
        this.spPr!.setGeometry(spPr.geometry!.clone())
        calculateForShape(this)
      }
    }
  }
}

const CXN_MARGIN = 6.35

/**
 * @usage 重新计算 cxnShape 的 spPr
 * @note 普通直线 'line' | 'straightConnector' 角度需要重置为 0(move/resize 等操作触发)
 * @note 弓形连接符等需要重新计算 rot
 * */
export function calculateCxnSpPr(
  begin: ConnectorInfo,
  end: ConnectorInfo,
  presetGeometry,
  widthOfPen
): SpPr {
  const s = turnOffRecordChanges()
  const spPr = new SpPr()
  const xfrm = new Xfrm(spPr)
  spPr.setXfrm(xfrm)
  xfrm.setParent(spPr)

  const _begin = begin.clone()
  const _end = end.clone()
  let angle = 0

  if (!widthOfPen) {
    widthOfPen = 12700
  }

  switch (begin.dir) {
    case ABS_DIRECTION.N: {
      angle = Math.PI / 2
      switch (_end.dir) {
        case ABS_DIRECTION.N: {
          _end.dir = ABS_DIRECTION.E
          break
        }
        case ABS_DIRECTION.S: {
          _end.dir = ABS_DIRECTION.W
          break
        }
        case ABS_DIRECTION.W: {
          _end.dir = ABS_DIRECTION.N
          break
        }
        case ABS_DIRECTION.E: {
          _end.dir = ABS_DIRECTION.S
          break
        }
      }
      break
    }
    case ABS_DIRECTION.S: {
      angle = -Math.PI / 2
      switch (_end.dir) {
        case ABS_DIRECTION.N: {
          _end.dir = ABS_DIRECTION.W
          break
        }
        case ABS_DIRECTION.S: {
          _end.dir = ABS_DIRECTION.E
          break
        }
        case ABS_DIRECTION.W: {
          _end.dir = ABS_DIRECTION.S
          break
        }
        case ABS_DIRECTION.E: {
          _end.dir = ABS_DIRECTION.N
          break
        }
      }
      break
    }
    case ABS_DIRECTION.W: {
      angle = Math.PI
      switch (_end.dir) {
        case ABS_DIRECTION.N: {
          _end.dir = ABS_DIRECTION.S
          break
        }
        case ABS_DIRECTION.S: {
          _end.dir = ABS_DIRECTION.N
          break
        }
        case ABS_DIRECTION.W: {
          _end.dir = ABS_DIRECTION.E
          break
        }
        case ABS_DIRECTION.E: {
          _end.dir = ABS_DIRECTION.W
          break
        }
      }
      break
    }
    default: {
      //East is default direction
      angle = 0
      break
    }
  }

  const transform = new Matrix2D()
  const isStraightLineConnector = isLineConnector(presetGeometry)

  if (!isStraightLineConnector) {
    MatrixUtils.rotateMatrix(transform, -angle)
  }

  _begin.transform(transform)
  _end.transform(transform)

  let extX = Math.max(0, Math.abs(_end.x - _begin.x))
  let extY = Math.max(0, Math.abs(_end.y - _begin.y))

  if (Math.abs(extX) < 0.5) {
    extX = 0
  }
  if (Math.abs(extY) < 0.5) {
    extY = 0
  }
  let flipV = false
  let flipH = false
  const rot = 0
  const adjMap = {}
  if (isStraightLineConnector) {
    flipH = _begin.x > _end.x
    flipV = _begin.y > _end.y
  } else {
    let prefix = 'bentConnector'
    if (presetGeometry.indexOf('curvedConnector') > -1) {
      prefix = 'curvedConnector'
    }
    //returns "bentConnector" by default
    switch (_end.dir) {
      case ABS_DIRECTION.N: {
        if (_end.bounds.l > _begin.bounds.r) {
          if (_end.bounds.t < _begin.y) {
            if (extX === 0) {
              extX = 1
            }
            if (extY === 0) {
              extY = 1
            }

            if (_end.y <= _begin.y) {
              presetGeometry = '4'
              flipV = true
              adjMap['adj1'] =
                (100000 *
                  (((_begin.bounds.r + _end.bounds.l) / 2 - _begin.x) / extX) +
                  0.5) >>
                0
              adjMap['adj2'] =
                ((100000 * (_begin.y - (_end.bounds.t - CXN_MARGIN))) / extY +
                  0.5) >>
                0
            } else {
              presetGeometry = '4'
              adjMap['adj1'] =
                (100000 *
                  (((_begin.bounds.r + _end.bounds.l) / 2 - _begin.x) / extX) +
                  0.5) >>
                0
              adjMap['adj2'] =
                (-100000 * ((_begin.y - (_end.bounds.t - CXN_MARGIN)) / extY) +
                  0.5) >>
                0
            }
          } else {
            presetGeometry = '2'
          }
        } else {
          if (_end.bounds.t > _begin.bounds.b) {
            if (_end.x < _begin.x) {
              if (extX === 0) {
                extX = 1
              }
              if (extY === 0) {
                extY = 1
              }
              presetGeometry = '4'
              flipH = true
              adjMap['adj1'] = -(
                ((100000 * (_begin.bounds.r + CXN_MARGIN - _begin.x)) / extX +
                  0.5) >>
                0
              )
              adjMap['adj2'] =
                ((100000 *
                  ((_end.bounds.t + _begin.bounds.b) / 2.0 - _begin.y)) /
                  extY) >>
                0
            } else {
              presetGeometry = '2'
            }
          } else if (_end.bounds.b < _begin.bounds.t) {
            if (extX === 0) {
              extX = 1
            }
            if (extY === 0) {
              extY = 1
            }
            if (_end.x < _begin.x) {
              presetGeometry = '4'
              flipH = true
              flipV = true
              adjMap['adj1'] = -(
                ((100000 * (_begin.bounds.r + CXN_MARGIN - _begin.x)) / extX +
                  0.5) >>
                0
              )
              adjMap['adj2'] =
                ((100000 * (_begin.y - (_end.bounds.t - CXN_MARGIN))) / extY +
                  0.5) >>
                0
            } else {
              presetGeometry = '4'
              flipV = true
              adjMap['adj1'] =
                ((100000 *
                  (Math.max(_begin.bounds.r, _end.bounds.r) +
                    CXN_MARGIN -
                    _begin.x)) /
                  extX) >>
                0
              adjMap['adj2'] =
                ((100000 * (_begin.y - (_end.bounds.t - CXN_MARGIN))) / extY +
                  0.5) >>
                0
            }
          } else {
            if (_end.y < _begin.y) {
              if (extX === 0) {
                extX = 1
              }
              if (extY === 0) {
                extY = 1
              }
              if (_end.x < _begin.x) {
                presetGeometry = '4'
                flipH = true
                flipV = true
                adjMap['adj1'] = -(
                  ((100000 * (_begin.bounds.r + CXN_MARGIN - _begin.x)) / extX +
                    0.5) >>
                  0
                )
                adjMap['adj2'] =
                  ((100000 * (_begin.y - (_end.bounds.t - CXN_MARGIN))) / extY +
                    0.5) >>
                  0
              } else {
                presetGeometry = '4'
                flipV = true
                adjMap['adj1'] =
                  ((100000 *
                    (Math.max(_begin.bounds.r, _end.bounds.r) +
                      CXN_MARGIN -
                      _begin.x)) /
                    extX +
                    0.5) >>
                  0
                adjMap['adj2'] =
                  ((100000 * (_begin.y - (_end.bounds.t - CXN_MARGIN))) / extY +
                    0.5) >>
                  0
              }
            } else {
              if (_end.x > _begin.x) {
                presetGeometry = '2'
              } else {
                if (extX === 0) {
                  extX = 1
                }
                if (extY === 0) {
                  extY = 1
                }
                presetGeometry = '4'
                flipH = true
                adjMap['adj1'] =
                  -(
                    (100000 *
                      (Math.max(_begin.bounds.r, _end.bounds.r) +
                        CXN_MARGIN -
                        _begin.x)) /
                      extX +
                    0.5
                  ) >> 0
                adjMap['adj2'] = -(
                  ((100000 *
                    (_begin.y -
                      (Math.min(_begin.bounds.t, _end.bounds.t) -
                        CXN_MARGIN))) /
                    extY +
                    0.5) >>
                  0
                )
              }
            }
          }
        }
        break
      }
      case ABS_DIRECTION.S: {
        if (_end.bounds.l > _begin.bounds.r) {
          if (_end.bounds.b < _begin.y) {
            presetGeometry = '2'
            flipV = true
          } else {
            if (extX === 0) {
              extX = 1
            }
            if (extY === 0) {
              extY = 1
            }
            if (_end.y < _begin.y) {
              presetGeometry = '4'
              flipV = true
              adjMap['adj2'] = -(
                (100000 * ((_end.bounds.b + CXN_MARGIN - _end.y) / extY) +
                  0.5) >>
                0
              )
              adjMap['adj1'] =
                ((100000 * ((_begin.bounds.r + _end.bounds.l) / 2 - _begin.x)) /
                  extX +
                  0.5) >>
                0
            } else {
              presetGeometry = '4'
              adjMap['adj1'] =
                (100000 *
                  (((_begin.bounds.r + _end.bounds.l) / 2.0 - _begin.x) /
                    extX) +
                  0.5) >>
                0
              adjMap['adj2'] =
                100000 +
                ((100000 * ((_end.bounds.b + CXN_MARGIN - _end.y) / extY) +
                  0.5) >>
                  0)
            }
          }
        } else {
          if (_end.bounds.b < _begin.bounds.t) {
            if (_end.x > _begin.bounds.r) {
              presetGeometry = '2'
              flipV = true
            } else {
              if (extX === 0) {
                extX = 1
              }
              if (extY === 0) {
                extY = 1
              }
              presetGeometry = '4'
              flipH = true
              flipV = true

              adjMap['adj1'] = -(
                ((100000 * (_begin.bounds.r + CXN_MARGIN - _begin.x)) / extX +
                  0.5) >>
                0
              )
              adjMap['adj2'] =
                ((100000 *
                  (_begin.y - (_end.bounds.b + _begin.bounds.t) / 2.0)) /
                  extY +
                  0.5) >>
                0
            }
          } else {
            if (_end.x < _begin.x) {
              if (extX === 0) {
                extX = 1
              }
              if (extY === 0) {
                extY = 1
              }
              if (_end.y > _begin.y) {
                presetGeometry = '4'
                flipH = true
                adjMap['adj1'] = -(
                  ((100000 *
                    (Math.max(_begin.bounds.r, _end.bounds.r) +
                      CXN_MARGIN -
                      _begin.x)) /
                    extX +
                    0.5) >>
                  0
                )
                adjMap['adj2'] =
                  (100000 *
                    ((Math.max(_end.bounds.b, _begin.bounds.b) -
                      _begin.y +
                      CXN_MARGIN) /
                      extY) +
                    0.5) >>
                  0
              } else {
                presetGeometry = '4'
                flipH = true
                flipV = true
                adjMap['adj1'] = -(
                  ((100000 *
                    (Math.max(_begin.bounds.r, _end.bounds.r) +
                      CXN_MARGIN -
                      _begin.x)) /
                    extX +
                    0.5) >>
                  0
                )
                adjMap['adj2'] =
                  -(
                    100000 *
                      ((Math.max(_end.bounds.b, _begin.bounds.b) -
                        _begin.y +
                        CXN_MARGIN) /
                        extY) +
                    0.5
                  ) >> 0
              }
            } else {
              if (_end.y > _begin.y) {
                if (extX === 0) {
                  extX = 1
                }
                if (extY === 0) {
                  extY = 1
                }
                presetGeometry = '4'

                adjMap['adj1'] =
                  ((100000 *
                    (Math.max(_begin.bounds.r, _end.bounds.r) +
                      CXN_MARGIN -
                      _begin.x)) /
                    extX +
                    0.5) >>
                  0
                adjMap['adj2'] =
                  ((100000 * (_end.bounds.b + CXN_MARGIN - _begin.y)) / extY +
                    0.5) >>
                  0
              } else {
                presetGeometry = '2'
                flipV = true
              }
            }
          }
        }
        break
      }
      case ABS_DIRECTION.W: {
        if (_begin.x < _end.x) {
          presetGeometry = '3'
          flipV = _begin.y > _end.y
        } else {
          presetGeometry = '5'
          if (extX === 0) {
            extX = 1
          }
          if (extY === 0) {
            extY = 1
          }
          if (_end.bounds.t > _begin.bounds.b) {
            flipH = true
            adjMap['adj1'] = -(
              ((100000 * (_begin.bounds.r + CXN_MARGIN - _begin.x)) / extX +
                0.5) >>
              0
            )
            adjMap['adj2'] =
              ((100000 * ((_end.bounds.t + _begin.bounds.b) / 2.0 - _begin.y)) /
                extY +
                0.5) >>
              0
            adjMap['adj3'] =
              ((100000 * (_begin.x - (_end.bounds.l - CXN_MARGIN))) / extX +
                0.5) >>
              0
          } else if (_end.bounds.b < _begin.bounds.t) {
            flipH = true
            flipV = true
            adjMap['adj1'] = -(
              ((100000 * (_begin.bounds.r + CXN_MARGIN - _begin.x)) / extX +
                0.5) >>
              0
            )
            adjMap['adj2'] =
              ((100000 * (_begin.y - (_end.bounds.b + _begin.bounds.t) / 2.0)) /
                extY +
                0.5) >>
              0
            adjMap['adj3'] =
              ((100000 * (_begin.x - (_end.bounds.l - CXN_MARGIN))) / extX +
                0.5) >>
              0
          } else {
            if (_end.y >= _begin.y) {
              flipH = true
              adjMap['adj1'] = -(
                ((100000 * (_begin.bounds.r + CXN_MARGIN - _begin.x)) / extX +
                  0.5) >>
                0
              )
              adjMap['adj2'] =
                ((100000 *
                  (Math.max(_begin.bounds.b, _end.bounds.b) +
                    CXN_MARGIN -
                    _begin.y)) /
                  extY +
                  0.5) >>
                0
              adjMap['adj3'] =
                ((100000 * (_begin.x - (_end.bounds.l - CXN_MARGIN))) / extX +
                  0.5) >>
                0
            } else {
              flipH = true
              flipV = true
              adjMap['adj1'] = -(
                ((100000 * (_begin.bounds.r + CXN_MARGIN - _begin.x)) / extX +
                  0.5) >>
                0
              )
              adjMap['adj2'] =
                ((100000 *
                  (_begin.y -
                    (Math.min(_begin.bounds.t, _end.bounds.t) - CXN_MARGIN))) /
                  extY +
                  0.5) >>
                0
              adjMap['adj3'] =
                ((100000 * (_begin.x - (_end.bounds.l - CXN_MARGIN))) / extX +
                  0.5) >>
                0
            }
          }
        }
        break
      }
      case ABS_DIRECTION.E: {
        if (_end.bounds.l > _begin.bounds.r) {
          if (extX === 0) {
            extX = 1
          }
          if (extY === 0) {
            extY = 1
          }
          if (_end.bounds.b < _begin.y) {
            presetGeometry = '3'
            flipV = true
            adjMap['adj1'] =
              ((100000 * (_end.bounds.r + CXN_MARGIN - _begin.x)) / extX) >> 0
          } else if (_end.bounds.t > _begin.y) {
            presetGeometry = '3'

            adjMap['adj1'] =
              (100000 + (100000 * (CXN_MARGIN / extX) + 0.5)) >> 0
          } else {
            if (_end.y <= _begin.y) {
              presetGeometry = '5'
              flipV = true
              adjMap['adj1'] =
                ((100000 * ((_end.bounds.l + _begin.bounds.r) / 2 - _begin.x)) /
                  extX +
                  0.5) >>
                0
              adjMap['adj2'] =
                (100000 +
                  (100000 * (_end.y - (_end.bounds.t - CXN_MARGIN))) / extY +
                  0.5) >>
                0
              adjMap['adj3'] =
                (100000 +
                  ((100000 * (_end.bounds.r + CXN_MARGIN - _end.x)) / extX +
                    0.5)) >>
                0
            } else {
              presetGeometry = '5'
              adjMap['adj1'] =
                ((100000 * ((_end.bounds.l + _begin.bounds.r) / 2 - _begin.x)) /
                  extX +
                  0.5) >>
                0
              adjMap['adj2'] =
                -(
                  (100000 * (_begin.y - (_end.bounds.t - CXN_MARGIN))) / extY +
                  0.5
                ) >> 0
              adjMap['adj3'] =
                (100000 +
                  ((100000 * (_end.bounds.r + CXN_MARGIN - _end.x)) / extX +
                    0.5)) >>
                0
            }
          }
        } else {
          if (extX === 0) {
            extX = 1
          }
          if (extY === 0) {
            extY = 1
          }
          if (
            _end.x >= _begin.bounds.l ||
            _end.y > _begin.bounds.b ||
            _end.y < _begin.bounds.t
          ) {
            if (_end.y < _begin.y) {
              if (_end.x < _begin.x) {
                flipH = true
                flipV = true
                presetGeometry = '3'
                adjMap['adj1'] =
                  -(
                    (100000 *
                      (Math.max(_end.bounds.r, _begin.bounds.r) +
                        CXN_MARGIN -
                        _begin.x)) /
                      extX +
                    0.5
                  ) >> 0
              } else {
                flipV = true
                presetGeometry = '3'
                adjMap['adj1'] =
                  (100000 +
                    ((100000 *
                      (Math.max(_end.bounds.r, _begin.bounds.r) +
                        CXN_MARGIN -
                        _end.x)) /
                      extX +
                      0.5)) >>
                  0
              }
            } else {
              if (_end.x < _begin.x) {
                flipH = true
                presetGeometry = '3'
                adjMap['adj1'] =
                  -(
                    (100000 *
                      (Math.max(_end.bounds.r, _begin.bounds.r) +
                        CXN_MARGIN -
                        _begin.x)) /
                      extX +
                    0.5
                  ) >> 0
              } else {
                presetGeometry = '3'
                adjMap['adj1'] =
                  (100000 +
                    ((100000 *
                      (Math.max(_end.bounds.r, _begin.bounds.r) +
                        CXN_MARGIN -
                        _end.x)) /
                      extX +
                      0.5)) >>
                  0
              }
            }
          } else {
            if (_end.y >= _begin.y) {
              presetGeometry = '5'
              flipH = true
              adjMap['adj1'] = -((100000 * (CXN_MARGIN / extX) + 0.5) >> 0)
              adjMap['adj2'] =
                (100000 +
                  (100000 * (_begin.bounds.b + CXN_MARGIN - _end.y)) / extY +
                  0.5) >>
                0
              adjMap['adj3'] =
                ((100000 *
                  (_begin.x - (_end.bounds.r + _begin.bounds.l) / 2.0)) /
                  extX +
                  0.5) >>
                0
            } else {
              presetGeometry = '5'
              flipH = true
              flipV = true
              adjMap['adj1'] = -((100000 * (CXN_MARGIN / extX) + 0.5) >> 0)
              adjMap['adj2'] =
                -(
                  (100000 * (_begin.bounds.b + CXN_MARGIN - _begin.y)) / extY +
                  0.5
                ) >> 0
              adjMap['adj3'] =
                ((100000 *
                  (_begin.x - (_end.bounds.r + _begin.bounds.l) / 2.0)) /
                  extX +
                  0.5) >>
                0
            }
          }
        }
        break
      }
    }
    presetGeometry = prefix + presetGeometry
  }
  const _posX = (end.x + begin.x) / 2.0 - extX / 2.0
  const _posY = (end.y + begin.y) / 2.0 - extY / 2.0
  const _ang = normalizeRotValue(rot - angle)

  const geomertry = createGeometry(presetGeometry)
  for (const key in adjMap) {
    if (adjMap.hasOwnProperty(key)) {
      geomertry.addAdjValue(key, adjMap[key])
    }
  }
  spPr.setGeometry(geomertry)
  geomertry.setParent(spPr)
  xfrm.setOffX(_posX)
  xfrm.setOffY(_posY)
  xfrm.setExtX(extX)
  xfrm.setExtY(extY)
  if (isStraightLineConnector) {
    flipH = _begin.x > _end.x
    flipV = _begin.y > _end.y
    xfrm.setRot(0)
  } else {
    // 'bentConnector' | 'curvedConnector'
    xfrm.setRot(_ang)
  }

  xfrm.setFlipH(flipH)
  xfrm.setFlipV(flipV)
  restoreRecordChangesState(s)
  return spPr
}

export function calcConnectionInfoByXY(
  info: ConnectorInfo,
  x: number,
  y: number
) {
  const connectInfo = new ConnectorInfo()
  connectInfo.x = x
  connectInfo.y = y
  connectInfo.bounds.fromBounds(new GraphicBounds(x, y, x, y))

  const diffX = x - info.x
  const diffY = y - info.y

  if (Math.abs(diffX) > Math.abs(diffY)) {
    if (diffX < 0) {
      connectInfo.dir = ABS_DIRECTION.E
    } else {
      connectInfo.dir = ABS_DIRECTION.W
    }
  } else {
    if (diffY < 0) {
      connectInfo.dir = ABS_DIRECTION.S
    } else {
      connectInfo.dir = ABS_DIRECTION.N
    }
  }
  return connectInfo
}
