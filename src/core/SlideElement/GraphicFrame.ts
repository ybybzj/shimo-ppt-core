// Import

import { EditActionFlag } from '../EditActionFlag'
import { idGenerator } from '../../globals/IdGenerator'
import { EditorUtil } from '../../globals/editor'
import { SlideElementBase } from './SlideElementBase'
import { Nullable } from '../../../liber/pervasive'

import { isDict } from '../../common/utils'
import { PointerEventInfo } from '../../common/dom/events'
import {
  Slide_Default_Width,
  Slide_Default_Height,
} from '../common/const/drawing'
import { InstanceType, isInstanceTypeOf } from '../instanceTypes'

import { getPresentation } from '../utilities/finders'
import { hookManager, Hooks } from '../hooks'

import { getTextStylesForSp } from '../utilities/shape/style'
import { Table } from '../Table/Table'
import { HoverInfo } from '../../ui/states/type'
import { SpPr, UniNvPr } from './attrs/shapePrs'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { getParentsOfElement } from '../utilities/shape/getters'
import { gDefaultColorMap } from '../color/clrMap'
import {
  clearParagraphFormattingForTextDocument,
  setParagraphAlignForTextDocument,
  setParagraphSpacingForTextDocument,
} from '../TextDocument/utils/operations'
import { Matrix2D } from '../graphic/Matrix'
import { setPropForTable } from '../utilities/table'
import { SlideElementParent } from '../Slide/type'
import {
  CalcStatus,
  GraphicFrameCalcStatusCode,
  GraphicFrameCalcStatusFlags,
  initCalcStatusCodeForGraphicFrame,
} from '../calculation/updateCalcStatus/calcStatus'
import { TableProp } from '../properties/tableProp'
import { TextOperationDirection } from '../utilities/type'
import { JCAlignTypeValues } from '../../io/dataType/style'
import { ParaSpacingOptions } from '../textAttributes/ParaSpacing'
import { updateCalcStatusForGraphicFrame } from '../calculation/updateCalcStatus/graphicFrame'

export class GraphicFrame extends SlideElementBase {
  readonly instanceType = InstanceType.GraphicFrame
  graphicObject: Nullable<Table>
  calcStatusCode!: GraphicFrameCalcStatusCode
  textTransform: Nullable<Matrix2D>
  base64Img: Nullable<string>
  base64ImgWidth: Nullable<number>
  base64ImgHeight: Nullable<number>
  invertTextTransform: Nullable<Matrix2D>
  isRightButtonPressed: boolean = false
  nvGraphicFramePr: Nullable<UniNvPr>
  constructor() {
    super()
    this.graphicObject = null
    this.nvGraphicFramePr = null

    this.id = idGenerator.newId()
    EditorUtil.entityRegistry.add(this, this.id)

    this.resetCalcStatus()
  }

  resetCalcStatus() {
    this.calcStatusCode =
      initCalcStatusCodeForGraphicFrame as GraphicFrameCalcStatusCode
  }

  setCalcStatusOf(flag: GraphicFrameCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode |
      flag) as GraphicFrameCalcStatusCode
  }

  unsetCalcStatusOf(flag: GraphicFrameCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode ^
      flag) as GraphicFrameCalcStatusCode
  }

  isCalcStatusSet(flag: GraphicFrameCalcStatusFlags) {
    return (this.calcStatusCode & flag) > 0
  }

  getTheme() {
    return getParentsOfElement(this).theme
  }
  getColorMap() {
    const parentByTypes = getParentsOfElement(this)
    if (parentByTypes.slide && parentByTypes.slide.clrMap) {
      return parentByTypes.slide.clrMap
    } else if (parentByTypes.layout && parentByTypes.layout.clrMap) {
      return parentByTypes.layout.clrMap
    } else if (parentByTypes.master && parentByTypes.master.clrMap) {
      return parentByTypes.master.clrMap
    }
    return gDefaultColorMap
  }

  getTextTransformOfParent() {
    return this.textTransform
  }

  setSpPr(spPr: Nullable<SpPr>) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_GraphicFrameSetSpPr,
      this.spPr,
      spPr
    )
    this.spPr = spPr
    updateCalcStatusForGraphicFrame(this)
  }

  setGraphicObject(graphicObject: Table) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_GraphicFrameSetGraphicObject,
      this.graphicObject,
      graphicObject
    )
    this.graphicObject = graphicObject
    if (this.graphicObject) {
      this.graphicObject.index = 0
      this.graphicObject.parent = this
    }
    updateCalcStatusForGraphicFrame(this)
  }

  setNvSpPr(pr: Nullable<UniNvPr>) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_GraphicFrameSetSetNvSpPr,
      this.nvGraphicFramePr,
      pr
    )
    this.nvGraphicFramePr = pr
  }

  setParent(parent: Nullable<SlideElementParent>) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_GraphicFrameSetSetParent,
      this.parent,
      parent
    )
    this.parent = parent
  }

  clearFormattingForParagraph(
    isFormatParaPr: boolean,
    isFormatTextPr: boolean
  ) {
    if (isDict(this.graphicObject)) {
      this.graphicObject.applyTextDocFunction(
        clearParagraphFormattingForTextDocument,
        [isFormatParaPr, isFormatTextPr]
      )
      this.setCalcStatusOf(CalcStatus.GraphicFrame.Content)
      this.setCalcStatusOf(CalcStatus.GraphicFrame.TextTransform)
    }
  }

  setProps(props: TableProp) {
    if (this.graphicObject) {
      setPropForTable(this.graphicObject, props)
      this.setCalcStatusOf(CalcStatus.GraphicFrame.Sizes)
      this.setCalcStatusOf(CalcStatus.GraphicFrame.Transform)
    }
  }

  getCursorInfoByPos(
    x: number,
    y: number,
    _e: PointerEventInfo
  ): Nullable<HoverInfo> {
    const tx = this.invertTransform.XFromPoint(x, y)
    const ty = this.invertTransform.YFromPoint(x, y)
    return this.graphicObject!.getCursorInfoByPos(tx, ty)
  }

  isOnlyOneElementSelected() {
    return true
  }

  isShape() {
    return false
  }

  isImage() {
    return false
  }

  isTable() {
    return isInstanceTypeOf(this.graphicObject, InstanceType.Table)
  }

  getTransform() {
    return this.transform
  }

  getSlideLimits() {
    return {
      x: 0,
      y: 0,
      xLimit: Slide_Default_Width,
      yLimit: Slide_Default_Height,
    }
  }

  getParentSlideIndex(): number {
    if (this.parent?.instanceType === InstanceType.Slide) {
      return this.parent.index!
    }
    return 0
  }

  Select() {}

  selectSelf() {
    const presentation = getPresentation(this)
    if (this.parent?.instanceType === InstanceType.Slide && presentation) {
      const selectionState = presentation.selectionState
      selectionState.reset(true)
      if (presentation.currentSlideIndex !== this.parent.index) {
        hookManager.invoke(Hooks.EditorUI.OnGoToSlide, {
          slideIndex: this.parent.index!,
        })
      }
      // 暂定 notes 不可插入表格
      presentation.isNotesFocused = false
      selectionState.setTextSelection(this)
    }
  }

  remove(
    dir: TextOperationDirection,
    isOnlyText?: boolean,
    isRemoveOnlySelection?: boolean
  ) {
    this.graphicObject!.remove(dir, isOnlyText, isRemoveOnlySelection)
    this.setCalcStatusOf(CalcStatus.GraphicFrame.Sizes)
    this.setCalcStatusOf(CalcStatus.GraphicFrame.Transform)
  }

  setAlignForParagraph(val: undefined | JCAlignTypeValues) {
    if (isDict(this.graphicObject)) {
      this.graphicObject.applyTextDocFunction(
        setParagraphAlignForTextDocument,
        [val]
      )
      this.setCalcStatusOf(CalcStatus.GraphicFrame.Content)
      this.setCalcStatusOf(CalcStatus.GraphicFrame.Transform)
    }
  }

  setSpacingForParagraph(val: ParaSpacingOptions) {
    if (isDict(this.graphicObject)) {
      this.graphicObject.applyTextDocFunction(
        setParagraphSpacingForTextDocument,
        [val]
      )
    }
  }

  getTextStyles(level: number) {
    return getTextStylesForSp(this, level)
  }

  isValid() {
    if (!this.graphicObject) {
      return false
    }
    return true
  }
  isCurrentSelected() {
    const selectionState = getPresentation(this)?.selectionState
    if (selectionState) {
      return selectionState.selectedTextSp === this
    }

    return false
  }
}
