import { valuesOfDict } from '../../../liber/pervasive'

export const radFactor = Math.PI / (60000 * 180)

export const degFactor = 1 / radFactor

// placeholder types
export const PlaceholderType = {
  body: 0,
  chart: 1,
  clipArt: 2, //(Clip Art)
  ctrTitle: 3, //(Centered Title)
  dgm: 4, //(Diagram)
  dt: 5, //(Date and Time)
  ftr: 6, //(Footer)
  hdr: 7, //(Header)
  media: 8, //(Media)
  obj: 9, //(Object)
  pic: 10, //(Picture)
  sldImg: 11, //(Slide Image)
  sldNum: 12, //(Slide Number)
  subTitle: 13, //(Subtitle)
  tbl: 14, //(Table)
  title: 15, //(Title)
} as const

export const FontStyleInd = { none: 2, major: 0, minor: 1 } as const
export type FontStyleIndValues = valuesOfDict<typeof FontStyleInd>
export const VerticalAnchorType = {
  BOTTOM: 0,
  CENTER: 1,
  DISTRIBUTED: 2,
  JUSTIFIED: 3,
  TOP: 4,
} as const
//Vertical Text Types
export const TextVerticalType = {
  eaVert: 0, // Vertical
  horz: 1, // Horizontal
  mongolianVert: 2, // Mongolian Vertical
  vert: 3, // Rotate 90
  vert270: 4, // Rotate 270
  wordArtVert: 5, // Stacked
  wordArtVertRtl: 6, // Vertical WordArt Right to Left
} as const
//Text Wrapping Types
export const TextWarpType = { NONE: 0, SQUARE: 1 } as const

export const TextAutoFitType = { NO: 0, SHAPE: 1, NORMAL: 2 } as const
export const BulletColorType = { NONE: 0, CLRTX: 1, CLR: 2 } as const
export const BulletSizeType = { NONE: 0, TX: 1, PCT: 2, PTS: 3 } as const
export const BulletTypefaceType = { NONE: 0, TX: 1, BUFONT: 2 } as const

// PrstColor
export const PresetColorNameValueMap = {
  ['aliceBlue']: 0xf0f8ff,
  ['antiqueWhite']: 0xfaebd7,
  ['aqua']: 0x00ffff,
  ['aquamarine']: 0x7fffd4,
  ['azure']: 0xf0ffff,
  ['beige']: 0xf5f5dc,
  ['bisque']: 0xffe4c4,
  ['black']: 0x000000,
  ['blanchedAlmond']: 0xffebcd,
  ['blue']: 0x0000ff,
  ['blueViolet']: 0x8a2be2,
  ['brown']: 0xa52a2a,
  ['burlyWood']: 0xdeb887,
  ['cadetBlue']: 0x5f9ea0,
  ['chartreuse']: 0x7fff00,
  ['chocolate']: 0xd2691e,
  ['coral']: 0xff7f50,
  ['cornflowerBlue']: 0x6495ed,
  ['cornsilk']: 0xfff8dc,
  ['crimson']: 0xdc143c,
  ['cyan']: 0x00ffff,
  ['dkBlue']: 0x00008b,
  ['dkCyan']: 0x008b8b,
  ['dkGoldenrod']: 0xb8860b,
  ['dkGray']: 0xa9a9a9,
  ['dkGreen']: 0x006400,
  ['dkKhaki']: 0xbdb76b,
  ['dkMagenta']: 0x8b008b,
  ['dkOliveGreen']: 0x556b2f,
  ['dkOrange']: 0xff8c00,
  ['dkOrchid']: 0x9932cc,
  ['dkRed']: 0x8b0000,
  ['dkSalmon']: 0xe9967a,
  ['dkSeaGreen']: 0x8fbc8b,
  ['dkSlateBlue']: 0x483d8b,
  ['dkSlateGray']: 0x2f4f4f,
  ['dkTurquoise']: 0x00ced1,
  ['dkViolet']: 0x9400d3,
  ['deepPink']: 0xff1493,
  ['deepSkyBlue']: 0xbfff,
  ['dimGray']: 0x696969,
  ['dodgerBlue']: 0x1e90ff,
  ['firebrick']: 0xb22222,
  ['floralWhite']: 0xfffaf0,
  ['forestGreen']: 0x228b22,
  ['fuchsia']: 0xff00ff,
  ['gainsboro']: 0xdcdcdc,
  ['ghostWhite']: 0xf8f8ff,
  ['gold']: 0xffd700,
  ['goldenrod']: 0xdaa520,
  ['gray']: 0x808080,
  ['green']: 0x008000,
  ['greenYellow']: 0xadff2f,
  ['honeydew']: 0xf0fff0,
  ['hotPink']: 0xff69b4,
  ['indianRed']: 0xcd5c5c,
  ['indigo']: 0x4b0082,
  ['ivory']: 0xfffff0,
  ['khaki']: 0xf0e68c,
  ['lavender']: 0xe6e6fa,
  ['lavenderBlush']: 0xfff0f5,
  ['lawnGreen']: 0x7cfc00,
  ['lemonChiffon']: 0xfffacd,
  ['ltBlue']: 0xadd8e6,
  ['ltCoral']: 0xf08080,
  ['ltCyan']: 0xe0ffff,
  ['ltGoldenrodYellow']: 0xfafa78,
  ['ltGray']: 0xd3d3d3,
  ['ltGreen']: 0x90ee90,
  ['ltPink']: 0xffb6c1,
  ['ltSalmon']: 0xffa07a,
  ['ltSeaGreen']: 0x20b2aa,
  ['ltSkyBlue']: 0x87cefa,
  ['ltSlateGray']: 0x778899,
  ['ltSteelBlue']: 0xb0c4de,
  ['ltYellow']: 0xffffe0,
  ['lime']: 0xff00,
  ['limeGreen']: 0x32cd32,
  ['linen']: 0xfaf0e6,
  ['magenta']: 0xff00ff,
  ['maroon']: 0x800000,
  ['medAquamarine']: 0x66cdaa,
  ['medBlue']: 0x0000cd,
  ['medOrchid']: 0xba55d3,
  ['medPurple']: 0x9370db,
  ['medSeaGreen']: 0x3cb371,
  ['medSlateBlue']: 0x7b68ee,
  ['medSpringGreen']: 0x00fa9a,
  ['medTurquoise']: 0x48d1cc,
  ['medVioletRed']: 0xc71585,
  ['midnightBlue']: 0x191970,
  ['mintCream']: 0xf5fffa,
  ['mistyRose']: 0xffe4ff,
  ['moccasin']: 0xffe4b5,
  ['navajoWhite']: 0xffdead,
  ['navy']: 0x000080,
  ['oldLace']: 0xfdf5e6,
  ['olive']: 0x808000,
  ['oliveDrab']: 0x6b8e23,
  ['orange']: 0xffa500,
  ['orangeRed']: 0xff4500,
  ['orchid']: 0xda70d6,
  ['paleGoldenrod']: 0xeee8aa,
  ['paleGreen']: 0x98fb98,
  ['paleTurquoise']: 0xafeeee,
  ['paleVioletRed']: 0xdb7093,
  ['papayaWhip']: 0xffefd5,
  ['peachPuff']: 0xffdab9,
  ['peru']: 0xcd853f,
  ['pink']: 0xffc0cb,
  ['plum']: 0xd3a0d3,
  ['powderBlue']: 0xb0e0e6,
  ['purple']: 0x800080,
  ['red']: 0xff0000,
  ['rosyBrown']: 0xbc8f8f,
  ['royalBlue']: 0x4169e1,
  ['saddleBrown']: 0x8b4513,
  ['salmon']: 0xfa8072,
  ['sandyBrown']: 0xf4a460,
  ['seaGreen']: 0x2e8b57,
  ['seaShell']: 0xfff5ee,
  ['sienna']: 0xa0522d,
  ['silver']: 0xc0c0c0,
  ['skyBlue']: 0x87ceeb,
  ['slateBlue']: 0x6a5aeb,
  ['slateGray']: 0x708090,
  ['snow']: 0xfffafa,
  ['springGreen']: 0x00ff7f,
  ['steelBlue']: 0x4682b4,
  ['tan']: 0xd2b48c,
  ['teal']: 0x008080,
  ['thistle']: 0xd8bfd8,
  ['tomato']: 0xff7347,
  ['turquoise']: 0x40e0d0,
  ['violet']: 0xee82ee,
  ['wheat']: 0xf5deb3,
  ['white']: 0xffffff,
  ['whiteSmoke']: 0xf5f5f5,
  ['yellow']: 0xffff00,
  ['yellowGreen']: 0x9acd32,
} as const

export type PresetColorValues = keyof typeof PresetColorNameValueMap

export const LineEndType = {
  None: 0,
  Arrow: 1,
  Diamond: 2,
  Oval: 3,
  Stealth: 4,
  Triangle: 5,
} as const

export const LineEndWidthTypes = {
  Large: 0,
  Mid: 1,
  Small: 2,
} as const

export const LineJoinType = {
  Empty: 0,
  Round: 1,
  Bevel: 2,
  Miter: 3,
} as const

export const LayoutType = {
  Title: 18, // ( Title ))
  Obj: 9, //Title and Object)
  Tx: 26, // ( Text ))
  Blank: 0, // Blank ))
} as const
export const TextOverflowType = { Clip: 0, Ellipsis: 1, Overflow: 2 } as const

export const BulletTypeValue = {
  NONE: 0,
  CHAR: 1,
  AUTONUM: 2,
  BLIP: 3,
} as const

export const ShapeLocks = {
  noGrp: 1,
  noUngrp: 4,
  noSelect: 16,
  noRot: 64,
  noModifyAspect: 256,
  noMove: 1024,
  noResize: 4096,
  noEditPoints: 16384,
  noAdjustHandles: 65536,
  noChangeArrowheads: 262144,
  noChangeShapeType: 1048576,
  noDrilldown: 4194304,
  noTextEdit: 8388608,
  noCrop: 16777216,
} as const

export const PlaceholderTexts = [
  'Slide text', // PlaceholderType.body
  'Chart', // PlaceholderType.chart
  'Clip Art', // PlaceholderType.clipArt
  'Slide title', // PlaceholderType.ctrTitle
  'Diagram', // PlaceholderType.dgm
  'Date and time', // PlaceholderType.dt
  'Footer', // PlaceholderType.ftr
  'Header', // PlaceholderType.hdr
  'Media', // PlaceholderType.media
  'Slide text', // PlaceholderType.obj
  'Picture', // PlaceholderType.pic
  'Image', // PlaceholderType.sldImg
  'Slide number', // PlaceholderType.sldNum
  'Slide subtitle', // PlaceholderType.subTitle
  'Table', // PlaceholderType.tbl
  'Slide title', // PlaceholderType.title
] as const
