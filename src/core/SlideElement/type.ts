import { Shape } from './Shape'
import { OleObject } from './OleObject'
import { ImageShape } from './Image'
import { GroupShape } from './GroupShape'
import { ConnectionShape } from './ConnectionShape'
import { GraphicFrame } from './GraphicFrame'
import { ChartObject } from './ChartObject'

export type SlideElement =
  | Shape
  | ConnectionShape
  | ImageShape
  | OleObject
  | ChartObject
  | GroupShape
  | GraphicFrame

export type TextSlideElement = Shape | GraphicFrame
