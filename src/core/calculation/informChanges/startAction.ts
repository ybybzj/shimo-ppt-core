import { EditActionFlag } from '../../EditActionFlag'
import { EditorUtil } from '../../../globals/editor'

export function startEditAction(
  actionType: EditActionFlag,
  ignoreUndoRedo?: boolean
) {
  EditorUtil.ChangesStack.createNewAction(actionType, ignoreUndoRedo)
}

export function setIgnoreUndoRedo(v: boolean) {
  EditorUtil.ChangesStack.setIgnoreUndoRedo(v)
}
