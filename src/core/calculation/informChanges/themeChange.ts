import { EditActionFlag } from '../../EditActionFlag'
import { EditorUtil } from '../../../globals/editor'
import { ThemeChanges } from '../../changes/otherChanges'
import { Presentation } from '../../Slide/Presentation'

export function informThemeChanges(
  targetEntity: Presentation,
  type: EditActionFlag,
  indexes
) {
  EditorUtil.ChangesStack.addToAction(
    new ThemeChanges(targetEntity, type, indexes)
  )
}
