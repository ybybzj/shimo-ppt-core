import { EditActionFlag } from '../../EditActionFlag'
import { EditorUtil } from '../../../globals/editor'
import { EntityContentChange } from '../../changes/contentChange'
import { EntityForChange, EntityObject } from '../../changes/type'

export function informEntityChildrenChange(
  targetEntity: EntityForChange,
  type: EditActionFlag,
  index: number,
  items: EntityObject[],
  isAdd: boolean
) {
  EditorUtil.ChangesStack.addToAction(
    new EntityContentChange(targetEntity, type, index, items, isAdd)
  )
}
