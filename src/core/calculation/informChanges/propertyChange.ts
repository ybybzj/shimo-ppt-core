import { EditActionFlag } from '../../EditActionFlag'
import { EditorUtil } from '../../../globals/editor'
import { EntityPropertyChange } from '../../changes/propertyChange'
import { EntityForChange } from '../../changes/type'

export function informEntityPropertyChange<T>(
  targetEntity: EntityForChange,
  type: EditActionFlag,
  oldPr: T,
  newPr: T
) {
  EditorUtil.ChangesStack.addToAction(
    new EntityPropertyChange(targetEntity, type, oldPr, newPr)
  )
}
