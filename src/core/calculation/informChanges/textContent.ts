import { EditorUtil } from '../../../globals/editor'
import { EditActionFlag } from '../../EditActionFlag'
import { InstanceType } from '../../instanceTypes'
import { EntityEditChange } from '../../changes/editChange'
import { TextDocument } from '../../TextDocument/TextDocument'

import { getParentDoc, TextContentItem } from '../utils'

export function informDocChange(docItem: TextContentItem | TextDocument) {
  const doc = getParentDoc(docItem)

  const lastChange = EditorUtil.ChangesStack.peekLastChange()
  if (doc != null && lastChange?.entity === doc) {
    return
  }
  const entity = doc ? doc : docItem // docItem may has not been added into ppt yet
  switch (docItem.instanceType) {
    case InstanceType.TextDocument:
    case InstanceType.ParaTextPr:
    case InstanceType.ParaHyperlink:
    case InstanceType.Paragraph: {
      EditorUtil.ChangesStack.addToAction(
        new EntityEditChange(entity, EditActionFlag.edit_TextContent_Change)
      )
      break
    }
    case InstanceType.ParaRun: {
      if (docItem.startParaLine === -1) {
        return
      }
      EditorUtil.ChangesStack.addToAction(
        new EntityEditChange(entity, EditActionFlag.edit_TextContent_Change)
      )
      break
    }
  }
}
