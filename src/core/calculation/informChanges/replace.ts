import { EditActionFlag } from '../../EditActionFlag'
import { EditorUtil } from '../../../globals/editor'
import { ReplaceChange } from '../../changes/replaceChange'
import { EntityForChange } from '../../changes/type'

export function informReplaceChange(
  targetEntity: EntityForChange,
  type: EditActionFlag,
  actionType: EditActionFlag
) {
  EditorUtil.ChangesStack.addToAction(
    new ReplaceChange(targetEntity, type, actionType)
  )
}
