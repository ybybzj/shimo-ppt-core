import { EditActionFlag } from '../../EditActionFlag'
import { EditorUtil } from '../../../globals/editor'
import { EntityEditChange } from '../../changes/editChange'
import { EntityForChange } from '../../changes/type'

export function informEntityEditChange(
  targetEntity: EntityForChange,
  type: EditActionFlag
) {
  EditorUtil.ChangesStack.addToAction(new EntityEditChange(targetEntity, type))
}
