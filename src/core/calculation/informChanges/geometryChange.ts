import { EditActionFlag } from '../../EditActionFlag'
import { EditorUtil } from '../../../globals/editor'
import { GeometryEditChange } from '../../changes/otherChanges'
import { Geometry } from '../../SlideElement/geometry/Geometry'

export function informGeometryEditChange(
  targetEntity: Geometry,
  type: EditActionFlag,
  extraData: any[]
) {
  EditorUtil.ChangesStack.addToAction(
    new GeometryEditChange(targetEntity, type, extraData)
  )
}
