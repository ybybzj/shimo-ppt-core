import { Nullable } from '../../../liber/pervasive'
import { InstanceType } from '../instanceTypes'
import { ParaHyperlink } from '../Paragraph/ParaContent/ParaHyperlink'
import { ParaRun } from '../Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../Paragraph/Paragraph'
import { ParaTextPr } from '../Paragraph/ParaContent/ParaTextPr'
import { TextDocument } from '../TextDocument/TextDocument'

export type TextContentItem = ParaHyperlink | Paragraph | ParaRun | ParaTextPr

export function getParentDoc(
  child: Nullable<TextContentItem | TextDocument>
): Nullable<TextDocument> {
  if (child == null || child.instanceType == null) {
    return undefined
  }
  switch (child.instanceType) {
    case InstanceType.TextDocument: {
      return child
    }
    case InstanceType.ParaHyperlink:
    case InstanceType.ParaRun: {
      return getParentDoc(child.paragraph)
    }
    case InstanceType.Paragraph: {
      return child.parent
    }

    case InstanceType.ParaTextPr: {
      return getParentDoc(child.parent)
    }
  }
  return undefined
}
