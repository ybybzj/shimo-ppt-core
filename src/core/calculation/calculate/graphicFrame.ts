import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../common/utils'
import { GraphicFrame } from '../../SlideElement/GraphicFrame'
import { calcTransform } from '../../utilities/shape/calcs'
import { CalcStatus } from '../updateCalcStatus/calcStatus'
import { calculateTableState } from './table'
import { calcShapeSnapXYs } from './utils'

export function calculateForGraphicFrame(sp: GraphicFrame) {
  if (sp.isDeleted || !sp.parent) return
  const s = turnOffRecordChanges()
  if (sp.isCalcStatusSet(CalcStatus.GraphicFrame.Table)) {
    calculateTableForGraphicFrame(sp)
    sp.unsetCalcStatusOf(CalcStatus.GraphicFrame.Table)
  }
  if (sp.isCalcStatusSet(CalcStatus.GraphicFrame.Sizes)) {
    calculateSizesForGraphicFrame(sp)
    sp.unsetCalcStatusOf(CalcStatus.GraphicFrame.Sizes)
  }
  if (sp.isCalcStatusSet(CalcStatus.GraphicFrame.Transform)) {
    calcTransform(sp)
    calcShapeSnapXYs(sp)
    sp.unsetCalcStatusOf(CalcStatus.GraphicFrame.Transform)
    sp.textTransform = sp.transform
    sp.invertTextTransform = sp.invertTransform
    sp.base64Img = null
    sp.bounds.l = sp.x
    sp.bounds.t = sp.y
    sp.bounds.r = sp.x + sp.extX
    sp.bounds.b = sp.y + sp.extY
    sp.bounds.x = sp.x
    sp.bounds.y = sp.y
    sp.bounds.w = sp.extX
    sp.bounds.h = sp.extY
  }
  restoreRecordChangesState(s)
}

export function calculateTableForGraphicFrame(sp: GraphicFrame) {
  if (sp.graphicObject) {
    sp.graphicObject.parent = sp
    sp.graphicObject.resetDimension(0, 0, sp.extX, 10000)
    calculateTableState(sp.graphicObject)
  }
}
export function calculateSizesForGraphicFrame(sp: GraphicFrame) {
  if (sp.graphicObject) {
    sp.graphicObject.xLimit -= sp.graphicObject.x
    sp.graphicObject.x = 0
    sp.graphicObject.y = 0
    const { right, left, bottom, top } = sp.graphicObject.getDocumentBounds()
    sp.extX = right - left
    sp.extY = bottom - top
    // Todo, 后续统一采用一处数据源
    if (sp.spPr?.xfrm) {
      sp.spPr.xfrm.extX = sp.extX
      sp.spPr.xfrm.extY = sp.extY
    }
  }
}
