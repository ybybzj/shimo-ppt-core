import { SlideMaster } from '../../Slide/SlideMaster'
import { isPlaceholder } from '../../utilities/shape/asserts'
import { CalcStatus, unsetAllCalcStatus } from '../updateCalcStatus/calcStatus'
import { calculateSlideElement } from './slideElement'

export function calculateForSlideMaster(slideMaster: SlideMaster) {
  const spTree = slideMaster.cSld.spTree
  const spCount = spTree.length
  const isBoundsDirty = slideMaster.isCalcStatusSet(
    CalcStatus.SlideMaster.Bounds
  )
  if (isBoundsDirty) {
    slideMaster.bounds.reset(
      slideMaster.width + 100.0,
      slideMaster.height + 100.0,
      -100.0,
      -100.0
    )
  }
  let isChecked = false
  for (let i = 0; i < spCount; ++i) {
    const sp = spTree[i]
    if (!isPlaceholder(sp)) {
      calculateSlideElement(sp)
      if (isBoundsDirty) {
        slideMaster.bounds.checkByBounds(sp.bounds)
      }
      isChecked = true
    }
  }
  if (isBoundsDirty) {
    if (isChecked) {
      slideMaster.bounds.updateWidthAndHeight()
    } else {
      slideMaster.bounds.reset(0.0, 0.0, 0.0, 0.0)
    }
  }
  unsetAllCalcStatus(slideMaster)
}
