import { isNumber } from '../../../common/utils'
import { EditorUtil } from '../../../globals/editor'
import { InstanceType } from '../../instanceTypes'
import { TEXTWIDTH_PER_UNIT } from '../../common/const/paraContent'
import { checkAlmostEqual, isRotateIn45D } from '../../common/utils'
import { TextDocument } from '../../TextDocument/TextDocument'
import {
  calculateTextDocumentBySize,
  calculateTextDocumentByBounds,
} from '../../TextDocument/typeset/calculate'
import { ParaRun } from '../../Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../../Paragraph/Paragraph'
import { TextVerticalType, TextWarpType } from '../../SlideElement/const'
import { BodyPr } from '../../SlideElement/attrs/bodyPr'
import { Shape } from '../../SlideElement/Shape'
import {
  getTextDocumentOfSp,
  isEavertTextContent,
  getFinalRotate,
} from '../../utilities/shape/getters'
import { Nullable } from '../../../../liber/pervasive'
import { MaxWidthLimitForTextWarpNone } from '../../common/const/doc'

export interface ContentMetrics {
  w: number
  h: number
  contentHeight: number
  correctHeight: number
  correctWidth: number
  heightLimitInSp: number
  widthLimitInSp: number
}

export function isEqualContentMetrics(
  c1: Nullable<ContentMetrics>,
  c2: Nullable<ContentMetrics>
) {
  if (c1 === c2) return true
  if (c1 == null && c2 == null) return true
  if (c1 == null || c2 == null) return false

  return (
    checkAlmostEqual(c1.w, c2.w) &&
    checkAlmostEqual(c1.h, c2.h) &&
    checkAlmostEqual(c1.contentHeight, c2.contentHeight) &&
    checkAlmostEqual(c1.correctHeight, c2.correctHeight) &&
    checkAlmostEqual(c1.correctWidth, c2.correctWidth) &&
    checkAlmostEqual(c1.heightLimitInSp, c2.heightLimitInSp) &&
    checkAlmostEqual(c1.widthLimitInSp, c2.widthLimitInSp)
  )
}

export function calculateShapeContent(sp: Shape): ContentMetrics | null {
  const textDoc = getTextDocumentOfSp(sp)
  if (textDoc) {
    const bodyPr = sp.getBodyPr()
    const isEaVert = isEavertTextContent(sp.txBody)
    if (isEaVert) {
      EditorUtil.FontKit.isEaVertText = true
    }
    const metricsOfContent = calculateTextDocument(sp, textDoc, bodyPr)

    if (isEaVert) {
      EditorUtil.FontKit.isEaVertText = false
    }

    sp.contentWidth = metricsOfContent.w
    sp.contentHeight = metricsOfContent.contentHeight

    return metricsOfContent
  }
  return null
}

// helpers
function calculateTextDocument(
  sp: Shape,
  textDoc: TextDocument,
  bodyPr: BodyPr
): ContentMetrics {
  const result = {
    w: 0,
    h: 0,
    contentHeight: 0,
    correctHeight: 0,
    correctWidth: 0,
    heightLimitInSp: 0,
    widthLimitInSp: 0,
  }
  const l_ins = isNumber(bodyPr.lIns) ? bodyPr.lIns : 2.54
  const r_ins = isNumber(bodyPr.rIns) ? bodyPr.rIns : 2.54
  const t_ins = isNumber(bodyPr.tIns) ? bodyPr.tIns : 1.27
  const b_ins = isNumber(bodyPr.bIns) ? bodyPr.bIns : 1.27

  const textRect = sp.getTextRect()
  const w = textRect.r - textRect.l - (l_ins + r_ins)

  const h = textRect.b - textRect.t - (t_ins + b_ins)

  const isVert =
    bodyPr.vert === TextVerticalType.vert ||
    bodyPr.vert === TextVerticalType.vert270 ||
    bodyPr.vert === TextVerticalType.eaVert

  switch (bodyPr.wrap) {
    case TextWarpType.NONE:
      const maxWidth = MaxWidthLimitForTextWarpNone
      const contentWidth = calculateTextDocumentByMaxWidth(textDoc, maxWidth)

      if (!isVert) {
        result.correctWidth = l_ins + r_ins
        result.correctHeight = t_ins + b_ins
        result.widthLimitInSp = w
        result.heightLimitInSp = h
      } else {
        result.correctWidth = t_ins + b_ins
        result.correctHeight = l_ins + r_ins
        result.widthLimitInSp = h
        result.heightLimitInSp = w
      }
      calculateTextDocumentBySize(
        textDoc,
        contentWidth,
        h,
        result.widthLimitInSp
      )
      result.w = contentWidth + 0.001
      result.contentHeight = textDoc.getWholeHeight()
      result.h = result.contentHeight
      break
    default:
      if (!bodyPr.upright) {
        if (!isVert) {
          result.w = w + 0.001
          result.h = h + 0.001
          result.correctWidth = l_ins + r_ins
          result.correctHeight = t_ins + b_ins
        } else {
          result.w = h + 0.001
          result.h = w + 0.001
          result.correctWidth = t_ins + b_ins
          result.correctHeight = l_ins + r_ins
        }
      } else {
        const finalRotate = getFinalRotate(sp)
        if (isRotateIn45D(finalRotate)) {
          if (!isVert) {
            result.w = w + 0.001
            result.h = h + 0.001
            result.correctWidth = l_ins + r_ins
            result.correctHeight = t_ins + b_ins
          } else {
            result.w = h + 0.001
            result.h = w + 0.001
            result.correctWidth = t_ins + b_ins
            result.correctHeight = l_ins + r_ins
          }
        } else {
          if (!isVert) {
            result.w = h + 0.001
            result.h = w + 0.001
            result.correctWidth = t_ins + b_ins
            result.correctHeight = l_ins + r_ins
          } else {
            result.w = w + 0.001
            result.h = h + 0.001
            result.correctWidth = l_ins + r_ins
            result.correctHeight = t_ins + b_ins
          }
        }
      }
      result.widthLimitInSp = result.w
      result.heightLimitInSp = result.h

      calculateTextDocumentBySize(textDoc, result.w, result.h)

      result.contentHeight = textDoc.getWholeHeight()

      break
  }
  return result
}

function calculateTextDocumentByMaxWidth(
  textDoc: TextDocument,
  maxWidth: number
) {
  const maxWidthObj = { maxWidth: 0 }
  calculateTextDocumentByBounds(textDoc, 0, 0, maxWidth, 20000)

  calculateMaxWidth(textDoc.children.elements, maxWidthObj)
  if (maxWidthObj.maxWidth === 0) {
    if (textDoc.isEmpty()) {
      const endRun = textDoc.children.at(0)?.children.at(0) as ParaRun
      const pEnd = endRun?.children.at(0)
      if (pEnd) {
        return pEnd.renderWidth / TEXTWIDTH_PER_UNIT
      }
    }
    return 0.001
  }
  return maxWidthObj.maxWidth
}

function calculateMaxWidth(
  paragraphs: readonly Paragraph[],
  maxObj: { maxWidth: number }
) {
  paragraphs.forEach((paragraph) => {
    if (paragraph?.instanceType === InstanceType.Paragraph) {
      const renderingState = paragraph.renderingState
      for (let i = 0, l = renderingState.getLineCount(); i < l; i++) {
        const line = renderingState.getLine(i)
        if (line != null) {
          const { x, w } = line.segment
          if (x + w > maxObj.maxWidth) {
            maxObj.maxWidth = x + w
          }
        }
      }
    }
  })
}
