import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { isSlideElement } from '../../utilities/shape/asserts'
import { hasCalcStatusSet } from '../updateCalcStatus/calcStatus'
import { calculateForGraphicFrame } from './graphicFrame'
import { calculateForGroup } from './group'
import { calculateForImageShape } from './image'
import { calculateForShape } from './shape'

export function calculateSlideElement(sp: SlideElement) {
  if (!isSlideElement(sp) || !hasCalcStatusSet(sp)) {
    return
  }
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      calculateForShape(sp)
      break
    }
    case InstanceType.ImageShape: {
      calculateForImageShape(sp)
      break
    }
    case InstanceType.GroupShape: {
      calculateForGroup(sp)
      break
    }
    case InstanceType.GraphicFrame: {
      calculateForGraphicFrame(sp)
      break
    }
  }
}
