import { isNumber } from '../../../common/utils'
import { InstanceType } from '../../instanceTypes'
import { calculateTextDocumentByBounds } from '../../TextDocument/typeset/calculate'
import { MatrixUtils } from '../../graphic/Matrix'
import { Slide } from '../../Slide/Slide'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { getTextDocumentOfSp } from '../../utilities/shape/getters'
import { calculateSlideElement } from './slideElement'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../common/utils'
import { Nullable } from '../../../../liber/pervasive'
import { calculatePlaceholderContent } from './shape'
import { CalcStatus, unsetAllCalcStatus } from '../updateCalcStatus/calcStatus'
import { Ln } from '../../SlideElement/attrs/line'
import { ColorRGBA } from '../../color/type'
import { Hooks, hookManager } from '../../hooks'
// import { updateFieldsForSlideElement } from '../../utilities/fields'

export function calculateForSlide(slide: Slide) {
  if (!slide.layout || !isNumber(slide.index)) {
    return
  }

  const calcStatus = CalcStatus.Slide

  if (slide.isCalcStatusSet(calcStatus.Background)) {
    calculateSlideBackground(slide)
  }
  if (slide.isCalcStatusSet(calcStatus.SpTree)) {
    calculateSpTreeOfSlide(slide)
  }
  if (slide.isCalcStatusSet(calcStatus.Notes)) {
    calculateNotesShape(slide)
  }

  unsetAllCalcStatus(slide)
  slide.base64Img = null
}

function calculateSlideBackground(slide: Slide) {
  let bgFill: Nullable<FillEffects>
  let rgba: undefined | ColorRGBA

  const layout = slide.layout
  const master = layout?.master
  const theme = master?.theme!
  if (slide.cSld.bg != null) {
    if (null != slide.cSld.bg.bgPr) bgFill = slide.cSld.bg.bgPr.fill
    else if (slide.cSld.bg.bgRef != null) {
      slide.cSld.bg.bgRef.color.updateBy(theme, slide, layout, master, rgba)
      rgba = slide.cSld.bg.bgRef.color.rgba

      bgFill = theme.themeElements.fmtScheme.getFillStyle(
        slide.cSld.bg.bgRef.idx,
        slide.cSld.bg.bgRef.color
      )
    }
  } else {
    if (layout != null) {
      if (layout.cSld.bg != null) {
        if (null != layout.cSld.bg.bgPr) {
          bgFill = layout.cSld.bg.bgPr.fill
        } else if (layout.cSld.bg.bgRef != null) {
          layout.cSld.bg.bgRef.color.updateBy(
            theme,
            slide,
            layout,
            master,
            rgba
          )
          rgba = layout.cSld.bg.bgRef.color.rgba

          bgFill = theme.themeElements.fmtScheme.getFillStyle(
            layout.cSld.bg.bgRef.idx,
            layout.cSld.bg.bgRef.color
          )
        }
      } else if (master != null) {
        if (master.cSld.bg != null) {
          if (null != master.cSld.bg.bgPr) {
            bgFill = master.cSld.bg.bgPr.fill
          } else if (master.cSld.bg.bgRef != null) {
            master.cSld.bg.bgRef.color.updateBy(
              theme,
              slide,
              layout,
              master,
              rgba
            )
            rgba = master.cSld.bg.bgRef.color.rgba
            bgFill = theme.themeElements.fmtScheme.getFillStyle(
              master.cSld.bg.bgRef.idx,
              master.cSld.bg.bgRef.color
            )
          }
        } else {
          bgFill = FillEffects.SolidRGBA(255, 255, 255, 255)
        }
      }
    }
  }

  if (bgFill != null) {
    bgFill.calculate(theme, slide, layout, master, rgba)
  }

  slide.backgroundFill = bgFill
}

function calculateSpTreeOfSlide(slide: Slide) {
  for (let i = 0; i < slide.cSld.spTree.length; ++i) {
    calculateSlideElement(slide.cSld.spTree[i])
  }
}

export function calculateNotesShape(slide: Slide) {
  const s = turnOffRecordChanges()
  if (!slide.notes) {
    slide.shapeOfNotes = null //ToDo: Create notes body shape
  } else {
    slide.shapeOfNotes = slide.notes.getShapeOfBodyPH()
    if (
      slide.shapeOfNotes &&
      slide.shapeOfNotes.instanceType !== InstanceType.Shape
    ) {
      slide.shapeOfNotes = null
    }
  }

  if (slide.shapeOfNotes) {
    slide.notes!.slide = slide

    const textDoc = getTextDocumentOfSp(slide.shapeOfNotes)
    let width
    if (textDoc) {
      // updateFieldsForSlideElement(slide.shapeOfNotes)
      slide.shapeOfNotes.textTransform.tx = 3
      slide.shapeOfNotes.textTransform.ty = 3
      slide.shapeOfNotes.invertTextTransform = MatrixUtils.invertMatrix(
        slide.shapeOfNotes.textTransform
      )
      width = hookManager.get(Hooks.EditorUI.GetWidthOfNotes)

      calculateTextDocumentByBounds(textDoc, 0, 0, width, 2000)

      slide.shapeOfNotes.contentWidth = width
      slide.shapeOfNotes.contentHeight = 2000
    }
    slide.shapeOfNotes.textTransformForPh = slide.shapeOfNotes.textTransform
    slide.shapeOfNotes.invertTextTransformForPh =
      slide.shapeOfNotes.invertTextTransform
    const oldGeometry = slide.shapeOfNotes.spPr!.geometry
    slide.shapeOfNotes.spPr!.geometry = undefined
    slide.shapeOfNotes.extX = width
    slide.shapeOfNotes.extY = 2000
    calculatePlaceholderContent(slide.shapeOfNotes)
    slide.shapeOfNotes.spPr!.geometry = oldGeometry
    slide.shapeOfNotes.pen = Ln.NoFill()
  }
  restoreRecordChangesState(s)
}
