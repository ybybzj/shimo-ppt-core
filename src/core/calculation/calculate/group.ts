import { InstanceType } from '../../instanceTypes'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../common/utils'
import { ConnectionShape } from '../../SlideElement/ConnectionShape'
import { GroupShape } from '../../SlideElement/GroupShape'
import {
  calculateBrush,
  calcTransform,
  calculateBounds,
} from '../../utilities/shape/calcs'
import { adjustXfrmOfGroup } from '../../utilities/shape/group'
import { CalcStatus } from '../updateCalcStatus/calcStatus'
import { calculateForGraphicFrame } from './graphicFrame'
import { calculateForImageShape } from './image'
import { calculateForShape } from './shape'
import { calculatePen, calcShapeSnapXYs } from './utils'

export function calculateForGroup(sp: GroupShape) {
  if (sp.isDeleted || !sp.parent) return
  const s = turnOffRecordChanges()
  const calcStatus = CalcStatus.GroupShape
  if (sp.isCalcStatusSet(calcStatus.Brush)) {
    calculateBrush(sp)
    sp.unsetCalcStatusOf(calcStatus.Brush)
  }
  if (sp.isCalcStatusSet(calcStatus.Pen)) {
    calculatePen(sp)
    sp.unsetCalcStatusOf(calcStatus.Pen)
  }
  if (sp.isCalcStatusSet(calcStatus.ChildrenObjects)) {
    sp.refreshSlideElementsInGroup()
    sp.unsetCalcStatusOf(calcStatus.ChildrenObjects)
    sp.setCalcStatusOf(calcStatus.Transform)
  }
  if (sp.isCalcStatusSet(calcStatus.Transform)) {
    adjustXfrmOfGroup(sp)
    calcTransform(sp)
    calcShapeSnapXYs(sp)
    sp.unsetCalcStatusOf(calcStatus.Transform)
  }

  const cxnSps: ConnectionShape[] = []
  for (let i = 0, l = sp.spTree.length; i < l; ++i) {
    const subSp = sp.spTree[i]
    switch (subSp.instanceType) {
      case InstanceType.Shape: {
        if (subSp.isConnectionShape()) {
          cxnSps.push(subSp as ConnectionShape)
        }
        calculateForShape(subSp)

        break
      }
      case InstanceType.ImageShape: {
        calculateForImageShape(subSp)
        break
      }
      case InstanceType.GroupShape: {
        calculateForGroup(subSp)
        break
      }
      case InstanceType.GraphicFrame: {
        calculateForGraphicFrame(subSp)
        break
      }
    }
  }

  if (sp.isCalcStatusSet(calcStatus.Bounds)) {
    calculateBounds(sp)
    sp.unsetCalcStatusOf(calcStatus.Bounds)
  }
  restoreRecordChangesState(s)
}
