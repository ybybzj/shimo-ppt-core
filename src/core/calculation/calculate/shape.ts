import { AlignKind } from '../../common/const/attrs'
import { isFiniteNumber, isNumber, isDict } from '../../../common/utils'
import { InstanceType, isInstanceTypeOf } from '../../instanceTypes'
import { EditorSettings } from '../../common/EditorSettings'
import {
  checkAlmostEqual,
  isRotateIn45D,
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../common/utils'
import { Matrix2D, MatrixUtils } from '../../graphic/Matrix'
import {
  TextAutoFitType,
  TextWarpType,
  TextVerticalType,
} from '../../SlideElement/const'
import { Shape } from '../../SlideElement/Shape'
import { TextPr } from '../../textAttributes/TextPr'
import { calculateTextDocumentBySize } from '../../TextDocument/typeset/calculate'
import {
  isPlaceholder,
  isPlaceholderWithEmptyContent,
} from '../../utilities/shape/asserts'
import { ensureSpPrXfrm } from '../../utilities/shape/attrs'
import {
  checkTextTransformMatrix,
  getTextDocumentOfSp,
  getFinalRotate,
} from '../../utilities/shape/getters'
import {
  calcTransformFormProps,
  calculateBounds,
  calculateBrush,
  calculateGeometryForSp,
  calcTransform,
  TransformProps,
} from '../../utilities/shape/calcs'
import { textPrOfFirstItem } from '../../utilities/tableOrTextDocContent/helpers'
import { getAdjustWHAndUpdateGeometry } from './geometry'
import { calculateShapeContent, isEqualContentMetrics } from './shapeContent'
import { calculatePen, calcShapeSnapXYs } from './utils'
import { CalcStatus } from '../updateCalcStatus/calcStatus'
import { calcScaleFactorsOfGroup } from '../../utilities/shape/group'
import { ManagedSliceUtil } from '../../../common/managedArray'

export function calculateForShape(sp: Shape, forceAutoFit: boolean = false) {
  if (sp.isDeleted || !sp.parent) return

  if (isInstanceTypeOf(sp.parent, InstanceType.Notes)) {
    return
  }
  const isCheckSlidePlaceholder =
    !isPlaceholder(sp) || isInstanceTypeOf(sp.parent, InstanceType.Slide)

  const canCheckAutoFit =
    isInstanceTypeOf(sp.parent, InstanceType.Slide) && !sp.isConnectionShape()
  const s = turnOffRecordChanges()

  const calcStatus = CalcStatus.Shape

  let calced: boolean = false

  if (sp.isCalcStatusSet(calcStatus.Brush)) {
    calculateBrush(sp)
    sp.unsetCalcStatusOf(calcStatus.Brush)
    calced = true
  }
  if (sp.isCalcStatusSet(calcStatus.Pen)) {
    calculatePen(sp)
    sp.unsetCalcStatusOf(calcStatus.Pen)
    calced = true
  }
  if (sp.isCalcStatusSet(calcStatus.Transform)) {
    calcTransform(sp)
    calcShapeSnapXYs(sp)
    sp.unsetCalcStatusOf(calcStatus.Transform)
    calced = true
  }
  if (sp.isCalcStatusSet(calcStatus.Geometry)) {
    calculateGeometryForSp(sp)
    sp.unsetCalcStatusOf(calcStatus.Geometry)
    calced = true
  }
  if (sp.isCalcStatusSet(calcStatus.Content) && isCheckSlidePlaceholder) {
    const oldMetrics = sp.calcInfo.metricsOfContent
    sp.calcInfo.metricsOfContent = calculateShapeContent(sp)
    if (!isEqualContentMetrics(oldMetrics, sp.calcInfo.metricsOfContent)) {
      sp.setCalcStatusOf(calcStatus.Bounds)
    }
    sp.unsetCalcStatusOf(calcStatus.Content)
    calced = true
  }
  if (
    sp.isCalcStatusSet(calcStatus.PlaceholderContent) &&
    isCheckSlidePlaceholder
  ) {
    calculatePlaceholderContent(sp)
    sp.unsetCalcStatusOf(calcStatus.PlaceholderContent)
    calced = true
  }
  if (sp.isCalcStatusSet(calcStatus.TextTransform) && isCheckSlidePlaceholder) {
    calculateTextTransform(sp)
    sp.unsetCalcStatusOf(calcStatus.TextTransform)
    calced = true
  }

  // adjust shape and geometry's drawing state by checking autoFit properties
  if (canCheckAutoFit && calced) {
    autoFitShape(sp, calcStatus, forceAutoFit)
    if (sp.isCalcStatusSet(calcStatus.Geometry)) {
      calculateGeometryForSp(sp)
      sp.unsetCalcStatusOf(calcStatus.Geometry)
    }
  }

  if (sp.isCalcStatusSet(calcStatus.Bounds)) {
    calculateBounds(sp)
    sp.unsetCalcStatusOf(calcStatus.Bounds)
  }
  restoreRecordChangesState(s)
}

function autoFitShape(sp: Shape, calcStatus, forceAutoFit = false) {
  if (forceAutoFit || sp.needAutofit()) {
    const metricsOfContent = sp.calcInfo.metricsOfContent

    if (metricsOfContent == null) {
      return
    }
    const bodyPr = sp.getBodyPr()

    const l_ins = isNumber(bodyPr?.lIns) ? bodyPr.lIns : 2.54
    const r_ins = isNumber(bodyPr?.rIns) ? bodyPr.rIns : 2.54
    const t_ins = isNumber(bodyPr?.tIns) ? bodyPr.tIns : 1.27
    const b_ins = isNumber(bodyPr?.bIns) ? bodyPr.bIns : 1.27

    const geometry = sp.spPr && sp.spPr.geometry
    const oldWidth = sp.extX
    const oldHeight = sp.extY
    let wAndH: {
      w: number
      h: number
      isInvalid: boolean
    }
    let deltaX = 0
    let deltaY = 0

    const isNotRect = sp.getGeometry()?.preset !== 'rect'
    const isEmptyDoc = getTextDocumentOfSp(sp)?.isEmpty({
      ignoreEnd: isNotRect,
    })
    const isExtExist =
      isFiniteNumber(sp.spPr!.xfrm?.extX) && isFiniteNumber(sp.spPr!.xfrm?.extY)
    const isAutoFit =
      isDict(bodyPr.textFit) &&
      bodyPr.textFit.type === TextAutoFitType.SHAPE &&
      !(isEmptyDoc && isExtExist)
    if (isAutoFit) {
      if (bodyPr.wrap === TextWarpType.NONE) {
        if (!bodyPr.upright) {
          if (
            !(
              bodyPr.vert === TextVerticalType.vert ||
              bodyPr.vert === TextVerticalType.vert270 ||
              bodyPr.vert === TextVerticalType.eaVert
            )
          ) {
            if (geometry) {
              wAndH = getAdjustWHAndUpdateGeometry(
                geometry,
                metricsOfContent.w + l_ins + r_ins,
                metricsOfContent.contentHeight + t_ins + b_ins,
                undefined,
                isAutoFit ? undefined : sp.extY
              )
              if (!wAndH.isInvalid) {
                sp.extX = wAndH.w
                sp.extY = wAndH.h
              }
            } else {
              sp.extX = metricsOfContent.w + l_ins + r_ins
              sp.extY = isAutoFit
                ? metricsOfContent.contentHeight + t_ins + b_ins
                : sp.extY
            }
          } else {
            if (geometry) {
              wAndH = getAdjustWHAndUpdateGeometry(
                geometry,
                metricsOfContent.contentHeight + l_ins + r_ins,
                metricsOfContent.w + t_ins + b_ins,
                isAutoFit ? undefined : sp.extX
              )
              if (!wAndH.isInvalid) {
                sp.extX = wAndH.w
                sp.extY = wAndH.h
              }
            } else {
              sp.extY = metricsOfContent.w + t_ins + b_ins
              sp.extX = isAutoFit
                ? metricsOfContent.contentHeight + l_ins + r_ins
                : sp.extX
            }
          }
        } else {
          const finalRotate = getFinalRotate(sp)
          if (isRotateIn45D(finalRotate)) {
            if (
              !(
                bodyPr.vert === TextVerticalType.vert ||
                bodyPr.vert === TextVerticalType.vert270 ||
                bodyPr.vert === TextVerticalType.eaVert
              )
            ) {
              if (geometry) {
                wAndH = getAdjustWHAndUpdateGeometry(
                  geometry,
                  metricsOfContent.w + l_ins + r_ins,
                  metricsOfContent.contentHeight + t_ins + b_ins,
                  undefined,
                  isAutoFit ? undefined : sp.extY
                )
                if (!wAndH.isInvalid) {
                  sp.extX = wAndH.w
                  sp.extY = wAndH.h
                }
              } else {
                sp.extX = metricsOfContent.w + l_ins + r_ins
                sp.extY = isAutoFit
                  ? metricsOfContent.contentHeight + t_ins + b_ins
                  : sp.extY
              }
            } else {
              if (geometry) {
                wAndH = getAdjustWHAndUpdateGeometry(
                  geometry,
                  metricsOfContent.contentHeight + l_ins + r_ins,
                  metricsOfContent.w + t_ins + b_ins,
                  isAutoFit ? undefined : sp.extX
                )
                if (!wAndH.isInvalid) {
                  sp.extX = wAndH.w
                  sp.extY = wAndH.h
                }
              } else {
                sp.extY = metricsOfContent.w + t_ins + b_ins
                sp.extX = isAutoFit
                  ? metricsOfContent.contentHeight + l_ins + r_ins
                  : sp.extX
              }
            }
          } else {
            if (
              !(
                bodyPr.vert === TextVerticalType.vert ||
                bodyPr.vert === TextVerticalType.vert270 ||
                bodyPr.vert === TextVerticalType.eaVert
              )
            ) {
              if (geometry) {
                wAndH = getAdjustWHAndUpdateGeometry(
                  geometry,
                  metricsOfContent.w + l_ins + r_ins,
                  metricsOfContent.contentHeight + t_ins + b_ins,
                  undefined,
                  isAutoFit ? undefined : sp.extY
                )
                if (!wAndH.isInvalid) {
                  sp.extX = wAndH.w
                  sp.extY = wAndH.h
                }
              } else {
                sp.extX = metricsOfContent.w + l_ins + r_ins
                sp.extY = isAutoFit
                  ? metricsOfContent.contentHeight + t_ins + b_ins
                  : sp.extY
              }
            } else {
              if (geometry) {
                wAndH = getAdjustWHAndUpdateGeometry(
                  geometry,
                  metricsOfContent.contentHeight + l_ins + r_ins,
                  metricsOfContent.w + t_ins + b_ins,
                  isAutoFit ? undefined : sp.extX
                )
                if (!wAndH.isInvalid) {
                  sp.extX = wAndH.w
                  sp.extY = wAndH.h
                }
              } else {
                sp.extY = metricsOfContent.w + t_ins + b_ins
                sp.extX = isAutoFit
                  ? metricsOfContent.contentHeight + l_ins + r_ins
                  : sp.extX
              }
            }
          }
        }
      } else {
        if (!bodyPr.upright) {
          if (
            !(
              bodyPr.vert === TextVerticalType.vert ||
              bodyPr.vert === TextVerticalType.vert270 ||
              bodyPr.vert === TextVerticalType.eaVert
            )
          ) {
            if (geometry) {
              wAndH = getAdjustWHAndUpdateGeometry(
                geometry,
                undefined,
                metricsOfContent.contentHeight + t_ins + b_ins,
                sp.extX,
                undefined
              )
              if (!wAndH.isInvalid) {
                sp.extY = wAndH.h
              }
            } else {
              sp.extY = metricsOfContent.contentHeight + t_ins + b_ins
            }
          } else {
            if (geometry) {
              wAndH = getAdjustWHAndUpdateGeometry(
                geometry,
                metricsOfContent.contentHeight + l_ins + b_ins,
                undefined,
                undefined,
                sp.extY
              )
              if (!wAndH.isInvalid) {
                sp.extX = wAndH.w
              }
            } else {
              sp.extX = metricsOfContent.contentHeight + l_ins + r_ins
            }
          }
        } else {
          const finalRotate = getFinalRotate(sp)
          if (isRotateIn45D(finalRotate)) {
            if (
              !(
                bodyPr.vert === TextVerticalType.vert ||
                bodyPr.vert === TextVerticalType.vert270 ||
                bodyPr.vert === TextVerticalType.eaVert
              )
            ) {
              if (geometry) {
                wAndH = getAdjustWHAndUpdateGeometry(
                  geometry,
                  undefined,
                  metricsOfContent.contentHeight + t_ins + b_ins,
                  sp.extX,
                  undefined
                )
                if (!wAndH.isInvalid) {
                  sp.extY = wAndH.h
                }
              } else {
                sp.extY = metricsOfContent.contentHeight + t_ins + b_ins
              }
            } else {
              if (geometry) {
                wAndH = getAdjustWHAndUpdateGeometry(
                  geometry,
                  metricsOfContent.contentHeight + l_ins + r_ins,
                  undefined,
                  undefined,
                  sp.extY
                )
                if (!wAndH.isInvalid) {
                  sp.extX = wAndH.w
                }
              } else {
                sp.extX = metricsOfContent.contentHeight + l_ins + r_ins
              }
            }
          } else {
            if (
              !(
                bodyPr.vert === TextVerticalType.vert ||
                bodyPr.vert === TextVerticalType.vert270 ||
                bodyPr.vert === TextVerticalType.eaVert
              )
            ) {
              if (geometry) {
                wAndH = getAdjustWHAndUpdateGeometry(
                  geometry,
                  metricsOfContent.contentHeight + l_ins + r_ins,
                  undefined,
                  undefined,
                  sp.extY
                )
                if (!wAndH.isInvalid) {
                  sp.extX = wAndH.w
                }
              } else {
                sp.extX = metricsOfContent.contentHeight + l_ins + r_ins
              }
            } else {
              if (geometry) {
                wAndH = getAdjustWHAndUpdateGeometry(
                  geometry,
                  undefined,
                  metricsOfContent.contentHeight + t_ins + b_ins,
                  sp.extX,
                  undefined
                )
                if (!wAndH.isInvalid) {
                  sp.extY = wAndH.h
                }
              } else {
                sp.extY = metricsOfContent.contentHeight + t_ins + b_ins
              }
            }
          }
        }
      }
    }

    // sync xfrm
    ensureSpPrXfrm(sp, true)
    // if (!sp.group) {
    sp.spPr!.xfrm!.extX = sp.extX
    sp.spPr!.xfrm!.extY = sp.extY
    // }

    const textDoc = getTextDocumentOfSp(sp)!
    let valueOfJc, valueOfAnchor
    if (isNumber(sp.rot) && !checkAlmostEqual(sp.rot, 0)) {
      valueOfJc = AlignKind.Center
      valueOfAnchor = 1
    } else {
      valueOfJc = textDoc.children.at(0)?.calcedPrs.pr?.paraPr.jcAlign
      valueOfAnchor = bodyPr.anchor
    }
    const oldJc = valueOfJc
    if (bodyPr.vert === TextVerticalType.eaVert) {
      // 竖排下，水平和垂直方向计算互换
      if (valueOfAnchor === 4) {
        valueOfJc = AlignKind.Right
      } else if (valueOfAnchor === 0) {
        valueOfJc = AlignKind.Left
      } else {
        valueOfJc = undefined
      }
      if (oldJc === AlignKind.Left) {
        valueOfAnchor = 4
      } else if (oldJc === AlignKind.Right) {
        valueOfAnchor = 0
      }
    }
    switch (valueOfJc) {
      case AlignKind.Right: {
        deltaX = oldWidth - sp.extX
        break
      }
      case AlignKind.Left: {
        deltaX = 0
        break
      }
      default: {
        deltaX = (oldWidth - sp.extX) / 2
        break
      }
    }

    switch (valueOfAnchor) {
      case 0: {
        //b
        deltaY = oldHeight - sp.extY
        break
      }
      case 1: //ctr
      case 2: //dist
      case 3: {
        //just
        deltaY = (oldHeight - sp.extY) / 2
        break
      }
      case 4: // right
      default: {
        break
      }
    }

    sp.x += deltaX
    sp.y += deltaY

    if (sp.group) {
      const { cx, cy } = calcScaleFactorsOfGroup(sp.group)
      sp.spPr!.xfrm!.offX = sp.x / cx + sp.group.spPr!.xfrm!.chOffX!
      sp.spPr!.xfrm!.offY = sp.y / cy + sp.group.spPr!.xfrm!.chOffY!
    } else {
      sp.spPr!.xfrm!.offX = sp.x
      sp.spPr!.xfrm!.offY = sp.y
    }

    refreshTransform(sp)
    calcShapeSnapXYs(sp)
    sp.setCalcStatusOf(calcStatus.Geometry)
    sp.setCalcStatusOf(calcStatus.Bounds)
  }
}

function refreshTransform(sp: Shape) {
  sp.base64Img = null
  const calcProps: TransformProps = {
    pos: { x: sp.x, y: sp.y },
    size: { w: sp.extX, h: sp.extY },
    flipH: sp.flipH,
    flipV: sp.flipV,
    rot: sp.rot,
    group: sp.group,
  }
  calcTransformFormProps(sp.transform, calcProps)
  sp.calcedTransform.reset(sp.transform)
  sp.invertTransform = MatrixUtils.invertMatrixFrom(
    sp.invertTransform,
    sp.transform
  )
}

function calculateTextTransform(sp: Shape) {
  const textDoc = getTextDocumentOfSp(sp)
  if (!textDoc) return

  const bodyPr = sp.getBodyPr()
  sp.clipRect = checkTextTransformMatrix(
    sp,
    sp.calcedTextTransform,
    textDoc,
    bodyPr
  )
  sp.textTransform = sp.calcedTextTransform.clone()
  sp.invertTextTransform = MatrixUtils.invertMatrix(sp.textTransform)

  const phContent = sp.txBody?.phContent
  if (phContent) {
    sp.textTransformForPh = new Matrix2D()
    sp.clipRectForPh = checkTextTransformMatrix(
      sp,
      sp.textTransformForPh,
      phContent,
      bodyPr
    )
    sp.invertTextTransformForPh = MatrixUtils.invertMatrix(
      sp.textTransformForPh
    )
  }
}

export function calculatePlaceholderContent(sp: Shape) {
  if (sp.txBody) {
    if (isPlaceholder(sp)) {
      if (!isPlaceholderWithEmptyContent(sp)) {
        return
      }

      if (!sp.txBody.phContent) {
        sp.txBody.phContent = sp.makePlaceholderContent()
      } else {
        sp.txBody.phContent.invalidateAllParagraphsCalcedPrs()
      }

      const textDoc = sp.txBody.phContent
      let w, bodyPr
      if (textDoc) {
        let h
        let l_ins, t_ins, r_ins, b_ins
        bodyPr = sp.getBodyPr()
        if (bodyPr) {
          l_ins = isNumber(bodyPr.lIns) ? bodyPr.lIns : 2.54
          r_ins = isNumber(bodyPr.rIns) ? bodyPr.rIns : 2.54
          t_ins = isNumber(bodyPr.tIns) ? bodyPr.tIns : 1.27
          b_ins = isNumber(bodyPr.bIns) ? bodyPr.bIns : 1.27
        } else {
          l_ins = 2.54
          r_ins = 2.54
          t_ins = 1.27
          b_ins = 1.27
        }
        if (
          sp.spPr!.geometry &&
          sp.spPr!.geometry.rect &&
          isNumber(sp.spPr!.geometry.rect.l) &&
          isNumber(sp.spPr!.geometry.rect.t) &&
          isNumber(sp.spPr!.geometry.rect.r) &&
          isNumber(sp.spPr!.geometry.rect.r)
        ) {
          w =
            sp.spPr!.geometry.rect.r -
            sp.spPr!.geometry.rect.l -
            (l_ins + r_ins)
          h =
            sp.spPr!.geometry.rect.b -
            sp.spPr!.geometry.rect.t -
            (t_ins + b_ins)
        } else {
          w = sp.extX - (l_ins + r_ins)
          h = sp.extY - (t_ins + b_ins)
        }

        if (!bodyPr.upright) {
          if (
            !(
              bodyPr.vert === TextVerticalType.vert ||
              bodyPr.vert === TextVerticalType.vert270 ||
              bodyPr.vert === TextVerticalType.eaVert
            )
          ) {
            sp.txBody.contentWidthOfPh = w
            sp.txBody.contentHeightPh = h
          } else {
            sp.txBody.contentWidthOfPh = h
            sp.txBody.contentHeightPh = w
          }
        } else {
          const finalRotate = getFinalRotate(sp)
          if (isRotateIn45D(finalRotate)) {
            if (
              !(
                bodyPr.vert === TextVerticalType.vert ||
                bodyPr.vert === TextVerticalType.vert270 ||
                bodyPr.vert === TextVerticalType.eaVert
              )
            ) {
              sp.txBody.contentWidthOfPh = w
              sp.txBody.contentHeightPh = h
            } else {
              sp.txBody.contentWidthOfPh = h
              sp.txBody.contentHeightPh = w
            }
          } else {
            if (
              !(
                bodyPr.vert === TextVerticalType.vert ||
                bodyPr.vert === TextVerticalType.vert270 ||
                bodyPr.vert === TextVerticalType.eaVert
              )
            ) {
              sp.txBody.contentWidthOfPh = h
              sp.txBody.contentHeightPh = w
            } else {
              sp.txBody.contentWidthOfPh = w
              sp.txBody.contentHeightPh = h
            }
          }
        }
      }

      const content_ = getTextDocumentOfSp(sp)
      const firstPara_ = content_?.children.at(0)
      const firstPara = textDoc.children.at(0)
      if (firstPara_ && firstPara) {
        firstPara.setPr(firstPara_.pr)
        const firstItemTextPr = textPrOfFirstItem(firstPara_)
        if (!firstPara.pr.defaultRunPr) {
          firstPara.pr.defaultRunPr = TextPr.new(firstItemTextPr)
        } else {
          firstPara.pr.defaultRunPr.merge(firstItemTextPr)
        }
      }

      if (EditorSettings.isRTL) {
        ManagedSliceUtil.forEach(textDoc.children, (para) => {
          if (para) {
            para.pr.rtl = true
          }
          const calcedParaPr = para.getCalcedParaPr()
          if (calcedParaPr.jcAlign !== AlignKind.Center) {
            para.pr.jcAlign = AlignKind.Right
            calcedParaPr.jcAlign = AlignKind.Right
          }
        })
      }
      calculateTextDocumentBySize(
        textDoc,
        sp.txBody.contentWidthOfPh!,
        sp.txBody.contentHeightPh!
      )
    } else {
      sp.txBody.phContent = null
    }
  }
}
