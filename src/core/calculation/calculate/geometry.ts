import { each } from '../../../../liber/l/each'
import { Dict, Nullable } from '../../../../liber/pervasive'
import { isNumber, toInt } from '../../../common/utils'
import {
  AdjCoordinate,
  AdjAngle,
  GDFormulaTypeValue,
  GDFormulaType,
  ConnectionSite,
  XYAdjustHandleInfo,
  PolarAdjustHandleInfo,
} from '../../../io/dataType/geometry'
import { THRESHOLD_DELTA } from '../../common/const/unit'
import { degFactor, radFactor } from '../../SlideElement/const'
import {
  CxnProps,
  GDInfo,
  Geometry,
  PolarAdjustHandleProps,
  XYAdjustHandleProps,
} from '../../SlideElement/geometry/Geometry'
import { Path, PathOperationType } from '../../SlideElement/geometry/Path'

const IterCount_Max = 50
const Delta_Max = 0.001

export function getAdjustWHAndUpdateGeometry(
  geom: Geometry,
  textWidth: Nullable<number>,
  textHeight: Nullable<number>,
  geometryWidth?: number,
  geometryHeight?: number
): {
  w: number
  h: number
  isInvalid: boolean
} {
  let delta = 0
  let width = textWidth
  let height = textHeight
  let nextWidth
  let nextHeight

  let loopCount = 0
  const rect: { l; t; r; b } = { l: null, t: null, r: null, b: null }
  if (!isNumber(geometryWidth) && !isNumber(geometryHeight)) {
    do {
      calcRectFromGeometry(rect, geom, width!, height!)
      const { r, l, b, t } = rect
      nextWidth = textWidth! - (r - l) + width!
      nextHeight = textHeight! - (b - t) + height!
      delta = Math.max(
        Math.abs(nextWidth - width!),
        Math.abs(nextHeight - height!)
      )
      width = nextWidth
      height = nextHeight
      ++loopCount
    } while (delta > Delta_Max && loopCount < IterCount_Max)
    return { w: width!, h: height!, isInvalid: delta > Delta_Max }
  } else if (isNumber(geometryWidth)) {
    do {
      calcRectFromGeometry(rect, geom, geometryWidth, height!)
      const { b, t } = rect
      nextHeight = textHeight! - (b - t) + height!
      delta = Math.abs(nextHeight - height!)
      height = nextHeight
      ++loopCount
    } while (delta > Delta_Max && loopCount < IterCount_Max)
    return {
      w: geometryWidth,
      h: height!,
      isInvalid: delta > Delta_Max,
    }
  } else {
    do {
      calcRectFromGeometry(rect, geom, width!, geometryHeight!)
      const { r, l } = rect
      nextWidth = textWidth! - (r - l) + width!
      delta = Math.abs(nextWidth - width!)
      width = nextWidth
      ++loopCount
    } while (delta > Delta_Max && loopCount < IterCount_Max)
    return {
      w: width!,
      h: geometryHeight!,
      isInvalid: delta > Delta_Max,
    }
  }
}

function calculatePath(path: Path, gdLst: Dict<number>) {
  let ch: number, cw: number
  if (path.pathWidth != null) {
    if (path.pathWidth > THRESHOLD_DELTA) {
      cw = gdLst['w'] / path.pathWidth
    } else {
      cw = 0
    }
  } else {
    cw = 1
  }
  if (path.pathHeight != null) {
    if (path.pathHeight > THRESHOLD_DELTA) {
      ch = gdLst['h'] / path.pathHeight
    } else {
      ch = 0
    }
  } else {
    ch = 1
  }
  const pathCommandsInfos = path.pathCommandsInfos
  const l = pathCommandsInfos.length

  let lastX: number = 0,
    lastY: number = 0
  for (let i = 0; i < l; ++i) {
    const cmd = pathCommandsInfos[i]
    switch (cmd.id) {
      case PathOperationType.moveTo:
      case PathOperationType.lineTo: {
        let x0 = gdLst[cmd.x]
        if (x0 == null) {
          x0 = toInt(cmd.x)
        }

        let y0 = gdLst[cmd.y]
        if (y0 == null) {
          y0 = toInt(cmd.y)
        }

        path.pathCommands[i] = { id: cmd.id, x: x0 * cw, y: y0 * ch }

        lastX = x0 * cw
        lastY = y0 * ch

        break
      }
      case PathOperationType.quadBezTo: {
        let x0 = gdLst[cmd.x0]
        if (x0 == null) {
          x0 = toInt(cmd.x0, 10)
        }

        let y0 = gdLst[cmd.y0]
        if (y0 == null) {
          y0 = toInt(cmd.y0, 10)
        }

        let x1 = gdLst[cmd.x1]
        if (x1 == null) {
          x1 = toInt(cmd.x1, 10)
        }

        let y1 = gdLst[cmd.y1]
        if (y1 == null) {
          y1 = toInt(cmd.y1, 10)
        }

        path.pathCommands[i] = {
          id: PathOperationType.quadBezTo,
          x0: x0 * cw,
          y0: y0 * ch,
          x1: x1 * cw,
          y1: y1 * ch,
        }

        lastX = x1 * cw
        lastY = y1 * ch
        break
      }
      case PathOperationType.cubicBezTo: {
        let x0 = gdLst[cmd.x0]
        if (x0 == null) {
          x0 = toInt(cmd.x0, 10)
        }

        let y0 = gdLst[cmd.y0]
        if (y0 == null) {
          y0 = toInt(cmd.y0, 10)
        }

        let x1 = gdLst[cmd.x1]
        if (x1 == null) {
          x1 = toInt(cmd.x1, 10)
        }

        let y1 = gdLst[cmd.y1]
        if (y1 == null) {
          y1 = toInt(cmd.y1, 10)
        }

        let x2 = gdLst[cmd.x2]
        if (x2 == null) {
          x2 = toInt(cmd.x2, 10)
        }

        let y2 = gdLst[cmd.y2]
        if (y2 == null) {
          y2 = toInt(cmd.y2, 10)
        }

        path.pathCommands[i] = {
          id: PathOperationType.cubicBezTo,
          x0: x0 * cw,
          y0: y0 * ch,
          x1: x1 * cw,
          y1: y1 * ch,
          x2: x2 * cw,
          y2: y2 * ch,
        }

        lastX = x2 * cw
        lastY = y2 * ch

        break
      }
      case PathOperationType.arcTo: {
        let hR = gdLst[cmd.hR]

        if (hR == null) {
          hR = toInt(cmd.hR, 10)
        }

        let wR = gdLst[cmd.wR]
        if (wR == null) {
          wR = toInt(cmd.wR, 10)
        }

        let stAng = gdLst[cmd.stAng]
        if (stAng == null) {
          stAng = toInt(cmd.stAng, 10)
        }

        let swAng = gdLst[cmd.swAng]
        if (swAng == null) {
          swAng = toInt(cmd.swAng, 10)
        }

        const a1 = stAng
        const a2 = stAng + swAng
        const a3 = swAng

        stAng =
          Math.atan2(
            ch * Math.sin(a1 * radFactor),
            cw * Math.cos(a1 * radFactor)
          ) / radFactor
        swAng =
          Math.atan2(
            ch * Math.sin(a2 * radFactor),
            cw * Math.cos(a2 * radFactor)
          ) /
            radFactor -
          stAng

        if (swAng > 0 && a3 < 0) swAng -= 21600000
        if (swAng < 0 && a3 > 0) swAng += 21600000
        if (swAng === 0 && a3 !== 0) swAng = 21600000

        const a = wR * cw
        const b = hR * ch
        const sin2 = Math.sin(stAng * radFactor)
        const cos2 = Math.cos(stAng * radFactor)
        const _xrad = cos2 / a
        const _yrad = sin2 / b
        const l = 1 / Math.sqrt(_xrad * _xrad + _yrad * _yrad)
        const xc = lastX - l * cos2
        const yc = lastY - l * sin2

        const sin1 = Math.sin((stAng + swAng) * radFactor)
        const cos1 = Math.cos((stAng + swAng) * radFactor)
        const _xrad1 = cos1 / a
        const _yrad1 = sin1 / b
        const l1 = 1 / Math.sqrt(_xrad1 * _xrad1 + _yrad1 * _yrad1)

        path.pathCommands[i] = {
          id: PathOperationType.arcTo,
          stX: lastX,
          stY: lastY,
          wR: wR * cw,
          hR: hR * ch,
          stAng: stAng * radFactor,
          swAng: swAng * radFactor,
        }

        lastX = xc + l1 * cos1
        lastY = yc + l1 * sin1

        break
      }
      case PathOperationType.close: {
        path.pathCommands[i] = { id: PathOperationType.close }
        break
      }
      default: {
        break
      }
    }
  }

  path.resetHittestPath2D()
}

export function calculateGeometry(
  geom: Geometry,
  w: number,
  h: number,
  force = false
) {
  if (geom.isNeedRecalc(w, h) === false && force !== true) return

  populateDefaultsForGdLst(w, h, geom.gdLst)
  populateGuideLst(geom.gdLstInfo, geom.gdLst)
  populateCnxLst(geom.cxnLstInfo, geom.cxnLst, geom.gdLst)
  populateAhXYList(geom.ahXYInfoLst, geom.ahXYLst, geom.gdLst)
  populateAhPolarList(geom.ahPolarInfoLst, geom.ahPolarLst, geom.gdLst)
  for (let i = 0, n = geom.pathLst.length; i < n; i++) {
    calculatePath(geom.pathLst[i], geom.gdLst)
  }

  geom.rect = { l: null, t: null, r: null, b: null }
  if (geom.geomRect) {
    geom.rect.l = geom.gdLst[geom.geomRect.l]
    if (geom.rect.l == null) {
      geom.rect.l = toInt(geom.geomRect.l)
    }

    geom.rect.t = geom.gdLst[geom.geomRect.t]
    if (geom.rect.t == null) {
      geom.rect.t = toInt(geom.geomRect.t)
    }

    geom.rect.r = geom.gdLst[geom.geomRect.r]
    if (geom.rect.r == null) {
      geom.rect.r = toInt(geom.geomRect.r)
    }

    geom.rect.b = geom.gdLst[geom.geomRect.b]
    if (geom.rect.b == null) {
      geom.rect.b = toInt(geom.geomRect.b)
    }
  } else {
    geom.rect.l = geom.gdLst['l']
    geom.rect.t = geom.gdLst['t']
    geom.rect.r = geom.gdLst['r']
    geom.rect.b = geom.gdLst['b']
  }
}

const _gdLst: Dict<number> = Object.create(null)
export function calcRectFromGeometry(
  rect: { l; t; r; b },
  geom: Geometry,
  w: number,
  h: number
) {
  // copy previous gdLst
  each((v, k) => {
    _gdLst[k] = v
  }, geom.gdLst)

  populateDefaultsForGdLst(w, h, _gdLst)
  populateGuideLst(geom.gdLstInfo, _gdLst)
  if (geom.geomRect) {
    rect.l = _gdLst[geom.geomRect.l]
    if (rect.l == null) {
      rect.l = toInt(geom.geomRect.l)
    }

    rect.t = _gdLst[geom.geomRect.t]
    if (rect.t == null) {
      rect.t = toInt(geom.geomRect.t)
    }

    rect.r = _gdLst[geom.geomRect.r]
    if (rect.r == null) {
      rect.r = toInt(geom.geomRect.r)
    }

    rect.b = _gdLst[geom.geomRect.b]
    if (rect.b == null) {
      rect.b = toInt(geom.geomRect.b)
    }
  } else {
    rect.l = _gdLst['l']
    rect.t = _gdLst['t']
    rect.r = _gdLst['r']
    rect.b = _gdLst['b']
  }
  return rect
}

function Cos(angle: number) {
  return Math.cos(radFactor * angle)
}

function Sin(angle: number) {
  return Math.sin(radFactor * angle)
}

function Tan(angle: number) {
  return Math.tan(radFactor * angle)
}

function Atan2(y: number, x: number) {
  return degFactor * Math.atan2(y, x)
}

function CosAtan2(x: number, y: number, z: number) {
  return x * Math.cos(Math.atan2(z, y))
}

function SinAtan2(x, y, z) {
  return x * Math.sin(Math.atan2(z, y))
}

function fromAdjValue(
  info: AdjCoordinate | AdjAngle | undefined,
  gdLst: Dict<number>
) {
  let ret: number | undefined = toInt(info)
  if (isNaN(ret)) ret = undefined
  if (info != null && ret == null) ret = gdLst[info]
  return ret
}

function populateDefaultsForGdLst(w: number, h: number, gdLst: Dict<number>) {
  gdLst['_3cd4'] = 16200000
  gdLst['_3cd8'] = 8100000
  gdLst['_5cd8'] = 13500000
  gdLst['_7cd8'] = 18900000
  gdLst['cd2'] = 10800000
  gdLst['cd4'] = 5400000
  gdLst['cd8'] = 2700000
  gdLst['l'] = 0
  gdLst['t'] = 0
  gdLst['h'] = h
  gdLst['b'] = h
  gdLst['hd2'] = h / 2
  gdLst['hd3'] = h / 3
  gdLst['hd4'] = h / 4
  gdLst['hd5'] = h / 5
  gdLst['hd6'] = h / 6
  gdLst['hd8'] = h / 8
  gdLst['hd10'] = h / 10
  gdLst['hd12'] = h / 12
  gdLst['hd32'] = h / 32
  gdLst['vc'] = h / 2
  gdLst['w'] = w
  gdLst['r'] = w
  gdLst['wd2'] = w / 2
  gdLst['wd3'] = w / 3
  gdLst['wd4'] = w / 4
  gdLst['wd5'] = w / 5
  gdLst['wd6'] = w / 6
  gdLst['wd8'] = w / 8
  gdLst['wd10'] = w / 10
  gdLst['wd12'] = w / 12
  gdLst['wd32'] = w / 32
  gdLst['hc'] = w / 2
  gdLst['ls'] = Math.max(w, h)
  gdLst['ss'] = Math.min(w, h)
  gdLst['ssd2'] = gdLst['ss'] / 2
  gdLst['ssd4'] = gdLst['ss'] / 4
  gdLst['ssd6'] = gdLst['ss'] / 6
  gdLst['ssd8'] = gdLst['ss'] / 8
  gdLst['ssd16'] = gdLst['ss'] / 16
  gdLst['ssd32'] = gdLst['ss'] / 32
}

function populateGuideValue(
  name: string,
  formula: GDFormulaTypeValue,
  x: string | undefined,
  y: string | undefined,
  z: string | undefined,
  gdLst: Dict<number>
) {
  let xt, yt, zt

  xt = x && gdLst[x]
  if (xt == null) xt = toInt(x)

  yt = y && gdLst[y]
  if (yt == null) yt = toInt(y)

  zt = z && gdLst[z]
  if (zt == null) zt = toInt(z)

  switch (formula) {
    case GDFormulaType.MULT_DIV: {
      gdLst[name] = (xt * yt) / zt
      break
    }
    case GDFormulaType.PLUS_MINUS: {
      gdLst[name] = xt + yt - zt
      break
    }
    case GDFormulaType.PLUS_DIV: {
      gdLst[name] = (xt + yt) / zt
      break
    }
    case GDFormulaType.IF_ELSE: {
      if (xt > 0) gdLst[name] = yt
      else gdLst[name] = zt
      break
    }

    case GDFormulaType.ABS: {
      gdLst[name] = Math.abs(xt)
      break
    }
    case GDFormulaType.AT2: {
      gdLst[name] = Atan2(yt, xt)
      break
    }
    case GDFormulaType.CAT2: {
      gdLst[name] = CosAtan2(xt, yt, zt)
      break
    }

    case GDFormulaType.COS: {
      gdLst[name] = xt * Cos(yt)
      break
    }

    case GDFormulaType.MAX: {
      gdLst[name] = Math.max(xt, yt)
      break
    }

    case GDFormulaType.MOD: {
      gdLst[name] = Math.sqrt(xt * xt + yt * yt + zt * zt)
      break
    }

    case GDFormulaType.PIN: {
      if (yt < xt) gdLst[name] = xt
      else if (yt > zt) gdLst[name] = zt
      else gdLst[name] = yt
      break
    }
    case GDFormulaType.SAT2: {
      gdLst[name] = SinAtan2(xt, yt, zt)
      break
    }
    case GDFormulaType.SIN: {
      gdLst[name] = xt * Sin(yt)
      break
    }
    case GDFormulaType.SQRT: {
      gdLst[name] = Math.sqrt(xt)
      break
    }

    case GDFormulaType.TAN: {
      gdLst[name] = xt * Tan(yt)
      break
    }
    case GDFormulaType.VALUE: {
      gdLst[name] = xt
      break
    }
    case GDFormulaType.MIN: {
      gdLst[name] = Math.min(xt, yt)
    }
  }
}

function populateGuideLst(gdLstInfo: GDInfo[], gdLst: Dict<number>) {
  for (let i = 0, n = gdLstInfo.length; i < n; i++) {
    const info = gdLstInfo[i]
    populateGuideValue(info.name, info.formula, info.x, info.y, info.z, gdLst)
  }
}

function populateCnxLst(
  cxnLstInfo: ConnectionSite[],
  cxnLst: CxnProps[],
  gdLst: Dict<number>
) {
  for (let i = 0, l = cxnLstInfo.length; i < l; i++) {
    const ang = fromAdjValue(cxnLstInfo[i].ang, gdLst)

    let x = gdLst[cxnLstInfo[i].x]
    if (x == null) x = toInt(cxnLstInfo[i].x)

    let y = gdLst[cxnLstInfo[i].y]
    if (y == null) y = toInt(cxnLstInfo[i].y)

    if (cxnLst[i] == null) cxnLst[i] = {} as CxnProps

    cxnLst[i].ang = ang
    cxnLst[i].x = x
    cxnLst[i].y = y
  }
}

function populateAhXYList(
  ahXYListInfo: XYAdjustHandleInfo[],
  ahXYLst: XYAdjustHandleProps[],
  gdLst: Dict<number>
) {
  for (let i = 0, n = ahXYListInfo.length; i < n; i++) {
    const minX = fromAdjValue(ahXYListInfo[i].minX, gdLst)

    const maxX = fromAdjValue(ahXYListInfo[i].maxX, gdLst)

    const minY = fromAdjValue(ahXYListInfo[i].minY, gdLst)

    const maxY = fromAdjValue(ahXYListInfo[i].maxY, gdLst)

    let posX = toInt(ahXYListInfo[i].posX)
    if (isNaN(posX)) {
      posX = gdLst[ahXYListInfo[i].posX]
    }

    let posY = toInt(ahXYListInfo[i].posY)
    if (isNaN(posY)) {
      posY = gdLst[ahXYListInfo[i].posY]
    }

    if (ahXYLst[i] == null) ahXYLst[i] = {} as XYAdjustHandleProps

    ahXYLst[i].gdRefX = ahXYListInfo[i].gdRefX
    ahXYLst[i].minX = minX
    ahXYLst[i].maxX = maxX

    ahXYLst[i].gdRefY = ahXYListInfo[i].gdRefY
    ahXYLst[i].minY = minY
    ahXYLst[i].maxY = maxY

    ahXYLst[i].posX = posX
    ahXYLst[i].posY = posY
  }
}

function populateAhPolarList(
  ahPolarInfoLst: PolarAdjustHandleInfo[],
  ahPolarLst: PolarAdjustHandleProps[],
  gdLst: Dict<number>
) {
  for (let i = 0, l = ahPolarInfoLst.length; i < l; i++) {
    const info_minR = ahPolarInfoLst[i].minR
    const minR = fromAdjValue(info_minR, gdLst)

    const info_maxR = ahPolarInfoLst[i].maxR
    const maxR = fromAdjValue(info_maxR, gdLst)

    const info_minAng = ahPolarInfoLst[i].minAng
    const minAng = fromAdjValue(info_minAng, gdLst)

    const info_maxAng = ahPolarInfoLst[i].maxAng
    const maxAng = fromAdjValue(info_maxAng, gdLst)

    let posX = toInt(ahPolarInfoLst[i].posX)
    if (isNaN(posX)) {
      posX = gdLst[ahPolarInfoLst[i].posX]
    }

    let posY = toInt(ahPolarInfoLst[i].posY)
    if (isNaN(posY)) {
      posY = gdLst[ahPolarInfoLst[i].posY]
    }

    if (ahPolarLst[i] == null) ahPolarLst[i] = {} as PolarAdjustHandleProps

    ahPolarLst[i].gdRefR = ahPolarInfoLst[i].gdRefR
    ahPolarLst[i].minR = minR
    ahPolarLst[i].maxR = maxR

    ahPolarLst[i].gdRefAng = ahPolarInfoLst[i].gdRefAng
    ahPolarLst[i].minAng = minAng
    ahPolarLst[i].maxAng = maxAng

    ahPolarLst[i].posX = posX
    ahPolarLst[i].posY = posY
  }
}
