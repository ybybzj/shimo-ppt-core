import {
  isDict,
  isFiniteNumber,
  isNumber,
  sortAscending,
} from '../../../common/utils'
import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { Matrix2D } from '../../graphic/Matrix'
import { Ln } from '../../SlideElement/attrs/line'
import {
  getParentsOfElement,
  getCalcedLine,
  getCalcedStyleForSp,
} from '../../utilities/shape/getters'

export function calculatePen(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
    case InstanceType.ImageShape: {
      // 同一层级，spPr 中的 ln 优先级高于 style 中的 lnRef
      // 不同层级，应用低级的 ln
      const { style: calced_style, lvl } = getCalcedStyleForSp(sp)
      const rgba = { r: 0, g: 0, b: 0, a: 255 }
      const parents = getParentsOfElement(sp)
      if (
        isDict(parents.theme) &&
        isDict(calced_style) &&
        isDict(calced_style.lnRef)
      ) {
        sp.pen = parents.theme.getLnStyle(
          calced_style.lnRef.idx,
          calced_style.lnRef.color
        )
      } else {
        sp.pen = null
      }

      const { ln: calcedLine, lvl: lnLvl } = getCalcedLine(sp)
      const comparedLvl =
        isFiniteNumber(lnLvl) && isFiniteNumber(lvl) ? lnLvl! <= lvl! : true
      if (calcedLine && comparedLvl) {
        if (!sp.pen) {
          sp.pen = new Ln()
        }
        sp.pen.merge(calcedLine)
      }
      if (sp.pen) {
        sp.pen.calculate(
          parents.theme,
          parents.slide,
          parents.layout,
          parents.master,
          rgba
        )
      }
      if (sp.instanceType === InstanceType.ImageShape) {
        if (sp.pen) {
          if (isNumber(sp.pen.w)) {
            sp.pen.w *= 2
          }
        }
      }
      break
    }
    case InstanceType.GroupShape:
    case InstanceType.GraphicFrame: {
      break
    }
  }
}

export function calcShapeSnapXYs(sp: SlideElement) {
  const { snapXs, snapYs } = calcSnapXYs(sp.extX, sp.extY, sp.transform)
  sp.snapXs = snapXs.sort(sortAscending)
  sp.snapYs = snapYs.sort(sortAscending)
}

export function calcSnapXYs(extX: number, extY: number, transform: Matrix2D) {
  const snapXs: number[] = []
  const snapYs: number[] = []
  addToArrUniq(snapXs, transform.XFromPoint(0, 0))
  addToArrUniq(snapYs, transform.YFromPoint(0, 0))
  addToArrUniq(snapXs, transform.XFromPoint(extX, 0))
  addToArrUniq(snapYs, transform.YFromPoint(extX, 0))

  addToArrUniq(snapXs, transform.XFromPoint(extX * 0.5, extY * 0.5))
  addToArrUniq(snapYs, transform.YFromPoint(extX * 0.5, extY * 0.5))
  addToArrUniq(snapXs, transform.XFromPoint(extX, extY))
  addToArrUniq(snapYs, transform.YFromPoint(extX, extY))
  addToArrUniq(snapXs, transform.XFromPoint(0, extY))
  addToArrUniq(snapYs, transform.YFromPoint(0, extY))
  return { snapXs, snapYs }
}

function addToArrUniq<T>(arr: T[], item: T) {
  if (arr.indexOf(item) === -1) {
    arr.push(item)
  }
}
