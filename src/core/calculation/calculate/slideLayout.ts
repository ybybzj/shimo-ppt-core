import { SlideLayout } from '../../Slide/SlideLayout'
import { isPlaceholder } from '../../utilities/shape/asserts'
import { CalcStatus, unsetAllCalcStatus } from '../updateCalcStatus/calcStatus'
import { calculateSlideElement } from './slideElement'

export function calculateForSlideLayout(slideLayout: SlideLayout) {
  const spTree = slideLayout.cSld.spTree
  const isBoundsDirty = slideLayout.isCalcStatusSet(
    CalcStatus.SlideLayout.Bounds
  )
  if (isBoundsDirty) {
    slideLayout.bounds.reset(
      slideLayout.width + 100.0,
      slideLayout.height + 100.0,
      -100.0,
      -100.0
    )
  }
  let isChecked = false
  const spCount = spTree.length
  for (let i = 0; i < spCount; ++i) {
    const sp = spTree[i]
    if (!isPlaceholder(sp)) {
      calculateSlideElement(sp)
      if (isBoundsDirty) {
        slideLayout.bounds.checkByBounds(sp.bounds)
      }
      isChecked = true
    }
  }
  if (isBoundsDirty) {
    if (isChecked) {
      slideLayout.bounds.updateWidthAndHeight()
      if (slideLayout.bounds.w < 0 || slideLayout.bounds.h < 0) {
        slideLayout.bounds.reset(0.0, 0.0, 0.0, 0.0)
      }
    } else {
      slideLayout.bounds.reset(0.0, 0.0, 0.0, 0.0)
    }
  }

  unsetAllCalcStatus(slideLayout)
}

export function calculateLayoutPlaceholders(layout: SlideLayout) {
  const sps = layout.cSld.spTree
  let index
  const count = sps.length
  for (index = 0; index < count; ++index) {
    if (isPlaceholder(sps[index])) {
      calculateSlideElement(sps[index])
    }
  }
}
