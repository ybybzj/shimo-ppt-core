import { EditorUtil } from '../../../globals/editor'

export function calculateForRendering(changedSlideNums?: number[]) {
  return EditorUtil.ChangesStack.calcRenderingState(changedSlideNums)
}
