import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../common/utils'
import { ImageShape } from '../../SlideElement/Image'
import {
  calculateBrush,
  calcTransform,
  calculateBounds,
  calculateGeometryForSp,
} from '../../utilities/shape/calcs'
import { CalcStatus } from '../updateCalcStatus/calcStatus'
import { calculatePen, calcShapeSnapXYs } from './utils'

export function calculateForImageShape(sp: ImageShape) {
  if (sp.isDeleted || !sp.parent) return
  const s = turnOffRecordChanges()
  const calcStatus = CalcStatus.ImageShape
  if (sp.isCalcStatusSet(calcStatus.Brush)) {
    calculateBrush(sp)
    sp.unsetCalcStatusOf(calcStatus.Brush)
  }

  if (sp.isCalcStatusSet(calcStatus.Pen)) {
    calculatePen(sp)
    sp.unsetCalcStatusOf(calcStatus.Pen)
  }
  if (sp.isCalcStatusSet(calcStatus.Transform)) {
    calcTransform(sp)
    calcShapeSnapXYs(sp)
    sp.unsetCalcStatusOf(calcStatus.Transform)
  }

  if (sp.isCalcStatusSet(calcStatus.Geometry)) {
    calculateGeometryForSp(sp)
    sp.unsetCalcStatusOf(calcStatus.Geometry)
  }
  if (sp.isCalcStatusSet(calcStatus.Bounds)) {
    calculateBounds(sp)
    sp.unsetCalcStatusOf(calcStatus.Bounds)
  }
  restoreRecordChangesState(s)
}
