import { sortAscending, startPerfTiming } from '../../../common/utils'
import { logger } from '../../../lib/debug/log'
import { hookManager, Hooks } from '../../hooks'
import { searchManager } from '../../search/SearchManager'
import { Presentation } from '../../Slide/Presentation'
import { Slide } from '../../Slide/Slide'

import { getAndUpdateCursorPositionForPresentation } from '../../utilities/cursor/cursorPosition'
import { goToSlide } from '../../utilities/shape/hookInvokers'
import { hasCalcStatusSet } from '../updateCalcStatus/calcStatus'
import { calculateForSlide } from './slide'
import { calculateForSlideLayout } from './slideLayout'
import { calculateForSlideMaster } from './slideMaster'

export function invalidAllSlidesTextCalcStatus(presentation: Presentation) {
  const hasSlide = presentation?.slides?.length > 0
  if (hasSlide) {
    logger.time('invalidAllSlides')
  }
  for (const slide of presentation.slides) {
    slide.invalidTextCalcStatus()
  }
  if (hasSlide) {
    logger.timeEnd('invalidAllSlides')
  }
}

export function calculateRenderingStateForSlides(
  slidesForCalc: Slide[],
  arrOfToRedrawSlides?: number[]
) {
  let hasCalced = false

  if (slidesForCalc.length <= 0) {
    return { hasCalced, isLayoutChanged: false }
  }

  const getCalcStateTiming = startPerfTiming()
  const isLayoutChanged = getLayoutAndMasterForCalc(slidesForCalc)

  if (isLayoutChanged) {
    hasCalced = true
  }

  for (let i = 0, l = slidesForCalc.length; i < l; i++) {
    const slide = slidesForCalc[i]
    if (slide && slide.isLoading !== true && hasCalcStatusSet(slide)) {
      hasCalced = true
      calculateForSlide(slide)
      if (arrOfToRedrawSlides) {
        const slideIdx = slide.index!
        arrOfToRedrawSlides.push(slideIdx)
      }
    }
  }

  const calcStateTiming = getCalcStateTiming()
  if (calcStateTiming > 100) {
    hookManager.invoke(Hooks.Document.TrackPerf, {
      type: 'CALC_STATUS',
      timing: calcStateTiming,
    })
  }

  if (hasCalced === true) {
    logger.log(
      'calcRenderStatus',
      calcStateTiming,
      slidesForCalc.map((s) => s.index)
    )
  }
  return { hasCalced, isLayoutChanged }
}

export function calculateRenderingState(
  presentation: Presentation,
  changedSilideNums?: number[]
) {
  if (presentation.slides.length <= 0) {
    return false
  }

  if (searchManager.isClearSearch) {
    searchManager.clear()
    searchManager.isClearSearch = false
  }

  const arrOfToRedrawSlides: number[] = []
  updateSlideIndices(presentation)

  const currentSlideIndex =
    presentation.currentSlideIndex < 0
      ? 0
      : presentation.currentSlideIndex > presentation.slides.length - 1
        ? presentation.slides.length - 1
        : presentation.currentSlideIndex
  const slidesForCalc = (
    changedSilideNums != null && changedSilideNums.length > 0
      ? changedSilideNums.map((slideNum) => presentation.slides[slideNum])
      : [presentation.slides[currentSlideIndex]]
  ).filter(Boolean)

  const { hasCalced, isLayoutChanged } = calculateRenderingStateForSlides(
    slidesForCalc,
    arrOfToRedrawSlides
  )

  const isCurrentSlideCalced =
    arrOfToRedrawSlides.indexOf(currentSlideIndex) > -1
  getAndUpdateCursorPositionForPresentation(presentation, true, true)

  arrOfToRedrawSlides.sort(sortAscending)
  for (let i = 0; i < arrOfToRedrawSlides.length; ++i) {
    hookManager.invoke(Hooks.EditorUI.OnCalculateSlide, {
      index: arrOfToRedrawSlides[i],
    })
  }

  if (isCurrentSlideCalced) {
    const curSlide = presentation.slides[presentation.currentSlideIndex]
    if (curSlide) {
      hookManager.invoke(Hooks.EditorUI.OnCalculateNotes, {
        slideIndex: currentSlideIndex,
        notesWidth: curSlide.notesWidth,
        notesHeight: curSlide.getNotesShapeHeight(),
      })

      hookManager.invoke(Hooks.EditorUI.OnUpdateSelectionState)
    }
  }

  if (!presentation.slides[presentation.currentSlideIndex]) {
    goToSlide(presentation.slides.length - 1)
  } else {
    if (presentation.isGoToSlide) {
      goToSlide(presentation.currentSlideIndex)
      presentation.isGoToSlide = false
    } else if (isLayoutChanged) {
      hookManager.invoke(Hooks.EditorUI.OnUpdateLayouts, false)
    }

    if (presentation.needUpdatePreview) {
      hookManager.invoke(Hooks.EditorUI.OnCalculatePreviews)
      presentation.needUpdatePreview = false
    }
  }

  if (presentation.slides[presentation.currentSlideIndex]) {
    hookManager.invoke(Hooks.EditorUI.OnUpdatePlaceholders, {
      slideIndex: presentation.currentSlideIndex,
    })
  }
  return hasCalced
}

function updateSlideIndices(presentation: Presentation) {
  for (let i = 0; i < presentation.slides.length; ++i) {
    presentation.slides[i].udpateIndex(i)
  }
}

function getLayoutAndMasterForCalc(slides: Slide[]) {
  let hasCalced: boolean = false
  for (const slide of slides) {
    if (slide.isLoading !== true) {
      const layout = slide.layout
      if (layout && hasCalcStatusSet(layout)) {
        hasCalced = true
        if (layout.master && hasCalcStatusSet(layout.master)) {
          calculateForSlideMaster(layout.master!)
        }
        calculateForSlideLayout(layout)
      }
    }
  }
  return hasCalced
}

// calc presentation without side effect
export function calculatePresentation(presentation: Presentation) {
  if (presentation.slides.length <= 0) {
    return false
  }

  updateSlideIndices(presentation)

  const slidesForCalc = (presentation.slides ?? []).filter(Boolean)

  calculateRenderingStateForSlides(slidesForCalc)
}
