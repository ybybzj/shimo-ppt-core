import { Table } from '../../Table/Table'
import { CellBorderType, TableRowsInfo } from '../../Table/common'
import { LineHeightRule, VertAlignJc } from '../../common/const/attrs'
import { TableCell } from '../../Table/TableCell'
import { calculateTextDocumentByBounds } from '../../TextDocument/typeset/calculate'
import { rowContentIterator } from '../../Table/utils/utils'
import { TableCellBorder } from '../../Table/attrs/CellBorder'
import { TableBorders } from '../../Table/attrs/TablePr'
import { ManagedArray, ManagedSliceUtil } from '../../../common/managedArray'
import { Nullable } from '../../../../liber/pervasive'
import { TCBorderValue, VMergeType } from '../../common/const/table'
import { makeArrayOf } from '../../../../liber/l/makeArray'
const maxBottomMargins: ManagedArray<number> = new ManagedArray()

export function calculateTableState(table: Table) {
  maxBottomMargins.reset()
  calculateTableGrid(table)
  calculateTableBorders(table, maxBottomMargins)
  table.dimension.reset(table.x, table.y, table.xLimit, table.yLimit)
  updateXForDimension(table)
  const isCalced = calcDimension(table, maxBottomMargins)
  updateYForDimension(table)
  if (isCalced) table.calculateInfo.reset()
}

export function calculateTableGrid(table: Table) {
  if (table.children.length <= 0) {
    return
  }

  const tableGrid = table.tableGrid
  let reducedGrid: number[] = []
  reducedGrid[-1] = 0
  tableGrid.reduce((acc, next) => {
    acc += next
    reducedGrid.push(acc)
    return acc
  }, 0)

  const minWidth = getMinTableWidth(table)
  ManagedSliceUtil.forEach(table.children, (row, rowIdx) => {
    row.setIndex(rowIdx)
    let curGridColIndex = 0
    ManagedSliceUtil.forEach(row.children, (cell, index) => {
      cell.setIndex(index)
      const gridSpan = cell.getGridSpan()
      if (curGridColIndex + gridSpan - 1 > reducedGrid.length) {
        for (
          let i = reducedGrid.length;
          i <= curGridColIndex + gridSpan - 1;
          i++
        ) {
          reducedGrid[i] = reducedGrid[i - 1] + 20
        }
      }
      curGridColIndex += gridSpan
    })

    if (curGridColIndex - 1 > reducedGrid.length) {
      for (let i = reducedGrid.length; i <= curGridColIndex - 1; i++) {
        reducedGrid[i] = reducedGrid[i - 1] + 20
      }
    }
  })

  if (minWidth > reducedGrid[reducedGrid.length - 1]) {
    reducedGrid = scaleTableWidth(
      table,
      reducedGrid,
      reducedGrid[reducedGrid.length - 1]
    )
  }

  table.tableGridCalculated = reducedGrid.map((grid, i) => {
    const preV = reducedGrid[i - 1] ?? 0
    return grid - preV
  })
  table.tableGridReduced = reducedGrid
  table.tableGridReducedRTL = []
  table.tableGridReducedRTL[-1] = 0
  table.tableGridCalculated
    .slice()
    .reverse()
    .reduce((acc, next) => {
      acc += next
      table.tableGridReducedRTL.push(acc)
      return acc
    }, 0)
}
export function calculateTableBorders(
  table: Table,
  maxBottomMargins: ManagedArray<number>
) {
  if (true !== table.calculateInfo.tableBorders) return

  for (let i = 0, l = table.children.length; i < l; i++) {
    maxBottomMargins.push(0)
  }

  const tableBorders = table.getTableBorders()

  ManagedSliceUtil.forEach(table.children, (row, rowIndex) => {
    let curGridColIndex = 0
    ManagedSliceUtil.forEach(row.children, (cell, cellIndex) => {
      const gridSpan = cell.getGridSpan()

      row.setCellMetrics(cellIndex, curGridColIndex, 0, 0, 0, 0, 0)
      const vMergeCount = table.getVMergeCountBelow(
        rowIndex,
        curGridColIndex,
        gridSpan
      )

      const curMaxBottomMargin = maxBottomMargins.at(rowIndex + vMergeCount - 1)

      if (curMaxBottomMargin == null) return

      const vMerge = cell.getVMerge()
      if (vMerge !== VMergeType.Continue) {
        const cellMargins = cell.getMargins()
        if (cellMargins.bottom!.w > curMaxBottomMargin) {
          maxBottomMargins.set(
            rowIndex + vMergeCount - 1,
            cellMargins.bottom!.w
          )
        }
      }

      const cellBorders = cell.getBorders()
      if (vMerge === VMergeType.Continue) {
        cell.setTopBorderInfo([])
      } else {
        _calculateTopCellBorder(
          table,
          cell,
          rowIndex,
          tableBorders,
          cellBorders.top,
          curGridColIndex
        )
      }
      _calculateBottomCellBorder(
        table,
        cell,
        rowIndex,
        tableBorders,
        cellBorders.bottom,
        vMergeCount
      )
      curGridColIndex += gridSpan
    })
  })

  ManagedSliceUtil.forEach(table.children, (row, rowIndex) => {
    let curGridColIndex = 0
    ManagedSliceUtil.forEach(row.children, (cell, cellIndex) => {
      const cellBorders = cell.getBorders()
      const gridSpan = cell.getGridSpan()
      const vMerge = cell.getVMerge()
      const vMergeCount = table.getVMergeCountBelow(
        rowIndex,
        curGridColIndex,
        gridSpan
      )

      if (VMergeType.Continue !== vMerge) {
        _calculateLeftCellBorder(
          table,
          cell,
          cellIndex,
          rowIndex,
          tableBorders,
          cellBorders.left,
          vMergeCount,
          curGridColIndex
        )
        _calculateRightCellBorder(
          table,
          cell,
          cellIndex,
          rowIndex,
          tableBorders,
          cellBorders.right,
          vMergeCount,
          curGridColIndex
        )
      } else {
        cell.setLeftBorderInfo([], 0)
        cell.setRightBorderInfo([], 0)
      }
      curGridColIndex += gridSpan
    })
  })

  calculateTableMetrics(table)

  table.calculateInfo.tableBorders = false
}

function _calculateTopCellBorder(
  table: Table,
  cell: TableCell,
  rowIndex: number,
  tableBorders: TableBorders,
  cellTopBorder: TableCellBorder,
  curGridColIndex: number
) {
  const gridSpan = cell.getGridSpan()
  if (0 === rowIndex) {
    const rankedBorder = _rankBorders(
      tableBorders.top!,
      cellTopBorder,
      true,
      cellTopBorder?.useTableBorder!
    )

    const topBorderInfo = Array.from<TableCellBorder>({
      length: gridSpan,
    }).fill(rankedBorder)
    cell.setTopBorderInfo(topBorderInfo)
  } else {
    // We are looking for the first cell in the previous line that intersects with [curGridCol, curGridCol + GridSpan]
    const prevRow = table.children.at(rowIndex - 1)!
    const prevRowCellsCount = prevRow.cellsCount

    let prevIndex = -1

    let prevGridColIndex = 0
    for (let ci = 0; ci < prevRowCellsCount; ci++) {
      const prevCell = prevRow.getCellByIndex(ci)!
      const prevGridSpan = prevCell.getGridSpan()

      if (
        prevGridColIndex <= curGridColIndex + gridSpan - 1 &&
        prevGridColIndex + prevGridSpan - 1 >= curGridColIndex
      ) {
        prevIndex = ci
        break
      }

      prevGridColIndex += prevGridSpan
    }

    const topBorderInfo: TableCellBorder[] = []
    const hasCellTopBorderSet = cell.pr.tableCellBorders.top != null

    if (-1 !== prevIndex) {
      while (
        prevGridColIndex <= curGridColIndex + gridSpan - 1 &&
        prevIndex < prevRowCellsCount
      ) {
        let prevCell = prevRow.getCellByIndex(prevIndex)!
        const prevGridSpan = prevCell.getGridSpan()

        // If the given cell participates in a vertical merge,
        // then we find the bottom cell.
        const vMergeOfPrev = prevCell.getVMerge()
        if (VMergeType.Continue === vMergeOfPrev) {
          prevCell = table._getTopLeftMergedCell(
            rowIndex - 1,
            prevGridColIndex,
            prevGridSpan
          )!
        }

        let rankedBorder: TableCellBorder
        const prevCellGridStart = prevRow.getCellInfo(prevIndex).startGridCol
        if (prevCellGridStart < curGridColIndex) {
          if (hasCellTopBorderSet) {
            rankedBorder = cellTopBorder
          } else {
            const prevBottomBorder = prevCell.getBorder(
              CellBorderType.Bottom,
              true
            )
            rankedBorder = _rankBorders(
              prevBottomBorder,
              cellTopBorder,
              prevBottomBorder.useTableBorder,
              cellTopBorder.useTableBorder
            )
          }
        } else if (prevCellGridStart === curGridColIndex) {
          const prevBottomBorder = prevCell.getBorder(CellBorderType.Bottom)
          rankedBorder = _rankBorders(
            prevBottomBorder,
            cellTopBorder,
            prevBottomBorder.useTableBorder,
            cellTopBorder.useTableBorder
          )
        } else {
          const hasPrevCellBottomBorderSet =
            prevCell.pr.tableCellBorders.bottom != null
          const prevBottomBorder = prevCell.getBorder(CellBorderType.Bottom)
          if (hasPrevCellBottomBorderSet) {
            rankedBorder = prevBottomBorder
          } else {
            const _cellTopBorder = cell.getBorder(CellBorderType.Top, true)
            rankedBorder = _rankBorders(
              prevBottomBorder,
              _cellTopBorder,
              prevBottomBorder.useTableBorder,
              _cellTopBorder.useTableBorder
            )
          }
        }

        let addCount = 0
        if (prevGridColIndex >= curGridColIndex) {
          if (
            prevGridColIndex + prevGridSpan - 1 >
            curGridColIndex + gridSpan - 1
          ) {
            addCount = curGridColIndex + gridSpan - prevGridColIndex
          } else addCount = prevGridSpan
        } else if (
          prevGridColIndex + prevGridSpan - 1 >
          curGridColIndex + gridSpan - 1
        ) {
          addCount = gridSpan
        } else addCount = prevGridColIndex + prevGridSpan - curGridColIndex

        for (let i = 0; i < addCount; i++) {
          topBorderInfo.push(rankedBorder)
        }

        prevIndex++
        prevGridColIndex += prevGridSpan
      }
    }

    cell.setTopBorderInfo(topBorderInfo)
  }
}

function _calculateBottomCellBorder(
  table: Table,
  cell: TableCell,
  rowIndex: number,
  tableBorders: TableBorders,
  cellBottomBorder: TableCellBorder,
  vMergeCount: number
) {
  // 根据PowerPoint逻辑，合并单元格的下边框是设置rowSpan所在单元格下边框
  if (table.children.length - 1 === rowIndex + vMergeCount - 1) {
    const rankedBorder = _rankBorders(
      tableBorders.bottom!,
      cellBottomBorder,
      true,
      cellBottomBorder?.useTableBorder!
    )

    cell.setBottomBorderInfo([rankedBorder])
  } else {
    cell.setBottomBorderInfo([cellBottomBorder])
  }
}

function _calculateLeftCellBorder(
  table: Table,
  cell: TableCell,
  cellIndex: number,
  rowIndex: number,
  tableBorders: TableBorders,
  cellLeftBorder: TableCellBorder,
  vMergeCount: number,
  curGridColIndex: number
) {
  const bordersInfo: {
    left: TableCellBorder[]
    maxSizeOfLeft: number
  } = {
    left: [],
    maxSizeOfLeft: 0,
  }
  const rc = table.children.length - rowIndex
  const l = Math.min(vMergeCount, rc)
  if (cellIndex === 0) {
    const leftBorder = _rankBorders(
      tableBorders.left!,
      cellLeftBorder,
      true,
      cellLeftBorder?.useTableBorder!
    )
    if (
      TCBorderValue.Single === leftBorder.value &&
      leftBorder.size! > bordersInfo.maxSizeOfLeft
    ) {
      bordersInfo.maxSizeOfLeft = leftBorder.size!
    }

    bordersInfo.left = makeArrayOf(l, leftBorder)
  } else {
    for (let ri = 0; ri < l; ri++) {
      const curRIndex = rowIndex + ri
      const row = table.children.at(curRIndex)!
      const curCellIndex = table.getCellIndexByGridColIndex(
        curRIndex,
        curGridColIndex
      )

      const prevCell = row.getCellByIndex(curCellIndex - 1)!
      const curCell = row.getCellByIndex(curCellIndex)!

      const hasPrevCellSetRightBorder =
        prevCell.pr.tableCellBorders.right != null
      const hasCurCellSetLeftBorder = curCell.pr.tableCellBorders.left != null

      let leftBorder: TableCellBorder
      if (hasPrevCellSetRightBorder && !hasCurCellSetLeftBorder) {
        leftBorder = prevCell.getBorder(CellBorderType.Right)
      } else if (!hasPrevCellSetRightBorder && hasCurCellSetLeftBorder) {
        leftBorder = curCell.getBorder(CellBorderType.Left)
      } else {
        const prevRightCellBorder = prevCell.getBorder(
          CellBorderType.Right,
          !hasPrevCellSetRightBorder
        )
        const curLeftCellBorder = curCell.getBorder(
          CellBorderType.Left,
          !hasCurCellSetLeftBorder
        )
        leftBorder = _rankBorders(
          prevRightCellBorder,
          curLeftCellBorder,
          prevRightCellBorder.useTableBorder,
          curLeftCellBorder?.useTableBorder!
        )
      }
      if (
        TCBorderValue.Single === leftBorder.value &&
        leftBorder.size! > bordersInfo.maxSizeOfLeft
      ) {
        bordersInfo.maxSizeOfLeft = leftBorder.size!
      }

      bordersInfo.left.push(leftBorder)
    }
  }
  cell.setLeftBorderInfo(bordersInfo.left, bordersInfo.maxSizeOfLeft)
}

function _calculateRightCellBorder(
  table: Table,
  cell: TableCell,
  cellIndex: number,
  rowIndex: number,
  tableBorders: TableBorders,
  cellRightBorder: TableCellBorder,
  vMergeCount: number,
  curGridColIndex: number
) {
  const bordersInfo: {
    right: TableCellBorder[]
    maxSizeOfRight: number
  } = {
    right: [],
    maxSizeOfRight: 0,
  }
  const rc = table.children.length - rowIndex
  const l = Math.min(rc, vMergeCount)
  const vMergeFirstRow = table.children.at(rowIndex)!

  if (cellIndex === vMergeFirstRow.cellsCount - 1) {
    const rightBorder = _rankBorders(
      tableBorders.right!,
      cellRightBorder,
      true,
      cellRightBorder?.useTableBorder!
    )
    if (
      TCBorderValue.Single === rightBorder.value &&
      rightBorder.size! > bordersInfo.maxSizeOfRight
    ) {
      bordersInfo.maxSizeOfRight = rightBorder.size!
    }

    bordersInfo.right = makeArrayOf(l, rightBorder)
  } else {
    for (let ri = 0; ri < l; ri++) {
      const curRIndex = rowIndex + ri
      const row = table.children.at(curRIndex)!

      const curCellIndex = table.getCellIndexByGridColIndex(
        curRIndex,
        curGridColIndex
      )
      const nextCell = row.getCellByIndex(curCellIndex + 1)!
      const curCell = row.getCellByIndex(curCellIndex)!

      const hasNextCellSetLeftBorder = nextCell.pr.tableCellBorders.left != null
      const hasCurCellSetRightBorder = curCell.pr.tableCellBorders.right != null

      let rightBorder: TableCellBorder
      if (hasNextCellSetLeftBorder && !hasCurCellSetRightBorder) {
        rightBorder = nextCell.getBorder(CellBorderType.Left)
      } else if (!hasNextCellSetLeftBorder && hasCurCellSetRightBorder) {
        rightBorder = curCell.getBorder(CellBorderType.Right)
      } else {
        const nextLeftCellBorder = nextCell.getBorder(
          CellBorderType.Left,
          !hasNextCellSetLeftBorder
        )
        const curCellRightBorder = curCell.getBorder(
          CellBorderType.Right,
          !hasCurCellSetRightBorder
        )
        rightBorder = _rankBorders(
          nextLeftCellBorder,
          curCellRightBorder,
          nextLeftCellBorder.useTableBorder,
          curCellRightBorder?.useTableBorder!
        )
      }

      if (
        TCBorderValue.Single === rightBorder.value &&
        rightBorder.size! > bordersInfo.maxSizeOfRight
      ) {
        bordersInfo.maxSizeOfRight = rightBorder.size!
      }
      bordersInfo.right.push(rightBorder)
    }
  }
  cell.setRightBorderInfo(bordersInfo.right, bordersInfo.maxSizeOfRight)
}

export function calculateTableMetrics(table: Table) {
  const reducedTableGrid = table.getTableGridReduced()
  ManagedSliceUtil.forEach(table.children, (row) => {
    let curGridColIndex = 0
    let maxXInRow = 0
    let minXInRow = 0

    rowContentIterator(
      table.isRTL,
      row,
      (cell, _logicCellIndex, visualCellIndex) => {
        const gridSpan = cell.getGridSpan()
        const vMerge = cell.getVMerge()
        const cellMargins = cell.getMargins()

        const cellStartX = reducedTableGrid[curGridColIndex - 1]
        const cellEndX = reducedTableGrid[curGridColIndex + gridSpan - 1]
        let xOfTextDocStart = cellStartX
        let xOfTextDocEnd = cellEndX

        if (VMergeType.Continue === vMerge) {
          xOfTextDocStart += cellMargins.left!.w
          xOfTextDocEnd -= cellMargins.right!.w
        } else {
          const { maxWidthOfLeft, maxWidthOfRight } = cell.getBorderInfo()
          if (maxWidthOfLeft / 2 > cellMargins.left!.w) {
            xOfTextDocStart += maxWidthOfLeft / 2
          } else {
            xOfTextDocStart += cellMargins.left!.w
          }
          if (maxWidthOfRight / 2 > cellMargins.right!.w) {
            xOfTextDocEnd -= maxWidthOfRight / 2
          } else {
            xOfTextDocEnd -= cellMargins.right!.w
          }
        }

        if (0 === visualCellIndex) {
          minXInRow = cellStartX
        }

        const cellsCount = row.cellsCount
        if (cellsCount - 1 === visualCellIndex) {
          maxXInRow = cellEndX
        }

        const { startGridCol } = row.getCellInfo(cell.index)
        cell.setMetrics(
          startGridCol,
          curGridColIndex,
          cellStartX,
          cellEndX,
          xOfTextDocStart,
          xOfTextDocEnd
        )

        curGridColIndex += gridSpan
      }
    )
    row.setMetrics(minXInRow, maxXInRow)
  })
}

export function calculateTableGridCols(table: Table) {
  ManagedSliceUtil.forEach(table.children, (row) => {
    let valueOfCurGridCol = 0
    ManagedSliceUtil.forEach(row.children, (cell, cellIndex) => {
      row.setCellMetrics(cellIndex, valueOfCurGridCol, 0, 0, 0, 0, 0)
      cell.setMetrics(valueOfCurGridCol, 0, 0, 0, 0, 0)
      valueOfCurGridCol += cell.getGridSpan()
    })
  })
}

function updateXForDimension(table: Table) {
  table.x = 0
  table.dimension.x = table.x
  table.dimension.xLimit = table.xLimit
}

function calcDimension(table: Table, maxBottomMargins: ManagedArray<number>) {
  table.tableRowsBottomPosition.reset()
  for (let i = 0, l = table.children.length; i < l; i++) {
    table.tableRowsBottomPosition.push(0)
  }
  const dimension = table.dimension
  let y = dimension.y
  let tableHeight = 0

  let xMax = -1
  let xMin = -1

  ManagedSliceUtil.forEach(table.children, (row, rowIndex) => {
    table.rowsInfo[rowIndex] = new TableRowsInfo()
    table.tableRowsBottomPosition.set(rowIndex, y)

    let curGridColIndex = 0

    const maxXOfRow = row.metrics.xMax
    const minXOfRow = row.metrics.xMin

    if (-1 === xMin || minXOfRow < xMin) xMin = minXOfRow

    if (-1 === xMax || maxXOfRow > xMax) xMax = maxXOfRow

    let maxTopMargin: number = 0
    ManagedSliceUtil.forEach(row.children, (cell) => {
      const vMerge = cell.getVMerge()
      const cellMargin = cell.getMargins()
      if (VMergeType.Restart === vMerge && cellMargin.top!.w > maxTopMargin) {
        maxTopMargin = cellMargin.top!.w
      }
    })

    const rowHeight = row.getHeight()
    const rowHeightValue =
      rowHeight.value + maxBottomMargins.at(rowIndex)! + maxTopMargin

    const textVerticalCells: TableCell[] = []
    let isAllCellsVertical = true
    const cellsCount = row.cellsCount

    let rowBottomPosition: Nullable<number>

    for (let cellIndex = 0; cellIndex < cellsCount; cellIndex++) {
      let cell = row.getCellByIndex(cellIndex)!
      const gridSpan = cell.getGridSpan()
      const vMerge = cell.getVMerge()
      let cellMargin = cell.getMargins()

      row.updateCellMetrics(cellIndex)

      const cellInfos = row.getCellInfo(cellIndex)

      const xOfTextDocStart = dimension.x + cellInfos.xOfTextDocStart
      const xOfTextDocEnd = dimension.x + cellInfos.xOfTextDocEnd

      let yOfTextDocStart = y + cellMargin.top!.w
      let yOfTextDocEnd = dimension.yLimit

      const verticalMergeCount = table.getVMergeCountBelow(
        rowIndex,
        curGridColIndex,
        gridSpan
      )
      const bottomMargin =
        maxBottomMargins.at(rowIndex + verticalMergeCount - 1) ?? 0
      yOfTextDocEnd -= bottomMargin

      cell.calculateInfo.y = yOfTextDocStart

      if (verticalMergeCount > 1) {
        curGridColIndex += gridSpan
        continue
      } else {
        if (VMergeType.Restart !== vMerge) {
          cell = table._getTopLeftMergedCell(
            rowIndex,
            curGridColIndex,
            gridSpan
          )!
          cellMargin = cell.getMargins()

          yOfTextDocStart = cell.calculateInfo.y! + cellMargin.top!.w
        }
      }

      if (cell.isTextVertical()) {
        textVerticalCells.push(cell)
        curGridColIndex += gridSpan
        continue
      }

      isAllCellsVertical = false

      calculateTextDocumentByBounds(
        cell.textDoc,
        xOfTextDocStart,
        yOfTextDocStart,
        xOfTextDocEnd,
        yOfTextDocEnd,
        {
          x0: dimension.x + cellInfos.cellStartX,
          x1: dimension.x + cellInfos.cellEndX,
        }
      )

      const textDocBounds = cell.textDoc.getDocumentBounds()
      const boundsBottom = textDocBounds.bottom + bottomMargin

      rowBottomPosition = table.tableRowsBottomPosition.at(rowIndex)
      if (null == rowBottomPosition || rowBottomPosition < boundsBottom) {
        table.tableRowsBottomPosition.set(rowIndex, boundsBottom)
      }

      curGridColIndex += gridSpan
    }

    rowBottomPosition = table.tableRowsBottomPosition.at(rowIndex)
    if (null == rowBottomPosition || rowBottomPosition === 0) {
      table.tableRowsBottomPosition.set(rowIndex, y)
    }

    if (
      true === isAllCellsVertical &&
      LineHeightRule.Auto === rowHeight.heightRule
    ) {
      table.tableRowsBottomPosition.set(
        rowIndex,
        y + 4.5 + maxBottomMargins.at(rowIndex)! + maxTopMargin
      )
    }

    table.rowsInfo[rowIndex].y = y
    table.rowsInfo[rowIndex].diffYOfTop = 0
    table.rowsInfo[rowIndex].x0 = minXOfRow
    table.rowsInfo[rowIndex].x1 = maxXOfRow

    rowBottomPosition = table.tableRowsBottomPosition.at(rowIndex)!
    let cellHeight = rowBottomPosition - y
    if (
      (LineHeightRule.AtLeast === rowHeight.heightRule ||
        LineHeightRule.Exact === rowHeight.heightRule) &&
      cellHeight < rowHeightValue
    ) {
      cellHeight = rowHeightValue
      table.tableRowsBottomPosition.set(rowIndex, y + cellHeight)
    }
    table.rowsInfo[rowIndex].h = cellHeight
    y += cellHeight
    tableHeight += cellHeight
    row.height = cellHeight

    _calculateVerticalTextCellDimension(
      table,
      textVerticalCells,
      rowIndex,
      maxBottomMargins
    )
  })

  _calculateVerticalAlignCellDimension(table)

  dimension.bounds.bottom = dimension.bounds.top + tableHeight
  dimension.bounds.left = xMin + dimension.x
  dimension.bounds.right = xMax + dimension.x
  dimension.height = tableHeight

  return true
}

function _calculateVerticalTextCellDimension(
  table: Table,
  cells: TableCell[],
  rowIndex: number,
  maxBottomMargins: ManagedArray<number>
) {
  const dimension = table.dimension
  cells.forEach((cell) => {
    const gridSpan = cell.getGridSpan()
    const curGridColIndex = cell.metrics.startGridCol

    cell = table._getTopLeftMergedCell(rowIndex, curGridColIndex, gridSpan)!

    const cellMargin = cell.getMargins()
    const cellMetrics = cell.parent.getCellInfo(cell.index)

    const xOfTextDocStart = dimension.x + cellMetrics.xOfTextDocStart
    const xOfTextDocEnd = dimension.x + cellMetrics.xOfTextDocEnd
    const cellContentStartY = cell.calculateInfo.y!

    let cellContentEndY = table.tableRowsBottomPosition.at(rowIndex)!

    const verticalMergeCount = table.getVMergeCountBelow(
      rowIndex,
      curGridColIndex,
      gridSpan
    )
    const bottomMargin =
      maxBottomMargins.at(rowIndex + verticalMergeCount - 1) ?? 0
    cellContentEndY -= bottomMargin

    cell.calculateInfo.xStart = xOfTextDocStart
    cell.calculateInfo.yStart = cellContentStartY
    cell.calculateInfo.xEnd = xOfTextDocEnd
    cell.calculateInfo.yEnd = cellContentEndY

    cell.calculateInfo.cellStartX = dimension.x + cellMetrics.cellStartX
    cell.calculateInfo.cellEndX = dimension.x + cellMetrics.cellEndX
    cell.calculateInfo.cellStartY = cellContentStartY - cellMargin.top!.w
    cell.calculateInfo.cellEndY = cellContentEndY + bottomMargin

    calculateTextDocumentByBounds(
      cell.textDoc,
      0,
      0,
      cellContentEndY - cellContentStartY,
      10000
    )
  })
}

function _calculateVerticalAlignCellDimension(table: Table) {
  ManagedSliceUtil.forEach(table.children, (row, currentRowIndex) => {
    const cellsCount = row.cellsCount
    for (let ci = 0; ci < cellsCount; ci++) {
      let cell = row.getCellByIndex(ci)!
      const verticalMergeCount = table.getVMergeCountBelow(
        currentRowIndex,
        cell.metrics.startGridCol,
        cell.getGridSpan()
      )

      if (verticalMergeCount > 1 && currentRowIndex !== table.lastRow) {
        continue
      } else {
        const vMerge = cell.getVMerge()
        if (VMergeType.Restart !== vMerge) {
          cell = table._getTopLeftMergedCell(
            currentRowIndex,
            cell.metrics.startGridCol,
            cell.getGridSpan()
          )!
        }
      }

      const cellMargin = cell.getMargins()
      const vAlign = cell.getVAlign()
      const tmpRowIndex = cell.parent.index

      if (VertAlignJc.Top === vAlign) {
        cell.calculateInfo!.vAlignOffsetY = 0
        continue
      }
      const rowBottomPos = table.tableRowsBottomPosition.at(currentRowIndex)!
      const y0 = table.rowsInfo[tmpRowIndex].y + cellMargin.top!.w
      const y1 = rowBottomPos - cellMargin.bottom!.w
      const { bottom, top } = cell.textDoc.getDocumentBounds()
      const contentHeight = bottom - top

      let cellHeight = y1 - y0
      let dy = 0
      if (true === cell.isTextVertical()) {
        const cellInfos = row.getCellInfo(ci)
        cellHeight =
          cellInfos.cellEndX -
          cellInfos.cellStartX -
          cellMargin.left!.w -
          cellMargin.right!.w
      }

      if (cellHeight - contentHeight > 0.001) {
        if (VertAlignJc.Bottom === vAlign) {
          dy = cellHeight - contentHeight
        } else if (VertAlignJc.Center === vAlign) {
          dy = (cellHeight - contentHeight) / 2
        }

        cell.textDoc.renderingState.translateBy(0, dy)
      }

      cell.calculateInfo.vAlignOffsetY = dy
    }
  })
}

function updateYForDimension(table: Table) {
  const limits = table.parent.getSlideLimits()
  const dimension = table.dimension
  const { x, y } = limits
  table.translateBy(x - dimension.x, y - dimension.y)
}

function getMinTableWidth(table: Table) {
  let minWidth: number = 0
  ManagedSliceUtil.forEach(table.children, (row) => {
    let rowWidth: number = 0
    ManagedSliceUtil.forEach(row.children, (cell) => {
      const margins = cell.getMargins()
      rowWidth += margins.left!.w + margins.right!.w
    })
    minWidth = minWidth < rowWidth ? rowWidth : minWidth
  })
  return minWidth
}

function getGridsOfMinWidth(table: Table) {
  const colsCount = table.tableGridCalculated.length
  const result: number[] = []
  for (let i = -1; i < colsCount; i++) result[i] = 0

  const rowsCount = table.children.length
  for (let ri = 0; ri < rowsCount; ri++) {
    const row = table.children.at(ri)!
    const cellsCount = row.cellsCount

    let curGridColIndex = 0

    for (let i = 0; i < cellsCount; i++) {
      const cell = row.getCellByIndex(i)!
      const cellMargins = cell.getMargins()
      const gridSpan = cell.getGridSpan()

      const cellMinWidth = cellMargins.left!.w + cellMargins.right!.w

      if (
        result[curGridColIndex + gridSpan - 1] <
        result[curGridColIndex - 1] + cellMinWidth
      ) {
        result[curGridColIndex + gridSpan - 1] =
          result[curGridColIndex - 1] + cellMinWidth
      }

      curGridColIndex += gridSpan
    }
  }

  return result
}

function scaleTableWidth(table: Table, grids: number[], tableW: number) {
  const minSumGrid = getGridsOfMinWidth(table)

  const gridsToModify: boolean[] = []
  for (let i = 0; i < grids.length; i++) {
    gridsToModify[i] = true
  }

  let count = gridsToModify.length

  const tableGrid: number[] = []
  tableGrid[0] = grids[0]
  for (let i = 1; i < grids.length; i++) {
    tableGrid[i] = grids[i] - grids[i - 1]
  }

  const minTableGrid: number[] = []
  minTableGrid[0] = minSumGrid[0]
  for (let i = 1; i < minSumGrid.length; i++) {
    minTableGrid[i] = minSumGrid[i] - minSumGrid[i - 1]
  }

  let curSpanWidth = grids[grids.length - 1]
  while (count > 0 && curSpanWidth > 0.001) {
    const factor = tableW / curSpanWidth

    const curTableGrid: number[] = []
    for (let i = 0; i < tableGrid.length; i++) {
      curTableGrid[i] = tableGrid[i]
    }

    for (let i = 0; i <= curTableGrid.length - 1; i++) {
      if (true === gridsToModify[i]) {
        curTableGrid[i] = curTableGrid[i] * factor
      }
    }

    let isBreak = true

    for (let i = 0; i <= curTableGrid.length - 1; i++) {
      if (
        true === gridsToModify[i] &&
        curTableGrid[i] - minTableGrid[i] < 0.001
      ) {
        isBreak = false
        gridsToModify[i] = false
        count--

        curSpanWidth -= tableGrid[i]
        tableW -= minTableGrid[i]

        tableGrid[i] = minTableGrid[i]
      }
    }

    if (true === isBreak) {
      for (let i = 0; i <= curTableGrid.length - 1; i++) {
        if (true === gridsToModify[i]) {
          tableGrid[i] = curTableGrid[i]
        }
      }

      break
    }
  }

  const result: number[] = []
  result[-1] = 0
  for (let i = 0; i < tableGrid.length; i++) {
    result[i] = tableGrid[i] + result[i - 1]
  }

  return result
}

function _rankBorders(
  border: TableCellBorder,
  _border: TableCellBorder,
  isTableBorder: boolean = false,
  _isTableBorder: boolean = false
) {
  // Cell border always trumps table border if first is given
  if (true === isTableBorder && _isTableBorder !== true) return _border

  if (true === _isTableBorder && isTableBorder !== true) return border

  // Non-empty border always wins
  if (TCBorderValue.None === border.value) return _border

  if (TCBorderValue.None === _border.value) return border

  // TODO: As soon as we implement drawing not only simple borders,
  // do processing here. W_b = Border.Size * Border_Num,
  // where Border_Num depends on Border.Value

  const bSize1 = border.size!
  const bSize2 = _border.size!
  if (bSize1 > bSize2) return border
  else if (bSize2 > bSize1) return _border

  let brightness1 = border.color!.r + border.color!.b + 2 * border.color!.g
  let brightness2 = _border.color!.r + _border.color!.b + 2 * _border.color!.g

  if (brightness1 < brightness2) return border
  else if (brightness2 < brightness1) return _border

  brightness1 = border.color!.b + 2 * border.color!.g
  brightness2 = _border.color!.b + 2 * _border.color!.g

  if (brightness1 < brightness2) return border
  else if (brightness2 < brightness1) return _border

  brightness1 = border.color!.g
  brightness2 = _border.color!.g

  if (brightness1 < brightness2) return border
  else if (brightness2 < brightness1) return _border

  // The two borders are ally identical, we don't care which one to draw.
  return border
}
