import { EditActionFlag } from '../../EditActionFlag'
import { Shape } from '../../SlideElement/Shape'
import { CalcStatus } from './calcStatus'
import {
  onModifyPosition,
  onModifyFill,
  onModifyLn,
  onModifyGeometry,
} from './onModifyAttributes'
import { updateCalcStatusForSlideElementParent } from './utils'

export function updateCalcStatusForShape(sp: Shape, editType: EditActionFlag) {
  let isModified: boolean = false
  switch (editType) {
    case EditActionFlag.edit_ShapeSetSpPr: {
      // 现在更新的原子单位是shape，需要全量更新spPr
      onModifyPosition(sp)
      onModifyFill(sp)
      onModifyLn(sp)
      onModifyGeometry(sp)
      isModified = true
      break
    }
    case EditActionFlag.edit_ShapeSetTxBody:
    case EditActionFlag.edit_ShapeSetBodyPr: {
      updateTextCalcStatusForShape(sp)
      isModified = true
      break
    }

    case EditActionFlag.edit_ShapeSetDeleted:
    case EditActionFlag.edit_ShapeSetNvSpPr:
    case EditActionFlag.edit_ShapeSetParent:
    case EditActionFlag.edit_ShapeSetGroup:
    case EditActionFlag.edit_ShapeSetStyle: {
      break
    }
    default: {
      updateTextCalcStatusForShape(sp)
      isModified = true
    }
  }
  if (isModified) {
    updateCalcStatusForSlideElementParent(sp)
  }
}

function updateTextCalcStatusForShape(sp: Shape) {
  sp.setCalcStatusOf(CalcStatus.Shape.Content)
  sp.setCalcStatusOf(CalcStatus.Shape.PlaceholderContent)
  sp.setCalcStatusOf(CalcStatus.Shape.TextTransform)
}
