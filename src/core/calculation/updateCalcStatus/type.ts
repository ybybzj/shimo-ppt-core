import { Slide } from '../../Slide/Slide'
import { SlideLayout } from '../../Slide/SlideLayout'
import { SlideMaster } from '../../Slide/SlideMaster'
import { GraphicFrame } from '../../SlideElement/GraphicFrame'
import { GroupShape } from '../../SlideElement/GroupShape'
import { ImageShape } from '../../SlideElement/Image'
import { Shape } from '../../SlideElement/Shape'

export type EntityWithCalcStatus =
  | Shape
  | ImageShape
  | GroupShape
  | GraphicFrame
  | Slide
  | SlideLayout
  | SlideMaster
