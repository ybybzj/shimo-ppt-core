import { Notes } from '../../Slide/Notes'
import { CalcStatus } from './calcStatus'

export function updateCalcStatusForNotes(notes: Notes) {
  const slide = notes.slide
  if (slide) {
    slide.setCalcStatusOf(CalcStatus.Slide.Notes)
  }
}
