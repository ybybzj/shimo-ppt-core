import { Opaque, valuesOfDict } from '../../../../liber/pervasive'
import { EntityWithCalcStatus } from './type'

type CalcStatusFlag<P extends string, V extends number> = Opaque<
  `${P}CalcStatusFlag`,
  V
>

const Shape = {
  Content: (0b1 << 11) as CalcStatusFlag<'ShapeContent', 0b100000000000>,
  Brush: (0b1 << 10) as CalcStatusFlag<'ShapeBrush', 0b010000000000>,
  Pen: (0b1 << 9) as CalcStatusFlag<'ShapePen', 0b001000000000>,
  Transform: (0b1 << 8) as CalcStatusFlag<'ShapeTransform', 0b000100000000>,
  TextTransform: (0b1 << 7) as CalcStatusFlag<
    'ShapeTextTransform',
    0b000010000000
  >,
  Bounds: (0b1 << 6) as CalcStatusFlag<'ShapeBounds', 0b000001000000>,
  Geometry: (0b1 << 5) as CalcStatusFlag<'ShapeGeometry', 0b000000100000>,
  Style: (0b1 << 4) as CalcStatusFlag<'ShapeStyle', 0b000000010000>,
  Fill: (0b1 << 3) as CalcStatusFlag<'ShapeFill', 0b000000001000>,
  Line: (0b1 << 2) as CalcStatusFlag<'ShapeLine', 0b000000000100>,
  Transparent: (0b1 << 1) as CalcStatusFlag<'ShapeTransparent', 0b000000000010>,
  PlaceholderContent: 0b1 as CalcStatusFlag<
    'ShapePlaceholderContent',
    0b000000000001
  >,
} as const

export type ShapeCalcStatusCode = Opaque<'ShapeCalcStatusCode', number>
export type ShapeCalcStatusFlags = valuesOfDict<typeof Shape>
export const initCalcStatusCodeForShape =
  Shape.Pen |
  Shape.Fill |
  Shape.Line |
  Shape.Brush |
  Shape.Style |
  Shape.Content |
  Shape.Bounds |
  Shape.Geometry |
  Shape.Transform |
  Shape.Transparent |
  Shape.TextTransform |
  Shape.PlaceholderContent

const ImageShape = {
  Brush: (0b1 << 8) as CalcStatusFlag<'ImageShapeBrush', 0b100000000>,
  Pen: (0b1 << 7) as CalcStatusFlag<'ImageShapePen', 0b010000000>,
  Transform: (0b1 << 6) as CalcStatusFlag<'ImageShapeTransform', 0b001000000>,
  Bounds: (0b1 << 5) as CalcStatusFlag<'ImageShapeBounds', 0b000100000>,
  Geometry: (0b1 << 4) as CalcStatusFlag<'ImageShapeGeometry', 0b000010000>,
  Style: (0b1 << 3) as CalcStatusFlag<'ImageShapeStyle', 0b000001000>,
  Fill: (0b1 << 2) as CalcStatusFlag<'ImageShapeFill', 0b000000100>,
  Line: (0b1 << 1) as CalcStatusFlag<'ImageShapeLine', 0b000000010>,
  Transparent: 0b1 as CalcStatusFlag<'ImageShapeTransparent', 0b000000001>,
} as const

export type ImageShapeCalcStatusCode = Opaque<
  'ImageShapeCalcStatusCode',
  number
>
export type ImageShapeCalcStatusFlags = valuesOfDict<typeof ImageShape>

export const initCalcStatusCodeForImageShape =
  ImageShape.Transparent |
  ImageShape.Transform |
  ImageShape.Geometry |
  ImageShape.Bounds |
  ImageShape.Style |
  ImageShape.Brush |
  ImageShape.Line |
  ImageShape.Fill |
  ImageShape.Pen

const GroupShape = {
  Brush: (0b1 << 5) as CalcStatusFlag<'GroupShapeBrush', 0b100000>,
  Pen: (0b1 << 4) as CalcStatusFlag<'GroupShapePen', 0b010000>,
  Transform: (0b1 << 3) as CalcStatusFlag<'GroupShapeTransform', 0b001000>,
  ChildrenObjects: (0b1 << 2) as CalcStatusFlag<
    'GroupShapeChildrenObjects',
    0b000100
  >,
  Bounds: (0b1 << 1) as CalcStatusFlag<'GroupShapeBounds', 0b000010>,
  ScaleFactors: 0b1 as CalcStatusFlag<'GroupShapeScaleFactors', 0b000001>,
} as const

export type GroupShapeCalcStatusCode = Opaque<
  'GroupShapeCalcStatusCode',
  number
>
export type GroupShapeCalcStatusFlags = valuesOfDict<typeof GroupShape>

export const initCalcStatusCodeForGroupShape =
  GroupShape.Pen |
  GroupShape.Brush |
  GroupShape.Bounds |
  GroupShape.Transform |
  GroupShape.ScaleFactors |
  GroupShape.ChildrenObjects

const GraphicFrame = {
  Transform: (0b1 << 5) as CalcStatusFlag<'GraphicFrameTransform', 0b100000>,
  Sizes: (0b1 << 4) as CalcStatusFlag<'GraphicFrameSizes', 0b010000>,
  Numbering: (0b1 << 3) as CalcStatusFlag<'GraphicFrameNumbering', 0b001000>,
  Table: (0b1 << 2) as CalcStatusFlag<'GraphicFrameTable', 0b000100>,
  Content: (0b1 << 1) as CalcStatusFlag<'GraphicFrameContent', 0b000010>,
  TextTransform: 0b1 as CalcStatusFlag<'GraphicFrameTextTransform', 0b000001>,
} as const

export type GraphicFrameCalcStatusCode = Opaque<
  'GraphicFrameCalcStatusCode',
  number
>
export type GraphicFrameCalcStatusFlags = valuesOfDict<typeof GraphicFrame>

export const initCalcStatusCodeForGraphicFrame =
  GraphicFrame.Transform |
  GraphicFrame.TextTransform |
  GraphicFrame.Content |
  GraphicFrame.Table |
  GraphicFrame.Sizes |
  GraphicFrame.Numbering

const Slide = {
  Comments: (0b1 << 3) as CalcStatusFlag<'SlideComments', 0b1000>,
  Background: (0b1 << 2) as CalcStatusFlag<'SlideBackground', 0b0100>,
  SpTree: (0b1 << 1) as CalcStatusFlag<'SlideSpTree', 0b0010>,
  Notes: 0b1 as CalcStatusFlag<'SlideNotes', 0b0001>,
} as const
export type SlideCalcStatusCode = Opaque<'SlideCalcStatusCode', number>
export type SlideCalcStatusFlags = valuesOfDict<typeof Slide>
export const initCalcStatusCodeForSlide =
  Slide.Notes | Slide.SpTree | Slide.Background | Slide.Comments

const SlideLayout = {
  Background: (0b1 << 2) as CalcStatusFlag<'SlideLayoutBackground', 0b100>,
  SpTree: (0b1 << 1) as CalcStatusFlag<'SlideLayoutSpTree', 0b010>,
  Bounds: 0b1 as CalcStatusFlag<'SlideLayoutBounds', 0b001>,
} as const
export type SlideLayoutCalcStatusCode = Opaque<
  'SlideLayoutCalcStatusCode',
  number
>
export type SlideLayoutCalcStatusFlags = valuesOfDict<typeof SlideLayout>
export const initCalcStatusCodeForSlideLayout =
  SlideLayout.Background | SlideLayout.SpTree | SlideLayout.Bounds

const SlideMaster = {
  Background: (0b1 << 2) as CalcStatusFlag<'SlideMasterBackground', 0b100>,
  SpTree: (0b1 << 1) as CalcStatusFlag<'SlideMasterSpTree', 0b010>,
  Bounds: 0b1 as CalcStatusFlag<'SlideMasterBounds', 0b001>,
} as const
export type SlideMasterCalcStatusCode = Opaque<
  'SlideMasterCalcStatusCode',
  number
>
export type SlideMasterCalcStatusFlags = valuesOfDict<typeof SlideMaster>
export const initCalcStatusCodeForSlideMaster =
  SlideMaster.Bounds | SlideMaster.SpTree | SlideMaster.Background

export const CalcStatus = {
  Shape,
  ImageShape,
  GroupShape,
  GraphicFrame,
  Slide,
  SlideLayout,
  SlideMaster,
} as const

export function unsetAllCalcStatus(e: EntityWithCalcStatus) {
  e.calcStatusCode = 0 as any
}

export function hasCalcStatusSet(e: EntityWithCalcStatus) {
  return e.calcStatusCode > 0
}
