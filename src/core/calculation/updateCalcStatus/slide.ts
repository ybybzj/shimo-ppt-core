import { EditActionFlag } from '../../EditActionFlag'
import { Slide } from '../../Slide/Slide'

import { CalcStatus } from './calcStatus'
import { onModifyTheme } from './onModifyAttributes'

export function updateCalcStatusForSlide(
  slide: Slide,
  editFlag: EditActionFlag
) {
  if (slide) {
    switch (editFlag) {
      case EditActionFlag.edit_Slide_SetBg: {
        slide.setCalcStatusOf(CalcStatus.Slide.Background)
        break
      }
      case EditActionFlag.edit_SlideSetSize:
      case EditActionFlag.edit_Slide_SetLayout: {
        invalidSlideThemeCalcStatus(slide)
        break
      }
      case EditActionFlag.edit_SlideSetNotes: {
        slide.setCalcStatusOf(CalcStatus.Slide.Notes)
        break
      }
      case EditActionFlag.edit_Slide_SetComments: {
        slide.setCalcStatusOf(CalcStatus.Slide.Comments)
        break
      }
      case EditActionFlag.edit_Slide_AddToSpTree:
      case EditActionFlag.edit_SlideReplaceFromSpTree:
      case EditActionFlag.edit_Slide_RemoveFromSpTree: {
        slide.setCalcStatusOf(CalcStatus.Slide.SpTree)
        break
      }
      default: {
        slide.setCalcStatusOf(CalcStatus.Slide.Notes)
        break
      }
    }
  }
}

export function invalidSlideThemeCalcStatus(slide: Slide) {
  slide.setCalcStatusOf(CalcStatus.Slide.SpTree)
  slide.setCalcStatusOf(CalcStatus.Slide.Background)
  for (let i = 0; i < slide.cSld.spTree.length; ++i) {
    onModifyTheme(slide.cSld.spTree[i])
  }
}
