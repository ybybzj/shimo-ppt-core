import { EditActionFlag } from '../../EditActionFlag'
import { ImageShape } from '../../SlideElement/Image'
import { CalcStatus } from './calcStatus'
import {
  onModifyPosition,
  onModifyFill,
  onModifyLn,
  onModifyGeometry,
} from './onModifyAttributes'
import { updateCalcStatusForSlideElementParent } from './utils'

export function updateCalcStatusForImageShape(
  image: ImageShape,
  editType: EditActionFlag
) {
  let isModified: boolean = false
  switch (editType) {
    case EditActionFlag.edit_ImageShapeSetBlipFill: {
      image.setCalcStatusOf(CalcStatus.ImageShape.Brush)
      image.setCalcStatusOf(CalcStatus.ImageShape.Fill)
      isModified = true
      break
    }
    case EditActionFlag.edit_ShapeSetDeleted: {
      break
    }
    case EditActionFlag.edit_ImageShapeSetSpPr: {
      // 现在更新的原子单位是shape，需要全量更新spPr
      onModifyPosition(image)
      onModifyFill(image)
      onModifyLn(image)
      onModifyGeometry(image)
      isModified = true
      break
    }
  }

  if (isModified) {
    updateCalcStatusForSlideElementParent(image)
  }
}
