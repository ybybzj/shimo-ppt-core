import { InstanceType } from '../../instanceTypes'
import { TextDocument } from '../../TextDocument/TextDocument'
import { TextBody } from '../../SlideElement/TextBody'
import { updateCalcStatusForGraphicFrameItem } from './graphicFrame'
import { getParentDoc, TextContentItem } from '../utils'
import { CalcStatus } from './calcStatus'
import { updateCalcStatusForSlideElementParent } from './utils'
import { EditActionFlag } from '../../EditActionFlag'
import { searchManager } from '../../search/SearchManager'

export function updateCalcStatusForTextBody(
  textBody: TextBody,
  _flag?: EditActionFlag
) {
  const sp = textBody.parent
  if (sp) {
    sp.setCalcStatusOf(CalcStatus.Shape.Content)
    sp.setCalcStatusOf(CalcStatus.Shape.PlaceholderContent)
    sp.setCalcStatusOf(CalcStatus.Shape.TextTransform)
    updateCalcStatusForSlideElementParent(sp)
    if (_flag === EditActionFlag.edit_TextBodySetContent) {
      searchManager.isClearSearch = true
    }
  }
}

export function updateCalcStatusForTextDocument(textDoc: TextDocument) {
  const parent = textDoc.parent
  if (parent) {
    switch (parent.instanceType) {
      case InstanceType.TextBody: {
        updateCalcStatusForTextBody(parent)
        break
      }
      case InstanceType.TableCell: {
        updateCalcStatusForGraphicFrameItem(parent)
        break
      }
    }
  }
}

export function isParentOfTextDocChanged(txtDoc: TextDocument): boolean {
  const parent = txtDoc.parent
  if (parent) {
    switch (parent.instanceType) {
      case InstanceType.TextBody: {
        const sp = parent.parent
        if (sp) {
          return (
            sp.isCalcStatusSet(CalcStatus.Shape.Content) ||
            sp.isCalcStatusSet(CalcStatus.Shape.PlaceholderContent)
          )
        } else {
          return false
        }
      }
      case InstanceType.TableCell: {
        const sp = parent.parent?.parent?.parent
        if (sp) {
          return sp.isCalcStatusSet(CalcStatus.GraphicFrame.Table)
        } else {
          return false
        }
      }
    }
  }

  return false
}

export function updateCalcStatusForTextContentItem(
  docItem: TextContentItem | TextDocument
) {
  const textDoc = getParentDoc(docItem)
  if (textDoc) {
    switch (docItem.instanceType) {
      case InstanceType.ParaRun:
      case InstanceType.ParaHyperlink: {
        docItem.paragraph.hasCalculated = false
        break
      }
      case InstanceType.Paragraph: {
        docItem.hasCalculated = false
        break
      }
      case InstanceType.ParaTextPr: {
        docItem.parent.hasCalculated = false
        break
      }
    }

    textDoc.hasCalculated = false
    updateCalcStatusForTextDocument(textDoc)
  }
}
