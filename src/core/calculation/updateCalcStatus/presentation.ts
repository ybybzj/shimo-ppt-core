import { EditActionFlag } from '../../EditActionFlag'
import { InstanceType } from '../../instanceTypes'
import { EditChange } from '../../changes/type'
import { Presentation } from '../../Slide/Presentation'
import { Slide } from '../../Slide/Slide'
import { SlideLayout } from '../../Slide/SlideLayout'
import { SlideMaster } from '../../Slide/SlideMaster'
import { updatePresentationFields } from '../../utilities/fields'
import { getLayoutAndMasterMaps } from '../../utilities/presentation/getters'
import { isPlaceholder } from '../../utilities/shape/asserts'
import { onModifyExtents, onModifyFill, onModifyLn } from './onModifyAttributes'

import { invalidSlideThemeCalcStatus } from './slide'
import {
  setBackgroudCalcStatusForContainer,
  setSpTreeCalcStatusForContainer,
} from './utils'

export function updateCalcStatusForPresentation(
  presentation: Presentation,
  change: EditChange
) {
  const changeCategory = change.Category
  const editType = change.editType

  const slides = presentation.slides
  switch (changeCategory) {
    case 'PropertyChange': {
      switch (editType) {
        case EditActionFlag.edit_Presentation_SetDefaultTextStyle: {
          for (let i = 0; i < slides.length; ++i) {
            setCalcStatusOnSizeChange(slides[i])
          }
          break
        }
        case EditActionFlag.edit_Presentation_SlideSize: {
          const { layouts, masters } = getLayoutAndMasterMaps(presentation)
          for (const key in masters) {
            if (masters.hasOwnProperty(key)) {
              setCalcStatusOnSizeChange(masters[key])
            }
          }
          for (const key in layouts) {
            if (layouts.hasOwnProperty(key)) {
              setCalcStatusOnSizeChange(layouts[key])
            }
          }
          for (let i = 0; i < slides.length; ++i) {
            setCalcStatusOnSizeChange(slides[i])
          }
          break
        }
      }
      break
    }
    case 'SpContentChange': {
      switch (editType) {
        case EditActionFlag.edit_Presentation_AddSlide: {
          for (let i = change.index; i < slides.length; ++i) {
            if (slides[i]) {
              updatePresentationFields(slides[i])
            }
          }
          break
        }
        case EditActionFlag.edit_Presentation_RemoveSlide: {
          for (let i = change.index; i < slides.length; ++i) {
            if (slides[i]) {
              updatePresentationFields(slides[i])
            }
          }
          break
        }
        case EditActionFlag.edit_Presentation_AddSlideMaster: {
          break
        }
      }
      break
    }

    case 'ThemeChange': {
      switch (editType) {
        case EditActionFlag.edit_Presentation_ChangeTheme: {
          for (let i = 0; i < change.indexes.length; ++i) {
            slides[change.indexes[i]] &&
              invalidSlideThemeCalcStatus(slides[change.indexes[i]])
          }
          break
        }
        case EditActionFlag.edit_Presentation_ChangeColorScheme: {
          const { layouts, masters } = getLayoutAndMasterMaps(presentation)
          for (const key in masters) {
            if (masters.hasOwnProperty(key)) {
              setCalcStatusOnColorSchemeChange(masters[key])
            }
          }
          for (const key in layouts) {
            if (layouts.hasOwnProperty(key)) {
              setCalcStatusOnColorSchemeChange(layouts[key])
            }
          }
          for (let i = 0; i < change.indexes.length; ++i) {
            slides[change.indexes[i]] &&
              setCalcStatusOnColorSchemeChange(slides[change.indexes[i]])
          }
          break
        }
      }
      break
    }
  }
}

function setCalcStatusOnSizeChange(
  container: Slide | SlideLayout | SlideMaster
) {
  setSpTreeCalcStatusForContainer(container)
  for (let i = 0; i < container.cSld.spTree.length; ++i) {
    const sp = container.cSld.spTree[i]
    onModifyExtents(sp)
  }
}

function setCalcStatusOnColorSchemeChange(
  container: Slide | SlideLayout | SlideMaster
) {
  setSpTreeCalcStatusForContainer(container)
  setBackgroudCalcStatusForContainer(container)
  for (let i = 0; i < container.cSld.spTree.length; ++i) {
    const sp = container.cSld.spTree[i]
    if (container.instanceType === InstanceType.Slide || !isPlaceholder(sp)) {
      onModifyFill(sp)
      onModifyLn(sp)
    }
  }
}
