import { EditActionFlag } from '../../EditActionFlag'
import { SpPr, Xfrm } from '../../SlideElement/attrs/shapePrs'
import { Geometry } from '../../SlideElement/geometry/Geometry'
import {
  onModifyGeometry,
  onModifyPosition,
  onModifyExtents,
  onModifyFlip,
  onModifyRot,
  onModifyFill,
  onModifyLn,
} from './onModifyAttributes'
import { updateCalcStatusForSlideElementParent } from './utils'

export function updateCalcStatusForGeometry(geometry: Geometry) {
  const sp = geometry.parent?.parent
  if (sp) {
    onModifyGeometry(sp)
    updateCalcStatusForSlideElementParent(sp)
  }
}

export function updateCalcStatusForXfrm(xfrm: Xfrm, editType: EditActionFlag) {
  const sp = xfrm.parent?.parent
  if (sp == null) {
    return
  }

  let isModified: boolean = false
  switch (editType) {
    case EditActionFlag.edit_Xfrm_SetOffX:
    case EditActionFlag.edit_Xfrm_SetOffY: {
      onModifyPosition(sp)
      isModified = true
      break
    }
    case EditActionFlag.edit_Xfrm_SetExtX:
    case EditActionFlag.edit_Xfrm_SetExtY: {
      onModifyExtents(sp)
      isModified = true
      break
    }
    case EditActionFlag.edit_Xfrm_SetChOffX:
    case EditActionFlag.edit_Xfrm_SetChOffY: {
      onModifyPosition(sp)
      isModified = true
      break
    }
    case EditActionFlag.edit_Xfrm_SetChExtX:
    case EditActionFlag.edit_Xfrm_SetChExtY: {
      onModifyExtents(sp)
      isModified = true
      break
    }
    case EditActionFlag.edit_Xfrm_SetFlipH:
    case EditActionFlag.edit_Xfrm_SetFlipV: {
      onModifyFlip(sp)
      isModified = true
      break
    }
    case EditActionFlag.edit_Xfrm_SetRot: {
      onModifyRot(sp)
      isModified = true
      break
    }
  }

  if (isModified) {
    updateCalcStatusForSlideElementParent(sp)
  }
}

export function updateCalcStatusForSpPr(spPr: SpPr, editType: EditActionFlag) {
  const sp = spPr.parent
  if (sp == null) {
    return
  }
  let isModified: boolean = false
  switch (editType) {
    case EditActionFlag.edit_SpPr_SetGeometry: {
      onModifyGeometry(sp)
      isModified = true
      break
    }
    case EditActionFlag.edit_SpPr_SetFill: {
      onModifyFill(sp)
      isModified = true
      break
    }
    case EditActionFlag.edit_SpPr_SetLn: {
      onModifyLn(sp)
      isModified = true
      break
    }
    case EditActionFlag.edit_SpPr_SetXfrm: {
      onModifyPosition(sp)
      onModifyExtents(sp)
      onModifyPosition(sp)
      onModifyExtents(sp)
      onModifyFlip(sp)
      onModifyRot(sp)
      isModified = true
      break
    }
  }

  if (isModified) {
    updateCalcStatusForSlideElementParent(sp)
  }
}
