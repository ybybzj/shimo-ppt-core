import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import { GraphicFrame } from '../../SlideElement/GraphicFrame'
import { Table } from '../../Table/Table'
import { TableCell } from '../../Table/TableCell'
import { TableRow } from '../../Table/TableRow'
import { CalcStatus } from './calcStatus'
import { updateCalcStatusForSlideElementParent } from './utils'
export function updateCalcStatusForGraphicFrame(sp: GraphicFrame) {
  sp.setCalcStatusOf(CalcStatus.GraphicFrame.Table)
  sp.setCalcStatusOf(CalcStatus.GraphicFrame.Sizes)
  sp.setCalcStatusOf(CalcStatus.GraphicFrame.Transform)
  updateCalcStatusForSlideElementParent(sp)
}

export function updateCalcStatusForGraphicFrameItem(
  item: TableCell | TableRow | Table | GraphicFrame
) {
  let sp: Nullable<GraphicFrame>
  let table: Nullable<Table>
  switch (item.instanceType) {
    case InstanceType.TableCell: {
      table = item.parent?.parent
      sp = table?.parent
      break
    }
    case InstanceType.TableRow: {
      table = item.parent
      sp = table?.parent
      break
    }
    case InstanceType.Table: {
      table = item
      sp = item.parent
      break
    }
    case InstanceType.GraphicFrame: {
      sp = item
      break
    }
  }

  if (table) {
    table.calculateInfo.calculateBorders()
  }

  if (sp && sp.instanceType === InstanceType.GraphicFrame) {
    updateCalcStatusForGraphicFrame(sp)
  }
}
