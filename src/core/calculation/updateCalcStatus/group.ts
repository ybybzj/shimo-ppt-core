import { EditActionFlag } from '../../EditActionFlag'
import { GroupShape } from '../../SlideElement/GroupShape'
import { onModifyPosition, onModifyFill } from './onModifyAttributes'
import {
  updateCalcStatusForSlideElementParent,
  updateChildrenCalcStatusForGroup,
} from './utils'
export function updateCalcStatusForGroup(
  group: GroupShape,
  editType: EditActionFlag
) {
  let isModified: boolean = false
  switch (editType) {
    case EditActionFlag.edit_ShapeSetDeleted: {
      break
    }
    case EditActionFlag.edit_GroupShapeAddToSpTree:
    case EditActionFlag.edit_GroupShapeRemoveFromSpTree: {
      if (!group.isDeleted) {
        updateChildrenCalcStatusForGroup(group)
        isModified = true
      }
      break
    }
    case EditActionFlag.edit_GroupShapeSetSpPr: {
      // 现在更新的原子单位是shape，需要全量更新spPr
      onModifyPosition(group)
      onModifyFill(group)
      isModified = true
      break
    }
  }

  if (isModified) {
    updateCalcStatusForSlideElementParent(group)
  }
}
