import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { GroupShape } from '../../SlideElement/GroupShape'
import { SlideElementContainer } from '../../utilities/type'
import { CalcStatus } from './calcStatus'

export function setTransformCalcStatus(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      sp.setCalcStatusOf(CalcStatus.Shape.Transform)
      break
    case InstanceType.GraphicFrame: {
      sp.setCalcStatusOf(CalcStatus.GraphicFrame.Transform)
      break
    }
    case InstanceType.ImageShape: {
      sp.setCalcStatusOf(CalcStatus.ImageShape.Transform)
      break
    }
    case InstanceType.GroupShape: {
      sp.setCalcStatusOf(CalcStatus.GroupShape.ScaleFactors)
      sp.setCalcStatusOf(CalcStatus.GroupShape.Transform)
      for (let i = 0; i < sp.spTree.length; ++i) {
        setTransformCalcStatus(sp.spTree[i])
      }
      break
    }
  }
}

export function setTextCalcStatus(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      sp.setCalcStatusOf(CalcStatus.Shape.Content)
      sp.setCalcStatusOf(CalcStatus.Shape.PlaceholderContent)
      sp.setCalcStatusOf(CalcStatus.Shape.TextTransform)
      break
    }
    case InstanceType.GroupShape: {
      if (sp.spTree) {
        for (let i = 0; i < sp.spTree.length; ++i) {
          const ssp = sp.spTree[i]
          setTextCalcStatus(ssp)
        }
      }
      break
    }
    case InstanceType.GraphicFrame: {
      if (sp.graphicObject) {
        sp.graphicObject.invalidateAllPrs()
        sp.graphicObject.calculateInfo.reset()
      }
      sp.setCalcStatusOf(CalcStatus.GraphicFrame.Table)
      sp.setCalcStatusOf(CalcStatus.GraphicFrame.Sizes)
      break
    }
  }
}

export function isFillCalcStatusSet(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      return sp.isCalcStatusSet(CalcStatus.Shape.Fill)
    case InstanceType.ImageShape:
      return sp.isCalcStatusSet(CalcStatus.ImageShape.Fill)
    default:
      return false
  }
}

export function unsetFillCalcStatus(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      sp.unsetCalcStatusOf(CalcStatus.Shape.Fill)
      break
    case InstanceType.ImageShape: {
      sp.unsetCalcStatusOf(CalcStatus.ImageShape.Fill)
      break
    }
  }
}

export function isTransparentCalcStatusSet(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      return sp.isCalcStatusSet(CalcStatus.Shape.Transparent)
    case InstanceType.ImageShape:
      return sp.isCalcStatusSet(CalcStatus.ImageShape.Transparent)
    default:
      return false
  }
}

export function unsetTransparentCalcStatus(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      sp.unsetCalcStatusOf(CalcStatus.Shape.Transparent)
      break
    case InstanceType.ImageShape: {
      sp.unsetCalcStatusOf(CalcStatus.ImageShape.Transparent)
      break
    }
  }
}

export function isTransformCalcStatusSet(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      return sp.isCalcStatusSet(CalcStatus.Shape.Transform)
    case InstanceType.GraphicFrame: {
      return sp.isCalcStatusSet(CalcStatus.GraphicFrame.Transform)
    }
    case InstanceType.ImageShape:
      return sp.isCalcStatusSet(CalcStatus.ImageShape.Transform)
    case InstanceType.GroupShape:
      return sp.isCalcStatusSet(CalcStatus.GroupShape.Transform)
    default:
      return false
  }
}

export function isLineCalcStatusSet(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      return sp.isCalcStatusSet(CalcStatus.Shape.Line)
    }
    case InstanceType.ImageShape: {
      return sp.isCalcStatusSet(CalcStatus.ImageShape.Line)
    }
    default: {
      return false
    }
  }
}

export function unsetLineCalcStatus(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      sp.unsetCalcStatusOf(CalcStatus.Shape.Line)
      break
    }
    case InstanceType.ImageShape: {
      sp.unsetCalcStatusOf(CalcStatus.ImageShape.Line)
      break
    }
  }
}

export function setSpTreeCalcStatusForContainer(
  container: SlideElementContainer
) {
  switch (container.instanceType) {
    case InstanceType.Slide: {
      container.setCalcStatusOf(CalcStatus.Slide.SpTree)
      break
    }
    case InstanceType.SlideLayout: {
      container.setCalcStatusOf(CalcStatus.SlideLayout.SpTree)
      break
    }
    case InstanceType.SlideMaster: {
      container.setCalcStatusOf(CalcStatus.SlideMaster.SpTree)
      break
    }
  }
}

export function setBackgroudCalcStatusForContainer(
  container: SlideElementContainer
) {
  switch (container.instanceType) {
    case InstanceType.Slide: {
      container.setCalcStatusOf(CalcStatus.Slide.Background)
      break
    }
    case InstanceType.SlideLayout: {
      container.setCalcStatusOf(CalcStatus.SlideLayout.Background)
      break
    }
    case InstanceType.SlideMaster: {
      container.setCalcStatusOf(CalcStatus.SlideMaster.Background)
      break
    }
  }
}

export function updateCalcStatusForSlideElementParent(sp: SlideElement) {
  const parent = sp.group ?? sp.parent
  if (parent == null) {
    return
  }
  switch (parent.instanceType) {
    case InstanceType.GroupShape: {
      updateChildrenCalcStatusForGroup(parent)
      updateCalcStatusForSlideElementParent(parent)
      break
    }
    case InstanceType.Slide: {
      parent.setCalcStatusOf(CalcStatus.Slide.SpTree)
      break
    }
    case InstanceType.SlideLayout: {
      parent.setCalcStatusOf(CalcStatus.SlideLayout.SpTree)
      break
    }
    case InstanceType.SlideMaster: {
      parent.setCalcStatusOf(CalcStatus.SlideMaster.SpTree)
      break
    }
    case InstanceType.Notes: {
      const slide = parent.slide
      if (slide) {
        slide.setCalcStatusOf(CalcStatus.Slide.Notes)
      }
      break
    }
  }
}

export function updateChildrenCalcStatusForGroup(group: GroupShape) {
  group.setCalcStatusOf(CalcStatus.GroupShape.ChildrenObjects)
  group.setCalcStatusOf(CalcStatus.GroupShape.Bounds)
  if (group.group) {
    updateChildrenCalcStatusForGroup(group.group)
  }
}
