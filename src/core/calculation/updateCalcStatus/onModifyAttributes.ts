import { isNumber } from '../../../common/utils'
import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { GroupShape } from '../../SlideElement/GroupShape'
import { isSlideElement, isPlaceholder } from '../../utilities/shape/asserts'
import { getTextDocumentOfSp } from '../../utilities/shape/getters'
import { CalcStatus } from './calcStatus'
import { setTransformCalcStatus } from './utils'

export function onModifyFill(sp: SlideElement) {
  if (!isSlideElement(sp)) {
    return
  }

  switch (sp.instanceType) {
    case InstanceType.Shape: {
      sp.setCalcStatusOf(CalcStatus.Shape.Brush)
      sp.setCalcStatusOf(CalcStatus.Shape.Fill)
      sp.setCalcStatusOf(CalcStatus.Shape.Transparent)
      break
    }
    case InstanceType.ImageShape: {
      sp.setCalcStatusOf(CalcStatus.ImageShape.Brush)
      break
    }
    case InstanceType.GroupShape: {
      for (let i = 0; i < sp.spTree.length; ++i) {
        onModifyFill(sp.spTree[i])
      }
      break
    }
  }
}

function onModifyPositionForGroup(grp: GroupShape) {
  setTransformCalcStatus(grp)
  for (let i = 0; i < grp.spTree.length; ++i) {
    onModifyPosition(grp.spTree[i])
  }
}
export function onModifyPosition(sp: SlideElement) {
  if (!isSlideElement(sp)) {
    return
  }
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      setTransformCalcStatus(sp)
      sp.setCalcStatusOf(CalcStatus.Shape.Bounds)
      sp.setCalcStatusOf(CalcStatus.Shape.TextTransform)
      break
    }
    case InstanceType.ImageShape: {
      setTransformCalcStatus(sp)
      sp.setCalcStatusOf(CalcStatus.ImageShape.Bounds)
      break
    }
    case InstanceType.GroupShape: {
      onModifyPositionForGroup(sp)
      break
    }
    case InstanceType.GraphicFrame: {
      sp.setCalcStatusOf(CalcStatus.GraphicFrame.Transform)
      break
    }
  }
}

export function onModifyExtents(sp: SlideElement) {
  if (!isSlideElement(sp)) {
    return
  }

  switch (sp.instanceType) {
    case InstanceType.Shape: {
      sp.setCalcStatusOf(CalcStatus.Shape.Geometry)
      sp.setCalcStatusOf(CalcStatus.Shape.Bounds)
      setTransformCalcStatus(sp)

      sp.setCalcStatusOf(CalcStatus.Shape.Content)
      sp.setCalcStatusOf(CalcStatus.Shape.PlaceholderContent)
      sp.setCalcStatusOf(CalcStatus.Shape.Transform)
      break
    }
    case InstanceType.ImageShape: {
      if (sp.isOleObject()) {
        const ole = sp
        if (!isNumber(ole.defaultSizeX) || !isNumber(ole.defaultSizeY)) {
          if (
            ole.spPr &&
            ole.spPr.xfrm &&
            isNumber(ole.spPr.xfrm.extX) &&
            isNumber(ole.spPr.xfrm.extY)
          ) {
            ole.defaultSizeX = ole.spPr.xfrm.extX
            ole.defaultSizeY = ole.spPr.xfrm.extY
          }
        }
      }
      sp.setCalcStatusOf(CalcStatus.ImageShape.Geometry)
      sp.setCalcStatusOf(CalcStatus.ImageShape.Bounds)
      setTransformCalcStatus(sp)
      break
    }
    case InstanceType.GroupShape: {
      setTransformCalcStatus(sp)
      break
    }
    case InstanceType.GraphicFrame: {
      sp.setCalcStatusOf(CalcStatus.GraphicFrame.Transform)
      break
    }
  }
}

export function onModifyChildPosition(sp: SlideElement) {
  if (!isSlideElement(sp)) {
    return
  }

  switch (sp.instanceType) {
    case InstanceType.Shape:
    case InstanceType.ImageShape:
    case InstanceType.GraphicFrame: {
      break
    }
    case InstanceType.GroupShape: {
      onModifyPositionForGroup(sp)
      break
    }
  }
}

export function onModifyFlip(sp: SlideElement) {
  if (!isSlideElement(sp)) {
    return
  }
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      setTransformCalcStatus(sp)
      sp.setCalcStatusOf(CalcStatus.Shape.TextTransform)
      sp.setCalcStatusOf(CalcStatus.Shape.Content)
      sp.setCalcStatusOf(CalcStatus.Shape.PlaceholderContent)
      break
    }
    case InstanceType.ImageShape: {
      setTransformCalcStatus(sp)
      break
    }
    case InstanceType.GraphicFrame: {
      break
    }
    case InstanceType.GroupShape: {
      onModifyPositionForGroup(sp)
      break
    }
  }
}

export function onModifyRot(sp: SlideElement) {
  if (!isSlideElement(sp)) {
    return
  }
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      setTransformCalcStatus(sp)
      if (sp.txBody && sp.txBody.bodyPr && sp.txBody.bodyPr.upright) {
        sp.setCalcStatusOf(CalcStatus.Shape.Content)
        sp.setCalcStatusOf(CalcStatus.Shape.PlaceholderContent)
      }

      sp.setCalcStatusOf(CalcStatus.Shape.TextTransform)
      sp.setCalcStatusOf(CalcStatus.Shape.Bounds)

      break
    }
    case InstanceType.ImageShape: {
      setTransformCalcStatus(sp)
      sp.setCalcStatusOf(CalcStatus.ImageShape.Bounds)
      break
    }
    case InstanceType.GraphicFrame: {
      break
    }
    case InstanceType.GroupShape: {
      onModifyPositionForGroup(sp)
      break
    }
  }
}

export function onModifyGeometry(sp: SlideElement) {
  if (!isSlideElement(sp)) {
    return
  }
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      sp.setCalcStatusOf(CalcStatus.Shape.Geometry)
      sp.setCalcStatusOf(CalcStatus.Shape.Bounds)
      sp.setCalcStatusOf(CalcStatus.Shape.Content)
      sp.setCalcStatusOf(CalcStatus.Shape.PlaceholderContent)
      sp.setCalcStatusOf(CalcStatus.Shape.TextTransform)
      break
    }
    case InstanceType.ImageShape: {
      sp.setCalcStatusOf(CalcStatus.ImageShape.Bounds)
      sp.setCalcStatusOf(CalcStatus.ImageShape.Geometry)
      break
    }
    case InstanceType.GraphicFrame:
    case InstanceType.GroupShape: {
      break
    }
  }
}

export function onModifyLn(sp: SlideElement) {
  if (!isSlideElement(sp)) {
    return
  }
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      sp.setCalcStatusOf(CalcStatus.Shape.Pen)
      sp.setCalcStatusOf(CalcStatus.Shape.Line)
      break
    }
    case InstanceType.ImageShape: {
      sp.setCalcStatusOf(CalcStatus.ImageShape.Line)
      sp.setCalcStatusOf(CalcStatus.ImageShape.Pen)
      break
    }
    case InstanceType.GraphicFrame: {
      break
    }
    case InstanceType.GroupShape: {
      for (let i = 0; i < sp.spTree.length; ++i) {
        onModifyLn(sp.spTree[i])
      }
      break
    }
  }
}

export function onModifyTheme(sp: SlideElement) {
  if (sp == null) {
    return
  }
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      sp.resetCalcStatus()

      if (
        isPlaceholder(sp) &&
        !(sp.spPr && sp.spPr.xfrm && sp.spPr.xfrm.isValid())
      ) {
        setTransformCalcStatus(sp)
        sp.setCalcStatusOf(CalcStatus.Shape.Geometry)
      }
      const textDoc = getTextDocumentOfSp(sp)
      if (textDoc) {
        textDoc.invalidateAllParagraphsCalcedPrs()
      }
      sp.setCalcStatusOf(CalcStatus.Shape.Content)
      sp.setCalcStatusOf(CalcStatus.Shape.Fill)
      sp.setCalcStatusOf(CalcStatus.Shape.Line)
      sp.setCalcStatusOf(CalcStatus.Shape.Pen)
      sp.setCalcStatusOf(CalcStatus.Shape.Brush)
      sp.setCalcStatusOf(CalcStatus.Shape.Style)
      sp.setCalcStatusOf(CalcStatus.Shape.Bounds)
      break
    }
    case InstanceType.ImageShape: {
      sp.resetCalcStatus()

      if (
        isPlaceholder(sp) &&
        !(sp.spPr && sp.spPr.xfrm && sp.spPr.xfrm.isValid())
      ) {
        setTransformCalcStatus(sp)
        sp.setCalcStatusOf(CalcStatus.ImageShape.Geometry)
      }
      sp.setCalcStatusOf(CalcStatus.ImageShape.Fill)
      sp.setCalcStatusOf(CalcStatus.ImageShape.Line)
      sp.setCalcStatusOf(CalcStatus.ImageShape.Pen)
      sp.setCalcStatusOf(CalcStatus.ImageShape.Brush)
      sp.setCalcStatusOf(CalcStatus.ImageShape.Style)
      sp.setCalcStatusOf(CalcStatus.ImageShape.Bounds)
      break
    }
    case InstanceType.GroupShape: {
      sp.resetCalcStatus()

      if (
        isPlaceholder(sp) &&
        !(sp.spPr && sp.spPr.xfrm && sp.spPr.xfrm.isNotEmptyForGroup())
      ) {
        setTransformCalcStatus(sp)
      }

      sp.setCalcStatusOf(CalcStatus.GroupShape.Bounds)

      if (Array.isArray(sp.spTree)) {
        for (let i = 0; i < sp.spTree.length; ++i) {
          onModifyTheme(sp.spTree[i])
        }
      }
      break
    }
    case InstanceType.GraphicFrame: {
      if (sp.graphicObject) {
        sp.graphicObject.invalidateAllPrs()
        sp.setCalcStatusOf(CalcStatus.GraphicFrame.Sizes)
        sp.setCalcStatusOf(CalcStatus.GraphicFrame.Table)
      }
      break
    }
  }
}
