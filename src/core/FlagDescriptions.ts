import { EditActionFlag } from './EditActionFlag'

const IDF = {
  edit_Unknown_Unknown: EditActionFlag.edit_Unknown_Unknown,

  edit_Paragraph_AddItem: EditActionFlag.edit_Paragraph_AddItem,
  edit_Paragraph_RemoveItem: EditActionFlag.edit_Paragraph_RemoveItem,
  edit_Paragraph_Align: EditActionFlag.edit_Paragraph_Align,
  edit_Paragraph_Ind_First: EditActionFlag.edit_Paragraph_Ind_First,
  edit_Paragraph_Ind_Right: EditActionFlag.edit_Paragraph_Ind_Right,
  edit_Paragraph_Ind_Left: EditActionFlag.edit_Paragraph_Ind_Left,
  edit_Paragraph_Spacing_Line: EditActionFlag.edit_Paragraph_Spacing_Line,
  edit_Paragraph_Spacing_LineRule:
    EditActionFlag.edit_Paragraph_Spacing_LineRule,
  edit_Paragraph_Spacing_Before: EditActionFlag.edit_Paragraph_Spacing_Before,
  edit_Paragraph_Spacing_After: EditActionFlag.edit_Paragraph_Spacing_After,
  edit_Paragraph_Spacing_AfterAutoSpacing:
    EditActionFlag.edit_Paragraph_Spacing_AfterAutoSpacing,
  edit_Paragraph_Spacing_BeforeAutoSpacing:
    EditActionFlag.edit_Paragraph_Spacing_BeforeAutoSpacing,
  edit_Paragraph_Shd_Value: EditActionFlag.edit_Paragraph_Shd_Value,
  edit_Paragraph_Shd_Color: EditActionFlag.edit_Paragraph_Shd_Color,
  edit_Paragraph_Shd_FillEffects: EditActionFlag.edit_Paragraph_Shd_FillEffects,
  edit_Paragraph_Shd: EditActionFlag.edit_Paragraph_Shd,
  edit_Paragraph_Tabs: EditActionFlag.edit_Paragraph_Tabs,
  edit_Paragraph_Pr: EditActionFlag.edit_Paragraph_Pr,
  edit_Paragraph_PresentationPr_Bullet:
    EditActionFlag.edit_Paragraph_PresentationPr_Bullet,
  edit_Paragraph_PresentationPr_Level:
    EditActionFlag.edit_Paragraph_PresentationPr_Level,
  edit_Paragraph_PrReviewInfo: EditActionFlag.edit_Paragraph_PrReviewInfo,
  edit_Paragraph_OutlineLvl: EditActionFlag.edit_Paragraph_OutlineLvl,
  edit_Paragraph_DefaultTabSize: EditActionFlag.edit_Paragraph_DefaultTabSize,

  edit_Table_TableCellMar: EditActionFlag.edit_Table_TableCellMar,
  edit_Table_TableAlign: EditActionFlag.edit_Table_TableAlign,
  edit_Table_TableBorder_Left: EditActionFlag.edit_Table_TableBorder_Left,
  edit_Table_TableBorder_Top: EditActionFlag.edit_Table_TableBorder_Top,
  edit_Table_TableBorder_Right: EditActionFlag.edit_Table_TableBorder_Right,
  edit_Table_TableBorder_Bottom: EditActionFlag.edit_Table_TableBorder_Bottom,
  edit_Table_TableBorder_InsideH: EditActionFlag.edit_Table_TableBorder_InsideH,
  edit_Table_TableBorder_InsideV: EditActionFlag.edit_Table_TableBorder_InsideV,
  edit_Table_TableShd: EditActionFlag.edit_Table_TableShd,
  edit_Table_AddRow: EditActionFlag.edit_Table_AddRow,
  edit_Table_RemoveRow: EditActionFlag.edit_Table_RemoveRow,
  edit_Table_TableGrid: EditActionFlag.edit_Table_TableGrid,
  edit_Table_TableLook: EditActionFlag.edit_Table_TableLook,
  edit_Table_TableStyleRowBandSize:
    EditActionFlag.edit_Table_TableStyleRowBandSize,
  edit_Table_TableStyleColBandSize:
    EditActionFlag.edit_Table_TableStyleColBandSize,
  edit_Table_TableStyle: EditActionFlag.edit_Table_TableStyle,
  edit_Table_Distance: EditActionFlag.edit_Table_Distance,
  edit_Table_Pr: EditActionFlag.edit_Table_Pr,
  edit_TableRow_Height: EditActionFlag.edit_TableRow_Height,
  edit_TableRow_AddCell: EditActionFlag.edit_TableRow_AddCell,
  edit_TableRow_RemoveCell: EditActionFlag.edit_TableRow_RemoveCell,
  edit_TableRow_Pr: EditActionFlag.edit_TableRow_Pr,
  edit_TableCell_GridSpan: EditActionFlag.edit_TableCell_GridSpan,
  edit_TableCell_Margins: EditActionFlag.edit_TableCell_Margins,
  edit_TableCell_Shd: EditActionFlag.edit_TableCell_Shd,
  edit_TableCell_VMerge: EditActionFlag.edit_TableCell_VMerge,
  edit_TableCell_Border_Left: EditActionFlag.edit_TableCell_Border_Left,
  edit_TableCell_Border_Right: EditActionFlag.edit_TableCell_Border_Right,
  edit_TableCell_Border_Top: EditActionFlag.edit_TableCell_Border_Top,
  edit_TableCell_Border_Bottom: EditActionFlag.edit_TableCell_Border_Bottom,
  edit_TableCell_VAlign: EditActionFlag.edit_TableCell_VAlign,
  edit_TableCell_Pr: EditActionFlag.edit_TableCell_Pr,
  edit_TableCell_TextDirection: EditActionFlag.edit_TableCell_TextDirection,

  edit_TextContent_Change: EditActionFlag.edit_TextContent_Change,
  edit_Comment_Change: EditActionFlag.edit_Comment_Change,
  edit_Comment_TypeInfo: EditActionFlag.edit_Comment_TypeInfo,
  edit_Comment_Position: EditActionFlag.edit_Comment_Position,
  edit_Comments_Add: EditActionFlag.edit_Comments_Add,
  edit_Comments_Remove: EditActionFlag.edit_Comments_Remove,

  edit_Shapes_SetLocks: EditActionFlag.edit_Shapes_SetLocks,

  edit_Presentation_AddSlide: EditActionFlag.edit_Presentation_AddSlide,
  edit_Presentation_RemoveSlide: EditActionFlag.edit_Presentation_RemoveSlide,
  edit_Presentation_SlideSize: EditActionFlag.edit_Presentation_SlideSize,
  edit_Presentation_AddSlideMaster:
    EditActionFlag.edit_Presentation_AddSlideMaster,
  edit_Presentation_RemoveSlideMaster:
    EditActionFlag.edit_Presentation_RemoveSlideMaster,
  edit_Presentation_UpdateSlideMaster:
    EditActionFlag.edit_Presentation_UpdateSlideMaster,
  edit_Presentation_ChangeTheme: EditActionFlag.edit_Presentation_ChangeTheme,
  edit_Presentation_ChangeColorScheme:
    EditActionFlag.edit_Presentation_ChangeColorScheme,
  edit_Presentation_SetShowPr: EditActionFlag.edit_Presentation_SetShowPr,
  edit_Presentation_SetDefaultTextStyle:
    EditActionFlag.edit_Presentation_SetDefaultTextStyle,
  edit_Presentation_AddSection: EditActionFlag.edit_Presentation_AddSection,
  edit_Presentation_RemoveSection:
    EditActionFlag.edit_Presentation_RemoveSection,
  edit_Presentation_SetFirstSlideIndex:
    EditActionFlag.edit_Presentation_SetFirstSlideIndex,

  edit_Xfrm_SetOffX: EditActionFlag.edit_Xfrm_SetOffX,
  edit_Xfrm_SetOffY: EditActionFlag.edit_Xfrm_SetOffY,
  edit_Xfrm_SetExtX: EditActionFlag.edit_Xfrm_SetExtX,
  edit_Xfrm_SetExtY: EditActionFlag.edit_Xfrm_SetExtY,
  edit_Xfrm_SetChOffX: EditActionFlag.edit_Xfrm_SetChOffX,
  edit_Xfrm_SetChOffY: EditActionFlag.edit_Xfrm_SetChOffY,
  edit_Xfrm_SetChExtX: EditActionFlag.edit_Xfrm_SetChExtX,
  edit_Xfrm_SetChExtY: EditActionFlag.edit_Xfrm_SetChExtY,
  edit_Xfrm_SetFlipH: EditActionFlag.edit_Xfrm_SetFlipH,
  edit_Xfrm_SetFlipV: EditActionFlag.edit_Xfrm_SetFlipV,
  edit_Xfrm_SetRot: EditActionFlag.edit_Xfrm_SetRot,
  edit_Xfrm_SetParent: EditActionFlag.edit_Xfrm_SetParent,
  edit_SpPr_SetBwMode: EditActionFlag.edit_SpPr_SetBwMode,
  edit_SpPr_SetXfrm: EditActionFlag.edit_SpPr_SetXfrm,
  edit_SpPr_SetGeometry: EditActionFlag.edit_SpPr_SetGeometry,
  edit_SpPr_SetFill: EditActionFlag.edit_SpPr_SetFill,
  edit_SpPr_SetLn: EditActionFlag.edit_SpPr_SetLn,
  edit_SpPr_SetParent: EditActionFlag.edit_SpPr_SetParent,

  edit_ShapeSetDeleted: EditActionFlag.edit_ShapeSetDeleted,
  edit_ShapeSetNvSpPr: EditActionFlag.edit_ShapeSetNvSpPr,
  edit_CxnSp_SetUniSpPr: EditActionFlag.edit_CxnSp_SetUniSpPr,
  edit_ShapeSetSpPr: EditActionFlag.edit_ShapeSetSpPr,
  edit_ShapeSetStyle: EditActionFlag.edit_ShapeSetStyle,
  edit_ShapeSetTxBody: EditActionFlag.edit_ShapeSetTxBody,
  edit_ShapeSetParent: EditActionFlag.edit_ShapeSetParent,
  edit_ShapeSetGroup: EditActionFlag.edit_ShapeSetGroup,
  edit_ShapeSetBodyPr: EditActionFlag.edit_ShapeSetBodyPr,
  edit_GroupShapeSetNvGrpSpPr: EditActionFlag.edit_GroupShapeSetNvGrpSpPr,
  edit_GroupShapeSetSpPr: EditActionFlag.edit_GroupShapeSetSpPr,
  edit_GroupShapeAddToSpTree: EditActionFlag.edit_GroupShapeAddToSpTree,
  edit_GroupShapeSetParent: EditActionFlag.edit_GroupShapeSetParent,
  edit_GroupShapeSetGroup: EditActionFlag.edit_GroupShapeSetGroup,
  edit_GroupShapeRemoveFromSpTree:
    EditActionFlag.edit_GroupShapeRemoveFromSpTree,
  edit_ImageShapeSetNvPicPr: EditActionFlag.edit_ImageShapeSetNvPicPr,
  edit_ImageShapeSetSpPr: EditActionFlag.edit_ImageShapeSetSpPr,
  edit_ImageShapeSetBlipFill: EditActionFlag.edit_ImageShapeSetBlipFill,
  edit_ImageShapeSetParent: EditActionFlag.edit_ImageShapeSetParent,
  edit_ImageShapeSetGroup: EditActionFlag.edit_ImageShapeSetGroup,
  edit_ImageShapeSetStyle: EditActionFlag.edit_ImageShapeSetStyle,
  edit_ImageShapeSetData: EditActionFlag.edit_ImageShapeSetData,
  edit_ImageShapeSetApplicationId:
    EditActionFlag.edit_ImageShapeSetApplicationId,
  edit_ImageShapeSetPixSizes: EditActionFlag.edit_ImageShapeSetPixSizes,
  edit_ImageShapeSetObjectFile: EditActionFlag.edit_ImageShapeSetObjectFile,
  edit_GeometrySetParent: EditActionFlag.edit_GeometrySetParent,
  edit_GeometryAddAdj: EditActionFlag.edit_GeometryAddAdj,
  edit_GeometrySetAdj: EditActionFlag.edit_GeometrySetAdj,
  edit_GeometryAddGuide: EditActionFlag.edit_GeometryAddGuide,
  edit_GeometryAddCnx: EditActionFlag.edit_GeometryAddCnx,
  edit_GeometryAddHandleXY: EditActionFlag.edit_GeometryAddHandleXY,
  edit_GeometryAddHandlePolar: EditActionFlag.edit_GeometryAddHandlePolar,
  edit_GeometryAddPath: EditActionFlag.edit_GeometryAddPath,
  edit_GeometryAddRect: EditActionFlag.edit_GeometryAddRect,
  edit_GeometrySetPreset: EditActionFlag.edit_GeometrySetPreset,

  edit_TextBodySetBodyPr: EditActionFlag.edit_TextBodySetBodyPr,
  edit_TextBodySetLstStyle: EditActionFlag.edit_TextBodySetLstStyle,
  edit_TextBodySetContent: EditActionFlag.edit_TextBodySetContent,
  edit_TextBodySetParent: EditActionFlag.edit_TextBodySetParent,
  edit_Slide_SetComments: EditActionFlag.edit_Slide_SetComments,
  edit_Slide_SetShow: EditActionFlag.edit_Slide_SetShow,
  edit_SlideSetShowPhAnim: EditActionFlag.edit_SlideSetShowPhAnim,
  edit_SlideSetShowMasterSp: EditActionFlag.edit_SlideSetShowMasterSp,
  edit_Slide_SetLayout: EditActionFlag.edit_Slide_SetLayout,
  edit_SlideSetIndex: EditActionFlag.edit_SlideSetIndex,
  edit_Slide_SetTiming: EditActionFlag.edit_Slide_SetTiming,
  edit_SlideSetSize: EditActionFlag.edit_SlideSetSize,
  edit_Slide_SetBg: EditActionFlag.edit_Slide_SetBg,
  edit_SlideSetLocks: EditActionFlag.edit_SlideSetLocks,
  edit_Slide_RemoveFromSpTree: EditActionFlag.edit_Slide_RemoveFromSpTree,
  edit_Slide_AddToSpTree: EditActionFlag.edit_Slide_AddToSpTree,
  edit_SlideSetCSldName: EditActionFlag.edit_SlideSetCSldName,
  edit_SlideSetClrMapOverride: EditActionFlag.edit_SlideSetClrMapOverride,
  edit_SlideSetNotes: EditActionFlag.edit_SlideSetNotes,
  edit_SlideReplaceFromSpTree: EditActionFlag.edit_SlideReplaceFromSpTree,
  edit_Slide_SetAnimation: EditActionFlag.edit_Slide_SetAnimation,
  edit_SlideLayoutSetMaster: EditActionFlag.edit_SlideLayoutSetMaster,
  edit_SlideLayoutSetMatchingName:
    EditActionFlag.edit_SlideLayoutSetMatchingName,
  edit_SlideLayoutSetType: EditActionFlag.edit_SlideLayoutSetType,
  edit_SlideLayoutSetBg: EditActionFlag.edit_SlideLayoutSetBg,
  edit_SlideLayoutSetCSldName: EditActionFlag.edit_SlideLayoutSetCSldName,
  edit_SlideLayoutSetShowPhAnim: EditActionFlag.edit_SlideLayoutSetShowPhAnim,
  edit_SlideLayoutSetShowMasterSp:
    EditActionFlag.edit_SlideLayoutSetShowMasterSp,
  edit_SlideLayoutSetClrMapOverride:
    EditActionFlag.edit_SlideLayoutSetClrMapOverride,
  edit_SlideLayoutAddToSpTree: EditActionFlag.edit_SlideLayoutAddToSpTree,
  edit_SlideLayoutSetSize: EditActionFlag.edit_SlideLayoutSetSize,
  edit_SlideLayout_SetTransition: EditActionFlag.edit_SlideLayout_SetTransition,
  edit_SlideMasterAddToSpTree: EditActionFlag.edit_SlideMasterAddToSpTree,
  edit_SlideMasterSetTheme: EditActionFlag.edit_SlideMasterSetTheme,
  edit_SlideMasterSetBg: EditActionFlag.edit_SlideMasterSetBg,
  edit_SlideMasterSetTxStyles: EditActionFlag.edit_SlideMasterSetTxStyles,
  edit_SlideMasterSetCSldName: EditActionFlag.edit_SlideMasterSetCSldName,
  edit_SlideMasterSetClrMapOverride:
    EditActionFlag.edit_SlideMasterSetClrMapOverride,
  edit_SlideMasterAddLayout: EditActionFlag.edit_SlideMasterAddLayout,
  edit_SlideMasterSetSize: EditActionFlag.edit_SlideMasterSetSize,
  edit_SlideMaster_SetTransition: EditActionFlag.edit_SlideMaster_SetTransition,
  edit_SlideComments_AddComment: EditActionFlag.edit_SlideComments_AddComment,
  edit_SlideComments_RemoveComment:
    EditActionFlag.edit_SlideComments_RemoveComment,

  edit_GraphicFrameSetSpPr: EditActionFlag.edit_GraphicFrameSetSpPr,
  edit_GraphicFrameSetGraphicObject:
    EditActionFlag.edit_GraphicFrameSetGraphicObject,
  edit_GraphicFrameSetSetNvSpPr: EditActionFlag.edit_GraphicFrameSetSetNvSpPr,
  edit_GraphicFrameSetSetParent: EditActionFlag.edit_GraphicFrameSetSetParent,
  edit_GraphicFrameSetSetGroup: EditActionFlag.edit_GraphicFrameSetSetGroup,
  edit_NotesMasterSetHF: EditActionFlag.edit_NotesMasterSetHF,
  edit_NotesMasterSetNotesStyle: EditActionFlag.edit_NotesMasterSetNotesStyle,
  edit_NotesMasterSetNotesTheme: EditActionFlag.edit_NotesMasterSetNotesTheme,
  edit_NotesMasterAddToSpTree: EditActionFlag.edit_NotesMasterAddToSpTree,
  edit_NotesMasterRemoveFromTree: EditActionFlag.edit_NotesMasterRemoveFromTree,
  edit_NotesMasterSetBg: EditActionFlag.edit_NotesMasterSetBg,
  edit_NotesMasterAddToNotesLst: EditActionFlag.edit_NotesMasterAddToNotesLst,
  edit_NotesMasterSetName: EditActionFlag.edit_NotesMasterSetName,
  edit_NotesSetClrMap: EditActionFlag.edit_NotesSetClrMap,
  edit_NotesSetShowMasterPhAnim: EditActionFlag.edit_NotesSetShowMasterPhAnim,
  edit_NotesSetShowMasterSp: EditActionFlag.edit_NotesSetShowMasterSp,
  edit_NotesAddToSpTree: EditActionFlag.edit_NotesAddToSpTree,
  edit_NotesRemoveFromTree: EditActionFlag.edit_NotesRemoveFromTree,
  edit_NotesSetBg: EditActionFlag.edit_NotesSetBg,
  edit_NotesSetName: EditActionFlag.edit_NotesSetName,
  edit_NotesSetSlide: EditActionFlag.edit_NotesSetSlide,
  edit_NotesSetNotesMaster: EditActionFlag.edit_NotesSetNotesMaster,

  action_EditOnPresentation: EditActionFlag.action_EditOnPresentation,

  action_Slide_AddNewShape: EditActionFlag.action_Slide_AddNewShape,

  action_Document_ReplaceAll: EditActionFlag.action_Document_ReplaceAll,
  action_Document_ReplaceSingle: EditActionFlag.action_Document_ReplaceSingle,
  action_Document_TableAddNewRow: EditActionFlag.action_Document_TableAddNewRow,

  action_Document_SetTextLang: EditActionFlag.action_Document_SetTextLang,

  action_Document_SetParagraphAlignByHotKey:
    EditActionFlag.action_Document_SetParagraphAlignByHotKey,

  action_Document_PasteByHotKey: EditActionFlag.action_Document_PasteByHotKey,

  action_Document_MoveTableBorder:
    EditActionFlag.action_Document_MoveTableBorder,

  action_Slide_Remove: EditActionFlag.action_Slide_Remove,

  action_SlideElements_EndAdjust: EditActionFlag.action_SlideElements_EndAdjust,
  action_SlideElements_EndControl:
    EditActionFlag.action_SlideElements_EndControl,
  action_SlideElements_CopyCtrl: EditActionFlag.action_SlideElements_CopyCtrl,
  action_Presentation_ParaApply: EditActionFlag.action_Presentation_ParaApply,
  action_Presentation_ParaFormatPaste:
    EditActionFlag.action_Presentation_ParaFormatPaste,
  action_Presentation_AddNewParagraph:
    EditActionFlag.action_Presentation_AddNewParagraph,
  action_Presentation_CreateGroup:
    EditActionFlag.action_Presentation_CreateGroup,
  action_Presentation_UnGroup: EditActionFlag.action_Presentation_UnGroup,
  action_Presentation_ParagraphAdd:
    EditActionFlag.action_Presentation_ParagraphAdd,
  action_Presentation_ParagraphClearFormatting:
    EditActionFlag.action_Presentation_ParagraphClearFormatting,

  action_Presentation_SetParagraphSpacing:
    EditActionFlag.action_Presentation_SetParagraphSpacing,
  action_Presentation_SetParagraphTabs:
    EditActionFlag.action_Presentation_SetParagraphTabs,
  action_Presentation_SetParagraphIndent:
    EditActionFlag.action_Presentation_SetParagraphIndent,
  action_Presentation_SetParagraphNumbering:
    EditActionFlag.action_Presentation_SetParagraphNumbering,
  action_Presentation_ParagraphIncDecFontSize:
    EditActionFlag.action_Presentation_ParagraphIncDecFontSize,
  action_Presentation_ParagraphIncDecIndent:
    EditActionFlag.action_Presentation_ParagraphIncDecIndent,
  action_Presentation_SetImageProps:
    EditActionFlag.action_Presentation_SetImageProps,
  action_Presentation_SetShapeProps:
    EditActionFlag.action_Presentation_SetShapeProps,
  action_Presentation_ChangeShapeType:
    EditActionFlag.action_Presentation_ChangeShapeType,
  action_Presentation_SetVerticalAlign:
    EditActionFlag.action_Presentation_SetVerticalAlign,
  action_Presentation_HyperlinkAdd:
    EditActionFlag.action_Presentation_HyperlinkAdd,
  action_Presentation_HyperlinkModify:
    EditActionFlag.action_Presentation_HyperlinkModify,
  action_Presentation_HyperlinkRemove:
    EditActionFlag.action_Presentation_HyperlinkRemove,
  action_Presentation_DisttributeHorizontal:
    EditActionFlag.action_Presentation_DisttributeHorizontal,
  action_Presentation_DisttributeVertical:
    EditActionFlag.action_Presentation_DisttributeVertical,
  action_Presentation_BringToFront:
    EditActionFlag.action_Presentation_BringToFront,
  action_Presentation_BringForward:
    EditActionFlag.action_Presentation_BringForward,
  action_Presentation_SendToBack: EditActionFlag.action_Presentation_SendToBack,
  action_Presentation_BringBackward:
    EditActionFlag.action_Presentation_BringBackward,
  action_Presentation_ApplyTiming:
    EditActionFlag.action_Presentation_ApplyTiming,
  action_Presentation_MoveSlidesToEnd:
    EditActionFlag.action_Presentation_MoveSlidesToEnd,
  action_Presentation_MoveSlidesToNext:
    EditActionFlag.action_Presentation_MoveSlidesToNext,
  action_Presentation_MoveSlidesToPrev:
    EditActionFlag.action_Presentation_MoveSlidesToPrev,
  action_Presentation_MoveSlidesToStart:
    EditActionFlag.action_Presentation_MoveSlidesToStart,

  action_Presentation_AddImage: EditActionFlag.action_Presentation_AddImage,
  action_Presentation_AddTable: EditActionFlag.action_Presentation_AddTable,
  action_Presentation_ChangeBackground:
    EditActionFlag.action_Presentation_ChangeBackground,
  action_Presentation_AddNextSlide:
    EditActionFlag.action_Presentation_AddNextSlide,
  action_Presentation_ShiftSlides:
    EditActionFlag.action_Presentation_ShiftSlides,
  action_Presentation_DeleteSlides:
    EditActionFlag.action_Presentation_DeleteSlides,
  action_Presentation_ChangeLayout:
    EditActionFlag.action_Presentation_ChangeLayout,
  action_Presentation_ChangeSlideSize:
    EditActionFlag.action_Presentation_ChangeSlideSize,
  action_Presentation_ChangeColorScheme:
    EditActionFlag.action_Presentation_ChangeColorScheme,
  action_Presentation_AddComment: EditActionFlag.action_Presentation_AddComment,
  action_Presentation_ChangeComment:
    EditActionFlag.action_Presentation_ChangeComment,

  action_Presentation_SetTextPrAlign:
    EditActionFlag.action_Presentation_SetTextPrAlign,

  action_Presentation_SetPrIndent:
    EditActionFlag.action_Presentation_SetPrIndent,
  action_Presentation_SetPrIndentRight:
    EditActionFlag.action_Presentation_SetPrIndentRight,
  action_Presentation_SetPrFirstLineIndent:
    EditActionFlag.action_Presentation_SetPrFirstLineIndent,

  action_Presentation_AddRowAbove:
    EditActionFlag.action_Presentation_AddRowAbove,
  action_Presentation_AddRowBelow:
    EditActionFlag.action_Presentation_AddRowBelow,
  action_Presentation_AddColLeft: EditActionFlag.action_Presentation_AddColLeft,
  action_Presentation_AddColRight:
    EditActionFlag.action_Presentation_AddColRight,
  action_Presentation_RemoveRow: EditActionFlag.action_Presentation_RemoveRow,
  action_Presentation_RemoveCol: EditActionFlag.action_Presentation_RemoveCol,
  action_Presentation_RemoveTable:
    EditActionFlag.action_Presentation_RemoveTable,
  action_Presentation_MergeCells: EditActionFlag.action_Presentation_MergeCells,
  action_Presentation_SplitCells: EditActionFlag.action_Presentation_SplitCells,
  action_Presentation_ApplyTable: EditActionFlag.action_Presentation_ApplyTable,
  action_Presentation_RemoveComment:
    EditActionFlag.action_Presentation_RemoveComment,

  action_Presentation_ChangeTheme:
    EditActionFlag.action_Presentation_ChangeTheme,

  action_Presentation_SetVert: EditActionFlag.action_Presentation_SetVert,

  action_Document_MergeLetter: EditActionFlag.action_Document_MergeLetter,
  action_Presentation_ApplyTimingToAll:
    EditActionFlag.action_Presentation_ApplyTimingToAll,

  action_Document_CompositeInput: EditActionFlag.action_Document_CompositeInput,
  action_Document_CompositeInputReplace:
    EditActionFlag.action_Document_CompositeInputReplace,

  action_Presentation_HideSlides: EditActionFlag.action_Presentation_HideSlides,

  action_Document_DistributeTableCells:
    EditActionFlag.action_Document_DistributeTableCells,

  action_Presentation_DuplicateSlides:
    EditActionFlag.action_Presentation_DuplicateSlides,

  action_Presentation_PreviewSlideTransition:
    EditActionFlag.action_Presentation_PreviewSlideTransition,

  action_Presentation_ApplyAnimation:
    EditActionFlag.action_Presentation_ApplyAnimation,
  action_Presentation_PreviewSlideAnimation:
    EditActionFlag.action_Presentation_PreviewSlideAnimation,
  action_Presentation_Crop: EditActionFlag.action_Presentation_Crop,
  action_SlideElements_EndMove: EditActionFlag.action_SlideElements_EndMove,
  action_SlideElements_EndRotate: EditActionFlag.action_SlideElements_EndRotate,
  action_SlideElements_EndResize: EditActionFlag.action_SlideElements_EndResize,
  action_Presentation_ApplyAudio: EditActionFlag.action_Presentation_ApplyAudio,
}

const flagDescMap = (() => {
  return Object.keys(IDF).reduce((res, k) => {
    res[IDF[k]] = k
    return res
  }, {})
})()

export function descriptionFromIdentityFlag(flag: EditActionFlag): string {
  const desc = flagDescMap[flag]
  return desc ?? 'Unknown'
}
