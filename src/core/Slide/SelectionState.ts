import { Nullable } from '../../../liber/pervasive'
import { PointerEvtTypeValue } from '../../common/dom/events'
import { InstanceType } from '../instanceTypes'
import { SlideElement, TextSlideElement } from '../SlideElement/type'
import {
  TableSelectionType,
  TableSelectionTypeValues,
} from '../common/const/table'
import { EditorSettings } from '../common/EditorSettings'
import { ParagraphElementPosition } from '../Paragraph/common'
import { Paragraph } from '../Paragraph/Paragraph'
import { updateSelectionStateForParagraph } from '../Paragraph/utils/selection'
import { GroupShape } from '../SlideElement/GroupShape'
import { ImageShape } from '../SlideElement/Image'
import { Shape } from '../SlideElement/Shape'
import { Table } from '../Table/Table'
import { TextDocument } from '../TextDocument/TextDocument'
import { getPresentation } from '../utilities/finders'
import { isSlideElementInPPT } from '../utilities/shape/asserts'
import { applySlideElementFormat } from '../utilities/shape/formatting'
import {
  getTextDocumentOfSp,
  getTopMostGroupShape,
} from '../utilities/shape/getters'
import { Presentation } from './Presentation'

type SelectionStatusInfo =
  | {
      status: 'root_text'
      selectedTextSp: TextSlideElement
      selectedElements: SlideElement[]
    }
  | {
      status: 'root_collection'
      selectedElements: SlideElement[]
    }
  | {
      status: 'group_text'
      group: GroupShape
      selectedTextSp: TextSlideElement
      selectedElements: SlideElement[]
    }
  | {
      status: 'group_collection'
      group: GroupShape
      selectedElements: SlideElement[]
    }

export class SelectionState {
  readonly instanceType = InstanceType.SelectionState
  selectedSlideElements: SlideElement[] = []

  private selectedTextSlideElement: Nullable<TextSlideElement> = null
  private selectedGroupInfo: {
    selectedTextSlideElement: Nullable<TextSlideElement>
    object: Nullable<GroupShape>
    selectedSlideElements: SlideElement[]
  } = {
    object: null,
    selectedTextSlideElement: null,
    selectedSlideElements: [],
  }
  private presentation: Presentation

  get selectionStatusInfo(): SelectionStatusInfo {
    const group = this.selectedGroupInfo.object
    const groupTextSelection = this.selectedGroupInfo.selectedTextSlideElement
    const groupSelectedElements = this.selectedGroupInfo.selectedSlideElements
    const selectedTextSp = this.selectedTextSlideElement
    const selectedElements = this.selectedSlideElements
    if (group != null) {
      if (groupTextSelection != null) {
        return {
          status: 'group_text',
          group: group,
          selectedTextSp: groupTextSelection,
          selectedElements: groupSelectedElements,
        }
      } else {
        return {
          status: 'group_collection',
          group: group,
          selectedElements: groupSelectedElements,
        }
      }
    } else {
      if (selectedTextSp) {
        return {
          status: 'root_text',
          selectedTextSp,
          selectedElements: selectedElements,
        }
      } else {
        return {
          status: 'root_collection',
          selectedElements: selectedElements,
        }
      }
    }
  }

  /** 多选对象, 其中包括组合对象时, 此时返回 null */
  get selectedGroup() {
    return this.selectedGroupInfo.object
  }

  set selectedGroup(grp: Nullable<GroupShape>) {
    const oldGrp = this.selectedGroupInfo.object
    if (oldGrp !== grp) {
      if (oldGrp != null) {
        this.resetGroupSelection()
        this.deselect(oldGrp)
      }
      if (grp != null) {
        this.select(grp)
      }
      this.selectedGroupInfo.object = grp
    }
  }

  get selectedTextSp() {
    const info = this.selectionStatusInfo

    return info.status === 'group_text' || info.status === 'root_text'
      ? info.selectedTextSp
      : null
  }

  get rootTextSelection() {
    const info = this.selectionStatusInfo

    return info.status === 'root_text' ? info.selectedTextSp : null
  }
  get groupSelection() {
    const info = this.selectionStatusInfo
    return info.status === 'group_text' || info.status === 'group_collection'
      ? info
      : null
  }

  get selectedElements() {
    const info = this.selectionStatusInfo
    return info.selectedElements || []
  }

  get selectedTable(): Nullable<Table> {
    const info = this.selectionStatusInfo
    const selectedSps = info.selectedElements || []
    if (
      selectedSps.length === 1 &&
      selectedSps[0].instanceType === InstanceType.GraphicFrame
    ) {
      return selectedSps[0].graphicObject
    }
    return undefined
  }

  get selectedSpForFormatting(): Nullable<Shape | ImageShape> {
    if (
      this.selectedTextSp != null ||
      EditorSettings.enableSpFormatPainting === false
    ) {
      return undefined
    }

    const info = this.selectionStatusInfo
    const selectedSps = info.selectedElements

    let sp = getSpForFormatting(selectedSps)

    if (sp == null) {
      sp = getSpForFormatting(this.selectedGroupInfo.selectedSlideElements)
    }
    return sp
  }

  get hasSelection() {
    return this.selectedSlideElements.length > 0
  }

  constructor(presentation: Presentation) {
    this.presentation = presentation
  }
  reset(ignoreContentSelect?: boolean) {
    this.resetRootSelection(ignoreContentSelect)
    for (const sp of this.selectedSlideElements) {
      sp.selected = false
    }
    this.selectedSlideElements.length = 0
  }

  resetRootSelection(ignoreContentSelect?: boolean) {
    const selectedTextSp = this.selectedTextSlideElement
    if (selectedTextSp) {
      if (ignoreContentSelect !== true) {
        clearTextSelectionContent(selectedTextSp)
      }
      this.selectedTextSlideElement = null
    } else {
      const table = this.selectedTable
      if (table) {
        table.removeSelection()
      }
    }
    this.resetGroupSelection(ignoreContentSelect)
  }

  resetGroupSelection(ignoreContentSelect?: boolean) {
    this.selectedGroupInfo.object = null
    const selectedTextSp = this.selectedGroupInfo.selectedTextSlideElement
    if (selectedTextSp) {
      if (ignoreContentSelect !== true) {
        clearTextSelectionContent(selectedTextSp)
      }
      this.selectedGroupInfo.selectedTextSlideElement = null
    }
    for (const sp of this.selectedGroupInfo.selectedSlideElements) {
      sp.selected = false
    }
    this.selectedGroupInfo.selectedSlideElements.length = 0
  }

  deselect(sp: SlideElement, ignoreContentSelect?: boolean) {
    const topMostGroup = getTopMostGroupShape(sp.group)

    const useGroupSelection =
      this.selectedGroupInfo.object != null &&
      this.selectedGroupInfo.object === topMostGroup

    const selectedSps = useGroupSelection
      ? this.selectedGroupInfo.selectedSlideElements
      : this.selectedSlideElements

    if (selectedSps && selectedSps.length > 0) {
      for (let i = 0; i < selectedSps.length; ++i) {
        if (selectedSps[i] === sp) {
          sp.selected = false
          selectedSps.splice(i, 1)
          if (useGroupSelection) {
            if (selectedSps.length <= 0) {
              this.resetGroupSelection()
            } else if (this.selectedGroupInfo.selectedTextSlideElement === sp) {
              if (ignoreContentSelect !== true) {
                clearTextSelectionContent(sp)
              }
              this.selectedGroupInfo.selectedTextSlideElement = null
            }
          } else if (this.selectedTextSlideElement === sp) {
            if (ignoreContentSelect !== true) {
              clearTextSelectionContent(sp)
            }
            this.selectedTextSlideElement = null
          } else if (this.selectedGroupInfo.object === sp) {
            this.resetGroupSelection()
          }
          return
        }
      }
    }
  }
  select(sp: SlideElement) {
    // first apply formatting on target sp
    if (
      this.presentation.spFormatCopied != null &&
      this.presentation.allowPasteFormat
    ) {
      applySlideElementFormat(sp, this.presentation.spFormatCopied)
      this.presentation.allowPasteFormat = false
    }

    const topMostGroup = getTopMostGroupShape(sp.group)

    const addToSelectedSps = (
      sp: SlideElement,
      selectedArr: SlideElement[]
    ) => {
      let i: number
      for (i = 0; i < selectedArr.length; ++i) {
        if (selectedArr[i] === sp) break
      }
      if (i === selectedArr.length) {
        sp.selected = true

        selectedArr.push(sp)
      }
    }

    if (topMostGroup) {
      if (!getPresentation(topMostGroup)) {
        return
      }

      if (this.selectedGroupInfo.object !== topMostGroup) {
        this.reset()
        this.selectedGroupInfo.object = topMostGroup
        this.select(topMostGroup)
      }
      addToSelectedSps(sp, this.selectedGroupInfo.selectedSlideElements)
    } else {
      addToSelectedSps(sp, this.selectedSlideElements)
    }
  }

  resetTextSelectionContent() {
    const statusInfo = this.selectionStatusInfo
    if (
      statusInfo.status === 'group_text' ||
      statusInfo.status === 'root_text'
    ) {
      clearTextSelectionContent(statusInfo.selectedTextSp)
    }
  }
  setTextSelection(sp: null | TextSlideElement) {
    if (sp == null) {
      this.resetTextSelectionContent()
    } else {
      this.select(sp)
    }

    if (this.selectedGroupInfo.object != null) {
      this.selectedGroupInfo.selectedTextSlideElement = sp
    } else {
      this.selectedTextSlideElement = sp
    }
  }

  checkSelection() {
    if (this.selectedGroup) {
      this.selectedGroupInfo.selectedSlideElements.forEach((sp) => {
        if (!isSlideElementInPPT(sp)) {
          this.deselect(sp)
        }
      })
    }

    this.selectedSlideElements.forEach((sp) => {
      if (!isSlideElementInPPT(sp)) {
        this.deselect(sp)
      }
    })
  }

  getSelectionState(): PPTSelectState {
    const state: Partial<PPTSelectState> = {}
    const statusInfo = this.selectionStatusInfo
    switch (statusInfo.status) {
      case 'group_text': {
        const group = statusInfo.group
        state.focus = true
        state.selectedGroup = group
        state.groupSelection = fillDocSelectStateForTextObject(
          {},
          statusInfo.selectedTextSp
        ) as PPTSelectState
        break
      }
      case 'group_collection': {
        const group = statusInfo.group
        state.focus = true
        state.selectedGroup = group
        state.groupSelection = fillDocSelectStateForSelectedElements(
          {},
          statusInfo.selectedElements
        ) as PPTSelectState
        break
      }
      case 'root_text': {
        state.focus = true
        fillDocSelectStateForTextObject(state, statusInfo.selectedTextSp)
        break
      }
      case 'root_collection': {
        fillDocSelectStateForSelectedElements(
          state,
          statusInfo.selectedElements
        )
        break
      }
    }

    state.curSlideIndex = this.presentation.currentSlideIndex
    state.focusOnNotes = this.presentation.isNotesFocused

    return state as PPTSelectState
  }

  setDocSelectInfo(info: PPTSelectState) {
    if (info.textSp && !info.textSp.isDeleted) {
      this.setTextSelection(info.textSp)
      if (
        info.textSp &&
        info.textSp.instanceType === InstanceType.GraphicFrame
      ) {
        setSelectionInfoForTable(
          info.textSp.graphicObject!,
          info.selectedTextStates as SelectionInfoOfTable
        )
      } else {
        const textTarget = getTextDocumentOfSp(info.textSp)
        if (textTarget) {
          setSelectionInfoForTextDocument(
            textTarget,
            info.selectedTextStates as SelectionInfoOfTextDocument
          )
        }
      }
    } else if (info.selectedGroup && !info.selectedGroup.isDeleted) {
      if (info.groupSelection) {
        const { textSp } = info.groupSelection
        if (textSp) {
          this.setTextSelection(textSp)
        }
        this.setDocSelectInfo(info.groupSelection)
      }
    } else {
      if (Array.isArray(info.selection)) {
        for (let i = 0; i < info.selection.length; ++i) {
          if (!info.selection[i].object.isDeleted) {
            this.select(info.selection[i].object)
          }
        }
      }
    }
  }
}

function clearTextSelectionContent(textSp: TextSlideElement) {
  if (textSp.instanceType === InstanceType.GraphicFrame) {
    if (textSp.graphicObject) {
      textSp.graphicObject.removeSelection()
    }
  } else {
    const textDoc = getTextDocumentOfSp(textSp)
    textDoc && textDoc.removeSelection()
  }
}

function fillDocSelectStateForTextObject(
  state: Partial<PPTSelectState>,
  selectedTextSp: TextSlideElement
) {
  state.textSp = selectedTextSp
  if (selectedTextSp.instanceType === InstanceType.GraphicFrame) {
    state.selectedTextStates =
      selectedTextSp.graphicObject &&
      getSelectionStateOfTable(selectedTextSp.graphicObject)
  } else {
    const textDoc = getTextDocumentOfSp(selectedTextSp)
    state.selectedTextStates =
      textDoc && getSelectionStateOfTextDocument(textDoc)
  }
  return state
}

function fillDocSelectStateForSelectedElements(
  state: Partial<PPTSelectState>,
  selectedElements: SlideElement[]
) {
  state.focus = selectedElements.length > 0
  state.selection = []

  for (const selectedSp of selectedElements) {
    state.selection.push({
      object: selectedSp,
    })
  }

  return state
}
export interface PPTSelectState {
  curSlideIndex: number
  focusOnNotes: boolean
  focus: boolean
  textSp?: TextSlideElement
  selectedTextStates?: Nullable<
    SelectionInfoOfTextDocument | SelectionInfoOfTable
  >
  selectedGroup?: GroupShape
  groupSelection?: PPTSelectState
  selection?: Array<{
    object: SlideElement
  }>
}

interface SelectionInfoOfParagraph {
  cursorPosInfo: {
    x: number
    y: number
    lineIndex: number
    elementPosition: ParagraphElementPosition
    curX: number
    curY: number
    colIndex: number
  }
  selection: {
    start: boolean
    isUse: boolean
    startPos: ParagraphElementPosition
    endPos: ParagraphElementPosition
  }
}

interface SelectionInfoOfTextDocument {
  cursorPosInfo: {
    x: number
    y: number
    elementIndex: number
    cursorX: number
    cursorY: number
  }
  selection: {
    start: boolean
    isUse: boolean
    startIndex: number
    endIndex: number
    contentSelectionInfo: SelectionInfoOfParagraph[]
  }
}

interface SelectionInfoOfTable {
  selection: {
    start: boolean
    isUse: boolean
    startPosInfo: {
      cellPos: {
        row: number
        cell: number
      }
      x: number
      y: number
      mouseEvtInfo: {
        clicks: number
        type: PointerEvtTypeValue
        ctrl: boolean
      }
    }
    endPosInfo: {
      cellPos: {
        row: number
        cell: number
      }
      x: number
      y: number
      mouseEvtInfo: {
        clicks: number
        type: PointerEvtTypeValue
        ctrl: boolean
      }
    }
    type: number
    selectionType: TableSelectionTypeValues
    selectionInfo: any
    currentRowIndex: number
    cellPositions: Array<{
      row: number
      cell: number
    }>
  }

  currentCellPos: {
    row: number
    cell: number
  }

  cellContentSelectionInfo: SelectionInfoOfTextDocument
}

function getSelectionStateOfParagraph(
  para: Paragraph
): SelectionInfoOfParagraph {
  const paragraphState: SelectionInfoOfParagraph = {
    cursorPosInfo: {
      x: para.cursorPosition.x,
      y: para.cursorPosition.y,
      lineIndex: para.cursorPosition.line,
      elementPosition: para.currentElementPosition(false, false),
      curX: para.cursorPosition.cursorX,
      curY: para.cursorPosition.cursorY,
      colIndex: para.cursorPosition.colIndex,
    },

    selection: {
      start: para.selection.start,
      isUse: para.selection.isUse,
      startPos: para.currentElementPosition(para.selection.isUse, true),
      endPos: para.currentElementPosition(para.selection.isUse, false),
    },
  }

  return paragraphState
}

function getSelectionStateOfTextDocument(
  textDoc: TextDocument
): SelectionInfoOfTextDocument {
  const cursorPosInfo = {
    x: textDoc.cursorPosition.x,
    y: textDoc.cursorPosition.y,
    elementIndex: textDoc.cursorPosition.childIndex,
    cursorX: textDoc.cursorPosition.cursorX,
    cursorY: textDoc.cursorPosition.cursorY,
  }

  const contentSelectionInfo: SelectionInfoOfParagraph[] = []

  if (true === textDoc.selection.isUse) {
    let startIndex = textDoc.selection.startIndex
    let endIndex = textDoc.selection.endIndex
    if (startIndex > endIndex) {
      const temp = startIndex
      startIndex = endIndex
      endIndex = temp
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const para = textDoc.children.at(i)
      para && contentSelectionInfo.push(getSelectionStateOfParagraph(para))
    }
  } else {
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
    para && contentSelectionInfo.push(getSelectionStateOfParagraph(para))
  }

  const selection = {
    start: textDoc.selection.start,
    isUse: textDoc.selection.isUse,
    startIndex: textDoc.selection.startIndex,
    endIndex: textDoc.selection.endIndex,
    contentSelectionInfo,
  }

  // }

  return {
    cursorPosInfo: cursorPosInfo,
    selection,
  }
}

function getSelectionStateOfTable(table: Table): SelectionInfoOfTable {
  const selection: SelectionInfoOfTable['selection'] = {
    start: table.selection.start,
    isUse: table.selection.isUse,
    startPosInfo: {
      cellPos: {
        row: table.selection.selectionStartInfo.cellPos.row,
        cell: table.selection.selectionStartInfo.cellPos.cell,
      },
      x: table.selection.selectionStartInfo.x,
      y: table.selection.selectionStartInfo.y,
      mouseEvtInfo: {
        clicks: table.selection.selectionStartInfo.mouseEvtInfo.clicks,
        type: table.selection.selectionStartInfo.mouseEvtInfo.type,
        ctrl: table.selection.selectionStartInfo.mouseEvtInfo.ctrl,
      },
    },
    endPosInfo: {
      cellPos: {
        row: table.selection.selectionEndInfo.cellPos.row,
        cell: table.selection.selectionEndInfo.cellPos.cell,
      },
      x: table.selection.selectionEndInfo.x,
      y: table.selection.selectionEndInfo.y,
      mouseEvtInfo: {
        clicks: table.selection.selectionEndInfo.mouseEvtInfo.clicks,
        type: table.selection.selectionEndInfo.mouseEvtInfo.type,
        ctrl: table.selection.selectionEndInfo.mouseEvtInfo.ctrl,
      },
    },
    type: table.selection.type,
    selectionType: TableSelectionType.COMMON,
    selectionInfo: null,
    currentRowIndex: table.selection.currentRowIndex,
    cellPositions: [],
  }

  if (
    true === table.selection.isUse &&
    TableSelectionType.CELL === table.selection.type
  ) {
    for (let i = 0; i < table.selection.cellPositions.length; i++) {
      selection.cellPositions[i] = {
        row: table.selection.cellPositions[i].row,
        cell: table.selection.cellPositions[i].cell,
      }
    }
  }

  const currentCellPos: SelectionInfoOfTable['currentCellPos'] = {
    row: table.curCell!.parent.index,
    cell: table.curCell!.index,
  }

  const cellContentSelectionInfo = getSelectionStateOfTextDocument(
    table.curCell!.textDoc
  )

  return {
    selection,
    currentCellPos: currentCellPos,
    cellContentSelectionInfo,
  }
}

function setSelectionInfoForParagraph(
  para: Paragraph,
  state: SelectionInfoOfParagraph
) {
  para.cursorPosition.x = state.cursorPosInfo.x
  para.cursorPosition.y = state.cursorPosInfo.y
  para.cursorPosition.line = state.cursorPosInfo.lineIndex
  para.cursorPosition.cursorX = state.cursorPosInfo.curX
  para.cursorPosition.cursorY = state.cursorPosInfo.curY
  para.cursorPosition.colIndex = state.cursorPosInfo.colIndex
  para.updateCursorPosByPosition(state.cursorPosInfo.elementPosition, true, -1)

  para.removeSelection()

  para.selection.start = state.selection.start
  para.selection.isUse = state.selection.isUse

  if (true === para.selection.isUse) {
    updateSelectionStateForParagraph(
      para,
      state.selection.startPos,
      state.selection.endPos
    )
  }
}

function setSelectionInfoForTextDocument(
  textDoc: TextDocument,
  state: SelectionInfoOfTextDocument
) {
  textDoc.cursorPosition = {
    x: state.cursorPosInfo.x,
    y: state.cursorPosInfo.y,
    childIndex: state.cursorPosInfo.elementIndex,
    cursorX: state.cursorPosInfo.cursorX,
    cursorY: state.cursorPosInfo.cursorY,
  }

  textDoc.selection = {
    start: state.selection.start,
    isUse: state.selection.isUse,
    startIndex: state.selection.startIndex,
    endIndex: state.selection.endIndex,
  }

  const contentSelectionInfo = state.selection.contentSelectionInfo
  if (true === textDoc.selection.isUse) {
    let startIndex = textDoc.selection.startIndex
    let endIndex = textDoc.selection.endIndex
    if (startIndex > endIndex) {
      const temp = startIndex
      startIndex = endIndex
      endIndex = temp
    }

    for (let i = startIndex; i <= endIndex; i++) {
      const paraSelectState = contentSelectionInfo[i - startIndex]
      const para = textDoc.children.at(i)
      if (para && paraSelectState) {
        setSelectionInfoForParagraph(para, paraSelectState)
      }
    }
  } else {
    const paraSelectState = contentSelectionInfo[0]
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
    if (para && paraSelectState) {
      setSelectionInfoForParagraph(para, paraSelectState)
    }
  }
}

function setSelectionInfoForTable(table: Table, state: SelectionInfoOfTable) {
  table.selection = {
    start: state.selection.start,
    isUse: state.selection.isUse,
    selectionStartInfo: {
      cellPos: {
        row: state.selection.startPosInfo.cellPos.row,
        cell: state.selection.startPosInfo.cellPos.cell,
      },
      x: state.selection.startPosInfo.x,
      y: state.selection.startPosInfo.y,
      mouseEvtInfo: {
        clicks: state.selection.startPosInfo.mouseEvtInfo.clicks,
        type: state.selection.startPosInfo.mouseEvtInfo.type,
        ctrl: state.selection.startPosInfo.mouseEvtInfo.ctrl,
      },
    },
    selectionEndInfo: {
      cellPos: {
        row: state.selection.endPosInfo.cellPos.row,
        cell: state.selection.endPosInfo.cellPos.cell,
      },
      x: state.selection.endPosInfo.x,
      y: state.selection.endPosInfo.y,
      mouseEvtInfo: {
        clicks: state.selection.endPosInfo.mouseEvtInfo.clicks,
        type: state.selection.endPosInfo.mouseEvtInfo.type,
        ctrl: state.selection.endPosInfo.mouseEvtInfo.ctrl,
      },
    },
    type: state.selection.type,
    cellPositions: [],
    selectionType: TableSelectionType.COMMON,
    selectionInfo: null,
    currentRowIndex: state.selection.currentRowIndex,
  }

  table.selection.cellPositions = []
  if (
    true === table.selection.isUse &&
    TableSelectionType.CELL === table.selection.type
  ) {
    for (let i = 0; i < state.selection.cellPositions.length; i++) {
      table.selection.cellPositions[i] = {
        row: state.selection.cellPositions[i].row,
        cell: state.selection.cellPositions[i].cell,
      }
    }
  }

  table.curCell = table.children
    .at(state.currentCellPos.row)
    ?.getCellByIndex(state.currentCellPos.cell)
  if (table.curCell) {
    setSelectionInfoForTextDocument(
      table.curCell.textDoc,
      state.cellContentSelectionInfo
    )
  }
}

function getSpForFormatting(sps: SlideElement[]): Nullable<Shape | ImageShape> {
  if (sps && sps.length === 1) {
    const sp = sps[0]
    if (
      sp.instanceType === InstanceType.Shape ||
      sp.instanceType === InstanceType.ImageShape
    ) {
      return sp
    }
  }

  return undefined
}
