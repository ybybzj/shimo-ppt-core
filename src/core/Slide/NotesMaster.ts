import {
  PlaceholderType,
  ShapeLocks,
  TextVerticalType,
} from '../SlideElement/const'
import { idGenerator } from '../../globals/IdGenerator'
import { EditActionFlag } from '../EditActionFlag'

import { isNumber, isDict } from '../../common/utils'
import { createGeometry } from '../SlideElement/geometry/creators'
import { ParaPr } from '../textAttributes/ParaPr'
import { AlignKind } from '../common/const/attrs'
import { TextPr } from '../textAttributes/TextPr'
import { TextListStyle } from '../textAttributes/TextListStyle'
import { InstanceType } from '../instanceTypes'
import {
  setDeletedForSp,
  setParentForSlideElement,
} from '../utilities/shape/updates'
import { ScaleOfPPTXSizes } from '../common/const/unit'
import { Nullable } from '../../../liber/pervasive'
import { CSld } from './CSld'

import { Bg } from '../SlideElement/attrs/bg'
import { ClrMap, setDefaultColorMap } from '../color/clrMap'
import { CComplexColor } from '../color/complexColor'
import { Ph } from '../SlideElement/attrs/ph'
import { UniNvPr, SpPr, Xfrm } from '../SlideElement/attrs/shapePrs'
import { Theme } from '../SlideElement/attrs/theme'
import { informEntityChildrenChange } from '../calculation/informChanges/contentChange'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { Shape } from '../SlideElement/Shape'
import { cloneSlideElement } from '../utilities/shape/clone'
import { collectAllFontNamesForSlideElement } from '../utilities/shape/getters'
import { setLock } from '../utilities/shape/locks'
import { ThemeColor } from '../../io/dataType/spAttrs'
import { FillEffects } from '../SlideElement/attrs/fill/fill'
import { SlideElement } from '../SlideElement/type'

const DefaultNoteMasterRefId = 'nmuoaprj'

export class NotesMaster {
  readonly instanceType = InstanceType.NotesMaster
  clrMap: ClrMap
  cSld: CSld
  hf: any
  txStyles: Nullable<TextListStyle>
  theme: Theme | null
  notesLst: any[]
  id: string
  refId?: string
  constructor() {
    this.clrMap = new ClrMap()
    this.cSld = new CSld(this)
    this.hf = null
    this.txStyles = null

    this.theme = null
    this.notesLst = []

    this.id = idGenerator.newId()
  }
  getRefId(): string {
    const refId = this.refId ?? DefaultNoteMasterRefId
    this.refId = refId
    return refId
  }
  setRefId(rId: string) {
    this.refId = rId
  }

  getId() {
    return this.id
  }

  setTheme(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_NotesMasterSetNotesTheme,
      this.theme,
      pr
    )
    this.theme = pr
  }

  setHF(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_NotesMasterSetHF,
      this.hf,
      pr
    )
    this.hf = pr
  }

  setTxStyles(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_NotesMasterSetNotesStyle,
      this.txStyles,
      pr
    )
    this.txStyles = pr
  }

  addToSpTreeAtIndex(index, obj) {
    const _index = Math.max(0, Math.min(index, this.cSld.spTree.length))
    informEntityChildrenChange(
      this,
      EditActionFlag.edit_NotesMasterAddToSpTree,
      _index,
      [obj],
      true
    )
    this.cSld.addSpAtIndex(obj, _index)
  }

  removeFromSpTreeAtIndex(index: number) {
    if (index > -1 && index < this.cSld.spTree.length) {
      informEntityChildrenChange(
        this,
        EditActionFlag.edit_NotesMasterRemoveFromTree,
        index,
        this.cSld.removeSpFromIndex(index),
        false
      )
    }
  }

  removeSpById(id) {
    for (let i = this.cSld.spTree.length - 1; i > -1; --i) {
      if (this.cSld.spTree[i].getId() === id) {
        this.removeFromSpTreeAtIndex(i)
      }
    }
  }

  changeBackground(bg) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_NotesMasterSetBg,
      this.cSld.bg,
      bg
    )
    this.cSld.bg = bg
  }

  setCSldName(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_NotesMasterSetName,
      this.cSld.name,
      pr
    )
    this.cSld.name = pr
  }

  addToNotesList(pr, pos) {
    const _pos = isNumber(pos)
      ? Math.max(0, Math.min(pos, this.notesLst.length))
      : this.notesLst.length
    informEntityChildrenChange(
      this,
      EditActionFlag.edit_NotesMasterAddToNotesLst,
      _pos,
      [pr],
      true
    )
    this.notesLst.splice(_pos, 0, pr)
  }

  getAllFonts(fonts) {
    let i
    if (this.theme) {
      this.theme.collectAllFontNames(fonts)
    }

    if (this.txStyles) {
      this.txStyles.collectAllFontNames(fonts)
    }

    for (i = 0; i < this.notesLst.length; ++i) {
      this.notesLst[i].getAllFonts(fonts)
    }

    for (i = 0; i < this.cSld.spTree.length; ++i) {
      collectAllFontNamesForSlideElement(this.cSld.spTree[i], fonts)
    }
  }

  clone(map) {
    map = map || {}
    let i
    const notesMaster = new NotesMaster()
    // if(this.clrMap){
    //     this.setClrMap(this.clrMap.clone());
    // }
    if (typeof this.cSld.name === 'string' && this.cSld.name.length > 0) {
      notesMaster.setCSldName(this.cSld.name)
    }
    if (this.cSld.bg) {
      notesMaster.changeBackground(this.cSld.bg.clone())
    }
    for (i = 0; i < this.cSld.spTree.length; ++i) {
      let copiedSp: SlideElement

      if (this.cSld.spTree[i].instanceType === InstanceType.GroupShape) {
        copiedSp = cloneSlideElement(this.cSld.spTree[i], map)
      } else {
        copiedSp = cloneSlideElement(this.cSld.spTree[i])
      }
      if (isDict(map)) {
        map[this.cSld.spTree[i].id] = copiedSp.id
      }
      notesMaster.addToSpTreeAtIndex(notesMaster.cSld.spTree.length, copiedSp)
      setParentForSlideElement(
        notesMaster.cSld.spTree[notesMaster.cSld.spTree.length - 1],
        notesMaster
      )
    }
    if (this.hf) {
      notesMaster.setHF(this.hf.clone())
    }
    if (this.txStyles) {
      notesMaster.setTxStyles(this.txStyles.clone())
    }
    return notesMaster
  }
}

export function createNotesMaster() {
  const noteMaster = new NotesMaster()
  const bg = Bg.Ref(1001, CComplexColor.Scheme(ThemeColor.Background1))

  noteMaster.changeBackground(bg)

  let shape = new Shape()
  setDeletedForSp(shape, false)
  let nvSpPr = new UniNvPr(shape)
  let cNvPr = nvSpPr.cNvPr
  cNvPr.setId(2)
  cNvPr.setName('Header Placeholder 1')
  let ph = Ph.new({
    type: PlaceholderType.hdr,
    sz: 2,
  })

  nvSpPr.nvPr.setPh(ph)
  shape.setNvSpPr(nvSpPr)
  setLock(shape, ShapeLocks.noGrp, true)
  shape.setSpPr(new SpPr())
  shape.spPr!.setParent(shape)
  shape.spPr!.setXfrm(new Xfrm(shape.spPr))
  shape.spPr!.xfrm!.setParent(shape.spPr)
  shape.spPr!.xfrm!.setOffX(0)
  shape.spPr!.xfrm!.setOffY(0)
  shape.spPr!.xfrm!.setExtX(2971800 / ScaleOfPPTXSizes)
  shape.spPr!.xfrm!.setExtY(458788 / ScaleOfPPTXSizes)
  shape.spPr!.setGeometry(createGeometry('rect'))
  shape.spPr!.geometry!.setParent(shape.spPr)
  shape.createTxBody()
  let bodyPr = shape.txBody!.bodyPr!.clone()
  bodyPr.vert = TextVerticalType.horz
  bodyPr.lIns = 91440 / ScaleOfPPTXSizes
  bodyPr.tIns = 45720 / ScaleOfPPTXSizes
  bodyPr.rIns = 91440 / ScaleOfPPTXSizes
  bodyPr.bIns = 45720 / ScaleOfPPTXSizes
  bodyPr.rtlCol = false
  bodyPr.anchor = 1
  shape.txBody!.setBodyPr(bodyPr)
  let txLstStyle = new TextListStyle()
  txLstStyle.levels[0] = ParaPr.new()
  txLstStyle.levels[0].jcAlign = AlignKind.Left
  txLstStyle.levels[0].defaultRunPr = TextPr.new()
  txLstStyle.levels[0].defaultRunPr.fontSize = 12
  shape.txBody!.setTextListStyle(txLstStyle)
  shape.setParent(noteMaster)
  shape.spTreeParent = noteMaster.cSld
  noteMaster.addToSpTreeAtIndex(0, shape)
  //endParaPr

  shape = new Shape()
  setDeletedForSp(shape, false)
  nvSpPr = new UniNvPr(shape)
  cNvPr = nvSpPr.cNvPr
  cNvPr.setId(3)
  cNvPr.setName('Date Placeholder 2')
  ph = Ph.new({
    type: PlaceholderType.dt,
    idx: 2 + '',
  })

  nvSpPr.nvPr.setPh(ph)
  shape.setNvSpPr(nvSpPr)
  setLock(shape, ShapeLocks.noGrp, true)
  shape.setSpPr(new SpPr())
  shape.spPr!.setParent(shape)
  shape.spPr!.setXfrm(new Xfrm(shape.spPr))
  shape.spPr!.xfrm!.setParent(shape.spPr)
  shape.spPr!.xfrm!.setOffX(3884613 / ScaleOfPPTXSizes)
  shape.spPr!.xfrm!.setOffY(0)
  shape.spPr!.xfrm!.setExtX(2971800 / ScaleOfPPTXSizes)
  shape.spPr!.xfrm!.setExtY(458788 / ScaleOfPPTXSizes)
  shape.spPr!.setGeometry(createGeometry('rect'))
  shape.spPr!.geometry!.setParent(shape.spPr)
  shape.createTxBody()
  bodyPr = shape.txBody!.bodyPr!.clone()
  bodyPr.vert = TextVerticalType.horz
  bodyPr.lIns = 91440 / ScaleOfPPTXSizes
  bodyPr.tIns = 45720 / ScaleOfPPTXSizes
  bodyPr.rIns = 91440 / ScaleOfPPTXSizes
  bodyPr.bIns = 45720 / ScaleOfPPTXSizes
  bodyPr.rtlCol = false
  bodyPr.anchor = 1
  shape.txBody!.setBodyPr(bodyPr)
  txLstStyle = new TextListStyle()
  txLstStyle.levels[0] = ParaPr.new()
  txLstStyle.levels[0].jcAlign = AlignKind.Right
  txLstStyle.levels[0].defaultRunPr = TextPr.new()
  txLstStyle.levels[0].defaultRunPr.fontSize = 12
  //endParaPr
  shape.txBody!.setTextListStyle(txLstStyle)
  shape.setParent(noteMaster)
  shape.spTreeParent = noteMaster.cSld
  noteMaster.addToSpTreeAtIndex(1, shape)

  shape = new Shape()
  setDeletedForSp(shape, false)
  nvSpPr = new UniNvPr(shape)
  cNvPr = nvSpPr.cNvPr
  cNvPr.setId(3)
  cNvPr.setName('Date Placeholder 2')
  ph = Ph.new({
    type: PlaceholderType.dt,
    idx: 3 + '',
  })

  nvSpPr.nvPr.setPh(ph)
  shape.setNvSpPr(nvSpPr)
  setLock(shape, ShapeLocks.noGrp, true)
  shape.setSpPr(new SpPr())
  shape.spPr!.setParent(shape)
  shape.spPr!.setXfrm(new Xfrm(shape.spPr))
  shape.spPr!.xfrm!.setParent(shape.spPr)
  shape.spPr!.xfrm!.setOffX(3884613 / ScaleOfPPTXSizes)
  shape.spPr!.xfrm!.setOffY(0)
  shape.spPr!.xfrm!.setExtX(2971800 / ScaleOfPPTXSizes)
  shape.spPr!.xfrm!.setExtY(458788 / ScaleOfPPTXSizes)
  shape.spPr!.setGeometry(createGeometry('rect'))
  shape.spPr!.geometry!.setParent(shape.spPr)
  shape.createTxBody()
  bodyPr = shape.txBody!.bodyPr!.clone()
  bodyPr.vert = TextVerticalType.horz
  bodyPr.lIns = 91440 / ScaleOfPPTXSizes
  bodyPr.tIns = 45720 / ScaleOfPPTXSizes
  bodyPr.rIns = 91440 / ScaleOfPPTXSizes
  bodyPr.bIns = 45720 / ScaleOfPPTXSizes
  bodyPr.rtlCol = false
  bodyPr.anchor = 1
  shape.txBody!.setBodyPr(bodyPr)
  txLstStyle = new TextListStyle()
  txLstStyle.levels[0] = ParaPr.new()
  txLstStyle.levels[0].jcAlign = AlignKind.Right
  txLstStyle.levels[0].defaultRunPr = TextPr.new()
  txLstStyle.levels[0].defaultRunPr.fontSize = 12
  //endParaPr
  shape.txBody!.setTextListStyle(txLstStyle)
  shape.setParent(noteMaster)
  shape.spTreeParent = noteMaster.cSld
  noteMaster.addToSpTreeAtIndex(2, shape)

  shape = new Shape()
  setDeletedForSp(shape, false)
  nvSpPr = new UniNvPr(shape)
  cNvPr = nvSpPr.cNvPr
  cNvPr.setId(5)
  cNvPr.setName('Notes Placeholder 4')
  ph = Ph.new({
    type: PlaceholderType.body,
    sz: 2,
    idx: 1 + '',
  })

  nvSpPr.nvPr.setPh(ph)
  shape.setNvSpPr(nvSpPr)
  setLock(shape, ShapeLocks.noGrp, true)
  shape.setSpPr(new SpPr())
  shape.spPr!.setParent(shape)
  shape.spPr!.setXfrm(new Xfrm(shape.spPr))
  shape.spPr!.xfrm!.setParent(shape.spPr)
  shape.spPr!.xfrm!.setOffX(685800 / ScaleOfPPTXSizes)
  shape.spPr!.xfrm!.setOffY(4400550 / ScaleOfPPTXSizes)
  shape.spPr!.xfrm!.setExtX(5486400 / ScaleOfPPTXSizes)
  shape.spPr!.xfrm!.setExtY(3600450 / ScaleOfPPTXSizes)
  shape.spPr!.setGeometry(createGeometry('rect'))
  shape.spPr!.geometry!.setParent(shape.spPr)
  shape.createTxBody()
  bodyPr = shape.txBody!.bodyPr!.clone()
  bodyPr.vert = TextVerticalType.horz
  bodyPr.lIns = 91440 / ScaleOfPPTXSizes
  bodyPr.tIns = 45720 / ScaleOfPPTXSizes
  bodyPr.rIns = 91440 / ScaleOfPPTXSizes
  bodyPr.bIns = 45720 / ScaleOfPPTXSizes
  bodyPr.rtlCol = false
  bodyPr.anchor = 1
  shape.txBody!.setBodyPr(bodyPr)
  txLstStyle = new TextListStyle()
  //endParaPr
  shape.txBody!.setTextListStyle(txLstStyle)
  shape.setParent(noteMaster)
  shape.spTreeParent = noteMaster.cSld
  noteMaster.addToSpTreeAtIndex(3, shape)

  shape = new Shape()
  setDeletedForSp(shape, false)
  nvSpPr = new UniNvPr(shape)
  cNvPr = nvSpPr.cNvPr
  cNvPr.setId(6)
  cNvPr.setName('Footer Placeholder 5')
  ph = Ph.new({
    type: PlaceholderType.ftr,
    idx: 4 + '',
    sz: 2,
  })

  nvSpPr.nvPr.setPh(ph)
  shape.setNvSpPr(nvSpPr)
  setLock(shape, ShapeLocks.noGrp, true)
  shape.setSpPr(new SpPr())
  shape.spPr!.setParent(shape)
  shape.spPr!.setXfrm(new Xfrm(shape.spPr))
  shape.spPr!.xfrm!.setParent(shape.spPr)
  shape.spPr!.xfrm!.setOffX(0)
  shape.spPr!.xfrm!.setOffY(8685213 / ScaleOfPPTXSizes)
  shape.spPr!.xfrm!.setExtX(2971800 / ScaleOfPPTXSizes)
  shape.spPr!.xfrm!.setExtY(458787 / ScaleOfPPTXSizes)
  shape.spPr!.setGeometry(createGeometry('rect'))
  shape.spPr!.geometry!.setParent(shape.spPr)
  shape.createTxBody()
  bodyPr = shape.txBody!.bodyPr!.clone()
  bodyPr.vert = TextVerticalType.horz
  bodyPr.lIns = 91440 / ScaleOfPPTXSizes
  bodyPr.tIns = 45720 / ScaleOfPPTXSizes
  bodyPr.rIns = 91440 / ScaleOfPPTXSizes
  bodyPr.bIns = 45720 / ScaleOfPPTXSizes
  bodyPr.rtlCol = false
  bodyPr.anchor = 0
  shape.txBody!.setBodyPr(bodyPr)
  txLstStyle = new TextListStyle()
  txLstStyle.levels[0] = ParaPr.new()
  txLstStyle.levels[0].jcAlign = AlignKind.Left
  txLstStyle.levels[0].defaultRunPr = TextPr.new()
  txLstStyle.levels[0].defaultRunPr.fontSize = 12
  //endParaPr
  shape.txBody!.setTextListStyle(txLstStyle)
  shape.setParent(noteMaster)
  shape.spTreeParent = noteMaster.cSld
  noteMaster.addToSpTreeAtIndex(4, shape)

  shape = new Shape()
  setDeletedForSp(shape, false)
  nvSpPr = new UniNvPr(shape)
  cNvPr = nvSpPr.cNvPr
  cNvPr.setId(7)
  cNvPr.setName('Slide Number Placeholder 6')
  ph = Ph.new({
    type: PlaceholderType.sldNum,
    idx: 10 + '',
    sz: 2,
  })

  nvSpPr.nvPr.setPh(ph)
  shape.setNvSpPr(nvSpPr)
  setLock(shape, ShapeLocks.noGrp, true)
  shape.setSpPr(new SpPr())
  shape.spPr!.setParent(shape)
  shape.spPr!.setXfrm(new Xfrm(shape.spPr))
  shape.spPr!.xfrm!.setParent(shape.spPr)
  shape.spPr!.xfrm!.setOffX(3884613 / ScaleOfPPTXSizes)
  shape.spPr!.xfrm!.setOffY(8685213 / ScaleOfPPTXSizes)
  shape.spPr!.xfrm!.setExtX(2971800 / ScaleOfPPTXSizes)
  shape.spPr!.xfrm!.setExtY(458787 / ScaleOfPPTXSizes)
  shape.spPr!.setGeometry(createGeometry('rect'))
  shape.spPr!.geometry!.setParent(shape.spPr)
  shape.createTxBody()
  bodyPr = shape.txBody!.bodyPr!.clone()
  bodyPr.vert = TextVerticalType.horz
  bodyPr.lIns = 91440 / ScaleOfPPTXSizes
  bodyPr.tIns = 45720 / ScaleOfPPTXSizes
  bodyPr.rIns = 91440 / ScaleOfPPTXSizes
  bodyPr.bIns = 45720 / ScaleOfPPTXSizes
  bodyPr.rtlCol = false
  bodyPr.anchor = 0
  shape.txBody!.setBodyPr(bodyPr)
  txLstStyle = new TextListStyle()
  txLstStyle.levels[0] = ParaPr.new()
  txLstStyle.levels[0].jcAlign = AlignKind.Right
  txLstStyle.levels[0].defaultRunPr = TextPr.new()
  txLstStyle.levels[0].defaultRunPr.fontSize = 12
  //endParaPr
  shape.txBody!.setTextListStyle(txLstStyle)
  shape.setParent(noteMaster)
  shape.spTreeParent = noteMaster.cSld
  noteMaster.addToSpTreeAtIndex(5, shape)

  //clrMap
  setDefaultColorMap(noteMaster.clrMap)

  txLstStyle = new TextListStyle()

  txLstStyle.levels[0] = ParaPr.new()
  txLstStyle.levels[0].ind.left = 0
  txLstStyle.levels[0].jcAlign = AlignKind.Left
  txLstStyle.levels[0].defaultTab = 914400 / ScaleOfPPTXSizes

  txLstStyle.levels[0].defaultRunPr = TextPr.new()
  txLstStyle.levels[0].defaultRunPr.fontSize = 12
  txLstStyle.levels[0].defaultRunPr.fillEffects = FillEffects.SolidScheme(
    ThemeColor.Text1,
    0
  )

  txLstStyle.levels[0].defaultRunPr.rFonts.ascii = {
    name: '+mn-lt',
    index: -1,
  }
  txLstStyle.levels[0].defaultRunPr.rFonts.eastAsia = {
    name: '+mn-ea',
    index: -1,
  }
  txLstStyle.levels[0].defaultRunPr.rFonts.cs = { name: '+mn-cs', index: -1 }

  txLstStyle.levels[1] = txLstStyle.levels[0].clone()
  txLstStyle.levels[1].ind.left = 457200 / ScaleOfPPTXSizes

  txLstStyle.levels[2] = txLstStyle.levels[0].clone()
  txLstStyle.levels[2].ind.left = 914400 / ScaleOfPPTXSizes

  txLstStyle.levels[3] = txLstStyle.levels[0].clone()
  txLstStyle.levels[3].ind.left = 1371600 / ScaleOfPPTXSizes

  txLstStyle.levels[4] = txLstStyle.levels[0].clone()
  txLstStyle.levels[4].ind.left = 1828800 / ScaleOfPPTXSizes

  txLstStyle.levels[5] = txLstStyle.levels[0].clone()
  txLstStyle.levels[5].ind.left = 2286000 / ScaleOfPPTXSizes

  txLstStyle.levels[6] = txLstStyle.levels[0].clone()
  txLstStyle.levels[6].ind.left = 2743200 / ScaleOfPPTXSizes

  txLstStyle.levels[7] = txLstStyle.levels[0].clone()
  txLstStyle.levels[7].ind.left = 3200400 / ScaleOfPPTXSizes

  txLstStyle.levels[8] = txLstStyle.levels[0].clone()
  txLstStyle.levels[8].ind.left = 3657600 / ScaleOfPPTXSizes
  noteMaster.setTxStyles(txLstStyle)
  return noteMaster
}
