import { Nullable } from '../../../liber/pervasive'
import { ParaHyperlink } from '../Paragraph/ParaContent/ParaHyperlink'
import { PresentationField } from '../Paragraph/ParaContent/PresentationField'
import { Paragraph } from '../Paragraph/Paragraph'
import { TableCell } from '../Table/TableCell'

export class CurrentFocusElementsInfo {
  isTable: boolean
  private _isMultiSelection: boolean
  paragraph: Nullable<Paragraph>
  hyperlink: Nullable<ParaHyperlink>
  cell: Nullable<TableCell>
  presentationField: Nullable<PresentationField>

  constructor(_oPr?) {
    this.isTable = false
    this._isMultiSelection = false
    this.paragraph = null
    this.hyperlink = null
    this.cell = null
    this.presentationField = null
  }
  reset() {
    this.isTable = false
    this._isMultiSelection = false
  }

  setTable() {
    this.isTable = true
  }

  setMultiSelection() {
    this._isMultiSelection = true
  }

  isMultiSelection() {
    return this._isMultiSelection
  }
  setCell(cell: TableCell) {
    this.cell = cell
  }
  getCell() {
    return this.cell
  }

  setParagraph(para: Paragraph) {
    this.paragraph = para
  }

  setHyperlink(hyperlink: ParaHyperlink) {
    this.hyperlink = hyperlink
  }

  getHyperlink() {
    if (this.hyperlink) return this.hyperlink

    return null
  }

  setPresentationField(field: PresentationField) {
    this.presentationField = field
  }
  getPresentationField() {
    return this.presentationField
  }
}
