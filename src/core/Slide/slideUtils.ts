import { SlideLayout } from './SlideLayout'
import { SlideMaster } from './SlideMaster'
import { Presentation } from './Presentation'

export function createDefaultSlideLayout(master: SlideMaster) {
  const layout = new SlideLayout(master.presentation)
  layout.theme = master.theme
  layout.master = master
  return layout
}
export function createDefaultMasterSlide(presentation: Presentation, theme) {
  const master = new SlideMaster(presentation, theme)
  master.theme = theme
  master.layouts[0] = createDefaultSlideLayout(master)
  return master
}
