import { CSld } from './CSld'
import { Notes } from './Notes'
import { NotesMaster } from './NotesMaster'
import { Slide } from './Slide'
import { SlideLayout } from './SlideLayout'
import { SlideMaster } from './SlideMaster'
import { GroupShape } from '../SlideElement/GroupShape'

export type SlideElementParent =
  | Slide
  | SlideLayout
  | SlideMaster
  | Notes
  | NotesMaster

export type SpTreeParent = CSld | GroupShape
