import { Nullable } from '../../../liber/pervasive'
import { InstanceType } from '../instanceTypes'
import { SlideElement } from '../SlideElement/type'
import { Notes } from './Notes'
import { NotesMaster } from './NotesMaster'
import { Slide } from './Slide'
import { SlideLayout } from './SlideLayout'
import { SlideMaster } from './SlideMaster'
import { onSlideElemntRemoved } from '../utilities/shape/updates'
import { Bg } from '../SlideElement/attrs/bg'
import { UniNvPr } from '../SlideElement/attrs/shapePrs'
import { getUniNvPr } from '../utilities/shape/getters'
import { EditorUtil } from '../../globals/editor'

type csldParent = Slide | SlideMaster | SlideLayout | NotesMaster | Notes

export class CSld {
  readonly instanceType = InstanceType.CSld
  name: string
  bg: Nullable<Bg>
  spTree: SlideElement[]
  maxId: number
  parent: csldParent
  constructor(parent: csldParent) {
    this.parent = parent
    this.name = ''
    this.bg = null
    this.spTree = []
    this.maxId = 1
  }

  get id(): string {
    return `csld${this.parent.id}`
  }

  getMaxId = (): number => {
    return this.maxId
  }

  genNewMaxId = (): number => {
    return ++this.maxId
  }

  updateMaxId(id: number) {
    if (typeof id === 'number' && id > this.maxId) {
      this.maxId = id
    }
  }

  addSpAtIndex(sp: SlideElement, index: number) {
    this.checkUniNvPrForSps([sp])
    sp.spTreeParent = this
    EditorUtil.entityRegistry.add(sp, sp.id)
    this.spTree.splice(index, 0, sp)
  }

  removeSpFromIndex(index: number) {
    const sp = this.spTree.splice(index, 1)
    if (sp.length > 0) {
      onSlideElemntRemoved(sp[0])
    }

    return sp
  }

  setSpTree(sps: SlideElement[]) {
    sps.forEach((sp) => {
      sp.spTreeParent = this
    })
    this.spTree = sps
  }

  checkUniNvPrForSps = (shapes: Array<SlideElement>) => {
    const currentIdMap = {}
    const needUpdateIdShapes: Array<SlideElement> = []
    this.spTree.forEach((shape) => {
      callOnSpUniNvPr(shape, (_, nvSpPr) => {
        if (nvSpPr) {
          const nid = nvSpPr.cNvPr.nid
          currentIdMap[`${nid}`] = true
        }
      })
    })
    const updateNvPrId = (shape, nvSpPr) => {
      const newNvSpPr = nvSpPr.clone()
      const newId = this.genNewMaxId()
      newNvSpPr.cNvPr.setId(newId)
      shape.setNvSpPr(newNvSpPr)
      currentIdMap[`${newId}`] = true
    }
    shapes.forEach((shape) => {
      callOnSpUniNvPr(shape, (shape, nvSpPr) => {
        if (!nvSpPr) {
          nvSpPr = new UniNvPr(shape)
          const newId = this.genNewMaxId()
          nvSpPr.cNvPr.setId(newId)
          shape.setNvSpPr(nvSpPr)
          currentIdMap[`${newId}`] = true
        } else if (nvSpPr) {
          const nid = nvSpPr.cNvPr.nid
          const isExistId = currentIdMap[`${nid}`]
          if (isExistId) {
            needUpdateIdShapes.push(shape)
          } else {
            if (typeof nid !== 'number' || nid <= 1) {
              updateNvPrId(shape, nvSpPr)
            } else {
              this.updateMaxId(nid)
            }
          }
        }
      })
    })
    needUpdateIdShapes.forEach((shape) => {
      const uniNvProps = getUniNvPr(shape)
      if (uniNvProps) {
        updateNvPrId(shape, uniNvProps)
      }
    })
  }
}

function callOnSpUniNvPr(
  sp: SlideElement,
  callback: (sp: SlideElement, nvSpPr: Nullable<UniNvPr>) => void
) {
  if (sp) {
    switch (sp.instanceType) {
      case InstanceType.GroupShape: {
        callback(sp, sp.nvGrpSpPr)
        for (let i = 0; i < sp.spTree.length; ++i) {
          callOnSpUniNvPr(sp.spTree[i], callback)
        }
        break
      }
      case InstanceType.ImageShape: {
        callback(sp, sp.nvPicPr)
        break
      }
      case InstanceType.GraphicFrame: {
        callback(sp, sp.nvGraphicFramePr)
        break
      }
      case InstanceType.Shape: {
        callback(sp, sp.nvSpPr)
        break
      }
    }
  }
}
