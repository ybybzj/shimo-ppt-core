import { isDict } from '../../common/utils'
import { idGenerator } from '../../globals/IdGenerator'
import { gRefIdCounter, RefIdPrefixes } from '../../globals/RefIdCounter'
import { EditActionFlag } from '../EditActionFlag'
import { InstanceType } from '../instanceTypes'
import { Presentation } from './Presentation'
import { Slide } from './Slide'
import { SlideMaster } from './SlideMaster'
import { isPlaceholder } from '../utilities/shape/asserts'
import { collectAllFontNamesForSlideElement } from '../utilities/shape/getters'
import { changeSlideElementSize } from '../utilities/shape/operations'
import { setParentForSlideElement } from '../utilities/shape/updates'
import { CanvasDrawer } from '../graphic/CanvasDrawer'
import { ShapeSizeUpdateInfo } from '../../re/export/Slide'
import { SlideTransition } from './SlideTransition'
import { Bounds, GraphicBounds } from '../graphic/Bounds'
import { drawSlideElement } from '../render/drawSlideElement'
import { CSld } from './CSld'
import { updatePresentationFields } from '../utilities/fields'
import { ClrMap } from '../color/clrMap'
import { HF } from '../SlideElement/attrs/hf'
import { informEntityChildrenChange } from '../calculation/informChanges/contentChange'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { cloneSlideElement } from '../utilities/shape/clone'
import { calculateForSlideLayout } from '../calculation/calculate/slideLayout'
import { Theme } from '../SlideElement/attrs/theme'
import { Nullable } from '../../../liber/pervasive'
import {
  initCalcStatusCodeForSlideLayout,
  SlideLayoutCalcStatusCode,
  SlideLayoutCalcStatusFlags,
} from '../calculation/updateCalcStatus/calcStatus'
import { SlideElement } from '../SlideElement/type'
import { PPT_Default_SIZE } from '../common/const/drawing'

export class SlideLayout {
  readonly instanceType = InstanceType.SlideLayout
  // OO Attrs
  cSld: CSld
  clrMap?: ClrMap
  hf?: HF
  transition?: SlideTransition
  matchingName: string
  preserve: boolean
  showMasterPhAnim: boolean
  showMasterSp?: boolean
  userDrawn: boolean
  type?: number
  master: null | SlideMaster
  theme?: Nullable<Theme>
  // other
  imgBase64: string
  thumbPixWidth: number // todo, remove
  thumbPixHeight: number
  width: number
  height: number
  bounds: GraphicBounds
  calcStatusCode!: SlideLayoutCalcStatusCode
  id: string
  refId?: string
  lastCalcSlideIndex: number
  presentation: Presentation
  constructor(presentation: Presentation) {
    this.presentation = presentation
    this.cSld = new CSld(this)
    this.clrMap = undefined
    this.matchingName = ''
    this.preserve = false
    this.showMasterPhAnim = false
    this.type = undefined
    this.userDrawn = true

    this.imgBase64 = ''
    this.thumbPixWidth = 0
    this.thumbPixHeight = 0

    this.width = presentation.width ?? PPT_Default_SIZE.width
    this.height = presentation.height ?? PPT_Default_SIZE.height

    this.master = null

    this.bounds = new GraphicBounds(0.0, 0.0, 0.0, 0.0)

    this.resetCalcStatus()
    this.lastCalcSlideIndex = -1
    this.id = idGenerator.newId()
  }
  resetCalcStatus() {
    this.calcStatusCode =
      initCalcStatusCodeForSlideLayout as SlideLayoutCalcStatusCode
  }

  setCalcStatusOf(flag: SlideLayoutCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode |
      flag) as SlideLayoutCalcStatusCode
  }

  unsetCalcStatusOf(flag: SlideLayoutCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode ^
      flag) as SlideLayoutCalcStatusCode
  }

  isCalcStatusSet(flag: SlideLayoutCalcStatusFlags) {
    return (this.calcStatusCode & flag) > 0
  }

  getRefId(): string {
    const refId = this.refId ?? gRefIdCounter.newIDBy(RefIdPrefixes.Layout)
    this.refId = refId
    return refId
  }
  setRefId(rId: string) {
    this.refId = rId
  }
  clone(map?) {
    map = map || {}
    const newLayout = new SlideLayout(this.presentation)
    if (typeof this.cSld.name === 'string' && this.cSld.name.length > 0) {
      newLayout.setCSldName(this.cSld.name)
    }
    if (this.cSld.bg) {
      newLayout.changeBackground(this.cSld.bg.clone())
    }
    for (let i = 0; i < this.cSld.spTree.length; ++i) {
      let copySp: SlideElement
      if (this.cSld.spTree[i].instanceType === InstanceType.GroupShape) {
        copySp = cloneSlideElement(this.cSld.spTree[i], map)
      } else {
        copySp = cloneSlideElement(this.cSld.spTree[i])
      }
      if (isDict(map)) {
        map[this.cSld.spTree[i].id] = copySp.id
      }
      newLayout.addSlideElementAt(newLayout.cSld.spTree.length, copySp)
      setParentForSlideElement(
        newLayout.cSld.spTree[newLayout.cSld.spTree.length - 1],
        newLayout
      )
    }

    if (this.clrMap) {
      newLayout.setClrMapOverride(this.clrMap.clone())
    }
    if (newLayout.matchingName !== this.matchingName) {
      newLayout.setMatchingName(this.matchingName)
    }

    if (newLayout.showMasterPhAnim !== this.showMasterPhAnim) {
      newLayout.setShowPhAnim(this.showMasterPhAnim)
    }
    if (this.type !== newLayout.type) {
      newLayout.setType(this.type)
    }
    return newLayout
  }
  setMaster(master) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideLayoutSetMaster,
      this.master,
      master
    )
    this.master = master
  }
  setMatchingName(name) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideLayoutSetMatchingName,
      this.matchingName,
      name
    )
    this.matchingName = name
  }
  setType(type) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideLayoutSetType,
      this.type,
      type
    )
    this.type = type
  }
  changeBackground(bg) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideLayoutSetBg,
      this.cSld.bg,
      bg
    )
    this.cSld.bg = bg
  }
  setCSldName(name) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideLayoutSetCSldName,
      this.cSld.name,
      name
    )
    this.cSld.name = name
  }

  setShowPhAnim(isShow) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideLayoutSetShowPhAnim,
      this.showMasterPhAnim,
      isShow
    )
    this.showMasterPhAnim = isShow
  }
  setShowMasterSp(isShow) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideLayoutSetShowMasterSp,
      this.showMasterSp,
      isShow
    )
    this.showMasterSp = isShow
  }
  setClrMapOverride(clrMap) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideLayoutSetClrMapOverride,
      this.clrMap,
      clrMap
    )
    this.clrMap = clrMap
  }

  applyTransition(transition?: SlideTransition) {
    if (this.transition == null && transition == null) {
      return
    }
    const orgTransition = this.transition?.clone()
    if (this.transition == null && transition != null) {
      this.transition = new SlideTransition()
      this.transition.reset()
    }

    if (transition != null) {
      this.transition?.applyProps(transition)
    } else {
      this.transition = undefined
    }

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideLayout_SetTransition,
      orgTransition,
      this.transition?.clone()
    )
  }

  addSlideElementAt(index, sp) {
    informEntityChildrenChange(
      this,
      EditActionFlag.edit_SlideLayoutAddToSpTree,
      index,
      [sp],
      true
    )
    this.cSld.addSpAtIndex(sp, index)
  }
  setSlideSize(w, h) {
    this.width = w
    this.height = h
  }

  updateSize(width, height, contentScaleInfo?: ShapeSizeUpdateInfo) {
    this.setSlideSize(width, height)
    if (contentScaleInfo) {
      const { scale, deltaX, deltaY } = contentScaleInfo
      const spTree = this.cSld.spTree
      for (let i = 0; i < spTree.length; ++i) {
        changeSlideElementSize(spTree[i], scale, scale, deltaX, deltaY)
      }
    }
  }

  getId() {
    return this.id
  }

  draw(graphics: CanvasDrawer, slide?: Slide, viewBounds?: Bounds) {
    if (slide) {
      if (slide.index !== this.lastCalcSlideIndex) {
        this.lastCalcSlideIndex = slide.index!
        updatePresentationFields(this)
        calculateForSlideLayout(this)
      }
    }
    for (let i = 0; i < this.cSld.spTree.length; ++i) {
      const sp = this.cSld.spTree[i]
      if (!isPlaceholder(sp)) {
        drawSlideElement(sp, graphics, undefined, viewBounds)
      }
    }
  }

  getAllFonts(fonts) {
    for (let i = 0; i < this.cSld.spTree.length; ++i) {
      collectAllFontNamesForSlideElement(this.cSld.spTree[i], fonts)
    }
  }

  scale(kw, kh) {
    const spTree = this.cSld.spTree
    for (let i = 0; i < spTree.length; ++i) {
      changeSlideElementSize(spTree[i], kw, kh)
    }
  }
}
