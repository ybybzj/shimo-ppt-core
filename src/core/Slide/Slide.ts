import { Dict, Nullable } from '../../../liber/pervasive'
import { isBool, isNumber, isDict } from '../../common/utils'
import { idGenerator } from '../../globals/IdGenerator'
import { gRefIdCounter, RefIdPrefixes } from '../../globals/RefIdCounter'
import { EditActionFlag } from '../EditActionFlag'
import { InstanceType } from '../instanceTypes'
import { getCanvasDrawer, CanvasDrawer } from '../graphic/CanvasDrawer'
import { Factor_mm_to_pix } from '../common/const/unit'
import { EditorSettings } from '../common/EditorSettings'
import { hookManager, Hooks } from '../hooks'
import { SlideComments } from './Comments/SlideComments'
import { Notes } from './Notes'
import { Presentation } from './Presentation'
import { SlideLayout } from './SlideLayout'
import { SlideMaster } from './SlideMaster'
import { SlideTransition } from './SlideTransition'
import { isPlaceholder } from '../utilities/shape/asserts'
import {
  collectAllFontNamesForSlideElement,
  getTextDocumentOfSp,
  getHierarchySps,
  getUniNvPr,
} from '../utilities/shape/getters'
import { changeSlideElementSize } from '../utilities/shape/operations'
import {
  onSlideElemntRemoved,
  setParentForSlideElement,
} from '../utilities/shape/updates'
import { checkExtentsByTextContent } from '../utilities/shape/extents'
import { GraphicsBoundsChecker } from '../graphic/Bounds'
import { Shape } from '../SlideElement/Shape'
import { SlideAnimation } from './Animation'
import { ShapeSizeUpdateInfo } from '../../re/export/Slide'
import { SlideElement } from '../SlideElement/type'
import { renderSlide } from '../render/slide'
import { renderShape } from '../render/shape'
import { CSld } from './CSld'
import { ClrMap, gDefaultColorMap } from '../color/clrMap'
import { FillEffects } from '../SlideElement/attrs/fill/fill'
import { informEntityChildrenChange } from '../calculation/informChanges/contentChange'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { informReplaceChange } from '../calculation/informChanges/replace'
import { onModifyTheme } from '../calculation/updateCalcStatus/onModifyAttributes'
import { cloneSlideElement } from '../utilities/shape/clone'
import { drawSelectionForTextDocument } from '../utilities/selection/draw'
import { ensureSpPrXfrm } from '../utilities/shape/attrs'
import { Bg } from '../SlideElement/attrs/bg'
import {
  CalcStatus,
  initCalcStatusCodeForSlide,
  SlideCalcStatusCode,
  SlideCalcStatusFlags,
} from '../calculation/updateCalcStatus/calcStatus'
import { updateCalcStatusForSlide } from '../calculation/updateCalcStatus/slide'
import { setTextCalcStatus } from '../calculation/updateCalcStatus/utils'
import { cloneSpWithFormatting } from '../utilities/shape/formatting'
import { resetConnectionShapesId } from '../utilities/helpers'
import { PPT_Default_SIZE } from '../common/const/drawing'

export class Slide {
  readonly instanceType = InstanceType.Slide
  presentation: Presentation
  cSld: CSld
  clrMap?: ClrMap
  show = true
  showMasterPhAnim = false
  showMasterSp?: boolean
  backgroundFill: Nullable<FillEffects>
  notes: Notes | null
  timing: SlideTransition
  animation: SlideAnimation
  calcStatusCode!: SlideCalcStatusCode
  width: number
  height: number
  id: string
  shapeOfNotes: Nullable<Shape>
  notesWidth: number
  slideComments: SlideComments | null
  base64Img: any
  index?: number
  layout?: SlideLayout
  master!: SlideMaster
  refId?: string
  isDeleted = false
  isLoading?: boolean
  constructor(presentation: Presentation, slideLayout, slideIndex) {
    this.presentation = presentation
    this.cSld = new CSld(this)
    this.clrMap = undefined
    this.show = true
    this.showMasterPhAnim = false
    this.showMasterSp = undefined
    this.backgroundFill = null
    this.notes = null
    this.slideComments = null
    this.timing = new SlideTransition()
    this.timing.reset()

    this.animation = new SlideAnimation(this)

    this.width = presentation.width ?? PPT_Default_SIZE.width
    this.height = presentation.height ?? PPT_Default_SIZE.height

    this.id = idGenerator.newId()
    this.shapeOfNotes = null
    this.notesWidth = -10.0

    if (slideLayout) {
      this.setLayout(slideLayout)
    }
    if (typeof slideIndex === 'number') {
      this.setIndex(slideIndex)
    }
    this.resetCalcStatus()
  }

  resetCalcStatus() {
    this.calcStatusCode = initCalcStatusCodeForSlide as SlideCalcStatusCode
  }

  setCalcStatusOf(flag: SlideCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode | flag) as SlideCalcStatusCode
  }

  unsetCalcStatusOf(flag: SlideCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode ^ flag) as SlideCalcStatusCode
  }

  isCalcStatusSet(flag: SlideCalcStatusFlags) {
    return (this.calcStatusCode & flag) > 0
  }

  getRefId(): string {
    const refId = this.refId ?? gRefIdCounter.newIDBy(RefIdPrefixes.Slide)
    this.refId = refId
    return refId
  }

  setRefId(rId: string) {
    this.refId = rId
  }

  clone(isSourceFormatting?: boolean) {
    const refsMap: Dict<string> = {}
    const newSlide = new Slide(this.presentation, this.layout, 0)
    if (typeof this.cSld.name === 'string' && this.cSld.name.length > 0) {
      newSlide.setCSldName(this.cSld.name)
    }
    if (this.cSld.bg) {
      newSlide.changeBackground(this.cSld.bg.clone())
    }

    const originIdMap: Dict = {}
    const copies: SlideElement[] = []
    for (let i = 0; i < this.cSld.spTree.length; ++i) {
      const curSp = this.cSld.spTree[i]

      const copySp = isSourceFormatting
        ? cloneSpWithFormatting(curSp, originIdMap)
        : cloneSlideElement(curSp, originIdMap)
      if (copySp) {
        copySp.spTreeParent = newSlide.cSld
        originIdMap[curSp.id] = copySp.id
        copies.push(copySp)
      }

      refsMap[curSp.getRefId()] = copySp.getRefId()

      newSlide.addSlideElementAt(newSlide.cSld.spTree.length, copySp)
      setParentForSlideElement(
        newSlide.cSld.spTree[newSlide.cSld.spTree.length - 1],
        newSlide
      )
    }
    resetConnectionShapesId(copies, originIdMap)

    if (this.clrMap) {
      newSlide.setClrMapOverride(this.clrMap.clone())
    }
    if (isBool(this.show)) {
      newSlide.setShow(this.show)
    }
    if (isBool(this.showMasterPhAnim)) {
      newSlide.setShowPhAnim(this.showMasterPhAnim)
    }
    if (isBool(this.showMasterSp)) {
      newSlide.setShowMasterSp(this.showMasterSp)
    }

    newSlide.applyTiming(this.timing.clone())
    newSlide.setSlideSize(this.width, this.height)

    newSlide.animation = this.animation.clone(newSlide, (timeNodes) => {
      for (const node of timeNodes) {
        const updatedRef = refsMap[node.spRefId]
        if (updatedRef != null) {
          node.spRefId = updatedRef
        }
      }
    })

    if (this.notes) {
      newSlide.setNotes(this.notes.clone())
    }

    if (
      !this.isCalcStatusSet(CalcStatus.Slide.Background) &&
      !this.isCalcStatusSet(CalcStatus.Slide.SpTree)
    ) {
      newSlide.base64Img = this.getBase64Image()
    }

    return newSlide
  }

  udpateIndex(index) {
    this.index = index
  }

  invalidTextCalcStatus() {
    this.setCalcStatusOf(CalcStatus.Slide.SpTree)
    for (let i = 0; i < this.cSld.spTree.length; ++i) {
      const sp = this.cSld.spTree[i]
      setTextCalcStatus(sp)
    }
  }

  removeComment(uid) {
    if (isDict(this.slideComments)) {
      this.slideComments.removeComment(uid)
    }
  }

  setNotes(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideSetNotes,
      this.notes,
      pr
    )
    this.notes = pr
    updateCalcStatusForSlide(this, EditActionFlag.edit_SlideSetNotes)
  }

  getSlideComments(): Nullable<SlideComments> {
    return this.slideComments
  }

  /** 由于协作粒度在 comment, 因此应保证只有新增评论导致新增 slideComment 时调用 */
  setSlideComments(slideComments: SlideComments) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Slide_SetComments,
      this.slideComments,
      slideComments
    )
    this.slideComments = slideComments
    updateCalcStatusForSlide(this, EditActionFlag.edit_Slide_SetComments)
  }

  setShow(isShow) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Slide_SetShow,
      this.show,
      isShow
    )
    this.show = isShow
    updateCalcStatusForSlide(this, EditActionFlag.edit_Slide_SetShow)
  }

  setShowPhAnim(isShow) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideSetShowPhAnim,
      this.showMasterPhAnim,
      isShow
    )
    this.showMasterPhAnim = isShow
    updateCalcStatusForSlide(this, EditActionFlag.edit_SlideSetShowPhAnim)
  }

  setShowMasterSp(isShow) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideSetShowMasterSp,
      this.showMasterSp,
      isShow
    )
    this.showMasterSp = isShow
    updateCalcStatusForSlide(this, EditActionFlag.edit_SlideSetShowMasterSp)
  }

  setLayout(layout) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Slide_SetLayout,
      this.layout,
      layout
    )
    this.layout = layout
    updateCalcStatusForSlide(this, EditActionFlag.edit_Slide_SetLayout)
  }

  setIndex(index) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideSetIndex,
      this.index,
      index
    )
    this.index = index
    updateCalcStatusForSlide(this, EditActionFlag.edit_SlideSetIndex)
  }

  applyTiming(timing: SlideTransition) {
    const oldTiming = this.timing.clone()
    this.timing.applyProps(timing)
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Slide_SetTiming,
      oldTiming,
      this.timing.clone()
    )
    updateCalcStatusForSlide(this, EditActionFlag.edit_Slide_SetTiming)
  }

  setSlideSize(w, h) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideSetSize,
      { w: this.width, h: this.height },
      { w, h }
    )
    this.width = w
    this.height = h
    updateCalcStatusForSlide(this, EditActionFlag.edit_SlideSetSize)
  }

  changeBackground(bg: Nullable<Bg>) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Slide_SetBg,
      this.cSld.bg,
      bg
    )
    this.cSld.bg = bg
    updateCalcStatusForSlide(this, EditActionFlag.edit_Slide_SetBg)
  }

  addSlideElementAt(index: Nullable<number>, sp: SlideElement) {
    index =
      isNumber(index) && index > -1 && index <= this.cSld.spTree.length
        ? index
        : this.cSld.spTree.length
    informEntityChildrenChange(
      this,
      EditActionFlag.edit_Slide_AddToSpTree,
      index,
      [sp],
      true
    )
    this.cSld.addSpAtIndex(sp, index)
    updateCalcStatusForSlide(this, EditActionFlag.edit_Slide_AddToSpTree)
  }

  isVisible() {
    return this.show !== false
  }

  removeFromSpTreeAtIndex(index: number) {
    if (index > -1 && index < this.cSld.spTree.length) {
      const sp = this.cSld.spTree[index]
      informEntityChildrenChange(
        this,
        EditActionFlag.edit_Slide_RemoveFromSpTree,
        index,
        [this.cSld.spTree[index]],
        false
      )
      this.cSld.removeSpFromIndex(index)
      const refId = sp.getRefId()
      hookManager.invoke(Hooks.Animation.OnRemoveShape, { slide: this, refId })
      updateCalcStatusForSlide(this, EditActionFlag.edit_Slide_RemoveFromSpTree)
    }
  }

  replaceFromSpTreeById(id, sp) {
    const spTree = this.cSld.spTree
    for (let i = 0; i < spTree.length; ++i) {
      const repalcedSp = spTree[i]
      if (repalcedSp.getId() === id) {
        sp.spTreeParent = spTree[i].spTreeParent
        sp.setRefId(repalcedSp.refId)
        sp.setParent(repalcedSp.parent)
        onSlideElemntRemoved(repalcedSp)

        // Todo, clarify actionType
        informReplaceChange(
          sp,
          EditActionFlag.edit_SlideReplaceFromSpTree,
          EditActionFlag.edit_SlideReplaceFromSpTree
        )

        spTree.splice(i, 1)
        this.cSld.addSpAtIndex(sp, i)
        hookManager.invoke(Hooks.Animation.OnReplaceShape, {
          slide: this,
          oldRefId: repalcedSp.getRefId(),
          newRefId: sp.getRefId(),
        })
        updateCalcStatusForSlide(
          this,
          EditActionFlag.edit_SlideReplaceFromSpTree
        )
        return i
      }
    }
  }

  removeSpById(id, notUpdateAnimation?) {
    const spTree = this.cSld.spTree
    for (let i = 0; i < spTree.length; ++i) {
      if (spTree[i].getId() === id) {
        const refId = spTree[i].getRefId()
        onSlideElemntRemoved(spTree[i])

        informEntityChildrenChange(
          this,
          EditActionFlag.edit_Slide_RemoveFromSpTree,
          i,
          [spTree[i]],
          false
        )
        spTree.splice(i, 1)
        if (notUpdateAnimation !== true) {
          hookManager.invoke(Hooks.Animation.OnRemoveShape, {
            slide: this,
            refId,
          })
        }
        updateCalcStatusForSlide(
          this,
          EditActionFlag.edit_Slide_RemoveFromSpTree
        )
        return i
      }
    }
    return null
  }

  replaceFromSpTreeByPlaceholder(phSp: SlideElement, sp: SlideElement) {
    this.replaceFromSpTreeById(phSp.id, sp)

    const nvProps = getUniNvPr(sp)
    if (phSp && nvProps) {
      const ph = getUniNvPr(phSp)?.nvPr?.ph
      if (ph) {
        nvProps.nvPr.setPh(ph.clone())
      }
    }
  }

  addToSpTreeAtIndex(index, obj) {
    this.addSlideElementAt(index, obj)
  }

  setCSldName(name) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideSetCSldName,
      this.cSld.name,
      name
    )
    this.cSld.name = name
    updateCalcStatusForSlide(this, EditActionFlag.edit_SlideSetCSldName)
  }

  setClrMapOverride(clrMap) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideSetClrMapOverride,
      this.clrMap,
      clrMap
    )
    this.clrMap = clrMap
    updateCalcStatusForSlide(this, EditActionFlag.edit_SlideSetClrMapOverride)
  }

  getAllFonts(fonts) {
    for (let i = 0; i < this.cSld.spTree.length; ++i) {
      collectAllFontNamesForSlideElement(this.cSld.spTree[i], fonts)
    }
  }

  updateSize(width, height, contentScaleInfo?: ShapeSizeUpdateInfo) {
    this.setSlideSize(width, height)
    if (contentScaleInfo) {
      const spTree = this.cSld.spTree
      const { scale, deltaX, deltaY } = contentScaleInfo
      for (let i = 0; i < spTree.length; ++i) {
        changeSlideElementSize(spTree[i], scale, scale, deltaX, deltaY)
      }
    }
  }

  getId() {
    return this.id
  }

  getColorMap() {
    if (this.clrMap) {
      return this.clrMap
    } else if (this.layout?.clrMap) {
      return this.layout.clrMap
    } else if (this.layout?.master?.clrMap) {
      return this.layout.master.clrMap
    }
    return gDefaultColorMap
  }

  getNotesShapeHeight() {
    if (!this.shapeOfNotes) {
      return 0
    }

    const textTarget = getTextDocumentOfSp(this.shapeOfNotes)
    if (textTarget) {
      return textTarget.getWholeHeight()
    }
    return 0
  }

  renderComments(graphics: GraphicsBoundsChecker | CanvasDrawer) {
    if (
      EditorSettings.isSupportComment &&
      EditorSettings.isShowComment &&
      this.slideComments
    ) {
      const comments = this.slideComments.getValidComments()
      for (let i = 0; i < comments.length; ++i) {
        comments[i].draw(graphics)
      }
    }
  }

  renderNotes(g) {
    if (this.shapeOfNotes) {
      renderShape(this.shapeOfNotes, g)
    }
  }

  getTheme() {
    return this.layout?.master?.theme
  }

  drawNotesSelection() {
    if (this.shapeOfNotes) {
      const textDoc = getTextDocumentOfSp(this.shapeOfNotes)
      if (textDoc) {
        hookManager.invoke(Hooks.EditorUI.OnUpdateCursorTransform, {
          transform: this.shapeOfNotes.textTransform,
        })
        drawSelectionForTextDocument(textDoc)
      }
    }
  }

  onUpdateSlide() {
    hookManager.invoke(Hooks.EditorUI.OnCalculateSlide, {
      index: this.index,
    })
  }

  getBase64Image() {
    if (typeof this.base64Img === 'string' && this.base64Img.length > 0) {
      return this.base64Img
    }

    const factor = Factor_mm_to_pix
    const width = (this.width * factor + 0.5) >> 0
    const height = (this.height * factor + 0.5) >> 0

    if (width <= 0 || height <= 0) return null

    const canvas = document.createElement('canvas')
    canvas.width = width
    canvas.height = height

    const ctx = canvas.getContext('2d')!
    const drawer = getCanvasDrawer(ctx, width, height, this.width, this.height)

    drawer.isInPreview = true
    drawer.setCoordTransform(0, 0)
    drawer.updateTransform(1, 0, 0, 1, 0, 0)

    renderSlide(this, drawer)
    drawer.resetState()
    drawer.isInPreview = false

    const ret = { imageCanvas: canvas, imageUrl: '' }
    try {
      ret.imageUrl = canvas.toDataURL('image/png')
    } catch (err) {
      ret.imageUrl = ''
    }
    return ret.imageUrl
  }

  checkPlaceholderXfrm() {
    const spTree = this.cSld.spTree
    for (let i = 0; i < spTree.length; ++i) {
      const sp = spTree[i]
      if (
        sp.instanceType === InstanceType.Shape ||
        sp.instanceType === InstanceType.ImageShape
      ) {
        if (isPlaceholder(sp)) {
          const hsps = getHierarchySps(sp)
          let j
          for (j = 0; j < hsps.length; ++j) {
            if (isDict(hsps[j])) break
          }
          if (j === hsps.length) {
            ensureSpPrXfrm(sp, true)
          }
        } else if (sp.instanceType === InstanceType.Shape) {
          onModifyTheme(sp)
          checkExtentsByTextContent(sp)
        }
      } else {
        if (sp.instanceType === InstanceType.GroupShape) {
          onModifyTheme(sp)
          checkExtentsByTextContent(sp)
        }
      }
    }
  }

  getSpTree() {
    return this.cSld.spTree
  }

  isEmpty() {
    const spTree = this.getSpTree()
    return spTree.length === 0
  }

  removeNotes() {
    if (this.notes) {
      this.notes.slide = null
      this.notes = null
    }
  }
}
