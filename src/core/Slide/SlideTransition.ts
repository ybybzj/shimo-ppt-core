import { SlideTransitionProps } from '../../exports/type'
import { InstanceType } from '../instanceTypes'
import {
  SlideTransitionData,
  CT_EightDirectionTransition,
  CT_OptionalBlackTransition,
  CT_SideDirectionTransition,
  CT_SplitTransition,
  SlideTransitionOption,
  ST_TransitionSideDirection_Default,
  ST_TransitionCornerDirection_Default,
  ST_TransitionEightDirection_Default,
  ST_Direction_Default,
  ST_TransitionInOutDirection_Default,
} from '../../io/dataType/transition'
import { assignVal } from '../../io/utils'
import { Nullable } from '../../../liber/pervasive'

export const TransitionTypes = {
  None: 0,
  Fade: 1,
  Push: 2,
  Wipe: 3,
  Split: 4,
  Uncover: 5,
  Cover: 6,
  Clock: 7,
  Zoom: 8,
} as const

export const TransitionOptions = {
  // Fade
  FadeSmoothly: 0,
  FadeThroughBlack: 1,

  // Push, Wipe, Cover, Uncover
  DirectionLeft: 0,
  DirectionTop: 1,
  DirectionRight: 2,
  DirectionBottom: 3,
  DirectionTopLeft: 4,
  DirectionTopRight: 5,
  DirectionBottomLeft: 6,
  DirectionBottomRight: 7,

  SplitVerticalIn: 8,
  SplitVerticalOut: 9,
  SplitHorizontalIn: 10,
  SplitHorizontalOut: 11,

  ClockClockwise: 0,
  ClockCounterclockwise: 1,
  ClockWedge: 2,

  ZoomIn: 0,
  ZoomOut: 1,
  ZoomAndRotate: 2,
} as const

export class SlideTransition {
  readonly instanceType = InstanceType.SlideTransition
  type?: (typeof TransitionTypes)[keyof typeof TransitionTypes]
  option?: -1 | (typeof TransitionOptions)[keyof typeof TransitionOptions]
  duration?: number
  advanceOnMouseClick?: boolean
  advanceAfter?: boolean
  advanceDuration?: number
  constructor() {
    this.type = undefined
    this.option = undefined
    this.duration = undefined

    this.advanceOnMouseClick = undefined
    this.advanceAfter = undefined
    this.advanceDuration = undefined
  }

  applyProps(v: SlideTransitionProps | SlideTransition) {
    if (null != v.type) {
      this.type = v.type
    }
    if (null != v.option) {
      this.option = v.option
    }
    if (null != v.duration) {
      this.duration = v.duration
    }

    if (null != v.advanceOnMouseClick) {
      this.advanceOnMouseClick = v.advanceOnMouseClick
    }
    if (null != v.advanceAfter) {
      this.advanceAfter = v.advanceAfter
    }
    if (null != v.advanceDuration) {
      this.advanceDuration = v.advanceDuration
    }
  }

  clone(_v?) {
    const slideTransition = new SlideTransition()

    slideTransition.type = this.type
    slideTransition.option = this.option
    slideTransition.duration = this.duration
    slideTransition.advanceOnMouseClick = this.advanceOnMouseClick
    slideTransition.advanceAfter = this.advanceAfter
    slideTransition.advanceDuration = this.advanceDuration

    return slideTransition
  }

  formatDuplicate(slideTransition?: SlideTransition) {
    if (!slideTransition) return

    slideTransition.type = this.type
    slideTransition.option = this.option
    slideTransition.duration = this.duration
    slideTransition.advanceOnMouseClick = this.advanceOnMouseClick
    slideTransition.advanceAfter = this.advanceAfter
    slideTransition.advanceDuration = this.advanceDuration
  }

  reset() {
    this.type = TransitionTypes.None
    this.option = -1
    this.duration = 500
    this.advanceOnMouseClick = true
    this.advanceAfter = false
    this.advanceDuration = 3000
  }

  toJSON(): SlideTransitionData {
    const data: SlideTransitionData = {}
    assignVal(data, 'advClick', this.advanceOnMouseClick)
    if (this.advanceAfter) {
      assignVal(data, 'advTm', this.advanceDuration)
    }
    if (this.type !== TransitionTypes.None) {
      const transDuration = this.duration!
      assignVal(data, 'dur', transDuration)
      if (transDuration < 250) {
        assignVal(data, 'spd', 'fast')
      } else if (transDuration > 1000) {
        assignVal(data, 'spd', 'slow')
      } else {
        assignVal(data, 'spd', 'med')
      }

      const transition = toSlideTransitionJSON(this, data)
      assignVal(data, 'transition', transition)
    }
    return data
  }

  fromJSON(data: SlideTransitionData) {
    this.reset()
    if (data['advClick'] != null) {
      this.advanceOnMouseClick = data['advClick']
    }

    if (data['advTm'] != null) {
      this.advanceAfter = true
      this.advanceDuration = data['advTm']
    }

    if (data['dur'] != null) {
      this.duration = data['dur']
    } else if (data['spd'] != null) {
      switch (data['spd']) {
        case 'fast':
          this.duration = 250
          break
        case 'med':
          this.duration = 500
          break
        case 'slow':
          this.duration = 750
          break
      }
    }

    fromSlideTransitionJSON(this, data)
  }
}

export function compareTiming(
  timing1: Nullable<SlideTransition>,
  timing2: Nullable<SlideTransition>
) {
  if (!timing1 || !timing2) {
    return null
  }
  const ret = new SlideTransition()
  if (timing1.type === timing2.type) {
    ret.type = timing1.type
  }
  if (timing1.type === timing2.type) {
    ret.option = timing1.type
  }
  if (timing1.duration === timing2.duration) {
    ret.duration = timing1.duration
  }
  if (timing1.advanceOnMouseClick === timing2.advanceOnMouseClick) {
    ret.advanceOnMouseClick = timing1.advanceOnMouseClick
  }
  if (timing1.advanceAfter === timing2.advanceAfter) {
    ret.advanceAfter = timing1.advanceAfter
  }
  if (timing1.advanceDuration === timing2.advanceDuration) {
    ret.advanceDuration = timing1.advanceDuration
  }
  return ret
}

// helpers
function toSlideTransitionJSON(
  transition: SlideTransition,
  data: SlideTransitionData
): SlideTransitionOption | undefined {
  switch (transition.type) {
    case TransitionTypes.Fade: {
      const option: CT_OptionalBlackTransition = {
        ['kind']: 'fade',
      }
      switch (transition.option) {
        case TransitionOptions.FadeSmoothly: {
          option['thruBlk'] = false
          break
        }
        case TransitionOptions.FadeThroughBlack: {
          option['thruBlk'] = true
          break
        }
        default:
          break
      }
      return option
    }
    case TransitionTypes.Push: {
      const option: CT_SideDirectionTransition = {
        ['kind']: 'push',
      }
      switch (transition.option) {
        case TransitionOptions.DirectionLeft: {
          assignVal(option, 'dir', 'r')
          break
        }
        case TransitionOptions.DirectionRight: {
          assignVal(option, 'dir', 'l')
          break
        }
        case TransitionOptions.DirectionTop: {
          assignVal(option, 'dir', 'd')
          break
        }
        case TransitionOptions.DirectionBottom: {
          assignVal(option, 'dir', 'u')
          break
        }
        default:
          break
      }
      return option
    }
    case TransitionTypes.Wipe: {
      let transKind, dir
      switch (transition.option) {
        case TransitionOptions.DirectionLeft: {
          transKind = 'wipe'
          dir = 'r'
          break
        }
        case TransitionOptions.DirectionRight: {
          transKind = 'wipe'
          dir = 'l'
          break
        }
        case TransitionOptions.DirectionTop: {
          transKind = 'wipe'
          dir = 'd'
          break
        }
        case TransitionOptions.DirectionBottom: {
          transKind = 'wipe'
          dir = 'u'
          break
        }
        case TransitionOptions.DirectionTopLeft: {
          transKind = 'strips'
          dir = 'rd'
          break
        }
        case TransitionOptions.DirectionTopRight: {
          transKind = 'strips'
          dir = 'ld'
          break
        }
        case TransitionOptions.DirectionBottomLeft: {
          transKind = 'strips'
          dir = 'ru'
          break
        }
        case TransitionOptions.DirectionBottomRight: {
          transKind = 'strips'
          dir = 'lu'
          break
        }
        default:
          break
      }

      if (transKind) {
        const option = {
          ['kind']: transKind,
        }
        if (dir) {
          option['dir'] = dir
        }
        return option
      }
      break
    }
    case TransitionTypes.Split: {
      const option: CT_SplitTransition = {
        ['kind']: 'split',
      }
      switch (transition.option) {
        case TransitionOptions.SplitHorizontalIn: {
          option['orient'] = 'horz'
          option['dir'] = 'in'
          break
        }
        case TransitionOptions.SplitHorizontalOut: {
          option['orient'] = 'horz'
          option['dir'] = 'out'
          break
        }
        case TransitionOptions.SplitVerticalIn: {
          option['orient'] = 'vert'
          option['dir'] = 'in'
          break
        }
        case TransitionOptions.SplitVerticalOut: {
          option['orient'] = 'vert'
          option['dir'] = 'out'
          break
        }
        default:
          break
      }
      return option
    }
    case TransitionTypes.Uncover:
    case TransitionTypes.Cover: {
      const option: CT_EightDirectionTransition = {
        ['kind']: transition.type === TransitionTypes.Cover ? 'cover' : 'pull',
      }

      switch (transition.option) {
        case TransitionOptions.DirectionLeft: {
          option['dir'] = 'r'
          break
        }
        case TransitionOptions.DirectionRight: {
          option['dir'] = 'l'
          break
        }
        case TransitionOptions.DirectionTop: {
          option['dir'] = 'd'
          break
        }
        case TransitionOptions.DirectionBottom: {
          option['dir'] = 'u'
          break
        }
        case TransitionOptions.DirectionTopLeft: {
          option['dir'] = 'rd'
          break
        }
        case TransitionOptions.DirectionTopRight: {
          option['dir'] = 'ld'
          break
        }
        case TransitionOptions.DirectionBottomLeft: {
          option['dir'] = 'ru'
          break
        }
        case TransitionOptions.DirectionBottomRight: {
          option['dir'] = 'lu'
          break
        }
        default:
          break
      }
      return option
    }
    case TransitionTypes.Clock: {
      switch (transition.option) {
        case TransitionOptions.ClockClockwise: {
          return {
            ['kind']: 'wheel',
            ['spokes']: 1,
          }
        }
        case TransitionOptions.ClockCounterclockwise: {
          assignVal(data, 'requires', 'p14')
          assignVal(data, 'dur', transition.duration)
          return {
            ['kind']: 'wheelReverse',
            ['spokes']: 1,
          }
        }
        case TransitionOptions.ClockWedge: {
          return {
            ['kind']: 'wedge',
          }
        }
        default:
          break
      }
      break
    }
    case TransitionTypes.Zoom: {
      switch (transition.option) {
        case TransitionOptions.ZoomIn: {
          assignVal(data, 'requires', 'p14')
          assignVal(data, 'dur', transition.duration)
          return {
            ['kind']: 'warp',
            ['dir']: 'in',
          }
        }
        case TransitionOptions.ZoomOut: {
          assignVal(data, 'requires', 'p14')
          assignVal(data, 'dur', transition.duration)
          return {
            ['kind']: 'warp',
            ['dir']: 'out',
          }
        }
        case TransitionOptions.ZoomAndRotate: {
          return {
            ['kind']: 'newsflash',
          }
        }
        default:
          break
      }
      break
    }
    default:
      break
  }
}

function fromSlideTransitionJSON(
  transition: SlideTransition,
  data: SlideTransitionData
) {
  const requires = data['requires']
  const option: SlideTransitionOption = data[
    'transition'
  ] as SlideTransitionOption
  if (option != null) {
    switch (option['kind']) {
      case 'fade': {
        transition.type = TransitionTypes.Fade

        if (option['thruBlk'] === true) {
          transition.option = TransitionOptions.FadeThroughBlack
        } else {
          transition.option = TransitionOptions.FadeSmoothly
        }
        break
      }
      case 'push': {
        transition.type = TransitionTypes.Push
        const dir = option['dir'] ?? ST_TransitionSideDirection_Default

        if ('l' === dir) {
          transition.option = TransitionOptions.DirectionRight
        }
        if ('r' === dir) {
          transition.option = TransitionOptions.DirectionLeft
        }
        if ('d' === dir) {
          transition.option = TransitionOptions.DirectionTop
        }
        if (dir === 'u') {
          transition.option = TransitionOptions.DirectionBottom
        }
        break
      }

      case 'wipe': {
        transition.type = TransitionTypes.Wipe

        const dir = option['dir'] ?? ST_TransitionSideDirection_Default
        if ('u' === dir) {
          transition.option = TransitionOptions.DirectionBottom
        }
        if ('r' === dir) {
          transition.option = TransitionOptions.DirectionLeft
        }
        if ('d' === dir) {
          transition.option = TransitionOptions.DirectionTop
        }
        if (dir === 'l') {
          transition.option = TransitionOptions.DirectionRight
        }
        break
      }

      case 'strips': {
        transition.type = TransitionTypes.Wipe
        const dir = option['dir'] ?? ST_TransitionCornerDirection_Default

        if ('rd' === dir) {
          transition.option = TransitionOptions.DirectionTopLeft
        }
        if ('ru' === dir) {
          transition.option = TransitionOptions.DirectionBottomLeft
        }
        if ('lu' === dir) {
          transition.option = TransitionOptions.DirectionBottomRight
        }
        if (dir === 'ld') {
          transition.option = TransitionOptions.DirectionTopRight
        }
        break
      }
      case 'cover': {
        transition.type = TransitionTypes.Cover
        const dir = option['dir'] ?? ST_TransitionEightDirection_Default

        if (dir === 'l') {
          transition.option = TransitionOptions.DirectionRight
        }

        if ('u' === dir) {
          transition.option = TransitionOptions.DirectionBottom
        }
        if ('r' === dir) {
          transition.option = TransitionOptions.DirectionLeft
        }
        if ('d' === dir) {
          transition.option = TransitionOptions.DirectionTop
        }
        if ('rd' === dir) {
          transition.option = TransitionOptions.DirectionTopLeft
        }
        if ('ru' === dir) {
          transition.option = TransitionOptions.DirectionBottomLeft
        }
        if ('lu' === dir) {
          transition.option = TransitionOptions.DirectionBottomRight
        }
        if ('ld' === dir) {
          transition.option = TransitionOptions.DirectionTopRight
        }

        break
      }
      case 'pull': {
        transition.type = TransitionTypes.Uncover
        const dir = option['dir'] ?? ST_TransitionEightDirection_Default

        if (dir === 'l') {
          transition.option = TransitionOptions.DirectionRight
        }

        if ('u' === dir) {
          transition.option = TransitionOptions.DirectionBottom
        }
        if ('r' === dir) {
          transition.option = TransitionOptions.DirectionLeft
        }
        if ('d' === dir) {
          transition.option = TransitionOptions.DirectionTop
        }
        if ('rd' === dir) {
          transition.option = TransitionOptions.DirectionTopLeft
        }
        if ('ru' === dir) {
          transition.option = TransitionOptions.DirectionBottomLeft
        }
        if ('lu' === dir) {
          transition.option = TransitionOptions.DirectionBottomRight
        }
        if ('ld' === dir) {
          transition.option = TransitionOptions.DirectionTopRight
        }

        break
      }
      case 'split': {
        transition.type = TransitionTypes.Split

        const orient = option['orient'] ?? ST_Direction_Default

        const isVert = orient === 'vert' ? true : false

        const dir = option['dir'] ?? ST_TransitionInOutDirection_Default

        const isOut = dir === 'out' ? true : false

        if (isVert) {
          if (isOut) {
            transition.option = TransitionOptions.SplitVerticalOut
          } else {
            transition.option = TransitionOptions.SplitVerticalIn
          }
        } else {
          if (isOut) {
            transition.option = TransitionOptions.SplitHorizontalOut
          } else {
            transition.option = TransitionOptions.SplitHorizontalIn
          }
        }
        break
      }
      case 'wheel': {
        transition.type = TransitionTypes.Clock
        transition.option = TransitionOptions.ClockClockwise
        break
      }

      case 'wheelReverse': {
        if (requires === 'p14') {
          transition.type = TransitionTypes.Clock
          transition.option = TransitionOptions.ClockCounterclockwise
        }
        break
      }
      case 'wedge': {
        transition.type = TransitionTypes.Clock
        transition.option = TransitionOptions.ClockWedge
        break
      }
      case 'warp': {
        if (requires === 'p14') {
          transition.type = TransitionTypes.Zoom
          transition.option = TransitionOptions.ZoomOut

          if ('in' === option['dir']) {
            transition.option = TransitionOptions.ZoomIn
          }
        }
        break
      }
      case 'newsflash': {
        transition.type = TransitionTypes.Zoom
        transition.option = TransitionOptions.ZoomAndRotate
        break
      }

      default: {
        transition.type = TransitionTypes.Fade
        transition.option = TransitionOptions.FadeSmoothly
      }
    }
  }
}
