import { LayoutType } from '../SlideElement/const'
import { EditActionFlag } from '../EditActionFlag'
import { Nullable } from '../../../liber/pervasive'
import { idGenerator } from '../../globals/IdGenerator'
import { isDict } from '../../common/utils'
import { SlideLayout } from './SlideLayout'
import { gRefIdCounter, RefIdPrefixes } from '../../globals/RefIdCounter'
import { TextStyles } from '../textAttributes/TextStyles'
import { InstanceType } from '../instanceTypes'
import { Slide } from './Slide'
import { Presentation } from './Presentation'
import { isPlaceholder } from '../utilities/shape/asserts'
import { changeSlideElementSize } from '../utilities/shape/operations'
import { setParentForSlideElement } from '../utilities/shape/updates'
import { CanvasDrawer } from '../graphic/CanvasDrawer'
import { translator } from '../../globals/translator'
import { ShapeSizeUpdateInfo } from '../../re/export/Slide'
import { SlideTransition } from './SlideTransition'
import { Bounds, GraphicBounds } from '../graphic/Bounds'
import { drawSlideElement } from '../render/drawSlideElement'
import { CSld } from './CSld'
import { updatePresentationFields } from '../utilities/fields'
import { ClrMap } from '../color/clrMap'
import { HF } from '../SlideElement/attrs/hf'
import { Theme } from '../SlideElement/attrs/theme'
import { informEntityChildrenChange } from '../calculation/informChanges/contentChange'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { cloneSlideElement } from '../utilities/shape/clone'
import { calculateForSlideMaster } from '../calculation/calculate/slideMaster'
import { collectAllFontNamesForSlideElement } from '../utilities/shape/getters'
import { checkTextFitForTextListStyle } from '../utilities/shape/calcs'
import {
  initCalcStatusCodeForSlideMaster,
  SlideMasterCalcStatusCode,
  SlideMasterCalcStatusFlags,
} from '../calculation/updateCalcStatus/calcStatus'
import { SlideElement } from '../SlideElement/type'
import { PPT_Default_SIZE } from '../common/const/drawing'

export class SlideMaster {
  readonly instanceType = InstanceType.SlideMaster
  cSld: CSld
  clrMap: ClrMap
  hf: HF
  layouts: SlideLayout[]
  txStyles?: TextStyles
  preserve: boolean
  imgBase64: string
  theme: Nullable<Theme>
  width: number
  height: number
  calcStatusCode!: SlideMasterCalcStatusCode
  bounds: GraphicBounds
  presentation: Presentation
  id: string
  refId?: string
  lastCalcSlideIndex: number
  themeId?: string
  transition?: SlideTransition
  constructor(presentation: Presentation, theme?: Theme) {
    this.cSld = new CSld(this)
    this.clrMap = new ClrMap()
    this.hf = new HF()
    this.layouts = []
    this.txStyles = undefined
    this.preserve = false
    this.imgBase64 = ''

    this.width = presentation.width ?? PPT_Default_SIZE.width
    this.height = presentation.height ?? PPT_Default_SIZE.height

    this.bounds = new GraphicBounds(0, 0, this.width, this.height)
    this.presentation = presentation
    this.theme = theme
    this.resetCalcStatus()

    this.lastCalcSlideIndex = -1
    this.id = idGenerator.newId()
  }
  resetCalcStatus() {
    this.calcStatusCode =
      initCalcStatusCodeForSlideMaster as SlideMasterCalcStatusCode
  }

  setCalcStatusOf(flag: SlideMasterCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode |
      flag) as SlideMasterCalcStatusCode
  }

  unsetCalcStatusOf(flag: SlideMasterCalcStatusFlags) {
    this.calcStatusCode = (this.calcStatusCode ^
      flag) as SlideMasterCalcStatusCode
  }

  isCalcStatusSet(flag: SlideMasterCalcStatusFlags) {
    return (this.calcStatusCode & flag) > 0
  }

  getRefId(): string {
    const refId = this.refId ?? gRefIdCounter.newIDBy(RefIdPrefixes.SlideMaster)
    this.refId = refId
    return refId
  }

  setRefId(rId: string) {
    this.refId = rId
  }

  addLayout(layout) {
    this.addToSldLayoutLstAtIndex(this.layouts.length, layout)
  }

  draw(graphics: CanvasDrawer, slide?: Slide, viewBounds?: Bounds) {
    if (slide) {
      if (slide.index !== this.lastCalcSlideIndex) {
        this.lastCalcSlideIndex = slide.index!
        updatePresentationFields(this)
        calculateForSlideMaster(this)
      }
    }

    for (let i = 0; i < this.cSld.spTree.length; ++i) {
      const sp = this.cSld.spTree[i]
      if (!isPlaceholder(sp)) {
        drawSlideElement(sp, graphics, undefined, viewBounds)
      }
    }
  }

  getMatchLayout(type, matchingName, cSldName, useTheme?) {
    let layoutType = type
    const layoutName = matchingName || cSldName

    if (type === LayoutType.Title && !(useTheme === true)) {
      layoutType = LayoutType.Obj
    }
    let matchedLayout = getLayoutByTypeAndName(
      layoutType,
      layoutName,
      this.layouts
    )
    if (!matchedLayout && type === LayoutType.Title && !(useTheme === true)) {
      layoutType = LayoutType.Tx
      matchedLayout = getLayoutByTypeAndName(
        layoutType,
        layoutName,
        this.layouts
      )
    }
    if (matchedLayout) {
      return matchedLayout
    }

    const matchedLayouts: Array<SlideLayout> | null =
      (layoutName && getLayoutsByName(this.layouts, layoutName)) ??
      getLayoutsByType(this.layouts, layoutType, true) ??
      getLayoutsByType(this.layouts, LayoutType.Blank, true) ??
      getLayoutsByName(this.layouts, '空白') ??
      getLayoutsByName(this.layouts, 'Blank') ??
      getLayoutsByType(this.layouts, LayoutType.Title, false)
    if (matchedLayouts?.length) {
      return matchedLayouts[0]
    }

    return this.layouts[0]
  }

  setSlideSize(w, h) {
    this.width = w
    this.height = h
  }

  private scaleTxStyles(kw: number) {
    let modified = false
    if (this.txStyles?.bodyStyle) {
      checkTextFitForTextListStyle(this.txStyles.bodyStyle, kw)
      modified = true
    }
    if (this.txStyles?.titleStyle) {
      checkTextFitForTextListStyle(this.txStyles.titleStyle, kw)
      modified = true
    }
    if (this.txStyles?.otherStyle) {
      checkTextFitForTextListStyle(this.txStyles.otherStyle, kw)
      modified = true
    }
    if (modified === true) {
      informEntityPropertyChange(
        this,
        EditActionFlag.edit_SlideMasterSetTxStyles,
        this.txStyles,
        this.txStyles
      )
    }
  }

  updateSize(width, height, contentScaleInfo?: ShapeSizeUpdateInfo) {
    this.setSlideSize(width, height)
    if (contentScaleInfo) {
      const { scale, deltaX, deltaY } = contentScaleInfo
      this.scaleTxStyles(scale)
      const spTree = this.cSld.spTree
      for (let i = 0; i < spTree.length; ++i) {
        changeSlideElementSize(spTree[i], scale, scale, deltaX, deltaY)
      }
    }
  }

  applyTransition(transition?: SlideTransition) {
    if (this.transition == null && transition == null) {
      return
    }
    const orgTransition = this.transition?.clone()
    if (this.transition == null && transition != null) {
      this.transition = new SlideTransition()
      this.transition.reset()
    }

    if (transition != null) {
      this.transition?.applyProps(transition)
    } else {
      this.transition = undefined
    }

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideMaster_SetTransition,
      orgTransition,
      this.transition?.clone()
    )
  }

  applyTransitionToAllLayouts() {
    const transition = this.transition?.clone()
    this.layouts.forEach((layout) => {
      layout.applyTransition(transition)
    })
  }

  setTheme(theme) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideMasterSetTheme,
      this.theme,
      theme
    )
    this.theme = theme
  }

  addSlideElementAt(index, sp) {
    informEntityChildrenChange(
      this,
      EditActionFlag.edit_SlideMasterAddToSpTree,
      index,
      [sp],
      true
    )
    this.cSld.addSpAtIndex(sp, index)
  }

  changeBackground(bg) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideMasterSetBg,
      this.cSld.bg,
      bg
    )
    this.cSld.bg = bg
  }

  setTextStyles(txStyles) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideMasterSetTxStyles,
      this.txStyles,
      txStyles
    )
    this.txStyles = txStyles
  }

  setCSldName(name) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideMasterSetCSldName,
      this.cSld.name,
      name
    )
    this.cSld.name = name
  }

  setClrMapOverride(clrMap) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_SlideMasterSetClrMapOverride,
      this.clrMap,
      clrMap
    )
    this.clrMap = clrMap
  }

  addToSldLayoutLstAtIndex(index, obj) {
    informEntityChildrenChange(
      this,
      EditActionFlag.edit_SlideMasterAddLayout,
      index,
      [obj],
      true
    )
    this.layouts.splice(index, 0, obj)
  }

  getId() {
    return this.id
  }

  getAllFonts(fonts) {
    let i
    if (this.theme) {
      this.theme.collectAllFontNames(fonts)
    }

    if (this.txStyles) {
      this.txStyles.collectAllFontNames(fonts)
    }

    for (i = 0; i < this.layouts.length; ++i) {
      this.layouts[i].getAllFonts(fonts)
    }

    for (i = 0; i < this.cSld.spTree.length; ++i) {
      collectAllFontNamesForSlideElement(this.cSld.spTree[i], fonts)
    }
  }

  clone(map) {
    map = map || {}
    const slideMaster = new SlideMaster(this.presentation)
    let i

    if (this.clrMap) {
      slideMaster.setClrMapOverride(this.clrMap.clone())
    }
    if (typeof this.cSld.name === 'string' && this.cSld.name.length > 0) {
      slideMaster.setCSldName(this.cSld.name)
    }
    if (this.cSld.bg) {
      slideMaster.changeBackground(this.cSld.bg.clone())
    }
    for (i = 0; i < this.cSld.spTree.length; ++i) {
      let copySp: SlideElement

      if (this.cSld.spTree[i].instanceType === InstanceType.GroupShape) {
        copySp = cloneSlideElement(this.cSld.spTree[i], map)
      } else {
        copySp = cloneSlideElement(this.cSld.spTree[i])
      }
      if (isDict(map)) {
        map[this.cSld.spTree[i].id] = copySp.id
      }
      slideMaster.addSlideElementAt(slideMaster.cSld.spTree.length, copySp)
      setParentForSlideElement(
        slideMaster.cSld.spTree[slideMaster.cSld.spTree.length - 1],
        slideMaster
      )
    }
    if (this.txStyles) {
      slideMaster.setTextStyles(this.txStyles.clone())
    }
    return slideMaster
  }

  scale(kw, kh) {
    this.scaleTxStyles(kw)
    const spTree = this.cSld.spTree
    for (let i = 0; i < spTree.length; ++i) {
      changeSlideElementSize(spTree[i], kw, kh)
    }
  }
}
// helpers
function getLayoutsByType(
  layouts,
  type,
  isEqual = true
): Array<SlideLayout> | null {
  const ret = layouts.filter((layout) =>
    isEqual ? type !== undefined && layout.type === type : layout.type !== type
  )
  return ret.length ? ret : null
}

function getLayoutsByName(layouts, name = ''): Array<SlideLayout> | null {
  const translatedName = translator.getValue(name).toLowerCase()
  const ret = layouts.filter((layout) => {
    const layoutName = (layout.matchingName || layout.cSld.name) ?? ''
    const lowerCaseLayoutName = layoutName.toLowerCase()
    // 尝试匹配翻译后的值
    return (
      lowerCaseLayoutName === name.toLowerCase() ||
      translator.getValue(layoutName).toLowerCase() === translatedName ||
      lowerCaseLayoutName === translatedName
    )
  })
  return ret.length ? ret : null
}

function getLayoutByTypeAndName(layoutType, layoutName, layouts) {
  if (layoutType != null) {
    const matchedLayoutsByType = getLayoutsByType(layouts, layoutType)
    if (matchedLayoutsByType?.length) {
      if (matchedLayoutsByType.length === 1) {
        return matchedLayoutsByType[0]
      } else if (matchedLayoutsByType.length > 1) {
        const matchedLayoutsByName = getLayoutsByName(
          matchedLayoutsByType,
          layoutName
        )
        if (matchedLayoutsByName?.length) {
          return matchedLayoutsByName[0]
        }
      } else {
        return matchedLayoutsByType[0]
      }
    }
  }
}
