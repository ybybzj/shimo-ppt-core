import { Nullable } from '../../../liber/pervasive'
import { isNumber, isDict, toInt, appendArrays } from '../../common/utils'
import { InstanceType } from '../instanceTypes'
import {
  AnimClass,
  AnimTypes,
  EntranceAnimationTypes,
  EmphasisAnimationTypes,
  ExitAnimationTypes,
  SlideAnimationTypes,
  CT_SlideTiming,
  CT_TimeNodeList,
  CT_TLTimeNodeParallel,
  CT_TLTimeNodeSequence,
  CT_TLCommonTimeNodeData,
  ST_TLTimeNodePresetClassType,
  CT_TLAnimateBehavior,
  CT_TLSetBehavior,
  CT_TLAnimateEffectBehavior,
  CT_TLAnimateRotationBehavior,
  CT_TLAnimateScaleBehavior,
  ST_TLTimeNodeType,
  CT_TLTimeTargetElement,
  CT_TimeNode,
  CT_TLCommonBehaviorData,
  CustomAnimTypes,
  FloatInDirection,
  FloatOutDirection,
  CT_TLMediaNodeAudio,
  CT_TLCommandBehavior,
} from '../../io/dataType/animation'
import {
  makeShapeAnimationQueue,
  ShapeAnimationQueue,
  ShapeAnimationUpdateInfo,
  ShapeAnimationNodeType,
} from '../../re/export/ShapeAnimation'
import { SlideElement } from '../SlideElement/type'
import { Presentation } from './Presentation'
import { Slide } from './Slide'
import { AnimationSetting, AudioSetting } from '../../exports/type'
import { EditorUtil } from '../../globals/editor'
import { EditActionFlag } from '../EditActionFlag'
import { assignVal, getValByKeyPath } from '../../io/utils'
import {
  isPlaceholderWithEmptyContent,
  isSlideElement,
} from '../utilities/shape/asserts'
import { EditorSettings } from '../common/EditorSettings'
import { type } from '../../../liber/type'
import { idGenerator } from '../../globals/IdGenerator'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { isAudioImage } from '../utilities/shape/image'

/** 计算 cTn 节点 id 用, tmRoot 和 mainSeq 固定占据 1 和 2, 每个 Slide 独立计数 */
const ROOT_TIME_NODE_ID = 1
const MAIN_SEQ_TIME_NODE_ID = 2

const REPEAT_COUNT_BASE = 1000

const globalTimeNodeInfo = { count: MAIN_SEQ_TIME_NODE_ID }

const getNextGTimeNodeId = () => ++globalTimeNodeInfo.count
const resetGTimeNodeId = () => {
  globalTimeNodeInfo.count = MAIN_SEQ_TIME_NODE_ID
}

const AnimCategoryMapping = {
  visibility: 1, // 0x0001 << 0,
  rot: 2, // 0x0001 << 1,
  alpha: 4, // 0x0001 << 2,
  scale: 8, //0x0001 << 3,
  pos: 16, //0x0001 << 4,
} as const

/** 按照修改属性对动画分类 */
export type AnimCategory =
  (typeof AnimCategoryMapping)[keyof typeof AnimCategoryMapping]
const animCategories = Object.keys(AnimCategoryMapping).map(
  (k) => AnimCategoryMapping[k]
) as AnimCategory[]

export type AnimConfig =
  | {
      type: 'Entrance_Appear'
    }
  | {
      type: 'Entrance_FadeIn'
    }
  | {
      type: 'Entrance_FlyIn'
      param: (typeof ExitAnimationTypes)['flyOut']['subTypes'][keyof (typeof ExitAnimationTypes)['flyOut']['subTypes']]
    }
  | {
      type: 'Entrance_FloatIn'
      param: (typeof FloatInDirection)[keyof typeof FloatInDirection]
    }
  | {
      type: 'Exit_Disappear'
    }
  | {
      type: 'Exit_FadeOut'
    }
  | {
      type: 'Exit_FlyOut'
      param: (typeof ExitAnimationTypes)['flyOut']['subTypes'][keyof (typeof ExitAnimationTypes)['flyOut']['subTypes']]
    }
  | {
      type: 'Exit_FloatOut'
      param: (typeof FloatOutDirection)[keyof typeof FloatOutDirection]
    }
  | {
      type: 'Emphasis_Spin'
      param: { rot: number } // 单位 1/360° (2PI 对应 XML 中 21600000)
    }
  | {
      type: 'Emphasis_GrowShrink'
      param: { sx: number; sy: number } // 单位 1/100% (100%对应 XML 中100000)
    }
  | {
      type: 'Emphasis_Blink'
    }

function getAnimClass(animConfig: AnimConfig): AnimClass {
  switch (animConfig.type) {
    case 'Entrance_Appear':
    case 'Entrance_FadeIn':
    case 'Entrance_FlyIn':
    case 'Entrance_FloatIn':
      return 'Entrance'
    case 'Exit_Disappear':
    case 'Exit_FadeOut':
    case 'Exit_FlyOut':
    case 'Exit_FloatOut':
      return 'Exit'
    case 'Emphasis_Blink':
    case 'Emphasis_Spin':
    case 'Emphasis_GrowShrink':
      return 'Emphasis'
  }
}

function isAnimConfigEqual(c1: AnimConfig, c2: AnimConfig) {
  if (c1 === c2) {
    return true
  }
  switch (c1.type) {
    case 'Entrance_Appear':
    case 'Entrance_FadeIn':
    case 'Exit_Disappear':
    case 'Exit_FadeOut':
    case 'Emphasis_Blink':
      return c1.type === c2.type
    case 'Entrance_FlyIn':
    case 'Entrance_FloatIn':
    case 'Exit_FlyOut':
    case 'Exit_FloatOut':
      return c1.type === c2.type && c1.param === c2.param
    case 'Emphasis_Spin':
      return c1.type === c2.type && c1.param?.rot === c2.param?.rot
    case 'Emphasis_GrowShrink':
      return (
        c1.type === c2.type &&
        c1.param?.sx === c2.param?.sx &&
        c1.param?.sy === c2.param?.sy
      )
    default:
      return false
  }
}

export type AnimationNode = TimeNode | AudioNode

export interface TimeNode {
  spRefId: string
  animConfig: AnimConfig
  delay: number // ms
  duration: number // ms
  repeatCount?: number
  triggerType: ShapeAnimationNodeType
  categories: number
}

export function parseTimeNode(node: TimeNode): TimeNode {
  return {
    spRefId: node['spRefId'],
    animConfig: node['animConfig'],
    delay: node['delay'],
    duration: node['duration'],
    repeatCount: node['repeatCount'],
    triggerType: node['triggerType'],
    categories: node['categories'],
  }
}

export function jsonfyTimeNode(node: TimeNode): TimeNode {
  const {
    spRefId,
    animConfig,
    delay,
    duration,
    repeatCount,
    triggerType,
    categories,
  } = node
  return {
    ['spRefId']: spRefId,
    ['animConfig']: animConfig,
    ['delay']: delay,
    ['duration']: duration,
    ['repeatCount']: repeatCount,
    ['triggerType']: triggerType,
    ['categories']: categories,
  }
}

export interface AudioNode {
  spRefId: string
  /** 本期暂时只支持进入 slide 自动播放(AfterEffect/WithEffect)或者任意点击后播放(ClickEffect) */
  triggerType: ShapeAnimationNodeType // 'in click sequence' (动画队列 clickEffect) | 'automatically'(进入自动播放) | 'when clicked on' (触发器 Trigger)
  /** 从当前页起始生效的页数(各家实现不一样, 这里我们按绝对 index 计算) */
  numSlide?: number
  loopUntilStopped: boolean
  hideWhenShow: boolean // default false, 对应 showWhenStopped
  duration: number // ms
  volume?: number
}
export function parseAudioNode(node: AudioNode): AudioNode {
  return {
    spRefId: node['spRefId'],
    triggerType: node['triggerType'],
    numSlide: node['numSlide'],
    loopUntilStopped: node['loopUntilStopped'],
    hideWhenShow: node['hideWhenShow'],
    duration: node['duration'],
    volume: node['volume'],
  }
}
export function jsonfyAudioNode(node: AudioNode): AudioNode {
  const {
    spRefId,
    triggerType,
    numSlide,
    loopUntilStopped,
    hideWhenShow,
    duration,
    volume,
  } = node

  return {
    ['spRefId']: spRefId,
    ['triggerType']: triggerType,
    ['numSlide']: numSlide,
    ['loopUntilStopped']: loopUntilStopped,
    ['hideWhenShow']: hideWhenShow,
    ['duration']: duration,
    ['volume']: volume,
  }
}
/*
 *  SlideAnimationTypes (xml 标准数据转换过后的用于指比对的常量) <===>  originData (本地存储的导出副本数据)
 *                                                                    ||
 *                                                           (format) || - (format)
 *                                                                |   ||
 *  SlideAnimationOptions (外部选项)=> AnimationSetting (接口格式) ====> TimeNode
 *                                                                    ||
 *                                                                 playNode => animQueue(播放队列)
 */

/** 对应一个 Slide 内的 timing => tnLst, 暂时不包括触发器，存储 originData, 修改数据后覆盖*/
export class SlideAnimation {
  readonly instanceType = InstanceType.SlideAnimation
  timeNodes: TimeNode[]
  audioNodes: AudioNode[]
  originData?: CT_SlideTiming
  slide: Slide
  autoStart: boolean
  id: string
  private animQueue: Nullable<ShapeAnimationQueue>

  constructor(slide: Slide) {
    this.timeNodes = []
    this.audioNodes = []
    this.slide = slide
    this.autoStart = false
    this.id = idGenerator.newId()
  }

  updateTimeNodes(timeNodes: TimeNode[]) {
    if (!EditorSettings.isSupportAnimation) return

    const oldNodes = this.cloneTimeNodes()
    this.updateTimingData(timeNodes, this.audioNodes)
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Slide_SetAnimation,
      oldNodes,
      this.cloneTimeNodes()
    )
  }

  updateAudioNodes(audioNodes: AudioNode[]) {
    if (!EditorSettings.isSupportAnimation) return

    const oldNodes = this.cloneAudioNodes()
    this.updateTimingData(this.timeNodes, audioNodes)
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Slide_SetAnimation,
      oldNodes,
      this.cloneAudioNodes()
    )
  }

  insertNodes(timeNodes: TimeNode[], audioNodes: AudioNode[]) {
    if (!EditorSettings.isSupportAnimation) return
    const newTimeNodes = this.timeNodes.slice(0)
    appendArrays(newTimeNodes, timeNodes)
    this.updateTimeNodes(newTimeNodes)

    if (!EditorSettings.isSupportAudio) {
      return
    }
    const newAudioNodes = this.audioNodes.slice(0)
    appendArrays(newAudioNodes, audioNodes)
    this.updateAudioNodes(newAudioNodes)
  }

  private updateTimingData(timeNodes: TimeNode[], audioNodes: AudioNode[]) {
    this.timeNodes = timeNodes
    this.audioNodes = audioNodes
    this.originData = assignTimingJSON(this.timeNodes, this.audioNodes)
    this.autoStart = calTimeNodesAutoStart(timeNodes)
    this.animQueue = undefined
  }

  /** 存在如 placeholder 填充内容替换为普通 shape , 则需要 */
  onReplaceShape(oldRefId, newRefId) {
    if (oldRefId === newRefId) {
      return
    }

    const spTree = this.slide.cSld.spTree
    const newSp = spTree.find((sp) => sp.getRefId() === newRefId)
    if (!newSp || !canAddAnimation(newSp)) return
    let changeFlag = false
    const newNodes = this.cloneTimeNodes()
    newNodes.forEach((tn) => {
      if (tn.spRefId === oldRefId) {
        tn.spRefId = newRefId
        changeFlag = true
      }
    })

    if (changeFlag) {
      this.updateTimeNodes(newNodes)
    }
  }

  isShapeInAnimation(sp: SlideElement) {
    const nodes = this.timeNodes || []
    let found = false
    for (let i = 0, l = nodes.length; i < l; i++) {
      if (nodes[i].spRefId === sp.refId) {
        found = true
        break
      }
    }
    return found
  }

  getAnimNodesBySp(
    sp: SlideElement
  ): Nullable<{ timeNodes: TimeNode[]; audioNodes: AudioNode[] }> {
    if (this.isEmpty()) return undefined
    const nodes = this.timeNodes
    const timeNodes: TimeNode[] = []
    let found = false
    for (let i = 0, l = nodes.length; i < l; i++) {
      const node = nodes[i]
      if (node.spRefId === sp.refId) {
        found = true
        timeNodes.push(jsonfyTimeNode(node))
      }
    }
    const audioNodes: AudioNode[] = []
    for (const node of this.audioNodes) {
      if (node.spRefId === sp.refId) {
        found = true
        audioNodes.push(jsonfyAudioNode(node))
      }
    }
    return found
      ? { ['timeNodes']: timeNodes, ['audioNodes']: audioNodes }
      : undefined
  }

  onDeleteShape(spRefId) {
    const newTimeNodes = this.cloneTimeNodes().filter(
      (tn) => tn.spRefId !== spRefId
    )
    const newAudioNodes = this.cloneAudioNodes().filter(
      (an) => an.spRefId !== spRefId
    )
    if (newTimeNodes.length < this.timeNodes.length) {
      this.updateTimeNodes(newTimeNodes)
    }
    if (newAudioNodes.length < this.audioNodes.length) {
      this.updateAudioNodes(newAudioNodes)
    }
  }

  reset() {
    this.timeNodes = []
    this.audioNodes = []
    this.originData = undefined
    this.animQueue = undefined
  }

  isEmpty() {
    return this.timeNodes.length <= 0 && this.audioNodes.length <= 0
  }

  cloneTimeNodes() {
    return cloneNodes(this.timeNodes)
  }

  cloneAudioNodes() {
    return cloneNodes(this.audioNodes)
  }

  clone(slide: Slide, adjustTimeNodes?: (nodes: Array<AnimationNode>) => void) {
    const dup = new SlideAnimation(slide)
    dup.originData = this.originData

    if (EditorSettings.isSupportAnimation) {
      const timeNodes = this.cloneTimeNodes()
      const audioNodes = this.cloneAudioNodes()

      if (adjustTimeNodes) {
        adjustTimeNodes(timeNodes)
        adjustTimeNodes(audioNodes)
      }
      dup.updateTimingData(timeNodes, audioNodes)
    }
    return dup
  }

  toJSON(): CT_SlideTiming | undefined {
    resetGTimeNodeId()
    if (this.originData) return this.originData
    return
  }

  fromJSON(slideTiming: CT_SlideTiming): boolean {
    if (EditorSettings.isSupportAnimation) {
      const { timeNodes, audioNodes, needFixAnimation } = readFromTiming(
        this.slide,
        slideTiming
      )
      this.timeNodes = timeNodes
      this.audioNodes = audioNodes
      this.animQueue = undefined
      this.autoStart = calTimeNodesAutoStart(this.timeNodes)
      // 导入时有损转换
      this.originData = assignTimingJSON(this.timeNodes, this.audioNodes)
      // 告知是否需要修复 modoc 数据
      return needFixAnimation
    }
    return false
  }

  getAnimQueue() {
    if (this.animQueue) {
      return this.animQueue
    }

    this.animQueue = makeAnimQueue(this.slide, this.timeNodes)
    this.resetAudioElementStatus()
    return this.animQueue
  }

  /** 初始化/更改音频设置后需要重新设置关联的 shape 属性 */
  resetAudioElementStatus() {
    const audioNodes = this.audioNodes
    if (audioNodes?.length) {
      for (const node of audioNodes) {
        const spTree = this.slide.cSld.spTree
        const sp = spTree.find((sp) => sp.getRefId() === node.spRefId)!
        if (!sp) continue
        sp.hideWhenShow = node.hideWhenShow
      }
    }
  }
}

// ---------------------------- SlideAnimation Helpers ---------------------------- //

/** 目前暂不支持模版占位符添加， 图片占位符(shape) 添加图片后会 replace 为 ImageShape */
export function canAddAnimation(sp: SlideElement) {
  return !isPlaceholderWithEmptyContent(sp)
}

function cloneNodes<T extends AnimationNode>(nodes: T[]): T[] {
  return nodes.map((node) => ({ ...node }))
}

function isTimeNodeEqual(node1: TimeNode, node2: TimeNode) {
  return (
    node1.categories === node2.categories &&
    node1.delay === node2.delay &&
    node1.duration === node2.duration &&
    node1.spRefId === node2.spRefId &&
    node1.triggerType === node2.triggerType &&
    node1.repeatCount === node2.repeatCount &&
    isAnimConfigEqual(node1.animConfig, node2.animConfig)
  )
}

function isAudioNodeEqual(node1: AudioNode, node2: AudioNode) {
  return (
    node1.spRefId === node2.spRefId &&
    node1.triggerType === node2.triggerType &&
    node1.numSlide === node2.numSlide &&
    node1.loopUntilStopped === node2.loopUntilStopped &&
    node1.hideWhenShow === node2.hideWhenShow &&
    node1.duration === node2.duration &&
    node1.volume === node2.volume
  )
}

function isAnimationNodeEqual(node1: AnimationNode, node2: AnimationNode) {
  if (isAudioNode(node1) && isAudioNode(node2)) {
    return isAudioNodeEqual(node1, node2)
  } else if (!isAudioNode(node1) && !isAudioNode(node2)) {
    return isTimeNodeEqual(node1, node2)
  }
  return false
}

export function isAnimationNodesEqual(
  nodes1: AnimationNode[],
  nodes2: AnimationNode[]
) {
  if (nodes1 == null && nodes2 == null) {
    return true
  }
  if (nodes1.length === nodes2.length) {
    return nodes1.every((node1, i) => isAnimationNodeEqual(node1, nodes2[i]))
  } else {
    return false
  }
}

function assignTimingJSON(
  timeNodes: TimeNode[],
  audioNodes: AudioNode[]
): CT_SlideTiming | undefined {
  if (timeNodes.length < 1 && audioNodes.length < 1) return
  const tnLst: CT_TimeNodeList = []
  const rootTimeNode = genRootTimeNode()
  tnLst.push(rootTimeNode)
  const mainSeqNode: CT_TLTimeNodeSequence = genMainSeqNode()
  assignVal(
    rootTimeNode['cTn'],
    'childTnLst',
    genTimeNodeList([mainSeqNode, ...audioNodes.map((n) => genAudioNode(n))])
  )
  // 由于目前设定音频自动/任意点击后播放, 因此在 timeNodes 头部插入对应的 Click/AfterEffect 节点
  const mergedTimeNodes: Array<AnimationNode> = [...timeNodes]
  for (const audioNode of audioNodes) {
    mergedTimeNodes.unshift(audioNode)
  }

  for (const mainParNodes of splitTimeNodesByClick(mergedTimeNodes)) {
    const beginWithoutClick = mainParNodes[0].triggerType !== 'ClickEffect'
    const wrapParasNode = genMainParNode(beginWithoutClick)
    mainSeqNode['cTn']['childTnLst']!.push(wrapParasNode)
    const afterNodesQueue = splitTimeNodesByAfter(mainParNodes)
    let afterAccTime = 0 // 一个 afterEffect 队列整体需要等待的 dalay
    for (const afterNodes of afterNodesQueue) {
      const parNode = genAfterNodesWrapParNode(afterNodes, afterAccTime)
      afterAccTime += calculateAterNodesQuqueTotalTime(afterNodes)
      wrapParasNode['cTn']['childTnLst']!.push(parNode)
    }
  }
  return { ['tnLst']: tnLst }
}

function _isEmptyDict(o: any): boolean {
  return !type.isObject(o) || Object.keys(o).length <= 0
}

// 原始数据中 tnLst 存在，且未完全解析 tnLst 时需要修复 modoc 数据
function readFromTiming(
  slide: Slide,
  slideTiming: CT_SlideTiming
): {
  timeNodes: TimeNode[]
  audioNodes: AudioNode[]
  needFixAnimation: boolean
} {
  if (_isEmptyDict(slideTiming)) {
    // 这里视empty dict为清空timing的数据，而不是异常数据
    return { timeNodes: [], audioNodes: [], needFixAnimation: false }
  }
  if (!slideTiming['tnLst'] || !slideTiming['tnLst'].length) {
    return { timeNodes: [], audioNodes: [], needFixAnimation: true }
  }

  // 根结点
  const rootTimeNode = getValByKeyPath(slideTiming, 'tnLst.0')
  if (
    rootTimeNode['type'] !== 'par' ||
    getValByKeyPath(rootTimeNode, 'cTn.nodeType') !== 'tmRoot' ||
    !getValByKeyPath(rootTimeNode, 'cTn.childTnLst') ||
    !getValByKeyPath(rootTimeNode, 'cTn.childTnLst')!.length
  ) {
    return { timeNodes: [], audioNodes: [], needFixAnimation: true }
  }
  // 根结点-主节点
  const mainSeqNode = getValByKeyPath(rootTimeNode, 'cTn.childTnLst.0')
  let main_seqq_ctn_nodeType
  if (
    mainSeqNode['type'] !== 'seq' ||
    ((main_seqq_ctn_nodeType = getValByKeyPath(mainSeqNode, 'cTn.nodeType')),
    main_seqq_ctn_nodeType !== 'mainSeq' &&
      main_seqq_ctn_nodeType !== 'interactiveSeq') ||
    !getValByKeyPath(mainSeqNode, 'cTn.childTnLst') ||
    !getValByKeyPath(mainSeqNode, 'cTn.childTnLst')!.length
  ) {
    return { timeNodes: [], audioNodes: [], needFixAnimation: true }
  }
  // 根结点-音频节点(解析音频节点异常不影响主节点解析)
  const audioNodes: AudioNode[] = []
  const rootChildTnLstLength = rootTimeNode.cTn.childTnLst!.length
  for (let i = 1; i < rootChildTnLstLength; i++) {
    const rootChildNode = rootTimeNode.cTn.childTnLst![i]
    if (rootChildNode.type === 'audio') {
      const audioNode = readAudioNodeFromJSON(rootChildNode)
      if (audioNode) {
        audioNodes.push(audioNode)
      }
    }
  }
  // 根结点-触发器节点(目前仅解析其中音频相关数据)
  if (0 < audioNodes.length) {
    for (let i = 1; i < rootChildTnLstLength; i++) {
      const rootChildNode = rootTimeNode.cTn.childTnLst![i]
      if (rootChildNode['type'] !== 'seq') continue
      const nodeType = getValByKeyPath(rootChildNode, 'cTn.nodeType')
      if (nodeType !== 'interactiveSeq') continue
      const parallelNodes = getValByKeyPath(rootChildNode, 'cTn.childTnLst')
      if (!parallelNodes?.length) continue
      for (const wrapParasNode of parallelNodes) {
        if (
          wrapParasNode['type'] !== 'par' ||
          !getValByKeyPath(wrapParasNode, 'cTn.childTnLst') ||
          !getValByKeyPath(wrapParasNode, 'cTn.childTnLst')!.length
        ) {
          continue
        }
        const childPars = getValByKeyPath(wrapParasNode, 'cTn.childTnLst')!
        for (const childPar of childPars) {
          if (childPar['type'] !== 'par') {
            continue
          }
          const cTns = readCTnFromParNode(childPar)
          // 仅从中获取音频信息, 目前不计入动画
          filterCTnByAudioNodes(cTns, audioNodes)
        }
      }
    }
  }

  // 根结点-主节点-以 ClickEffect 节点分割的队列
  const timeNodes: TimeNode[] = []
  let cTnCount = 0
  const parallelNodes = getValByKeyPath(mainSeqNode, 'cTn.childTnLst')!
  for (const wrapParasNode of parallelNodes) {
    if (
      wrapParasNode['type'] !== 'par' ||
      !getValByKeyPath(wrapParasNode, 'cTn.childTnLst') ||
      !getValByKeyPath(wrapParasNode, 'cTn.childTnLst')!.length
    ) {
      continue
    }
    // 以 AfterEffect 节点分割的队列(第一个元素为单个 [ClickNode] 或 [afterNode, ...WithNodes])
    const childPars = getValByKeyPath(wrapParasNode, 'cTn.childTnLst')!
    for (const childPar of childPars) {
      if (childPar['type'] !== 'par') {
        return { timeNodes: [], audioNodes: [], needFixAnimation: true }
      }
      let cTns = readCTnFromParNode(childPar)
      // 与 audioNode 匹配的动画节点不进入动画序列解析为 timeNode, 也不计入异常数据
      if (0 < audioNodes.length) {
        // Todo: 凡是有音频动画, 不仅仅是 mediaCall 类型 timeNode, 所有关联的动画都不解析
        cTns = filterCTnByAudioNodes(cTns, audioNodes)
      }

      cTnCount += cTns.length
      for (const cTn of cTns) {
        const timeNode = readTimeNodeFromJSON(cTn)
        if (timeNode) {
          timeNodes.push(timeNode)
        }
      }
    }
  }
  // mainSeq(或触发器中) 可能无法找到对应的 mediacall node
  const validAudioNodes = audioNodes.filter((node) => !!node.triggerType)

  return {
    timeNodes,
    audioNodes: validAudioNodes,
    needFixAnimation:
      cTnCount !== timeNodes.length ||
      validAudioNodes.length !== audioNodes.length,
  }
}

/**
 * 由于目前音频暂不进入动画体系, 因此与 audioNode 匹配的正确的动画节点不进入动画序列解析为 timeNode, 也不会计入异常(不支持)数据
 * @note 期间若有匹配的 timeNode 会完善对应 audioNode 中的数据
 * */
function filterCTnByAudioNodes(
  cTns: CT_TLCommonTimeNodeData[],
  audioNodes: AudioNode[]
) {
  return cTns.filter((cTn) => {
    const presetID = getValByKeyPath(cTn, 'presetID')
    const presetClass = getValByKeyPath(cTn, 'presetClass')
    const presetSubtype = getValByKeyPath(cTn, 'presetSubtype')
    if (presetID !== 1 || presetClass !== 'mediacall' || presetSubtype !== 0) {
      return true
    }
    const nodeType = getValByKeyPath(cTn, 'nodeType')
    const childTnLst = getValByKeyPath(cTn, 'childTnLst')
    if (!Array.isArray(childTnLst) || !childTnLst.length || !nodeType) {
      return true
    }

    // 与 audioNode 关联的(合法的) mediaCall node 不进入计数, 仅用于解析数据
    return !childTnLst.find((childCtn) => {
      if (childCtn.type !== 'cmd') return false
      const tgtElNode = getValByKeyPath(childCtn, 'cBhvr.tgtEl')
      const duration = getValByKeyPath(childCtn, 'cBhvr.cTn.dur')
      if (!tgtElNode || !isNumber(duration)) return false
      const spRefId = formatSpRefId(tgtElNode)
      if (!spRefId) return false
      // 没有关联或者关联的 mediaCall node 缺少必要数据, 仍需要修复数据
      return audioNodes.find((audioNode) => {
        if (audioNode.spRefId === spRefId) {
          const triggerType = formatTriggerType(nodeType)
          if (!triggerType) return false
          audioNode.triggerType = triggerType
          audioNode.duration = duration
          return true
        }
        return false
      })
    })
  })
}

/**
 * @usage 解析单个 clickNode 代表的 main Parallel 节点内 cTn.childlst 的 pars 节点
 * @note 其中的 pars 按照 affterEffect 节点分组, 但无论是否分组都会用 par 节点再包一层
 * @note 递归向下, 根据是否有 presetClass 判断是否为动画数据节点
 * */
function readCTnFromParNode(
  par: CT_TLTimeNodeParallel
): CT_TLCommonTimeNodeData[] {
  let cTns: CT_TLCommonTimeNodeData[] = []
  const cTn = par['cTn']
  const presetClass = cTn['presetClass']
  const nodeType = cTn['nodeType']
  const presetID = cTn['presetID']
  const isWrapPar = !presetClass || !nodeType || !isNumber(presetID)
  const childLst = cTn['childTnLst']
  if (isWrapPar && childLst) {
    const innerPars = childLst
    for (const innerPar of innerPars) {
      if (innerPar['type'] !== 'par') continue
      const innerCTns = readCTnFromParNode(innerPar)
      cTns = cTns.concat(innerCTns)
    }
  } else {
    cTns.push(cTn)
  }
  return cTns
}

/** 以 ClickEffect 节点将 TimeNodes 分割为 Martix*/
function splitTimeNodesByClick(
  timeNodes: Array<AnimationNode>
): Array<Array<AnimationNode>> {
  const timeNodesArr: Array<Array<AnimationNode>> = []
  let timeNodeArr: Array<AnimationNode> = []
  for (const timeNode of timeNodes) {
    if (timeNode.triggerType !== 'ClickEffect') {
      timeNodeArr.push(timeNode)
    } else {
      // 第一个 Click 节点
      if (!timeNodesArr.length) {
        if (timeNodeArr.length) {
          timeNodesArr.push(timeNodeArr)
        }
        timeNodeArr = [timeNode]
      } else {
        timeNodesArr.push(timeNodeArr)
        timeNodeArr = [timeNode]
      }
    }
  }
  if (timeNodeArr.length) timeNodesArr.push(timeNodeArr)
  return timeNodesArr
}

/** 以 Click 分割的队列中， 将其再按照 afterEffect 分割, 第一个一定为 click 节点 */
function splitTimeNodesByAfter(
  timeNodes: Array<AnimationNode>
): Array<Array<AnimationNode>> {
  const timeNodesArr: Array<Array<AnimationNode>> = []
  let timeNodeArr: Array<AnimationNode> = []
  for (const timeNode of timeNodes) {
    if (timeNode.triggerType !== 'WithEffect') {
      // 第一个 Click 节点
      if (!timeNodesArr.length) {
        if (timeNodeArr.length) {
          timeNodesArr.push(timeNodeArr)
        }
        timeNodeArr = [timeNode]
      } else {
        timeNodesArr.push(timeNodeArr)
        timeNodeArr = [timeNode]
      }
    } else {
      timeNodeArr.push(timeNode)
    }
  }
  if (timeNodeArr.length) timeNodesArr.push(timeNodeArr)
  return timeNodesArr
}

function isAudioNode(node: AnimationNode): node is AudioNode {
  return (
    (node as AudioNode).hideWhenShow !== undefined ||
    (node as AudioNode).loopUntilStopped !== undefined
  )
}

/**
 * 一个 click 序列内 afterEffect 再分割产生的队列整体需要等待的时间，为该 click 序列内之前播放的整体时间
 * 若该队列同时存在 withEffect，则取时间最长者
 * Powerpoint 演示时依赖该值(单独预览则计算得出)
 * */
function calculateAterNodesQuqueTotalTime(timeNodes: Array<AnimationNode>) {
  let afterAccTime = 0
  let prevAfterNodeTotalTime = 0 // 上个 afterEffect 节点的耗时(若 WithEffect 跨度更长，则需取 withEffect 节点时长)
  for (const timeNode of timeNodes) {
    let totalTime: number
    if (isAudioNode(timeNode)) {
      // 音频不计入动画时间
      totalTime = 0
    } else {
      const repeatCount = timeNode.repeatCount ?? 1
      totalTime = timeNode.duration * repeatCount + timeNode.delay
    }
    switch (timeNode.triggerType) {
      case 'ClickEffect':
        afterAccTime += totalTime
        break
      case 'WithEffect':
        if (totalTime > prevAfterNodeTotalTime) {
          afterAccTime += totalTime - prevAfterNodeTotalTime
        }
        break
      case 'AfterEffect':
        afterAccTime += totalTime
        prevAfterNodeTotalTime = totalTime
        break
    }
  }
  return afterAccTime
}

function genRootTimeNode(): CT_TLTimeNodeParallel {
  return {
    ['type']: 'par',
    ['cTn']: {
      ['id']: ROOT_TIME_NODE_ID,
      ['dur']: 'indefinite',
      ['restart']: 'never',
      ['nodeType']: 'tmRoot',
    },
  }
}

function genMainSeqNode(): CT_TLTimeNodeSequence {
  return {
    ['type']: 'seq',
    ['concurrent']: true,
    ['nextAc']: 'seek',
    ['cTn']: {
      ['id']: MAIN_SEQ_TIME_NODE_ID,
      ['dur']: 'indefinite',
      ['nodeType']: 'mainSeq',
      ['childTnLst']: [],
    },
    ['prevCondLst']: [
      {
        ['cond']: {
          ['evt']: 'onPrev',
          ['delay']: 0,
          ['content']: {
            ['type']: 'tgtEl',
            ['tgtEl']: { ['name']: 'sldTgt' },
          },
        },
      },
    ],
    ['nextCondLst']: [
      {
        ['cond']: {
          ['evt']: 'onNext',
          ['delay']: 0,
          ['content']: {
            ['type']: 'tgtEl',
            ['tgtEl']: { ['name']: 'sldTgt' },
          },
        },
      },
    ],
  }
}

function genAudioNode(audioNode: AudioNode): CT_TLMediaNodeAudio {
  return {
    ['type']: 'audio',
    ['mediaNode']: {
      ['numSld']: audioNode.numSlide,
      ['vol']: isNumber(audioNode.volume) ? audioNode.volume * 1000 : undefined,
      ['showWhenStopped']: !audioNode.hideWhenShow,
      ['cTn']: {
        ['id']: getNextGTimeNodeId(),
        ['repeatCount']: audioNode.loopUntilStopped ? 'indefinite' : undefined,
        ['fill']: 'hold',
        // ['display']: false, // '0'
        ['stCondLst']: [{ ['cond']: { ['delay']: 'indefinite' } }],
        ['endCondLst']: [
          {
            ['cond']: {
              ['evt']: 'onStopAudio',
              ['delay']: 0,
              ['content']: {
                ['type']: 'tgtEl',
                ['tgtEl']: { ['name']: 'sldTgt' },
              },
            },
          },
        ],
      },
      ['tgtEl']: { ['name']: 'spTgt', ['spid']: audioNode.spRefId },
    },
  }
}

/** 生成媒体对应的动画节点(目前仅音频节点) */
function genMediaTimeNode(audioNode: AudioNode): CT_TLCommonTimeNodeData {
  return {
    ['id']: getNextGTimeNodeId(),
    ['fill']: 'hold',
    ['presetID']: 1,
    ['presetClass']: 'mediacall',
    ['presetSubtype']: 0,
    ['nodeType']: formatNodeType(audioNode.triggerType),
    ['stCondLst']: [{ ['cond']: { ['delay']: 0 } }],
    ['childTnLst']: [genCommandNode(audioNode.spRefId, audioNode.duration)],
  }
}

/** 若起始节点非 clickEffect 节点, 则默认绑定与 mainSeq 节点 begin 同步触发 */
function genMainParNode(beginWithoutClick = false): CT_TLTimeNodeParallel {
  const mainParNode: CT_TLTimeNodeParallel = {
    ['type']: 'par',
    ['cTn']: {
      ['id']: getNextGTimeNodeId(),
      ['fill']: 'hold',
      ['stCondLst']: [{ ['cond']: { ['delay']: 'indefinite' } }],
      ['childTnLst']: [],
    },
  }
  if (beginWithoutClick) {
    mainParNode['cTn']['stCondLst']!.push({
      ['cond']: {
        ['evt']: 'onBegin',
        ['delay']: 0,
        ['content']: { ['type']: 'tn', ['tn']: MAIN_SEQ_TIME_NODE_ID },
      },
    })
  }
  return mainParNode
}

/** 以 clickEffect 为节点包裹的队列中， 需要 afterEffect 效果再包裹 */
function genAfterNodesWrapParNode(
  timeNodes: Array<AnimationNode>,
  queueDelay = 0
) {
  const contentParNodes: CT_TLTimeNodeParallel[] = []
  const childWrapParaNode: CT_TLTimeNodeParallel = {
    ['type']: 'par',
    ['cTn']: {
      ['id']: getNextGTimeNodeId(),
      ['fill']: 'hold',
      ['stCondLst']: [{ ['cond']: { ['delay']: queueDelay } }],
      ['childTnLst']: contentParNodes,
    },
  }
  for (const timeNode of timeNodes) {
    const commonTimeNodeData = isAudioNode(timeNode)
      ? genMediaTimeNode(timeNode)
      : assginCTnJSON(timeNode)
    contentParNodes.push({ ['type']: 'par', ['cTn']: commonTimeNodeData })
  }
  return childWrapParaNode
}

/** @note Entrance delay = 1 和 Exit delay = duration - 1 */
function genSetVisibleNode(spRefId, visible: boolean, dur?): CT_TLSetBehavior {
  return {
    ['type']: 'set',
    ['cBhvr']: {
      ['cTn']: {
        ['id']: getNextGTimeNodeId(),
        ['dur']: 1,
        ['fill']: 'hold',
        ['stCondLst']: [{ ['cond']: { ['delay']: visible ? 0 : dur! - 1 } }],
      },
      ['tgtEl']: { ['name']: 'spTgt', ['spid']: spRefId },
      ['attrNameLst']: ['style.visibility'],
    },
    ['to']: { ['type']: 'str', ['val']: visible ? 'visible' : 'hidden' },
  }
}

function genCommandNode(
  spRefId: string,
  duration: number
): CT_TLCommandBehavior {
  return {
    ['type']: 'cmd',
    ['cmdType']: 'call',
    ['cmd']: 'playFrom(0.0)',
    ['cBhvr']: genBehaviorNode(spRefId, duration),
  }
}

type AnimEffectFilterType = 'fade'

/** @note Powerpoint fade 效果依赖于 filter， 即识别 Anim 节点而非 cTn type 来进行动画效果 */
function genAnimEffect(
  spRefId: string,
  dur: number,
  visible: boolean,
  filter?: AnimEffectFilterType
): CT_TLAnimateEffectBehavior {
  const animEffectNode: CT_TLAnimateEffectBehavior = {
    ['type']: 'animEffect',
    ['transition']: visible ? 'in' : 'out',
    ['cBhvr']: {
      ['cTn']: { ['id']: getNextGTimeNodeId(), ['dur']: dur },
      ['tgtEl']: { ['name']: 'spTgt', ['spid']: spRefId },
    },
  }
  assignVal(animEffectNode, 'filter', filter as string | undefined)
  return animEffectNode
}

function genAnimFly(
  spRefId: string,
  type: 'in' | 'out',
  dur: number,
  subTypeId: number,
  dir: 'x' | 'y'
): CT_TLAnimateBehavior {
  let startX, startY, endX, endY
  if (type === 'in') {
    switch (subTypeId) {
      case EntranceAnimationTypes['flyIn']['subTypes']['fromTop']:
        startX = 'ppt_x'
        startY = '0-#ppt_h/2'
        break
      case EntranceAnimationTypes['flyIn']['subTypes']['fromBottom']:
        startX = 'ppt_x'
        startY = '1 + #ppt_h/2'
        break
      case EntranceAnimationTypes['flyIn']['subTypes']['fromLeft']:
        startX = '0 - ppt_w/2'
        startY = '#ppt_y'
        break
      case EntranceAnimationTypes['flyIn']['subTypes']['fromRight']:
        startX = '1 + ppt_w/2'
        startY = '#ppt_y'
        break
      case EntranceAnimationTypes['flyIn']['subTypes']['fromTopLeft']:
        startX = '0-#ppt_w/2'
        startY = '0-#ppt_h/2'
        break
      case EntranceAnimationTypes['flyIn']['subTypes']['fromTopRight']:
        startX = '1+#ppt_w/2'
        startY = '0-#ppt_h/2'
        break
      case EntranceAnimationTypes['flyIn']['subTypes']['fromBottomLeft']:
        startX = '0-#ppt_w/2'
        startY = '1+#ppt_h/2'
        break
      case EntranceAnimationTypes['flyIn']['subTypes']['fromBottomRight']:
        startX = '1+#ppt_w/2'
        startY = '1+#ppt_h/2'
        break
    }
    endX = '#ppt_x'
    endY = '#ppt_y'
  } else {
    startX = '#ppt_x'
    startY = '#ppt_y'
    switch (subTypeId) {
      case ExitAnimationTypes['flyOut']['subTypes']['toTop']:
        endX = '#ppt_x'
        endY = '0-ppt_h/2'
        break
      case ExitAnimationTypes['flyOut']['subTypes']['toBottom']:
        endX = '#ppt_x'
        endY = '1+ppt_h/2'
        break
      case ExitAnimationTypes['flyOut']['subTypes']['toLeft']:
        endX = '0-ppt_w/2'
        endY = '#ppt_y'
        break
      case ExitAnimationTypes['flyOut']['subTypes']['toRight']:
        endX = '1+ppt_w/2'
        endY = '#ppt_y'
        break
      case ExitAnimationTypes['flyOut']['subTypes']['toTopLeft']:
        endX = '0-ppt_w/2'
        endY = '0-ppt_h/2'
        break
      case ExitAnimationTypes['flyOut']['subTypes']['toTopRight']:
        endX = '1+ppt_w/2'
        endY = '0-ppt_h/2'
        break
      case ExitAnimationTypes['flyOut']['subTypes']['toBottomLeft']:
        endX = '0-ppt_w/2'
        endY = '1+ppt_h/2'
        break
      case ExitAnimationTypes['flyOut']['subTypes']['toBottomRight']:
        endX = '1+ppt_w/2'
        endY = '1+ppt_h/2'
    }
  }
  const [startVal, endVal] = dir === 'x' ? [startX, endX] : [startY, endY]
  return genAnimLine(spRefId, dur, dir, startVal, endVal)
}

function genAnimFloat(
  spRefId: string,
  type: 'in' | 'out',
  dur: number,
  param:
    | (typeof FloatInDirection)[keyof typeof FloatInDirection]
    | (typeof FloatOutDirection)[keyof typeof FloatOutDirection],
  dir: 'x' | 'y'
): CT_TLAnimateBehavior {
  const [startX, endX] = ['#ppt_x', '#ppt_x']
  let startY: string, endY: string
  switch (type) {
    case 'in':
      startY = param === FloatInDirection.up ? '#ppt_y+.1' : '#ppt_y-.1'
      endY = '#ppt_y'
      break
    case 'out':
      startY = '#ppt_y'
      endY = param === FloatOutDirection.down ? '#ppt_y+.1' : '#ppt_y-.1'
      break
  }
  const [startVal, endVal] = dir === 'x' ? [startX, endX] : [startY, endY]
  return genAnimLine(spRefId, dur, dir, startVal, endVal)
}

function genAnimLine(
  spRefId: string,
  dur: number,
  dir: 'x' | 'y',
  startVal: string,
  endVal: string
): CT_TLAnimateBehavior {
  return {
    ['type']: 'anim',
    ['calcmode']: 'lin',
    ['valueType']: 'num',
    ['cBhvr']: genBehaviorNode(
      spRefId,
      dur,
      [dir === 'x' ? 'ppt_x' : 'ppt_y'],
      { addtive: 'base' }
    ),
    ['tavLst']: [
      {
        ['tav']: {
          ['tm']: 0,
          ['val']: { ['type']: 'str', ['val']: startVal },
        },
      },
      {
        ['tav']: {
          ['tm']: 100000,
          ['val']: { ['type']: 'str', ['val']: endVal },
        },
      },
    ],
  }
}

function genAnimRot(spId, dur, rot: number): CT_TLAnimateRotationBehavior {
  return {
    ['type']: 'animRot',
    ['by']: (rot / 360) * 21600000,
    ['cBhvr']: {
      ['cTn']: { ['id']: getNextGTimeNodeId(), ['dur']: dur, ['fill']: 'hold' },
      ['tgtEl']: { ['name']: 'spTgt', ['spid']: spId },
      ['attrNameLst']: ['r'],
    },
  }
}

function genAnimScale(spId, dur, sx, sy): CT_TLAnimateScaleBehavior {
  return {
    ['type']: 'animScale',
    ['cBhvr']: {
      ['cTn']: { ['id']: getNextGTimeNodeId(), ['dur']: dur, ['fill']: 'hold' },
      ['tgtEl']: { ['name']: 'spTgt', ['spid']: spId },
    },
    ['by']: { ['x']: sx * 100000, ['y']: sy * 100000 },
  }
}

function genAnimBlink(spRefId: string, dur: number): CT_TLAnimateBehavior {
  return {
    ['type']: 'anim',
    ['calcmode']: 'discrete',
    ['valueType']: 'str',
    ['cBhvr']: genBehaviorNode(spRefId, dur, ['style.visibility']),
    ['tavLst']: [
      {
        ['tav']: {
          ['tm']: 0,
          ['val']: { ['type']: 'str', ['val']: 'hidden' },
        },
      },
      {
        ['tav']: {
          ['tm']: 50000,
          ['val']: { ['type']: 'str', ['val']: 'visible' },
        },
      },
    ],
  }
}

/** 目前除 mediaNodes 外均需要 attrLst */
function genBehaviorNode(
  spRefId: string,
  dur: number,
  attrs?: string[],
  opts?: { addtive? }
): CT_TLCommonBehaviorData {
  const bhvrNode: CT_TLCommonBehaviorData = {
    ['cTn']: { ['id']: getNextGTimeNodeId(), ['dur']: dur, ['fill']: 'hold' },
    ['tgtEl']: { ['name']: 'spTgt', ['spid']: spRefId },
    ['attrNameLst']: attrs,
  }
  if (opts) assignVal(bhvrNode, 'additive', opts.addtive)
  return bhvrNode
}

function genTimeNodeList(tnArr: CT_TimeNode[]) {
  return tnArr as CT_TimeNodeList
}

// ---------------------------- TimeNode Helpers ---------------------------- //

/** presetId, presetSubtype 和 class 唯一确定一种动画 */
function formatAnimUnionKey(timeNode: TimeNode): {
  presetID: number
  presetSubtype: number
  animType: number
} {
  let presetID: number, animType: number
  const animConfig = timeNode.animConfig

  const presetSubtype = (animConfig as any).param ?? 0
  switch (animConfig.type) {
    case 'Entrance_Appear': {
      presetID = SlideAnimationTypes['Entrance']['appear']['id']
      animType = presetID
      break
    }
    case 'Entrance_FadeIn': {
      presetID = SlideAnimationTypes['Entrance']['fadeIn']['id']
      animType = presetID
      break
    }
    case 'Entrance_FlyIn': {
      presetID = SlideAnimationTypes['Entrance']['flyIn']['id']
      animType = presetID
      break
    }
    case 'Entrance_FloatIn': {
      presetID = animConfig.param
      animType = CustomAnimTypes['Entrance']['FloatIn']
      break
    }

    case 'Emphasis_Spin': {
      presetID = SlideAnimationTypes['Emphasis']['spin']['id']
      animType = presetID
      break
    }
    case 'Emphasis_GrowShrink': {
      presetID = SlideAnimationTypes['Emphasis']['growShrink']['id']
      animType = presetID
      break
    }
    case 'Emphasis_Blink': {
      presetID = SlideAnimationTypes['Emphasis']['blink']['id']
      animType = presetID
      break
    }
    case 'Exit_Disappear': {
      presetID = SlideAnimationTypes['Exit']['disappear']['id']
      animType = presetID
      break
    }
    case 'Exit_FadeOut': {
      presetID = SlideAnimationTypes['Exit']['fadeOut']['id']
      animType = presetID
      break
    }
    case 'Exit_FlyOut': {
      presetID = SlideAnimationTypes['Exit']['flyOut']['id']
      animType = presetID
      break
    }
    case 'Exit_FloatOut': {
      presetID = animConfig.param
      animType = CustomAnimTypes['Exit']['FloatOut']
      break
    }
  }

  return { presetID, presetSubtype, animType }
}

/**
 * 单条动画配置, 对应最细粒度 cTn 节点, group 内的 sp 不允许单独设置动画
 * cTn 内部的 delay 为改节点设置的 delay, 除此之外上层 cTn 包裹节点还有 after 前的 dalay
 * */
function assginCTnJSON(timeNode: TimeNode): CT_TLCommonTimeNodeData {
  const { spRefId, animConfig, delay, duration, repeatCount } = timeNode
  const { presetID, presetSubtype } = formatAnimUnionKey(timeNode)

  const cTn: CT_TLCommonTimeNodeData = {
    ['id']: getNextGTimeNodeId(),
    ['fill']: 'hold',
    ['presetID']: presetID,
    ['presetSubtype']: presetSubtype,
    ['nodeType']: formatNodeType(timeNode.triggerType),
    ['stCondLst']: [{ ['cond']: { ['delay']: delay } }],
    ['grpId']: 0,
  }
  if (repeatCount) cTn['repeatCount'] = repeatCount * REPEAT_COUNT_BASE
  let contentChildArr: CT_TimeNode[]
  let setVisbibleNode
  switch (getAnimClass(animConfig)) {
    case 'Entrance': {
      assignVal(cTn, 'presetClass', 'entr')
      setVisbibleNode = genSetVisibleNode(spRefId, true)
      break
    }
    case 'Exit': {
      assignVal(cTn, 'presetClass', 'exit')
      setVisbibleNode = genSetVisibleNode(spRefId, false, duration)
      break
    }
    case 'Emphasis': {
      assignVal(cTn, 'presetClass', 'emph')
      break
    }
  }
  switch (animConfig.type) {
    case 'Entrance_Appear': {
      contentChildArr = [setVisbibleNode]
      break
    }
    case 'Entrance_FadeIn': {
      const animEffectNode = genAnimEffect(spRefId, duration, true, 'fade')
      contentChildArr = [setVisbibleNode, animEffectNode]
      break
    }
    case 'Entrance_FlyIn': {
      const subType = animConfig.param
      const animXNode = genAnimFly(spRefId, 'in', duration, subType, 'x')
      const animYNode = genAnimFly(spRefId, 'in', duration, subType, 'y')
      contentChildArr = [setVisbibleNode, animXNode, animYNode]
      break
    }
    case 'Entrance_FloatIn': {
      const dir = animConfig.param
      const animEffectNode = genAnimEffect(spRefId, duration, true, 'fade')
      const animXNode = genAnimFloat(spRefId, 'in', duration, dir, 'x')
      const animYNode = genAnimFloat(spRefId, 'in', duration, dir, 'y')
      contentChildArr = [setVisbibleNode, animEffectNode, animXNode, animYNode]
      break
    }
    case 'Exit_Disappear': {
      contentChildArr = [setVisbibleNode]
      break
    }
    case 'Exit_FadeOut': {
      const animEffectNode = genAnimEffect(spRefId, duration, false, 'fade')
      contentChildArr = [animEffectNode, setVisbibleNode]
      break
    }
    case 'Exit_FlyOut': {
      const subType = animConfig.param
      cTn.presetSubtype = subType
      const animXNode = genAnimFly(spRefId, 'out', duration, subType, 'x')
      const animYNode = genAnimFly(spRefId, 'out', duration, subType, 'y')
      contentChildArr = [animXNode, animYNode, setVisbibleNode]
      break
    }
    case 'Exit_FloatOut': {
      const dir = animConfig.param
      const animEffectNode = genAnimEffect(spRefId, duration, true, 'fade')
      const animXNode = genAnimFloat(spRefId, 'out', duration, dir, 'x')
      const animYNode = genAnimFloat(spRefId, 'out', duration, dir, 'y')
      contentChildArr = [animEffectNode, animXNode, animYNode, setVisbibleNode]
      break
    }
    case 'Emphasis_Spin': {
      const rot = animConfig.param.rot
      const animRotNode = genAnimRot(spRefId, duration, rot)
      contentChildArr = [animRotNode]
      break
    }
    case 'Emphasis_GrowShrink': {
      const { sx, sy } = animConfig.param
      const animScaleNode = genAnimScale(spRefId, duration, sx, sy)
      contentChildArr = [animScaleNode]
      break
    }
    case 'Emphasis_Blink': {
      const animNode = genAnimBlink(spRefId, duration)
      contentChildArr = [animNode]
      break
    }
  }

  assignVal(cTn, 'childTnLst', genTimeNodeList(contentChildArr))
  return cTn
}

/** cTn 为包含业务数据, 包裹 stCondLst 和 childTnLst 的最小单元 */
function readTimeNodeFromJSON(
  cTn: CT_TLCommonTimeNodeData
): TimeNode | undefined {
  // cTn 已经检查过一定有 nodeType / presetClass / presetID
  const nodeType = getValByKeyPath(cTn, 'nodeType')
  const stCondLst = getValByKeyPath(cTn, 'stCondLst')
  const presetClass = getValByKeyPath(cTn, 'presetClass')
  const presetID = getValByKeyPath(cTn, 'presetID')
  const childTnLst = getValByKeyPath(cTn, 'childTnLst')
  const repeatCount = getValByKeyPath(cTn, 'repeatCount')
  if (!childTnLst || !Array.isArray(childTnLst) || !childTnLst.length) return
  const animClass = formatAnimClass(presetClass)
  const triggerType = formatTriggerType(nodeType)
  if (!triggerType || !stCondLst || !animClass) return
  const delay = getValByKeyPath(stCondLst[0], 'cond.delay')
  if (!isNumber(delay)) return
  let duration = 0
  let animConfig: AnimConfig
  let tgtElNode: CT_TLTimeTargetElement

  switch (animClass) {
    case 'Entrance': {
      const animType = formatAnimType(animClass, presetID)
      if (!animType) return
      switch (animType) {
        case 'appear':
          const setVisbibleNode = childTnLst[0]
          if (setVisbibleNode?.type !== 'set') return
          tgtElNode = getValByKeyPath(setVisbibleNode, 'cBhvr.tgtEl')
          duration = 1
          animConfig = { type: 'Entrance_Appear' }
          break
        case 'fadeIn': {
          const animEffectNode = childTnLst[1]
          if (animEffectNode?.type !== 'animEffect') return
          tgtElNode = getValByKeyPath(animEffectNode, 'cBhvr.tgtEl')
          const animDuration = getValByKeyPath(animEffectNode, 'cBhvr.cTn.dur')
          if (!isNumber(animDuration)) return
          duration = animDuration
          animConfig = { type: 'Entrance_FadeIn' }
          break
        }
        case 'flyIn': {
          const [animX, animY] = [childTnLst[1], childTnLst[2]]
          if (animX?.type !== 'anim' || animY?.type !== 'anim') return
          tgtElNode = getValByKeyPath(animX, 'cBhvr.tgtEl')
          const durationX = getValByKeyPath(animX, 'cBhvr.cTn.dur')
          const durationY = getValByKeyPath(animY, 'cBhvr.cTn.dur')
          if (!isNumber(durationX) || !isNumber(durationY)) return
          duration = Math.max(durationX, durationY)
          const subTypeId = getValByKeyPath(cTn, 'presetSubtype')
          if (!subTypeId) return
          animConfig = {
            type: 'Entrance_FlyIn',
            param: subTypeId as any,
          }
          break
        }
        case 'floatIn-Up':
        case 'floatIn-Down': {
          const animEffectNode = childTnLst[1]
          if (animEffectNode.type !== 'animEffect') return
          const [animX, animY] = [childTnLst[2], childTnLst[3]]
          if (animX?.type !== 'anim' || animY?.type !== 'anim') return
          tgtElNode = getValByKeyPath(animEffectNode, 'cBhvr.tgtEl')
          const durationFade = getValByKeyPath(animEffectNode, 'cBhvr.cTn.dur')
          const durationX = getValByKeyPath(animX, 'cBhvr.cTn.dur')
          const durationY = getValByKeyPath(animY, 'cBhvr.cTn.dur')
          if (
            !isNumber(durationFade) ||
            !isNumber(durationX) ||
            !isNumber(durationY)
          ) {
            return
          }

          duration = Math.max(durationFade, durationX, durationY)
          const param =
            animType === 'floatIn-Up'
              ? FloatInDirection.up
              : FloatInDirection.down
          animConfig = {
            type: 'Entrance_FloatIn',
            param,
          }
          break
        }
      }
      break
    }
    case 'Emphasis': {
      const animType = formatAnimType(animClass, presetID)
      if (!animType) return
      switch (animType) {
        case 'spin': {
          const animRotNode = childTnLst[0]
          if (animRotNode?.type !== 'animRot') return
          tgtElNode = getValByKeyPath(animRotNode, 'cBhvr.tgtEl')
          const animDur = getValByKeyPath(animRotNode, 'cBhvr.cTn.dur')
          let rot = getValByKeyPath(animRotNode, 'by')
          if (!isNumber(animDur) || !isNumber(rot)) return
          duration = animDur
          rot = (360 * rot) / 21600000
          animConfig = {
            type: 'Emphasis_Spin',
            param: { rot: rot },
          }
          break
        }
        case 'growShrink': {
          const animScaleNode = childTnLst[0]
          if (animScaleNode?.type !== 'animScale') return
          tgtElNode = getValByKeyPath(animScaleNode, 'cBhvr.tgtEl')
          const animDuration = getValByKeyPath(animScaleNode, 'cBhvr.cTn.dur')
          if (!isNumber(animDuration) || !animScaleNode.by) return
          duration = animDuration
          let sx = getValByKeyPath(animScaleNode, 'by.x')
          let sy = getValByKeyPath(animScaleNode, 'by.y')
          if (!isNumber(sx) || !isNumber(sy)) return
          ;[sx, sy] = [sx / 100000, sy / 100000]
          animConfig = {
            type: 'Emphasis_GrowShrink',
            param: { sx, sy },
          }
          break
        }
        case 'blink': {
          const animNode = childTnLst[0]
          if (animNode?.type !== 'anim') return
          tgtElNode = getValByKeyPath(animNode, 'cBhvr.tgtEl')
          const animDuration = getValByKeyPath(animNode, 'cBhvr.cTn.dur')
          if (!isNumber(animDuration)) return
          duration = animDuration
          animConfig = { type: 'Emphasis_Blink' }
        }
      }
      break
    }
    case 'Exit': {
      const animType = formatAnimType(animClass, presetID)
      if (!animType) return
      switch (animType) {
        case 'disappear':
          const setVisbibleNode = childTnLst[childTnLst.length - 1]
          if (setVisbibleNode?.type !== 'set') return
          tgtElNode = getValByKeyPath(setVisbibleNode, 'cBhvr.tgtEl')
          duration = 1
          animConfig = { type: 'Exit_Disappear' }
          break
        case 'fadeOut': {
          const animEffectNode = childTnLst[0]
          if (animEffectNode?.type !== 'animEffect') return
          tgtElNode = getValByKeyPath(animEffectNode, 'cBhvr.tgtEl')
          const animDuration = getValByKeyPath(animEffectNode, 'cBhvr.cTn.dur')
          if (!isNumber(animDuration)) return
          duration = animDuration
          animConfig = { type: 'Exit_FadeOut' }
          break
        }
        case 'flyOut': {
          const [animX, animY] = [childTnLst[0], childTnLst[1]]
          if (animX?.type !== 'anim' || animY?.type !== 'anim') return
          tgtElNode = getValByKeyPath(animX, 'cBhvr.tgtEl')
          const durationX = getValByKeyPath(animX, 'cBhvr.cTn.dur')
          const durationY = getValByKeyPath(animY, 'cBhvr.cTn.dur')
          if (!isNumber(durationX) || !isNumber(durationY)) return
          duration = Math.max(durationX, durationY)
          const subTypeId = getValByKeyPath(cTn, 'presetSubtype')
          if (!subTypeId) return
          animConfig = {
            type: 'Exit_FlyOut',
            param: subTypeId as any,
          }
          break
        }
        case 'floatOut-Down':
        case 'floatOut-Up': {
          const animEffectNode = childTnLst[0]
          if (animEffectNode?.type !== 'animEffect') return
          const [animX, animY] = [childTnLst[1], childTnLst[2]]
          if (animX?.type !== 'anim' || animY?.type !== 'anim') return
          tgtElNode = getValByKeyPath(animEffectNode, 'cBhvr.tgtEl')
          const durationFade = getValByKeyPath(animEffectNode, 'cBhvr.cTn.dur')
          const durationX = getValByKeyPath(animX, 'cBhvr.cTn.dur')
          const durationY = getValByKeyPath(animY, 'cBhvr.cTn.dur')
          if (
            !isNumber(durationFade) ||
            !isNumber(durationX) ||
            !isNumber(durationY)
          ) {
            return
          }
          duration = Math.max(durationFade, durationX, durationY)
          const param =
            animType === 'floatOut-Down'
              ? FloatOutDirection.down
              : FloatOutDirection.up
          animConfig = {
            type: 'Exit_FloatOut',
            param,
          }
          break
        }
      }
      break
    }
  }
  if (!tgtElNode) return
  const spRefId = formatSpRefId(tgtElNode)
  if (!spRefId) return
  const categories = formatCategory(animConfig)
  const timeNode: TimeNode = {
    spRefId,
    animConfig,
    delay,
    duration,
    triggerType,
    categories,
  }
  if (repeatCount && isNumber(repeatCount) && 2000 <= repeatCount) {
    timeNode.repeatCount = toInt((repeatCount as number) / REPEAT_COUNT_BASE)
  }
  return timeNode
}

/** nodeType 和 duration 需要后续从动画序列中读取 */
function readAudioNodeFromJSON(
  node: CT_TLMediaNodeAudio
): AudioNode | undefined {
  const mediaNode = getValByKeyPath(node, 'mediaNode')
  if (!mediaNode) return
  const mediaCTn = getValByKeyPath(mediaNode, 'cTn')
  if (!mediaCTn) return
  const tgtElNode = getValByKeyPath(mediaNode, 'tgtEl')
  if (!tgtElNode) return
  const spRefId = formatSpRefId(tgtElNode)
  if (!spRefId) return
  const numSld = getValByKeyPath(mediaNode, 'numSld')
  const vol = getValByKeyPath(mediaNode, 'vol')
  const showWhenStopped = getValByKeyPath(mediaNode, 'showWhenStopped')
  const repeatCount = getValByKeyPath(mediaCTn, 'repeatCount')
  const audioNode: Omit<AudioNode, 'triggerType' | 'duration'> = {
    spRefId,
    hideWhenShow: showWhenStopped == null ? false : !showWhenStopped,
    loopUntilStopped: repeatCount === 'indefinite',
    numSlide: numSld,
    volume: isNumber(vol) ? vol / 1000 : undefined,
  }
  return audioNode as AudioNode
}

function formatAnimClass(
  presetClass?: ST_TLTimeNodePresetClassType
): AnimClass | undefined {
  if (!presetClass) return
  switch (presetClass) {
    case 'entr':
      return 'Entrance'
    case 'exit':
      return 'Exit'
    case 'emph':
      return 'Emphasis'
    default:
      return
  }
}

function formatTriggerType(
  triggerType: Nullable<ST_TLTimeNodeType>
): ShapeAnimationNodeType | undefined {
  if (!triggerType) return
  if (triggerType === 'clickEffect') return 'ClickEffect'
  if (triggerType === 'withEffect') return 'WithEffect'
  if (triggerType === 'afterEffect') return 'AfterEffect'
  return
}

function formatNodeType(nodeType: ShapeAnimationNodeType) {
  if (nodeType === 'ClickEffect') return 'clickEffect'
  if (nodeType === 'WithEffect') return 'withEffect'
  if (nodeType === 'AfterEffect') return 'afterEffect'
}

function formatAnimType<T extends AnimClass>(animClass: T, presetId?: number) {
  if (!isNumber(presetId)) return
  const animTypes = SlideAnimationTypes[animClass]
  for (const animType in animTypes) {
    if (animTypes[animType]['id'] === presetId) {
      return animType as AnimTypes<T>
    }
  }
}

function formatCategory(animConfig: AnimConfig): number {
  switch (animConfig.type) {
    case 'Entrance_Appear':
    case 'Exit_Disappear':
    case 'Emphasis_Blink':
      return AnimCategoryMapping.visibility
    case 'Entrance_FadeIn':
    case 'Exit_FadeOut':
      return AnimCategoryMapping.alpha
    case 'Entrance_FlyIn':
    case 'Exit_FlyOut':
      return AnimCategoryMapping.pos
    case 'Entrance_FloatIn':
    case 'Exit_FloatOut':
      return (
        AnimCategoryMapping.visibility |
        AnimCategoryMapping.alpha |
        AnimCategoryMapping.pos
      )
    case 'Emphasis_Spin':
      return AnimCategoryMapping.rot
    case 'Emphasis_GrowShrink':
      return AnimCategoryMapping.scale
  }
}

function formatDuration(animConfig: AnimConfig, duration: number): number {
  const isValidDuration = isNumber(duration) && 0 < duration
  const formatDuration = isValidDuration ? duration : 500
  switch (animConfig.type) {
    case 'Entrance_Appear':
    case 'Exit_Disappear':
      return 1
    case 'Emphasis_Blink':
    case 'Entrance_FadeIn':
    case 'Exit_FadeOut':
    case 'Entrance_FlyIn':
    case 'Exit_FlyOut':
    case 'Entrance_FloatIn':
    case 'Exit_FloatOut':
    case 'Emphasis_Spin':
    case 'Emphasis_GrowShrink':
      return formatDuration
  }
}

function formatSpRefId(tgtElNode: CT_TLTimeTargetElement) {
  if (tgtElNode['name'] !== 'spTgt') return
  return tgtElNode['spid']
}

export function readTimeNodeToSetting(
  slide: Slide,
  timeNode: TimeNode
): AnimationSetting | undefined {
  const spTree = slide.cSld.spTree
  const sp = spTree.find((sp) => sp.getRefId() === timeNode.spRefId)
  if (!sp) return
  const { animType } = formatAnimUnionKey(timeNode)

  const animSetting: AnimationSetting = {
    ['ShapeId']: sp.getId(),
    ['AnimClass']: getAnimClass(timeNode.animConfig),
    ['AnimType']: animType,
    ['AnimDelay']: timeNode.delay,
    ['AnimDuration']: timeNode.duration,
    ['AnimTriggerType']: timeNode.triggerType,
  }
  if (timeNode.repeatCount) animSetting.AnimRepeatCount = timeNode.repeatCount
  switch (timeNode.animConfig.type) {
    case 'Entrance_Appear':
    case 'Exit_Disappear':
    case 'Emphasis_Blink':
    case 'Entrance_FadeIn':
    case 'Exit_FadeOut': {
      break
    }
    case 'Entrance_FlyIn':
    case 'Exit_FlyOut':
    case 'Entrance_FloatIn':
    case 'Exit_FloatOut': {
      animSetting['AnimSubType'] = timeNode.animConfig.param
      break
    }
    case 'Emphasis_Spin':
    case 'Emphasis_GrowShrink': {
      animSetting['AnimParam'] = timeNode.animConfig.param
      break
    }
  }
  return animSetting
}

export function writeTimeNodeFromSetting(
  setting: AnimationSetting
): TimeNode | undefined {
  // 混淆处理
  const ShapeId = setting['ShapeId']
  const AnimClass = setting['AnimClass']
  const AnimType = setting['AnimType']
  const AnimDelay = setting['AnimDelay']
  const AnimDuration = setting['AnimDuration']
  const AnimTriggerType = setting['AnimTriggerType']
  const AnimSubType = setting['AnimSubType']
  const AnimParam = setting['AnimParam']
  const AnimRepeatCount = setting['AnimRepeatCount']
  const sp = EditorUtil.entityRegistry.getEntityById(ShapeId) as SlideElement
  if (!sp || !canAddAnimation(sp) || !isSlideElement(sp)) return
  let animConfig: Nullable<AnimConfig>
  switch (true) {
    case AnimClass === 'Entrance' &&
      AnimType === EntranceAnimationTypes['appear']['id']:
      animConfig = { type: 'Entrance_Appear' }
      break
    case AnimClass === 'Entrance' &&
      AnimType === EntranceAnimationTypes['fadeIn']['id']:
      animConfig = { type: 'Entrance_FadeIn' }
      break
    case AnimClass === 'Entrance' &&
      AnimType === EntranceAnimationTypes['flyIn']['id'] &&
      isNumber(AnimSubType):
      animConfig = { type: 'Entrance_FlyIn', param: AnimSubType as any }
      break
    case AnimClass === 'Entrance' &&
      AnimType === CustomAnimTypes['Entrance']['FloatIn'] &&
      isNumber(AnimSubType):
      animConfig = { type: 'Entrance_FloatIn', param: AnimSubType as any }
      break
    case AnimClass === 'Emphasis' &&
      AnimType === EmphasisAnimationTypes['spin']['id'] &&
      isDict(AnimParam):
      animConfig = { type: 'Emphasis_Spin', param: AnimParam as any }
      break
    case AnimClass === 'Emphasis' &&
      AnimType === EmphasisAnimationTypes['growShrink']['id'] &&
      isDict(AnimParam):
      animConfig = { type: 'Emphasis_GrowShrink', param: AnimParam as any }
      break
    case AnimClass === 'Emphasis' &&
      AnimType === EmphasisAnimationTypes['blink']['id']:
      animConfig = { type: 'Emphasis_Blink' }
      break
    case AnimClass === 'Exit' &&
      AnimType === ExitAnimationTypes['disappear']['id']:
      animConfig = { type: 'Exit_Disappear' }
      break
    case AnimClass === 'Exit' &&
      AnimType === ExitAnimationTypes['fadeOut']['id']:
      animConfig = { type: 'Exit_FadeOut' }
      break
    case AnimClass === 'Exit' &&
      AnimType === ExitAnimationTypes['flyOut']['id'] &&
      isNumber(AnimSubType):
      animConfig = { type: 'Exit_FlyOut', param: AnimSubType as any }
      break
    case AnimClass === 'Exit' &&
      AnimType === CustomAnimTypes['Exit']['FloatOut'] &&
      isNumber(AnimSubType):
      animConfig = { type: 'Exit_FloatOut', param: AnimSubType as any }
      break
  }
  if (!animConfig) return
  const timeNode: TimeNode = {
    spRefId: sp.getRefId(),
    animConfig: animConfig,
    delay: AnimDelay,
    duration: formatDuration(animConfig, AnimDuration),
    triggerType: AnimTriggerType,
    categories: formatCategory(animConfig),
  }
  if (AnimRepeatCount) timeNode.repeatCount = AnimRepeatCount
  return timeNode
}

// ---------------------------- Updater Helpers ---------------------------- //

export function animCategoryToInt(cat: AnimCategory) {
  return AnimCategoryMapping[cat]
}

type playNode = {
  sp: SlideElement
  nodeType: ShapeAnimationNodeType
  duration: number
  delay: number
  category: ReturnType<typeof animCategoryToInt>
  updater: ReturnType<typeof genTimeNodeUpdater>
}

/**
 * 将 TimeNode 组装为 Player 需要 node 的工具方法
 * 过程中会涉及到 node 的拆分，拆分的原则是只能产生 WithEffect(为了保证循环/上下条效果)
 * */
export function playNodeMaker(slide: Slide, timeNode: TimeNode): playNode[] {
  const presentation = slide.presentation
  const spTree = slide.cSld.spTree
  const sp = spTree.find((sp) => sp.getRefId() === timeNode.spRefId)

  if (sp == null) return []

  let duration = timeNode.duration
  if (timeNode.repeatCount && 1 < timeNode.repeatCount) {
    duration *= timeNode.repeatCount
  }

  // 分片：将 timeNode 按 category 效果第二次拆分，拆分后的第一个以后的子 Node 应为 WithEffect
  const playNodes: playNode[] = []
  const nodeCats = timeNode.categories

  for (let i = 0; i < animCategories.length; i++) {
    const animCategory = animCategories[i]
    if (nodeCats & animCategory) {
      playNodes.push({
        sp: sp,
        nodeType: playNodes.length > 0 ? 'WithEffect' : timeNode.triggerType,
        duration: duration,
        delay: timeNode.delay,
        category: animCategory,
        updater: genTimeNodeUpdater(timeNode, presentation, animCategory),
      })
    }
  }
  return playNodes
}

/** 生成 slide 的播放队列 */
export function makeAnimQueue(slide: Slide, timeNodes: TimeNode[]) {
  if (timeNodes.length < 1) return
  const nodes = playNodeMaker(slide, timeNodes[0])

  if (nodes.length < 1) return
  for (let i = 1, l = timeNodes.length; i < l; i++) {
    const i_nodes = playNodeMaker(slide, timeNodes[i])
    appendArrays(nodes, i_nodes)
  }

  return makeShapeAnimationQueue(nodes[0], nodes.slice(1))
}

// new

/**
 * ShapeAnimationUpdateInfo 中除 visibility/alpha 外均应存储 delta 值
 * @note 注意严格按照修改的属性值去赋值, 如 scale 仅修改 sx/sy, 最后虽然表现有位移, 也应在别处专门处理
 * @param category 分片需要实现的效果分类，每个分片应该严格修改自身所属的效果
 * */
export function genTimeNodeUpdater(
  timeNode: TimeNode,
  presentation: Presentation,
  category?: number
) {
  return (
    sp: SlideElement,
    percentage: number,
    prevUpdateInfo: ShapeAnimationUpdateInfo
  ): ShapeAnimationUpdateInfo => {
    // 重复播放时, 若 mod === 0取结束点
    if (timeNode.repeatCount && 1 < timeNode.repeatCount) {
      percentage = getPercentageForOneLoop(percentage, timeNode.repeatCount)
    }
    // 匀速 / 加速 可在各自动画类型计算方法中进行单独调整
    switch (timeNode.animConfig.type) {
      case 'Entrance_Appear': {
        if (percentage <= 0) return { ...prevUpdateInfo, visibility: false }
        return {
          ...prevUpdateInfo,
          visibility: percentage >= 1 ? true : false,
        }
      }
      case 'Entrance_FadeIn': {
        if (percentage <= 0) return { ...prevUpdateInfo, visibility: false }
        return { ...prevUpdateInfo, alpha: percentage, visibility: true }
      }
      case 'Entrance_FlyIn': {
        if (percentage <= 0) return { ...prevUpdateInfo, visibility: false }
        const posDelta = getFlyInUpdateInfo(
          sp,
          percentage,
          timeNode.animConfig.param,
          presentation
        )
        return { ...prevUpdateInfo, ...posDelta, visibility: true }
      }
      case 'Entrance_FloatIn': {
        if (!isNumber(category)) return { ...prevUpdateInfo }
        const { alpha, y } = getFloatInUpdateInfo(
          sp,
          percentage,
          timeNode.animConfig.param,
          presentation
        )
        if (category & AnimCategoryMapping.visibility) {
          if (percentage <= 0) return { ...prevUpdateInfo, visibility: false }
          return { ...prevUpdateInfo, visibility: true }
        } else if (category & AnimCategoryMapping.alpha) {
          return { ...prevUpdateInfo, ...{ alpha } }
        } else {
          return { ...prevUpdateInfo, ...{ y } }
        }
      }

      case 'Emphasis_Spin': {
        const rot = getRotateUpdateInfo(
          percentage,
          timeNode.animConfig.param.rot,
          prevUpdateInfo
        )
        return { ...prevUpdateInfo, rot }
      }
      case 'Emphasis_GrowShrink': {
        const scaleDelta = getScaleUpdateInfo(
          percentage,
          timeNode.animConfig.param,
          prevUpdateInfo
        )
        return { ...prevUpdateInfo, ...scaleDelta }
      }
      case 'Emphasis_Blink': {
        if (percentage <= 0) {
          return { ...prevUpdateInfo }
        } else if (percentage <= 0.5) {
          return { ...prevUpdateInfo, visibility: false }
        } else if (percentage <= 1) {
          return { ...prevUpdateInfo, visibility: true }
        } else {
          return { ...prevUpdateInfo }
        }
      }

      case 'Exit_Disappear': {
        if (percentage <= 0) return { ...prevUpdateInfo }
        return {
          ...prevUpdateInfo,
          visibility: percentage >= 1 ? false : prevUpdateInfo.visibility,
        }
      }
      case 'Exit_FadeOut': {
        if (percentage <= 0) return { ...prevUpdateInfo }
        if (1 <= percentage) return { ...prevUpdateInfo, visibility: false }
        return { ...prevUpdateInfo, alpha: 1 - percentage }
      }
      case 'Exit_FlyOut': {
        if (percentage <= 0) return { ...prevUpdateInfo }
        if (1 <= percentage) return { ...prevUpdateInfo, visibility: false }
        const posDelta = getFlyOutUpdateInfo(
          sp,
          percentage,
          timeNode.animConfig.param,
          presentation,
          prevUpdateInfo
        )
        return { ...prevUpdateInfo, ...posDelta }
      }
      case 'Exit_FloatOut': {
        if (!isNumber(category)) return { ...prevUpdateInfo }
        const { alpha, y } = getFloatOutUpdateInfo(
          sp,
          percentage,
          timeNode.animConfig.param,
          presentation
        )
        if (category & AnimCategoryMapping.visibility) {
          if (percentage <= 0) return { ...prevUpdateInfo }
          return {
            ...prevUpdateInfo,
            visibility: percentage >= 1 ? false : prevUpdateInfo.visibility,
          }
        } else if (category & AnimCategoryMapping.alpha) {
          return { ...prevUpdateInfo, ...{ alpha } }
        } else {
          return { ...prevUpdateInfo, ...{ y } }
        }
      }
    }
  }
}

export function getFlyInUpdateInfo(
  sp: SlideElement,
  percentage: number,
  subTypeId: (typeof EntranceAnimationTypes)['flyIn']['subTypes'][keyof (typeof EntranceAnimationTypes)['flyIn']['subTypes']],
  presentation: Presentation
) {
  const [endX, endY] = [sp.x, sp.y]
  let startX: number, startY: number
  switch (subTypeId) {
    case EntranceAnimationTypes['flyIn']['subTypes']['fromTop']:
      startX = endX
      startY = 0 - sp.extY
      break
    case EntranceAnimationTypes['flyIn']['subTypes']['fromBottom']:
      startX = endX
      startY = presentation.height
      break
    case EntranceAnimationTypes['flyIn']['subTypes']['fromLeft']:
      startX = 0 - sp.extX
      startY = endY
      break
    case EntranceAnimationTypes['flyIn']['subTypes']['fromRight']:
      startX = presentation.width
      startY = endY
      break
    case EntranceAnimationTypes['flyIn']['subTypes']['fromTopLeft']:
      startX = 0 - sp.extX
      startY = 0 - sp.extY
      break
    case EntranceAnimationTypes['flyIn']['subTypes']['fromTopRight']:
      startX = presentation.width
      startY = 0 - sp.extY
      break
    case EntranceAnimationTypes['flyIn']['subTypes']['fromBottomLeft']:
      startX = 0 - sp.extX
      startY = presentation.height
      break
    case EntranceAnimationTypes['flyIn']['subTypes']['fromBottomRight']:
      startX = presentation.width
      startY = presentation.height
  }
  const deltaX = (startX - endX) * (1 - percentage)
  const deltaY = (startY - endY) * (1 - percentage)
  return { x: deltaX, y: deltaY }
}

/** 数据层为 #ppt_y+.1，偏移🈯️为 0.1 * presentationHeight */
export function getFloatInUpdateInfo(
  sp: SlideElement,
  percentage: number,
  direction: (typeof FloatInDirection)[keyof typeof FloatInDirection],
  presentation: Presentation
) {
  const endY = sp.y
  const startY =
    direction === FloatInDirection.up
      ? endY + presentation.height * 0.1
      : endY - presentation.height * 0.1
  const deltaY = (startY - endY) * (1 - percentage)
  return { y: deltaY, alpha: percentage }
}

export function getRotateUpdateInfo(
  percentage: number,
  angle: number,
  prevInfo: ShapeAnimationUpdateInfo
) {
  // 注意 rot 总是相对于起始值去计算, 单位为 1/360
  return (prevInfo.rot + percentage * angle) % 360
}

export function getScaleUpdateInfo(
  percentage: number,
  scaleInfo: { sx: number; sy: number },
  prevInfo: ShapeAnimationUpdateInfo
) {
  const { w: preSx, h: preSy } = prevInfo
  const { sx, sy } = scaleInfo
  const [startSx, startSy] = [sx * preSx, sy * preSy]
  const scaleX = preSx + (startSx - preSx) * percentage
  const scaleY = preSy + (startSy - preSy) * percentage
  return { w: scaleX, h: scaleY }
}

export function getFlyOutUpdateInfo(
  sp: SlideElement,
  precentage: number,
  subTypeId: (typeof EntranceAnimationTypes)['flyIn']['subTypes'][keyof (typeof EntranceAnimationTypes)['flyIn']['subTypes']],
  presentation: Presentation,
  prevInfo: ShapeAnimationUpdateInfo
) {
  const [originX, originY] = [sp.x, sp.y]
  const [preDeltaX, preDeltaY] = [prevInfo.x, prevInfo.y]
  const [startX, startY] = [originX + preDeltaX, originY + preDeltaY]
  let endX: number, endY: number
  switch (subTypeId) {
    case ExitAnimationTypes['flyOut']['subTypes']['toTop']:
      endX = startX
      endY = 0 - sp.extY
      break
    case ExitAnimationTypes['flyOut']['subTypes']['toBottom']:
      endX = startX
      endY = presentation.height
      break
    case ExitAnimationTypes['flyOut']['subTypes']['toLeft']:
      endX = 0 - sp.extX
      endY = startY
      break
    case ExitAnimationTypes['flyOut']['subTypes']['toRight']:
      endX = presentation.width
      endY = startY
      break
    case ExitAnimationTypes['flyOut']['subTypes']['toTopLeft']:
      endX = 0 - sp.extX
      endY = 0 - sp.extY
      break
    case ExitAnimationTypes['flyOut']['subTypes']['toTopRight']:
      endX = presentation.width
      endY = 0 - sp.extY
      break
    case ExitAnimationTypes['flyOut']['subTypes']['toBottomLeft']:
      endX = 0 - sp.extX
      endY = presentation.height
      break
    case ExitAnimationTypes['flyOut']['subTypes']['toBottomRight']:
      endX = presentation.width
      endY = presentation.height
  }
  const deltaX = (endX - startX) * precentage
  const deltaY = (endY - startY) * precentage
  return { x: preDeltaX + deltaX, y: preDeltaY + deltaY }
}

export function getFloatOutUpdateInfo(
  sp: SlideElement,
  percentage: number,
  direction: (typeof FloatOutDirection)[keyof typeof FloatOutDirection],
  presentation: Presentation
) {
  const startY = sp.y
  const endY =
    direction === FloatOutDirection.up
      ? startY - presentation.height * 0.1
      : startY + presentation.height * 0.1
  const deltaY = (endY - startY) * percentage
  return { y: deltaY, alpha: 1 - percentage }
}

function calTimeNodesAutoStart(timeNodes: TimeNode[]) {
  if (!timeNodes.length) return false
  return timeNodes[0].triggerType !== 'ClickEffect'
}

// helpers
function getPercentageForOneLoop(
  totalPercentage: number,
  loopCount: number
): number {
  if (loopCount <= 1) {
    return totalPercentage
  }
  if (totalPercentage >= 1.0 && loopCount > 1) {
    return 1.0
  }
  // 为了提高精度而使用大整数做基数
  const virtualDuration = REPEAT_COUNT_BASE * loopCount
  const virtualPassedTimeForOneLoop =
    (totalPercentage * virtualDuration) % REPEAT_COUNT_BASE
  return virtualPassedTimeForOneLoop / REPEAT_COUNT_BASE
}

export function deepCloneTimeNode(timeNode: TimeNode): TimeNode {
  return JSON.parse(JSON.stringify(timeNode))
}

export function timeNodesToAnimationSettings(
  slide: Slide,
  animationNodes: Nullable<TimeNode[]>
) {
  if (!animationNodes || animationNodes.length <= 0) return []
  const animationSettings: AnimationSetting[] = []
  for (const timeNode of animationNodes) {
    const setting = readTimeNodeToSetting(slide, timeNode)
    if (setting) animationSettings.push(setting)
  }
  return animationSettings
}

export function audioNodesToAnimationSettings(
  slide: Slide,
  audioNodes: AudioNode[]
) {
  if (!audioNodes || audioNodes.length <= 0) return []
  const animationSettings: AudioSetting[] = []
  for (const audioNode of audioNodes) {
    const setting = readAudioNodeToSetting(slide, audioNode)
    if (setting) animationSettings.push(setting)
  }
  return animationSettings
}

export function readAudioNodeToSetting(
  slide: Slide,
  audioNode: AudioNode
): AudioSetting | undefined {
  const spTree = slide.cSld.spTree
  const sp = spTree.find((sp) => sp.getRefId() === audioNode.spRefId)
  if (!sp) return
  const audioSetting: AudioSetting = {
    ['shapeId']: sp.getId(),
    ['acrossSlideNum']: isNumber(audioNode.numSlide) ? audioNode.numSlide : 1,
    ['triggerType']: audioNode.triggerType === 'ClickEffect' ? 'click' : 'auto',
    ['hideWhenShow']: audioNode.hideWhenShow,
    ['loopUntilStopped']: audioNode.loopUntilStopped,
    ['duration']: audioNode.duration,
    ['volume']: audioNode.volume,
  }
  return audioSetting
}

export function writeAudioNodeFromSetting(
  setting: AudioSetting
): AudioNode | undefined {
  const shapeId = setting['shapeId']
  const sp = EditorUtil.entityRegistry.getEntityById(shapeId) as SlideElement
  if (!sp || !isSlideElement(sp) || !isAudioImage(sp)) return
  const acrossSlideNum = setting['acrossSlideNum']
  const triggerType = setting['triggerType']
  const hideWhenShow = setting['hideWhenShow']
  const loopUntilStopped = setting['loopUntilStopped']
  const duration = setting['duration']
  const volume = setting['volume']
  const audioNode: AudioNode = {
    spRefId: sp.getRefId(),
    triggerType: triggerType === 'click' ? 'ClickEffect' : 'WithEffect',
    numSlide: 1 < acrossSlideNum ? acrossSlideNum : undefined,
    hideWhenShow,
    loopUntilStopped,
    duration: isNumber(duration) ? duration : 1,
    volume,
  }
  return audioNode
}

export function findSpTimeNodes(slide: Slide, sp: SlideElement) {
  const timeNodes = slide.animation.timeNodes
  return timeNodes.filter((node) => node.spRefId === sp.refId)
}

export function findSpAudioNode(slide: Slide, sp: SlideElement) {
  const audioNodes = slide.animation.audioNodes
  return audioNodes.find((node) => node.spRefId === sp.refId)
}
