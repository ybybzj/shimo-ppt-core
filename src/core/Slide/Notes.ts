//Temporary function

import { PlaceholderType, ShapeLocks } from '../SlideElement/const'
import { idGenerator } from '../../globals/IdGenerator'
import { EditActionFlag } from '../EditActionFlag'
import { isDict, isBool } from '../../common/utils'
import { TextListStyle } from '../textAttributes/TextListStyle'
import { NotesMaster } from './NotesMaster'
import { Slide } from './Slide'
import { gRefIdCounter, RefIdPrefixes } from '../../globals/RefIdCounter'
import { InstanceType, isInstanceTypeOf } from '../instanceTypes'
import { getPresentation } from '../utilities/finders'
import { hookManager, Hooks } from '../hooks'
import {
  isPlaceholder,
  isPlaceholderWithEmptyContent,
} from '../utilities/shape/asserts'
import {
  collectAllFontNamesForSlideElement,
  getPlaceholderType,
} from '../utilities/shape/getters'
import {
  setDeletedForSp,
  setParentForSlideElement,
} from '../utilities/shape/updates'
import { drawSlideElement } from '../render/drawSlideElement'
import { CSld } from './CSld'
import { BodyPr } from '../SlideElement/attrs/bodyPr'
import { ClrMap } from '../color/clrMap'
import { Ph } from '../SlideElement/attrs/ph'
import { UniNvPr, SpPr } from '../SlideElement/attrs/shapePrs'
import { informEntityChildrenChange } from '../calculation/informChanges/contentChange'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { Shape } from '../SlideElement/Shape'
import { cloneSlideElement } from '../utilities/shape/clone'
import { setLock } from '../utilities/shape/locks'
import { updateCalcStatusForNotes } from '../calculation/updateCalcStatus/notes'
import { SlideElement } from '../SlideElement/type'

export class Notes {
  readonly instanceType = InstanceType.Notes
  clrMap?: ClrMap
  cSld: CSld
  showMasterPhAnim: any
  showMasterSp: any
  slide: Slide | null
  master: NotesMaster | null
  id: string
  refId?: string
  constructor() {
    this.clrMap = undefined
    this.cSld = new CSld(this)
    this.showMasterPhAnim = null
    this.showMasterSp = null
    this.slide = null
    this.master = null
    this.id = idGenerator.newId()
  }
  getRefId(): string {
    const refId = this.refId ?? gRefIdCounter.newIDBy(RefIdPrefixes.Notes)
    this.refId = refId
    return refId
  }
  setRefId(rId: string) {
    this.refId = rId
  }

  getId() {
    return this.id
  }

  setClrMapOverride(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_NotesSetClrMap,
      this.clrMap,
      pr
    )
    this.clrMap = pr
    updateCalcStatusForNotes(this)
  }

  setShowMasterPhAnim(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_NotesSetShowMasterPhAnim,
      this.showMasterPhAnim,
      pr
    )
    this.showMasterPhAnim = pr
    updateCalcStatusForNotes(this)
  }

  setShowMasterSp(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_NotesSetShowMasterSp,
      this.showMasterSp,
      pr
    )
    this.showMasterSp = pr
    updateCalcStatusForNotes(this)
  }

  addToSpTreeAtIndex(index, obj) {
    const _index = Math.max(0, Math.min(index, this.cSld.spTree.length))
    informEntityChildrenChange(
      this,
      EditActionFlag.edit_NotesAddToSpTree,
      _index,
      [obj],
      true
    )
    this.cSld.addSpAtIndex(obj, _index)
    updateCalcStatusForNotes(this)
  }

  removeFromSpTreeAtIndex(index: number) {
    if (index > -1 && index < this.cSld.spTree.length) {
      informEntityChildrenChange(
        this,
        EditActionFlag.edit_NotesRemoveFromTree,
        index,
        this.cSld.removeSpFromIndex(index),
        false
      )
      updateCalcStatusForNotes(this)
    }
  }

  removeSpById(id) {
    for (let i = this.cSld.spTree.length - 1; i > -1; --i) {
      if (this.cSld.spTree[i].getId() === id) {
        this.removeFromSpTreeAtIndex(i)
      }
    }
  }

  changeBackground(bg) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_NotesSetBg,
      this.cSld.bg,
      bg
    )
    this.cSld.bg = bg
    updateCalcStatusForNotes(this)
  }

  setCSldName(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_NotesSetName,
      this.cSld.name,
      pr
    )
    this.cSld.name = pr
  }

  setSlide(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_NotesSetSlide,
      this.slide,
      pr
    )
    this.slide = pr
    updateCalcStatusForNotes(this)
  }

  setNotesMaster(pr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_NotesSetNotesMaster,
      this.master,
      pr
    )
    this.master = pr
    updateCalcStatusForNotes(this)
  }

  getShapeOfBodyPH() {
    const arrOfSpTree = this.cSld.spTree
    for (let i = 0; i < arrOfSpTree.length; ++i) {
      const sp = arrOfSpTree[i]
      if (isPlaceholder(sp) && sp.instanceType === InstanceType.Shape) {
        if (getPlaceholderType(sp) === PlaceholderType.body) {
          return sp
        }
      }
    }
    return null
  }

  draw(graphics) {
    const arrOfSpTree = this.cSld.spTree
    for (let i = 0; i < arrOfSpTree.length; ++i) {
      const sp = arrOfSpTree[i]
      if (isPlaceholder(sp)) {
        if (getPlaceholderType(sp) === PlaceholderType.body) {
          drawSlideElement(sp, graphics)
          return
        }
      }
    }
  }

  getAllFonts(fonts) {
    let i
    for (i = 0; i < this.cSld.spTree.length; ++i) {
      collectAllFontNamesForSlideElement(this.cSld.spTree[i], fonts)
    }
  }

  getTheme() {
    return this.master!.theme
  }

  clone(map?) {
    map = map || {}
    const newNotes = new Notes()
    if (this.clrMap) {
      newNotes.setClrMapOverride(this.clrMap.clone())
    }

    if (typeof this.cSld.name === 'string' && this.cSld.name.length > 0) {
      newNotes.setCSldName(this.cSld.name)
    }
    if (this.cSld.bg) {
      newNotes.changeBackground(this.cSld.bg.clone())
    }
    for (let i = 0; i < this.cSld.spTree.length; ++i) {
      let copiedSp: SlideElement
      if (this.cSld.spTree[i].instanceType === InstanceType.GroupShape) {
        copiedSp = cloneSlideElement(this.cSld.spTree[i], map)
      } else {
        copiedSp = cloneSlideElement(this.cSld.spTree[i])
      }
      if (isDict(map)) {
        map[this.cSld.spTree[i].id] = copiedSp.id
      }
      newNotes.addToSpTreeAtIndex(newNotes.cSld.spTree.length, copiedSp)
      setParentForSlideElement(
        newNotes.cSld.spTree[newNotes.cSld.spTree.length - 1],
        newNotes
      )
    }
    if (isBool(this.showMasterPhAnim)) {
      newNotes.setShowMasterPhAnim(this.showMasterPhAnim)
    }
    if (isBool(this.showMasterSp)) {
      newNotes.setShowMasterSp(this.showMasterSp)
    }
    newNotes.setNotesMaster(this.master)

    return newNotes
  }

  isEmptyNotes() {
    if (this.refId != null) {
      return false
    }
    return this.isEmpty()
  }

  isEmpty() {
    const bodyShape = this.getShapeOfBodyPH()
    if (!bodyShape) {
      return true
    }
    return isPlaceholderWithEmptyContent(bodyShape)
  }

  onUpdateSlide() {
    const presentation = getPresentation(this)!
    if (this.slide) {
      if (presentation.currentSlideIndex === this.slide.index) {
        hookManager.invoke(Hooks.EditorUI.OnCalculateNotes, {
          slideIndex: this.slide.index,
          notesWidth: this.slide.notesWidth,
          notesHeight: this.slide.getNotesShapeHeight(),
        })
      }
    }
  }

  getSpTree() {
    const ret: any[] = []
    const bodyShape = this.getShapeOfBodyPH()
    if (bodyShape && isInstanceTypeOf(bodyShape, InstanceType.Shape)) {
      ret.push(bodyShape)
    }
    return ret
  }
}

export function createNotes() {
  const newNotes = new Notes()
  let shape = new Shape()
  setDeletedForSp(shape, false)
  let nvSpPr = new UniNvPr(shape)
  let cNvPr = nvSpPr.cNvPr
  cNvPr.setId(2)
  cNvPr.setName('Slide Image Placeholder 1')
  let ph = Ph.new({
    type: PlaceholderType.sldImg,
  })
  nvSpPr.nvPr.setPh(ph)
  shape.setNvSpPr(nvSpPr)
  setLock(shape, ShapeLocks.noGrp, true)
  setLock(shape, ShapeLocks.noRot, true)
  setLock(shape, ShapeLocks.noModifyAspect, true)
  shape.setSpPr(new SpPr())
  shape.spPr!.setParent(shape)
  shape.setParent(newNotes)
  shape.spTreeParent = newNotes.cSld
  newNotes.addToSpTreeAtIndex(0, shape)

  shape = new Shape()
  setDeletedForSp(shape, false)
  nvSpPr = new UniNvPr(shape)
  cNvPr = nvSpPr.cNvPr
  cNvPr.setId(3)
  cNvPr.setName('Notes Placeholder 2')
  ph = Ph.new({
    type: PlaceholderType.body,
    idx: 1 + '',
  })

  nvSpPr.nvPr.setPh(ph)
  shape.setNvSpPr(nvSpPr)
  setLock(shape, ShapeLocks.noGrp, true)
  shape.setSpPr(new SpPr())
  shape.spPr!.setParent(shape)
  shape.createTxBody()
  let bodyPr = new BodyPr()
  shape.txBody!.setBodyPr(bodyPr)
  let txLstStyle = new TextListStyle()
  shape.txBody!.setTextListStyle(txLstStyle)
  shape.setParent(newNotes)
  shape.spTreeParent = newNotes.cSld
  newNotes.addToSpTreeAtIndex(1, shape)

  shape = new Shape()
  setDeletedForSp(shape, false)
  nvSpPr = new UniNvPr(shape)
  cNvPr = nvSpPr.cNvPr
  cNvPr.setId(4)
  cNvPr.setName('Slide Number Placeholder 3')
  ph = Ph.new({
    type: PlaceholderType.sldNum,
    sz: 2,
    idx: 10 + '',
  })

  nvSpPr.nvPr.setPh(ph)
  shape.setNvSpPr(nvSpPr)
  setLock(shape, ShapeLocks.noGrp, true)
  shape.setSpPr(new SpPr())
  shape.spPr!.setParent(shape)
  shape.createTxBody()
  bodyPr = new BodyPr()
  shape.txBody!.setBodyPr(bodyPr)
  txLstStyle = new TextListStyle()
  shape.txBody!.setTextListStyle(txLstStyle)
  shape.setParent(newNotes)
  shape.spTreeParent = newNotes.cSld
  newNotes.addToSpTreeAtIndex(2, shape)
  return newNotes
}
