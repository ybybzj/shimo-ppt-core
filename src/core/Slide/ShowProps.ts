import { Nullable } from '../../../liber/pervasive'
import { isDict, isNumber, isBool } from '../../common/utils'
import { InstanceType } from '../instanceTypes'
import { CShowPrData } from '../../io/dataType/presentation'
import { assignVal, assignJSON, readFromJSON } from '../../io/utils'
import { CComplexColor } from '../color/complexColor'

export class ShowProps {
  readonly instanceType = InstanceType.ShowPr
  browse?: boolean
  kiosk?: { restart?: number }
  penClr?: Nullable<CComplexColor>
  present: boolean
  show?: {
    showAll: boolean
    range?: { start: number; end: number }
    custShow?: number
  }
  loop?: boolean
  showAnimation?: boolean
  showNarration?: boolean
  useTimings?: boolean
  range: any
  constructor() {
    this.present = false
  }

  toJSON(): CShowPrData {
    const data: CShowPrData = { ['present']: this.present }
    assignVal(data, 'browse', this.browse)

    if (isDict(this.kiosk) && isNumber(this.kiosk!.restart)) {
      data['kiosk'] = { ['restart']: this.kiosk!.restart }
    }

    assignJSON(data, 'penClr', this.penClr)

    if (isDict(this.show)) {
      data['show'] = { ['showAll']: this.show!.showAll }
      if (!data['show']['showAll']) {
        if (this.show!.range) {
          data['show']['range'] = {
            ['start']: this.show!.range.start,
            ['end']: this.show!.range.end,
          }
        } else if (isNumber(this.show!.custShow)) {
          data['show']['custShow'] = this.show!.custShow
        }
      }
    }

    if (isBool(this.loop)) {
      data['loop'] = this.loop
    }
    if (isBool(this.showAnimation)) {
      data['showAnimation'] = this.showAnimation
    }
    if (isBool(this.showNarration)) {
      data['showNarration'] = this.showNarration
    }
    if (isBool(this.useTimings)) {
      data['useTimings'] = this.useTimings
    }
    return data
  }

  fromJSON(data: CShowPrData) {
    if (data['present'] != null) {
      this.present = data['present']
    }
    if (isBool(data['browse'])) {
      this.browse = data['browse']
    }
    if (isDict(data['kiosk'])) {
      if (isNumber(data['kiosk']!['restart'])) {
        this.kiosk = { restart: data['kiosk']!['restart'] }
      }
    }

    if (isDict(data['penClr'])) {
      this.penClr = readFromJSON(data['penClr']!, CComplexColor)
    }

    if (isDict(data['show'])) {
      this.show = data['show']
    }

    if (isBool(data['loop'])) {
      this.loop = data['loop']
    }
    if (isBool(data['showAnimation'])) {
      this.showAnimation = data['showAnimation']
    }
    if (isBool(data['showNarration'])) {
      this.showNarration = data['showNarration']
    }
    if (isBool(data['useTimings'])) {
      this.useTimings = data['useTimings']
    }
  }

  clone() {
    const dup = new ShowProps()
    dup.browse = this.browse
    if (isDict(this.kiosk)) {
      dup.kiosk = {}
      if (isBool(this.kiosk!.restart)) {
        dup.kiosk.restart = this.kiosk!.restart
      }
    }
    if (this.penClr) {
      dup.penClr = this.penClr.clone()
    }
    dup.present = this.present
    if (isDict(this.show)) {
      dup.show = { showAll: this.show!.showAll }

      if (isDict(this.show!.range)) {
        dup.show.range = {
          start: this.show!.range!.start,
          end: this.show!.range!.end,
        }
      } else if (isNumber(this.show!.custShow)) {
        dup.show.custShow = this.show!.custShow
      }
    }
    dup.loop = this.loop
    dup.showAnimation = this.showAnimation
    dup.showNarration = this.showNarration
    dup.useTimings = this.useTimings
    return dup
  }
}
