/* eslint-disable no-unused-expressions */
import { Dict, Nullable } from '../../../liber/pervasive'
import { ScaleOfPPTXSizes } from '../common/const/unit'
import { FontStyleInd, PresetColorValues } from '../SlideElement/const'
import { FillEffects } from '../SlideElement/attrs/fill/fill'
import { CComplexColor } from '../color/complexColor'
import { FontRef } from '../SlideElement/attrs/refs'
import { ThemeColor, ThemeColorValue } from '../../io/dataType/spAttrs'
import { TablePartStyleOptions } from '../Table/attrs/TablePartStyle'
import { TableStyles } from '../Table/attrs/TableStyles'
import { TableStyle } from '../Table/attrs/TableStyle'
import { TCBorderValue } from '../common/const/table'

function createPrstComplexColor(prst: PresetColorValues) {
  return CComplexColor.Prst(prst)
}
/**
 * ooxml标准中内置 Table Style
 * https://docs.microsoft.com/en-us/openspecs/office_standards/ms-oe376/d1f27e91-0523-459b-bc14-ba61b29e95e6
 */
const BuiltInTableStyleNameIdMap = {
  'No Style, No Grid': '{2D5ABB26-0587-4C30-8999-92F81FD0307C}',
  'Themed Style 1 - Accent 1': '{3C2FFA5D-87B4-456A-9821-1D502468CF0F}',
  'Themed Style 1 - Accent 2': '{284E427A-3D55-4303-BF80-6455036E1DE7}',
  'Themed Style 1 - Accent 3': '{69C7853C-536D-4A76-A0AE-DD22124D55A5}',
  'Themed Style 1 - Accent 4': '{775DCB02-9BB8-47FD-8907-85C794F793BA}',
  'Themed Style 1 - Accent 5': '{35758FB7-9AC5-4552-8A53-C91805E547FA}',
  'Themed Style 1 - Accent 6': '{08FB837D-C827-4EFA-A057-4D05807E0F7C}',
  'No Style, Table Grid': '{5940675A-B579-460E-94D1-54222C63F5DA}',
  'Themed Style 2 - Accent 1': '{D113A9D2-9D6B-4929-AA2D-F23B5EE8CBE7}',
  'Themed Style 2 - Accent 2': '{18603FDC-E32A-4AB5-989C-0864C3EAD2B8}',
  'Themed Style 2 - Accent 3': '{306799F8-075E-4A3A-A7F6-7FBC6576F1A4}',
  'Themed Style 2 - Accent 4': '{E269D01E-BC32-4049-B463-5C60D7B0CCD2}',
  'Themed Style 2 - Accent 5': '{327F97BB-C833-4FB7-BDE5-3F7075034690}',
  'Themed Style 2 - Accent 6': '{638B1855-1B75-4FBE-930C-398BA8C253C6}',
  'Light Style 1': '{9D7B26C5-4107-4FEC-AEDC-1716B250A1EF}',
  'Light Style 1 - Accent 1': '{3B4B98B0-60AC-42C2-AFA5-B58CD77FA1E5}',
  'Light Style 1 - Accent 2': '{0E3FDE45-AF77-4B5C-9715-49D594BDF05E}',
  'Light Style 1 - Accent 3': '{C083E6E3-FA7D-4D7B-A595-EF9225AFEA82}',
  'Light Style 1 - Accent 4': '{D27102A9-8310-4765-A935-A1911B00CA55}',
  'Light Style 1 - Accent 5': '{5FD0F851-EC5A-4D38-B0AD-8093EC10F338}',
  'Light Style 1 - Accent 6': '{68D230F3-CF80-4859-8CE7-A43EE81993B5}',
  'Light Style 2': '{7E9639D4-E3E2-4D34-9284-5A2195B3D0D7}',
  'Light Style 2 - Accent 1': '{69012ECD-51FC-41F1-AA8D-1B2483CD663E}',
  'Light Style 2 - Accent 2': '{72833802-FEF1-4C79-8D5D-14CF1EAF98D9}',
  'Light Style 2 - Accent 3': '{F2DE63D5-997A-4646-A377-4702673A728D}',
  'Light Style 2 - Accent 4': '{17292A2E-F333-43FB-9621-5CBBE7FDCDCB}',
  'Light Style 2 - Accent 5': '{5A111915-BE36-4E01-A7E5-04B1672EAD32}',
  'Light Style 2 - Accent 6': '{912C8C85-51F0-491E-9774-3900AFEF0FD7}',
  'Light Style 3': '{616DA210-FB5B-4158-B5E0-FEB733F419BA}',
  'Light Style 3 - Accent 1': '{BC89EF96-8CEA-46FF-86C4-4CE0E7609802}',
  'Light Style 3 - Accent 2': '{5DA37D80-6434-44D0-A028-1B22A696006F}',
  'Light Style 3 - Accent 3': '{8799B23B-EC83-4686-B30A-512413B5E67A}',
  'Light Style 3 - Accent 4': '{ED083AE6-46FA-4A59-8FB0-9F97EB10719F}',
  'Light Style 3 - Accent 5': '{BDBED569-4797-4DF1-A0F4-6AAB3CD982D8}',
  'Light Style 3 - Accent 6': '{E8B1032C-EA38-4F05-BA0D-38AFFFC7BED3}',
  'Medium Style 1': '{793D81CF-94F2-401A-BA57-92F5A7B2D0C5}',
  'Medium Style 1 - Accent 1': '{B301B821-A1FF-4177-AEE7-76D212191A09}',
  'Medium Style 1 - Accent 2': '{9DCAF9ED-07DC-4A11-8D7F-57B35C25682E}',
  'Medium Style 1 - Accent 3': '{1FECB4D8-DB02-4DC6-A0A2-4F2EBAE1DC90}',
  'Medium Style 1 - Accent 4': '{1E171933-4619-4E11-9A3F-F7608DF75F80}',
  'Medium Style 1 - Accent 5': '{FABFCF23-3B69-468F-B69F-88F6DE6A72F2}',
  'Medium Style 1 - Accent 6': '{10A1B5D5-9B99-4C35-A422-299274C87663}',
  'Medium Style 2': '{073A0DAA-6AF3-43AB-8588-CEC1D06C72B9}',
  'Medium Style 2 - Accent 1': '{5C22544A-7EE6-4342-B048-85BDC9FD1C3A}',
  'Medium Style 2 - Accent 2': '{21E4AEA4-8DFA-4A89-87EB-49C32662AFE0}',
  'Medium Style 2 - Accent 3': '{F5AB1C69-6EDB-4FF4-983F-18BD219EF322}',
  'Medium Style 2 - Accent 4': '{00A15C55-8517-42AA-B614-E9B94910E393}',
  'Medium Style 2 - Accent 5': '{7DF18680-E054-41AD-8BC1-D1AEF772440D}',
  'Medium Style 2 - Accent 6': '{93296810-A885-4BE3-A3E7-6D5BEEA58F35}',
  'Medium Style 3': '{8EC20E35-A176-4012-BC5E-935CFFF8708E}',
  'Medium Style 3 - Accent 1': '{6E25E649-3F16-4E02-A733-19D2CDBF48F0}',
  'Medium Style 3 - Accent 2': '{85BE263C-DBD7-4A20-BB59-AAB30ACAA65A}',
  'Medium Style 3 - Accent 3': '{EB344D84-9AFB-497E-A393-DC336BA19D2E}',
  'Medium Style 3 - Accent 4': '{EB9631B5-78F2-41C9-869B-9F39066F8104}',
  'Medium Style 3 - Accent 5': '{74C1A8A3-306A-4EB7-A6B1-4F7E0EB9C5D6}',
  'Medium Style 3 - Accent 6': '{2A488322-F2BA-4B5B-9748-0D474271808F}',
  'Medium Style 4': '{D7AC3CCA-C797-4891-BE02-D94E43425B78}',
  'Medium Style 4 - Accent 1': '{69CF1AB2-1976-4502-BF36-3FF5EA218861}',
  'Medium Style 4 - Accent 2': '{8A107856-5554-42FB-B03E-39F5DBC370BA}',
  'Medium Style 4 - Accent 3': '{0505E3EF-67EA-436B-97B2-0124C06EBD24}',
  'Medium Style 4 - Accent 4': '{C4B1156A-380E-4F78-BDF5-A606A8083BF9}',
  'Medium Style 4 - Accent 5': '{22838BEF-8BB2-4498-84A7-C5851F593DF1}',
  'Medium Style 4 - Accent 6': '{16D9F66E-5EB9-4882-86FB-DCBF35E3C3E4}',
  'Dark Style 1': '{E8034E78-7F5D-4C2E-B375-FC64B27BC917}',
  'Dark Style 1 - Accent 1': '{125E5076-3810-47DD-B79F-674D7AD40C01}',
  'Dark Style 1 - Accent 2': '{37CE84F3-28C3-443E-9E96-99CF82512B78}',
  'Dark Style 1 - Accent 3': '{D03447BB-5D67-496B-8E87-E561075AD55C}',
  'Dark Style 1 - Accent 4': '{E929F9F4-4A8F-4326-A1B4-22849713DDAB}',
  'Dark Style 1 - Accent 5': '{8FD4443E-F989-4FC4-A0C8-D5A2AF1F390B}',
  'Dark Style 1 - Accent 6': '{AF606853-7671-496A-8E4F-DF71F8EC918B}',
  'Dark Style 2': '{5202B0CA-FC54-4496-8BCA-5EF66A818D29}',
  'Dark Style 2 – Accent 1/Accent2': '{0660B408-B3CF-4A94-85FC-2B1E0A45F4A2}',
  'Dark Style 2 – Accent 3/Accent4': '{91EBBBCC-DAD2-459C-BE2E-F6DE35CF9A28}',
  'Dark Style 2 – Accent 5/Accent6': '{46F890A9-2807-4EBB-B81D-B2AA78EC7F39}',
}

function createDefaultTableStylesOfPresentation(styles: TableStyles) {
  let style: TableStyle

  const defStyle = createMediumStyleTwo(ThemeColor.Accent1)
  styles.add(defStyle)

  const defNoStyle = createNoStyleTwo(ThemeColor.Dark1)
  styles.add(defNoStyle)

  style = createNoStyleOne(ThemeColor.Dark1)
  styles.add(style)

  for (let i = 0; i < 6; ++i) {
    style = createThemedStyleOne(i as ThemeColorValue)
    styles.add(style)
  }

  for (let i = 0; i < 6; ++i) {
    style = createThemedStyleTwo(i as ThemeColorValue)
    styles.add(style)
  }

  style = createLightStyleOne(ThemeColor.Dark1)
  styles.add(style)

  for (let i = 0; i < 6; ++i) {
    style = createLightStyleOne(i as ThemeColorValue)
    styles.add(style)
  }
  style = createLightStyleTwo(ThemeColor.Dark1)
  styles.add(style)

  for (let i = 0; i < 6; ++i) {
    style = createLightStyleTwo(i as ThemeColorValue)
    styles.add(style)
  }
  style = createLightStyleThree(ThemeColor.Dark1)
  styles.add(style)

  for (let i = 0; i < 6; ++i) {
    style = createLightStyleThree(i as ThemeColorValue)
    styles.add(style)
  }

  style = createMediumStyleOne(ThemeColor.Dark1)
  styles.add(style)

  for (let i = 0; i < 6; ++i) {
    style = createMediumStyleOne(i as ThemeColorValue)
    styles.add(style)
  }
  style = createMediumStyleTwo(ThemeColor.Dark1)
  styles.add(style)

  for (let i = 1; i < 6; ++i) {
    style = createMediumStyleTwo(i as ThemeColorValue)
    styles.add(style)
  }
  style = createMediumStyleThree(ThemeColor.Dark1)
  styles.add(style)

  for (let i = 0; i < 6; ++i) {
    style = createMediumStyleThree(i as ThemeColorValue)
    styles.add(style)
  }
  style = createMediumStyleFour(ThemeColor.Dark1)
  styles.add(style)

  for (let i = 0; i < 6; ++i) {
    style = createMediumStyleFour(i as ThemeColorValue)
    styles.add(style)
  }

  style = createBlackStyleOne(ThemeColor.Accent3)
  styles.add(style)

  for (let i = 0; i < 6; ++i) {
    style = createDarkStyleOne(i as ThemeColorValue)
    styles.add(style)
  }

  for (let i = 0; i < 3; i++) {
    style = createDarkStyleTwo((2 * i) as ThemeColorValue)
    styles.add(style)
  }

  return {
    defStyleId: defStyle.id,
    defNoStyleId: defNoStyle.id,
  }
}

function getTableStyleIdByName(name: string) {
  return BuiltInTableStyleNameIdMap[name] || null
}

export interface TablePrProps {
  tableBorders?: TableBordersProps
}

interface TableBordersProps {
  left: TableBorderProps
  right: TableBorderProps
  top: TableBorderProps
  bottom: TableBorderProps
  insideH: TableBorderProps
  insideV: TableBorderProps
}

interface TableBorderProps {
  color?: { r; g; b }
  fillEffects?: FillEffects
  space?: number
  size?: number
  value: (typeof TCBorderValue)[keyof typeof TCBorderValue]
}

function createThemedStyleOne(schemeId: ThemeColorValue) {
  let name
  if (schemeId === 8) {
    name = 'Themed Style 1'
  } else {
    name = 'Themed Style 1 - Accent ' + (schemeId + 1)
  }
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideV: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0.6),
      },
    },
  })

  let tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0.8),
      },
    },
  }

  style.tableBand1Horz.fromOptions(tblStyleData)
  style.tableBand1Vert.fromOptions(tblStyleData)

  tblStyleData = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {},
  }
  style.tableLastCol.fromOptions(tblStyleData)
  style.tableFirstCol.fromOptions(tblStyleData)

  const lastRowStyleData: TablePartStyleOptions = {
    tableCellPr: {},
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
  }
  lastRowStyleData.tableCellPr!.tableCellBorders = {
    right: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.None,
    },
    left: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.None,
    },
  }
  style.tableLastRow.fromOptions(lastRowStyleData)

  tblStyleData.tableCellPr!.shd = {
    fillEffects: FillEffects.SolidScheme(schemeId, 0),
  }
  tblStyleData.tableCellPr!.tableCellBorders = {
    bottom: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  tblStyleData.textPr = {
    bold: true,
    fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
    fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
  }
  style.tableFirstRow.fromOptions(tblStyleData)
  return style
}

function createThemedStyleTwo(schemeId: ThemeColorValue) {
  let name
  if (schemeId === 8) {
    name = 'Themed Style 2'
  } else {
    name = 'Themed Style 2 - Accent ' + (schemeId + 1)
  }
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      insideV: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
      },
    },
  })

  let tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0.8),
      },
    },
  }
  style.tableBand1Horz.fromOptions(tblStyleData)
  style.tableBand1Vert.fromOptions(tblStyleData)

  tblStyleData = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
    },
    tableCellPr: {},
  }
  style.tableLastCol.fromOptions(tblStyleData)
  style.tableFirstCol.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.shd = {
    fillEffects: FillEffects.SolidScheme(schemeId, 0),
  }
  tblStyleData.tableCellPr!.tableCellBorders = {
    top: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }

  style.tableLastRow.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    bottom: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  tblStyleData.textPr = {
    bold: true,
    fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
    fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
  }
  style.tableFirstRow.fromOptions(tblStyleData)
  return style
}

function createLightStyleOne(schemeId: ThemeColorValue) {
  let name
  if (schemeId === 8) {
    name = 'Light Style 1'
  } else {
    name = 'Light Style 1 - Accent ' + (schemeId + 1)
  }
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      insideV: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      },
    },
  })

  let tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0.4),
      },
    },
  }
  style.tableBand1Horz.fromOptions(tblStyleData)
  style.tableBand1Vert.fromOptions(tblStyleData)

  tblStyleData = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {},
  }
  style.tableLastCol.fromOptions(tblStyleData)
  style.tableFirstCol.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    top: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(schemeId, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }

  style.tableLastRow.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    bottom: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(schemeId, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  tblStyleData.textPr = {
    bold: true,
    fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
    fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
  }
  tblStyleData.tableCellPr!.shd = {
    fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
  }
  style.tableFirstRow.fromOptions(tblStyleData)
  return style
}

function createLightStyleTwo(schemeId: ThemeColorValue) {
  let name
  if (schemeId === 8) {
    name = 'Light Style 2'
  } else {
    name = 'Light Style 2 - Accent ' + (schemeId + 1)
  }
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      insideV: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      },
    },
  })
  let tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      },
      tableCellBorders: {
        top: {
          color: { r: 0, g: 0, b: 0 },
          fillEffects: FillEffects.SolidScheme(schemeId, 0),
          space: 0,
          size: 12700 / ScaleOfPPTXSizes,
          value: TCBorderValue.Single,
        },
        bottom: {
          color: { r: 0, g: 0, b: 0 },
          fillEffects: FillEffects.SolidScheme(schemeId, 0),
          space: 0,
          size: 12700 / ScaleOfPPTXSizes,
          value: TCBorderValue.Single,
        },
      },
    },
  }
  const styleTableBand1Vertical: TablePartStyleOptions = {
    tableCellPr: {
      tableCellBorders: {
        left: {
          color: { r: 0, g: 0, b: 0 },
          fillEffects: FillEffects.SolidScheme(schemeId, 0),
          space: 0,
          size: 12700 / ScaleOfPPTXSizes,
          value: TCBorderValue.Single,
        },
        right: {
          color: { r: 0, g: 0, b: 0 },
          fillEffects: FillEffects.SolidScheme(schemeId, 0),
          space: 0,
          size: 12700 / ScaleOfPPTXSizes,
          value: TCBorderValue.Single,
        },
      },
    },
  }
  style.tableBand1Horz.fromOptions(tblStyleData)
  style.tableBand1Vert.fromOptions(styleTableBand1Vertical)

  tblStyleData = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {},
  }
  style.tableLastCol.fromOptions(tblStyleData)
  style.tableFirstCol.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    top: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(schemeId, 0),
      space: 0,
      size: 38100 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  tblStyleData.tableCellPr!.shd = {
    fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
  }
  style.tableLastRow.fromOptions(tblStyleData)
  tblStyleData.tableCellPr!.tableCellBorders = {
    bottom: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(schemeId, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  tblStyleData.textPr = {
    bold: true,
    fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
    fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
  }

  tblStyleData.tableCellPr!.shd = {
    fillEffects: FillEffects.SolidScheme(schemeId, 0),
  }

  style.tableFirstRow.fromOptions(tblStyleData)
  return style
}

function createLightStyleThree(schemeId: ThemeColorValue) {
  let name
  if (schemeId === 8) {
    name = 'Light Style 3'
  } else {
    name = 'Light Style 3 - Accent ' + (schemeId + 1)
  }
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideV: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      },
    },
  })
  let tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0.2),
      },
    },
  }
  style.tableBand1Horz.fromOptions(tblStyleData)
  style.tableBand1Vert.fromOptions(tblStyleData)

  tblStyleData = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {},
  }
  style.tableLastCol.fromOptions(tblStyleData)
  style.tableFirstCol.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    top: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(schemeId, 0),
      space: 0,
      size: 38100 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }

  tblStyleData.tableCellPr!.shd = {
    fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
  }

  style.tableLastRow.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    bottom: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(schemeId, 0),
      space: 0,
      size: 38100 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }

  tblStyleData.textPr = {
    bold: true,
    fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
    fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
  }
  style.tableFirstRow.fromOptions(tblStyleData)
  return style
}

function createMediumStyleOne(schemeId: ThemeColorValue) {
  let name
  if (schemeId === 8) {
    name = 'Medium Style 1'
  } else {
    name = 'Medium Style 1 - Accent ' + (schemeId + 1)
  }
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideV: {
        value: TCBorderValue.None,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      },
    },
  })
  let tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0.2),
      },
    },
  }
  style.tableBand1Horz.fromOptions(tblStyleData)
  style.tableBand1Vert.fromOptions(tblStyleData)

  tblStyleData = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {},
  }
  style.tableLastCol.fromOptions(tblStyleData)
  style.tableFirstCol.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    top: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(schemeId, 0),
      space: 0,
      size: 38100 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  tblStyleData.tableCellPr!.shd = {
    fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
  }
  style.tableLastRow.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    top: {
      value: TCBorderValue.None,
    },
  }
  tblStyleData.tableCellPr!.shd = {
    fillEffects: FillEffects.SolidScheme(schemeId, 0),
  }
  tblStyleData.tableCellPr!.tableCellBorders = {
    bottom: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(schemeId, 0),
      space: 0,
      size: 38100 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  tblStyleData.textPr = {
    bold: true,
    fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
    fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
  }
  style.tableFirstRow.fromOptions(tblStyleData)
  return style
}

function createMediumStyleTwo(schemeId: ThemeColorValue) {
  let name
  if (schemeId === ThemeColor.Dark1) {
    name = 'Medium Style 2'
  } else {
    name = 'Medium Style 2 - Accent ' + (schemeId + 1)
  }
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideV: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0.2),
      },
    },
  })
  let tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0.4),
      },
    },
  }
  style.tableBand1Horz.fromOptions(tblStyleData)
  style.tableBand1Vert.fromOptions(tblStyleData)

  tblStyleData = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
      },
    },
  }

  style.tableLastCol.fromOptions(tblStyleData)
  style.tableFirstCol.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    top: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      space: 0,
      size: 38100 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }

  style.tableLastRow.fromOptions(tblStyleData)
  tblStyleData.tableCellPr!.tableCellBorders = {
    bottom: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      space: 0,
      size: 38100 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }

  style.tableFirstRow.fromOptions(tblStyleData)
  return style
}

function createMediumStyleThree(schemeId: ThemeColorValue) {
  let name
  if (schemeId === ThemeColor.Dark1) {
    name = 'Medium Style 3'
  } else {
    name = 'Medium Style 3 - Accent ' + (schemeId + 1)
  }
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
        space: 0,
        size: 38100 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
        space: 0,
        size: 38100 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      insideV: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      },
    },
  })
  let tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Accent3, 0.2),
      },
    },
  }
  style.tableBand1Horz.fromOptions(tblStyleData)
  style.tableBand1Vert.fromOptions(tblStyleData)

  tblStyleData = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
      },
    },
  }
  style.tableLastCol.fromOptions(tblStyleData)
  style.tableFirstCol.fromOptions(tblStyleData)
  tblStyleData.tableCellPr!.shd = {
    fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
  }
  tblStyleData.tableCellPr!.tableCellBorders = {
    top: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
      space: 0,
      size: 38100 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  tblStyleData.textPr = {
    bold: true,
    fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
    fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
  }

  style.tableLastRow.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    bottom: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
      space: 0,
      size: 38100 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  tblStyleData.textPr = {
    bold: true,
    fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
    fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
  }
  tblStyleData.tableCellPr!.shd = {
    fillEffects: FillEffects.SolidScheme(schemeId, 0),
  }
  style.tableFirstRow.fromOptions(tblStyleData)
  return style
}

function createMediumStyleFour(schemeId: ThemeColorValue) {
  let name
  if (schemeId === ThemeColor.Dark1) {
    name = 'Medium Style 4'
  } else {
    name = 'Medium Style 4 - Accent ' + (schemeId + 1)
  }
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideV: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0.2),
      },
    },
  })
  let tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0.4),
      },
    },
  }
  style.tableBand1Horz.fromOptions(tblStyleData)
  style.tableBand1Vert.fromOptions(tblStyleData)

  tblStyleData = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {},
  }

  style.tableFirstCol.fromOptions(tblStyleData)
  style.tableLastCol.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    top: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(schemeId, 0),
      space: 0,
      size: 38100 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  tblStyleData.tableCellPr!.shd = {
    fillEffects: FillEffects.SolidScheme(schemeId, 0.2),
  }
  style.tableLastRow.fromOptions(tblStyleData)
  tblStyleData.tableCellPr!.tableCellBorders = {
    bottom: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(schemeId, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  tblStyleData.tableCellPr!.shd = {
    fillEffects: FillEffects.SolidScheme(schemeId, 0.2),
  }
  tblStyleData.textPr = {
    bold: true,
    fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
    fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
  }
  style.tableFirstRow.fromOptions(tblStyleData)
  return style
}

function createNoStyleOne(schemeId) {
  const name = 'No Style, No Grid'
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      insideV: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      },
    },
  })
  let tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      },
    },
  }
  style.tableBand1Horz.fromOptions(tblStyleData)
  style.tableBand1Vert.fromOptions(tblStyleData)

  tblStyleData = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      },
    },
  }
  style.tableLastCol.fromOptions(tblStyleData)
  style.tableFirstCol.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    top: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.None,
    },
  }
  style.tableLastRow.fromOptions(tblStyleData)
  tblStyleData.tableCellPr!.tableCellBorders = {
    bottom: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(schemeId, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.None,
    },
  }
  tblStyleData.textPr = {
    bold: true,
    fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
    fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
  }
  style.tableFirstRow.fromOptions(tblStyleData)
  return style
}

function createDarkStyleOne(schemeId: ThemeColorValue) {
  let name
  if (schemeId === ThemeColor.Dark1) {
    name = 'Dark Style 1'
  } else {
    name = 'Dark Style 1 - Accent ' + (schemeId + 1)
  }
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideV: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
      },
    },
  })
  const tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, -0.6),
      },
    },
  }
  style.tableBand1Vert.fromOptions(tblStyleData)
  style.tableBand1Horz.fromOptions(tblStyleData)

  const firstLastColStyleData: TablePartStyleOptions = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, -0.6),
      },
      tableCellBorders: {
        right: {
          color: { r: 0, g: 0, b: 0 },
          fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
          space: 0,
          size: 38100 / ScaleOfPPTXSizes,
          value: TCBorderValue.Single,
        },
      },
    },
  }
  style.tableFirstCol.fromOptions(firstLastColStyleData)
  firstLastColStyleData.tableCellPr!.tableCellBorders = {
    left: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      space: 0,
      size: 38100 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
    right: {
      value: TCBorderValue.None,
    },
  }
  style.tableLastCol.fromOptions(firstLastColStyleData)

  const lastRowStyleData: TablePartStyleOptions = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, -0.4),
      },
    },
  }
  lastRowStyleData.tableCellPr!.tableCellBorders = {
    top: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      space: 0,
      size: 38100 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  style.tableLastRow.fromOptions(lastRowStyleData)

  const firstRowStyleData: TablePartStyleOptions = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
      },
      tableCellBorders: {
        bottom: {
          color: { r: 0, g: 0, b: 0 },
          fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
          space: 0,
          size: 38100 / ScaleOfPPTXSizes,
          value: TCBorderValue.Single,
        },
      },
    },
  }
  style.tableFirstRow.fromOptions(firstRowStyleData)
  return style
}

function createNoStyleTwo(schemeId: ThemeColorValue) {
  const name = 'No Style, Table Grid'
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },

      insideV: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.Single,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      },
    },
  })
  let tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      },
    },
  }
  style.tableBand1Horz.fromOptions(tblStyleData)
  style.tableBand1Vert.fromOptions(tblStyleData)

  tblStyleData = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      },
    },
  }
  style.tableLastCol.fromOptions(tblStyleData)
  style.tableFirstCol.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    top: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  style.tableLastRow.fromOptions(tblStyleData)
  tblStyleData.tableCellPr!.tableCellBorders = {
    bottom: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(schemeId, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  tblStyleData.textPr = {
    bold: true,
    fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
    fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
  }
  style.tableFirstRow.fromOptions(tblStyleData)
  return style
}

function createBlackStyleOne(schemeId: ThemeColorValue) {
  const name = 'Dark Style 1'
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      insideV: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0.2),
      },
    },
  })
  let tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0.4),
      },
    },
  }
  style.tableBand1Horz.fromOptions(tblStyleData)
  style.tableBand1Vert.fromOptions(tblStyleData)

  tblStyleData = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
      },
    },
  }
  style.tableLastCol.fromOptions(tblStyleData)
  style.tableFirstCol.fromOptions(tblStyleData)

  tblStyleData.tableCellPr!.tableCellBorders = {
    top: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(schemeId, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.None,
    },
  }
  style.tableLastRow.fromOptions(tblStyleData)
  tblStyleData.tableCellPr!.tableCellBorders = {
    bottom: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }
  tblStyleData.textPr = {
    bold: true,
    fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
    fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
  }
  style.tableFirstRow.fromOptions(tblStyleData)
  return style
}

function createDarkStyleTwo(schemeId: ThemeColorValue) {
  const name =
    'Dark Style 2 - Accent ' + (schemeId + 1) + '/' + 'Accent ' + (schemeId + 2)
  const guid = getTableStyleIdByName(name)
  const style = new TableStyle(name, guid)

  style.tablePr.fromOptions({
    tableBorders: {
      left: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      right: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      top: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      bottom: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      insideH: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },

      insideV: {
        color: { r: 0, g: 0, b: 0 },
        fillEffects: FillEffects.SolidScheme(schemeId, 0),
        space: 0,
        size: 12700 / ScaleOfPPTXSizes,
        value: TCBorderValue.None,
      },
    },
  })
  style.tableWholeTable.fromOptions({
    textPr: {
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0.2),
      },
    },
  })
  let tblStyleData: TablePartStyleOptions = {
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(schemeId, 0.4),
      },
    },
  }
  style.tableBand1Horz.fromOptions(tblStyleData)
  style.tableBand1Vert.fromOptions(tblStyleData)

  tblStyleData = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
    },
    tableCellPr: {
      shd: {
        fillEffects: FillEffects.SolidScheme(
          (schemeId + 1) as ThemeColorValue,
          0
        ),
      },
    },
  }
  const lastStyleData: TablePartStyleOptions = {
    textPr: {
      bold: true,
      fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
    },
    tableCellPr: {},
  }
  style.tableLastCol.fromOptions(lastStyleData)
  style.tableFirstCol.fromOptions(lastStyleData)

  lastStyleData.tableCellPr!.shd = {
    fillEffects: FillEffects.SolidScheme(schemeId, 0.2),
  }
  lastStyleData.tableCellPr!.tableCellBorders = {
    top: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Dark1, 0),
      space: 0,
      size: 38100 / ScaleOfPPTXSizes,
      value: TCBorderValue.Single,
    },
  }

  style.tableLastRow.fromOptions(lastStyleData)
  tblStyleData.tableCellPr!.tableCellBorders = {
    bottom: {
      color: { r: 0, g: 0, b: 0 },
      fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
      space: 0,
      size: 12700 / ScaleOfPPTXSizes,
      value: TCBorderValue.None,
    },
  }
  tblStyleData.textPr = {
    bold: true,
    fontRef: FontRef.new(FontStyleInd.minor, createPrstComplexColor('black')),
    fillEffects: FillEffects.SolidScheme(ThemeColor.Light1, 0),
  }
  style.tableFirstRow.fromOptions(tblStyleData)
  return style
}

class GlobalTableStylesManager {
  tableStylesInUseMap!: Dict<string>
  tableStyles!: TableStyles
  defaultIdOfTableStyle: Nullable<string>

  addTableStyle(style: TableStyle) {
    this.tableStyles.add(style)
  }

  init() {
    this.tableStylesInUseMap = {}
    this.tableStyles = new TableStyles()
    const { defStyleId, defNoStyleId } = createDefaultTableStylesOfPresentation(
      this.tableStyles
    )
    this.defaultIdOfTableStyle = defStyleId
    this.tableStyles.setDefaultTableStyleId(defNoStyleId)
  }

  reset() {
    this.defaultIdOfTableStyle = null
    this.tableStylesInUseMap = {}
  }

  setDefaultTableStyleId(guid: string) {
    const sid = this.tableStyles.getIdByGuid(guid)
    if (sid != null) this.defaultIdOfTableStyle = sid
  }

  setTableStylesInUseMap(id: string, guid: string) {
    if (this.getTableStyle(id)) {
      this.tableStylesInUseMap[id] = guid
    }
  }

  updateTableStylesInUseMap(newMap: Dict<string>) {
    this.tableStylesInUseMap = newMap
  }

  getTableStyle(id: string) {
    return this.tableStyles.Get(id)
  }
}

export const gGlobalTableStyles = new GlobalTableStylesManager()
