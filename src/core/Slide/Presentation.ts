import { Dict, Nullable } from '../../../liber/pervasive'
import { LineHeightRule, FillKIND, AlignKindValue } from '../common/const/attrs'
import { sortAscending, isNumber } from '../../common/utils'
import { idGenerator } from '../../globals/IdGenerator'
import { EditActionFlag } from '../EditActionFlag'
import { InstanceType, isInstanceTypeOf } from '../instanceTypes'
import { GraphicBounds, GraphicsBoundsChecker } from '../graphic/Bounds'
import { CanvasDrawer } from '../graphic/CanvasDrawer'
import { gDefaultColorMap } from '../color/clrMap'
import { FocusTarget } from '../common/const/drawing'
import { Factor_pix_to_mm } from '../common/const/unit'
import { EditorSettings } from '../common/EditorSettings'
import { checkAlmostEqual, normalizeRotValue } from '../common/utils'
import { hookManager, Hooks } from '../hooks'
import { ParaSpace } from '../Paragraph/RunElement/ParaSpace'
import { ParaText } from '../Paragraph/RunElement/ParaText'
import { ParaRun } from '../Paragraph/ParaContent/ParaRun'
import { PlaceholderType } from '../SlideElement/const'
import {
  buildImageShape,
  ImageShapeSubTypeMap,
  ImageShapeSubTypes,
} from '../utilities/shape/creators'
import { GraphicFrame } from '../SlideElement/GraphicFrame'
import { ParaPr } from '../textAttributes/ParaPr'
import { TextPr, TextPrOptions } from '../textAttributes/TextPr'
import { TextListStyle } from '../textAttributes/TextListStyle'
import { TableLook } from '../Table/common'
import { Table } from '../Table/Table'
import { Comment, ExtensionCmContentData } from './Comments/Comment'
import { PresProps } from './PresProps'
import { createNotes, Notes } from './Notes'
import { NotesMaster } from './NotesMaster'
import { Slide } from './Slide'
import { SlideMaster } from './SlideMaster'
import { gGlobalTableStyles } from './GlobalTableStyles'
import {
  goToSlide,
  emitEditorEvent,
  syncSelectionState,
} from '../utilities/shape/hookInvokers'
import { SlideLayout } from './SlideLayout'
import {
  isPlaceholder,
  isPlaceholderWithEmptyContent,
  isSlideElementInPPT,
} from '../utilities/shape/asserts'
import {
  getMatchedSlideElementForPh,
  getTextDocumentOfSp,
  getPlaceholderType,
  getIndexOfPlaceholder,
  getHierarchySps,
  isSpHasSingleBodyPh,
  getBoundsOfGroupingSps,
  collectCxnShapes,
  getUniNvPr,
} from '../utilities/shape/getters'
import {
  addToParentSpTree,
  setGroup,
  setDeletedForSp,
  clearSlideElementContent,
} from '../utilities/shape/updates'
import { searchManager } from '../search/SearchManager'
import { TextDocument } from '../TextDocument/TextDocument'
import { ShowProps } from './ShowProps'
import { EditorUtil } from '../../globals/editor'
import { CmContentData } from '../../io/dataType/comment'
import { SlideComments } from './Comments/SlideComments'
import { LoadableImage } from '../../images/utils'
import { getDefaultScaleUpdateInfo } from '../../re/export/Slide'
import { ImageShape } from '../SlideElement/Image'
import { SlideSelectManager } from './SelectManager'
import { SelectionState } from './SelectionState'
import { SlideElement } from '../SlideElement/type'
import { groupComparor } from '../utilities/shape/compare'
import {
  addNewParagraph,
  addItemToParagraph,
  clearFormattingForparagraph,
  pasteParagraphFormat,
  changeFontSizeParagraph,
  changeIndentLevelForParagraph,
  setAlignForParagraph,
  setIndentForParagraph,
  setParagraphNumbering,
  setSpacingForParagraph,
  setParagraphTextDirection,
  applyTextPrToParagraph,
} from '../utilities/presentation/docContent'
import {
  getSlideElementsForGrouping,
  createGrpSpForGrouping,
  getCurrentParaPr,
  getTextPrOfCurrentSelection,
  getThemeForEditContainer,
  getSlideElementsOfEditContainer,
} from '../utilities/presentation/getters'
import {
  applySlideElementProps,
  setTableProps,
} from '../utilities/presentation/propsAppliers'
import { Offset, Size } from '../common/types'
import { TableOrTextDocument, TextOperationDirection } from '../utilities/type'
import { TextContentDimensionLimit } from '../common/const/paragraph'

import {
  BulletListType,
  getBulletByListInfo,
} from '../SlideElement/attrs/bullet'
import { BlipFill } from '../SlideElement/attrs/fill/blipFill'
import { SpPr, Xfrm, UniNvPr } from '../SlideElement/attrs/shapePrs'
import { Theme, ClrScheme } from '../SlideElement/attrs/theme'
import { informEntityChildrenChange } from '../calculation/informChanges/contentChange'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { calculateForGraphicFrame } from '../calculation/calculate/graphicFrame'
import { cloneSlideElement } from '../utilities/shape/clone'
import { deleteSlideElementfromParent } from '../utilities/shape/delete'
import { calculateForRendering } from '../calculation/calculate/calculateForRendering'
import { calculateNotesShape } from '../calculation/calculate/slide'
import { startEditAction } from '../calculation/informChanges/startAction'
import {
  getAndUpdateCursorPositionForPresentation,
  getAndUpdateCursorPositionForTargetOrTextContent,
  calculateCursorPositionForParagraph,
} from '../utilities/cursor/cursorPosition'
import { moveCursorToStartPosInTable } from '../utilities/cursor/moveToEdgePos'
import { getCurrentParagraph } from '../utilities/tableOrTextDocContent/getter'
import { ensureSpPrXfrm } from '../utilities/shape/attrs'
import { GroupShape } from '../SlideElement/GroupShape'
import { TableProp } from '../properties/tableProp'
import { Bg } from '../SlideElement/attrs/bg'
import { Shape } from '../SlideElement/Shape'
import { EntityContentChange } from '../changes/contentChange'
import { updateCalcStatusForPresentation } from '../calculation/updateCalcStatus/presentation'
import { ThemeChanges } from '../changes/otherChanges'
import { adjustXfrmOfGroup, normalizeGroupExts } from '../utilities/shape/group'
import { PresentationSectionList } from './PrSection'
import { CalcStatus } from '../calculation/updateCalcStatus/calcStatus'
import { RelativeRect } from '../SlideElement/attrs/fill/RelativeRect'
import { RunContentElement } from '../Paragraph/RunElement/type'
import { defaultContentStylePrs } from '../textAttributes/ContentStyle'
import { SlideElementOptions } from '../properties/options'
import { PresetShapeTypes } from '../../exports/graphic'
import { TextAnchorTypeOO, TextVerticalTypeOO } from '../../io/dataType/spAttrs'
import { UploadUrlResult } from '../../editor/type'
import {
  getSlideElementFormat,
  SlideElementFormat,
} from '../utilities/shape/formatting'
import {
  InputCommentData,
  toLocalCmContentData,
  toServerCmContentData,
} from '../utilities/comments'
import { ParaSpacingOptions } from '../textAttributes/ParaSpacing'
import { ParaIndOptions } from '../textAttributes/ParaInd'

export class PrSection {
  readonly instanceType = InstanceType.PrSection
  name: any
  startIndex: any
  guid: any
  id: string
  constructor() {
    this.name = null
    this.startIndex = null
    this.guid = null
    this.id = idGenerator.newId()
  }

  getId() {
    return this.id
  }

  setName(pr) {
    this.name = pr
  }

  setGuid(pr) {
    this.guid = pr
  }
}

export class Presentation {
  readonly instanceType = InstanceType.Presentation
  lockInterfaceEvents: boolean
  id: string
  currentSlideIndex: number
  /** 管理 Slides 的选中状态信息 */
  slideSelectManager: SlideSelectManager
  selectionState: SelectionState
  textPrCopied: Nullable<TextPr>
  paraPrCopied: Nullable<ParaPr>
  spFormatCopied: Nullable<SlideElementFormat>
  allowPasteFormat: boolean = true
  slides: Slide[]
  themesMap: Record<string, Theme>
  preservedSlideMasters: SlideMaster[]
  slideMasters: SlideMaster[]
  notesMasters: NotesMaster[]
  notes: any[]
  width = 254
  height = 142
  needUpdatePreview: boolean
  compositeInputInfo: Nullable<{
    run: ParaRun
    index: number
    length: number
  }>
  isGoToSlide: boolean
  showPr: Nullable<ShowProps>
  sectionList: Nullable<PresentationSectionList>
  cursorPosition: { x: number; y: number }
  isNotesFocused: boolean
  recentMaster: SlideMaster | null
  defaultTextStyle?: TextListStyle
  app?
  core?
  pres?: PresProps
  isLoadComplete?: boolean
  constructor() {
    this.lockInterfaceEvents = false
    // EditorUtil.ChangesStack.setPresentation(this)
    this.id = idGenerator.newId()
    this.currentSlideIndex = 0
    this.slideSelectManager = new SlideSelectManager(this)
    this.selectionState = new SelectionState(this)
    this.textPrCopied = null
    this.paraPrCopied = null

    this.slides = []
    this.themesMap = {}
    this.preservedSlideMasters = []
    this.slideMasters = []
    this.notesMasters = []
    this.notes = []
    this.needUpdatePreview = false
    this.compositeInputInfo = null
    gGlobalTableStyles.init()
    this.isGoToSlide = false
    this.showPr = null
    this.cursorPosition = { x: 0, y: 0 }
    this.isNotesFocused = false
    this.recentMaster = null
  }

  reset() {
    this.currentSlideIndex = 0
    this.slideSelectManager.reset()
    this.selectionState.reset()
    this.textPrCopied = null
    this.paraPrCopied = null

    this.slides = []
    this.slideMasters = []
    this.notesMasters = []
    this.notes = []

    this.width = 254
    this.height = 142
    searchManager.isClearSearch = true
    this.needUpdatePreview = false
    gGlobalTableStyles.reset()
    this.compositeInputInfo = null
    this.isGoToSlide = false
    this.showPr = null
    this.cursorPosition = { x: 0, y: 0 }
    this.isNotesFocused = false
    this.recentMaster = null
  }

  isCurrentSelected() {
    return false
  }

  canEdit() {
    return !EditorSettings.isViewMode
  }

  setSelectedSlideIndexes(indexes: number[]) {
    this.slideSelectManager.setSelectedIndices(indexes)
  }

  getSelectedSlideIndices() {
    return this.slideSelectManager.getSelectedIndices()
  }

  setFocusType(focusType: FocusTarget) {
    this.slideSelectManager.setFocusTargetType(focusType)
  }

  setFocusTargetType(focusType: FocusTarget) {
    const oldType = this.getFocusType()
    if (oldType === focusType) {
      return
    }
    this.setFocusType(focusType)
  }

  getFocusType() {
    return this.slideSelectManager.getFocusTargetType()
  }

  getCurrentFocusEditContainer(): Nullable<Slide | Notes> {
    const slide = this.slides[this.currentSlideIndex]
    if (slide) {
      return this.isNotesFocused ? slide.notes : slide
    }
    return null
  }

  getCurrentTextTarget(orTable?: false): Nullable<TextDocument>
  getCurrentTextTarget(orTable: true): Nullable<TableOrTextDocument>
  getCurrentTextTarget(orTable = false) {
    const selectedTextSp = this.selectionState.selectedTextSp
    if (selectedTextSp && isSlideElementInPPT(selectedTextSp)) {
      if (orTable) {
        if (selectedTextSp.instanceType === InstanceType.GraphicFrame) {
          return selectedTextSp.graphicObject as TableOrTextDocument
        }
      }

      return getTextDocumentOfSp(selectedTextSp)
    }
    return null
  }

  private _addTextForComposite(valueOfCharCode) {
    if (null == this.compositeInputInfo) return

    const run = this.compositeInputInfo.run
    const valueOfPos =
      this.compositeInputInfo.index + this.compositeInputInfo.length
    let char
    if (32 === valueOfCharCode || 12288 === valueOfCharCode) {
      char = ParaSpace.new()
    } else {
      char = ParaText.new(valueOfCharCode)
    }
    run.addItemAt(valueOfPos, char, true)
    this.compositeInputInfo.length++
  }

  private _removeTextForComposite(valueOfCount) {
    if (null == this.compositeInputInfo) return

    const run = this.compositeInputInfo.run
    const valueOfPos =
      this.compositeInputInfo.index + this.compositeInputInfo.length

    const valueOfDelCount = Math.max(
      0,
      Math.min(
        valueOfCount,
        this.compositeInputInfo.length,
        run.children.length,
        valueOfPos
      )
    )

    run.children.recycle(
      run.removeContent(valueOfPos - valueOfDelCount, valueOfDelCount, true)
    )

    this.compositeInputInfo.length -= valueOfDelCount
  }

  replaceCompositeInput(charCodes) {
    if (null == this.compositeInputInfo) return
    startEditAction(EditActionFlag.action_Document_CompositeInputReplace)
    this._removeTextForComposite(this.compositeInputInfo.length)
    for (
      let valueOfIndex = 0, valueOfCount = charCodes.length;
      valueOfIndex < valueOfCount;
      ++valueOfIndex
    ) {
      this._addTextForComposite(charCodes[valueOfIndex])
    }
    calculateForRendering()
    hookManager.invoke(Hooks.EditorUI.OnUpdateSelectionState)
  }

  endComposite() {
    if (null == this.compositeInputInfo) return

    const run = this.compositeInputInfo.run
    run.setCompositeInputInfo(null)
    this.compositeInputInfo = null

    this.syncEditorUIState()

    hookManager.invoke(Hooks.EditorUI.OnFirePaint)
  }

  setLoopShow(value: boolean) {
    if (value === false) {
      if (!this.showPr) {
        return
      } else {
        if (this.showPr.loop !== false) {
          const showPr = this.showPr.clone()
          showPr.loop = false
          this.setShowPr(showPr)
        }
      }
    } else {
      if (!this.showPr) {
        const showPr = new ShowProps()
        showPr.loop = true
        this.setShowPr(showPr)
      } else {
        if (!this.showPr.loop) {
          const showPr = this.showPr.clone()
          showPr.loop = true
          this.setShowPr(showPr)
        }
      }
    }
  }

  isLoopShow() {
    if (this.showPr) {
      return this.showPr.loop === true
    }
    return false
  }

  setShowPr(showPr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Presentation_SetShowPr,
      this.showPr,
      showPr
    )
    this.showPr = showPr
  }

  setUseTiming(value) {
    if (value === true) {
      if (!this.showPr) {
        return
      } else {
        if (this.showPr.useTimings !== true) {
          const copyShowPr = this.showPr.clone()
          copyShowPr.useTimings = true
          this.setShowPr(copyShowPr)
        }
      }
    } else {
      if (!this.showPr) {
        const showPr = new ShowProps()
        showPr.useTimings = false
        this.setShowPr(showPr)
      } else {
        if (this.showPr.useTimings !== false) {
          const copyShowPr = this.showPr.clone()
          copyShowPr.useTimings = false
          this.setShowPr(copyShowPr)
        }
      }
    }
  }

  isUseTiming() {
    if (this.showPr) {
      return this.showPr.useTimings === false ? false : true
    }
    return true
  }

  isEmpty() {
    let res = true
    for (let i = 0; i < this.slides.length; i++) {
      const slide = this.slides[i]
      const isEmpty = slide.isEmpty()
      if (!isEmpty) {
        res = false
        break
      }
    }

    return res
  }

  getLayoutRefs() {
    const res: Record<string, number[]> = {}
    for (const slide of this.slides) {
      const masterRefId = slide.master?.refId
      const layoutRefId = slide.layout?.refId
      if (!masterRefId || !layoutRefId) return
      const refId = `${masterRefId}-${layoutRefId}`
      if (!res[refId]) {
        res[refId] = [slide.index!]
      } else {
        res[refId].push(slide.index!)
      }
    }
    return res
  }

  addSlideMaster(pos: number, master: SlideMaster) {
    if (!master) return
    informEntityChildrenChange(
      this,
      EditActionFlag.edit_Presentation_AddSlideMaster,
      pos,
      [master],
      true
    )
    this.slideMasters.splice(pos, 0, master)
    const theme = master.theme
    if (theme) {
      this.themesMap[theme.getRefId()] = theme
    }
  }

  removeSlideMaster(master: SlideMaster) {
    if (!master) return
    const pos = this.slideMasters.indexOf(master)
    if (pos >= 0) {
      informEntityChildrenChange(
        this,
        EditActionFlag.edit_Presentation_RemoveSlideMaster,
        pos,
        [master],
        true
      )
      this.slideMasters.splice(pos, 1)
    }
  }

  updateSlideMaster(master: SlideMaster) {
    if (!master) return
    const pos = this.slideMasters.indexOf(master)
    if (pos >= 0) {
      informEntityChildrenChange(
        this,
        EditActionFlag.edit_Presentation_UpdateSlideMaster,
        pos,
        [master],
        true
      )
    }
  }

  getId() {
    return this.id
  }

  renderComments(
    slideIndex: number,
    drawContext: GraphicsBoundsChecker | CanvasDrawer
  ) {
    this.slides[slideIndex] &&
      this.slides[slideIndex].renderComments(drawContext)
  }

  getCursorPositionInText() {
    let position: { x: number; y: number } | undefined
    const textDoc = this.getCurrentTextTarget(false)
    if (textDoc) {
      const element = textDoc.children.at(textDoc.cursorPosition.childIndex)
      if (element) {
        const pos = calculateCursorPositionForParagraph(
          element,
          false,
          false,
          true
        )
        let x, y
        if (pos.transform) {
          x = pos.transform.XFromPoint(pos.x, pos.y + pos.height!)
          y = pos.transform.YFromPoint(pos.x, pos.y + pos.height!)
        } else {
          x = pos.x
          y = pos.y + pos.height!
        }
        position = { x: x, y: y }
      }
    }
    return position
  }

  addNewParagraph() {
    startEditAction(EditActionFlag.action_Presentation_AddNewParagraph)
    addNewParagraph(this)
    calculateForRendering()
  }

  clearSearchResult() {
    searchManager.clear()
    hookManager.invoke(Hooks.EditorUI.OnFirePaint)
    hookManager.invoke(Hooks.EditorUI.OnFireNotePaint)
  }

  groupShapes() {
    startEditAction(EditActionFlag.action_Presentation_CreateGroup)
    this.createGroupShape()
    calculateForRendering()

    this.syncEditorUIState()
  }

  unGroupShapes() {
    startEditAction(EditActionFlag.action_Presentation_UnGroup)
    this.execUnGroup()
    calculateForRendering()
    this.syncEditorUIState()
  }

  addImageShape(w, h, src: UploadUrlResult) {
    if (this.slides[this.currentSlideIndex]) {
      this.setFocusType(FocusTarget.EditorView)

      this.isNotesFocused = false

      startEditAction(EditActionFlag.action_Presentation_AddImage)
      const image = buildImageShape(
        src,
        (this.slides[this.currentSlideIndex].width - w) / 2,
        (this.slides[this.currentSlideIndex].height - h) / 2,
        w,
        h
      )
      image.setParent(this.slides[this.currentSlideIndex])
      image.spTreeParent = this.slides[this.currentSlideIndex].cSld
      addToParentSpTree(image)
      this.selectionState.reset()
      this.selectionState.select(image)
      calculateForRendering()
      this.syncEditorUIState()
      this.calcForEmptyNotes()
    }
  }

  addImageByPlaceholder(
    image: LoadableImage,
    slideRefId: string,
    placeholderShapeId: string
  ) {
    const slide =
      (slideRefId &&
        this.slides.find((slide) => slide.getRefId() === slideRefId)) ||
      this.slides[this.currentSlideIndex]
    if (placeholderShapeId && image && image.Image) {
      const selectionState =
        slide === this.slides[this.currentSlideIndex]
          ? this.selectionState
          : null
      const phSp =
        EditorUtil.entityRegistry.getEntityById<Shape>(placeholderShapeId)
      if (phSp && selectionState) {
        startEditAction(EditActionFlag.action_Presentation_AddImage)
        selectionState.reset()

        const { x, y, extX, extY, rot } = phSp
        let imageShape: ImageShape
        const ph = phSp.nvSpPr?.nvPr?.ph
        if (ph?.type === PlaceholderType.pic) {
          const srcRect = new RelativeRect()
          const imageWidthInMM = Math.max(
            image.Image.width * Factor_pix_to_mm,
            1
          )
          const imageHeightInMM = Math.max(
            image.Image.height * Factor_pix_to_mm,
            1
          )
          const ratio = extY / extX
          const imageRatio = imageHeightInMM / imageWidthInMM

          if (imageRatio >= ratio) {
            const h = imageRatio * extX
            const offset = ((h - extY) / 2 / h) * 100
            srcRect.set(0, offset, 100, 100 - offset)
          }

          if (imageRatio < ratio) {
            const w = (1 / imageRatio) * extY
            const offset = ((w - extX) / 2 / w) * 100
            srcRect.set(offset, 0, 100 - offset, 100)
          }
          imageShape = buildImageShape(
            { url: image.src, encryptUrl: image.encryptSrc },
            x,
            y,
            extX,
            extY
          )
          const blipFill = imageShape.blipFill ?? new BlipFill()
          blipFill.setStretch(true)
          blipFill.setSrcRect(srcRect)
          imageShape.blipFill = blipFill
        } else {
          let w = extX
          let h = extY
          const _w = Math.max(image.Image.width * Factor_pix_to_mm, 1)
          const _h = Math.max(image.Image.height * Factor_pix_to_mm, 1)
          if (_w < w && _h < h) {
            w = _w
            h = _h
          } else {
            const factor = Math.min(w / _w, h / _h)
            w = Math.max(5, _w * factor)
            h = Math.max(5, _h * factor)
          }
          imageShape = buildImageShape(
            { url: image.src, encryptUrl: image.encryptSrc },
            x + extX / 2.0 - w / 2.0,
            y + extY / 2.0 - h / 2.0,
            w,
            h
          )
        }

        if (phSp.spPr) {
          imageShape.setSpPr(phSp.spPr.clone())
          imageShape.spPr?.setParent(imageShape)
        }

        if (isNumber(rot)) {
          if (imageShape.spPr && imageShape.spPr.xfrm) {
            imageShape.spPr.xfrm.setRot(rot)
          }
        }
        imageShape.setParent(this.slides[this.currentSlideIndex])
        this.slides[this.currentSlideIndex].replaceFromSpTreeByPlaceholder(
          phSp,
          imageShape
        )

        selectionState.select(imageShape)
        calculateForRendering()
        this.syncEditorUIState()
        this.calcForEmptyNotes()
        return
      } else {
        return
      }
    }
  }

  addImages<T extends ImageShapeSubTypes>(
    images: LoadableImage[],
    options: {
      type: T
      slideRefId?: string
      callback?: (image: ImageShapeSubTypeMap[T]) => void
      offset?: Offset
      size?: Size
    } = { type: 'image' as T }
  ): ImageShapeSubTypeMap[T][] {
    const imageShapes: ImageShapeSubTypeMap[T][] = []
    if (images.length) {
      const { slideRefId, callback, size, offset, type } = options
      const slide =
        (slideRefId &&
          this.slides.find((slide) => slide.getRefId() === slideRefId)) ||
        this.slides[this.currentSlideIndex]

      this.setFocusType(FocusTarget.EditorView)

      this.isNotesFocused = false
      const selectionState =
        slide === this.slides[this.currentSlideIndex]
          ? this.selectionState
          : null
      startEditAction(EditActionFlag.action_Presentation_AddImage)
      if (selectionState) {
        selectionState.reset()
      }
      images.forEach((image) => {
        const imgWidth = size ? size.width : (image.Image?.width ?? slide.width)
        const imgHeight = size
          ? size.height
          : (image.Image?.height ?? slide.height)

        let w = slide ? slide.width : 0
        let h = slide ? slide.height : 0
        const imageWidthInMM = Math.max(imgWidth * Factor_pix_to_mm, 1)
        const imageHeightInMM = Math.max(imgHeight * Factor_pix_to_mm, 1)
        h = Math.max(5, Math.min(h, imageHeightInMM))
        w = Math.max(5, Math.min((h * imageWidthInMM) / imageHeightInMM))

        if (w > slide.width) {
          h = (h / w) * slide.width
          w = slide.width
        }
        const x = offset ? offset.x * Factor_pix_to_mm : (slide.width - w) / 2
        const y = offset ? offset.y * Factor_pix_to_mm : (slide.height - h) / 2
        const imageShape = buildImageShape(
          { url: image.src, encryptUrl: image.encryptSrc },
          x,
          y,
          w,
          h,
          type
        )
        if (callback) {
          callback(imageShape)
        }
        imageShapes.push(imageShape)
        imageShape.setParent(slide)
        imageShape.spTreeParent = slide.cSld
        addToParentSpTree(imageShape)
        if (selectionState) {
          selectionState.select(imageShape)
        }
      })

      calculateForRendering()
      this.syncEditorUIState()
      this.calcForEmptyNotes()
    }
    return imageShapes
  }

  removeSelection() {
    this.selectionState.reset()
  }

  adjustGraphicFrameRowHeight(
    graphicFrame: GraphicFrame,
    isIgnoreHeight?: boolean
  ) {
    calculateForGraphicFrame(graphicFrame)
    const rows = graphicFrame.graphicObject?.children
    if (rows == null) {
      return
    }

    for (let i = 0, l = rows.length; i < l; ++i) {
      const row = rows.at(i)!
      if (
        !isIgnoreHeight &&
        row.pr &&
        row.pr.height &&
        row.pr.height.heightRule === LineHeightRule.AtLeast &&
        isNumber(row.pr.height.value) &&
        row.pr.height.value > 0
      ) {
        continue
      }
      let maxTopMargin = 0,
        maxBottomMargin = 0,
        maxTopBorder = 0,
        maxBottomBorder = 0
      for (let j = 0, lj = row.children.length; j < lj; ++j) {
        const cell = row.children.at(j)!
        const margins = cell.getMargins()
        if (margins.bottom!.w > maxBottomMargin) {
          maxBottomMargin = margins.bottom!.w
        }
        if (margins.top!.w > maxTopMargin) {
          maxTopMargin = margins.top!.w
        }
        const borders = cell.getBorders()
        if (borders.top!.size! > maxTopBorder) {
          maxTopBorder = borders.top!.size!
        }
        if (borders.bottom!.size! > maxBottomBorder) {
          maxBottomBorder = borders.bottom!.size!
        }
      }
      row.setHeight({
        value:
          row.height -
          maxTopMargin -
          maxBottomMargin -
          maxTopBorder / 2 -
          maxBottomBorder / 2,
        heightRule: LineHeightRule.AtLeast,
      })
    }
    // 此处只是为了计算高度, 下一轮 table 仍需 calc
    graphicFrame.setCalcStatusOf(CalcStatus.GraphicFrame.Table)
  }

  addTable(cols: number, rows: number) {
    if (!this.slides[this.currentSlideIndex]) return

    startEditAction(EditActionFlag.action_Presentation_AddTable)
    const graphicFrame = this.createGraphicFrame(
      cols,
      rows,
      this.slides[this.currentSlideIndex],
      gGlobalTableStyles.defaultIdOfTableStyle
    )

    this.setFocusType(FocusTarget.EditorView)

    this.isNotesFocused = false
    this.slides[this.currentSlideIndex].addToSpTreeAtIndex(
      this.slides[this.currentSlideIndex].cSld.spTree.length,
      graphicFrame
    )

    this.adjustGraphicFrameRowHeight(graphicFrame)

    graphicFrame.selectSelf()
    moveCursorToStartPosInTable(graphicFrame.graphicObject!)
    calculateForRendering()
    this.syncEditorUIState()
    hookManager.invoke(Hooks.EditorUI.OnUpdateSelectionState)
    this.calcForEmptyNotes()
  }

  createGraphicFrame(
    cols,
    rows,
    parent,
    styleId,
    width?: number,
    height?: number,
    posX?: number,
    posY?: number
  ) {
    const extX = isNumber(width) ? width : (this.width * 2) / 3
    let [x, y] = [0, 0]
    if (isNumber(posX) && isNumber(posY)) {
      x = posX
      y = posY
    }
    const grid: number[] = []

    for (let i = 0; i < cols; i++) grid[i] = extX / cols

    let rowHeight
    if (isNumber(height)) {
      rowHeight = height / rows
    }

    const graphicFrame = new GraphicFrame()
    graphicFrame.spTreeParent = parent?.cSld
    graphicFrame.setParent(parent)
    graphicFrame.setSpPr(new SpPr())
    graphicFrame.spPr!.setParent(graphicFrame)
    graphicFrame.spPr!.setXfrm(new Xfrm(graphicFrame.spPr!))
    graphicFrame.spPr!.xfrm!.setParent(graphicFrame.spPr)
    graphicFrame.spPr!.xfrm!.setOffX((this.width - extX) / 2)
    graphicFrame.spPr!.xfrm!.setOffY(this.height / 5)
    graphicFrame.spPr!.xfrm!.setExtX(extX)
    graphicFrame.spPr!.xfrm!.setExtY(7.478268771701388 * rows)
    graphicFrame.setNvSpPr(new UniNvPr(graphicFrame))

    const table = new Table(graphicFrame, rows, cols, grid)
    table.resetDimension(x, y, extX, 100000)
    if (typeof styleId === 'string') {
      table.setTableStyleId(styleId)
    }
    table.setTableLook(
      new TableLook(false, true, false, false, true, false, false)
    )
    for (let i = 0, l = table.children.length; i < l; ++i) {
      const row = table.children.at(i)!
      if (isNumber(rowHeight)) {
        row.setHeight({ value: rowHeight, heightRule: LineHeightRule.AtLeast })
      }
    }
    graphicFrame.setGraphicObject(table)
    setDeletedForSp(graphicFrame, false)
    return graphicFrame
  }

  applyTextPrToParagraph(textPr: TextPrOptions, isDirty?, keepInterface?) {
    addOrApplyPrToParagraph(
      this,
      applyTextPrToParagraph,
      textPr,
      isDirty,
      keepInterface
    )
  }

  addToParagraph(item: RunContentElement, isDirty?, keepInterface?) {
    addOrApplyPrToParagraph(
      this,
      addItemToParagraph,
      item,
      isDirty,
      keepInterface
    )
  }

  clearFormattingForParagraph(
    isFormatParaPr: boolean,
    isFormatTextPr: boolean
  ) {
    startEditAction(EditActionFlag.action_Presentation_ParagraphClearFormatting)
    clearFormattingForparagraph(this, isFormatParaPr, isFormatTextPr)
    calculateForRendering()

    this.syncEditorUIState()
  }

  getSelectionBounds() {
    const selectedElements = this.selectionState.selectedElements
    if (selectedElements.length > 0) {
      return getBoundsOfGroupingSps([selectedElements[0]])
    }
    return new GraphicBounds(0, 0, 0, 0)
  }

  clearConnectorInfo(sps: SlideElement[]) {
    const spTree = this.getCurrentFocusEditContainer()?.cSld.spTree
    const allConnectors = collectCxnShapes(spTree)
    // Connector可能连接到另一个Connector上
    const allShapes = sps.concat(allConnectors)
    for (let i = 0; i < allConnectors.length; ++i) {
      for (let j = 0; j < allShapes.length; ++j) {
        allConnectors[i].resetWithShape(allShapes[j])
      }
    }
  }

  onRemove(dir: TextOperationDirection, isRemoveOnlySelection?) {
    const selectionState = this.selectionState
    const selectedTextObject = selectionState.selectedTextSp
    if (selectedTextObject) {
      const target = this.getCurrentTextTarget(true)
      if (target) {
        target.remove(dir, isRemoveOnlySelection)
        getAndUpdateCursorPositionForTargetOrTextContent(target)
      }
    } else if (selectionState && selectionState.hasSelection) {
      const groupSelection = selectionState.groupSelection
      if (groupSelection) {
        this.clearConnectorInfo(groupSelection.selectedElements)
        const groups: Dict<GroupShape> = {}
        const arrOfGroups: GroupShape[] = []

        for (let i = 0; i < groupSelection.selectedElements.length; ++i) {
          const sp = groupSelection.selectedElements[i]
          const group = sp.group!
          group.removeSpById(sp.getId())
          groups[group.getId() + ''] = group
          setDeletedForSp(sp, true)
        }
        groups[groupSelection.group.getId() + ''] = groupSelection.group
        for (const key in groups) {
          if (groups.hasOwnProperty(key)) arrOfGroups.push(groups[key])
        }
        arrOfGroups.sort(groupComparor)
        for (let i = 0; i < arrOfGroups.length; ++i) {
          const group = arrOfGroups[i]
          if (group.group) {
            if (group.spTree.length === 0) {
              group.group.removeSpById(group.getId())
            } else if (group.spTree.length === 1) {
              const sp = group.spTree[0]
              const hc = sp.spPr!.xfrm!.extX / 2
              const vc = sp.spPr!.xfrm!.extY / 2
              const xc = sp.transform.XFromPoint(hc, vc)
              const yc = sp.transform.YFromPoint(hc, vc)
              const rel_xc = group.group.invertTransform.XFromPoint(xc, yc)
              const rel_yc = group.group.invertTransform.YFromPoint(xc, yc)
              sp.spPr!.xfrm!.setOffX(rel_xc - hc)
              sp.spPr!.xfrm!.setOffY(rel_yc - vc)
              sp.spPr!.xfrm!.setRot(normalizeRotValue(group.rot + sp.rot))
              sp.spPr!.xfrm!.setFlipH(
                group.spPr!.xfrm!.flipH === true
                  ? !(sp.spPr!.xfrm!.flipH === true)
                  : sp.spPr!.xfrm!.flipH === true
              )
              sp.spPr!.xfrm!.setFlipV(
                group.spPr!.xfrm!.flipV === true
                  ? !(sp.spPr!.xfrm!.flipV === true)
                  : sp.spPr!.xfrm!.flipV === true
              )
              setGroup(sp, group.group)
              for (let j = 0; j < group.group.spTree.length; ++j) {
                if (group.group.spTree[j] === group) {
                  group.group.addToSpTree(j, sp)
                  group.group.removeSpById(group.getId())
                }
              }
            }
          } else {
            if (group.spTree.length === 0) {
              this.selectionState.resetRootSelection()
              this.onRemove(dir)
              return
            } else if (group.spTree.length === 1) {
              const sp = group.spTree[0]
              sp.spPr!.xfrm!.setOffX(
                group.spPr!.xfrm!.offX + sp.spPr!.xfrm!.offX
              )
              sp.spPr!.xfrm!.setOffY(
                group.spPr!.xfrm!.offY + sp.spPr!.xfrm!.offY
              )
              sp.spPr!.xfrm!.setRot(normalizeRotValue(group.rot + sp.rot))
              sp.spPr!.xfrm!.setFlipH(
                group.spPr!.xfrm!.flipH === true
                  ? !(sp.spPr!.xfrm!.flipH === true)
                  : sp.spPr!.xfrm!.flipH === true
              )
              sp.spPr!.xfrm!.setFlipV(
                group.spPr!.xfrm!.flipV === true
                  ? !(sp.spPr!.xfrm!.flipV === true)
                  : sp.spPr!.xfrm!.flipV === true
              )
              setGroup(sp, null)
              addToParentSpTree(sp)
              deleteSlideElementfromParent(group)
              selectionState.reset()
              selectionState.select(sp)
            } else {
              adjustXfrmOfGroup(group)
            }
            selectionState.resetRootSelection()
            return
          }
        }
        selectionState.resetRootSelection()
      } else {
        const selectedElements =
          this.selectionState?.selectedSlideElements || []
        this.clearConnectorInfo(selectedElements)
        for (let i = 0; i < selectedElements.length; ++i) {
          deleteSlideElementfromParent(selectedElements[i], true)
          setDeletedForSp(selectedElements[i], true)
        }
        selectionState.reset()
      }
    }
  }

  onSetParagraphTextDirection(flag: EditActionFlag, isRTL) {
    startEditAction(flag)
    setParagraphTextDirection(this, isRTL)
    calculateForRendering()
  }

  onSetParagraphAlign(flag: EditActionFlag, align) {
    startEditAction(flag)
    setAlignForParagraph(this, align)
    calculateForRendering()
  }

  setParagraphTextDirection(isRTL: boolean) {
    this.onSetParagraphTextDirection(
      EditActionFlag.action_Presentation_SetParagraphSpacing,
      isRTL
    )
    this.syncEditorUIState()
  }

  /** createAction: action_Presentation_SetParagraphSpacing, then syncEditorUIState*/
  setAlignForParagraph(align: AlignKindValue) {
    this.onSetParagraphAlign(
      EditActionFlag.action_Presentation_SetParagraphSpacing,
      align
    )
    this.syncEditorUIState()
  }

  setSpacingForParagraph(spacing: ParaSpacingOptions) {
    startEditAction(EditActionFlag.action_Presentation_SetParagraphSpacing)
    setSpacingForParagraph(this, spacing)
    calculateForRendering()

    this.syncEditorUIState()
  }

  setIndentForParagraph(ind: ParaIndOptions) {
    startEditAction(EditActionFlag.action_Presentation_SetParagraphIndent)
    setIndentForParagraph(this, ind)
    calculateForRendering()

    this.syncEditorUIState()
  }

  setParagraphNumbering(listInfo: BulletListType) {
    startEditAction(EditActionFlag.action_Presentation_SetParagraphNumbering)
    setParagraphNumbering(this, getBulletByListInfo(listInfo))
    calculateForRendering()

    this.syncEditorUIState()
  }

  changeFontSize(isIncrease) {
    startEditAction(EditActionFlag.action_Presentation_ParagraphIncDecFontSize)
    changeFontSizeParagraph(this, isIncrease)
    calculateForRendering()

    this.syncEditorUIState()
  }

  changeIndentLevel(isIncrease) {
    startEditAction(EditActionFlag.action_Presentation_ParagraphIncDecIndent)
    changeIndentLevelForParagraph(this, isIncrease)
    calculateForRendering()

    this.syncEditorUIState()
  }

  canChangeParagraphIndentLevel(isIncrease) {
    const textDoc = this.getCurrentTextTarget()
    if (textDoc) {
      const textSp = this.selectionState.selectedTextSp
      if (
        (textSp?.instanceType === InstanceType.Shape ||
          textSp?.instanceType === InstanceType.GraphicFrame) &&
        (!isPlaceholder(textSp) ||
          (getPlaceholderType(textSp) !== PlaceholderType.title &&
            getPlaceholderType(textSp) !== PlaceholderType.ctrTitle))
      ) {
        return textDoc.canChangeParagraphIndentLevel(isIncrease)
      }
    }
    return false
  }

  applyImageProps(props: SlideElementOptions, options?: { shapeId?: string }) {
    startEditAction(EditActionFlag.action_Presentation_SetImageProps)
    applySlideElementProps(this, props, options)
    calculateForRendering()

    this.syncEditorUIState()
  }

  applyShapeProps(shapeProps: SlideElementOptions) {
    startEditAction(EditActionFlag.action_Presentation_SetShapeProps)
    applySlideElementProps(this, shapeProps)
    calculateForRendering()

    this.syncEditorUIState()
  }

  modifyShapeType(shapeType: PresetShapeTypes) {
    startEditAction(EditActionFlag.action_Presentation_ChangeShapeType)
    const options: SlideElementOptions = { type: shapeType }
    applySlideElementProps(this, options)
    calculateForRendering()

    this.syncEditorUIState()
  }

  setVerticalAlign(align: TextAnchorTypeOO) {
    startEditAction(EditActionFlag.action_Presentation_SetVerticalAlign)
    const options: SlideElementOptions = {
      verticalTextAlign: align,
    }
    applySlideElementProps(this, options)
    calculateForRendering()

    this.syncEditorUIState()
  }

  setVert(align: TextVerticalTypeOO) {
    startEditAction(EditActionFlag.action_Presentation_SetVert)
    const options: SlideElementOptions = { vert: align }
    applySlideElementProps(this, options)
    calculateForRendering()

    this.syncEditorUIState()
  }

  getTextStyles(_l: number) {
    return {
      style: defaultContentStylePrs(),
    }
  }

  getTheme() {
    return this.slideMasters[0].theme
  }

  getColorMap() {
    return gDefaultColorMap
  }

  getSlideLimits() {
    return TextContentDimensionLimit
  }

  getCursorPosition() {
    return {
      x: this.cursorPosition.x,
      y: this.cursorPosition.y,
    }
  }

  setTableProps(props: TableProp) {
    setTableProps(this, props)

    calculateForRendering()

    this.syncEditorUIState()
    syncSelectionState()
  }

  getFirstParaPr(): undefined | ParaPr {
    const ret = getCurrentParaPr(this, true)
    return ret ?? undefined
  }

  getCalcedParaPr(): ParaPr
  getCalcedParaPr(ensureRet: false): undefined | ParaPr
  getCalcedParaPr(ensureRet = true) {
    const ret = getCurrentParaPr(this)
    if (ret) {
      return ret
    }
    return ensureRet === true ? ParaPr.new() : undefined
  }

  getFirstParaTextPr() {
    const ret = getTextPrOfCurrentSelection(this, true)
    if (ret) {
      if (ret.rFonts) {
        const theme = getThemeForEditContainer(this)
        if (theme) {
          const fontScheme = theme.themeElements.fontScheme
          if (ret.rFonts.ascii) {
            ret.rFonts.ascii.name = fontScheme.checkFont(ret.rFonts.ascii.name)
          }
          if (ret.rFonts.eastAsia) {
            ret.rFonts.eastAsia.name = fontScheme.checkFont(
              ret.rFonts.eastAsia.name
            )
          }
          if (ret.rFonts.hAnsi) {
            ret.rFonts.hAnsi.name = fontScheme.checkFont(ret.rFonts.hAnsi.name)
          }
          if (ret.rFonts.cs) {
            ret.rFonts.cs.name = fontScheme.checkFont(ret.rFonts.cs.name)
          }
          if (ret.fontFamily && ret.fontFamily.name) {
            ret.fontFamily.name = fontScheme.checkFont(ret.fontFamily.name)
          }
        }
      }
    }
    return ret ?? undefined
  }

  getCalcedTextPr(): TextPr
  getCalcedTextPr(ensureRet: false): undefined | TextPr
  getCalcedTextPr(ensureRet = true) {
    const ret = getTextPrOfCurrentSelection(this)
    if (ret) {
      if (ret.rFonts) {
        const theme = getThemeForEditContainer(this)
        if (theme) {
          const fontScheme = theme.themeElements.fontScheme
          if (ret.rFonts.ascii) {
            ret.rFonts.ascii.name = fontScheme.checkFont(ret.rFonts.ascii.name)
          }
          if (ret.rFonts.eastAsia) {
            ret.rFonts.eastAsia.name = fontScheme.checkFont(
              ret.rFonts.eastAsia.name
            )
          }
          if (ret.rFonts.hAnsi) {
            ret.rFonts.hAnsi.name = fontScheme.checkFont(ret.rFonts.hAnsi.name)
          }
          if (ret.rFonts.cs) {
            ret.rFonts.cs.name = fontScheme.checkFont(ret.rFonts.cs.name)
          }
          if (ret.fontFamily && ret.fontFamily.name) {
            ret.fontFamily.name = fontScheme.checkFont(ret.fontFamily.name)
          }
        }
      }
      return ret
    }
    return ensureRet === true ? TextPr.new() : undefined
  }

  fillTableStyleIdMap(map: Dict<string>) {
    for (let i = 0; i < this.slides.length; ++i) {
      const slide = this.slides[i]
      this.collectSpsStyleId(map, slide.cSld.spTree)
      if (slide.notes) {
        this.collectSpsStyleId(map, slide.notes.cSld.spTree)
      }
    }
  }

  collectSpsStyleId(map: Dict<string>, spTree: SlideElement[]) {
    for (let i = 0; i < spTree.length; ++i) {
      const sp = spTree[i]
      if (sp.instanceType === InstanceType.GraphicFrame) {
        const styleId = sp.graphicObject?.tableStyleId
        const style =
          typeof styleId === 'string'
            ? gGlobalTableStyles.getTableStyle(styleId)
            : null
        if (styleId && style) {
          map[styleId] = style.guid
        }
      } else if (sp.instanceType === InstanceType.GroupShape) {
        this.collectSpsStyleId(map, sp.spTree)
      }
    }
  }

  isSlideVisible(index: number) {
    const slide = this.slides[index]
    if (!slide) {
      return false
    }
    return slide.isVisible()
  }

  hideSlides(isHide: boolean, arrOfSlides?) {
    if (!this.canEdit()) {
      return
    }
    let arrOfSelectedArray
    if (Array.isArray(arrOfSlides)) {
      arrOfSelectedArray = arrOfSlides
    } else {
      arrOfSelectedArray = this.getSelectedSlideIndices()
    }

    startEditAction(EditActionFlag.action_Presentation_HideSlides)
    const isShow = !isHide
    let slide: Slide
    let valueOfIndex
    for (let i = 0; i < arrOfSelectedArray.length; ++i) {
      valueOfIndex = arrOfSelectedArray[i]
      slide = this.slides[valueOfIndex]
      if (slide) {
        slide.setShow(isShow)
        hookManager.invoke(Hooks.EditorUI.OnCalculateSlide, {
          index: valueOfIndex,
        })
      }
    }
  }

  selectAll() {
    const selectionState = this.selectionState

    const selectionStatusInfo = selectionState.selectionStatusInfo
    switch (selectionStatusInfo.status) {
      case 'group_text':
      case 'root_text': {
        const selectedTextSp = selectionStatusInfo.selectedTextSp
        if (selectedTextSp.instanceType === InstanceType.GraphicFrame) {
          selectedTextSp.graphicObject!.selectAll()
        } else {
          const textDoc = this.getCurrentTextTarget()
          if (textDoc) {
            textDoc.selectAll()
          }
        }
        break
      }
      case 'group_collection': {
        selectionState.resetGroupSelection()
        const group = selectionStatusInfo.group

        for (let i = group.slideElementsInGroup.length - 1; i > -1; --i) {
          selectionState.select(group.slideElementsInGroup[i])
        }
        break
      }
      case 'root_collection': {
        selectionState.reset()
        const sps = getSlideElementsOfEditContainer(this)
        if (sps) {
          for (let i = sps.length - 1; i > -1; --i) {
            selectionState.select(sps[i])
          }
        }
        break
      }
    }
    this.syncEditorUIState()
  }

  calcForEmptyNotes() {
    const slide = this.slides[this.currentSlideIndex]
    if (slide && slide.shapeOfNotes) {
      const textDoc = getTextDocumentOfSp(slide.shapeOfNotes)
      if (textDoc && textDoc.isEmpty()) {
        const slide = this.slides[this.currentSlideIndex]
        hookManager.invoke(Hooks.EditorUI.OnCalculateNotes, {
          slideIndex: this.currentSlideIndex,
          notesWidth: slide.notesWidth,
          notesHeight: slide.getNotesShapeHeight(),
        })

        return true
      }
    }
    return false
  }

  copyTextContentFormat() {
    this.textPrCopied = this.getFirstParaTextPr()
    this.paraPrCopied = this.getFirstParaPr()
    this.spFormatCopied = undefined
  }

  copySpFormat(sp: SlideElement) {
    this.spFormatCopied = getSlideElementFormat(sp)
    this.textPrCopied = undefined
    this.paraPrCopied = undefined
  }

  getFormatSrc(): 'text' | 'sp' | undefined {
    return this.spFormatCopied != null
      ? 'sp'
      : this.textPrCopied != null || this.paraPrCopied != null
        ? 'text'
        : undefined
  }

  pasteTextContentFormat() {
    const textPrCopied = this.textPrCopied
    const paraPrCopied = this.paraPrCopied
    if (textPrCopied || paraPrCopied) {
      startEditAction(EditActionFlag.action_Presentation_ParaFormatPaste)
      pasteParagraphFormat(this, textPrCopied, paraPrCopied)
      calculateForRendering()

      return true
    }
    return false
  }

  getSelectedTable() {
    if (this.slides[this.currentSlideIndex]) {
      const target =
        this.selectionState.selectedTextSp ??
        this.selectionState.selectedTable?.parent
      if (target && target.instanceType === InstanceType.GraphicFrame) {
        return target.graphicObject
      }
    }
  }

  canMergeTableCells() {
    return !!this.getSelectedTable()?.canMergeTableCells()
  }

  canSplitTableCells() {
    return !!this.getSelectedTable()?.canSplitTableCells()
  }

  syncEditorUIState() {
    hookManager.invoke(Hooks.EditorUI.OnSyncInterfaceState)
  }

  changeBackground(bg: Nullable<Bg>, slideIndices: number[]) {
    startEditAction(EditActionFlag.action_Presentation_ChangeBackground)
    for (let i = 0; i < slideIndices.length; ++i) {
      this.slides[slideIndices[i]].changeBackground(bg)
    }
    calculateForRendering()
  }

  setCurSlide(slideIndex: number, isForce?: boolean) {
    if (-1 === slideIndex) {
      this.currentSlideIndex = -1
      this.syncEditorUIState()
      return false
    }
    const newCurSlideIndex = Math.min(
      this.slides.length - 1,
      Math.max(0, slideIndex)
    )
    this.selectionState.reset()
    if (
      (newCurSlideIndex !== this.currentSlideIndex &&
        newCurSlideIndex < this.slides.length) ||
      isForce
    ) {
      this.currentSlideIndex = newCurSlideIndex
      this.isNotesFocused = false
      this.onResizeForNotes()

      const slide = this.slides[this.currentSlideIndex]
      hookManager.invoke(Hooks.EditorUI.OnCalculateNotes, {
        slideIndex: this.currentSlideIndex,
        notesWidth: slide.notesWidth,
        notesHeight: slide.getNotesShapeHeight(),
      })

      this.syncEditorUIState()

      if (this.slides[this.currentSlideIndex]) {
        hookManager.invoke(Hooks.EditorUI.OnUpdatePlaceholders, {
          slideIndex: this.currentSlideIndex,
        })
      }

      return true
    } else {
      this.syncEditorUIState()
    }

    const master = this.slides[this.currentSlideIndex]?.layout?.master
    if (master) {
      this.recentMaster = master
    }
    return false
  }

  updateCurrentSlideIndex(index: number) {
    this.currentSlideIndex = index
  }

  onResizeForNotes() {
    if (!this.slides[this.currentSlideIndex]) {
      return false
    }
    const slide = this.slides[this.currentSlideIndex]
    const newWidth = hookManager.get(Hooks.EditorUI.GetWidthOfNotes)!
    if (checkAlmostEqual(slide.notesWidth, newWidth)) {
      return false
    }
    slide.notesWidth = newWidth
    calculateNotesShape(slide)

    hookManager.invoke(Hooks.EditorUI.OnCalculateNotes, {
      slideIndex: this.currentSlideIndex,
      notesWidth: newWidth,
      notesHeight: slide.getNotesShapeHeight(),
    })

    return true
  }

  drawNotes(slideIndex, graphics) {
    if (this.slides[slideIndex]) {
      EditorSettings.isRenderingNotes = true
      this.slides[slideIndex].renderNotes(graphics)
      EditorSettings.isRenderingNotes = false
    }
  }

  updateDocumentState() {
    hookManager.invoke(Hooks.EditorUI.OnUpdateSelectionState)
    this.syncEditorUIState()
  }

  canGroup() {
    return getSlideElementsForGrouping(this).length > 1
  }

  canUnGroup(): boolean
  canUnGroup(b: true): GroupShape[]
  canUnGroup(isRetArray?) {
    const selectedElements = this.selectionState.selectedSlideElements
    const curGroups: GroupShape[] = []
    for (let i = 0; i < selectedElements.length; ++i) {
      const sp = selectedElements[i]
      if (sp.instanceType === InstanceType.GroupShape) {
        if (!(isRetArray === true)) {
          return true
        }
        curGroups.push(sp)
      }
    }
    return isRetArray === true ? curGroups : false
  }

  private execUnGroup() {
    const groups = this.canUnGroup(true)
    let spsOfCurEditorContainer
    if (groups.length > 0) {
      this.selectionState.reset()
      let i, j
      let sp: SlideElement, valueOfInsertPos
      for (i = 0; i < groups.length; ++i) {
        const group = groups[i]
        normalizeGroupExts(group)

        spsOfCurEditorContainer =
          this.getCurrentFocusEditContainer()?.cSld.spTree
        valueOfInsertPos = undefined
        for (j = 0; j < spsOfCurEditorContainer.length; ++j) {
          if (spsOfCurEditorContainer[j] === group) {
            valueOfInsertPos = j
            break
          }
        }

        const spTree = group.spTree
        let xc, yc
        for (j = 0; j < spTree.length; ++j) {
          sp = spTree[j]
          const spXfrm = sp.spPr!.xfrm!
          spXfrm.setRot(normalizeRotValue(sp.rot + group.rot))
          xc = sp.transform.XFromPoint(sp.extX / 2.0, sp.extY / 2.0)
          yc = sp.transform.YFromPoint(sp.extX / 2.0, sp.extY / 2.0)
          spXfrm.setOffX(xc - sp.extX / 2.0)
          spXfrm.setOffY(yc - sp.extY / 2.0)
          spXfrm.setFlipH(
            group.spPr!.xfrm!.flipH === true
              ? !(spXfrm.flipH === true)
              : spXfrm.flipH === true
          )
          spXfrm.setFlipV(
            group.spPr!.xfrm!.flipV === true
              ? !(spXfrm.flipV === true)
              : spXfrm.flipV === true
          )
          setGroup(sp, null)
          if (
            sp.spPr?.fillEffects?.fill?.type === FillKIND.GRP &&
            group.spPr?.fillEffects
          ) {
            sp.spPr.setFill(group.spPr.fillEffects.clone())
          }
          if (isNumber(valueOfInsertPos)) {
            addToParentSpTree(sp, valueOfInsertPos + j)
          } else {
            addToParentSpTree(sp)
          }
          EditorUtil.entityRegistry.add(sp, sp.id)
          this.selectionState.select(sp)
        }
        group.isDeleted = true
        deleteSlideElementfromParent(group)
      }
    }
  }

  createGroupShape() {
    const editContainer = this.getCurrentFocusEditContainer()
    if (editContainer?.instanceType !== InstanceType.Slide) {
      return
    }

    const spArr = getSlideElementsForGrouping(this)
    const group = createGrpSpForGrouping(spArr)
    if (group) {
      this.selectionState.reset()
      if (editContainer && editContainer.cSld) {
        group.setParent(editContainer)
        group.spTreeParent = editContainer.cSld
      }
      addToParentSpTree(group)
      this.selectionState.select(group)
      calculateForRendering()
    }
  }

  getSelectedSpsCount(): number {
    const selectedElements = this.selectionState.selectedElements
    return selectedElements.length
  }

  removeTargetBeforePaste() {
    const textTarget = this.getCurrentTextTarget()
    if (textTarget) {
      textTarget.remove(-1, true, true)
    }
  }

  addNextSlide(layoutId?: string) {
    if (!this.canEdit()) {
      return
    }
    startEditAction(EditActionFlag.action_Presentation_AddNextSlide)

    let newSlide: Slide,
      layout: SlideLayout | undefined,
      i,
      phType,
      sp,
      hf,
      isSpecialPh

    this.slideMasters.forEach((master) =>
      master.layouts.forEach((l) => {
        if (l.id === layoutId) {
          layout = l
        }
      })
    )
    if (this.slides[this.currentSlideIndex]) {
      const curSlide = this.slides[this.currentSlideIndex]
      const curLayout = curSlide.layout
      const master = curLayout?.master
      if (master && !layout && curLayout) {
        layout = curLayout.master!.getMatchLayout(
          curLayout.type,
          curLayout.matchingName,
          curLayout.cSld.name
        )
        if (layout) {
          if (master.layouts.indexOf(layout) === 0 && master.layouts[1]) {
            layout = master.layouts[1]
          }
        }
      }

      hf = layout!.master?.hf
      newSlide = new Slide(this, layout, this.currentSlideIndex + 1)
      newSlide.setNotes(createNotes())
      newSlide.notes!.setNotesMaster(this.notesMasters[0])
      newSlide.notes!.setSlide(newSlide)

      const transition = layout!.transition || layout!.master?.transition
      if (transition != null) {
        newSlide.applyTiming(transition.clone())
      }

      for (i = 0; i < layout!.cSld.spTree.length; ++i) {
        if (isPlaceholder(layout!.cSld.spTree[i])) {
          phType = getPlaceholderType(layout!.cSld.spTree[i])
          isSpecialPh =
            phType === PlaceholderType.dt ||
            phType === PlaceholderType.ftr ||
            phType === PlaceholderType.hdr ||
            phType === PlaceholderType.sldNum
          if (
            !isSpecialPh ||
            (hf &&
              ((phType === PlaceholderType.dt && hf.dt !== false) ||
                (phType === PlaceholderType.ftr && hf.ftr !== false) ||
                (phType === PlaceholderType.hdr && hf.hdr !== false) ||
                (phType === PlaceholderType.sldNum && hf.sldNum !== false)))
          ) {
            sp = copyLayoutElement(layout!.cSld.spTree[i])
            sp.setParent(newSlide)
            !isSpecialPh && clearSlideElementContent(sp)
            newSlide.addToSpTreeAtIndex(newSlide.cSld.spTree.length, sp)
          }
        }
      }
      newSlide.setIndex(this.currentSlideIndex + 1)
      newSlide.setSlideSize(this.width, this.height)
      this.insertSlide(this.currentSlideIndex + 1, newSlide)

      for (i = this.currentSlideIndex + 2; i < this.slides.length; ++i) {
        this.slides[i].setIndex(i)
      }
      calculateForRendering()
    } else {
      const master = this.recentMaster || this.slideMasters[0]

      if (!layout) {
        layout = master.layouts[0]
      }

      newSlide = new Slide(this, layout, this.currentSlideIndex + 1)
      newSlide.setNotes(createNotes())
      newSlide.notes!.setNotesMaster(this.notesMasters[0])
      newSlide.notes!.setSlide(newSlide)

      const transition = layout.transition || layout.master?.transition
      if (transition != null) {
        newSlide.applyTiming(transition.clone())
      }

      layout.cSld.spTree.forEach((slideElement) => {
        if (isPlaceholder(slideElement)) {
          phType = getPlaceholderType(slideElement)
          if (
            phType !== PlaceholderType.dt &&
            phType !== PlaceholderType.ftr &&
            phType !== PlaceholderType.hdr &&
            phType !== PlaceholderType.sldNum
          ) {
            sp = copyLayoutElement(slideElement)
            sp.setParent(newSlide)
            clearSlideElementContent(sp)
            newSlide.addToSpTreeAtIndex(newSlide.cSld.spTree.length, sp)
          }
        }
      })
      newSlide.setIndex(this.currentSlideIndex + 1)
      newSlide.setSlideSize(this.width, this.height)
      this.insertSlide(this.currentSlideIndex + 1, newSlide)
      calculateForRendering()
    }
    goToSlide(this.currentSlideIndex + 1)
    this.syncEditorUIState()
  }

  duplicateSlide() {
    if (!this.canEdit()) {
      return
    }
    const selectedSlideIndices = this.getSelectedSlideIndices()
    if (selectedSlideIndices != null) {
      this.reorderSlides(
        Math.max(...selectedSlideIndices) + 1,
        selectedSlideIndices,
        true
      )
    }
  }

  reorderSlides(pos: number, array: number[], isCopy = false) {
    if (!this.canEdit()) {
      return
    }
    startEditAction(
      isCopy !== true
        ? EditActionFlag.action_Presentation_ShiftSlides
        : EditActionFlag.action_Presentation_DuplicateSlides
    )
    array.sort(sortAscending)
    const deleted: Nullable<Slide>[] = []
    let i

    if (!isCopy) {
      for (i = array.length - 1; i > -1; --i) {
        deleted.push(this.removeSlide(array[i]))
      }

      const _pos = pos
      for (i = 0; i < array.length; ++i) {
        if (array[i] < _pos) {
          --pos
        } else {
          break
        }
      }
    } else {
      for (i = array.length - 1; i > -1; --i) {
        const slideCopy = this.slides[array[i]].clone()
        deleted.push(slideCopy)
      }
    }

    const selectedSlideIndex = this.currentSlideIndex
    let newSelectedSlideIndex = 0

    const newSelectedSlidesPosArr: number[] = []

    deleted.reverse()
    for (i = 0; i < deleted.length; ++i) {
      newSelectedSlidesPosArr.push(pos + i)
      const deletedSlide = deleted[i]
      if (deletedSlide) {
        this.insertSlide(pos + i, deletedSlide)
      }
    }
    for (i = 0; i < this.slides.length; ++i) {
      if (this.slides[i].index === selectedSlideIndex) newSelectedSlideIndex = i

      this.slides[i].udpateIndex(i)
    }
    calculateForRendering()
    hookManager.invoke(Hooks.EditorUI.OnCalculatePreviews)
    goToSlide(newSelectedSlideIndex)
    return newSelectedSlidesPosArr
  }

  deleteSlides(array): Nullable<number> {
    let toSlideIndex: Nullable<number> = null
    if (array.length > 0) {
      startEditAction(EditActionFlag.action_Presentation_DeleteSlides)
      const oldCount = this.slides.length
      array.sort(sortAscending)
      for (let i = array.length - 1; i > -1; --i) {
        this.removeSlide(array[i])
      }
      for (let i = 0; i < this.slides.length; ++i) {
        this.slides[i].udpateIndex(i)
      }
      if (array[array.length - 1] !== oldCount - 1) {
        toSlideIndex = array[array.length - 1] + 1 - array.length
      } else {
        toSlideIndex = this.slides.length - 1
      }
    }

    return toSlideIndex
  }

  changeLayout(indexes: number[], master: SlideMaster, layoutIndex: number) {
    startEditAction(EditActionFlag.action_Presentation_ChangeLayout)
    const layout = master.layouts[layoutIndex]
    for (let i = 0; i < indexes.length; ++i) {
      const slide = this.slides[indexes[i]]
      applyLayoutToSlide(slide, layout)
    }
    calculateForRendering()
  }

  applySlideMaster(master: SlideMaster, slidesIndex: Nullable<number[]>) {
    if (!master) {
      return
    }

    if (!this.slideMasters.find((slideMaster) => slideMaster === master)) {
      this.addSlideMaster(this.slideMasters.length, master)
      const sizeUpdateInfo = getDefaultScaleUpdateInfo({
        from: { width: master.width, height: master.height },
        to: { width: this.width, height: this.height },
      })
      master.presentation = this
      master.updateSize(this.width, this.height, sizeUpdateInfo)
      master.layouts.forEach((layout) => {
        layout.updateSize(this.width, this.height, sizeUpdateInfo)
      })
    }
    slidesIndex = Array.isArray(slidesIndex)
      ? slidesIndex
      : this.slides.map((_, index) => index)

    const preSlideMasters: Array<SlideMaster | null> = []
    slidesIndex.forEach((slideIndex) => {
      const slide = this.slides[slideIndex]
      const layout = slide.layout
      if (layout) {
        if (!preSlideMasters.includes(layout.master)) {
          preSlideMasters.push(layout.master)
        }
        const newLayout =
          master.getMatchLayout(
            layout.type,
            layout.matchingName,
            layout.cSld.name,
            true
          ) ?? master.layouts[0]
        applyLayoutToSlide(slide, newLayout)
        slide.checkPlaceholderXfrm()
      }
    })
    const editChange = new ThemeChanges(
      this,
      EditActionFlag.edit_Presentation_ChangeTheme,
      slidesIndex
    )

    EditorUtil.ChangesStack.addToAction(editChange)

    // 清理对应幻灯片未使用且未被 preserve 的主题
    const slideMasters = this.slides.map((slide) => slide.layout?.master)
    preSlideMasters.forEach((master) => {
      if (
        master &&
        master.preserve !== true &&
        !slideMasters.includes(master)
      ) {
        this.removeSlideMaster(master)
      }
    })

    const selectionState = this.selectionState
    selectionState.checkSelection()

    updateCalcStatusForPresentation(this, editChange)

    calculateForRendering()

    this.syncEditorUIState()
  }

  changeColorScheme(colorScheme) {
    if (!(colorScheme instanceof ClrScheme)) {
      return
    }
    startEditAction(EditActionFlag.action_Presentation_ChangeColorScheme)

    const indices: number[] = []
    for (let i = 0; i < this.slides.length; ++i) {
      const slide = this.slides[i]
      const master = slide.layout?.master
      const theme = master?.theme
      if (theme && !theme.themeElements.clrScheme.sameAs(colorScheme)) {
        theme.changeColorScheme(colorScheme.clone())
      }
      indices.push(i)
    }
    const editChange = new ThemeChanges(
      this,
      EditActionFlag.edit_Presentation_ChangeColorScheme,
      indices
    )
    EditorUtil.ChangesStack.addToAction(editChange)

    updateCalcStatusForPresentation(this, editChange)
    calculateForRendering()
    this.syncEditorUIState()
  }

  removeSlide(pos) {
    if (!this.canEdit()) {
      return
    }
    if (isNumber(pos) && pos > -1 && pos < this.slides.length) {
      const editChange = new EntityContentChange(
        this,
        EditActionFlag.edit_Presentation_RemoveSlide,
        pos,
        [this.slides[pos]],
        false
      )
      EditorUtil.ChangesStack.addToAction(editChange)

      // 删除评论 op 跟随 removeSlideOp 一起生成, 此处不从接口层面进行删除, 仅作为变更发起者通知外部重绘评论
      const deleteSlide = this.slides.splice(pos, 1)[0]
      deleteSlide.isDeleted = true
      const preSlideComment = deleteSlide.slideComments
      if (preSlideComment && preSlideComment.comments.length) {
        hookManager.invoke(Hooks.Emit.EmitChangeComment)
      } else {
        // 严格意义上此时 comment 数据未发生变化, 但若以 commentChange 为唯一事件源则需发送
        if (this.slides.length && this.slides[pos]) {
          const nextSlideComment = this.slides[pos].slideComments
          if (nextSlideComment && nextSlideComment.comments.length) {
            hookManager.invoke(Hooks.Emit.EmitChangeComment)
          }
        }
      }
      updateCalcStatusForPresentation(this, editChange)
      if (this.sectionList) {
        this.sectionList.removeSlide(deleteSlide.getRefId())
      }
      return deleteSlide
    }
    return null
  }

  insertSlide(index: number, slide: Slide, sectionId?: string) {
    const editChange = new EntityContentChange(
      this,
      EditActionFlag.edit_Presentation_AddSlide,
      index,
      [slide],
      true
    )
    EditorUtil.ChangesStack.addToAction(editChange)

    this.slides.splice(index, 0, slide)
    if (slide.slideComments) {
      const arrOfSlideComments = slide.slideComments.comments
      for (let i = 0; i < arrOfSlideComments.length; ++i) {
        hookManager.invoke(Hooks.Emit.EmitAddComment, {
          id: arrOfSlideComments[i].getId(),
          CommentData: arrOfSlideComments[i].extensionData,
        })
      }
    }
    updateCalcStatusForPresentation(this, editChange)
    if (this.sectionList) {
      this.sectionList.insertSlide(index, slide.getRefId(), sectionId)
    }
  }

  moveSlides(slideIndices: number[], index) {
    let insertIndex = index
    const removedSlides: Slide[] = []
    for (let i = slideIndices.length - 1; i > -1; --i) {
      removedSlides.push(this.removeSlide(slideIndices[i])!)
      if (slideIndices[i] < index) {
        --insertIndex
      }
    }
    removedSlides.reverse()
    for (let i = 0; i < removedSlides.length; ++i) {
      this.insertSlide(insertIndex + i, removedSlides[i])
    }
    calculateForRendering()
    hookManager.invoke(Hooks.EditorUI.OnCalculatePreviews)
  }

  onCommentAuthorChange() {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Presentation_SetPresPr,
      null,
      this.pres
    )
  }

  addComment(
    inputData: InputCommentData,
    commentXY?: { x: number; y: number },
    slideIndex?: number
  ) {
    const index = isNumber(slideIndex) ? slideIndex : this.currentSlideIndex
    const curSlide = this.slides[index]
    if (!curSlide) return

    let cmContentData: CmContentData
    if (inputData['origin'] === 'server') {
      cmContentData = toServerCmContentData(inputData)
    } else {
      // origin === 'local'
      cmContentData = toLocalCmContentData(inputData, curSlide.slideComments)
    }

    startEditAction(EditActionFlag.action_Presentation_AddComment, true)
    // 注意添加 slideComments 则不再单独发送单个 comment 变更
    const isSendCommentChange = !curSlide.slideComments

    if (isSendCommentChange) {
      curSlide.setSlideComments(new SlideComments(curSlide))
    }

    const comment = new Comment(curSlide.slideComments)
    comment.selected = true
    comment.fromJSON(cmContentData)
    comment.uid = comment.getUid()
    if (commentXY != null) {
      comment.x = commentXY.x
      comment.y = commentXY.y
    }

    const slideComments = curSlide.slideComments!
    slideComments.addComment(comment, isSendCommentChange)

    for (let i = slideComments.comments.length - 1; i > -1; --i) {
      slideComments.comments[i].selected = false
    }

    return comment
  }

  updateCommentContent(uid: string, content: string) {
    const slidesWithComments = this.slides.filter((s) => !!s.slideComments)
    for (const s of slidesWithComments) {
      const cm = s.slideComments!.findCommentByUid(uid)
      if (cm) {
        startEditAction(EditActionFlag.action_Presentation_ChangeComment, true)
        cm.updateContentText(content)
        break
      }
    }
  }

  canAddComment() {
    if (!this.canEdit()) return false
    if (!EditorSettings.isSupportComment) return false
    if (!EditorSettings.isShowComment) return false
    return true
  }

  hideComments() {
    if (EditorSettings.isShowComment) {
      EditorSettings.isShowComment = false
      return true
    }
    return false
  }

  removeComment(uid) {
    startEditAction(EditActionFlag.action_Presentation_RemoveComment, true)

    let slideIdxForRemovedComments: number = -1

    for (let i = 0; i < this.slides.length; ++i) {
      const slide = this.slides[i]
      const comments = slide.slideComments?.comments
      if (comments) {
        for (let j = 0; j < comments.length; ++j) {
          if (comments[j].getUid() === uid) {
            slide.removeComment(uid)
            slideIdxForRemovedComments = i
            break
          }
        }
      }
    }
    if (slideIdxForRemovedComments > -1) {
      calculateForRendering()
    }

    return slideIdxForRemovedComments
  }

  /** 获取当前 slideComments ���新插入一条评论应该放置的位置, 第一条则返�� -1 */
  getCurrentCommentIndex(y: number): number {
    const slideComment = this.slides[this.currentSlideIndex].slideComments
    if (slideComment && slideComment.comments.length) {
      const sortedComments = this.getSortCommentsByYAxisAsc(
        slideComment!.comments.filter((comment) => !comment.parentComment)
      )
      let i = 0
      const length = sortedComments.length
      for (i; i < length; i++) {
        if (y <= sortedComments[i].y) return i - 1
      }
      return i - 1
    }
    return -1
  }

  /** comments 按 y 坐标从小到大排序, 次按时间从早到晚排序 */
  getSortCommentsByYAxisAsc(ccoments: Comment[]) {
    const comments = ccoments.map((item) => item)
    comments.sort((comment, nextComment) => {
      if (comment.y <= nextComment.y) {
        if (comment.y === nextComment.y) {
          if (comment.timeStamp >= nextComment.timeStamp) return 1
          return -1
        } else {
          return -1
        }
      }
      return 1
    })
    return comments
  }

  /** 默认获取当前页评论 */
  getSlideComments(
    slideIndex?: number
  ): Dict<
    Array<ExtensionCmContentData & { percentX: number; percentY: number }>
  > {
    let slide: Nullable<Slide>
    if (isNumber(slideIndex) && this.slides[slideIndex]) {
      slide = this.slides[slideIndex]
    } else {
      slide = this.slides[this.currentSlideIndex]
    }
    if (!slide) return {}
    const slideComment = slide.slideComments
    const slideRefId = slide.getRefId()
    if (!slideComment) return { [slideRefId]: [] }
    const result = this.getSortCommentsByYAxisAsc(
      slideComment.comments.filter((comment) => !comment.parentComment)
    ).map((comment) => {
      return {
        ...comment.toExtensionJSON(),
        ['percentX']: comment.x / this.width,
        ['percentY']: comment.y / this.height,
      }
    })
    return { [slideRefId]: result }
  }

  canCopyCut() {
    const textTarget = this.getCurrentTextTarget()

    const selectedElements = this.selectionState.selectedSlideElements
    if (textTarget) {
      if (
        true === textTarget.hasTextSelection() &&
        true !== textTarget.hasNoSelection(true)
      ) {
        return true
      } else {
        return false
      }
    } else if (selectedElements.length > 0) {
      return true
    }

    // 只选中幻灯片时
    const focusType = this.getFocusType()
    if (
      focusType === FocusTarget.Thumbnails &&
      this.getSelectedSlideIndices().length > 0
    ) {
      return true
    }
  }

  beforeStartAddShape() {
    const oldFocusOnNotes = this.isNotesFocused
    this.isNotesFocused = false
    this.setFocusType(FocusTarget.EditorView)
    if (oldFocusOnNotes) {
      this.removeSelection()
    }
    emitEditorEvent('HideComment')
  }
}

const _getHierarchySpsInfo: { isBadMatch?: boolean } = {}
function applyLayoutToSlide(slide: Slide, layout: SlideLayout) {
  for (let j = slide.cSld.spTree.length - 1; j > -1; --j) {
    const shape: any = slide.cSld.spTree[j]
    if (isPlaceholderWithEmptyContent(shape)) {
      slide.removeSpById(shape.getId())
    }
  }

  const matchedShapes: Dict<true> = {}
  let lastInsertPosForUnmatched = -1
  for (let j = 0; j < layout.cSld.spTree.length; ++j) {
    const phSp = layout.cSld.spTree[j]
    if (phSp && isPlaceholder(phSp)) {
      const phType = getPlaceholderType(phSp)
      const hf = layout.master?.hf
      const isSpecialPh =
        phType === PlaceholderType.dt ||
        phType === PlaceholderType.ftr ||
        phType === PlaceholderType.hdr ||
        phType === PlaceholderType.sldNum
      if (
        !isSpecialPh ||
        (hf &&
          ((phType === PlaceholderType.dt && hf.dt !== false) ||
            (phType === PlaceholderType.ftr && hf.ftr !== false) ||
            (phType === PlaceholderType.hdr && hf.hdr !== false) ||
            (phType === PlaceholderType.sldNum && hf.sldNum !== false)))
      ) {
        const matchedShape = getMatchedSlideElementForPh(
          slide,
          getPlaceholderType(phSp),
          getIndexOfPlaceholder(phSp),
          isSpHasSingleBodyPh(phSp),
          undefined,
          matchedShapes,
          true
        )
        if (matchedShape == null) {
          const sp = copyLayoutElement(phSp)
          sp.setParent(slide)
          clearSlideElementContent(sp)
          lastInsertPosForUnmatched = lastInsertPosForUnmatched + 1
          slide.addToSpTreeAtIndex(lastInsertPosForUnmatched, sp)
          matchedShapes[sp.id] = true
        } else if (matchedShape) {
          // 找到匹配形状，将 ph 进行关联
          matchedShapes[matchedShape.id] = true
          const nvProps = getUniNvPr(matchedShape)
          if (nvProps) {
            const uniNvProps = getUniNvPr(phSp)
            if (uniNvProps) {
              const { ph } = uniNvProps.nvPr
              if (ph) {
                nvProps.nvPr.setPh(ph.clone())
              }
            }
          }
        }
      }
    }
  }
  slide.setLayout(layout)

  for (let j = slide.cSld.spTree.length - 1; j > -1; --j) {
    const shape: any = slide.cSld.spTree[j]
    const hsps = getHierarchySps(shape, undefined, _getHierarchySpsInfo)
    let isNoPlaceholder = true
    let isNeedResetTransform = false
    let notNullPH
    for (let t = 0; t < hsps.length; ++t) {
      const sp = hsps[t]
      if (sp) {
        if (
          sp.parent &&
          isInstanceTypeOf(sp.parent, InstanceType.SlideLayout)
        ) {
          isNoPlaceholder = false
        }
        if (!notNullPH && sp.spPr?.xfrm?.isValid()) {
          isNeedResetTransform = true
          notNullPH = hsps[t]
        }
      }
    }
    if (isNoPlaceholder) {
      const _hsps = getHierarchySps(shape)
      let t
      for (t = 0; t < _hsps.length; ) {
        const sp = hsps[t]
        if (sp?.spPr?.xfrm?.isValid()) {
          ++t
          break
        }
        ++t
      }

      if (t === _hsps.length) {
        ensureSpPrXfrm(shape)
      }
    } else {
      if (isNeedResetTransform) {
        if (shape.spPr?.xfrm?.isValid()) {
          if (!isInstanceTypeOf(shape, InstanceType.GraphicFrame)) {
            shape.spPr.setXfrm(null)
          }
        } else {
          if (notNullPH) {
            if (!shape.spPr && notNullPH.spPr) {
              shape.setSpPr(notNullPH.spPr.clone())
              shape.spPr.setParent(shape)
            }
            if (!shape.spPr.xfrm && notNullPH.spPr && notNullPH.spPr.xfrm) {
              shape.spPr.setXfrm(notNullPH.spPr.xfrm.clone())
            }
          }
        }
      }
    }
  }
}

function copyLayoutElement(sp: SlideElement) {
  const element = cloneSlideElement(sp)

  // 参考Powerpoint，从版式复制形状，不带上样式
  if (element.instanceType === InstanceType.Shape && element.txBody?.bodyPr) {
    element.txBody.bodyPr.reset()
    element.txBody.lstStyle = undefined
  }

  return element
}

function addOrApplyPrToParagraph<
  T,
  Fn extends (presetation: Presentation, item: T, isDirty?: boolean) => void,
>(presentation: Presentation, fn: Fn, item: T, isDirty?, keepInterface?) {
  if (presentation.slides[presentation.currentSlideIndex]) {
    if (presentation.isNotesFocused) {
      const slide = presentation.slides[presentation.currentSlideIndex]
      if (slide.notes) {
        fn(presentation, item, isDirty)
        isDirty = false
      }
    } else {
      fn(presentation, item, isDirty)
      const selectTextSp = presentation.selectionState.selectedTextSp
      if (!selectTextSp || selectTextSp instanceof GraphicFrame) {
        isDirty = false
      }
    }
    if (false === isDirty) {
      calculateForRendering()
      getAndUpdateCursorPositionForPresentation(presentation)
      const textDoc = presentation.getCurrentTextTarget(false)
      if (textDoc) {
        const paragraph = getCurrentParagraph(textDoc, true)
        if (paragraph) {
          paragraph.cursorPosition.cursorX = paragraph.cursorPosition.x
          paragraph.cursorPosition.cursorY = paragraph.cursorPosition.y
        }
      }
    }

    if (!(keepInterface === true)) {
      presentation.syncEditorUIState()
    }
  }
}
