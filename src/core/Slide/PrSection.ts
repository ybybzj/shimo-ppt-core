import { CTSection, CTSectionLst } from '../../io/dataType/presentation'
import { Presentation } from './Presentation'

export interface PrSectionItem {
  name: string
  uid: string
  slideRefs: Set<string>
}

export class PresentationSectionList {
  private prSections: PrSectionItem[] = []
  private presentation: Presentation
  constructor(presentation: Presentation) {
    this.presentation = presentation
  }
  reset() {
    for (const item of this.prSections) {
      item.slideRefs.clear()
    }
    this.prSections.length = 0
  }
  fromJSON(data: CTSection[]) {
    this.reset()
    if (data.length <= 0) {
      return
    }

    const slideRefs = new Set<string>()
    this.presentation.slides.forEach((s) => {
      if (s.refId != null) slideRefs.add(s.refId)
    })
    for (const secData of data) {
      const item: PrSectionItem = {
        name: secData['name'],
        uid: secData['id'],
        slideRefs: new Set<string>(),
      }

      const sldLst = secData['sldLst']
      if (sldLst && sldLst.length > 0) {
        for (const srid of sldLst) {
          if (slideRefs[srid] != null) {
            item.slideRefs.add(srid)
          }
        }
      }
      this.prSections.push(item)
    }
    slideRefs.clear()
  }

  toJSON(): undefined | CTSectionLst {
    if (this.prSections.length <= 0) return undefined
    return this.prSections.map((item) => {
      return {
        ['name']: item.name,
        ['id']: item.uid,
        ['sldLst']: Array.from(item.slideRefs),
      }
    })
  }
  isEmpty() {
    return this.prSections.length <= 0
  }

  insertSlide(index: number, slideRefId: string, sectionId?: string) {
    if (this.isEmpty()) return
    if (sectionId != null) {
      const sectionItem = this.prSections.find((item) => item.uid === sectionId)
      if (sectionItem != null) {
        sectionItem.slideRefs.add(slideRefId)
        return
      }
    }

    const prevSlide = this.presentation.slides[index]

    if (prevSlide == null) {
      const secItem = this.prSections[0]
      secItem.slideRefs.add(slideRefId)
      return
    }

    const targetSecItem = this.prSections.find((item) =>
      item.slideRefs.has(prevSlide.getRefId())
    )
    if (targetSecItem) {
      targetSecItem.slideRefs.add(slideRefId)
    }
  }

  removeSlide(slideRefId: string) {
    if (this.isEmpty()) return
    for (const item of this.prSections) {
      if (item.slideRefs.has(slideRefId)) {
        item.slideRefs.delete(slideRefId)
      }
    }
  }
}
