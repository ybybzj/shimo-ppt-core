/* eslint-disable @typescript-eslint/no-unused-vars */
import { Dict, Nullable } from '../../../liber/pervasive'
import { CommentsAuthorData } from '../../io/dataType/comment'
import { PresData } from '../../io/dataType/presentation'
import {
  assignJSON,
  assignVal,
  readFromJSON,
  readFromJSONArray,
} from '../../io/utils'
import { ScaleOfPPTXSizes } from '../common/const/unit'
import { TextListStyle } from '../textAttributes/TextListStyle'
import { CommentAuthor } from './Comments/CommentAuthor'
import { commentAuthorDict } from './Comments/GlobalCommentId'
import { Presentation } from './Presentation'

export class PresProps {
  defaultTextStyle?: null | TextListStyle
  slideSize: { cx?: number; cy?: number; type?: string }
  notesSz: null | { cx?: number; cy?: number }
  autoCompressPictures?: null | boolean
  bookmarkIdSeed?: null | number
  compatMode?: null | boolean
  embedTrueTypeFonts?: null | boolean
  firstSlideNum?: null | number
  removePersonalInfoOnSave?: null | boolean
  rtl?: null | boolean
  saveSubsetFonts?: null | boolean
  serverZoom?: null | string
  showSpecialPlsOnTitleSld?: null | boolean
  strictFirstAndLastChars?: null | boolean
  constructor() {
    this.defaultTextStyle = null
    this.slideSize = {}
    this.notesSz = null
    this.autoCompressPictures = null
    this.bookmarkIdSeed = null
    this.compatMode = null
    this.embedTrueTypeFonts = null
    this.firstSlideNum = null
    this.removePersonalInfoOnSave = null
    this.rtl = null
    this.saveSubsetFonts = null
    this.serverZoom = null
    this.showSpecialPlsOnTitleSld = null
    this.strictFirstAndLastChars = null
  }

  toJSON(presentation: Presentation): PresData {
    const data: PresData = {}
    assignVal(
      data,
      keyOfAttr('autoCompressPictures'),
      this.autoCompressPictures
    )
    assignVal(data, keyOfAttr('bookmarkIdSeed'), this.bookmarkIdSeed)
    assignVal(data, keyOfAttr('compatMode'), this.compatMode)
    assignVal(data, keyOfAttr('embedTrueTypeFonts'), this.embedTrueTypeFonts)
    assignVal(data, keyOfAttr('firstSlideNum'), this.firstSlideNum)
    assignVal(
      data,
      keyOfAttr('removePersonalInfoOnSave'),
      this.removePersonalInfoOnSave
    )
    assignVal(data, keyOfAttr('rtl'), this.rtl)
    assignVal(data, keyOfAttr('saveSubsetFonts'), this.saveSubsetFonts)
    assignVal(data, keyOfAttr('serverZoom'), this.serverZoom)
    assignVal(
      data,
      keyOfAttr('showSpecialPlsOnTitleSld'),
      this.showSpecialPlsOnTitleSld
    )
    assignVal(
      data,
      keyOfAttr('strictFirstAndLastChars'),
      this.strictFirstAndLastChars
    )

    assignJSON(data, 'defaultTextStyle', presentation.defaultTextStyle)

    if (this.slideSize) {
      this.slideSize.cx = (presentation.width * ScaleOfPPTXSizes) >> 0
      this.slideSize.cy = (presentation.height * ScaleOfPPTXSizes) >> 0

      data['sldSz'] = {
        ['cx']: this.slideSize.cx,
        ['cy']: this.slideSize.cy,
      }
    }

    this.notesSz = {}
    this.notesSz.cx = (presentation.height * ScaleOfPPTXSizes) >> 0
    this.notesSz.cy = (presentation.width * ScaleOfPPTXSizes) >> 0
    const commentsAuthors: CommentsAuthorData[] = []
    for (const id in commentAuthorDict) {
      commentsAuthors.push(commentAuthorDict[id].toJSON())
    }
    data['commentsAuthors'] = commentsAuthors
    data['notesSz'] = {
      ['cx']: this.notesSz.cx,
      ['cy']: this.notesSz.cy,
    }

    return data
  }

  fromJSON(data: PresData) {
    {
      const key = keyOfAttr('autoCompressPictures')
      const valueOfSrc = data[key]
      if (valueOfSrc != null) {
        this.autoCompressPictures = valueOfSrc
      }
    }
    {
      const key = keyOfAttr('bookmarkIdSeed')
      const valueOfSrc = data[key]
      if (valueOfSrc != null) {
        this.bookmarkIdSeed = valueOfSrc
      }
    }
    {
      const key = keyOfAttr('compatMode')
      const valueOfSrc = data[key]
      if (valueOfSrc != null) {
        this.compatMode = valueOfSrc
      }
    }
    {
      const key = keyOfAttr('embedTrueTypeFonts')
      const valueOfSrc = data[key]
      if (valueOfSrc != null) {
        this.embedTrueTypeFonts = valueOfSrc
      }
    }
    {
      const key = keyOfAttr('firstSlideNum')
      const valueOfSrc = data[key]
      if (valueOfSrc != null) {
        this.firstSlideNum = valueOfSrc
      }
    }
    {
      const key = keyOfAttr('removePersonalInfoOnSave')
      const valueOfSrc = data[key]
      if (valueOfSrc != null) {
        this.removePersonalInfoOnSave = valueOfSrc
      }
    }
    {
      const key = keyOfAttr('rtl')
      const valueOfSrc = data[key]
      if (valueOfSrc != null) {
        this.rtl = valueOfSrc
      }
    }
    {
      const key = keyOfAttr('saveSubsetFonts')
      const valueOfSrc = data[key]
      if (valueOfSrc != null) {
        this.saveSubsetFonts = valueOfSrc
      }
    }
    {
      const key = keyOfAttr('serverZoom')
      const valueOfSrc = data[key]
      if (valueOfSrc != null) {
        this.serverZoom = valueOfSrc
      }
    }
    {
      const key = keyOfAttr('showSpecialPlsOnTitleSld')
      const valueOfSrc = data[key]
      if (valueOfSrc != null) {
        this.showSpecialPlsOnTitleSld = valueOfSrc
      }
    }
    {
      const key = keyOfAttr('strictFirstAndLastChars')
      const valueOfSrc = data[key]
      if (valueOfSrc != null) {
        this.strictFirstAndLastChars = valueOfSrc
      }
    }

    const commentAuthorList = readFromJSONArray(
      data['commentsAuthors'],
      CommentAuthor
    ) as CommentAuthor[]
    if (commentAuthorList?.length) {
      for (const author of commentAuthorList) {
        commentAuthorDict[String(author.id)] = author
      }
    }

    const defaultTextStyle = readFromJSON(
      data['defaultTextStyle'],
      TextListStyle
    )
    if (defaultTextStyle) {
      this.defaultTextStyle = defaultTextStyle
    }

    if (data['sldSz']) {
      if (data['sldSz']['cx'] != null) {
        this.slideSize.cx = data['sldSz']['cx']
      }
      if (data['sldSz']['cy'] != null) {
        this.slideSize.cy = data['sldSz']['cy']
      }
    }
  }
}

function keyOfAttr<S extends string>(k: S): `attr${Capitalize<S>}` {
  return `attr${capitalize(k)}` as `attr${Capitalize<S>}`
}
function capitalize(s: string): string {
  if (s.length <= 0) return s
  return s[0].toUpperCase() + s.slice(1)
}
