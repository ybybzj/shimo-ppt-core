import { FocusTarget } from '../common/const/drawing'
import { hookManager, Hooks } from '../hooks'
import { Presentation } from './Presentation'

export class SlideSelectManager {
  presentation: Presentation
  focusTargetType: FocusTarget = FocusTarget.EditorView
  selectedSlideIndices: number[] = []
  private isLockOnEditor: boolean = false

  constructor(presentation: Presentation) {
    this.presentation = presentation
  }

  reset() {
    this.selectedSlideIndices = []
    this.focusTargetType = FocusTarget.EditorView
  }

  setSelectedIndices(indexes: number[]) {
    this.selectedSlideIndices = indexes
  }

  getSelectedIndices() {
    return this.selectedSlideIndices
  }

  setFocusTargetType(focusType: FocusTarget) {
    if (focusType === FocusTarget.Thumbnails && this.isLockOnEditor === true) {
      return
    }
    this.focusTargetType = focusType
    hookManager.invoke(Hooks.EditorUI.OnComponentFocus, { focusType })
  }

  getFocusTargetType() {
    return this.focusTargetType
  }

  invokeWithLockFocusOnMainObject(callback: () => void) {
    this.isLockOnEditor = true
    callback()
    this.isLockOnEditor = false
  }

  lockFocusOnMain() {
    this.isLockOnEditor = true
  }

  unlockFocusOnMain() {
    this.isLockOnEditor = false
  }
}
