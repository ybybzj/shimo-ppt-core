import { Dict } from '../../../../liber/pervasive'
import { CommentsAuthorData } from '../../../io/dataType/comment'
import { readFromJSON } from '../../../io/utils'
import { Hooks, hookManager } from '../../hooks'
import { CommentAuthor } from './CommentAuthor'

export const commentAuthorDict: Dict<CommentAuthor> = {}

export function getCommentAuthorLastId(
  authorId: number | string
): number | null {
  const author = commentAuthorDict['' + authorId]
  if (!author) {
    return null
  }
  return author.lastId
}

export function addCommentAuthor(authorData: CommentsAuthorData) {
  const auhorId = authorData.id
  const author = readFromJSON(authorData, CommentAuthor)!
  commentAuthorDict[auhorId] = author

  informCommentAuthorChange()
}

export function updateCommentAuthorLastId(authorId: number, lastId: number) {
  const author = commentAuthorDict[authorId]
  if (author && author.updateLastId(lastId)) {
    informCommentAuthorChange()
  }
  return author.lastId
}

export function increaseCommentAuthorLastId(authorId: number) {
  const author = commentAuthorDict[authorId]
  if (author) {
    author.increaseLastId()
  }
  informCommentAuthorChange()
  return author?.lastId
}

export function updateCommentAuthorLastIdOnRemove(
  authorId: number,
  idx: number
) {
  const author = commentAuthorDict[authorId]
  if (author && author.onRemove(idx)) {
    if (author.isEmpty()) {
      delete commentAuthorDict[authorId]
    }

    informCommentAuthorChange()
  }
}

function informCommentAuthorChange() {
  hookManager.invoke(Hooks.Emit.EmitChangeCommentAuthor)
}
