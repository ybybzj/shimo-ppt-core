import { EditorUtil } from '../../../globals/editor'
import { idGenerator } from '../../../globals/IdGenerator'
import { EditActionFlag } from '../../EditActionFlag'
import { InstanceType } from '../../instanceTypes'
import { CmContentData, CommentsAuthorData } from '../../../io/dataType/comment'
import { assignVal } from '../../../io/utils'
import { hookManager, Hooks } from '../../hooks'
import {
  addCommentAuthor,
  commentAuthorDict,
  getCommentAuthorLastId,
  increaseCommentAuthorLastId,
  updateCommentAuthorLastId,
  updateCommentAuthorLastIdOnRemove,
} from './GlobalCommentId'
import { SlideComments } from './SlideComments'
import { Dict, Nullable } from '../../../../liber/pervasive'
import { CommentStatusType } from '../../common/const/drawing'
import { padNumber, toInt } from '../../../common/utils'
import { CanvasDrawer } from '../../graphic/CanvasDrawer'
import { GraphicsBoundsChecker } from '../../graphic/Bounds'
import { renderPresentationComment } from '../../render/comment'
import { checkBounds_comment } from '../../checkBounds/comment'
import { informEntityEditChange } from '../../calculation/informChanges/editChanges'
import { informEntityPropertyChange } from '../../calculation/informChanges/propertyChange'

export class Comment {
  readonly instanceType = InstanceType.Comment
  authorId: number
  authorName: string
  exportAdditionalData: string
  exportTimeStamp: number // 导出 xml 需, Time + m_nTimeZoneBias * 6000
  exportTimeZoneBias: string
  id: string // 全局 id, 用于全局获取对象
  idx: number | null // 每个 author 独立累加计算, 无法作为唯一标志
  parentComment: Nullable<Comment>
  replies: Comment[]
  selected: boolean // TODO: remove viewModel Status
  slideComments: SlideComments
  solved: boolean
  text: string
  timeStamp: number
  uid: string | null // unique id, 用于导入导出的 Dict key, authorId 补全 4 位, idx 补全四位组成
  x: number
  y: number
  extensionData?: Dict // textExt, 对接石墨评论
  parentId?: number
  parentAuthorId?: number
  // 临时 comment 可能没有 slideComment, 此处不指明类型
  constructor(slideComments) {
    this.id = idGenerator.newId()
    this.uid = null
    this.idx = null
    this.uid = null
    this.x = 0
    this.y = 0
    this.selected = false
    this.slideComments = slideComments
    this.text = ''
    this.timeStamp = Date.parse(new Date().toString())
    this.authorId = 0
    this.authorName = ''
    this.solved = false
    this.replies = []
    this.exportTimeZoneBias = '0'
    this.exportTimeStamp =
      this.timeStamp + Number(this.exportTimeZoneBias) * 6000
    this.exportAdditionalData = ''
    EditorUtil.entityRegistry.add(this, this.id)
  }

  onDispose() {
    this.slideComments = null as any
    EditorUtil.entityRegistry.removeEntityById(this.id)

    const authorLastidx = getCommentAuthorLastId(this.authorId)
    if (authorLastidx != null && this.idx != null) {
      updateCommentAuthorLastIdOnRemove(this.authorId, this.idx)
    }
  }

  /** 获取内存标记 id, 可以使用 Editor.gTableId.getById(id) 全局获取该对象*/
  getId() {
    return this.id
  }

  /** 导出到 xml 用 id, 与 AuthorId 唯一确定一条评论 */
  getIdx(): number {
    const authorLastidx = getCommentAuthorLastId(this.authorId)
    if (authorLastidx == null) {
      const authorData: CommentsAuthorData = {
        id: this.authorId,
        name: this.authorName,
        initials: '',
        lastidx: 1,
      }
      addCommentAuthor(authorData)
      this.setIdx(authorData.lastidx)
      return authorData.lastidx
    } else {
      if (this.idx == null) {
        increaseCommentAuthorLastId(this.authorId)
        this.setIdx(authorLastidx)
      } else {
        updateCommentAuthorLastId(this.authorId, this.idx)
      }
      return this.idx as number
    }
  }
  setIdx(id: number) {
    this.idx = id
  }

  /** 导入导出约定 id 作 key, 唯一 id,  由 4 位 AuthorId + 4 位 Idx 组成 */
  getUid(): string {
    if (this.uid) return this.uid
    const authorId = this.getAuthorId()
    const idx = this.getIdx()
    this.setUid(padNumber(authorId, 4) + padNumber(idx, 4))
    return this.uid!
  }

  setUid(uid: string) {
    if (this.uid) return
    if (!uid) return
    this.uid = uid
  }

  getX(): number {
    return this.x
  }
  setX(x: number) {
    this.x = x
  }

  getY(): number {
    return this.y
  }
  setY(y: number) {
    this.y = y
  }

  getText(): string {
    return this.text
  }
  setText(text: string) {
    this.text = text
  }

  getAuthorId(): number {
    return this.authorId
  }
  setAuthorId(id: number) {
    this.authorId = id
  }
  getAuthorName(): string {
    return this.authorName
  }
  setAuthorName(name: string) {
    this.authorName = name
  }

  getExportAdditionalData() {
    return this.exportAdditionalData
  }
  setExportAdditionalData(addtionalData) {
    this.exportAdditionalData = addtionalData
  }

  getParentAuthorId(): number | undefined {
    return this.parentAuthorId
  }
  setParentAuthorId(id: number) {
    this.parentAuthorId = id
  }

  getParentCommentId() {
    return this.parentId
  }
  setParentCommentId(id) {
    this.parentId = id
  }

  getExportTimeZoneBias(): string {
    return this.exportTimeZoneBias
  }
  setExportTimeZoneBias(timeZone: string) {
    this.exportTimeZoneBias = timeZone
  }

  getTimeStamp(): number {
    return this.timeStamp
  }
  setTimeStamp(time: number) {
    this.timeStamp = time
  }

  getExportTimeStamp(): number {
    return this.exportTimeStamp
  }
  setExportTimeStamp(time: number) {
    this.exportTimeStamp = time
  }

  getExtensionData() {
    return this.extensionData
  }

  setExtensionData(data) {
    this.extensionData = data
  }

  setContent(data: CmContentData) {
    this.fromJSON(data)
    informEntityEditChange(this, EditActionFlag.edit_Comment_Change)
  }

  updateContentText(text: string) {
    this.setText(text)
    informEntityEditChange(this, EditActionFlag.edit_Comment_Change)
  }

  clone(parent) {
    const ret = new Comment(parent)
    ret.x = this.x
    ret.y = this.y
    ret.text = this.text
    ret.timeStamp = this.timeStamp
    ret.exportTimeZoneBias = this.exportTimeZoneBias
    ret.exportTimeStamp = this.exportTimeStamp
    ret.authorId = this.authorId
    ret.authorName = this.authorName
    ret.solved = this.solved
    ret.exportAdditionalData = this.exportAdditionalData
    ret.replies = this.replies.map((item) => item.clone(parent))
    ret.parentId = this.parentId
    ret.parentAuthorId = this.parentAuthorId
    ret.extensionData = this.extensionData
    return ret
  }

  setPosition(x, y) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Comment_Position,
      { x: this.x, y: this.y },
      { x, y }
    )
    this.x = x
    this.y = y
  }

  draw(graphics: CanvasDrawer | GraphicsBoundsChecker) {
    const isHovered = hookManager.get(Hooks.Cursor.GetHoveredStatus, {
      gTableId: this.getId(),
    })
    const status = this.selected
      ? CommentStatusType.Select
      : isHovered
      ? CommentStatusType.Hover
      : CommentStatusType.Default

    if (graphics.instanceType === InstanceType.CanvasDrawer) {
      renderPresentationComment(graphics, status, this.x, this.y)
    } else {
      checkBounds_comment(graphics, status, this.x, this.y)
    }
  }

  dateToISO8601(d: Date) {
    function pad(n) {
      return n < 10 ? '0' + n : n
    }
    return (
      d.getUTCFullYear() +
      '-' +
      pad(d.getUTCMonth() + 1) +
      '-' +
      pad(d.getUTCDate()) +
      'T' +
      pad(d.getUTCHours()) +
      ':' +
      pad(d.getUTCMinutes()) +
      ':' +
      pad(d.getUTCSeconds()) +
      'Z'
    )
  }

  iso8601ToDate(dateStr) {
    const numericKeys = [1, 4, 5, 6, 7, 10, 11]
    let minutesOffset = 0
    let struct
    if (
      (struct =
        /^(\d{4}|[+\-]\d{6})(?:-(\d{2})(?:-(\d{2}))?)?(?:T(\d{2}):(\d{2})(?::(\d{2})(?:\.(\d{3}))?)?(?:(Z)|([+\-])(\d{2})(?::(\d{2}))?)?)?$/.exec(
          dateStr
        ))
    ) {
      // avoid NaN timestamps caused by “undefined” values being passed to Date.UTC
      for (let i = 0, k; (k = numericKeys[i]); ++i) {
        struct[k] = +struct[k] || 0
      }

      // allow undefined days and months
      struct[2] = (+struct[2] || 1) - 1
      struct[3] = +struct[3] || 1

      if (struct[8] !== 'Z' && struct[9] !== undefined) {
        minutesOffset = struct[10] * 60 + struct[11]

        if (struct[9] === '+') {
          minutesOffset = 0 - minutesOffset
        }
      }

      const _ret = new Date(
        Date.UTC(
          struct[1],
          struct[2],
          struct[3],
          struct[4],
          struct[5] + minutesOffset,
          struct[6],
          struct[7]
        )
      )
      return '' + _ret.getTime()
    }
    return '1'
  }

  iso8601ToTime(dateStr): number {
    const numericKeys = [1, 4, 5, 6, 7, 10, 11]
    let minutesOffset = 0
    let struct
    if (
      (struct =
        /^(\d{4}|[+\-]\d{6})(?:-(\d{2})(?:-(\d{2}))?)?(?:T(\d{2}):(\d{2})(?::(\d{2})(?:\.(\d{3}))?)?(?:(Z)|([+\-])(\d{2})(?::(\d{2}))?)?)?$/.exec(
          dateStr
        ))
    ) {
      // avoid NaN timestamps caused by “undefined” values being passed to Date.UTC
      for (let i = 0, k; (k = numericKeys[i]); ++i) {
        struct[k] = +struct[k] || 0
      }

      // allow undefined days and months
      struct[2] = (+struct[2] || 1) - 1
      struct[3] = +struct[3] || 1

      if (struct[8] !== 'Z' && struct[9] !== undefined) {
        minutesOffset = struct[10] * 60 + struct[11]

        if (struct[9] === '+') {
          minutesOffset = 0 - minutesOffset
        }
      }

      const ret = new Date(
        Date.UTC(
          struct[1],
          struct[2],
          struct[3],
          struct[4],
          struct[5] + minutesOffset,
          struct[6],
          struct[7]
        )
      )
      return Date.parse(ret.toString())
    }
    return Date.parse(new Date().toString())
  }

  toJSON(): CmContentData {
    if (!this.extensionData) {
      // 导入原生评论
      const cmContentData: CmContentData = {
        ['origin']: 'local',
        ['authorId']: this.authorId,
        ['idx']: this.getIdx(),
        ['uid']: this.getUid(),
        ['pos']: { x: toInt(this.x * 22.66), y: toInt(this.y * 22.66) },
        ['text']: this.text,
      }
      assignVal(
        cmContentData,
        'dt',
        this.dateToISO8601(new Date(this.timeStamp))
      )
      if (this.parentAuthorId) {
        assignVal(cmContentData, 'parentAuthorId', this.parentAuthorId)
      }
      if (this.parentId) {
        assignVal(cmContentData, 'parentCommentId', this.parentId)
      }
      assignVal(cmContentData, 'timeZoneBias', this.exportTimeZoneBias)
      if (this.exportAdditionalData) {
        assignVal(cmContentData, 'additionalData', this.exportAdditionalData)
      }
      return cmContentData
    } else {
      // 新增石墨评论
      const cmContentData: CmContentData = {
        ['origin']: 'server',
        ['uid']: this.getUid(),
        ['pos']: { x: toInt(this.x * 22.66), y: toInt(this.y * 22.66) },
        ['textExt']: this.extensionData,
      }
      return cmContentData
    }
  }

  toExtensionJSON(): ExtensionCmContentData {
    const cmContentData = this.toJSON()
    if (cmContentData.origin !== 'server') {
      if (this.replies.length) {
        cmContentData['replies'] = []
        this.replies.forEach((reply) => {
          cmContentData['replies'].push(reply.toExtensionJSON())
        })
      }
      cmContentData['authorName'] = this.getAuthorName()
    }
    return cmContentData
  }

  fromJSON(data: CmContentData) {
    const uid = data['uid']
    if (data.origin !== 'server') {
      // 导入原生评论, 兼容原 idx 作 key
      const authorId = data['authorId']
      const idx = data['idx']
      const pos = data['pos']
      const text = data['text']
      const additionalData = data['additionalData']
      const dt = data['dt']
      const parentAuthorId = data['parentAuthorId']
      const parentCommentId = data['parentCommentId']
      const timeZoneBias = data['timeZoneBias']
      this.setAuthorId(authorId)

      this.setAuthorName(commentAuthorDict[this.authorId]?.name ?? '')

      this.setIdx(idx)
      if (uid) {
        this.setUid(uid)
      }
      this.x = toInt(pos.x / 22.66)
      this.y = toInt(pos.y / 22.66)
      this.setText(text)
      if (additionalData) this.setExportAdditionalData(additionalData)
      if (dt) {
        this.setTimeStamp(this.iso8601ToTime(dt))
        this.setTimeStamp(this.timeStamp + Number(data.timeZoneBias) * 6000)
      }
      if (parentAuthorId) this.setParentAuthorId(parentAuthorId)
      if (parentCommentId) this.setParentCommentId(parentCommentId)
      if (timeZoneBias) this.setExportTimeZoneBias(timeZoneBias)
    } else {
      // 石墨评论, 协作接受新增评论场景
      const pos = data['pos']
      const uid = data['uid']
      this.setExtensionData(data['textExt'])
      this.x = toInt(pos.x / 22.66)
      this.y = toInt(pos.y / 22.66)
      if (uid) this.setUid(data['uid'])
    }
  }
}

type ExtensionReplyData = {
  replies: ExtensionCmContentData
  authorName: string
}
export type ExtensionCmContentData = CmContentData | ExtensionReplyData
