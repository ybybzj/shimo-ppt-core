import { Dict, Nullable } from '../../../../liber/pervasive'
import { EditorKit } from '../../../editor/global'
import { idGenerator } from '../../../globals/IdGenerator'
import { gRefIdCounter, RefIdPrefixes } from '../../../globals/RefIdCounter'
import { EditActionFlag } from '../../EditActionFlag'
import { InstanceType } from '../../instanceTypes'
import { CmContentData } from '../../../io/dataType/comment'
import { assignJSON, readFromJSON } from '../../../io/utils'

import { hookManager, Hooks } from '../../hooks'
import { Slide } from '../Slide'
import { Comment } from './Comment'
import { informEntityChildrenChange } from '../../calculation/informChanges/contentChange'
import { find } from '../../../../liber/l/find'

export class SlideComments {
  readonly instanceType = InstanceType.SlideComments
  comments: Comment[]
  slide: Slide
  id: string
  refId?: string
  constructor(slide: Slide) {
    this.comments = []
    this.slide = slide
    this.id = idGenerator.newId()
  }
  getRefId(): string {
    const refId = this.refId ?? gRefIdCounter.newIDBy(RefIdPrefixes.Comment)
    this.refId = refId
    return refId
  }
  setRefId(rId: string) {
    this.refId = rId
  }

  getId() {
    return this.id
  }

  addComment(comment: Comment, ignoreSendChange = false) {
    if (comment.parentId) {
      for (const cmItem of this.comments) {
        if (
          cmItem.idx === comment.parentId &&
          cmItem.authorId === comment.parentAuthorId
        ) {
          cmItem.replies.push(comment)
          comment.parentComment = cmItem
          break
        }
      }
    }

    if (ignoreSendChange !== true) {
      informEntityChildrenChange(
        this,
        EditActionFlag.edit_SlideComments_AddComment,
        this.comments.length,
        [comment],
        true
      )
    }
    this.comments.splice(this.comments.length, 0, comment)
  }

  getComments(): Comment[] {
    return this.comments
  }
  setComments(comments: Comment[]) {
    this.comments = comments
  }
  getValidComments(): Comment[] {
    if (this.comments.length) {
      const checkFunc = EditorKit.shouldDisplayCommentFunc
      return this.comments.filter((item) => {
        const isValid = checkFunc ? checkFunc(item.toJSON()) : true
        return !item.parentComment && isValid
      })
    }
    return []
  }

  getSlide(): Slide {
    return this.slide
  }
  setSlide(slide: Slide) {
    this.slide = slide
  }

  changeComment(uid, commentData: CmContentData) {
    for (let i = 0; i < this.comments.length; ++i) {
      if (this.comments[i].getUid() === uid) {
        this.comments[i].setContent(commentData)
        return
      }
    }
  }

  findCommentByUid(uid: string): Nullable<Comment> {
    return find((cm) => cm.getUid() === uid, this.comments)
  }

  _removeComment(uid) {
    const removedUids: Record<string, true> = {}

    for (const comment of this.comments) {
      if (comment.getUid() === uid) {
        removedUids[uid] = true
        const parent = comment.parentComment
        if (parent) {
          const delIndex = parent.replies.findIndex(
            (item) => item.idx === comment.idx
          )
          parent.replies.splice(delIndex, 1)
        }

        _onCollectRemoveUidsForReplies(comment.replies, removedUids)
      }
    }
    let hasRemoved: boolean = false
    const updatedComments = this.comments.filter((cmt, i) => {
      const isRemove = removedUids[cmt.getUid()]
      if (isRemove) {
        informEntityChildrenChange(
          this,
          EditActionFlag.edit_SlideComments_RemoveComment,
          i,
          [cmt],
          false
        )
        cmt.onDispose()
        hasRemoved = true
      }
      return !isRemove
    })
    this.comments = updatedComments

    return hasRemoved
  }

  removeComment(uid) {
    const result = this._removeComment(uid)
    if (result === true) {
      hookManager.invoke(Hooks.Comments.OnRemoveComment, { id: uid })
    }
  }

  toJSON(): Dict<CmContentData> | null {
    if (this.comments.length) {
      const cmContentDataDict: Dict<CmContentData> = {}
      for (const comment of this.comments) {
        assignJSON(cmContentDataDict, String(comment.getUid()), comment)
      }
      return cmContentDataDict
    } else {
      return null
    }
  }

  fromJSON(cmContentDataDict: Dict<CmContentData>) {
    const comments: Comment[] = []
    for (const uid in cmContentDataDict) {
      const comment = readFromJSON(cmContentDataDict[uid], Comment)
      if (comment) {
        comment.slideComments = this
        comments.push(comment)
      }
    }
    for (const comment of comments) {
      if (comment.parentId && comment.parentAuthorId) {
        for (const parentComment of comments) {
          if (
            comment.parentId === parentComment.idx &&
            comment.parentAuthorId === parentComment.getAuthorId()
          ) {
            parentComment.replies.push(comment)
            comment.parentComment = parentComment
          }
        }
      }
    }
    this.setComments(comments)
  }
}

function _onCollectRemoveUidsForReplies(replies: Comment[], collection: Record<string, true>) {
  if (replies.length > 0) {
    for (const cmt of replies) {
      collection[cmt.getUid()] = true
      _onCollectRemoveUidsForReplies(cmt.replies, collection)
    }
  }
}