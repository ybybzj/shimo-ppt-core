import { CommentsAuthorData } from '../../../io/dataType/comment'

export class CommentAuthor {
  name: string
  id: number
  lastId: number
  private nextLastId: number
  private removedIdxQueue: number[] = []
  initials: string
  constructor() {
    this.name = ''
    this.id = 0
    this.lastId = 0
    this.nextLastId = 0
    this.initials = ''
  }

  toJSON(): CommentsAuthorData {
    const data: CommentsAuthorData = {
      ['id']: this.id,
      ['initials']: this.initials,
      ['lastidx']: this.lastId,
      ['name']: this.name,
    }
    return data
  }

  fromJSON(data: CommentsAuthorData) {
    this.name = data['name']
    this.id = data['id']
    this.lastId = data['lastidx']
    this.initials = data['initials']
    this.calculate()
  }

  private calculate() {
    const arr = this.name.split(' ')
    this.initials = ''
    for (let i = 0; i < arr.length; i++) {
      if (arr[i].length > 0) this.initials += arr[i].substring(0, 1)
    }
    this.nextLastId = 0
    this.removedIdxQueue = []
  }

  increaseLastId() {
    this.nextLastId = this.lastId
    this.lastId += 1
  }

  updateLastId(lastId: number) {
    switch (true) {
      case lastId < this.nextLastId: {
        const index = this.removedIdxQueue.indexOf(lastId)
        if (index > -1) {
          this.removedIdxQueue.splice(index, 1)
        }
        return false
      }
      case lastId > this.nextLastId && lastId < this.lastId: {
        let updated = false
        for (let i = this.nextLastId + 1; i < lastId; i++) {
          if (this.removedIdxQueue.indexOf(i) === -1) {
            this.removedIdxQueue.push(i)
            updated = true
          }
        }
        if (updated) {
          this.removedIdxQueue.sort()
        }

        this.nextLastId = lastId
        return false
      }
      case lastId > this.lastId: {
        this.nextLastId = this.lastId
        this.lastId = lastId
        return true
      }
      default:
        return false
    }
  }

  onRemove(idx: number) {
    switch (true) {
      case idx < this.nextLastId: {
        updateRemoveQueue(idx, this.removedIdxQueue, this.nextLastId)
        return false
      }
      case idx === this.nextLastId: {
        this.nextLastId = nextLastIdFromQueue(
          this.removedIdxQueue,
          this.nextLastId
        )
        return false
      }
      case idx === this.lastId: {
        this.lastId = this.nextLastId
        this.nextLastId = nextLastIdFromQueue(
          this.removedIdxQueue,
          this.nextLastId
        )
        return true
      }
      default:
        return false
    }
  }

  isEmpty() {
    return this.lastId <= 0 && this.removedIdxQueue.length <= 0
  }
}

// helpers
function updateRemoveQueue(
  removedIdx: number,
  q: number[],
  nextLastId: number
): boolean {
  if (removedIdx >= nextLastId) return false
  if (q.indexOf(removedIdx) === -1) {
    q.push(removedIdx)
    q.sort()
    return true
  }
  return false
}

function nextLastIdFromQueue(q: number[], removedLastId: number): number {
  const nextLastId = removedLastId - 1
  if (q.length <= 0) return nextLastId
  const nextRemovedIdx = q[q.length - 1]!
  if (nextRemovedIdx < nextLastId) return nextLastId
  else {
    q.pop()
    return nextLastIdFromQueue(q, nextRemovedIdx)
  }
}
