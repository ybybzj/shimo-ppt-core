import { Dict, Nullable } from '../../../liber/pervasive'
import { BrowserInfo } from '../../common/browserInfo'
import { InstanceType } from '../instanceTypes'
import { SlideElement } from '../SlideElement/type'
import {
  CommentStatus,
  CommentStatusType,
  GCommentIconsSource,
} from '../common/const/drawing'
import { Factor_mm_to_pix, Factor_pix_to_mm } from '../common/const/unit'
import { EditorSettings } from '../common/EditorSettings'
import { ZoomValue } from '../common/zoomValue'
import { ConnectionShape } from '../SlideElement/ConnectionShape'

export function mmsPerPt(value: number) {
  let ptsPerMM = Factor_mm_to_pix
  if (!EditorSettings.isRenderingNotes) {
    ptsPerMM = (ZoomValue.value * Factor_mm_to_pix) / 100
  }
  return value / ptsPerMM
}

export function getWidthOfComment(
  status: CommentStatus,
  zoomValue?: number
): number {
  const commentIconType = status ?? CommentStatusType.Default
  const srcWidthInMM =
    GCommentIconsSource[commentIconType].width * Factor_pix_to_mm
  return zoomValue == null
    ? srcWidthInMM
    : srcWidthInMM * ((zoomValue * BrowserInfo.PixelRatio) / 100)
}

export function getHeightOfComment(
  status: CommentStatus,
  zoomValue?: number
): number {
  const commentIconType = status ?? CommentStatusType.Default
  const srcHeightInMM =
    GCommentIconsSource[commentIconType].height * Factor_pix_to_mm
  return zoomValue == null
    ? srcHeightInMM
    : (srcHeightInMM * zoomValue * BrowserInfo.PixelRatio) / 100
}

export function convertPxToMM(pix: number): number {
  return mmsPerPt(pix) ?? pix / Factor_mm_to_pix
}

export function resetConnectionShapesId(
  arrOfCopyObjects: Array<SlideElement | { slideElement: SlideElement }>,
  map: Dict<string>
) {
  for (let i = 0; i < arrOfCopyObjects.length; ++i) {
    const sp = (arrOfCopyObjects[i] as { slideElement: SlideElement })
      .slideElement
      ? (arrOfCopyObjects[i] as { slideElement: SlideElement }).slideElement
      : (arrOfCopyObjects[i] as SlideElement)
    if (sp.instanceType === InstanceType.Shape && sp.isConnectionShape()) {
      const cnxSp = sp as ConnectionShape
      const stId = cnxSp.getStCxnId()
      const endId = cnxSp.getEndCxnId()
      let stCxnId: Nullable<string> = null,
        endCxnId: Nullable<string> = null
      if (stId && map[stId]) {
        stCxnId = map[stId]
      }
      if (endId && map[endId]) {
        endCxnId = map[endId]
      }
      if (stId !== stCxnId || endCxnId !== endId) {
        const nvUniSpPr = cnxSp.nvSpPr!.nvUniSpPr.clone()
        if (!stCxnId) {
          nvUniSpPr.stCxnIdx = null
          nvUniSpPr.stCxnId = null
        } else {
          nvUniSpPr.stCxnId = stCxnId
        }
        if (!endCxnId) {
          nvUniSpPr.endCxnIdx = null
          nvUniSpPr.endCxnId = null
        } else {
          nvUniSpPr.endCxnId = endCxnId
        }
        cnxSp.nvSpPr!.setUniSpPr(nvUniSpPr)
      }
    } else if (sp.instanceType === InstanceType.GroupShape) {
      resetConnectionShapesId(sp.spTree, map)
    }
  }
}

export function checkBackupBulletText(char) {
  const WingdingsBulletBackupArialMap = {
    ['•']: '•',
    ['²']: '✧',
    ['ü']: '✓',
    ['Ø']: '➤',
    ['à']: '→',
    ['w']: '◆',
    ['§']: '■',
  }
  return WingdingsBulletBackupArialMap[char] ?? '•'
}

export function checkBackupBulletShimoText(char) {
  const WingdingsBulletBackupShimoMap = {
    ['•']: String.fromCodePoint(57345), // 'U+E001',
    ['²']: String.fromCodePoint(57346), // 'U+E002',
    ['ü']: String.fromCodePoint(57347), // 'U+E003',
    ['Ø']: String.fromCodePoint(57348), // 'U+E004',
    ['à']: String.fromCodePoint(57349), // 'U+E005',
    ['w']: String.fromCodePoint(57350), // 'U+E006',
    ['§']: String.fromCodePoint(57351), // 'U+E007',
  }
  return WingdingsBulletBackupShimoMap[char] ?? '•'
}

export function getRTLMirrorChar(char: string) {
  const map = {
    '(': ')',
    ')': '(',
  }
  return map[char] ?? char
}
