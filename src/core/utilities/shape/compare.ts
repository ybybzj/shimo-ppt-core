import { Nullable } from '../../../../liber/pervasive'
import { SlideElement } from '../../SlideElement/type'
import { GroupShape } from '../../SlideElement/GroupShape'

export function groupComparor(a: SlideElement, b: SlideElement) {
  if (a.group == null && b.group == null) return 0
  if (a.group == null) return 1
  if (b.group == null) return -1

  let c1 = 0
  let grp: Nullable<GroupShape> = a.group
  while (grp != null) {
    ++c1
    grp = grp.group
  }
  let c2 = 0
  grp = b.group
  while (grp != null) {
    ++c2
    grp = grp.group
  }
  return c1 - c2
}
