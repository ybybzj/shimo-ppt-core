import { Nullable } from '../../../../liber/pervasive'
import { PresetShapeTypes } from '../../../exports/graphic'
import { InstanceType, isInstanceTypeOf } from '../../instanceTypes'
import { ImageSubTypes } from '../../../io/dataType/spAttrs'
import { SlideElement } from '../../SlideElement/type'
import { PlaceholderType } from '../../SlideElement/const'
import { BodyPr } from '../../SlideElement/attrs/bodyPr'
import { GroupShape } from '../../SlideElement/GroupShape'
import { Shape } from '../../SlideElement/Shape'
import { getPresentation } from '../finders'
import { OleObject } from '../../SlideElement/OleObject'

export function isSlideElement(o: any): o is SlideElement {
  if (Object(o) !== o || o.instanceType == null) {
    return false
  }
  switch (o.instanceType) {
    case InstanceType.Shape:
    case InstanceType.ImageShape:
    case InstanceType.GroupShape:
    case InstanceType.GraphicFrame:
      return true
    default:
      return false
  }
}
export function isGroup(o: any): o is GroupShape {
  if (Object(o) !== o || o.instanceType == null) {
    return false
  }
  return o.instanceType === InstanceType.GroupShape
}

export function isShapeOfTable(sp: SlideElement): boolean {
  return (
    sp.instanceType === InstanceType.GraphicFrame &&
    isInstanceTypeOf(sp.graphicObject, InstanceType.Table)
  )
}

export function isNoteShape(sp: SlideElement): boolean {
  const parent = sp.parent
  return isInstanceTypeOf(parent, InstanceType.Notes)
}

export function isPlaceholder(sp: SlideElement): boolean {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      return sp?.nvSpPr?.nvPr?.ph != null
    case InstanceType.ImageShape:
      return sp?.nvPicPr?.nvPr?.ph != null
    case InstanceType.GroupShape:
      return sp?.nvGrpSpPr?.nvPr?.ph != null
    case InstanceType.GraphicFrame:
      return sp?.nvGraphicFramePr?.nvPr?.ph != null
    default:
      return false
  }
}

export function isPlaceholderWithEmptyContent(sp: SlideElement): boolean {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      if (isPlaceholder(sp)) {
        if (
          sp.nvSpPr!.nvPr.ph!.type === PlaceholderType.title ||
          sp.nvSpPr!.nvPr.ph!.type === PlaceholderType.ctrTitle ||
          sp.nvSpPr!.nvPr.ph!.type === PlaceholderType.body ||
          sp.nvSpPr!.nvPr.ph!.type === PlaceholderType.subTitle ||
          sp.nvSpPr!.nvPr.ph!.type == null ||
          sp.nvSpPr!.nvPr.ph!.type === PlaceholderType.dt ||
          sp.nvSpPr!.nvPr.ph!.type === PlaceholderType.ftr ||
          sp.nvSpPr!.nvPr.ph!.type === PlaceholderType.hdr ||
          sp.nvSpPr!.nvPr.ph!.type === PlaceholderType.sldNum ||
          sp.nvSpPr!.nvPr.ph!.type === PlaceholderType.sldImg
        ) {
          if (sp.txBody) {
            if (sp.txBody.textDoc) {
              return sp.txBody.textDoc.isEmpty()
            }
            return true
          }
          return true
        }
        if (
          sp.nvSpPr!.nvPr.ph!.type === PlaceholderType.chart ||
          sp.nvSpPr!.nvPr.ph!.type === PlaceholderType.media
        ) {
          return true
        }
        if (sp.nvSpPr!.nvPr.ph!.type === PlaceholderType.pic) {
          let isEmptyText = true
          if (sp.txBody) {
            if (sp.txBody.textDoc) {
              isEmptyText = sp.txBody.textDoc.isEmpty()
            }
          }
          return isEmptyText
        }
      }
      return false

    case InstanceType.ImageShape:
    case InstanceType.GroupShape:
    case InstanceType.GraphicFrame:
      return false
  }
}

export function isSlideElementInPPT(sp: SlideElement) {
  if (sp.group) {
    const arrOfSpTree = sp.group.spTree
    for (let i = 0; i < arrOfSpTree.length; ++i) {
      if (arrOfSpTree[i] === sp) {
        return isSlideElementInPPT(sp.group)
      }
    }
    return false
  }
  if (sp.parent && sp.parent.cSld && !!getPresentation(sp.parent)) {
    const arrOfSpTree = sp.parent.cSld.spTree
    for (let i = 0; i < arrOfSpTree.length; ++i) {
      if (arrOfSpTree[i] === sp) {
        return true
      }
    }
  }
  return false
}

export function isChartImage(sp: Nullable<SlideElement>): boolean {
  if (sp == null) {
    return false
  }
  switch (sp.instanceType) {
    case InstanceType.ImageShape:
      return sp?.subType === ImageSubTypes.Chart || sp.isChartObject()
    default:
      return false
  }
}

export function isOleImage(sp: Nullable<SlideElement>): sp is OleObject {
  if (sp == null) {
    return false
  }
  switch (sp.instanceType) {
    case InstanceType.ImageShape:
      return sp.isOleObject()
    default:
      return false
  }
}

export function isCustomImage(sp: SlideElement): boolean {
  switch (sp.instanceType) {
    case InstanceType.ImageShape:
      return sp?.subType === ImageSubTypes.Custom
    default:
      return false
  }
}

export function isImageFillShape(sp: SlideElement): boolean {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      const brush = sp.brush
      if (
        brush &&
        brush.fill &&
        brush.fill.instanceType === InstanceType.BlipFill &&
        brush.fill.imageSrcId
      ) {
        return true
      }
      return false
    case InstanceType.ImageShape:
    case InstanceType.GroupShape:
    case InstanceType.GraphicFrame:
      return false
  }
}

export function isLineConnector(preset: Nullable<PresetShapeTypes>) {
  if (!preset) return false
  return preset === 'line' || preset.indexOf('straightConnector') > -1
}

export function isEmptySlideElement(sp: any) {
  if (!isSlideElement(sp)) {
    return true
  }
  switch (sp.instanceType) {
    case InstanceType.GraphicFrame:
      return sp.graphicObject == null || sp.graphicObject.isEmpty()
    case InstanceType.Shape:
    case InstanceType.ImageShape:
    case InstanceType.GroupShape:
      return false
    default:
      return false
  }
}

export function isBodyPrPresetSet(bodyPr: Nullable<BodyPr>) {
  return (
    bodyPr != null &&
    bodyPr.prstTxWarp != null &&
    bodyPr.prstTxWarp.preset !== 'textNoShape'
  )
}

export function isTextContentSelected(shape: Shape) {
  const selectedTextSp = getPresentation(shape)?.selectionState.selectedTextSp
  return selectedTextSp ? selectedTextSp === shape : false
}

export function isValidSlideElement(sp: SlideElement) {
  if (!isSlideElement(sp)) {
    return false
  }
  if (sp.isDeleted === true) {
    return false
  }
  return sp.isValid()
}

export function isLinePreset(preset: Nullable<string>) {
  return (
    preset === 'line' ||
    preset === 'bentConnector2' ||
    preset === 'bentConnector3' ||
    preset === 'bentConnector4' ||
    preset === 'bentConnector5' ||
    preset === 'curvedConnector2' ||
    preset === 'curvedConnector3' ||
    preset === 'curvedConnector4' ||
    preset === 'curvedConnector5' ||
    preset === 'straightConnector1'
  )
}

export function isLineShape(sp: SlideElement) {
  return !!(
    sp?.instanceType === InstanceType.Shape &&
    sp.spPr &&
    sp.spPr.geometry &&
    isLinePreset(sp.spPr.geometry.preset)
  )
}
