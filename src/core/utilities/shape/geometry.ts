import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../common/utils'
import { calculateGeometry } from '../../calculation/calculate/geometry'
import { createGeometry } from '../../SlideElement/geometry/creators'

export function getGeometryForSp(sp: SlideElement) {
  if (
    sp.instanceType === InstanceType.Shape ||
    sp.instanceType === InstanceType.ImageShape
  ) {
    if (sp.calcedGeometry) {
      return sp.calcedGeometry
    }
  }

  let geom = sp.spPr?.geometry
  if (geom) {
    return geom
  }

  const s = turnOffRecordChanges()
  geom = createGeometry('rect')
  calculateGeometry(geom, sp.extX, sp.extY)
  restoreRecordChangesState(s)
  return geom
}
