import { SlideElement } from '../../SlideElement/type'
import {
  createMatrix,
  MatrixUtils,
  IMatrix2D,
  Matrix2D,
} from '../../graphic/Matrix'
import { ShapeDrawerContext } from '../../graphic/type'
import { Bounds, WithBounds } from '../../graphic/Bounds'
import { ScaleOfPPTXSizes } from '../../common/const/unit'
import { WithExts } from '../../render/drawShape/type'
import { getTopMostGroupShape } from './getters'

const _updateBounds = (bounds: WithBounds, x: number, y: number) =>
  Bounds.prototype.update.call(bounds, x, y)
export function checkBoundsForShape(sp: WithExts, bounds: WithBounds) {
  _updateBounds(bounds, 0, 0)
  _updateBounds(bounds, sp.extX, 0)
  _updateBounds(bounds, sp.extX, sp.extY)
  _updateBounds(bounds, 0, sp.extY)
}

export function updateBoundsByXYExt(
  bounds: WithBounds,
  x: number,
  y: number,
  extX: number,
  extY: number
) {
  _updateBounds(bounds, x, y)
  _updateBounds(bounds, x + extX, y)
  _updateBounds(bounds, x + extX, y + extY)
  _updateBounds(bounds, x, y + extY)
}

export function getShapeBounds(sp: SlideElement): {
  x: number
  y: number
  w: number
  h: number
  strokeWidth: number
} {
  const bounds = new Bounds()
  checkBoundsForShape(sp, bounds)
  const x = bounds.minX
  const y = bounds.minY
  const w = bounds.maxX - x
  const h = bounds.maxY - y
  let strokeWidth = sp.pen?.w == null ? 12700 : parseInt('' + sp.pen.w)
  strokeWidth /= ScaleOfPPTXSizes
  return {
    x,
    y,
    w: w + strokeWidth,
    h: h + strokeWidth,
    strokeWidth,
  }
}

/** 不计算 strokeWidth 的坐标 */
export function getShapeCoordBound(sp: WithExts): WithBounds {
  const checker = new Bounds()
  checkBoundsForShape(sp, checker)
  const { minX, minY, maxX, maxY } = checker
  return { minX, minY, maxX, maxY }
}

export function getFlipTransformForSp(sp: SlideElement): IMatrix2D {
  const flipTransform = new Matrix2D()
  const topMostGroup = getTopMostGroupShape(sp)

  let w: number, h: number, flipH: boolean, flipV: boolean
  if (topMostGroup) {
    const bounds = getShapeBounds(topMostGroup)
    w = bounds.w
    h = bounds.h
    flipH = !!topMostGroup.flipH
    flipV = !!topMostGroup.flipV
  } else {
    const bounds = getShapeBounds(sp)
    w = bounds.w
    h = bounds.h
    flipH = !!sp.flipH
    flipV = !!sp.flipV
  }

  MatrixUtils.flip(flipTransform, w, h, flipH, flipV)
  return flipTransform
}

export function getInvertTransformForConvert(sp: SlideElement): Matrix2D {
  let invertTr: Matrix2D = sp.invertTransform
  const topMostGroup = getTopMostGroupShape(sp)
  if (topMostGroup) {
    invertTr = topMostGroup.invertTransform
  }
  return createMatrix(invertTr)
}

export function invertTransformConverter(
  sp: SlideElement,
  g: ShapeDrawerContext,
  tr: IMatrix2D
) {
  const invertTr = getInvertTransformForConvert(sp)
  const transform = createMatrix(tr)
  if (invertTr) {
    MatrixUtils.multiplyMatrixes(transform, invertTr)
  }

  const flipTransform = getFlipTransformForSp(sp)
  MatrixUtils.multiplyMatrixes(transform, flipTransform)
  return transform
}
