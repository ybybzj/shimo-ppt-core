import { isBase64Url, isDict } from '../../../common/utils'
import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { getBase64ImgForSlideElement } from '../../render/toImage'
import { gGlobalTableStyles } from '../../Slide/GlobalTableStyles'
import { GraphicFrame } from '../../SlideElement/GraphicFrame'
import { GroupShape } from '../../SlideElement/GroupShape'
import { ImageShape } from '../../SlideElement/Image'
import { Shape } from '../../SlideElement/Shape'
import { setLocks } from './locks'
import { setGroup, setDeletedForSp } from './updates'

import { ConnectionShape } from '../../SlideElement/ConnectionShape'
import { OleObject } from '../../SlideElement/OleObject'
import { CalcStatus } from '../../calculation/updateCalcStatus/calcStatus'
import { Dict, Nullable } from '../../../../liber/pervasive'
import { BlipFill } from '../../SlideElement/attrs/fill/blipFill'
import { hookManager, Hooks } from '../../hooks'
import { SpPr } from '../../SlideElement/attrs/shapePrs'

export function cloneSlideElement<T extends SlideElement>(sp: T, map?): T {
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      const isConnector = sp.isConnectionShape()
      const dup = isConnector ? new ConnectionShape() : new Shape()
      dup.spTreeParent = sp.spTreeParent
      if (sp.nvSpPr) dup.setNvSpPr(sp.nvSpPr.clone(dup))
      if (sp.spPr) {
        dup.setSpPr(sp.spPr.clone())
        dup.spPr!.setParent(dup)
      }
      if (sp.style) {
        dup.setStyle(sp.style.clone())
      }
      if (sp.txBody) {
        dup.setTxBody(sp.txBody.clone())
        dup.txBody!.setParent(dup)
      }
      if (sp.attrUseBgFill === true) {
        dup.attrUseBgFill = true
      }
      populateSp<Shape | ConnectionShape>(sp, dup)
      return dup as T
    }

    case InstanceType.GroupShape: {
      return cloneGroupShape(sp, cloneSlideElement, map) as T
    }
    case InstanceType.GraphicFrame: {
      const ret = new GraphicFrame()
      ret.spTreeParent = sp.spTreeParent
      if (sp.graphicObject) {
        ret.setGraphicObject(sp.graphicObject.clone(ret))
        if (gGlobalTableStyles.tableStyles != null) {
          ret.graphicObject!.resetDimension(
            0,
            0,
            sp.graphicObject.xLimit,
            sp.graphicObject.yLimit
          )
        }
      }
      if (sp.nvGraphicFramePr) {
        ret.setNvSpPr(sp.nvGraphicFramePr.clone())
      }
      if (sp.spPr) {
        ret.setSpPr(sp.spPr.clone())
        ret.spPr!.setParent(ret)
      }
      setDeletedForSp(ret, false)

      if (
        !sp.isCalcStatusSet(CalcStatus.GraphicFrame.Table) &&
        !sp.isCalcStatusSet(CalcStatus.GraphicFrame.Sizes) &&
        !sp.isCalcStatusSet(CalcStatus.GraphicFrame.Transform)
      ) {
        ret.base64Img = getBase64ImgForSlideElement(sp)
        ret.base64ImgWidth = sp.base64ImgWidth
        ret.base64ImgHeight = sp.base64ImgHeight
      }
      return ret as T
    }
    case InstanceType.ImageShape: {
      // clone ChartObject as ImageShape
      const isOleObject = sp.isOleObject()
      const dup = isOleObject ? new OleObject() : new ImageShape()
      dup.spTreeParent = sp.spTreeParent
      dup.subType = sp.subType
      if (sp.nvPicPr) {
        dup.setNvPicPr(sp.nvPicPr.clone())
      }
      if (sp.spPr) {
        dup.setSpPr(sp.spPr.clone())
        dup.spPr!.setParent(dup)
      }
      if (sp.blipFill) {
        if (sp.isChartObject()) {
          const blipFill = new BlipFill(getBase64ImgForSlideElement(sp))
          blipFill.setStretch(true)
          dup.setBlipFill(blipFill)
        } else {
          const imgSrc = sp.blipFill.imageSrcId
          if (isBase64Url(imgSrc)) {
            hookManager.invoke(Hooks.Attrs.CollectBase64ImageFromDataChange, {
              dataUrl: imgSrc,
              sp: dup,
            })
          }
          dup.setBlipFill(sp.blipFill.clone())
        }
      }
      if (sp.style) {
        dup.setStyle(sp.style.clone())
      }

      if (isOleObject) {
        const oleObjectCopy = dup as OleObject
        const oleObject = sp as OleObject
        oleObject.copyTo(oleObjectCopy)
      }

      populateSp<OleObject | ImageShape>(sp, dup)
      return dup as T
    }
  }
}

export function cloneGroupShape(
  group: GroupShape,
  cloneFn: <T extends SlideElement>(sp: T, map?: Dict) => T,
  map?: Dict
): GroupShape {
  const dup = new GroupShape()
  dup.spTreeParent = group.spTreeParent
  if (group.nvGrpSpPr) {
    dup.setNvGrpSpPr(group.nvGrpSpPr.clone())
  }
  if (group.spPr) {
    dup.setSpPr(group.spPr.clone())
    dup.spPr!.setParent(dup)
  }
  for (let i = 0; i < group.spTree.length; ++i) {
    const ssp = group.spTree[i]
    const subCopy = cloneFn(ssp, map)

    if (isDict(map)) {
      map[ssp.id] = subCopy.id
    }
    dup.addToSpTree(dup.spTree.length, subCopy)
    setGroup(dup.spTree[dup.spTree.length - 1], dup)
  }

  populateSp(group, dup)

  return dup
}

export function updateOffsetByGroupParent(sp: SlideElement, grp: GroupShape) {
  let group: Nullable<GroupShape> = grp
  while (group != null) {
    updateSpPr(sp.spPr, group.spPr)
    group = group.group
  }
}

function updateSpPr(basePr: Nullable<SpPr>, grpSpPr: Nullable<SpPr>) {
  if (basePr == null || basePr.xfrm == null) return
  if (grpSpPr == null || grpSpPr.xfrm == null) return
  basePr.xfrm.offX += grpSpPr.xfrm.offX
  basePr.xfrm.offY += grpSpPr.xfrm.offY
}

function populateSp<T extends SlideElement>(srcSp: T, targetSp: T) {
  setDeletedForSp(targetSp, srcSp.isDeleted)
  setLocks(targetSp, srcSp.locks)
  targetSp.base64Img = getBase64ImgForSlideElement(srcSp as unknown as Shape)
  targetSp.base64ImgHeight = srcSp.base64ImgHeight
  targetSp.base64ImgWidth = srcSp.base64ImgWidth
}
