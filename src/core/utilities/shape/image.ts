import { MediaType } from '../../common/const/attrs'
import { isErrorMediaUrl } from '../../../images/errorImageSrc'
import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { EditorSettings } from '../../common/EditorSettings'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { ImageShape } from '../../SlideElement/Image'
import { getMediaFromShape } from './getters'

export function isVideoImage(sp: SlideElement): boolean {
  if (EditorSettings.isSupportVideo === false) {
    return false
  }
  switch (sp.instanceType) {
    case InstanceType.ImageShape:
      const unimedia = getMediaFromShape(sp)
      if (isErrorMediaUrl(unimedia?.media)) {
        return false
      }
      return unimedia?.type === MediaType.video
    default:
      return false
  }
}

export function isAudioImage(sp: SlideElement): boolean {
  if (EditorSettings.isSupportVideo === false) {
    return false
  }
  switch (sp.instanceType) {
    case InstanceType.ImageShape:
      const unimedia = getMediaFromShape(sp)
      if (isErrorMediaUrl(unimedia?.media)) {
        return false
      }
      return unimedia?.type === MediaType.audio
    default:
      return false
  }
}

export enum ImageWidgetType {
  Video,
  Gif,
}

export function setRenderingContentPenBrush(sp: ImageShape) {
  const transparent =
    sp.blipFill?.getTransparent && sp.blipFill.getTransparent()
  sp.brush = FillEffects.Blip(sp.blipFill)
  sp.brush.transparent = transparent
  sp.pen = null
}
