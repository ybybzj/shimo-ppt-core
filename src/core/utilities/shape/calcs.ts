import { LineHeightRule } from '../../common/const/attrs'
import { isNumber, isDict } from '../../../common/utils'
import { InstanceType, isInstanceTypeOf } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { GraphicsBoundsChecker } from '../../graphic/Bounds'
import { Matrix2D, MatrixUtils } from '../../graphic/Matrix'
import { normalizeRotValue } from '../../common/utils'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { Xfrm } from '../../SlideElement/attrs/shapePrs'
import { convertPxToMM } from '../helpers'
import { isPlaceholder } from './asserts'
import {
  checkTextTransformMatrix,
  getCalcedFill,
  getCalcedStyleForSp,
  getCalcedTransparent,
  getFinalFlipH,
  getFinalFlipV,
  getFinalRotate,
  getHierarchySps,
  getParentsOfElement,
} from './getters'
import { Dict, Nullable } from '../../../../liber/pervasive'
import { Paragraph } from '../../Paragraph/Paragraph'
import { ParaHyperlink } from '../../Paragraph/ParaContent/ParaHyperlink'
import { hookManager, Hooks } from '../../hooks'
import { GroupShape } from '../../SlideElement/GroupShape'
import { checkBounds_SlideElement } from '../../checkBounds/slideElement'
import { getGeometryForSp } from './geometry'
import { convertToConnectorInfo } from '../../SlideElement/ConnectorInfo'
import { calculateGeometry } from '../../calculation/calculate/geometry'
import { ParaPr } from '../../textAttributes/ParaPr'
import { TextListStyle } from '../../textAttributes/TextListStyle'
import { TableCell } from '../../Table/TableCell'
import { CalcStatus } from '../../calculation/updateCalcStatus/calcStatus'
import { calcScaleFactorsOfGroup } from './group'
import { ManagedSliceUtil } from '../../../common/managedArray'
import { ParaSpacingOptions } from '../../textAttributes/ParaSpacing'
import { CONTROL_DISTANCE_ROTATE } from '../../common/const/control'

export function calcTransform(sp: SlideElement) {
  sp.base64Img = null
  getAndUpdateTransform(sp)
  sp.calcedTransform.reset(sp.transform)
  sp.invertTransform = MatrixUtils.invertMatrixFrom(
    sp.invertTransform,
    sp.transform
  )
}

export function getInvertTransformWithAnimation(sp: SlideElement) {
  const isInShowMod = hookManager.get(Hooks.EditorUI.GetIsInShowMod)

  if (isInShowMod) {
    const animationDrawInfo = sp.getAnimationDrawInfo()
    if (animationDrawInfo) {
      return animationDrawInfo.action === 'NoDraw'
        ? null
        : animationDrawInfo.transform &&
            MatrixUtils.invertMatrix(animationDrawInfo.transform)
    }
  }

  return getInvertTransform(sp)
}

export function getInvertTextTransformWithAnimation(sp: SlideElement) {
  const isInShowMod = hookManager.get(Hooks.EditorUI.GetIsInShowMod)

  if (isInShowMod && sp.instanceType === InstanceType.Shape) {
    const animationDrawInfo = sp.getAnimationDrawInfo()
    if (animationDrawInfo) {
      switch (animationDrawInfo.action) {
        case 'NoDraw':
          return null
        case 'Draw': {
          const phContent = sp.txBody?.phContent
          if (phContent) {
            const transform = new Matrix2D()
            const bodyPr = sp.getBodyPr()
            checkTextTransformMatrix(sp, transform, phContent, bodyPr, {
              transform: animationDrawInfo.transform!,
              rot: animationDrawInfo.rot ?? 0,
            })
            return MatrixUtils.invertMatrix(transform)
          }
        }
      }
    }
  }
  return sp.instanceType === InstanceType.Shape ? sp.invertTextTransform : null
}
export function getInvertTransform(sp: SlideElement): Matrix2D {
  switch (sp.instanceType) {
    case InstanceType.Shape:
    case InstanceType.GroupShape:
      return sp.invertTransform
    case InstanceType.ImageShape:
      if (sp.isCalcStatusSet(CalcStatus.ImageShape.Transform)) {
        calcTransform(sp)
        sp.unsetCalcStatusOf(CalcStatus.ImageShape.Transform)
      }
      return sp.invertTransform
    case InstanceType.GraphicFrame:
      if (sp.isCalcStatusSet(CalcStatus.GraphicFrame.Transform)) {
        calcTransform(sp)
        sp.unsetCalcStatusOf(CalcStatus.GraphicFrame.Transform)
      }
      return sp.invertTransform
  }
}

export function getSpRotateAng(sp: SlideElement, x: number, y: number): number {
  const transform = sp.getTransform()
  const rotDist = convertPxToMM(CONTROL_DISTANCE_ROTATE)
  const hc = sp.extX * 0.5
  const vc = sp.extY * 0.5
  const xc = transform.XFromPoint(hc, vc)
  const yc = transform.YFromPoint(hc, vc)
  const rotX = transform.XFromPoint(hc, -rotDist)
  const rotY = transform.YFromPoint(hc, -rotDist)

  const invertTransform = getInvertTransform(sp)
  const tx = invertTransform.XFromPoint(x, y)

  const vx1 = x - xc
  const vy1 = y - yc

  const vx2 = rotX - xc
  const vy2 = rotY - yc

  const flipH = getFinalFlipH(sp)
  const flipV = getFinalFlipV(sp)
  const isSameFlip = (flipH && flipV) || (!flipH && !flipV)
  const ang =
    tx > sp.extX * 0.5
      ? Math.atan2(Math.abs(vx1 * vy2 - vy1 * vx2), vx1 * vx2 + vy1 * vy2)
      : -Math.atan2(Math.abs(vx1 * vy2 - vy1 * vx2), vx1 * vx2 + vy1 * vy2)
  return isSameFlip ? ang : -ang
}

function getAndUpdateTransform(sp: SlideElement) {
  if (!isDict(sp.group)) {
    if (sp.spPr?.xfrm?.isValid()) {
      const xfrm = sp.spPr.xfrm
      sp.x = xfrm.offX
      sp.y = xfrm.offY
      sp.extX = xfrm.extX
      sp.extY = xfrm.extY
      sp.rot = isNumber(xfrm.rot) ? xfrm.rot : 0
      sp.flipH = xfrm.flipH === true
      sp.flipV = xfrm.flipV === true
    } else {
      if (isPlaceholder(sp)) {
        const hsps = getHierarchySps(sp)
        let i: number
        for (i = 0; i < hsps.length; ++i) {
          const hsp = hsps[i]
          if (hsp?.spPr?.xfrm?.isValid()) {
            const xfrm = hsp.spPr.xfrm
            sp.x = xfrm.offX
            sp.y = xfrm.offY
            sp.extX = xfrm.extX
            sp.extY = xfrm.extY
            sp.rot = isNumber(xfrm.rot) ? xfrm.rot : 0
            sp.flipH = xfrm.flipH === true
            sp.flipV = xfrm.flipV === true
            break
          }
        }
        if (i === hsps.length) {
          sp.x = 0
          sp.y = 0
          sp.extX = 5
          sp.extY = 5
          sp.rot = 0
          sp.flipH = false
          sp.flipV = false
        }
      } else {
        sp.x = 0
        sp.y = 0

        sp.extX = 5
        sp.extY = 5
        sp.rot = 0
        sp.flipH = false
        sp.flipV = false
      }
    }
  } else {
    let xfrm: Xfrm
    if (sp.spPr && sp.spPr.xfrm && sp.spPr.xfrm.isValid()) {
      xfrm = sp.spPr.xfrm!
    } else {
      if (isPlaceholder(sp)) {
        const hsps = getHierarchySps(sp)
        let i: number
        for (i = 0; i < hsps.length; ++i) {
          const hsp = hsps[i]
          if (hsp?.spPr?.xfrm?.isValid()) {
            xfrm = hsp.spPr.xfrm
            break
          }
        }
        if (i === hsps.length) {
          xfrm = new Xfrm(sp.spPr)
          xfrm.offX = 0
          xfrm.offX = 0
          xfrm.extX = 5
          xfrm.extY = 5
        }
      } else {
        xfrm = new Xfrm(sp.spPr)
        xfrm.offX = 0
        xfrm.offY = 0
        xfrm.extX = 5
        xfrm.extY = 5
      }
    }

    const { coff, cx, cy } = calcScaleFactorsOfGroup(sp.group)
    const { chOffX, chOffY } = sp.group.spPr!.xfrm!
    sp.x = coff * (xfrm!.offX - chOffX)
    sp.y = coff * (xfrm!.offY - chOffY)
    sp.extX = cx * xfrm!.extX
    sp.extY = cy * xfrm!.extY
    sp.rot = isNumber(xfrm!.rot) ? xfrm!.rot : 0
    sp.flipH = xfrm!.flipH === true
    sp.flipV = xfrm!.flipV === true
  }

  const calcProps: TransformProps = {
    pos: { x: sp.x, y: sp.y },
    size: { w: sp.extX, h: sp.extY },
    flipH: sp.flipH,
    flipV: sp.flipV,
    rot: sp.rot,
    group: sp.group,
  }

  calcTransformFormProps(sp.transform, calcProps)
}

export interface TransformProps {
  pos: { x: number; y: number }
  size: { w: number; h: number }
  flipH: boolean
  flipV: boolean
  rot: number
  group?: Nullable<GroupShape>
}

/**
 * 计算 transform 的统一逻辑, 包含自身位移，缩放(翻转)，旋转，以及上层 Group (可选)
 * 返回全新的 transform 引用，外部直接替换时注意关联对象的引用
 * */
export function calcTransformFormProps(
  transform: Matrix2D,
  props: TransformProps
) {
  transform.reset()
  const {
    pos: { x, y },
    size: { w, h },
    flipH,
    flipV,
    rot,
    group,
  } = props
  const hc = w / 2
  const vc = h / 2
  MatrixUtils.translateMatrix(transform, -hc, -vc)

  if (flipH) MatrixUtils.scaleMatrix(transform, -1, 1)
  if (flipV) MatrixUtils.scaleMatrix(transform, 1, -1)
  MatrixUtils.rotateMatrix(transform, -rot)

  MatrixUtils.translateMatrix(transform, x + hc, y + vc)
  if (group) {
    MatrixUtils.multiplyMatrixes(transform, group.getCalcedTransform())
  }
}

export function calculateBrush(sp: SlideElement) {
  if (
    sp.instanceType === InstanceType.GroupShape ||
    sp.instanceType === InstanceType.GraphicFrame
  ) {
    return
  }

  const { style: calcedStyle } = getCalcedStyleForSp(sp)
  const rgba = { r: 0, g: 0, b: 0, a: 255 }
  const parents = getParentsOfElement(sp)
  if (
    isDict(parents.theme) &&
    isDict(calcedStyle) &&
    isDict(calcedStyle.fillRef)
  ) {
    sp.brush = parents.theme.getFillStyle(
      calcedStyle.fillRef.idx,
      calcedStyle.fillRef.color
    )
  } else {
    sp.brush = new FillEffects()
  }

  sp.brush!.merge(getCalcedFill(sp))
  sp.brush!.transparent = getCalcedTransparent(sp)
  sp.brush!.calculate(
    parents.theme,
    parents.slide,
    parents.layout,
    parents.master,
    rgba
  )
}

export function calculateBounds(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
    case InstanceType.ImageShape: {
      const checker = new GraphicsBoundsChecker()
      checkBounds_SlideElement(sp, checker)
      checker.correctBoundsWithLines()

      sp.bounds.x = checker.bounds.minX
      sp.bounds.y = checker.bounds.minY
      sp.bounds.l = checker.bounds.minX
      sp.bounds.t = checker.bounds.minY
      sp.bounds.r = checker.bounds.maxX
      sp.bounds.b = checker.bounds.maxY
      sp.bounds.w = checker.bounds.maxX - checker.bounds.minX
      sp.bounds.h = checker.bounds.maxY - checker.bounds.minY
      break
    }
    case InstanceType.GroupShape: {
      const sp_tree = sp.spTree
      let arrOfX_max: number[] = [],
        arrOfY_max: number[] = [],
        arrOfX_min: number[] = [],
        arrOfY_min: number[] = []
      for (let i = 0; i < sp_tree.length; ++i) {
        calculateBounds(sp_tree[i])
        const { l, t, r, b } = sp_tree[i].bounds
        arrOfX_max.push(r)
        arrOfX_min.push(l)
        arrOfY_max.push(b)
        arrOfY_min.push(t)
      }

      if (!sp.group) {
        const tr = sp.calcedTransform
        const { xArr, yArr } = calcXYPointsByTransform(tr, sp.extX, sp.extY)
        arrOfX_max = arrOfX_max.concat(xArr)
        arrOfX_min = arrOfX_min.concat(xArr)
        arrOfY_max = arrOfY_max.concat(yArr)
        arrOfY_min = arrOfY_min.concat(yArr)
      }

      sp.bounds.x = Math.min(...arrOfX_min)
      sp.bounds.y = Math.min(...arrOfY_min)
      sp.bounds.l = sp.bounds.x
      sp.bounds.t = sp.bounds.y
      sp.bounds.r = Math.max(...arrOfX_max)
      sp.bounds.b = Math.max(...arrOfY_max)
      sp.bounds.w = sp.bounds.r - sp.bounds.l
      sp.bounds.h = sp.bounds.b - sp.bounds.t
      break
    }
  }
}

export function calcXYPointsByTransform(tr: Matrix2D, w: number, h: number) {
  const xArr: number[] = []
  const yArr: number[] = []
  xArr.push(tr.XFromPoint(0, 0))
  yArr.push(tr.YFromPoint(0, 0))
  xArr.push(tr.XFromPoint(w, 0))
  yArr.push(tr.YFromPoint(w, 0))
  xArr.push(tr.XFromPoint(w, h))
  yArr.push(tr.YFromPoint(w, h))
  xArr.push(tr.XFromPoint(0, h))
  yArr.push(tr.YFromPoint(0, h))
  return { xArr, yArr }
}

export function checkTextFit(
  sp: SlideElement | TableCell,
  fontScale?: number,
  spacingScale?: number
) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      const txBody = sp.txBody
      if (txBody?.bodyPr) {
        if (isNumber(fontScale) || isNumber(spacingScale)) {
          const textDoc = txBody.textDoc ?? txBody.phContent
          if (textDoc) {
            ManagedSliceUtil.forEach(textDoc.children, (para) => {
              updateTextFitInPara(para, fontScale, spacingScale)
            })
          }
        }
      }

      if (
        sp.parent?.instanceType === InstanceType.SlideLayout ||
        sp.parent?.instanceType === InstanceType.SlideMaster
      ) {
        const lstStyle = txBody?.lstStyle
        if (lstStyle) {
          checkTextFitForTextListStyle(lstStyle, fontScale, spacingScale)
        }
      }
      break
    case InstanceType.TableCell:
      if (isNumber(fontScale) || isNumber(spacingScale)) {
        const textDoc = sp.textDoc
        if (textDoc) {
          ManagedSliceUtil.forEach(textDoc.children, (para) => {
            updateTextFitInPara(para, fontScale, spacingScale)
          })
        }
      }
      break
  }
}

function updateTextFitInPara(
  paragraph: Paragraph,
  fontScale?: number,
  spacingScale?: number
) {
  if (isNumber(spacingScale)) {
    const paraPr = paragraph.pr
    const spacing = paraPr.spacing
    const newSpacing: Dict = {}
    const spc = spacing.line! * spacingScale
    newSpacing.lineRule = spacing.lineRule
    if (isNumber(spc)) {
      if (spacing.lineRule === LineHeightRule.Auto) {
        newSpacing.line = spacing.line! - spacingScale
      } else {
        newSpacing.line = spc
      }
    }
    paragraph.setSpacing(newSpacing)
  }

  if (isNumber(fontScale)) {
    checkParagraphContent(paragraph, fontScale)
  }
}

function checkParagraphContent(
  para: Paragraph | ParaHyperlink,
  fontScale: number
) {
  for (let r = 0, l = para.children.length; r < l; ++r) {
    const item = para.children.at(r)!
    switch (item.instanceType) {
      case InstanceType.ParaRun: {
        const pr = item.pr
        if (isNumber(pr.fontSize)) {
          item.setFontSize(Math.round(pr.fontSize * fontScale))
        }
        break
      }
      case InstanceType.ParaHyperlink: {
        checkParagraphContent(item, fontScale)
        break
      }
    }
  }

  if (para.instanceType === InstanceType.Paragraph) {
    if (para.paraTextPr && para.paraTextPr.textPr) {
      if (isNumber(para.paraTextPr.textPr.fontSize)) {
        para.paraTextPr.setFontSize(
          Math.round(para.paraTextPr.textPr.fontSize * fontScale)
        )
      }
    }
    if (isNumber(para.pr?.defaultRunPr?.fontSize)) {
      para.pr!.defaultRunPr!.fontSize = Math.round(
        para.pr.defaultRunPr!.fontSize * fontScale
      )
    }

    para.invalidateCalcedPrs()
  }
}

function checkTextFitForParaPr(
  paraPr: ParaPr,
  fontScale?: number,
  spacingScale?: number
) {
  if (isNumber(spacingScale)) {
    const spacing = paraPr.spacing
    const newSpacing: ParaSpacingOptions = {}
    if (spacing.line != null) {
      const spc = spacing.line * spacingScale
      newSpacing.lineRule = spacing.lineRule
      if (isNumber(spc)) {
        if (spacing.lineRule === LineHeightRule.Auto) {
          newSpacing.line = spacing.line! - spacingScale
        } else {
          newSpacing.line = spc
        }
      }
      spacing.merge(newSpacing)
    }
  }

  if (isNumber(fontScale)) {
    const defaultTextPr = paraPr.defaultRunPr
    if (defaultTextPr?.fontSize != null) {
      defaultTextPr.fontSize *= fontScale
    }
  }
}

export function checkTextFitForTextListStyle(
  lstStyle: TextListStyle,
  fontScale?: number,
  spacingScale?: number
) {
  if (isNumber(fontScale) || isNumber(spacingScale)) {
    lstStyle.levels.forEach((lvl) => {
      if (lvl) {
        checkTextFitForParaPr(lvl, fontScale, spacingScale)
      }
    })
  }
}

export function getConnectionInfo(
  sp: SlideElement,
  index: string | Nullable<number>,
  group?: GroupShape
) {
  calcTransform(sp)
  if (index != null) {
    const connectionObject = getGeometryForSp(sp).cxnLst[index]
    if (connectionObject) {
      const connInfo = {
        idx: Number(index),
        ang: connectionObject.ang!,
        x: connectionObject.x,
        y: connectionObject.y,
      }
      let rot = normalizeRotValue(getFinalRotate(sp))
      let flipH = getFinalFlipH(sp)
      let flipV = getFinalFlipV(sp)
      let bounds = sp.bounds
      let transform = sp.transform

      if (group) {
        rot = normalizeRotValue(
          (sp.group ? getFinalRotate(sp.group) : 0) +
            rot -
            getFinalRotate(group)
        )
        if (getFinalFlipH(group)) {
          flipH = !flipH
        }
        if (getFinalFlipV(group)) {
          flipV = !flipV
        }
        bounds = bounds.clone()
        bounds.transform(group.invertTransform)
        transform = transform.clone()
        MatrixUtils.multiplyMatrixes(transform, group.invertTransform)
      }
      return convertToConnectorInfo(
        rot,
        flipH,
        flipV,
        transform,
        bounds,
        connInfo
      )
    }
  }
  return null
}

export function calculateGeometryForSp(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
    case InstanceType.ImageShape: {
      sp.calcedGeometry = null
      if (isDict(sp.spPr?.geometry)) {
        sp.calcedGeometry = sp.spPr!.geometry
      } else {
        const hsps = getHierarchySps(sp)
        for (let i = 0; i < hsps.length; ++i) {
          const ssp = hsps[i]
          const geometry = ssp?.spPr?.geometry
          if (geometry) {
            const geom = geometry.clone()
            geom.parent = sp.spPr
            sp.calcedGeometry = geom
            break
          }
        }
      }
      if (isInstanceTypeOf(sp.calcedGeometry, InstanceType.Geometry)) {
        if (sp.instanceType === InstanceType.ImageShape) {
          if (sp.isCalcStatusSet(CalcStatus.ImageShape.Transform)) {
            calcTransform(sp)
            sp.unsetCalcStatusOf(CalcStatus.ImageShape.Transform)
          }
        }
        const { extX, extY } = sp
        calculateGeometry(sp.calcedGeometry!, extX, extY)
      }
      break
    }
  }
}
