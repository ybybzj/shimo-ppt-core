import { isBool } from '../../../common/utils'
import { EditActionFlag } from '../../EditActionFlag'
import { SlideElement } from '../../SlideElement/type'
import { informEntityPropertyChange } from '../../calculation/informChanges/propertyChange'
import { ShapeLocks } from '../../SlideElement/const'
type LOCKS_MASKS_Value = (typeof ShapeLocks)[keyof typeof ShapeLocks]
export function setLocks(sp: SlideElement, valueOfLocks) {
  informEntityPropertyChange(
    sp,
    EditActionFlag.edit_Shapes_SetLocks,
    sp.locks,
    valueOfLocks
  )
  sp.locks = valueOfLocks
}

export function checkLock(sp: SlideElement, valueOfMask: LOCKS_MASKS_Value) {
  return !!(sp.locks & valueOfMask && sp.locks & (valueOfMask << 1))
}

export function setLock(
  sp: SlideElement,
  lock: LOCKS_MASKS_Value,
  v?: boolean
) {
  if (!isBool(v)) {
    setLocks(sp, ~lock & sp.locks)
  } else {
    setLocks(sp, sp.locks | lock | (v ? lock << 1 : 0))
  }
}
