import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { calculateForShape } from '../../calculation/calculate/shape'

import { ensureSpPrXfrm } from './attrs'
import { getTopMostGroupShape } from './getters'
import { CalcStatus } from '../../calculation/updateCalcStatus/calcStatus'
import { setTransformCalcStatus } from '../../calculation/updateCalcStatus/utils'
import { adjustXfrmOfGroup, normalizeGroupExts } from './group'

export function checkExtentsByTextContent(
  sp: SlideElement,
  forceAutoFit = false
): boolean {
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      if (forceAutoFit || sp.needAutofit()) {
        const topMostGroup = getTopMostGroupShape(sp)
        if (topMostGroup) {
          normalizeGroupExts(topMostGroup)
        }
        sp.setCalcStatusOf(CalcStatus.Shape.Geometry)
        sp.setCalcStatusOf(CalcStatus.Shape.Bounds)
        setTransformCalcStatus(sp)

        sp.setCalcStatusOf(CalcStatus.Shape.Content)
        sp.setCalcStatusOf(CalcStatus.Shape.PlaceholderContent)
        sp.setCalcStatusOf(CalcStatus.Shape.TextTransform)

        calculateForShape(sp, forceAutoFit)

        ensureSpPrXfrm(sp, true)
        sp.spPr!.xfrm!.setExtX(sp.extX + 0.001)
        sp.spPr!.xfrm!.setExtY(sp.extY + 0.001)
        sp.spPr!.xfrm!.setOffX(sp.x)
        sp.spPr!.xfrm!.setOffY(sp.y)

        if (topMostGroup) {
          adjustXfrmOfGroup(topMostGroup)
        }

        return true
      }
      break
    }
    case InstanceType.GroupShape: {
      let ret = false
      for (let i = 0; i < sp.spTree.length; ++i) {
        const ssp = sp.spTree[i]

        if (checkExtentsByTextContent(ssp)) {
          ret = true
          break
        }
      }
      return ret
    }
  }

  return false
}
