import { Dict, Nullable } from '../../../../liber/pervasive'
import { FillKIND } from '../../common/const/attrs'
import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { CalcStatus } from '../../calculation/updateCalcStatus/calcStatus'
import { ClrMap } from '../../color/clrMap'
import { CComplexColor } from '../../color/complexColor'
import { RecyclableArray } from '../../../common/managedArray'
import { ParaRun } from '../../Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../../Paragraph/Paragraph'
import { ParagraphItem } from '../../Paragraph/type'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { CGs, GradFill } from '../../SlideElement/attrs/fill/GradFill'
import { PatternFill } from '../../SlideElement/attrs/fill/PatternFill'
import { SolidFill } from '../../SlideElement/attrs/fill/SolidFill'
import { SpPr } from '../../SlideElement/attrs/shapePrs'
import { Theme } from '../../SlideElement/attrs/theme'
import { GraphicFrame } from '../../SlideElement/GraphicFrame'
import { Shape } from '../../SlideElement/Shape'
import { TableCellPr, TableCellPrOptions } from '../../Table/attrs/TableCellPr'
import { TableBorders } from '../../Table/attrs/TablePr'
import { ParaPr } from '../../textAttributes/ParaPr'
import { TextPr } from '../../textAttributes/TextPr'
import { isLineShape, isPlaceholder } from './asserts'
import { ensureSpPrXfrm } from './attrs'
import { cloneGroupShape, cloneSlideElement } from './clone'
import { Ln } from '../../SlideElement/attrs/line'
import { BodyPr } from '../../SlideElement/attrs/bodyPr'
import { TextListStyle } from '../../textAttributes/TextListStyle'
import { ShapeStyle } from '../../SlideElement/attrs/ShapeStyle'
// import { getFirstRunElementInfo } from '../../../re/export/textContent'
import { createTextBodyWithText } from '../../../copyPaste/utils'
import { createTextDocumentFromString } from '../tableOrTextDocContent/creators'
import { calculateForRendering } from '../../calculation/calculate/calculateForRendering'
import { ColorModifiers } from '../../color/ColorModifiers'
import { isNumber } from '../../../common/utils'
function cloneCColorForFormatting(
  ccolor: Nullable<CComplexColor>
): CComplexColor | undefined {
  if (ccolor == null) return undefined
  const rgb = ccolor.rgba
  const dup = CComplexColor.RGBA(rgb.r, rgb.g, rgb.b)
  const alpha = ccolor.mods?.getAlpha()
  if (alpha != null) {
    dup.mods = new ColorModifiers()
    dup.mods.updateAlpha(alpha)
  }

  return dup
}

function cloneFillEffectForFormatting(fillEffects: FillEffects): FillEffects {
  const dup = new FillEffects()
  const srcFill = fillEffects.fill
  if (srcFill) {
    switch (srcFill.type) {
      case FillKIND.BLIP: {
        dup.fill = srcFill.clone()
        break
      }
      case FillKIND.SOLID: {
        const fill = new SolidFill()
        if (srcFill.color) {
          fill.color = cloneCColorForFormatting(srcFill.color)
        }
        dup.fill = fill
        break
      }
      case FillKIND.PATT: {
        const fill = new PatternFill()
        if (srcFill.fgClr) {
          fill.fgClr = cloneCColorForFormatting(srcFill.fgClr)
        }
        if (srcFill.bgClr) {
          fill.bgClr = cloneCColorForFormatting(srcFill.bgClr)
        }
        fill.patternType = srcFill.patternType
        dup.fill = fill
        break
      }
      case FillKIND.GRAD: {
        const fill = new GradFill()
        if (srcFill.ln) {
          fill.ln = srcFill.ln.clone()
        }
        if (srcFill.path) {
          fill.path = srcFill.path.clone()
        }
        for (let i = 0; i < srcFill.gsList.length; ++i) {
          const gs = srcFill.gsList[i]
          fill.gsList.push(new CGs(gs.pos, cloneCColorForFormatting(gs.color)))
        }
        dup.fill = fill
        break
      }
      case FillKIND.NOFILL:
      case FillKIND.GRP: {
        dup.fill = srcFill
        break
      }
    }
  }
  dup.transparent = fillEffects.transparent
  return dup
}

function updateFormattingForTextPr(
  textPr: TextPr,
  theme: Theme,
  colorMap: ClrMap
) {
  const fontScheme = theme.themeElements.fontScheme
  if (textPr.rFonts) {
    if (textPr.rFonts.ascii) {
      textPr.rFonts.ascii.name = fontScheme.checkFont(textPr.rFonts.ascii.name)
    }
    if (textPr.rFonts.eastAsia) {
      textPr.rFonts.eastAsia.name = fontScheme.checkFont(
        textPr.rFonts.eastAsia.name
      )
    }
    if (textPr.rFonts.hAnsi) {
      textPr.rFonts.hAnsi.name = fontScheme.checkFont(textPr.rFonts.hAnsi.name)
    }
    if (textPr.rFonts.cs) {
      textPr.rFonts.cs.name = fontScheme.checkFont(textPr.rFonts.cs.name)
    }
  }
  if (textPr.fillEffects) {
    textPr.fillEffects.check(theme, colorMap)
    textPr.fillEffects = cloneFillEffectForFormatting(textPr.fillEffects)
  }
  return textPr
}

function saveFormattingForParaItems(
  sourceItems: RecyclableArray<ParagraphItem> | RecyclableArray<ParaRun>,
  copyItems: RecyclableArray<ParagraphItem> | RecyclableArray<ParaRun>,
  theme,
  colorMap,
  pr: ParaPr
) {
  const isMergeRunPr = copyItems === sourceItems
  for (let i = 0; i < copyItems.length; ++i) {
    const sourceItem = sourceItems.at(i)
    const copyItem = copyItems.at(i)!
    if (copyItem.instanceType === InstanceType.ParaRun && copyItem.pr) {
      if (isMergeRunPr) {
        const defRunPr = pr.defaultRunPr!.clone()
        defRunPr.merge(
          updateFormattingForTextPr(copyItem.pr.clone(), theme, colorMap)
        )
        copyItem.fromTextPr(defRunPr)
      } else {
        copyItem.fromTextPr(
          updateFormattingForTextPr(copyItem.pr.clone(), theme, colorMap)
        )
      }
    } else if (
      sourceItem &&
      sourceItem.instanceType === InstanceType.ParaHyperlink &&
      copyItem.instanceType === InstanceType.ParaHyperlink
    ) {
      saveFormattingForParaItems(
        sourceItem.children,
        copyItem.children,
        theme,
        colorMap,
        pr
      )
    }
  }
}

export function saveFormattingForParagraphs(
  arrOfSourceContent: readonly Paragraph[],
  arrOfCopyContent: readonly Paragraph[],
  theme,
  colorMap
) {
  if (arrOfCopyContent.length === arrOfSourceContent.length) {
    let para: Paragraph
    for (let i = 0; i < arrOfSourceContent.length; ++i) {
      para = arrOfSourceContent[i]
      if (para.calcedPrs.pr) {
        const pr = para.calcedPrs.pr.paraPr.clone()
        pr.defaultRunPr = updateFormattingForTextPr(
          para.calcedPrs.pr.textPr.clone(),
          theme,
          colorMap
        )
        arrOfCopyContent[i].setPr(pr)
        saveFormattingForParaItems(
          para.children,
          arrOfCopyContent[i].children,
          theme,
          colorMap,
          pr
        )
      } else {
        if (
          arrOfCopyContent[i].pr &&
          arrOfCopyContent[i].pr.defaultRunPr &&
          arrOfCopyContent[i].pr.defaultRunPr!.fillEffects
        ) {
          const pr = arrOfCopyContent[i].pr.clone()
          pr.defaultRunPr!.fillEffects!.check(theme, colorMap)
          pr.defaultRunPr!.fillEffects = cloneFillEffectForFormatting(
            pr.defaultRunPr!.fillEffects!
          )
          pr.defaultRunPr = updateFormattingForTextPr(
            pr.defaultRunPr!,
            theme,
            colorMap
          )
          arrOfCopyContent[i].setPr(pr)
        }
      }
    }
  }
}

export function cloneSpWithFormatting<T extends SlideElement>(
  sp: T,
  map?: Dict
): T {
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      const dup = cloneSlideElement<Shape>(sp)
      if (sp.pen || sp.brush) {
        if (!dup.spPr) {
          dup.setSpPr(new SpPr())
          dup.spPr!.setParent(dup)
        }
        if (sp.brush) {
          dup.spPr!.setFill(cloneFillEffectForFormatting(sp.brush))
        }
        if (sp.pen) {
          const dupLn = sp.pen.clone({ cloneFill: false })
          if (sp.pen.fillEffects) {
            dupLn.fillEffects = cloneFillEffectForFormatting(sp.pen.fillEffects)
          }
          dup.spPr!.setLn(dupLn)
        }
      }
      if (dup.txBody && dup.txBody.textDoc) {
        const theme = sp.getTheme()
        const colorMap = sp.getColorMap()
        if (sp.txBody && sp.txBody.textDoc) {
          saveFormattingForParagraphs(
            sp.txBody.textDoc.children.elements,
            dup.txBody.textDoc.children.elements,
            theme,
            colorMap
          )
        }
      }
      if (
        isPlaceholder(dup) &&
        !sp.isCalcStatusSet(CalcStatus.Shape.Transform)
      ) {
        const xfrm = dup.spPr!.xfrm
        if (!xfrm || !xfrm.isValid()) {
          dup.x = sp.x
          dup.y = sp.y
          dup.extX = sp.extX
          dup.extY = sp.extY
          ensureSpPrXfrm(dup, true)
        }
      }
      return dup as T
    }

    case InstanceType.GroupShape:
      return cloneGroupShape(sp, cloneSpWithFormatting, map) as T
    case InstanceType.ImageShape:
      return cloneSlideElement(sp, map)
    case InstanceType.GraphicFrame: {
      const ret = cloneSlideElement<GraphicFrame>(sp)
      const retTable = ret.graphicObject
      const sourceTable = sp.graphicObject
      const theme = sp.getTheme()
      const colorMap = sp.getColorMap()

      if (retTable && sourceTable) {
        let pr2 = retTable.pr
        if (sourceTable.calcedPrs.pr && sourceTable.calcedPrs.pr.tablePr) {
          retTable.setPr(sourceTable.calcedPrs.pr.tablePr.clone())
          pr2 = retTable.pr
          updateFillEffectsOfTblBordersForFormatting(
            pr2.tableBorders,
            theme,
            colorMap
          )
        }
        if (retTable.children.length === sourceTable.children.length) {
          for (let i = 0, l = retTable.children.length; i < l; ++i) {
            const arrOfSourceCells = sourceTable.children.at(i)!.children
            const arrOfCopyCells = retTable.children.at(i)!.children

            if (arrOfSourceCells.length === arrOfCopyCells.length) {
              for (let j = 0; j < arrOfSourceCells.length; ++j) {
                const sourceRow = arrOfSourceCells.at(j)!
                const copyRow = arrOfCopyCells.at(j)!

                const arrOfSourceContent = sourceRow.textDoc.children
                const arrOfCopyContent = copyRow.textDoc.children

                saveFormattingForParagraphs(
                  arrOfSourceContent.elements,
                  arrOfCopyContent.elements,
                  theme,
                  colorMap
                )

                if (sourceRow.calcedPrs.pr) {
                  const cellPropts: TableCellPrOptions = {}
                  if (pr2.tableBorders) {
                    cellPropts.tableCellBorders = {}
                    if (i === 0) {
                      if (pr2.tableBorders.top) {
                        cellPropts.tableCellBorders.top = pr2.tableBorders.top
                      }
                    }
                    if (i === retTable.lastRow) {
                      if (pr2.tableBorders.bottom) {
                        cellPropts.tableCellBorders.bottom =
                          pr2.tableBorders.bottom
                      }
                    }
                    if (pr2.tableBorders.insideH) {
                      if (i !== 0) {
                        cellPropts.tableCellBorders.top =
                          pr2.tableBorders.insideH
                      }
                      if (i !== retTable.lastRow) {
                        cellPropts.tableCellBorders.bottom =
                          pr2.tableBorders.insideH
                      }
                    }
                    if (j === 0) {
                      if (pr2.tableBorders.left) {
                        cellPropts.tableCellBorders.left = pr2.tableBorders.left
                      }
                    }
                    if (j === arrOfSourceCells.length - 1) {
                      if (pr2.tableBorders.right) {
                        cellPropts.tableCellBorders.right =
                          pr2.tableBorders.right
                      }
                    }
                    if (pr2.tableBorders.insideV) {
                      if (j !== 0) {
                        cellPropts.tableCellBorders.left =
                          pr2.tableBorders.insideV
                      }
                      if (j !== arrOfSourceCells.length - 1) {
                        cellPropts.tableCellBorders.right =
                          pr2.tableBorders.insideV
                      }
                    }
                  }
                  const cellPr = TableCellPr.new(cellPropts)
                  cellPr.merge(arrOfSourceCells.at(j)!.calcedPrs.pr!)
                  copyRow.setPr(cellPr)
                  const pr = copyRow.pr

                  if (pr.shd && pr.shd.fillEffects) {
                    const fill = pr.shd.fillEffects?.fill
                    if (fill && fill.type !== FillKIND.NOFILL) {
                      updateFillEffectInObjectForFormatting(
                        pr.shd,
                        theme,
                        colorMap
                      )
                    }
                  }

                  updateFillEffectsOfTblBordersForFormatting(
                    pr.tableCellBorders,
                    theme,
                    colorMap,
                    false
                  )
                }
              }
            }
          }
        }
      }
      return ret as T
    }
  }
}

function updateFillEffectInObjectForFormatting(
  withFillEffect: Nullable<{ fillEffects?: Nullable<FillEffects> }>,
  theme: Nullable<Theme>,
  colorMap: ClrMap
) {
  const fillEffects = withFillEffect && withFillEffect.fillEffects
  if (fillEffects) {
    fillEffects.check(theme, colorMap)
    const rgba = fillEffects.getRGBAColor()
    const newFillEffects = FillEffects.SolidRGBA(rgba.r, rgba.g, rgba.b)
    const transparent = rgba.a
    if (transparent < 255) {
      newFillEffects.transparent = transparent
    }
    withFillEffect.fillEffects = newFillEffects
  }
}

function updateFillEffectsOfTblBordersForFormatting(
  tableBorders: Nullable<TableBorders>,
  theme: Nullable<Theme>,
  colorMap: ClrMap,
  isTableBorders = true
) {
  if (tableBorders) {
    updateFillEffectInObjectForFormatting(tableBorders.bottom, theme, colorMap)
    updateFillEffectInObjectForFormatting(tableBorders.left, theme, colorMap)
    updateFillEffectInObjectForFormatting(tableBorders.right, theme, colorMap)
    updateFillEffectInObjectForFormatting(tableBorders.top, theme, colorMap)
    if (isTableBorders === true) {
      updateFillEffectInObjectForFormatting(
        tableBorders.insideH,
        theme,
        colorMap
      )
      updateFillEffectInObjectForFormatting(
        tableBorders.insideV,
        theme,
        colorMap
      )
    }
  }
}

export interface SlideElementFormat {
  brush: FillEffects | null
  pen: Ln | null
  style: ShapeStyle | null
  attrUseBgFill?: boolean
  bodyPr?: BodyPr | null
  lstStyle?: TextListStyle | null
  paraPr?: ParaPr | null
  textPr?: TextPr | null
  transparent?: number | null
}

export function getSlideElementFormat(
  sp: SlideElement
): Nullable<SlideElementFormat> {
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      const ret: SlideElementFormat = {
        style: null,
        brush: null,
        pen: null,
      }

      if (sp.style) {
        ret.style = sp.style
      }

      ret.attrUseBgFill = !!sp.attrUseBgFill

      if (sp.brush) {
        ret.brush = cloneFillEffectForFormatting(sp.brush)
      }

      if (sp.pen) {
        const pen = sp.pen.clone({ cloneFill: false })
        if (sp.pen.fillEffects) {
          pen.fillEffects = cloneFillEffectForFormatting(sp.pen.fillEffects)
        }
        ret.pen = pen
      }

      const txBody = sp.txBody
      if (txBody) {
        const bodyPr = txBody.calcedBodyPr ?? txBody.bodyPr
        if (bodyPr) {
          ret.bodyPr = bodyPr.clone()
        } else {
          ret.bodyPr = null
        }

        const lstStyle = txBody.lstStyle
        if (lstStyle) {
          ret.lstStyle = lstStyle.clone()
        } else {
          ret.lstStyle = null
        }

        const textDoc = txBody.textDoc
        if (textDoc) {
          const oldIsSetToAll = textDoc.isSetToAll
          textDoc.isSetToAll = true
          const paraPr = textDoc.getFirstParaPr()
          const textPr = textDoc.getFirstParaTextPr()
          textDoc.isSetToAll = oldIsSetToAll

          ret.paraPr = paraPr ?? null
          ret.textPr = textPr ?? null
        }
      }

      return ret
    }
    case InstanceType.ImageShape: {
      const ret: SlideElementFormat = {
        style: null,
        brush: null,
        pen: null,
      }

      if (sp.style) {
        ret.style = sp.style
      }

      if (sp.brush) {
        ret.brush = cloneFillEffectForFormatting(sp.brush)
      }

      if (sp.pen) {
        const pen = sp.pen.clone({ cloneFill: false })
        if (sp.pen.fillEffects) {
          pen.fillEffects = cloneFillEffectForFormatting(sp.pen.fillEffects)
        }

        if (isNumber(pen.w)) {
          pen.w /= 2
        }

        ret.pen = pen
      }

      if (sp.blipFill) {
        const transparent = sp.blipFill.getTransparent()
        if (transparent != null) {
          ret.transparent = transparent
        } else {
          ret.transparent = null
        }
      }

      return ret
    }
  }
}

export function applySlideElementFormat(
  sp: SlideElement,
  format: SlideElementFormat
) {
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      const {
        brush,
        pen,
        style,
        attrUseBgFill,
        bodyPr,
        lstStyle,
        textPr,
        paraPr,
      } = format
      if (!sp.spPr) {
        sp.setSpPr(new SpPr())
        sp.spPr!.setParent(sp)
      }
      const spPr = sp.spPr!

      spPr.setFill(brush ?? undefined)

      if (pen) {
        if (!isLineShape(sp)) {
          pen.headEnd = undefined
          pen.tailEnd = undefined
        }
        spPr.setLn(pen)
      } else if (pen === null) {
        spPr.setLn(undefined)
      }

      if (attrUseBgFill !== undefined) {
        sp.attrUseBgFill = attrUseBgFill
      }

      if (style !== undefined) {
        sp.setStyle(style)
      }

      if (
        bodyPr !== undefined ||
        lstStyle !== undefined ||
        textPr !== undefined ||
        paraPr !== undefined
      ) {
        if (sp.txBody == null) {
          sp.setTxBody(createTextBodyWithText('', sp, bodyPr ?? undefined))
        } else if (bodyPr) {
          sp.txBody.setBodyPr(bodyPr)
        }

        const txBody = sp.txBody!

        if (lstStyle !== undefined) {
          txBody.setTextListStyle(lstStyle ?? undefined)
        }

        if (textPr !== undefined || paraPr !== undefined) {
          if (txBody.textDoc == null) {
            txBody.setContent(createTextDocumentFromString('', txBody))
          }
          const textDoc = txBody.textDoc!

          for (let i = 0, l = textDoc.children.length; i < l; i++) {
            const item = textDoc.children.at(i)!
            item.isSetToAll = true
            item.pasteFormatting(textPr, paraPr)
            item.isSetToAll = false
          }
        }
      }
      calculateForRendering()
      break
    }
    case InstanceType.ImageShape: {
      const { brush, pen, style, transparent } = format
      if (!sp.spPr) {
        sp.setSpPr(new SpPr())
        sp.spPr!.setParent(sp)
      }
      const spPr = sp.spPr!
      if (brush !== undefined) {
        spPr.setFill(brush ?? undefined)
      }

      if (pen) {
        pen.headEnd = undefined
        pen.tailEnd = undefined
        spPr.setLn(pen)
      } else if (pen === null) {
        spPr.setLn(undefined)
      }

      if (style !== undefined) {
        sp.setStyle(style ?? undefined)
      }

      if (transparent !== undefined && sp.blipFill) {
        sp.blipFill.updateTransparent(transparent)
      }
      calculateForRendering()
      break
    }
    case InstanceType.GroupShape: {
      const ssps = sp.spTree
      for (const ssp of ssps) {
        applySlideElementFormat(ssp, format)
      }
      break
    }
  }
}
