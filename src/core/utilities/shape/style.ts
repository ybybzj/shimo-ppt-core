import { Nullable } from '../../../../liber/pervasive'
import { LineHeightRule } from '../../common/const/attrs'
import { isDict } from '../../../common/utils'
import { InstanceType } from '../../instanceTypes'
import { Notes } from '../../Slide/Notes'
import { NotesMaster } from '../../Slide/NotesMaster'
import { Presentation } from '../../Slide/Presentation'
import { Slide } from '../../Slide/Slide'
import { SlideLayout } from '../../Slide/SlideLayout'
import { SlideMaster } from '../../Slide/SlideMaster'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { FontStyleInd } from '../../SlideElement/const'
import { GraphicFrame } from '../../SlideElement/GraphicFrame'
import { Shape } from '../../SlideElement/Shape'
import {
  ContentStyle,
  contentStyleCollector,
  TextContentStylePrs,
} from '../../textAttributes/ContentStyle'
import { ParaPrOptions } from '../../textAttributes/ParaPr'
import { TextPrOptions } from '../../textAttributes/TextPr'
import { getTextStyleByMaster } from '../master'
import { isPlaceholder } from './asserts'
import {
  getParentsOfElement,
  getHierarchySps,
  getCalcedStyleForSp,
} from './getters'

export type TextStylesInfo = {
  style: TextContentStylePrs
  shape?: Shape | GraphicFrame
  slide?: Nullable<Slide>
  layout?: Nullable<SlideLayout>
  master?: Nullable<SlideMaster | NotesMaster>
  presentation?: Nullable<Presentation>
  notes?: Nullable<Notes>
}

const defaultParaPrOpts: ParaPrOptions = {
  spacing: { lineRule: LineHeightRule.Auto, line: 1, before: 0, after: 0 },
  defaultTab: 25.4,
}

const defaultTextPrOpts: TextPrOptions = {
  rFonts: {
    ascii: { name: '+mn-lt', index: -1 },
    eastAsia: { name: '+mn-ea', index: -1 },
    cs: { name: '+mn-cs', index: -1 },
    hAnsi: { name: '+mn-lt', index: -1 },
  },
}

export function getTextStylesForSp(
  sp: Shape | GraphicFrame,
  level: number
): TextStylesInfo {
  const { theme, presentation, master, slide, layout, notes } =
    getParentsOfElement(sp)
  const defaultParaStyle = ContentStyle.new()
  defaultParaStyle.paraPr.fromOptions(defaultParaPrOpts)

  if (theme) {
    defaultParaStyle.textPr.fromOptions(defaultTextPrOpts)
  }
  if (sp.instanceType === InstanceType.GraphicFrame) {
    defaultParaStyle.textPr.setFontSize(18)
  }
  if (presentation && presentation.defaultTextStyle) {
    defaultParaStyle.fromParaPrOptions(presentation.defaultTextStyle.levels[9])

    if (!isDict(master) || !isDict(master.txStyles) || !isPlaceholder(sp)) {
      defaultParaStyle.fromParaPrOptions(
        presentation.defaultTextStyle.levels[level]
      )
    }
  }

  let masterStyle: ContentStyle | undefined
  if (isDict(master) && isDict(master.txStyles)) {
    const masterTextStyles = getTextStyleByMaster(master, sp)
    if (
      isDict(masterTextStyles) &&
      isDict(masterTextStyles.levels) &&
      isDict(masterTextStyles.levels[level])
    ) {
      masterStyle = ContentStyle.new()
      const masterTextStyle = masterTextStyles.levels[level]!
      masterStyle.fromParaPrOptions(masterTextStyle)
    }
  }

  const hsps = getHierarchySps(sp, false)
  const hStyles: ContentStyle[] = []
  for (let i = 0, l = hsps.length; i < l; ++i) {
    const hsp = hsps[i]
    if (
      hsp?.instanceType === InstanceType.Shape &&
      hsp?.txBody?.lstStyle?.levels[level]
    ) {
      const hParaPr = hsp.txBody.lstStyle.levels[level]!
      const hStyle = ContentStyle.new()
      hStyle.fromParaPrOptions(hParaPr)
      hStyles.push(hStyle)
    }
  }

  let spStyle: ContentStyle | undefined
  if (
    sp.instanceType === InstanceType.Shape &&
    sp.txBody &&
    sp.txBody.lstStyle &&
    sp.txBody.lstStyle.levels[level]
  ) {
    spStyle = ContentStyle.new()
    spStyle.fromParaPrOptions(sp.txBody.lstStyle.levels[level]!)
  }

  let spTextStyle: ContentStyle | undefined
  const { style: calcedStyle } = getCalcedStyleForSp(sp)
  if (isDict(calcedStyle) && isDict(calcedStyle.fontRef)) {
    spTextStyle = ContentStyle.new()
    let fontName
    if (calcedStyle.fontRef.idx === FontStyleInd.major) {
      fontName = '+mj-'
    } else fontName = '+mn-'

    spTextStyle.textPr.fromOptions({
      rFonts: {
        ascii: {
          name: fontName + 'lt',
          index: -1,
        },
        eastAsia: {
          name: fontName + 'ea',
          index: -1,
        },
        cs: {
          name: fontName + 'cs',
          index: -1,
        },
        hAnsi: {
          name: fontName + 'lt',
          index: -1,
        },
      },
    })

    if (
      calcedStyle.fontRef.color != null &&
      calcedStyle.fontRef.color.color != null
    ) {
      const fillEffects = FillEffects.SolidCColor(calcedStyle.fontRef.color)

      spTextStyle.textPr.setFillEffects(fillEffects)
    }
  }

  if (isPlaceholder(sp) || sp.instanceType === InstanceType.GraphicFrame) {
    contentStyleCollector.add(defaultParaStyle)
    contentStyleCollector.add(masterStyle)
  } else {
    contentStyleCollector.add(masterStyle)
    contentStyleCollector.add(defaultParaStyle)
  }

  for (let i = hStyles.length - 1; i > -1; --i) {
    if (hStyles[i]) {
      contentStyleCollector.add(hStyles[i])
    }
  }

  contentStyleCollector.add(spTextStyle)

  contentStyleCollector.add(spStyle)

  const resultStyle = contentStyleCollector.collect() // will recycle contentStyles during collecting
  return {
    style: resultStyle,
    shape: sp,
    slide: slide,
    layout: layout,
    master: master,
    presentation: presentation,
    notes: notes,
  }
}
