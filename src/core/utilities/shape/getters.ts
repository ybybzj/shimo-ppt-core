import { findIndex } from '../../../../liber/l/findIndex'
import { Dict, Nullable } from '../../../../liber/pervasive'
import { FillKIND } from '../../common/const/attrs'
import { ManagedSliceUtil } from '../../../common/managedArray'
import {
  checkImageSrc,
  isBool,
  isDict,
  isNumber,
  toInt,
} from '../../../common/utils'
import { UploadUrlResult } from '../../../editor/type'
import { PresetShapeTypes } from '../../../exports/graphic'
import { InstanceType, isInstanceTypeOf } from '../../instanceTypes'
import { PhTypeOO } from '../../../io/dataType/spAttrs'
import { SlideElement } from '../../SlideElement/type'
import { CalcStatus } from '../../calculation/updateCalcStatus/calcStatus'
import {
  isFillCalcStatusSet,
  isLineCalcStatusSet,
  isTransparentCalcStatusSet,
  unsetFillCalcStatus,
  unsetLineCalcStatus,
  unsetTransparentCalcStatus,
} from '../../calculation/updateCalcStatus/utils'
import { ScaleOfPPTXSizes } from '../../common/const/unit'
import { TableSelectionType, VMergeType } from '../../common/const/table'
import {
  checkAlmostEqual,
  isRotateIn45D,
  normalizeRotValue,
} from '../../common/utils'
import { GraphicBounds } from '../../graphic/Bounds'
import { Matrix2D, MatrixUtils } from '../../graphic/Matrix'
import { Notes } from '../../Slide/Notes'
import { NotesMaster } from '../../Slide/NotesMaster'
import { Presentation } from '../../Slide/Presentation'
import { Slide } from '../../Slide/Slide'
import { SlideLayout } from '../../Slide/SlideLayout'
import { SlideMaster } from '../../Slide/SlideMaster'
import { BodyPr } from '../../SlideElement/attrs/bodyPr'
import { BlipFill } from '../../SlideElement/attrs/fill/blipFill'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { GradFill } from '../../SlideElement/attrs/fill/GradFill'
import { SpPr, UniMedia } from '../../SlideElement/attrs/shapePrs'
import { Theme } from '../../SlideElement/attrs/theme'
import { ConnectionShape } from '../../SlideElement/ConnectionShape'
import {
  PlaceholderType,
  radFactor,
  TextOverflowType,
  TextVerticalType,
} from '../../SlideElement/const'
import { GraphicFrame } from '../../SlideElement/GraphicFrame'
import { GroupShape } from '../../SlideElement/GroupShape'
import { ImageShape } from '../../SlideElement/Image'
import { Shape } from '../../SlideElement/Shape'
import { SlideElementParent } from '../../Slide/type'
import { Table } from '../../Table/Table'
import { TextDocument } from '../../TextDocument/TextDocument'
import { findClosestParent, getPresentation } from '../finders'
import { isPlaceholder, isSlideElement } from './asserts'
import { ParaPr } from '../../textAttributes/ParaPr'
import { TextPr } from '../../textAttributes/TextPr'

export function getPlaceholderType(sp: SlideElement) {
  if (!isPlaceholder(sp)) return null
  switch (sp.instanceType) {
    case InstanceType.Shape:
      return sp?.nvSpPr?.nvPr?.ph?.type ?? null
    case InstanceType.ImageShape:
      return sp.nvPicPr!.nvPr!.ph!.type ?? null
    case InstanceType.GroupShape:
      return sp.nvGrpSpPr!.nvPr!.ph!.type ?? null
    case InstanceType.GraphicFrame:
      return sp.nvGraphicFramePr!.nvPr!.ph!.type ?? null
  }
}

export function getIndexOfPlaceholder(sp: SlideElement): Nullable<string> {
  if (!isPlaceholder(sp)) return null

  switch (sp.instanceType) {
    case InstanceType.Shape:
      return sp?.nvSpPr?.nvPr?.ph?.idx ?? null
    case InstanceType.ImageShape:
      return sp?.nvPicPr?.nvPr?.ph?.idx ?? null
    case InstanceType.GroupShape:
      return sp.nvGrpSpPr?.nvPr?.ph?.idx ?? null
    case InstanceType.GraphicFrame:
      return sp?.nvGraphicFramePr?.nvPr?.ph?.idx ?? null
  }
}

export function isSpHasSingleBodyPh(sp: SlideElement): boolean {
  if (!isPlaceholder(sp)) return false
  const phType = getPlaceholderType(sp)
  if (phType !== PlaceholderType.body && phType != null) return false
  if (sp.parent && sp.parent.cSld && Array.isArray(sp.parent.cSld.spTree)) {
    const spTree = sp.parent.cSld.spTree
    for (let i = 0; i < spTree.length; ++i) {
      if (
        spTree[i] !== sp &&
        getPlaceholderType(spTree[i]) === PlaceholderType.body
      ) {
        return false
      }
    }
  }
  return true
}
function adjustPhType(type: Nullable<number>) {
  let result: number
  if (type == null) {
    result = PlaceholderType.body
  } else {
    if (type === PlaceholderType.ctrTitle) {
      result = PlaceholderType.title
    } else {
      result = type
    }
  }
  return result
}

function adjustPhIndex(index: Nullable<string>) {
  return index == null ? 0 : index
}

function _getSpTreeForMatch(
  shapeContainer: Slide | SlideLayout | SlideMaster,
  matchedShapes?: Dict<true>
) {
  let spTree = shapeContainer.cSld.spTree
  if (matchedShapes != null) {
    spTree = spTree.filter((sp) => !matchedShapes[sp.id])
  }

  return spTree
}

export function getMatchedSlideElementForPh(
  shapeContainer: Nullable<Slide | SlideLayout | SlideMaster>,
  phType: Nullable<number>,
  phIdx: Nullable<string>,
  isSingleBody?: boolean,
  info?,
  matchedShapes?: Dict<true>,
  isTryOnlyMatchType?: boolean
) {
  if (!shapeContainer) return null
  const inputAdjustedType = adjustPhType(phType)

  const inputAdjustedIndex = adjustPhIndex(phIdx)

  const spTree = _getSpTreeForMatch(shapeContainer, matchedShapes)
  let numOfBodyPH = 0
  let lastSpOfBodyPH: Nullable<SlideElement>,
    lastType: Nullable<PhTypeOO>,
    lastIndex: Nullable<string>
  let matchTypeAndIndex: Nullable<SlideElement>,
    matchSpecialType: Nullable<SlideElement>,
    matchIndex: Nullable<SlideElement>,
    matchType: Nullable<SlideElement>
  for (let shapeIndex = 0; shapeIndex < spTree.length; ++shapeIndex) {
    const slideElement = spTree[shapeIndex]
    if (!isPlaceholder(slideElement)) {
      continue
    }

    let index: Nullable<string>, type: Nullable<PhTypeOO>
    if (slideElement.instanceType === InstanceType.Shape) {
      index = slideElement.nvSpPr?.nvPr.ph?.idx
      type = slideElement.nvSpPr?.nvPr.ph?.type
    }
    if (slideElement.instanceType === InstanceType.ImageShape) {
      index = slideElement.nvPicPr?.nvPr.ph?.idx
      type = slideElement.nvPicPr?.nvPr.ph?.type
    }
    if (slideElement.instanceType === InstanceType.GroupShape) {
      index = slideElement.nvGrpSpPr?.nvPr.ph?.idx
      type = slideElement.nvGrpSpPr?.nvPr.ph?.type
    }
    const finalPhType = adjustPhType(type)

    const finalPhIndex = adjustPhIndex(index)

    if (
      (inputAdjustedType === finalPhType &&
        inputAdjustedIndex === finalPhIndex) ||
      (inputAdjustedType === PlaceholderType.title &&
        inputAdjustedType === finalPhType)
    ) {
      if (info) {
        info.isBadMatch = !(type === phType && index === phIdx)
      }
      matchTypeAndIndex = slideElement
      break
    } else if (
      inputAdjustedType === PlaceholderType.sldNum ||
      inputAdjustedType === PlaceholderType.dt ||
      inputAdjustedType === PlaceholderType.ftr ||
      inputAdjustedType === PlaceholderType.hdr
    ) {
      if (inputAdjustedType === type) {
        if (info) {
          info.isBadMatch = !(type === phType && index === phIdx)
        }
        if (matchSpecialType == null) {
          matchSpecialType = slideElement
        }
      }
    }

    if (info == null) {
      if (inputAdjustedType === type && inputAdjustedIndex === finalPhIndex) {
        if (info) {
          info.isBadMatch = true
        }
        if (matchIndex == null) {
          matchIndex = slideElement
        }
      }

      if (isTryOnlyMatchType) {
        if (inputAdjustedType === finalPhType) {
          if (info) {
            info.isBadMatch = true
          }
          if (matchType == null) {
            matchType = slideElement
          }
        }
      }
    }
    if (PlaceholderType.body === type) {
      ++numOfBodyPH
      lastSpOfBodyPH = slideElement
      lastType = type
      lastIndex = index
    }
  }

  if (matchTypeAndIndex != null) {
    return matchTypeAndIndex
  }
  if (matchSpecialType != null) {
    return matchSpecialType
  }

  if (info) {
    return null
  }
  if (
    numOfBodyPH === 1 &&
    inputAdjustedType === PlaceholderType.body &&
    isSingleBody
  ) {
    if (info) {
      info.isBadMatch = !(lastType === phType && lastIndex === phIdx)
    }
    return lastSpOfBodyPH
  }

  if (matchIndex != null) {
    return matchIndex
  }

  if (matchType != null) {
    return matchType
  }

  if (numOfBodyPH === 1 && isSingleBody) {
    if (info) {
      info.isBadMatch = !(lastType === phType && lastIndex === phIdx)
    }
    return lastSpOfBodyPH
  }

  return null
}

export function getHierarchySps(
  sp: SlideElement,
  isSingleBody?: boolean,
  info?
) {
  const calcedHSps: Array<Nullable<SlideElement>> = []
  if (sp.parent) {
    if (isPlaceholder(sp)) {
      const typeOfPH = getPlaceholderType(sp)
      const indexOfPH = getIndexOfPlaceholder(sp)
      if (!isBool(isSingleBody)) {
        isSingleBody = isSpHasSingleBodyPh(sp)
      }
      switch (sp.parent.instanceType) {
        case InstanceType.Slide: {
          const parentSlide = sp.parent as Slide
          calcedHSps.push(
            getMatchedSlideElementForPh(
              parentSlide.layout!,
              typeOfPH,
              indexOfPH,
              isSingleBody,
              info
            )
          )
          calcedHSps.push(
            getMatchedSlideElementForPh(
              parentSlide.layout!.master,
              typeOfPH,
              indexOfPH,
              true
            )
          )
          break
        }

        case InstanceType.SlideLayout: {
          const parentLayout = sp.parent as SlideLayout
          calcedHSps.push(
            getMatchedSlideElementForPh(
              parentLayout.master,
              typeOfPH,
              indexOfPH,
              true
            )
          )
          break
        }
      }
    }
  }

  return calcedHSps
}

export function getTextDocumentOfSp(sp: SlideElement, x?: number, y?: number) {
  if (sp == null) {
    return null
  }
  switch (sp.instanceType) {
    case InstanceType.GroupShape:
    case InstanceType.ImageShape:
      return null
    case InstanceType.Shape: {
      if (sp.txBody) {
        return sp.txBody.textDoc
      }
      return null
    }
    case InstanceType.GraphicFrame: {
      if (
        sp.graphicObject &&
        (false === sp.graphicObject.selection.isUse ||
          (true === sp.graphicObject.selection.isUse &&
            TableSelectionType.TEXT === sp.graphicObject.selection.type))
      ) {
        const table = sp.graphicObject as Table
        if (sp.invertTextTransform && x != null && y != null) {
          const tx = sp.invertTextTransform.XFromPoint(x, y)
          const ty = sp.invertTextTransform.YFromPoint(x, y)
          const pos = table.getCellByPoint(tx, ty)
          if (pos) {
            const { row: rIndex, cell: cIndex } = pos
            const row = table.children.at(rIndex)
            const cell = row?.children.at(cIndex)
            return cell?.textDoc
          }
        } else if (table.curCell) {
          return table.curCell.textDoc
        }
      }
      return null
    }
  }
}

export function getDocPrsOfSlideElements(slideElements: SlideElement[]): { paraPrs: ParaPr[], textPrs: TextPr[] } {
  const paraPrs: ParaPr[] = []
  const textPrs: TextPr[] = []

  function handleDocPrs(slideEls: SlideElement[]) {
    if (!slideEls?.length) return
    slideEls.forEach((object) => {
      if (object.instanceType === InstanceType.GroupShape) {
        handleDocPrs(object.slideElementsInGroup)
      } else {
        const textDoc = getTextDocumentOfSp(object)
        if (textDoc) {
          textDoc.selectAll()
          const paraPr = textDoc.getCalcedParaPr()
          const textPr = textDoc.getCalcedTextPr()!
          textDoc.removeSelection()
          if (paraPr) {
            paraPrs.push(paraPr)
          }
          if (textPr) {
            textPrs.push(textPr)
          }
        }

      }
    })
  }
  handleDocPrs(slideElements)
  
  return {
    paraPrs,
    textPrs
  }
}

export function getShapeContentText(sp: SlideElement) {
  if (sp == null) {
    return ''
  }
  switch (sp.instanceType) {
    case InstanceType.GroupShape:
    case InstanceType.ImageShape:
      return ''
    case InstanceType.Shape:
    case InstanceType.GraphicFrame: {
      const textDoc = getTextDocumentOfSp(sp)
      let result = ''
      if (textDoc) {
        const len = textDoc.children.length
        if (len > 0) {
          ManagedSliceUtil.forEach(textDoc.children, (paragraph, index) => {
            if (paragraph?.instanceType === InstanceType.Paragraph) {
              const text = paragraph.getText()
              if (typeof text === 'string') {
                result += text
              }
            }
            if (len > 1 && index !== len - 1) {
              result += '\n'
            }
          })
        }
      }
      return result
    }
  }
  return ''
}

export function getShapeName(sp: SlideElement) {
  if (sp == null) {
    return ''
  }

  // name
  const nvProps = getCNvProps(sp)
  return nvProps?.name ? nvProps.name : ''
}

/** 没有 preset 则为 custGeometry, 即自定义形状 */
export function getShapeType(sp: SlideElement): PresetShapeTypes | 'custom' {
  // shapeDesc
  const geometry = (sp.spPr as SpPr).geometry
  if (geometry?.preset) return geometry?.preset as PresetShapeTypes
  return 'custom'
}

export function getBlipFill(sp: SlideElement): BlipFill | null {
  if (sp == null) {
    return null
  }
  switch (sp.instanceType) {
    case InstanceType.GroupShape:
    case InstanceType.GraphicFrame:
      return null
    case InstanceType.ImageShape:
    case InstanceType.Shape:
      if (sp.blipFill) return sp.blipFill
      if (sp.brush?.fill && sp.brush?.fill?.type === FillKIND.BLIP) {
        return sp.brush.fill
      }
      return null
  }
}

export function getCalcedLine(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
    case InstanceType.ImageShape: {
      let lvl = 0
      const isLineDirty = isLineCalcStatusSet(sp)
      if (isLineDirty) {
        sp.calcedLine = null
        if (sp.spPr?.ln) {
          sp.calcedLine = sp.spPr!.ln!.clone()
        } else {
          const hsps = getHierarchySps(sp)
          for (let i = 0; i < hsps.length; ++i) {
            const hsp = hsps[i]
            if (hsp?.spPr?.ln) {
              sp.calcedLine = hsp.spPr.ln.clone()
              lvl = i + 1
              break
            }
          }
        }
        if (sp.instanceType === InstanceType.Shape) {
          sp.unsetCalcStatusOf(CalcStatus.Shape.Line)
        } else {
          unsetLineCalcStatus(sp)
        }
      }
      return { ln: sp.calcedLine, lvl }
    }
    case InstanceType.GroupShape:
    case InstanceType.GraphicFrame:
      return {}
  }
}

export function getParentsOfElement(sp: SlideElement | Slide | Notes): {
  presentation: Nullable<Presentation>
  slide: Nullable<Slide>
  layout: Nullable<SlideLayout>
  master: Nullable<SlideMaster | NotesMaster>
  theme: Nullable<Theme>
  notes?: Nullable<Notes>
} {
  switch (sp.instanceType) {
    case InstanceType.Slide:
    case InstanceType.Notes:
      return getParentsByTypes(sp)
    default: {
      if (sp.parent && sp.parent.instanceType != null) {
        const parent = sp.parent
        return getParentsByTypes(parent)
      }
    }
  }
  function getParentsByTypes(parent: SlideElementParent): {
    presentation: Nullable<Presentation>
    slide: Nullable<Slide>
    layout: Nullable<SlideLayout>
    master: Nullable<SlideMaster | NotesMaster>
    theme: Nullable<Theme>
    notes?: Nullable<Notes>
  } {
    const presentation = getPresentation(parent)
    switch (parent.instanceType) {
      case InstanceType.Slide: {
        return {
          presentation,
          slide: parent,
          layout: parent.layout,
          master: parent.layout ? parent.layout.master : null,
          theme: (sp as Shape).themeOverride
            ? (sp as Shape).themeOverride
            : parent.layout && parent.layout.master
            ? parent.layout.master.theme
            : null,
        }
      }
      case InstanceType.SlideLayout: {
        return {
          presentation,
          slide: null,
          layout: parent,
          master: parent.master,
          theme: (sp as Shape).themeOverride
            ? (sp as Shape).themeOverride
            : parent.master
            ? parent.master.theme
            : null,
        }
      }
      case InstanceType.SlideMaster: {
        return {
          presentation,
          slide: null,
          layout: null,
          master: parent,
          theme: (sp as Shape).themeOverride
            ? (sp as Shape).themeOverride
            : parent.theme,
        }
      }
      case InstanceType.Notes: {
        return {
          presentation,
          slide: null,
          layout: null,
          master: parent.master,
          theme: (sp as Shape).themeOverride
            ? (sp as Shape).themeOverride
            : parent.master
            ? parent.master.theme
            : null,
          notes: parent,
        }
      }
      case InstanceType.NotesMaster: {
        return {
          presentation,
          slide: null,
          layout: null,
          master: parent,
          theme: (sp as Shape).themeOverride
            ? (sp as Shape).themeOverride
            : parent
            ? parent.theme
            : null,
          notes: null,
        }
      }
      default: {
        return {
          presentation: null,
          slide: null,
          layout: null,
          master: null,
          theme: null,
        }
      }
    }
  }
  return {
    presentation: null,
    slide: null,
    layout: null,
    master: null,
    theme: null,
  }
}

export function getCalcedFill(sp: SlideElement, ignoreGradFill = false) {
  let calcedFill: Nullable<FillEffects>
  if (
    sp.instanceType === InstanceType.GroupShape ||
    ((sp.instanceType === InstanceType.Shape ||
      sp.instanceType === InstanceType.ImageShape) &&
      isFillCalcStatusSet(sp))
  ) {
    calcedFill = null
    if (sp.spPr?.fillEffects?.fill) {
      if (
        ignoreGradFill !== true &&
        isInstanceTypeOf(sp.spPr.fillEffects.fill, InstanceType.GradFill) &&
        (sp.spPr.fillEffects.fill as GradFill).gsList.length === 0
      ) {
        const parentByTypes = getParentsOfElement(sp)
        const theme = parentByTypes.theme!
        const fmt_scheme = theme.themeElements.fmtScheme
        const fill_style_lst = fmt_scheme.fillStyleLst
        for (let i = fill_style_lst.length - 1; i > -1; --i) {
          if (
            fill_style_lst[i] &&
            isInstanceTypeOf(fill_style_lst[i].fill, InstanceType.GradFill)
          ) {
            sp.spPr.fillEffects = fill_style_lst[i].clone()
            break
          }
        }
      }
      calcedFill = sp.spPr.fillEffects.clone()
      if (calcedFill?.fill?.type === FillKIND.GRP) {
        if (sp.group) {
          const group_calced_fill = getCalcedFill(sp.group, true)
          if (group_calced_fill?.fill) {
            calcedFill = group_calced_fill.clone()
          } else {
            calcedFill = null
          }
        } else {
          calcedFill = null
        }
      }
    } else if (
      isDict(sp.group) &&
      sp.instanceType !== InstanceType.ImageShape // image shape donot use parent group fill
    ) {
      const group_calced_fill = getCalcedFill(sp.group)
      if (group_calced_fill?.fill) {
        calcedFill = group_calced_fill.clone()
      } else {
        const hsps = getHierarchySps(sp)
        for (const hsp of hsps) {
          if (hsp?.spPr?.fillEffects?.fill) {
            calcedFill = hsp.spPr.fillEffects.clone()
            break
          }
        }
      }
    } else {
      const hsps = getHierarchySps(sp)
      for (let i = 0; i < hsps.length; ++i) {
        const hsp = hsps[i]
        if (hsp?.spPr?.fillEffects?.fill) {
          calcedFill = hsp.spPr.fillEffects.clone()
          break
        }
      }
    }
    if (sp.instanceType !== InstanceType.GroupShape) {
      unsetFillCalcStatus(sp)
    }
  }
  return calcedFill
}

export function getCalcedTransparent(sp: SlideElement) {
  let calcedTransparent
  if (
    sp.instanceType === InstanceType.Shape ||
    sp.instanceType === InstanceType.ImageShape
  ) {
    if (isTransparentCalcStatusSet(sp)) {
      calcedTransparent = null
      if (sp.spPr?.fillEffects) {
        if (isNumber(sp.spPr.fillEffects?.transparent)) {
          calcedTransparent = sp.spPr!.fillEffects!.transparent
        } else {
          if (sp.spPr.fillEffects?.fill?.type === FillKIND.GRP) {
            if (isNumber(sp.group?.spPr?.fillEffects?.transparent)) {
              calcedTransparent = sp.group!.spPr!.fillEffects!.transparent
            }
          }
        }
      }
      if (null != calcedTransparent) {
        unsetTransparentCalcStatus(sp)
        return calcedTransparent
      }

      const hsps = getHierarchySps(sp)
      for (let i = 0; i < hsps.length; ++i) {
        const sp = hsps[i]
        if (sp?.spPr?.fillEffects?.transparent) {
          calcedTransparent = sp.spPr.fillEffects.transparent
          break
        }
      }

      unsetTransparentCalcStatus(sp)
    }
  }

  return calcedTransparent
}
function _getSpStyle(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.GraphicFrame:
    case InstanceType.GroupShape:
      return undefined
    default:
      return sp.style
  }
}
export function getCalcedStyleForSp(sp: SlideElement) {
  const spStyle = _getSpStyle(sp)
  if (spStyle) {
    return { style: spStyle, lvl: 0 }
  }

  const hsps = getHierarchySps(sp)
  for (let i = 0; i < hsps.length; ++i) {
    const hsp = hsps[i]
    const hstyle = hsp ? _getSpStyle(hsp) : undefined
    if (hstyle) {
      return { style: hstyle, lvl: i + 1 }
    }
  }
  return {}
}

export function testTextDocOfShape(textDoc: Nullable<TextDocument>) {
  if (!textDoc) {
    return textDoc
  }
  if (textDoc.parent?.instanceType === InstanceType.TextBody) {
    if (textDoc.isEmpty() && isPlaceholder(textDoc.parent.parent)) {
      return textDoc
    }
    const bodyPr = textDoc.parent.getBodyPr()
    if (bodyPr.vertOverflow !== TextOverflowType.Overflow) {
      return textDoc
    }
  }
  return null
}

export function getMediaFromShape(sp: SlideElement): UniMedia | undefined {
  if (sp) {
    const unimedia = getUniNvPr(sp)?.nvPr?.unimedia
    return unimedia
  }
}

export function isEavertTextContent(txBody?: any): boolean {
  if (txBody && isInstanceTypeOf(txBody, InstanceType.TextBody)) {
    return txBody.getBodyPr()?.vert === TextVerticalType.eaVert
  }
  return false
}

export function getSpImages(
  sp: SlideElement,
  images: Dict = {}
): Dict<Boolean> {
  if (!sp) {
    return images
  }
  switch (sp.instanceType) {
    case InstanceType.GroupShape:
      sp.spTree.forEach((subSp) => {
        getSpImages(subSp, images)
      })
      break
    case InstanceType.ImageShape:
      if (
        isInstanceTypeOf(sp.blipFill, InstanceType.BlipFill) &&
        typeof sp.blipFill?.imageSrcId === 'string'
      ) {
        images[checkImageSrc(sp.blipFill.imageSrcId)] = true
      }
      break
    case InstanceType.Shape:
      const imageSrcId = (sp.spPr?.fillEffects?.fill as BlipFill)?.imageSrcId
      if (
        isInstanceTypeOf(sp.spPr?.fillEffects?.fill, InstanceType.BlipFill) &&
        typeof imageSrcId === 'string'
      ) {
        images[checkImageSrc(imageSrcId)] = true
      }
      break
    case InstanceType.GraphicFrame:
      break
  }
  return images
}

/** 获取 shape 内所有的图片资源以 {url, encryptUrl?}[] 的形式输出*/
export function getSpImgInfo(
  sp: SlideElement,
  imgInfos: Array<UploadUrlResult> = []
): Array<UploadUrlResult> {
  if (!sp) {
    return imgInfos
  }
  switch (sp.instanceType) {
    case InstanceType.GroupShape:
      sp.spTree.forEach((subSp) => {
        getSpImages(subSp, imgInfos)
      })
      break
    case InstanceType.ImageShape:
      if (
        isInstanceTypeOf(sp.blipFill, InstanceType.BlipFill) &&
        typeof sp.blipFill?.imageSrcId === 'string'
      ) {
        imgInfos.push({
          url: sp.blipFill.imageSrcId,
          encryptUrl: sp.blipFill.encryptUrl,
        })
      }
      break
    case InstanceType.Shape:
      const imageSrcId = (sp.spPr?.fillEffects?.fill as BlipFill)?.imageSrcId
      if (
        isInstanceTypeOf(sp.spPr?.fillEffects?.fill, InstanceType.BlipFill) &&
        typeof imageSrcId === 'string'
      ) {
        imgInfos.push({
          url: imageSrcId,
          encryptUrl: (sp.spPr?.fillEffects?.fill as BlipFill).encryptUrl,
        })
      }
      break
    case InstanceType.GraphicFrame:
      break
  }
  return imgInfos
}

export function getHyperlinkFromShape(sp: SlideElement) {
  const nvProps = getCNvProps(sp)
  return nvProps?.hlinkClick
}

export function getParentSlide(target: any): Nullable<Slide> {
  if (!isSlideElement(target)) {
    return null
  }
  return findClosestParent(
    target,
    (parent) => parent.instanceType === InstanceType.Slide
  ) as Nullable<Slide>
}

/** 根据坐标获取单元格位置 */
export function getCellBoundsByIndex(
  sp: GraphicFrame,
  rowIndex: number,
  cellIndex: number
) {
  if (!sp.graphicObject) return
  const { tableGridReduced, rowsInfo, children: rows } = sp.graphicObject
  const row = rows.at(rowIndex)
  if (row == null) return

  const cells = row.children
  const cell = cells.at(cellIndex)
  if (!cell) return

  const isRTL = sp.graphicObject.isRTL === true
  let [l, r] = isRTL ? [sp.x + sp.extX, sp.x + sp.extX] : [sp.x, sp.x]
  let accGridSpan = 0

  for (let i = 0; i <= cellIndex; i++) {
    const curGridSpan = cells.at(i)!.getGridSpan()
    accGridSpan += curGridSpan
    if (i === cellIndex) {
      const direction = isRTL ? -1 : 1
      l += direction * tableGridReduced[accGridSpan - curGridSpan - 1]
      r += direction * tableGridReduced[accGridSpan - 1]
    }
  }

  let [t, b] = [sp.y, sp.y]
  for (let i = 0; i <= rowIndex; i++) {
    const rowHeight = rowsInfo[i].h ?? rows.at(i)!.height
    if (i === rowIndex) {
      b = t + rowHeight
    } else {
      t += rowHeight
    }
  }
  const rowCount = rows.length
  for (let j = rowIndex + 1; j < rowCount; j++) {
    const underCell = rows.at(j)?.children.at(cellIndex)
    if (!underCell || underCell.getVMerge() !== VMergeType.Continue) break
    const rowHeight = rowsInfo[j].h ?? rows.at(j)!.height
    b += rowHeight
  }
  const [w, h] = [r - l, b - t]
  return { l, t, r, b, w, h }
}

export function getSpStrokeWidth(sp: SlideElement) {
  return (sp.pen?.w == null ? 12700 : toInt(sp.pen.w)) / ScaleOfPPTXSizes
}

function getBoundsInnerGroup(sp: SlideElement) {
  const r = sp.rot
  if (!isNumber(r) || isRotateIn45D(r)) {
    return new GraphicBounds(sp.x, sp.y, sp.x + sp.extX, sp.y + sp.extY)
  } else {
    const hc = sp.extX * 0.5
    const vc = sp.extY * 0.5
    const xc = sp.x + hc
    const yc = sp.y + vc
    return new GraphicBounds(xc - vc, yc - hc, xc + vc, yc + hc)
  }
}

export function getBoundsOfGroupingSps(sps: SlideElement[]): GraphicBounds {
  let bounds = getBoundsInnerGroup(sps[0])
  let maxX = bounds.r
  let maxY = bounds.b
  let minX = bounds.l
  let minY = bounds.t
  for (let i = 1; i < sps.length; ++i) {
    bounds = getBoundsInnerGroup(sps[i])
    if (maxX < bounds.r) maxX = bounds.r
    if (maxY < bounds.b) maxY = bounds.b
    if (minX > bounds.l) minX = bounds.l
    if (minY > bounds.t) minY = bounds.t
  }
  return new GraphicBounds(minX, minY, maxX, maxY)
}

export function collectCxnShapes(
  arrOfSps: Nullable<SlideElement[]>,
  allSps: ConnectionShape[] = []
) {
  if (!arrOfSps || !Array.isArray(arrOfSps)) {
    return allSps
  }

  for (let i = 0; i < arrOfSps.length; ++i) {
    const sp = arrOfSps[i]
    if (sp.instanceType === InstanceType.Shape && sp.isConnectionShape()) {
      allSps.push(sp as ConnectionShape)
    } else if (sp.instanceType === InstanceType.GroupShape) {
      collectCxnShapes(sp.spTree, allSps)
    }
  }
  return allSps
}

export function getFinalRotate(sp: SlideElement) {
  return sp.group == null ? sp.rot : sp.rot + getFinalRotate(sp.group)
}

function getFlipsForSp(sp: SlideElement, transform?: Matrix2D, rot?: number) {
  transform = transform ?? sp.calcedTransform
  const rotate = rot ?? getFinalRotate(sp)
  const ltx = transform.XFromPoint(0, 0)
  const lty = transform.YFromPoint(0, 0)

  const rtx = transform.XFromPoint(sp.extX, 0)
  const rty = transform.YFromPoint(sp.extX, 0)

  const rbx = transform.XFromPoint(sp.extX, sp.extY)
  const rby = transform.YFromPoint(sp.extX, sp.extY)

  const t = new Matrix2D()
  MatrixUtils.rotateMatrix(t, rotate)

  const rot_ltx = t.XFromPoint(ltx, lty)

  const rot_rtx = t.XFromPoint(rtx, rty)
  const rot_rty = t.YFromPoint(rtx, rty)

  const rot_rby = t.YFromPoint(rbx, rby)
  return {
    flipH: rot_ltx > rot_rtx,
    flipV: rot_rty > rot_rby,
  }
}

export function checkTextTransformMatrix(
  sp: Shape,
  matrix: Matrix2D,
  textDoc: TextDocument,
  bodyPr: BodyPr,
  opts?: {
    isIgnoreInsets?: boolean
    transform?: Matrix2D
    rot?: number
  }
): {
  x: number
  y: number
  w: number
  h: number
} {
  matrix.reset()
  const { isIgnoreInsets, transform, rot } = opts ?? {}
  const spTransform = transform ?? sp.calcedTransform
  const contentHeight = textDoc.getWholeHeight()
  const textRect = sp.getTextRect()
  const lIns = isIgnoreInsets ? 0 : isNumber(bodyPr.lIns) ? bodyPr.lIns : 2.54
  const tIns = isIgnoreInsets ? 0 : isNumber(bodyPr.tIns) ? bodyPr.tIns : 1.27
  const rIns = isIgnoreInsets ? 0 : isNumber(bodyPr.rIns) ? bodyPr.rIns : 2.54
  const bIns = isIgnoreInsets ? 0 : isNumber(bodyPr.bIns) ? bodyPr.bIns : 1.27

  let l = textRect.l + lIns
  let t = textRect.t + tIns
  let r = textRect.r - rIns
  let b = textRect.b - bIns
  let c

  if (l >= r) {
    c = (l + r) * 0.5
    l = c - 0.01
    r = c + 0.01
  }

  if (t >= b) {
    c = (t + b) * 0.5
    t = c - 0.01
    b = c + 0.01
  }

  const xc = textDoc.renderingState.xLimit / 2.0
  const yc = contentHeight / 2.0

  const _rot = normalizeRotValue(
    (isNumber(bodyPr.rot) ? bodyPr.rot : 0) * radFactor
  )

  if (!checkAlmostEqual(_rot, 0.0)) {
    MatrixUtils.translateMatrix(matrix, -xc, -yc)
    MatrixUtils.rotateMatrix(matrix, -_rot)
    MatrixUtils.translateMatrix(matrix, xc, yc)
  }
  const lt_tx = spTransform.XFromPoint(l, t)
  const lt_ty = spTransform.YFromPoint(l, t)

  const rt_tx = spTransform.XFromPoint(r, t)
  const rt_ty = spTransform.YFromPoint(r, t)

  const lb_tx = spTransform.XFromPoint(l, b)
  const lb_ty = spTransform.YFromPoint(l, b)

  const rb_tx = spTransform.XFromPoint(r, b)
  const rb_ty = spTransform.YFromPoint(r, b)

  const dtx = rt_tx - lt_tx
  const dty = rt_ty - lt_ty

  const lt_rb_dx = rb_tx - lt_tx
  const lt_rb_dy = rb_ty - lt_ty

  let vertShift
  const heightLimitInSp = b - t
  const widthLimitInSp = r - l
  let clipRect
  if (!bodyPr.upright) {
    if (
      !(
        bodyPr.vert === TextVerticalType.vert ||
        bodyPr.vert === TextVerticalType.vert270 ||
        bodyPr.vert === TextVerticalType.eaVert
      )
    ) {
      if (
        bodyPr.vertOverflow === TextOverflowType.Overflow ||
        contentHeight < heightLimitInSp
      ) {
        switch (bodyPr.anchor) {
          case 0: {
            //b
            // (Text Anchor Enum ( Bottom ))
            vertShift = heightLimitInSp - contentHeight
            break
          }
          case 1: {
            //ctr
            // (Text Anchor Enum ( Center ))
            vertShift = (heightLimitInSp - contentHeight) * 0.5
            break
          }
          case 2: {
            //dist

            vertShift = (heightLimitInSp - contentHeight) * 0.5
            break
          }
          case 3: {
            //just

            vertShift = (heightLimitInSp - contentHeight) * 0.5
            break
          }
          case 4: {
            //t
            //Top
            vertShift = 0
            break
          }
        }
      } else {
        const firstLine = textDoc.children.at(0)?.renderingState.getLine(0)
        if (
          bodyPr.vertOverflow === TextOverflowType.Clip &&
          firstLine &&
          firstLine.bottom > heightLimitInSp
        ) {
          const firstLineBottom = firstLine.bottom
          switch (bodyPr.anchor) {
            case 0: {
              //b
              // (Text Anchor Enum ( Bottom ))
              vertShift = heightLimitInSp - firstLineBottom
              break
            }
            case 1: {
              //ctr
              // (Text Anchor Enum ( Center ))
              vertShift = (heightLimitInSp - firstLineBottom) * 0.5
              break
            }
            case 2: {
              //dist
              // (Text Anchor Enum ( Distributed ))
              vertShift = (heightLimitInSp - firstLineBottom) * 0.5
              break
            }
            case 3: {
              //just
              // (Text Anchor Enum ( Justified ))
              vertShift = (heightLimitInSp - firstLineBottom) * 0.5
              break
            }
            case 4: {
              //t
              //Top
              vertShift = 0
              break
            }
          }
        } else {
          vertShift = heightLimitInSp - contentHeight
          if (bodyPr.anchor === 0) {
            vertShift = heightLimitInSp - contentHeight
          } else {
            vertShift = 0
          }
        }
      }

      MatrixUtils.translateMatrix(matrix, 0, vertShift)
      let alpha
      if (lt_rb_dx * dty - lt_rb_dy * dtx <= 0) {
        alpha = Math.atan2(dty, dtx)
        MatrixUtils.rotateMatrix(matrix, -alpha)
        MatrixUtils.translateMatrix(matrix, lt_tx, lt_ty)
      } else {
        alpha = Math.atan2(dty, dtx)
        MatrixUtils.rotateMatrix(matrix, Math.PI - alpha)
        MatrixUtils.translateMatrix(matrix, rt_tx, rt_ty)
      }
    } else {
      if (
        bodyPr.vertOverflow === TextOverflowType.Overflow ||
        contentHeight <= widthLimitInSp
      ) {
        switch (bodyPr.anchor) {
          case 0: {
            //b
            // (Text Anchor Enum ( Bottom ))
            vertShift = widthLimitInSp - contentHeight
            break
          }
          case 1: {
            //ctr
            // (Text Anchor Enum ( Center ))
            vertShift = (widthLimitInSp - contentHeight) * 0.5
            break
          }
          case 2: {
            //dist
            // (Text Anchor Enum ( Distributed ))
            vertShift = (widthLimitInSp - contentHeight) * 0.5
            break
          }
          case 3: {
            //just
            // (Text Anchor Enum ( Justified ))
            vertShift = (widthLimitInSp - contentHeight) * 0.5
            break
          }
          case 4: {
            //t
            //Top
            vertShift = 0
            break
          }
        }
      } else {
        if (bodyPr.anchor === 0) {
          vertShift = widthLimitInSp - contentHeight
        } else {
          vertShift = 0
        }
      }

      MatrixUtils.translateMatrix(matrix, 0, vertShift)
      const alpha = Math.atan2(dty, dtx)
      if (
        bodyPr.vert === TextVerticalType.vert ||
        bodyPr.vert === TextVerticalType.eaVert
      ) {
        if (lt_rb_dx * dty - lt_rb_dy * dtx <= 0) {
          MatrixUtils.rotateMatrix(matrix, -alpha - Math.PI * 0.5)
          MatrixUtils.translateMatrix(matrix, rt_tx, rt_ty)
        } else {
          MatrixUtils.rotateMatrix(matrix, Math.PI * 0.5 - alpha)
          MatrixUtils.translateMatrix(matrix, lt_tx, lt_ty)
        }
      } else {
        if (lt_rb_dx * dty - lt_rb_dy * dtx <= 0) {
          MatrixUtils.rotateMatrix(matrix, -alpha - Math.PI * 1.5)
          MatrixUtils.translateMatrix(matrix, lb_tx, lb_ty)
        } else {
          MatrixUtils.rotateMatrix(matrix, -Math.PI * 0.5 - alpha)
          MatrixUtils.translateMatrix(matrix, rb_tx, rb_ty)
        }
      }
    }
    if (sp.spPr && isDict(sp.spPr.geometry) && isDict(sp.spPr.geometry!.rect)) {
      const rect = sp.spPr.geometry!.rect!
      const Diff = 1.6
      let clipW = rect.r - rect.l + Diff
      if (clipW <= 0) {
        clipW = 0.01
      }
      let clipH = rect.b - rect.t + Diff - bIns - tIns
      if (clipH < 0) {
        clipH = 0.01
      }
      clipRect = {
        x: rect.l - Diff,
        y: rect.t - Diff + tIns,
        w: clipW,
        h: clipH,
      }
    } else {
      clipRect = {
        x: -1.6,
        y: tIns,
        w: sp.extX + 3.2,
        h: sp.extY - bIns,
      }
    }
  } else {
    const finalRotate = rot ?? getFinalRotate(sp)
    const finalFlip = getFlipsForSp(sp, transform, rot)

    const hc = sp.extX * 0.5
    const vc = sp.extY * 0.5
    const xc = spTransform.XFromPoint(hc, vc)
    const yc = spTransform.YFromPoint(hc, vc)

    let contentWidth, contentHeight
    if (isRotateIn45D(finalRotate)) {
      if (
        !(
          bodyPr.vert === TextVerticalType.vert ||
          bodyPr.vert === TextVerticalType.vert270 ||
          bodyPr.vert === TextVerticalType.eaVert
        )
      ) {
        contentWidth = r - l
        contentHeight = b - t
      } else {
        contentWidth = b - t
        contentHeight = r - l
      }
    } else {
      if (
        !(
          bodyPr.vert === TextVerticalType.vert ||
          bodyPr.vert === TextVerticalType.vert270 ||
          bodyPr.vert === TextVerticalType.eaVert
        )
      ) {
        contentWidth = b - t
        contentHeight = r - l
      } else {
        contentWidth = r - l
        contentHeight = b - t
      }
    }

    switch (bodyPr.anchor) {
      case 0: {
        //b
        // (Text Anchor Enum ( Bottom ))
        vertShift = contentHeight - contentHeight
        break
      }
      case 1: {
        //ctr
        // (Text Anchor Enum ( Center ))
        vertShift = (contentHeight - contentHeight) * 0.5
        break
      }
      case 2: {
        //dist
        // (Text Anchor Enum ( Distributed ))
        vertShift = (contentHeight - contentHeight) * 0.5
        break
      }
      case 3: {
        //just
        // (Text Anchor Enum ( Justified ))
        vertShift = (contentHeight - contentHeight) * 0.5
        break
      }
      case 4: {
        //t
        //Top
        vertShift = 0
        break
      }
    }

    const textRectXCenter = l + (r - l) * 0.5
    const textRectYCenter = t + (b - t) * 0.5

    const vx = textRectXCenter - hc
    const vy = textRectYCenter - vc

    const text_xc = finalFlip.flipH ? xc - vx : xc + vx
    const text_yc = finalFlip.flipV ? yc - vy : yc + vy

    MatrixUtils.translateMatrix(matrix, 0, vertShift)
    if (
      bodyPr.vert === TextVerticalType.vert ||
      bodyPr.vert === TextVerticalType.eaVert
    ) {
      MatrixUtils.translateMatrix(
        matrix,
        -contentWidth * 0.5,
        -contentHeight * 0.5
      )
      MatrixUtils.rotateMatrix(matrix, -Math.PI * 0.5)
      MatrixUtils.translateMatrix(
        matrix,
        contentWidth * 0.5,
        contentHeight * 0.5
      )
    }
    if (bodyPr.vert === TextVerticalType.vert270) {
      MatrixUtils.translateMatrix(
        matrix,
        -contentWidth * 0.5,
        -contentHeight * 0.5
      )
      MatrixUtils.rotateMatrix(matrix, -Math.PI * 1.5)
      MatrixUtils.translateMatrix(
        matrix,
        contentWidth * 0.5,
        contentHeight * 0.5
      )
    }
    MatrixUtils.translateMatrix(
      matrix,
      text_xc - contentWidth * 0.5,
      text_yc - contentHeight * 0.5
    )

    const Diff = 1.6

    let clipW = textRect.r - textRect.l + Diff - lIns - rIns
    if (clipW <= 0) {
      clipW = 0.01
    }
    let clipH = textRect.b - textRect.t + Diff - bIns - tIns
    if (clipH < 0) {
      clipH = 0.01
    }
    clipRect = {
      x: textRect.l + lIns - Diff,
      y: textRect.t - Diff + tIns,
      w: clipW,
      h: clipH,
    }
  }
  return clipRect
}
export function collectAllFontNamesForSlideElement(
  sp: SlideElement,
  allFonts: Dict
) {
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      if (sp.txBody) {
        sp.txBody!.textDoc!.collectAllFontNames(allFonts)
        if (sp.txBody && sp.txBody.lstStyle) {
          sp.txBody.lstStyle.collectAllFontNames(allFonts)
        }
        delete allFonts['+mj-lt']
        delete allFonts['+mn-lt']
        delete allFonts['+mj-ea']
        delete allFonts['+mn-ea']
        delete allFonts['+mj-cs']
        delete allFonts['+mn-cs']
      }
      break
    }
    case InstanceType.GraphicFrame: {
      if (sp.graphicObject) {
        for (let i = 0, l = sp.graphicObject.children.length; i < l; ++i) {
          const row = sp.graphicObject.children.at(i)!
          const cells = row.children
          for (let j = 0, l = cells.length; j < l; ++j) {
            cells.at(j)!.textDoc.collectAllFontNames(allFonts)
          }
        }
        delete allFonts['+mj-lt']
        delete allFonts['+mn-lt']
        delete allFonts['+mj-ea']
        delete allFonts['+mn-ea']
        delete allFonts['+mj-cs']
        delete allFonts['+mn-cs']
      }
      break
    }
    case InstanceType.GroupShape: {
      for (let i = 0; i < sp.spTree.length; ++i) {
        collectAllFontNamesForSlideElement(sp.spTree[i], allFonts)
      }
      break
    }
  }
}

export function getUniNvPr(sp: Nullable<SlideElement>) {
  if (sp == null) return undefined
  switch (sp.instanceType) {
    case InstanceType.GroupShape:
      return sp.nvGrpSpPr
    case InstanceType.ImageShape:
      return sp.nvPicPr
    case InstanceType.Shape:
      return sp.nvSpPr
    case InstanceType.GraphicFrame:
      return sp.nvGraphicFramePr
  }
}

export function getCNvProps(sp: Nullable<SlideElement>) {
  const uniNvPr = getUniNvPr(sp)
  if (uniNvPr) {
    return uniNvPr.cNvPr
  }
  return null
}

export function getNvProps(sp: Nullable<SlideElement>) {
  const uniNvPr = getUniNvPr(sp)
  if (uniNvPr) {
    return uniNvPr.nvPr
  }
  return null
}
export function getTitleForSp(sp: Nullable<SlideElement>) {
  const nvPr = getCNvProps(sp)
  if (nvPr) {
    return nvPr.title ? nvPr.title : undefined
  }
  return undefined
}

export function getDescriptionForSp(sp: Nullable<SlideElement>) {
  const nvPr = getCNvProps(sp)
  if (nvPr) {
    return nvPr.descr ? nvPr.descr : undefined
  }
  return undefined
}
export function getGeomPreset(sp: SlideElement) {
  if (sp.spPr && sp.spPr.geometry) {
    return sp.spPr.geometry.preset
  } else {
    if (
      (sp.instanceType === InstanceType.Shape ||
        sp.instanceType === InstanceType.ImageShape) &&
      sp.calcedGeometry
    ) {
      return sp.calcedGeometry.preset
    }
    return null
  }
}

export function getBlipFillForSp(sp: SlideElement): Nullable<BlipFill> {
  switch (sp.instanceType) {
    case InstanceType.Shape:
    case InstanceType.ImageShape:
      return (sp as ImageShape).blipFill
    default:
      return null
  }
}

export function getTopMostGroupShape(
  sp: Nullable<SlideElement>
): Nullable<GroupShape> {
  if (sp == null) return undefined
  if (!sp.group) {
    if (sp.instanceType === InstanceType.GroupShape) {
      return sp
    }
    return null
  }
  return getTopMostGroupShape(sp.group)
}

export function getFinalFlipH(sp: SlideElement) {
  if (!sp.group) return sp.flipH
  return getFinalFlipH(sp.group) ? !sp.flipH : sp.flipH
}

export function getFinalFlipV(sp: SlideElement) {
  if (!sp.group) return sp.flipV
  return getFinalFlipV(sp.group) ? !sp.flipV : sp.flipV
}

export function getOrderForSlideElement(target: SlideElement): number {
  const spTree = target.spTreeParent?.spTree
  if (!Array.isArray(spTree)) {
    return -1
  }
  return findIndex((sp) => sp === target, spTree)
}

export function isBgTransparentForSp(sp: SlideElement): boolean {
  if (sp.instanceType === InstanceType.Shape && !sp.isConnectionShape()) {
    const fillEffects = sp.brush
    if (fillEffects == null) return true

    let transparent
    if (fillEffects?.fill?.type === FillKIND.BLIP) {
      transparent = fillEffects.fill.getTransparent()
    } else if (fillEffects?.transparent != null) {
      transparent = fillEffects.transparent
    } else if (fillEffects?.fill?.type === FillKIND.NOFILL) {
      transparent = 0
    }

    return transparent != null && transparent <= 0
  }

  return false
}
