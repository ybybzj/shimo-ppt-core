import { setLock } from './locks'
import { setDeletedForSp } from './updates'
import { ShapeLocks } from '../../SlideElement/const'
import { createGeometry } from '../../SlideElement/geometry/creators'
import { BlipFill } from '../../SlideElement/attrs/fill/blipFill'
import { SpPr, Xfrm, UniNvPr } from '../../SlideElement/attrs/shapePrs'
import { ImageShape } from '../../SlideElement/Image'
import { UploadUrlResult } from '../../../editor/type'
import { OleObject } from '../../SlideElement/OleObject'

export interface ImageShapeSubTypeMap {
  image: ImageShape
  ole: OleObject
}

export type ImageShapeSubTypes = keyof ImageShapeSubTypeMap

export function buildImageShape<T extends ImageShapeSubTypes>(
  srcInfo: UploadUrlResult,
  x: number,
  y: number,
  extX: number,
  extY: number,
  type: T = 'image' as T
): ImageShapeSubTypeMap[T] {
  const image = type === 'image' ? new ImageShape() : new OleObject()
  fillImage(image, srcInfo, x, y, extX, extY)
  return image as ImageShapeSubTypeMap[T]
}

function fillImage(
  image: ImageShape,
  srcInfo: UploadUrlResult,
  x: number,
  y: number,
  extX: number,
  extY: number
) {
  image.setSpPr(new SpPr())
  image.spPr!.setParent(image)
  image.spPr!.setGeometry(createGeometry('rect'))
  image.spPr!.setXfrm(new Xfrm(image.spPr))
  image.spPr!.xfrm!.setParent(image.spPr)
  image.spPr!.xfrm!.setOffX(x)
  image.spPr!.xfrm!.setOffY(y)
  image.spPr!.xfrm!.setExtX(extX)
  image.spPr!.xfrm!.setExtY(extY)

  const blipFill = new BlipFill(srcInfo.url, srcInfo.encryptUrl)
  blipFill.setStretch(true)
  image.setBlipFill(blipFill)
  const nvPicPr = new UniNvPr(image)
  image.setNvPicPr(nvPicPr)
  setLock(image, ShapeLocks.noModifyAspect, true)
  setDeletedForSp(image, false)
}
