import { ConnectionShape } from '../../SlideElement/ConnectionShape'
import { getCNvProps, getUniNvPr } from './getters'
import { SlideElement } from '../../SlideElement/type'
import { InstanceType } from '../../instanceTypes'
import { Shape } from '../../SlideElement/Shape'
import { isGroup } from './asserts'
import { gEntityRegistry } from '../../EntityRegistry'

const gShapesMap: Map<string, string> = new Map()
const gCxnSpToSpMap: Map<string, [string | null, string | null]> = new Map()
export function collectCxnSpToSpMapState(
  sp: ConnectionShape,
  startSpId?: number,
  endSpId?: number
) {
  if (sp && sp.isConnectionShape()) {
    const id = sp.id
    if (id != null) {
      if (startSpId != null || endSpId != null) {
        gCxnSpToSpMap.set(String(id), [
          startSpId != null ? String(startSpId) : null,
          endSpId != null ? String(endSpId) : null,
        ])
      } else {
        const nvUniSpPr = getUniNvPr(sp)?.nvUniSpPr
        if (nvUniSpPr) {
          const { stCxnId, endCxnId } = nvUniSpPr
          gCxnSpToSpMap.set(String(id), [
            stCxnId != null ? String(stCxnId) : null,
            endCxnId != null ? String(endCxnId) : null,
          ])
        }
      }
    }
  }
}
export function collectCNvNidToSpIdMap(sp: SlideElement): void {
  const id = getCNvProps(sp)?.nid
  if (id != null) {
    gShapesMap.set(String(id), sp.id)
  }
  // shapesMap[sp.getRefId()] = sp // 复制粘贴幻灯片时生成新的sp
}

export function clearCxnState() {
  if (gShapesMap.size > 0) {
    gShapesMap.clear()
  }
  if (gCxnSpToSpMap.size > 0) {
    gCxnSpToSpMap.clear()
  }
}

export function applyCxnSpToSpMapState() {
  gCxnSpToSpMap.forEach(([stCxnId, endCxnId], cxnSpId) => {
    const cxnSp = gEntityRegistry.getEntityById<ConnectionShape>(cxnSpId)
    // Todo, 排查 cxnSp undefined case
    if (
      cxnSp &&
      cxnSp.instanceType === InstanceType.Shape &&
      cxnSp.isConnectionShape()
    ) {
      const nvUniSpPr = getUniNvPr(cxnSp)?.nvUniSpPr
      if (nvUniSpPr) {
        const sp_stCxnId = gShapesMap.get(String(stCxnId))
        if (sp_stCxnId != null) {
          nvUniSpPr.stCxnId = sp_stCxnId
        }
        const sp_endCxnId = gShapesMap.get(String(endCxnId))
        if (sp_endCxnId != null) {
          nvUniSpPr.endCxnId = sp_endCxnId
        }
        cxnSp.nvSpPr!.setUniSpPr(nvUniSpPr.clone())
      }
    }
  })
}

function getCxnSpRefIdAndUpdate(sp: SlideElement) {
  if (sp.instanceType === InstanceType.Shape && sp.isConnectionShape()) {
    const nvUniSpPr = getUniNvPr(sp)?.nvUniSpPr
    if (nvUniSpPr) {
      const { stCxnId, endCxnId } = nvUniSpPr
      const startSp = gEntityRegistry.getEntityById<Shape>(stCxnId)
      const endSp = gEntityRegistry.getEntityById<Shape>(endCxnId)
      const startSpNid = getCNvProps(startSp)?.nid
      const endSpNid = getCNvProps(endSp)?.nid
      collectCxnSpToSpMapState(sp as ConnectionShape, startSpNid, endSpNid)
    }
  }
}

export function collectCxnStateForSp(sp: SlideElement) {
  collectCNvNidToSpIdMap(sp)
  getCxnSpRefIdAndUpdate(sp)
  if (isGroup(sp)) {
    sp.spTree.forEach((ssp) => {
      collectCNvNidToSpIdMap(ssp)
      getCxnSpRefIdAndUpdate(ssp)
    })
  }
}
