import { InstanceType, isInstanceTypeOf } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { CalcStatus } from '../../calculation/updateCalcStatus/calcStatus'
import { isRotateIn45D } from '../../common/utils'
import { isNumbersTheSame } from '../../common/helpers'
import { GroupShape } from '../../SlideElement/GroupShape'
import { isGroup } from './asserts'
import { ensureSpPrXfrm } from './attrs'
import { GroupShapeRenderingMargin } from '../../common/const/ui'

export function calcScaleFactorsOfGroup(grp: GroupShape): {
  cx: number
  cy: number
  coff: number
} {
  const xfrm = grp.spPr?.xfrm
  if (grp.isCalcStatusSet(CalcStatus.GroupShape.ScaleFactors)) {
    let cx = 1
    let cy = 1
    if (xfrm?.isNotEmptyForGroup()) {
      const { extX, extY, chExtX, chExtY } = xfrm
      cx = chExtX > 0 ? extX! / chExtX : 1
      cy = chExtY > 0 ? extY! / chExtY : 1
    }
    if (grp.group?.instanceType === InstanceType.GroupShape) {
      const group_scale_factor = calcScaleFactorsOfGroup(grp.group)
      cx *= group_scale_factor.cx
      cy *= group_scale_factor.cy
    }
    grp.scaleFactors.cx = cx
    grp.scaleFactors.cy = cy
    grp.unsetCalcStatusOf(CalcStatus.GroupShape.ScaleFactors)
  }

  const sf = grp.scaleFactors

  const coff =
    sf.cx !== 1 || sf.cy !== 1 ? (xfrm!.extX > xfrm!.extY ? sf.cx : sf.cy) : 1
  return {
    cx: sf.cx,
    cy: sf.cy,
    coff,
  }
}

export function normalizeGroupExts(group: GroupShape) {
  for (const sp of group.spTree) {
    if (sp.instanceType === InstanceType.GroupShape) {
      normalizeGroupExts(sp)
    } else {
      _normalizeXfrmForSp(sp)
    }
  }
  _normalizeXfrmForSp(group)
  const xfrm = group.spPr!.xfrm
  if (xfrm) {
    xfrm.setChExtX(xfrm.extX)
    xfrm.setChExtY(xfrm.extY)
    xfrm.setChOffX(0)
    xfrm.setChOffY(0)
  }
}
function _normalizeXfrmForSp(sp: SlideElement) {
  if (isInstanceTypeOf(sp.group, InstanceType.GroupShape)) {
    const { cx, cy } = calcScaleFactorsOfGroup(sp.group!)
    const grpXfrm = sp.group?.spPr?.xfrm!
    const chOffX = grpXfrm?.chOffX ?? 0
    const chOffY = grpXfrm?.chOffY ?? 0
    if (cx === 1 && cy === 1 && chOffX === 0 && chOffY === 0) {
      return
    }
    const xfrm = sp.spPr!.xfrm!
    const isNotRotate = isRotateIn45D(xfrm?.rot ?? 0)

    let offX: number
    let offY: number
    let extX: number
    let extY: number
    if (isNotRotate) {
      extX = cx * xfrm.extX!
      extY = cy * xfrm.extY!
      offX = cx * (xfrm.offX! - chOffX)
      offY = cy * (xfrm.offY! - chOffY)
    } else {
      extX = cy * xfrm.extX!
      extY = cx * xfrm.extY!
      const _offX = (xfrm.offX - chOffX + (xfrm.extX - xfrm.extY) * 0.5) * cx
      const _offY = (xfrm.offY - chOffY + (xfrm.extY - xfrm.extX) * 0.5) * cy
      offX = _offX + (extY - extX) * 0.5
      offY = _offY + (extX - extY) * 0.5
    }

    if (!isNumbersTheSame(offX, xfrm.offX)) {
      xfrm.setOffX(offX)
    }
    if (!isNumbersTheSame(offY, xfrm.offY)) {
      xfrm.setOffY(offY)
    }
    if (!isNumbersTheSame(extX, xfrm.extX!)) {
      xfrm.setExtX(extX)
    }
    if (!isNumbersTheSame(extY, xfrm.extY!)) {
      xfrm.setExtY(extY)
    }
  }
}

export function adjustXfrmOfGroup(group: GroupShape, ignoreNormalize = false) {
  const { spTree } = group
  if (spTree.length <= 0) {
    return
  }
  if (!ignoreNormalize) {
    normalizeGroupExts(group)
  }

  for (const sp of group.spTree) {
    if (isGroup(sp)) {
      adjustXfrmOfGroup(sp, true)
    }
  }

  let minX, maxX, minY, maxY
  for (const sp of spTree) {
    ensureSpPrXfrm(sp)
    const xfrm = sp.spPr!.xfrm!
    const {
      minX: _minX,
      minY: _minY,
      maxX: _maxX,
      maxY: _maxY,
    } = getXfrmBounds(xfrm)
    minX = minX == null ? _minX : Math.min(minX, _minX)
    minY = minY == null ? _minY : Math.min(minY, _minY)
    maxX = maxX == null ? _maxX : Math.max(maxX, _maxX)
    maxY = maxY == null ? _maxY : Math.max(maxY, _maxY)
  }

  const minXForOffset = minX
  const minYForOffset = minY
  if (group.spPr!.xfrm?.flipH === true) {
    const t = maxX
    maxX = group.spPr!.xfrm.extX! - minX
    minX = group.spPr!.xfrm.extX! - t
  }

  if (group.spPr!.xfrm?.flipV === true) {
    const t = maxY
    maxY = group.spPr!.xfrm.extY! - minY
    minY = group.spPr!.xfrm.extY! - t
  }

  const xfrm = group.spPr!.xfrm!
  const rot = xfrm.rot == null ? 0 : xfrm.rot
  const chExtX = Math.abs(maxX - minX)
  const chExtY = Math.abs(maxY - minY)
  const halfExtX = xfrm.extX! * 0.5
  const halfExtY = xfrm.extY! * 0.5
  const orgX0 =
    group.spPr!.xfrm?.offX! +
    halfExtX -
    (halfExtX * Math.cos(rot) - halfExtY * Math.sin(rot))
  const orgY0 =
    group.spPr!.xfrm?.offY! +
    halfExtY -
    (halfExtX * Math.sin(rot) + halfExtY * Math.cos(rot))
  const dx = minX * Math.cos(rot) - minY * Math.sin(rot)
  const dy = minX * Math.sin(rot) + minY * Math.cos(rot)
  const x0 = orgX0 + dx
  const y0 = orgY0 + dy
  const hc = chExtX * 0.5
  const vc = chExtY * 0.5
  const _xc = x0 + (hc * Math.cos(rot) - vc * Math.sin(rot))
  const _yc = y0 + (hc * Math.sin(rot) + vc * Math.cos(rot))

  const offX = _xc - hc
  const offY = _yc - vc

  xfrm.setOffX(offX)
  xfrm.setOffY(offY)
  xfrm.setExtX(chExtX)
  xfrm.setExtY(chExtY)
  xfrm.setChExtX(chExtX)
  xfrm.setChExtY(chExtY)
  xfrm.setChOffX(0)
  xfrm.setChOffY(0)

  for (const sp of spTree) {
    const xfrm = sp.spPr?.xfrm!
    xfrm.setOffX(xfrm.offX! - minXForOffset)
    xfrm.setOffY(xfrm.offY! - minYForOffset)
  }
}

function getXfrmBounds(xfrm: {
  rot?: number
  offX: number
  offY: number
  extX: number
  extY: number
}): {
  minX: number
  minY: number
  maxX: number
  maxY: number
} {
  let minX, minY, maxX, maxY
  const rot = xfrm?.rot ?? 0
  if (isRotateIn45D(rot)) {
    minX = xfrm.offX
    minY = xfrm.offY
    maxX = xfrm.offX + xfrm.extX
    maxY = xfrm.offY + xfrm.extY
  } else {
    const xc = xfrm.offX + xfrm.extX * 0.5
    const yc = xfrm.offY + xfrm.extY * 0.5
    minX = xc - xfrm.extY * 0.5
    minY = yc - xfrm.extX * 0.5
    maxX = xc + xfrm.extY * 0.5
    maxY = yc + xfrm.extX * 0.5
  }

  return {
    minX,
    minY,
    maxX,
    maxY,
  }
}

export function calcGroupShapeRenderingRect(
  x: number,
  y: number,
  extX: number,
  extY: number
) {
  return {
    x: x - GroupShapeRenderingMargin,
    y: y - GroupShapeRenderingMargin,
    extX: extX + 2 * GroupShapeRenderingMargin,
    extY: extY + 2 * GroupShapeRenderingMargin,
  }
}
