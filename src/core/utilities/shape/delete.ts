import { Nullable } from '../../../../liber/pervasive'
import { SlideElement } from '../../SlideElement/type'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../common/utils'
import { Notes } from '../../Slide/Notes'
import { Slide } from '../../Slide/Slide'
import { isPlaceholder, isPlaceholderWithEmptyContent } from './asserts'
import { cloneSlideElement } from './clone'
import { getHierarchySps, getTextDocumentOfSp } from './getters'
import { InstanceType } from '../../instanceTypes'

export function deleteSlideElementfromParent(
  sp: Nullable<SlideElement>,
  isCheckPlaceholder?,
  notUpdateAnimation?
) {
  let pos
  const editContainer = sp?.parent as Slide | Notes
  if (
    !editContainer ||
    (editContainer.instanceType !== InstanceType.Slide &&
      editContainer.instanceType !== InstanceType.Notes)
  ) {
    return pos
  }

  if (sp != null && sp.parent && sp.parent.cSld && sp.parent.cSld.spTree) {
    const editContainer = sp.parent as Slide | Notes
    if (
      isCheckPlaceholder &&
      isPlaceholder(sp) &&
      !isPlaceholderWithEmptyContent(sp)
    ) {
      const placeholderSp = getHierarchySps(sp)[0]
      if (placeholderSp) {
        const dup = cloneSlideElement(placeholderSp)

        const s = turnOffRecordChanges()
        const doc_content = getTextDocumentOfSp(dup)
        if (doc_content) {
          doc_content.isSetToAll = true
          doc_content.remove(-1)
          doc_content.isSetToAll = false
        }
        restoreRecordChangesState(s)

        pos = (editContainer as Slide).replaceFromSpTreeById(sp.id, dup)
      } else {
        pos = editContainer.removeSpById(sp.id, notUpdateAnimation)
      }
    } else {
      pos = editContainer.removeSpById(sp.id, notUpdateAnimation)
    }
  }
  return pos
}
