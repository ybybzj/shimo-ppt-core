import { SlideElement } from '../../SlideElement/type'
import { Matrix2D } from '../../graphic/Matrix'
import { ShapeDrawerContext } from '../../graphic/type'

export type transformUpdater = (
  sp: SlideElement,
  g: ShapeDrawerContext,
  m: Matrix2D
) => Matrix2D
