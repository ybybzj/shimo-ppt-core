import { hookManager, Hooks } from '../../hooks'
import { EditorEvents } from '../../hooks/emit'
import { ParaPr } from '../../textAttributes/ParaPr'

export function goToSlide(slideIndex: number, isForce?) {
  hookManager.invoke(Hooks.EditorUI.OnGoToSlide, {
    slideIndex: slideIndex,
    isForce,
  })
}

export function emitEditorEvent(eventType: EditorEvents, ...args: any[]) {
  hookManager.invoke(Hooks.Emit.EmitEvent, {
    eventType,
    args,
  })
}

export function applyImageScrId() {
  hookManager.invoke(Hooks.EditorUI.OnApplyPropsWithImageSrcId)
}

export function syncSelectionState() {
  hookManager.invoke(Hooks.EditorUI.OnUpdateSelectionState)
}

export function updateParagraphProps(pPr: ParaPr) {
  hookManager.invoke(Hooks.EditorUI.OnUpdateParagraphProp, { paraPr: pPr })
}

export function updateTextPr(textPr) {
  hookManager.invoke(Hooks.EditorUI.UpdateTextPr, { textPr })
}

export function updateParagraphTab(defaultTab, paraTabs) {
  hookManager.invoke(Hooks.EditorUI.UpdateParaTab, {
    defaultTab,
    paraTabs,
  })
}
