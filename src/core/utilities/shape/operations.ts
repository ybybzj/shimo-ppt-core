import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { EditorSettings } from '../../common/EditorSettings'
import {
  calculateSizesForGraphicFrame,
  calculateTableForGraphicFrame,
} from '../../calculation/calculate/graphicFrame'
import { Xfrm } from '../../SlideElement/attrs/shapePrs'
import { isChartImage, isPlaceholder, isSlideElement } from './asserts'
import { checkExtentsByTextContent } from './extents'
import { checkTextFit } from './calcs'
import { setTransformCalcStatus } from '../../calculation/updateCalcStatus/utils'
import { ManagedSliceUtil } from '../../../common/managedArray'

export function canRotate(sp: SlideElement): boolean {
  if (EditorSettings.isViewMode) {
    return false
  }
  switch (sp.instanceType) {
    case InstanceType.Shape:
      return true
    case InstanceType.ImageShape:
      if (sp.isOleObject() || isChartImage(sp)) {
        return false
      }

      return true
    case InstanceType.GroupShape:
      for (let i = 0; i < sp.spTree.length; ++i) {
        const _sp = sp.spTree[i]
        if (!canRotate(_sp)) return false
      }
      return true
    case InstanceType.GraphicFrame:
      return false
  }
}

export function canResize(sp: SlideElement): boolean {
  if (EditorSettings.isViewMode) {
    return false
  }
  switch (sp.instanceType) {
    case InstanceType.Shape:
      return true
    case InstanceType.ImageShape:
      return true
    case InstanceType.GroupShape:
      for (let i = 0; i < sp.spTree.length; ++i) {
        const _sp = sp.spTree[i]
        if (!canResize(_sp)) return false
      }
      return true
    case InstanceType.GraphicFrame:
      return true
  }
}

export function canSlideElementGroup(sp: SlideElement): boolean {
  if (EditorSettings.isViewMode) {
    return false
  }
  switch (sp.instanceType) {
    case InstanceType.Shape:
      return !isPlaceholder(sp)
    case InstanceType.ImageShape:
      return true
    case InstanceType.GroupShape:
      return true
    case InstanceType.GraphicFrame:
      return false
    default:
      return false
  }
}

function toNum(x: Nullable<number>): number {
  return x != null ? x : 0
}

/** @note placeholder 等没有 xfrm 的情况, 会在 calculateLoaclTransform 内单独处理 */

export function changeSlideElementSize(
  sp: SlideElement,
  kw: number,
  kh: number,
  deltaX?: number,
  deltaY?: number,
  forceChangeText = false
) {
  const needCheckExtents = _changeSlideElementSize(
    sp,
    kw,
    kh,
    deltaX,
    deltaY,
    forceChangeText
  )
  if (needCheckExtents) {
    checkExtentsByTextContent(sp)
  }
}

function _changeSlideElementSize(
  sp: SlideElement,
  kw: number,
  kh: number,
  deltaX?: number,
  deltaY?: number,
  forceChangeText = false
): boolean {
  if (!isSlideElement(sp)) return false

  let needCheckExtents = false

  switch (sp.instanceType) {
    case InstanceType.Shape:
    case InstanceType.ImageShape: {
      if (sp.spPr && sp.spPr.xfrm) {
        const xfrm = sp.spPr.xfrm as Xfrm
        if (xfrm.isValid()) {
          xfrm.setOffX(xfrm.offX! * kw + toNum(deltaX))
          xfrm.setOffY(xfrm.offY! * kh + toNum(deltaY))
          xfrm.setExtX(xfrm.extX! * kw)
          xfrm.setExtY(xfrm.extY! * kh)
        }
      }
      if (
        sp.instanceType === InstanceType.Shape &&
        (forceChangeText || isChangeFontSize(sp))
      ) {
        const textScale = Math.min(kw, kh)
        checkTextFit(sp, textScale)
        needCheckExtents = true
      }
      break
    }
    case InstanceType.GroupShape: {
      if (sp.spPr && sp.spPr.xfrm) {
        const spTree = sp.spTree
        for (let i = 0; i < spTree.length; ++i) {
          const _needCheckExtents = _changeSlideElementSize(
            spTree[i],
            kw,
            kh,
            undefined,
            undefined,
            forceChangeText
          )
          if (_needCheckExtents) {
            needCheckExtents = true
          }
        }
        const xfrm = sp.spPr.xfrm as Xfrm
        if (xfrm.isNotEmptyForGroup()) {
          xfrm.setOffX(xfrm.offX! * kw + toNum(deltaX))
          xfrm.setOffY(xfrm.offY! * kh + toNum(deltaY))
          xfrm.setExtX(xfrm.extX! * kw)
          xfrm.setExtY(xfrm.extY! * kh)
          xfrm.setChExtX(xfrm.chExtX! * kw)
          xfrm.setChExtY(xfrm.chExtY! * kh)
          xfrm.setChOffX(xfrm.chOffX! * kw)
          xfrm.setChOffY(xfrm.chOffY! * kh)
        }
      }
      break
    }
    case InstanceType.GraphicFrame: {
      const table = sp.graphicObject
      if (table) {
        const newGrids = table.tableGrid.map((x) => x * kw)
        table.setTableGrid(newGrids)
        ManagedSliceUtil.forEach(table.children, (row) => {
          if (row.pr.height) {
            row.pr.height.value = (row.pr.height.value ?? 0) * kw
          }
          ManagedSliceUtil.forEach(row.children, (cell) => {
            cell.pr.margins = undefined
            checkTextFit(cell, kw)
          })
        })
      }
      if (sp.spPr && sp.spPr.xfrm) {
        const xfrm = sp.spPr.xfrm as Xfrm
        if (xfrm.isValid()) {
          xfrm.setOffX(xfrm.offX! * kw + toNum(deltaX))
          xfrm.setOffY(xfrm.offY! * kh + toNum(deltaY))
          const resizeX = xfrm.extX! * kw
          const resizeY = xfrm.extY! * kh
          // copy 时 table 没有 rowInfo 需要重新计算
          calculateTableForGraphicFrame(sp)
          table!.resize(resizeX, resizeY)
        }
      }
      calculateTableForGraphicFrame(sp)
      calculateSizesForGraphicFrame(sp)
      break
    }
  }
  setTransformCalcStatus(sp)

  return needCheckExtents
}

function isChangeFontSize(sp: SlideElement) {
  const hasXfrm = !!sp.spPr?.xfrm
  const instanceTypeOfParent = sp.parent?.instanceType

  const isValidShape =
    instanceTypeOfParent === InstanceType.Slide ||
    instanceTypeOfParent === InstanceType.SlideMaster ||
    instanceTypeOfParent === InstanceType.SlideLayout

  return hasXfrm ? isValidShape : isValidShape && isPlaceholder(sp)
}
