import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { BodyPr } from '../../SlideElement/attrs/bodyPr'
import { BlipFill } from '../../SlideElement/attrs/fill/blipFill'
import { GrpFill, NoFill } from '../../SlideElement/attrs/fill/otherFills'
import { GradFill } from '../../SlideElement/attrs/fill/GradFill'
import { PatternFill } from '../../SlideElement/attrs/fill/PatternFill'
import { RelativeRect } from '../../SlideElement/attrs/fill/RelativeRect'
import { SolidFill } from '../../SlideElement/attrs/fill/SolidFill'
import { SpPr, Xfrm } from '../../SlideElement/attrs/shapePrs'
import { TextAutoFitType, TextWarpType } from '../../SlideElement/const'

export type FillEffectType =
  | BlipFill
  | NoFill
  | SolidFill
  | GradFill
  | PatternFill
  | GrpFill

export function getRasterImageIdFromFill(
  fill: Nullable<FillEffectType>
): Nullable<string> {
  if (fill && fill.instanceType === InstanceType.BlipFill) {
    return fill.imageSrcId
  }
  return null
}

export function getSrcRectFromFill(
  fill: Nullable<FillEffectType>
): Nullable<RelativeRect> {
  if (fill && fill.instanceType === InstanceType.BlipFill) {
    return fill.srcRect
  }
  return null
}

export function getFillRectFromFill(
  fill: Nullable<FillEffectType>
): Nullable<RelativeRect> {
  if (fill && fill.instanceType === InstanceType.BlipFill) {
    return fill.fillRect
  }
  return null
}
export function checkShapeBodyAutoFitReset(shape: SlideElement) {
  if (shape?.instanceType === InstanceType.Shape) {
    if (!shape.txBody) return
    const bodyPr = shape.getBodyPr()
    if (bodyPr.textFit?.type === TextAutoFitType.SHAPE) {
      const props = shape.txBody.bodyPr?.clone() ?? new BodyPr()
      if (bodyPr.wrap === TextWarpType.NONE) {
        props.wrap = TextWarpType.SQUARE
      }
      shape.txBody.setBodyPr(props)
    }
  }
}

export function ensureSpPrXfrm(sp: SlideElement, isNoResetAutofit?) {
  if (!sp) return
  if (!sp.spPr) {
    sp!.setSpPr(new SpPr())
    sp.spPr!.setParent(sp)
  }
  if (!sp.spPr!.xfrm) {
    sp.spPr!.setXfrm(new Xfrm(sp.spPr))
    sp.spPr!.xfrm!.setParent(sp.spPr)
    if (sp.group) {
      sp.spPr!.xfrm!.setOffX(0)
      sp.spPr!.xfrm!.setOffY(0)
    } else {
      sp.spPr!.xfrm!.setOffX(sp.x)
      sp.spPr!.xfrm!.setOffY(sp.y)
    }

    sp.spPr!.xfrm!.setExtX(sp.extX)
    sp.spPr!.xfrm!.setExtY(sp.extY)
    sp.spPr!.xfrm!.setRot(sp.rot)
    if (isNoResetAutofit !== true) {
      checkShapeBodyAutoFitReset(sp)
    }
  }
}
