import { EditorUtil } from '../../../globals/editor'
import { EditActionFlag } from '../../EditActionFlag'
import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { TextDocument } from '../../TextDocument/TextDocument'
import { LineEndType, LineEndWidthTypes } from '../../SlideElement/const'
import { createGeometry } from '../../SlideElement/geometry/creators'

import { TextBody } from '../../SlideElement/TextBody'
import { isSlideElement } from './asserts'
import { checkExtentsByTextContent } from './extents'
import { BodyPr } from '../../SlideElement/attrs/bodyPr'
import {
  FillEffects,
  validateFillMods,
} from '../../SlideElement/attrs/fill/fill'
import { Ln, LnOptions } from '../../SlideElement/attrs/line'
import { informEntityPropertyChange } from '../../calculation/informChanges/propertyChange'
import { calculatePen } from '../../calculation/calculate/utils'
import { isNumber } from '../../../common/utils'
import { getCNvProps } from './getters'
import { SpPr, Xfrm } from '../../SlideElement/attrs/shapePrs'
import { SlideElementParent } from '../../Slide/type'
import { CalcStatus } from '../../calculation/updateCalcStatus/calcStatus'
import { isTransformCalcStatusSet } from '../../calculation/updateCalcStatus/utils'
import { ThemeColor } from '../../../io/dataType/spAttrs'
import { Nullable } from '../../../../liber/pervasive'
import { ShapeStyle } from '../../SlideElement/attrs/ShapeStyle'
import { ScaleOfPPTXSizes } from '../../common/const/unit'
import { StrokeType } from '../../common/const/attrs'
import { StrokeOptions } from '../../properties/options'
import { ParaPr } from '../../textAttributes/ParaPr'

export function addToParentSpTree(sp: SlideElement, index?: number) {
  if (sp.parent?.instanceType === InstanceType.Slide) {
    sp.parent.addSlideElementAt(index, sp)
  }
}

export function setGroup(sp: SlideElement, group) {
  let historyFlag
  switch (sp.instanceType) {
    case InstanceType.Shape:
      historyFlag = EditActionFlag.edit_ShapeSetGroup
      break
    case InstanceType.GroupShape:
      historyFlag = EditActionFlag.edit_GroupShapeSetGroup
      break
    case InstanceType.ImageShape:
      historyFlag = EditActionFlag.edit_ImageShapeSetGroup
      break
    case InstanceType.GraphicFrame:
      historyFlag = EditActionFlag.edit_GraphicFrameSetSetGroup
      break
  }
  if (historyFlag != null) {
    informEntityPropertyChange(sp, historyFlag, sp.group, group)
    sp.group = group
  }
}

export function setParentForSlideElement(
  sp: SlideElement,
  parent: SlideElementParent
) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      sp.setParent(parent)
      break
    case InstanceType.GraphicFrame:
      sp.setParent(parent)
      break
    case InstanceType.ImageShape:
      sp.setParent(parent)
      break
    case InstanceType.GroupShape:
      sp.setParent(parent)
      const spTree = sp.spTree
      if (Array.isArray(spTree)) {
        for (let i = 0; i < spTree.length; ++i) {
          setParentForSlideElement(spTree[i], parent)
        }
      }
      break
  }
}

export function changePresetOfGeometry(sp: SlideElement, preset: string) {
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      if (preset === 'textRect') {
        sp.spPr!.setGeometry(createGeometry('rect'))
        sp.setStyle(ShapeStyle.TextRectDefault())
        const fill = FillEffects.SolidScheme(ThemeColor.Light1)

        sp.spPr!.setFill(fill as FillEffects)

        const ln = Ln.new({
          w: 6350,
          fillEffects: FillEffects.SolidPrst('black'),
        })

        sp.spPr!.setLn(ln as Ln)

        if (!sp.txBody) {
          resetTxBodyForShape(sp, BodyPr.Default())
        }
        return
      }
      let resultGeomPreset
      let spLine: Nullable<Ln>

      if (sp.spPr!.ln == null) {
        spLine = null
      } else {
        spLine = sp.spPr!.ln.clone()
      }

      let opts: LnOptions
      switch (preset) {
        case 'lineWithArrow': {
          resultGeomPreset = 'line'
          opts = {
            tailEnd: {
              type: LineEndType.Arrow,
              len: LineEndWidthTypes.Mid,
              w: LineEndWidthTypes.Mid,
            },
          }

          break
        }
        case 'lineWithTwoArrows': {
          resultGeomPreset = 'line'
          opts = {
            tailEnd: {
              type: LineEndType.Arrow,
              len: LineEndWidthTypes.Mid,
              w: LineEndWidthTypes.Mid,
            },
            headEnd: {
              type: LineEndType.Arrow,
              len: LineEndWidthTypes.Mid,
              w: LineEndWidthTypes.Mid,
            },
          }

          break
        }
        case 'bentConnector5WithArrow': {
          resultGeomPreset = 'bentConnector5'
          opts = {
            tailEnd: {
              type: LineEndType.Arrow,
              len: LineEndWidthTypes.Mid,
              w: LineEndWidthTypes.Mid,
            },
          }

          break
        }
        case 'bentConnector5WithTwoArrows': {
          resultGeomPreset = 'bentConnector5'
          opts = {
            tailEnd: {
              type: LineEndType.Arrow,
              len: LineEndWidthTypes.Mid,
              w: LineEndWidthTypes.Mid,
            },
            headEnd: {
              type: LineEndType.Arrow,
              len: LineEndWidthTypes.Mid,
              w: LineEndWidthTypes.Mid,
            },
          }

          break
        }
        case 'curvedConnector3WithArrow': {
          resultGeomPreset = 'curvedConnector3'
          opts = {
            tailEnd: {
              type: LineEndType.Arrow,
              len: LineEndWidthTypes.Mid,
              w: LineEndWidthTypes.Mid,
            },
          }

          break
        }
        case 'curvedConnector3WithTwoArrows': {
          resultGeomPreset = 'curvedConnector3'
          opts = {
            tailEnd: {
              type: LineEndType.Arrow,
              len: LineEndWidthTypes.Mid,
              w: LineEndWidthTypes.Mid,
            },
            headEnd: {
              type: LineEndType.Arrow,
              len: LineEndWidthTypes.Mid,
              w: LineEndWidthTypes.Mid,
            },
          }

          break
        }
        default: {
          resultGeomPreset = preset
          opts = {
            tailEnd: null,
            headEnd: null,
          }

          break
        }
      }
      const newLine = Ln.fromOptions(spLine, opts)
      if (resultGeomPreset != null) {
        sp.spPr!.setGeometry(createGeometry(resultGeomPreset))
      } else {
        sp.spPr!.setGeometry(undefined)
      }
      checkExtentsByTextContent(sp)
      if (
        (!sp.brush || !sp.brush.fill) &&
        (!sp.pen || !sp.pen.fillEffects || !sp.pen.fillEffects.fill)
      ) {
        const ln = Ln.new({
          fillEffects: FillEffects.SolidScheme(ThemeColor.Accent1),
        })
        ln.merge(newLine)
        sp.spPr!.setLn(ln)
      } else sp.spPr!.setLn(newLine)
      break
    }
    case InstanceType.ImageShape: {
      if (preset === 'textRect') {
        return
      }
      sp.spPr!.setGeometry(createGeometry(preset))
      break
    }
    case InstanceType.GroupShape: {
      for (const _sp of sp.spTree) {
        changePresetOfGeometry(_sp, preset)
      }
      break
    }
  }
}

export function onSlideElemntRemoved(sp: SlideElement) {
  sp.spTreeParent = undefined
  EditorUtil.entityRegistry.removeEntityById(sp.id)
}

export function setDeletedForSp(sp: SlideElement, isDeleted: boolean) {
  if (isSlideElement(sp) === false) {
    return
  }
  informEntityPropertyChange(
    sp,
    EditActionFlag.edit_ShapeSetDeleted,
    sp.isDeleted,
    isDeleted
  )
  _setDeleted(sp, isDeleted)
}

function _setDeleted(sp: SlideElement, isDeleted: boolean) {
  sp.isDeleted = isDeleted
  if (sp.instanceType === InstanceType.GroupShape) {
    for (let i = 0; i < sp.spTree.length; ++i) {
      const ssp = sp.spTree[i]
      _setDeleted(ssp, isDeleted)
    }
  }
}

export function clearSlideElementContent(sp: SlideElement) {
  if (sp.instanceType === InstanceType.Shape) {
    sp.clearContent()
  }
}

/** 修改边框 */
function correctStroke(
  options: Nullable<StrokeOptions>,
  stroke: Nullable<Ln>
): Ln {
  if (null == options) return stroke!

  const opts: LnOptions = {}

  const type = options.type
  const w = options.width

  if (w != null) opts.w = w * ScaleOfPPTXSizes

  const color = options.color
  if (type === StrokeType.NONE) {
    opts.fillEffects = FillEffects.NoFill()
  } else if (type != null) {
    if (null != color && null != color) {
      opts.fillEffects = FillEffects.SolidComputed(color)
    }
  }

  const join = options.lineJoin
  if (null != join) {
    opts.Join = { type: join }
  }

  const cap = options.lineCap
  if (null != cap) {
    opts.cap = cap
  }

  const beginOfLineEndType = options.beginOfLineEndType
  if (null != beginOfLineEndType) {
    opts.headEnd = {
      type: beginOfLineEndType,
    }
  }

  const endOfLineEndType = options.endOfLineEndType
  if (null != endOfLineEndType) {
    opts.tailEnd = {
      type: endOfLineEndType,
    }
  }

  const beginOfLineEndWidth = options.beginOfLineEndWidth
  if (null != beginOfLineEndWidth) {
    opts.headEnd = {
      w: 2 - ((beginOfLineEndWidth / 3) >> 0),
      len: 2 - (beginOfLineEndWidth % 3),
    }
  }

  const endOflineEndWidth = options.endOflineEndWidth
  if (null != endOflineEndWidth) {
    opts.tailEnd = {
      w: 2 - ((endOflineEndWidth / 3) >> 0),
      len: 2 - (endOflineEndWidth % 3),
    }
  }

  if (isNumber(options.prstDash)) {
    opts.prstDash = options.prstDash
  }
  return Ln.fromOptions(stroke, opts)
}

export function setLineForSlideElement(sp: SlideElement, line: StrokeOptions) {
  if (!isSlideElement(sp)) {
    return
  }
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      if (sp.isCalcStatusSet(CalcStatus.Shape.Pen)) {
        calculatePen(sp)
      }
      const stroke = correctStroke(line, sp.pen)
      if (stroke.fillEffects) {
        validateFillMods(stroke.fillEffects)
      }
      sp.spPr!.setLn(stroke)
      break
    }
    case InstanceType.ImageShape: {
      if (sp.isCalcStatusSet(CalcStatus.ImageShape.Pen)) {
        calculatePen(sp)
      }
      const stroke = correctStroke(line, sp.pen)
      // 不传宽度的情况，line的宽度使用之前宽度 / 2 （reference: calculatePen里面）
      if (line.width == null && isNumber(stroke.w)) {
        stroke.w /= 2
      }
      if (stroke.fillEffects) {
        validateFillMods(stroke.fillEffects)
      }
      sp.spPr?.setLn(stroke)
      break
    }
    case InstanceType.GraphicFrame: {
      break
    }
    case InstanceType.GroupShape: {
      for (const childSp of sp.spTree) {
        setLineForSlideElement(childSp, line)
      }
      break
    }
  }
}

export function setTitle(sp: SlideElement, title) {
  if (null == title) {
    return
  }
  const nvPr = getCNvProps(sp)
  if (nvPr) {
    nvPr.setTitle(title ? title : null)
  }
}

export function setDescription(sp: SlideElement, description) {
  if (null == description) {
    return
  }
  const nvPr = getCNvProps(sp)
  if (nvPr) {
    nvPr.setDescr(description ? description : null)
  }
}

export function updateSpPrXfrm(sp: SlideElement) {
  if (isTransformCalcStatusSet(sp)) {
    return
  }
  if (!sp.spPr) {
    sp.setSpPr(new SpPr())
    sp.spPr!.setParent(sp)
  }
  if (!sp.spPr!.xfrm) {
    sp.spPr!.setXfrm(new Xfrm(sp.spPr))
    sp.spPr!.xfrm!.setParent(sp.spPr)
  }
  sp.spPr!.xfrm!.setOffX(sp.x)
  sp.spPr!.xfrm!.setOffY(sp.y)
  sp.spPr!.xfrm!.setExtX(sp.extX)
  sp.spPr!.xfrm!.setExtY(sp.extY)
}

export function resetTxBodyForShape(
  shape: SlideElement,
  bodyPr?: BodyPr,
  paraPr?: ParaPr
) {
  if (shape.instanceType !== InstanceType.Shape) return shape
  const txBody = new TextBody(shape)
  const textDoc = TextDocument.new(txBody, 0, 0, 0, 0)
  // update textDoc
  if (paraPr != null) {
    const para = textDoc.children.at(0)!
    const pPr = paraPr.clone()
    pPr.defaultRunPr = undefined
    para.setPr(pPr)
    const rPr = paraPr.defaultRunPr
    if (rPr) {
      para.setTextPr(rPr)
    }
  }
  // update txBody
  txBody.setContent(textDoc)
  if (bodyPr != null) {
    txBody.setBodyPr(bodyPr)
  }

  //update shape
  shape.setTxBody(txBody)

  return shape
}
