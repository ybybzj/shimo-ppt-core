import { TextDocument } from '../TextDocument/TextDocument'
import { Table } from '../Table/Table'
import { Slide } from '../Slide/Slide'
import { SlideLayout } from '../Slide/SlideLayout'
import { SlideMaster } from '../Slide/SlideMaster'
export type TableOrTextDocument = Table | TextDocument
export type TextOperationDirection = 1 | -1
export type SlideElementContainer = Slide | SlideLayout | SlideMaster
