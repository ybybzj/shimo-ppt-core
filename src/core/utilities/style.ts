import { TextPr } from '../textAttributes/TextPr'
import { Theme } from '../SlideElement/attrs/theme'

export function updateTextPrFonts(textPr: TextPr, theme: Theme) {
  if (theme && theme.themeElements && theme.themeElements.fontScheme) {
    const fontScheme = theme.themeElements.fontScheme
    if (textPr.fontFamily) {
      textPr.fontFamily.name = fontScheme.checkFont(textPr.fontFamily.name)
    }
    if (textPr.rFonts) {
      if (textPr.rFonts.ascii) {
        textPr.rFonts.ascii.name = fontScheme.checkFont(
          textPr.rFonts.ascii.name
        )
      }
      if (textPr.rFonts.eastAsia) {
        textPr.rFonts.eastAsia.name = fontScheme.checkFont(
          textPr.rFonts.eastAsia.name
        )
      }
      if (textPr.rFonts.hAnsi) {
        textPr.rFonts.hAnsi.name = fontScheme.checkFont(
          textPr.rFonts.hAnsi.name
        )
      }
      if (textPr.rFonts.cs) {
        textPr.rFonts.cs.name = fontScheme.checkFont(textPr.rFonts.cs.name)
      }
    }
  }
  return textPr
}
