import { Nullable } from '../../../liber/pervasive'
import { InstanceType } from '../instanceTypes'
import { SlideElement } from '../SlideElement/type'
import { EntityObject } from '../changes/type'
import { Presentation } from '../Slide/Presentation'
import { Slide } from '../Slide/Slide'
import { SlideLayout } from '../Slide/SlideLayout'
import { SlideMaster } from '../Slide/SlideMaster'

export function getParent(
  targetClass: EntityObject,
  _presentation?: Presentation
): undefined | EntityObject {
  switch (targetClass.instanceType) {
    case InstanceType.Slide:
    case InstanceType.SlideMaster:
      const target = targetClass as Slide | SlideMaster
      return target.presentation
    case InstanceType.SlideLayout:
      const layout = targetClass as SlideLayout
      return layout.master || layout.presentation
    case InstanceType.SlideComments:
    case InstanceType.Notes:
      return targetClass.slide
    case InstanceType.ParaRun:
      return targetClass.Paragraph
    case InstanceType.Shape:
    case InstanceType.GraphicFrame:
    case InstanceType.GroupShape:
    case InstanceType.ImageShape:
      return targetClass.spTreeParent || targetClass.group
    case InstanceType.Comment:
      return targetClass.slideComments
    case InstanceType.SlideAnimation:
      return targetClass.slide
    case InstanceType.Presentation:
      return undefined
    default:
      return (
        (targetClass.getParent && targetClass.getParent()) || targetClass.parent
      )
  }
}

export function findClosestParent(
  targetClass: EntityObject,
  finder: (parent: EntityObject) => boolean,
  presentation?: Presentation
): EntityObject | undefined {
  let targetParent = getParent(targetClass, presentation)
  let result: EntityObject | undefined
  while (targetParent) {
    if (finder(targetParent)) {
      result = targetParent
      break
    }

    const upParent = getParent(targetParent, presentation)
    if (upParent === targetParent) {
      console.error(
        '%c [getParentIds]invalid parent!',
        'color: #FFCD3A',
        targetClass
      )
      break
    }
    targetParent = upParent
  }
  return result
}

export function getPresentation(target: EntityObject): Nullable<Presentation> {
  return findClosestParent(
    target,
    (parent) => parent.instanceType === InstanceType.Presentation
  ) as Nullable<Presentation>
}

export function findParentSlide(target: SlideElement): Nullable<Slide> {
  return findClosestParent(
    target,
    (parent) => parent.instanceType === InstanceType.Slide
  ) as Nullable<Slide>
}
