import { find } from '../../../liber/l/find'
import { Dict, Nullable } from '../../../liber/pervasive'
import { CmContentData, CommentsAuthorData } from '../../io/dataType/comment'
import {
  addCommentAuthor,
  getCommentAuthorLastId,
  increaseCommentAuthorLastId,
} from '../Slide/Comments/GlobalCommentId'
import { SlideComments } from '../Slide/Comments/SlideComments'

export interface LocalInputCommentData {
  origin: 'local'
  authorId: number
  authorName: string
  text: string
  parentId?: string // parent's uid
  parentAuthorId?: number
}
export interface ServerInputCommentData {
  origin: 'server'
  id: string
  textExt: Dict
}

export type InputCommentData = LocalInputCommentData | ServerInputCommentData

export function toServerCmContentData(
  data: ServerInputCommentData
): CmContentData {
  const cmContentData: CmContentData = {
    origin: 'server',
    uid: data['id'],
    // pos 在实现层组装
    pos: { x: 0, y: 0 },
    textExt: data['textExt'],
  }
  return cmContentData
}

export function toLocalCmContentData(
  data: LocalInputCommentData,
  slideComments: Nullable<SlideComments>
): CmContentData {
  const authorId = data ['authorId']
  let authorLastidx = getCommentAuthorLastId(authorId)
  if (authorLastidx == null) {
    const authorData: CommentsAuthorData = {
      id: authorId,
      name: data['authorName'],
      initials: '',
      lastidx: 1,
    }
    addCommentAuthor(authorData)
    authorLastidx = 1
  } else {
    authorLastidx = increaseCommentAuthorLastId(authorId)!
  }
  const cmContentData: CmContentData = {
    origin: 'local',
    authorId: authorId,
    idx: authorLastidx,
    uid: '', // 由authorId 及 idx 组合生成
    pos: { x: 0, y: 0 }, // 内部组装
    text: data['text'],
  }

  const comments = slideComments?.comments ?? []
  const parentId = data['parentId']
  if (parentId && comments.length > 0) {
    const parentComment = find((cm) => cm.getUid() === parentId, comments)
    if (parentComment) {
      cmContentData.parentCommentId = parentComment.getIdx()
      cmContentData.parentAuthorId = parentComment.getAuthorId()
    }
  }
  return cmContentData
}
