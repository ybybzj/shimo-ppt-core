import { EditActionFlag } from '../../EditActionFlag'

import { Presentation } from '../../Slide/Presentation'
import { hookManager, Hooks } from '../../hooks'

import { HightLight_None } from '../../common/const/unit'
import { computedRGBColorFromStr } from '../../color/ComputedRGBAColor'
import { EditorUtil } from '../../../globals/editor'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { startEditAction } from '../../calculation/informChanges/startAction'
import { EditorSettings } from '../../common/EditorSettings'
import { CComplexColor } from '../../color/complexColor'
import { TextPrOptions } from '../../textAttributes/TextPr'

export function setFont(presentation: Presentation, name: string): void {
  startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
  const textPr: TextPrOptions = {
    fontFamily: {
      name: name,
      index: -1,
    },
  }
  presentation.applyTextPrToParagraph(textPr, false)

  EditorUtil.FontKit.ensureHBFont(textPr, undefined, () => {
    hookManager.invoke(Hooks.Editor.OnRefreshPresentation, {
      isDirty: true,
    })
  })
}

export function setFontSize(presentation: Presentation, size: number): void {
  startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
  if (size <= 1) {
    size = 1
  }
  presentation.applyTextPrToParagraph({ fontSize: size }, false)

  // for the mobile version it is important
  if (EditorSettings.isMobile) {
    presentation.syncEditorUIState()
    hookManager.invoke(Hooks.EditorUI.OnUpdateLayouts, true)
  }
}

export function setBold(presentation: Presentation, value: boolean): void {
  startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
  presentation.applyTextPrToParagraph({ bold: value }, false)
}

export function setItalic(presentation: Presentation, value: boolean): void {
  startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
  presentation.applyTextPrToParagraph({ italic: value }, false)
}

export function setUnderline(presentation: Presentation, value: boolean): void {
  startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
  presentation.applyTextPrToParagraph({ underline: value }, false)
}

export function setStrikeout(presentation: Presentation, value: boolean): void {
  startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
  presentation.applyTextPrToParagraph(
    {
      strikeout: value,
    },
    false
  )
}

// Specifies the amount of vertical spacing between lines of text within the paragraph.
export function setLineSpacing(
  presentation: Presentation,
  type: number,
  value: number
): void {
  presentation.setSpacingForParagraph({
    lineRule: type,
    line: value,
  })
}

//  Specifies the spacing that should be added after the last line or before the first line of the paragraph.
export function setLineSpacingBeforeAfter(
  presentation: Presentation,
  type: number,
  value: number
): void {
  switch (type) {
    case 0:
      presentation.setSpacingForParagraph({ before: value })
      break
    case 1:
      presentation.setSpacingForParagraph({ after: value })
      break
  }
}

export function setFontSizeIn(presentation: Presentation): void {
  presentation.changeFontSize(true)
}

export function setFontSizeOut(presentation: Presentation): void {
  presentation.changeFontSize(false)
}

export function setHighlight(
  presentation: Presentation,
  isHighlight: boolean,
  color?: string
) {
  startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)

  if (!isHighlight || !color) {
    presentation.applyTextPrToParagraph({ highLight: HightLight_None })
  } else {
    const { r, g, b } = computedRGBColorFromStr(color)
    const ccolor = CComplexColor.RGBA(r, g, b)
    ccolor.check(null, null)
    presentation.applyTextPrToParagraph({
      highLight: ccolor,
    })
  }
  presentation.syncEditorUIState()
}

export function setTextColor(presentation: Presentation, color: string) {
  startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
  const fillEffects = FillEffects.SolidComputed(computedRGBColorFromStr(color))

  presentation.applyTextPrToParagraph({ fillEffects: fillEffects }, false)
  presentation.syncEditorUIState()
}

export function setTextColorByRGB(
  presentation: Presentation,
  r: number,
  g: number,
  b: number
) {
  startEditAction(EditActionFlag.action_Presentation_ParagraphAdd)
  presentation.applyTextPrToParagraph(
    {
      fillEffects: FillEffects.SolidRGBA(r, g, b),
    },
    false
  )
}
