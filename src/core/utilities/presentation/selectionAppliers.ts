import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import { SlideElement, TextSlideElement } from '../../SlideElement/type'

import { SelectionState } from '../../Slide/SelectionState'
import { GraphicFrame } from '../../SlideElement/GraphicFrame'
import { Shape } from '../../SlideElement/Shape'
import { checkExtentsByTextContent } from '../shape/extents'
import { getTextDocumentOfSp, getGeomPreset } from '../shape/getters'
import {
  moveCursorToEndPosInTextDocument,
  moveCursorToStartPosInTable,
  moveCursorToStartPosInTextDocument,
} from '../cursor/moveToEdgePos'
import {
  addNewParagraphInTextDocument,
  addToParagraphInTextDocument,
  applyTextPrToParagraphInTextDocument,
  pasteFormattingForTextDocument,
  TextDocumentOperationArgs,
  TextDocumentOperationFns,
} from '../../TextDocument/utils/operations'
import { Table } from '../../Table/Table'
import { TextDocument } from '../../TextDocument/TextDocument'
import { isLinePreset } from '../shape/asserts'

export function applyTextDocumentOperationFunction<
  T extends TextDocumentOperationFns,
>(
  target: Nullable<SelectionState>,
  opFn: T,
  args: TextDocumentOperationArgs<T>
) {
  if (target == null) {
    return
  }
  const selectionStatusInfo = target.selectionStatusInfo

  switch (selectionStatusInfo.status) {
    case 'group_text':
    case 'root_text': {
      applyOperationFunctionToTextSlideElement(
        selectionStatusInfo.selectedTextSp,
        opFn,
        args
      )
      break
    }
    case 'group_collection':
    case 'root_collection': {
      applyOperationFunctionToSelectedElements(
        selectionStatusInfo.selectedElements,
        opFn,
        args
      )
      break
    }
  }
}

function applyOperationFunctionToTextSlideElement<
  T extends TextDocumentOperationFns,
>(sp: TextSlideElement, opFn: T, args: TextDocumentOperationArgs<T>) {
  if (sp.instanceType !== InstanceType.GraphicFrame) {
    applyOperationFnForTextDoc(getTextDocumentOfSp(sp)!, opFn, args)
    checkExtentsByTextContent(sp)
  } else {
    applyOperationFnForTable(sp.graphicObject!, opFn, args)
  }
}

function applyOperationFunctionToSelectedElements<
  T extends TextDocumentOperationFns,
>(
  selectedElements: SlideElement[],
  opFn: T,
  args: TextDocumentOperationArgs<T>
) {
  if (selectedElements.length > 0) {
    applyToSlideElements(selectedElements, opFn, args)
  }
}

function applyToSlideElements<T extends TextDocumentOperationFns>(
  arr: SlideElement[],
  fn: T,
  args: TextDocumentOperationArgs<T>
) {
  if (arr == null || arr.length <= 0) {
    return
  }
  let ret = false
  for (let i = 0; i < arr.length; ++i) {
    const sp = arr[i] as SlideElement
    if (!sp) {
      continue
    }
    switch (sp.instanceType) {
      case InstanceType.GroupShape: {
        const retForGroup = applyToSlideElements(
          sp.slideElementsInGroup,
          fn,
          args
        )
        if (retForGroup) {
          ret = true
        }
        break
      }
      case InstanceType.GraphicFrame: {
        sp.graphicObject!.isSetToAll = true
        applyOperationFnForTable(sp.graphicObject!, fn, args)
        sp.graphicObject!.isSetToAll = false
        ret = true
        break
      }
      case InstanceType.Shape: {
        let textDoc = getTextDocumentOfSp(sp)
        if (!textDoc) {
          sp.createTxBody()
          textDoc = getTextDocumentOfSp(sp)!
        }

        if (textDoc) {
          textDoc.isSetToAll = true
          applyOperationFnForTextDoc(textDoc, fn, args)
          textDoc.isSetToAll = false
          ret = true
        }
        break
      }
    }
    checkExtentsByTextContent(sp)
  }
  return ret
}

export function applyTextOperationFunction<T extends TextDocumentOperationFns>(
  target: Nullable<SelectionState>,
  fn: T,
  args: TextDocumentOperationArgs<T>
) {
  if (target == null) {
    return
  }

  const selectionStatusInfo = target.selectionStatusInfo

  switch (selectionStatusInfo.status) {
    case 'group_text':
    case 'root_text': {
      appplyTextFunctionToTextSlideElement(
        selectionStatusInfo.selectedTextSp,
        fn,
        args
      )
      break
    }
    case 'group_collection':
    case 'root_collection': {
      if (
        fn === applyTextPrToParagraphInTextDocument ||
        fn === pasteFormattingForTextDocument
      ) {
        applyTextDocumentOperationFunction(target, fn, args)
      } else {
        const selectedElements = selectionStatusInfo.selectedElements
        if (
          selectedElements &&
          selectedElements.length === 1 &&
          ((selectedElements[0].instanceType === InstanceType.Shape &&
            !isLinePreset(getGeomPreset(selectedElements[0]))) ||
            selectedElements[0].instanceType === InstanceType.GraphicFrame)
        ) {
          const newTextSelection = selectedElements[0]
          if (newTextSelection.instanceType === InstanceType.GraphicFrame) {
            moveCursorToStartPosInTable(newTextSelection.graphicObject!, false)
          } else {
            const textDoc = getTextDocumentOfSp(newTextSelection)
            if (textDoc) {
              moveCursorToEndPosInTextDocument(textDoc, false)
            }
          }

          appplyTextFunctionToTextSlideElement(newTextSelection, fn, args)

          target.setTextSelection(newTextSelection)
        }
      }
      break
    }
  }
}
function applyTextOperationFunctionToShape<T extends TextDocumentOperationFns>(
  shape: Shape,
  opFn: T,
  args: TextDocumentOperationArgs<T>
) {
  let textDoc = getTextDocumentOfSp(shape)
  if (!textDoc) {
    shape.createTxBody()
    textDoc = getTextDocumentOfSp(shape)!
    moveCursorToStartPosInTextDocument(textDoc)
  }
  if (textDoc) {
    applyOperationFnForTextDoc(textDoc, opFn, args)
  }
}

function applyTextOperationFunctionToGraphicFrame<
  T extends TextDocumentOperationFns,
>(graphicFrame: GraphicFrame, opFn: T, args: TextDocumentOperationArgs<T>) {
  if (opFn === addToParagraphInTextDocument) {
    if (
      (args[0].instanceType === InstanceType.ParaNewLine ||
        args[0].instanceType === InstanceType.ParaText ||
        args[0].instanceType === InstanceType.ParaSpace ||
        args[0].instanceType === InstanceType.ParaTab) &&
      graphicFrame.graphicObject!.selection.isUse
    ) {
      graphicFrame.graphicObject!.remove(1, undefined, true)
    }
  } else if (opFn === addNewParagraphInTextDocument) {
    graphicFrame.graphicObject!.selection.isUse &&
      graphicFrame.graphicObject!.remove(1, undefined, true)
  }
  applyOperationFnForTable(graphicFrame.graphicObject!, opFn, args)
}

function appplyTextFunctionToTextSlideElement<
  T extends TextDocumentOperationFns,
>(sp: TextSlideElement, opFn: T, args: TextDocumentOperationArgs<T>) {
  if (sp.instanceType === InstanceType.Shape) {
    applyTextOperationFunctionToShape(sp, opFn, args)
  } else if (sp.instanceType === InstanceType.GraphicFrame) {
    applyTextOperationFunctionToGraphicFrame(sp, opFn, args)
  }
}

function applyOperationFnForTextDoc<T extends TextDocumentOperationFns>(
  textDoc: TextDocument,
  fn: T,
  args: TextDocumentOperationArgs<T>
) {
  if (textDoc) {
    fn(textDoc, ...args)
  }
}
function applyOperationFnForTable<T extends TextDocumentOperationFns>(
  table: Table,
  fn: T,
  args: TextDocumentOperationArgs<T>
) {
  if (table == null) return
  if (fn === addNewParagraphInTextDocument) {
    addNewParagraphInTextDocument(table.curCell!.textDoc)
  } else if (fn === addToParagraphInTextDocument) {
    table.applyTextDocFunction(
      addToParagraphInTextDocument,
      args as unknown as TextDocumentOperationArgs<
        typeof addToParagraphInTextDocument
      >,
      false
    )
  } else if (fn === applyTextPrToParagraphInTextDocument) {
    table.applyTextDocFunction(
      applyTextPrToParagraphInTextDocument,
      args as unknown as TextDocumentOperationArgs<
        typeof applyTextPrToParagraphInTextDocument
      >,
      true
    )
  } else if (fn === pasteFormattingForTextDocument) {
    table.applyTextDocFunction(
      pasteFormattingForTextDocument,
      args as unknown as TextDocumentOperationArgs<
        typeof pasteFormattingForTextDocument
      >,
      undefined,
      true
    )
  } else {
    table.applyTextDocFunction(fn, args)
  }
}
