import { isBool, isNumber, isDict } from '../../../common/utils'
import { EditorUtil } from '../../../globals/editor'
import { InstanceType } from '../../instanceTypes'
import { getAllShapes } from '../../../ui/rendering/editorHandler/utils'

import { addObjectInArray } from '../../common/helpers'
import { checkAlmostEqual, normalizeRotValue } from '../../common/utils'
import { TableProp, TablePropOptions } from '../../properties/tableProp'
import { Presentation } from '../../Slide/Presentation'
import { ShapeLocks, VerticalAnchorType } from '../../SlideElement/const'
import { ImageShape } from '../../SlideElement/Image'
import { applyImageScrId } from '../shape/hookInvokers'
import { isCustomImage } from '../shape/asserts'
import { checkExtentsByTextContent } from '../shape/extents'
import {
  changePresetOfGeometry,
  setDescription,
  setLineForSlideElement,
  setTitle,
} from '../shape/updates'
import {
  getSelectedConnectors,
  getSelectedSlideElementsByTypes,
} from './getters'
import { FillBlipProp, FillGrad, FillHatch } from '../../properties/props'
import { calculateForShape } from '../../calculation/calculate/shape'
import { calculateForGroup } from '../../calculation/calculate/group'
import { getTopMostGroupShape } from '../shape/getters'
import { setLock } from '../shape/locks'
import { Nullable } from '../../../../liber/pervasive'
import {
  FillKIND,
  FillBlipType,
  FillGradType,
  FillGradPathType,
  VertAlignJc,
} from '../../common/const/attrs'
import { GradientDirections } from '../../../exports/type'
import { USER_TEXTURE_PRESETS } from '../../common/const/texture'
import { CComplexColor, updateComplexColor } from '../../color/complexColor'
import {
  FillEffects,
  validateFillMods,
} from '../../SlideElement/attrs/fill/fill'
import { SlideElement } from '../../SlideElement/type'
import { calculateBrush } from '../shape/calcs'
import { checkShapeBodyAutoFitReset, ensureSpPrXfrm } from '../shape/attrs'
import { GroupShape } from '../../SlideElement/GroupShape'
import { setPropForTable } from '../table'
import { CalcStatus } from '../../calculation/updateCalcStatus/calcStatus'
import { setTextCalcStatus } from '../../calculation/updateCalcStatus/utils'
import { adjustXfrmOfGroup } from '../shape/group'
import {
  RelativeRect,
  RelativeRectOptions,
} from '../../SlideElement/attrs/fill/RelativeRect'
import {
  BlipFill,
  BlipTileInfoOptions,
} from '../../SlideElement/attrs/fill/blipFill'
import {
  CGs,
  GradLinOptions,
  GradPathOptions,
  CGsOptions,
} from '../../SlideElement/attrs/fill/GradFill'
import { PatternFill } from '../../SlideElement/attrs/fill/PatternFill'
import { PresetPatternNumVal } from '../../../io/dataType/spAttrs'
import { ShapeFillOptions, SlideElementOptions } from '../../properties/options'
import { AlphaModFix } from '../../SlideElement/attrs/fill/Effects'

export function applySlideElementProps(
  presentation: Presentation,
  props: SlideElementOptions,
  options?: { shapeId?: string }
) {
  const spsByTypes = getSelectedSlideElementsByTypes(presentation, false)
  let i: number
  if (isNumber(props.verticalTextAlign)) {
    for (i = 0; i < spsByTypes.shapes.length; ++i) {
      spsByTypes.shapes[i].setVerticalAlign(props.verticalTextAlign)
    }
    for (i = 0; i < spsByTypes.groups.length; ++i) {
      spsByTypes.groups[i].setVerticalAlign(props.verticalTextAlign)
    }
    if (spsByTypes.tables.length === 1) {
      const opts: TablePropOptions = {}
      if (props.verticalTextAlign === VerticalAnchorType.BOTTOM) {
        opts.cellsVertAlign = VertAlignJc.Bottom
      } else if (props.verticalTextAlign === VerticalAnchorType.CENTER) {
        opts.cellsVertAlign = VertAlignJc.Center
      } else {
        opts.cellsVertAlign = VertAlignJc.Top
      }

      const tableProps = new TableProp(opts)

      const selectedTextSp = presentation.selectionState.selectedTextSp
      if (selectedTextSp === spsByTypes.tables[0]) {
        setPropForTable(spsByTypes.tables[0].graphicObject!, tableProps)
      } else {
        spsByTypes.tables[0].graphicObject!.selectAll()
        setPropForTable(spsByTypes.tables[0].graphicObject!, tableProps)
        spsByTypes.tables[0].graphicObject!.removeSelection()
      }
      presentation.adjustGraphicFrameRowHeight(spsByTypes.tables[0])
    }
  }

  if (isNumber(props.columnCount)) {
    for (i = 0; i < spsByTypes.shapes.length; ++i) {
      spsByTypes.shapes[i].setColumnCount(props.columnCount)
      calculateForShape(spsByTypes.shapes[i])
      checkExtentsByTextContent(spsByTypes.shapes[i])
    }
    for (i = 0; i < spsByTypes.groups.length; ++i) {
      spsByTypes.groups[i].setColumnCount(props.columnCount)
      calculateForGroup(spsByTypes.groups[i])
      checkExtentsByTextContent(spsByTypes.groups[i])
    }
  }

  if (isNumber(props.columnSpace)) {
    for (i = 0; i < spsByTypes.shapes.length; ++i) {
      spsByTypes.shapes[i].setSpaceColumn(props.columnSpace)
      calculateForShape(spsByTypes.shapes[i])
      checkExtentsByTextContent(spsByTypes.shapes[i])
    }
    for (i = 0; i < spsByTypes.groups.length; ++i) {
      spsByTypes.groups[i].setSpaceColumn(props.columnSpace)
      calculateForGroup(spsByTypes.groups[i])
      checkExtentsByTextContent(spsByTypes.groups[i])
    }
  }

  if (isNumber(props.vert)) {
    for (i = 0; i < spsByTypes.shapes.length; ++i) {
      spsByTypes.shapes[i].setVert(props.vert)
    }
    for (i = 0; i < spsByTypes.groups.length; ++i) {
      spsByTypes.groups[i].setVert(props.vert)
    }
  }
  if (isDict(props.paddings)) {
    for (i = 0; i < spsByTypes.shapes.length; ++i) {
      spsByTypes.shapes[i].setPaddings(props.paddings)
    }
    for (i = 0; i < spsByTypes.groups.length; ++i) {
      spsByTypes.groups[i].setPaddings(props.paddings)
    }
  }
  if (typeof props.type === 'string') {
    const arrOfSps: SlideElement[] = []
    for (i = 0; i < spsByTypes.shapes.length; ++i) {
      if (spsByTypes.shapes[i].isConnectionShape() === false) {
        changePresetOfGeometry(spsByTypes.shapes[i], props.type)
        arrOfSps.push(spsByTypes.shapes[i])
      }
    }
    for (i = 0; i < spsByTypes.groups.length; ++i) {
      changePresetOfGeometry(spsByTypes.groups[i], props.type)
      getAllShapes(spsByTypes.groups[i].spTree, arrOfSps)
    }
    for (i = 0; i < spsByTypes.images.length; ++i) {
      changePresetOfGeometry(spsByTypes.images[i], props.type)
      arrOfSps.push(spsByTypes.images[i])
    }
    presentation.clearConnectorInfo(arrOfSps)
  }
  if (isDict(props.stroke)) {
    for (const sp of spsByTypes.shapes) {
      setLineForSlideElement(sp, props.stroke)
    }
    for (const sp of spsByTypes.groups) {
      setLineForSlideElement(sp, props.stroke)
    }
    for (const sp of spsByTypes.images) {
      setLineForSlideElement(sp, props.stroke)
    }
  }
  if (isDict(props.fill)) {
    for (i = 0; i < spsByTypes.shapes.length; ++i) {
      changeFillForSp(spsByTypes.shapes[i], props.fill as ShapeFillOptions)
    }
    for (i = 0; i < spsByTypes.groups.length; ++i) {
      changeFillForSp(spsByTypes.groups[i], props.fill as ShapeFillOptions)
    }
  }

  for (i = 0; i < spsByTypes.shapes.length; ++i) {
    setTitle(spsByTypes.shapes[i], props.title)
  }
  for (i = 0; i < spsByTypes.groups.length; ++i) {
    setTitle(spsByTypes.groups[i], props.title)
  }

  for (i = 0; i < spsByTypes.images.length; ++i) {
    setTitle(spsByTypes.images[i], props.title)
  }

  for (i = 0; i < spsByTypes.shapes.length; ++i) {
    setDescription(spsByTypes.shapes[i], props.description)
  }
  for (i = 0; i < spsByTypes.groups.length; ++i) {
    setDescription(spsByTypes.groups[i], props.description)
  }

  for (i = 0; i < spsByTypes.images.length; ++i) {
    setDescription(spsByTypes.images[i], props.description)
  }

  if (options?.shapeId) {
    const imageShape = EditorUtil.entityRegistry.getEntityById<ImageShape>(
      options?.shapeId
    )
    if (imageShape && props.imageUrl) {
      if (isCustomImage(imageShape)) {
        // 非本地图片保留裁剪状态
        const blipFill = imageShape.blipFill.clone()
        blipFill.setImageSrcId(props.imageUrl)
        blipFill.setEncryptUrl(props.encryptImageUrl)
        imageShape.setBlipFill(blipFill)
        applyImageScrId()
      } else {
        imageShape.setBlipFill(
          new BlipFill(props.imageUrl, props.encryptImageUrl)
        )
      }
    }
  } else {
    if (typeof props.imageUrl === 'string' && props.imageUrl.length > 0) {
      for (i = 0; i < spsByTypes.images.length; ++i) {
        const image = spsByTypes.images[i] as ImageShape
        const blipFill = image.blipFill.clone()
        blipFill.setImageSrcId(props.imageUrl)
        blipFill.setEncryptUrl(props.encryptImageUrl)
        image.setBlipFill(blipFill)
        // crop side effect
        applyImageScrId()
      }
    }
  }

  if (props.effects?.length) {
    for (i = 0; i < spsByTypes.images.length; ++i) {
      const { blipFill } = spsByTypes.images[i]
      blipFill!.effects = props.effects
        .concat(blipFill!.effects || [])
        .reduce((acc: AlphaModFix[], val) => {
          if (!acc.find((effect) => effect.Type === val.Type)) {
            acc.push(val)
          }
          return acc
        }, [])
      spsByTypes.images[i].setBlipFill(blipFill)
    }
  }

  const arrOfGroups: GroupShape[] = []
  let isCheckConnectors = false
  let moveFlag = true
  if (isNumber(props.width) || isNumber(props.height)) {
    for (i = 0; i < spsByTypes.shapes.length; ++i) {
      ensureSpPrXfrm(spsByTypes.shapes[i])
      if (isNumber(props.width)) {
        spsByTypes.shapes[i].spPr!.xfrm!.setExtX(props.width)
      }
      if (isNumber(props.height)) {
        spsByTypes.shapes[i].spPr!.xfrm!.setExtY(props.height)
      }

      checkShapeBodyAutoFitReset(spsByTypes.shapes[i])
      if (spsByTypes.shapes[i].group) {
        addObjectInArray(
          arrOfGroups,
          getTopMostGroupShape(spsByTypes.shapes[i].group!)
        )
      }
    }
    if (isNumber(props.width) && isNumber(props.height)) {
      for (i = 0; i < spsByTypes.images.length; ++i) {
        ensureSpPrXfrm(spsByTypes.images[i])
        spsByTypes.images[i].spPr!.xfrm!.setExtX(props.width)
        spsByTypes.images[i].spPr!.xfrm!.setExtY(props.height)
        if (spsByTypes.images[i].group) {
          addObjectInArray(
            arrOfGroups,
            getTopMostGroupShape(spsByTypes.images[i].group!)
          )
        }
      }
    }

    isCheckConnectors = true
    moveFlag = false
  }

  if (isBool(props.lockAspect)) {
    for (i = 0; i < spsByTypes.shapes.length; ++i) {
      setLock(
        spsByTypes.shapes[i],
        ShapeLocks.noModifyAspect,
        props.lockAspect ? true : undefined
      )
    }
    for (i = 0; i < spsByTypes.images.length; ++i) {
      setLock(
        spsByTypes.images[i],
        ShapeLocks.noModifyAspect,
        props.lockAspect ? true : undefined
      )
    }
  }
  if (
    (isDict(props.position) &&
      isNumber(props.position.x) &&
      isNumber(props.position.y)) ||
    isBool(props.flipH) ||
    isBool(props.flipV) ||
    isBool(props.InvertFlipH) ||
    isBool(props.InvertFlipV) ||
    isNumber(props.rot) ||
    isNumber(props.rotToIncr)
  ) {
    const isUpdatePosition =
      isDict(props.position) &&
      isNumber(props.position.x) &&
      isNumber(props.position.y)

    ;[...spsByTypes.shapes, ...spsByTypes.images, ...spsByTypes.groups].forEach(
      (shape) => {
        ensureSpPrXfrm(shape)
        if (isUpdatePosition) {
          shape.spPr!.xfrm!.setOffX(props.position?.x)
          shape.spPr!.xfrm!.setOffY(props.position?.y)
        }
        const orgRot = normalizeRotValue(shape.rot)
        if (isBool(props.flipH)) {
          const needUpdate = shape.flipH !== props.flipH
          if (needUpdate) {
            shape.spPr!.xfrm!.setFlipH(props.flipH)
            shape.spPr!.xfrm!.setRot(2 * Math.PI - orgRot)
          }
        }
        if (isBool(props.flipV)) {
          const needUpdate = shape.flipV !== props.flipV
          if (needUpdate) {
            shape.spPr!.xfrm!.setFlipV(props.flipV)
            shape.spPr!.xfrm!.setRot(2 * Math.PI - orgRot)
          }
        }
        if (isNumber(props.rotToIncr)) {
          shape.spPr!.xfrm!.setRot(
            normalizeRotValue(shape.rot + props.rotToIncr)
          )
        }
        if (isNumber(props.rot)) {
          shape.spPr!.xfrm!.setRot(normalizeRotValue(props.rot))
        }
        if (isBool(props.InvertFlipH)) {
          shape.spPr!.xfrm!.setFlipH(!shape.flipH)
          shape.spPr!.xfrm!.setRot(2 * Math.PI - orgRot)
        }
        if (isBool(props.InvertFlipV)) {
          shape.spPr!.xfrm!.setFlipV(!shape.flipV)
          shape.spPr!.xfrm!.setRot(2 * Math.PI - orgRot)
        }
        if (shape.group) {
          addObjectInArray(arrOfGroups, getTopMostGroupShape(shape.group))
        }
      }
    )
    isCheckConnectors = true
  }

  if (isDict(props.srcRect)) {
    for (i = 0; i < spsByTypes.images.length; ++i) {
      const blipFill = spsByTypes.images[i].blipFill.clone()
      blipFill.setSrcRect(props.srcRect as RelativeRect)
      spsByTypes.images[i].setBlipFill(blipFill)
    }
    for (i = 0; i < spsByTypes.shapes.length; ++i) {
      const brush = spsByTypes.shapes[i].brush
      if (
        brush &&
        brush.fill &&
        brush.fill.instanceType === InstanceType.BlipFill
      ) {
        brush.fill.fromOptions(spsByTypes.images[i].blipFill)
        brush.fill.setSrcRect(props.srcRect as RelativeRect)
      }
    }
  }

  if (isCheckConnectors) {
    const arrOfConnectors = getSelectedConnectors(presentation)
    for (i = 0; i < arrOfConnectors.length; ++i) {
      arrOfConnectors[i].calcTransform(moveFlag)
      const group = getTopMostGroupShape(arrOfConnectors[i])
      if (group) {
        addObjectInArray(arrOfGroups, group)
      }
    }
  }

  for (i = 0; i < arrOfGroups.length; ++i) {
    adjustXfrmOfGroup(arrOfGroups[i])
  }

  for (i = 0; i < spsByTypes.shapes.length; ++i) {
    setTextCalcStatus(spsByTypes.shapes[i])
    calculateForShape(spsByTypes.shapes[i])
  }
  for (i = 0; i < spsByTypes.groups.length; ++i) {
    setTextCalcStatus(spsByTypes.groups[i])
    calculateForGroup(spsByTypes.groups[i])
  }

  return spsByTypes
}

export function setTableProps(presentation: Presentation, props: TableProp) {
  const { tables } = getSelectedSlideElementsByTypes(presentation)
  if (tables.length) {
    tables.forEach((table) => {
      const rowHeight = props.rowHeight
      const prevIsSelectCell = props.isSelectCell

      let isIgnoreHeight = false
      if (isNumber(props.rowHeight)) {
        if (checkAlmostEqual(props.rowHeight, 0.0)) {
          props.rowHeight = 1.0
        }
        isIgnoreHeight = false
      }
      const selectedTable = presentation.selectionState.selectedTable
      if (
        selectedTable === table.graphicObject &&
        (selectedTable?.selection.isUse ||
          presentation.selectionState.selectedTextSp != null)
      ) {
        props.isSelectCell = true
      }

      if (props.isSelectCell === true) {
        setPropForTable(table.graphicObject!, props)
      } else {
        table.graphicObject!.selectAll()
        setPropForTable(table.graphicObject!, props)
        table.graphicObject!.removeSelection()
      }

      props.rowHeight = rowHeight
      props.isSelectCell = prevIsSelectCell
      presentation.adjustGraphicFrameRowHeight(table, isIgnoreHeight)
    })
  }
}

export function updateFillEffectsByProp(
  props: ShapeFillOptions,
  fill: Nullable<FillEffects>
) {
  const ret: FillEffects = fill != null ? fill.clone() : new FillEffects()
  if (null == props) return ret

  const { fill: fillProp, type: fillType, transparent } = props

  if (null != fillType) {
    switch (fillType) {
      case FillKIND.NOFILL: {
        ret.NoFill()
        break
      }
      case FillKIND.GRP: {
        ret.Grp()
        break
      }
      case FillKIND.BLIP: {
        const blipFillProp = fillProp as FillBlipProp
        let url = blipFillProp.url
        const textureId = blipFillProp.textureId
        if (
          null != textureId &&
          0 <= textureId &&
          textureId < USER_TEXTURE_PRESETS.length
        ) {
          url = USER_TEXTURE_PRESETS[textureId]
        }

        const isValidUrl = typeof url === 'string' && url.length > 0
        const tileType = blipFillProp.type
        let tile: BlipTileInfoOptions | undefined,
          srcRect: RelativeRect | undefined,
          stretch: boolean | undefined
        if (tileType === FillBlipType.STRETCH) {
          tile = undefined
          srcRect = undefined
          stretch = true
        } else if (tileType === FillBlipType.TILE) {
          tile = {}
          stretch = false
          srcRect = undefined
        }
        const blipOpts = {
          imageSrcId: isValidUrl ? url : undefined,
          srcRect,
          stretch,
          tile,
        }

        ret.Blip(blipOpts)
        break
      }
      case FillKIND.PATT: {
        if (ret.fill == null) {
          ret.fill = new PatternFill()
        }
        const pattFillProp = fillProp as FillHatch

        let patternType: PresetPatternNumVal | undefined,
          fgClr: Nullable<CComplexColor> | undefined,
          bgClr: Nullable<CComplexColor> | undefined

        if (null != pattFillProp.patternType) {
          patternType = pattFillProp.patternType
        }
        if (null != pattFillProp.fgClr) {
          fgClr = updateComplexColor(
            pattFillProp.fgClr,
            CComplexColor.RGBA(0, 0, 0)
          )
        }

        if (null != pattFillProp.bgClr) {
          bgClr = updateComplexColor(
            pattFillProp.bgClr,
            CComplexColor.RGBA(0, 0, 0)
          )
        }

        ret.Pattern({
          patternType,
          fgClr,
          bgClr,
        })
        break
      }
      case FillKIND.GRAD: {
        let ln: GradLinOptions | undefined,
          path: GradPathOptions | undefined,
          tileRect: RelativeRectOptions | undefined,
          gsList: CGsOptions[] | undefined

        const gradFillProp = fillProp as FillGrad
        const {
          gsLst: propGsLst,
          gradType,
          pathType,
          pathDirection,
        } = gradFillProp

        if (propGsLst.length > 1) {
          gsList = propGsLst.map((gs) => {
            const ccolor = updateComplexColor(gs.color, undefined)
            const gradientStop = new CGs(gs.pos, ccolor ?? undefined)
            return gradientStop
          })
        }

        if (FillGradType.LINEAR === gradType) {
          const { linearAngle: angle, linearScale: scale } = gradFillProp

          ln = {
            angle: 0,
            scale: false,
          }
          if (null != angle) ln.angle = angle
          if (null != scale) ln.scale = scale
          path = undefined
        } else if (FillGradType.PATH === gradType && pathType) {
          switch (pathType) {
            case FillGradPathType.Rect:
            case FillGradPathType.Shape:
            case FillGradPathType.Circle:
              ln = undefined
              path = {
                path: pathType,
              }

              if (isNumber(pathDirection)) {
                switch (pathDirection) {
                  case GradientDirections.FromBottomRightCorner:
                    path.fillRect = { l: 100, t: 100, r: 100, b: 100 }
                    tileRect = { l: 0, t: 0, r: 200, b: 200 }
                    break
                  case GradientDirections.FromBottomLeftCorner:
                    path.fillRect = { l: 0, t: 100, r: 0, b: 100 }
                    tileRect = { l: -100, t: 0, r: 100, b: 200 }
                    break
                  case GradientDirections.FromCenter:
                    path.fillRect = { l: 50, t: 50, r: 50, b: 50 }
                    break
                  case GradientDirections.FromTopRightCorner:
                    path.fillRect = { l: 100, t: 0, r: 100, b: 0 }
                    tileRect = { l: 0, t: -100, r: 200, b: 200 }
                    break
                  case GradientDirections.FromTopLeftCorner:
                    path.fillRect = { l: 0, t: 0, r: 0, b: 0 }
                    tileRect = { l: -100, t: -100, r: 100, b: 100 }
                    break
                }
              }
          }
        }
        ret.Grad({
          ln,
          path,
          tileRect,
          gsList,
        })
        break
      }
      case FillKIND.SOLID: {
        ret.SolidComputed((fillProp as any).color)
        break
      }
      case FillKIND.NONE: {
        // only change transparency
        if (transparent != null) {
          ret.updateAlpha(transparent)
        }
        break
      }
    }
  }

  if (null != transparent) {
    ret.transparent = transparent
  }

  if (ret.transparent != null) {
    if (ret.fill && ret.fill.type === FillKIND.BLIP) {
      ret.fill.updateTransparent(ret.transparent)
    }
  }

  return ret
}

function changeFillForSp(sp: SlideElement, fillProp: ShapeFillOptions) {
  if (sp.instanceType === InstanceType.Shape) {
    if (sp.isCalcStatusSet(CalcStatus.Shape.Brush)) {
      calculateBrush(sp)
    }
    const fillEffects = updateFillEffectsByProp(fillProp, sp.brush)
    validateFillMods(fillEffects)
    sp.spPr!.setFill(fillEffects)
  } else if (sp.instanceType === InstanceType.GroupShape) {
    for (let i = 0; i < sp.spTree.length; ++i) {
      changeFillForSp(sp.spTree[i], fillProp)
    }
  }
}
