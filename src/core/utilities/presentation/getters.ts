import { Dict, Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { gDefaultColorMap } from '../../color/clrMap'
import { isSlide } from '../../common/utils'
import { Presentation } from '../../Slide/Presentation'
import { ConnectionShape } from '../../SlideElement/ConnectionShape'
import { SpPr, Xfrm } from '../../SlideElement/attrs/shapePrs'
import { GraphicFrame } from '../../SlideElement/GraphicFrame'
import { GroupShape } from '../../SlideElement/GroupShape'
import { ImageShape } from '../../SlideElement/Image'
import { Shape } from '../../SlideElement/Shape'
import { ParaPr } from '../../textAttributes/ParaPr'
import {
  collectCxnShapes,
  getBoundsOfGroupingSps,
  getTextDocumentOfSp,
} from '../shape/getters'
import { canSlideElementGroup } from '../shape/operations'
import { setDeletedForSp, setGroup } from '../shape/updates'
import { SlideLayout } from '../../Slide/SlideLayout'
import { SlideMaster } from '../../Slide/SlideMaster'
import { getSelectedTextOfTextDocument } from '../selection/getter'
import { ensureSpPrXfrm } from '../shape/attrs'
import { TextPr } from '../../textAttributes/TextPr'
import { deleteSlideElementfromParent } from '../shape/delete'
import { Table } from '../../Table/Table'

export function getSelectedSlideElementsByTypes(
  presentation: Presentation,
  includeGroupes?: boolean
) {
  const selectedElements = presentation.selectionState?.selectedElements || []
  return groupSlideElmentsByTypes(selectedElements, includeGroupes)
}

export interface SlideElementByTypes {
  shapes: Array<Shape>
  images: Array<ImageShape>
  groups: Array<GroupShape>
  tables: Array<GraphicFrame>
  // ole 尽可能复用 image 逻辑
}

function groupSlideElmentsByTypes(
  sps: SlideElement[],
  includeGroupes?: boolean
): SlideElementByTypes {
  const ret: SlideElementByTypes = {
    shapes: [],
    images: [],
    groups: [],
    tables: [],
    oleObjects: [],
  } as any
  for (let i = 0; i < sps.length; ++i) {
    const sp = sps[i]
    const type = sp.instanceType
    switch (type) {
      case InstanceType.Shape: {
        ret.shapes.push(sp)
        break
      }
      case InstanceType.ImageShape: {
        ret.images.push(sp)
        break
      }

      case InstanceType.GroupShape: {
        ret.groups.push(sp)
        if (includeGroupes) {
          const spsOfGrp = groupSlideElmentsByTypes(sp.spTree, true)
          ret.shapes = ret.shapes.concat(spsOfGrp.shapes)
          ret.images = ret.images.concat(spsOfGrp.images)
          ret.tables = ret.tables.concat(spsOfGrp.tables)
        }
        break
      }
      case InstanceType.GraphicFrame: {
        ret.tables.push(sp)
        break
      }
    }
  }
  return ret
}

export function getSlideElementsForGrouping(presentation: Presentation) {
  const spTree = presentation.getCurrentFocusEditContainer()?.cSld.spTree
  const sps: SlideElement[] = []
  if (spTree) {
    for (let i = 0; i < spTree.length; ++i) {
      const sp = spTree[i]
      if (sp.selected && canSlideElementGroup(sp)) {
        sps.push(sp)
      }
    }
  }
  return sps
}

export function createGrpSpForGrouping(spArr: SlideElement[]) {
  const spsForGroup = spArr

  if (spsForGroup.length < 2) return null
  for (let i = spArr.length - 1; i > -1; --i) {
    deleteSlideElementfromParent(spArr[i])
  }
  const bounds = getBoundsOfGroupingSps(spsForGroup)
  const maxX = bounds.r
  const maxY = bounds.b
  const minX = bounds.l
  const minY = bounds.t
  const group = new GroupShape()
  group.setSpPr(new SpPr())
  group.spPr!.setParent(group)
  group.spPr!.setXfrm(new Xfrm(group.spPr))
  const xfrm = group.spPr!.xfrm!
  xfrm.setParent(group.spPr)
  xfrm.setOffX(minX)
  xfrm.setOffY(minY)
  xfrm.setExtX(maxX - minX)
  xfrm.setExtY(maxY - minY)
  xfrm.setChExtX(maxX - minX)
  xfrm.setChExtY(maxY - minY)
  xfrm.setChOffX(0)
  xfrm.setChOffY(0)
  for (let i = 0; i < spsForGroup.length; ++i) {
    ensureSpPrXfrm(spsForGroup[i])
    spsForGroup[i].spPr!.xfrm!.setOffX(spsForGroup[i].x - minX)
    spsForGroup[i].spPr!.xfrm!.setOffY(spsForGroup[i].y - minY)
    setGroup(spsForGroup[i], group)
    group.addToSpTree(group.spTree.length, spsForGroup[i])
  }
  setDeletedForSp(group, false)
  return group
}

export function getSlideElementsOfEditContainer(
  presentation: Presentation
): SlideElement[] {
  return presentation.getCurrentFocusEditContainer()?.cSld.spTree || []
}

export function collectCxnsFromSlideElements(
  presentation: Presentation,
  sps: SlideElement[],
  result: Nullable<ConnectionShape[]>,
  arrOfConnectors?: Nullable<ConnectionShape[]>
) {
  const ret = Array.isArray(result) ? result : []

  arrOfConnectors = Array.isArray(arrOfConnectors)
    ? arrOfConnectors
    : collectCxnShapes(getSlideElementsOfEditContainer(presentation), [])

  for (let i = 0; i < arrOfConnectors.length; ++i) {
    const cxnSp = arrOfConnectors[i]
    for (let j = 0; j < sps.length; ++j) {
      const sp = sps[j]
      if (sp.instanceType === InstanceType.GroupShape) {
        collectCxnsFromSlideElements(
          presentation,
          sp.spTree,
          ret,
          arrOfConnectors
        )
      } else {
        if (
          ret.indexOf(cxnSp) === -1 && // not in the result
          (sp.getId() === cxnSp.getStCxnId() ||
            sp.getId() === cxnSp.getEndCxnId() ||
            sp === cxnSp)
        ) {
          ret.push(cxnSp)
        }
      }
    }
  }
  return ret
}

export function getSelectedConnectors(presentation: Presentation) {
  const allSelectedSps = presentation.selectionState?.selectedElements || []
  const allConnectors = collectCxnsFromSlideElements(
    presentation,
    allSelectedSps,
    []
  )
  const ret: ConnectionShape[] = []
  for (let i = 0; i < allConnectors.length; ++i) {
    if (!allConnectors[i].selected) {
      ret.push(allConnectors[i])
    }
  }
  return ret
}

export function getCurrentParaPr(
  presentation: Presentation,
  onlyFirstParaPr = false
) {
  const selectedTextSp = presentation.selectionState.selectedTextSp

  if (selectedTextSp) {
    if (selectedTextSp.instanceType === InstanceType.GraphicFrame) {
      return onlyFirstParaPr
        ? selectedTextSp.graphicObject!.getFirstParaPr()
        : selectedTextSp.graphicObject!.getCalcedParaPr()
    } else {
      const textDoc = presentation.getCurrentTextTarget()
      if (textDoc) {
        return onlyFirstParaPr
          ? textDoc.getFirstParaPr()
          : textDoc.getCalcedParaPr()
      }
    }
  } else {
    const getPropsFromSps = (sps: SlideElement[]) => {
      let curPr: Nullable<ParaPr>, result: Nullable<ParaPr>
      for (let i = 0; i < sps.length; ++i) {
        const sp = sps[i]
        curPr = null
        if (sp.instanceType === InstanceType.GroupShape) {
          curPr = getPropsFromSps(sp.slideElementsInGroup)
        } else if (sp.instanceType === InstanceType.GraphicFrame) {
          if (sp.graphicObject) {
            const table = sp.graphicObject as Table

            const row = table.children.at(0)
            const cell = row?.children.at(0)
            const textDoc = cell?.textDoc
            if (textDoc) {
              textDoc.isSetToAll = true
              curPr = onlyFirstParaPr
                ? textDoc.getFirstParaPr()
                : textDoc.getCalcedParaPr()
              textDoc.isSetToAll = false
            }
          }
        } else {
          const textDoc = getTextDocumentOfSp(sp)
          if (textDoc) {
            textDoc.isSetToAll = true
            curPr = onlyFirstParaPr
              ? textDoc.getFirstParaPr()
              : textDoc.getCalcedParaPr()
            textDoc.isSetToAll = false
          }
        }

        if (curPr) {
          if (!result) result = curPr
          else result.intersect(curPr)
        }
      }
      return result
    }

    const selectedElements = presentation.selectionState?.selectedElements || []
    const result = getPropsFromSps(selectedElements)
    return result
  }
}

export function getTextPrOfCurrentSelection(
  presentation: Presentation,
  isFirstParaTextPr = false
) {
  const selectedTextSp = presentation.selectionState.selectedTextSp
  if (selectedTextSp) {
    if (selectedTextSp.instanceType === InstanceType.GraphicFrame) {
      return isFirstParaTextPr
        ? selectedTextSp.graphicObject!.getFirstParaTextPr()
        : selectedTextSp.graphicObject!.getCalcedTextPr()
    } else {
      const textDoc = presentation.getCurrentTextTarget()
      if (textDoc) {
        return isFirstParaTextPr
          ? textDoc.getFirstParaTextPr()
          : textDoc.getCalcedTextPr()
      }
    }
  } else {
    const getPropsFromSps = (sps: SlideElement[]) => {
      let curPr: Nullable<TextPr>, result: Nullable<TextPr>
      for (let i = 0; i < sps.length; ++i) {
        curPr = null
        const sp = sps[i]
        if (sp.instanceType === InstanceType.GroupShape) {
          curPr = getPropsFromSps(sp.slideElementsInGroup)
        } else if (sp.instanceType === InstanceType.GraphicFrame) {
          if (sp.graphicObject) {
            const table = sp.graphicObject as Table

            const row = table.children.at(0)
            const cell = row?.children.at(0)
            const textDoc = cell?.textDoc
            if (textDoc) {
              textDoc.isSetToAll = true
              curPr = isFirstParaTextPr
                ? textDoc.getFirstParaTextPr()
                : textDoc.getCalcedTextPr()
              textDoc.isSetToAll = false
            }
          }
        } else {
          const textDoc = getTextDocumentOfSp(sp)
          if (textDoc) {
            textDoc.isSetToAll = true
            curPr = isFirstParaTextPr
              ? textDoc.getFirstParaTextPr()
              : textDoc.getCalcedTextPr()
            textDoc.isSetToAll = false
          }
        }

        if (curPr) {
          if (!result) result = curPr
          else result.subDiff(curPr)
        }
      }
      return result
    }

    const selectedElements = presentation.selectionState?.selectedElements || []
    const result = getPropsFromSps(selectedElements)
    return result
  }
}

export function getThemeForEditContainer(presentation: Presentation) {
  return presentation.getCurrentFocusEditContainer()?.getTheme()
}

export function getColorMapForEditContainer(presentation: Presentation) {
  const editContainer = presentation.getCurrentFocusEditContainer()
  if (editContainer) {
    if (editContainer.clrMap) {
      return editContainer.clrMap
    } else if (isSlide(editContainer) && editContainer.layout) {
      if (editContainer.layout.clrMap) {
        return editContainer.layout.clrMap
      } else if (editContainer.layout.master) {
        if (editContainer.layout.master.clrMap) {
          return editContainer.layout.master.clrMap
        }
      }
    } else if (editContainer.master) {
      if (editContainer.master.clrMap) {
        return editContainer.master.clrMap
      }
    }
  }
  return gDefaultColorMap
}

export function getSelectedText(
  presentation: Presentation,
  isClearText?: boolean,
  pr?: { insertNewLine?: boolean; insertNewLineAtParaEnd?: boolean }
) {
  const textDoc = presentation.getCurrentTextTarget()
  if (textDoc) {
    return getSelectedTextOfTextDocument(textDoc, isClearText, pr)
  } else {
    return ''
  }
}

export function getSelectedSpText(
  presentation: Presentation,
  pr?: { insertNewLine?: boolean; insertNewLineAtParaEnd?: boolean }
) {
  const selectedTextSp = presentation.selectionState.selectedTextSp

  const textDoc =
    selectedTextSp != null ? getTextDocumentOfSp(selectedTextSp) : undefined

  if (textDoc) {
    const oldSetToAll = textDoc.isSetToAll
    textDoc.isSetToAll = true
    const textStr = getSelectedTextOfTextDocument(textDoc, true, pr)
    textDoc.isSetToAll = oldSetToAll
    return textStr
  } else {
    return ''
  }
}

export function getLayoutAndMasterMaps(presentation: Presentation) {
  const ret: {
    layouts: Dict<SlideLayout>
    masters: Dict<SlideMaster>
  } = {
    layouts: {},
    masters: {},
  }

  const slides = presentation.slides
  for (let i = 0, l = slides.length; i < l; ++i) {
    const layout = slides[i].layout
    if (layout) {
      ret.layouts[layout.id] = layout
      if (layout.master) {
        ret.masters[layout.master!.id] = layout.master!
      }
    }
  }
  return ret
}
