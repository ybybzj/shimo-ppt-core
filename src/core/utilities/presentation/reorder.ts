import { Dict } from '../../../../liber/pervasive'
import { EditorUtil } from '../../../globals/editor'
import { EditActionFlag } from '../../EditActionFlag'
import { Presentation } from '../../Slide/Presentation'
import { Slide } from '../../Slide/Slide'
import { addToParentSpTree } from '../shape/updates'
import { getSlideElementsOfEditContainer } from './getters'
import { deleteSlideElementfromParent } from '../shape/delete'
import { startEditAction } from '../../calculation/informChanges/startAction'
import { calculateForRendering } from '../../calculation/calculate/calculateForRendering'
import { CTSectionLst } from '../../../io/dataType/presentation'
import { PresentationSectionList } from '../../Slide/PrSection'

export function reorderSlides(
  presentation: Presentation,
  refs: string[],
  slidesMap: Dict<Slide>,
  sectionList?: CTSectionLst | null
) {
  const slides = refs.map((rid) => slidesMap[rid])
  presentation.slides.forEach((s) => {
    if (refs.indexOf(s.getRefId()) <= -1) {
      EditorUtil.entityRegistry.removeEntityById(s.id)
    }
  })
  presentation.slides.length = 0
  for (let i = 0, l = slides.length; i < l; i++) {
    const slide = slides[i]
    if (slide) {
      presentation.slides.push(slide)
      slide.udpateIndex(presentation.slides.length - 1)
    }
  }
  applysectionPrList(presentation, sectionList)
}

export function applysectionPrList(
  presentation: Presentation,
  sectionList?: CTSectionLst | null
) {
  if (sectionList !== undefined) {
    presentation.sectionList =
      presentation.sectionList ?? new PresentationSectionList(presentation)
    if (sectionList === null) {
      presentation.sectionList.reset()
    } else {
      presentation.sectionList.fromJSON(sectionList)
    }
  }
}

export function reorderSlideMasters(
  presentation: Presentation,
  refs: string[]
) {
  presentation.slideMasters.forEach((s) => {
    if (refs.indexOf(s.getRefId()) <= -1) {
      EditorUtil.entityRegistry.removeEntityById(s.id)
    }
  })
  const masters = presentation.slideMasters.map((s) => s)
  presentation.slideMasters.length = 0
  refs.forEach((ref) => {
    const master = masters.find((master) => master.getRefId() === ref)
    if (master) {
      presentation.slideMasters.push(master)
    }
  })
}

export function bringToFront(presentation: Presentation) {
  startEditAction(EditActionFlag.action_Presentation_BringToFront)
  const sp_tree = getSlideElementsOfEditContainer(presentation)
  const selectedGroup = presentation.selectionState.selectedGroup
  if (selectedGroup != null) {
    selectedGroup.bringToFront()
  } else if (sp_tree) {
    const selected: any[] = []
    for (let i = 0; i < sp_tree.length; ++i) {
      if (sp_tree[i].selected) {
        selected.push(sp_tree[i])
      }
    }
    for (let i = sp_tree.length - 1; i > -1; --i) {
      if (sp_tree[i].selected) {
        deleteSlideElementfromParent(sp_tree[i], undefined, true)
      }
    }
    for (let i = 0; i < selected.length; ++i) {
      addToParentSpTree(selected[i], sp_tree.length)
    }
  }
  presentation.getCurrentFocusEditContainer()!.onUpdateSlide()

  calculateForRendering()
}

export function bringForward(presentation: Presentation) {
  startEditAction(EditActionFlag.action_Presentation_BringForward)
  const sp_tree = getSlideElementsOfEditContainer(presentation)
  const selectedGroup = presentation.selectionState.selectedGroup
  if (selectedGroup != null) {
    selectedGroup.bringForward()
  } else if (sp_tree) {
    for (let i = sp_tree.length - 1; i > -1; --i) {
      const sp = sp_tree[i]
      if (sp.selected && i < sp_tree.length - 1 && !sp_tree[i + 1].selected) {
        deleteSlideElementfromParent(sp, undefined, true)
        addToParentSpTree(sp, i + 1)
      }
    }
  }
  presentation.getCurrentFocusEditContainer()!.onUpdateSlide()
  calculateForRendering()
}

export function sendToBack(presentation: Presentation) {
  startEditAction(EditActionFlag.action_Presentation_SendToBack)
  const sp_tree = getSlideElementsOfEditContainer(presentation)
  const selectedGroup = presentation.selectionState.selectedGroup
  if (selectedGroup != null) {
    selectedGroup.sendToBack()
  } else if (sp_tree) {
    let j = 0
    for (let i = 0; i < sp_tree.length; ++i) {
      if (sp_tree[i].selected) {
        const object = sp_tree[i]
        deleteSlideElementfromParent(object, undefined, true)
        addToParentSpTree(object, j)
        ++j
      }
    }
  }
  presentation.getCurrentFocusEditContainer()!.onUpdateSlide()
  calculateForRendering()
}

export function bringBackward(presentation: Presentation) {
  startEditAction(EditActionFlag.action_Presentation_BringBackward)
  const sp_tree = getSlideElementsOfEditContainer(presentation)
  const selectedGroup = presentation.selectionState.selectedGroup
  if (selectedGroup != null) {
    selectedGroup.bringBackward()
  } else if (sp_tree) {
    for (let i = 0; i < sp_tree.length; ++i) {
      const sp = sp_tree[i]
      if (sp.selected && i > 0 && !sp_tree[i - 1].selected) {
        deleteSlideElementfromParent(sp, undefined, true)
        addToParentSpTree(sp, i - 1)
      }
    }
  }
  presentation.getCurrentFocusEditContainer()!.onUpdateSlide()
  calculateForRendering()
}
