import { Hooks } from '../../hooks'
import { hookManager } from '../../hooks/manager'

export function moveCursorForward() {
  hookManager.invoke(Hooks.EditorUI.OnMoveCursorForward)
}
