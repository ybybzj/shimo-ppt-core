import { EditActionFlag } from '../../EditActionFlag'
import {
  compareSlideSizes,
  getDestSlideSizeInfo,
  getScaleUpdateInfoByChangeInfo,
  SlideSizeChangeInfo,
  SlideSizeComparisonInfo,
} from '../../../re/export/Slide'
import { ScaleOfPPTXSizes } from '../../common/const/unit'
import { Presentation } from '../../Slide/Presentation'
import { calculateForRendering } from '../../calculation/calculate/calculateForRendering'
import { startEditAction } from '../../calculation/informChanges/startAction'
import {
  restoreRecordChangesState,
  turnOffRecordChanges,
} from '../../common/utils'
import { updateCalcStatusForPresentation } from '../../calculation/updateCalcStatus/presentation'
import { EntityPropertyChange } from '../../changes/propertyChange'
import { EditorUtil } from '../../../globals/editor'

/** 仅修改 presentation size, 不关心 sp 的变更, 单位为 mm */
export function changeSlideSizeForPresentation(
  presentation: Presentation,
  width: number,
  height: number
) {
  const s = turnOffRecordChanges()
  for (let i = 0; i < presentation.slideMasters.length; ++i) {
    presentation.slideMasters[i].updateSize(width, height)
    const master = presentation.slideMasters[i]
    for (let j = 0; j < master.layouts.length; ++j) {
      master.layouts[j].updateSize(width, height)
    }
  }
  for (let i = 0; i < presentation.slides.length; ++i) {
    presentation.slides[i].updateSize(width, height)
  }
  restoreRecordChangesState(s)
}

export function getResizeOptionsBySize(
  presentation: Presentation,
  width,
  height
): SlideSizeComparisonInfo {
  return compareSlideSizes({
    from: { width: presentation.width, height: presentation.height },
    to: { width, height },
  })
}

/** 内部调用需生成默认选项赋值 */
export function updatePresentationSize(
  presentation: Presentation,
  scaleInfo: SlideSizeChangeInfo
) {
  const { width, height } = getDestSlideSizeInfo(scaleInfo)
  const { scale, deltaX, deltaY } = getScaleUpdateInfoByChangeInfo(scaleInfo)
  const contentScaleInfo = { scale, deltaX, deltaY }
  startEditAction(EditActionFlag.action_Presentation_ChangeSlideSize)
  const editChange = new EntityPropertyChange(
    presentation,
    EditActionFlag.edit_Presentation_SlideSize,
    { w: presentation.width, h: presentation.height },
    { w: width, h: height }
  )

  EditorUtil.ChangesStack.addToAction(editChange)
  presentation.width = width
  presentation.height = height
  if (presentation.pres) {
    presentation.pres.slideSize.cx = width * ScaleOfPPTXSizes
    presentation.pres.slideSize.cy = height * ScaleOfPPTXSizes
  }
  for (let i = 0; i < presentation.slideMasters.length; i++) {
    presentation.slideMasters[i].updateSize(width, height, contentScaleInfo)
    const master = presentation.slideMasters[i]
    for (let j = 0; j < master.layouts.length; j++) {
      master.layouts[j].updateSize(width, height, contentScaleInfo)
    }
  }
  for (let i = 0; i < presentation.slides.length; i++) {
    presentation.slides[i].updateSize(width, height, contentScaleInfo)
  }
  updateCalcStatusForPresentation(presentation, editChange)
  calculateForRendering()
  presentation.syncEditorUIState()
}
