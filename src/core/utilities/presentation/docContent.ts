import { isNumber } from '../../../common/utils'
import { Presentation } from '../../Slide/Presentation'
import {
  applyTextDocumentOperationFunction,
  applyTextOperationFunction,
} from './selectionAppliers'
import setParagraphIndentForTextDocument, {
  addNewParagraphInTextDocument,
  addToParagraphInTextDocument,
  applyTextPrToParagraphInTextDocument,
  changeFontSizeForTextDocument,
  changeParagraphIndentLevelForTextDocument,
  clearParagraphFormattingForTextDocument,
  pasteFormattingForTextDocument,
  setParagraphAlignForTextDocument,
  setParagraphNumberingForTextDocument,
  setParagraphSpacingForTextDocument,
  setParagraphTextDirectionForTextDocument,
} from '../../TextDocument/utils/operations'
import { TextPrOptions } from '../../textAttributes/TextPr'
import { RunContentElement } from '../../Paragraph/RunElement/type'
import { ParaSpacingOptions } from '../../textAttributes/ParaSpacing'
import { ParaIndOptions } from '../../textAttributes/ParaInd'

export function addNewParagraph(presentation: Presentation) {
  applyTextOperationFunction(
    presentation.selectionState,
    addNewParagraphInTextDocument,
    []
  )
}

export function applyTextPrToParagraph(
  presentation: Presentation,
  textPr: TextPrOptions,
  isDirty?: boolean
) {
  applyTextOperationFunction(
    presentation.selectionState,
    applyTextPrToParagraphInTextDocument,
    [textPr, isDirty]
  )
}

export function addItemToParagraph(
  presentation: Presentation,
  item: RunContentElement,
  isDirty?: boolean
) {
  applyTextOperationFunction(
    presentation.selectionState,
    addToParagraphInTextDocument,
    [item, isDirty]
  )
}

export function setParagraphTextDirection(
  presentation: Presentation,
  isRTL: boolean
) {
  applyTextDocumentOperationFunction(
    presentation.selectionState,
    setParagraphTextDirectionForTextDocument,
    [isRTL]
  )
}

export function setAlignForParagraph(presentation: Presentation, align) {
  applyTextDocumentOperationFunction(
    presentation.selectionState,
    setParagraphAlignForTextDocument,
    [align]
  )
}

export function clearFormattingForparagraph(
  presentation: Presentation,
  isFormatParaPr,
  isFormatTextPr
) {
  applyTextDocumentOperationFunction(
    presentation.selectionState,
    clearParagraphFormattingForTextDocument,
    [isFormatParaPr, isFormatTextPr]
  )
}

export function setSpacingForParagraph(
  presentation: Presentation,
  spacing: ParaSpacingOptions
) {
  applyTextDocumentOperationFunction(
    presentation.selectionState,
    setParagraphSpacingForTextDocument,
    [spacing]
  )
}

export function setParagraphNumbering(presentation: Presentation, numInfo) {
  applyTextDocumentOperationFunction(
    presentation.selectionState,
    setParagraphNumberingForTextDocument,
    [numInfo]
  )
}

export function setIndentForParagraph(
  presentation: Presentation,
  indent: ParaIndOptions
) {
  if (isNumber(indent.left) && indent.left < 0) {
    indent.left = 0
  }
  applyTextDocumentOperationFunction(
    presentation.selectionState,
    setParagraphIndentForTextDocument,
    [indent]
  )
}

export function changeFontSizeParagraph(
  presentation: Presentation,
  isIncrease
) {
  applyTextDocumentOperationFunction(
    presentation.selectionState,
    changeFontSizeForTextDocument,
    [isIncrease]
  )
}

export function changeIndentLevelForParagraph(
  presentation: Presentation,
  isIncrease
) {
  applyTextDocumentOperationFunction(
    presentation.selectionState,
    changeParagraphIndentLevelForTextDocument,
    [isIncrease]
  )
}

export function pasteParagraphFormat(
  presentation: Presentation,
  textPr,
  paraPr
) {
  applyTextOperationFunction(
    presentation.selectionState,
    pasteFormattingForTextDocument,
    [textPr, paraPr]
  )
}
