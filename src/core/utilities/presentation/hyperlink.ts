import { EditActionFlag } from '../../EditActionFlag'
import { startEditAction } from '../../calculation/informChanges/startAction'
import { calculateForRendering } from '../../calculation/calculate/calculateForRendering'
import { Presentation } from '../../Slide/Presentation'
import { checkExtentsByTextContent } from '../shape/extents'
import { HyperlinkOptions } from '../../properties/options'

export function canInsertHyperlink(
  presentation: Presentation,
  isCheckInHyperlink?: boolean
) {
  const textDoc = presentation.getCurrentTextTarget()
  if (textDoc) {
    return textDoc.canInsertHyperlink(isCheckInHyperlink)
  }
  return false
}

export function removeHyperLinkFunc(presentation: Presentation) {
  startEditAction(EditActionFlag.action_Presentation_HyperlinkRemove)
  const textDoc = presentation.getCurrentTextTarget()
  if (textDoc) {
    textDoc.removeHyperlink()
    const selectedTextSp = presentation.selectionState.selectedTextSp
    if (selectedTextSp) {
      checkExtentsByTextContent(selectedTextSp)
    }
  }

  calculateForRendering()
}

export function updateHyperlinkFunc(
  presentation: Presentation,
  hyperlinkOptions
) {
  startEditAction(EditActionFlag.action_Presentation_HyperlinkModify)
  const textDoc = presentation.getCurrentTextTarget()
  if (textDoc) {
    textDoc.updateHyperlink(hyperlinkOptions)
    const selectedTextSp = presentation.selectionState.selectedTextSp
    if (selectedTextSp) {
      checkExtentsByTextContent(selectedTextSp)
    }
  }

  calculateForRendering()
}

export function insertHyperlinkFunc(
  presentation: Presentation,
  hyperlinkOptions: HyperlinkOptions
) {
  startEditAction(EditActionFlag.action_Presentation_HyperlinkAdd)
  let isCheckExtents = false
  const textDoc = presentation.getCurrentTextTarget()
  if (textDoc) {
    if (
      null != hyperlinkOptions.text &&
      '' !== hyperlinkOptions.text &&
      true === textDoc.hasTextSelection()
    ) {
      presentation.onRemove(-1, undefined)
      isCheckExtents = true
    }

    textDoc.insertHyperlink(hyperlinkOptions)
    if (isCheckExtents) {
      const selectedTextSp = presentation.selectionState.selectedTextSp
      if (selectedTextSp) {
        checkExtentsByTextContent(selectedTextSp)
      }
    }
  }

  calculateForRendering()
}
