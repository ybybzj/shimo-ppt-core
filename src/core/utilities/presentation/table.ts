import { Nullable } from '../../../../liber/pervasive'
import { isNumber } from '../../../common/utils'
import { InstanceType } from '../../instanceTypes'
import { calculateForRendering } from '../../calculation/calculate/calculateForRendering'
import { Presentation } from '../../Slide/Presentation'
import { Table } from '../../Table/Table'
import {
  addColumnToTable,
  addRowToTable,
  distributeCellsForTable,
  mergeCellsForTable,
  removeColumnOfTable,
  removeRowOfTable,
  splitCellsForTable,
} from '../../Table/utils/operations'
import { moveCursorToStartPosInTable } from '../cursor/moveToEdgePos'
import { setDeletedForSp } from '../shape/updates'
import { getSelectedSlideElementsByTypes } from './getters'

function applyTableOperation(
  presentation: Presentation,
  tableOpFn: (table: Table, ...args: any[]) => any,
  isBefore?: boolean,
  isAll?: boolean,
  numOfCols?: number,
  numOfRows?: number
): { result: any; isApplied: boolean } {
  let result
  let table: Nullable<Table>

  if (presentation.slides[presentation.currentSlideIndex]) {
    let args: any[]
    if (isNumber(numOfRows) && isNumber(numOfCols)) {
      args = [numOfRows, numOfCols]
    } else {
      args = [isBefore]
    }
    const selectionState = presentation.selectionState
    const selectedGraphicFrame =
      selectionState.selectedTextSp ?? selectionState.selectedTable?.parent

    if (
      selectedGraphicFrame &&
      selectedGraphicFrame.instanceType === InstanceType.GraphicFrame
    ) {
      table = selectedGraphicFrame.graphicObject!
      result = tableOpFn.apply(void 0, [table, ...args])
    } else {
      const spsBytypes = getSelectedSlideElementsByTypes(presentation, true)
      if (spsBytypes.tables.length === 1) {
        const graphicFrame = spsBytypes.tables[0]
        const table = graphicFrame.graphicObject!
        if (tableOpFn !== distributeCellsForTable) {
          graphicFrame.selectSelf()
          if (!(isAll === true)) {
            if (isBefore) {
              moveCursorToStartPosInTable(table)
            } else {
              moveCursorToStartPosInTable(table)
            }
          } else {
            table.selectAll()
          }
        }

        result = tableOpFn.apply(void 0, [table, ...args])
      }
    }

    if (table) {
      if (table.children.length === 0) {
        const removed = removeTable(presentation)
        return { result, isApplied: removed }
      }
      calculateForRendering()
    }
  }
  return { result, isApplied: !!table }
}

export function removeTable(presentation: Presentation): boolean {
  let removed = false
  if (presentation.slides[presentation.currentSlideIndex]) {
    const spsBytypes = getSelectedSlideElementsByTypes(presentation, true)
    if (spsBytypes.tables.length === 1) {
      const selectionState = presentation.selectionState
      if (selectionState) {
        selectionState.deselect(spsBytypes.tables[0])
        selectionState.resetRootSelection()
      }
      if (spsBytypes.tables[0].group) {
        spsBytypes.tables[0].group.removeSpById(spsBytypes.tables[0].id)
      } else {
        presentation.slides[presentation.currentSlideIndex].removeSpById(
          spsBytypes.tables[0].id
        )
      }
      setDeletedForSp(spsBytypes.tables[0], true)
      calculateForRendering()
      removed = true
    }
  }
  return removed
}

export function addTableRow(presentation: Presentation, isBefore) {
  return applyTableOperation(presentation, addRowToTable, isBefore)
}

export function addTableColumn(presentation: Presentation, isBefore) {
  return applyTableOperation(presentation, addColumnToTable, isBefore)
}

export function removeTableRow(presentation: Presentation) {
  return applyTableOperation(presentation, removeRowOfTable)
}

export function removeTableColumn(presentation: Presentation) {
  return applyTableOperation(presentation, removeColumnOfTable, true)
}

export function distributeTableCells(presentation: Presentation, isHorizontal) {
  return applyTableOperation(
    presentation,
    distributeCellsForTable,
    isHorizontal
  )
}

export function mergeTableCells(presentation: Presentation) {
  return applyTableOperation(presentation, mergeCellsForTable, false, true)
}

export function splitTableCells(
  presentation: Presentation,
  numOfCols,
  numOfRows
) {
  return applyTableOperation(
    presentation,
    splitCellsForTable,
    true,
    true,
    parseInt(numOfCols, 10),
    parseInt(numOfRows, 10)
  )
}
