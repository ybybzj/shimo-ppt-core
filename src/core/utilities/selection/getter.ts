import { Nullable } from '../../../../liber/pervasive'
import { stringFromCharCode } from '../../../common/utils'
import { InstanceType } from '../../instanceTypes'
import { ParaHyperlink } from '../../Paragraph/ParaContent/ParaHyperlink'
import { ParaRun } from '../../Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../../Paragraph/Paragraph'
import { TextDocument } from '../../TextDocument/TextDocument'

export function getSelectedTextOfTextDocument(
  textDoc: TextDocument,
  isClearText?: boolean,
  pr?: { insertNewLine?: boolean; insertNewLineAtParaEnd?: boolean }
) {
  let para: Nullable<Paragraph>
  if (true === textDoc.isSetToAll) {
    if (true === isClearText && textDoc.children.length === 1) {
      para = textDoc.children.at(0)!
      para.isSetToAll = true
      const result = getSelectedTextOfParagraph(para, true, pr)
      para.isSetToAll = false
      return result
    } else if (true !== isClearText && textDoc.children.length > 1) {
      let result = ''
      const l = textDoc.children.length
      for (let i = 0; i < l; i++) {
        para = textDoc.children.at(i)!
        para.isSetToAll = true
        result += getSelectedTextOfParagraph(para, false, pr)
        para.isSetToAll = false
      }

      return result
    }
  } else {
    if (true === textDoc.selection.isUse || false === textDoc.selection.isUse) {
      if (
        true === isClearText &&
        (textDoc.selection.startIndex === textDoc.selection.endIndex ||
          false === textDoc.selection.isUse)
      ) {
        const index =
          true === textDoc.selection.isUse
            ? textDoc.selection.startIndex
            : textDoc.cursorPosition.childIndex
        para = textDoc.children.at(index)
        if (para) return getSelectedTextOfParagraph(para, true, pr)
      } else if (false === isClearText) {
        const startIndex =
          true === textDoc.selection.isUse
            ? Math.min(textDoc.selection.startIndex, textDoc.selection.endIndex)
            : textDoc.cursorPosition.childIndex
        const endIndex =
          true === textDoc.selection.isUse
            ? Math.max(textDoc.selection.startIndex, textDoc.selection.endIndex)
            : textDoc.cursorPosition.childIndex

        let result = ''

        for (let i = startIndex; i <= endIndex; i++) {
          para = textDoc.children.at(i)
          if (para) result += getSelectedTextOfParagraph(para, false, pr)
        }

        return result
      }
    }
  }

  return null
}

function getSelectedTextOfParagraph(
  para: Paragraph,
  isClearText?: boolean,
  pr?: { insertNewLine?: boolean; insertNewLineAtParaEnd?: boolean }
) {
  let str = ''
  const l = para.children.length
  for (let i = 0; i < l; i++) {
    const paraItem = para.children.at(i)!
    const _str =
      paraItem.instanceType === InstanceType.ParaRun
        ? getSelectedTextOfParaRun(
            paraItem,
            true === para.isSetToAll,
            isClearText,
            pr
          )
        : getSelectedTextOfParaHyperlink(
            paraItem,
            true === para.isSetToAll,
            isClearText,
            pr
          )

    if (null == _str) return null

    str += _str
  }

  return str
}

function getSelectedTextOfParaRun(
  run: ParaRun,
  isAll: boolean,
  isClearText?: boolean,
  pr?: { insertNewLine?: boolean; insertNewLineAtParaEnd?: boolean }
) {
  let startIndex = 0
  let endIndex = 0

  if (true === isAll) {
    startIndex = 0
    endIndex = run.children.length
  } else if (true === run.selection.isUse) {
    startIndex = run.state.selection.startIndex
    endIndex = run.state.selection.endIndex

    if (startIndex > endIndex) {
      const t = endIndex
      endIndex = startIndex
      startIndex = t
    }
  }

  let str = ''

  for (let i = startIndex; i < endIndex; i++) {
    const item = run.children.at(i)
    if (item == null) continue
    const type = item.instanceType

    switch (type) {
      case InstanceType.ParaText: {
        str += stringFromCharCode(item.value)
        break
      }
      case InstanceType.ParaSpace:
      case InstanceType.ParaTab:
        str += ' '
        break
      case InstanceType.ParaNewLine: {
        if (pr && true === pr.insertNewLine) {
          str += '\r'
        }
        break
      }
      case InstanceType.ParaEnd: {
        if (pr && true === pr.insertNewLineAtParaEnd) {
          if (
            run.paragraph &&
            null === run.paragraph.getNextPara() &&
            run.paragraph.parent &&
            true === run.paragraph.parent.isInTableCell() &&
            true !== run.paragraph.parent.isInLastCellOfRow()
          ) {
            str += '\t'
          } else {
            str += '\r\n'
          }
        }

        break
      }
      default: {
        if (true === isClearText) return null

        break
      }
    }
  }

  return str
}

function getSelectedTextOfParaHyperlink(
  hyperlink: ParaHyperlink,
  isAll: boolean,
  isClearText?: boolean,
  pr?: { insertNewLine?: boolean; insertNewLineAtParaEnd?: boolean }
) {
  let str = ''
  for (let i = 0, l = hyperlink.children.length; i < l; i++) {
    const s = getSelectedTextOfParaRun(
      hyperlink.children.at(i)!,
      isAll,
      isClearText,
      pr
    )

    if (null === s) return null

    str += s
  }

  return str
}
