import { Nullable } from '../../../../liber/pervasive'
import { LineHeightRule } from '../../common/const/attrs'
import {
  PointerEventInfo,
  PointerEvtTypeValue,
  POINTER_EVENT_TYPE,
} from '../../../common/dom/events'
import { isFiniteNumber } from '../../../common/utils'
import { EditActionFlag } from '../../EditActionFlag'
import { InstanceType } from '../../instanceTypes'
import { TextContentDimensionLimit } from '../../common/const/paragraph'
import { TableSelectionType } from '../../common/const/table'
import { MatrixUtils } from '../../graphic/Matrix'
import { hookManager, Hooks } from '../../hooks'
import { ParagraphLocationInfo } from '../../Paragraph/common'
import { Paragraph } from '../../Paragraph/Paragraph'
import { ParagraphItem } from '../../Paragraph/type'
import {
  findWordEndPos,
  findWordStartPos,
  getEndPosOfParagraph,
  getSearchPositionInfoFromParagraph,
  getStartPosOfParagraph,
} from '../../Paragraph/utils/position'
import {
  correctParaSelectionState,
  getCurrentFocusElementsInfoForParagraph,
  updateSelectionBeginAndEndForParagraph,
  updateSelectionStateForParagraph,
} from '../../Paragraph/utils/selection'
import { startEditAction } from '../../calculation/informChanges/startAction'
import { calculateTableGrid } from '../../calculation/calculate/table'
import { calculateForRendering } from '../../calculation/calculate/calculateForRendering'
import { CurrentFocusElementsInfo } from '../../Slide/CurrentFocusElementsInfo'
import { Table } from '../../Table/Table'
import { rowContentIterator, hitInTableBorder } from '../../Table/utils/utils'
import { TextDocument } from '../../TextDocument/TextDocument'
import { getAndUpdateCursorPositionForPresentation } from '../cursor/cursorPosition'
import { moveCursorToXYInParagraph } from '../cursor/moveToXY'
import { TableOrTextDocument, TextOperationDirection } from '../type'
import { PPTPositionStateCollection } from '../tableOrTextDocContent/position'
import {
  getPosIndexByDocPosInfo,
  validateSelectionPosition,
} from '../tableOrTextDocContent/helpers'
import { getPresentation } from '../finders'
import { RowInfo, TableSelectionInfo } from '../../Table/type'
import { ParaRun } from '../../Paragraph/ParaContent/ParaRun'
import { EditorSkin } from '../../common/const/drawing'
import { ManagedSliceUtil } from '../../../common/managedArray'
import { getMinAndMaxArrayValue } from '../../../ui/features/guideLines/utils'

export function setSelectionEndForTable(
  table: Table,
  x: number,
  y: number,
  mouseEvent: PointerEventInfo,
  isInTextEditing: boolean
) {
  if (table.selection.selectionType === TableSelectionType.BORDER) {
    return checkBorderSelection(table, x, y, mouseEvent)
  }

  const cellPos = table.getCellByPoint(x, y)
  const pos = {
    row: cellPos.row,
    cell: cellPos.cell,
  }

  switch (table.selection.selectionType) {
    case TableSelectionType.ROWS:
      pos.cell = table.getRow(pos.row)!.cellsCount - 1
      break
    case TableSelectionType.COLUMNS:
      const row = table.getRow(cellPos.row)!
      const valueOfEndRow = table.getRowsCount() - 1
      const valueOfEndCell = table.getCellIndexByGridColIndex(
        valueOfEndRow,
        row.getCellInfo(cellPos.cell).startGridCol,
        true
      )
      if (-1 !== valueOfEndCell) {
        pos.row = valueOfEndRow
        pos.cell = valueOfEndCell
      }
      break
  }

  table.children.at(pos.row)!.getCellByIndex(pos.cell)!.setCursorPosXY(x, y)
  table.selection.cellPositions = []
  table.selection.selectionEndInfo.cellPos = pos
  table.selection.selectionEndInfo.x = x
  table.selection.selectionEndInfo.y = y
  table.selection.selectionEndInfo.mouseEvtInfo = {
    clicks: mouseEvent.clicks,
    type: mouseEvent.type,
    ctrl: mouseEvent.ctrlKey,
  }
  table.selection.currentRowIndex = pos.row

  // When selecting within a cell, we select the contents of the cell
  if (
    TableSelectionType.COMMON === table.selection.selectionType &&
    table.parent.isOnlyOneElementSelected() &&
    isInTextEditing &&
    table.selection.selectionStartInfo.cellPos.row ===
      table.selection.selectionEndInfo.cellPos.row &&
    table.selection.selectionStartInfo.cellPos.cell ===
      table.selection.selectionEndInfo.cellPos.cell
  ) {
    const e = new PointerEventInfo()
    e.clicks = table.selection.selectionStartInfo.mouseEvtInfo.clicks
    e.type = table.selection.selectionStartInfo.mouseEvtInfo.type
    e.ctrlKey = table.selection.selectionStartInfo.mouseEvtInfo.ctrl
    table.curCell!.setContentSelectionStart(
      table.selection.selectionStartInfo.x,
      table.selection.selectionStartInfo.y,
      new PointerEventInfo()
    )

    table.selection.type = TableSelectionType.TEXT

    const curCell = table.curCell!
    let transform = curCell.getTextTransformByDirection()
    let _x = x
    let _y = y
    if (null != transform) {
      transform = MatrixUtils.invertMatrix(transform)
      _x = transform.XFromPoint(x, y)
      _y = transform.YFromPoint(x, y)
    }
    setSelectionEndForTextDocument(curCell.textDoc, _x, _y, mouseEvent)

    if (POINTER_EVENT_TYPE.Up === mouseEvent.type) {
      table.selection.start = false
    }

    if (false === table.curCell!.textDoc.selection.isUse) {
      table.selection.isUse = false
      table.selection.start = false
      moveCursorToXYInTable(table, x, y)
      return
    }
  } else {
    if (POINTER_EVENT_TYPE.Up === mouseEvent.type) {
      table.selection.start = false
      table.curCell = table.children.at(pos.row)!.getCellByIndex(pos.cell)

      if (
        TableSelectionType.CELLS === table.selection.selectionType &&
        table.selection.selectionStartInfo.cellPos.cell ===
          table.selection.selectionEndInfo.cellPos.cell &&
        table.selection.selectionStartInfo.cellPos.row ===
          table.selection.selectionEndInfo.cellPos.row &&
        mouseEvent.clicks > 1 &&
        0 === mouseEvent.clicks % 2
      ) {
        table.selection.selectionStartInfo.cellPos.cell = 0
        table.selection.selectionEndInfo.cellPos.cell =
          table.getRow(table.selection.selectionStartInfo.cellPos.row)!
            .cellsCount - 1
      }
    }

    table.selection.type = TableSelectionType.CELL
    if (mouseEvent.isTouchMode()) {
      table.selection.start = false
    }
    table.updateSelectedCellPositions(
      TableSelectionType.ROWS === table.selection.selectionType,
      false
    )
  }
}

function checkBorderSelection(
  table: Table,
  x: number,
  y: number,
  mouseEvent: PointerEventInfo
) {
  const dimension = table.dimension
  const reducedTableGrid = table.getTableGridReduced()
  const presentation = getPresentation(table)
  if (
    !presentation ||
    true !== presentation.canEdit() ||
    table.selection.selectionInfo == null
  ) {
    return
  }

  let _x = x
  let _y = y

  // Check if you accidentally click on the border. (i.e. random single tap or small offset)
  if (
    true !== table.selection.selectionInfo.start ||
    Math.abs(x - table.selection.selectionInfo.startX) > 0.05 ||
    Math.abs(y - table.selection.selectionInfo.startY) > 0.05
  ) {
    _x = parseFloat(x.toFixed(2))
    _y = parseFloat(y.toFixed(2))
    table.selection.selectionInfo.start = false
  } else {
    _x = table.selection.selectionInfo.x
    _y = table.selection.selectionInfo.y
  }

  table.selection.selectionInfo.x = _x
  table.selection.selectionInfo.y = _y

  // Handle the case when the border has not changed its original position
  if (
    Math.abs(_x - table.selection.selectionInfo.startCenterX) < 0.001 &&
    Math.abs(_y - table.selection.selectionInfo.startCenterY) < 0.001
  ) {
    table.selection.selectionType = TableSelectionType.COMMON
    table.selection.selectionInfo = null

    return
  }

  const calcMinColDx = () => {
    const index = table.selection.selectionInfo!.index
    let rowIndex = table.selection.selectionInfo!.cellPos.row
    const row = table.children.at(rowIndex)!
    let colIndex = 0
    if (index === table.rowsAndColsMarkInfo.cols.length) {
      colIndex =
        row.getCellInfo(index - 1).curGridCol +
        row.getCellByIndex(index - 1)!.getGridSpan()
    } else {
      colIndex = row.getCellInfo(index).curGridCol
    }
    let dx = _x - (dimension.x + reducedTableGrid[colIndex - 1])
    const initDx = dx

    // 修正 Dx 的值，单元格 W 不小于左右 Margin 和
    const l = table.children.length
    for (rowIndex = 0; rowIndex < l; rowIndex++) {
      const row = table.children.at(rowIndex)!
      let tmpDx = dx
      let isFromLeft = true
      let isFromRight = false
      rowContentIterator(table.isRTL, row, (cell) => {
        const cellMargins = cell.getMargins()
        const curStartGrid = row.getCellInfo(cell.index).curGridCol
        const curEndGrid = curStartGrid + cell.getGridSpan() - 1

        const minCellWidth = cellMargins.left!.w + cellMargins.right!.w

        let w = 0
        if (isFromLeft) {
          if (curStartGrid === colIndex) {
            isFromLeft = false
            isFromRight = false
            const curW =
              reducedTableGrid[curEndGrid] - reducedTableGrid[colIndex - 1]
            let destW = curW - dx
            if (destW < minCellWidth) {
              destW = minCellWidth
            }
            w = destW
            dx = curW - destW
          } else {
            if (
              (curEndGrid + 1 < colIndex &&
                reducedTableGrid[colIndex - 1] - reducedTableGrid[curEndGrid] <
                  0.635) ||
              curEndGrid + 1 === colIndex ||
              (curEndGrid + 1 > colIndex &&
                reducedTableGrid[curEndGrid] - reducedTableGrid[colIndex - 1] <
                  0.635)
            ) {
              isFromLeft = false
              const curW =
                reducedTableGrid[colIndex - 1] -
                reducedTableGrid[curStartGrid - 1]
              let destW = curW + dx
              if (destW < minCellWidth) {
                destW = minCellWidth
              }
              w = destW
              dx = destW - curW
            }

            if (isFromLeft) {
              w =
                reducedTableGrid[curEndGrid] -
                reducedTableGrid[curStartGrid - 1]
            }

            if (!isFromLeft) {
              tmpDx =
                w -
                (reducedTableGrid[colIndex - 1] -
                  reducedTableGrid[curStartGrid - 1])
              isFromRight = true
            }
          }
        } else if (isFromRight) {
          isFromRight = false
          const curW =
            reducedTableGrid[curEndGrid] - reducedTableGrid[curStartGrid - 1]
          let destW = curW - tmpDx
          if (destW < minCellWidth) {
            destW = minCellWidth
          }
          w = destW
          dx = curW - destW
        } else {
          w = reducedTableGrid[curEndGrid] - reducedTableGrid[curStartGrid - 1]
        }
      })
    }
    return { initDx, dx, colIndex }
  }

  if (mouseEvent.type === POINTER_EVENT_TYPE.Up) {
    startEditAction(EditActionFlag.action_Document_MoveTableBorder)
    if (true === table.selection.selectionInfo.isColumn) {
      // 拖动列边框

      const { dx, colIndex } = calcMinColDx()

      const rowsInfo: Array<RowInfo[]> = []
      ManagedSliceUtil.forEach(
        table.children,
        (row, currentRowIndex: number) => {
          rowsInfo[currentRowIndex] = []
          let tmpDx = dx
          let isFromLeft = true
          let isFromRight = false
          rowContentIterator(table.isRTL, row, (cell) => {
            const curStartGrid = row.getCellInfo(cell.index).curGridCol
            const curEndGrid = curStartGrid + cell.getGridSpan() - 1

            let w = 0
            if (isFromLeft) {
              if (curStartGrid === colIndex) {
                isFromLeft = false
                isFromRight = false
                w =
                  reducedTableGrid[curEndGrid] -
                  reducedTableGrid[colIndex - 1] -
                  dx
              } else {
                if (
                  (curEndGrid + 1 < colIndex &&
                    reducedTableGrid[colIndex - 1] -
                      reducedTableGrid[curEndGrid] <
                      0.635) ||
                  curEndGrid + 1 === colIndex ||
                  (curEndGrid + 1 > colIndex &&
                    reducedTableGrid[curEndGrid] -
                      reducedTableGrid[colIndex - 1] <
                      0.635)
                ) {
                  isFromLeft = false
                  w =
                    reducedTableGrid[colIndex - 1] -
                    reducedTableGrid[curStartGrid - 1] +
                    dx
                }

                if (isFromLeft) {
                  w =
                    reducedTableGrid[curEndGrid] -
                    reducedTableGrid[curStartGrid - 1]
                } else {
                  tmpDx =
                    w -
                    (reducedTableGrid[colIndex - 1] -
                      reducedTableGrid[curStartGrid - 1])
                  isFromRight = true
                }
              }
            } else if (isFromRight) {
              isFromRight = false
              w =
                reducedTableGrid[curEndGrid] -
                reducedTableGrid[curStartGrid - 1] -
                tmpDx
            } else {
              w =
                reducedTableGrid[curEndGrid] -
                reducedTableGrid[curStartGrid - 1]
            }

            rowsInfo[currentRowIndex].push({ w: w, type: 0, gridSpan: 1 })
          })
          if (table.isRTL) {
            rowsInfo[currentRowIndex].reverse()
          }
        }
      )

      table.createTableGrid(rowsInfo)
      calculateTableGrid(table)
    } else {
      // 拖动行边框
      const rowIndex = table.selection.selectionInfo.index
      if (0 === rowIndex) {
        const dy = _y - table.rowsAndColsMarkInfo.rows[0].y
        dimension.y += dy
        _updateTablePosition(table, dimension.x, dimension.y)
      } else {
        const row = table.children.at(rowIndex - 1)!
        const _oldY =
          table.rowsAndColsMarkInfo.rows[
            table.selection.selectionInfo.index - 1
          ].y +
          table.rowsAndColsMarkInfo.rows[
            table.selection.selectionInfo.index - 1
          ].h
        const dy = _y - _oldY
        const totalH =
          table.rowsAndColsMarkInfo.rows[
            table.selection.selectionInfo.index - 1
          ].h + dy
        const minRowHeight = table.getMinRowHeight(rowIndex - 1)
        const newRowHeight = Math.max(
          minRowHeight,
          totalH - (row.height - row.getHeight().value)
        )
        row.setHeight({
          value: newRowHeight,
          heightRule: LineHeightRule.AtLeast,
        })
      }
    }

    calculateForRendering()

    table.selection.selectionType = TableSelectionType.COMMON
    table.selection.selectionInfo = null
  } else if (mouseEvent.type === POINTER_EVENT_TYPE.Move) {
    if (true === table.selection.selectionInfo.isColumn) {
      // 拖动列边框
      const { dx, initDx } = calcMinColDx()

      const pos = _x - (initDx - dx) + table.parent.snapXs[0]
      hookManager.invoke(Hooks.EditorUI.OnAddGuideline, {
        line: {
          pos,
          rangePos: getMinAndMaxArrayValue(table.parent.snapYs),
          offset: 0,
          color: EditorSkin.Color_GuideLine_Light,
          isVertical: true,
        },
      })
    } else {
      // 拖动行边框
      const rowIndex = table.selection.selectionInfo.index
      if (0 !== rowIndex) {
        const targetRowIndex = rowIndex - 1
        const rowHYInfo =
          table.rowsAndColsMarkInfo.rows[
            table.selection.selectionInfo.index - 1
          ]
        const minYPos = rowHYInfo.y + table.getMinRowHeight(targetRowIndex)
        const pos = table.parent.y + (_y < minYPos ? minYPos : _y)

        hookManager.invoke(Hooks.EditorUI.OnAddGuideline, {
          line: {
            pos,
            rangePos: getMinAndMaxArrayValue(table.parent.snapXs),
            offset: 0,
            color: EditorSkin.Color_GuideLine_Light,
          },
        })
      }
    }
  }
}

export function setSelectionEndForTextDocument(
  textDoc: TextDocument,
  x: number,
  y: number,
  mouseEvent: {
    type: PointerEvtTypeValue
    clicks: number
  }
) {
  if (textDoc.renderingState == null) return

  if (false === textDoc.selection.isUse) return

  const selection = textDoc.selection
  const elementIndex = textDoc.getElementIndexByXY(x, y)
  textDoc.cursorPosition.childIndex = elementIndex
  const orgEndIndex = selection.endIndex
  selection.endIndex = elementIndex

  if (orgEndIndex < selection.startIndex && orgEndIndex < selection.endIndex) {
    const _endIndex = Math.min(selection.startIndex, selection.endIndex)
    for (let i = orgEndIndex; i < _endIndex; i++) {
      const para = textDoc.children.at(i)
      para && para.removeSelection()
    }
  } else if (
    orgEndIndex > selection.startIndex &&
    orgEndIndex > selection.endIndex
  ) {
    const _startIndex = Math.max(selection.startIndex, selection.endIndex)
    for (let i = _startIndex + 1; i <= orgEndIndex; i++) {
      const para = textDoc.children.at(i)
      para && para.removeSelection()
    }
  }

  const [Dir_Left, Dir_Origin, Dir_Right] = [-1, 0, 1] as const
  const direction =
    elementIndex > selection.startIndex
      ? Dir_Right
      : elementIndex < selection.startIndex
        ? Dir_Left
        : Dir_Origin

  if (POINTER_EVENT_TYPE.Up === mouseEvent.type) {
    if (true === textDoc.selection.isUse) {
      textDoc.selection.start = false
      const para = textDoc.children.at(textDoc.selection.startIndex)
      if (para) {
        para.selection.start = false
      }
    }
  }

  let start: number, end: number
  if (Dir_Origin === direction) {
    const para = textDoc.children.at(selection.startIndex)
    const colIndex = textDoc.getElementColIndexByXY(selection.startIndex, x)
    para && setSelectionEndForParagraph(para, x, y, colIndex, mouseEvent)

    if (para && false === para.hasTextSelection()) {
      selection.isUse = false
    } else {
      selection.isUse = true
    }

    return
  } else if (direction > 0) {
    start = selection.startIndex
    end = selection.endIndex
  } else {
    end = selection.startIndex
    start = selection.endIndex
  }

  const startPara = textDoc.children.at(start)
  if (
    startPara &&
    direction === Dir_Right &&
    true === startPara.hasNoSelection() &&
    startPara.selection.startIndex === startPara.children.length - 1
  ) {
    startPara.selection.startIndex = 0
    startPara.selection.endIndex = startPara.children.length - 1
  }
  const elementColIndex = textDoc.getElementColIndexByXY(elementIndex, x)
  const para = textDoc.children.at(elementIndex)
  para && setSelectionEndForParagraph(para, x, y, elementColIndex, mouseEvent)

  for (let index = start; index <= end; index++) {
    const para = textDoc.children.at(index)
    if (para) {
      para.selection.isUse = true

      switch (index) {
        case start:
          updateSelectionBeginAndEndForParagraph(
            para,
            direction === Dir_Right ? false : true,
            false
          )
          break
        case end:
          updateSelectionBeginAndEndForParagraph(
            para,
            direction === Dir_Right ? true : false,
            true
          )
          break
        default:
          para.selectAll(direction)
          break
      }
    }
  }
}

function moveCursorToXYInTextDocument(
  textDoc: TextDocument,
  x: number,
  y: number,
  isAddToSelect: boolean
) {
  if (textDoc.renderingState == null) return

  if (true === textDoc.selection.isUse) {
    if (true === isAddToSelect) {
      const mouseEvent = new PointerEventInfo()
      mouseEvent.type = POINTER_EVENT_TYPE.Up
      setSelectionEndForTextDocument(textDoc, x, y, mouseEvent)
    } else {
      textDoc.removeSelection()

      const elmIndex = textDoc.getElementIndexByXY(x, y)
      textDoc.cursorPosition.childIndex = elmIndex
      const para = textDoc.children.at(elmIndex)
      para && moveCursorToXYInParagraph(para, x, y, false, false)

      textDoc.onParaPrUpdate()
      textDoc.onTextPrUpdate()
    }
  } else {
    if (true === isAddToSelect) {
      textDoc.startSelectFromCurrentPosition()
      const mouseEvent = new PointerEventInfo()
      mouseEvent.type = POINTER_EVENT_TYPE.Up
      setSelectionEndForTextDocument(textDoc, x, y, mouseEvent)
    } else {
      const elmIndex = textDoc.getElementIndexByXY(x, y)
      textDoc.cursorPosition.childIndex = elmIndex

      const para = textDoc.children.at(elmIndex)
      para && moveCursorToXYInParagraph(para, x, y, false, false)

      textDoc.onParaPrUpdate()
      textDoc.onTextPrUpdate()
    }
  }
}

function moveCursorToXYInTable(table: Table, x: number, y: number) {
  const pos = table.getCellByPoint(x, y)
  const row = table.children.at(pos.row)
  const cell = row?.getCellByIndex(pos.cell)
  if (cell == null) {
    return
  }

  table.selection.type = TableSelectionType.TEXT
  table.selection.selectionType = TableSelectionType.COMMON
  table.selection.selectionStartInfo.cellPos = { row: pos.row, cell: pos.cell }
  table.selection.selectionEndInfo.cellPos = { row: pos.row, cell: pos.cell }
  table.selection.currentRowIndex = pos.row

  table.curCell = cell
  hookManager.invoke(Hooks.EditorUI.OnCursorShow)

  let transform = cell.getTextTransformByDirection()
  if (null != transform) {
    transform = MatrixUtils.invertMatrix(transform)
    x = transform.XFromPoint(x, y)
    y = transform.YFromPoint(x, y)
  }
  moveCursorToXYInTextDocument(cell.textDoc, x, y, false)
  const presentation = getPresentation(table)
  if (presentation) {
    getAndUpdateCursorPositionForPresentation(presentation)
  }
}
export function setSelectionEndForParagraph(
  paragraph: Paragraph,
  x: number,
  y: number,
  colIndex: number,
  mouseEvent: { type: PointerEvtTypeValue; clicks: number }
) {
  paragraph.cursorPosition.cursorX = x
  paragraph.cursorPosition.cursorY = y

  paragraph.selection.endBySet = true

  const searchInfoForCenter = getSearchPositionInfoFromParagraph(
    paragraph,
    x,
    y,
    colIndex,
    false,
    true
  )
  const searchInfo = getSearchPositionInfoFromParagraph(
    paragraph,
    x,
    y,
    colIndex,
    false,
    false
  )

  paragraph.updateCursorPosByPosition(searchInfo.pos, true, searchInfo.line)

  updateSelectionStateForParagraph(
    paragraph,
    paragraph.currentElementPosition(true, true),
    searchInfoForCenter.pos
  )

  const selectionStartPosition = paragraph.currentElementPosition(true, true)
  const selectionEndPosition = paragraph.currentElementPosition(true, false)

  if (
    0 === selectionStartPosition.compare(selectionEndPosition) &&
    POINTER_EVENT_TYPE.Up === mouseEvent.type
  ) {
    const info = new CurrentFocusElementsInfo()
    getCurrentFocusElementsInfoForParagraph(paragraph, info)

    const clickCount = mouseEvent.clicks % 2

    if (1 >= mouseEvent.clicks) {
      paragraph.removeSelection()
    } else if (0 === clickCount) {
      const startLocInfo = new ParagraphLocationInfo()
      const endLocInfo = new ParagraphLocationInfo()

      findWordEndPos(paragraph, endLocInfo, searchInfoForCenter.pos)
      findWordStartPos(paragraph, startLocInfo, endLocInfo.pos)

      const startPosition =
        true === startLocInfo.found
          ? startLocInfo.pos
          : getStartPosOfParagraph(paragraph)
      const endPosition =
        true === endLocInfo.found
          ? endLocInfo.pos
          : getEndPosOfParagraph(paragraph, false)

      paragraph.selection.isUse = true
      updateSelectionStateForParagraph(paragraph, startPosition, endPosition)
    } // ( 1 == ClickCounter % 2 )
    else {
      paragraph.selectAll(1)
    }
  }
}

function setStartSelectionForParagraph(
  paragraph: Paragraph,
  x: number,
  y: number,
  colIndex: number
) {
  if (true === paragraph.selection.isUse) paragraph.removeSelection()

  const searchInfoForCenter = getSearchPositionInfoFromParagraph(
    paragraph,
    x,
    y,
    colIndex,
    false,
    true
  )
  const searchInfo = getSearchPositionInfoFromParagraph(
    paragraph,
    x,
    y,
    colIndex,
    false,
    false
  )

  paragraph.selection.isUse = true
  paragraph.selection.start = true
  paragraph.selection.startBySet = true

  paragraph.updateCursorPosByPosition(searchInfo.pos, true, searchInfo.line)

  updateSelectionStateForParagraph(
    paragraph,
    searchInfoForCenter.pos,
    searchInfoForCenter.pos
  )
}

export function setSelectionStartForTextDocument(
  textDoc: TextDocument,
  x: number,
  y: number,
  e: PointerEventInfo
) {
  if (textDoc.renderingState == null) return

  const elmIndex = textDoc.getElementIndexByXY(x, y)

  const oldIsSelectionUse = textDoc.selection.isUse
  const para = textDoc.children.at(elmIndex)

  if (!(true === oldIsSelectionUse && true === e.shiftKey)) {
    if (true === textDoc.selection.isUse && e.clicks <= 1) {
      textDoc.removeSelection()
    }
  }

  textDoc.selection.isUse = true
  textDoc.selection.start = true

  if (true === oldIsSelectionUse && true === e.shiftKey) {
    setSelectionEndForTextDocument(textDoc, x, y, {
      type: POINTER_EVENT_TYPE.Up,
      clicks: 1,
    })
    textDoc.selection.isUse = true
    textDoc.selection.start = true
    textDoc.selection.endIndex = elmIndex
  } else {
    const colIndex = textDoc.getElementColIndexByXY(elmIndex, x)
    if (para) {
      setStartSelectionForParagraph(para, x, y, colIndex)

      setSelectionEndForParagraph(para, x, y, colIndex, {
        type: POINTER_EVENT_TYPE.Move,
        clicks: 1,
      })
    }

    textDoc.selection.isUse = true
    textDoc.selection.startIndex = elmIndex
    textDoc.selection.endIndex = elmIndex

    textDoc.cursorPosition.childIndex = elmIndex
  }
}
export function setSelectionStartForTable(
  table: Table,
  x: number,
  y: number,
  mouseEvent: PointerEventInfo,
  isInTextEditing: boolean
) {
  const dimension = table.dimension

  const hitInfo = hitInTableBorder(table, x, y, {
    isInTouchMode: mouseEvent.isTouchMode(),
    isInTextEditing,
  })

  const pos = hitInfo.cellPos
  if (
    hitInfo.selectColum ||
    hitInfo.selectRow ||
    hitInfo.selectCell ||
    (-1 === hitInfo.Border && isInTextEditing !== true)
  ) {
    table.removeSelection()

    table.curCell = table.children.at(pos.row)?.getCellByIndex(pos.cell)
    if (table.curCell) {
      table.curCell.setContentSelectionStart(x, y, mouseEvent)
    }
    table.selection.isUse = true
    table.selection.start = true
    table.selection.type = TableSelectionType.CELL
    table.selection.selectionType = TableSelectionType.CELLS
    table.selection.selectionInfo = null

    table.selection.selectionStartInfo.cellPos = pos
    table.selection.selectionStartInfo.x = x
    table.selection.selectionStartInfo.y = y
    table.selection.selectionStartInfo.mouseEvtInfo = {
      clicks: mouseEvent.clicks,
      type: mouseEvent.type,
      ctrl: mouseEvent.ctrlKey,
    }

    const endCellPos = {
      row: pos.row,
      cell: pos.cell,
    }

    if (hitInfo.selectRow) {
      endCellPos.cell = table.getRow(pos.row)!.cellsCount - 1
      table.selection.selectionType = TableSelectionType.ROWS
    } else if (hitInfo.selectColum) {
      const row = table.getRow(pos.row)!
      const valueOfEndRow = table.getRowsCount() - 1
      const valueOfEndCell = table.getCellIndexByGridColIndex(
        valueOfEndRow,
        row.getCellInfo(pos.cell).startGridCol,
        true
      )

      if (-1 !== valueOfEndCell) {
        endCellPos.row = valueOfEndRow
        endCellPos.cell = valueOfEndCell
        table.selection.selectionType = TableSelectionType.COLUMNS
      }
    }

    table.selection.selectionEndInfo.cellPos = endCellPos
    table.selection.selectionEndInfo.x = x
    table.selection.selectionEndInfo.y = y
    table.selection.selectionEndInfo.mouseEvtInfo = {
      clicks: mouseEvent.clicks,
      type: mouseEvent.type,
      ctrl: mouseEvent.ctrlKey,
    }

    table.selection.type = TableSelectionType.CELL
    table.updateSelectedCellPositions(false, false)
  } else if (-1 === hitInfo.Border && isInTextEditing) {
    {
      table.removeSelection()

      table.curCell = table.children.at(pos.row)?.getCellByIndex(pos.cell)
      if (table.curCell) {
        table.curCell.setContentSelectionStart(x, y, mouseEvent)
      }

      table.selection.isUse = true
      table.selection.start = true
      table.selection.type = TableSelectionType.TEXT
      table.selection.selectionType = TableSelectionType.COMMON
      table.selection.selectionInfo = null

      table.selection.selectionStartInfo.cellPos = pos
      table.selection.selectionStartInfo.x = x
      table.selection.selectionStartInfo.y = y
      table.selection.selectionStartInfo.mouseEvtInfo = {
        clicks: mouseEvent.clicks,
        type: mouseEvent.type,
        ctrl: mouseEvent.ctrlKey,
      }
    }
  } else if (hitInfo.Border !== -1) {
    _updateTableMarkInfo(table, pos.row)
    table.selection.selectionType = TableSelectionType.BORDER
    const selectionInfo: Partial<TableSelectionInfo> = {}

    const row = table.children.at(pos.row)

    let _X = x
    let _Y = y

    if (0 === hitInfo.Border || 2 === hitInfo.Border) {
      const limitH = TextContentDimensionLimit.yLimit

      let minY = 0
      const maxY = limitH

      selectionInfo.isColumn = false

      const lastIndex = table.children.length - 1
      if (0 === hitInfo.Border) {
        selectionInfo.index = pos.row
      } else selectionInfo.index = hitInfo.row + 1

      if (0 !== selectionInfo.index) {
        const tmpRow = selectionInfo.index - 1
        minY = table.rowsInfo[tmpRow].y
      }

      // Correct Y so that initially the line runs exactly along the border
      if (selectionInfo.index !== lastIndex + 1) {
        _Y = table.rowsInfo[selectionInfo.index].y
      } else {
        _Y =
          table.rowsInfo[selectionInfo.index - 1].y +
          table.rowsInfo[selectionInfo.index - 1].h
      }

      selectionInfo.min = minY
      selectionInfo.max = maxY

      selectionInfo.cellPos = {
        row: pos.row,
        cell: pos.cell,
      }

      if (null != selectionInfo.min) {
        _Y = Math.max(_Y, selectionInfo.min)
      }

      if (null != selectionInfo.max) {
        _Y = Math.min(_Y, selectionInfo.max)
      }
    } else if (row) {
      const cellsCount = row.cellsCount
      let xMin
      let xMax

      selectionInfo.isColumn = true
      if (3 === hitInfo.Border) {
        selectionInfo.index = pos.cell
      } else {
        selectionInfo.index = table.isRTL ? pos.cell - 1 : pos.cell + 1
      }

      if (0 !== selectionInfo.index) {
        const leftCellIndex = table.isRTL
          ? selectionInfo.index + 1
          : selectionInfo.index - 1
        const margins = row.getCellByIndex(leftCellIndex)!.getMargins()
        xMin =
          dimension.x +
          row.getCellInfo(leftCellIndex).cellStartX +
          margins.left!.w +
          margins.right!.w
      }

      if (cellsCount !== selectionInfo.index) {
        const margins = row.getCellByIndex(selectionInfo.index)!.getMargins()
        xMax =
          dimension.x +
          row.getCellInfo(selectionInfo.index).cellEndX -
          (margins.left!.w + margins.right!.w)
      }

      if (cellsCount !== selectionInfo.index) {
        _X = dimension.x + row.getCellInfo(selectionInfo.index).cellStartX
      } else {
        _X = dimension.x + row.getCellInfo(selectionInfo.index - 1).cellEndX
      }

      selectionInfo.min = xMin
      selectionInfo.max = xMax

      selectionInfo.cellPos = {
        row: pos.row,
        cell: pos.cell,
      }

      if (null != selectionInfo.min) {
        _X = Math.max(_X, selectionInfo.min)
      }

      if (null != selectionInfo.max) {
        _X = Math.min(_X, selectionInfo.max)
      }
    }

    selectionInfo.x = _X
    selectionInfo.y = _Y

    selectionInfo.startCenterX = _X // border 的中点, 固定值
    selectionInfo.startCenterY = _Y
    selectionInfo.startX = x // 实际 mouseDown 的位置
    selectionInfo.startY = y
    selectionInfo.start = true

    table.selection.selectionInfo = selectionInfo as TableSelectionInfo
    hookManager.invoke(Hooks.EditorUI.OnLockCursor)
  }
}

function _updateTableMarkInfo(table: Table, valueOfRowIndex: number) {
  const dimension = table.dimension
  if (!dimension) return

  const row = table.getRow(valueOfRowIndex)
  if (row == null) return

  const cellsCount = row.cellsCount

  table.rowsAndColsMarkInfo.cols = []

  for (let ci = 0; ci < cellsCount; ++ci) {
    const cell = row.getCellByIndex(ci)!
    const cellInfo = row.getCellInfo(ci)

    const valueOfStartGridCol = cellInfo.startGridCol
    const valueOfGridSpan = cell.getGridSpan()

    table.rowsAndColsMarkInfo.cols.push(
      table.tableGridReduced[valueOfStartGridCol + valueOfGridSpan - 1] -
        table.tableGridReduced[valueOfStartGridCol - 1]
    )
  }

  const lastRow = table.lastRow
  table.rowsAndColsMarkInfo.rows = []
  for (let ri = 0; ri <= lastRow; ri++) {
    if (
      table.rowsInfo[ri] &&
      isFiniteNumber(table.rowsInfo[ri].y) &&
      isFiniteNumber(table.rowsInfo[ri].h)
    ) {
      table.rowsAndColsMarkInfo.rows.push({
        y: table.rowsInfo[ri].y,
        h: table.rowsInfo[ri].h,
      })
    }
  }
}

function _updateTablePosition(table: Table, x: number, y: number) {
  table.x = 0.0
  table.y = 0.0
  const graphicFrame = table.parent
  if (
    graphicFrame.spPr &&
    graphicFrame.spPr.xfrm &&
    graphicFrame.spPr.xfrm.isValid()
  ) {
    const xfrm = graphicFrame.spPr.xfrm
    xfrm.setOffX(xfrm.offX + x)
    xfrm.setOffY(xfrm.offY + y)
  }
}

export function setContentSelectionInTableOrTextDocument(
  target: TableOrTextDocument,
  startPosCollection: PPTPositionStateCollection,
  endPosCollection: PPTPositionStateCollection,
  lvl: number,
  startFlag: 0 | 1 | -1,
  endFlag: 0 | 1 | -1
) {
  if (target.instanceType === InstanceType.Table) {
    setContentSelectionInTable(
      target,
      startPosCollection,
      endPosCollection,
      lvl,
      startFlag,
      endFlag
    )
  } else {
    setContentSelectionInTextDocument(
      target,
      startPosCollection,
      endPosCollection,
      lvl,
      startFlag,
      endFlag
    )
  }
}

function setContentSelectionInTextDocument(
  textDoc: TextDocument,
  startPosCollection: PPTPositionStateCollection,
  endPosCollection: PPTPositionStateCollection,
  lvl: number,
  startFlag: 0 | 1 | -1,
  endFlag: 0 | 1 | -1
) {
  if (
    validateSelectionPosition(
      textDoc,
      startPosCollection,
      endPosCollection,
      lvl,
      startFlag,
      endFlag
    ) === false
  ) {
    return
  }

  if (textDoc.children.length <= 0) return

  let startIndex = getPosIndexByDocPosInfo(
      textDoc,
      startFlag,
      startPosCollection,
      lvl
    ),
    endIndex = getPosIndexByDocPosInfo(textDoc, endFlag, endPosCollection, lvl)

  let _startPosCollection: null | PPTPositionStateCollection =
      startPosCollection,
    _startFlag = startFlag
  if (null !== startPosCollection && true === startPosCollection[lvl].deleted) {
    if (startIndex < textDoc.children.length) {
      _startPosCollection = null
      _startFlag = 1
    } else if (startIndex > 0) {
      startIndex--
      _startPosCollection = null
      _startFlag = -1
    } else {
      return
    }
  }

  let _endPosCollection: null | PPTPositionStateCollection = endPosCollection,
    _endFlag = endFlag
  if (null !== endPosCollection && true === endPosCollection[lvl].deleted) {
    if (endIndex < textDoc.children.length) {
      _endPosCollection = null
      _endFlag = 1
    } else if (endIndex > 0) {
      endIndex--
      _endPosCollection = null
      _endFlag = -1
    } else {
      return
    }
  }

  startIndex = Math.min(textDoc.children.length - 1, Math.max(0, startIndex))
  endIndex = Math.min(textDoc.children.length - 1, Math.max(0, endIndex))

  textDoc.selection.isUse = true
  textDoc.selection.startIndex = startIndex
  textDoc.selection.endIndex = endIndex

  let para: Nullable<Paragraph>
  if (startIndex !== endIndex) {
    para = textDoc.children.at(startIndex)
    para &&
      setContentSelectionInParagraph(
        para,
        _startPosCollection,
        null,
        lvl + 1,
        _startFlag,
        startIndex > endIndex ? 1 : -1
      )

    para = textDoc.children.at(endIndex)
    para &&
      setContentSelectionInParagraph(
        para,
        null,
        _endPosCollection,
        lvl + 1,
        startIndex > endIndex ? -1 : 1,
        _endFlag
      )

    let _startPos = startIndex
    let _endPos = endIndex
    let direction: TextOperationDirection = 1

    if (_startPos > _endPos) {
      _startPos = endIndex
      _endPos = startIndex
      direction = -1
    }

    for (let i = _startPos + 1; i < _endPos; i++) {
      para = textDoc.children.at(i)
      para && para.selectAll(direction)
    }
  } else {
    para = textDoc.children.at(startIndex)
    para &&
      setContentSelectionInParagraph(
        para,
        _startPosCollection,
        _endPosCollection,
        lvl + 1,
        _startFlag,
        _endFlag
      )
  }
}

function setContentSelectionInTable(
  table: Table,
  startPosCollection: PPTPositionStateCollection,
  endPosCollection: PPTPositionStateCollection,
  lvl: number,
  startFlag: 0 | 1 | -1,
  endFlag: 0 | 1 | -1
) {
  if (
    validateSelectionPosition(
      table,
      startPosCollection,
      endPosCollection,
      lvl,
      startFlag,
      endFlag
    ) === false
  ) {
    return
  }

  let isOnlyOne = true
  let startRowIndex = getPosIndexByDocPosInfo(
    table,
    startFlag,
    startPosCollection,
    lvl
  )
  if (startFlag !== 0) {
    isOnlyOne = false
  }

  let endRowIndex = getPosIndexByDocPosInfo(
    table,
    endFlag,
    endPosCollection,
    lvl
  )
  if (endFlag !== 0) {
    isOnlyOne = false
  }

  let _startPosCollection: null | PPTPositionStateCollection =
      startPosCollection,
    _startFlag = startFlag
  if (null !== startPosCollection && true === startPosCollection[lvl].deleted) {
    if (startRowIndex < table.children.length) {
      _startPosCollection = null
      _startFlag = 1
    } else if (startRowIndex > 0) {
      startRowIndex--
      _startPosCollection = null
      _startFlag = -1
    } else {
      return
    }
  }

  let _endPosCollection: null | PPTPositionStateCollection = endPosCollection,
    _endFlag = endFlag
  if (null !== endPosCollection && true === endPosCollection[lvl].deleted) {
    if (endRowIndex < table.children.length) {
      _endPosCollection = null
      _endFlag = 1
    } else if (endRowIndex > 0) {
      endRowIndex--
      _endPosCollection = null
      _endFlag = -1
    } else {
      return
    }
  }

  let startCellIndex = getPosIndexByDocPosInfo(
    table.children.at(startRowIndex)!,
    _startFlag,
    _startPosCollection!,
    lvl + 1
  )

  let endCellIndex = getPosIndexByDocPosInfo(
    table.children.at(endRowIndex)!,
    _endFlag,
    _endPosCollection!,
    lvl + 1
  )

  let __startPosCollection = _startPosCollection,
    __startFlag = _startFlag
  if (
    null !== _startPosCollection &&
    true === _startPosCollection[lvl + 1].deleted
  ) {
    if (startCellIndex < table.children.at(startRowIndex)!.cellsCount) {
      __startPosCollection = null
      __startFlag = 1
    } else if (startCellIndex > 0) {
      startCellIndex--
      __startPosCollection = null
      __startFlag = -1
    } else {
      return
    }
  }

  let __endPosCollection = _endPosCollection,
    __endFlag = _endFlag
  if (
    null !== _endPosCollection &&
    true === _endPosCollection[lvl + 1].deleted
  ) {
    if (endCellIndex < table.children.at(endCellIndex)!.cellsCount) {
      __endPosCollection = null
      __endFlag = 1
    } else if (endCellIndex > 0) {
      endCellIndex--
      __endPosCollection = null
      __endFlag = -1
    } else {
      return
    }
  }

  table.selection.isUse = true
  table.selection.selectionStartInfo.cellPos = {
    row: startRowIndex,
    cell: startCellIndex,
  }
  table.selection.selectionEndInfo.cellPos = {
    row: endRowIndex,
    cell: endCellIndex,
  }
  table.selection.currentRowIndex = endRowIndex
  table.selection.cellPositions = []
  table.selection.selectionType = TableSelectionType.COMMON
  table.selection.selectionInfo = null

  if (startRowIndex === endRowIndex && startCellIndex === endCellIndex) {
    table.curCell = table.getRow(startRowIndex)?.getCellByIndex(startCellIndex)
    table.selection.type = TableSelectionType.TEXT
    if (table.curCell) {
      setContentSelectionInTextDocument(
        table.curCell.textDoc,
        __startPosCollection!,
        __endPosCollection!,
        lvl + 2,
        __startFlag,
        __endFlag
      )
    }
  } else {
    table.selection.type = TableSelectionType.CELL
    table.updateSelectedCellPositions(isOnlyOne ? false : true)
  }

  if (
    null !== endPosCollection &&
    undefined !== endPosCollection[lvl].type &&
    undefined !== endPosCollection[lvl].selectionType
  ) {
    table.selection.type = endPosCollection[lvl].type!
    table.selection.selectionType = endPosCollection[lvl].selectionType!

    if (TableSelectionType.CELL === table.selection.type) {
      table.updateSelectedCellPositions(
        !isOnlyOne || TableSelectionType.ROWS === table.selection.selectionType
          ? true
          : false
      )
    }
  }
}

function setContentSelectionInParagraph(
  para: Paragraph,
  startPosCollection: Nullable<PPTPositionStateCollection>,
  endPosCollection: Nullable<PPTPositionStateCollection>,
  lvl: number,
  startFlag: 0 | 1 | -1,
  endFlag: 0 | 1 | -1
) {
  if (
    validateSelectionPosition(
      para,
      startPosCollection,
      endPosCollection,
      lvl,
      startFlag,
      endFlag
    ) === false
  ) {
    return
  }

  let startIndex = getPosIndexByDocPosInfo(
      para,
      startFlag,
      startPosCollection,
      lvl
    ),
    endIndex = getPosIndexByDocPosInfo(para, endFlag, endPosCollection, lvl)

  let _startPosCollection = startPosCollection,
    _startFlag = startFlag
  if (null != startPosCollection && true === startPosCollection[lvl].deleted) {
    if (startIndex < para.children.length) {
      _startPosCollection = null
      _startFlag = 1
    } else if (startIndex > 0) {
      startIndex--
      _startPosCollection = null
      _startFlag = -1
    } else {
      return
    }
  }

  let _endPosCollection = endPosCollection,
    _endFlag = endFlag
  if (null != endPosCollection && true === endPosCollection[lvl].deleted) {
    if (endIndex < para.children.length) {
      _endPosCollection = null
      _endFlag = 1
    } else if (endIndex > 0) {
      endIndex--
      _endPosCollection = null
      _endFlag = -1
    } else {
      return
    }
  }

  para.selection.isUse = true
  para.selection.startIndex = startIndex
  para.selection.endIndex = endIndex
  correctParaSelectionState(para)

  let paraItem: Nullable<ParagraphItem>
  if (startIndex !== endIndex) {
    paraItem = para.children.at(startIndex)
    if (paraItem) {
      setContentSelectionForParaItem(
        paraItem,
        _startPosCollection,
        null,
        lvl + 1,
        _startFlag,
        startIndex > endIndex ? 1 : -1
      )
    }
    paraItem = para.children.at(endIndex)
    if (paraItem) {
      setContentSelectionForParaItem(
        paraItem,
        null,
        _endPosCollection,
        lvl + 1,
        startIndex > endIndex ? -1 : 1,
        _endFlag
      )
    }

    let _startPos = startIndex
    let _endPos = endIndex
    let direction: TextOperationDirection = 1

    if (_startPos > _endPos) {
      _startPos = endIndex
      _endPos = startIndex
      direction = -1
    }

    for (let i = _startPos + 1; i < _endPos; i++) {
      paraItem = para.children.at(i)
      paraItem && paraItem.selectAll(direction)
    }
  } else {
    paraItem = para.children.at(startIndex)
    if (paraItem) {
      setContentSelectionForParaItem(
        paraItem,
        _startPosCollection,
        _endPosCollection,
        lvl + 1,
        _startFlag,
        _endFlag
      )
    }
  }
}

function setContentSelectionForParaItem(
  paraItem: ParagraphItem,
  startPosCollection: Nullable<PPTPositionStateCollection>,
  endPosCollection: Nullable<PPTPositionStateCollection>,
  lvl: number,
  startFlag: 0 | 1 | -1,
  endFlag: 0 | 1 | -1
) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    const startIndex = getPosIndexByDocPosInfo(
      paraItem,
      startFlag,
      startPosCollection,
      lvl
    )

    const endIndex = getPosIndexByDocPosInfo(
      paraItem,
      endFlag,
      endPosCollection,
      lvl
    )

    const selection = paraItem.state.selection
    selection.startIndex = startIndex
    selection.endIndex = endIndex
    selection.isUse = true
  } else {
    if (paraItem.children.length <= 0) return

    if (
      validateSelectionPosition(
        paraItem,
        startPosCollection,
        endPosCollection,
        lvl,
        startFlag,
        endFlag
      ) === false
    ) {
      return
    }

    let startIndex = getPosIndexByDocPosInfo(
        paraItem,
        startFlag,
        startPosCollection,
        lvl
      ),
      endIndex = getPosIndexByDocPosInfo(
        paraItem,
        endFlag,
        endPosCollection,
        lvl
      )

    let _startPosCollection = startPosCollection,
      _startFlag = startFlag
    if (
      null != startPosCollection &&
      true === startPosCollection[lvl].deleted
    ) {
      if (startIndex < paraItem.children.length) {
        _startPosCollection = null
        _startFlag = 1
      } else if (startIndex > 0) {
        startIndex--
        _startPosCollection = null
        _startFlag = -1
      } else {
        return
      }
    }

    let _endPosCollection = endPosCollection,
      _endFlag = endFlag
    if (null != endPosCollection && true === endPosCollection[lvl].deleted) {
      if (endIndex < paraItem.children.length) {
        _endPosCollection = null
        _endFlag = 1
      } else if (endIndex > 0) {
        endIndex--
        _endPosCollection = null
        _endFlag = -1
      } else {
        return
      }
    }

    paraItem.selection.isUse = true
    paraItem.selection.startIndex = Math.max(
      0,
      Math.min(paraItem.children.length - 1, startIndex)
    )
    paraItem.selection.endIndex = Math.max(
      0,
      Math.min(paraItem.children.length - 1, endIndex)
    )

    let run: Nullable<ParaRun>
    if (startIndex !== endIndex) {
      run = paraItem.children.at(startIndex)
      if (run) {
        setContentSelectionForParaItem(
          run,
          _startPosCollection,
          null,
          lvl + 1,
          _startFlag,
          startIndex > endIndex ? 1 : -1
        )
      }

      run = paraItem.children.at(endIndex)
      if (run) {
        setContentSelectionForParaItem(
          run,
          null,
          _endPosCollection,
          lvl + 1,
          startIndex > endIndex ? -1 : 1,
          _endFlag
        )
      }

      let _startIndex = startIndex
      let _endIndex = endIndex
      let direction: TextOperationDirection = 1

      if (_startIndex > _endIndex) {
        _startIndex = endIndex
        _endIndex = startIndex
        direction = -1
      }

      for (let i = _startIndex + 1; i < _endIndex; i++) {
        run = paraItem.children.at(i)
        run && run.selectAll(direction)
      }
    } else {
      run = paraItem.children.at(startIndex)
      if (run) {
        setContentSelectionForParaItem(
          run,
          _startPosCollection,
          _endPosCollection,
          lvl + 1,
          _startFlag,
          _endFlag
        )
      }
    }
  }
}
