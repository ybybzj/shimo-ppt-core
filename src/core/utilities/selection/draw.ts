import { InstanceType } from '../../instanceTypes'
import { compareIndices } from '../../../re/export/textContent'
import { TableSelectionType, VMergeType } from '../../common/const/table'
import { hookManager, Hooks } from '../../hooks'
import { LineSegmentIndexesUtil } from '../../Paragraph/ParaContent/LineSegmentIndexes'
import { getWidthAtIndices } from '../../Paragraph/ParaContent/utils/typeset'
import {
  Paragraph,
  ParagraphSelectionDrawInfo,
} from '../../Paragraph/Paragraph'
import { ParagraphItem } from '../../Paragraph/type'
import { getParagraphSelectionIndices } from '../../Paragraph/utils/typeset'
import { Table } from '../../Table/Table'
import { TableCell } from '../../Table/TableCell'
import { TextDocument } from '../../TextDocument/TextDocument'

export function drawSelectionForTableCells(table: Table) {
  if (table.selection.type === TableSelectionType.CELL) {
    drawSelectionForTable(table)
  }
}

export function drawSelectionForTable(table: Table) {
  if (false === table.selection.isUse) return

  const { x } = table.dimension
  const slideIndex = table.getParentSlideIndex()

  let rowH: number
  switch (table.selection.type) {
    case TableSelectionType.CELL: {
      table.selection.cellPositions.forEach(
        ({ row: rowIndex, cell: cellIndex }) => {
          const row = table.children.at(rowIndex)
          if (row) {
            const { cellStartX, cellEndX, startGridCol } =
              row.getCellInfo(cellIndex)
            const xStart = x + cellStartX
            const xEnd = x + cellEndX

            rowH = table.rowsInfo[rowIndex].h
            for (let i = rowIndex + 1; i < table.children.length; ++i) {
              const _row = table.children.at(i)!
              const cell = _row.getCellByStartGridColIndex(startGridCol)

              if (!cell) {
                break
              }
              const vMerge = cell.getVMerge()
              if (VMergeType.Continue === vMerge) {
                rowH += table.rowsInfo[i].h
              } else {
                break
              }
            }
            hookManager.invoke(Hooks.EditorUI.AddSlideSelection, {
              slideIndex: slideIndex,
              x: xStart,
              y:
                table.rowsInfo[rowIndex].y +
                table.rowsInfo[rowIndex].diffYOfTop,
              width: xEnd - xStart,
              height: rowH,
              matrix: table.parent?.transform,
            })
          }
        }
      )
      break
    }
    case TableSelectionType.TEXT: {
      const cell = table.children
        .at(table.selection.selectionStartInfo.cellPos.row)
        ?.getCellByIndex(table.selection.selectionStartInfo.cellPos.cell)
      if (cell) {
        drawSelectionForTableCell(cell)
      }
      break
    }
  }
}

function drawSelectionForTableCell(cell: TableCell) {
  const transform = cell.getTextTransformByDirection()
  hookManager.invoke(Hooks.EditorUI.OnMultiplyTargetTransform, {
    transform: transform,
    callback: () => {
      drawSelectionForTextDocument(cell.textDoc)
    },
  })
}

const SelectionEdgeFlag = {
  Start: 0x1,
  End: 0x10,
} as const

function _getSelectionEdge(i: number, start: number, end: number) {
  let result: number = 0
  if (i === start) {
    result |= SelectionEdgeFlag.Start
  }

  if (i === end) {
    result |= SelectionEdgeFlag.End
  }

  return result
}

export function drawSelectionForTextDocument(textDoc: TextDocument) {
  const dimension = textDoc.renderingState
  if (dimension == null) return
  const startIndex = dimension.startParaIndex
  const endIndex = dimension.endIndex

  if (true === textDoc.selection.isUse) {
    let start = textDoc.selection.startIndex
    let end = textDoc.selection.endIndex

    if (start > end) {
      start = textDoc.selection.endIndex
      end = textDoc.selection.startIndex
    }

    start = Math.max(start, startIndex)
    end = Math.min(end, endIndex)

    if (dimension.section == null || dimension.section.isEmpty()) {
      for (let i = start; i <= end; i++) {
        const para = textDoc.children.at(i)
        para &&
          drawSelectionForParagraph(para, 0, _getSelectionEdge(i, start, end))
      }
    } else {
      const section = dimension.section
      for (
        let colIndex = 0, colsCount = section.columns.length;
        colIndex < colsCount;
        ++colIndex
      ) {
        const startPosIndex = dimension.startParaIndex
        const endPosIndex = dimension.endIndex

        let startIndex = textDoc.selection.startIndex
        let endIndex = textDoc.selection.endIndex

        if (startIndex > endIndex) {
          startIndex = textDoc.selection.endIndex
          endIndex = textDoc.selection.startIndex
        }

        startIndex = Math.max(startIndex, startPosIndex)
        endIndex = Math.min(endIndex, endPosIndex)

        for (let j = startIndex; j <= endIndex; ++j) {
          const elmColIndex = textDoc.getRelativeColIndexForParaElem(
            j,
            colIndex
          )
          const para = textDoc.children.at(j)
          para &&
            drawSelectionForParagraph(
              para,
              elmColIndex,
              _getSelectionEdge(j, startIndex, endIndex)
            )
        }
      }
    }
  }
}

function drawSelectionForParagraph(
  para: Paragraph,
  colIndex: number,
  edgeFlag: number
) {
  if (true !== para.selection.isUse) return
  const renderingState = para.renderingState
  const columns = renderingState.columns
  if (colIndex < 0 || colIndex >= columns.length) return
  const slideIndex = para.getParentSlideIndex()
  if (0 === colIndex && columns.at(0)!.endLine < 0) return

  const column = columns.at(colIndex)!

  let startIndex = para.selection.startIndex
  let endIndex = para.selection.endIndex

  if (startIndex > endIndex) {
    startIndex = para.selection.endIndex
    endIndex = para.selection.startIndex
  }

  const { startLine: startLineIndex, endLine: endLineIndex } = column
  const startLine = renderingState.getLine(startLineIndex)!
  const endLine = renderingState.getLine(endLineIndex)!
  if (
    startIndex > endLine.getEndPosIndex() ||
    endIndex < startLine.getStartPosIndex()
  ) {
    return
  } else {
    const lineCount = renderingState.getLineCount()
    startIndex = Math.max(startIndex, startLine.getStartPosIndex())
    endIndex = Math.min(
      endIndex,
      endLineIndex !== lineCount - 1
        ? endLine.getEndPosIndex()
        : para.children.length - 1
    )
  }

  const selectionDrawInfo: ParagraphSelectionDrawInfo = {
    startX: 0,
    width: 0,
    startY: 0,
    height: 0,
    searchingStartX: true,
  }

  let startCurPos: undefined | { x: number; y: number; size: number },
    endCursorPos: undefined | { x: number; y: number; size: number }

  const isInTouchMode = hookManager.get(Hooks.Cursor.GetIsInTouchMode)
  for (let curLine = startLineIndex; curLine <= endLineIndex; curLine++) {
    const paraLine = renderingState.getLine(curLine)!
    if (paraLine) {
      const startY = column.y + paraLine.top
      const height = paraLine.bottom - paraLine.top
      const segment = paraLine.segment
      let startX = segment.XForRender
      if (!para.isRTL && curLine === para.paraNumbering.line) {
        startX += para.paraNumbering.renderWidth
      }

      const hasArabic = para.renderingState.hasArabicAtLine(curLine)
      const indicesArrayInfo = paraLine.getReorderPosInfoArray()
      // 由于混排中 selection 可能非连续, 按字符 drawSelection
      if ((hasArabic === true || para.isRTL) && indicesArrayInfo != null) {
        const segIndices = getParagraphSelectionIndices(
          para,
          startIndex,
          endIndex
        )

        if (segIndices) {
          const { startDices, endDices } = segIndices
          if (startDices && endDices) {
            for (let i = 0, l = indicesArrayInfo.length; i < l; i++) {
              const { indices } = indicesArrayInfo.at(i)!
              const renderWidth = getWidthAtIndices(para, indices)

              if (
                0 <= compareIndices(indices, startDices) &&
                0 >= compareIndices(indices, endDices) &&
                renderWidth > 0.001
              ) {
                hookManager.invoke(Hooks.EditorUI.AddSlideSelection, {
                  slideIndex: slideIndex,
                  x: startX,
                  y: startY,
                  width: renderWidth,
                  height: height,
                })
                if (
                  !!(edgeFlag & SelectionEdgeFlag.Start) &&
                  startCurPos == null
                ) {
                  startCurPos = {
                    x: startX,
                    y: startY,
                    size: height,
                  }
                }
                if (!!(edgeFlag & SelectionEdgeFlag.End)) {
                  if (endCursorPos == null) {
                    endCursorPos = {
                      x: startX + renderWidth,
                      y: startY,
                      size: height,
                    }
                  } else {
                    endCursorPos.x = startX + renderWidth
                    endCursorPos.y = startY
                    endCursorPos.size = height
                  }
                }
              }
              startX += renderWidth
            }
          }
        }
      } else {
        const segmentStartPos = segment.startIndex
        const segmentEndIndex = segment.endIndex

        if (startIndex > segmentEndIndex || endIndex < segmentStartPos) {
          continue
        }
        selectionDrawInfo.startY = startY
        selectionDrawInfo.height = height
        selectionDrawInfo.startX = startX
        selectionDrawInfo.width = 0
        selectionDrawInfo.searchingStartX = true

        for (let i = segmentStartPos; i <= segmentEndIndex; i++) {
          const item = para.children.at(i)
          item &&
            drawSelectionByInfo(
              item,
              curLine,
              selectionDrawInfo,
              isInTouchMode === true
            )
        }

        const {
          startX: _startX,
          startY: _startY,
          width: w,
          height: h,
        } = selectionDrawInfo
        if (w > 0.001) {
          hookManager.invoke(Hooks.EditorUI.AddSlideSelection, {
            slideIndex: slideIndex,
            x: _startX,
            y: _startY,
            width: w,
            height: h,
          })
          if (!!(edgeFlag & SelectionEdgeFlag.Start) && startCurPos == null) {
            startCurPos = {
              x: _startX,
              y: startY,
              size: height,
            }
          }
          if (!!(edgeFlag & SelectionEdgeFlag.End)) {
            if (endCursorPos == null) {
              endCursorPos = {
                x: _startX + w,
                y: startY,
                size: height,
              }
            } else {
              endCursorPos.x = _startX + w
              endCursorPos.y = startY
              endCursorPos.size = height
            }
          }
        } else {
          if (edgeFlag === SelectionEdgeFlag.Start && startCurPos == null) {
            startCurPos = {
              x: _startX,
              y: startY,
              size: height,
            }
          }

          if (edgeFlag === SelectionEdgeFlag.End && endCursorPos == null) {
            endCursorPos = {
              x: startX,
              y: startY,
              size: height,
            }
          }
        }
      }
    }
  }

  if (isInTouchMode === true) {
    if (startCurPos != null) {
      hookManager.invoke(Hooks.Cursor.DrawSelectionCursor, {
        edge: 'start',
        x: startCurPos.x,
        y: startCurPos.y,
        size: startCurPos.size,
      })
    }

    if (endCursorPos != null) {
      hookManager.invoke(Hooks.Cursor.DrawSelectionCursor, {
        edge: 'end',
        x: endCursorPos.x,
        y: endCursorPos.y,
        size: endCursorPos.size,
      })
    }
  }
}

function drawSelectionByInfo(
  paraItem: ParagraphItem,
  lineindex: number,
  selectionDrawInfo: ParagraphSelectionDrawInfo,
  isInTouchMode = false
) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    lineindex = lineindex - paraItem.startParaLine
    const lineSegmentIndexes = paraItem.lineSegmentIndexes
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      lineindex
    )
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      lineindex
    )

    const selection = paraItem.state.selection
    let startElmIndex = selection.startIndex
    let endElmIndex = selection.endIndex

    if (startElmIndex > endElmIndex) {
      startElmIndex = selection.endIndex
      endElmIndex = selection.startIndex
    }

    let searchingStart = selectionDrawInfo.searchingStartX

    for (let i = startIndex; i < endIndex; i++) {
      const item = paraItem.children.at(i)
      if (item == null) continue

      let isDrawSelection = false

      if (true === searchingStart) {
        if (true === selection.isUse && i >= startElmIndex && i < endElmIndex) {
          searchingStart = false

          isDrawSelection = true
        } else {
          selectionDrawInfo.startX += item.getVisibleWidth()
        }
      } else {
        if (true === selection.isUse && i >= startElmIndex && i < endElmIndex) {
          isDrawSelection = true
        }
      }

      if (true === isDrawSelection) {
        const w =
          isInTouchMode && item.instanceType === InstanceType.ParaEnd
            ? 0
            : item.getVisibleWidth()
        selectionDrawInfo.width += w
      }
    }

    selectionDrawInfo.searchingStartX = searchingStart
  } else {
    const line = lineindex - paraItem.startParaLine
    const lineSegmentIndexes = paraItem.lineSegmentIndexes
    const startIndex = LineSegmentIndexesUtil.getStartIndexOfLine(
      lineSegmentIndexes,
      line
    )
    const endIndex = LineSegmentIndexesUtil.getEndIndexOfLine(
      lineSegmentIndexes,
      line
    )

    for (let i = startIndex; i <= endIndex; i++) {
      const run = paraItem.children.at(i)
      run &&
        drawSelectionByInfo(run, lineindex, selectionDrawInfo, isInTouchMode)
    }
  }
}
