import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import { TableSelectionType } from '../../common/const/table'
import { getCurrentTextPrInParaItem } from '../../Paragraph/ParaContent/utils/pr'
import { Paragraph } from '../../Paragraph/Paragraph'
import { getCurrentFocusElementsInfoForParagraph } from '../../Paragraph/utils/selection'

import { CurrentFocusElementsInfo } from '../../Slide/CurrentFocusElementsInfo'
import { Table } from '../../Table/Table'
import { TextPr } from '../../textAttributes/TextPr'
import { TextDocument } from '../../TextDocument/TextDocument'
import { TableOrTextDocument } from '../type'

export function getCurrentParagraph(
  target: TableOrTextDocument,
  isIgnoreSelection?: boolean
) {
  if (target.instanceType === InstanceType.Table) {
    return getCurrentParagraphInTable(target, isIgnoreSelection)
  } else {
    return getCurrentParagraphInTextDocument(target, isIgnoreSelection)
  }
}

function getCurrentParagraphInTextDocument(
  textDoc: TextDocument,
  isIgnoreSelection?: boolean
) {
  const index =
    true === textDoc.selection.isUse && true !== isIgnoreSelection
      ? textDoc.selection.startIndex
      : textDoc.cursorPosition.childIndex
  if (index < 0 || index >= textDoc.children.length) return null

  return textDoc.children.at(index)
}

function getCurrentParagraphInTable(table: Table, isIgnoreSelection?: boolean) {
  if (true === isIgnoreSelection) {
    if (table.curCell) {
      return getCurrentParagraphInTextDocument(
        table.curCell.textDoc,
        isIgnoreSelection
      )
    } else {
      return null
    }
  } else {
    const selectionPos = table.getSelectedCellPositions()
    if (selectionPos.length > 0) {
      const cellIndex = selectionPos[0].cell
      const rowIndex = selectionPos[0].row

      const textDoc = table.getRow(rowIndex)?.getCellByIndex(cellIndex)?.textDoc
      if (textDoc == null) return null

      if (
        true === table.selection.isUse &&
        TableSelectionType.CELL === table.selection.type
      ) {
        textDoc.isSetToAll = true
        const res = getCurrentParagraphInTextDocument(
          textDoc,
          isIgnoreSelection
        )
        textDoc.isSetToAll = false
        return res
      } else {
        return getCurrentParagraphInTextDocument(textDoc, isIgnoreSelection)
      }
    }
  }

  return null
}

export function getCurrentTextPr(target: TableOrTextDocument) {
  return target.instanceType === InstanceType.Table
    ? getCurrentTextPrInTable(target)
    : getCurrentTextPrInTextDocument(target)
}

function getCurrentTextPrInTextDocument(textDoc: TextDocument) {
  let result: Nullable<TextPr>
  if (textDoc.children.length <= 0) return result

  if (true === textDoc.isSetToAll) {
    const para = textDoc.children.at(0)!
    result = getCurrentTextPrInParagraph(para)
    return result
  }

  if (true === textDoc.selection.isUse) {
    let startIndex = textDoc.selection.startIndex
    if (textDoc.selection.endIndex < startIndex) {
      startIndex = textDoc.selection.endIndex
    }

    const para = textDoc.children.at(startIndex)
    if (para) result = getCurrentTextPrInParagraph(para)
  } else {
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
    if (para) result = getCurrentTextPrInParagraph(para)
  }

  return result
}

function getCurrentTextPrInTable(table: Table) {
  let textPr: Nullable<TextPr>
  if (true === table.isSetToAll) {
    const row = table.children.at(0)
    const cell = row?.getCellByIndex(0)
    if (cell) {
      cell.textDoc.isSetToAll = true
      textPr = getCurrentTextPrInTextDocument(cell.textDoc)
      cell.textDoc.isSetToAll = false
    }
    return textPr
  }

  if (
    true === table.selection.isUse &&
    TableSelectionType.CELL === table.selection.type
  ) {
    const cellPos = table.selection.cellPositions[0]
    const row = table.children.at(cellPos.row)
    const cell = row?.getCellByIndex(cellPos.cell)
    if (cell) {
      cell.textDoc.isSetToAll = true
      textPr = getCurrentTextPrInTextDocument(cell.textDoc)
      cell.textDoc.isSetToAll = false
    }
    return textPr
  }

  return table.curCell
    ? getCurrentTextPrInTextDocument(table.curCell.textDoc)
    : undefined
}

function getCurrentTextPrInParagraph(paragraph: Paragraph) {
  let textPr: Nullable<TextPr>
  if (true === paragraph.isSetToAll) {
    paragraph.selectAll(1)

    const count = paragraph.children.length
    let index = 0
    while (
      index < count &&
      true === paragraph.children.at(index)!.hasNoSelection()
    ) {
      index++
    }

    textPr = getCurrentTextPrInParaItem(paragraph.children.at(index))

    paragraph.removeSelection()
  } else {
    if (true === paragraph.selection.isUse) {
      let startPosIndex = paragraph.selection.startIndex
      let endPosIndex = paragraph.selection.endIndex

      if (startPosIndex > endPosIndex) {
        startPosIndex = paragraph.selection.endIndex
        endPosIndex = paragraph.selection.startIndex
      }

      startPosIndex = Math.max(0, startPosIndex)
      while (
        startPosIndex < endPosIndex &&
        true === paragraph.children.at(startPosIndex)!.hasNoSelection()
      ) {
        startPosIndex++
      }

      textPr = getCurrentTextPrInParaItem(paragraph.children.at(startPosIndex))
    } else {
      textPr = getCurrentTextPrInParaItem(
        paragraph.children.at(paragraph.cursorPosition.elementIndex)
      )
    }
  }

  return textPr
}

export function getCurrentFocusElementsInfo(
  target: TableOrTextDocument,
  focusInfo: CurrentFocusElementsInfo
) {
  if (target.instanceType === InstanceType.TextDocument) {
    getCurrentFocusElementsInfoForTextDocument(target, focusInfo)
  } else {
    getCurrentFocusElementsInfoForTable(target, focusInfo)
  }
}

function getCurrentFocusElementsInfoForTextDocument(
  textDoc: TextDocument,
  focusInfo: CurrentFocusElementsInfo
) {
  if (true === textDoc.isSetToAll) {
    const count = textDoc.children.length
    if (count > 1) focusInfo.setMultiSelection()
    else if (count === 1) {
      getCurrentFocusElementsInfoForParagraph(
        textDoc.children.at(0)!,
        focusInfo
      )
    }
  } else {
    if (true === textDoc.selection.isUse) {
      if (textDoc.selection.startIndex !== textDoc.selection.endIndex) {
        focusInfo.setMultiSelection()
      } else {
        const para = textDoc.children.at(textDoc.selection.startIndex)
        para && getCurrentFocusElementsInfoForParagraph(para, focusInfo)
      }
    } else {
      const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
      para && getCurrentFocusElementsInfoForParagraph(para, focusInfo)
    }
  }
}

function getCurrentFocusElementsInfoForTable(
  table: Table,
  focusInfo: CurrentFocusElementsInfo
) {
  focusInfo.setTable()

  if (
    false === table.selection.isUse ||
    (true === table.selection.isUse &&
      TableSelectionType.TEXT === table.selection.type)
  ) {
    getCurrentFocusElementsInfoForTextDocument(
      table.curCell!.textDoc,
      focusInfo
    )
  } else if (
    true === table.selection.isUse &&
    TableSelectionType.CELL === table.selection.type &&
    table.selection.selectionStartInfo.cellPos.row ===
      table.selection.selectionEndInfo.cellPos.row &&
    table.selection.selectionStartInfo.cellPos.cell ===
      table.selection.selectionEndInfo.cellPos.cell
  ) {
    const row = table.getRow(table.selection.selectionStartInfo.cellPos.row)
    if (!row) return

    const cell = row.getCellByIndex(
      table.selection.selectionStartInfo.cellPos.cell
    )
    if (!cell) return

    focusInfo.setCell(cell)
  }
}
