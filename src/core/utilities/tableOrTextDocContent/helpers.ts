import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import { ParaHyperlink } from '../../Paragraph/ParaContent/ParaHyperlink'
import { ParaRun } from '../../Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../../Paragraph/Paragraph'
import { getSearchPositionInfoFromParagraph } from '../../Paragraph/utils/position'
import { Table } from '../../Table/Table'
import { TableRow } from '../../Table/TableRow'
import { TextDocument } from '../../TextDocument/TextDocument'
import { PPTPositionStateCollection } from './position'

export function validateSelectionPosition(
  entity: Paragraph | Table | TextDocument | ParaHyperlink,
  startPosCollection: Nullable<PPTPositionStateCollection>,
  endPosCollection: Nullable<PPTPositionStateCollection>,
  lvl: number,
  startFlag: number,
  endFlag: number
): boolean {
  if (
    (0 === startFlag &&
      (!startPosCollection ||
        !startPosCollection[lvl] ||
        entity !== startPosCollection[lvl].entity)) ||
    (0 === endFlag &&
      (!endPosCollection ||
        !endPosCollection[lvl] ||
        entity !== endPosCollection[lvl].entity))
  ) {
    return false
  }

  return true
}

export function getPosIndexByDocPosInfo(
  entity: Paragraph | Table | TableRow | TextDocument | ParaHyperlink | ParaRun,
  flag: 0 | 1 | -1,
  posInfo: Nullable<PPTPositionStateCollection>,
  lvl: number
): number {
  let index = 0
  switch (flag) {
    case 0:
      index = posInfo![lvl].position
      break
    case 1:
      index = 0
      break
    case -1:
      index =
        entity.instanceType === InstanceType.ParaRun
          ? entity.children.length
          : entity.children.length - 1
      break
  }
  return index
}

function isCoordsInSelectionForParagraph(
  para: Paragraph,
  x: number,
  y: number,
  colIndex: number
) {
  if (
    colIndex < 0 ||
    colIndex >= para.renderingState.columns.length ||
    true !== para.selection.isUse
  ) {
    return false
  }

  const searchInfo = getSearchPositionInfoFromParagraph(
    para,
    x,
    y,
    colIndex,
    false,
    false,
    false
  )

  if (true === searchInfo.isInTextContent) {
    return para.isContentPosSelected(searchInfo.positionInText)
  }

  return false
}

export function isCoordsInSelectionForTextDocument(
  textDoc: TextDocument,
  x: number,
  y: number
) {
  if (true === textDoc.selection.isUse || true === textDoc.isSetToAll) {
    let start = textDoc.selection.startIndex
    let end = textDoc.selection.endIndex

    if (start > end) {
      start = textDoc.selection.endIndex
      end = textDoc.selection.startIndex
    }

    const elementIndex = textDoc.getElementIndexByXY(x, y)
    if (elementIndex > start && elementIndex < end) {
      return true
    } else if (elementIndex < start || elementIndex > end) {
      return false
    } else {
      const colIndex = textDoc.getElementColIndexByXY(elementIndex, x)
      const para = textDoc.children.at(elementIndex)
      return para
        ? isCoordsInSelectionForParagraph(para, x, y, colIndex)
        : false
    }
  }

  return false
}

export function textPrOfFirstItem(para: Paragraph) {
  if (
    para.children.length <= 0 ||
    para.children.at(0)!.instanceType !== InstanceType.ParaRun
  ) {
    return para.paraTextPr.textPr
  }

  return (para.children.at(0) as ParaRun).pr
}
