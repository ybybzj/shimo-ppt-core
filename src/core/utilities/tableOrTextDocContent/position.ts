import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import { TableSelectionTypeValues } from '../../common/const/table'
import { ParaHyperlink } from '../../Paragraph/ParaContent/ParaHyperlink'
import { ParaRun } from '../../Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../../Paragraph/Paragraph'
import { ParagraphItem } from '../../Paragraph/type'
import { Table } from '../../Table/Table'
import { TableRow } from '../../Table/TableRow'
import { TextDocument } from '../../TextDocument/TextDocument'
import { TableOrTextDocument } from '../type'
export type PositionStateEntity =
  | ParaRun
  | TextDocument
  | Paragraph
  | ParaHyperlink
  | ParaRun
  | Table
  | TableRow

export type PPTPositionStateCollection = Array<{
  entity: PositionStateEntity
  position: number
  deleted?: boolean
  type?: number
  selectionType?: TableSelectionTypeValues
}>

export function getPositionStateForRun(
  paraRun: ParaRun,
  runPos: number,
  collection: PPTPositionStateCollection
) {
  collection.push({ entity: paraRun, position: runPos })
  if (paraRun.paragraph) {
    const para = paraRun.paragraph
    const paraElementPos = para.getContentPositionByElement(paraRun)
    if (null != paraElementPos) {
      let lvl = paraElementPos.getLevel()
      while (lvl > 0) {
        const pos = paraElementPos.get(lvl)
        paraElementPos.decreaseLevel(1)
        const entity = para.elementAtElementPos(paraElementPos)
        lvl--

        if (entity) {
          collection.splice(0, 0, { entity: entity, position: pos })
        }
      }
      collection.splice(0, 0, {
        entity: para,
        position: paraElementPos.get(0),
      })
    }

    const textDoc = para.parent
    if (textDoc) {
      collection.splice(0, 0, {
        entity: textDoc,
        position: para.getIndex(),
      })
      if (textDoc.parent?.instanceType === InstanceType.TableCell) {
        const cell = textDoc.parent
        const row = cell?.parent
        if (row) {
          collection.splice(0, 0, {
            entity: row,
            position: cell.index,
          })

          const table = row.parent
          if (table) {
            collection.splice(0, 0, {
              entity: table,
              position: row.index,
            })
          }
        }
      }
    }
  }
}

export function getPositionStateForTableOrTextDoc(
  target: TableOrTextDocument,
  isSelection: boolean,
  isStart: boolean,
  collection: PPTPositionStateCollection
): PPTPositionStateCollection {
  if (collection == null) {
    collection = []
  }
  if (target.instanceType === InstanceType.Table) {
    const currentRowIndex =
      true === isSelection
        ? true === isStart
          ? target.selection.selectionStartInfo.cellPos.row
          : target.selection.selectionEndInfo.cellPos.row
        : target.curCell!.parent.index
    const curCellIndex =
      true === isSelection
        ? true === isStart
          ? target.selection.selectionStartInfo.cellPos.cell
          : target.selection.selectionEndInfo.cellPos.cell
        : target.curCell!.index

    const row = target.getRow(currentRowIndex)
    collection.push({
      entity: target,
      position: currentRowIndex,
      type: target.selection.type,
      selectionType: target.selection.selectionType,
    })
    if (row) {
      collection.push({
        entity: row,
        position: curCellIndex,
      })
    }

    if (row && curCellIndex >= 0 && curCellIndex < row.cellsCount) {
      const cell = row.getCellByIndex(curCellIndex)!
      getPositionStateForTableOrTextDoc(
        cell.textDoc,
        isSelection,
        isStart,
        collection
      )
    }
  } else if (target.instanceType === InstanceType.TextDocument) {
    const pos =
      true === isSelection
        ? true === isStart
          ? target.selection.startIndex
          : target.selection.endIndex
        : target.cursorPosition.childIndex
    collection.push({ entity: target, position: pos })

    const para = target.children.at(pos)
    if (para) {
      const index = collection.length

      const paraElementPos = para.currentElementPosition(isSelection, isStart)

      let lvl = paraElementPos.getLevel()
      while (lvl > 0) {
        const pos = paraElementPos.get(lvl)
        paraElementPos.decreaseLevel(1)
        const entity = para.elementAtElementPos(paraElementPos)
        lvl--

        if (entity) {
          collection.splice(index, 0, { entity: entity, position: pos })
        }
      }

      collection.splice(index, 0, {
        entity: para,
        position: paraElementPos.get(0),
      })
    }
  }

  return collection
}

export function setPositionStateForTableOrTextDoc(
  target: TableOrTextDocument,
  posInfo: PPTPositionStateCollection,
  lvl: number,
  flag: 0 | 1 | -1
) {
  if (target.instanceType === InstanceType.Table) {
    setPositionForTable(target, posInfo, lvl, flag)
  } else {
    setPositionForTextDocume(target, posInfo, lvl, flag)
  }
}

function setPositionForTable(
  table: Table,
  posInfo: PPTPositionStateCollection,
  lvl: number,
  flag: 0 | 1 | -1
) {
  if (table.getRowsCount() <= 0) return

  if (0 === flag && posInfo[lvl] == null) return
  // Todo 后续细粒度 apply (而不是新生成)时, 恢复对引用的检查

  let curRow = 0
  switch (flag) {
    case 0:
      curRow = posInfo[lvl].position
      break
    case 1:
      curRow = 0
      break
    case -1:
      curRow = table.children.length - 1
      break
  }

  let _posState: Nullable<PPTPositionStateCollection> = posInfo,
    _flag = flag
  if (null !== posInfo && true === posInfo[lvl].deleted) {
    if (curRow < table.children.length) {
      _posState = null
      _flag = 1
    } else if (curRow > 0) {
      curRow--
      _posState = null
      _flag = -1
    } else {
      return
    }
  }

  if (curRow >= table.getRowsCount()) {
    curRow = table.getRowsCount() - 1
    _posState = null
    _flag = -1
  } else if (curRow < 0) {
    curRow = 0
    _posState = null
    _flag = 1
  }

  const row = table.getRow(curRow)
  if (!row) return

  let curCell = 0
  switch (_flag) {
    case 0:
      curCell = _posState![lvl + 1].position
      break
    case 1:
      curCell = 0
      break
    case -1:
      curCell = row.cellsCount - 1
      break
  }

  let __docPos: Nullable<PPTPositionStateCollection> = _posState,
    __flag = _flag
  if (null !== _posState && true === _posState[lvl + 1].deleted) {
    if (curCell < row.cellsCount) {
      __docPos = null
      __flag = 1
    } else if (curCell > 0) {
      curCell--
      __docPos = null
      __flag = -1
    } else {
      return
    }
  }

  if (curCell >= row.cellsCount) {
    curCell = row.cellsCount - 1
    __docPos = null
    __flag = -1
  } else if (curCell < 0) {
    curCell = 0
    __docPos = null
    __flag = 1
  }

  const cell = row.getCellByIndex(curCell)
  if (!cell) return

  table.curCell = cell
  // selection 可能被 change 所破坏因此不能直接 restore, 改为 cucell selection
  table.selection.selectionStartInfo.cellPos = {
    row: row.index,
    cell: cell.index,
  }
  table.selection.selectionEndInfo.cellPos = {
    row: row.index,
    cell: cell.index,
  }
  setPositionForTextDocume(table.curCell.textDoc, __docPos!, lvl + 2, __flag)
}

function setPositionForTextDocume(
  textDoc: TextDocument,
  posInfo: PPTPositionStateCollection,
  lvl: number,
  flag: 0 | 1 | -1
) {
  if (0 === flag && posInfo[lvl] == null) return
  // Todo 后续细粒度 apply (而不是新生成)时, 恢复对引用的检查

  if (textDoc.children.length <= 0) return

  let pos = 0
  switch (flag) {
    case 0:
      pos = posInfo[lvl].position
      break
    case 1:
      pos = 0
      break
    case -1:
      pos = textDoc.children.length - 1
      break
  }

  let _posState: Nullable<PPTPositionStateCollection> = posInfo,
    _flag = flag
  if (null !== posInfo && true === posInfo[lvl].deleted) {
    if (pos < textDoc.children.length) {
      _posState = null
      _flag = 1
    } else if (pos > 0) {
      pos--
      _posState = null
      _flag = -1
    } else {
      return
    }
  }

  pos = Math.min(textDoc.children.length - 1, Math.max(0, pos))
  textDoc.cursorPosition.childIndex = pos
  const para = textDoc.children.at(pos)
  para && setPositionForParagraph(para, _posState!, lvl + 1, _flag)
}

function setPositionForParagraph(
  paragraph: Paragraph,
  posInfo: PPTPositionStateCollection,
  lvl: number,
  flag: 0 | 1 | -1
) {
  if (0 === flag && (!posInfo[lvl] || paragraph !== posInfo[lvl].entity)) {
    return
  }

  let pos = 0
  switch (flag) {
    case 0:
      pos = posInfo[lvl].position
      break
    case 1:
      pos = 0
      break
    case -1:
      pos = paragraph.children.length - 1
      break
  }

  let _posState: Nullable<PPTPositionStateCollection> = posInfo,
    _flag = flag
  if (null !== posInfo && true === posInfo[lvl].deleted) {
    if (pos < paragraph.children.length) {
      _posState = null
      _flag = 1
    } else if (pos > 0) {
      pos--
      _posState = null
      _flag = -1
    } else {
      return
    }
  }

  if (pos === paragraph.children.length - 1 && paragraph.children.length > 1) {
    pos = paragraph.children.length - 2
    _flag = -1
    _posState = null
  }

  paragraph.cursorPosition.elementIndex = pos
  const paraItem = paragraph.children.at(pos)
  if (paraItem) {
    setPositionForParaItem(paraItem, _posState!, lvl + 1, _flag)
  } else paragraph.correctElementIndex()
}
function setPositionForParaItem(
  paraItem: ParagraphItem,
  posInfo: PPTPositionStateCollection,
  lvl: number,
  flag: 0 | 1 | -1
) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    if (paraItem.children.length <= 0) return

    if (0 === flag && (!posInfo[lvl] || paraItem !== posInfo[lvl].entity)) {
      return
    }

    let pos = 0
    switch (flag) {
      case 0:
        pos = posInfo[lvl].position
        break
      case 1:
        pos = 0
        break
      case -1:
        pos = paraItem.children.length - 1
        break
    }

    let _posState: Nullable<PPTPositionStateCollection> = posInfo,
      _flag = flag
    if (null !== posInfo && true === posInfo[lvl].deleted) {
      if (pos < paraItem.children.length) {
        _posState = null
        _flag = 1
      } else if (pos > 0) {
        pos--
        _posState = null
        _flag = -1
      } else {
        return
      }
    }

    paraItem.state.elementIndex = Math.max(
      0,
      Math.min(paraItem.children.length - 1, pos)
    )

    if (paraItem.cursorIndex) paraItem.cursorIndex = paraItem.state.elementIndex

    const run = paraItem.children.at(pos)
    if (run) {
      setPositionForParaRun(run, _posState!, lvl + 1, _flag)
    }
  } else {
    setPositionForParaRun(paraItem, posInfo, lvl, flag)
  }
}
function setPositionForParaRun(
  run: ParaRun,
  posInfo: PPTPositionStateCollection,
  depth: number,
  flag: 0 | 1 | -1
) {
  let pos = 0
  switch (flag) {
    case 1:
      pos = 0
      break
    case -1:
      pos = run.children.length
      break
    case 0:
      pos = posInfo[depth].position
      break
  }

  const valueOfLen = run.children.length
  if (
    valueOfLen > 0 &&
    pos >= valueOfLen &&
    InstanceType.ParaEnd === run.children.at(valueOfLen - 1)!.instanceType
  ) {
    pos = valueOfLen - 1
  }

  run.state.elementIndex = pos
}
