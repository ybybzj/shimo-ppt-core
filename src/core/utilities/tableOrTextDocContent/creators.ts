import { TextDocument } from '../../TextDocument/TextDocument'
import { TextBody } from '../../SlideElement/TextBody'
import { moveCursorToStartPosInTextDocument } from '../cursor/moveToEdgePos'

export function createTextDocumentFromString(str: string, parent: TextBody) {
  const textDoc = TextDocument.new(parent, 0, 0, 0, 0)
  moveCursorToStartPosInTextDocument(textDoc, false)
  textDoc.insertText(str)
  return textDoc
}
