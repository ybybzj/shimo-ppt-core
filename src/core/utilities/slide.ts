import { Nullable } from '../../../liber/pervasive'
import { Slide } from '../Slide/Slide'
import { SlideLayout } from '../Slide/SlideLayout'
import { SlideMaster } from '../Slide/SlideMaster'
import { BlipFill } from '../SlideElement/attrs/fill/blipFill'
import { checkImageSrc } from '../../common/utils'
import { InstanceType } from '../instanceTypes'
import { SlideElement } from '../SlideElement/type'
import { SlideComments } from '../Slide/Comments/SlideComments'
import { getSpImages } from './shape/getters'
import { EntityForCalculation } from '../changes/type'
import { TransitionTypes } from '../Slide/SlideTransition'

export function isSlideHasTransition(slide: Slide): boolean {
  return slide.timing?.type !== TransitionTypes.None
}

export function isSlideHasAnimation(slide: Slide): boolean {
  return slide.animation && !!slide.animation.timeNodes.length
}

export function getAllImages(
  slideLike: Slide | SlideLayout | SlideMaster,
  images = {}
) {
  const imageSrcId = getSlideBackgroundImageSrcId(slideLike)
  if (imageSrcId) {
    images[checkImageSrc(imageSrcId)] = true
  }
  const spTree = slideLike.cSld.spTree
  spTree.forEach((sp) => {
    getSpImages(sp, images)
  })
  return images
}

export function getSlideBackgroundImageSrcId(
  slideLike: Slide | SlideLayout | SlideMaster
): string | undefined {
  const fill = slideLike.cSld.bg?.bgPr?.fill?.fill
  if (fill instanceof BlipFill && typeof fill.imageSrcId === 'string') {
    return fill.imageSrcId
  }
}

export function checkHitInSlideInnerArea(
  slideLike: Slide | SlideLayout | SlideMaster,
  x: number,
  y: number
): boolean {
  const { width, height } = slideLike
  return x > 0 && x < width && y > 0 && y < height
}

export function getSlideIndex(
  sp: EntityForCalculation | SlideComments
): Nullable<number> {
  if (
    sp.instanceType === InstanceType.SlideMaster ||
    sp.instanceType === InstanceType.SlideLayout
  ) {
    return null
  }

  if (sp.instanceType === InstanceType.Slide) {
    return sp.index
  }
  if (sp.instanceType === InstanceType.SlideComments) {
    if (sp.slide) {
      return sp.slide.index
    }
    return null
  }

  if (sp.parent?.instanceType === InstanceType.Slide) {
    return sp.parent.index
  }
  return null
}

function getAllSlideElements(spTree: SlideElement[]) {
  return spTree.reduce((acc: SlideElement[], element: SlideElement) => {
    if (element) {
      switch (element.instanceType) {
        case InstanceType.GroupShape:
          acc = acc.concat(getAllSlideElements(element.spTree))
          break
        default:
          acc.push(element)
      }
    }
    return acc
  }, [])
}

export function getShapeFromSlideByRefId(slide: Slide, spRefId: string) {
  if (slide) {
    const shapes = getAllSlideElements(slide.cSld.spTree)
    return shapes.find((sp) => sp.getRefId() === spRefId)
  }
}
