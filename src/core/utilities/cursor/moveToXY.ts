import { Paragraph } from '../../Paragraph/Paragraph'
import { getSearchPositionInfoFromParagraph } from '../../Paragraph/utils/position'
import { calculateCursorPositionForParagraph } from './cursorPosition'

export function moveCursorToXYInParagraph(
  paragraph: Paragraph,
  x: number,
  y: number,
  isLine: boolean,
  isDontChangeRealPos: boolean,
  colIndex?: number
) {
  const searchInfo = getSearchPositionInfoFromParagraph(
    paragraph,
    x,
    y,
    colIndex,
    isLine,
    false
  )

  paragraph.updateCursorPosByPosition(searchInfo.pos, false, searchInfo.line)
  calculateCursorPositionForParagraph(paragraph, false, false, false)

  if (isDontChangeRealPos !== true) {
    paragraph.cursorPosition.cursorX = paragraph.cursorPosition.x
    paragraph.cursorPosition.cursorY = paragraph.cursorPosition.y
  }

  if (true !== isLine) {
    paragraph.cursorPosition.cursorX = x
    paragraph.cursorPosition.cursorY = y
  }
}
