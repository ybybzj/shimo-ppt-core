import { InstanceType } from '../../instanceTypes'
import { TableSelectionType } from '../../common/const/table'
import { Paragraph } from '../../Paragraph/Paragraph'
import { Table } from '../../Table/Table'
import { TableCell } from '../../Table/TableCell'
import { TextDocument } from '../../TextDocument/TextDocument'
import { TableOrTextDocument } from '../type'

export function getCursorCoordsInParagraph(para: Paragraph) {
  return { x: para.cursorPosition.cursorX, y: para.cursorPosition.cursorY }
}

export function getCursorCoordsInTableOrTextDocument(
  target: TableOrTextDocument
) {
  if (target.instanceType === InstanceType.Table) {
    return getCursorCoordsInTable(target)
  } else {
    return getCursorCoordsInTextDocument(target)
  }
}

function getCursorCoordsInTextDocument(textDoc: TextDocument) {
  const index =
    true === textDoc.selection.isUse
      ? textDoc.selection.endIndex
      : textDoc.cursorPosition.childIndex
  const para = textDoc.children.at(index)
  return para ? getCursorCoordsInParagraph(para) : { x: 0, y: 0 }
}

function getCursorCoordsInTable(table: Table) {
  if (
    true === table.selection.isUse &&
    TableSelectionType.CELL === table.selection.type
  ) {
    if (table.selection.cellPositions.length < 0) return { x: 0, y: 0 }

    const pos = table.selection.cellPositions[0]
    const cell = table.children.at(pos.row)?.getCellByIndex(pos.cell)
    if (cell == null) return { x: 0, y: 0 }
    const para = cell.textDoc.getFirstParagraph()!

    return { x: para.renderingState.x, y: para.renderingState.y }
  } else return getCursorCoordsInTextDocument(table.curCell!.textDoc)
}

export function getCursorCoordsInCell(cell: TableCell) {
  return {
    x: cell.textDoc.cursorPosition.cursorX,
    y: cell.textDoc.cursorPosition.cursorY,
  }
}
