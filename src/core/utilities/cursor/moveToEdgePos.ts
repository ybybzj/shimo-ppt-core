import { InstanceType } from '../../instanceTypes'
import { TableSelectionType } from '../../common/const/table'
import { ParagraphElementPosition } from '../../Paragraph/common'
import { ParaHyperlink } from '../../Paragraph/ParaContent/ParaHyperlink'
import { ParaRun } from '../../Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../../Paragraph/Paragraph'
import {
  getEndPosOfParagraph,
  getStartPosOfParagraph,
} from '../../Paragraph/utils/position'
import { updateSelectionStateForParagraph } from '../../Paragraph/utils/selection'
import { Table } from '../../Table/Table'
import { TextDocument } from '../../TextDocument/TextDocument'

export function moveCursorToStartPosForParaItem(
  paraItem: ParaRun | ParaHyperlink
) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    paraItem.state.elementIndex = 0
  } else {
    paraItem.state.elementIndex = 0

    if (paraItem.children.length > 0) {
      paraItem.children.at(0)!.state.elementIndex = 0
    }
  }
}
export function moveCursorToStartPosInParagraph(
  paragraph: Paragraph,
  isAddToSelect?: boolean
) {
  if (true === isAddToSelect) {
    let startPosition: ParagraphElementPosition

    if (true === paragraph.selection.isUse) {
      startPosition = paragraph.currentElementPosition(true, true)
    } else startPosition = paragraph.currentElementPosition(false, false)

    const endPosition = getStartPosOfParagraph(paragraph)

    paragraph.selection.isUse = true
    paragraph.selection.start = false

    updateSelectionStateForParagraph(paragraph, startPosition, endPosition)
  } else {
    paragraph.removeSelection()

    paragraph.cursorPosition.elementIndex = 0
    paragraph.cursorPosition.line = -1
    const item = paragraph.children.at(0)
    item && moveCursorToStartPosForParaItem(item)
    paragraph.correctElementIndex(false)
  }
}

export function moveCursorToStartPosInTextDocument(
  textDoc: TextDocument,
  isAddToSelect?: boolean
) {
  if (true === isAddToSelect) {
    const startIndex =
      true === textDoc.selection.isUse
        ? textDoc.selection.startIndex
        : textDoc.cursorPosition.childIndex
    const endIndex = 0

    textDoc.selection.start = false
    textDoc.selection.isUse = true
    textDoc.selection.startIndex = startIndex
    textDoc.selection.endIndex = endIndex

    textDoc.cursorPosition.childIndex = 0

    for (let i = startIndex - 1; i >= endIndex; i--) {
      const para = textDoc.children.at(i)
      para && para.selectAll(-1)
    }

    const para = textDoc.children.at(startIndex)
    para && moveCursorToStartPosInParagraph(para, true)
  } else {
    textDoc.removeSelection()

    textDoc.selection.start = false
    textDoc.selection.isUse = false
    textDoc.selection.startIndex = 0
    textDoc.selection.endIndex = 0

    textDoc.cursorPosition.childIndex = 0
    const para = textDoc.children.at(0)
    para && moveCursorToStartPosInParagraph(para, false)
  }
}

export function moveCursorToStartPosInTable(
  table: Table,
  isAddToSelect?: boolean
) {
  if (true === isAddToSelect) {
    const startRowIndex =
      true === table.selection.isUse
        ? table.selection.selectionStartInfo.cellPos.row
        : table.curCell!.parent.index
    const startRow = table.children.at(startRowIndex)
    if (startRow == null) return
    const endRowIndex = 0

    table.selection.isUse = true
    table.selection.start = false
    table.selection.type = TableSelectionType.CELL
    table.selection.selectionType = TableSelectionType.COMMON
    table.selection.selectionStartInfo.cellPos = {
      row: startRowIndex,
      cell: startRow.cellsCount - 1,
    }
    table.selection.selectionEndInfo.cellPos = { row: endRowIndex, cell: 0 }
    table.selection.currentRowIndex = endRowIndex

    table.updateSelectedCellPositions()
  } else {
    table.curCell = table.children.at(0)?.getCellByIndex(0)

    table.selection.isUse = false
    table.selection.start = false
    table.selection.selectionStartInfo.cellPos = { row: 0, cell: 0 }
    table.selection.selectionEndInfo.cellPos = { row: 0, cell: 0 }
    table.selection.currentRowIndex = 0
    if (table.curCell) moveCursorToStartPosInTextDocument(table.curCell.textDoc)
  }
}

export function moveCursorToEndPosForParaItem(
  paraItem: ParaRun | ParaHyperlink,
  selectFromEnd?: boolean
) {
  if (paraItem.instanceType === InstanceType.ParaRun) {
    if (true === selectFromEnd) {
      const selection = paraItem.state.selection
      selection.isUse = true
      selection.startIndex = paraItem.children.length
      selection.endIndex = paraItem.children.length
    } else {
      let index = paraItem.children.length

      while (index > 0) {
        if (
          InstanceType.ParaEnd === paraItem.children.at(index - 1)!.instanceType
        ) {
          index--
        } else break
      }

      paraItem.state.elementIndex = index
    }
  } else {
    const contentLen = paraItem.children.length

    if (contentLen > 0) {
      paraItem.state.elementIndex = contentLen - 1
      moveCursorToEndPosForParaItem(
        paraItem.children.at(contentLen - 1)!,
        selectFromEnd
      )
    }
  }
}

export function moveCursorToEndPosInParagraph(
  paragraph: Paragraph,
  isAddToSelect?: boolean,
  isStartSelectFromEnd?: boolean
) {
  if (true === isAddToSelect) {
    let startPosition: ParagraphElementPosition

    if (true === paragraph.selection.isUse) {
      startPosition = paragraph.currentElementPosition(true, true)
    } else if (true === isStartSelectFromEnd) {
      startPosition = getEndPosOfParagraph(paragraph, true)
    } else startPosition = paragraph.currentElementPosition(false, false)

    const endPosition = getEndPosOfParagraph(paragraph, true)

    paragraph.selection.isUse = true
    paragraph.selection.start = false

    updateSelectionStateForParagraph(paragraph, startPosition, endPosition)
  } else {
    if (true === isStartSelectFromEnd) {
      paragraph.selection.isUse = true
      paragraph.selection.start = false

      paragraph.selection.startIndex = paragraph.children.length - 1
      paragraph.selection.endIndex = paragraph.children.length - 1

      paragraph.cursorPosition.elementIndex = paragraph.children.length - 1

      const item = paragraph.children.at(paragraph.cursorPosition.elementIndex)
      item && moveCursorToEndPosForParaItem(item, true)
    } else {
      paragraph.removeSelection()

      paragraph.cursorPosition.elementIndex = paragraph.children.length - 1
      paragraph.cursorPosition.line = -1
      const item = paragraph.children.at(paragraph.cursorPosition.elementIndex)
      item && moveCursorToEndPosForParaItem(item)
      paragraph.correctElementIndex(false)
    }
  }
}

export function moveCursorToEndPosInTextDocument(
  textDoc: TextDocument,
  isAddToSelect?: boolean,
  isStartSelectFromEnd?: boolean
) {
  if (true === isAddToSelect) {
    const startIndex =
      true === textDoc.selection.isUse
        ? textDoc.selection.startIndex
        : textDoc.cursorPosition.childIndex
    const endIndex = textDoc.children.length - 1

    textDoc.selection.start = false
    textDoc.selection.isUse = true
    textDoc.selection.startIndex = startIndex
    textDoc.selection.endIndex = endIndex

    textDoc.cursorPosition.childIndex = textDoc.children.length - 1

    for (let i = startIndex + 1; i <= endIndex; i++) {
      const para = textDoc.children.at(i)
      para && para.selectAll(1)
    }

    const para = textDoc.children.at(startIndex)
    para && moveCursorToEndPosInParagraph(para, true)
  } else {
    if (true === isStartSelectFromEnd) {
      textDoc.selection.start = false
      textDoc.selection.isUse = true
      textDoc.selection.startIndex = textDoc.children.length - 1
      textDoc.selection.endIndex = textDoc.children.length - 1
      textDoc.cursorPosition.childIndex = textDoc.children.length - 1
      const para = textDoc.children.at(textDoc.children.length - 1)
      para && moveCursorToEndPosInParagraph(para, false, true)
    } else {
      textDoc.removeSelection()

      textDoc.selection.start = false
      textDoc.selection.isUse = false
      textDoc.selection.startIndex = 0
      textDoc.selection.endIndex = 0

      textDoc.cursorPosition.childIndex = textDoc.children.length - 1
      const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
      para && moveCursorToEndPosInParagraph(para, false)
    }
  }
}

export function moveCursorToEndPosInTable(
  table: Table,
  isAddToSelect?: boolean
) {
  if (true === isAddToSelect) {
    const startRowIndex =
      true === table.selection.isUse
        ? table.selection.selectionStartInfo.cellPos.row
        : table.curCell!.parent.index
    const endRowIndex = table.children.length - 1

    const endRow = table.children.at(endRowIndex)
    if (endRow) {
      table.selection.isUse = true
      table.selection.start = false
      table.selection.type = TableSelectionType.CELL
      table.selection.selectionType = TableSelectionType.COMMON
      table.selection.selectionStartInfo.cellPos = {
        row: startRowIndex,
        cell: 0,
      }
      table.selection.selectionEndInfo.cellPos = {
        row: endRowIndex,
        cell: endRow.cellsCount - 1,
      }
      table.selection.currentRowIndex = endRowIndex

      table.updateSelectedCellPositions()
    }
  } else {
    const row = table.children.at(table.children.length - 1)
    if (row) {
      table.curCell = row.getCellByIndex(row.cellsCount - 1)

      table.selection.isUse = false
      table.selection.start = false
      table.selection.selectionStartInfo.cellPos = {
        row: row.index,
        cell: table.curCell!.index,
      }
      table.selection.selectionEndInfo.cellPos = {
        row: row.index,
        cell: table.curCell!.index,
      }
      table.selection.currentRowIndex = row.index

      if (table.curCell) moveCursorToEndPosInTextDocument(table.curCell.textDoc)
    }
  }
}
