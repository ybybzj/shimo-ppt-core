import { EditActionFlag } from '../../EditActionFlag'
import { TableSelectionType, VMergeType } from '../../common/const/table'
import { startEditAction } from '../../calculation/informChanges/startAction'
import { calculateForRendering } from '../../calculation/calculate/calculateForRendering'
import { Table } from '../../Table/Table'
import { CellPos } from '../../Table/type'
import { addRowToTable } from '../../Table/utils/operations'
import { moveCursorToStartPosInTextDocument } from './moveToEdgePos'

export function moveCursorByCell(table: Table, isNext: boolean) {
  if (
    true === table.selection.isUse &&
    TableSelectionType.CELL === table.selection.type
  ) {
    const cellPos = table.selection.cellPositions[0]
    table.selection.type = TableSelectionType.TEXT
    table.curCell = table.children.at(cellPos.row)?.getCellByIndex(cellPos.cell)
    if (table.curCell) {
      table.curCell.textDoc.selectAll()
    }
  } else if (table.curCell) {
    let curCell = table.curCell
    const cellIndex = table.curCell.index
    const rowIndex = table.curCell.parent.index
    const cellPos = {
      cell: cellIndex,
      row: rowIndex,
    }

    if (true === isNext) {
      let cell = getNextCell(table, cellPos)
      while (null != cell && VMergeType.Restart !== cell.getVMerge()) {
        cell = getNextCell(table, cellPos)
      }

      if (null != cell) curCell = cell
      else {
        startEditAction(EditActionFlag.action_Document_TableAddNewRow)
        addRowToTable(table, false)
        calculateForRendering()

        let cell = getNextCell(table, cellPos)
        while (null != cell && VMergeType.Restart !== cell.getVMerge()) {
          cell = getNextCell(table, cellPos)
        }

        if (null != cell) curCell = cell
      }
    } else {
      let cell = getPrevCell(table, cellPos)
      while (null != cell && VMergeType.Restart !== cell.getVMerge()) {
        cell = getPrevCell(table, cellPos)
      }

      if (null != cell) curCell = cell
    }

    curCell.textDoc.selectAll()

    if (true === curCell.textDoc.hasNoSelection(false)) {
      moveCursorToStartPosInTextDocument(curCell.textDoc)
      table.selection.isUse = false
    } else {
      table.selection.isUse = true
    }
    table.selection.type = TableSelectionType.TEXT
    table.selection.currentRowIndex = curCell.parent.index
    table.selection.selectionStartInfo.cellPos = {
      row: curCell.parent.index,
      cell: curCell.index,
    }
    table.selection.selectionEndInfo.cellPos = {
      row: curCell.parent.index,
      cell: curCell.index,
    }
    table.curCell = curCell
    table.selectSelf()
  }
}
function getNextCell(table: Table, cellPos: CellPos) {
  const cellIndex = cellPos.cell
  const rowIndex = cellPos.row
  const curRow = table.children.at(rowIndex)
  if (curRow == null) return null

  if (cellIndex < curRow.cellsCount - 1) {
    cellPos.cell = cellIndex + 1
    return curRow.getCellByIndex(cellPos.cell)
  } else if (rowIndex < table.children.length - 1) {
    cellPos.row = rowIndex + 1
    cellPos.cell = 0
    return table.children.at(cellPos.row)?.getCellByIndex(cellPos.cell)
  } else return null
}

function getPrevCell(table: Table, cellPos: CellPos) {
  const cellIndex = cellPos.cell
  const rowIndex = cellPos.row

  if (cellIndex > 0) {
    cellPos.cell = cellIndex - 1
    return table.children.at(cellPos.row)?.getCellByIndex(cellPos.cell)
  } else if (rowIndex > 0 && rowIndex < table.children.length) {
    cellPos.row = rowIndex - 1
    const prevRow = table.children.at(rowIndex - 1)!
    cellPos.cell = prevRow.cellsCount - 1
    return prevRow.getCellByIndex(cellPos.cell)
  } else return null
}
