import { InstanceType } from '../../instanceTypes'
import { ParaHyperlink } from '../../Paragraph/ParaContent/ParaHyperlink'
import { ParaRun } from '../../Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../../Paragraph/Paragraph'
import { ParagraphItem } from '../../Paragraph/type'
import { updateStartPosForParaItem } from '../../Paragraph/utils/position'
import {
  moveCursorToStartPosForParaItem,
  moveCursorToEndPosForParaItem,
} from './moveToEdgePos'

export function moveCursorOutsideParaRun(run: ParaRun, isBefore: boolean) {
  const parent = run.getParent()
  if (!parent || parent.instanceType === InstanceType.ParaRun) return

  const runIndex = _getRunIndexInParent(run, parent)

  if (isBefore) {
    if (runIndex <= 0) {
      setParaItemToCurrentElement(run)

      moveCursorToStartPosForParaItem(run)
    } else {
      const element = parent.getChild(runIndex - 1)!
      setParaItemToCurrentElement(element)

      moveCursorToEndPosForParaItem(element)
    }
  } else {
    if (runIndex >= parent.childrenCount() - 1) {
      setParaItemToCurrentElement(run)

      moveCursorToEndPosForParaItem(run)
    } else {
      const element = parent.getChild(runIndex + 1)!
      setParaItemToCurrentElement(element)

      moveCursorToStartPosForParaItem(element)
    }
  }
}

function _getRunIndexInParent(run: ParaRun, parent: Paragraph | ParaHyperlink) {
  if (!parent.children) return -1

  for (let i = 0, l = parent.children.length; i < l; ++i) {
    if (run === parent.children.at(i)!) return i
  }

  return -1
}

function setParaItemToCurrentElement(paraItem: ParagraphItem) {
  const elementPosition =
    paraItem.paragraph.getContentPositionByElement(paraItem)
  if (!elementPosition) return

  const startPosition = elementPosition.clone()
  updateStartPosForParaItem(
    paraItem,
    startPosition,
    startPosition.getLevel() + 1
  )

  paraItem.paragraph.updateCursorPosByPosition(startPosition, true, -1, false)
  paraItem.paragraph.selectSelf()
}
