import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import { TableSelectionType } from '../../common/const/table'
import { MatrixUtils } from '../../graphic/Matrix'
import { ParagraphElementPosition } from '../../Paragraph/common'
import { Paragraph } from '../../Paragraph/Paragraph'
import {
  getCurrentParaItemPosInfo,
  getEndPosOfParagraph,
  getStartPosOfParagraph,
} from '../../Paragraph/utils/position'
import { updateSelectionStateForParagraph } from '../../Paragraph/utils/selection'
import { Table } from '../../Table/Table'
import { TableCell } from '../../Table/TableCell'
import { TextDocument } from '../../TextDocument/TextDocument'
import { TableOrTextDocument } from '../type'
import { calculateCursorPositionForParagraph } from './cursorPosition'
import {
  moveCursorToEndPosInParagraph,
  moveCursorToStartPosInParagraph,
  moveCursorToStartPosInTextDocument,
} from './moveToEdgePos'
import { moveCursorToXYInParagraph } from './moveToXY'
import { getCursorCoordsInCell, getCursorCoordsInParagraph } from './utils'
//#region move cursor up
export function moveCursorUpForTableOrContent(
  target: TableOrTextDocument,
  isAddToSelect: boolean
) {
  if (target.instanceType === InstanceType.TextDocument) {
    return moveCursorUpForTextDocument(target, isAddToSelect)
  } else {
    return moveCursorUpForTable(target, isAddToSelect)
  }
}

function moveCursorUpForParagraph(
  paragraph: Paragraph,
  isAddToSelect: boolean
) {
  let result = true
  let endPosition: ParagraphElementPosition
  if (true === paragraph.selection.isUse) {
    const curStartPosition = paragraph.currentElementPosition(true, true)
    const curEndPosition = paragraph.currentElementPosition(true, false)

    if (true === isAddToSelect) {
      const itemPosInfo = paragraph.paraItemPosInfoByContentPos(curEndPosition)
      if (itemPosInfo == null) return false

      const curLine = itemPosInfo.line

      if (0 === curLine) {
        endPosition = getStartPosOfParagraph(paragraph)

        result = false
      } else {
        moveCursorToXYInParagraph(
          paragraph,
          paragraph.cursorPosition.cursorX,
          curLine - 1,
          true,
          true
        )
        endPosition = paragraph.currentElementPosition(false, false)
      }

      paragraph.selection.isUse = true
      updateSelectionStateForParagraph(paragraph, curStartPosition, endPosition)
    } else {
      let topPosition = curStartPosition
      if (curStartPosition.compare(curEndPosition) > 0) {
        topPosition = curEndPosition
      }

      const itemPosInfo = paragraph.paraItemPosInfoByContentPos(topPosition)
      if (itemPosInfo == null) return false
      const curLine = itemPosInfo.line

      paragraph.updateCursorPosByPosition(topPosition, false, curLine)

      calculateCursorPositionForParagraph(paragraph, true, false, false)
      paragraph.cursorPosition.cursorX = paragraph.cursorPosition.x
      paragraph.cursorPosition.cursorY = paragraph.cursorPosition.y

      paragraph.removeSelection()

      if (0 === curLine) {
        return false
      } else {
        moveCursorToXYInParagraph(
          paragraph,
          paragraph.cursorPosition.cursorX,
          curLine - 1,
          true,
          true
        )
      }
    }
  } else {
    const itemPosInfo = getCurrentParaItemPosInfo(paragraph)
    if (itemPosInfo == null) return false
    const curLine = itemPosInfo.line

    if (true === isAddToSelect) {
      const startPosition = paragraph.currentElementPosition(false, false)
      let endPosition: ParagraphElementPosition

      if (0 === curLine) {
        endPosition = getStartPosOfParagraph(paragraph)

        result = false
      } else {
        moveCursorToXYInParagraph(
          paragraph,
          paragraph.cursorPosition.cursorX,
          curLine - 1,
          true,
          true
        )
        endPosition = paragraph.currentElementPosition(false, false)
      }

      paragraph.selection.isUse = true
      updateSelectionStateForParagraph(paragraph, startPosition, endPosition)
    } else {
      if (0 === curLine) {
        return false
      } else {
        moveCursorToXYInParagraph(
          paragraph,
          paragraph.cursorPosition.cursorX,
          curLine - 1,
          true,
          true
        )
      }
    }
  }

  return result
}

function moveCursorUpForTextDocument(
  textDoc: TextDocument,
  isAddToSelect: boolean
) {
  if (textDoc.cursorPosition.childIndex < 0) return false

  let result = true

  if (true === textDoc.selection.isUse) {
    if (true === isAddToSelect) {
      const selectDir =
        textDoc.selection.startIndex === textDoc.selection.endIndex
          ? 0
          : textDoc.selection.startIndex < textDoc.selection.endIndex
          ? 1
          : -1

      let para = textDoc.children.at(textDoc.selection.endIndex)
      if (para && false === moveCursorUpForParagraph(para, true)) {
        if (0 !== textDoc.selection.endIndex) {
          const { x, y } = getCursorCoordsInParagraph(para)
          textDoc.cursorPosition.cursorX = x
          textDoc.cursorPosition.cursorY = y

          if (1 === selectDir) para.removeSelection()

          textDoc.selection.endIndex--
          para = textDoc.children.at(textDoc.selection.endIndex)
          para &&
            moveCursorUpToLastRowInParagraph(
              para,
              textDoc.cursorPosition.cursorX,
              textDoc.cursorPosition.cursorY,
              true
            )
        } else {
          result = false
        }
      }

      para = textDoc.children.at(textDoc.selection.startIndex)
      if (
        textDoc.selection.startIndex === textDoc.selection.endIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        textDoc.selection.isUse = false
      }

      textDoc.cursorPosition.childIndex = textDoc.selection.endIndex
    } else {
      let startIndex = textDoc.selection.startIndex
      if (startIndex > textDoc.selection.endIndex) {
        startIndex = textDoc.selection.endIndex
      }

      textDoc.cursorPosition.childIndex = startIndex

      let para = textDoc.children.at(textDoc.cursorPosition.childIndex)
      if (para && false === moveCursorUpForParagraph(para, false)) {
        if (0 !== textDoc.cursorPosition.childIndex) {
          const { x, y } = getCursorCoordsInParagraph(para)
          textDoc.cursorPosition.cursorX = x
          textDoc.cursorPosition.cursorY = y

          textDoc.cursorPosition.childIndex--
          para = textDoc.children.at(textDoc.cursorPosition.childIndex)
          para &&
            moveCursorUpToLastRowInParagraph(
              para,
              textDoc.cursorPosition.cursorX,
              textDoc.cursorPosition.cursorY,
              false
            )
        } else {
          result = false
        }
      }

      textDoc.removeSelection()
    }
  } else {
    if (true === isAddToSelect) {
      textDoc.selection.isUse = true
      textDoc.selection.startIndex = textDoc.cursorPosition.childIndex
      textDoc.selection.endIndex = textDoc.cursorPosition.childIndex

      let para = textDoc.children.at(textDoc.cursorPosition.childIndex)
      if (para && false === moveCursorUpForParagraph(para, true)) {
        if (0 !== textDoc.cursorPosition.childIndex) {
          const { x, y } = getCursorCoordsInParagraph(para)
          textDoc.cursorPosition.cursorX = x
          textDoc.cursorPosition.cursorY = y

          textDoc.cursorPosition.childIndex--
          para = textDoc.children.at(textDoc.cursorPosition.childIndex)
          para &&
            moveCursorUpToLastRowInParagraph(
              para,
              textDoc.cursorPosition.cursorX,
              textDoc.cursorPosition.cursorY,
              true
            )
          textDoc.selection.endIndex = textDoc.cursorPosition.childIndex
        } else {
          result = false
        }
      }

      para = textDoc.children.at(textDoc.selection.startIndex)
      if (
        textDoc.selection.startIndex === textDoc.selection.endIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        textDoc.selection.isUse = false
      }

      textDoc.cursorPosition.childIndex = textDoc.selection.endIndex
    } else {
      let para = textDoc.children.at(textDoc.cursorPosition.childIndex)
      if (para && false === moveCursorUpForParagraph(para, false)) {
        if (0 !== textDoc.cursorPosition.childIndex) {
          const { x, y } = getCursorCoordsInParagraph(para)
          textDoc.cursorPosition.cursorX = x
          textDoc.cursorPosition.cursorY = y

          textDoc.cursorPosition.childIndex--
          para = textDoc.children.at(textDoc.cursorPosition.childIndex)
          para &&
            moveCursorUpToLastRowInParagraph(
              para,
              textDoc.cursorPosition.cursorX,
              textDoc.cursorPosition.cursorY,
              false
            )
        } else {
          result = false
        }
      }
    }
  }

  return result
}

function moveCursorUpForTable(table: Table, isAddToSelect: boolean) {
  if (
    true === table.selection.isUse &&
    TableSelectionType.CELL === table.selection.type
  ) {
    if (true === isAddToSelect) {
      let result = true
      const endCellPos = table.selection.selectionEndInfo.cellPos
      if (0 === endCellPos.row) {
        result = false
      } else {
        const endCell = table.children
          .at(endCellPos.row)
          ?.getCellByIndex(endCellPos.cell)
        if (endCell == null) return true

        const { x, y } = getCursorCoordsInCell(endCell)

        const prevRow = table.children.at(endCellPos.row - 1)
        let cell: Nullable<TableCell>
        if (prevRow) {
          for (let cellIndex = 0; cellIndex < prevRow.cellsCount; cellIndex++) {
            cell = prevRow.getCellByIndex(cellIndex)
            const cellInfo = prevRow.getCellInfo(cellIndex)
            if (x <= cellInfo.cellEndX) break
          }
        }

        if (null == cell) return true

        cell.setCursorPosXY(x, y)
        table.curCell = cell
        table.selection.selectionEndInfo.cellPos = {
          cell: cell.index,
          row: cell.parent.index,
        }
      }

      table.updateSelectedCellPositions()
      return result
    } else {
      if (table.selection.cellPositions.length < 0) return true

      const pos = table.selection.cellPositions[0]
      const cell = table.children.at(pos.row)?.getCellByIndex(pos.cell)
      if (cell == null) return true

      const para = cell.textDoc.getFirstParagraph()!
      const x = para.renderingState.x
      const y = para.renderingState.y

      table.selection.isUse = false
      if (0 === pos.row) {
        table.curCell = cell
        moveCursorToStartPosInTextDocument(table.curCell.textDoc)
        table.curCell.setCursorPosXY(x, y)

        return false
      } else {
        const prevRow = table.children.at(pos.row - 1)
        let prevCell
        if (prevRow) {
          for (let cellIndex = 0; cellIndex < prevRow.cellsCount; cellIndex++) {
            prevCell = prevRow.getCellByIndex(cellIndex)
            const cellInfo = prevRow.getCellInfo(cellIndex)
            if (x <= cellInfo.cellEndX) break
          }
        }

        if (null == prevCell) return true

        moveCursorUpToLastRowForCell(prevCell, x, y, false)
        table.curCell = prevCell
        return true
      }
    }
  } else if (table.curCell) {
    if (
      false ===
      moveCursorUpForTextDocument(table.curCell.textDoc, isAddToSelect)
    ) {
      if (0 === table.curCell.parent.index) {
        return true
      }

      if (true === isAddToSelect) {
        table.selection.isUse = true
        table.selection.type = TableSelectionType.CELL
        table.selection.selectionStartInfo.cellPos = {
          row: table.curCell.parent.index,
          cell: table.curCell.index,
        }

        let result = true
        if (0 === table.curCell.parent.index) {
          table.selection.selectionEndInfo.cellPos = { row: 0, cell: 0 }
          result = false
        } else {
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          const { x, y } = getCursorCoordsInCell(table.curCell)
          const prevRow = table.children.at(table.curCell.parent.index - 1)
          let cell: Nullable<TableCell>
          if (prevRow) {
            for (let ci = 0; ci < prevRow.cellsCount; ci++) {
              cell = prevRow.getCellByIndex(ci)
              const cellInfo = prevRow.getCellInfo(ci)
              if (x <= cellInfo.cellEndX) break
            }
          }
          if (null == cell) return true

          cell.setCursorPosXY(x, y)
          table.curCell = cell
          table.selection.selectionEndInfo.cellPos = {
            cell: cell.index,
            row: cell.parent.index,
          }
        }

        table.updateSelectedCellPositions()
        return result
      } else {
        if (0 === table.curCell.parent.index) return false
        else {
          const { x, y } = getCursorCoordsInCell(table.curCell)
          const prevRow = table.children.at(table.curCell.parent.index - 1)
          let cell: Nullable<TableCell>
          if (prevRow) {
            for (let ci = 0; ci < prevRow.cellsCount; ci++) {
              cell = prevRow.getCellByIndex(ci)
              const cellInfo = prevRow.getCellInfo(ci)
              if (x <= cellInfo.cellEndX) break
            }
          }

          if (null == cell) return true

          cell = table.getTopLeftMergedCell(cell.index, cell.parent.index)
          if (null == cell) return true

          moveCursorUpToLastRowForCell(cell, x, y, false)
          table.curCell = cell
          table.selection.selectionEndInfo.cellPos = {
            cell: cell.index,
            row: cell.parent.index,
          }
          table.selection.currentRowIndex = cell.parent.index

          return true
        }
      }
    } else {
      if (true === isAddToSelect) {
        table.selection.isUse = true
        table.selection.type = TableSelectionType.TEXT
        table.selection.selectionStartInfo.cellPos = {
          cell: table.curCell.index,
          row: table.curCell.parent.index,
        }
        table.selection.selectionEndInfo.cellPos = {
          cell: table.curCell.index,
          row: table.curCell.parent.index,
        }
      }

      return true
    }
  }
}

function moveCursorUpToLastRowInParagraph(
  paragraph: Paragraph,
  x: number,
  y: number,
  isAddToSelect: boolean
) {
  paragraph.cursorPosition.cursorX = x
  paragraph.cursorPosition.cursorY = y

  moveCursorToXYInParagraph(
    paragraph,
    x,
    paragraph.renderingState.getLineCount() - 1,
    true,
    true,
    paragraph.renderingState.columns.length - 1
  )

  if (true === isAddToSelect) {
    if (false === paragraph.selection.isUse) {
      paragraph.selection.isUse = true
      updateSelectionStateForParagraph(
        paragraph,
        getEndPosOfParagraph(paragraph, true),
        paragraph.currentElementPosition(false, false)
      )
    } else {
      updateSelectionStateForParagraph(
        paragraph,
        paragraph.currentElementPosition(true, true),
        paragraph.currentElementPosition(false, false)
      )
    }
  }
}

function moveCursorUpToLastRowForTextDocument(
  textDoc: TextDocument,
  x: number,
  y: number,
  isAddToSelect: boolean
) {
  textDoc.cursorPosition.cursorX = x
  textDoc.cursorPosition.cursorY = y
  if (true === isAddToSelect) {
    if (true !== textDoc.selection.isUse) {
      textDoc.cursorPosition.childIndex = textDoc.children.length - 1
      textDoc.selection.isUse = true
      textDoc.selection.start = false
      textDoc.selection.startIndex = textDoc.cursorPosition.childIndex
      textDoc.selection.endIndex = textDoc.cursorPosition.childIndex

      const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
      if (para) {
        moveCursorToEndPosInParagraph(para, false, true)
        moveCursorUpToLastRowInParagraph(para, x, y, true)
      }
    } else {
      const startIndex = textDoc.selection.startIndex
      const endIndex = textDoc.children.length - 1

      textDoc.cursorPosition.childIndex = endIndex

      const _S =
        textDoc.selection.startIndex <= textDoc.selection.endIndex
          ? textDoc.selection.startIndex
          : textDoc.selection.endIndex
      const _E =
        textDoc.selection.startIndex <= textDoc.selection.endIndex
          ? textDoc.selection.endIndex
          : textDoc.selection.startIndex
      for (let valueOfPos = _S; valueOfPos <= _E; ++valueOfPos) {
        if (valueOfPos !== startIndex) {
          const para = textDoc.children.at(valueOfPos)
          para && para.removeSelection()
        }
      }

      if (startIndex === endIndex) {
        textDoc.selection.startIndex = startIndex
        textDoc.selection.endIndex = startIndex
        const para = textDoc.children.at(startIndex)
        para && moveCursorUpToLastRowInParagraph(para, x, y, true)
      } else {
        let para = textDoc.children.at(startIndex)
        para && moveCursorToEndPosInParagraph(para, true)
        for (
          let valueOfPos = startIndex + 1;
          valueOfPos <= endIndex;
          ++valueOfPos
        ) {
          para = textDoc.children.at(valueOfPos)
          para && para.selectAll(1)
        }

        para = textDoc.children.at(endIndex)
        para && moveCursorUpToLastRowInParagraph(para, x, y, true)
      }
    }
  } else {
    textDoc.cursorPosition.childIndex = textDoc.children.length - 1
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
    para && moveCursorUpToLastRowInParagraph(para, x, y, false)
  }
}

function moveCursorUpToLastRowForCell(
  cell: TableCell,
  x: number,
  y: number,
  isAddToSelect: boolean
) {
  let transform = cell.getTextTransformByDirection()
  if (null != transform) {
    transform = MatrixUtils.invertMatrix(transform)
    x = transform.XFromPoint(x, y)
    y = transform.YFromPoint(x, y)
  }
  return moveCursorUpToLastRowForTextDocument(cell.textDoc, x, y, isAddToSelect)
}
//#endregion

//#region move cursor down
function moveCursorDownInParagraph(
  paragraph: Paragraph,
  isAddToSelect: boolean
) {
  let result = true
  let endPosition: ParagraphElementPosition
  if (true === paragraph.selection.isUse) {
    const curStartPosition = paragraph.currentElementPosition(true, true)
    const curEndPosition = paragraph.currentElementPosition(true, false)

    if (true === isAddToSelect) {
      const linePos = paragraph.paraItemPosInfoByContentPos(curEndPosition)
      if (linePos == null) return false
      const curLine = linePos.line

      if (paragraph.renderingState.isLastLine(curLine)) {
        endPosition = getEndPosOfParagraph(paragraph, true)

        result = false
      } else {
        moveCursorToXYInParagraph(
          paragraph,
          paragraph.cursorPosition.cursorX,
          curLine + 1,
          true,
          true
        )
        endPosition = paragraph.currentElementPosition(false, false)
      }

      paragraph.selection.isUse = true
      updateSelectionStateForParagraph(paragraph, curStartPosition, endPosition)
    } else {
      let bottomPosition = curEndPosition
      if (curStartPosition.compare(curEndPosition) > 0) {
        bottomPosition = curStartPosition
      }

      const linePos = paragraph.paraItemPosInfoByContentPos(bottomPosition)
      if (linePos == null) return false
      const curLine = linePos.line

      paragraph.updateCursorPosByPosition(bottomPosition, false, curLine)

      calculateCursorPositionForParagraph(paragraph, true, false, false)
      paragraph.cursorPosition.cursorX = paragraph.cursorPosition.x
      paragraph.cursorPosition.cursorY = paragraph.cursorPosition.y

      paragraph.removeSelection()

      if (paragraph.renderingState.isLastLine(curLine)) {
        return false
      } else {
        moveCursorToXYInParagraph(
          paragraph,
          paragraph.cursorPosition.cursorX,
          curLine + 1,
          true,
          true
        )
      }
    }
  } else {
    const linePos = getCurrentParaItemPosInfo(paragraph)
    if (linePos == null) return false
    const curLine = linePos.line

    if (true === isAddToSelect) {
      const startPos = paragraph.currentElementPosition(false, false)
      let endPosition: ParagraphElementPosition

      if (paragraph.renderingState.isLastLine(curLine)) {
        endPosition = getEndPosOfParagraph(paragraph, true)

        result = false
      } else {
        moveCursorToXYInParagraph(
          paragraph,
          paragraph.cursorPosition.cursorX,
          curLine + 1,
          true,
          true
        )
        endPosition = paragraph.currentElementPosition(false, false)
      }

      paragraph.selection.isUse = true
      updateSelectionStateForParagraph(paragraph, startPos, endPosition)
    } else {
      if (paragraph.renderingState.isLastLine(curLine)) {
        return false
      } else {
        moveCursorToXYInParagraph(
          paragraph,
          paragraph.cursorPosition.cursorX,
          curLine + 1,
          true,
          true
        )
      }
    }
  }

  return result
}
export function moveCursorDownInTextDocument(
  textDoc: TextDocument,
  isAddToSelect: boolean
) {
  if (textDoc.cursorPosition.childIndex < 0) return false

  let result = true

  if (true === textDoc.selection.isUse) {
    if (true === isAddToSelect) {
      const selectDirection =
        textDoc.selection.startIndex === textDoc.selection.endIndex
          ? 0
          : textDoc.selection.startIndex < textDoc.selection.endIndex
          ? 1
          : -1

      let para = textDoc.children.at(textDoc.selection.endIndex)
      if (para && false === moveCursorDownInParagraph(para, true)) {
        if (textDoc.children.length - 1 !== textDoc.selection.endIndex) {
          const { x, y } = getCursorCoordsInParagraph(para)
          textDoc.cursorPosition.cursorX = x
          textDoc.cursorPosition.cursorY = y

          if (-1 === selectDirection) para.removeSelection()

          textDoc.selection.endIndex++
          para = textDoc.children.at(textDoc.selection.endIndex)
          para &&
            moveCursorDownToFirstRowInParagraph(
              para,
              textDoc.cursorPosition.cursorX,
              textDoc.cursorPosition.cursorY,
              true
            )
        } else {
          result = false
        }
      }

      para = textDoc.children.at(textDoc.selection.startIndex)

      if (
        textDoc.selection.startIndex === textDoc.selection.endIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        textDoc.selection.isUse = false
      }

      textDoc.cursorPosition.childIndex = textDoc.selection.endIndex
    } else {
      let endIndex = textDoc.selection.endIndex
      if (endIndex < textDoc.selection.startIndex) {
        endIndex = textDoc.selection.startIndex
      }

      textDoc.cursorPosition.childIndex = endIndex

      let para = textDoc.children.at(textDoc.cursorPosition.childIndex)
      if (para && false === moveCursorDownInParagraph(para, false)) {
        if (textDoc.children.length - 1 !== textDoc.cursorPosition.childIndex) {
          const { x, y } = getCursorCoordsInParagraph(para)
          textDoc.cursorPosition.cursorX = x
          textDoc.cursorPosition.cursorY = y

          textDoc.cursorPosition.childIndex++
          para = textDoc.children.at(textDoc.cursorPosition.childIndex)
          para &&
            moveCursorDownToFirstRowInParagraph(
              para,
              textDoc.cursorPosition.cursorX,
              textDoc.cursorPosition.cursorY,
              false
            )
        } else {
          result = false
        }
      }

      textDoc.removeSelection()
    }
  } else {
    if (true === isAddToSelect) {
      textDoc.selection.isUse = true
      textDoc.selection.startIndex = textDoc.cursorPosition.childIndex
      textDoc.selection.endIndex = textDoc.cursorPosition.childIndex

      let para = textDoc.children.at(textDoc.cursorPosition.childIndex)
      if (para && false === moveCursorDownInParagraph(para, true)) {
        if (textDoc.children.length - 1 !== textDoc.cursorPosition.childIndex) {
          const { x, y } = getCursorCoordsInParagraph(para)
          textDoc.cursorPosition.cursorX = x
          textDoc.cursorPosition.cursorY = y

          textDoc.cursorPosition.childIndex++
          para = textDoc.children.at(textDoc.cursorPosition.childIndex)
          para &&
            moveCursorDownToFirstRowInParagraph(
              para,
              textDoc.cursorPosition.cursorX,
              textDoc.cursorPosition.cursorY,
              true
            )
          textDoc.selection.endIndex = textDoc.cursorPosition.childIndex
        } else {
          result = false
        }
      }

      para = textDoc.children.at(textDoc.selection.startIndex)
      if (
        textDoc.selection.startIndex === textDoc.selection.endIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        textDoc.selection.isUse = false
      }

      textDoc.cursorPosition.childIndex = textDoc.selection.endIndex
    } else {
      let para = textDoc.children.at(textDoc.cursorPosition.childIndex)

      if (para && false === moveCursorDownInParagraph(para, isAddToSelect)) {
        if (textDoc.children.length - 1 !== textDoc.cursorPosition.childIndex) {
          const { x, y } = getCursorCoordsInParagraph(para)
          textDoc.cursorPosition.cursorX = x
          textDoc.cursorPosition.cursorY = y

          textDoc.cursorPosition.childIndex++
          para = textDoc.children.at(textDoc.cursorPosition.childIndex)
          para &&
            moveCursorDownToFirstRowInParagraph(
              para,
              textDoc.cursorPosition.cursorX,
              textDoc.cursorPosition.cursorY,
              false
            )
        } else {
          result = false
        }
      }
    }
  }

  return result
}
export function moveCursorDownInTable(table: Table, isAddToSelect: boolean) {
  if (table.children.length <= 0) return true
  if (
    true === table.selection.isUse &&
    TableSelectionType.CELL === table.selection.type
  ) {
    if (true === isAddToSelect) {
      let result = true
      const endCellPos = table.selection.selectionEndInfo.cellPos
      if (table.children.length - 1 === endCellPos.row) {
        result = false
      } else {
        const endCell = table.children
          .at(endCellPos.row)
          ?.getCellByIndex(endCellPos.cell)
        if (endCell == null) return true

        const { x, y } = getCursorCoordsInCell(endCell)

        const nextRow = table.children.at(endCellPos.row + 1)
        let cell: Nullable<TableCell>
        if (nextRow) {
          for (let cellIndex = 0; cellIndex < nextRow.cellsCount; cellIndex++) {
            cell = nextRow.getCellByIndex(cellIndex)
            const cellInfo = nextRow.getCellInfo(cellIndex)
            if (x <= cellInfo.cellEndX) break
          }
        }
        if (null == cell) return true

        cell.setCursorPosXY(x, y)
        table.curCell = cell
        table.selection.selectionEndInfo.cellPos = {
          cell: cell.index,
          row: cell.parent.index,
        }
      }

      table.updateSelectedCellPositions()
      return result
    } else {
      if (table.selection.cellPositions.length < 0) return true

      const pos =
        table.selection.cellPositions[table.selection.cellPositions.length - 1]
      const cell = table.children.at(pos.row)?.getCellByIndex(pos.cell)
      if (cell == null) return true

      const para = cell.textDoc.getFirstParagraph()!
      const x = para.renderingState.x
      const y = para.renderingState.y

      table.selection.isUse = false
      if (table.children.length - 1 === pos.row) {
        table.curCell = cell
        moveCursorToStartPosInTextDocument(table.curCell.textDoc)
        table.curCell.setCursorPosXY(x, y)

        return false
      } else {
        const nextRow = table.children.at(pos.row + 1)
        let nextCell
        if (nextRow) {
          for (let cellIndex = 0; cellIndex < nextRow.cellsCount; cellIndex++) {
            nextCell = nextRow.getCellByIndex(cellIndex)
            const cellInfo = nextRow.getCellInfo(cellIndex)
            if (x <= cellInfo.cellEndX) break
          }
        }

        if (null == nextCell) return true

        moveCursorDownToFirstRowInTableCell(nextCell, x, y, false)
        table.curCell = nextCell
        return true
      }
    }
  } else if (table.curCell) {
    if (
      false ===
      moveCursorDownInTextDocument(table.curCell.textDoc, isAddToSelect)
    ) {
      if (true === isAddToSelect) {
        table.selection.isUse = true
        table.selection.type = TableSelectionType.CELL
        table.selection.selectionStartInfo.cellPos = {
          row: table.curCell.parent.index,
          cell: table.curCell.index,
        }

        let result = true
        if (table.children.length - 1 === table.curCell.parent.index) {
          table.selection.selectionEndInfo.cellPos = {
            row: table.children.length - 1,
            cell: table.children.at(table.children.length - 1)!.cellsCount - 1,
          }
          result = false
        } else {
          const { x, y } = getCursorCoordsInCell(table.curCell!)
          const nextRow = table.children.at(table.curCell.parent.index + 1)
          let cell: Nullable<TableCell>
          if (nextRow) {
            for (let ci = 0; ci < nextRow.cellsCount; ci++) {
              cell = nextRow.getCellByIndex(ci)
              const cellInfo = nextRow.getCellInfo(ci)
              if (x <= cellInfo.cellEndX) break
            }
          }
          if (null == cell) return true

          cell.setCursorPosXY(x, y)
          table.curCell = cell
          table.selection.selectionEndInfo.cellPos = {
            cell: cell.index,
            row: cell.parent.index,
          }
        }

        table.updateSelectedCellPositions()
        return result
      } else {
        const verticalMergeCount = table.getVMergeCountBelow(
          table.curCell.parent.index,
          table.curCell.parent.getCellInfo(table.curCell.index).startGridCol,
          table.curCell.getGridSpan()
        )

        if (
          table.children.length - 1 ===
          table.curCell.parent.index + verticalMergeCount - 1
        ) {
          return false
        } else {
          const { x, y } = getCursorCoordsInCell(table.curCell)

          const nextRow = table.children.at(
            table.curCell.parent.index + verticalMergeCount
          )
          let cell: Nullable<TableCell>
          if (nextRow) {
            for (let ci = 0; ci < nextRow.cellsCount; ci++) {
              cell = nextRow.getCellByIndex(ci)
              const cellInfo = nextRow.getCellInfo(ci)
              if (x <= cellInfo.cellEndX) break
            }
          }
          if (null == cell) return true

          moveCursorDownToFirstRowInTableCell(cell, x, y, false)
          table.curCell = cell
          table.selection.selectionEndInfo.cellPos = {
            cell: cell.index,
            row: cell.parent.index,
          }
          table.selection.currentRowIndex = cell.parent.index

          return true
        }
      }
    } else {
      if (true === isAddToSelect) {
        table.selection.isUse = true
        table.selection.type = TableSelectionType.TEXT
        table.selection.selectionStartInfo.cellPos = {
          cell: table.curCell.index,
          row: table.curCell.parent.index,
        }
        table.selection.selectionEndInfo.cellPos = {
          cell: table.curCell.index,
          row: table.curCell.parent.index,
        }
      }

      return true
    }
  }
}

function moveCursorDownToFirstRowInParagraph(
  paragraph: Paragraph,
  x: number,
  y: number,
  isAddToSelect: boolean
) {
  paragraph.cursorPosition.cursorX = x
  paragraph.cursorPosition.cursorY = y

  moveCursorToXYInParagraph(paragraph, x, 0, true, true)

  if (true === isAddToSelect) {
    if (false === paragraph.selection.isUse) {
      paragraph.selection.isUse = true
      updateSelectionStateForParagraph(
        paragraph,
        getStartPosOfParagraph(paragraph),
        paragraph.currentElementPosition(false, false)
      )
    } else {
      updateSelectionStateForParagraph(
        paragraph,
        paragraph.currentElementPosition(true, true),
        paragraph.currentElementPosition(false, false)
      )
    }
  }
}
function moveCursorDownToFirstRowInTextDocument(
  textDoc: TextDocument,
  x: number,
  y: number,
  isAddToSelect: boolean
) {
  textDoc.cursorPosition.cursorX = x
  textDoc.cursorPosition.cursorY = y
  if (true === isAddToSelect) {
    if (true !== textDoc.selection.isUse) {
      textDoc.cursorPosition.childIndex = 0
      textDoc.selection.isUse = true
      textDoc.selection.start = false
      textDoc.selection.startIndex = 0
      textDoc.selection.endIndex = 0

      const firstPara = textDoc.children.at(0)
      if (firstPara) {
        moveCursorToStartPosInParagraph(firstPara, false)
        moveCursorDownToFirstRowInParagraph(firstPara, x, y, true)
      }
    } else {
      const sIndex = textDoc.selection.startIndex
      const eIndex = 0

      textDoc.cursorPosition.childIndex = eIndex

      const startIndex =
        textDoc.selection.startIndex <= textDoc.selection.endIndex
          ? textDoc.selection.startIndex
          : textDoc.selection.endIndex
      const endIndex =
        textDoc.selection.startIndex <= textDoc.selection.endIndex
          ? textDoc.selection.endIndex
          : textDoc.selection.startIndex
      for (let i = startIndex; i <= endIndex; ++i) {
        if (i !== sIndex) {
          const para = textDoc.children.at(i)
          para && para.removeSelection()
        }
      }

      if (sIndex === eIndex) {
        textDoc.selection.startIndex = sIndex
        textDoc.selection.endIndex = sIndex
        const para = textDoc.children.at(sIndex)
        para && moveCursorDownToFirstRowInParagraph(para, x, y, true)
      } else {
        let para = textDoc.children.at(sIndex)
        para && moveCursorToStartPosInParagraph(para, true)
        for (let i = eIndex; i < sIndex; ++i) {
          para = textDoc.children.at(i)
          para && para.selectAll(-1)
        }
        para = textDoc.children.at(eIndex)
        para && moveCursorDownToFirstRowInParagraph(para, x, y, true)
      }
    }
  } else {
    textDoc.cursorPosition.childIndex = 0
    const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
    para && moveCursorDownToFirstRowInParagraph(para, x, y, false)
  }
}
function moveCursorDownToFirstRowInTableCell(
  cell: TableCell,
  x: number,
  y: number,
  isAddToSelect: boolean
) {
  let transform = cell.getTextTransformByDirection()
  if (null != transform) {
    transform = MatrixUtils.invertMatrix(transform)
    x = transform.XFromPoint(x, y)
    y = transform.YFromPoint(x, y)
  }
  return moveCursorDownToFirstRowInTextDocument(
    cell.textDoc,
    x,
    y,
    isAddToSelect
  )
}
//#endregion
