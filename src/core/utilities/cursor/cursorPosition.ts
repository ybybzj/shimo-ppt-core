import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import {
  decreaseRunElementIndices,
  getRunAndHyperlinkInfo,
  getRunElementByIndices,
  runElementEndIndicesFromPos,
  RunElementIndices,
  runElementIndicesFromPos,
} from '../../../re/export/textContent'
import { hookManager, Hooks } from '../../hooks'
import { calculateCursorPosForParaItem } from '../../Paragraph/ParaContent/utils/cursor'
import { updateTextCursor } from '../../Paragraph/ParaContent/utils/typeset'
import { Paragraph } from '../../Paragraph/Paragraph'
import { CursorPosition } from '../../Paragraph/type'
import { getCurrentParaItemPosInfo } from '../../Paragraph/utils/position'
import {
  getCurrentParaElementPosInfo,
  getXForCursorAtBiDiLine,
} from '../../Paragraph/utils/typeset'
import { Presentation } from '../../Slide/Presentation'
import { TableOrTextDocument } from '../type'

export function getAndUpdateCursorPositionForPresentation(
  presentation: Presentation,
  isUpdateX?: boolean,
  isUpdateY?: boolean
): Nullable<CursorPosition> {
  const textDoc = presentation.getCurrentTextTarget(true)
  if (textDoc) {
    return getAndUpdateCursorPositionForTargetOrTextContent(
      textDoc,
      isUpdateX,
      isUpdateY
    )
  }

  return {
    x: 0,
    y: 0,
    height: 0,
    lineAndCol: { line: 0, colIndex: 0 },
    transform: null,
  }
}

export function getAndUpdateCursorPositionForTargetOrTextContent(
  target: TableOrTextDocument,
  isUpdateX?: boolean,
  isUpdateY?: boolean
): Nullable<CursorPosition> {
  if (target.instanceType === InstanceType.Table) {
    if (target.curCell) {
      const cell = target.curCell
      const transform = cell.getTextTransformByDirection()
      const ret: {
        result: Nullable<CursorPosition>
      } = { result: undefined }
      hookManager.invoke(Hooks.EditorUI.OnMultiplyTargetTransform, {
        transform: transform,
        callback: () => {
          ret.result = getAndUpdateCursorPositionForTargetOrTextContent(
            cell.textDoc
          )
        },
      })
      return ret.result
    }

    return null
  } else {
    const curParaIndex = target.cursorPosition.childIndex

    const para = curParaIndex >= 0 ? target.children.at(curParaIndex) : null
    if (para) {
      const cursorPosInfo = getAndUpdateCursorPositionForParagraph(
        para,
        isUpdateX,
        isUpdateY
      )

      if (cursorPosInfo) {
        if (isUpdateX) target.cursorPosition.cursorX = cursorPosInfo.x
        if (isUpdateY) target.cursorPosition.cursorY = cursorPosInfo.y
      }
      return cursorPosInfo
    }
  }
}

export function getAndUpdateCursorPositionForParagraph(
  paragraph: Paragraph,
  isUpdateX?: boolean,
  isUpdateY?: boolean
): CursorPosition {
  const cursorPosInfo = calculateCursorPositionForParagraph(
    paragraph,
    true,
    true,
    false
  )

  if (isUpdateX) paragraph.cursorPosition.cursorX = cursorPosInfo.x
  if (isUpdateY) paragraph.cursorPosition.cursorY = cursorPosInfo.y
  return cursorPosInfo
}

export function calculateCursorPositionForParagraph(
  paragraph: Paragraph,
  updateCursorPos: boolean,
  updateCursor: boolean,
  returnCursorInfo: boolean
): CursorPosition {
  const transform = paragraph.getTextTransformOfParent()

  if (
    !paragraph.hasCalculated ||
    paragraph.renderingState.hasLine(0) === false
  ) {
    return {
      x: 0,
      y: 0,
      height: 0,
      lineAndCol: { line: 0, colIndex: 0 },
      transform: transform,
    }
  }

  const paraPos = getCurrentParaItemPosInfo(paragraph)

  if (
    paraPos == null ||
    -1 === paraPos.line ||
    paragraph.renderingState.hasLine(paraPos.line) === false
  ) {
    return {
      x: 0,
      y: 0,
      height: 0,
      lineAndCol: { line: 0, colIndex: 0 },
      transform: transform,
    }
  }

  let curLine = paraPos.line
  const colIndex = paraPos.colIndex

  if (-1 !== paragraph.cursorPosition.line) {
    curLine = paragraph.cursorPosition.line
  }

  const segment = paragraph.renderingState.getSegmentsByLine(curLine)

  if (segment == null) {
    return {
      x: 0,
      y: 0,
      height: 0,
      lineAndCol: { line: 0, colIndex: 0 },
      transform: transform,
    }
  }

  let x = segment.XForRender
  const y = paragraph.renderingState.getYByLineAndCol(curLine, colIndex)

  if (!paragraph.isRTL && paragraph.paraNumbering.isSegmentValid(curLine)) {
    x += paragraph.paraNumbering.renderWidth
  }

  const hasArabic = paragraph.renderingState.hasArabicAtLine(curLine)
  if (paragraph.isRTL || hasArabic) {
    const curPosInfo = getCurrentParaElementPosInfo(paragraph)
    if (curPosInfo) {
      let isDrawAfter = false
      let curIndices: Nullable<RunElementIndices>
      let curRun = curPosInfo.run
      switch (curPosInfo.kind) {
        case 'behindLastInRun': {
          isDrawAfter = true
          curIndices = runElementEndIndicesFromPos(paragraph, curPosInfo.pos)
          break
        }
        case 'inRunContent':
        case 'inEmptyRun': {
          curIndices = runElementIndicesFromPos(paragraph, curPosInfo.pos)
          break
        }
      }

      // adjust cursorPos for render
      const curRunElm =
        curIndices && getRunElementByIndices(paragraph, curIndices)
      const instanceType = curRunElm?.instanceType
      switch (instanceType) {
        case InstanceType.ParaEnd: {
          curIndices =
            decreaseRunElementIndices(paragraph, curIndices!) ?? curIndices
          isDrawAfter = true
          const parentInfo = getRunAndHyperlinkInfo(paragraph, curIndices!)
          if (parentInfo) {
            curRun = parentInfo.run
          }
          break
        }
        case InstanceType.ParaText: {
          if (curRunElm?.isArabic()) {
            const prevIndices = decreaseRunElementIndices(
              paragraph,
              curIndices!
            )
            const prevElement =
              prevIndices && getRunElementByIndices(paragraph, prevIndices)
            if (
              prevElement?.instanceType === InstanceType.ParaText &&
              !prevElement.isArabic()
            ) {
              isDrawAfter = true
              curIndices = prevIndices
              const parentInfo = getRunAndHyperlinkInfo(paragraph, curIndices!)
              if (parentInfo) {
                curRun = parentInfo.run
              }
            }
          }
        }
      }

      const resX = getXForCursorAtBiDiLine(
        paragraph,
        curIndices,
        x,
        curLine,
        isDrawAfter
      )

      if (resX != null && curRun != null) {
        const res = updateTextCursor(
          curRun,
          paragraph,
          resX,
          y,
          colIndex,
          curLine,
          updateCursorPos,
          updateCursor,
          returnCursorInfo
        )
        res.transform = transform
        return res
      }
    }
  } else if (paragraph.children.length > 0) {
    const startIndex = segment.startIndex
    const endIndex = segment.endIndex

    for (
      let i = startIndex < 0 ? 0 : startIndex,
        _endIndex = Math.min(endIndex, paragraph.children.length - 1);
      i <= _endIndex;
      i++
    ) {
      const item = paragraph.children.at(i)!

      const res = calculateCursorPosForParaItem(
        item,
        x,
        y,
        i === paragraph.cursorPosition.elementIndex ? true : false,
        curLine,
        colIndex,
        updateCursorPos,
        updateCursor,
        returnCursorInfo
      )

      if (i === paragraph.cursorPosition.elementIndex) {
        res.transform = transform
        return res
      } else {
        x = res.x
      }
    }
  }

  return {
    x: x,
    y: y,
    lineAndCol: {
      line: curLine,
      colIndex: colIndex,
    },
    transform: transform,
  }
}
