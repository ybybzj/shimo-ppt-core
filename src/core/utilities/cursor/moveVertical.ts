import { Nullable } from '../../../../liber/pervasive'
import { TableSelectionType, VMergeType } from '../../common/const/table'
import { ParagraphLocationInfo } from '../../Paragraph/common'
import { Paragraph } from '../../Paragraph/Paragraph'
import {
  findLeftContentPosition,
  findRightContentPosition,
  findWordEndPos,
  findWordStartPos,
} from '../../Paragraph/utils/position'
import { updateSelectionStateForParagraph } from '../../Paragraph/utils/selection'
import { Table } from '../../Table/Table'
import { TextDocument } from '../../TextDocument/TextDocument'
import { calculateCursorPositionForParagraph } from './cursorPosition'
import {
  moveCursorToEndPosInParagraph,
  moveCursorToEndPosInTextDocument,
  moveCursorToStartPosInParagraph,
  moveCursorToStartPosInTextDocument,
} from './moveToEdgePos'

//#region move cursor left
export function moveCursorLeftInParagraph(
  paragraph: Paragraph,
  isAddToSelect: boolean,
  moveWord: boolean
) {
  if (true === paragraph.selection.isUse) {
    const endPosition = paragraph.currentElementPosition(true, false)
    const startPosition = paragraph.currentElementPosition(true, true)

    if (true !== isAddToSelect) {
      const presentationField = paragraph.getPresentationField()

      const checkedStartPos = paragraph.currentElementPosition(true, true)
      const checkedndPos = paragraph.currentElementPosition(true, false)

      let selectPos = checkedStartPos
      if (checkedStartPos.compare(checkedndPos) > 0) {
        selectPos = checkedndPos
      }

      paragraph.removeSelection()

      let pos = selectPos

      if (presentationField) {
        const locInfo = new ParagraphLocationInfo()
        findLeftContentPosition(paragraph, locInfo, selectPos)
        pos = locInfo.pos
      }

      paragraph.updateCursorPosByPosition(pos, true, -1)
    } else {
      const locInfo = new ParagraphLocationInfo()

      if (true === moveWord) {
        findWordStartPos(paragraph, locInfo, endPosition)
      } else findLeftContentPosition(paragraph, locInfo, endPosition)

      if (true === locInfo.found) {
        updateSelectionStateForParagraph(paragraph, startPosition, locInfo.pos)
      } else {
        return false
      }
    }
  } else {
    const locInfo = new ParagraphLocationInfo()
    const elementPosition = paragraph.currentElementPosition(false, false)

    if (true === moveWord) {
      findWordStartPos(paragraph, locInfo, elementPosition)
    } else findLeftContentPosition(paragraph, locInfo, elementPosition)

    if (true === isAddToSelect) {
      if (true === locInfo.found) {
        paragraph.selection.isUse = true
        updateSelectionStateForParagraph(
          paragraph,
          elementPosition,
          locInfo.pos
        )
      } else {
        paragraph.selection.isUse = false
        return false
      }
    } else {
      if (true === locInfo.found) {
        paragraph.updateCursorPosByPosition(locInfo.pos, true, -1)
      } else {
        return false
      }
    }
  }

  if (true === paragraph.selection.isUse) {
    const elementPosition = paragraph.currentElementPosition(true, false)
    paragraph.updateCursorPosByPosition(elementPosition, false, -1)
  }

  calculateCursorPositionForParagraph(paragraph, true, false, false)

  paragraph.cursorPosition.cursorX = paragraph.cursorPosition.x
  paragraph.cursorPosition.cursorY = paragraph.cursorPosition.y

  return true
}
export function moveCursorLeftInTextDocument(
  textDoc: TextDocument,
  isAddToSelect: boolean,
  moveWord: boolean
) {
  const isRTL = textDoc.children.at(textDoc.cursorPosition.childIndex)?.isRTL
  if (isRTL === true) {
    return moveCursorForwardInTextDocument(textDoc, isAddToSelect, moveWord)
  } else {
    return moveCursorBackwardInTextDocument(textDoc, isAddToSelect, moveWord)
  }
}

export function moveCursorLeftInTable(
  table: Table,
  isAddToSelect: boolean,
  moveWord: boolean
) {
  if (
    true === table.selection.isUse &&
    table.selection.type === TableSelectionType.CELL
  ) {
    if (true === isAddToSelect) {
      const startCellPos = table.selection.selectionStartInfo.cellPos
      const endCellPos = table.selection.selectionEndInfo.cellPos

      if (
        startCellPos.cell === endCellPos.cell &&
        startCellPos.row === endCellPos.row &&
        table.parent.isOnlyOneElementSelected()
      ) {
        table.selection.type = TableSelectionType.TEXT
        return true
      } else {
        let result = true
        if (
          (0 === endCellPos.cell && 0 === endCellPos.row) ||
          (!table.parent.isOnlyOneElementSelected() &&
            0 === endCellPos.row &&
            0 === startCellPos.row)
        ) {
          table.selection.selectionEndInfo.cellPos = { cell: 0, row: 0 }
          result = false
        } else if (
          endCellPos.cell > 0 &&
          table.parent.isOnlyOneElementSelected()
        ) {
          table.selection.selectionEndInfo.cellPos = {
            cell: endCellPos.cell - 1,
            row: endCellPos.row,
          }
        } else {
          table.selection.selectionEndInfo.cellPos = {
            cell: 0,
            row: endCellPos.row - 1,
          }
        }

        table.updateSelectedCellPositions()

        return result
      }
    } else {
      table.selection.isUse = false
      const cellPos = table.selection.cellPositions[0]
      table.curCell = table.children
        .at(cellPos.row)
        ?.getCellByIndex(cellPos.cell)
      if (table.curCell) {
        moveCursorToStartPosInTextDocument(table.curCell.textDoc)
      }
      return true
    }
  } else if (table.curCell) {
    if (
      false ===
      moveCursorLeftInTextDocument(
        table.curCell.textDoc,
        isAddToSelect,
        moveWord
      )
    ) {
      if (false === isAddToSelect) {
        let valueOfCurCell = table.curCell.getIndex()
        let valueOfCurRow = table.curCell.getRow().getIndex()
        if (0 !== valueOfCurCell || 0 !== valueOfCurRow) {
          while (true) {
            if (valueOfCurCell > 0) {
              valueOfCurCell--
            } else if (valueOfCurRow > 0) {
              valueOfCurRow--
              valueOfCurCell = table.getRow(valueOfCurRow)!.cellsCount - 1
            } else {
              table.curCell = table.getRow(0)?.getCellByIndex(0)
              break
            }

            const cell = table
              .getRow(valueOfCurRow)
              ?.getCellByIndex(valueOfCurCell)
            if (cell && VMergeType.Restart !== cell.getVMerge()) continue

            table.curCell = cell
            break
          }

          if (table.curCell) {
            moveCursorToEndPosInTextDocument(table.curCell.textDoc)
          }
        } else {
          return false
        }
      } else {
        table.selection.isUse = true
        table.selection.type = TableSelectionType.CELL

        let result = true
        table.selection.selectionStartInfo.cellPos = {
          cell: table.curCell.index,
          row: table.curCell.parent.index,
        }

        if (0 === table.curCell.index && 0 === table.curCell.parent.index) {
          table.selection.selectionEndInfo.cellPos = {
            cell: table.curCell.parent.cellsCount - 1,
            row: 0,
          }
          result = false
        } else if (table.curCell.index > 0) {
          table.selection.selectionEndInfo.cellPos = {
            cell: table.curCell.index - 1,
            row: table.curCell.parent.index,
          }
        } else {
          table.selection.selectionEndInfo.cellPos = {
            cell: 0,
            row: table.curCell.parent.index - 1,
          }
        }

        table.updateSelectedCellPositions()

        return result
      }
    } else {
      if (true === isAddToSelect) {
        table.selection.isUse = true
        table.selection.type = TableSelectionType.TEXT
        table.selection.selectionStartInfo.cellPos = {
          cell: table.curCell.index,
          row: table.curCell.parent.index,
        }
        table.selection.selectionEndInfo.cellPos = {
          cell: table.curCell.index,
          row: table.curCell.parent.index,
        }
      }

      return true
    }
  }
}
//#endregion

//#region move cursor right
export function moveCursorRightInParagraph(
  paragraph: Paragraph,
  isAddToSelect: boolean,
  moveWord: boolean
) {
  if (true === paragraph.selection.isUse) {
    const endPosition = paragraph.currentElementPosition(true, false)
    const startPosition = paragraph.currentElementPosition(true, true)

    if (true !== isAddToSelect) {
      if (true === paragraph.isParaEndSelected()) {
        paragraph.removeSelection()
        moveCursorToEndPosInParagraph(paragraph, false)
        return false
      } else {
        const presentationField = paragraph.getPresentationField()

        const curStartPosition = paragraph.currentElementPosition(true, true)
        const curEndPosition = paragraph.currentElementPosition(true, false)

        let selectPos = curEndPosition
        if (curStartPosition.compare(curEndPosition) > 0) {
          selectPos = curStartPosition
        }

        paragraph.removeSelection()

        let pos = selectPos

        if (presentationField) {
          const locInfo = new ParagraphLocationInfo()
          findRightContentPosition(paragraph, locInfo, selectPos)
          pos = locInfo.pos
        }
        paragraph.updateCursorPosByPosition(pos, true, -1)
      }
    } else {
      const locInfo = new ParagraphLocationInfo()

      if (true === moveWord) {
        findWordEndPos(paragraph, locInfo, endPosition, true)
      } else {
        findRightContentPosition(paragraph, locInfo, endPosition, true)
      }

      if (true === locInfo.found) {
        updateSelectionStateForParagraph(paragraph, startPosition, locInfo.pos)
      } else {
        return false
      }
    }
  } else {
    const locInfo = new ParagraphLocationInfo()
    const elementPosition = paragraph.currentElementPosition(false, false)

    if (true === moveWord) {
      findWordEndPos(paragraph, locInfo, elementPosition, isAddToSelect)
    } else {
      findRightContentPosition(
        paragraph,
        locInfo,
        elementPosition,
        isAddToSelect
      )
    }

    if (true === isAddToSelect) {
      if (true === locInfo.found) {
        paragraph.selection.isUse = true
        updateSelectionStateForParagraph(
          paragraph,
          elementPosition,
          locInfo.pos
        )
      } else {
        paragraph.selection.isUse = false
        return false
      }
    } else {
      if (true === locInfo.found) {
        paragraph.updateCursorPosByPosition(locInfo.pos, true, -1)
      } else {
        return false
      }
    }
  }

  if (true === paragraph.selection.isUse) {
    const elementPosition = paragraph.currentElementPosition(true, false)
    paragraph.updateCursorPosByPosition(elementPosition, false, -1)
  }

  calculateCursorPositionForParagraph(paragraph, true, false, false)

  paragraph.cursorPosition.cursorX = paragraph.cursorPosition.x
  paragraph.cursorPosition.cursorY = paragraph.cursorPosition.y

  return true
}

export function moveCursorRightInTextDocument(
  textDoc: TextDocument,
  isAddToSelect: boolean,
  moveWord: boolean,
  isMoveForward?: boolean
) {
  if (isMoveForward) {
    return moveCursorForwardInTextDocument(textDoc, isAddToSelect, moveWord)
  } else {
    const isRTL = textDoc.children.at(textDoc.cursorPosition.childIndex)?.isRTL
    if (isRTL === true) {
      return moveCursorBackwardInTextDocument(textDoc, isAddToSelect, moveWord)
    } else {
      return moveCursorForwardInTextDocument(textDoc, isAddToSelect, moveWord)
    }
  }
}

export function moveCursorRightInTable(
  table: Table,
  isAddToSelect: boolean,
  moveWord: boolean
) {
  const rowCount = table.children.length
  if (rowCount <= 0) return false
  if (
    true === table.selection.isUse &&
    table.selection.type === TableSelectionType.CELL
  ) {
    if (true === isAddToSelect) {
      const startCellPos = table.selection.selectionStartInfo.cellPos
      const endCellPos = table.selection.selectionEndInfo.cellPos

      if (
        startCellPos.cell === endCellPos.cell &&
        startCellPos.row === endCellPos.row &&
        table.parent.isOnlyOneElementSelected()
      ) {
        table.selection.type = TableSelectionType.TEXT
        return true
      } else {
        const endRowIndex = Math.max(0, endCellPos.row)
        const endCellIndex = Math.max(0, endCellPos.cell)

        let result = true
        if (rowCount - 1 <= endRowIndex) {
          const lastRow = table.getRow(rowCount - 1)!
          table.selection.selectionEndInfo.cellPos = {
            cell: lastRow.cellsCount - 1,
            row: lastRow.index,
          }

          result = false
        } else {
          const endRow = table.getRow(endRowIndex)!
          if (
            endCellIndex < endRow.cellsCount - 1 &&
            table.parent.isOnlyOneElementSelected()
          ) {
            table.selection.selectionEndInfo.cellPos = {
              cell: endCellIndex + 1,
              row: endRowIndex,
            }
          } else {
            table.selection.selectionEndInfo.cellPos = {
              cell: table.getRow(endRowIndex + 1)!.cellsCount - 1,
              row: endRowIndex + 1,
            }
          }
        }

        table.updateSelectedCellPositions()

        return result
      }
    } else {
      table.selection.isUse = false
      const pos =
        table.selection.cellPositions[table.selection.cellPositions.length - 1]
      table.curCell = table.children.at(pos.row)?.getCellByIndex(pos.cell)
      if (table.curCell) moveCursorToEndPosInTextDocument(table.curCell.textDoc)
      return true
    }
  } else if (table.curCell) {
    if (
      false ===
      moveCursorRightInTextDocument(
        table.curCell.textDoc,
        isAddToSelect,
        moveWord,
        true
      )
    ) {
      if (false === isAddToSelect) {
        let curCellIndex = table.curCell.index
        const curRow = table.curCell.parent
        let curRowIndex = curRow.getIndex()
        let cellsCount = curRow.cellsCount
        const rowsCount = table.children.length
        if (rowsCount - 1 > curRowIndex || cellsCount - 1 > curCellIndex) {
          while (true) {
            if (curCellIndex < cellsCount - 1) {
              curCellIndex++
            } else if (curRowIndex < rowsCount - 1) {
              curRowIndex++
              curCellIndex = 0
              cellsCount = table.getRow(curRowIndex)!.cellsCount
            } else {
              const lastRow = table.getRow(rowsCount - 1)!
              table.curCell = lastRow.getCellByIndex(lastRow.cellsCount - 1)
              break
            }

            const cell = table.getRow(curRowIndex)?.getCellByIndex(curCellIndex)
            if (cell && VMergeType.Restart !== cell.getVMerge()) continue

            table.curCell = cell
            break
          }

          if (table.curCell) {
            moveCursorToStartPosInTextDocument(table.curCell.textDoc)
          }
        } else {
          return false
        }
      } else {
        table.selection.isUse = true
        table.selection.type = TableSelectionType.CELL

        const lastRow = table.getRow(rowCount - 1)!
        const curRow = table.curCell.parent

        table.selection.selectionStartInfo.cellPos = {
          cell: table.curCell.index,
          row: table.curCell.parent.index,
        }

        let result = true

        if (table.curCell.index < curRow.cellsCount - 1) {
          table.selection.selectionEndInfo.cellPos = {
            cell: table.curCell.index + 1,
            row: table.curCell.parent.index,
          }
        } else if (table.curCell.parent.index >= rowCount - 1) {
          table.selection.selectionEndInfo.cellPos = {
            cell: lastRow.cellsCount - 1,
            row: lastRow.index,
          }

          result = false
        } else {
          table.selection.selectionEndInfo.cellPos = {
            cell: table.getRow(table.curCell.parent.index + 1)!.cellsCount - 1,
            row: table.curCell.parent.index + 1,
          }
        }

        table.updateSelectedCellPositions()

        return result
      }
    } else {
      if (true === isAddToSelect) {
        table.selection.isUse = true
        table.selection.type = TableSelectionType.TEXT
        table.selection.selectionStartInfo.cellPos = {
          cell: table.curCell!.index,
          row: table.curCell!.parent.index,
        }
        table.selection.selectionEndInfo.cellPos = {
          cell: table.curCell!.index,
          row: table.curCell!.parent.index,
        }
      }

      return true
    }
  }
}

//#endregion

export function moveCursorForwardInTextDocument(
  texDoc: TextDocument,
  isAddToSelect: boolean,
  moveWord: boolean
) {
  if (texDoc.cursorPosition.childIndex < 0) return false

  let result = true

  let para: Nullable<Paragraph>
  if (true === texDoc.selection.isUse) {
    if (true === isAddToSelect) {
      para = texDoc.children.at(texDoc.selection.endIndex)
      if (para && false === moveCursorRightInParagraph(para, true, moveWord)) {
        if (texDoc.children.length - 1 !== texDoc.selection.endIndex) {
          texDoc.selection.endIndex++
          texDoc.cursorPosition.childIndex = texDoc.selection.endIndex

          para = texDoc.children.at(texDoc.selection.endIndex)
          if (para) {
            moveCursorToStartPosInParagraph(para, false)
            moveCursorRightInParagraph(para, true, moveWord)
          }
        } else {
          result = false
        }
      }

      para = texDoc.children.at(texDoc.selection.endIndex)
      if (
        texDoc.selection.endIndex !== texDoc.selection.startIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        texDoc.selection.endIndex++
        texDoc.cursorPosition.childIndex = texDoc.selection.endIndex
      }

      para = texDoc.children.at(texDoc.selection.startIndex)
      if (
        texDoc.selection.startIndex === texDoc.selection.endIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        texDoc.selection.isUse = false
        texDoc.cursorPosition.childIndex = texDoc.selection.endIndex
      }
    } else {
      let endIndex = texDoc.selection.endIndex
      if (endIndex < texDoc.selection.startIndex) {
        endIndex = texDoc.selection.startIndex
      }

      texDoc.cursorPosition.childIndex = endIndex

      para = texDoc.children.at(texDoc.cursorPosition.childIndex)
      if (
        para &&
        true === para.isParaEndSelected() &&
        texDoc.cursorPosition.childIndex < texDoc.children.length - 1
      ) {
        texDoc.cursorPosition.childIndex = endIndex + 1
        para = texDoc.children.at(texDoc.cursorPosition.childIndex)
        para && moveCursorToStartPosInParagraph(para, false)
      } else {
        para = texDoc.children.at(texDoc.cursorPosition.childIndex)
        para && moveCursorRightInParagraph(para, false, moveWord)
      }

      texDoc.removeSelection()
    }
  } else {
    if (true === isAddToSelect) {
      texDoc.selection.isUse = true
      texDoc.selection.startIndex = texDoc.cursorPosition.childIndex
      texDoc.selection.endIndex = texDoc.cursorPosition.childIndex

      para = texDoc.children.at(texDoc.cursorPosition.childIndex)
      if (para && false === moveCursorRightInParagraph(para, true, moveWord)) {
        if (texDoc.children.length - 1 !== texDoc.cursorPosition.childIndex) {
          texDoc.cursorPosition.childIndex++
          texDoc.selection.endIndex = texDoc.cursorPosition.childIndex

          para = texDoc.children.at(texDoc.cursorPosition.childIndex)
          if (para) {
            moveCursorToStartPosInParagraph(para, false)
            moveCursorRightInParagraph(para, true, moveWord)
          }
        } else {
          result = false
        }
      }

      para = texDoc.children.at(texDoc.selection.startIndex)
      if (
        texDoc.selection.startIndex === texDoc.selection.endIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        texDoc.selection.isUse = false
        texDoc.cursorPosition.childIndex = texDoc.selection.endIndex
      }
    } else {
      para = texDoc.children.at(texDoc.cursorPosition.childIndex)
      if (para && false === moveCursorRightInParagraph(para, false, moveWord)) {
        if (texDoc.children.length - 1 !== texDoc.cursorPosition.childIndex) {
          texDoc.cursorPosition.childIndex++
          para = texDoc.children.at(texDoc.cursorPosition.childIndex)
          para && moveCursorToStartPosInParagraph(para, false)
        } else {
          result = false
        }
      }
    }
  }

  return result
}
function moveCursorBackwardInTextDocument(
  texDoc: TextDocument,
  isAddToSelect: boolean,
  moveWord: boolean
) {
  if (texDoc.cursorPosition.childIndex < 0) return false

  let result = true

  let para: Nullable<Paragraph>
  if (true === texDoc.selection.isUse) {
    if (true === isAddToSelect) {
      para = texDoc.children.at(texDoc.selection.endIndex)
      if (para && false === moveCursorLeftInParagraph(para, true, moveWord)) {
        if (0 !== texDoc.selection.endIndex) {
          texDoc.selection.endIndex--
          texDoc.cursorPosition.childIndex = texDoc.selection.endIndex

          para = texDoc.children.at(texDoc.selection.endIndex)
          if (para) {
            moveCursorToEndPosInParagraph(para, true, true)
            moveCursorLeftInParagraph(para, true, moveWord)
          }
        } else {
          result = false
        }
      }
      para = texDoc.children.at(texDoc.selection.endIndex)

      if (
        texDoc.selection.endIndex !== texDoc.selection.startIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        texDoc.selection.endIndex--
        texDoc.cursorPosition.childIndex = texDoc.selection.endIndex
      }

      para = texDoc.children.at(texDoc.selection.startIndex)
      if (
        texDoc.selection.startIndex === texDoc.selection.endIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        texDoc.selection.isUse = false
        texDoc.cursorPosition.childIndex = texDoc.selection.endIndex
      }
    } else {
      let start = texDoc.selection.startIndex
      if (start > texDoc.selection.endIndex) start = texDoc.selection.endIndex

      texDoc.cursorPosition.childIndex = start

      para = texDoc.children.at(texDoc.cursorPosition.childIndex)
      para && moveCursorLeftInParagraph(para, false, moveWord)

      texDoc.removeSelection()
    }
  } else {
    if (true === isAddToSelect) {
      texDoc.selection.isUse = true
      texDoc.selection.startIndex = texDoc.cursorPosition.childIndex
      texDoc.selection.endIndex = texDoc.cursorPosition.childIndex

      para = texDoc.children.at(texDoc.cursorPosition.childIndex)
      if (para && false === moveCursorLeftInParagraph(para, true, moveWord)) {
        if (0 !== texDoc.cursorPosition.childIndex) {
          texDoc.cursorPosition.childIndex--
          texDoc.selection.endIndex = texDoc.cursorPosition.childIndex

          para = texDoc.children.at(texDoc.cursorPosition.childIndex)
          if (para) {
            moveCursorToEndPosInParagraph(para, true, true)
            moveCursorLeftInParagraph(para, true, moveWord)
          }
        } else {
          result = false
        }
      }

      para = texDoc.children.at(texDoc.selection.startIndex)
      if (
        texDoc.selection.startIndex === texDoc.selection.endIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        texDoc.selection.isUse = false
        texDoc.cursorPosition.childIndex = texDoc.selection.endIndex
      }
    } else {
      para = texDoc.children.at(texDoc.cursorPosition.childIndex)
      if (para && false === moveCursorLeftInParagraph(para, false, moveWord)) {
        if (0 !== texDoc.cursorPosition.childIndex) {
          texDoc.cursorPosition.childIndex--

          para = texDoc.children.at(texDoc.cursorPosition.childIndex)
          para && moveCursorToEndPosInParagraph(para, false, false)
        } else {
          result = false
        }
      }
    }
  }

  return result
}
