import { TableSelectionType } from '../../common/const/table'
import { ParagraphLocationInfo } from '../../Paragraph/common'
import { Paragraph } from '../../Paragraph/Paragraph'
import {
  findEndSegmentPos,
  findStartSegmentPos,
} from '../../Paragraph/utils/position'
import { updateSelectionStateForParagraph } from '../../Paragraph/utils/selection'
import { Table } from '../../Table/Table'
import { TextDocument } from '../../TextDocument/TextDocument'
import { calculateCursorPositionForParagraph } from './cursorPosition'
import { moveCursorLeftInTable, moveCursorRightInTable } from './moveVertical'

export function moveCursorToEndOfLineInParagraph(
  paragraph: Paragraph,
  isAddToSelect: boolean
) {
  if (true === paragraph.selection.isUse) {
    const endPosition = paragraph.currentElementPosition(true, false)
    const startPosition = paragraph.currentElementPosition(true, true)

    if (true === isAddToSelect) {
      const locInfo = new ParagraphLocationInfo()
      findEndSegmentPos(paragraph, locInfo, endPosition)

      updateSelectionStateForParagraph(paragraph, startPosition, locInfo.pos)
    } else {
      let rightPosition = endPosition
      if (endPosition.compare(startPosition) < 0) {
        rightPosition = startPosition
      }

      const locInfo = new ParagraphLocationInfo()
      findEndSegmentPos(paragraph, locInfo, rightPosition)

      paragraph.removeSelection()

      paragraph.updateCursorPosByPosition(locInfo.pos, false, locInfo.line)
    }
  } else {
    const locInfo = new ParagraphLocationInfo()
    const elementPosition = paragraph.currentElementPosition(false, false)
    findEndSegmentPos(paragraph, locInfo, elementPosition)

    if (true === isAddToSelect) {
      paragraph.selection.isUse = true
      updateSelectionStateForParagraph(paragraph, elementPosition, locInfo.pos)
    } else {
      paragraph.updateCursorPosByPosition(locInfo.pos, false, locInfo.line)
    }
  }

  if (true === paragraph.selection.isUse) {
    const elementPosition = paragraph.currentElementPosition(true, false)
    paragraph.updateCursorPosByPosition(elementPosition, false, -1)
  }

  calculateCursorPositionForParagraph(paragraph, true, false, false)

  paragraph.cursorPosition.cursorX = paragraph.cursorPosition.x
  paragraph.cursorPosition.cursorY = paragraph.cursorPosition.y
}
export function moveCursorToEndOfLineInTextDocument(
  textDoc: TextDocument,
  isAddToSelect: boolean
) {
  if (textDoc.cursorPosition.childIndex < 0) return false

  if (true === textDoc.selection.isUse) {
    if (true === isAddToSelect) {
      let para = textDoc.children.at(textDoc.selection.endIndex)
      para && moveCursorToEndOfLineInParagraph(para, isAddToSelect)

      para = textDoc.children.at(textDoc.selection.startIndex)

      if (
        textDoc.selection.startIndex === textDoc.selection.endIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        textDoc.selection.isUse = false
        textDoc.cursorPosition.childIndex = textDoc.selection.endIndex
      }
    } else {
      const elmIndex =
        textDoc.selection.endIndex >= textDoc.selection.startIndex
          ? textDoc.selection.endIndex
          : textDoc.selection.startIndex
      textDoc.cursorPosition.childIndex = elmIndex

      const para = textDoc.children.at(elmIndex)
      para && moveCursorToEndOfLineInParagraph(para, isAddToSelect)

      textDoc.removeSelection()
    }
  } else {
    if (true === isAddToSelect) {
      textDoc.selection.isUse = true
      textDoc.selection.startIndex = textDoc.cursorPosition.childIndex
      textDoc.selection.endIndex = textDoc.cursorPosition.childIndex

      let para = textDoc.children.at(textDoc.cursorPosition.childIndex)
      para && moveCursorToEndOfLineInParagraph(para, isAddToSelect)

      para = textDoc.children.at(textDoc.selection.startIndex)
      if (
        textDoc.selection.startIndex === textDoc.selection.endIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        textDoc.selection.isUse = false
        textDoc.cursorPosition.childIndex = textDoc.selection.endIndex
      }
    } else {
      const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
      para && moveCursorToEndOfLineInParagraph(para, isAddToSelect)
    }
  }
}
export function moveCursorToEndOfLineInTable(
  table: Table,
  isAddToSelect: boolean
) {
  if (
    true === table.selection.isUse &&
    TableSelectionType.CELL === table.selection.type
  ) {
    return moveCursorRightInTable(table, isAddToSelect, false)
  } else {
    const result = moveCursorToEndOfLineInTextDocument(
      table.curCell!.textDoc,
      isAddToSelect
    )
    if (true === table.curCell!.textDoc.hasTextSelection()) {
      table.selection.isUse = true
      table.selection.type = TableSelectionType.TEXT
      table.selection.selectionStartInfo.cellPos = {
        cell: table.curCell!.index,
        row: table.curCell!.parent.index,
      }
      table.selection.selectionEndInfo.cellPos = {
        cell: table.curCell!.index,
        row: table.curCell!.parent.index,
      }
    } else {
      table.selection.isUse = false
    }

    return result
  }
}

export function moveCursorToStartOfLineInParagraph(
  paragraph: Paragraph,
  isAddToSelect: boolean
) {
  if (true === paragraph.selection.isUse) {
    const endPosition = paragraph.currentElementPosition(true, false)
    const startPosition = paragraph.currentElementPosition(true, true)

    if (true === isAddToSelect) {
      const locInfo = new ParagraphLocationInfo()
      findStartSegmentPos(paragraph, locInfo, endPosition)

      updateSelectionStateForParagraph(paragraph, startPosition, locInfo.pos)
    } else {
      let leftPos = startPosition
      if (startPosition.compare(endPosition) > 0) {
        leftPos = endPosition
      }

      const locInfo = new ParagraphLocationInfo()
      findStartSegmentPos(paragraph, locInfo, leftPos)

      paragraph.removeSelection()

      paragraph.updateCursorPosByPosition(locInfo.pos, false, locInfo.line)
    }
  } else {
    const locInfo = new ParagraphLocationInfo()
    const elementPosition = paragraph.currentElementPosition(false, false)

    findStartSegmentPos(paragraph, locInfo, elementPosition)

    if (true === isAddToSelect) {
      paragraph.selection.isUse = true
      updateSelectionStateForParagraph(paragraph, elementPosition, locInfo.pos)
    } else {
      paragraph.updateCursorPosByPosition(locInfo.pos, false, locInfo.line)
    }
  }

  if (true === paragraph.selection.isUse) {
    const position = paragraph.currentElementPosition(true, false)
    paragraph.updateCursorPosByPosition(position, false, -1)
  }

  calculateCursorPositionForParagraph(paragraph, true, false, false)

  paragraph.cursorPosition.cursorX = paragraph.cursorPosition.x
  paragraph.cursorPosition.cursorY = paragraph.cursorPosition.y
}
export function moveCursorToStartOfLineInTextDocument(
  textDoc: TextDocument,
  isAddToSelect: boolean
) {
  if (textDoc.cursorPosition.childIndex < 0) return false

  if (true === textDoc.selection.isUse) {
    if (true === isAddToSelect) {
      let para = textDoc.children.at(textDoc.selection.endIndex)
      para && moveCursorToStartOfLineInParagraph(para, isAddToSelect)

      para = textDoc.children.at(textDoc.selection.startIndex)
      if (
        textDoc.selection.startIndex === textDoc.selection.endIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        textDoc.selection.isUse = false
        textDoc.cursorPosition.childIndex = textDoc.selection.endIndex
      }
    } else {
      const pos =
        textDoc.selection.startIndex <= textDoc.selection.endIndex
          ? textDoc.selection.startIndex
          : textDoc.selection.endIndex
      textDoc.cursorPosition.childIndex = pos

      const para = textDoc.children.at(pos)
      para && moveCursorToStartOfLineInParagraph(para, isAddToSelect)

      textDoc.removeSelection()
    }
  } else {
    if (true === isAddToSelect) {
      textDoc.selection.isUse = true
      textDoc.selection.startIndex = textDoc.cursorPosition.childIndex
      textDoc.selection.endIndex = textDoc.cursorPosition.childIndex

      let para = textDoc.children.at(textDoc.cursorPosition.childIndex)
      para && moveCursorToStartOfLineInParagraph(para, isAddToSelect)

      para = textDoc.children.at(textDoc.selection.startIndex)
      if (
        textDoc.selection.startIndex === textDoc.selection.endIndex &&
        (para == null || false === para.hasTextSelection())
      ) {
        textDoc.selection.isUse = false
        textDoc.cursorPosition.childIndex = textDoc.selection.endIndex
      }
    } else {
      const para = textDoc.children.at(textDoc.cursorPosition.childIndex)
      para && moveCursorToStartOfLineInParagraph(para, isAddToSelect)
    }
  }
}
export function moveCursorToStartOfLineInTable(
  table: Table,
  isAddToSelect: boolean
) {
  if (
    true === table.selection.isUse &&
    TableSelectionType.CELL === table.selection.type
  ) {
    return moveCursorLeftInTable(table, isAddToSelect, false)
  } else {
    const result = moveCursorToStartOfLineInTextDocument(
      table.curCell!.textDoc,
      isAddToSelect
    )
    if (true === table.curCell!.textDoc.hasTextSelection()) {
      table.selection.isUse = true
      table.selection.type = TableSelectionType.TEXT
      table.selection.selectionStartInfo.cellPos = {
        cell: table.curCell!.index,
        row: table.curCell!.parent.index,
      }
      table.selection.selectionEndInfo.cellPos = {
        cell: table.curCell!.index,
        row: table.curCell!.parent.index,
      }
    } else {
      table.selection.isUse = false
    }

    return result
  }
}
