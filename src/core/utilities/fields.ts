import { Nullable } from '../../../liber/pervasive'
import { InstanceType } from '../instanceTypes'
import { SlideElement } from '../SlideElement/type'
import { TextDocument } from '../TextDocument/TextDocument'
import { Slide } from '../Slide/Slide'
import { SlideLayout } from '../Slide/SlideLayout'
import { SlideMaster } from '../Slide/SlideMaster'
import { getTextDocumentOfSp } from './shape/getters'
import { updateCalcStatusForTextContentItem } from '../calculation/updateCalcStatus/textContent'
import { PresentationField } from '../Paragraph/ParaContent/PresentationField'

export function updatePresentationFields(
  container: Slide | SlideLayout | SlideMaster
) {
  const spTree = container.cSld.spTree
  for (let i = 0, l = spTree.length; i < l; i++) {
    const subSp = spTree[i]
    updateFieldsForSlideElement(subSp)
  }
  if (container.instanceType === InstanceType.Slide && container.shapeOfNotes) {
    updateFieldsForSlideElement(container.shapeOfNotes)
  }
}

export function updateFieldsForSlideElement(sp: SlideElement) {
  switch (sp.instanceType) {
    case InstanceType.Shape: {
      const textDoc = getTextDocumentOfSp(sp)
      if (textDoc) {
        const presentationFields = textDoc.getPresentationFields()
        _onUpdateFields(textDoc, presentationFields)
      }
      break
    }
    case InstanceType.GroupShape: {
      for (let i = 0, l = sp.spTree.length; i < l; i++) {
        const subSp = sp.spTree[i]
        updateFieldsForSlideElement(subSp)
      }
    }
  }
}

function _onUpdateFields(
  textDoc: Nullable<TextDocument>,
  presentationFields: PresentationField[]
) {
  if (textDoc) {
    if (presentationFields.length > 0) {
      for (let i = 0; i < presentationFields.length; i++) {
        presentationFields[i].calculateInfo.reMeasure = true
      }
      updateCalcStatusForTextContentItem(textDoc)
    }
  }
}
