import { Dict, Nullable, valuesOfDict } from '../../../liber/pervasive'
import { ShdType, VertAlignJc } from '../common/const/attrs'

import {
  CellTextDirection,
  TableLengthUnitType,
  TableSelectionType,
  TableTextDirection,
  TableTextDirectionValues,
} from '../common/const/table'
import { copyDict, intersectObjects } from '../common/helpers'
import {
  BackgroundInTable,
  TableProp,
  TablePropOptions,
} from '../properties/tableProp'
import { CellBorderType, SetCellMarginType, TableLook } from '../Table/common'
import { Table } from '../Table/Table'
import { TableCell } from '../Table/TableCell'
import { CellPos } from '../Table/type'
import { getMergedCells } from '../Table/utils/utils'
import { TableCellBorder } from '../Table/attrs/CellBorder'
import { CShd, CShdOptions } from '../Table/attrs/CShd'
import { TableCellMarginsOptions } from '../Table/attrs/tableMargins'
export function getTableProps(table: Table): TablePropOptions {
  const tablePr = table.getCalcedPr(false).tablePr

  const pr: Partial<TablePropOptions> = {}
  pr.defaultMarginsOfTable = {
    left: tablePr.tableCellMargins.left!.w,
    right: tablePr.tableCellMargins.right!.w,
    top: tablePr.tableCellMargins.top!.w,
    bottom: tablePr.tableCellMargins.bottom!.w,
  }

  if (
    true === table.selection.isUse &&
    TableSelectionType.CELL === table.selection.type
  ) {
    pr.isSelectCell = true

    let cellMargins: Nullable<TableCellMarginsOptions>
    let hasCellMargins = false

    let tcBorderValue_left: Nullable<TableCellBorder>
    let tcBorderValue_right: Nullable<TableCellBorder>
    let tcBorderValue_top: Nullable<TableCellBorder>
    let tcBorderValue_bottom: Nullable<TableCellBorder>
    let tcBorderValue_insideH: Nullable<TableCellBorder>
    let tcBorderValue_insideV: Nullable<TableCellBorder>

    let cellShd: Nullable<CShd>

    let prev_row = -1
    let isFirstRow = true

    let vAlign: Nullable<valuesOfDict<typeof VertAlignJc>>
    let textDirection: Nullable<TableTextDirectionValues>

    let valueOfRowHeight: Nullable<number>

    for (let i = 0; i < table.selection.cellPositions.length; i++) {
      const cellPos = table.selection.cellPositions[i]
      const row = table.children.at(cellPos.row)
      if (row == null) continue
      const cell = row.getCellByIndex(cellPos.cell)
      if (cell == null) continue
      const cellBorders = cell.getBorders()
      const cell_margins = cell.getMargins()
      const cell_shd = cell.getShd()!

      if (0 === i) {
        vAlign = cell.getVAlign()
        textDirection = cell.getTextDirection()
      } else {
        if (vAlign !== cell.getVAlign()) vAlign = null

        if (textDirection !== cell.getTextDirection()) textDirection = null
      }

      if (0 === i) {
        cellShd = cell_shd
      } else {
        if (
          null != cellShd &&
          (cellShd.value !== cell_shd.value ||
            cellShd.color!.r !== cell_shd.color!.r ||
            cellShd.color!.g !== cell_shd.color!.g ||
            cellShd.color!.b !== cell_shd.color!.b)
        ) {
          cellShd = null
        }
      }

      if (0 === i || table.selection.cellPositions[i - 1].row !== cellPos.row) {
        if (null == tcBorderValue_left) tcBorderValue_left = cellBorders.left
        else {
          tcBorderValue_left = table._intersectionOfCellBorders(
            tcBorderValue_left,
            cellBorders.left
          )
        }
      } else {
        if (null == tcBorderValue_insideV) {
          tcBorderValue_insideV = cellBorders.left
        } else {
          tcBorderValue_insideV = table._intersectionOfCellBorders(
            tcBorderValue_insideV,
            cellBorders.left
          )
        }
      }

      if (
        table.selection.cellPositions.length - 1 === i ||
        table.selection.cellPositions[i + 1].row !== cellPos.row
      ) {
        if (null == tcBorderValue_right) {
          tcBorderValue_right = cellBorders.right
        } else {
          tcBorderValue_right = table._intersectionOfCellBorders(
            tcBorderValue_right,
            cellBorders.right
          )
        }
      } else {
        if (null == tcBorderValue_insideV) {
          tcBorderValue_insideV = cellBorders.right
        } else {
          tcBorderValue_insideV = table._intersectionOfCellBorders(
            tcBorderValue_insideV,
            cellBorders.right
          )
        }
      }

      if (prev_row !== cellPos.row) {
        if (-1 !== prev_row) isFirstRow = false

        if (false === isFirstRow) {
          if (null == tcBorderValue_insideH) {
            tcBorderValue_insideH = tcBorderValue_bottom
            tcBorderValue_insideH = table._intersectionOfCellBorders(
              tcBorderValue_insideH!,
              cellBorders.top
            )
          } else {
            tcBorderValue_insideH = table._intersectionOfCellBorders(
              tcBorderValue_insideH,
              tcBorderValue_bottom!
            )
            tcBorderValue_insideH = table._intersectionOfCellBorders(
              tcBorderValue_insideH,
              cellBorders.top
            )
          }
        } else {
          if (null == tcBorderValue_top) tcBorderValue_top = cellBorders.top
        }

        tcBorderValue_bottom = cellBorders.bottom
        prev_row = cellPos.row
      } else {
        if (false === isFirstRow) {
          if (null == tcBorderValue_insideH) {
            tcBorderValue_insideH = cellBorders.top
          } else {
            tcBorderValue_insideH = table._intersectionOfCellBorders(
              tcBorderValue_insideH,
              cellBorders.top
            )
          }
        } else {
          if (null == tcBorderValue_top) tcBorderValue_top = cellBorders.top
          else {
            tcBorderValue_top = table._intersectionOfCellBorders(
              tcBorderValue_top,
              cellBorders.top
            )
          }
        }

        tcBorderValue_bottom = table._intersectionOfCellBorders(
          tcBorderValue_bottom!,
          cellBorders.bottom
        )
      }

      if (true !== cell.hasTableMargins()) {
        if (null == cellMargins) {
          cellMargins = copyDict(cell_margins)
        } else {
          if (cellMargins.left!.w !== cell_margins.left!.w) {
            cellMargins.left = undefined
          }

          if (cellMargins.right!.w !== cell_margins.right!.w) {
            cellMargins.right = undefined
          }

          if (cellMargins.top!.w !== cell_margins.top!.w) {
            cellMargins.top = undefined
          }

          if (cellMargins.bottom!.w !== cell_margins.bottom!.w) {
            cellMargins.bottom = undefined
          }
        }
      } else {
        hasCellMargins = true
      }

      let valueOfCurRowHeight
      const rowHeight = row.getHeight()
      if (rowHeight.isAuto()) {
        const valueOfCurRow = row.getIndex()

        let valueOfRowSummaryH = 0

        if (table.rowsInfo[valueOfCurRow]) {
          valueOfRowSummaryH += table.rowsInfo[valueOfCurRow].h

          if (table.rowsInfo[valueOfCurRow].diffYOfTop) {
            valueOfRowSummaryH -= table.rowsInfo[valueOfCurRow].diffYOfTop
          }

          valueOfRowSummaryH -= row.getTopMargin() + row.getBottomMargin()
        }

        valueOfCurRowHeight = valueOfRowSummaryH
      } else {
        valueOfCurRowHeight = rowHeight.getValue()
      }

      if (null == valueOfRowHeight) valueOfRowHeight = valueOfCurRowHeight
      else if (
        undefined !== valueOfRowHeight &&
        Math.abs(valueOfRowHeight - valueOfCurRowHeight) > 0.001
      ) {
        valueOfRowHeight = undefined
      }
    }

    pr.cellsVertAlign = vAlign
    pr.textDirectionInCell = textDirection
    pr.rowHeight = valueOfRowHeight

    pr.cellBorders = {
      left: tcBorderValue_left ? tcBorderValue_left.clone() : undefined,
      right: tcBorderValue_right ? tcBorderValue_right.clone() : undefined,
      top: tcBorderValue_top ? tcBorderValue_top.clone() : undefined,
      bottom: tcBorderValue_bottom ? tcBorderValue_bottom.clone() : undefined,
      insideH:
        null == tcBorderValue_insideH
          ? undefined
          : tcBorderValue_insideH.clone(),
      insideV:
        null == tcBorderValue_insideV
          ? undefined
          : tcBorderValue_insideV.clone(),
    }

    if (null == cellShd) pr.cellsBackground = null
    else pr.cellsBackground = BackgroundInTable.fromShd(cellShd)

    if (null == cellMargins) {
      pr.cellMargins = {
        flag: 0,
      }
    } else {
      let flag: 0 | 1 | 2 = 2
      if (true === hasCellMargins) flag = 1

      pr.cellMargins = {
        left: cellMargins.left!.w,
        right: cellMargins.right!.w,
        top: cellMargins.top!.w,
        bottom: cellMargins.bottom!.w,
        flag: flag,
      }
    }
  } else {
    pr.isSelectCell = false

    const cell = table.curCell!
    const cellMargins = cell.getMargins()
    const cellShd = cell.getShd()!

    if (true === cell.hasTableMargins()) {
      pr.cellMargins = {
        flag: 0,
      }
    } else {
      pr.cellMargins = {
        left: cellMargins.left!.w,
        right: cellMargins.right!.w,
        top: cellMargins.top!.w,
        bottom: cellMargins.bottom!.w,
        flag: 2,
      }
    }

    pr.cellsVertAlign = cell.getVAlign()
    pr.textDirectionInCell = cell.getTextDirection()

    pr.cellsBackground = BackgroundInTable.fromShd(cellShd)

    const cellBorders = cell.getBorders()

    pr.cellBorders = {
      left: cellBorders.left.clone(),
      right: cellBorders.right.clone(),
      top: cellBorders.top.clone(),
      bottom: cellBorders.bottom.clone(),
      insideH: undefined,
      insideV: undefined,
    }

    const rowHeight = table.curCell!.parent.getHeight()
    if (rowHeight.isAuto()) {
      const row = table.curCell!.getRow()
      const valueOfCurRow = row.getIndex()

      let valueOfRowSummaryH = 0

      if (table.rowsInfo[valueOfCurRow]) {
        valueOfRowSummaryH += table.rowsInfo[valueOfCurRow].h

        if (table.rowsInfo[valueOfCurRow].diffYOfTop) {
          valueOfRowSummaryH -= table.rowsInfo[valueOfCurRow].diffYOfTop
        }

        valueOfRowSummaryH -= row.getTopMargin() + row.getBottomMargin()
      }

      pr.rowHeight = valueOfRowSummaryH
    } else {
      pr.rowHeight = rowHeight.getValue()
    }
  }

  const selectedCellPositions = table.getSelectedCellPositions()

  const cells: Dict = {}
  for (
    let valueOfIndex = 0, valueOfCount = selectedCellPositions.length;
    valueOfIndex < valueOfCount;
    ++valueOfIndex
  ) {
    const valueOfCurCell = selectedCellPositions[valueOfIndex].cell
    if (!cells[valueOfCurCell]) cells[valueOfCurCell] = 1
  }

  switch (pr.cellsVertAlign) {
    case VertAlignJc.Top:
      pr.cellsVertAlign = VertAlignJc.Top
      break
    case VertAlignJc.Bottom:
      pr.cellsVertAlign = VertAlignJc.Bottom
      break
    case VertAlignJc.Center:
      pr.cellsVertAlign = VertAlignJc.Center
      break
    default:
      pr.cellsVertAlign = null
      break
  }

  switch (pr.textDirectionInCell) {
    case TableTextDirection.LRTB:
      pr.textDirectionInCell = CellTextDirection.LRTB
      break
    case TableTextDirection.TBRL:
      pr.textDirectionInCell = CellTextDirection.TBRL
      break
    case TableTextDirection.BTLR:
      pr.textDirectionInCell = CellTextDirection.BTLR
      break
    default:
      pr.textDirectionInCell = null
      break
  }

  // default props
  {
    pr.position = {
      x: table.parent.x,
      y: table.parent.y,
    }

    pr.tablePaddings = {
      left: 0,
      right: 0,
      top: 0,
      bottom: 0,
    }
  }

  pr.tableBorders = tablePr.tableBorders

  pr.tableBackground = BackgroundInTable.fromShd(tablePr.shd!)

  pr.styleId = table.tableStyleId
  pr.tableLook = table.tableLook

  return pr as TablePropOptions
}

export function setPropForTable(table: Table, props: TableProp) {
  const tablePr = table.getCalcedPr(false).tablePr

  let needChange

  if (null != props.styleId) {
    table.setTableStyleId(props.styleId)
  }

  // TableLook
  if (props.tableLook != null) {
    const tableLook = new TableLook(
      props.tableLook.firstCol,
      props.tableLook.firstRow,
      props.tableLook.lastCol,
      props.tableLook.lastRow,
      props.tableLook.bandRow,
      props.tableLook.bandCol,
      props.tableLook.rtl
    )
    table.setTableLook(tableLook)
  }

  if (props.rtl !== undefined) {
    table.tableLook.rtl = props.rtl
    table.setTableLook(table.tableLook)
  }

  if (null != props.rowHeight) table.setRowHeight(props.rowHeight)

  if (table.isEmpty()) return

  if (props.defaultMarginsOfTable != null) {
    needChange = false

    const tableDefaultMargins = props.defaultMarginsOfTable
    const leftMargin =
      null != tableDefaultMargins.left
        ? tableDefaultMargins.left
        : tablePr.tableCellMargins.left!.w
    const rightMargin =
      null != tableDefaultMargins.right
        ? tableDefaultMargins.right
        : tablePr.tableCellMargins.right!.w
    const topMargin =
      null != tableDefaultMargins.top
        ? tableDefaultMargins.top
        : tablePr.tableCellMargins.top!.w
    const bottomMargin =
      null != tableDefaultMargins.bottom
        ? tableDefaultMargins.bottom
        : tablePr.tableCellMargins.bottom!.w

    if (
      leftMargin !== tablePr.tableCellMargins.left!.w ||
      rightMargin !== tablePr.tableCellMargins.right!.w ||
      topMargin !== tablePr.tableCellMargins.top!.w ||
      bottomMargin !== tablePr.tableCellMargins.bottom!.w
    ) {
      needChange = true
    }

    if (true === needChange) {
      table.setTableCellMargin(leftMargin, topMargin, rightMargin, bottomMargin)
    }
  }

  if (props.cellMargins != null) {
    needChange = false

    switch (props.cellMargins.flag) {
      case 0: {
        if (
          true === table.selection.isUse &&
          TableSelectionType.CELL === table.selection.type
        ) {
          for (let i = 0; i < table.selection.cellPositions.length; i++) {
            const cellPos = table.selection.cellPositions[i]
            const cell = table.children
              .at(cellPos.row)
              ?.getCellByIndex(cellPos.cell)

            if (cell && null != cell.pr.margins) {
              cell.setMargins(null)
              needChange = true
            }
          }
        } else {
          const cell = table.curCell

          if (cell && null != cell.pr.margins) {
            cell.setMargins(null)
            needChange = true
          }
        }

        break
      }
      case 1: {
        if (
          true === table.selection.isUse &&
          TableSelectionType.CELL === table.selection.type
        ) {
          for (let i = 0; i < table.selection.cellPositions.length; i++) {
            const cellPos = table.selection.cellPositions[i]
            const cell = table.children
              .at(cellPos.row)
              ?.getCellByIndex(cellPos.cell)

            if (cell && true !== cell.hasTableMargins()) {
              if (null != props.cellMargins.left) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.left,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Left,
                })
              }

              if (null != props.cellMargins.right) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.right,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Right,
                })
              }

              if (null != props.cellMargins.top) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.top,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Top,
                })
              }

              if (null != props.cellMargins.bottom) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.bottom,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Bootom,
                })
              }

              needChange = true
            }
          }
        } else {
          const cell = table.curCell
          if (cell) {
            if (true !== cell.hasTableMargins()) {
              if (null != props.cellMargins.left) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.left,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Left,
                })
              }

              if (null != props.cellMargins.right) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.right,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Right,
                })
              }

              if (null != props.cellMargins.top) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.top,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Top,
                })
              }

              if (null != props.cellMargins.bottom) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.bottom,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Bootom,
                })
              }
            } else {
              if (null != props.cellMargins.left) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.left,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Left,
                })
              } else {
                cell.setMargins({
                  margin: {
                    w: tablePr.tableCellMargins.left!.w,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Left,
                })
              }

              if (null != props.cellMargins.right) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.right,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Right,
                })
              } else {
                cell.setMargins({
                  margin: {
                    w: tablePr.tableCellMargins.right!.w,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Right,
                })
              }

              if (null != props.cellMargins.top) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.top,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Top,
                })
              } else {
                cell.setMargins({
                  margin: {
                    w: tablePr.tableCellMargins.top!.w,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Top,
                })
              }

              if (null != props.cellMargins.bottom) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.bottom,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Bootom,
                })
              } else {
                cell.setMargins({
                  margin: {
                    w: tablePr.tableCellMargins.bottom!.w,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Bootom,
                })
              }
            }
          }
          needChange = true
        }

        break
      }
      case 2: {
        needChange = true

        if (
          true === table.selection.isUse &&
          TableSelectionType.CELL === table.selection.type
        ) {
          for (let i = 0; i < table.selection.cellPositions.length; i++) {
            const cellPos = table.selection.cellPositions[i]
            const cell = table.children
              .at(cellPos.row)
              ?.getCellByIndex(cellPos.cell)
            if (cell) {
              if (true !== cell.hasTableMargins()) {
                if (null != props.cellMargins.left) {
                  cell.setMargins({
                    margin: {
                      w: props.cellMargins.left,
                      type: TableLengthUnitType.MM,
                    },
                    type: SetCellMarginType.Left,
                  })
                }

                if (null != props.cellMargins.right) {
                  cell.setMargins({
                    margin: {
                      w: props.cellMargins.right,
                      type: TableLengthUnitType.MM,
                    },
                    type: SetCellMarginType.Right,
                  })
                }

                if (null != props.cellMargins.top) {
                  cell.setMargins({
                    margin: {
                      w: props.cellMargins.top,
                      type: TableLengthUnitType.MM,
                    },
                    type: SetCellMarginType.Top,
                  })
                }

                if (null != props.cellMargins.bottom) {
                  cell.setMargins({
                    margin: {
                      w: props.cellMargins.bottom,
                      type: TableLengthUnitType.MM,
                    },
                    type: SetCellMarginType.Bootom,
                  })
                }
              } else {
                if (null != props.cellMargins.left) {
                  cell.setMargins({
                    margin: {
                      w: props.cellMargins.left,
                      type: TableLengthUnitType.MM,
                    },
                    type: SetCellMarginType.Left,
                  })
                } else {
                  cell.setMargins({
                    margin: {
                      w: tablePr.tableCellMargins.left!.w,
                      type: TableLengthUnitType.MM,
                    },
                    type: SetCellMarginType.Left,
                  })
                }

                if (null != props.cellMargins.right) {
                  cell.setMargins({
                    margin: {
                      w: props.cellMargins.right,
                      type: TableLengthUnitType.MM,
                    },
                    type: SetCellMarginType.Right,
                  })
                } else {
                  cell.setMargins({
                    margin: {
                      w: tablePr.tableCellMargins.right!.w,
                      type: TableLengthUnitType.MM,
                    },
                    type: SetCellMarginType.Right,
                  })
                }

                if (null != props.cellMargins.top) {
                  cell.setMargins({
                    margin: {
                      w: props.cellMargins.top,
                      type: TableLengthUnitType.MM,
                    },
                    type: SetCellMarginType.Top,
                  })
                } else {
                  cell.setMargins({
                    margin: {
                      w: tablePr.tableCellMargins.top!.w,
                      type: TableLengthUnitType.MM,
                    },
                    type: SetCellMarginType.Top,
                  })
                }

                if (null != props.cellMargins.bottom) {
                  cell.setMargins({
                    margin: {
                      w: props.cellMargins.bottom,
                      type: TableLengthUnitType.MM,
                    },
                    type: SetCellMarginType.Bootom,
                  })
                } else {
                  cell.setMargins({
                    margin: {
                      w: tablePr.tableCellMargins.bottom!.w,
                      type: TableLengthUnitType.MM,
                    },
                    type: SetCellMarginType.Bootom,
                  })
                }
              }
            }
          }
        } else {
          const cell = table.curCell
          if (cell) {
            if (true !== cell.hasTableMargins()) {
              if (null != props.cellMargins.left) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.left,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Left,
                })
              }

              if (null != props.cellMargins.right) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.right,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Right,
                })
              }

              if (null != props.cellMargins.top) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.top,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Top,
                })
              }

              if (null != props.cellMargins.bottom) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.bottom,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Bootom,
                })
              }
            } else {
              if (null != props.cellMargins.left) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.left,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Left,
                })
              } else {
                cell.setMargins({
                  margin: {
                    w: tablePr.tableCellMargins.left!.w,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Left,
                })
              }

              if (null != props.cellMargins.right) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.right,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Right,
                })
              } else {
                cell.setMargins({
                  margin: {
                    w: tablePr.tableCellMargins.right!.w,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Right,
                })
              }

              if (null != props.cellMargins.top) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.top,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Top,
                })
              } else {
                cell.setMargins({
                  margin: {
                    w: tablePr.tableCellMargins.top!.w,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Top,
                })
              }

              if (null != props.cellMargins.bottom) {
                cell.setMargins({
                  margin: {
                    w: props.cellMargins.bottom,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Bootom,
                })
              } else {
                cell.setMargins({
                  margin: {
                    w: tablePr.tableCellMargins.bottom!.w,
                    type: TableLengthUnitType.MM,
                  },
                  type: SetCellMarginType.Bootom,
                })
              }
            }
          }
          needChange = true
        }

        break
      }
    }
  }

  // tableBorders
  if (null != props.tableBorders) {
    if (
      false === _isEmptyBorder(props.tableBorders.top) &&
      false ===
        _isTableBordersEqual(props.tableBorders.top, tablePr.tableBorders.top)
    ) {
      table.setTopTableBorder(props.tableBorders.top)

      const row = table.children.at(0)!
      for (let ci = 0, l = row.cellsCount; ci < l; ci++) {
        const cell = row.getCellByIndex(ci)!
        cell.setBorder(null, CellBorderType.Top)
      }
    }

    if (
      false === _isEmptyBorder(props.tableBorders.bottom) &&
      false ===
        _isTableBordersEqual(
          props.tableBorders.bottom,
          tablePr.tableBorders.bottom
        )
    ) {
      table.setBottomTableBorder(props.tableBorders.bottom)
      const row = table.children.at(table.children.length - 1)!
      for (let ci = 0, l = row.cellsCount; ci < l; ci++) {
        const cell = row.getCellByIndex(ci)!
        cell.setBorder(null, CellBorderType.Bottom)
      }
    }

    if (
      false === _isEmptyBorder(props.tableBorders.left) &&
      false ===
        _isTableBordersEqual(props.tableBorders.left, tablePr.tableBorders.left)
    ) {
      table.setLeftTableBorder(props.tableBorders.left)

      for (let ri = 0, l = table.children.length; ri < l; ri++) {
        const cell = table.children.at(ri)!.getCellByIndex(0)
        cell && cell.setBorder(null, CellBorderType.Left)
      }
    }

    if (
      false === _isEmptyBorder(props.tableBorders.right) &&
      false ===
        _isTableBordersEqual(
          props.tableBorders.right,
          tablePr.tableBorders.right
        )
    ) {
      table.setRightTableBorder(props.tableBorders.right)

      for (let ri = 0, l = table.children.length; ri < l; ri++) {
        const row = table.children.at(ri)!
        const cell = row.getCellByIndex(row.cellsCount - 1)
        cell && cell.setBorder(null, CellBorderType.Right)
      }
    }

    if (
      false === _isEmptyBorder(props.tableBorders.insideH) &&
      false ===
        _isTableBordersEqual(
          props.tableBorders.insideH,
          tablePr.tableBorders.insideH
        )
    ) {
      table.setInsideHTableBorder(props.tableBorders.insideH)

      for (let ri = 0, l = table.children.length; ri < l; ri++) {
        const row = table.children.at(ri)!
        const cellsCount = row.cellsCount

        for (let ci = 0; ci < cellsCount; ci++) {
          const cell = row.getCellByIndex(ci)!

          if (0 !== ri) {
            cell.setBorder(null, CellBorderType.Top)
          }

          if (table.children.length - 1 !== ri) {
            cell.setBorder(null, CellBorderType.Bottom)
          }
        }
      }
    }

    if (
      false === _isEmptyBorder(props.tableBorders.insideV) &&
      false ===
        _isTableBordersEqual(
          props.tableBorders.insideV,
          tablePr.tableBorders.insideV
        )
    ) {
      table.setInsideVTableBorder(props.tableBorders.insideV)

      for (let ri = 0, l = table.children.length; ri < l; ri++) {
        const row = table.children.at(ri)!
        const cellsCount = row.cellsCount

        for (let ci = 0; ci < cellsCount; ci++) {
          const cell = row.getCellByIndex(ci)!

          if (0 !== ci) {
            cell.setBorder(null, CellBorderType.Left)
          }

          if (cellsCount - 1 !== ci) {
            cell.setBorder(null, CellBorderType.Right)
          }
        }
      }
    }
  }

  _setCellBorders(table, props)

  if (props.tableBackground != null) {
    if (
      props.tableBackground.value !== tablePr.shd!.value ||
      props.tableBackground.color!.r !== tablePr.shd!.color!.r ||
      props.tableBackground.color!.g !== tablePr.shd!.color!.g ||
      props.tableBackground.color!.b !== tablePr.shd!.color!.b
    ) {
      table.setShd(
        props.tableBackground.value!,
        props.tableBackground.color!.r,
        props.tableBackground.color!.g,
        props.tableBackground.color!.b
      )
      // bRedraw = true
    }

    if (false === props.isSelectCell) {
      for (let ri = 0, l = table.children.length; ri < l; ri++) {
        const row = table.children.at(ri)!
        for (let ci = 0; ci < row.cellsCount; ci++) {
          const cell = row.getCellByIndex(ci)!
          cell.setShd({
            value: ShdType.Nil,
            color: { r: 0, g: 0, b: 0 },
          })
        }
      }
    }
  }

  if (null != props.cellsBackground) {
    if (false === props.isSelectCell) {
      for (let ri = 0, l = table.children.length; ri < l; ri++) {
        const row = table.children.at(ri)!
        for (let ci = 0; ci < row.cellsCount; ci++) {
          const cell = row.getCellByIndex(ci)!
          const newShd: CShdOptions = {
            value: props.cellsBackground.value!,
            fillEffects: props.cellsBackground.fillEffects?.clone(),
          }

          if (props.cellsBackground.color) {
            newShd.color = {
              r: props.cellsBackground.color.r,
              g: props.cellsBackground.color.g,
              b: props.cellsBackground.color.b,
            }
          }
          cell.setShd(newShd)

          // bRedraw = true
        }
      }
    } else if (
      true === table.selection.isUse &&
      TableSelectionType.CELL === table.selection.type
    ) {
      for (let i = 0; i < table.selection.cellPositions.length; i++) {
        const cellPos = table.selection.cellPositions[i]
        const cell = table.children
          .at(cellPos.row)!
          .getCellByIndex(cellPos.cell)!
        const cellShd = cell.getShd()

        if (
          props.cellsBackground.value !== cellShd.value ||
          (props.cellsBackground.color &&
            (props.cellsBackground.color!.r !== cellShd.color!.r ||
              props.cellsBackground.color!.g !== cellShd.color!.g ||
              props.cellsBackground.color!.b !== cellShd.color!.b)) ||
          (props.cellsBackground.fillEffects &&
            intersectObjects(
              props.cellsBackground.fillEffects,
              cellShd.fillEffects
            ))
        ) {
          const newShd: CShdOptions = {
            value: props.cellsBackground.value!,
            fillEffects: props.cellsBackground.fillEffects?.clone(),
          }

          if (props.cellsBackground.color) {
            newShd.color = {
              r: props.cellsBackground.color.r,
              g: props.cellsBackground.color.g,
              b: props.cellsBackground.color.b,
            }
          }

          cell.setShd(newShd)
        }
      }
    } else {
      const cell = table.curCell!
      const cellShd = cell.getShd()

      if (
        props.cellsBackground.value !== cellShd.value ||
        (props.cellsBackground.color &&
          (props.cellsBackground.color!.r !== cellShd.color!.r ||
            props.cellsBackground.color!.g !== cellShd.color!.g ||
            props.cellsBackground.color!.b !== cellShd.color!.b)) ||
        (props.cellsBackground.fillEffects &&
          intersectObjects(
            props.cellsBackground.fillEffects,
            cellShd.fillEffects
          ))
      ) {
        const newShd: CShdOptions = {
          value: props.cellsBackground.value!,
          fillEffects: props.cellsBackground.fillEffects?.clone(),
        }
        if (props.cellsBackground.color) {
          newShd.color = {
            r: props.cellsBackground.color.r,
            g: props.cellsBackground.color.g,
            b: props.cellsBackground.color.b,
          }
        }
        cell.setShd(newShd)
      }
    }
  }

  if (null != props.cellsVertAlign && null != props.cellsVertAlign) {
    if (
      table.selection.isUse === true &&
      TableSelectionType.CELL === table.selection.type
    ) {
      const l = table.selection.cellPositions.length
      for (let i = 0; i < l; i++) {
        const cellPos = table.selection.cellPositions[i]
        const cell = table.children
          .at(cellPos.row)!
          .getCellByIndex(cellPos.cell)!
        cell.setVAlign(props.cellsVertAlign)
      }
    } else {
      table.curCell!.setVAlign(props.cellsVertAlign)
    }
  }

  // TextDirection
  if (null != props.textDirectionInCell) {
    let textDirection
    switch (props.textDirectionInCell) {
      case CellTextDirection.LRTB:
        textDirection = TableTextDirection.LRTB
        break
      case CellTextDirection.TBRL:
        textDirection = TableTextDirection.TBRL
        break
      case CellTextDirection.BTLR:
        textDirection = TableTextDirection.BTLR
        break
    }

    if (null != textDirection) {
      if (
        table.selection.isUse === true &&
        TableSelectionType.CELL === table.selection.type
      ) {
        const l = table.selection.cellPositions.length
        for (let i = 0; i < l; ++i) {
          const cellPos = table.selection.cellPositions[i]
          const cell = table.children
            .at(cellPos.row)!
            .getCellByIndex(cellPos.cell)!
          cell.setTextDirection(textDirection)
        }
      } else {
        table.curCell!.setTextDirection(textDirection)
      }
    }
  }

  return true
}

function _getGridEndOfCellPos(table: Table, cellPos: CellPos) {
  const row = table.children.at(cellPos.row)!
  const cell = row.getCellByIndex(cellPos.cell)!

  return row.getCellInfo(cellPos.cell).startGridCol + cell.getGridSpan()! - 1
}

function _setCellBorders(table: Table, props: TableProp) {
  // CellBorders
  if (null != props.cellBorders) {
    let cellPositions: Nullable<CellPos[]>

    if (
      true === table.selection.isUse &&
      TableSelectionType.CELL === table.selection.type
    ) {
      cellPositions = []
      for (let i = 0, l = table.selection.cellPositions.length; i < l; i++) {
        const rowIndex = table.selection.cellPositions[i].row
        const cellIndex = table.selection.cellPositions[i].cell

        const row = table.children.at(rowIndex)!
        const startGridCol = row.getCellInfo(cellIndex).startGridCol
        const gridSpan = row.getCellByIndex(cellIndex)!.getGridSpan()
        const _cellPositions = _cellPositionsFromCells(
          getMergedCells(table, rowIndex, startGridCol, gridSpan)
        )
        cellPositions = cellPositions.concat(_cellPositions)
      }
    } else if (table.curCell) {
      const curCell = table.curCell
      const curRow = curCell.parent
      const rowIndex = curRow.index
      const cellIndex = curCell.index
      const startGridCol = curRow.getCellInfo(cellIndex).startGridCol
      const gridSpan = curCell.getGridSpan()
      cellPositions = _cellPositionsFromCells(
        getMergedCells(table, rowIndex, startGridCol, gridSpan)
      )
    }

    if (cellPositions == null || cellPositions.length <= 0) {
      return
    }

    const hasTopBorder =
      false === _isEmptyBorder(props.cellBorders.top) ? true : false
    const hasBottomBorder =
      false === _isEmptyBorder(props.cellBorders.bottom) ? true : false
    const hasLeftBorder =
      false === _isEmptyBorder(props.cellBorders.left) ? true : false
    const hasRightBorder =
      false === _isEmptyBorder(props.cellBorders.right) ? true : false
    const hasInsideHBorder =
      false === _isEmptyBorder(props.cellBorders.insideH) ? true : false
    const hasInsideVBorder =
      false === _isEmptyBorder(props.cellBorders.insideV) ? true : false

    if (cellPositions.length > 1) {
      cellPositions = cellPositions.sort(
        (pos1, pos2) => pos1.row - pos2.row || pos1.cell - pos2.cell
      )
    }

    // first row grid info
    const firstRowStartCellPos = cellPositions[0]
    const firstRowIndex = firstRowStartCellPos.row

    const firstRow_gridStart = table.children
      .at(firstRowIndex)!
      .getCellInfo(firstRowStartCellPos.cell).startGridCol

    let firstRowEndCellPos = firstRowStartCellPos
    for (let i = 1, l = cellPositions.length; i < l; i++) {
      const cellPos = cellPositions[i]
      if (cellPos.row !== firstRowStartCellPos.row) {
        break
      } else {
        firstRowEndCellPos = cellPos
      }
    }

    const firstRow_gridEnd = _getGridEndOfCellPos(table, firstRowEndCellPos)

    // last row grid info
    const lastRowEndCellPos = cellPositions[cellPositions.length - 1]
    const lastRowIndex = lastRowEndCellPos.row

    const lastRow_gridEnd = _getGridEndOfCellPos(table, lastRowEndCellPos)

    let lastRowStartCellPos = lastRowEndCellPos
    for (let i = cellPositions.length - 2; i >= 0; i--) {
      const cellPos = cellPositions[i]
      if (cellPos.row !== lastRowEndCellPos.row) {
        break
      } else {
        lastRowStartCellPos = cellPos
      }
    }

    const lastRow_gridStart = table.children
      .at(lastRowStartCellPos.row)!
      .getCellInfo(lastRowStartCellPos.cell).startGridCol

    // setting top border while there is a row right above, should handle the bottom border of the row right above
    if (true === hasTopBorder && firstRowIndex > 0) {
      let startCellIndex = 0,
        endCellIndex = 0
      let hasStart = false
      let hasEnd = false

      const rowAbove = table.children.at(firstRowIndex - 1)!
      for (let i = 0, l = rowAbove.cellsCount; i < l; i++) {
        const startGridCol = rowAbove.getCellInfo(i).startGridCol
        const lastGridCol =
          startGridCol + rowAbove.getCellByIndex(i)!.getGridSpan()! - 1

        if (false === hasStart) {
          if (startGridCol < firstRow_gridStart) continue
          else {
            startCellIndex = i
            hasStart = true

            if (lastGridCol < firstRow_gridEnd) continue
            else {
              // (lastGridCol >= firstRow_gridEnd)
              endCellIndex = i
              hasEnd = true
              break
            }
          }
        }

        if (false === hasEnd) {
          if (lastGridCol < firstRow_gridEnd) continue
          else {
            // (lastGridCol >= firstRow_gridEnd)
            endCellIndex = i
            hasEnd = true
            break
          }
        }
      }

      if (true === hasStart && true === hasEnd) {
        for (let i = startCellIndex; i <= endCellIndex; i++) {
          const cell = rowAbove.getCellByIndex(i)!
          cell.setBorder(props.cellBorders.top, CellBorderType.Bottom)
        }
      }
    }

    // setting bottom border while there is a row right below, should handle the top border of the row right below
    if (lastRowIndex < table.children.length - 1 && true === hasBottomBorder) {
      let startCellIndex = 0,
        endCellIndex = 0
      let hasStart = false
      let hasEnd = false

      const rowBelow = table.children.at(lastRowIndex + 1)!
      for (let i = 0, l = rowBelow.cellsCount; i < l; i++) {
        const startGridCol = rowBelow.getCellInfo(i).startGridCol
        const lastGridCol =
          startGridCol + rowBelow.getCellByIndex(i)!.getGridSpan() - 1

        if (false === hasStart) {
          if (startGridCol < lastRow_gridStart) continue
          else {
            startCellIndex = i
            hasStart = true

            if (lastGridCol < lastRow_gridEnd) continue
            else {
              endCellIndex = i
              hasEnd = true
              break
            }
          }
        }

        if (false === hasEnd) {
          if (lastGridCol < lastRow_gridEnd) continue
          else {
            endCellIndex = i
            hasEnd = true
            break
          }
        }
      }

      if (true === hasStart && true === hasEnd) {
        for (let i = startCellIndex; i <= endCellIndex; i++) {
          const cell = rowBelow.getCellByIndex(i)!
          cell.setBorder(props.cellBorders.bottom, CellBorderType.Top)
        }
      }
    }

    let curRowIndex = firstRowIndex
    let startCellIndex = firstRowStartCellPos.cell,
      endCellIndex = firstRowStartCellPos.cell
    for (let i = 0; i < cellPositions.length; i++) {
      const cellPos = cellPositions[i]

      if (curRowIndex !== cellPos.row) {
        const row = table.children.at(curRowIndex)!

        if (startCellIndex > 0 && true === hasLeftBorder) {
          row
            .getCellByIndex(startCellIndex - 1)!
            .setBorder(props.cellBorders.left, CellBorderType.Right)
        }

        if (endCellIndex < row.cellsCount - 1 && true === hasRightBorder) {
          row
            .getCellByIndex(endCellIndex + 1)!
            .setBorder(props.cellBorders.right, CellBorderType.Left)
        }

        for (let ci = startCellIndex; ci <= endCellIndex; ci++) {
          const cell = row.getCellByIndex(ci)!

          if (firstRowIndex === curRowIndex && true === hasTopBorder) {
            cell.setBorder(props.cellBorders.top, CellBorderType.Top)
          } else if (
            firstRowIndex !== curRowIndex &&
            true === hasInsideHBorder
          ) {
            cell.setBorder(props.cellBorders.insideH, CellBorderType.Top)
          }

          if (lastRowIndex === curRowIndex && true === hasBottomBorder) {
            cell.setBorder(props.cellBorders.bottom, CellBorderType.Bottom)
          } else if (
            lastRowIndex !== curRowIndex &&
            true === hasInsideHBorder
          ) {
            cell.setBorder(props.cellBorders.insideH, CellBorderType.Bottom)
          }

          if (ci === startCellIndex && true === hasLeftBorder) {
            cell.setBorder(props.cellBorders.left, CellBorderType.Left)
          } else if (ci !== startCellIndex && true === hasInsideVBorder) {
            cell.setBorder(props.cellBorders.insideV, CellBorderType.Left)
          }

          if (ci === endCellIndex && true === hasRightBorder) {
            cell.setBorder(props.cellBorders.right, CellBorderType.Right)
          } else if (ci !== endCellIndex && true === hasInsideVBorder) {
            cell.setBorder(props.cellBorders.insideV, CellBorderType.Right)
          }
        }

        startCellIndex = cellPos.cell
        endCellIndex = cellPos.cell
        curRowIndex = cellPos.row
      } else endCellIndex = cellPos.cell

      if (cellPositions.length - 1 === i) {
        const row = table.children.at(curRowIndex)!
        if (startCellIndex > 0 && true === hasLeftBorder) {
          row
            .getCellByIndex(startCellIndex - 1)!
            .setBorder(props.cellBorders.left, CellBorderType.Right)
        }

        if (endCellIndex < row.cellsCount - 1 && true === hasRightBorder) {
          row
            .getCellByIndex(endCellIndex + 1)!
            .setBorder(props.cellBorders.right, CellBorderType.Left)
        }

        for (let ci = startCellIndex; ci <= endCellIndex; ci++) {
          const cell = row.getCellByIndex(ci)!

          if (firstRowIndex === cellPos.row && true === hasTopBorder) {
            cell.setBorder(props.cellBorders.top, CellBorderType.Top)
          } else if (
            firstRowIndex !== cellPos.row &&
            true === hasInsideHBorder
          ) {
            cell.setBorder(props.cellBorders.insideH, CellBorderType.Top)
          }

          if (lastRowIndex === cellPos.row && true === hasBottomBorder) {
            cell.setBorder(props.cellBorders.bottom, CellBorderType.Bottom)
          } else if (
            lastRowIndex !== cellPos.row &&
            true === hasInsideHBorder
          ) {
            cell.setBorder(props.cellBorders.insideH, CellBorderType.Bottom)
          }

          if (ci === startCellIndex && true === hasLeftBorder) {
            cell.setBorder(props.cellBorders.left, CellBorderType.Left)
          } else if (ci !== startCellIndex && true === hasInsideVBorder) {
            cell.setBorder(props.cellBorders.insideV, CellBorderType.Left)
          }

          if (ci === endCellIndex && true === hasRightBorder) {
            cell.setBorder(props.cellBorders.right, CellBorderType.Right)
          } else if (ci !== endCellIndex && true === hasInsideVBorder) {
            cell.setBorder(props.cellBorders.insideV, CellBorderType.Right)
          }
        }
      }
    }
  }
}

function _cellPositionsFromCells(cells: TableCell[]) {
  const ret: Array<CellPos> = []
  for (let i = 0, l = cells.length; i < l; i++) {
    const cell = cells[i]
    ret.push({ cell: cell.index, row: cell.parent.index })
  }

  return ret
}

function _isEmptyBorder(border: Nullable<TableCellBorder>) {
  if (null == border) return true

  if (null != border.value) return false

  if (null != border.size) return false

  if (
    (null != border.color &&
      (null != border.color.r ||
        null != border.color.g ||
        null != border.color.b)) ||
    border.fillEffects != null
  ) {
    return false
  }

  return true
}
function _isTableBordersEqual(
  border1: Nullable<TableCellBorder>,
  border2: Nullable<TableCellBorder>
) {
  if (border1 == null || border2 == null) {
    return true
  }

  if (border1.value !== border2.value) return false

  if (border1.size !== border2.size) return false

  if (
    border1.color?.r !== border2.color?.r ||
    border1.color?.g !== border2.color?.g ||
    border1.color?.b !== border2.color?.b
  ) {
    return false
  }

  return true
}
