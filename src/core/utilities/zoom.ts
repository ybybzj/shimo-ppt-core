import { BrowserInfo } from '../../common/browserInfo'
import { SlideDrawerCONST } from '../../ui/rendering/Drawer/const'
import { Factor_mm_to_pix } from '../common/const/unit'
import { Presentation } from '../Slide/Presentation'

export function zoomValueForFitToSlide(
  presentation: Presentation,
  containerDemension?: { width(): number; height(): number },
  ignoreBorder = false
) {
  let value = 100
  if (!presentation || !containerDemension) return value

  const w = containerDemension.width() / BrowserInfo.PixelRatio
  const h = containerDemension.height() / BrowserInfo.PixelRatio

  const slideWidth = presentation.width * Factor_mm_to_pix
  const slideHeight = presentation.height * Factor_mm_to_pix

  let horizentalZoom = 100
  const border = ignoreBorder === true ? 0 : 2 * SlideDrawerCONST.BORDER
  if (0 !== slideWidth) {
    horizentalZoom = (100 * (w - border)) / slideWidth
  }
  let verticalZoom = 100
  if (0 !== slideHeight) {
    verticalZoom = (100 * (h - border)) / slideHeight
  }

  value = (Math.min(horizentalZoom, verticalZoom) - 0.5) >> 0

  if (value < 5) value = 5
  return value
}

export function zoomFromScale(scale: number) {
  const zoom = (scale * 100 * 25.4) / (96 * BrowserInfo.PixelRatio)

  return zoom
}
