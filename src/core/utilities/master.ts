import { InstanceType } from '../instanceTypes'
import { PhTypeOO } from '../../io/dataType/spAttrs'
import { NotesMaster } from '../Slide/NotesMaster'
import { SlideMaster } from '../Slide/SlideMaster'
import { GraphicFrame } from '../SlideElement/GraphicFrame'
import { Shape } from '../SlideElement/Shape'
import { isPlaceholder } from './shape/asserts'
import { getPlaceholderType } from './shape/getters'

export function getTextStyleByPhType(
  master: SlideMaster | NotesMaster,
  phType: PhTypeOO | null
) {
  if (master.instanceType === InstanceType.SlideMaster) {
    return master.txStyles?.getStyleByPlaceholderType(phType)
  } else {
    return master.txStyles
  }
}

export function getTextStyleByMaster(
  master: SlideMaster | NotesMaster,
  sp: Shape | GraphicFrame
) {
  if (master.instanceType === InstanceType.NotesMaster) {
    return master.txStyles
  } else {
    if (isPlaceholder(sp) && sp.instanceType !== InstanceType.GraphicFrame) {
      return master.txStyles?.getStyleByPlaceholderType(getPlaceholderType(sp))
    } else {
      return master.txStyles?.otherStyle
    }
  }
}
