import { TextDocBounds } from '../TextDocument/DocumentBounds'
import { valuesOfDict } from '../../../liber/pervasive'
import { assignVal } from '../../io/utils'
import { TableLookData } from '../../io/dataType/table'
import { TableLengthOptions } from './attrs/TableLength'
import { TableCellMarginsOptions } from './attrs/tableMargins'

export class TableRowsInfo {
  y: number
  h: number
  diffYOfTop: number
  x0: number
  x1: number
  constructor() {
    this.y = 0
    this.h = 0
    this.diffYOfTop = 0
    this.x0 = 0
    this.x1 = 0
  }

  Init() {
    this.y = 0.0
    this.h = 0.0
    this.diffYOfTop = 0.0
  }
}

export interface TableLookOptions {
  firstCol?: boolean
  firstRow?: boolean
  lastCol?: boolean
  lastRow?: boolean
  bandRow?: boolean
  bandCol?: boolean
  rtl?: boolean
}
export class TableLook {
  firstCol!: boolean
  firstRow!: boolean
  lastCol!: boolean
  lastRow!: boolean
  bandRow!: boolean
  bandCol!: boolean
  rtl!: boolean
  constructor(
    firstCol?: boolean,
    firstRow?: boolean,
    lastCol?: boolean,
    lastRow?: boolean,
    bandRow?: boolean,
    bandCol?: boolean,
    rtl?: boolean
  ) {
    this.Set(firstCol, firstRow, lastCol, lastRow, bandRow, bandCol, rtl)
  }

  Set(
    firstCol?: boolean,
    firstRow?: boolean,
    lastCol?: boolean,
    lastRow?: boolean,
    bandRow?: boolean,
    bandCol?: boolean,
    rtl?: boolean
  ) {
    this.firstCol = true === firstCol
    this.firstRow = true === firstRow
    this.lastCol = true === lastCol
    this.lastRow = true === lastRow
    this.bandRow = true === bandRow
    this.bandCol = true === bandCol
    this.rtl = true === rtl
  }

  clone() {
    return new TableLook(
      this.firstCol,
      this.firstRow,
      this.lastCol,
      this.lastRow,
      this.bandRow,
      this.bandCol,
      this.rtl
    )
  }

  isFirstCol() {
    return this.firstCol
  }

  isFirstRow() {
    return this.firstRow
  }

  isLastCol() {
    return this.lastCol
  }

  isLastRow() {
    return this.lastRow
  }

  isBandRow() {
    return this.bandRow
  }

  isBandCol() {
    return this.bandCol
  }

  toJSON(): TableLookData {
    const data: TableLookData = {}
    assignVal(data, 'firstCol', this.firstCol)
    assignVal(data, 'firstRow', this.firstRow)
    assignVal(data, 'lastCol', this.lastCol)
    assignVal(data, 'lastRow', this.lastRow)
    assignVal(data, 'bandRow', this.bandRow)
    assignVal(data, 'bandCol', this.bandCol)
    assignVal(data, 'rtl', this.rtl)
    return data
  }

  fromJSON(data: TableLookData) {
    if (data['firstCol'] != null) {
      this.firstCol = data['firstCol']
    }

    if (data['firstRow'] != null) {
      this.firstRow = data['firstRow']
    }

    if (data['lastCol'] != null) {
      this.lastCol = data['lastCol']
    }

    if (data['lastRow'] != null) {
      this.lastRow = data['lastRow']
    }

    if (data['bandRow'] != null) {
      this.bandRow = data['bandRow']
    }

    if (data['bandCol'] != null) {
      this.bandCol = data['bandCol']
    }

    if (data['rtl'] != null) {
      this.rtl = data['rtl']
    }
  }
}

export class TableDimension {
  x: number
  y: number
  xLimit: number
  yLimit: number
  bounds: TextDocBounds
  height: number
  constructor(x: number, y: number, xLimit: number, yLimit: number) {
    this.x = x
    this.y = y
    this.xLimit = xLimit
    this.yLimit = yLimit
    this.bounds = new TextDocBounds(x, y, xLimit, y)
    this.height = 0
  }

  reset(x?: number, y?: number, xLimit?: number, yLimit?: number) {
    this.x = x ?? 0
    this.y = y ?? 0
    this.xLimit = xLimit ?? 0
    this.yLimit = yLimit ?? 0
    this.bounds.reset(x, y, xLimit, y)
    this.height = 0
  }

  isEmptyBounds() {
    return this.bounds.isEmpty()
  }
  translateBy(Dx: number, Dy: number) {
    this.x += Dx
    this.y += Dy
    this.xLimit += Dx
    this.yLimit += Dy
    this.bounds.translateBy(Dx, Dy)
  }
}

export class TableCalcInfo {
  tableGrid: boolean
  tableBorders: boolean
  constructor() {
    this.tableGrid = true
    this.tableBorders = true
  }

  calculateBorders() {
    this.tableBorders = true
  }

  reset() {
    this.tableGrid = true
    this.tableBorders = true
  }
}

export const CellBorderType = {
  Top: 0,
  Right: 1,
  Bottom: 2,
  Left: 3,
} as const
export type CellBorderTypeValue = valuesOfDict<typeof CellBorderType>

export const SetCellMarginType = {
  All: -1,
  Top: 0,
  Right: 1,
  Bootom: 2,
  Left: 3,
} as const

export type SetCellMarginParam =
  | {
      type: typeof SetCellMarginType.All
      margins: TableCellMarginsOptions
    }
  | {
      type: typeof SetCellMarginType.Top
      margin: TableLengthOptions
    }
  | {
      type: typeof SetCellMarginType.Right
      margin: TableLengthOptions
    }
  | {
      type: typeof SetCellMarginType.Bootom
      margin: TableLengthOptions
    }
  | {
      type: typeof SetCellMarginType.Left
      margin: TableLengthOptions
    }
