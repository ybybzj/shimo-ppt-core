import { Nullable } from '../../../../liber/pervasive'
import { LineHeightRule } from '../../common/const/attrs'
import { toInt } from '../../../common/utils'
import { Errors } from '../../../exports/type'
import { TableSelectionType, VMergeType } from '../../common/const/table'
import { hookManager, Hooks } from '../../hooks'
import { ErrorDataForTableOp } from '../../properties/props'
import { calculateForRendering } from '../../calculation/calculate/calculateForRendering'
import { calculateTableGrid } from '../../calculation/calculate/table'
import { applyTextPrToParagraphInTextDocument } from '../../TextDocument/utils/operations'
import { moveCursorToStartPosInTextDocument } from '../../utilities/cursor/moveToEdgePos'
import { textPrOfFirstItem } from '../../utilities/tableOrTextDocContent/helpers'
import { Table } from '../Table'
import { TableCell } from '../TableCell'
import { TableRow } from '../TableRow'
import { CellPos, RowInfo } from '../type'
import {
  getMergedCells,
  correctVMergeForTable,
  getArrOfRowInfos,
} from './utils'
import { ManagedSliceUtil } from '../../../common/managedArray'
import { CellBorderType } from '../common'

export function addColumnToTable(
  table: Table,
  isBefore = true,
  cellPos?: { row: number; cell: number; count: number }
) {
  isBefore = table.isRTL ? !isBefore : isBefore
  const reducedTableGrid = table.tableGridReduced
  let cellPositions: CellPos[] = []

  let count = 1
  let width = 0

  if (cellPos) {
    cellPositions.push({ row: cellPos.row, cell: cellPos.cell })
    count = cellPos.count
  } else {
    if (
      true === table.selection.isUse &&
      TableSelectionType.CELL === table.selection.type
    ) {
      cellPositions = table.selection.cellPositions

      let prevRowIndex = -1
      count = 0
      for (let i = 0; i < table.selection.cellPositions.length; i++) {
        if (-1 !== prevRowIndex) {
          if (prevRowIndex === table.selection.cellPositions[i].row) count++
          else break
        } else {
          count++
          prevRowIndex = table.selection.cellPositions[i].row
        }
      }
    } else {
      if (table.curCell) {
        cellPositions[0] = {
          row: table.curCell.parent.index,
          cell: table.curCell.index,
        }
        count = 1
      }
    }
  }

  if (cellPositions.length <= 0) return

  if (true === isBefore) {
    const startRow = table.children.at(cellPositions[0].row)
    if (startRow == null) return

    const startCell = startRow.getCellByIndex(cellPositions[0].cell)
    if (startCell == null) return

    const startGridOfFirstCell = startRow.getCellInfo(
      cellPositions[0].cell
    ).startGridCol
    const endGridOfFirstCell =
      startGridOfFirstCell + startCell.getGridSpan() - 1
    width =
      reducedTableGrid[endGridOfFirstCell] -
      reducedTableGrid[startGridOfFirstCell - 1]
  } else {
    const lastIndex = cellPositions.length - 1
    const { row, cell: ci } = cellPositions[lastIndex]

    const lastRow = table.children.at(row)
    if (lastRow == null) return

    const lastCell = lastRow.getCellByIndex(ci)
    if (lastCell == null) return

    const startGridOfLastCell = lastRow.getCellInfo(ci).startGridCol
    const endGridOfLastCell = startGridOfLastCell + lastCell.getGridSpan() - 1
    width =
      reducedTableGrid[endGridOfLastCell] -
      reducedTableGrid[startGridOfLastCell - 1]
  }

  const rowsInfo: Array<RowInfo[]> = []
  const addInfo: number[] = []
  if (true === isBefore) {
    let gridStartIndex = -1
    cellPositions.forEach((cellPos) => {
      const row = table.children.at(cellPos.row)
      const curStartGrid = row?.getCellInfo(cellPos.cell).startGridCol

      if (
        curStartGrid != null &&
        (-1 === gridStartIndex ||
          (-1 !== gridStartIndex && gridStartIndex > curStartGrid))
      ) {
        gridStartIndex = curStartGrid
      }
    })

    ManagedSliceUtil.forEach(table.children, (row, ri) => {
      rowsInfo[ri] = []
      addInfo[ri] = 0

      ManagedSliceUtil.forEach(row.children, (cell, curCellIndex) => {
        const curStartGrid = row.getCellInfo(curCellIndex).startGridCol
        const curEndGrid = curStartGrid + cell.getGridSpan() - 1
        if (curStartGrid <= gridStartIndex) {
          addInfo[ri] = curCellIndex
        }
        const w =
          reducedTableGrid[curEndGrid] - reducedTableGrid[curStartGrid - 1]
        rowsInfo[ri].push({ w: w, type: 0, gridSpan: 1 })
      })
    })

    ManagedSliceUtil.forEach(table.children, (row, ri) => {
      let isBefore2 = false
      if (rowsInfo.length > 0 && rowsInfo[ri][0].type === -1) {
        isBefore2 = true
      }

      for (let i = 0; i < count; i++) {
        const index = addInfo[ri]
        const newCell = row.addCell(index, null, false)

        const nextCell =
          index >= row.cellsCount - 1
            ? row.getCellByIndex(index - 1)!
            : row.getCellByIndex(index + 1)!
        newCell.applyPr(nextCell.pr, true)

        const firstPara = nextCell.textDoc.getFirstParagraph()!
        const textPr = textPrOfFirstItem(firstPara)
        newCell.textDoc.isSetToAll = true
        applyTextPrToParagraphInTextDocument(newCell.textDoc, textPr)
        newCell.textDoc.isSetToAll = false

        if (false === isBefore2) {
          rowsInfo[ri].splice(index, 0, {
            w: width,
            type: 0,
            gridSpan: 1,
          })
        } else {
          rowsInfo[ri].splice(index + 1, 0, {
            w: width,
            type: 0,
            gridSpan: 1,
          })
        }
      }
    })
  } else {
    let grid_end = -1
    cellPositions.forEach((cellPos) => {
      const row = table.children.at(cellPos.row)
      if (row == null) return

      const { startGridCol } = row.getCellInfo(cellPos.cell)
      const cell = row.getCellByIndex(cellPos.cell)
      if (cell == null) return

      const curEndGrid = startGridCol + cell.getGridSpan() - 1

      if (-1 === grid_end || (-1 !== grid_end && grid_end < curEndGrid)) {
        grid_end = curEndGrid
      }
    })

    ManagedSliceUtil.forEach(table.children, (row, ri) => {
      rowsInfo[ri] = []
      addInfo[ri] = -1
      ManagedSliceUtil.forEach(row.children, (cell, curCellIndex) => {
        const curStartGrid = row.getCellInfo(curCellIndex).startGridCol
        const curEndGrid = curStartGrid + cell.getGridSpan() - 1
        if (curEndGrid <= grid_end) {
          addInfo[ri] = curCellIndex
        }
        const w =
          reducedTableGrid[curEndGrid] - reducedTableGrid[curStartGrid - 1]
        rowsInfo[ri].push({ w: w, type: 0, gridSpan: 1 })
      })
    })

    ManagedSliceUtil.forEach(table.children, (row, ri) => {
      let isBefore2 = false
      if (rowsInfo.length > 0 && rowsInfo[ri][0].type === -1) {
        isBefore2 = true
      }

      for (let i = 0; i < count; i++) {
        const cellIndex = addInfo[ri]
        const newCell = row.addCell(cellIndex + 1, null, false)
        const nextCell =
          cellIndex + 1 >= row.cellsCount - 1
            ? row.getCellByIndex(cellIndex)!
            : row.getCellByIndex(cellIndex + 2)!
        newCell.applyPr(nextCell.pr, true)

        const firstPara = nextCell.textDoc.getFirstParagraph()!
        const textPr = textPrOfFirstItem(firstPara)
        newCell.textDoc.isSetToAll = true
        applyTextPrToParagraphInTextDocument(newCell.textDoc, textPr)
        newCell.textDoc.isSetToAll = false

        if (false === isBefore2) {
          rowsInfo[ri].splice(cellIndex + 1, 0, {
            w: width,
            type: 0,
            gridSpan: 1,
          })
        } else {
          rowsInfo[ri].splice(cellIndex + 2, 0, {
            w: width,
            type: 0,
            gridSpan: 1,
          })
        }
      }
    })
  }

  table.createTableGrid(rowsInfo)

  table.selection.isUse = true

  if (null != table.selection.cellPositions) {
    table.selection.cellPositions.length = 0
  } else {
    table.selection.cellPositions = []
  }

  table.selection.isUse = true
  table.selection.type = TableSelectionType.CELL

  for (let ri = 0, rl = table.children.length; ri < rl; ri++) {
    const startCell = true === isBefore ? addInfo[ri] : addInfo[ri] + 1
    for (let i = 0; i < count; i++) {
      table.selection.cellPositions.push({
        row: ri,
        cell: startCell + i,
      })
    }
  }

  calculateTableGrid(table)
  calculateForRendering()
}

export function addRowToTable(
  table: Table,
  isBefore = true,
  cellPos?: { row: number; cell: number; count: number }
) {
  let cellsPosIndexes: Array<{ row: number; cell: number }> = []

  let count = 1
  let rowId = 0

  if (cellPos) {
    cellsPosIndexes.push({ row: cellPos.row, cell: cellPos.cell })
    count = cellPos.count
  } else {
    if (
      true === table.selection.isUse &&
      TableSelectionType.CELL === table.selection.type
    ) {
      cellsPosIndexes = table.selection.cellPositions

      let prevRowIndex = -1
      count = 0
      for (let i = 0; i < table.selection.cellPositions.length; i++) {
        if (prevRowIndex !== table.selection.cellPositions[i].row) {
          count++
          prevRowIndex = table.selection.cellPositions[i].row
        }
      }
    } else {
      if (table.curCell) {
        cellsPosIndexes[0] = {
          row: table.curCell.parent.index,
          cell: table.curCell.index,
        }
        count = 1
      }
    }
  }

  if (cellsPosIndexes.length <= 0) return

  if (true === isBefore) rowId = cellsPosIndexes[0].row
  else rowId = cellsPosIndexes[cellsPosIndexes.length - 1].row

  const row = table.children.at(rowId)
  if (row == null) return

  const cellsCount = row.cellsCount

  const cellsInfo: Array<{
    vMergeCountAbove: number
    vMergeCountBelow: number
  }> = []
  for (let cellIndex = 0; cellIndex < cellsCount; cellIndex++) {
    const cell = row.getCellByIndex(cellIndex)!
    const cellInfo = row.getCellInfo(cellIndex)

    const cellStartGrid = cellInfo.startGridCol
    const gridSpanOfCell = cell.getGridSpan()

    const vMergeCountAbove = table.getVMergeCountAbove(
      rowId,
      cellStartGrid,
      gridSpanOfCell
    )
    const vMergeCountBelow = table.getVMergeCountBelow(
      rowId,
      cellStartGrid,
      gridSpanOfCell
    )

    cellsInfo[cellIndex] = {
      vMergeCountAbove,
      vMergeCountBelow,
    }
  }

  for (let i = 0; i < count; i++) {
    let newRow: TableRow

    if (true === isBefore) {
      newRow = table.addRow(rowId, cellsCount, true)
    } else newRow = table.addRow(rowId + 1, cellsCount, true)

    newRow.applyPr(row.pr)

    for (let cellIndex = 0; cellIndex < cellsCount; cellIndex++) {
      const newCell = newRow.getCellByIndex(cellIndex)!
      const oldCell = row.getCellByIndex(cellIndex)!

      newCell.applyPr(oldCell.pr)

      const firstPara = oldCell.getTextDoc().getFirstParagraph()
      if (firstPara) {
        const newCellContent = newCell.getTextDoc()

        ManagedSliceUtil.forEach(newCellContent.children, (para) => {
          para.setPr(firstPara.pr)
          para.setTextPr(textPrOfFirstItem(firstPara))
        })
      }

      if (true === isBefore) {
        if (cellsInfo[cellIndex].vMergeCountAbove > 1) {
          newCell.setVMerge(VMergeType.Continue)
        } else newCell.setVMerge(VMergeType.Restart)
      } else {
        if (cellsInfo[cellIndex].vMergeCountBelow > 1) {
          newCell.setVMerge(VMergeType.Continue)
        } else newCell.setVMerge(VMergeType.Restart)
      }
    }
  }

  table.selection.isUse = true

  if (null != table.selection.cellPositions) {
    table.selection.cellPositions.length = 0
  } else table.selection.cellPositions = []

  table.selection.isUse = true
  table.selection.type = TableSelectionType.CELL

  const startRowIndex = true === isBefore ? rowId : rowId + 1
  for (let i = 0; i < count; i++) {
    const row = table.children.at(startRowIndex + i)
    if (row) {
      const cellsCount = row.cellsCount

      for (let j = 0; j < cellsCount; j++) {
        const cell = row.getCellByIndex(j)!
        if (VMergeType.Continue === cell.getVMerge()) continue

        table.selection.cellPositions.push({ row: startRowIndex + i, cell: j })
      }
    }
  }

  table.invalidateAllPrs()
}

export function distributeCellsForTable(table: Table, isHorizontal: boolean) {
  if (isHorizontal) return _distributeByColumn(table)
  else return _distributeByRow(table)
}

function _distributeByColumn(table: Table) {
  const isSetToAll = table.isSetToAll
  if (
    !table.selection.isUse ||
    TableSelectionType.TEXT === table.selection.type
  ) {
    table.isSetToAll = true
  }

  const selectedCellPositions = table.getSelectedCellPositions()
  table.isSetToAll = isSetToAll

  if (selectedCellPositions.length <= 1) return false

  const arrOfRows: Array<{
    startCellIndex: number
    endCellIndex: number
  }> = []
  const arrOfCheckMergedCells: number[][] = []
  for (let i = 0, l = selectedCellPositions.length; i < l; ++i) {
    const curCellIndex = selectedCellPositions[i].cell
    const curRowIndex = selectedCellPositions[i].row
    if (!arrOfRows[curRowIndex]) {
      arrOfRows[curRowIndex] = {
        startCellIndex: curCellIndex,
        endCellIndex: curCellIndex,
      }
    } else {
      if (arrOfRows[curRowIndex].startCellIndex > curCellIndex) {
        arrOfRows[curRowIndex].startCellIndex = curCellIndex
      }

      if (arrOfRows[curRowIndex].endCellIndex < curCellIndex) {
        arrOfRows[curRowIndex].endCellIndex = curCellIndex
      }
    }

    const curRow = table.children.at(curRowIndex)
    const cell = curRow?.getCellByIndex(curCellIndex)
    if (cell) {
      const cellInfo = curRow!.getCellInfo(curCellIndex)
      const cellsOfmerged = getMergedCells(
        table,
        curRowIndex,
        cellInfo.startGridCol,
        cell.getGridSpan()
      )

      if (cellsOfmerged.length > 1) {
        arrOfCheckMergedCells.push([curRowIndex])
      }

      for (
        let valueOfMergeIndex = 1, valueOfMergedCount = cellsOfmerged.length;
        valueOfMergeIndex < valueOfMergedCount;
        ++valueOfMergeIndex
      ) {
        const valueOfCurCell2 = cellsOfmerged[valueOfMergeIndex].index
        const valueOfCurRow2 = cellsOfmerged[valueOfMergeIndex].parent.index

        if (!arrOfRows[valueOfCurRow2]) {
          arrOfRows[valueOfCurRow2] = {
            startCellIndex: valueOfCurCell2,
            endCellIndex: valueOfCurCell2,
          }
        } else {
          if (arrOfRows[valueOfCurRow2].startCellIndex > valueOfCurCell2) {
            arrOfRows[valueOfCurRow2].startCellIndex = valueOfCurCell2
          }

          if (arrOfRows[valueOfCurRow2].endCellIndex < valueOfCurCell2) {
            arrOfRows[valueOfCurRow2].endCellIndex = valueOfCurCell2
          }
        }

        arrOfCheckMergedCells[arrOfCheckMergedCells.length - 1].push(
          valueOfCurRow2
        )
      }
    }
  }

  for (let i = 0, l = arrOfCheckMergedCells.length; i < l; ++i) {
    let valueOfFirstStartGridCol, firstGridSpans

    for (let ri = 0, rl = arrOfCheckMergedCells[i].length; ri < rl; ++ri) {
      const valueOfCurRow = arrOfCheckMergedCells[i][ri]
      const row = table.getRow(valueOfCurRow)

      let valueOfStartGridCol = 0
      const gridSpans: number[] = []

      if (row) {
        for (let ci = 0, cc = row.cellsCount; ci < cc; ++ci) {
          const valueOfGridSpan = row.getCellByIndex(ci)!.getGridSpan()
          if (ci < arrOfRows[valueOfCurRow].startCellIndex) {
            valueOfStartGridCol += valueOfGridSpan
          } else if (ci <= arrOfRows[valueOfCurRow].endCellIndex) {
            gridSpans.push(valueOfGridSpan)
          } else break
        }
      }

      if (null == valueOfFirstStartGridCol) {
        valueOfFirstStartGridCol = valueOfStartGridCol
        firstGridSpans = gridSpans
      } else {
        if (
          valueOfStartGridCol !== valueOfFirstStartGridCol ||
          firstGridSpans.length !== gridSpans.length
        ) {
          return false
        } else {
          for (
            let valueOfSpanIndex = 0, valueOfSpansCount = gridSpans.length;
            valueOfSpanIndex < valueOfSpansCount;
            ++valueOfSpanIndex
          ) {
            if (
              firstGridSpans[valueOfSpanIndex] !== gridSpans[valueOfSpanIndex]
            ) {
              return false
            }
          }
        }
      }
    }
  }

  const rowsInfo = getArrOfRowInfos(table)
  for (const i in arrOfRows) {
    if (!rowsInfo[i] || rowsInfo[i].length <= 0) {
      continue
    }

    const valueOfStartCell = arrOfRows[i].startCellIndex
    const valueOfEndCell = arrOfRows[i].endCellIndex

    let valueOfSum = 0
    const valueOfAdd = -1 === rowsInfo[i][0].type ? 1 : 0

    for (let ci = valueOfStartCell; ci <= valueOfEndCell; ++ci) {
      valueOfSum += rowsInfo[i][ci + valueOfAdd].w
    }

    for (let ci = valueOfStartCell; ci <= valueOfEndCell; ++ci) {
      const valueOfNewW = valueOfSum / (valueOfEndCell - valueOfStartCell + 1)
      rowsInfo[i][ci + valueOfAdd].w = valueOfNewW

      const row = table.getRow(Number(i))
      const cell = row?.getCellByIndex(ci)
      if (!row || !cell) continue
    }
  }

  table.createTableGrid(rowsInfo)
  calculateTableGrid(table)

  return true
}

function _distributeByRow(table: Table) {
  const isSetToAll = table.isSetToAll
  if (
    !table.selection.isUse ||
    TableSelectionType.TEXT === table.selection.type
  ) {
    table.isSetToAll = true
  }

  const selectedCellPositions = table.getSelectedCellPositions()
  table.isSetToAll = isSetToAll

  if (selectedCellPositions.length <= 1) return false

  const rowIndices: boolean[] = []
  let valueOfRowsCount = 0
  for (
    let valueOfIndex = 0, valueOfCount = selectedCellPositions.length;
    valueOfIndex < valueOfCount;
    ++valueOfIndex
  ) {
    if (true !== rowIndices[selectedCellPositions[valueOfIndex].row]) {
      rowIndices[selectedCellPositions[valueOfIndex].row] = true
      valueOfRowsCount++
    }

    const cell = table.getTopLeftMergedCell(
      selectedCellPositions[valueOfIndex].cell,
      selectedCellPositions[valueOfIndex].row
    )
    if (cell) {
      const valueOfVMergeCount = table.getVMergeNum(
        cell.index,
        cell.parent.index
      )
      if (valueOfVMergeCount > 1) {
        for (
          let valueOfCurRow = cell.parent.index;
          valueOfCurRow < cell.parent.index + valueOfVMergeCount - 1;
          ++valueOfCurRow
        ) {
          if (true !== rowIndices[valueOfCurRow]) {
            rowIndices[valueOfCurRow] = true
            valueOfRowsCount++
          }
        }
      }
    }
  }

  if (valueOfRowsCount <= 0) return false

  let valueOfSumH = 0
  for (const i in rowIndices) {
    let valueOfRowSummaryH = 0
    valueOfRowSummaryH += table.rowsInfo[i].h
    valueOfRowSummaryH -= table.rowsInfo[i].diffYOfTop

    const row = table.getRow(Number(i))
    if (row) {
      valueOfRowSummaryH -= row.getTopMargin() + row.getBottomMargin()
    }
    valueOfSumH += valueOfRowSummaryH
  }

  let valueOfNewValueH = valueOfSumH / valueOfRowsCount
  for (const _i in rowIndices) {
    const valueOfCurRow = toInt(_i)

    const row = table.getRow(valueOfCurRow)

    if (row) {
      for (let ci = 0, cc = row.cellsCount; ci < cc; ++ci) {
        const cell = row.getCellByIndex(ci)!
        if (VMergeType.Restart !== cell.getVMerge()) continue

        const valueOfVMergeCount = table.getVMergeNum(ci, valueOfCurRow)

        const valueOfNewH = valueOfNewValueH

        const valueOfContentH = cell.getTextDoc().getWholeHeight()
        if (valueOfNewH * valueOfVMergeCount < valueOfContentH) {
          valueOfNewValueH += valueOfContentH / valueOfVMergeCount - valueOfNewH
        }
      }
    }
  }

  for (const _i in rowIndices) {
    const curRowIndex = toInt(_i)

    const row = table.getRow(curRowIndex)
    if (row) {
      const rowHeight = row.getHeight()
      const valueOfNewH = valueOfNewValueH

      row.setHeight({
        value: valueOfNewH,
        heightRule:
          rowHeight?.heightRule === LineHeightRule.Exact
            ? LineHeightRule.Exact
            : LineHeightRule.AtLeast,
      })
    }
  }

  return true
}

export function mergeCellsForTable(table: Table, isRemoveMerge) {
  if (
    true !== table.selection.isUse ||
    TableSelectionType.CELL !== table.selection.type ||
    table.selection.cellPositions.length <= 1
  ) {
    return false
  }

  const { isCanMerge, rowsInfo, gridStart, gridEnd } = table.getMergeInfo()

  if (false === isCanMerge) return false

  const topLeftPos = table.selection.cellPositions[0]
  const topLeftCell = table.children
    .at(topLeftPos.row)
    ?.getCellByIndex(topLeftPos.cell)

  // 合并内容到左上角单元格
  if (topLeftCell) {
    let topRightCell: Nullable<TableCell> = null
    let bottomLeftCell: Nullable<TableCell> = null
    let maxRow = topLeftPos.row
    let maxCol = topLeftPos.cell
    for (let i = 1, l = table.selection.cellPositions.length; i < l; i++) {
      const pos = table.selection.cellPositions[i]
      const row = table.children.at(pos.row)

      if (row) {
        const cell = row.getCellByIndex(pos.cell)

        if (cell) {
          if (pos.row === topLeftPos.row && pos.cell > maxCol) {
            topRightCell = cell
            maxCol = pos.cell
          }

          if (pos.cell === topLeftPos.cell && pos.row > maxRow) {
            bottomLeftCell = cell
            maxRow = pos.row
          }

          topLeftCell.addTextDoc(cell.textDoc)
          cell.textDoc.clearContent()
        }
      }
    }

    // 保留原来边缘单元格边框样式
    if (topRightCell) {
      topLeftCell.setBorder(
        topRightCell.pr.tableCellBorders.right,
        CellBorderType.Right
      )
    }
    if (bottomLeftCell) {
      topLeftCell.setBorder(
        bottomLeftCell.pr.tableCellBorders.bottom,
        CellBorderType.Bottom
      )
    }
  }
  rowsInfo.forEach((rowInfo, RowIndex) => {
    if (rowInfo != null) {
      const row = table.children.at(RowIndex)
      if (row) {
        for (let cellIndex = 0; cellIndex < row.cellsCount; cellIndex++) {
          const { startGridCol } = row.getCellInfo(cellIndex)
          // 操作行中横向第一个 cell ,以此为基准进行合并，否则删除
          if (gridStart === startGridCol) {
            // 上下合并 / 左右合并
            if (RowIndex !== topLeftPos.row) {
              const cell = row.getCellByIndex(cellIndex)!
              cell.setGridSpan(gridEnd - gridStart + 1)
              cell.setVMerge(VMergeType.Continue)
            } else if (topLeftCell) {
              topLeftCell.setGridSpan(gridEnd - gridStart + 1)
            }
          } else if (startGridCol > gridStart && startGridCol <= gridEnd) {
            row.removeCell(cellIndex)
            cellIndex--
          } else if (startGridCol > gridEnd) {
            break
          }
        }
      }
    }
  })

  _checkRows(table, true !== isRemoveMerge ? true : false)
  table.selection.isUse = true
  table.selection.selectionStartInfo.cellPos = topLeftPos
  table.selection.selectionEndInfo.cellPos = topLeftPos
  table.selection.type = TableSelectionType.CELL
  table.selection.cellPositions = [topLeftPos]
  table.curCell = topLeftCell

  if (true !== isRemoveMerge) {
    calculateForRendering()
  }

  return true
}

/**
 * Deleting a column either by selection, or by the current cell.
 */
export function removeColumnOfTable(table: Table) {
  if (table.children.length <= 0) return false
  const reducedTableGrid = table.getTableGridReduced()
  // Find the right and left borders of the selected cells.
  let cellPositions: CellPos[] = []

  if (
    true === table.selection.isUse &&
    TableSelectionType.CELL === table.selection.type
  ) {
    cellPositions = table.selection.cellPositions
  } else {
    if (table.curCell) {
      cellPositions.push({
        row: table.curCell.parent.index,
        cell: table.curCell.index,
      })
    }
  }

  if (cellPositions.length <= 0) return false

  let startGrid = -1
  let endGrid = -1
  for (let i = 0, l = cellPositions.length; i < l; i++) {
    const row = table.children.at(cellPositions[i].row)
    if (row) {
      const cell = row.getCellByIndex(cellPositions[i].cell)

      if (cell) {
        const curStartGrid = row.getCellInfo(cellPositions[i].cell).startGridCol
        const curEndGrid = curStartGrid + cell.getGridSpan() - 1

        if (
          -1 === startGrid ||
          (-1 !== startGrid && startGrid > curStartGrid)
        ) {
          startGrid = curStartGrid
        }

        if (-1 === endGrid || (-1 !== endGrid && endGrid < curEndGrid)) {
          endGrid = curEndGrid
        }
      }
    }
  }

  // We go over all the rows and see if any cell has an intersection with the [Grid_start, Grid_end] segment,
  // then we delete this cell.
  const removeIndices: number[][] = []
  const rowsInfo: Array<RowInfo[]> = []

  for (let ri = 0, l = table.children.length; ri < l; ri++) {
    removeIndices[ri] = []
    rowsInfo[ri] = []

    const row = table.children.at(ri)!
    const cellsCount = row.cellsCount
    for (let ci = 0; ci < cellsCount; ci++) {
      const cell = row.getCellByIndex(ci)!
      const curStartGrid = row.getCellInfo(ci).startGridCol
      const gridSpan = cell.getGridSpan()
      const curEndGrid = curStartGrid + gridSpan - 1
      const w =
        reducedTableGrid[curEndGrid] - reducedTableGrid[curStartGrid - 1]
      if (curEndGrid <= endGrid && curStartGrid >= startGrid) {
        removeIndices[ri].push(ci)
      } else if (curStartGrid <= endGrid && curEndGrid >= startGrid) {
        let overlapW =
          curEndGrid <= endGrid
            ? reducedTableGrid[curEndGrid] - reducedTableGrid[startGrid - 1]
            : reducedTableGrid[endGrid] - reducedTableGrid[curStartGrid - 1]
        if (curStartGrid <= startGrid && curEndGrid >= endGrid) {
          overlapW = reducedTableGrid[endGrid] - reducedTableGrid[startGrid - 1]
        }
        rowsInfo[ri].push({ w: w - overlapW, type: 0, gridSpan: 1 })
      } else {
        rowsInfo[ri].push({ w: w, type: 0, gridSpan: 1 })
      }
    }
  }

  // Delete all cells
  for (let ri = 0, l = table.children.length; ri < l; ri++) {
    const row = table.children.at(ri)!
    for (let i = removeIndices[ri].length - 1; i >= 0; i--) {
      const curCell = removeIndices[ri][i]
      row.removeCell(curCell)
    }
  }

  // When deleting a column, it is possible that the entire row is deleted
  for (let ri = table.children.length - 1; ri >= 0; ri--) {
    // The entire row is deleted if there are no cell records in rowsInfo (i.e. with type equal to 0)
    let isRemove = true
    for (let i = 0, l = rowsInfo[ri].length; i < l; i++) {
      if (0 === rowsInfo[ri][i].type) {
        isRemove = false
        break
      }
    }

    if (true === isRemove) {
      table.removeRow(ri)
      rowsInfo.splice(ri, 1)
    }
  }

  // Returning the cursor
  hookManager.invoke(Hooks.EditorUI.OnCursorShow)
  hookManager.invoke(Hooks.EditorUI.OnSetInTextSelection, { isEnabled: false })

  if (table.children.length <= 0) return true

  table.createTableGrid(rowsInfo)

  // We go over all the cells and look at their vertical union, whether it has been broken
  correctVMergeForTable(table)

  // It is possible that we still have lines that are completely composed of vertically merged cells.
  for (let ri = table.children.length - 1; ri >= 0; ri--) {
    let isRemove = true
    const row = table.children.at(ri)!
    const cellsCount = row.cellsCount

    for (let ci = 0; ci < cellsCount; ci++) {
      const cell = row.getCellByIndex(ci)!
      if (VMergeType.Continue !== cell.getVMerge()) {
        isRemove = false
        break
      }
    }

    if (true === isRemove) {
      table.removeRow(ri)
    }
  }
  if (table.children.length <= 0) return true

  // Move the cursor to the beginning of the next column
  const rowIndex = 0
  const row = table.children.at(rowIndex)!
  const cellsCount = row.cellsCount
  const curCell =
    removeIndices[0][0] === undefined
      ? cellsCount - 1
      : Math.min(removeIndices[0][0], cellsCount - 1)

  table.curCell = row.getCellByIndex(curCell)!
  moveCursorToStartPosInTextDocument(table.curCell.textDoc)

  table.selection.isUse = false
  table.selection.start = false
  table.selection.selectionStartInfo.cellPos = { row: rowIndex, cell: curCell }
  table.selection.selectionEndInfo.cellPos = { row: rowIndex, cell: curCell }
  table.selection.currentRowIndex = rowIndex

  calculateTableGrid(table)
  calculateForRendering()

  return true
}

export function removeRowOfTable(table: Table, index: number) {
  if (table.children.length <= 0) return false
  const rowsToRemove: number[] = []

  if (null == index) {
    if (
      true === table.selection.isUse &&
      TableSelectionType.CELL === table.selection.type
    ) {
      let prevRow = -1
      for (let i = 0; i < table.selection.cellPositions.length; i++) {
        const cellPos = table.selection.cellPositions[i]
        if (cellPos.row !== prevRow) {
          rowsToRemove.push(cellPos.row)
        }

        prevRow = cellPos.row
      }
    } else if (table.curCell) {
      rowsToRemove.push(table.curCell.parent.index)
    }
  } else rowsToRemove.push(index)

  if (rowsToRemove.length <= 0) return false

  const firstRowToRemove = rowsToRemove[0]
  let curRow = Math.max(rowsToRemove[rowsToRemove.length - 1] + 1, 0)
  if (curRow < table.children.length) {
    const row = table.children.at(curRow)!
    const cellsCount = row.cellsCount

    for (let ci = 0; ci < cellsCount; ci++) {
      const cell = row.getCellByIndex(ci)!
      const vMerge = cell.getVMerge()

      if (VMergeType.Continue !== vMerge) continue

      const vMerge_count = table.getVMergeCountAbove(
        curRow,
        row.getCellInfo(ci).startGridCol,
        cell.getGridSpan()
      )
      if (curRow - (vMerge_count - 1) >= firstRowToRemove) {
        cell.setVMerge(VMergeType.Restart)
      }
    }
  }

  table.removeSelection()

  for (let i = rowsToRemove.length - 1; i >= 0; i--) {
    table.removeRow(rowsToRemove[i])
  }

  hookManager.invoke(Hooks.EditorUI.OnCursorShow)
  hookManager.invoke(Hooks.EditorUI.OnSetInTextSelection, { isEnabled: false })

  if (table.children.length <= 0) return true

  curRow = Math.min(rowsToRemove[0], table.children.length - 1)

  table.curCell = table.children.at(curRow)?.getCellByIndex(0)
  if (table.curCell) {
    moveCursorToStartPosInTextDocument(table.curCell.textDoc)
  }

  table.invalidateAllPrs()

  calculateForRendering()

  return true
}

/** @param 每个单元格要拆分的行列数 */
export function splitCellsForTable(
  table: Table,
  rowIndex: number,
  colIndex: number
) {
  const hasSelect = table.selection.isUse
  const selectType = table.selection.type
  const selectedCellsPos = table.selection.cellPositions
  const isSelectText = hasSelect && selectType === TableSelectionType.TEXT
  const isSelectCells =
    hasSelect &&
    selectType === TableSelectionType.CELL &&
    selectedCellsPos.length >= 1

  if (!(!hasSelect || isSelectText || isSelectCells)) return false

  let cellPos: CellPos
  let cell: TableCell
  // 默认选中 / 手动选中文本
  if (!hasSelect || (hasSelect && TableSelectionType.TEXT === selectType)) {
    cell = table.curCell!
    cellPos = { cell: cell.index, row: cell.parent.index }
    _splitTableCell(table, cell, cellPos, rowIndex, colIndex)
  }
  // 选中单元格
  else {
    const cells = selectedCellsPos.map((pos) =>
      table.children.at(pos.row)?.getCellByIndex(pos.cell)
    )
    cells.forEach((cell) => {
      if (cell) {
        cellPos = { cell: cell.index, row: cell.parent.index }
        _splitTableCell(table, cell, cellPos, rowIndex, colIndex)
      }
    })
  }

  return true
}

function _splitTableCell(
  table: Table,
  cell: TableCell,
  cellPos: CellPos,
  numOfRows: number,
  numOfCols: number
) {
  const row = table.children.at(cellPos.row)
  if (row == null) return
  const gridStart = row.getCellInfo(cellPos.cell).startGridCol
  const gridSpan = cell.getGridSpan()
  const verticalMergeCount = table.getVMergeCountBelow(
    cellPos.row,
    gridStart,
    gridSpan
  )

  if (verticalMergeCount > 1) {
    if (numOfRows > verticalMergeCount) {
      // Error message: "Value Rows must be between 1 and" + VMerge_count
      const err = new ErrorDataForTableOp()
      err.setValue(verticalMergeCount)
      hookManager.invoke(Hooks.Emit.EmitEvent, {
        eventType: 'Error',
        args: [Errors.SplitCellExceedMaxRows, err],
      })
      return false
    } else if (0 !== verticalMergeCount % numOfRows) {
      // Error message: "Value must be a divisor of the number" + VMerge_count
      const err = new ErrorDataForTableOp()
      err.setValue(verticalMergeCount)
      hookManager.invoke(Hooks.Emit.EmitEvent, {
        eventType: 'Error',
        args: [Errors.SplitCellErrorRows, err],
      })
      return false
    }
  }

  /** 涉及的 tableCells, 存在 vMerge 或拆分为多行时存在多个 */
  const cells: Nullable<TableCell>[] = []
  const cellsPositions: Nullable<CellPos>[] = []
  /** 涉及的 tableRows. 存在 vMerge */
  const rows: Nullable<TableRow>[] = []

  // 不存在垂直拆分，直接收集其下方的 cell
  if (numOfRows <= 1) {
    const rowPos = Math.max(0, cellPos.row)
    for (
      let index = 0,
        l = Math.min(verticalMergeCount, table.children.length - rowPos);
      index < l;
      index++
    ) {
      const tmpRow = table.children.at(rowPos + index)!
      rows.push(tmpRow)
      cells.push(null)
      cellsPositions.push(null)

      // Looking for a cell starting with Grid_start
      const cellsNum = tmpRow.cellsCount
      for (let cellIndex = 0; cellIndex < cellsNum; cellIndex++) {
        const startCol = tmpRow.getCellInfo(cellIndex).startGridCol
        if (startCol === gridStart) {
          cells[index] = tmpRow.getCellByIndex(cellIndex)
          cellsPositions[index] = { row: rowPos + index, cell: cellIndex }
        }
      }
    }
  } else {
    if (verticalMergeCount > 1) {
      const newVMergeNum = verticalMergeCount / numOfRows
      const rowPos = Math.max(0, cellPos.row)
      for (
        let i = 0,
          l = Math.min(verticalMergeCount, table.children.length - rowPos);
        i < l;
        i++
      ) {
        const tmpRow = table.children.at(rowPos + i)!

        rows.push(tmpRow)
        cells.push(null)
        cellsPositions.push(null)

        // Looking for a cell starting with Grid_start
        const cellsCount = tmpRow.cellsCount
        for (let ci = 0; ci < cellsCount; ci++) {
          const startGridCol = tmpRow.getCellInfo(ci).startGridCol
          if (startGridCol === gridStart) {
            const tmpCell = tmpRow.getCellByIndex(ci)!
            cells[i] = tmpCell
            cellsPositions[i] = { row: rowPos + i, cell: ci }

            if (0 === i % newVMergeNum) {
              tmpCell.setVMerge(VMergeType.Restart)
            } else tmpCell.setVMerge(VMergeType.Continue)
          }
        }
      }
    } else {
      rows.push(row)
      cells.push(cell)
      cellsPositions.push(cellPos)

      const cellsCount = row.cellsCount
      for (let index = 1; index < numOfRows; index++) {
        const newRow = table.addRow(cellPos.row + index, cellsCount)
        newRow.applyPr(row.pr)
        rows.push(newRow)
        cells.push(null)
        cellsPositions.push(null)

        for (let cellIndex = 0; cellIndex < cellsCount; cellIndex++) {
          const newCell = newRow.getCellByIndex(cellIndex)!
          const oldCell = row.getCellByIndex(cellIndex)!
          newCell.applyPr(oldCell.pr)

          if (cellIndex === cellPos.cell) {
            cells[index] = newCell
            cellsPositions[index] = {
              row: cellPos.row + index,
              cell: cellIndex,
            }
          } else {
            newCell.setVMerge(VMergeType.Continue)
          }
        }
      }
    }
  }

  // 水平拆分
  if (numOfCols > 1) {
    const reducedTableGrid = table.getTableGridReduced()
    // Find the positions of the new columns in the grid
    const sumWidthBefore = reducedTableGrid[gridStart - 1]
    const sumWidth = reducedTableGrid[gridStart + gridSpan - 1]
    const spanWidth = sumWidth - sumWidthBefore
    const gridWidth = spanWidth / numOfCols

    /** 记录从各 index 处需要添加 grid 的数量 */
    const gridCalcInfoOfNum: number[] = new Array(
      table.tableGridCalculated.length
    ).fill(0)

    /** 每个新增节点包含的分隔线数量 */
    const newCellsGapNum: number[] = new Array(numOfCols).fill(1)

    // 重新计算的 grid width
    const gridCalcInfoOfWidth: number[] = table.tableGridCalculated.slice(0)

    let colIndex = 0
    let curGridSumWidth = sumWidthBefore + gridWidth
    for (let index = gridStart; index < gridStart + gridSpan; index++) {
      let isNewCol = true

      // 累计的 Width 已经达到原边界，已经划分完毕
      if (Math.abs(curGridSumWidth - reducedTableGrid[index]) < 0.001) {
        colIndex++
        curGridSumWidth += gridWidth
        isNewCol = false
        continue
      }
      // 累计的 Width 仍没有达到原边界, 需要继续划分
      while (curGridSumWidth < reducedTableGrid[index]) {
        if (0 === gridCalcInfoOfNum[index]) {
          gridCalcInfoOfWidth[index] =
            curGridSumWidth - reducedTableGrid[index - 1]
        }
        gridCalcInfoOfNum[index] += 1
        colIndex++
        curGridSumWidth += gridWidth
        // 误差兼容
        if (Math.abs(curGridSumWidth - reducedTableGrid[index]) < 0.001) {
          colIndex++
          curGridSumWidth += gridWidth
          isNewCol = false
          break
        }
      }
      if (true === isNewCol) newCellsGapNum[colIndex] += 1
    }

    // Add in this line (Cols - 1) cells, with the same settings,
    // as the original. We take the GridSpan value from the Grid_Info_new array

    for (let rowIndex = 0; rowIndex < rows.length; rowIndex++) {
      if (null != cells[rowIndex] && null != cellsPositions[rowIndex]) {
        const tmpRow = rows[rowIndex]!
        const tmpCell = cells[rowIndex]!
        const tmpCellPos = cellsPositions[rowIndex]!
        tmpCell.setGridSpan(newCellsGapNum[0]) // 被拆分的第一个 gridSpan一定是 1

        for (let index = 1; index < numOfCols; index++) {
          const inserIndex = tmpCellPos.cell + index
          const newCell = tmpRow.addCell(inserIndex, null, false)
          newCell.applyPr(tmpCell.pr)
          newCell.setGridSpan(newCellsGapNum[index])
        }
      }
    }

    const oldGridLength = table.tableGridCalculated.length
    let newGrids = [...table.tableGrid]

    for (let index = oldGridLength - 1; index >= 0; index--) {
      let summary = table.tableGridCalculated[index]

      if (gridCalcInfoOfNum[index] > 0) {
        newGrids[index] = gridCalcInfoOfWidth[index]
        summary -= gridCalcInfoOfWidth[index] - gridWidth

        for (
          let newIndex = 0;
          newIndex < gridCalcInfoOfNum[index];
          newIndex++
        ) {
          summary -= gridWidth
          if (newIndex !== gridCalcInfoOfNum[index] - 1) {
            newGrids.splice(index + newIndex + 1, 0, gridWidth)
          } else newGrids.splice(index + newIndex + 1, 0, summary)
        }
      }
    }

    const cellMargins = cell.getMargins()
    newGrids = newGrids.map((grid) =>
      Math.max(grid, cellMargins.left!.w + cellMargins.right!.w)
    )

    table.setTableGrid(newGrids)

    // We go through all the rows and change the GridSpan in the cells, in accordance with the values of the Grid_Info array
    for (
      let rowIndex = 0, l = table.children.length;
      rowIndex < l;
      rowIndex++
    ) {
      if (
        rowIndex >= cellsPositions[0]!.row &&
        rowIndex <= cellsPositions[cellsPositions.length - 1]!.row
      ) {
        continue
      }

      const tmpRow = table.children.at(rowIndex)!
      const cellsNum = tmpRow.cellsCount
      for (let cellIndex = 0; cellIndex < cellsNum; cellIndex++) {
        const tmpCell = tmpRow.getCellByIndex(cellIndex)!
        const tmpGridSpan = tmpCell.getGridSpan()
        const tmpStartGrid = tmpRow.getCellInfo(cellIndex).startGridCol
        let sumGridSpan = tmpGridSpan

        for (let col = tmpStartGrid; col < tmpStartGrid + tmpGridSpan; col++) {
          sumGridSpan += gridCalcInfoOfNum[col]
        }
        tmpCell.setGridSpan(sumGridSpan)
      }
    }
  }

  table.updateIndices()
  table.invalidateAllPrs()
  calculateTableGrid(table)
  calculateForRendering()
}
/** @usage 检查是否需要从表格中删除不必要的行(可能发生在合并/删除单元格等行为之后) */
function _checkRows(table: Table, isSaveHeight: boolean) {
  const rowsToRemove: number[] = []

  const rowsToCalcHeight: number[] = []
  const rows = table.children
  // 我们遍历所有的行，如果在某行中所有的单元格都有垂直联合，那么我们删除这样一行，和之前的，我们设置了最小高度——这两行的高度之和，此外，我们需要为其中的行设置最小高度

  for (let ri = 0, l = rows.length; ri < l; ri++) {
    const row = rows.at(ri)!
    const cells = row.children
    let isVmerge_Restart = false
    let isVmerge_Continue = false
    let isNeedDeleteRow = true

    for (let ci = 0, l = cells.length; ci < l; ci++) {
      const cell = row.getCellByIndex(ci)!
      const vMerge = cell.getVMerge()

      // 若一行中所有 cell 均为 vMerge，则需(merge)删除该行
      if (vMerge !== VMergeType.Continue) {
        const verticalMergeCount = table.getVMergeCountBelow(
          ri,
          row.getCellInfo(ci).startGridCol,
          cell.getGridSpan()
        )
        if (verticalMergeCount > 1) isVmerge_Restart = true
        isNeedDeleteRow = false
      } else {
        isVmerge_Continue = true
      }
    }

    if (true === isVmerge_Continue && true === isVmerge_Restart) {
      rowsToCalcHeight.push(ri)
    }

    if (true === isNeedDeleteRow) rowsToRemove.push(ri)
  }

  // 找出需要设置最小高度的线条
  for (let index = 0; index < rowsToCalcHeight.length; index++) {
    const rowIndex = rowsToCalcHeight[index]
    const row = table.children.at(rowIndex)!
    const cellsCount = row.cellsCount
    let minHeight = -1

    for (let curCell = 0; curCell < cellsCount; curCell++) {
      const cell = row.getCellByIndex(curCell)!
      const vMerge = cell.getVMerge()
      if (VMergeType.Restart === vMerge) {
        const _minH = cell.textDoc.lastParaEndTextHeight()
        if (_minH < minHeight || minHeight === -1) {
          minHeight = _minH
        }
      }
    }

    const oldHeight = row.getHeight()

    if (
      null == oldHeight ||
      LineHeightRule.Auto === oldHeight.heightRule ||
      minHeight > oldHeight.value
    ) {
      row.setHeight({ value: minHeight, heightRule: LineHeightRule.AtLeast })
    }
  }

  if (rowsToRemove.length <= 0) return false

  if (true === isSaveHeight) {
    // 计算行高，以便删除后表格的一般视图不会改变
    for (let i = 0; i < rowsToRemove.length; ) {
      let curRowSpan = 1

      const startRow = rowsToRemove[i]
      while (
        i + curRowSpan < rowsToRemove.length &&
        startRow + curRowSpan === rowsToRemove[i + curRowSpan]
      ) {
        curRowSpan++
      }

      if (startRow > 0) {
        const totalHeight =
          table.rowsInfo[startRow - 1 + curRowSpan].h +
          table.rowsInfo[startRow - 1 + curRowSpan].y -
          table.rowsInfo[startRow - 1].y
        table.children.at(startRow - 1)!.setHeight({
          value: totalHeight,
          heightRule: LineHeightRule.AtLeast,
        })
      }
      i += curRowSpan
    }
  }

  // 从最后一行开始删除，以免重新计算行号
  for (let i = rowsToRemove.length - 1; i >= 0; i--) {
    const rowToRemove = rowsToRemove[i]
    table.removeRow(rowToRemove)
  }

  return true
}
