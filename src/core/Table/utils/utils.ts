import { Nullable } from '../../../../liber/pervasive'
import { RecyclableArray } from '../../../common/managedArray'
import { VMergeType } from '../../common/const/table'
import { mmsPerPt } from '../../utilities/helpers'
import { Table } from '../Table'
import { TableCell } from '../TableCell'
import { TableRow } from '../TableRow'
import { RowInfo } from '../type'

export function resetChildren<
  ResetArgs extends any[],
  T extends { reset(this: T, ...args: ResetArgs) },
>(
  parent: { children: RecyclableArray<T> },
  count: number,
  ctor: {
    new: (...args: ResetArgs) => T
  },
  ...resetArgs: ResetArgs
) {
  if (count === 0) {
    parent.children.clear()
    return
  }

  let i = 0
  const orgCount = parent.children.length
  const l = Math.min(orgCount, count)
  while (i < l) {
    parent.children.at(i)!.reset(...resetArgs)
    i += 1
  }
  if (orgCount < count) {
    while (i < count) {
      parent.children.insert(i, ctor.new(...resetArgs))
      i += 1
    }
  } else if (orgCount > count) {
    parent.children.recycle(parent.children.remove(i, orgCount - count))
  }
}

export function rowContentIterator(
  isReverse: boolean,
  row: TableRow,
  cb: (cell: TableCell, logicCellIndex: number, visualCellIndex?: number) => any
) {
  const cellsCount = row.children.length
  if (isReverse === true) {
    for (let i = cellsCount - 1; i >= 0; i--) {
      const cell = row.children.at(i)!
      cb(cell, i, cellsCount - i - 1)
    }
  } else {
    for (let i = 0; i < cellsCount; i++) {
      const cell = row.children.at(i)!
      cb(cell, i, i)
    }
  }
}

export function getReversedArray<T>(array: Nullable<T[]>, isReverse: boolean) {
  if (!isReverse || !array) {
    return array
  }
  return array.slice().reverse()
}

export function hitInTableBorder(
  table: Table,
  x: number,
  y: number,
  opts: { isInTouchMode: boolean; isInTextEditing: boolean } = {
    isInTouchMode: false,
    isInTextEditing: false,
  }
) {
  const cellPos = table.getCellByPoint(x, y)

  const result = {
    cellPos: cellPos,
    Border: -1,
    row: cellPos.row,
    selectRow: false,
    selectColum: false,
    selectCell: false,
  }

  const valueOfCurRow = cellPos.row
  const valueOfCurCell = cellPos.cell

  const row = table.getRow(valueOfCurRow)
  if (row == null) return result
  const cell = row.getCellByIndex(valueOfCurCell)
  if (cell == null) return result
  const cellInfo = row.getCellInfo(valueOfCurCell)

  const valueOfVMergeCount = table.getVMergeNum(valueOfCurCell, valueOfCurRow)
  const verticalMergeCount = table.getVertMergeCount(
    valueOfCurRow,
    cellInfo.startGridCol,
    cell.getGridSpan()
  )
  if (verticalMergeCount <= 0) return result

  const dimension = table.dimension

  const cellStartX = dimension.x + cellInfo.cellStartX
  const cellEndX = dimension.x + cellInfo.cellEndX

  const cellStartY = table.rowsInfo[valueOfCurRow].y
  const cellEndY =
    table.rowsInfo[valueOfCurRow + verticalMergeCount - 1].y +
    table.rowsInfo[valueOfCurRow + verticalMergeCount - 1].h

  const hitRadius = mmsPerPt(opts.isInTouchMode ? 10 : 3)

  const { left: left, right: right } = cell.getMargins()

  if (y <= cellStartY + hitRadius && y >= cellStartY - hitRadius) {
    result.Border = 0
  } else if (y <= cellEndY + hitRadius && y >= cellEndY - hitRadius) {
    if (verticalMergeCount !== valueOfVMergeCount) {
      result.Border = -1
    } else {
      result.Border = 2
      result.row = valueOfCurRow + valueOfVMergeCount - 1
    }
  }

  if (result.Border === -1) {
    let hitRadiusLeft = hitRadius
    let hitRadiusRight = hitRadius
    if (opts.isInTextEditing && opts.isInTouchMode) {
      hitRadiusLeft = Math.min(left!.w, hitRadius)
      hitRadiusRight = Math.min(right!.w, hitRadius)
    }
    if (
      x <= cellStartX + hitRadiusLeft - 0.001 &&
      x >= cellStartX - hitRadiusLeft + 0.001
    ) {
      result.Border = 3
    } else if (
      x <= cellEndX + hitRadiusRight &&
      x >= cellEndX - hitRadiusRight
    ) {
      result.Border = 1
    }
  }

  if (0 === valueOfCurCell && x <= cellStartX) {
    result.selectRow = true
    result.Border = -1
  } else if (0 === valueOfCurRow && y <= cellStartY + hitRadius) {
    result.selectColum = true
    result.Border = -1
  } else if (cellStartX + hitRadius <= x && x <= cellEndX - hitRadius) {
    if (table.isRTL) {
      if (x >= cellEndX - right!.w + 0.001) {
        result.selectCell = true
        result.Border = -1
      }
    } else {
      if (x <= cellStartX + left!.w - 0.001) {
        result.selectCell = true
        result.Border = -1
      }
    }
  }

  return result
}

export function getMergedCells(
  table: Table,
  rowIndex: number,
  startGridCol: number,
  gridSpan: number
) {
  const cellIndex = table.getCellIndexByGridColIndex(rowIndex, startGridCol)
  if (-1 === cellIndex) return []

  const row = table.children.at(rowIndex)!

  const cell = row.getCellByIndex(cellIndex)!
  if (gridSpan !== cell.getGridSpan()) return []

  const cells = [cell]

  for (let i = rowIndex - 1; i >= 0; i--) {
    const cellIndex = table.getCellIndexByGridColIndex(i, startGridCol)
    if (-1 === cellIndex) break

    const cell = table.children.at(i)!.getCellByIndex(cellIndex)!
    if (gridSpan !== cell.getGridSpan()) break

    const vMerge = cell.getVMerge()
    if (VMergeType.Continue !== vMerge) break

    cells.unshift(cell)
  }

  for (let i = rowIndex + 1, l = table.children.length; i < l; i++) {
    const cellIndex = table.getCellIndexByGridColIndex(i, startGridCol)
    if (-1 === cellIndex) break

    const cell = table.children.at(i)!.getCellByIndex(cellIndex)!
    if (gridSpan !== cell.getGridSpan()) break

    const vMerge = cell.getVMerge()
    if (VMergeType.Continue !== vMerge) break

    cells.push(cell)
  }

  return cells
}

export function getArrOfRowInfos(table: Table) {
  const rowsInfo: Array<RowInfo[]> = []

  for (let ri = 0, rc = table.children.length; ri < rc; ++ri) {
    rowsInfo.push([])

    const row = table.children.at(ri)!
    for (let ci = 0, cc = row.cellsCount; ci < cc; ++ci) {
      const cell = row.getCellByIndex(ci)!
      const cellInfo = row.getCellInfo(ci)

      if (!cellInfo) {
        rowsInfo[ri].push({
          w: 0,
          type: 0,
          gridSpan: 1,
        })
      } else {
        const valueOfCurGridStart = cellInfo.startGridCol
        const valueOfCurGridEnd = valueOfCurGridStart + cell.getGridSpan() - 1

        rowsInfo[ri].push({
          w:
            table.tableGridReduced[valueOfCurGridEnd] -
            table.tableGridReduced[valueOfCurGridStart - 1],
          type: 0,
          gridSpan: 1,
        })
      }
    }
  }

  return rowsInfo
}

export function correctVMergeForTable(table: Table) {
  for (let ri = 0, rc = table.children.length; ri < rc; ++ri) {
    const row = table.children.at(ri)!
    let valueOfGridCol = 0
    for (let ci = 0, cc = row.cellsCount; ci < cc; ++ci) {
      const cell = row.getCellByIndex(ci)!
      const valueOfVMergeType = cell.getVMerge()
      const valueOfGridSpan = cell.getGridSpan()

      if (VMergeType.Continue === valueOfVMergeType) {
        let isNeedReset = true
        if (ri > 0) {
          const prevRow = table.children.at(ri - 1)!
          let valueOfPrevGridCol = 0
          for (
            let valueOfPrevCell = 0, valueOfPrevCellsCount = prevRow.cellsCount;
            valueOfPrevCell < valueOfPrevCellsCount;
            ++valueOfPrevCell
          ) {
            const prevCell = prevRow.getCellByIndex(valueOfPrevCell)!
            const valueOfPrevGridSpan = prevCell.getGridSpan()

            if (valueOfPrevGridCol === valueOfGridCol) {
              if (valueOfPrevGridSpan === valueOfGridSpan) isNeedReset = false

              break
            } else if (valueOfPrevGridCol > valueOfGridCol) break

            valueOfPrevGridCol += valueOfPrevGridSpan
          }
        }

        if (true === isNeedReset) cell.setVMerge(VMergeType.Restart)
      }

      valueOfGridCol += valueOfGridSpan
    }
  }
}
