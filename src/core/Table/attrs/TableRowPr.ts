import { LineHeightRule } from '../../common/const/attrs'
import { TableRowHeight } from './TableRowHeight'
import { isObjectsTheSame } from '../../common/helpers'
import { assignJSON, readFromJSON } from '../../../io/utils'
import { CTableRowPrData } from '../../../io/dataType/style'
import { Nullable } from '../../../../liber/pervasive'
export interface TableRowPrOptions {
  height?: TableRowHeight
}
export class TableRowPr {
  height?: TableRowHeight
  static new(opts?: TableRowPrOptions) {
    const ret = new TableRowPr()
    if (opts != null) ret.fromOptions(opts)
    return ret
  }
  constructor() {
    this.height = undefined
  }
  reset() {
    this.height = undefined
  }

  clone() {
    return TableRowPr.new(this)
  }

  merge(rowPr: Nullable<TableRowPrOptions>) {
    if (null != rowPr?.height) this.height = rowPr.height
  }

  sameAs(rowPr: TableRowPrOptions) {
    if (true !== isObjectsTheSame(this.height, rowPr.height)) {
      return false
    }

    return true
  }

  setDefault() {
    this.height = new TableRowHeight(0, LineHeightRule.Auto)
  }

  fromOptions(opts: Nullable<TableRowPrOptions>) {
    if (opts == null) return
    if (null != opts.height) {
      this.height = opts.height
    }
  }

  toJSON(): CTableRowPrData {
    const data: CTableRowPrData = {}
    assignJSON(data, 'height', this.height)
    return data
  }

  fromJSON(data: CTableRowPrData) {
    if (data['height'] != null) {
      this.height = readFromJSON(data['height'], TableRowHeight, [
        0,
        LineHeightRule.Auto,
      ])
    }
  }
}
