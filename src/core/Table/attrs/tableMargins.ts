import { Nullable } from '../../../../liber/pervasive'
import { fromOptions, isObjectsTheSame } from '../../common/helpers'
import { TableLength, TableLengthOptions } from './TableLength'

export interface TableCellMargins {
  bottom?: TableLength
  left?: TableLength
  right?: TableLength
  top?: TableLength
}

export interface TableCellMarginsOptions {
  bottom?: TableLengthOptions
  left?: TableLengthOptions
  right?: TableLengthOptions
  top?: TableLengthOptions
}

export function setTCMarginsOptions(
  margins: TableCellMargins | undefined,
  opts: Nullable<TableCellMarginsOptions>
): TableCellMargins | undefined {
  if (opts == null) return margins

  margins = margins ?? {}

  margins.bottom = fromOptions(margins.bottom, opts.bottom, TableLength)
  margins.left = fromOptions(margins.left, opts.left, TableLength)
  margins.right = fromOptions(margins.right, opts.right, TableLength)
  margins.top = fromOptions(margins.top, opts.top, TableLength)

  return margins
}

export function isTCMarginsTheSame(
  margins1: Nullable<TableCellMargins>,
  margins2: Nullable<TableCellMarginsOptions>
) {
  if (margins1 == null && margins2 == null) return true
  if (margins1 == null || margins2 == null) return false
  return (
    isObjectsTheSame(margins1.bottom, margins2.bottom) &&
    isObjectsTheSame(margins1.top, margins2.top) &&
    isObjectsTheSame(margins1.left, margins2.left) &&
    isObjectsTheSame(margins1.right, margins2.right)
  )
}
