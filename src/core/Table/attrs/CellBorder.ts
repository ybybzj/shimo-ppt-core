import { CHexColor, CHexColorOptions } from '../../color/CHexColor'
import { Factor_pt_to_mm, ScaleOfPPTXSizes } from '../../common/const/unit'

import { isNumber } from '../../../common/utils'
import { FillKIND } from '../../common/const/attrs'
import { fromOptions, isObjectsTheSame } from '../../common/helpers'
import { assignJSON, readFromJSON } from '../../../io/utils'
import { CTableCellBorderData } from '../../../io/dataType/style'
import {
  assignLnByBorderPr,
  applyLnToBorderPr,
} from '../../textAttributes/utils'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { StyleRef } from '../../SlideElement/attrs/refs'
import { Theme } from '../../SlideElement/attrs/theme'
import { ClrMap } from '../../color/clrMap'
import { Nullable } from '../../../../liber/pervasive'
import { PoolAllocator, Recyclable } from '../../../common/allocator'
import { ColorRGBA } from '../../color/type'
import { defaultRGBAColor, updateRGBA } from '../../color/complexColor'
import { TCBorderValue, TCBorderValues } from '../../common/const/table'

export interface TableCellBorderOptions {
  color?: CHexColorOptions
  fillEffects?: FillEffects
  lineRef?: StyleRef
  space?: number
  size?: number
  value?: TCBorderValues
  useTableBorder?: boolean
}

export interface TableCellBordersInfo {
  bottom?: TableCellBorderOptions
  left?: TableCellBorderOptions
  right?: TableCellBorderOptions
  top?: TableCellBorderOptions
}

export interface TableCellBorders {
  bottom?: TableCellBorder
  left?: TableCellBorder
  right?: TableCellBorder
  top?: TableCellBorder
}

export class TableCellBorder extends Recyclable<TableCellBorder> {
  color: CHexColor
  fillEffects?: FillEffects
  lineRef?: StyleRef
  space: number
  size: number
  value: TCBorderValues
  useTableBorder: boolean
  private static allocator: PoolAllocator<TableCellBorder, []> =
    PoolAllocator.getAllocator<TableCellBorder, []>(TableCellBorder)
  static new(opts?: Nullable<TableCellBorderOptions>) {
    const ret = TableCellBorder.allocator.allocate()
    if (opts != null) ret.fromOptions(opts)
    return ret
  }
  static recycle(tcBorder: TableCellBorder) {
    TableCellBorder.allocator.recycle(tcBorder)
  }

  constructor() {
    super()
    this.color = new CHexColor(0, 0, 0)
    this.fillEffects = undefined
    this.lineRef = undefined
    this.space = 0
    this.size = 0.5 * Factor_pt_to_mm
    this.value = TCBorderValue.None
    this.useTableBorder = false
  }

  reset() {
    this.color.set(0, 0, 0)
    this.fillEffects = undefined
    this.lineRef?.reset()
    this.space = 0
    this.size = 0.5 * Factor_pt_to_mm
    this.value = TCBorderValue.None
    this.useTableBorder = false
  }

  clone() {
    return TableCellBorder.new(this)
  }

  sameAs(border: TableCellBorderOptions) {
    if (
      true !== isObjectsTheSame(this.color, border.color) ||
      true !== isObjectsTheSame(this.fillEffects, border.fillEffects) ||
      this.space !== border.space ||
      this.size !== border.size ||
      this.value !== border.value
    ) {
      return false
    }

    return true
  }

  getColorByTheme(
    theme: Nullable<Theme>,
    colorMap: Nullable<ClrMap>,
    rgba?: ColorRGBA
  ) {
    rgba = rgba ?? defaultRGBAColor()
    if (null != this.fillEffects) {
      this.fillEffects.check(theme, colorMap)
      const _rgba = this.fillEffects.getRGBAColor(rgba)
      return _rgba
    } else {
      updateRGBA(rgba, this.color!)
      return rgba
    }
  }

  refreshTheme(theme: Nullable<Theme>) {
    if (this.lineRef && theme) {
      const pen = theme.getLnStyle(this.lineRef.idx, this.lineRef.color)

      this.fillEffects = pen.fillEffects
      this.lineRef = undefined
      this.size = isNumber(pen.w)
        ? pen.w / ScaleOfPPTXSizes
        : 12700 / ScaleOfPPTXSizes
    }
    if (
      !this.fillEffects ||
      !this.fillEffects.fill ||
      this.fillEffects.fill.type === FillKIND.NOFILL
    ) {
      this.value = TCBorderValue.None
    }
  }

  fromOptions(opts: Nullable<TableCellBorderOptions>) {
    if (opts == null) return
    if (opts.color != null) {
      const { r, g, b, auto } = opts.color
      this.color.set(r, g, b, auto)
    }
    if (opts.fillEffects != null) this.fillEffects = opts.fillEffects.clone()
    if (opts.lineRef != null) this.lineRef = opts.lineRef
    if (opts.space != null) this.space = opts.space
    if (opts.size != null) this.size = opts.size
    if (opts.value != null) this.value = opts.value
    if (opts.useTableBorder != null) this.useTableBorder = opts.useTableBorder
  }

  toJSON(): CTableCellBorderData {
    const data: CTableCellBorderData = {}
    assignLnByBorderPr(data, 'ln', this)

    assignJSON(data, 'lineRef', this.lineRef)

    return data
  }

  fromJSON(data: CTableCellBorderData) {
    applyLnToBorderPr(data['ln'], this)
    if (data['lineRef']) {
      this.lineRef = readFromJSON(data['lineRef'], StyleRef)
      this.value = TCBorderValue.Single
    }
  }

  getWidth() {
    if (TCBorderValue.None === this.value) return 0

    return this.size
  }
}
export function mergeTableCellBorders(
  selfTableCellBorders: TableCellBorders,
  optsBordersInfo: Nullable<TableCellBordersInfo>
) {
  if (optsBordersInfo != null) {
    selfTableCellBorders.bottom = fromOptions(
      selfTableCellBorders.bottom,
      optsBordersInfo.bottom,
      TableCellBorder
    )

    selfTableCellBorders.top = fromOptions(
      selfTableCellBorders.top,
      optsBordersInfo.top,
      TableCellBorder
    )
    selfTableCellBorders.left = fromOptions(
      selfTableCellBorders.left,
      optsBordersInfo.left,
      TableCellBorder
    )
    selfTableCellBorders.right = fromOptions(
      selfTableCellBorders.right,
      optsBordersInfo.right,
      TableCellBorder
    )
  }
}

export function cloneTableCellBorders(
  borders: TableCellBorders
): TableCellBorders {
  const ret: TableCellBorders = {}

  if (borders.top) {
    ret.top = borders.top.clone()
  }
  if (borders.bottom) {
    ret.top = borders.bottom.clone()
  }
  if (borders.left) {
    ret.top = borders.left.clone()
  }
  if (borders.right) {
    ret.top = borders.right.clone()
  }

  return ret
}
