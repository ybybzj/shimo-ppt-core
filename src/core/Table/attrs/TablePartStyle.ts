import { TextPr, TextPrOptions } from '../../textAttributes/TextPr'
import { ParaPr, ParaPrOptions } from '../../textAttributes/ParaPr'
import { TablePr, TablePrOptions } from './TablePr'
import { TableRowPr, TableRowPrOptions } from './TableRowPr'
import { TableCellPr, TableCellPrOptions } from './TableCellPr'
import {
  readFromJSON,
  assignVal,
  assignJSON,
  isEmptyPropValue,
} from '../../../io/utils'
import {
  CTableStylePrData,
  TableCellTextStyle,
  TableCellStyle,
  toOnOffStyleType,
  TextPrFont,
  TableCellBorderStyle,
  fromOnOffStyleType,
} from '../../../io/dataType/style'
import { FillKIND, ShdType } from '../../common/const/attrs'

import { TableCellBorder } from './CellBorder'
import { CShd } from './CShd'
import { CComplexColor } from '../../color/complexColor'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { FontRef, StyleRef } from '../../SlideElement/attrs/refs'
import { SolidFill } from '../../SlideElement/attrs/fill/SolidFill'
import { isObjectsTheSame } from '../../common/helpers'
import { Nullable } from '../../../../liber/pervasive'
import { Theme } from '../../SlideElement/attrs/theme'
import { PoolAllocator, Recyclable } from '../../../common/allocator'
export interface TablePartStyleOptions {
  textPr?: TextPrOptions
  paraPr?: ParaPrOptions
  tablePr?: TablePrOptions
  tableRowPr?: TableRowPrOptions
  tableCellPr?: TableCellPrOptions
}
export class TablePartStyle extends Recyclable<TablePartStyle> {
  textPr: TextPr
  paraPr: ParaPr
  tablePr: TablePr
  tableRowPr: TableRowPr
  tableCellPr: TableCellPr
  private static allocator: PoolAllocator<TablePartStyle, []> =
    PoolAllocator.getAllocator<TablePartStyle, []>(TablePartStyle)

  static new(opts?: TablePartStyleOptions) {
    const ret = TablePartStyle.allocator.allocate()
    if (opts != null) ret.fromOptions(opts)
    return ret
  }

  static recycle(tps: TablePartStyle) {
    TablePartStyle.allocator.recycle(tps)
  }

  constructor() {
    super()
    this.textPr = TextPr.new()
    this.paraPr = ParaPr.new()
    this.tablePr = TablePr.new()
    this.tableRowPr = new TableRowPr()
    this.tableCellPr = TableCellPr.new()
  }

  reset() {
    this.textPr.reset()
    this.paraPr.reset()
    this.tablePr.reset()
    this.tableRowPr.reset()
    this.tableCellPr.reset()
  }

  merge(opts: TablePartStyleOptions) {
    this.textPr.fromOptions(opts.textPr)
    this.paraPr.fromOptions(opts.paraPr)
    this.tablePr.fromOptions(opts.tablePr)
    this.tableRowPr.fromOptions(opts.tableRowPr)
    this.tableCellPr.fromOptions(opts.tableCellPr)
  }

  clone() {
    const tableStylePr = TablePartStyle.new()
    tableStylePr.textPr = this.textPr.clone()
    tableStylePr.paraPr = this.paraPr.clone()
    tableStylePr.tablePr = this.tablePr.clone()
    tableStylePr.tableRowPr = this.tableRowPr.clone()
    tableStylePr.tableCellPr = this.tableCellPr.clone()
    return tableStylePr
  }

  sameAs(pr: TablePartStyleOptions) {
    return (
      pr != null &&
      isObjectsTheSame(this.textPr, pr.textPr) &&
      isObjectsTheSame(this.paraPr, pr.paraPr) &&
      isObjectsTheSame(this.tablePr, pr.tablePr) &&
      isObjectsTheSame(this.tableRowPr, pr.tableRowPr) &&
      isObjectsTheSame(this.tableCellPr, pr.tableCellPr)
    )
  }

  refreshTheme(theme: Nullable<Theme>) {
    this.textPr.refreshTheme()
    this.tableCellPr.refreshTheme(theme)
  }

  fromOptions(opts: Nullable<TablePartStyleOptions>) {
    if (opts == null) return

    if (null != opts.textPr) this.textPr.fromOptions(opts.textPr)

    if (null != opts.paraPr) this.paraPr.fromOptions(opts.paraPr)

    if (null != opts.tablePr) this.tablePr.fromOptions(opts.tablePr)

    if (null != opts.tableRowPr) {
      this.tableRowPr.fromOptions(opts.tableRowPr)
    }

    if (null != opts.tableCellPr) {
      this.tableCellPr.fromOptions(opts.tableCellPr)
    }
  }

  toJSON(tablePr?: TablePr): CTableStylePrData {
    const data: CTableStylePrData = {}
    assignVal(data, 'txStyle', this.toTXStyleData())
    assignVal(data, 'cellStyle', this.toCellStyleData(tablePr))
    return data
  }
  private toTXStyleData(): TableCellTextStyle {
    const data: TableCellTextStyle = {}
    const textPr = this.textPr
    if (textPr?.bold != null) {
      assignVal(data, 'bold', toOnOffStyleType(textPr?.bold))
    }

    if (textPr?.italic != null) {
      assignVal(data, 'italic', toOnOffStyleType(textPr?.italic))
    }

    if (textPr?.fontRef != null) {
      assignJSON(data, 'fontRef', textPr.fontRef)
    }

    if (textPr?.fillEffects?.fill?.type === FillKIND.SOLID) {
      const ccolor = (textPr.fillEffects.fill as SolidFill).color
      assignJSON(data, 'color', ccolor)
    }

    if (textPr?.rFonts != null) {
      if (textPr.rFonts.ascii) {
        assignVal(data, 'latin', {
          ['typeface']: textPr.rFonts.ascii.name,
        } as TextPrFont)
      }
      if (textPr.rFonts.eastAsia) {
        assignVal(data, 'ea', {
          ['typeface']: textPr.rFonts.eastAsia.name,
        } as TextPrFont)
      }
      if (textPr.rFonts.cs) {
        assignVal(data, 'cs', {
          ['typeface']: textPr.rFonts.cs.name,
        } as TextPrFont)
      }
    }
    return data
  }
  private toCellStyleData(tablePr?: TablePr): TableCellStyle {
    const data: TableCellStyle = {}
    const shd = this.tableCellPr?.shd
    if (shd != null) {
      if (shd.fillRef != null) {
        assignJSON(data, 'fillRef', shd.fillRef)
      }
      if (shd.fillEffects != null) {
        assignJSON(data, 'fill', shd.fillEffects)
      }
    }

    const borders = this.toBorderStyleData(tablePr)
    if (!isEmptyPropValue(borders)) {
      assignVal(data, 'borders', borders)
    }

    return data
  }

  private toBorderStyleData(tablePr?: TablePr): TableCellBorderStyle {
    const data: TableCellBorderStyle = {}
    const borderPr: {
      left?: TableCellBorder
      right?: TableCellBorder
      top?: TableCellBorder
      bottom?: TableCellBorder
      insideH?: TableCellBorder
      insideV?: TableCellBorder
    } = tablePr?.tableBorders ?? this.tableCellPr?.tableCellBorders

    if (!tablePr && !borderPr.insideH && this.tablePr?.tableBorders?.insideH) {
      borderPr.insideH = this.tablePr?.tableBorders?.insideH
    }
    if (!tablePr && !borderPr.insideV && this.tablePr?.tableBorders?.insideV) {
      borderPr.insideV = this.tablePr?.tableBorders?.insideV
    }

    assignJSON(data, 'left', borderPr.left)
    assignJSON(data, 'right', borderPr.right)
    assignJSON(data, 'top', borderPr.top)
    assignJSON(data, 'bottom', borderPr.bottom)
    assignJSON(data, 'insideH', borderPr.insideH)
    assignJSON(data, 'insideV', borderPr.insideV)

    return data
  }
  fromJSON(data: CTableStylePrData) {
    if (data['txStyle'] != null) {
      this.fromTXStyleData(data['txStyle'])
    }

    if (data['cellStyle'] != null) {
      this.fromCellStyleData(data['cellStyle'])
    }
  }

  private fromTXStyleData(data: TableCellTextStyle) {
    const textPr = this.textPr
    if (data['bold'] != null) {
      textPr.bold = fromOnOffStyleType(data['bold'])
    }
    if (data['italic'] != null) {
      textPr.italic = fromOnOffStyleType(data['italic'])
    }

    if (data['fontRef'] != null) {
      textPr.fontRef = readFromJSON(data['fontRef'], FontRef)
    }

    if (data['color'] != null) {
      const ccolor = readFromJSON(data['color'], CComplexColor)
      textPr.fillEffects = FillEffects.SolidCColor(ccolor)
    }

    if (!isEmptyPropValue(data['latin'])) {
      textPr.rFonts.ascii = {
        name: data['latin']!['typeface'] || '',
        index: -1,
      }
      textPr.rFonts.hAnsi = { name: textPr.rFonts.ascii!.name, index: -1 }
    }

    if (!isEmptyPropValue(data['ea'])) {
      textPr.rFonts.eastAsia = {
        name: data['ea']!['typeface'] || '',
        index: -1,
      }
    }

    if (!isEmptyPropValue(data['cs'])) {
      textPr.rFonts.cs = { name: data['cs']!['typeface'] || '', index: -1 }
    }
  }
  private fromCellStyleData(data: TableCellStyle) {
    if (!isEmptyPropValue(data['fillRef'])) {
      this.tableCellPr.shd = this.tableCellPr.shd ?? CShd.new()
      this.tableCellPr.shd.value = ShdType.Clear
      this.tableCellPr.shd.fillRef = readFromJSON(data['fillRef'], StyleRef)
    } else if (!isEmptyPropValue(data['fill'])) {
      this.tableCellPr.shd = this.tableCellPr.shd ?? CShd.new()
      this.tableCellPr.shd.value = ShdType.Clear
      this.tableCellPr.shd.fillEffects = readFromJSON(data['fill'], FillEffects)
    }

    if (data['borders'] != null) {
      this.fromBorderStyleData(data['borders'])
    }
  }
  private fromBorderStyleData(data: TableCellBorderStyle) {
    if (data['left'] != null) {
      this.tableCellPr.tableCellBorders.left = readFromJSON(
        data['left'],
        TableCellBorder
      )
    }
    if (data['right'] != null) {
      this.tableCellPr.tableCellBorders.right = readFromJSON(
        data['right'],
        TableCellBorder
      )
    }
    if (data['top'] != null) {
      this.tableCellPr.tableCellBorders.top = readFromJSON(
        data['top'],
        TableCellBorder
      )
    }
    if (data['bottom'] != null) {
      this.tableCellPr.tableCellBorders.bottom = readFromJSON(
        data['bottom'],
        TableCellBorder
      )
    }

    if (data['insideH'] != null) {
      this.tablePr.tableBorders.insideH = readFromJSON(
        data['insideH'],
        TableCellBorder
      )
    }

    if (data['insideV'] != null) {
      this.tablePr.tableBorders.insideV = readFromJSON(
        data['insideV'],
        TableCellBorder
      )
    }
  }

  setDefault() {
    this.textPr.setDefault()
    this.paraPr.setDefault()
    this.tablePr.setDefault()
    this.tableRowPr.setDefault()
    this.tableCellPr.setDefault()
  }
}
