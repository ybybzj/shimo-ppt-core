import { CShd, CShdOptions } from './CShd'
import { ScaleOfPPTXSizes } from '../../common/const/unit'
import { TableLength } from './TableLength'
import {
  TableCellBorder,
  TableCellBorders,
  TableCellBordersInfo,
  mergeTableCellBorders,
} from './CellBorder'
import { fromOptions, isObjectsTheSame } from '../../common/helpers'
import { Nullable, valuesOfDict } from '../../../../liber/pervasive'
import {
  assignVal,
  assignJSON,
  readFromJSON,
  isEmptyPropValue,
  scaleValue,
} from '../../../io/utils'
import { CTableCellPrData } from '../../../io/dataType/style'
import { isNumber } from '../../../common/utils'
import { bodyPrAnchor, ShdType, VertAlignJc } from '../../common/const/attrs'
import {
  toTextAnchorType,
  toTextVerticalType,
  fromTextVerticalType,
  fromTextAnchorType,
} from '../../../io/dataType/spAttrs'
import { TextVerticalType } from '../../SlideElement/const'
import {
  assignLnByBorderPr,
  applyLnToBorderPr,
} from '../../textAttributes/utils'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { StyleRef } from '../../SlideElement/attrs/refs'
import {
  isTCMarginsTheSame,
  setTCMarginsOptions,
  TableCellMargins,
  TableCellMarginsOptions,
} from './tableMargins'
import { TablePr } from './TablePr'
import { Theme } from '../../SlideElement/attrs/theme'
import { PoolAllocator, Recyclable } from '../../../common/allocator'
import {
  CellTextDirection,
  TableLengthUnitType,
  TableTextDirection,
  TableTextDirectionValues,
  VMergeType,
  VMergeTypeValues,
} from '../../common/const/table'

export interface TableCellPrOptions {
  gridSpan?: number
  shd?: CShdOptions
  margins?: Nullable<TableCellMarginsOptions>
  tableCellBorders?: TableCellBordersInfo
  vAlign?: Nullable<valuesOfDict<typeof VertAlignJc>>
  vMerge?: VMergeTypeValues
  textDirection?: TableTextDirectionValues
}
export class TableCellPr extends Recyclable<TableCellPr> {
  gridSpan?: number
  shd?: CShd
  margins?: TableCellMargins
  tableCellBorders: TableCellBorders
  vAlign?: Nullable<valuesOfDict<typeof VertAlignJc>>
  vMerge?: VMergeTypeValues
  textDirection?: TableTextDirectionValues
  private static allocator: PoolAllocator<TableCellPr, []> =
    PoolAllocator.getAllocator<TableCellPr, []>(TableCellPr)
  static new(opts?: TableCellPrOptions) {
    const ret = TableCellPr.allocator.allocate() as TableCellPr
    if (opts != null) ret.fromOptions(opts)
    return ret
  }
  static recycle(tcPr: TableCellPr) {
    TableCellPr.allocator.recycle(tcPr)
  }
  constructor() {
    super()
    this.gridSpan = undefined
    this.shd = undefined
    this.margins = undefined
    this.tableCellBorders = {
      bottom: undefined,
      left: undefined,
      right: undefined,
      top: undefined,
    }
    this.vAlign = undefined
    this.vMerge = undefined
    this.textDirection = undefined
  }

  reset() {
    this.gridSpan = undefined
    this.shd = undefined
    this.margins = undefined
    if (this.tableCellBorders.bottom != null) {
      TableCellBorder.recycle(this.tableCellBorders.bottom)
      this.tableCellBorders.bottom = undefined
    }
    if (this.tableCellBorders.top != null) {
      TableCellBorder.recycle(this.tableCellBorders.top)
      this.tableCellBorders.top = undefined
    }
    if (this.tableCellBorders.right != null) {
      TableCellBorder.recycle(this.tableCellBorders.right)
      this.tableCellBorders.right = undefined
    }
    if (this.tableCellBorders.left != null) {
      TableCellBorder.recycle(this.tableCellBorders.left)
      this.tableCellBorders.left = undefined
    }

    this.vAlign = undefined
    this.vMerge = undefined
    this.textDirection = undefined
  }

  clone() {
    return TableCellPr.new(this)
  }

  merge(cellPr: TableCellPrOptions) {
    this.fromOptions(cellPr)
  }

  sameAs(cellPr: TableCellPrOptions) {
    if (
      this.gridSpan !== cellPr.gridSpan ||
      true !== isObjectsTheSame(this.shd, cellPr.shd) ||
      !isTCMarginsTheSame(this.margins, cellPr.margins) ||
      true !==
        isObjectsTheSame(
          this.tableCellBorders.bottom,
          cellPr.tableCellBorders?.bottom
        ) ||
      true !==
        isObjectsTheSame(
          this.tableCellBorders.left,
          cellPr.tableCellBorders?.left
        ) ||
      true !==
        isObjectsTheSame(
          this.tableCellBorders.right,
          cellPr.tableCellBorders?.right
        ) ||
      true !==
        isObjectsTheSame(
          this.tableCellBorders.top,
          cellPr.tableCellBorders?.top
        ) ||
      this.vAlign !== cellPr.vAlign ||
      this.vMerge !== cellPr.vMerge ||
      this.textDirection !== cellPr.textDirection
    ) {
      return false
    }

    return true
  }

  setDefault() {
    this.gridSpan = 1
    this.shd = CShd.Nil()
    this.margins = undefined
    this.tableCellBorders.bottom = undefined
    this.tableCellBorders.left = undefined
    this.tableCellBorders.right = undefined
    this.tableCellBorders.top = undefined
    this.vAlign = VertAlignJc.Top
    this.vMerge = VMergeType.Restart
    this.textDirection = TableTextDirection.LRTB
  }

  fromOptions(opts: Nullable<TableCellPrOptions>) {
    if (opts == null) return

    if (opts.gridSpan != null) {
      this.gridSpan = opts.gridSpan
    }

    this.shd = fromOptions(this.shd, opts.shd, CShd)

    if (opts.margins === null) {
      this.margins = undefined
    } else {
      this.margins = setTCMarginsOptions(this.margins, opts.margins)
    }

    if (opts.tableCellBorders != null) {
      mergeTableCellBorders(this.tableCellBorders, opts.tableCellBorders)
    }

    this.vAlign = opts.vAlign ?? this.vAlign
    this.vMerge = opts.vMerge ?? this.vMerge
    this.textDirection = opts.textDirection ?? this.textDirection
  }

  refreshTheme(theme: Nullable<Theme>) {
    if (this.shd) {
      this.shd.refreshTheme(theme)
    }
    if (this.tableCellBorders.bottom) {
      this.tableCellBorders.bottom.refreshTheme(theme)
    }
    if (this.tableCellBorders.left) {
      this.tableCellBorders.left.refreshTheme(theme)
    }
    if (this.tableCellBorders.right) {
      this.tableCellBorders.right.refreshTheme(theme)
    }
    if (this.tableCellBorders.top) {
      this.tableCellBorders.top.refreshTheme(theme)
    }
  }
  toJSON(tablePr?: TablePr): CTableCellPrData {
    const data: CTableCellPrData = {}
    // margins
    const cellMargin = this.margins
    const margins = cellMargin ?? tablePr?.tableCellMargins
    if (margins?.left?.w != null) {
      assignVal(data, 'marL', scaleValue(margins.left.w, ScaleOfPPTXSizes))
    }
    if (margins?.top?.w != null) {
      assignVal(data, 'marT', scaleValue(margins.top.w, ScaleOfPPTXSizes))
    }
    if (margins?.right?.w != null) {
      assignVal(data, 'marR', scaleValue(margins.right.w, ScaleOfPPTXSizes))
    }
    if (margins?.bottom?.w != null) {
      assignVal(data, 'marB', scaleValue(margins.bottom.w, ScaleOfPPTXSizes))
    }

    // fill
    const shd = this.shd
    if (shd != null) {
      if (shd.fillRef != null) {
        assignJSON(data, 'fillRef', shd.fillRef)
      }
      if (shd.value !== ShdType.Nil && shd.fillEffects != null) {
        assignJSON(data, 'fill', shd.fillEffects)
      } else {
        const fill = FillEffects.NoFill()
        assignJSON(data, 'fill', fill)
      }
    }

    //vert
    if (isNumber(this.textDirection)) {
      switch (this.textDirection) {
        case CellTextDirection.LRTB: {
          assignVal(data, 'vert', toTextVerticalType(TextVerticalType.horz))
          break
        }
        case CellTextDirection.TBRL: {
          assignVal(data, 'vert', toTextVerticalType(TextVerticalType.eaVert))
          break
        }
        case CellTextDirection.BTLR: {
          assignVal(data, 'vert', toTextVerticalType(TextVerticalType.vert270))
          break
        }
        default: {
          assignVal(data, 'vert', toTextVerticalType(TextVerticalType.horz))
        }
      }
    }

    // anchor
    if (isNumber(this.vAlign)) {
      switch (this.vAlign) {
        case VertAlignJc.Bottom: {
          assignVal(data, 'anchor', toTextAnchorType(bodyPrAnchor.b))
          break
        }
        case VertAlignJc.Center: {
          assignVal(data, 'anchor', toTextAnchorType(bodyPrAnchor.ctr))
          break
        }
        case VertAlignJc.Top: {
          assignVal(data, 'anchor', toTextAnchorType(bodyPrAnchor.t))
          break
        }
      }
    }

    // borders
    if (this.tableCellBorders.left != null) {
      assignLnByBorderPr(data, 'lnL', this.tableCellBorders.left)
    }

    if (this.tableCellBorders.right != null) {
      assignLnByBorderPr(data, 'lnR', this.tableCellBorders.right)
    }
    if (this.tableCellBorders.top != null) {
      assignLnByBorderPr(data, 'lnT', this.tableCellBorders.top)
    }
    if (this.tableCellBorders.bottom != null) {
      assignLnByBorderPr(data, 'lnB', this.tableCellBorders.bottom)
    }
    return data
  }

  fromJSON(data: CTableCellPrData) {
    // margins
    const marL = data['marL']
    const marR = data['marR']
    const marT = data['marT']
    const marB = data['marB']

    if (marL != null || marR != null || marT != null || marB != null) {
      this.margins = this.margins ?? {}
    }

    if (marL != null) {
      this.margins!.left = new TableLength(
        TableLengthUnitType.MM,
        marL / ScaleOfPPTXSizes
      )
    }

    if (marR != null) {
      this.margins!.right = new TableLength(
        TableLengthUnitType.MM,
        marR / ScaleOfPPTXSizes
      )
    }

    if (marT != null) {
      this.margins!.top = new TableLength(
        TableLengthUnitType.MM,
        marT / ScaleOfPPTXSizes
      )
    }

    if (marB != null) {
      this.margins!.bottom = new TableLength(
        TableLengthUnitType.MM,
        marB / ScaleOfPPTXSizes
      )
    }

    // vert

    if (data['vert'] != null) {
      const valueOfVert = fromTextVerticalType(data['vert'])
      switch (valueOfVert) {
        case TextVerticalType.eaVert:
          this.textDirection = CellTextDirection.TBRL
          break
        case TextVerticalType.horz:
          this.textDirection = CellTextDirection.LRTB
          break
        case TextVerticalType.mongolianVert:
          this.textDirection = CellTextDirection.TBRL
          break
        case TextVerticalType.vert:
          this.textDirection = CellTextDirection.TBRL
          break
        case TextVerticalType.vert270:
          this.textDirection = CellTextDirection.BTLR
          break
        case TextVerticalType.wordArtVert:
          this.textDirection = CellTextDirection.BTLR
          break
        case TextVerticalType.wordArtVertRtl:
          this.textDirection = CellTextDirection.TBRL
          break
        default:
          this.textDirection = CellTextDirection.LRTB
          break
      }
    }

    // anchor
    if (data['anchor'] != null) {
      const valueOfVertAlign = fromTextAnchorType(data['anchor'])
      switch (valueOfVertAlign) {
        case bodyPrAnchor.b: {
          //bottom
          this.vAlign = VertAlignJc.Bottom
          break
        }
        case bodyPrAnchor.ctr:
        case bodyPrAnchor.just:
        case bodyPrAnchor.dist: {
          this.vAlign = VertAlignJc.Center
          break
        }
        case bodyPrAnchor.t: {
          //top
          this.vAlign = VertAlignJc.Top
          break
        }
      }
    }

    // borders

    const lnL = data['lnL']
    const lnR = data['lnR']
    const lnT = data['lnT']
    const lnB = data['lnB']

    if (lnL != null || lnR != null || lnT != null || lnB != null) {
      this.tableCellBorders = this.tableCellBorders ?? {}
    }

    if (lnL != null) {
      this.tableCellBorders.left = TableCellBorder.new()
      applyLnToBorderPr(lnL, this.tableCellBorders.left)
    }

    if (lnR != null) {
      this.tableCellBorders.right = TableCellBorder.new()
      applyLnToBorderPr(lnR, this.tableCellBorders.right)
    }

    if (lnT != null) {
      this.tableCellBorders.top = TableCellBorder.new()
      applyLnToBorderPr(lnT, this.tableCellBorders.top)
    }

    if (lnB != null) {
      this.tableCellBorders.bottom = TableCellBorder.new()
      applyLnToBorderPr(lnB, this.tableCellBorders.bottom)
    }

    // fill
    if (!isEmptyPropValue(data['fillRef'])) {
      this.shd = this.shd ?? CShd.new()
      this.shd.value = ShdType.Clear
      this.shd.fillRef = readFromJSON(data['fillRef'], StyleRef)
    } else if (!isEmptyPropValue(data['fill'])) {
      this.shd = this.shd ?? CShd.new()
      this.shd.value = ShdType.Clear
      this.shd.fillEffects = readFromJSON(data['fill'], FillEffects)
    }
  }

  isEmpty() {
    if (
      null != this.gridSpan ||
      null != this.shd ||
      null != this.margins ||
      null != this.tableCellBorders.bottom ||
      null != this.tableCellBorders.left ||
      null != this.tableCellBorders.right ||
      null != this.tableCellBorders.top ||
      null != this.vAlign ||
      null != this.vMerge ||
      null != this.textDirection
    ) {
      return false
    }

    return true
  }
}
