import { Nullable } from '../../../../liber/pervasive'
import { CTableMeasurementData } from '../../../io/dataType/style'
import { TableLengthUnitTypeValues } from '../../common/const/table'
export interface TableLengthOptions {
  type: TableLengthUnitTypeValues
  w: number
}
export class TableLength {
  type: TableLengthUnitTypeValues
  w: number
  constructor(type: TableLengthUnitTypeValues, w: number) {
    this.type = type
    this.w = w
  }

  clone() {
    return new TableLength(this.type, this.w)
  }

  sameAs(other: TableLengthOptions) {
    if (this.type !== other.type || this.w !== other.w) return false

    return true
  }
  toJSON(): CTableMeasurementData {
    return {
      ['w']: this.w,
      ['type']: this.type,
    }
  }

  fromJSON(data: CTableMeasurementData) {
    this.w = data['w']
    this.type = data['type'] as TableLengthUnitTypeValues
  }

  fromOptions(opts: Nullable<TableLengthOptions>) {
    if (opts == null) return
    this.w = opts.w ?? this.w
    this.type = opts.type ?? this.type
  }

  getValue() {
    return this.w
  }
}
