import { valuesOfDict } from '../../../../liber/pervasive'
import { LineHeightRule } from '../../common/const/attrs'
import { CTableRowHeightData } from '../../../io/dataType/style'
import { isNumbersTheSame } from '../../common/helpers'

export type HeightRuleValues = valuesOfDict<typeof LineHeightRule>
export interface TableRowHeightOptions {
  value: number
  heightRule: HeightRuleValues
}
export class TableRowHeight {
  value: number
  heightRule: HeightRuleValues
  constructor(value: number, hRule: HeightRuleValues) {
    this.value = value
    this.heightRule = hRule
  }

  clone() {
    return new TableRowHeight(this.value, this.heightRule)
  }

  sameAs(other: TableRowHeight) {
    if (
      !isNumbersTheSame(this.value, other.value) ||
      this.heightRule !== other.heightRule
    ) {
      return false
    }

    return true
  }

  toJSON(): CTableRowHeightData {
    return {
      ['value']: this.value,
      ['hRule']: this.heightRule,
    }
  }

  fromJSON(data: CTableRowHeightData) {
    this.value = data['value']
    this.heightRule = data['hRule'] as HeightRuleValues
  }

  isAuto() {
    return this.heightRule === LineHeightRule.Auto ? true : false
  }

  getValue() {
    return this.value
  }

  getHRule() {
    return this.heightRule
  }
}
