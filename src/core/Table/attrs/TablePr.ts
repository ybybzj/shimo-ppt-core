import { CShd, CShdOptions } from './CShd'
import { TableCellBorder, TableCellBorderOptions } from './CellBorder'
import { TableLength } from './TableLength'
import {
  fromOptions,
  isObjectsTheSame,
  recycleObject,
} from '../../common/helpers'
import {
  assignVal,
  assignJSON,
  readFromJSON,
  isEmptyPropValue,
} from '../../../io/utils'
import { CTablePrData } from '../../../io/dataType/style'
import {
  isTCMarginsTheSame,
  setTCMarginsOptions,
  TableCellMargins,
  TableCellMarginsOptions,
} from './tableMargins'
import { Nullable } from '../../../../liber/pervasive'
import { Theme } from '../../SlideElement/attrs/theme'
import { PoolAllocator, Recyclable } from '../../../common/allocator'
import { TableLengthUnitType } from '../../common/const/table'

export interface TableBorders {
  bottom?: TableCellBorder
  left?: TableCellBorder
  right?: TableCellBorder
  top?: TableCellBorder
  insideH?: TableCellBorder
  insideV?: TableCellBorder
}
export interface TableBordersOptions {
  bottom?: TableCellBorderOptions
  left?: TableCellBorderOptions
  right?: TableCellBorderOptions
  top?: TableCellBorderOptions
  insideH?: TableCellBorderOptions
  insideV?: TableCellBorderOptions
}

export interface TablePrOptions {
  colBandSize?: number
  rowBandSize?: number
  shd?: CShdOptions
  tableBorders?: TableBordersOptions
  tableCellMargins?: null | TableCellMarginsOptions
}

export class TablePr extends Recyclable<TablePr> {
  colBandSize?: number
  rowBandSize?: number
  shd?: CShd
  tableBorders: TableBorders
  tableCellMargins: TableCellMargins
  private static allocator: PoolAllocator<TablePr, []> =
    PoolAllocator.getAllocator<TablePr, []>(TablePr)

  static new(opts?: TablePrOptions) {
    const ret = TablePr.allocator.allocate()
    if (opts != null) ret.fromOptions(opts)
    return ret
  }
  static recycle(tbPr: TablePr) {
    TablePr.allocator.recycle(tbPr)
  }
  constructor() {
    super()
    this.colBandSize = undefined
    this.rowBandSize = undefined
    this.shd = undefined
    this.tableBorders = {
      bottom: undefined,
      left: undefined,
      right: undefined,
      top: undefined,
      insideH: undefined,
      insideV: undefined,
    }
    this.tableCellMargins = {
      bottom: undefined,
      left: undefined,
      right: undefined,
      top: undefined,
    }
  }

  reset() {
    this.colBandSize = undefined
    this.rowBandSize = undefined
    this.shd?.reset()
    this.tableBorders.bottom = recycleObject(
      this.tableBorders.bottom,
      TableCellBorder
    )
    this.tableBorders.left = recycleObject(
      this.tableBorders.left,
      TableCellBorder
    )
    this.tableBorders.right = recycleObject(
      this.tableBorders.right,
      TableCellBorder
    )
    this.tableBorders.top = recycleObject(
      this.tableBorders.top,
      TableCellBorder
    )
    this.tableBorders.insideH = recycleObject(
      this.tableBorders.insideH,
      TableCellBorder
    )
    this.tableBorders.insideV = recycleObject(
      this.tableBorders.insideV,
      TableCellBorder
    )

    this.tableCellMargins.bottom = undefined
    this.tableCellMargins.left = undefined
    this.tableCellMargins.right = undefined
    this.tableCellMargins.top = undefined
  }

  clone() {
    return TablePr.new(this)
  }

  merge(tablePr: TablePrOptions) {
    this.fromOptions(tablePr)
  }

  sameAs(opts: TablePrOptions) {
    if (
      opts == null ||
      this.colBandSize !== opts.colBandSize ||
      this.rowBandSize !== opts.rowBandSize ||
      true !==
        isObjectsTheSame(this.tableBorders.bottom, opts.tableBorders?.bottom) ||
      true !==
        isObjectsTheSame(this.tableBorders.left, opts.tableBorders?.left) ||
      true !==
        isObjectsTheSame(this.tableBorders.right, opts.tableBorders?.right) ||
      true !==
        isObjectsTheSame(this.tableBorders.top, opts.tableBorders?.top) ||
      true !==
        isObjectsTheSame(
          this.tableBorders.insideH,
          opts.tableBorders?.insideH
        ) ||
      true !==
        isObjectsTheSame(
          this.tableBorders.insideV,
          opts.tableBorders?.insideV
        ) ||
      true !== isTCMarginsTheSame(this.tableCellMargins, opts.tableCellMargins)
    ) {
      return false
    }

    return true
  }

  fromOptions(opts: Nullable<TablePrOptions>) {
    if (opts == null) return
    if (opts.colBandSize != null) this.colBandSize = opts.colBandSize
    if (opts.rowBandSize != null) this.rowBandSize = opts.rowBandSize

    this.shd = fromOptions(this.shd, opts.shd, CShd)

    if (null != opts.tableBorders) {
      this.tableBorders.bottom = fromOptions(
        this.tableBorders.bottom,
        opts.tableBorders.bottom,
        TableCellBorder
      )

      this.tableBorders.top = fromOptions(
        this.tableBorders.top,
        opts.tableBorders.top,
        TableCellBorder
      )
      this.tableBorders.left = fromOptions(
        this.tableBorders.left,
        opts.tableBorders.left,
        TableCellBorder
      )
      this.tableBorders.right = fromOptions(
        this.tableBorders.right,
        opts.tableBorders.right,
        TableCellBorder
      )
      this.tableBorders.insideH = fromOptions(
        this.tableBorders.insideH,
        opts.tableBorders.insideH,
        TableCellBorder
      )
      this.tableBorders.insideV = fromOptions(
        this.tableBorders.insideV,
        opts.tableBorders.insideV,
        TableCellBorder
      )
    }

    if (null === opts.tableCellMargins) {
      this.tableCellMargins = {
        bottom: undefined,
        left: undefined,
        right: undefined,
        top: undefined,
      }
    } else {
      this.tableCellMargins = setTCMarginsOptions(
        this.tableCellMargins,
        opts.tableCellMargins
      )!
    }
  }

  refreshTheme(theme: Nullable<Theme>) {
    if (this.shd) {
      this.shd.refreshTheme(theme)
    }
    if (this.tableBorders.bottom) {
      this.tableBorders.bottom.refreshTheme(theme)
    }
    if (this.tableBorders.left) {
      this.tableBorders.left.refreshTheme(theme)
    }
    if (this.tableBorders.right) {
      this.tableBorders.right.refreshTheme(theme)
    }
    if (this.tableBorders.top) {
      this.tableBorders.top.refreshTheme(theme)
    }
    if (this.tableBorders.insideH) {
      this.tableBorders.insideH.refreshTheme(theme)
    }
    if (this.tableBorders.insideV) {
      this.tableBorders.insideV.refreshTheme(theme)
    }
  }
  toJSON(): CTablePrData {
    const data: CTablePrData = {
      ['tableBorders']: {},
      ['tableCellMar']: {},
    }
    assignVal(data, 'tableStyleColBandSize', this.colBandSize)
    assignVal(data, 'tableStyleRowBandSize', this.rowBandSize)

    assignJSON(data, 'shd', this.shd)

    assignJSON(data['tableBorders']!, 'bottom', this.tableBorders.bottom)
    assignJSON(data['tableBorders']!, 'left', this.tableBorders.left)
    assignJSON(data['tableBorders']!, 'right', this.tableBorders.right)
    assignJSON(data['tableBorders']!, 'top', this.tableBorders.top)
    assignJSON(data['tableBorders']!, 'insideH', this.tableBorders.insideH)
    assignJSON(data['tableBorders']!, 'insideV', this.tableBorders.insideV)

    if (isEmptyPropValue(data['tableBorders'])) {
      delete data['tableBorders']
    }

    assignJSON(data['tableCellMar']!, 'bottom', this.tableCellMargins.bottom)
    assignJSON(data['tableCellMar']!, 'left', this.tableCellMargins.left)
    assignJSON(data['tableCellMar']!, 'right', this.tableCellMargins.right)
    assignJSON(data['tableCellMar']!, 'top', this.tableCellMargins.top)

    if (isEmptyPropValue(data['tableCellMar'])) {
      delete data['tableCellMar']
    }

    return data
  }

  fromJSON(data: CTablePrData) {
    if (data['tableStyleColBandSize'] != null) {
      this.colBandSize = data['tableStyleColBandSize']
    }
    if (data['tableStyleRowBandSize'] != null) {
      this.rowBandSize = data['tableStyleRowBandSize']
    }
    if (data['shd'] != null) {
      this.shd = readFromJSON(data['shd'], CShd)
    }

    if (data['tableBorders'] != null) {
      if (data['tableBorders']['bottom'] != null) {
        this.tableBorders.bottom = readFromJSON(
          data['tableBorders']['bottom'],
          TableCellBorder
        )
      }
      if (data['tableBorders']['right'] != null) {
        this.tableBorders.right = readFromJSON(
          data['tableBorders']['right'],
          TableCellBorder
        )
      }
      if (data['tableBorders']['left'] != null) {
        this.tableBorders.left = readFromJSON(
          data['tableBorders']['left'],
          TableCellBorder
        )
      }
      if (data['tableBorders']['top'] != null) {
        this.tableBorders.top = readFromJSON(
          data['tableBorders']['top'],
          TableCellBorder
        )
      }
      if (data['tableBorders']['insideH'] != null) {
        this.tableBorders.insideH = readFromJSON(
          data['tableBorders']['insideH'],
          TableCellBorder
        )
      }
      if (data['tableBorders']['insideV'] != null) {
        this.tableBorders.insideV = readFromJSON(
          data['tableBorders']['insideV'],
          TableCellBorder
        )
      }
    }

    if (data['tableCellMar'] != null) {
      if (data['tableCellMar']['bottom'] != null) {
        this.tableCellMargins.bottom = readFromJSON(
          data['tableCellMar']['bottom'],
          TableLength,
          [TableLengthUnitType.AUTO, 0]
        )
      }
      if (data['tableCellMar']['left'] != null) {
        this.tableCellMargins.left = readFromJSON(
          data['tableCellMar']['left'],
          TableLength,
          [TableLengthUnitType.AUTO, 0]
        )
      }
      if (data['tableCellMar']['right'] != null) {
        this.tableCellMargins.right = readFromJSON(
          data['tableCellMar']['right'],
          TableLength,
          [TableLengthUnitType.AUTO, 0]
        )
      }
      if (data['tableCellMar']['top'] != null) {
        this.tableCellMargins.top = readFromJSON(
          data['tableCellMar']['top'],
          TableLength,
          [TableLengthUnitType.AUTO, 0]
        )
      }
    }
  }

  setDefault() {
    this.colBandSize = 1
    this.rowBandSize = 1
    this.shd = CShd.Nil()
    this.tableBorders.bottom = TableCellBorder.new()
    this.tableBorders.left = TableCellBorder.new()
    this.tableBorders.right = TableCellBorder.new()
    this.tableBorders.top = TableCellBorder.new()
    this.tableBorders.insideH = TableCellBorder.new()
    this.tableBorders.insideV = TableCellBorder.new()
    this.tableCellMargins.bottom = new TableLength(TableLengthUnitType.MM, 1.27)
    this.tableCellMargins.left = new TableLength(
      TableLengthUnitType.MM,
      2.54 /*5.4 * c_Factor_pt_to_mm*/
    ) // 5.4pt
    this.tableCellMargins.right = new TableLength(
      TableLengthUnitType.MM,
      2.54 /*5.4 * c_Factor_pt_to_mm*/
    ) // 5.4pt
    this.tableCellMargins.top = new TableLength(TableLengthUnitType.MM, 1.27)
  }
}
