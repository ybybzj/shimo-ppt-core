import { Nullable, Dict } from '../../../../liber/pervasive'
import { ParaPr } from '../../textAttributes/ParaPr'
import { TextPr } from '../../textAttributes/TextPr'
import { TableCellPr } from './TableCellPr'
import { TablePartStyle } from './TablePartStyle'
import { TablePr } from './TablePr'
import { TableRowPr } from './TableRowPr'
import { TableStyle, TableStylePrs } from './TableStyle'

export class TableStyles {
  defaultStyle: {
    paraPr: ParaPr
    textPr: TextPr
    tablePr: TablePr
    tableRowPr: TableRowPr
    tableCellPr: TableCellPr
    tableStyleId: Nullable<string>
  }
  private styles: Dict<TableStyle>
  private guidsIdMap: Record<string, string>
  constructor() {
    this.defaultStyle = {
      paraPr: ParaPr.new(),
      textPr: TextPr.new(),
      tablePr: TablePr.new(),
      tableRowPr: new TableRowPr(),
      tableCellPr: TableCellPr.new(),

      tableStyleId: undefined,
    }

    this.defaultStyle.paraPr.setDefault()
    this.defaultStyle.textPr.setDefault()
    this.defaultStyle.tablePr.setDefault()
    this.defaultStyle.tableRowPr.setDefault()
    this.defaultStyle.tableCellPr.setDefault()

    this.styles = {}
    this.guidsIdMap = {}
  }

  add(style: TableStyle) {
    const id = style.getId()
    const guid = style.guid
    const oldId = this.guidsIdMap[guid]
    if (oldId) {
      delete this.styles[oldId]
    }
    this.styles[id] = style
    this.guidsIdMap[guid] = id

    return id
  }

  remove(id: string) {
    const style = this.styles[id]
    if (style) {
      delete this.styles[id]
      const guid = style.guid
      delete this.guidsIdMap[guid]
    }
  }

  setDefaultTableStyleId(id: string) {
    if (id !== this.defaultStyle.tableStyleId) {
      this.defaultStyle.tableStyleId = id
    }
  }

  getPr(styleId: Nullable<string>): TableStylePrs {
    if (null == styleId) styleId = this.defaultStyle.tableStyleId

    const tablePrs: TableStylePrs = {
      textPr: TextPr.new(this.defaultStyle.textPr),
      paraPr: ParaPr.new(this.defaultStyle.paraPr),

      tablePr: this.defaultStyle.tablePr.clone(),
      tableRowPr: this.defaultStyle.tableRowPr.clone(),
      tableCellPr: this.defaultStyle.tableCellPr.clone(),

      tableFirstCol: TablePartStyle.new(),
      tableFirstRow: TablePartStyle.new(),
      tableLastCol: TablePartStyle.new(),
      tableLastRow: TablePartStyle.new(),
      tableBand1Horz: TablePartStyle.new(),
      tableBand1Vert: TablePartStyle.new(),
      tableBand2Horz: TablePartStyle.new(),
      tableBand2Vert: TablePartStyle.new(),
      tableNWCell: TablePartStyle.new(),
      tableNECell: TablePartStyle.new(),
      tableSWCell: TablePartStyle.new(),
      tableSECell: TablePartStyle.new(),
      tableWholeTable: TablePartStyle.new(),
    }

    this._getPr(tablePrs, styleId, true)

    tablePrs.paraPr.merge(tablePrs.tableWholeTable.paraPr)
    tablePrs.textPr.merge(tablePrs.tableWholeTable.textPr)
    tablePrs.tablePr.merge(tablePrs.tableWholeTable.tablePr)
    tablePrs.tableRowPr.merge(tablePrs.tableWholeTable.tableRowPr)
    tablePrs.tableCellPr.merge(tablePrs.tableWholeTable.tableCellPr)

    return tablePrs
  }

  getDefaultTableStyle() {
    return this.defaultStyle.tableStyleId
  }

  private _getPr(
    tablePrs: TableStylePrs,
    styleId: Nullable<string>,
    isUseDefault: boolean
  ) {
    const style = styleId ? this.styles[styleId] : undefined
    let defaultId: Nullable<string>
    if (null == styleId || null == style) {
      if (true === isUseDefault) {
        defaultId = this.defaultStyle.tableStyleId
        const defaultStyle =
          defaultId != null ? this.styles[defaultId] : undefined

        if (defaultStyle != null) {
          tablePrs.paraPr.merge(defaultStyle.paraPr)
          tablePrs.textPr.merge(defaultStyle.textPr)
          tablePrs.tablePr.merge(defaultStyle.tablePr!)
          tablePrs.tableRowPr.merge(defaultStyle.tableRowPr)
          tablePrs.tableCellPr.merge(defaultStyle.tableCellPr!)
        }
      }

      return
    }

    if (true === isUseDefault) {
      defaultId = this.defaultStyle.tableStyleId
    }

    if ('Normal Table' !== style.getName()) {
      tablePrs.paraPr.merge(style.paraPr)
      tablePrs.textPr.merge(style.textPr)

      tablePrs.tablePr.merge(style.tablePr)
      tablePrs.tableRowPr.merge(style.tableRowPr)
      tablePrs.tableCellPr.merge(style.tableCellPr!)

      tablePrs.tableBand1Horz.merge(style.tableBand1Horz)
      tablePrs.tableBand1Vert.merge(style.tableBand1Vert)
      tablePrs.tableBand2Horz.merge(style.tableBand2Horz)
      tablePrs.tableBand2Vert.merge(style.tableBand2Vert)
      tablePrs.tableFirstCol.merge(style.tableFirstCol)
      tablePrs.tableFirstRow.merge(style.tableFirstRow)
      tablePrs.tableLastCol.merge(style.tableLastCol)
      tablePrs.tableLastRow.merge(style.tableLastRow)
      tablePrs.tableNWCell.merge(style.tableNWCell)
      tablePrs.tableNECell.merge(style.tableNECell)
      tablePrs.tableSWCell.merge(style.tableSWCell)
      tablePrs.tableSECell.merge(style.tableSECell)
      tablePrs.tableWholeTable.merge(style.tableWholeTable)
    }
  }

  collectAllFontNames(allFonts: Dict<boolean>) {
    for (const sid in this.styles) {
      const style = this.styles[sid]
      style.collectAllFontNames(allFonts)
    }

    this.defaultStyle.textPr.collectAllFontNames(allFonts)
  }

  Get(styleId: Nullable<string>): Nullable<TableStyle> {
    return styleId != null ? this.styles[styleId] : undefined
  }

  getIdByGuid(guid: string) {
    const ids = Object.keys(this.styles)
    for (const id of ids) {
      const style = this.styles[id]
      if (style._guid === guid) {
        return id
      }
    }
  }

  getIdByName(name: string) {
    const ids = Object.keys(this.styles)
    for (const id of ids) {
      const style = this.styles[id]
      if (style.name === name) {
        return id
      }
    }
  }

  getIdGuidMap() {
    return Object.keys(this.styles).reduce((ret, k) => {
      const guid = this.styles[k]._guid
      if (guid != null) {
        ret[k] = guid
      }

      return ret
    }, {} as Dict<string>)
  }
}
