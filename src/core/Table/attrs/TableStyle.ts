import { Dict, Nullable } from '../../../../liber/pervasive'
import { ShdType } from '../../common/const/attrs'
import { GUID } from '../../../common/guid'
import { idGenerator } from '../../../globals/IdGenerator'
import { TableStyleData } from '../../../io/dataType/style'
import {
  assignVal,
  encodeNullableValue,
  isEmptyPropValue,
  toJSON,
  assignJSON,
  readFromJSON,
} from '../../../io/utils'
import { isObjectsTheSame, recycleObject } from '../../common/helpers'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { StyleRef } from '../../SlideElement/attrs/refs'
import { ParaPr, ParaPrOptions } from '../../textAttributes/ParaPr'
import { TextPr, TextPrOptions } from '../../textAttributes/TextPr'
import { CShd } from './CShd'
import { TableCellPr, TableCellPrOptions } from './TableCellPr'
import { TablePartStyle, TablePartStyleOptions } from './TablePartStyle'
import { TablePr, TablePrOptions } from './TablePr'
import { TableRowPr, TableRowPrOptions } from './TableRowPr'
export interface TableStyleOptions {
  name?: string
  textPr?: TextPrOptions
  paraPr?: ParaPrOptions
  tablePr?: TablePrOptions
  tableRowPr?: TableRowPrOptions
  tableCellPr?: TableCellPrOptions
  tableBand1Horz?: TablePartStyleOptions
  tableBand1Vert?: TablePartStyleOptions
  tableBand2Horz?: TablePartStyleOptions
  tableBand2Vert?: TablePartStyleOptions
  tableFirstCol?: TablePartStyleOptions
  tableFirstRow?: TablePartStyleOptions
  tableLastCol?: TablePartStyleOptions
  tableLastRow?: TablePartStyleOptions
  tableNWCell?: TablePartStyleOptions
  tableNECell?: TablePartStyleOptions
  tableSWCell?: TablePartStyleOptions
  tableSECell?: TablePartStyleOptions
  tableWholeTable?: TablePartStyleOptions
  guid?: string
}

export interface TableStylePrs {
  textPr: TextPr
  paraPr: ParaPr
  tablePr: TablePr
  tableRowPr: TableRowPr
  tableCellPr: TableCellPr
  tableBand1Horz: TablePartStyle
  tableBand1Vert: TablePartStyle
  tableBand2Horz: TablePartStyle
  tableBand2Vert: TablePartStyle
  tableFirstCol: TablePartStyle
  tableFirstRow: TablePartStyle
  tableLastCol: TablePartStyle
  tableLastRow: TablePartStyle
  tableNWCell: TablePartStyle
  tableNECell: TablePartStyle
  tableSWCell: TablePartStyle
  tableSECell: TablePartStyle
  tableWholeTable: TablePartStyle
}

export function recycleTableStylePrs(prs: TableStylePrs) {
  recycleObject(prs.textPr, TextPr)
  recycleObject(prs.paraPr, ParaPr)
  recycleObject(prs.tablePr, TablePr)

  recycleObject(prs.tableCellPr, TableCellPr)
  recycleObject(prs.tableBand1Horz, TablePartStyle)
  recycleObject(prs.tableBand1Vert, TablePartStyle)
  recycleObject(prs.tableBand2Horz, TablePartStyle)
  recycleObject(prs.tableBand2Vert, TablePartStyle)
  recycleObject(prs.tableFirstCol, TablePartStyle)
  recycleObject(prs.tableFirstRow, TablePartStyle)
  recycleObject(prs.tableLastCol, TablePartStyle)
  recycleObject(prs.tableLastRow, TablePartStyle)
  recycleObject(prs.tableNWCell, TablePartStyle)
  recycleObject(prs.tableNECell, TablePartStyle)
  recycleObject(prs.tableSWCell, TablePartStyle)
  recycleObject(prs.tableSECell, TablePartStyle)
  recycleObject(prs.tableWholeTable, TablePartStyle)
}
export class TableStyle {
  id: string
  name: string
  textPr: TextPr
  paraPr: ParaPr
  tablePr: TablePr
  tableRowPr: TableRowPr
  tableCellPr: TableCellPr
  tableBand1Horz: TablePartStyle
  tableBand1Vert: TablePartStyle
  tableBand2Horz: TablePartStyle
  tableBand2Vert: TablePartStyle
  tableFirstCol: TablePartStyle
  tableFirstRow: TablePartStyle
  tableLastCol: TablePartStyle
  tableLastRow: TablePartStyle
  tableNWCell: TablePartStyle
  tableNECell: TablePartStyle
  tableSWCell: TablePartStyle
  tableSECell: TablePartStyle
  tableWholeTable: TablePartStyle
  _guid?: string
  set guid(guid: string) {
    this._guid = guid
  }
  get guid() {
    if (this._guid == null) {
      this._guid = '{' + GUID() + '}'
    }

    return this._guid
  }
  static new(name: string, opts?: TableStyleOptions) {
    const ret = new TableStyle(name)
    if (opts != null) ret.fromOptions(opts)
    return ret
  }
  constructor(name: string, guid?: string | null | undefined) {
    this.id = idGenerator.newId()

    this.name = name

    if (guid != null) {
      this._guid = guid
    }

    this.textPr = TextPr.new()
    this.paraPr = ParaPr.new()

    this.tablePr = TablePr.new()
    this.tableRowPr = new TableRowPr()
    this.tableCellPr = TableCellPr.new()

    this.tableBand1Horz = TablePartStyle.new()
    this.tableBand1Vert = TablePartStyle.new()
    this.tableBand2Horz = TablePartStyle.new()
    this.tableBand2Vert = TablePartStyle.new()
    this.tableFirstCol = TablePartStyle.new()
    this.tableFirstRow = TablePartStyle.new()
    this.tableLastCol = TablePartStyle.new()
    this.tableLastRow = TablePartStyle.new()
    this.tableNWCell = TablePartStyle.new()
    this.tableNECell = TablePartStyle.new()
    this.tableSWCell = TablePartStyle.new()
    this.tableSECell = TablePartStyle.new()
    this.tableWholeTable = TablePartStyle.new()
  }

  getId() {
    return this.id
  }
  fromOptions(opts: Nullable<TableStyleOptions>) {
    if (opts == null) return

    if (opts.name != null) this.name = opts.name
    if (opts.guid != null) this.guid = opts.guid
    if (opts.textPr != null) this.textPr.fromOptions(opts.textPr)
    if (opts.paraPr != null) this.paraPr.fromOptions(opts.paraPr)
    if (opts.tablePr != null) this.tablePr.fromOptions(opts.tablePr)
    if (opts.tableRowPr != null) this.tableRowPr.fromOptions(opts.tableRowPr)
    if (opts.tableCellPr != null) this.tableCellPr.fromOptions(opts.tableCellPr)
    if (opts.tableBand1Horz != null) {
      this.tableBand1Horz.fromOptions(opts.tableBand1Horz)
    }
    if (opts.tableBand1Vert != null) {
      this.tableBand1Vert.fromOptions(opts.tableBand1Vert)
    }
    if (opts.tableBand2Horz != null) {
      this.tableBand2Horz.fromOptions(opts.tableBand2Horz)
    }
    if (opts.tableBand2Vert != null) {
      this.tableBand2Vert.fromOptions(opts.tableBand2Vert)
    }
    if (opts.tableFirstCol != null) {
      this.tableFirstCol.fromOptions(opts.tableFirstCol)
    }
    if (opts.tableFirstRow != null) {
      this.tableFirstRow.fromOptions(opts.tableFirstRow)
    }
    if (opts.tableLastCol != null) {
      this.tableLastCol.fromOptions(opts.tableLastCol)
    }
    if (opts.tableLastRow != null) {
      this.tableLastRow.fromOptions(opts.tableLastRow)
    }
    if (opts.tableNWCell != null) this.tableNWCell.fromOptions(opts.tableNWCell)
    if (opts.tableNECell != null) this.tableNECell.fromOptions(opts.tableNECell)
    if (opts.tableSWCell != null) this.tableSWCell.fromOptions(opts.tableSWCell)
    if (opts.tableSECell != null) this.tableSECell.fromOptions(opts.tableSECell)
    if (opts.tableWholeTable != null) {
      this.tableWholeTable.fromOptions(opts.tableWholeTable)
    }
  }

  clone() {
    return TableStyle.new(this.name, this)
  }

  updateTextPr(value: Nullable<TextPrOptions>) {
    this.textPr.reset()
    this.textPr.fromOptions(value)
  }

  reset(name?: string) {
    this.name = name ?? this.name

    this.textPr.reset()
    this.paraPr.reset()

    this.tablePr?.reset()
    this.tableRowPr?.reset()
    this.tableCellPr?.reset()

    this.tableBand1Horz.reset()
    this.tableBand1Vert.reset()
    this.tableBand2Horz.reset()
    this.tableBand2Vert.reset()

    this.tableFirstCol.reset()

    this.tableFirstRow.reset()

    this.tableLastCol.reset()

    this.tableLastRow.reset()
    this.tableNWCell.reset()
    this.tableNECell.reset()
    this.tableSWCell.reset()
    this.tableSECell.reset()

    this.tableWholeTable.reset()
  }

  collectAllFontNames(allFonts: Dict<boolean>) {
    if (null != this.textPr) {
      this.textPr.collectAllFontNames(allFonts)
    }
  }

  sameAs(style: TableStyleOptions) {
    if (
      style == null ||
      style.name !== this.name ||
      true !== isObjectsTheSame(this.textPr, style.textPr) ||
      true !== isObjectsTheSame(this.paraPr, style.paraPr) ||
      true !== isObjectsTheSame(this.tablePr, style.tablePr) ||
      true !== isObjectsTheSame(this.tableRowPr, style.tableRowPr) ||
      true !== isObjectsTheSame(this.tableCellPr, style.tableCellPr) ||
      true !== isObjectsTheSame(this.tableBand1Horz, style.tableBand1Horz) ||
      true !== isObjectsTheSame(this.tableBand1Vert, style.tableBand1Vert) ||
      true !== isObjectsTheSame(this.tableBand2Horz, style.tableBand2Horz) ||
      true !== isObjectsTheSame(this.tableBand2Vert, style.tableBand2Vert) ||
      true !== isObjectsTheSame(this.tableFirstCol, style.tableFirstCol) ||
      true !== isObjectsTheSame(this.tableFirstRow, style.tableFirstRow) ||
      true !== isObjectsTheSame(this.tableLastCol, style.tableLastCol) ||
      true !== isObjectsTheSame(this.tableLastRow, style.tableLastRow) ||
      true !== isObjectsTheSame(this.tableNWCell, style.tableNWCell) ||
      true !== isObjectsTheSame(this.tableNECell, style.tableNECell) ||
      true !== isObjectsTheSame(this.tableSWCell, style.tableSWCell) ||
      true !== isObjectsTheSame(this.tableSECell, style.tableSECell) ||
      true !== isObjectsTheSame(this.tableWholeTable, style.tableWholeTable)
    ) {
      return false
    }

    return true
  }

  toJSON(): TableStyleData {
    const data: TableStyleData = {
      ['id']: this.guid,
    }
    assignVal(data, 'name', encodeNullableValue(this.name))

    const shd = this.tablePr?.shd

    if (!isEmptyPropValue(shd?.fillRef)) {
      const fillRef = toJSON(shd!.fillRef!)
      data['tblBgFillRef'] = fillRef
    } else if (!isEmptyPropValue(shd?.fillEffects)) {
      const fillEffects = toJSON(shd!.fillEffects!)
      data['tblBgFill'] = fillEffects
    }

    assignJSON(data, 'tableBand1Horz', this.tableBand1Horz)
    assignJSON(data, 'tableBand1Vert', this.tableBand1Vert)
    assignJSON(data, 'tableBand2Horz', this.tableBand2Horz)
    assignJSON(data, 'tableBand2Vert', this.tableBand2Vert)
    assignJSON(data, 'tableFirstCol', this.tableFirstCol)
    assignJSON(data, 'tableFirstRow', this.tableFirstRow)
    assignJSON(data, 'tableLastCol', this.tableLastCol)
    assignJSON(data, 'tableLastRow', this.tableLastRow)
    assignJSON(data, 'tableNWCell', this.tableNWCell)
    assignJSON(data, 'tableNECell', this.tableNECell)
    assignJSON(data, 'tableSWCell', this.tableSWCell)
    assignJSON(data, 'tableSECell', this.tableSECell)
    assignJSON(data, 'tableWholeTable', this.tableWholeTable, this.tablePr)
    return data
  }
  fromJSON(data: TableStyleData) {
    if (data['id'] != null) {
      this.guid = data['id']
    }

    if (data['name'] != null) {
      this.name = data['name']
    }

    if (
      (data['tblBgFillRef'] != null || data['tblBgFill'] != null) &&
      this.tablePr != null
    ) {
      this.tablePr.shd = this.tablePr.shd ?? CShd.new()
      this.tablePr.shd.value = ShdType.Clear
      if (data['tblBgFillRef'] != null) {
        this.tablePr.shd.fillRef = readFromJSON(data['tblBgFillRef'], StyleRef)
      } else if (data['tblBgFill'] != null) {
        this.tablePr.shd.fillEffects = readFromJSON(
          data['tblBgFill'],
          FillEffects
        )
      }
    }

    if (data['tableWholeTable'] != null) {
      const pr = readFromJSON(data['tableWholeTable'], TablePartStyle)!

      this.tableWholeTable = pr
    }

    if (data['tableBand1Horz'] != null) {
      const pr = readFromJSON(data['tableBand1Horz'], TablePartStyle)!

      this.tableBand1Horz = pr
    }
    if (data['tableBand1Vert'] != null) {
      const pr = readFromJSON(data['tableBand1Vert'], TablePartStyle)!

      this.tableBand1Vert = pr
    }
    if (data['tableBand2Horz'] != null) {
      const pr = readFromJSON(data['tableBand2Horz'], TablePartStyle)!

      this.tableBand2Horz = pr
    }
    if (data['tableBand2Vert'] != null) {
      const pr = readFromJSON(data['tableBand2Vert'], TablePartStyle)!

      this.tableBand2Vert = pr
    }
    if (data['tableFirstCol'] != null) {
      const pr = readFromJSON(data['tableFirstCol'], TablePartStyle)!

      this.tableFirstCol = pr
    }
    if (data['tableFirstRow'] != null) {
      const pr = readFromJSON(data['tableFirstRow'], TablePartStyle)!

      this.tableFirstRow = pr
    }
    if (data['tableLastCol'] != null) {
      const pr = readFromJSON(data['tableLastCol'], TablePartStyle)!

      this.tableLastCol = pr
    }
    if (data['tableLastRow'] != null) {
      const pr = readFromJSON(data['tableLastRow'], TablePartStyle)!

      this.tableLastRow = pr
    }
    if (data['tableNWCell'] != null) {
      const pr = readFromJSON(data['tableNWCell'], TablePartStyle)!

      this.tableNWCell = pr
    }
    if (data['tableNECell'] != null) {
      const pr = readFromJSON(data['tableNECell'], TablePartStyle)!

      this.tableNECell = pr
    }
    if (data['tableSWCell'] != null) {
      const pr = readFromJSON(data['tableSWCell'], TablePartStyle)!

      this.tableSWCell = pr
    }
    if (data['tableSECell'] != null) {
      const pr = readFromJSON(data['tableSECell'], TablePartStyle)!

      this.tableSECell = pr
    }

    if (this.tablePr && this.tableWholeTable.tablePr.tableBorders.insideH) {
      this.tablePr.tableBorders.insideH =
        this.tableWholeTable.tablePr.tableBorders.insideH
      delete this.tableWholeTable.tablePr.tableBorders.insideH
    }
    if (this.tablePr && this.tableWholeTable.tablePr.tableBorders.insideV) {
      this.tablePr.tableBorders.insideV =
        this.tableWholeTable.tablePr.tableBorders.insideV
      delete this.tableWholeTable.tablePr.tableBorders.insideV
    }
    if (this.tablePr && this.tableWholeTable.tableCellPr.tableCellBorders.top) {
      this.tablePr.tableBorders.top =
        this.tableWholeTable.tableCellPr.tableCellBorders.top
      delete this.tableWholeTable.tableCellPr.tableCellBorders.top
    }
    if (
      this.tablePr &&
      this.tableWholeTable.tableCellPr.tableCellBorders.bottom
    ) {
      this.tablePr.tableBorders.bottom =
        this.tableWholeTable.tableCellPr.tableCellBorders.bottom
      delete this.tableWholeTable.tableCellPr.tableCellBorders.bottom
    }
    if (
      this.tablePr &&
      this.tableWholeTable.tableCellPr.tableCellBorders.left
    ) {
      this.tablePr.tableBorders.left =
        this.tableWholeTable.tableCellPr.tableCellBorders.left
      delete this.tableWholeTable.tableCellPr.tableCellBorders.left
    }
    if (
      this.tablePr &&
      this.tableWholeTable.tableCellPr.tableCellBorders.right
    ) {
      this.tablePr.tableBorders.right =
        this.tableWholeTable.tableCellPr.tableCellBorders.right
      delete this.tableWholeTable.tableCellPr.tableCellBorders.right
    }
  }

  getName() {
    return this.name
  }
}
