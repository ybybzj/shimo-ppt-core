import { CHexColor, CHexColorOptions } from '../../color/CHexColor'
import { ShdType } from '../../common/const/attrs'
import { isObjectsTheSame } from '../../common/helpers'
import { assignJSON, fromJSON, readFromJSON } from '../../../io/utils'
import { CShdData } from '../../../io/dataType/style'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { StyleRef } from '../../SlideElement/attrs/refs'
import { Nullable, valuesOfDict } from '../../../../liber/pervasive'
import { Theme } from '../../SlideElement/attrs/theme'
import { ClrMap } from '../../color/clrMap'
export interface CShdOptions {
  value?: valuesOfDict<typeof ShdType>
  color?: CHexColorOptions
  fillEffects?: FillEffects
  fillRef?: StyleRef
}
export class CShd {
  value: valuesOfDict<typeof ShdType>
  color: CHexColor
  fillEffects?: FillEffects
  fillRef?: StyleRef
  static Nil() {
    const ret = new CShd()
    ret.value = ShdType.Nil
    return ret
  }
  static new(opts?: {
    value?: valuesOfDict<typeof ShdType>
    fillEffects?: FillEffects
    color?: CHexColor
  }) {
    const ret = new CShd()
    if (opts != null) {
      const { value, fillEffects, color } = opts
      if (value != null) ret.value = value
      if (fillEffects != null) ret.fillEffects = fillEffects
      if (color != null) ret.color = color
    }
    return ret
  }

  constructor() {
    this.value = ShdType.Clear
    this.color = new CHexColor(255, 255, 255)
    this.fillEffects = undefined
    this.fillRef = undefined
  }
  reset() {
    this.value = ShdType.Clear
    this.color.set(255, 255, 255)
    this.fillEffects = undefined
    this.fillRef?.reset()
  }

  fromOptions(opts: Nullable<CShdOptions>) {
    if (opts == null) return
    if (opts.value != null) {
      this.value = opts.value
    } else {
      this.value = ShdType.Clear
    }

    if (opts.color != null) {
      this.color.set(opts.color.r, opts.color.g, opts.color.b, opts.color.auto)
    }

    if (opts.fillEffects != null) {
      this.fillEffects = opts.fillEffects.clone()
    }

    if (opts.fillRef != null) {
      this.fillRef = opts.fillRef
    }
  }

  clone() {
    const dup = new CShd()
    dup.value = this.value

    dup.color.set(this.color.r, this.color.g, this.color.b, this.color.auto)

    if (null != this.fillEffects) dup.fillEffects = this.fillEffects.clone()

    if (null != this.fillRef) dup.fillRef = this.fillRef.clone()

    return dup
  }

  sameAs(shd: CShdOptions) {
    if (this.value === shd.value) {
      switch (this.value) {
        case ShdType.Nil:
          return true

        case ShdType.Clear: {
          return (
            isObjectsTheSame(this.color, shd.color) &&
            isObjectsTheSame(this.fillEffects, shd.fillEffects)
          )
        }
      }
    }

    return false
  }

  getColorByTheme(theme: Nullable<Theme>, colorMap: Nullable<ClrMap>) {
    if (null != this.fillEffects) {
      this.fillEffects.check(theme, colorMap)
      const rgba = this.fillEffects.getRGBAColor()
      return new CHexColor(rgba.r, rgba.g, rgba.b, false)
    } else return this.color
  }

  refreshTheme(theme: Nullable<Theme>) {
    if (this.fillRef && theme) {
      this.fillEffects = theme.getFillStyle(
        this.fillRef.idx,
        this.fillRef.color
      )
      this.fillRef = undefined
    }
  }

  toJSON(): CShdData {
    const data: CShdData = {
      ['value']: this.value,
    }
    if (this.value === ShdType.Clear) {
      assignJSON(data, 'color', this.color)
      assignJSON(data, 'unifill', this.fillEffects)
      assignJSON(data, 'fillRef', this.fillRef)
    }
    return data
  }

  fromJSON(data: CShdData) {
    this.value = data['value'] as valuesOfDict<typeof ShdType>
    if (this.value === ShdType.Clear) {
      data['color'] && fromJSON(this.color!, data['color'])
      this.fillEffects = readFromJSON(data['unifill'], FillEffects)
      this.fillRef = readFromJSON(data['fillRef'], StyleRef)
    }
  }
}
