import { TableCell } from './TableCell'

import { TableRowPr, TableRowPrOptions } from './attrs/TableRowPr'

// import { gDefaultTableRowPr } from '../textAttributes/globals'

import { TableRowHeight, TableRowHeightOptions } from './attrs/TableRowHeight'
import { idGenerator } from '../../globals/IdGenerator'
import { EditActionFlag } from '../EditActionFlag'
import { InstanceType } from '../instanceTypes'
import { Table } from './Table'
import { Nullable } from '../../../liber/pervasive'

import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { calculateTableGridCols } from '../calculation/calculate/table'

import { updateCalcStatusForGraphicFrameItem } from '../calculation/updateCalcStatus/graphicFrame'
import { isObjectsTheSame } from '../common/helpers'
import { ManagedSliceUtil, RecyclableArray } from '../../common/managedArray'
import { PoolAllocator, Recyclable } from '../../common/allocator'
import { resetChildren } from './utils/utils'
import { VMergeType } from '../common/const/table'
interface CellInfoData {
  startGridCol: number
  startGridColRTL: number
  curGridCol: number
  cellStartX: number
  cellEndX: number
  xOfTextDocStart: number
  xOfTextDocEnd: number
}
class CellInfo extends Recyclable<CellInfo> {
  /** 单元格起始 index，从 0 开始，emptyCell 也会计入 */
  startGridCol?: number
  startGridColRTL?: number
  curGridCol?: number
  cellStartX?: number
  cellEndX?: number
  xOfTextDocStart?: number
  xOfTextDocEnd?: number
  private static allocator: PoolAllocator<CellInfo, []> =
    PoolAllocator.getAllocator<CellInfo, []>(CellInfo)
  static new() {
    return CellInfo.allocator.allocate()
  }
  static recycle(info: CellInfo) {
    CellInfo.allocator.recycle(info)
  }
  constructor() {
    super()
  }
  reset() {
    this.startGridCol = undefined
    this.startGridColRTL = undefined
    this.curGridCol = undefined
    this.cellStartX = undefined
    this.cellEndX = undefined
    this.xOfTextDocStart = undefined
    this.xOfTextDocEnd = undefined
  }
  set(
    startGridCol: number,
    startGridColRTL: number,
    curGridCol: number,
    cellStartX: number,
    cellEndX: number,
    xOfTextDocStart: number,
    xOfTextDocEnd: number
  ) {
    this.startGridCol = startGridCol
    this.startGridColRTL = startGridColRTL
    this.curGridCol = curGridCol
    this.cellStartX = cellStartX
    this.cellEndX = cellEndX
    this.xOfTextDocStart = xOfTextDocStart
    this.xOfTextDocEnd = xOfTextDocEnd
  }
}

export class TableRow extends Recyclable<TableRow> {
  readonly instanceType = InstanceType.TableRow
  id: string
  parent: Table
  next: TableRow | null
  prev: TableRow | null
  children: RecyclableArray<TableCell>
  private cellInfos: RecyclableArray<CellInfo>
  metrics: { xMin: number; xMax: number }
  calcedPrs: { pr: Nullable<TableRowPr>; isDirty: boolean }
  pr: TableRowPr
  height: number
  index: number
  private static allocator: PoolAllocator<
    TableRow,
    [table: Table, cols: number]
  > = PoolAllocator.getAllocator<TableRow, [table: Table, cols: number]>(
    TableRow
  )
  static new(table: Table, cols: number) {
    return TableRow.allocator.allocate(table, cols)
  }
  static recycle(tr: TableRow) {
    TableRow.allocator.recycle(tr)
  }
  constructor(table: Table, cols: number) {
    super()
    this.id = idGenerator.newId()

    this.parent = table

    this.next = null
    this.prev = null

    this.calcedPrs = {
      pr: null,
      isDirty: true,
    }

    // table cell 会依赖上面的 calcedPrs
    this.children = new RecyclableArray(TableCell.recycle)
    for (let i = 0; i < cols; i++) {
      this.children.push(TableCell.new(this))
    }

    this.updateCellIndices()

    this.cellInfos = new RecyclableArray(CellInfo.recycle)

    this.metrics = {
      xMin: 0,
      xMax: 0,
    }

    this.pr = new TableRowPr()

    this.height = 0

    this.index = 0
  }

  reset(Table: Table, columnCount: number) {
    this.parent = Table

    this.next = null
    this.prev = null

    //reset children

    resetChildren<[TableRow], TableCell>(this, columnCount, TableCell, this)

    this.updateCellIndices()

    this.cellInfos.clear()

    this.metrics.xMin = 0
    this.metrics.xMax = 0

    this.calcedPrs.pr?.reset()
    this.calcedPrs.isDirty = true

    this.pr.reset()

    this.height = 0

    this.index = 0
  }

  getId() {
    return this.id
  }

  clone(table: Table) {
    const row = TableRow.new(table, 0)

    row.setPr(this.pr)

    const cellsCount = this.children.length
    for (let i = 0; i < cellsCount; i++) {
      row.children.push(this.children.at(i)!.clone(row))
    }

    row.updateCellIndices()

    return row
  }

  isInPPT(id?: string) {
    let isUse = false
    if (null != id) {
      isUse =
        ManagedSliceUtil.findIndex(
          this.children,
          (item) => item.getId() === id
        ) !== -1
    } else {
      isUse = true
    }

    if (true === isUse && null != this.parent) {
      return this.parent.isInPPT(this.getId())
    }

    return false
  }

  setIndex(index: number) {
    if (index !== this.index) {
      this.index = index
      this.invalidateCalcedPrs()
    }
  }

  setMetrics(xMin: number, xMax: number) {
    this.metrics.xMin = Math.min(xMin, xMax)
    this.metrics.xMax = Math.max(xMin, xMax)
  }

  beforeDelete() {
    const cellsCount = this.cellsCount
    for (let cellIndex = 0; cellIndex < cellsCount; cellIndex++) {
      const cell = this.getCellByIndex(cellIndex)!

      const paras = cell.textDoc.children
      const l = paras.length
      for (let i = 0; i < l; i++) {
        paras.at(i)!.beforeDelete()
      }
    }
  }

  invalidateCalcedPrs(bubbleUp = true) {
    if (this.calcedPrs.isDirty === false) {
      this.calcedPrs.isDirty = true
      if (bubbleUp) {
        updateCalcStatusForGraphicFrameItem(this)
      }
    }
  }

  getCalcedPr(isCopy?: boolean): TableRowPr {
    if (true === this.calcedPrs.isDirty) {
      this.calcedPrs.pr = this._calcPr()
      this.calcedPrs.isDirty = false
    }

    if (false === isCopy) return this.calcedPrs.pr!
    else return this.calcedPrs.pr!.clone()
  }

  _calcPr() {
    const tablePr = this.parent.getCalcedPr(false)
    const tableLook = this.parent.getTableLook()
    const index = this.index

    const rowPr = tablePr.tableRowPr.clone()

    if (true === tableLook.isBandRow()) {
      const rowBandSize = tablePr.tablePr.rowBandSize!
      const curIndex = true !== tableLook.isFirstRow() ? index : index - 1
      const bandGroupIndex =
        1 !== rowBandSize ? Math.floor(curIndex / rowBandSize) : curIndex
      if (0 === bandGroupIndex % 2) {
        rowPr.merge(tablePr.tableBand1Horz.tableRowPr)
      } else rowPr.merge(tablePr.tableBand2Horz.tableRowPr)
    }

    if (true === tableLook.isLastRow() && this.parent.lastRow === index) {
      rowPr.merge(tablePr.tableLastRow.tableRowPr)
    }

    if (true === tableLook.isFirstRow() && 0 === index) {
      rowPr.merge(tablePr.tableFirstRow.tableRowPr)
    }

    rowPr.merge(this.pr)

    return rowPr
  }

  clearFormatting(isRemoveMerge: boolean) {
    if (true === isRemoveMerge) {
      this.setHeight(undefined)
    }
    ManagedSliceUtil.forEach(this.children, (cell) =>
      cell.clearFormatting(isRemoveMerge)
    )
  }

  setPr(rowPr: TableRowPrOptions) {
    if (rowPr === this.pr) return
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_TableRow_Pr,
      this.pr,
      rowPr
    )
    this.pr.fromOptions(rowPr)
    this.invalidateCalcedPrs()
  }

  getHeight() {
    const rowPr = this.getCalcedPr(false)
    return rowPr.height!
  }

  setHeight(value: Nullable<TableRowHeightOptions>) {
    if (isObjectsTheSame(this.pr.height, value)) {
      return
    }

    const orgHeight = this.pr.height
    const newHeight =
      null != value
        ? new TableRowHeight(value.value, value.heightRule)
        : undefined

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_TableRow_Height,
      orgHeight,
      newHeight
    )
    this.pr.height = newHeight
    this.invalidateCalcedPrs()
  }

  applyPr(rowPr: TableRowPr) {
    if (undefined === rowPr.height) this.setHeight(undefined)
    else this.setHeight(rowPr.height)
  }

  getCellByIndex(index: number) {
    if (index < 0 || index >= this.children.length) return null

    return this.children.at(index)
  }

  get cellsCount() {
    return this.children.length
  }

  setCellMetrics(
    index: number,
    startGridCol: number,
    startGridColRTL: number,
    cellStartX: number,
    cellEndX: number,
    xOfTextDocStart: number,
    xOfTextDocEnd: number
  ) {
    const isRTL = this.getTable().isRTL
    let cellInfo = this.cellInfos.at(index)
    if (cellInfo == null) {
      cellInfo = CellInfo.new()
      this.cellInfos.insert(index, cellInfo)
    }
    cellInfo.set(
      startGridCol,
      startGridColRTL,
      isRTL ? startGridColRTL : startGridCol,
      cellStartX,
      cellEndX,
      xOfTextDocStart,
      xOfTextDocEnd
    )
  }

  updateCellMetrics(index: number) {
    const cell = this.children.at(index)
    if (cell) {
      const {
        startGridCol,
        startGridColRTL,
        cellStartX,
        cellEndX,
        xOfTextDocStart,
        xOfTextDocEnd,
      } = cell.metrics

      this.setCellMetrics(
        index,
        startGridCol,
        startGridColRTL,
        cellStartX,
        cellEndX,
        xOfTextDocStart,
        xOfTextDocEnd
      )
    }
  }

  getCellInfo(index: number): CellInfoData {
    let cellInfo = this.cellInfos.at(index)
    if (!cellInfo || undefined === cellInfo.startGridCol) {
      const table = this.getTable()
      calculateTableGridCols(table)
    }

    cellInfo = this.cellInfos.at(index)
    return {
      startGridCol: cellInfo?.startGridCol ?? 0,
      startGridColRTL: cellInfo?.startGridColRTL ?? 0,
      curGridCol: cellInfo?.curGridCol ?? 0,
      cellStartX: cellInfo?.cellStartX ?? 0,
      cellEndX: cellInfo?.cellEndX ?? 0,
      xOfTextDocStart: cellInfo?.xOfTextDocStart ?? 0,
      xOfTextDocEnd: cellInfo?.xOfTextDocEnd ?? 0,
    }
  }

  getStartGridColumn(index: number) {
    const maxIndex = Math.min(this.children.length - 1, index - 1)
    let curGridColIndex = 0
    for (let i = 0; i <= maxIndex; i++) {
      const cell = this.getCellByIndex(i)!
      const gridSpan = cell.getGridSpan()

      curGridColIndex += gridSpan
    }

    return curGridColIndex
  }

  getStartGridColRTL(index: number) {
    const len = this.children.length - 1
    let curGridColIndex = 0
    for (let ci = len; ci >= index + 1; ci--) {
      const cell = this.getCellByIndex(ci)!
      const gridSpan = cell.getGridSpan()

      curGridColIndex += gridSpan
    }

    return curGridColIndex
  }

  getCellByStartGridColIndex(startGridColIndex: number) {
    return ManagedSliceUtil.find(
      this.children,
      (_, index) => this.getCellInfo(index)?.startGridCol === startGridColIndex
    )
  }

  removeCell(index: number) {
    const cell = this.children.at(index)
    if (cell == null) return

    this.children.recycle(this.children.remove(index, 1))
    this.cellInfos.recycle(this.cellInfos.remove(index, 1))

    this.updateCellIndices(index)

    updateCalcStatusForGraphicFrameItem(this)
  }

  addCell(
    index: number,
    cell?: Nullable<TableCell>,
    needReIndex?: boolean
  ): TableCell {
    const newCell = cell ? cell : TableCell.new(this)
    newCell.setIndex(index)

    this.children.insert(index, newCell)
    this.cellInfos.insert(index, CellInfo.new())

    if (true === needReIndex) {
      this.updateCellIndices(index)
    } else {
      if (index > 0) {
        const prevCell = this.children.at(index - 1)!
        prevCell.next = newCell
        newCell.prev = prevCell
      } else newCell.prev = null

      if (index < this.children.length - 1) {
        const nextCell = this.children.at(index + 1)!
        nextCell.prev = newCell
        newCell.next = nextCell
      } else newCell.next = null
    }
    updateCalcStatusForGraphicFrameItem(this)
    return newCell
  }

  updateCellIndices(startIndex: number = 0) {
    for (let i = startIndex, l = this.children.length; i < l; i++) {
      const cell = this.children.at(i)!
      cell.setIndex(i)
      cell.prev = i > 0 ? this.children.at(i - 1)! : null
      cell.next = i < this.children.length - 1 ? this.children.at(i + 1)! : null
      cell.parent = this
    }
  }

  getTable() {
    return this.parent
  }

  getIndex() {
    return this.index
  }

  getTopMargin() {
    let topMargin: number = 0
    ManagedSliceUtil.forEach(this.children, (cell) => {
      if (VMergeType.Restart !== cell.getVMerge()) {
        return
      }
      const margins = cell.getMargins()
      topMargin = Math.max(topMargin, margins.top!.w)
    })
    return topMargin
  }

  getBottomMargin() {
    let bottomMargin: number = 0
    ManagedSliceUtil.forEach(this.children, (cell, i) => {
      if (VMergeType.Restart !== cell.getVMerge()) {
        return
      }
      const valueOfVMergeCount = this.parent.getVMergeNum(i, this.index)
      if (valueOfVMergeCount > 1) {
        return
      }
      const margins = cell.getMargins()
      bottomMargin = Math.max(bottomMargin, margins.bottom!.w)
    })
    return bottomMargin
  }
}
