import { Nullable } from '../../../liber/pervasive'
import { PointerEvtTypeValue } from '../../common/dom/events'
import { TableSelectionTypeValues } from '../common/const/table'

export interface CellPos {
  row: number
  cell: number
  curCell?
}

export interface RowInfo {
  w: number
  type: number
  gridSpan: number
}

interface TableSelectPositionInfo {
  cellPos: {
    cell: number
    row: number
  }
  x: number
  y: number
  mouseEvtInfo: {
    clicks: number
    type: PointerEvtTypeValue
    ctrl: boolean
  }
}

export interface TableSelection {
  /** 选中 cell 时, MouseDown 且没有 MouseUp */
  start: boolean
  /** 是否正在进行选中操作或已有选中文本/单元格 */
  isUse: boolean
  selectionStartInfo: TableSelectPositionInfo
  selectionEndInfo: TableSelectPositionInfo
  /** 选中文本 - 1, 选中单元格 - 0 */
  type: number
  /** 多选 tableCell 的信息 */
  cellPositions: CellPos[]
  selectionType: TableSelectionTypeValues
  selectionInfo: Nullable<TableSelectionInfo>
  currentRowIndex: number
}

export interface TableSelectionInfo {
  start: boolean
  startX: number
  startY: number
  x: number
  y: number
  startCenterX: number
  startCenterY: number
  index: number
  cellPos: CellPos
  isColumn: boolean
  max: number
  min: number
}
