import { Nullable, valuesOfDict } from '../../../liber/pervasive'
import { ShdType, LineHeightRule } from '../common/const/attrs'
import { POINTER_EVENT_TYPE } from '../../common/dom/events'
import { convertMMToTwips } from '../../common/utils'
import { EditActionFlag } from '../EditActionFlag'
import { idGenerator } from '../../globals/IdGenerator'
import { InstanceType } from '../instanceTypes'
import { cursorExtType } from '../../ui/editorUI/uiCursor'
import { HoverInfo } from '../../ui/states/type'
import { TableCellBorder, TableCellBorderOptions } from './attrs/CellBorder'
import { CShd } from './attrs/CShd'

import { TableLength } from './attrs/TableLength'
import { TablePr } from './attrs/TablePr'
import {
  TCBorderValue,
  TableLengthUnitType,
  TableSelectionType,
  VMergeType,
  VMergeTypeValues,
} from '../common/const/table'

import { informEntityChildrenChange } from '../calculation/informChanges/contentChange'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { calculateTableGrid } from '../calculation/calculate/table'
import { calculateForRendering } from '../calculation/calculate/calculateForRendering'
import { GraphicFrame } from '../SlideElement/GraphicFrame'

import { getPresentation } from '../utilities/finders'
import { isSlideElementInPPT } from '../utilities/shape/asserts'
import {
  updateParagraphProps,
  updateTextPr,
} from '../utilities/shape/hookInvokers'
import {
  TableDimension,
  TableLook,
  TableCalcInfo,
  TableRowsInfo,
} from './common'
import { TableCell } from './TableCell'
import { TableRow } from './TableRow'
import {
  rowContentIterator,
  hitInTableBorder,
  resetChildren,
} from './utils/utils'
import { CellPos, RowInfo, TableSelection } from './type'
import {
  TextDocumentOperationArgs,
  TextDocumentOperationFns,
} from '../TextDocument/utils/operations'
import { TextOperationDirection } from '../utilities/type'
import { updateCalcStatusForGraphicFrameItem } from '../calculation/updateCalcStatus/graphicFrame'
import { isObjectsTheSame, recycleObject } from '../common/helpers'
import { gGlobalTableStyles } from '../Slide/GlobalTableStyles'
import { recycleTableStylePrs, TableStylePrs } from './attrs/TableStyle'
import { ParaPr } from '../textAttributes/ParaPr'
import { TextPr } from '../textAttributes/TextPr'
import {
  ManagedArray,
  ManagedSliceUtil,
  RecyclableArray,
} from '../../common/managedArray'

export class Table {
  readonly instanceType = InstanceType.Table
  // base properties
  id: string
  parent: GraphicFrame
  index: number
  x: number
  y: number
  xLimit: number
  yLimit: number
  rowsAndColsMarkInfo: RowsAndColsMarkInfo
  calcedPrs: {
    pr: Nullable<TableStylePrs>
    isDirty: boolean
  }
  pr: TablePr
  tableStyleId: Nullable<string>
  tableLook: TableLook
  /** 每个 grid 到最左侧起点的宽总和，注意从 -1: 0 开始 */
  tableGridReduced: number[]
  /** 每个 grid 自身的宽度 */
  tableGridReducedRTL: number[]
  tableGrid: number[]
  /** 每个 grid 自身的宽度 */
  tableGridCalculated: number[]
  calculateInfo: TableCalcInfo

  rowsInfo: TableRowsInfo[]
  tableRowsBottomPosition: ManagedArray<number> = new ManagedArray()
  selection!: TableSelection
  dimension: TableDimension
  /** 选中多个 cell 时为最后一个 */
  curCell: Nullable<TableCell>
  isSetToAll: boolean
  children: RecyclableArray<TableRow>

  constructor(
    parent: GraphicFrame,
    rowsCount: number,
    colsCount: number,
    tableGrid?: Array<number>
  ) {
    this.id = idGenerator.newId()

    this.parent = parent
    this.index = -1

    this.x = 0
    this.y = 0
    this.xLimit = 0
    this.yLimit = 0

    this.rowsAndColsMarkInfo = new RowsAndColsMarkInfo()

    this.calcedPrs = {
      pr: null,
      isDirty: true,
    }

    this.pr = TablePr.new()
    this.tableStyleId = null
    this.tableLook = new TableLook(true, true, false, false, true, false, false)

    this.tableGridReduced = []
    this.tableGridReducedRTL = []
    this.tableGrid = tableGrid ?? []
    this.tableGridCalculated = this.tableGrid.slice()

    this.calculateInfo = new TableCalcInfo()

    this.children = new RecyclableArray(TableRow.recycle)
    for (let i = 0; i < rowsCount; i++) {
      this.children.push(TableRow.new(this, colsCount))
    }

    this.updateRowIndices(0)

    this.rowsInfo = []
    this.dimension = new TableDimension(0, 0, 0, 0)

    if (this.children.length > 0) {
      this.curCell = this.children.at(0)!.getCellByIndex(0)
    } else {
      this.curCell = null
    }

    this.isSetToAll = false
    this.resetSelection()
  }
  reset(
    parent: GraphicFrame,
    rowsCount: number,
    colsCount: number,
    tableGrid: Nullable<number[]>
  ) {
    this.parent = parent
    this.index = -1

    this.x = 0
    this.y = 0
    this.xLimit = 0
    this.yLimit = 0

    this.rowsAndColsMarkInfo.reset()

    if (this.calcedPrs.pr) {
      recycleTableStylePrs(this.calcedPrs.pr)
    }
    this.calcedPrs.pr = null
    this.calcedPrs.isDirty = true

    this.pr.reset()
    this.tableStyleId = null
    this.tableLook.Set(true, true, false, false, true, false, false)

    this.tableGridReduced.length = 0
    this.tableGridReducedRTL.length = 0
    if (tableGrid) {
      this.tableGrid = tableGrid
      this.tableGridCalculated = tableGrid.slice()
    } else {
      this.tableGrid.length = 0
      this.tableGridCalculated.length = 0
    }

    this.calculateInfo.reset()

    resetChildren(this, rowsCount, TableRow, this, colsCount)

    this.updateRowIndices(0)

    this.rowsInfo = []
    this.tableRowsBottomPosition.reset()
    this.dimension.reset(0, 0, 0, 0)

    if (this.children.length > 0) {
      this.curCell = this.children.at(0)!.getCellByIndex(0)
    } else {
      this.curCell = null
    }

    this.isSetToAll = false
    this.resetSelection()
  }

  getParent() {
    return this.parent
  }

  setParent(parent: GraphicFrame) {
    this.parent = parent
  }

  getId() {
    return this.id
  }

  getParentSlideIndex() {
    return this.parent.getParentSlideIndex()
  }

  isOnlyOneElementSelected() {
    if (this.parent) return this.parent.isOnlyOneElementSelected()

    return false
  }

  resetSelection() {
    this.selection = {
      start: false,
      isUse: false,
      selectionStartInfo: {
        cellPos: { row: 0, cell: 0 },
        x: 0,
        y: 0,
        mouseEvtInfo: {
          clicks: 1,
          type: POINTER_EVENT_TYPE.Down,
          ctrl: false,
        },
      },
      selectionEndInfo: {
        cellPos: { row: 0, cell: 0 },
        x: 0,
        y: 0,
        mouseEvtInfo: {
          clicks: 1,
          type: POINTER_EVENT_TYPE.Down,
          ctrl: false,
        },
      },
      type: TableSelectionType.TEXT,
      cellPositions: [],
      selectionType: TableSelectionType.COMMON,
      selectionInfo: null,
      currentRowIndex: 0,
    }

    this.dimension.reset()

    if (this.children.length > 0) {
      this.curCell = this.children.at(0)!.getCellByIndex(0)
    } else this.curCell = null

    this.isSetToAll = false
  }

  get isRTL() {
    return this.tableLook.rtl === true
  }

  get lastRow() {
    return this.children.length - 1
  }

  getTableGridReduced() {
    return this.isRTL ? this.tableGridReducedRTL : this.tableGridReduced
  }

  isEmpty(): boolean {
    return !this.children || this.children.length <= 0
  }

  updateRowIndices(startIndex = 0) {
    for (let i = startIndex; i < this.children.length; i++) {
      const row = this.children.at(i)!
      row.setIndex(i)
      row.prev = i > 0 ? this.children.at(i - 1)! : null
      row.next = i < this.children.length - 1 ? this.children.at(i + 1)! : null
      row.parent = this
    }
  }

  clone(parent: GraphicFrame) {
    const tableGrid = this.tableGrid.slice()
    const table = new Table(parent, 0, 0, tableGrid) as Table
    table.setTableStyleId(this.tableStyleId)
    table.setTableLook(this.tableLook.clone())
    table.setPr(this.pr.clone())

    ManagedSliceUtil.forEach(this.children, (row) => {
      table.children.push(row.clone(table))
    })

    table.updateRowIndices(0)

    if (table.children.length > 0 && table.children.at(0)!.cellsCount > 0) {
      table.curCell = table.children.at(0)!.getCellByIndex(0)
    }

    return table
  }

  getTheme() {
    return this.parent.getTheme()
  }

  getColorMap() {
    return this.parent.getColorMap()
  }

  getTextStyles(lvl: number) {
    return this.parent.getTextStyles(lvl)
  }

  getTextBgColor() {
    const shd = this.getShd()!
    if (ShdType.Nil !== shd.value) {
      return shd.getColorByTheme(this.getTheme(), this.getColorMap())
    }

    return undefined
  }

  getDocumentBounds() {
    return this.dimension.bounds
  }

  translateBy(dx: number, dy: number) {
    this.dimension.translateBy(dx, dy)

    this.x += dx
    this.y += dy
    this.xLimit += dx
    this.yLimit += dy

    const lastRow = this.children.length - 1
    for (let ri = 0; ri <= lastRow; ri++) {
      const row = this.children.at(ri)!
      const cellsCount = row.cellsCount
      for (let ci = 0; ci < cellsCount; ci++) {
        const cell = row.getCellByIndex(ci)!
        if (VMergeType.Restart === cell.getVMerge()) {
          cell.translateContentBy(dx, dy)
        }
      }

      this.rowsInfo[ri].y += dy
      const rowBottomPos = this.tableRowsBottomPosition.at(ri)!
      this.tableRowsBottomPosition.set(ri, rowBottomPos + dy)
    }
  }

  resetDimension(x: number, y: number, xLimit: number, yLimit: number) {
    this.x = x
    this.y = y + 0.001
    this.xLimit = xLimit
    this.yLimit = yLimit

    this.dimension.reset(x, y, xLimit, yLimit)
  }

  getTextTransformOfParent() {
    return this.parent.getTextTransformOfParent()
  }

  isInPPT(id?: string) {
    let isUse = false
    if (null != id) {
      isUse =
        ManagedSliceUtil.findIndex(
          this.children,
          (item) => item.getId() === id
        ) !== -1
    } else {
      isUse = true
    }

    if (true === isUse && null != this.parent) {
      return isSlideElementInPPT(this.parent)
    }

    return false
  }

  /** 计算 cursorType 统一 return 至最上级处理*/
  getCursorInfoByPos(x: number, y: number): Nullable<HoverInfo> {
    if (
      true === this.selection.start ||
      TableSelectionType.BORDER === this.selection.selectionType
    ) {
      return
    }

    const hitInfo = hitInTableBorder(this, x, y)
    if (true === hitInfo.selectRow) {
      return { objectId: this.id, cursorType: cursorExtType.SelectTableRow }
    } else if (true === hitInfo.selectColum) {
      return { objectId: this.id, cursorType: cursorExtType.SelectTableColumn }
    } else if (true === hitInfo.selectCell) {
      return { objectId: this.id, cursorType: cursorExtType.SelectTableCell }
    } else if (-1 !== hitInfo.Border) {
      const transform = this.getTextTransformOfParent()!
      if (null !== transform) {
        const dX = Math.abs(
          transform.XFromPoint(0, 0) - transform.XFromPoint(0, 1)
        )
        const dY = Math.abs(
          transform.YFromPoint(0, 0) - transform.YFromPoint(0, 1)
        )

        if (Math.abs(dY) > Math.abs(dX)) {
          switch (hitInfo.Border) {
            case 0:
            case 2:
              return { objectId: this.id, cursorType: 'row-resize' }
            case 1:
            case 3:
              return { objectId: this.id, cursorType: 'col-resize' }
          }
        } else {
          switch (hitInfo.Border) {
            case 0:
            case 2:
              return { objectId: this.id, cursorType: 'col-resize' }
            case 1:
            case 3:
              return { objectId: this.id, cursorType: 'row-resize' }
          }
        }
      } else {
        switch (hitInfo.Border) {
          case 0:
          case 2:
            return { objectId: this.id, cursorType: 'row-resize' }
          case 1:
          case 3:
            return { objectId: this.id, cursorType: 'col-resize' }
        }
      }
    }

    const cellPos = this.getCellByPoint(x, y)
    const cell = this.children.at(cellPos.row)?.getCellByIndex(cellPos.cell)
    if (cell == null) return undefined

    const hoverInfo = cell.getCursorInfoByPos(x, y)
    // 返回 tableCell 的 Id 而非内部 paragraph 的 Id
    if (hoverInfo) {
      hoverInfo.objectId = cell.getId()
    }
    return hoverInfo
  }

  syncEditorUIState() {
    if (
      true !== this.selection.isUse ||
      TableSelectionType.CELL !== this.selection.type
    ) {
      this.curCell!.textDoc.syncEditorUIState()
    } else {
      const paraPr = this.getCalcedParaPr()
      if (null != paraPr) {
        updateParagraphProps(paraPr)
      }

      const textPr = this.getCalcedTextPr()
      if (null != textPr) {
        const theme = this.getTheme()
        if (theme && theme.themeElements && theme.themeElements.fontScheme) {
          const fontScheme = theme.themeElements.fontScheme
          if (textPr.fontFamily) {
            textPr.fontFamily.name = fontScheme.checkFont(
              textPr.fontFamily.name
            )
          }
          if (textPr.rFonts) {
            if (textPr.rFonts.ascii) {
              textPr.rFonts.ascii.name = fontScheme.checkFont(
                textPr.rFonts.ascii.name
              )
            }
            if (textPr.rFonts.eastAsia) {
              textPr.rFonts.eastAsia.name = fontScheme.checkFont(
                textPr.rFonts.eastAsia.name
              )
            }
            if (textPr.rFonts.hAnsi) {
              textPr.rFonts.hAnsi.name = fontScheme.checkFont(
                textPr.rFonts.hAnsi.name
              )
            }
            if (textPr.rFonts.cs) {
              textPr.rFonts.cs.name = fontScheme.checkFont(
                textPr.rFonts.cs.name
              )
            }
          }
        }
        updateTextPr(textPr)
      }
    }
  }

  selectSelf() {
    this.parent.selectSelf()
  }

  canSplitTableCells() {
    if (
      !(
        false === this.selection.isUse ||
        (true === this.selection.isUse &&
          (TableSelectionType.TEXT === this.selection.type ||
            (TableSelectionType.CELL === this.selection.type &&
              this.selection.cellPositions.length >= 1)))
      )
    ) {
      return false
    }

    return true
  }

  canMergeTableCells() {
    if (
      true !== this.selection.isUse ||
      TableSelectionType.CELL !== this.selection.type ||
      this.selection.cellPositions.length <= 1
    ) {
      return false
    }

    return this.getMergeInfo().isCanMerge
  }

  removeSelection() {
    if (false === this.selection.isUse) return

    if (this.children.length <= 0) {
      this.curCell = null
    } else {
      if (TableSelectionType.TEXT === this.selection.type) {
        this.curCell = this.children
          .at(this.selection.selectionStartInfo.cellPos.row)
          ?.getCellByIndex(this.selection.selectionStartInfo.cellPos.cell)
        if (this.curCell) {
          this.curCell!.textDoc.removeSelection()
        }
      } else if (
        this.children.length > 0 &&
        this.children.at(0)!.cellsCount > 0
      ) {
        this.curCell = this.children.at(0)!.getCellByIndex(0)!
        this.curCell.textDoc.removeSelection()
      }
    }

    this.selection.isUse = false
    this.selection.start = false

    this.selection.selectionStartInfo.cellPos = { row: 0, cell: 0 }
    this.selection.selectionEndInfo.cellPos = { row: 0, cell: 0 }
  }

  hasNoSelection(isCheckEnd?: boolean) {
    if (true === this.selection.isUse) {
      return TableSelectionType.CELL === this.selection.type
        ? false
        : this.curCell!.textDoc.hasNoSelection(isCheckEnd)
    }
    return true
  }

  selectAll(dir?: TextOperationDirection) {
    this.selection.isUse = true
    this.selection.start = false
    this.selection.type = TableSelectionType.CELL
    this.selection.selectionType = TableSelectionType.COMMON
    this.selection.selectionInfo = null

    const startPos = {
      row: 0,
      cell: 0,
    }
    const rowCount = this.children.length
    const endPos =
      rowCount > 0
        ? {
            row: rowCount - 1,
            cell: Math.max(this.children.at(rowCount - 1)!.cellsCount - 1, 0),
          }
        : {
            row: 0,
            cell: 0,
          }

    if (dir && dir < 0) {
      this.selection.selectionEndInfo.cellPos = startPos
      this.selection.selectionStartInfo.cellPos = endPos
    } else {
      this.selection.selectionStartInfo.cellPos = startPos
      this.selection.selectionEndInfo.cellPos = endPos
    }

    this.updateSelectedCellPositions()
  }

  applyTextDocFunction<T extends TextDocumentOperationFns>(
    func: T,
    args: TextDocumentOperationArgs<T>,
    isApplySelection: boolean = true,
    ignoreCalcState?: boolean
  ) {
    if (
      isApplySelection &&
      (true === this.isSetToAll ||
        (true === this.selection.isUse &&
          TableSelectionType.CELL === this.selection.type &&
          this.selection.cellPositions.length > 0))
    ) {
      const cellsPositions = this.getSelectedCellPositions()
      for (let i = 0; i < cellsPositions.length; i++) {
        const pos = cellsPositions[i]
        const row = this.children.at(pos.row)
        if (row) {
          const cell = row.getCellByIndex(pos.cell)
          if (cell) {
            const textDoc = cell.textDoc
            textDoc.isSetToAll = true
            func(textDoc, ...args)
            textDoc.isSetToAll = false
          }
        }
      }

      if (ignoreCalcState !== true) {
        calculateForRendering()
      }
    } else {
      func(this.curCell!.textDoc, ...args)
    }
  }

  remove(
    dir: TextOperationDirection,
    isRemoveOnlySelection?: boolean,
    isOnTextAdd?: boolean
  ) {
    if (
      true === this.isSetToAll ||
      (true === this.selection.isUse &&
        TableSelectionType.CELL === this.selection.type &&
        this.selection.cellPositions.length > 0)
    ) {
      const cellPositions = this.getSelectedCellPositions()

      if (true === isOnTextAdd && cellPositions.length > 0) {
        const cellPos = cellPositions[0]
        const cell = this.children.at(cellPos.row)?.getCellByIndex(cellPos.cell)
        if (cell) {
          cell.textDoc.selectAll()
          cell.textDoc.remove(dir, isRemoveOnlySelection, true)

          this.curCell = cell

          this.selection.isUse = false
          this.selection.start = false

          this.selection.selectionStartInfo.cellPos = {
            row: cell.parent.index,
            cell: cell.index,
          }
          this.selection.selectionEndInfo.cellPos = {
            row: cell.parent.index,
            cell: cell.index,
          }

          this.selectSelf()
          calculateForRendering()
        }
      } else {
        const cellPositions = this.getSelectedCellPositions()
        for (let i = 0; i < cellPositions.length; i++) {
          const pos = cellPositions[i]
          const cell = this.children.at(pos.row)?.getCellByIndex(pos.cell)

          if (cell) {
            const textDoc = cell.textDoc
            textDoc.isSetToAll = true
            cell.textDoc.remove(dir, isRemoveOnlySelection, false)
            textDoc.isSetToAll = false
          }
        }

        const cellPos = cellPositions[0]
        const cell = this.children.at(cellPos.row)?.getCellByIndex(cellPos.cell)
        if (cell) {
          this.curCell = cell

          this.selection.isUse = false
          this.selection.start = false

          this.selection.selectionStartInfo.cellPos = {
            row: cell.parent.index,
            cell: cell.index,
          }
          this.selection.selectionEndInfo.cellPos = {
            row: cell.parent.index,
            cell: cell.index,
          }

          calculateForRendering()
        }
      }
    } else {
      this.curCell!.textDoc.remove(dir, isRemoveOnlySelection, isOnTextAdd)

      if (false === this.curCell!.textDoc.hasTextSelection()) {
        const cell = this.curCell!

        this.selection.isUse = false
        this.selection.start = false

        this.selection.selectionStartInfo.cellPos = {
          row: cell.parent.index,
          cell: cell.index,
        }
        this.selection.selectionEndInfo.cellPos = {
          row: cell.parent.index,
          cell: cell.index,
        }
      }
    }
  }

  hasTextSelection() {
    if (
      (true === this.selection.isUse &&
        TableSelectionType.CELL === this.selection.type) ||
      TableSelectionType.BORDER === this.selection.selectionType
    ) {
      return true
    } else if (true === this.selection.isUse) {
      return this.curCell!.textDoc.hasTextSelection()
    }

    return false
  }

  getFirstParaPr() {
    let paraPr: Nullable<ParaPr>
    if (this.children.length <= 0) return paraPr

    if (true === this.isSetToAll || this.selection.isUse === false) {
      const row = this.children.at(0)!
      const cell = row.getCellByIndex(0)

      if (cell) {
        cell.textDoc.isSetToAll = true
        paraPr = cell.textDoc.getFirstParaPr()
        cell.textDoc.isSetToAll = false
      }

      return paraPr
    }

    if (
      true === this.selection.isUse &&
      TableSelectionType.CELL === this.selection.type &&
      this.selection.cellPositions.length > 0
    ) {
      const cellPos = this.selection.cellPositions[0]

      const cell = this.children.at(cellPos.row)?.getCellByIndex(cellPos.cell)

      if (cell) {
        cell.textDoc.isSetToAll = true
        paraPr = cell.textDoc.getFirstParaPr()
        cell.textDoc.isSetToAll = false
      }

      return paraPr
    }

    return this.curCell ? this.curCell.textDoc.getFirstParaPr() : paraPr
  }

  getCalcedParaPr() {
    let paraPr = ParaPr.new()
    if (this.children.length <= 0) return paraPr

    if (true === this.isSetToAll) {
      let row = this.children.at(0)!
      let cell = row.getCellByIndex(0)

      if (cell) {
        cell.textDoc.isSetToAll = true
        paraPr = cell.textDoc.getCalcedParaPr()
        cell.textDoc.isSetToAll = false
      }

      for (let ri = 0, l = this.children.length; ri < l; ri++) {
        row = this.children.at(ri)!
        const cellsCount = row.cellsCount
        const startCell = ri === 0 ? 1 : 0

        for (let ci = startCell; ci < cellsCount; ci++) {
          cell = row.getCellByIndex(ci)
          if (cell) {
            cell.textDoc.isSetToAll = true
            const curPr = cell.textDoc.getCalcedParaPr()
            cell.textDoc.isSetToAll = false

            paraPr = paraPr.intersect(curPr) ?? paraPr
          }
        }
      }

      return paraPr
    }

    if (
      true === this.selection.isUse &&
      TableSelectionType.CELL === this.selection.type &&
      this.selection.cellPositions.length > 0
    ) {
      let cellPos = this.selection.cellPositions[0]

      let cell = this.children.at(cellPos.row)?.getCellByIndex(cellPos.cell)

      if (cell) {
        cell.textDoc.isSetToAll = true
        paraPr = cell.textDoc.getCalcedParaPr()
        cell.textDoc.isSetToAll = false
      }

      for (let i = 1, l = this.selection.cellPositions.length; i < l; i++) {
        cellPos = this.selection.cellPositions[i]
        cell = this.children.at(cellPos.row)?.getCellByIndex(cellPos.cell)

        if (cell) {
          cell.textDoc.isSetToAll = true
          const curPr = cell.textDoc.getCalcedParaPr()
          cell.textDoc.isSetToAll = false

          paraPr = paraPr.intersect(curPr) ?? paraPr
        }
      }

      return paraPr
    }

    return this.curCell ? this.curCell.textDoc.getCalcedParaPr() : paraPr
  }

  getFirstParaTextPr() {
    let textPr: Nullable<TextPr>
    if (this.children.length <= 0) return textPr

    if (true === this.isSetToAll) {
      for (let ri = 0, l = this.children.length; ri < l; ri++) {
        const row = this.children.at(ri)!
        const cellsCount = row.cellsCount

        for (let ci = 0; ci < cellsCount; ci++) {
          const cell = row.getCellByIndex(ci)
          if (cell) {
            cell.textDoc.isSetToAll = true
            textPr = cell.textDoc.getFirstParaTextPr()
            cell.textDoc.isSetToAll = false
            if (textPr) {
              break
            }
          }
        }
        if (textPr) {
          break
        }
      }

      return textPr
    }

    if (
      true === this.selection.isUse &&
      TableSelectionType.CELL === this.selection.type &&
      this.selection.cellPositions.length > 0
    ) {
      let cellPos = this.selection.cellPositions[0]

      for (let i = 0, l = this.selection.cellPositions.length; i < l; i++) {
        cellPos = this.selection.cellPositions[i]
        const cell = this.children.at(cellPos.row)?.getCellByIndex(cellPos.cell)

        if (cell) {
          cell.textDoc.isSetToAll = true
          textPr = cell.textDoc.getFirstParaTextPr()
          cell.textDoc.isSetToAll = false
        }
        if (textPr) {
          break
        }
      }

      return textPr
    }

    return this.curCell ? this.curCell.textDoc.getCalcedTextPr() : textPr
  }

  getCalcedTextPr() {
    const textPr = TextPr.new()
    if (this.children.length <= 0) return textPr

    if (true === this.isSetToAll) {
      let cell = this.children.at(0)!.getCellByIndex(0)

      if (cell) {
        cell.textDoc.isSetToAll = true
        textPr.fromOptions(cell.textDoc.getCalcedTextPr())
        cell.textDoc.isSetToAll = false
      }

      for (let ri = 0, l = this.children.length; ri < l; ri++) {
        const row = this.children.at(ri)!
        const cellsCount = row.cellsCount
        const startCellIdx = ri === 0 ? 1 : 0

        for (let ci = startCellIdx; ci < cellsCount; ci++) {
          cell = row.getCellByIndex(ci)
          if (cell) {
            cell.textDoc.isSetToAll = true
            const curPr = cell.textDoc.getCalcedTextPr()
            cell.textDoc.isSetToAll = false

            textPr.subDiff(curPr)
          }
        }
      }

      return textPr
    }

    if (
      true === this.selection.isUse &&
      TableSelectionType.CELL === this.selection.type &&
      this.selection.cellPositions.length > 0
    ) {
      let cellPos = this.selection.cellPositions[0]
      let cell = this.children.at(cellPos.row)?.getCellByIndex(cellPos.cell)

      if (cell) {
        cell.textDoc.isSetToAll = true
        textPr.fromOptions(cell.textDoc.getCalcedTextPr())
        cell.textDoc.isSetToAll = false
      }

      for (let i = 1, l = this.selection.cellPositions.length; i < l; i++) {
        cellPos = this.selection.cellPositions[i]
        cell = this.children.at(cellPos.row)?.getCellByIndex(cellPos.cell)

        if (cell) {
          cell.textDoc.isSetToAll = true
          const curPr = cell.textDoc.getCalcedTextPr()
          cell.textDoc.isSetToAll = false

          textPr.subDiff(curPr)
        }
      }

      return textPr
    }

    return this.curCell ? this.curCell.textDoc.getCalcedTextPr() : textPr
  }

  invalidateCalcPrs(bubbleUp = true) {
    if (this.calcedPrs.isDirty === false) {
      this.calcedPrs.isDirty = true
      if (bubbleUp) {
        updateCalcStatusForGraphicFrameItem(this)
      }
    }
  }

  invalidateAllPrs(bubbleUp = true) {
    this.invalidateCalcPrs(bubbleUp)

    const rowsCount = this.children.length
    for (let ri = 0; ri < rowsCount; ri++) {
      const row = this.children.at(ri)!
      row.invalidateCalcedPrs(false)

      const cellsCount = row.cellsCount
      for (let ci = 0; ci < cellsCount; ci++) {
        const cell = row.getCellByIndex(ci)!
        cell.invalidateCalcedPrs(false)
      }
    }
  }

  /**
   * We form the final properties of the paragraph based on the style and direct settings.
   */
  getCalcedPr(isCopy?: boolean) {
    if (true === this.calcedPrs.isDirty) {
      if (this.calcedPrs.pr) {
        recycleTableStylePrs(this.calcedPrs.pr)
      }
      this.calcedPrs.pr = this.calcPr()
      this.calcedPrs.isDirty = false
    }

    if (false === isCopy) {
      return this.calcedPrs.pr!
    } else {
      const calcedPr = this.calcedPrs.pr!
      const pr: TableStylePrs = {
        textPr: calcedPr.textPr.clone(),
        paraPr: calcedPr.paraPr.clone(),

        tablePr: calcedPr.tablePr.clone(),
        tableRowPr: calcedPr.tableRowPr.clone(),
        tableCellPr: calcedPr.tableCellPr.clone(),

        tableFirstCol: calcedPr.tableFirstCol.clone(),
        tableFirstRow: calcedPr.tableFirstRow.clone(),
        tableLastCol: calcedPr.tableLastCol.clone(),
        tableLastRow: calcedPr.tableLastRow.clone(),
        tableBand1Horz: calcedPr.tableBand1Horz.clone(),
        tableBand1Vert: calcedPr.tableBand1Vert.clone(),
        tableBand2Horz: calcedPr.tableBand2Horz.clone(),
        tableBand2Vert: calcedPr.tableBand2Vert.clone(),
        tableNWCell: calcedPr.tableNWCell.clone(),
        tableNECell: calcedPr.tableNECell.clone(),
        tableSWCell: calcedPr.tableSWCell.clone(),
        tableSECell: calcedPr.tableSECell.clone(),
        tableWholeTable: calcedPr.tableWholeTable.clone(),
      }
      return pr
    }
  }

  getStyleId() {
    return this.tableStyleId ?? null
  }

  setStyle(id: string) {
    if (this.tableStyleId != null) {
      delete this.tableStyleId
    }
    this.calcedPrs.isDirty = true

    if (null == id) return

    if (id !== gGlobalTableStyles.tableStyles.getDefaultTableStyle()) {
      this.tableStyleId = id
    }

    this.calcedPrs.isDirty = true
  }

  /**
   * We form the final properties of the table based on the style and direct settings.
   */
  private calcPr() {
    const styles = gGlobalTableStyles.tableStyles
    const styleId = this.getStyleId()

    // Reading properties for the current style
    const pr = styles.getPr(styleId)
    this.refreshTheme(pr)

    // copy the direct settings of the paragraph.
    pr.tablePr.merge(this.pr)

    return pr
  }

  refreshTheme(pr: TableStylePrs) {
    const theme = this.getTheme()
    pr.tablePr.refreshTheme(theme)
    pr.textPr.refreshTheme()
    pr.tableCellPr.refreshTheme(theme)
    pr.tableFirstCol.refreshTheme(theme)
    pr.tableFirstRow.refreshTheme(theme)
    pr.tableLastCol.refreshTheme(theme)
    pr.tableLastRow.refreshTheme(theme)
    pr.tableBand1Horz.refreshTheme(theme)
    pr.tableBand1Vert.refreshTheme(theme)
    pr.tableBand2Horz.refreshTheme(theme)
    pr.tableBand2Vert.refreshTheme(theme)
    pr.tableNWCell.refreshTheme(theme)
    pr.tableNECell.refreshTheme(theme)
    pr.tableSWCell.refreshTheme(theme)
    pr.tableSECell.refreshTheme(theme)
  }

  clearFormatting(isRemoveMerge: boolean) {
    this.setRowBandSize(undefined)
    this.setColBandSize(undefined)
    this.setShd(undefined)
    this.setBottomTableBorder(undefined)
    this.setLeftTableBorder(undefined)
    this.setRightTableBorder(undefined)
    this.setTopTableBorder(undefined)
    this.setInsideVTableBorder(undefined)
    this.setInsideHTableBorder(undefined)
    this.setTableCellMargin(undefined, undefined, undefined, undefined)

    for (let i = 0, l = this.children.length; i < l; i++) {
      this.children.at(i)!.clearFormatting(isRemoveMerge)
    }
  }

  setPr(tablePr: TablePr) {
    if (tablePr === this.pr) return
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_Pr,
      this.pr,
      tablePr
    )
    recycleObject(this.pr, TablePr)
    this.pr = tablePr
    this.invalidateAllPrs()
  }

  setTableStyleId(
    styleId: Nullable<string | number>,
    isNoClearFormatting?: boolean
  ) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_TableStyle,
      this.tableStyleId,
      styleId
    )
    this.tableStyleId = styleId == null ? undefined : styleId + ''

    if (!(isNoClearFormatting === true)) {
      this.clearFormatting(false)
    }
    this.invalidateAllPrs()
  }

  getTableStyleId() {
    return this.tableStyleId
  }

  setTableLook(tableLook: TableLook) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_TableLook,
      this.tableLook,
      tableLook
    )
    this.tableLook = tableLook
    this.invalidateAllPrs()
  }

  getTableLook() {
    return this.tableLook
  }

  setRowBandSize(value: undefined | number) {
    if (this.pr.rowBandSize === value) return

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_TableStyleRowBandSize,
      this.pr.rowBandSize,
      value
    )
    this.pr.rowBandSize = value
    this.invalidateCalcPrs()
  }

  setColBandSize(v: undefined | number) {
    if (this.pr.colBandSize === v) return

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_TableStyleColBandSize,
      this.pr.colBandSize,
      v
    )
    this.pr.colBandSize = v
    this.invalidateCalcPrs()
  }

  setTableCellMargin(
    left: Nullable<number>,
    top: Nullable<number>,
    right: Nullable<number>,
    bottom: Nullable<number>
  ) {
    const orgLeftLen =
      null == this.pr.tableCellMargins.left
        ? undefined
        : this.pr.tableCellMargins.left
    const orgRightLen =
      null == this.pr.tableCellMargins.right
        ? undefined
        : this.pr.tableCellMargins.right
    const orgTopLen =
      null == this.pr.tableCellMargins.top
        ? undefined
        : this.pr.tableCellMargins.top
    const orgBottomLen =
      null == this.pr.tableCellMargins.bottom
        ? undefined
        : this.pr.tableCellMargins.bottom

    const leftLen =
      null == left ? undefined : new TableLength(TableLengthUnitType.MM, left)
    const rightLen =
      null == right ? undefined : new TableLength(TableLengthUnitType.MM, right)
    const topLen =
      null == top ? undefined : new TableLength(TableLengthUnitType.MM, top)
    const bottomLen =
      null == bottom
        ? undefined
        : new TableLength(TableLengthUnitType.MM, bottom)

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_TableCellMar,
      {
        left: orgLeftLen,
        right: orgRightLen,
        top: orgTopLen,
        bottom: orgBottomLen,
      },
      {
        left: leftLen,
        right: rightLen,
        top: topLen,
        bottom: bottomLen,
      }
    )

    this.pr.tableCellMargins.left = leftLen
    this.pr.tableCellMargins.right = rightLen
    this.pr.tableCellMargins.top = topLen
    this.pr.tableCellMargins.bottom = bottomLen

    this.invalidateCalcPrs()
  }

  getTableCellMargins() {
    const pr = this.getCalcedPr(false).tablePr
    return pr.tableCellMargins
  }

  setLeftTableBorder(borderOpts: Nullable<TableCellBorderOptions>) {
    if (null == this.pr.tableBorders.left && null == borderOpts) return

    const border = borderOpts ? TableCellBorder.new(borderOpts) : undefined

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_TableBorder_Left,
      this.pr.tableBorders.left,
      border
    )
    this.pr.tableBorders.left = border
    this.invalidateCalcPrs()
  }

  setRightTableBorder(borderOpts: Nullable<TableCellBorderOptions>) {
    if (null == this.pr.tableBorders.right && null == borderOpts) return

    const border = borderOpts ? TableCellBorder.new(borderOpts) : undefined

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_TableBorder_Right,
      this.pr.tableBorders.right,
      border
    )
    this.pr.tableBorders.right = border
    this.invalidateCalcPrs()
  }

  setTopTableBorder(borderOpts: Nullable<TableCellBorderOptions>) {
    if (null == this.pr.tableBorders.top && null == borderOpts) return

    const border = borderOpts ? TableCellBorder.new(borderOpts) : undefined

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_TableBorder_Top,
      this.pr.tableBorders.top,
      border
    )
    this.pr.tableBorders.top = border
    this.invalidateCalcPrs()
  }

  setBottomTableBorder(borderOpts: Nullable<TableCellBorderOptions>) {
    if (null == this.pr.tableBorders.bottom && null == borderOpts) {
      return
    }

    const border = borderOpts ? TableCellBorder.new(borderOpts) : undefined

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_TableBorder_Bottom,
      this.pr.tableBorders.bottom,
      border
    )
    this.pr.tableBorders.bottom = border
    this.invalidateCalcPrs()
  }

  setInsideHTableBorder(borderOpts: Nullable<TableCellBorderOptions>) {
    if (null == this.pr.tableBorders.insideH && null == borderOpts) {
      return
    }

    const border = borderOpts ? TableCellBorder.new(borderOpts) : undefined

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_TableBorder_InsideH,
      this.pr.tableBorders.insideH,
      border
    )
    this.pr.tableBorders.insideH = border
    this.invalidateCalcPrs()
  }

  setInsideVTableBorder(borderOpts: Nullable<TableCellBorderOptions>) {
    if (null == this.pr.tableBorders.insideV && null == borderOpts) {
      return
    }

    const border = borderOpts ? TableCellBorder.new(borderOpts) : undefined

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_TableBorder_InsideV,
      this.pr.tableBorders.insideV,
      border
    )
    this.pr.tableBorders.insideV = border
    this.invalidateCalcPrs()
  }

  getTableBordersInfo() {
    const pr = this.getCalcedPr(false).tablePr
    return pr.tableBorders
  }

  setShd(
    value: Nullable<valuesOfDict<typeof ShdType>>,
    r?: number,
    g?: number,
    b?: number
  ) {
    if (null == value && null == this.pr.shd) return

    let shd: CShd | undefined
    if (null != value) {
      shd = CShd.new({
        value: value,
      })
      shd.color.set(r, g, b)
    }

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_TableShd,
      this.pr.shd,
      shd
    )
    this.pr.shd = shd
    this.invalidateCalcPrs()
  }

  getShd() {
    const pr = this.getCalcedPr(false).tablePr
    return pr.shd
  }

  getTableBorders() {
    const tableBorders = this.getTableBordersInfo()
    if (tableBorders) {
      const theme = this.getTheme()
      const colorMap = this.getColorMap()
      const borderTypes = Object.keys(tableBorders)
      borderTypes.forEach((type) => {
        const border = tableBorders[type] as TableCellBorder
        if (border?.fillEffects) {
          if (theme && colorMap) {
            border.fillEffects.check(theme, colorMap)
            const { r, g, b } = border.fillEffects.getRGBAColor()
            border.color.set(r, g, b)
          }
        }
      })
    }
    return tableBorders
  }

  getMergeInfo(): {
    gridStart: number
    gridEnd: number
    rowsInfo: Array<{ gridStart: number; gridEnd: number }>
    isCanMerge: boolean
  } {
    let isCanMerge = true

    let gridStart = -1
    let gridEnd = -1

    const rowsInfo: Array<{ gridStart: number; gridEnd: number }> = []
    let rowMinIndex = -1
    let rowMaxIndex = -1

    this.selection.cellPositions.forEach((cellPos) => {
      const row = this.children.at(cellPos.row)
      if (row == null) return

      const cell = row.getCellByIndex(cellPos.cell)
      if (cell == null) return

      const cellInfo = row.getCellInfo(cellPos.cell)
      const startGridCol = cellInfo.startGridCol
      const lastGridCol = startGridCol + cell.getGridSpan() - 1

      const verticalMergeCount = this.getVMergeCountBelow(
        cellPos.row,
        cellInfo.startGridCol,
        cell.getGridSpan()
      )

      for (
        let ri = cellPos.row;
        ri <= cellPos.row + verticalMergeCount - 1;
        ri++
      ) {
        if (rowsInfo[ri] == null) {
          rowsInfo[ri] = {
            gridStart: startGridCol,
            gridEnd: lastGridCol,
          }

          if (-1 === rowMaxIndex || ri > rowMaxIndex) {
            rowMaxIndex = ri
          }

          if (-1 === rowMinIndex || ri < rowMinIndex) {
            rowMinIndex = ri
          }
        } else {
          if (startGridCol < rowsInfo[ri].gridStart) {
            rowsInfo[ri].gridStart = startGridCol
          }

          if (lastGridCol > rowsInfo[ri].gridEnd) {
            rowsInfo[ri].gridEnd = lastGridCol
          }
        }
      }
    })

    for (let ri = rowMinIndex; ri <= rowMaxIndex; ++ri) {
      if (!rowsInfo[ri]) {
        isCanMerge = false
        break
      }
    }

    for (const i in rowsInfo) {
      if (-1 === gridStart) {
        gridStart = rowsInfo[i].gridStart
      } else if (gridStart !== rowsInfo[i].gridStart) {
        isCanMerge = false
        break
      }

      if (-1 === gridEnd) gridEnd = rowsInfo[i].gridEnd
      else if (gridEnd !== rowsInfo[i].gridEnd) {
        isCanMerge = false
        break
      }
    }

    if (true === isCanMerge) {
      let topRow = -1
      let bottomRow = -1

      for (let gridIndex = gridStart; gridIndex <= gridEnd; gridIndex++) {
        let topCellPos: Nullable<CellPos>
        let bottomCellPos: Nullable<CellPos>
        for (let i = 0; i < this.selection.cellPositions.length; i++) {
          const cellPos = this.selection.cellPositions[i]
          const row = this.children.at(cellPos.row)
          if (row == null) continue

          const cell = row.getCellByIndex(cellPos.cell)
          if (cell == null) continue

          const startGridCol = row.getCellInfo(cellPos.cell).startGridCol
          const lastGridCol = startGridCol + cell.getGridSpan() - 1

          if (gridIndex >= startGridCol && gridIndex <= lastGridCol) {
            if (null == topCellPos || topCellPos.row > cellPos.row) {
              topCellPos = cellPos
            }

            if (null == bottomCellPos || bottomCellPos.row < cellPos.row) {
              bottomCellPos = cellPos
            }
          }
        }

        if (null == topCellPos || null == bottomCellPos) {
          isCanMerge = false
          break
        }

        if (-1 === topRow) topRow = topCellPos.row
        else if (topRow !== topCellPos.row) {
          isCanMerge = false
          break
        }

        const row = this.children.at(bottomCellPos.row)
        if (row == null) continue

        const cell = row.getCellByIndex(bottomCellPos.cell)
        if (cell == null) continue

        const verticalMergeCount = this.getVMergeCountBelow(
          bottomCellPos.row,
          row.getCellInfo(bottomCellPos.cell).startGridCol,
          cell.getGridSpan()
        )
        const curBottomRowIndex = bottomCellPos.row + verticalMergeCount - 1

        if (-1 === bottomRow) bottomRow = curBottomRowIndex
        else if (bottomRow !== curBottomRowIndex) {
          isCanMerge = false
          break
        }
      }
    }

    return {
      gridStart: gridStart,
      gridEnd: gridEnd,
      rowsInfo: rowsInfo,
      isCanMerge: isCanMerge,
    }
  }

  getCellByPoint(x: number, y: number) {
    const dimension = this.dimension
    const colsCount = this.tableGridCalculated.length
    const reducedTableGrid = this.getTableGridReduced()

    const twX = convertMMToTwips(x)
    const twDimensionX = convertMMToTwips(dimension.x)
    let gridIndex = 0
    if (twX >= twDimensionX) {
      for (gridIndex = 0; gridIndex < colsCount; gridIndex++) {
        const colStartTwips = convertMMToTwips(
          dimension.x + reducedTableGrid[gridIndex - 1]
        )
        const colEndTwips = convertMMToTwips(
          dimension.x + reducedTableGrid[gridIndex]
        )
        if (colStartTwips <= twX && twX < colEndTwips) break
      }
    }

    if (this.isRTL) {
      gridIndex = colsCount - 1 - gridIndex
    }

    if (gridIndex >= colsCount) gridIndex = colsCount - 1

    const lastIndex = this.children.length - 1
    for (let ri = 0; ri <= lastIndex; ri++) {
      const row = this.children.at(ri)!
      const cellsCount = row.cellsCount
      let curGridColIndex = 0
      for (let ci = 0; ci < cellsCount; ci++) {
        const cell = row.getCellByIndex(ci)!
        const gridSpan = cell.getGridSpan()
        const vMerge = cell.getVMerge()

        if (VMergeType.Continue === vMerge && 0 !== ri) {
          curGridColIndex += gridSpan
          continue
        }

        const verticalMergeCount = this.getVertMergeCount(
          ri,
          curGridColIndex,
          gridSpan
        )

        if (verticalMergeCount <= 0) {
          curGridColIndex += gridSpan
          continue
        }

        if (
          gridIndex >= curGridColIndex &&
          gridIndex < curGridColIndex + gridSpan
        ) {
          if (
            null != this.rowsInfo[ri + verticalMergeCount - 1].y &&
            null != this.rowsInfo[ri + verticalMergeCount - 1].h &&
            (y <=
              this.rowsInfo[ri + verticalMergeCount - 1].y +
                this.rowsInfo[ri + verticalMergeCount - 1].h ||
              ri + verticalMergeCount - 1 >= lastIndex)
          ) {
            if (VMergeType.Continue === vMerge && 0 === ri) {
              const cell = this._getTopLeftMergedCell(
                ri,
                curGridColIndex,
                gridSpan
              )
              if (null != cell) {
                return { row: cell.parent.index, cell: cell.index }
              } else {
                return { row: 0, cell: 0 }
              }
            } else {
              return { row: ri, cell: ci }
            }
          }
        }

        curGridColIndex += gridSpan
      }
    }

    return { row: 0, cell: 0 }
  }

  private getGridPosByXY(
    x: number,
    y: number
  ): { row: number; gridCol: number } {
    const reducedTableGrid = this.getTableGridReduced()
    const dimension = this.dimension
    const colsCount = this.tableGridCalculated.length
    const rowsCount = this.children.length
    let curGrid = 0
    let curRow = 0

    const twX = convertMMToTwips(x)
    const twDimensionX = convertMMToTwips(dimension.x)
    if (twX >= twDimensionX) {
      for (curGrid = 0; curGrid < colsCount; curGrid++) {
        const twipColStart = convertMMToTwips(
          dimension.x + reducedTableGrid[curGrid - 1]
        )
        const twipColEnd = convertMMToTwips(
          dimension.x + reducedTableGrid[curGrid]
        )
        if (twipColStart <= twX && twX < twipColEnd) break
      }
    }
    if (curGrid >= colsCount) {
      curGrid = colsCount - 1
    }

    for (curRow = 0; curRow < rowsCount; curRow++) {
      const rowInfo = this.rowsInfo[curRow]
      if (rowInfo.y <= y && y < rowInfo.y + rowInfo.h) {
        break
      }
    }
    if (curRow >= rowsCount) {
      curRow = rowsCount - 1
    }

    return { row: curRow, gridCol: curGrid }
  }

  getVMergeCountBelow(
    startRowIndex: number,
    startGridCol: number,
    gridSpan: number
  ) {
    let verticalMergeCount = 1
    const rows = this.children
    // 遍历起始 cell 下方的所有行
    for (
      let index = Math.max(startRowIndex + 1, 0), l = rows.length;
      index < l;
      index++
    ) {
      const row = rows.at(index)!
      const cellsCount = row.cellsCount
      let curGridColIndex = 0
      let curCell = 0
      let wasMerged = false
      // 找到 startGridCol 处的 cell, 叠加 count
      while (curGridColIndex <= startGridCol && curCell < cellsCount) {
        const cell = row.getCellByIndex(curCell)!
        const gridSpanOfCell = cell.getGridSpan()
        const vMerge = cell.getVMerge()
        const isTargetCell =
          curGridColIndex === startGridCol && gridSpan === gridSpanOfCell

        if (isTargetCell && VMergeType.Continue === vMerge) {
          wasMerged = true
          verticalMergeCount++
          break
        } else if (isTargetCell && VMergeType.Continue !== vMerge) {
          wasMerged = true
          return verticalMergeCount
        } else if (
          curGridColIndex <= startGridCol + gridSpan - 1 &&
          curGridColIndex + gridSpanOfCell - 1 >= startGridCol
        ) {
          break
        }
        curGridColIndex += gridSpanOfCell
        curCell++
      }
      if (false === wasMerged) break
    }
    return verticalMergeCount
  }

  getVMergeCountAbove(
    startRowIndex: number,
    startGridCol: number,
    gridSpan: number
  ) {
    let vmergeCount = 1

    const rowCount = this.children.length
    if (rowCount <= 0) return vmergeCount

    const startRow = this.children.at(startRowIndex)!
    if (startRow == null) return vmergeCount

    let startVMergeType: VMergeTypeValues = VMergeType.Restart
    const l = startRow.cellsCount
    for (let i = 0; i < l; i++) {
      const _startGridCol = startRow.getCellInfo(i).startGridCol
      if (_startGridCol === startGridCol) {
        startVMergeType = startRow.getCellByIndex(i)!.getVMerge()!
        break
      }
    }

    if (VMergeType.Restart === startVMergeType) return vmergeCount

    for (let i = Math.min(startRowIndex - 1, rowCount - 1); i >= 0; i--) {
      const row = this.children.at(i)!
      let curGridColIndex = 0
      let curCellIndex = 0
      const cellsCount = row.cellsCount

      let wasMerged = false
      while (curGridColIndex <= startGridCol && curCellIndex < cellsCount) {
        const cell = row.getCellByIndex(curCellIndex)!
        const gridSpanOfCell = cell.getGridSpan()
        const vMerge = cell.getVMerge()

        if (
          curGridColIndex === startGridCol &&
          gridSpan === gridSpanOfCell &&
          VMergeType.Continue === vMerge
        ) {
          wasMerged = true
          vmergeCount++
          break
        } else if (
          curGridColIndex === startGridCol &&
          gridSpan === gridSpanOfCell &&
          VMergeType.Continue !== vMerge
        ) {
          wasMerged = true
          vmergeCount++
          return vmergeCount
        } else if (
          curGridColIndex <= startGridCol + gridSpan - 1 &&
          curGridColIndex + gridSpanOfCell - 1 >= startGridCol
        ) {
          break
        }

        curGridColIndex += gridSpanOfCell
        curCellIndex++
      }

      if (false === wasMerged) break
    }

    return vmergeCount
  }

  removeRow(rowIndex: number) {
    if (rowIndex >= this.children.length || rowIndex < 0) return
    const rowRemoved = this.children.at(rowIndex)!
    rowRemoved.beforeDelete()

    informEntityChildrenChange(
      this,
      EditActionFlag.edit_Table_RemoveRow,
      rowIndex,
      [rowRemoved],
      false
    )

    this.children.recycle(this.children.remove(rowIndex, 1))
    this.tableRowsBottomPosition.remove(rowIndex, 1)
    this.rowsInfo.splice(rowIndex, 1)

    this.updateRowIndices(rowIndex)

    updateCalcStatusForGraphicFrameItem(this)
  }

  addRow(
    rowIndex: number,
    cellsCount: number,
    isUpdateIndices?: boolean,
    newRow?: TableRow
  ): TableRow {
    if (rowIndex < 0) rowIndex = 0

    if (rowIndex >= this.children.length) rowIndex = this.children.length

    newRow = newRow == null ? TableRow.new(this, cellsCount) : newRow

    informEntityChildrenChange(
      this,
      EditActionFlag.edit_Table_AddRow,
      rowIndex,
      [newRow],
      true
    )

    this.children.insert(rowIndex, newRow)
    this.tableRowsBottomPosition.insert(rowIndex, 0)
    this.rowsInfo.splice(rowIndex, 0, new TableRowsInfo())

    if (true === isUpdateIndices) {
      this.updateRowIndices(rowIndex)
    } else {
      if (rowIndex > 0) {
        const prevRow = this.children.at(rowIndex - 1)!
        prevRow.next = newRow
        newRow.prev = prevRow
      } else newRow.prev = null

      if (rowIndex < this.children.length - 1) {
        const nextRow = this.children.at(rowIndex + 1)!
        nextRow.prev = newRow
        newRow.next = nextRow
      } else newRow.next = null
    }

    newRow.parent = this

    updateCalcStatusForGraphicFrameItem(this)
    return newRow
  }

  updateIndices() {
    this.updateRowIndices(0)
    ManagedSliceUtil.forEach(this.children, (row) => {
      row.updateCellIndices()
    })
  }

  createTableGrid(rowsInfo: Array<RowInfo[]>) {
    const indices: number[] = []
    const arrOfX: number[] = []
    for (let i = 0; i < rowsInfo.length; i++) {
      indices[i] = 0
      arrOfX[i] = rowsInfo[i][0].w

      for (let j = 0; j < rowsInfo[i].length; j++) {
        rowsInfo[i][j].gridSpan = 1

        if (1 !== rowsInfo[i][rowsInfo[i].length - 1].type) {
          rowsInfo[i].push({ w: 0, type: 1, gridSpan: 0 })
        } else {
          rowsInfo[i][rowsInfo[i].length - 1] = {
            w: 0,
            type: 1,
            gridSpan: 0,
          }
        }
      }
    }

    const tableGrid: number[] = []
    let isEnd = false
    let prevX = 0
    while (true !== isEnd) {
      let minX = -1
      for (let i = 0; i < rowsInfo.length; i++) {
        if (
          (minX === -1 || arrOfX[i] < minX) &&
          !(
            rowsInfo[i].length - 1 === indices[i] &&
            1 === rowsInfo[i][indices[i]].type
          )
        ) {
          minX = arrOfX[i]
        }
      }

      for (let i = 0; i < rowsInfo.length; i++) {
        if (
          rowsInfo[i].length - 1 === indices[i] &&
          1 === rowsInfo[i][indices[i]].type
        ) {
          rowsInfo[i][indices[i]].gridSpan++
        } else {
          if (Math.abs(minX - arrOfX[i]) < 0.001) {
            indices[i]++
            arrOfX[i] += rowsInfo[i][indices[i]].w
          } else {
            rowsInfo[i][indices[i]].gridSpan++
          }
        }
      }

      tableGrid.push(minX - prevX)
      prevX = minX

      isEnd = true
      for (let i = 0; i < rowsInfo.length; i++) {
        if (rowsInfo[i].length - 1 !== indices[i]) {
          isEnd = false
          break
        }
      }
    }

    for (let i = 0; i < rowsInfo.length; i++) {
      const rowInfo = rowsInfo[i]
      const row = this.children.at(i)

      let curIndex = 0
      if (-1 === rowInfo[0].type) {
        curIndex++
      }

      for (let ci = 0; curIndex < rowInfo.length; curIndex++, ci++) {
        if (1 === rowInfo[curIndex].type) break

        const cell = row?.getCellByIndex(ci)
        if (cell) {
          cell.setGridSpan(rowInfo[curIndex].gridSpan)
        }
      }

      curIndex = rowInfo.length - 1
    }
    this.setTableGrid(tableGrid)
    return tableGrid
  }

  _getTopLeftMergedCell(
    startRowIndex: number,
    startGridCol: number,
    gridSpan: number
  ) {
    const rowCount = this.children.length
    if (rowCount <= 0) return undefined
    let result: TableCell | undefined
    for (let i = Math.min(startRowIndex, rowCount - 1); i >= 0; i--) {
      const row = this.children.at(i)!
      let curGridColIndex = 0
      let curCellIndex = 0
      const cellsCount = row.cellsCount

      let isMerged = false
      while (curGridColIndex <= startGridCol && curCellIndex < cellsCount) {
        const cell = row.getCellByIndex(curCellIndex)!
        const gridSpanOfCell = cell.getGridSpan()
        const vMerge = cell.getVMerge()

        if (
          curGridColIndex === startGridCol &&
          gridSpan === gridSpanOfCell &&
          VMergeType.Continue === vMerge
        ) {
          isMerged = true
          result = cell
          break
        } else if (
          curGridColIndex === startGridCol &&
          gridSpan === gridSpanOfCell &&
          VMergeType.Continue !== vMerge
        ) {
          isMerged = true
          result = cell
          return result
        }
        // If the given cell has an intersection with a given interval, but does not completely coincide with it
        else if (
          curGridColIndex <= startGridCol + gridSpan - 1 &&
          curGridColIndex + gridSpanOfCell - 1 >= startGridCol
        ) {
          break
        }

        curGridColIndex += gridSpanOfCell
        curCellIndex++
      }

      if (false === isMerged) break
    }

    return result
  }

  _getEndMergedCell(
    startRowIndex: number,
    startGridCol: number,
    gridSpan: number
  ) {
    if (this.children.length <= 0) return undefined

    let result: undefined | TableCell
    for (
      let i = Math.max(startRowIndex, 0), l = this.children.length;
      i < l;
      i++
    ) {
      const row = this.children.at(i)!
      let curGridColIndex = 0
      let curCellIndex = 0
      const cellsCount = row.cellsCount

      let isMerged = false
      while (curGridColIndex <= startGridCol && curCellIndex < cellsCount) {
        const cell = row.getCellByIndex(curCellIndex)!
        const gridSpanOfCell = cell.getGridSpan()
        const vMerge = cell.getVMerge()

        if (curGridColIndex === startGridCol && gridSpan === gridSpanOfCell) {
          if (VMergeType.Continue === vMerge || i === startRowIndex) {
            isMerged = true
            result = cell
            break
          } else return result
        }
        // If the given cell has an intersection with a given interval, but does not completely coincide with it
        else if (
          curGridColIndex <= startGridCol + gridSpan - 1 &&
          curGridColIndex + gridSpanOfCell - 1 >= startGridCol
        ) {
          break
        }

        curGridColIndex += gridSpanOfCell
        curCellIndex++
      }

      if (false === isMerged) break
    }

    return result
  }

  getTopLeftMergedCell(cellIndex: number, rowIndex: number) {
    const row = this.children.at(rowIndex)
    if (row == null) return undefined

    const cell = row.getCellByIndex(cellIndex)
    const cellInfo = row.getCellInfo(cellIndex)

    return cell
      ? this._getTopLeftMergedCell(
          rowIndex,
          cellInfo.startGridCol,
          cell.getGridSpan()
        )
      : undefined
  }

  getVMergeNum(cellIndex: number, rowIndex: number) {
    const row = this.getRow(rowIndex)
    if (!row) return 1

    const cell = row.getCellByIndex(cellIndex)
    if (!cell) return 1

    const cellInfo = row.getCellInfo(cellIndex)
    if (!cellInfo) return 1

    return this.getVMergeCountBelow(
      rowIndex,
      cellInfo.startGridCol,
      cell.getGridSpan()
    )
  }

  /**
   * We get the cell number in a given row for a given column
   * @param {number} rowIndex
   * @param {number} startGridColIndex
   * @param {boolean} [canOverlap = false] true - we are looking for the cell in which this column began, false - we are looking for a cell that strictly began with the given column
   * @returns {number} Return -1 if no cell found
   */
  getCellIndexByGridColIndex(
    rowIndex: number,
    startGridColIndex: number,
    canOverlap?: boolean
  ): number {
    const row = this.getRow(rowIndex)
    if (!row) return -1

    let curGridColIndex = 0
    if (canOverlap) {
      for (let ci = 0, l = row.cellsCount; ci < l; ++ci) {
        if (startGridColIndex === curGridColIndex) return ci
        else if (curGridColIndex > startGridColIndex) {
          return ci - 1
        }

        const cell = row.getCellByIndex(ci)!
        curGridColIndex += cell.getGridSpan()
      }

      return row.cellsCount - 1
    } else {
      for (let ci = 0, l = row.cellsCount; ci < l; ++ci) {
        if (startGridColIndex === curGridColIndex) return ci
        else if (curGridColIndex > startGridColIndex) return -1

        const cell = row.getCellByIndex(ci)!
        curGridColIndex += cell.getGridSpan()
      }
    }

    return -1
  }

  /**
   * Updating the array of selected cells
   * @param isForceSelectByLines whether to use line selection
   */
  updateSelectedCellPositions(
    isForceSelectByLines = false,
    calcSelectionXY = true
  ) {
    this.selection.type = TableSelectionType.CELL
    this.selection.cellPositions = []

    if (!isForceSelectByLines) {
      let {
        x: startX,
        y: startY,
        cellPos: startCellPos,
      } = this.selection.selectionStartInfo
      let {
        x: endX,
        y: endY,
        cellPos: endCellPos,
      } = this.selection.selectionEndInfo

      // Selection中未计算过XY位置情况
      if (calcSelectionXY) {
        const startRow = this.children.at(startCellPos.row)
        if (startRow == null) return

        const endRow = this.children.at(endCellPos.row)
        if (endRow == null) return

        const startRowInfo = this.rowsInfo[startCellPos.row]
        const startCellInfo = startRow.getCellInfo(startCellPos.cell)
        startY = startRowInfo.y + 0.1
        startX = startCellInfo.cellStartX + 0.1
        const endRowInfo = this.rowsInfo[endCellPos.row]
        const endCellInfo = endRow.getCellInfo(endCellPos.cell)
        endY = endRowInfo.y + endRowInfo.h - 0.1
        endX = endCellInfo.cellEndX - 0.1
      }
      let { row: startRow, gridCol: startGridCol } = this.getGridPosByXY(
        startX,
        startY
      )
      let { row: endRow, gridCol: lastGridCol } = this.getGridPosByXY(
        endX,
        endY
      )

      if (endRow < startRow) {
        const t = startRow
        startRow = endRow
        endRow = t
      }
      if (lastGridCol < startGridCol) {
        const t = startGridCol
        startGridCol = lastGridCol
        lastGridCol = t
      }

      const temPosMap = {}
      for (let ri = startRow; ri <= endRow; ri++) {
        const row = this.children.at(ri)
        if (row) {
          let curGridColIndex = 0
          rowContentIterator(this.isRTL, row, (cell) => {
            const gridSpan = cell.getGridSpan()
            if (
              (curGridColIndex >= startGridCol &&
                curGridColIndex <= lastGridCol) ||
              (curGridColIndex + gridSpan - 1 >= startGridCol &&
                curGridColIndex + gridSpan - 1 <= lastGridCol)
            ) {
              const mergedCellOfTopLeft = this.getTopLeftMergedCell(
                cell.index,
                ri
              )!
              const isNewCellPos =
                !temPosMap[
                  `${mergedCellOfTopLeft.parent.index}-${mergedCellOfTopLeft.index}`
                ]
              if (isNewCellPos) {
                const row = mergedCellOfTopLeft.parent.index
                const cell = mergedCellOfTopLeft.index
                this.selection.cellPositions.push({
                  row: row,
                  cell: cell,
                })
                temPosMap[`${row}-${cell}`] = true
              }
            }

            curGridColIndex += gridSpan
          })
        }
      }
    } else {
      const rowsCount = this.children.length

      let startRow = Math.min(
        Math.max(0, this.selection.selectionStartInfo.cellPos.row),
        rowsCount - 1
      )
      let endRow = Math.min(
        Math.max(0, this.selection.selectionEndInfo.cellPos.row),
        rowsCount - 1
      )

      if (endRow < startRow) {
        const t = startRow
        startRow = endRow
        endRow = t
      }

      for (let ri = startRow; ri <= endRow; ri++) {
        const row = this.children.at(ri)!
        ManagedSliceUtil.forEach(row.children, (cell) => {
          if (cell.getVMerge() !== VMergeType.Continue) {
            this.selection.cellPositions.push({ row: ri, cell: cell.index })
          }
        })
      }
    }

    if (this.selection.cellPositions.length > 1) {
      this.selection.cellPositions.sort(
        (pos, pos1) => pos.row - pos1.row || pos.cell - pos1.cell
      )
      this.selection.currentRowIndex =
        this.selection.cellPositions[
          this.selection.cellPositions.length - 1
        ].row
    }

    if (true === this.selection.isUse && false === this.selection.start) {
      const paraPr = this.getCalcedParaPr()
      if (null != paraPr) {
        updateParagraphProps(paraPr)
      }

      const textPr = this.getCalcedTextPr()
      if (null != textPr) {
        updateTextPr(textPr)
      }
    }
  }

  _intersectionOfCellBorders(
    border1: TableCellBorder,
    border2: TableCellBorder
  ) {
    const result: TableCellBorderOptions = {}

    if (border1.value === border2.value) result.value = border1.value

    if (border1.size === border2.size) result.size = border1.size

    if (isObjectsTheSame(border1.color, border2.color)) {
      result.color = border1.color
    }

    if (isObjectsTheSame(border1.fillEffects, border2.fillEffects)) {
      result.fillEffects = border1.fillEffects
    }

    return TableCellBorder.new(result)
  }

  getSelectedCellPositions() {
    let cellsPos: CellPos[]
    if (true === this.isSetToAll) {
      cellsPos = []
      for (
        let rowIndex = 0, l = this.children.length;
        rowIndex < l;
        rowIndex++
      ) {
        const row = this.children.at(rowIndex)!
        for (let ci = 0; ci < row.cellsCount; ci++) {
          const cell = row.getCellByIndex(ci)!
          const vmerge = cell.getVMerge()

          if (VMergeType.Continue === vmerge) continue

          cellsPos.push({ cell: ci, row: rowIndex })
        }
      }
    } else if (
      true === this.selection.isUse &&
      TableSelectionType.CELL === this.selection.type
    ) {
      cellsPos = this.selection.cellPositions
    } else {
      cellsPos = [
        { cell: this.curCell!.index, row: this.curCell!.parent.index },
      ]
    }

    return cellsPos
  }

  getVertMergeCount(rowIndex: number, startGridCol: number, gridSpan: number) {
    let vMergeCount = this.getVMergeCountBelow(rowIndex, startGridCol, gridSpan)
    const lastRow = this.children.length - 1
    if (rowIndex + vMergeCount - 1 >= lastRow) {
      vMergeCount = lastRow + 1 - rowIndex
    }

    return vMergeCount
  }

  _getSelectedRowsIndexRange() {
    const cellsPos = this.getSelectedCellPositions()
    let startIndex = -1
    let endIndex = -2
    cellsPos.forEach((cellPos) => {
      const rowIndex = cellPos.row
      if (-1 === startIndex || startIndex > rowIndex) {
        startIndex = rowIndex
      }

      if (-1 === endIndex || endIndex < rowIndex) {
        endIndex = rowIndex
      }
    })

    return {
      startIndex,
      endIndex,
    }
  }

  getRow(index: number) {
    return this.children.at(index)
  }

  getRowsCount() {
    return this.children.length
  }

  startSelectFromCurrentPosition() {
    this.selection.isUse = true

    this.selection.selectionStartInfo.cellPos = {
      cell: this.curCell!.index,
      row: this.curCell!.parent.index,
    }
    this.selection.selectionEndInfo.cellPos = {
      cell: this.curCell!.index,
      row: this.curCell!.parent.index,
    }
    this.updateSelectedCellPositions()

    const presentation = getPresentation(this)
    if (presentation) {
      const realPos = presentation.getCursorPosition()
      this.selection.selectionStartInfo.x = realPos.x
      this.selection.selectionStartInfo.y = realPos.y
    }

    this.selection.type = TableSelectionType.TEXT
    this.selection.currentRowIndex = this.curCell!.parent.index

    this.curCell!.textDoc.startSelectFromCurrentPosition()
  }

  isSelectedAll() {
    if (true !== this.selection.isUse) return false

    let index = 0
    const cellPositions = this.selection.cellPositions
    for (let rIdx = 0, rc = this.children.length; rIdx < rc; rIdx++) {
      const row = this.children.at(rIdx)!
      for (let cIdx = 0, cc = row.cellsCount; cIdx < cc; cIdx++, index++) {
        if (index >= cellPositions.length) return false

        const cellPos = cellPositions[index]
        if (cellPos.row !== rIdx || cellPos.cell !== cIdx) return false
      }
    }

    return true
  }

  getIndex() {
    if (!this.parent) return -1

    return this.index
  }

  setTableGrid(grids: Array<number>) {
    let isChanged = false
    if (grids.length !== this.tableGrid.length) {
      isChanged = true
    } else {
      for (
        let valueOfIndex = 0, valueOfCount = grids.length;
        valueOfIndex < valueOfCount;
        ++valueOfIndex
      ) {
        if (
          Math.abs(grids[valueOfIndex] - this.tableGrid[valueOfIndex]) > 0.001
        ) {
          isChanged = true
          break
        }
      }
    }

    if (!isChanged) return

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_Table_TableGrid,
      this.tableGrid,
      grids
    )
    this.tableGrid = grids
    updateCalcStatusForGraphicFrameItem(this)
  }

  resize(width: number, height: number) {
    if (this.dimension.isEmptyBounds()) return
    const minWidth = this._getMinWidth()
    const minHeight = this._getMinHeight()
    const totalHeight = this._getWholeHeight()

    const bounds = this.getDocumentBounds()
    let diffX = width - bounds.right + bounds.left
    let diffY = height - bounds.bottom + bounds.top

    const totalWidth = bounds.right - bounds.left

    if (totalWidth + diffX < minWidth) diffX = minWidth - totalWidth

    if (totalHeight + diffY < minHeight) {
      diffY = minHeight - totalHeight
    }

    if (diffY > 0.01) {
      const heightOfRows: number[] = []
      let tableSumH = 0

      for (let ri = 0, l = this.getRowsCount(); ri < l; ++ri) {
        const rowTotalHeight = this.rowsInfo[ri].h
        heightOfRows[ri] = rowTotalHeight
        tableSumH += rowTotalHeight
      }

      for (let ri = 0, l = this.children.length; ri < l; ++ri) {
        const row = this.children.at(ri)!
        const rowHeight = row.getHeight()
        let newRowHeight =
          (heightOfRows[ri] / tableSumH) * (totalHeight + diffY)
        if (this.rowsInfo[ri] && this.rowsInfo[ri].diffYOfTop) {
          newRowHeight -= this.rowsInfo[ri].diffYOfTop
        }

        let topMargin = 0,
          botMargin = 0
        for (let ci = 0, cellsCount = row.cellsCount; ci < cellsCount; ++ci) {
          const cell = row.getCellByIndex(ci)!
          const margins = cell.getMargins()

          if (margins.top!.w > topMargin) topMargin = margins.top!.w

          if (margins.bottom!.w > botMargin) botMargin = margins.bottom!.w
        }

        newRowHeight -= topMargin + botMargin

        row.setHeight({
          value: newRowHeight,
          heightRule:
            rowHeight.heightRule === LineHeightRule.Exact
              ? LineHeightRule.Exact
              : LineHeightRule.AtLeast,
        })
      }
    } else if (diffY < -0.01) {
      let newTableH = totalHeight + diffY
      const minHeightOfRows: number[] = []
      const heightOfRows: number[] = []
      let tableTotalHeight = 0
      const arrOfRowFlag: boolean[] = []

      for (let ri = 0, l = this.getRowsCount(); ri < l; ++ri) {
        let rowTotalHeight = 0
        rowTotalHeight += this.rowsInfo[ri].h

        heightOfRows[ri] = rowTotalHeight
        minHeightOfRows[ri] = this.getMinRowHeight(ri)
        arrOfRowFlag[ri] = true
        tableTotalHeight += rowTotalHeight
      }

      const newRowHeights: number[] = []
      while (true) {
        let isBreak = false
        let isContinue = false
        for (
          let ri = 0, valueOfRowsCount = this.getRowsCount();
          ri < valueOfRowsCount;
          ++ri
        ) {
          if (arrOfRowFlag[ri]) {
            newRowHeights[ri] =
              (heightOfRows[ri] / tableTotalHeight) * newTableH
            if (newRowHeights[ri] < minHeightOfRows[ri]) {
              tableTotalHeight -= heightOfRows[ri]
              newTableH -= minHeightOfRows[ri]
              newRowHeights[ri] = minHeightOfRows[ri]
              arrOfRowFlag[ri] = false

              if (newTableH < 0.01 || tableTotalHeight < 0.01) {
                isBreak = true
              }

              isContinue = true
            }
          }
        }

        if (isBreak || !isContinue) break
      }

      for (let ri = 0, l = this.children.length; ri < l; ++ri) {
        if (null != newRowHeights[ri]) {
          const row = this.children.at(ri)!
          const rowHeight = row.getHeight()
          let newRowHeight = newRowHeights[ri]

          if (this.rowsInfo[ri] && this.rowsInfo[ri].diffYOfTop) {
            newRowHeight -= this.rowsInfo[ri].diffYOfTop
          }

          let valueOfTopMargin = 0,
            valueOfBotMargin = 0
          for (
            let valueOfCurCell = 0, valueOfCellsCount = row.cellsCount;
            valueOfCurCell < valueOfCellsCount;
            ++valueOfCurCell
          ) {
            const cell = row.getCellByIndex(valueOfCurCell)!
            const margins = cell.getMargins()

            if (margins.top!.w > valueOfTopMargin) {
              valueOfTopMargin = margins.top!.w
            }

            if (margins.bottom!.w > valueOfBotMargin) {
              valueOfBotMargin = margins.bottom!.w
            }
          }

          newRowHeight -= valueOfTopMargin + valueOfBotMargin

          row.setHeight({
            value: newRowHeight,
            heightRule:
              rowHeight.heightRule === LineHeightRule.Exact
                ? LineHeightRule.Exact
                : LineHeightRule.AtLeast,
          })
        }
      }
    }

    calculateTableGrid(this)

    if (diffX > 0.001) {
      const colWidths: number[] = []
      let valueOfTableSumW = 0

      for (
        let colIndex = 0, valueOfColsCount = this.tableGridCalculated.length;
        colIndex < valueOfColsCount;
        ++colIndex
      ) {
        colWidths[colIndex] = this.tableGridCalculated[colIndex]
        valueOfTableSumW += colWidths[colIndex]
      }

      const newGrids: number[] = []
      for (
        let colIndex = 0, valueOfColsCount = this.tableGridCalculated.length;
        colIndex < valueOfColsCount;
        ++colIndex
      ) {
        newGrids[colIndex] =
          (colWidths[colIndex] / valueOfTableSumW) * (valueOfTableSumW + diffX)
      }

      this.setTableGrid(newGrids)
    } else if (diffX < -0.01) {
      const arrOfColsMinW = this._getMinWidth(true)
      const arrOfColsW: number[] = []
      let valueOfTableSumW = 0
      const arrOfColsFlag: boolean[] = []

      for (
        let colIndex = 0, valueOfColsCount = this.tableGridCalculated.length;
        colIndex < valueOfColsCount;
        ++colIndex
      ) {
        arrOfColsW[colIndex] = this.tableGridCalculated[colIndex]
        valueOfTableSumW += arrOfColsW[colIndex]
        arrOfColsFlag[colIndex] = true
      }

      let valueOfNewTableW = valueOfTableSumW + diffX
      const newGrids: number[] = []
      while (true) {
        let isBreak = false
        let isContinue = false
        for (
          let colIndex = 0, valueOfColsCount = this.tableGridCalculated.length;
          colIndex < valueOfColsCount;
          ++colIndex
        ) {
          if (arrOfColsFlag[colIndex]) {
            newGrids[colIndex] =
              (arrOfColsW[colIndex] / valueOfTableSumW) * valueOfNewTableW
            if (newGrids[colIndex] < arrOfColsMinW[colIndex]) {
              valueOfTableSumW -= arrOfColsW[colIndex]
              valueOfNewTableW -= arrOfColsMinW[colIndex]
              newGrids[colIndex] = arrOfColsMinW[colIndex]
              arrOfColsFlag[colIndex] = false

              if (valueOfNewTableW < 0.01 || valueOfTableSumW < 0.01) {
                isBreak = true
              }

              isContinue = true
            }
          }
        }

        if (isBreak || !isContinue) break
      }

      this.setTableGrid(newGrids)
    }
  }

  _getMinWidth(): number
  _getMinWidth(returnColIndices: true): number[]
  _getMinWidth(returnColIndices?: boolean | undefined) {
    const minMargins: number[] = []

    const valueOfGridCount = this.tableGrid.length
    for (
      let valueOfCurCol = 0;
      valueOfCurCol < valueOfGridCount;
      ++valueOfCurCol
    ) {
      minMargins[valueOfCurCol] = 0
    }

    for (let i = 0, rowCount = this.children.length; i < rowCount; ++i) {
      const row = this.children.at(i)!

      let valueOfCurGridCol = 0
      for (
        let valueOfCurCell = 0, valueOfCellsCount = row.cellsCount;
        valueOfCurCell < valueOfCellsCount;
        ++valueOfCurCell
      ) {
        const cell = row.getCellByIndex(valueOfCurCell)!
        const valueOfGridSpan = cell.getGridSpan()
        const cellMargins = cell.getMargins()
        const cellRBorder = cell.getBorder(1)
        const cellLBorder = cell.getBorder(3)

        let valueOfCellMarginsLeftW = 0,
          valueOfCellMarginsRightW = 0
        if (TCBorderValue.None !== cellRBorder.value) {
          valueOfCellMarginsRightW += Math.max(
            cellRBorder.size! / 2,
            cellMargins.right!.w
          )
        } else valueOfCellMarginsRightW += cellMargins.right!.w

        if (TCBorderValue.None !== cellLBorder.value) {
          valueOfCellMarginsLeftW += Math.max(
            cellLBorder.size! / 2,
            cellMargins.left!.w
          )
        } else valueOfCellMarginsLeftW += cellMargins.left!.w

        if (valueOfGridSpan <= 1) {
          if (
            minMargins[valueOfCurGridCol] <
            valueOfCellMarginsLeftW + valueOfCellMarginsRightW
          ) {
            minMargins[valueOfCurGridCol] =
              valueOfCellMarginsLeftW + valueOfCellMarginsRightW
          }
        } else {
          if (minMargins[valueOfCurGridCol] < valueOfCellMarginsLeftW) {
            minMargins[valueOfCurGridCol] = valueOfCellMarginsLeftW
          }

          if (
            minMargins[valueOfCurGridCol + valueOfGridSpan - 1] <
            valueOfCellMarginsRightW
          ) {
            minMargins[valueOfCurGridCol + valueOfGridSpan - 1] =
              valueOfCellMarginsRightW
          }
        }

        valueOfCurGridCol += valueOfGridSpan
      }
    }

    if (returnColIndices) return minMargins

    let valueOfSumMin = 0
    for (
      let valueOfCurCol = 0;
      valueOfCurCol < valueOfGridCount;
      ++valueOfCurCol
    ) {
      valueOfSumMin += minMargins[valueOfCurCol]
    }

    return valueOfSumMin
  }

  _getMinHeight() {
    let valueOfSumMin = 0
    for (
      let rIdx = 0, rowCount = this.children.length;
      rIdx < rowCount;
      ++rIdx
    ) {
      const row = this.children.at(rIdx)!

      let valueOfMaxTopMargin = 0,
        valueOfMaxBottomMargin = 0,
        valueOfMaxTopBorder = 0,
        valueOfMaxBottomBorder = 0

      for (
        let valueOfCurCell = 0, valueOfCellsCount = row.cellsCount;
        valueOfCurCell < valueOfCellsCount;
        ++valueOfCurCell
      ) {
        const cell = row.getCellByIndex(valueOfCurCell)!
        const cellMargins = cell.getMargins()
        const cellTBorder = cell.getBorder(0)
        const cellBBorder = cell.getBorder(2)

        if (
          TCBorderValue.None !== cellTBorder.value &&
          valueOfMaxTopBorder < cellTBorder.size!
        ) {
          valueOfMaxTopBorder = cellTBorder.size!
        }

        if (
          TCBorderValue.None !== cellBBorder.value &&
          valueOfMaxBottomBorder < cellBBorder.size!
        ) {
          valueOfMaxBottomBorder = cellBBorder.size!
        }

        if (valueOfMaxTopMargin < cellMargins.top!.w) {
          valueOfMaxTopMargin = cellMargins.top!.w
        }

        if (valueOfMaxBottomMargin < cellMargins.bottom!.w) {
          valueOfMaxBottomMargin = cellMargins.bottom!.w
        }
      }

      valueOfSumMin += 4.5
      valueOfSumMin += Math.max(valueOfMaxTopBorder, valueOfMaxTopMargin)
      valueOfSumMin += valueOfMaxBottomMargin

      if (rowCount - 1 === rIdx) {
        valueOfSumMin += valueOfMaxBottomBorder
      }
    }

    return valueOfSumMin
  }

  getMinRowHeight(curRowIdx: number) {
    let valueOfSumMin = 0
    const row = this.children.at(curRowIdx)

    let valueOfMaxTopMargin = 0,
      valueOfMaxBottomMargin = 0,
      valueOfMaxTopBorder = 0

    if (row) {
      for (let ci = 0, l = row.cellsCount; ci < l; ++ci) {
        const cell = row.getCellByIndex(ci)!
        const cellMargins = cell.getMargins()
        const cellTBorder = cell.getBorder(0)
        // const cellBBorder = cell.getBorder(2)

        if (
          TCBorderValue.None !== cellTBorder.value &&
          valueOfMaxTopBorder < cellTBorder.size!
        ) {
          valueOfMaxTopBorder = cellTBorder.size!
        }

        if (valueOfMaxTopMargin < cellMargins.top!.w) {
          valueOfMaxTopMargin = cellMargins.top!.w
        }

        if (valueOfMaxBottomMargin < cellMargins.bottom!.w) {
          valueOfMaxBottomMargin = cellMargins.bottom!.w
        }
      }
    }
    valueOfSumMin += 4.5

    valueOfSumMin += Math.max(valueOfMaxTopBorder, valueOfMaxTopMargin)
    valueOfSumMin += valueOfMaxBottomMargin

    return valueOfSumMin
  }

  _getWholeHeight() {
    const bounds = this.getDocumentBounds()
    return bounds.bottom - bounds.top
  }

  setRowHeight(valueOfHeight: number) {
    const rowsRange = this._getSelectedRowsIndexRange()
    for (
      let index = rowsRange.startIndex;
      index <= rowsRange.endIndex;
      ++index
    ) {
      const row = this.children.at(index)
      if (row) {
        const height = row.getHeight()
        row.setHeight({
          value: valueOfHeight,
          heightRule: height.isAuto()
            ? LineHeightRule.AtLeast
            : height.getHRule(),
        })
      }
    }
  }

  getSelectionInfo():
    | { type: 'table' }
    | { type: 'cell'; cellInfo: { rowIndex: number; cellIndex: number } } {
    const { cellPositions, selectionType, selectionStartInfo } = this.selection
    const isSelectInCell =
      selectionType === TableSelectionType.COMMON ||
      selectionType === TableSelectionType.CELLS
    const isMultiSelectCells = cellPositions.length > 1
    if (!isSelectInCell || isMultiSelectCells) {
      return { type: 'table' }
    }
    return {
      type: 'cell',
      cellInfo: {
        rowIndex: selectionStartInfo.cellPos.row,
        cellIndex: selectionStartInfo.cellPos.cell,
      },
    }
  }
}

class RowsAndColsMarkInfo {
  cols: number[]
  rows: Array<{ h: number; y: number }>

  constructor() {
    this.cols = []

    this.rows = []
  }

  reset() {
    this.cols.length = 0

    this.rows.length = 0
  }
}
