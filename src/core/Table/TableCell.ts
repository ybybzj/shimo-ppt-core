import { idGenerator } from '../../globals/IdGenerator'
import { TableCellPr } from './attrs/TableCellPr'

// import {
//   gDefaultTableCellPr,
//   gDefaultParaPr,
//   gDefaultTextPr,
// } from '../textAttributes/globals'
import {
  TableSelectionType,
  TableTextDirection,
  TableTextDirectionValues,
  VMergeTypeValues,
} from '../common/const/table'
import { Matrix2D, MatrixUtils } from '../graphic/Matrix'
import { CShd, CShdOptions } from './attrs/CShd'
import {
  TableCellBorder,
  cloneTableCellBorders,
  mergeTableCellBorders,
} from './attrs/CellBorder'
import { ShdType, LineHeightRule, VertAlignJc } from '../common/const/attrs'

import { EditActionFlag } from '../EditActionFlag'
import { Nullable, valuesOfDict } from '../../../liber/pervasive'
import { TextDocument } from '../TextDocument/TextDocument'
import { TableRow } from './TableRow'
import { InstanceType } from '../instanceTypes'
import { Table } from './Table'
import { HoverInfo } from '../../ui/states/type'
import { informEntityPropertyChange } from '../calculation/informChanges/propertyChange'
import { setSelectionStartForTextDocument } from '../utilities/selection/setter'
import { TextPr } from '../textAttributes/TextPr'
import { ParaPr } from '../textAttributes/ParaPr'
import { PointerEventInfo } from '../../common/dom/events'
import {
  CellBorderType,
  CellBorderTypeValue,
  SetCellMarginParam,
  SetCellMarginType,
} from './common'
import { CHexColor } from '../color/CHexColor'
import { TablePartStyle } from './attrs/TablePartStyle'
import { updateCalcStatusForGraphicFrameItem } from '../calculation/updateCalcStatus/graphicFrame'
import { isObjectsTheSame } from '../common/helpers'
import { gGlobalTableStyles } from '../Slide/GlobalTableStyles'
import { PoolAllocator, Recyclable } from '../../common/allocator'
import { TableCellMarginsOptions } from './attrs/tableMargins'
export interface TableCellCalcInfo {
  y?: number
  xStart?: number
  yStart?: number
  xEnd?: number
  yEnd?: number
  cellStartX?: number
  cellEndX?: number
  cellStartY?: number
  cellEndY?: number
  vAlignOffsetY?: number
}
//----------------------------------------------------------------------------------------------------------------------
export class TableCell extends Recyclable<TableCell> {
  readonly instanceType = InstanceType.TableCell
  id: string
  parent: TableRow
  prev: Nullable<TableCell>
  next: Nullable<TableCell>
  textDoc: TextDocument
  calcedPrs: {
    pr: Nullable<TableCellPr>
    textPr: Nullable<TextPr>
    paraPr: Nullable<ParaPr>
    isDirty: boolean
  }
  pr: TableCellPr
  borderInfo: {
    top: Nullable<TableCellBorder[]>
    left: Nullable<TableCellBorder[]>
    right: Nullable<TableCellBorder[]>
    bottom: Nullable<TableCellBorder[]>
    maxWidthOfLeft: number
    maxWidthOfRight: number
  }
  metrics: {
    startGridCol: number
    startGridColRTL: number
    cellStartX: number
    cellEndX: number
    xOfTextDocStart: number
    xOfTextDocEnd: number
  }
  calculateInfo: TableCellCalcInfo
  index: number
  private static allocator: PoolAllocator<TableCell, [row: TableRow]> =
    PoolAllocator.getAllocator<TableCell, [row: TableRow]>(TableCell)
  static new(row: TableRow) {
    return TableCell.allocator.allocate(row)
  }
  static recycle(tc: TableCell) {
    TableCell.allocator.recycle(tc)
  }
  constructor(row: TableRow) {
    super()
    this.id = idGenerator.newId()

    this.parent = row

    this.prev = null
    this.next = null
    this.calcedPrs = {
      pr: null,
      textPr: null,
      paraPr: null,
      isDirty: true,
    }
    this.textDoc = TextDocument.new(this, 0, 0, 0, 0)

    this.pr = TableCellPr.new()

    this.borderInfo = {
      top: null,
      left: null,
      right: null,
      bottom: null,
      maxWidthOfLeft: 0,
      maxWidthOfRight: 0,
    }

    this.metrics = {
      startGridCol: 0,
      startGridColRTL: 0,
      cellStartX: 0,
      cellEndX: 0,
      xOfTextDocStart: 0,
      xOfTextDocEnd: 0,
    }

    this.calculateInfo = {
      y: 0,

      xStart: 0,
      yStart: 0,
      xEnd: 0,
      yEnd: 0,

      cellStartX: 0,
      cellEndX: 0,
      cellStartY: 0,
      cellEndY: 0,

      vAlignOffsetY: 0,
    }

    this.index = 0
  }
  reset(row: TableRow) {
    this.parent = row

    this.prev = null
    this.next = null
    this.textDoc.reset(this, 0, 0, 0, 0)

    // reset calcedPrs
    if (this.calcedPrs.pr != null) {
      TableCellPr.recycle(this.calcedPrs.pr)
      this.calcedPrs.pr = null
    }
    if (this.calcedPrs.textPr != null) {
      TextPr.recycle(this.calcedPrs.textPr)
      this.calcedPrs.textPr = null
    }

    if (this.calcedPrs.paraPr != null) {
      ParaPr.recycle(this.calcedPrs.paraPr)
      this.calcedPrs.paraPr = null
    }

    this.calcedPrs.isDirty = true

    this.pr.reset()

    // reset borderInfo

    this.borderInfo.top = null

    this.borderInfo.left = null

    this.borderInfo.right = null

    this.borderInfo.bottom = null

    this.borderInfo.maxWidthOfLeft = 0
    this.borderInfo.maxWidthOfRight = 0

    // reset metrics
    this.metrics.startGridCol = 0
    this.metrics.startGridColRTL = 0
    this.metrics.cellStartX = 0
    this.metrics.cellEndX = 0
    this.metrics.xOfTextDocStart = 0
    this.metrics.xOfTextDocEnd = 0

    // reset calculateInfo

    this.calculateInfo.y = 0

    this.calculateInfo.xStart = 0
    this.calculateInfo.yStart = 0
    this.calculateInfo.xEnd = 0
    this.calculateInfo.yEnd = 0

    this.calculateInfo.cellStartX = 0
    this.calculateInfo.cellEndX = 0
    this.calculateInfo.cellStartY = 0
    this.calculateInfo.cellEndY = 0

    this.calculateInfo.vAlignOffsetY = 0

    this.index = 0
  }

  getId() {
    return this.id
  }

  getTheme() {
    return this.parent.parent.getTheme()
  }

  getColorMap() {
    return this.parent.parent.getColorMap()
  }

  clone(row: TableRow) {
    const cell = TableCell.new(row)

    cell.applyPr(this.pr.clone(), false)

    cell.textDoc.populateBy(this.textDoc)

    cell.borderInfo.top = this.borderInfo.top
    cell.borderInfo.left = this.borderInfo.left
    cell.borderInfo.right = this.borderInfo.right
    cell.borderInfo.bottom = this.borderInfo.bottom
    cell.borderInfo.maxWidthOfLeft = this.borderInfo.maxWidthOfLeft
    cell.borderInfo.maxWidthOfRight = this.borderInfo.maxWidthOfRight

    cell.metrics.startGridCol = this.metrics.startGridCol
    cell.metrics.startGridColRTL = this.metrics.startGridColRTL
    cell.metrics.cellStartX = this.metrics.cellStartX
    cell.metrics.cellEndX = this.metrics.cellEndX
    cell.metrics.xOfTextDocStart = this.metrics.xOfTextDocStart
    cell.metrics.xOfTextDocEnd = this.metrics.xOfTextDocEnd

    return cell
  }

  setIndex(index: number) {
    if (index !== this.index) {
      this.index = index
      this.invalidateCalcedPrs()
    }
  }

  setMetrics(
    startGridCol: number,
    startGridColRTL: number,
    cellStartX: number,
    cellEndX: number,
    xOfTextDocStart: number,
    xOfTextDocEnd: number
  ) {
    this.metrics.startGridCol = startGridCol
    this.metrics.startGridColRTL = startGridColRTL
    this.metrics.cellStartX = cellStartX
    this.metrics.cellEndX = cellEndX
    this.metrics.xOfTextDocStart = xOfTextDocStart
    this.metrics.xOfTextDocEnd = xOfTextDocEnd
  }

  invalidateCalcedPrs(bubbleUp = true) {
    this.calcedPrs.isDirty = true
    this.textDoc.invalidateAllParagraphsCalcedPrs(false)
    if (bubbleUp) {
      updateCalcStatusForGraphicFrameItem(this)
    }
  }

  getCalcedPr(isCopy?: boolean) {
    if (true === this.calcedPrs.isDirty) {
      const pr = this.calcPr()
      this.calcedPrs.pr = pr.CellPr
      this.calcedPrs.paraPr = pr.paraPr
      this.calcedPrs.textPr = pr.textPr
      this.calcedPrs.isDirty = false
    }

    if (false === isCopy) return this.calcedPrs.pr!
    else return this.calcedPrs.pr!.clone()
  }

  calcBordersFromTableStyle() {
    const table = this.parent.parent
    const tablePr = table.getCalcedPr(false)
    const tableLook = table.getTableLook()
    const cellIndex = this.index
    const rowIndex = this.parent.index

    const tableCellBorders = cloneTableCellBorders(
      tablePr.tableCellPr.tableCellBorders
    )

    if (true === tableLook.isBandRow()) {
      const rowBandSize = tablePr.tablePr.rowBandSize!
      const __rowIndex =
        true !== tableLook.isFirstRow() ? rowIndex : rowIndex - 1
      const _rowIndex =
        1 !== rowBandSize ? Math.floor(__rowIndex / rowBandSize) : __rowIndex
      let tablePartStyle: TablePartStyle
      if (0 === _rowIndex % 2) tablePartStyle = tablePr.tableBand1Horz
      else tablePartStyle = tablePr.tableBand2Horz

      mergeTableCellBorders(
        tableCellBorders,
        tablePartStyle.tableCellPr.tableCellBorders
      )
    }

    if (true === tableLook.isBandCol()) {
      let isFirstCol = false
      if (true === tableLook.isFirstCol()) {
        const tableStyle = gGlobalTableStyles.tableStyles.Get(
          this.parent.parent.getTableStyleId()
        )
        if (tableStyle && tableStyle.tableFirstCol) {
          const colStyle = tableStyle.tableFirstCol
          if (true !== colStyle.tableCellPr.isEmpty()) {
            isFirstCol = true
          }
        }
      }

      const colBandSize = tablePr.tablePr.colBandSize!
      const colIndex = true !== isFirstCol ? cellIndex : cellIndex - 1
      const _colIndex =
        1 !== colBandSize ? Math.floor(colIndex / colBandSize) : colIndex
      let tablePartStyle: TablePartStyle
      if (0 === _colIndex % 2) tablePartStyle = tablePr.tableBand1Vert
      else tablePartStyle = tablePr.tableBand2Vert

      mergeTableCellBorders(
        tableCellBorders,
        tablePartStyle.tableCellPr.tableCellBorders
      )
    }

    if (
      true === tableLook.isLastCol() &&
      this.parent.cellsCount - 1 === cellIndex
    ) {
      mergeTableCellBorders(
        tableCellBorders,
        tablePr.tableLastCol.tableCellPr.tableCellBorders
      )
    }

    if (true === tableLook.isFirstCol() && 0 === cellIndex) {
      mergeTableCellBorders(
        tableCellBorders,
        tablePr.tableFirstCol.tableCellPr.tableCellBorders
      )
    }

    if (true === tableLook.isLastRow() && table.lastRow === rowIndex) {
      mergeTableCellBorders(
        tableCellBorders,
        tablePr.tableLastRow.tableCellPr.tableCellBorders
      )
    }

    if (true === tableLook.isFirstRow() && 0 === rowIndex) {
      mergeTableCellBorders(
        tableCellBorders,
        tablePr.tableFirstRow.tableCellPr.tableCellBorders
      )
    }

    if (
      this.parent.cellsCount - 1 === cellIndex &&
      table.lastRow === rowIndex &&
      true === tableLook.isLastRow() &&
      true === tableLook.isLastCol()
    ) {
      mergeTableCellBorders(
        tableCellBorders,
        tablePr.tableSECell.tableCellPr.tableCellBorders
      )
    }

    if (
      0 === cellIndex &&
      table.lastRow === rowIndex &&
      true === tableLook.isLastRow() &&
      true === tableLook.isFirstCol()
    ) {
      mergeTableCellBorders(
        tableCellBorders,
        tablePr.tableSWCell.tableCellPr.tableCellBorders
      )
    }

    if (
      this.parent.cellsCount - 1 === cellIndex &&
      0 === rowIndex &&
      true === tableLook.isFirstRow() &&
      true === tableLook.isLastCol()
    ) {
      mergeTableCellBorders(
        tableCellBorders,
        tablePr.tableNECell.tableCellPr.tableCellBorders
      )
    }

    if (
      0 === cellIndex &&
      0 === rowIndex &&
      true === tableLook.isFirstRow() &&
      true === tableLook.isFirstCol()
    ) {
      mergeTableCellBorders(
        tableCellBorders,
        tablePr.tableNWCell.tableCellPr.tableCellBorders
      )
    }

    return tableCellBorders
  }

  private calcPr() {
    const table = this.parent.parent
    const tablePr = table.getCalcedPr(false)
    const tableLook = table.getTableLook()
    const cellIndex = this.index
    const rowIndex = this.parent.index

    const cellPr = tablePr.tableCellPr.clone()
    const paraPr = tablePr.paraPr.clone()

    const textPr = tablePr.tableWholeTable.textPr.clone()

    if (true === tableLook.isBandRow()) {
      const rowBandSize = tablePr.tablePr.rowBandSize!
      const __rowIndex =
        true !== tableLook.isFirstRow() ? rowIndex : rowIndex - 1
      const _rowIndex =
        1 !== rowBandSize ? Math.floor(__rowIndex / rowBandSize) : __rowIndex
      let tablePartStyle: TablePartStyle
      if (0 === _rowIndex % 2) tablePartStyle = tablePr.tableBand1Horz
      else tablePartStyle = tablePr.tableBand2Horz

      cellPr.merge(tablePartStyle.tableCellPr)
      textPr.merge(tablePartStyle.textPr)
      paraPr.merge(tablePartStyle.paraPr)
    }

    if (true === tableLook.isBandCol()) {
      let isFirstCol = false
      if (true === tableLook.isFirstCol()) {
        const tableStyle = gGlobalTableStyles.tableStyles.Get(
          this.parent.parent.getTableStyleId()
        )
        if (tableStyle && tableStyle.tableFirstCol) {
          const colStyle = tableStyle.tableFirstCol
          if (
            true !== colStyle.tableCellPr.isEmpty() ||
            true !== colStyle.paraPr.isEmpty() ||
            true !== colStyle.textPr.isEmpty()
          ) {
            isFirstCol = true
          }
        }
      }

      const colBandSize = tablePr.tablePr.colBandSize!
      const colIndex = true !== isFirstCol ? cellIndex : cellIndex - 1
      const _colIndex =
        1 !== colBandSize ? Math.floor(colIndex / colBandSize) : colIndex
      let tablePartStyle: TablePartStyle
      if (0 === _colIndex % 2) tablePartStyle = tablePr.tableBand1Vert
      else tablePartStyle = tablePr.tableBand2Vert

      cellPr.merge(tablePartStyle.tableCellPr)
      textPr.merge(tablePartStyle.textPr)
      paraPr.merge(tablePartStyle.paraPr)
    }

    if (
      true === tableLook.isLastCol() &&
      this.parent.cellsCount - 1 === cellIndex
    ) {
      cellPr.merge(tablePr.tableLastCol.tableCellPr)
      textPr.merge(tablePr.tableLastCol.textPr)
      paraPr.merge(tablePr.tableLastCol.paraPr)
    }

    if (true === tableLook.isFirstCol() && 0 === cellIndex) {
      cellPr.merge(tablePr.tableFirstCol.tableCellPr)
      textPr.merge(tablePr.tableFirstCol.textPr)
      paraPr.merge(tablePr.tableFirstCol.paraPr)
    }

    if (true === tableLook.isLastRow() && table.lastRow === rowIndex) {
      cellPr.merge(tablePr.tableLastRow.tableCellPr)
      textPr.merge(tablePr.tableLastRow.textPr)
      paraPr.merge(tablePr.tableLastRow.paraPr)
    }

    if (true === tableLook.isFirstRow() && 0 === rowIndex) {
      cellPr.merge(tablePr.tableFirstRow.tableCellPr)
      textPr.merge(tablePr.tableFirstRow.textPr)
      paraPr.merge(tablePr.tableFirstRow.paraPr)
    }

    if (
      this.parent.cellsCount - 1 === cellIndex &&
      table.lastRow === rowIndex &&
      true === tableLook.isLastRow() &&
      true === tableLook.isLastCol()
    ) {
      cellPr.merge(tablePr.tableSECell.tableCellPr)
      textPr.merge(tablePr.tableSECell.textPr)
      paraPr.merge(tablePr.tableSECell.paraPr)
    }

    if (
      0 === cellIndex &&
      table.lastRow === rowIndex &&
      true === tableLook.isLastRow() &&
      true === tableLook.isFirstCol()
    ) {
      cellPr.merge(tablePr.tableSWCell.tableCellPr)
      textPr.merge(tablePr.tableSWCell.textPr)
      paraPr.merge(tablePr.tableSWCell.paraPr)
    }

    if (
      this.parent.cellsCount - 1 === cellIndex &&
      0 === rowIndex &&
      true === tableLook.isFirstRow() &&
      true === tableLook.isLastCol()
    ) {
      cellPr.merge(tablePr.tableNECell.tableCellPr)
      textPr.merge(tablePr.tableNECell.textPr)
      paraPr.merge(tablePr.tableNECell.paraPr)
    }

    if (
      0 === cellIndex &&
      0 === rowIndex &&
      true === tableLook.isFirstRow() &&
      true === tableLook.isFirstCol()
    ) {
      cellPr.merge(tablePr.tableNWCell.tableCellPr)
      textPr.merge(tablePr.tableNWCell.textPr)
      paraPr.merge(tablePr.tableNWCell.paraPr)
    }

    if (
      null == cellPr.margins &&
      null != this.pr.margins &&
      null != this.pr.margins
    ) {
      cellPr.margins = {}
    }

    cellPr.merge(this.pr)

    cellPr.refreshTheme(table.getTheme())

    return { CellPr: cellPr, paraPr: paraPr, textPr: textPr }
  }

  getTextStyles(l: number) {
    return this.parent.parent.getTextStyles(l)
  }

  getTableStyleForParagraph() {
    this.getCalcedPr(false)

    const textPr = this.calcedPrs.textPr?.clone()
    const paraPr = this.calcedPrs.paraPr?.clone()

    return { textPr: textPr, paraPr: paraPr }
  }

  getTextBgColor() {
    const shd = this.getShd()!

    if (ShdType.Nil !== shd.value) {
      return shd.getColorByTheme(this.getTheme(), this.getColorMap())
    }

    return this.parent.parent.getTextBgColor()
  }

  isInPPT(id?: string) {
    if (id != null) {
      if (!this.textDoc || this.textDoc.getId() !== id) {
        return false
      }
    }
    if (null != this.parent) return this.parent.isInPPT(this.getId())

    return false
  }

  selectSelf() {
    const table = this.parent.parent

    table.selection.start = false
    table.selection.type = TableSelectionType.TEXT
    table.selection.isUse = this.textDoc.hasTextSelection()

    table.selection.selectionStartInfo.cellPos = {
      row: this.parent.index,
      cell: this.index,
    }
    table.selection.selectionEndInfo.cellPos = {
      row: this.parent.index,
      cell: this.index,
    }

    table.curCell = this

    table.selectSelf()
  }

  isCurrentSelected() {
    const table: Table = this.parent.parent
    if (false === table.selection.isUse && this === table.curCell) {
      return table.parent.isCurrentSelected()
    }

    return false
  }

  getParentSlideIndex() {
    return this.parent.parent.getParentSlideIndex()
  }

  getTextTransformOfParent() {
    const transformOfParent = this.parent.parent.getTextTransformOfParent()
    const transform = this.getTextTransformByDirection()
    if (transform && transformOfParent) {
      MatrixUtils.multiplyMatrixes(transform, transformOfParent)
      return transform
    }
    return transformOfParent || transform
  }

  setContentSelectionStart(x: number, y: number, e: PointerEventInfo) {
    let transform = this.getTextTransformByDirection()
    if (null != transform) {
      transform = MatrixUtils.invertMatrix(transform)
      x = transform.XFromPoint(x, y)
      y = transform.YFromPoint(x, y)
    }

    setSelectionStartForTextDocument(this.textDoc, x, y, e)
  }

  /** 计算 cursorType 统一 return 至最上级处理*/
  getCursorInfoByPos(x: number, y: number): Nullable<HoverInfo> {
    let transform = this.getTextTransformByDirection()
    if (null != transform) {
      transform = MatrixUtils.invertMatrix(transform)
      x = transform.XFromPoint(x, y)
      y = transform.YFromPoint(x, y)
    }

    return this.textDoc.getCursorInfoByPos(x, y)
  }

  isHitInText(x: number, y: number) {
    let transform = this.getTextTransformByDirection()
    if (null != transform) {
      transform = MatrixUtils.invertMatrix(transform)
      x = transform.XFromPoint(x, y)
      y = transform.YFromPoint(x, y)
    }
    return this.textDoc.isHitInText(x, y)
  }

  setCursorPosXY(x: number, y: number) {
    let transform = this.getTextTransformByDirection()
    if (null != transform) {
      transform = MatrixUtils.invertMatrix(transform)
      x = transform.XFromPoint(x, y)
      y = transform.YFromPoint(x, y)
    }
    this.textDoc.cursorPosition.cursorX = x
    this.textDoc.cursorPosition.cursorY = y
  }

  translateContentBy(dX: number, dY: number) {
    if (true === this.isTextVertical()) {
      this.calculateInfo.xStart! += dX
      this.calculateInfo.yStart! += dY
      this.calculateInfo.xEnd! += dX
      this.calculateInfo.yEnd! += dY

      this.calculateInfo.cellStartX! += dX
      this.calculateInfo.cellStartY! += dY
      this.calculateInfo.cellEndX! += dX
      this.calculateInfo.cellEndY! += dY

      this.calculateInfo.y! += dY
    } else {
      this.textDoc.renderingState.translateBy(dX, dY)
    }
  }

  getTextTransformByDirection() {
    let transform: Nullable<Matrix2D>
    const textDirection = this.getTextDirection()

    if (TableTextDirection.BTLR === textDirection) {
      transform = new Matrix2D()
      MatrixUtils.rotateMatrix(transform, 0.5 * Math.PI)
      MatrixUtils.translateMatrix(
        transform,
        this.calculateInfo.xStart!,
        this.calculateInfo.yEnd!
      )
    } else if (TableTextDirection.TBRL === textDirection) {
      transform = new Matrix2D()
      MatrixUtils.rotateMatrix(transform, -0.5 * Math.PI)
      MatrixUtils.translateMatrix(
        transform,
        this.calculateInfo.xEnd!,
        this.calculateInfo.yStart!
      )
    }

    return transform
  }

  addTextDoc(textDoc: TextDocument) {
    this.textDoc.addContent(textDoc)
  }

  clearFormatting(isRemoveMerge: boolean) {
    this.setShd(undefined)
    this.setMargins(undefined)
    this.setBorder(undefined, CellBorderType.Top)
    this.setBorder(undefined, CellBorderType.Right)
    this.setBorder(undefined, CellBorderType.Bottom)
    this.setBorder(undefined, CellBorderType.Left)

    if (true === isRemoveMerge) {
      this.setGridSpan(undefined)
      this.setVMerge(undefined)
    }
  }

  setPr(cellPr: TableCellPr) {
    informEntityPropertyChange(
      this,
      EditActionFlag.edit_TableCell_Pr,
      this.pr,
      cellPr
    )
    this.pr = cellPr
    this.invalidateCalcedPrs()
  }

  applyPr(cellPr: TableCellPr, isCopyOnlyVisualProps?: boolean) {
    if (true !== isCopyOnlyVisualProps) {
      // GridSpan
      if (undefined === cellPr.gridSpan) this.setGridSpan(undefined)
      else this.setGridSpan(cellPr.gridSpan)
    }

    // Shd
    if (undefined === cellPr.shd) this.setShd(undefined)
    else {
      const newShd = {
        value: cellPr.shd.value,
        color: {
          r: cellPr.shd.color!.r,
          g: cellPr.shd.color!.g,
          b: cellPr.shd.color!.b,
        },
        fillEffects: cellPr.shd.fillEffects
          ? cellPr.shd.fillEffects.clone()
          : undefined,
      }

      this.setShd(newShd)
    }

    if (true !== isCopyOnlyVisualProps) {
      // vMerge
      this.setVMerge(cellPr.vMerge)
    }

    // Border Top
    if (undefined === cellPr.tableCellBorders.top) {
      this.setBorder(undefined, CellBorderType.Top)
    } else {
      const topBorder =
        null === cellPr.tableCellBorders.top
          ? null
          : cellPr.tableCellBorders.top.clone()

      this.setBorder(topBorder, CellBorderType.Top)
    }

    // Border bottom
    if (undefined === cellPr.tableCellBorders.bottom) {
      this.setBorder(undefined, CellBorderType.Bottom)
    } else {
      const bottomBorder =
        null === cellPr.tableCellBorders.bottom
          ? null
          : cellPr.tableCellBorders.bottom.clone()

      this.setBorder(bottomBorder, CellBorderType.Bottom)
    }

    // Border left
    if (undefined === cellPr.tableCellBorders.left) {
      this.setBorder(undefined, CellBorderType.Left)
    } else {
      const leftBorder =
        null === cellPr.tableCellBorders.left
          ? null
          : cellPr.tableCellBorders.left.clone()

      this.setBorder(leftBorder, CellBorderType.Left)
    }

    // Border right
    if (undefined === cellPr.tableCellBorders.right) {
      this.setBorder(undefined, CellBorderType.Right)
    } else {
      const rightBorder =
        null === cellPr.tableCellBorders.right
          ? null
          : cellPr.tableCellBorders.right.clone()

      this.setBorder(rightBorder, CellBorderType.Right)
    }

    // Margins
    if (!cellPr.margins) {
      this.setMargins(undefined)
    } else {
      const margins: TableCellMarginsOptions = {}

      if (cellPr.margins.top) {
        margins.top = {
          w: cellPr.margins.top.w,
          type: cellPr.margins.top.type,
        }
      }

      if (cellPr.margins.left) {
        margins.left = {
          w: cellPr.margins.left.w,
          type: cellPr.margins.left.type,
        }
      }

      if (cellPr.margins.bottom) {
        margins.bottom = {
          w: cellPr.margins.bottom.w,
          type: cellPr.margins.bottom.type,
        }
      }

      if (cellPr.margins.right) {
        margins.right = {
          w: cellPr.margins.right.w,
          type: cellPr.margins.right.type,
        }
      }

      this.setMargins({ margins, type: SetCellMarginType.All })
    }

    // VAlign
    this.setVAlign(cellPr.vAlign)

    // TextDirection
    this._setTextDirection(cellPr.textDirection!)
  }

  getGridSpan() {
    return this.getCalcedPr(false).gridSpan!
  }

  setGridSpan(value: undefined | number) {
    if (this.pr.gridSpan === value) return

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_TableCell_GridSpan,
      this.pr.gridSpan,
      value
    )
    this.pr.gridSpan = value
    this.invalidateCalcedPrs()
  }

  getMargins() {
    const cellMargins = this.getCalcedPr(false).margins
    if (!cellMargins) {
      return this.parent.parent.getTableCellMargins()
    } else {
      const tableCellDefMargins = this.parent.parent.getTableCellMargins()
      const margins = {
        top: cellMargins.top ?? tableCellDefMargins.top,
        bottom: cellMargins.bottom ?? tableCellDefMargins.bottom,
        left: cellMargins.left ?? tableCellDefMargins.left,
        right: cellMargins.right ?? tableCellDefMargins.right,
      }

      return margins
    }
  }

  hasTableMargins() {
    const margins = this.getCalcedPr(false).margins
    return !margins
  }

  setMargins(marginsParam: Nullable<SetCellMarginParam>) {
    const oldValue = undefined === this.pr.margins ? undefined : this.pr.margins

    if (null == marginsParam) {
      if (marginsParam !== this.pr.margins) {
        informEntityPropertyChange(
          this,
          EditActionFlag.edit_TableCell_Margins,
          oldValue,
          marginsParam
        )
        this.pr.margins = undefined
        this.invalidateCalcedPrs()
      }

      return
    }

    let newMargins = this.pr.margins

    let needChange = false
    const margins = this.parent.parent.getTableCellMargins()
    if (null == newMargins) {
      newMargins = {
        left: margins.left!.clone(),
        right: margins.right!.clone(),
        top: margins.top!.clone(),
        bottom: margins.bottom!.clone(),
      }

      needChange = true
    }

    switch (marginsParam.type) {
      case SetCellMarginType.All: {
        needChange = true
        const margins = marginsParam.margins
        if (margins.top) {
          newMargins.top!.w = margins.top.w
          newMargins.top!.type = margins.top.type
        }

        if (margins.right) {
          newMargins.right!.w = margins.right.w
          newMargins.right!.type = margins.right.type
        }

        if (margins.bottom) {
          newMargins.bottom!.w = margins.bottom.w
          newMargins.bottom!.type = margins.bottom.type
        }

        if (margins.left) {
          newMargins.left!.w = margins.left.w
          newMargins.left!.type = margins.left.type
        }

        break
      }
      case SetCellMarginType.Top: {
        const margin = marginsParam.margin
        if (
          (true !== needChange && newMargins.top!.w !== margin.w) ||
          newMargins.top!.type !== margin.type
        ) {
          needChange = true
        }

        newMargins.top!.w = margin.w
        newMargins.top!.type = margin.type
        break
      }
      case SetCellMarginType.Right: {
        const margin = marginsParam.margin
        if (
          (true !== needChange && newMargins.right!.w !== margin.w) ||
          newMargins.right!.type !== margin.type
        ) {
          needChange = true
        }

        newMargins.right!.w = margin.w
        newMargins.right!.type = margin.type
        break
      }
      case SetCellMarginType.Bootom: {
        const margin = marginsParam.margin
        if (
          (true !== needChange && newMargins.bottom!.w !== margin.w) ||
          newMargins.bottom!.type !== margin.type
        ) {
          needChange = true
        }

        newMargins.bottom!.w = margin.w
        newMargins.bottom!.type = margin.type
        break
      }
      case SetCellMarginType.Left: {
        const margin = marginsParam.margin
        if (
          (true !== needChange && newMargins.left!.w !== margin.w) ||
          newMargins.left!.type !== margin.type
        ) {
          needChange = true
        }

        newMargins.left!.w = margin.w
        newMargins.left!.type = margin.type
        break
      }
    }

    if (true === needChange) {
      informEntityPropertyChange(
        this,
        EditActionFlag.edit_TableCell_Margins,
        oldValue,
        newMargins
      )
      this.pr.margins = newMargins
      this.invalidateCalcedPrs()
    }
  }

  getShd() {
    return this.getCalcedPr(false).shd!
  }

  setShd(shdOpts: Nullable<CShdOptions>) {
    if (undefined === shdOpts && undefined === this.pr.shd) return

    if (undefined === shdOpts) {
      informEntityPropertyChange(
        this,
        EditActionFlag.edit_TableCell_Shd,
        this.pr.shd,
        undefined
      )
      this.pr.shd = undefined
      this.invalidateCalcedPrs()
    } else if (
      undefined === this.pr.shd ||
      false === isObjectsTheSame(this.pr.shd, shdOpts)
    ) {
      const shd = this.pr.shd ?? CShd.new()
      shd.reset()
      shd.fromOptions(shdOpts)
      informEntityPropertyChange(
        this,
        EditActionFlag.edit_TableCell_Shd,
        this.pr.shd,
        shd
      )
      this.pr.shd = shd
      this.invalidateCalcedPrs()
    }
  }

  getVAlign() {
    const vAlign = this.getCalcedPr(false).vAlign
    return vAlign
  }

  setVAlign(value: Nullable<valuesOfDict<typeof VertAlignJc>>) {
    if (value === this.pr.vAlign) return

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_TableCell_VAlign,
      this.pr.vAlign,
      value
    )
    this.pr.vAlign = value
    this.invalidateCalcedPrs()
  }

  isTextVertical() {
    const textDirection = this.getTextDirection()
    if (
      TableTextDirection.BTLR === textDirection ||
      TableTextDirection.TBRL === textDirection
    ) {
      return true
    }

    return false
  }

  getTextDirection() {
    return this.getCalcedPr(false).textDirection
  }

  _setTextDirection(direction: TableTextDirectionValues) {
    if (direction !== this.pr.textDirection) {
      informEntityPropertyChange(
        this,
        EditActionFlag.edit_TableCell_TextDirection,
        this.pr.textDirection,
        direction
      )
      this.pr.textDirection = direction
      this.invalidateCalcedPrs()
    }
  }

  setTextDirection(textDirection: TableTextDirectionValues) {
    const isTextVertical =
      TableTextDirection.BTLR === textDirection ||
      TableTextDirection.TBRL === textDirection
        ? true
        : false

    const orgTextDirection = this.getTextDirection()
    this._setTextDirection(textDirection)

    if (orgTextDirection !== textDirection) {
      if (true === isTextVertical) {
        const row = this.parent
        const rowHeight = this.parent.getHeight()

        if (LineHeightRule.Auto === rowHeight.heightRule) {
          row.setHeight({ value: 20, heightRule: LineHeightRule.AtLeast })
        } else if (rowHeight.value < 20) {
          row.setHeight({ value: 20, heightRule: rowHeight.heightRule })
        }
      }
      const paras = this.textDoc.children
      for (let i = 0, l = paras.length; i < l; ++i) {
        const para = paras.at(i)!
        para.updateParaPropsForVerticalText(isTextVertical)
      }
    }
  }

  getBorders() {
    return {
      top: this.getBorder(CellBorderType.Top),
      right: this.getBorder(CellBorderType.Right),
      bottom: this.getBorder(CellBorderType.Bottom),
      left: this.getBorder(CellBorderType.Left),
    }
  }

  getBorder(type: CellBorderTypeValue, ignoreSelfBorder = false) {
    const tableBorders = this.parent.parent.getTableBordersInfo()
    const borders = ignoreSelfBorder
      ? this.calcBordersFromTableStyle()
      : this.getCalcedPr(false).tableCellBorders
    let border: TableCellBorder | undefined
    let useTableBorder = false
    switch (type) {
      case CellBorderType.Top: {
        if (null != borders.top) {
          border = borders.top
        } else {
          if (0 !== this.parent.index) {
            border = tableBorders.insideH
          } else {
            border = tableBorders.top
          }
          useTableBorder = true
        }

        break
      }
      case CellBorderType.Right: {
        if (null != borders.right) border = borders.right
        else {
          if (this.parent.children.length - 1 !== this.index) {
            border = tableBorders.insideV
          } else {
            border = tableBorders.right
          }
          useTableBorder = true
        }

        break
      }
      case CellBorderType.Bottom: {
        if (null != borders.bottom) {
          border = borders.bottom
        } else {
          if (this.parent.parent.lastRow !== this.parent.index) {
            border = tableBorders.insideH
          } else {
            border = tableBorders.bottom
          }
          useTableBorder = true
        }

        break
      }
      case CellBorderType.Left: {
        if (null != borders.left) {
          border = borders.left
        } else {
          if (0 !== this.index) {
            border = tableBorders.insideV
          } else {
            border = tableBorders.left
          }
          useTableBorder = true
        }

        break
      }
    }

    if (border?.fillEffects?.check) {
      const theme = this.getTheme()
      const colorMap = this.getColorMap()
      if (theme && colorMap) {
        border.fillEffects.check(theme, colorMap)
        const { r, g, b } = border.fillEffects.getRGBAColor()
        border.color.set(r, g, b)
      }
    }

    border!.useTableBorder = useTableBorder

    return border!
  }

  setBorder(border: Nullable<TableCellBorder>, type: CellBorderTypeValue) {
    let orgBorder = this.pr.tableCellBorders.top
    switch (type) {
      case CellBorderType.Top:
        orgBorder = this.pr.tableCellBorders.top
        break
      case CellBorderType.Right:
        orgBorder = this.pr.tableCellBorders.right
        break
      case CellBorderType.Bottom:
        orgBorder = this.pr.tableCellBorders.bottom
        break
      case CellBorderType.Left:
        orgBorder = this.pr.tableCellBorders.left
        break
    }

    let newBorder: TableCellBorder | undefined
    if (!border) {
      if (border === orgBorder) {
        return
      }
      newBorder = undefined
    } else if (!orgBorder) {
      newBorder = this.getBorder(type).clone()
      newBorder.value = null != border.value ? border.value : newBorder.value
      newBorder.size = null != border.size ? border.size : newBorder.size
      if (!newBorder.color) {
        newBorder.color = new CHexColor(0, 0, 0)
      }
      newBorder.color.r =
        null != border.color ? border.color.r : newBorder.color.r
      newBorder.color.g =
        null != border.color ? border.color.g : newBorder.color.g
      newBorder.color.b =
        null != border.color ? border.color.b : newBorder.color.b
      newBorder.fillEffects =
        null != border.fillEffects ? border.fillEffects : newBorder.fillEffects
    } else {
      newBorder = TableCellBorder.new()
      const _border = orgBorder ?? TableCellBorder.new()
      newBorder.value = null != border.value ? border.value : _border.value
      newBorder.size = null != border.size ? border.size : _border.size
      newBorder.color!.r =
        null != border.color ? border.color.r : _border.color!.r
      newBorder.color!.g =
        null != border.color ? border.color.g : _border.color!.g
      newBorder.color!.b =
        null != border.color ? border.color.b : _border.color!.b
      newBorder.fillEffects =
        null != border.fillEffects ? border.fillEffects : _border.fillEffects
    }
    switch (type) {
      case CellBorderType.Top: {
        informEntityPropertyChange(
          this,
          EditActionFlag.edit_TableCell_Border_Top,
          this.pr.tableCellBorders.top,
          newBorder
        )
        this.pr.tableCellBorders.top = newBorder
        break
      }
      case CellBorderType.Right: {
        informEntityPropertyChange(
          this,
          EditActionFlag.edit_TableCell_Border_Right,
          this.pr.tableCellBorders.right,
          newBorder
        )
        this.pr.tableCellBorders.right = newBorder
        break
      }
      case CellBorderType.Bottom: {
        informEntityPropertyChange(
          this,
          EditActionFlag.edit_TableCell_Border_Bottom,
          this.pr.tableCellBorders.bottom,
          newBorder
        )
        this.pr.tableCellBorders.bottom = newBorder
        break
      }
      case CellBorderType.Left: {
        informEntityPropertyChange(
          this,
          EditActionFlag.edit_TableCell_Border_Left,
          this.pr.tableCellBorders.left,
          newBorder
        )
        this.pr.tableCellBorders.left = newBorder
        break
      }
    }

    this.invalidateCalcedPrs()
  }

  setTopBorderInfo(info: TableCellBorder[]) {
    this.borderInfo.top = info
  }

  setBottomBorderInfo(info: TableCellBorder[]) {
    this.borderInfo.bottom = info
  }

  setLeftBorderInfo(info: TableCellBorder[], max: number) {
    this.borderInfo.left = info
    this.borderInfo.maxWidthOfLeft = max
  }

  setRightBorderInfo(info: TableCellBorder[], max: number) {
    this.borderInfo.right = info
    this.borderInfo.maxWidthOfRight = max
  }

  getBorderInfo() {
    return this.borderInfo
  }

  getTextDoc() {
    return this.textDoc
  }

  getRow() {
    return this.parent
  }

  getTable() {
    const row = this.getRow()
    return row ? row.getTable() : null
  }

  getIndex() {
    return this.index
  }

  isLastInRow() {
    if (!this.parent || !this.parent.parent) return false
    const table = this.parent.parent
    const cellPositions = table.getSelectedCellPositions()
    return !cellPositions.find(
      (pos) => pos.row === this.parent.index && pos.cell > this.index
    )
  }

  getVMerge() {
    return this.getCalcedPr(false).vMerge
  }

  setVMerge(valueOfType?: VMergeTypeValues) {
    if (valueOfType === this.pr.vMerge) return

    informEntityPropertyChange(
      this,
      EditActionFlag.edit_TableCell_VMerge,
      this.pr.vMerge,
      valueOfType
    )
    this.pr.vMerge = valueOfType
    this.invalidateCalcedPrs()
  }
}
