import { EditActionFlag } from '../EditActionFlag'
import { startEditAction } from '../calculation/informChanges/startAction'
import { calculateForRendering } from '../calculation/calculate/calculateForRendering'
import { Presentation } from '../Slide/Presentation'
import { findTextInPPT, findTextInPresentation } from './findText'
import { searchManager } from './SearchManager'
import { getAndUpdateCursorPositionForPresentation } from '../utilities/cursor/cursorPosition'
import { emitEditorEvent } from '../utilities/shape/hookInvokers'

/**
 * @returns null 表示该次执行匹配, 否则表示替换的数量
 */
export function replaceText(
  presentation: Presentation,
  text: string,
  replaceWith: string,
  isReplaceAll: boolean,
  isCaseSensitive = false
): { replaceNum: number } | null {
  findTextInPresentation(presentation, text, { caseSensitive: isCaseSensitive })
  const replaceableNum = searchManager.searchElementsCount
  let isExecuteReplace = false

  if (!replaceableNum) return { ['replaceNum']: 0 }
  if (isReplaceAll) {
    isExecuteReplace = true
    replaceTextInPresentation(presentation, replaceWith, true, -1)
  } else {
    const curId = searchManager.currentSearchId
    if (-1 !== curId) {
      isExecuteReplace = true
      replaceTextInPresentation(presentation, replaceWith, false, curId)
    }
  }
  // 替换成功后需重新查找一遍，更新search结果(count, curIndex)
  const { count, currentIndex } = findTextInPPT(
    presentation,
    text,
    true,
    isCaseSensitive
  )
  emitEditorEvent('FindText', { count, curIndex: currentIndex })
  if (isExecuteReplace) {
    return { ['replaceNum']: isReplaceAll ? replaceableNum : 1 }
  }
  return null
}

function replaceTextInPresentation(
  presentation: Presentation,
  newStr: string,
  isReplaceAll: boolean,
  searchResultId: number
) {
  const textDoc = presentation.getCurrentTextTarget()
  if (textDoc) {
    textDoc.removeSelection()
  }
  startEditAction(
    isReplaceAll
      ? EditActionFlag.action_Document_ReplaceAll
      : EditActionFlag.action_Document_ReplaceSingle
  )
  // TODO - 考虑添加新的事件, 应对全局替换文本较多时需要分片的情况
  if (true === isReplaceAll) {
    searchManager.replaceAll(newStr)
  } else {
    searchManager.replace(newStr, searchResultId, false)
  }

  presentation.lockInterfaceEvents = true
  calculateForRendering()
  getAndUpdateCursorPositionForPresentation(presentation)
  presentation.lockInterfaceEvents = false
}
