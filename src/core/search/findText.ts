import { InstanceType } from '../instanceTypes'
import { SlideElement } from '../SlideElement/type'
import { SearchType } from '../common/const/doc'
import { TextDocument } from '../TextDocument/TextDocument'
import { hookManager, Hooks } from '../hooks'
import { Paragraph } from '../Paragraph/Paragraph'
import { RunElementsCollector } from '../Paragraph/RunElementsCollector'
import { Presentation } from '../Slide/Presentation'
import { Slide } from '../Slide/Slide'
import { emitEditorEvent } from '../utilities/shape/hookInvokers'
import { getNextSearchIdForPresentation, selectSearchResultById } from './utils'
import { searchManager } from './SearchManager'
import {
  ParagraphSearchParams,
  ParagraphSearchElement,
  SearchResultOfParagraph,
} from './searchStates'
import { ParagraphElementPosition } from '../Paragraph/common'
import { ParagraphItem } from '../Paragraph/type'
import { ParaText } from '../Paragraph/RunElement/ParaText'
import { ParaSpace } from '../Paragraph/RunElement/ParaSpace'
import { ParaTab } from '../Paragraph/RunElement/ParaTab'

interface FindTextResult {
  currentIndex?: number
  count: number
}

export function findTextInPPT(
  presentation: Presentation,
  text: string,
  isNext: boolean,
  isCaseSensitive = false,
  noJump = false
): FindTextResult {
  const props = { caseSensitive: isCaseSensitive }
  if (noJump) {
    const { count } = findTextInPresentation(presentation, text, props, true)
    return { count }
  }
  const { count, useCache } = findTextInPresentation(presentation, text, props)

  let curIndex
  if (count) {
    const id = useCache
      ? getNextSearchIdForPresentation(presentation, isNext)!
      : '1'
    selectSearchResultById(id)
    curIndex = searchManager.getIndexById(id + '')
    if (curIndex >= 0) curIndex += 1
  }

  return {
    currentIndex: curIndex,
    count,
  }
}
/**
 * 搜索操作, 不包括跳转下一条逻辑, 视情况刷新页面
 * @notes 接收变更/搜索框输入文本重新查找不希望跳页, 此时需要发送事件通知外部总量变更
 *
 * */

export function findTextInPresentation(
  presentation: Presentation,
  str: string,
  props: { caseSensitive: boolean },
  noJump = false
) {
  const preCount = searchManager.searchElementsCount
  if (true === searchManager.matchSearchCondition(str, props)) {
    // 避免段时间内上次查找设置的 flag 导致后续异步的 calc 重置了 searchManager 状态
    searchManager.isClearSearch = false
    return { count: preCount, useCache: true }
  }
  const preSearchText = searchManager.searchText
  if (preSearchText !== '') {
    searchManager.clear()
  }

  searchManager.set(str, props)

  for (let i = 0; i < presentation.slides.length; ++i) {
    findTextInSlide(presentation.slides[i], str, props, SearchType.Common)
  }
  if (preSearchText !== '') {
    searchManager.isClearSearch = true
  }

  const count = searchManager.searchElementsCount
  if (noJump) {
    hookManager.invoke(Hooks.EditorUI.OnFirePaint)
    emitEditorEvent('FindText', { count })
    return { count }
  }
  // 没有结果导致不会进入 selectSearchResult 流程, 因此需要手动刷新一次
  if (preCount && !count) {
    hookManager.invoke(Hooks.EditorUI.OnFirePaint)
  }
  return { count }
}

function findTextInSlide(slide: Slide, str, props, type) {
  const spTree = slide.cSld.spTree
  for (const sp of spTree) {
    findTextInSlideElement(sp, str, props, type)
  }
  if (slide.shapeOfNotes) {
    findTextInSlideElement(slide.shapeOfNotes, str, props, type)
  }
}

/**
 * SlideElement 搜索文本的统一入口
 * @notes ImageShape/CxnShape 无文本
 */
function findTextInSlideElement(
  sp: SlideElement,
  str: string,
  props = { caseSensitive: false },
  type = SearchType.Common
) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      if (sp.txBody && sp.txBody.textDoc) {
        findTextInDocument(sp.txBody.textDoc, str, props, type)
      }
      break
    case InstanceType.ImageShape:
      break
    case InstanceType.GroupShape:
      const childSpLen = sp.slideElementsInGroup.length
      for (let i = 0; i < childSpLen; ++i) {
        findTextInSlideElement(sp.slideElementsInGroup[i], str, props, type)
      }
      break
    case InstanceType.GraphicFrame:
      const table = sp.graphicObject
      if (!table) return
      const tableRows = table.children
      for (let curRow = 0, l = tableRows.length; curRow < l; curRow++) {
        const row = tableRows.at(curRow)!
        const cellsCount = row.cellsCount
        for (let curCell = 0; curCell < cellsCount; curCell++) {
          findTextInDocument(
            row.getCellByIndex(curCell)!.textDoc,
            str,
            props,
            type
          )
        }
      }
      break
  }
}
/**
 * call Paragraph.findText, 本地缓存一份 ParaRun[] 的搜索结果
 * * call ParaRun/ParaHyperLink.findText, 查询内容存入 searchManager.element 中
 */
function findTextInDocument(document: TextDocument, str, props, type) {
  const count = document.children.length
  for (let index = 0; index < count; index++) {
    findTextInParagraph(document.children.at(index)!, str, props, type)
  }
}

/** */
const MaxStrLen = 100

/** 填充 searchManager, 将结果缓存在 this.searchResultElements 中 */
export function findTextInParagraph(paragraph: Paragraph, str, props, type) {
  const searchParams = new ParagraphSearchParams(paragraph, str, props, type)
  const contentLen = paragraph.children.length
  for (let i = 0; i < contentLen; i++) {
    const paraItem = paragraph.children.at(i)!
    searchParams.pos.updatePosByLevel(i, 0)
    findTextInParaItem(paraItem, searchParams, 1)
  }

  for (const k in paragraph.searchResultElements) {
    const startPosition = paragraph.searchResultElements[k].startPosition
    const endPosition = paragraph.searchResultElements[k].endPosition
    let result = ''

    if (str.length >= MaxStrLen) {
      result = '<b>'
      for (let i = 0; i < MaxStrLen - 1; i++) {
        result += str[i]
      }
      result += '</b>...'
    } else {
      result = '<b>' + str + '</b>'

      const count = MaxStrLen - str.length
      const runElmsAfter = new RunElementsCollector<
        ParaText | ParaSpace | ParaTab
      >(endPosition, count, [
        InstanceType.ParaText,
        InstanceType.ParaSpace,
        InstanceType.ParaTab,
      ])
      const runElmsBefore = new RunElementsCollector<
        ParaText | ParaSpace | ParaTab
      >(startPosition, count, [
        InstanceType.ParaText,
        InstanceType.ParaSpace,
        InstanceType.ParaTab,
      ])

      runElmsAfter.updateNextRunElementsPosInParagraph(paragraph)
      runElmsBefore.updatePrevRunElementsPosInParagraph(paragraph)

      const halfLeaveCount = count / 2

      if (
        runElmsAfter.elements.length >= halfLeaveCount &&
        runElmsBefore.elements.length >= halfLeaveCount
      ) {
        for (let i = 0; i < halfLeaveCount; i++) {
          const elemBefore = runElmsBefore.elements[i]
          const elemAfter = runElmsAfter.elements[i]

          result =
            (InstanceType.ParaText === elemBefore.instanceType
              ? elemBefore.value
              : ' ') +
            result +
            (InstanceType.ParaText === elemAfter.instanceType
              ? elemAfter.value
              : ' ')
        }
      } else if (runElmsAfter.elements.length < halfLeaveCount) {
        let count = runElmsAfter.elements.length
        for (let i = 0; i < count; i++) {
          const item = runElmsAfter.elements[i]
          result =
            result +
            (InstanceType.ParaText === item.instanceType ? item.value : ' ')
        }

        count = Math.min(
          2 * halfLeaveCount - runElmsAfter.elements.length,
          runElmsBefore.elements.length
        )
        for (let i = 0; i < count; i++) {
          const item = runElmsBefore.elements[i]
          result =
            (InstanceType.ParaText === item.instanceType ? item.value : ' ') +
            result
        }
      } else {
        let count = runElmsAfter.elements.length
        for (let i = 0; i < count; i++) {
          const item = runElmsAfter.elements[i]
          result =
            result +
            (InstanceType.ParaText === item.instanceType ? item.value : ' ')
        }

        count = runElmsBefore.elements.length
        for (let i = 0; i < count; i++) {
          const item = runElmsBefore.elements[i]
          result =
            (InstanceType.ParaText === item.instanceType ? item.value : ' ') +
            result
        }
      }
    }

    paragraph.searchResultElements[k].result = result
  }
}

export function findTextInParaItem(
  paraItem: ParagraphItem,
  searchParams: ParagraphSearchParams,
  lvl: number
) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    paraItem.searchResults = []

    const l = paraItem.children.length
    for (let i = 0; i < l; i++) {
      const run = paraItem.children.at(i)!

      searchParams.pos.updatePosByLevel(i, lvl)

      findTextInParaItem(run, searchParams, lvl + 1)
    }
  } else {
    /** 填充 this.searchResultElements 及 searchManager*/
    paraItem.searchResults = []

    const paraGraph = searchParams.paragraph
    const str = searchParams.searchText
    const props = searchParams.props
    const type = searchParams.type

    const isCaseSensitive = props.caseSensitive

    const l = paraItem.children.length

    for (let i = 0; i < l; i++) {
      const item = paraItem.children.at(i)!
      const char = str[searchParams.searchCursor]
      const isSpaceEqual =
        ' ' === char && InstanceType.ParaSpace === item.instanceType
      const isTextEqual =
        InstanceType.ParaText === item.instanceType &&
        ((true !== isCaseSensitive &&
          String.fromCodePoint(item.value) &&
          String.fromCodePoint(item.value).toLowerCase() ===
            char.toLowerCase()) ||
          (true === isCaseSensitive &&
            str &&
            item.value === str.charCodeAt(searchParams.searchCursor)))
      if (isSpaceEqual || isTextEqual) {
        if (0 === searchParams.searchCursor) {
          const startElmPosition = searchParams.pos.clone()
          startElmPosition.updatePosByLevel(i, lvl)
          searchParams.startPosition = startElmPosition
        }

        searchParams.searchCursor++

        if (searchParams.searchCursor === str.length) {
          const endElmPosition = searchParams.pos.clone()
          endElmPosition.updatePosByLevel(i + 1, lvl)

          const id = searchManager.add(paraGraph)
          addSearchResultInParagraph(
            paraGraph,
            id,
            searchParams.startPosition!,
            endElmPosition,
            type
          )

          searchParams.reset()
        }
      } else if (0 !== searchParams.searchCursor) {
        searchParams.reset()
        // remacth from current index
        i -= 1
      }
    }
  }
}

function addSearchResultInParagraph(
  paragraph: Paragraph,
  id,
  startElmPosition: ParagraphElementPosition,
  endElmPosition: ParagraphElementPosition,
  type
) {
  const searchResult = new ParagraphSearchElement(
    startElmPosition,
    endElmPosition,
    type,
    id
  )

  paragraph.searchResultElements[id] = searchResult
  searchResult.searchStartItems.push(this)
  searchResult.searchEndItems.push(this)

  let paraItem = paragraph.children.at(startElmPosition.get(0))
  paraItem &&
    addSearchResultInParaItem(paraItem, searchResult, true, startElmPosition, 1)

  paraItem = paragraph.children.at(endElmPosition.get(0))
  paraItem &&
    addSearchResultInParaItem(paraItem, searchResult, false, endElmPosition, 1)
}

function addSearchResultInParaItem(
  paraItem: ParagraphItem,
  searchResult: ParagraphSearchElement,
  isStart: boolean,
  elementPosition: ParagraphElementPosition,
  lvl: number
) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    if (true === isStart) searchResult.searchStartItems.push(paraItem)
    else searchResult.searchEndItems.push(paraItem)

    paraItem.searchResults.push(
      new SearchResultOfParagraph(searchResult, isStart, lvl)
    )

    const run = paraItem.children.at(elementPosition.get(lvl))
    run &&
      addSearchResultInParaItem(
        run,
        searchResult,
        isStart,
        elementPosition,
        lvl + 1
      )
  } else {
    if (true === isStart) searchResult.searchStartItems.push(paraItem)
    else searchResult.searchEndItems.push(paraItem)

    paraItem.searchResults.push(
      new SearchResultOfParagraph(searchResult, isStart, lvl)
    )
  }
}
