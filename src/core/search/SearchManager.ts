import { Dict } from '../../../liber/pervasive'
import { InstanceType } from '../instanceTypes'
import { Paragraph } from '../Paragraph/Paragraph'
import { ParagraphItem } from '../Paragraph/type'
import {
  clearClosestPositions,
  updateClosestPosForParagraph,
} from '../Paragraph/utils/position'
import { updateSelectionStateForParagraph } from '../Paragraph/utils/selection'
import { ParagraphSearchElement } from './searchStates'
export class SearchManager {
  searchText: string
  caseSensitive: boolean
  id: number
  searchElementsCount: number
  private searchElements: Dict<Paragraph>
  currentSearchId: number
  isSearchNext: boolean
  selection: boolean
  isClearSearch = true
  constructor() {
    this.searchText = ''
    this.caseSensitive = false
    this.id = 0
    this.searchElementsCount = 0
    this.searchElements = {}
    this.currentSearchId = -1
    this.isSearchNext = true
    this.selection = false
  }

  set(text: string, props: { caseSensitive: boolean }) {
    this.searchText = text
    this.caseSensitive = props.caseSensitive
    this.selection = true
  }

  saveSearchState() {
    if (this.searchText) {
      return { text: this.searchText, caseSensitive: this.caseSensitive }
    }
  }

  reset() {
    this.searchText = ''
    this.caseSensitive = false
  }

  matchSearchCondition(text: string, props: { caseSensitive: boolean }) {
    if (
      this.searchText === text &&
      this.caseSensitive === props.caseSensitive
    ) {
      return true
    }
    return false
  }

  clear() {
    this.searchText = ''
    this.caseSensitive = false

    for (const sid in this.searchElements) {
      const paragraph = this.searchElements[sid]
      for (const id in paragraph.searchResultElements) {
        const searchResult = paragraph.searchResultElements[id]

        let count = searchResult.searchStartItems.length
        for (let i = 1; i < count; i++) {
          searchResult.searchStartItems[i].searchResults = []
        }

        count = searchResult.searchEndItems.length
        for (let i = 1; i < count; i++) {
          searchResult.searchEndItems[i].searchResults = []
        }
      }

      paragraph.searchResultElements = {}
    }

    this.id = 0
    this.searchElementsCount = 0
    this.searchElements = {}
    this.currentSearchId = -1
    this.isSearchNext = true
    this.selection = false
  }

  add(para: Paragraph) {
    this.searchElementsCount++
    this.id++
    this.searchElements[this.id] = para
    return this.id
  }

  select(id) {
    const paragraph = this.searchElements[id]
    if (null != paragraph) {
      const searchedElement = paragraph.searchResultElements[id]
      if (null != searchedElement) {
        paragraph.selection.isUse = true
        paragraph.selection.start = false

        updateSelectionStateForParagraph(
          paragraph,
          searchedElement.startPosition,
          searchedElement.endPosition
        )
        paragraph.updateCursorPosByPosition(
          searchedElement.startPosition,
          false,
          -1
        )

        paragraph.selectSelf()
      }

      this.currentSearchId = id
    }
  }

  getIndexById(id: string): number {
    const idArr = Object.keys(this.searchElements)
    return idArr.indexOf(id)
  }

  resetCurrentSearchId() {
    this.currentSearchId = -1
  }

  replace(newText: string, id: number, isRestorePos: boolean) {
    const paragraph = this.searchElements[id]
    if (null != paragraph) {
      const paraSearchElm = paragraph.searchResultElements[id]
      if (null != paraSearchElm) {
        let pos, startPosition, endPosition, hasSelection
        if (true === isRestorePos) {
          hasSelection = paragraph.hasTextSelection()
          pos = paragraph.currentElementPosition(false, false)
          startPosition = paragraph.currentElementPosition(true, true)
          endPosition = paragraph.currentElementPosition(true, false)

          updateClosestPosForParagraph(paragraph, {
            paragraph: paragraph,
            pos: pos,
          })
          updateClosestPosForParagraph(paragraph, {
            paragraph: paragraph,
            pos: startPosition,
          })
          updateClosestPosForParagraph(paragraph, {
            paragraph: paragraph,
            pos: endPosition,
          })
        }

        const startElmPosition = paraSearchElm.startPosition
        const startRun =
          paraSearchElm.searchStartItems[
            paraSearchElm.searchStartItems.length - 1
          ]

        const runPos = startElmPosition.get(
          paraSearchElm.searchStartItems.length - 1
        )
        if (startRun.instanceType === InstanceType.ParaRun) {
          startRun.insertText(newText, runPos)
        }

        paragraph.selection.isUse = true
        updateSelectionStateForParagraph(
          paragraph,
          paraSearchElm.startPosition,
          paraSearchElm.endPosition
        )
        paragraph.remove(1)

        paragraph.removeSelection()
        paragraph.updateCursorPosByPosition(
          paraSearchElm.startPosition,
          true,
          -1
        )

        this.searchElementsCount--

        removeSearchResultForParagraph(paragraph, id)
        delete this.searchElements[id]

        if (true === isRestorePos) {
          updateSelectionStateForParagraph(
            paragraph,
            startPosition,
            endPosition
          )
          paragraph.updateCursorPosByPosition(pos, true, -1)
          paragraph.selection.isUse = hasSelection
          clearClosestPositions(paragraph)
        }
      }
    }
  }

  replaceAll(newStr) {
    for (let id = this.id; id >= 0; --id) {
      if (this.searchElements[id]) this.replace(newStr, id, true)
    }

    this.clear()
  }

  setDirection(v) {
    this.isSearchNext = v
  }

  getDirection() {
    return this.isSearchNext
  }
}

export const searchManager = new SearchManager()

function removeSearchResultForParagraph(paragraph: Paragraph, id: number) {
  const searchResult = paragraph.searchResultElements[id]
  if (undefined !== searchResult) {
    let count = searchResult.searchStartItems.length
    for (let i = 1; i < count; i++) {
      removeSearchResultForParaItem(
        searchResult.searchStartItems[i],
        searchResult
      )
    }

    count = searchResult.searchEndItems.length
    for (let i = 1; i < count; i++) {
      removeSearchResultForParaItem(
        searchResult.searchEndItems[i],
        searchResult
      )
    }

    delete paragraph.searchResultElements[id]
  }
}

function removeSearchResultForParaItem(
  searchItem: ParagraphItem,
  searchResult: ParagraphSearchElement
) {
  let marksCount = searchItem.searchResults.length
  for (let i = 0; i < marksCount; i++) {
    const mark = searchItem.searchResults[i]
    if (searchResult === mark.searchResult) {
      searchItem.searchResults.splice(i, 1)
      i--
      marksCount--
    }
  }
}
