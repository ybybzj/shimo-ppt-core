import { SearchType } from '../common/const/doc'
import { ParagraphElementPosition } from '../Paragraph/common'
import { Paragraph } from '../Paragraph/Paragraph'
import { ParagraphItem } from '../Paragraph/type'

//----------------------------------------------------------------------------------------------------------------------
export class ParagraphSearchParams {
  paragraph: Paragraph
  searchText: string
  props: { caseSensitive: boolean }
  type: (typeof SearchType)[keyof typeof SearchType]
  pos: ParagraphElementPosition
  startPosition: ParagraphElementPosition | null
  searchCursor: number
  constructor(paragraph, text, props, type) {
    this.paragraph = paragraph
    this.searchText = text
    this.props = props
    this.type = type

    this.pos = new ParagraphElementPosition()

    this.startPosition = null
    this.searchCursor = 0
  }

  reset() {
    this.startPosition = null
    this.searchCursor = 0
  }
}

export class SearchResultOfParagraph {
  searchResult: ParagraphSearchElement
  isStart: boolean
  level: number
  constructor(
    searchResult: ParagraphSearchElement,
    isStart: boolean,
    lvl: number
  ) {
    this.searchResult = searchResult
    this.isStart = isStart
    this.level = lvl
  }
}

//----------------------------------------------------------------------------------------------------------------------
// CParagraphSearchElement

//----------------------------------------------------------------------------------------------------------------------
export class ParagraphSearchElement {
  startPosition: ParagraphElementPosition
  endPosition: ParagraphElementPosition
  type: any
  result: string
  id: any
  searchStartItems: Array<ParagraphItem>
  searchEndItems: Array<ParagraphItem>
  constructor(
    startPosition: ParagraphElementPosition,
    endPosition: ParagraphElementPosition,
    type,
    id
  ) {
    this.startPosition = startPosition
    this.endPosition = endPosition
    this.type = type
    this.result = ''
    this.id = id

    this.searchStartItems = []
    this.searchEndItems = []
  }
}
