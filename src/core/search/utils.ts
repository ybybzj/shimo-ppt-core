import { Nullable } from '../../../liber/pervasive'
import { isDict } from '../../common/utils'
import { InstanceType } from '../instanceTypes'
import { SlideElement } from '../SlideElement/type'
import { TableSelectionType } from '../common/const/table'
import { TextDocument } from '../TextDocument/TextDocument'
import { hookManager, Hooks } from '../hooks'
import { ParagraphElementPosition } from '../Paragraph/common'
import { ParaHyperlink } from '../Paragraph/ParaContent/ParaHyperlink'
import { ParaRun } from '../Paragraph/ParaContent/ParaRun'
import { Paragraph } from '../Paragraph/Paragraph'
import { Presentation } from '../Slide/Presentation'
import { Slide } from '../Slide/Slide'
import { getPresentation } from '../utilities/finders'
import { searchManager } from './SearchManager'
import { ParagraphItem } from '../Paragraph/type'
import {
  getEndPosOfParagraph,
  getStartPosOfParagraph,
} from '../Paragraph/utils/position'
/** 根据 searchManager 中存储的 id 选中对应 paragraph 的 searchResult */
export function selectSearchResultById(id) {
  searchManager.select(id)
  hookManager.invoke(Hooks.EditorUI.OnSyncInterfaceState)
  hookManager.invoke(Hooks.EditorUI.OnUpdateSelectionState)
  hookManager.invoke(Hooks.EditorUI.OnFirePaint)
  hookManager.invoke(Hooks.EditorUI.OnFireNotePaint)
}
/**
 * - 获取当前 searchResult 的下/上一条 id(index)
 * - 搜索顺序依赖于 spTree 中的顺序, 即层级顺序
 * */
export function getNextSearchIdForPresentation(
  presentation: Presentation,
  isNext = true
) {
  if (presentation.slides.length > 0) {
    let i, id: Nullable<string>
    let startIndexForGroup = 0
    let shouldSkipCurNote = false
    let startIndex
    const curSlideIndex = presentation.currentSlideIndex
    const curSlide = presentation.slides[curSlideIndex]
    const slideNoteShape = curSlide.shapeOfNotes
    const spTree = curSlide.cSld.spTree
    const curTextObj = presentation.selectionState.selectedTextSp
    // 若当前已有选中文本且存在搜索结果, 则直接返回结果 id
    if (curTextObj) {
      id = getNextSearchResultIdForSlideElement(curTextObj, isNext, true)
      if (id) return id
    }

    const selectionState = presentation.selectionState
    if (isNext) {
      // 当前已选中组合, 从组合优先搜索
      if (selectionState.groupSelection) {
        const selectedGroup = selectionState.groupSelection.group
        const childSps = selectedGroup.slideElementsInGroup
        for (i = 0; i < childSps.length; ++i) {
          if (
            childSps[i].selected &&
            childSps[i].instanceType === InstanceType.Shape
          ) {
            // 当前选中则需跳过寻找下一个
            const isCurrent = isDict(curTextObj)
            id = getNextSearchResultIdForSlideElement(
              childSps[i],
              isNext,
              isCurrent
            )
            if (id) return id
            startIndexForGroup = i + 1
          }
        }
        for (i = startIndexForGroup; i < childSps.length; ++i) {
          if (childSps[i].instanceType === InstanceType.Shape) {
            id = getNextSearchResultIdForSlideElement(
              childSps[i],
              isNext,
              false
            )
            if (id) return id
          }
        }
        for (i = 0; i < spTree.length; ++i) {
          if (spTree[i] === selectedGroup) {
            startIndex = i + 1
            break
          }
        }
        if (i === spTree.length) {
          startIndex = spTree.length
        }
      }
      // 选中 Notes, 则跳过当前评论
      else if (presentation.isNotesFocused) {
        startIndex = spTree.length
        shouldSkipCurNote = true
      }
      // 选中 sp, 从下一个 sp 开始搜索
      else {
        for (i = 0; i < spTree.length; ++i) {
          if (spTree[i].selected) {
            startIndex = curTextObj ? i + 1 : i
            break
          }
        }
        if (i === spTree.length) {
          startIndex = spTree.length
        }
      }

      // 从当前选中的对象下标开始搜索
      id = getNextSearchIdForSlide(curSlide, isNext, startIndex)
      if (id) return id

      if (slideNoteShape && !shouldSkipCurNote) {
        id = getNextSearchResultIdForSlideElement(slideNoteShape, isNext, false)
        if (id) return id
      }
      // 去下一张Slide/从头开始 继续搜索
      for (i = curSlideIndex + 1; i < presentation.slides.length; ++i) {
        id = getNextSearchIdForSlide(presentation.slides[i], isNext, 0)
        if (id) return id
        const afterNoteShape = presentation.slides[i].shapeOfNotes
        if (afterNoteShape) {
          id = getNextSearchResultIdForSlideElement(
            afterNoteShape,
            isNext,
            false
          )
          if (id) return id
        }
      }
      for (i = 0; i <= curSlideIndex; ++i) {
        id = getNextSearchIdForSlide(presentation.slides[i], isNext, 0)
        if (id) return id
        const preNoteShape = presentation.slides[i].shapeOfNotes
        if (preNoteShape) {
          id = getNextSearchResultIdForSlideElement(preNoteShape, isNext, false)
          if (id) return id
        }
      }
    }
    // 搜索上一条结果, 基础逻辑同搜索下一条, 主要区别在于搜索的优先顺序调整
    else {
      if (selectionState.groupSelection) {
        const selectedGroup = selectionState.groupSelection.group
        const childSps = selectedGroup.slideElementsInGroup
        for (i = childSps.length - 1; i > -1; --i) {
          if (
            childSps[i].selected &&
            childSps[i].instanceType === InstanceType.Shape
          ) {
            const isCurrent = isDict(curTextObj)
            id = getNextSearchResultIdForSlideElement(
              childSps[i],
              isNext,
              isCurrent
            )
            if (id) return id
            startIndexForGroup = i - 1
          }
        }
        for (i = startIndexForGroup; i > -1; --i) {
          if (childSps[i].instanceType === InstanceType.Shape) {
            id = getNextSearchResultIdForSlideElement(
              childSps[i],
              isNext,
              false
            )
            if (id) return id
          }
        }
        for (i = 0; i < spTree.length; ++i) {
          if (spTree[i] === selectedGroup) {
            startIndex = i - 1
            break
          }
        }
        if (i === spTree.length) {
          startIndex = -1
        }
      } else if (presentation.isNotesFocused) {
        startIndex = spTree.length - 1
      } else {
        for (i = spTree.length - 1; i > -1; --i) {
          if (spTree[i].selected) {
            startIndex = curTextObj ? i - 1 : i
            break
          }
        }
        if (i === spTree.length) {
          startIndex = -1
        }
      }
      // 搜索上一条默认忽略当前 notes(因为已经是最后一条)
      id = getNextSearchIdForSlide(curSlide, isNext, startIndex)
      if (id) return id
      // 去上一张Slide/从末尾开始, 继续搜索,注意搜索上一条时应优先搜索 notes
      for (i = curSlideIndex - 1; i > -1; --i) {
        const preNoteShape = presentation.slides[i].shapeOfNotes
        if (preNoteShape) {
          id = getNextSearchResultIdForSlideElement(preNoteShape, isNext, false)
          if (id) return id
        }
        const startIndex = presentation.slides[i].cSld.spTree.length - 1
        id = getNextSearchIdForSlide(presentation.slides[i], isNext, startIndex)
        if (id) return id
      }
      for (i = presentation.slides.length - 1; i >= curSlideIndex; --i) {
        const afterNoteShape = presentation.slides[i].shapeOfNotes
        if (afterNoteShape) {
          id = getNextSearchResultIdForSlideElement(
            afterNoteShape,
            isNext,
            false
          )
          if (id) return id
        }
        const startIndex = presentation.slides[i].cSld.spTree.length - 1
        id = getNextSearchIdForSlide(presentation.slides[i], isNext, startIndex)
        if (id) return id
      }
    }
  }
  return null
}

function getNextSearchIdForSlide(
  slide: Slide,
  isNext: boolean,
  startIndex: number
) {
  const spTree = slide.cSld.spTree
  let i
  if (isNext) {
    for (i = startIndex; i < spTree.length; ++i) {
      const id = getNextSearchResultIdForSlideElement(spTree[i], isNext, false)
      if (id) return id
    }
  } else {
    for (i = startIndex; i > -1; --i) {
      const id = getNextSearchResultIdForSlideElement(spTree[i], isNext, false)
      if (id) return id
    }
  }
  return
}

function getNextSearchResultIdForSlideElement(
  sp: SlideElement,
  isNext: boolean,
  isCurrent: boolean
): Nullable<string> {
  let id
  switch (sp.instanceType) {
    case InstanceType.Shape:
      if (sp.txBody && sp.txBody.textDoc) {
        return getIdForSearchForTextDocument(
          sp.txBody.textDoc,
          isNext,
          isCurrent
        )
      }
      return
    case InstanceType.ImageShape:
      return
    case InstanceType.GroupShape:
      let current = -1
      const childSps = sp.slideElementsInGroup

      const selectionState = getPresentation(sp)?.selectionState
      const selectedTextSp = selectionState?.selectedTextSp
      if (true === isCurrent && selectedTextSp) {
        for (let i = 0; i < childSps.length; ++i) {
          if (childSps[i] === selectedTextSp) {
            current = i
            break
          }
        }
      }
      if (true === isNext) {
        const startIndex = -1 !== current ? current : 0
        for (let i = startIndex; i < childSps.length; i++) {
          id = getNextSearchResultIdForSlideElement(
            childSps[i],
            true,
            i === current
          )
          if (id) return id
        }
      } else {
        const startIndex = -1 !== current ? current : childSps.length - 1
        for (let i = startIndex; i >= 0; i--) {
          id = getNextSearchResultIdForSlideElement(
            childSps[i],
            false,
            i === current
          )
          if (id) return id
        }
      }
      return
    case InstanceType.GraphicFrame:
      const table = sp.graphicObject!
      if (true === isCurrent) {
        let curRow = 0
        let curCell = 0
        if (
          true === table.selection.isUse &&
          TableSelectionType.CELL === table.selection.type
        ) {
          const pos =
            true === isNext
              ? table.selection.cellPositions[
                  table.selection.cellPositions.length - 1
                ]
              : table.selection.cellPositions[0]
          curRow = pos.row
          curCell = pos.curCell
        } else {
          id = getIdForSearchForTextDocument(
            table.curCell!.textDoc,
            isNext,
            true
          )
          if (id) return id

          curRow = table.curCell!.parent.index
          curCell = table.curCell!.index
        }

        const rowsCount = table.children.length
        if (true === isNext) {
          for (
            let rowIndex = Math.max(curRow, 0);
            rowIndex < rowsCount;
            rowIndex++
          ) {
            const row = table.children.at(rowIndex)!
            const cellsCount = row.cellsCount
            const startCell = rowIndex === curRow ? curCell + 1 : 0
            for (
              let cellIndex = startCell;
              cellIndex < cellsCount;
              cellIndex++
            ) {
              const cell = row.getCellByIndex(cellIndex)!
              id = getIdForSearchForTextDocument(cell.textDoc, true, false)
              if (id) return id
            }
          }
        } else {
          for (
            let rowIndex = Math.min(curRow, rowsCount - 1);
            rowIndex >= 0;
            rowIndex--
          ) {
            const row = table.children.at(rowIndex)!
            const cellsCount = row.cellsCount
            const startCell = rowIndex === curRow ? curCell - 1 : cellsCount - 1
            for (let cellIndex = startCell; cellIndex >= 0; cellIndex--) {
              const cell = row.getCellByIndex(cellIndex)!
              id = getIdForSearchForTextDocument(cell.textDoc, false, false)
              if (id) return id
            }
          }
        }
      } else {
        const rowsCount = table.children.length
        if (true === isNext) {
          for (let rowIndex = 0; rowIndex < rowsCount; rowIndex++) {
            const row = table.children.at(rowIndex)!
            const cellsCount = row.cellsCount
            for (let cellIndex = 0; cellIndex < cellsCount; cellIndex++) {
              const cell = row.getCellByIndex(cellIndex)!
              id = getIdForSearchForTextDocument(cell.textDoc, true, false)
              if (id) return id
            }
          }
        } else {
          for (let rowIndex = rowsCount - 1; rowIndex >= 0; rowIndex--) {
            const row = table.children.at(rowIndex)!
            const cellsCount = row.cellsCount
            for (let cellIndex = cellsCount - 1; cellIndex >= 0; cellIndex--) {
              const cell = row.getCellByIndex(cellIndex)!
              id = getIdForSearchForTextDocument(cell.textDoc, false, false)
              if (id) return id
            }
          }
        }
      }
      return id
  }
}

function getIdForSearchForTextDocument(
  document: TextDocument,
  isNext: boolean,
  isCurrent
): Nullable<string> {
  let id

  if (true === isCurrent) {
    let elmIndex = document.cursorPosition.childIndex
    if (true === document.selection.isUse) {
      elmIndex =
        true === isNext
          ? Math.max(document.selection.startIndex, document.selection.endIndex)
          : Math.min(document.selection.startIndex, document.selection.endIndex)
    }

    if (true === isNext) {
      const para = document.children.at(elmIndex)
      id = para ? getIdForSearchForParagraph(para, true, true) : null

      if (null != id) {
        return id
      }

      elmIndex++

      const count = document.children.length
      while (elmIndex < count) {
        const para = document.children.at(elmIndex)
        id = para ? getIdForSearchForParagraph(para, true, false) : null
        if (null != id) {
          return id
        }

        elmIndex++
      }
    } else {
      const para = document.children.at(elmIndex)
      id = para ? getIdForSearchForParagraph(para, false, true) : null

      if (null != id) {
        return id
      }

      elmIndex--

      while (elmIndex >= 0) {
        const para = document.children.at(elmIndex)
        id = para ? getIdForSearchForParagraph(para, false, false) : null
        if (null != id) {
          return id
        }

        elmIndex--
      }
    }
  } else {
    const l = document.children.length
    if (true === isNext) {
      let index = 0
      while (index < l) {
        id = getIdForSearchForParagraph(
          document.children.at(index)!,
          true,
          false
        )
        if (null != id) {
          return id
        }

        index++
      }
    } else {
      let index = l - 1
      while (index >= 0) {
        id = getIdForSearchForParagraph(
          document.children.at(index)!,
          false,
          false
        )
        if (null != id) {
          return id
        }

        index--
      }
    }
  }

  return null
}

function getIdForSearchForParagraph(
  paragraph: Paragraph,
  isNext: boolean,
  isCurrent: boolean
) {
  let pos: ParagraphElementPosition

  if (true === isCurrent) {
    if (true === paragraph.selection.isUse) {
      let startPosition = paragraph.currentElementPosition(true, true)
      let enedPosition = paragraph.currentElementPosition(true, false)

      if (startPosition.compare(enedPosition) > 0) {
        const t = enedPosition
        enedPosition = startPosition
        startPosition = t
      }

      if (true === isNext) pos = enedPosition
      else pos = startPosition
    } else pos = paragraph.currentElementPosition(false, false)
  } else {
    if (true === isNext) pos = getStartPosOfParagraph(paragraph)
    else pos = getEndPosOfParagraph(paragraph, false)
  }

  if (true === isNext) {
    const startIndex = pos.get(0)
    const contentLen = paragraph.children.length

    for (let i = startIndex; i < contentLen; i++) {
      const elementId = getIdForSearchForParaItem(
        paragraph.children.at(i)!,
        true,
        i === startIndex ? true : false,
        pos,
        1
      )
      if (null !== elementId) return elementId
    }
  } else {
    const startIndex = pos.get(0)

    for (let i = startIndex; i >= 0; i--) {
      const item = paragraph.children.at(i)
      const elementId = item
        ? getIdForSearchForParaItem(
            item,
            false,
            i === startIndex ? true : false,
            pos,
            1
          )
        : null
      if (null != elementId) return elementId
    }
  }

  return null
}

function getIdForSearchForParaItem(
  paraItem: ParagraphItem,
  isNext: boolean,
  fromElmPosition: boolean,
  elementPosition: ParagraphElementPosition,
  lvl: number
) {
  if (paraItem.instanceType === InstanceType.ParaHyperlink) {
    return getIdForSearchForHyperlink(
      paraItem,
      isNext,
      fromElmPosition,
      elementPosition,
      lvl
    )
  } else {
    return getIdForSearchForPararRun(
      paraItem,
      isNext,
      fromElmPosition,
      elementPosition,
      lvl
    )
  }
}

function getIdForSearchForHyperlink(
  paraHyperlink: ParaHyperlink,
  isNext: boolean,
  fromElmPosition: boolean,
  pos: ParagraphElementPosition,
  lvl: number
) {
  let startIndex = 0

  if (true === fromElmPosition) {
    startIndex = pos.get(lvl)
  } else {
    if (true === isNext) {
      startIndex = 0
    } else {
      startIndex = paraHyperlink.children.length - 1
    }
  }

  if (true === isNext) {
    const l = paraHyperlink.children.length

    for (let i = startIndex; i < l; i++) {
      const run = paraHyperlink.children.at(i)
      const elementId = run
        ? getIdForSearchForPararRun(
            run,
            true,
            fromElmPosition && i === startIndex ? true : false,
            pos,
            lvl + 1
          )
        : null
      if (null != elementId) return elementId
    }
  } else {
    for (let i = startIndex; i >= 0; i--) {
      const run = paraHyperlink.children.at(i)
      const elementId = run
        ? getIdForSearchForPararRun(
            run,
            false,
            fromElmPosition && i === startIndex ? true : false,
            pos,
            lvl + 1
          )
        : null
      if (null != elementId) return elementId
    }
  }

  return null
}

function getIdForSearchForPararRun(
  paraRun: ParaRun,
  isNext: boolean,
  fromElmPosition: boolean,
  elementPosition: ParagraphElementPosition,
  lvl: number
) {
  let startIndex = 0

  if (true === fromElmPosition) {
    startIndex = elementPosition.get(lvl)
  } else {
    if (true === isNext) {
      startIndex = 0
    } else {
      startIndex = paraRun.children.length
    }
  }

  let retId = null

  if (true === isNext) {
    let adjPosIdx = paraRun.children.length

    for (let sIdx = 0, l = paraRun.searchResults.length; sIdx < l; sIdx++) {
      const res = paraRun.searchResults[sIdx]
      const resIdx = res.searchResult.startPosition.get(res.level)

      if (
        res.searchResult.searchStartItems.length > 0 &&
        paraRun ===
          res.searchResult.searchStartItems[
            res.searchResult.searchStartItems.length - 1
          ] &&
        resIdx >= startIndex &&
        resIdx < adjPosIdx
      ) {
        retId = res.searchResult.id
        adjPosIdx = resIdx
      }
    }
  } else {
    let adjPosIdx = -1

    for (let sIdx = 0, l = paraRun.searchResults.length; sIdx < l; sIdx++) {
      const res = paraRun.searchResults[sIdx]
      const resIdx = res.searchResult.startPosition.get(res.level)

      if (
        res.searchResult.searchStartItems.length > 0 &&
        paraRun ===
          res.searchResult.searchStartItems[
            res.searchResult.searchStartItems.length - 1
          ] &&
        resIdx < startIndex &&
        resIdx > adjPosIdx
      ) {
        retId = res.searchResult.id
        adjPosIdx = resIdx
      }
    }
  }

  return retId
}
