import { Nullable } from '../../../liber/pervasive'

type CanvasImage = HTMLCanvasElement & { ctx: CanvasRenderingContext2D }
export class CacheImage {
  image: null | CanvasImage
  isLocked: number
  availableCount: number
  constructor() {
    this.image = null
    this.isLocked = 0
    this.availableCount = 0
  }
  reset() {
    this.image = null
    this.isLocked = 0
    this.availableCount = 0
  }
}

const MIN_IMAGE_COUNT = 1
export class ImagePoolManager {
  private images: CacheImage[]
  private count: number
  constructor() {
    this.images = []
    this.count = 0
  }
  private _freeUnusedImages() {
    for (let i = 0; i < this.count; ++i) {
      if (
        this.images[i].isLocked === 0 &&
        this.images[i].availableCount >= MIN_IMAGE_COUNT
      ) {
        this.images[i].reset()
        this.images.splice(i, 1)
        --i
        --this.count
      }
    }
  }

  release(cacheImage: Nullable<CacheImage>) {
    if (null == cacheImage) return

    cacheImage.isLocked = 0
    cacheImage.availableCount = 0
  }

  getImage(w: number, h: number): CacheImage {
    for (let i = 0; i < this.count; ++i) {
      if (this.images[i].isLocked) continue
      if (this.images[i].image != null) {
        const wI = this.images[i].image!.width
        const hI = this.images[i].image!.height
        if (wI === w && hI === h) {
          this.images[i].isLocked = 1
          this.images[i].availableCount = 0

          initCacheImage(this.images[i], w, h)

          return this.images[i]
        }
      }
      this.images[i].availableCount++
    }
    this._freeUnusedImages()
    const index = this.count
    this.count++

    this.images[index] = new CacheImage()
    const image = document.createElement('canvas') as CanvasImage

    image.width = w
    image.height = h

    const ctx = image.getContext('2d')!
    image.ctx = ctx

    this.images[index].image = image
    this.images[index].isLocked = 1
    this.images[index].availableCount = 0
    initCacheImage(this.images[index], w, h)
    return this.images[index]
  }

  clear() {
    for (let i = 0; i < this.count; ++i) {
      this.images[i].reset()
      this.images.splice(i, 1)
      --i
      --this.count
    }
  }
}

function initCacheImage(img: CacheImage, w: number, h: number) {
  img.image!.width = w
  img.image!.height = h
  img.image!.ctx.globalAlpha = 1.0

  img.image!.ctx.clearRect(0, 0, w, h)
}
