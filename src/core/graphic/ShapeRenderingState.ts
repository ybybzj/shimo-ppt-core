import { Nullable } from '../../../liber/pervasive'
import { FillKIND } from '../common/const/attrs'
import { ScaleOfPPTXSizes } from '../common/const/unit'
import { Matrix2D } from './Matrix'
import { defaultRGBAColor } from '../color/complexColor'
import { FillEffects } from '../SlideElement/attrs/fill/fill'
import { Ln } from '../SlideElement/attrs/line'

export interface ShapeRenderingState {
  /** 手动设置的 x 偏移量, 如背景填充中会用到 */
  l: number
  /** 手动设置的 y 偏移量, 如背景填充中会用到 */
  t: number
  extX: number
  extY: number

  slideWidth: number
  slideHeight: number

  fillEffects: Nullable<FillEffects>
  Ln: Nullable<Ln>
  isTexture: boolean
  isNoFillForce: boolean
  /** 人为设置过无边框 */
  isNoStrokeForce: boolean
  isNoSmartForce: boolean
  fillRGBAColor: Nullable<{
    r: number
    g: number
    b: number
    a: number
  }>
  StrokeRGBAColor: Nullable<{
    r: number
    g: number
    b: number
    a: number
  }>
  strokeWidth: number
  orgLineJoin: any

  isRect: boolean
  useBgFill: boolean
  useBgFillMatrix?: Matrix2D
  // presentation: Nullable<Presentation>
  // slide: Nullable<Slide>
  // bounds
  minX: number
  minY: number
  maxX: number
  maxY: number
}

export function getInitState(): ShapeRenderingState {
  return {
    l: 0,
    t: 0,
    extX: 0,
    extY: 0,
    slideWidth: 0,
    slideHeight: 0,
    fillEffects: null,
    Ln: null,
    isTexture: false,
    isNoFillForce: false,
    /** 人为设置过无边框 */
    isNoStrokeForce: false,
    isNoSmartForce: false,
    fillRGBAColor: null,
    StrokeRGBAColor: null,
    strokeWidth: 0,
    orgLineJoin: null,

    isRect: false,
    useBgFill: false,
    useBgFillMatrix: undefined,
    // presentation: null,
    // slide: null,
    minX: 0xffff,
    minY: 0xffff,
    maxX: -0xffff,
    maxY: -0xfff,
  }
}

interface FillEffectsInfo {
  noFill: boolean
  isTexture: boolean
  fillRGBAColor?: { r: number; g: number; b: number; a: number }
  isCheckBounds?: boolean
}

export function getFillEffectsInfo(
  fillEffects: Nullable<FillEffects>
): FillEffectsInfo {
  const info: FillEffectsInfo = {
    noFill: false,
    isTexture: false,
  }

  if (fillEffects == null || fillEffects.fill == null) {
    info.noFill = true
  } else {
    const fill = fillEffects.fill
    switch (fill.type) {
      case FillKIND.BLIP: {
        info.isTexture = true
        break
      }
      case FillKIND.SOLID: {
        if (fill.color) {
          info.fillRGBAColor = fill.color.rgba
        } else {
          info.fillRGBAColor = defaultRGBAColor()
        }
        break
      }
      case FillKIND.GRAD: {
        const c = fill.gsList
        if (c.length === 0) info.fillRGBAColor = defaultRGBAColor()
        else {
          if (fill.gsList[0].color) {
            info.fillRGBAColor = fill.gsList[0].color.rgba
          } else {
            info.fillRGBAColor = defaultRGBAColor()
          }
        }

        info.isCheckBounds = true

        break
      }
      case FillKIND.PATT: {
        info.isCheckBounds = true
        break
      }
      case FillKIND.NOFILL: {
        info.noFill = true
        break
      }
      default: {
        info.noFill = true
        break
      }
    }
    if (info.isTexture === true) {
      info.isCheckBounds = true
    }
  }

  return info
}

interface LnRenderingInfo {
  noStroke: boolean
  StrokeRGBAColor?: {
    r: number
    g: number
    b: number
    a: number
  }
  strokeWidth?: number
}

export function getLnInfo(ln: Nullable<Ln>): LnRenderingInfo {
  const info: LnRenderingInfo = {
    noStroke: false,
  }
  if (ln == null || ln.fillEffects == null || ln.fillEffects.fill == null) {
    info.noStroke = true
  } else {
    const fill = ln.fillEffects.fill
    switch (fill.type) {
      case FillKIND.BLIP: {
        info.StrokeRGBAColor = defaultRGBAColor()
        break
      }
      case FillKIND.SOLID: {
        if (fill.color) {
          info.StrokeRGBAColor = fill.color.rgba
        } else {
          info.StrokeRGBAColor = defaultRGBAColor()
        }
        break
      }
      case FillKIND.GRAD: {
        const c = fill.gsList
        if (c.length === 0) info.StrokeRGBAColor = defaultRGBAColor()
        else {
          if (fill.gsList[0].color) {
            info.StrokeRGBAColor = fill.gsList[0].color.rgba
          } else {
            info.StrokeRGBAColor = defaultRGBAColor()
          }
        }

        break
      }
      case FillKIND.PATT: {
        if (fill.fgClr) {
          info.StrokeRGBAColor = fill.fgClr.rgba
        } else {
          info.StrokeRGBAColor = defaultRGBAColor()
        }
        break
      }
      case FillKIND.NOFILL: {
        info.noStroke = true
        break
      }
      default: {
        info.noStroke = true
        break
      }
    }

    info.strokeWidth = ln.w == null ? 12700 : parseInt('' + ln.w)
    info.strokeWidth /= ScaleOfPPTXSizes
  }
  return info
}
