import { EditLayerDrawer } from '../../ui/rendering/Drawer/EditLayerDrawer'
import { GraphicsBoundsChecker } from './Bounds'
import { CanvasDrawer } from './CanvasDrawer'

export interface ClipRect {
  x: number
  y: number
  w: number
  h: number
}

export type ShapeDrawerContext =
  | EditLayerDrawer
  | GraphicsBoundsChecker
  | CanvasDrawer

export type CanvasPathLike =
  | CanvasRenderingContext2D
  | Path2D
  | OffscreenCanvasRenderingContext2D
