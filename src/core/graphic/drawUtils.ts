import { BrowserInfo } from '../../common/browserInfo'
import { isErrorMediaUrl } from '../../images/errorImageSrc'
import {
  GifIndicatorIcon,
  PictureErrorLoadingImage,
  PictureLoadingImage,
  VideoPlayButtonImage,
} from '../../images/images'
import { translator } from '../../globals/translator'
import { Factor_mm_to_pix, Factor_pix_to_mm } from '../common/const/unit'
import { ShapeRenderingState } from './ShapeRenderingState'
import { ImageWidgetType } from '../utilities/shape/image'
import { CanvasDrawer } from './CanvasDrawer'
import { deg2rad, IMatrix2D, Matrix2D, MatrixUtils, rad2deg } from './Matrix'
import { LineEndType } from '../SlideElement/const'
import { Nullable, valuesOfDict } from '../../../liber/pervasive'
import { FillGradPathType, FillKIND } from '../common/const/attrs'
import { GradientDirections } from '../../exports/type'
import { SpRenderingContext } from '../render/drawShape/type'
import { getCanvasDrawerContext, getTransformFromDrawCxt } from './utils'
import { FillEffects } from '../SlideElement/attrs/fill/fill'
import { BGCOLOR_DRAW_IMAGE } from './const'
import { PresetLineDashStyleValues } from '../../io/dataType/spAttrs'
import { RelativeRect } from '../SlideElement/attrs/fill/RelativeRect'
import { InstanceType, isInstanceTypeOf } from '../instanceTypes'

export function drawRect(
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  r: number,
  b: number,
  isClever?: boolean
) {
  if (isClever) ctx.rect(x + 0.5, y + 0.5, r - x + 1, b - y + 1)
  else {
    ctx.moveTo(x, y)
    ctx.rect(x, y, r - x + 1, b - y + 1)
  }
}

export function drawRectByLineTo(
  ctx: CanvasRenderingContext2D,
  x1: number,
  y1: number,
  x2: number,
  y2: number,
  x3: number,
  y3: number,
  x4: number,
  y4: number
) {
  ctx.moveTo(x1, y1)
  ctx.lineTo(x2, y2)
  ctx.lineTo(x4, y4)
  ctx.lineTo(x3, y3)
  ctx.closePath()

  ctx.stroke()
}

export function drawAclinicRectByLineTo(
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  r: number,
  b: number
) {
  const _x = x + 0.5
  const _y = y + 0.5
  const _r = r + 0.5
  const _b = b + 0.5
  ctx.moveTo(x, _y)
  ctx.lineTo(r - 1, _y)

  ctx.moveTo(_r, y)
  ctx.lineTo(_r, b - 1)

  ctx.moveTo(r + 1, _b)
  ctx.lineTo(x + 2, _b)

  ctx.moveTo(_x, b + 1)
  ctx.lineTo(_x, y + 2)

  ctx.stroke()
}

// 无倾斜rect
export function drawAclinicRectWithDashBorder(
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  r: number,
  b: number,
  dashDotW: number,
  dashDistW: number
) {
  const _x = x + 0.5
  const _y = y + 0.5
  const _r = r + 0.5
  const _b = b + 0.5

  if (typeof ctx.setLineDash === 'function') {
    ctx.setLineDash([dashDotW, dashDistW])

    //ctx.rect(x + 0.5, y + 0.5, r - x, b - y);
    ctx.moveTo(x, _y)
    ctx.lineTo(r - 1, _y)

    ctx.moveTo(_r, y)
    ctx.lineTo(_r, b - 1)

    ctx.moveTo(r + 1, _b)
    ctx.lineTo(x + 2, _b)

    ctx.moveTo(_x, b + 1)
    ctx.lineTo(_x, y + 2)

    ctx.stroke()
    ctx.setLineDash([])
    return
  }

  for (let i = x; i < r; i += dashDistW) {
    ctx.moveTo(i, _y)
    i += dashDotW

    if (i > r - 1) i = r - 1

    ctx.lineTo(i, _y)
  }
  for (let i = y; i < b; i += dashDistW) {
    ctx.moveTo(_r, i)
    i += dashDotW

    if (i > b - 1) i = b - 1

    ctx.lineTo(_r, i)
  }
  for (let i = r + 1; i > x + 1; i -= dashDistW) {
    ctx.moveTo(i, _b)
    i -= dashDotW

    if (i < x + 2) i = x + 2

    ctx.lineTo(i, _b)
  }
  for (let i = b + 1; i > y + 1; i -= dashDistW) {
    ctx.moveTo(_x, i)
    i -= dashDotW

    if (i < y + 2) i = y + 2

    ctx.lineTo(_x, i)
  }

  ctx.stroke()
}

function drawDashLine(
  ctx: CanvasRenderingContext2D,
  x1: number,
  y1: number,
  x2: number,
  y2: number,
  dashDotW: number,
  dashDistW: number
) {
  let len = Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1))
  if (len < 1) len = 1

  const lx1 = Math.abs((dashDotW * (x2 - x1)) / len)
  const ly1 = Math.abs((dashDotW * (y2 - y1)) / len)
  const lx2 = Math.abs((dashDistW * (x2 - x1)) / len)
  const ly2 = Math.abs((dashDistW * (y2 - y1)) / len)

  if (lx1 < 0.01 && ly1 < 0.01) return
  if (lx2 < 0.01 && ly2 < 0.01) return

  if (x1 <= x2 && y1 <= y2) {
    for (let i = x1, j = y1; i <= x2 && j <= y2; i += lx2, j += ly2) {
      ctx.moveTo(i, j)

      i += lx1
      j += ly1

      if (i > x2) i = x2
      if (j > y2) j = y2

      ctx.lineTo(i, j)
    }
  } else if (x1 <= x2 && y1 > y2) {
    for (let i = x1, j = y1; i <= x2 && j >= y2; i += lx2, j -= ly2) {
      ctx.moveTo(i, j)

      i += lx1
      j -= ly1

      if (i > x2) i = x2
      if (j < y2) j = y2

      ctx.lineTo(i, j)
    }
  } else if (x1 > x2 && y1 <= y2) {
    for (let i = x1, j = y1; i >= x2 && j <= y2; i -= lx2, j += ly2) {
      ctx.moveTo(i, j)

      i -= lx1
      j += ly1

      if (i < x2) i = x2
      if (j > y2) j = y2

      ctx.lineTo(i, j)
    }
  } else {
    for (let i = x1, j = y1; i >= x2 && j >= y2; i -= lx2, j -= ly2) {
      ctx.moveTo(i, j)

      i -= lx1
      j -= ly1

      if (i < x2) i = x2
      if (j < y2) j = y2

      ctx.lineTo(i, j)
    }
  }
}

export function drawRectWithDashBorder(
  ctx: CanvasRenderingContext2D,
  x1: number,
  y1: number,
  x2: number,
  y2: number,
  x3: number,
  y3: number,
  x4: number,
  y4: number,
  dashDotW: number,
  dashDistW: number
) {
  if (typeof ctx.setLineDash === 'function') {
    ctx.setLineDash([dashDotW, dashDistW])

    ctx.moveTo(x1, y1)
    ctx.lineTo(x2, y2)
    ctx.lineTo(x4, y4)
    ctx.lineTo(x3, y3)
    ctx.closePath()

    ctx.stroke()
    ctx.setLineDash([])
    return
  }

  drawDashLine(ctx, x1, y1, x2, y2, dashDotW, dashDistW)
  drawDashLine(ctx, x2, y2, x4, y4, dashDotW, dashDistW)
  drawDashLine(ctx, x4, y4, x3, y3, dashDotW, dashDistW)
  drawDashLine(ctx, x3, y3, x1, y1, dashDotW, dashDistW)

  ctx.stroke()
}

const DASH_PATTERNS = [
  [4, 3], // dash
  [4, 3, 1, 3], // dashDot
  [1, 3], // dot
  [8, 3], // lgDash
  [8, 3, 1, 3], // lgDashDot
  [8, 3, 1, 3, 1, 3], // lgDashDotDot
  undefined, // solid
  [3, 1], // sysDash
  [3, 1, 1, 1], // sysDashDot
  [3, 1, 1, 1, 1, 1], // sysDashDotDot
  [1, 1], // sysDot
] as const

export function setPenDash(
  prstDash: Nullable<PresetLineDashStyleValues>,
  canvasDrawer: CanvasDrawer,
  w: number
) {
  if (prstDash != null && DASH_PATTERNS[prstDash]) {
    const arr = DASH_PATTERNS[prstDash]!.slice()
    for (let i = 0; i < arr.length; i++) {
      arr[i] *= w
    }
    canvasDrawer.setPenDash(arr)
  } else {
    canvasDrawer.setPenDash(null)
  }
}

export function updatePenDash(
  state: ShapeRenderingState,
  canvasDrawer: CanvasDrawer
) {
  setPenDash(state.Ln?.prstDash, canvasDrawer, state.strokeWidth)
}

export function getScaleFactor(zoom: number) {
  return (zoom * Factor_mm_to_pix * BrowserInfo.PixelRatio) / 100
}

function formatPoint(x0, y0, angle, x1, y1) {
  const cos = Math.cos(angle)
  const sin = Math.sin(angle)

  const ex = -sin
  const ey = cos

  const a = cos / sin
  const b = ex / ey

  const x = (a * b * y1 - a * b * y0 - (a * x1 - b * x0)) / (b - a)
  const y = (x - x0) / a + y0

  return { x, y }
}

export function createGradientPoints(minX, minY, maxX, maxY, deg, scale) {
  const points = { x0: 0, y0: 0, x1: 0, y1: 0 }

  deg = deg / 60000
  while (deg < 0) deg += 360
  while (deg >= 360) deg -= 360

  if (Math.abs(deg) < 1) {
    points.x0 = minX
    points.y0 = minY
    points.x1 = maxX
    points.y1 = minY

    return points
  } else if (Math.abs(deg - 90) < 1) {
    points.x0 = minX
    points.y0 = minY
    points.x1 = minX
    points.y1 = maxY

    return points
  } else if (Math.abs(deg - 180) < 1) {
    points.x0 = maxX
    points.y0 = minY
    points.x1 = minX
    points.y1 = minY

    return points
  } else if (Math.abs(deg - 270) < 1) {
    points.x0 = minX
    points.y0 = maxY
    points.x1 = minX
    points.y1 = minY

    return points
  }

  let rad = deg2rad(deg)
  if (!scale) {
    if (deg > 0 && deg < 90) {
      const p = formatPoint(minX, minY, rad, maxX, maxY)

      points.x0 = minX
      points.y0 = minY
      points.x1 = p.x
      points.y1 = p.y

      return points
    }
    if (deg > 90 && deg < 180) {
      const p = formatPoint(maxX, minY, rad, minX, maxY)

      points.x0 = maxX
      points.y0 = minY
      points.x1 = p.x
      points.y1 = p.y

      return points
    }
    if (deg > 180 && deg < 270) {
      const p = formatPoint(maxX, maxY, rad, minX, minY)

      points.x0 = maxX
      points.y0 = maxY
      points.x1 = p.x
      points.y1 = p.y

      return points
    }
    if (deg > 270 && deg < 360) {
      const p = formatPoint(minX, maxY, rad, maxX, minY)

      points.x0 = minX
      points.y0 = maxY
      points.x1 = p.x
      points.y1 = p.y

      return points
    }

    return points
  }

  const quadGrad = Math.PI / 2 - Math.atan2(maxY - minY, maxX - minX)
  const supplymentaryQuadGrad = Math.PI / 2 - quadGrad
  if (deg > 0 && deg < 90) {
    if (deg <= 45) {
      rad = (quadGrad * deg) / 45
    } else {
      rad = quadGrad + (supplymentaryQuadGrad * (deg - 45)) / 45
    }

    const p = formatPoint(minX, minY, rad, maxX, maxY)

    points.x0 = minX
    points.y0 = minY
    points.x1 = p.x
    points.y1 = p.y

    return points
  }
  if (deg > 90 && deg < 180) {
    if (deg <= 135) {
      rad = Math.PI / 2 + (supplymentaryQuadGrad * (deg - 90)) / 45
    } else {
      rad = Math.PI - (quadGrad * (deg - 135)) / 45
    }

    const p = formatPoint(maxX, minY, rad, minX, maxY)

    points.x0 = maxX
    points.y0 = minY
    points.x1 = p.x
    points.y1 = p.y

    return points
  }
  if (deg > 180 && deg < 270) {
    if (deg <= 225) {
      rad = Math.PI + (quadGrad * (deg - 180)) / 45
    } else {
      rad = (3 * Math.PI) / 2 - (supplymentaryQuadGrad * (deg - 225)) / 45
    }

    const p = formatPoint(maxX, maxY, rad, minX, minY)

    points.x0 = maxX
    points.y0 = maxY
    points.x1 = p.x
    points.y1 = p.y

    return points
  }
  if (deg > 270 && deg < 360) {
    if (deg <= 315) {
      rad = (3 * Math.PI) / 2 + (supplymentaryQuadGrad * (deg - 270)) / 45
    } else {
      rad = 2 * Math.PI - (quadGrad * (deg - 315)) / 45
    }

    const p = formatPoint(minX, maxY, rad, maxX, minY)

    points.x0 = minX
    points.y0 = maxY
    points.x1 = p.x
    points.y1 = p.y

    return points
  }

  return points
}

export function drawLoadingImage(
  g: CanvasDrawer,
  x: number,
  y: number,
  w: number,
  h: number
) {
  g.save()
  g.setFillStyle(BGCOLOR_DRAW_IMAGE)
  g.rect(x, y, w, h)
  g.clip()
  g.fillRect(x, y, w, h)
  const width = PictureLoadingImage.width * Factor_pix_to_mm
  const height = PictureLoadingImage.height * Factor_pix_to_mm
  g._drawImageElement(
    PictureLoadingImage,
    x + w / 2 - width / 2,
    y + h / 2 - height / 2,
    width,
    height
  )
  g.restore()
}

export function drawLoadFailedImage(
  g: CanvasDrawer,
  x: number,
  y: number,
  w: number,
  h: number
) {
  // 图片加载失败
  const textCenterX = x + w / 2
  const textCenterY = y + h / 2 + 10

  // let isNoIntGrid = false

  g.setLineWidth(1)
  g.save()
  g.setFillStyle(BGCOLOR_DRAW_IMAGE)
  g.rect(x, y, w, h)
  g.clip()
  g.fillRect(x, y, w, h)
  if (!g.isInShowMode) {
    const iconOnlyWidth = 70
    const iconOnlyHeight = 40
    let width = PictureErrorLoadingImage.width * Factor_pix_to_mm
    if (w >= iconOnlyWidth && h >= iconOnlyHeight) {
      g._drawImageElement(
        PictureErrorLoadingImage,
        x + w / 2 - width / 2,
        y + h / 2 - width / 2 - 7,
        width,
        width
      )
      g.setFillStyle('rgba(65, 70, 75, 0.6)')

      let scale = (g.ZoomValue * BrowserInfo.PixelRatio) / 100
      if (!MatrixUtils.isOnlyTranslate(g.getTransform())) {
        scale /= g.getCoordTransformScale().sx
      }
      g.setCtxFont(`${26 * scale}px PingFang SC`)
      g.setCtxTextAlign('center')
      g.setCtxTextBaseline('middle')

      const text = translator.getValue('加载失败，点击重试')
      const { width: textWidth } = g.measureText(text)
      const textWidthInMM = textWidth * Factor_pix_to_mm
      if (textWidthInMM > width) {
        g.setCtxFont(
          `${26 * scale - (textWidthInMM / width) * 1.8}px PingFang SC`
        )
      }
      g.fillCtxText(text, textCenterX, textCenterY)
    } else {
      width = Math.min(h, Math.min(w, width))

      g._drawImageElement(
        PictureErrorLoadingImage,
        x + w / 2 - width / 2,
        y + h / 2 - width / 2,
        width,
        width
      )
    }
  }
  g.restore()
}

export function drawImageTypeDecoration(
  g: CanvasDrawer,
  imgSrc: string,
  x: number,
  y: number,
  w: number,
  h: number
) {
  g.save()
  switch (g.imageWidgetType) {
    case ImageWidgetType.Video:
      if (!isErrorMediaUrl(imgSrc)) {
        // 绘制视频播放按钮
        const { width, height } = getImageSize(VideoPlayButtonImage, w, h)
        g._drawImageElement(
          VideoPlayButtonImage,
          x + w / 2 - width / 2,
          y + h / 2 - height / 2,
          width,
          height
        )
      }
      break
    case ImageWidgetType.Gif:
      const { width, height } = getImageSize(GifIndicatorIcon, w, h)
      g._drawImageElement(
        GifIndicatorIcon,
        x + w - width - 16 * Factor_pix_to_mm,
        y + 16 * Factor_pix_to_mm,
        width,
        height
      )
      break
  }
  g.restore()
}
function getImageSize(image, w: number, h: number) {
  let width = (image.width / 2) * Factor_pix_to_mm
  let height = (image.height / 2) * Factor_pix_to_mm
  if (w < width) {
    height = (height / width) * w
    width = w
  }
  if (h < height) {
    width = (width / height) * h
    height = h
  }
  return { width, height }
}

export function drawPointsQueue(
  state: ShapeRenderingState,
  canvasDrawer: CanvasDrawer
) {
  const arr = canvasDrawer.pointsCollection
  if (arr != null && arr.length > 1) {
    canvasDrawer.setPenDash(null)

    const invertTrans = canvasDrawer._invertTransform()

    const x1 = canvasDrawer._XFromPoint(0, 0)
    const y1 = canvasDrawer._YFromPoint(0, 0)
    const x2 = canvasDrawer._XFromPoint(1, 1)
    const y2 = canvasDrawer._YFromPoint(1, 1)
    const factor = Math.sqrt(
      ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) / 2
    )

    const cxt = canvasDrawer.context
    const widthOfPen = cxt.lineWidth * factor
    let maxW
    if (canvasDrawer.isRenderThumbnail === true) maxW = 2

    const maxEps = 0.001

    if (state.Ln!.headEnd != null) {
      const x1 = canvasDrawer._XFromPoint(arr[0].x, arr[0].y)
      const y1 = canvasDrawer._YFromPoint(arr[0].x, arr[0].y)
      let x2 = canvasDrawer._XFromPoint(arr[1].x, arr[1].y)
      let y2 = canvasDrawer._YFromPoint(arr[1].x, arr[1].y)

      const maxDelta = Math.max(state.Ln!.headEnd.getLen(widthOfPen), 5)

      let delta = Math.max(Math.abs(x1 - x2), Math.abs(y1 - y2))
      let curP = 2
      while (delta < maxDelta && curP < arr.length) {
        x2 = canvasDrawer._XFromPoint(arr[curP].x, arr[curP].y)
        y2 = canvasDrawer._YFromPoint(arr[curP].x, arr[curP].y)
        delta = Math.max(Math.abs(x1 - x2), Math.abs(y1 - y2))
        curP++
      }

      if (delta > maxEps) {
        canvasDrawer.pointsCollection = null
        drawLineEnd(
          x1,
          y1,
          x2,
          y2,
          state.Ln!.headEnd.type,
          state.Ln!.headEnd.getWidth(widthOfPen, maxW),
          state.Ln!.headEnd.getLen(widthOfPen, maxW),
          state,
          canvasDrawer,
          invertTrans
        )
        canvasDrawer.pointsCollection = arr
      }
    }

    if (state.Ln!.tailEnd != null) {
      const idx1 = arr.length - 1
      const idx2 = arr.length - 2
      const x1 = canvasDrawer._XFromPoint(arr[idx1].x, arr[idx1].y)
      const y1 = canvasDrawer._YFromPoint(arr[idx1].x, arr[idx1].y)
      let x2 = canvasDrawer._XFromPoint(arr[idx2].x, arr[idx2].y)
      let y2 = canvasDrawer._YFromPoint(arr[idx2].x, arr[idx2].y)

      const maxEpsDelta = Math.max(state.Ln!.tailEnd.getLen(widthOfPen), 5)

      let max_delta = Math.max(Math.abs(x1 - x2), Math.abs(y1 - y2))
      let curPIdx = idx2 - 1
      while (max_delta < maxEpsDelta && curPIdx >= 0) {
        x2 = canvasDrawer._XFromPoint(arr[curPIdx].x, arr[curPIdx].y)
        y2 = canvasDrawer._YFromPoint(arr[curPIdx].x, arr[curPIdx].y)
        max_delta = Math.max(Math.abs(x1 - x2), Math.abs(y1 - y2))
        curPIdx--
      }

      if (max_delta > maxEps) {
        canvasDrawer.pointsCollection = null
        drawLineEnd(
          x1,
          y1,
          x2,
          y2,
          state.Ln!.tailEnd.type,
          state.Ln!.tailEnd.getWidth(widthOfPen, maxW),
          state.Ln!.tailEnd.getLen(widthOfPen, maxW),
          state,
          canvasDrawer,
          invertTrans
        )
        canvasDrawer.pointsCollection = arr
      }
    }
    updatePenDash(state, canvasDrawer)
  }
}

function drawLineEnd(
  endOfX: number,
  endOfY: number,
  prevOfX: number,
  prevOfY: number,
  type: valuesOfDict<typeof LineEndType> | undefined,
  w: number,
  len: number,
  state: ShapeRenderingState,
  g: CanvasDrawer,
  trans: Matrix2D
) {
  switch (type) {
    case LineEndType.None:
      break
    case LineEndType.Arrow: {
      let ex = prevOfX - endOfX
      let ey = prevOfY - endOfY
      const elen = Math.sqrt(ex * ex + ey * ey)
      ex /= elen
      ey /= elen

      const vx = ey
      const vy = -ex

      const tmpx = endOfX + len * ex
      const tmpy = endOfY + len * ey

      const x1 = tmpx + (vx * w) / 2
      const y1 = tmpy + (vy * w) / 2

      const x3 = tmpx - (vx * w) / 2
      const y3 = tmpy - (vy * w) / 2

      g._begin()
      g._moveTo(trans.XFromPoint(x1, y1), trans.YFromPoint(x1, y1))
      g._lineTo(
        trans.XFromPoint(endOfX, endOfY),
        trans.YFromPoint(endOfX, endOfY)
      )
      g._lineTo(trans.XFromPoint(x3, y3), trans.YFromPoint(x3, y3))
      drawStrokeByState(state, g)
      g._end()

      break
    }
    case LineEndType.Diamond: {
      let ex = prevOfX - endOfX
      let ey = prevOfY - endOfY
      const elen = Math.sqrt(ex * ex + ey * ey)
      ex /= elen
      ey /= elen

      const vx = ey
      const vy = -ex

      const tmpx = endOfX + (len / 2) * ex
      const tmpy = endOfY + (len / 2) * ey

      const x1 = endOfX + (vx * w) / 2
      const y1 = endOfY + (vy * w) / 2

      const x3 = endOfX - (vx * w) / 2
      const y3 = endOfY - (vy * w) / 2

      const tmpx2 = endOfX - (len / 2) * ex
      const tmpy2 = endOfY - (len / 2) * ey

      g._begin()
      g._moveTo(trans.XFromPoint(tmpx, tmpy), trans.YFromPoint(tmpx, tmpy))
      g._lineTo(trans.XFromPoint(x1, y1), trans.YFromPoint(x1, y1))
      g._lineTo(trans.XFromPoint(tmpx2, tmpy2), trans.YFromPoint(tmpx2, tmpy2))
      g._lineTo(trans.XFromPoint(x3, y3), trans.YFromPoint(x3, y3))
      g._close()
      g.drawFillWithPenStyle()
      g._end()

      g._begin()
      g._moveTo(trans.XFromPoint(tmpx, tmpy), trans.YFromPoint(tmpx, tmpy))
      g._lineTo(trans.XFromPoint(x1, y1), trans.YFromPoint(x1, y1))
      g._lineTo(trans.XFromPoint(tmpx2, tmpy2), trans.YFromPoint(tmpx2, tmpy2))
      g._lineTo(trans.XFromPoint(x3, y3), trans.YFromPoint(x3, y3))
      g._close()
      drawStrokeByState(state, g)
      g._end()

      break
    }
    case LineEndType.Oval: {
      let ex = prevOfX - endOfX
      let ey = prevOfY - endOfY
      const elen = Math.sqrt(ex * ex + ey * ey)
      ex /= elen
      ey /= elen

      const vx = ey
      const vy = -ex

      const tmpx = endOfX + (len / 2) * ex
      const tmpy = endOfY + (len / 2) * ey

      const tmpx2 = endOfX - (len / 2) * ex
      const tmpy2 = endOfY - (len / 2) * ey

      const cx1 = tmpx + (vx * 3 * w) / 4
      const cy1 = tmpy + (vy * 3 * w) / 4
      const cx2 = tmpx2 + (vx * 3 * w) / 4
      const cy2 = tmpy2 + (vy * 3 * w) / 4

      const cx3 = tmpx - (vx * 3 * w) / 4
      const cy3 = tmpy - (vy * 3 * w) / 4
      const cx4 = tmpx2 - (vx * 3 * w) / 4
      const cy4 = tmpy2 - (vy * 3 * w) / 4

      g._begin()
      g._moveTo(trans.XFromPoint(tmpx, tmpy), trans.YFromPoint(tmpx, tmpy))
      g._bezierCurveTo(
        trans.XFromPoint(cx1, cy1),
        trans.YFromPoint(cx1, cy1),
        trans.XFromPoint(cx2, cy2),
        trans.YFromPoint(cx2, cy2),
        trans.XFromPoint(tmpx2, tmpy2),
        trans.YFromPoint(tmpx2, tmpy2)
      )

      g._bezierCurveTo(
        trans.XFromPoint(cx4, cy4),
        trans.YFromPoint(cx4, cy4),
        trans.XFromPoint(cx3, cy3),
        trans.YFromPoint(cx3, cy3),
        trans.XFromPoint(tmpx, tmpy),
        trans.YFromPoint(tmpx, tmpy)
      )

      g.drawFillWithPenStyle()
      g._end()

      g._begin()
      g._moveTo(trans.XFromPoint(tmpx, tmpy), trans.YFromPoint(tmpx, tmpy))
      g._bezierCurveTo(
        trans.XFromPoint(cx1, cy1),
        trans.YFromPoint(cx1, cy1),
        trans.XFromPoint(cx2, cy2),
        trans.YFromPoint(cx2, cy2),
        trans.XFromPoint(tmpx2, tmpy2),
        trans.YFromPoint(tmpx2, tmpy2)
      )

      g._bezierCurveTo(
        trans.XFromPoint(cx4, cy4),
        trans.YFromPoint(cx4, cy4),
        trans.XFromPoint(cx3, cy3),
        trans.YFromPoint(cx3, cy3),
        trans.XFromPoint(tmpx, tmpy),
        trans.YFromPoint(tmpx, tmpy)
      )

      drawStrokeByState(state, g)
      g._end()
      break
    }
    case LineEndType.Stealth: {
      let ex = prevOfX - endOfX
      let ey = prevOfY - endOfY
      const elen = Math.sqrt(ex * ex + ey * ey)
      ex /= elen
      ey /= elen

      const vx = ey
      const vy = -ex

      const tmpx = endOfX + len * ex
      const tmpy = endOfY + len * ey

      const x1 = tmpx + (vx * w) / 2
      const y1 = tmpy + (vy * w) / 2

      const x3 = tmpx - (vx * w) / 2
      const y3 = tmpy - (vy * w) / 2

      const x4 = endOfX + (len - w / 2) * ex
      const y4 = endOfY + (len - w / 2) * ey

      g._begin()
      g._moveTo(trans.XFromPoint(x1, y1), trans.YFromPoint(x1, y1))
      g._lineTo(
        trans.XFromPoint(endOfX, endOfY),
        trans.YFromPoint(endOfX, endOfY)
      )
      g._lineTo(trans.XFromPoint(x3, y3), trans.YFromPoint(x3, y3))
      g._lineTo(trans.XFromPoint(x4, y4), trans.YFromPoint(x4, y4))
      g._close()
      g.drawFillWithPenStyle()
      g._end()

      g._begin()
      g._moveTo(trans.XFromPoint(x1, y1), trans.YFromPoint(x1, y1))
      g._lineTo(
        trans.XFromPoint(endOfX, endOfY),
        trans.YFromPoint(endOfX, endOfY)
      )
      g._lineTo(trans.XFromPoint(x3, y3), trans.YFromPoint(x3, y3))
      g._lineTo(trans.XFromPoint(x4, y4), trans.YFromPoint(x4, y4))
      g._close()
      drawStrokeByState(state, g)
      g._end()

      break
    }
    case LineEndType.Triangle: {
      let ex = prevOfX - endOfX
      let ey = prevOfY - endOfY
      const elen = Math.sqrt(ex * ex + ey * ey)
      ex /= elen
      ey /= elen

      const vx = ey
      const vy = -ex

      const tmpx = endOfX + len * ex
      const tmpy = endOfY + len * ey

      const x1 = tmpx + (vx * w) / 2
      const y1 = tmpy + (vy * w) / 2

      const x3 = tmpx - (vx * w) / 2
      const y3 = tmpy - (vy * w) / 2

      g._begin()
      g._moveTo(trans.XFromPoint(x1, y1), trans.YFromPoint(x1, y1))
      g._lineTo(
        trans.XFromPoint(endOfX, endOfY),
        trans.YFromPoint(endOfX, endOfY)
      )
      g._lineTo(trans.XFromPoint(x3, y3), trans.YFromPoint(x3, y3))
      g._close()
      g.drawFillWithPenStyle()
      g._end()

      g._begin()
      g._moveTo(trans.XFromPoint(x1, y1), trans.YFromPoint(x1, y1))
      g._lineTo(
        trans.XFromPoint(endOfX, endOfY),
        trans.YFromPoint(endOfX, endOfY)
      )
      g._lineTo(trans.XFromPoint(x3, y3), trans.YFromPoint(x3, y3))
      g._close()
      drawStrokeByState(state, g)
      g._end()
      break
    }
  }
}

export function drawStrokeByState(
  state: ShapeRenderingState,
  g: CanvasDrawer,
  p?: Path2D
) {
  const rgba = state.StrokeRGBAColor!
  // gradient 则取 ln, 否则取计算的单色
  g.setPenColor(rgba.r, rgba.g, rgba.b, rgba.a)

  if (state.isRect) {
    if (null != state.extX) {
      g.rectWithPen(0, 0, state.extX, state.extY!, state.strokeWidth, p)
    } else g.drawStroke(p)
  } else {
    g.drawStroke(p)
  }
}

export function getGradInfoByFillInfo(
  drawContext: SpRenderingContext,
  fillEffects: Nullable<FillEffects>,
  transparent: Nullable<number>,
  x: number,
  y: number,
  w: number,
  h: number
) {
  const fill = fillEffects?.fill
  if (fill?.type !== FillKIND.GRAD) return
  const g = getCanvasDrawerContext(drawContext)

  let gradient: CanvasGradient
  if (fill.ln) {
    let angle = fill.ln.angle
    if (fill.rotateWithShape === false) {
      const transform = getTransformFromDrawCxt(drawContext)
      if (transform) {
        angle = getGradientAngleNoRotate(angle, transform)
      }
    }

    const points = createGradientPoints(
      x,
      y,
      w + x,
      h + y,
      angle,
      fill.ln.scale
    )
    gradient = g.createLinearGradient(
      points.x0,
      points.y0,
      points.x1,
      points.y1
    )
  } else if (fill.path?.path != null) {
    switch (fill.path.path) {
      case FillGradPathType.Rect:
      case FillGradPathType.Shape:
      case FillGradPathType.Circle:
        const direciton = fill.path.getDirection()
        let cx, cy
        let r = Math.max(w, h)
        switch (direciton) {
          case GradientDirections.FromBottomRightCorner:
            ;[cx, cy] = [x + w, y + h]
            break
          case GradientDirections.FromBottomLeftCorner:
            ;[cx, cy] = [x, y + h]
            break
          case GradientDirections.FromTopRightCorner:
            ;[cx, cy] = [x + w, y]
            break
          case GradientDirections.FromTopLeftCorner:
            ;[cx, cy] = [x, y]
            break
          case GradientDirections.FromCenter:
          default:
            ;[cx, cy] = [x + w / 2, y + h / 2]
            r = Math.max(w, h) / 2
            break
        }
        gradient = g.createRadialGradient(cx, cy, 1, cx, cy, r)
    }
  } else {
    const points = createGradientPoints(x, y, x + w, y + h, 0, false)
    gradient = g.createLinearGradient(
      points.x0,
      points.y0,
      points.x1,
      points.y1
    )
  }

  for (let i = 0; i < fill.gsList.length; i++) {
    gradient!.addColorStop(
      fill.gsList[i].pos / 100000,
      fill.gsList[i].color!.getCSSColorString(transparent)
    )
  }
  return gradient!
}

function getGradientAngleNoRotate(angle: number, transform: IMatrix2D) {
  const x1 = 0.0
  const y1 = 0.0
  const x2 = 1.0
  const y2 = 0.0

  let rotMartix = new Matrix2D()
  rotMartix.rotate(-angle / 60000)

  const x11 = rotMartix.XFromPoint(x1, y1)
  const y11 = rotMartix.YFromPoint(x1, y1)
  const x22 = rotMartix.XFromPoint(x2, y2)
  const y22 = rotMartix.YFromPoint(x2, y2)

  rotMartix = MatrixUtils.invertMatrix(transform)

  const _x1 = rotMartix.XFromPoint(x11, y11)
  const _y1 = rotMartix.YFromPoint(x11, y11)
  const _x2 = rotMartix.XFromPoint(x22, y22)
  const _y2 = rotMartix.YFromPoint(x22, y22)

  const y = _y2 - _y1
  const x = _x2 - _x1

  let a = 0
  if (Math.abs(y) < 0.001) {
    if (x > 0) a = 0
    else a = 180
  } else if (Math.abs(x) < 0.001) {
    if (y > 0) a = 90
    else a = 270
  } else {
    a = Math.atan2(y, x)
    a = rad2deg(a)
  }

  if (a < 0) a += 360

  return a * 60000
}

export function isSrcRect(o: any): o is RelativeRect {
  return (
    isInstanceTypeOf(o, InstanceType.RelativeRect) &&
    (o as RelativeRect).isValid()
  )
}
export function adjustCoordsByOffsetRect(
  srcRect: RelativeRect,
  srcX: number,
  srcY: number,
  srcWidth: number,
  srcHeight: number,
  targetX: number,
  targetY: number,
  targetWidth: number,
  targetHeight: number
):
  | {
      sx: number
      sy: number
      sw: number
      sh: number

      tx: number
      ty: number
      tw: number
      th: number
    }
  | undefined {
  const sw = srcWidth
  const sh = srcHeight

  let sx = srcX
  let sy = srcY
  let sr = srcX + sw
  let sb = srcY + sh
  let x = targetX
  let y = targetY
  let w = targetWidth
  let h = targetHeight

  const l = srcRect.l!
  const t = srcRect.t!
  const r = 100 - srcRect.r!
  const b = 100 - srcRect.b!

  sx += (l * sw) / 100
  sr -= (r * sw) / 100
  sy += (t * sh) / 100
  sb -= (b * sh) / 100

  let _sw = sw
  _sw -= sx
  _sw += sr - sw

  let _sh = sh
  _sh -= sy
  _sh += sb - sh

  const _w = w
  const _h = h
  if (sx < srcX) {
    x += ((srcX - sx) * _w) / _sw
    w -= ((srcX - sx) * _w) / _sw
    sx = srcX
  }
  if (sy < srcY) {
    y += ((srcY - sy) * _h) / _sh
    h -= ((srcY - sy) * _h) / _sh
    sy = srcY
  }
  if (sr > srcX + sw) {
    w -= ((sr - sw - srcX) * _w) / _sw
    sr = srcX + sw
  }
  if (sb > srcY + sh) {
    h -= ((sb - sh - srcY) * _h) / _sh
    sb = srcY + sh
  }

  if (sx >= sr || sx >= srcX + sw || sr <= srcX || w <= 0) return
  if (sy >= sb || sy >= srcY + sh || sb <= srcY || h <= 0) return

  return {
    sx: sx,
    sy: sy,
    sw: sr - sx,
    sh: sb - sy,

    tx: x,
    ty: y,
    tw: w,
    th: h,
  }
}
