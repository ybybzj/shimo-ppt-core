import { Dict, valuesOfDict } from '../../../liber/pervasive'
import { PresetPatternVal } from '../../io/dataType/spAttrs'
import { PresetPatternValToNumMap } from '../common/const/attrs'

const PATTERN_TX_SIZE = 8

function getPartAndMask(
  index: number,
  flags: valuesOfDict<typeof gPatternFillFlags>
): {
  part: number
  mask: number
} {
  index = index < 0 ? 0 : index > 63 ? 63 : index

  const partIndex = Math.floor(index / 16)
  const mask = 15 - (index % 16)
  return {
    part: flags[partIndex],
    mask,
  }
}

function getPatternBitFlag(
  flags: valuesOfDict<typeof gPatternFillFlags>,
  index: number
): boolean {
  const { part, mask } = getPartAndMask(index, flags)
  const bit = part & (1 << mask)
  return bit > 0 ? false : true
}

const pattern_brushes_cache: Dict<PatternBrush> = {}

const gPatternFillFlags = {
  ['cross']: [
    0b0000000001110111, 0b0111011101110111, 0b0000000001110111,
    0b0111011101110111,
  ],
  ['dashDnDiag']: [
    0b1111111111111111, 0b0111011110111011, 0b1101110111101110,
    0b1111111111111111,
  ],
  ['dashHorz']: [
    0b0000111111111111, 0b1111111111111111, 0b1111000011111111,
    0b1111111111111111,
  ],
  ['dashUpDiag']: [
    0b1111111111111111, 0b1110111011011101, 0b1011101101110111,
    0b1111111111111111,
  ],
  ['dashVert']: [
    0b0111111101111111, 0b0111111101111111, 0b1111011111110111,
    0b1111011111110111,
  ],
  ['diagBrick']: [
    0b1111111011111101, 0b1111101111110111, 0b1110011111011011,
    0b1011110101111110,
  ],
  ['diagCross']: [
    0b0111110110111011, 0b1101011111101111, 0b1101011110111011,
    0b0111110111111110,
  ],
  ['divot']: [
    0b1111111111101111, 0b1111011111101111, 0b1111111101111111,
    0b1111111001111111,
  ],
  ['dkDnDiag']: [
    0b0011001110011001, 0b1100110001100110, 0b0011001110011001,
    0b1100110001100110,
  ],
  ['dkHorz']: [
    0b0000000000000000, 0b1111111111111111, 0b0000000000000000,
    0b1111111111111111,
  ],
  ['dkUpDiag']: [
    0b1100110010011001, 0b0011001101100110, 0b1100110010011001,
    0b0011001101100110,
  ],
  ['dkVert']: [
    0b0011001100110011, 0b0011001100110011, 0b0011001100110011,
    0b0011001100110011,
  ],
  ['dnDiag']: [
    0b0111011110111011, 0b1101110111101110, 0b0111011110111011,
    0b1101110111101110,
  ],
  ['dotDmnd']: [
    0b0111111111111111, 0b1101110111111111, 0b1111011111111111,
    0b1101110111111111,
  ],
  ['dotGrid']: [
    0b0101010111111111, 0b0111111111111111, 0b0111111111111111,
    0b0111111111111111,
  ],
  ['horz']: [
    0b0000000011111111, 0b1111111111111111, 0b0000000011111111,
    0b1111111111111111,
  ],
  ['horzBrick']: [
    0b0000000001111111, 0b0111111101111111, 0b0000000011110111,
    0b1111011111110111,
  ],
  ['lgCheck']: [
    0b0000111100001111, 0b0000111100001111, 0b1111000011110000,
    0b1111000011110000,
  ],
  ['lgConfetti']: [
    0b0100111011001111, 0b1111110011100100, 0b0010011100111111,
    0b1111001101110010,
  ],
  ['lgGrid']: [
    0b0000000001111111, 0b0111111101111111, 0b0111111101111111,
    0b0111111101111111,
  ],
  ['ltDnDiag']: [
    0b0111011110111011, 0b1101110111101110, 0b0111011110111011,
    0b1101110111101110,
  ],
  ['ltHorz']: [
    0b0000000011111111, 0b1111111111111111, 0b0000000011111111,
    0b1111111111111111,
  ],
  ['ltUpDiag']: [
    0b1110111011011101, 0b1011101101110111, 0b1110111011011101,
    0b1011101101110111,
  ],
  ['ltVert']: [
    0b0111011101110111, 0b0111011101110111, 0b0111011101110111,
    0b0111011101110111,
  ],
  ['narHorz']: [
    0b0000000011111111, 0b0000000011111111, 0b0000000011111111,
    0b0000000011111111,
  ],
  ['narVert']: [
    0b1010101010101010, 0b1010101010101010, 0b1010101010101010,
    0b1010101010101010,
  ],
  ['openDmnd']: [
    0b0111110110111011, 0b1101011111101111, 0b1101011110111011,
    0b0111110111111110,
  ],
  ['pct10']: [
    0b0111111111111111, 0b1111011111111111, 0b0111111111111111,
    0b1111011111111111,
  ],
  ['pct20']: [
    0b0111011111111111, 0b1101110111111111, 0b0111011111111111,
    0b1101110111111111,
  ],
  ['pct25']: [
    0b0111011111011101, 0b0111011111011101, 0b0111011111011101,
    0b0111011111011101,
  ],
  ['pct30']: [
    0b0101010110111011, 0b0101010111101110, 0b0101010110111011,
    0b0101010111101110,
  ],
  ['pct40']: [
    0b0101010110101010, 0b0101010110101110, 0b0101010110101010,
    0b0101010111101010,
  ],
  ['pct5']: [
    0b0111111111111111, 0b1111111111111111, 0b1111011111111111,
    0b1111111111111111,
  ],
  ['pct50']: [
    0b0101010110101010, 0b0101010110101010, 0b0101010110101010,
    0b0101010110101010,
  ],
  ['pct60']: [
    0b0001000110101010, 0b0100010010101010, 0b0001000110101010,
    0b0100010010101010,
  ],
  ['pct70']: [
    0b1000100000100010, 0b1000100000100010, 0b1000100000100010,
    0b1000100000100010,
  ],
  ['pct75']: [
    0b1000100000000000, 0b0010001000000000, 0b1000100000000000,
    0b0010001000000000,
  ],
  ['pct80']: [
    0b0001000000000000, 0b0000000100000000, 0b0001000000000000,
    0b0000000100000000,
  ],
  ['pct90']: [
    0b0000000000000000, 0b0000000000001000, 0b0000000000000000,
    0b0000000010000000,
  ],
  ['plaid']: [
    0b0101010110101010, 0b0101010110101010, 0b0000111100001111,
    0b0000111100001111,
  ],
  ['shingle']: [
    0b1111110001111011, 0b1011011111001111, 0b1111001111111101,
    0b1111111011111110,
  ],
  ['smCheck']: [
    0b0110011010011001, 0b1001100101100110, 0b0110011010011001,
    0b1001100101100110,
  ],
  ['smConfetti']: [
    0b0111111111110111, 0b1011111111111101, 0b1110111111111110,
    0b1101111111111011,
  ],
  ['smGrid']: [
    0b0000000001110111, 0b0111011101110111, 0b0000000001110111,
    0b0111011101110111,
  ],
  ['solidDmnd']: [
    0b1110111111000111, 0b1000001100000001, 0b1000001111000111,
    0b1110111111111111,
  ],
  ['sphere']: [
    0b1000100001110110, 0b0111000001110000, 0b1000100001100111,
    0b0000011100000111,
  ],
  ['trellis']: [
    0b0000000010011001, 0b0000000001100110, 0b0000000010011001,
    0b0000000001100110,
  ],
  ['upDiag']: [
    0b1100110010011001, 0b0011001101100110, 0b1100110010011001,
    0b0011001101100110,
  ],
  ['vert']: [
    0b0111011101110111, 0b0111011101110111, 0b0111011101110111,
    0b0111011101110111,
  ],
  ['wave']: [
    0b1111111111100111, 0b1101101000111111, 0b1111111111100111,
    0b1101101000111111,
  ],
  ['wdDnDiag']: [
    0b0011111000011111, 0b1000111111000111, 0b1110001111110001,
    0b1111100001111100,
  ],
  ['wdUpDiag']: [
    0b0111110011111000, 0b1111000111100011, 0b1100011110001111,
    0b0001111100111110,
  ],
  ['weave']: [
    0b0111011110101011, 0b1101110110111011, 0b0111011111101011,
    0b1101110110101110,
  ],
  ['zigZag']: [
    0b0111111010111101, 0b1101101111100111, 0b0111111010111101,
    0b1101101111100111,
  ],
} as const

class PatternBrush {
  name: PresetPatternVal
  canvas: HTMLCanvasElement
  ctx: CanvasRenderingContext2D
  imageData: ImageData
  fgClr: {
    r: number
    g: number
    b: number
    a: number
  }
  bgClr: {
    r: number
    g: number
    b: number
    a: number
  }
  constructor(name: PresetPatternVal) {
    this.name = name
    if (null == PresetPatternValToNumMap[name]) {
      this.name = 'cross'
    }

    this.fgClr = {
      r: -1,
      g: -1,
      b: -1,
      a: 255,
    }
    this.bgClr = {
      r: -1,
      g: -1,
      b: -1,
      a: 255,
    }

    this.canvas = document.createElement('canvas')
    this.canvas.width = PATTERN_TX_SIZE
    this.canvas.height = PATTERN_TX_SIZE

    this.ctx = this.canvas.getContext('2d')!
    this.imageData = this.ctx.createImageData(PATTERN_TX_SIZE, PATTERN_TX_SIZE)
  }

  updateByColors(
    r: number,
    g: number,
    b: number,
    a: number,
    br: number,
    bg: number,
    bb: number,
    ba: number
  ) {
    if (this.ctx == null || null == this.imageData) {
      return
    }

    if (
      this.fgClr.r === r &&
      this.fgClr.g === g &&
      this.fgClr.b === b &&
      this.fgClr.a === a &&
      this.bgClr.r === br &&
      this.bgClr.g === bg &&
      this.bgClr.b === bb &&
      this.bgClr.a === ba
    ) {
      return
    }

    this.fgClr.r = r
    this.fgClr.g = g
    this.fgClr.b = b
    this.fgClr.a = a

    this.bgClr.r = br
    this.bgClr.g = bg
    this.bgClr.b = bb
    this.bgClr.a = ba

    const len = PATTERN_TX_SIZE * PATTERN_TX_SIZE
    const pattenFlag = gPatternFillFlags[this.name]

    const destData = this.imageData.data
    let index = 0

    for (let i = 0; i < len; i++) {
      const flag = getPatternBitFlag(pattenFlag, i)
      if (flag) {
        destData[index++] = r
        destData[index++] = g
        destData[index++] = b
        destData[index++] = a
      } else {
        destData[index++] = br
        destData[index++] = bg
        destData[index++] = bb
        destData[index++] = ba
      }
    }

    this.ctx.putImageData(this.imageData, 0, 0)
  }
}

export function getPatternBrush(
  name: PresetPatternVal,
  r: number,
  g: number,
  b: number,
  a: number,
  br: number,
  bg: number,
  bb: number,
  ba: number
) {
  let brush = pattern_brushes_cache[name]
  if (brush !== undefined) {
    brush.updateByColors(r, g, b, a, br, bg, bb, ba)
    return brush
  }

  brush = new PatternBrush(name)
  brush.updateByColors(r, g, b, a, br, bg, bb, ba)
  pattern_brushes_cache[name] = brush
  return brush
}
