import { BrowserInfo } from '../../common/browserInfo'
import { gDPI, Factor_in_to_mm, Factor_mm_to_pix } from '../common/const/unit'
import { InstanceType } from '../instanceTypes'
import { IMatrix2D, Matrix2D, MatrixUtils } from './Matrix'
import { EditorUtil } from '../../globals/editor'
import { toggleCxtTransformReset } from './utils'

import { Nullable } from '../../../liber/pervasive'
import { zoomFromScale } from '../utilities/zoom'
import { FontKitTextPr } from '../../fonts/FontKit'
import {
  adjustCoordsByOffsetRect,
  drawAclinicRectWithDashBorder,
  drawImageTypeDecoration,
  drawLoadFailedImage,
  drawLoadingImage,
  drawRectWithDashBorder,
  isSrcRect,
} from './drawUtils'
import {
  checkErrorImage,
  ImportErrorImageUrls,
} from '../../images/errorImageSrc'
import { hookManager, Hooks } from '../hooks'
import { FontClassValues } from '../../fonts/const'
import { RelativeRect } from '../SlideElement/attrs/fill/RelativeRect'
import {
  ImageCacheData,
  LoadableImage,
  StatusOfLoadableImage,
} from '../../images/utils'
import { BGCOLOR_DRAW_IMAGE } from './const'
import { ColorRGBA } from '../color/type'
import { ClipRect } from './type'
import { BlipFill } from '../SlideElement/attrs/fill/blipFill'

export class CanvasDrawer {
  readonly instanceType = InstanceType.CanvasDrawer
  context!: CanvasRenderingContext2D
  pointsCollection: Nullable<Array<{ x: number; y: number }>>
  isCxtTransformReset: boolean
  pattenFillTransformScaleX: number
  pattenFillTransformScaleY: number
  isInShowMode: boolean
  isNoRenderEmptyPlaceholder: boolean = false
  isInViewMode: boolean = false
  isInPreview: boolean = false
  isRenderThumbnail: Nullable<boolean> = null
  imageWidgetType: Nullable<number> = null
  isInUse: boolean = false

  penStyle: PenStyle
  brushStyle: BrushStyle
  isTextStrokeSet: boolean = false
  isTextFillSet: boolean = true
  isBgTransparent: boolean = false

  finalTransform: Matrix2D
  private invertFinalTransform: Matrix2D
  private coordinateTransform: Matrix2D
  transform: Matrix2D
  private dpiX: number
  private dpiY: number
  private drawerState: DrawerState
  private dashParams: Nullable<number[]>
  private _zoomValue = 100
  get ZoomValue() {
    return this._zoomValue
  }
  constructor() {
    this.dpiX = gDPI * BrowserInfo.PixelRatio
    this.dpiY = gDPI * BrowserInfo.PixelRatio

    this.penStyle = new PenStyle()
    this.brushStyle = new BrushStyle()

    this.coordinateTransform = new Matrix2D()
    this.transform = new Matrix2D()
    this.finalTransform = new Matrix2D()
    this.invertFinalTransform = new Matrix2D()

    this.pointsCollection = null

    this.isCxtTransformReset = true

    this.pattenFillTransformScaleX = 1
    this.pattenFillTransformScaleY = 1

    this.isInShowMode = false

    this.drawerState = new DrawerState(this)

    this.dashParams = null
  }
  resetState() {
    this.dpiX = gDPI * BrowserInfo.PixelRatio
    this.dpiY = gDPI * BrowserInfo.PixelRatio

    this.penStyle.reset()
    this.brushStyle.reset()

    this.coordinateTransform.reset()
    this.transform.reset()
    this.finalTransform.reset()
    this.invertFinalTransform.reset()

    this.pointsCollection = null

    this.isCxtTransformReset = true

    this.pattenFillTransformScaleX = 1
    this.pattenFillTransformScaleY = 1

    this.isInShowMode = false

    this.drawerState.reset()

    this.isNoRenderEmptyPlaceholder = false
    this.isInViewMode = false
    this.isInPreview = false
    this.isBgTransparent = false
    this.isTextFillSet = true
    this.isTextStrokeSet = false
    this.isRenderThumbnail = null
    this.imageWidgetType = null
    this.dashParams = null
    this.isInUse = false
    this._zoomValue = 100
  }

  init(
    context: CanvasRenderingContext2D,
    widthInPx,
    heightInPx,
    widthInMM?,
    heightInMM?
  ) {
    this.isInUse = true
    this.context = context
    const lHeightPix = heightInPx >> 0
    const lWidthPix = widthInPx >> 0
    if (widthInMM != null) {
      this.dpiX = (25.4 * lWidthPix) / widthInMM
    }
    if (heightInMM != null) {
      this.dpiY = (25.4 * lHeightPix) / heightInMM
    }
    const sx = this.dpiX / 25.4
    const sy = this.dpiY / 25.4

    this._zoomValue = zoomFromScale(Math.min(sx, sy))
    this.coordinateTransform.sx = sx
    this.coordinateTransform.sy = sy

    this.pattenFillTransformScaleX = 1 / this.coordinateTransform.sx
    this.pattenFillTransformScaleY = 1 / this.coordinateTransform.sy

    this.context.save()
  }

  initWithScale(context: CanvasRenderingContext2D, sx: number, sy: number) {
    this.isInUse = true
    this.context = context
    this._zoomValue = zoomFromScale(Math.min(sx, sy))
    this.coordinateTransform.sx = sx
    this.dpiX = 25.4 * sx
    this.coordinateTransform.sy = sy
    this.dpiY = 25.4 * sy
    this.pattenFillTransformScaleX = 1 / this.coordinateTransform.sx
    this.pattenFillTransformScaleY = 1 / this.coordinateTransform.sy
    this.context.save()
  }
  reset() {
    this.transform.reset()
    this.calcFinalTransform(false)

    if (!this.isCxtTransformReset) {
      this.context.setTransform(
        this.coordinateTransform.sx,
        0,
        0,
        this.coordinateTransform.sy,
        0,
        0
      )
    }
  }
  // pen methods
  setPenColor(r: number, g: number, b: number, a: number) {
    this.penStyle.setColor(r, g, b, a)

    this.context.strokeStyle = styleStringFromRGBA(this.penStyle.color)
  }

  setPenWidth(w: number) {
    this.penStyle.setWidth(w / 1000)

    if (!this.isCxtTransformReset) {
      if (0 !== this.penStyle.lineWidth) {
        this.context.lineWidth = this.penStyle.lineWidth
      } else {
        const x1 = this.finalTransform.XFromPoint(0, 0)
        const y1 = this.finalTransform.YFromPoint(0, 0)
        const x2 = this.finalTransform.XFromPoint(1, 1)
        const y2 = this.finalTransform.YFromPoint(1, 1)

        const factor = Math.sqrt(
          ((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) / 2
        )
        this.context.lineWidth = 1 / factor
      }
    } else {
      if (0 !== this.penStyle.lineWidth) {
        const m = this.finalTransform
        const x = m.sx + m.shx
        const y = m.sy + m.shy

        const factor = Math.sqrt((x * x + y * y) / 2)
        this.context.lineWidth = this.penStyle.lineWidth * factor
      } else {
        this.context.lineWidth = 1
      }
    }
  }

  setPenDash(params: Nullable<number[]>) {
    if (!this.context.setLineDash) return

    this.dashParams = params && params.length > 0 ? params.slice() : null
    this.context.setLineDash(params ? params : [])
  }

  // brush methods
  setBrushColor(r: number, g: number, b: number, a: number) {
    this.brushStyle.setColor(r, g, b, a)
    this.context.fillStyle = styleStringFromRGBA(this.brushStyle.color)
  }

  // fonts
  setEaVertTextFlag(isEaVertText: boolean) {
    EditorUtil.FontKit.isEaVertText = isEaVertText
  }

  setTextPr(textPr: FontKitTextPr) {
    EditorUtil.FontKit.setTextPr(textPr)
  }

  setFontClass(fc: FontClassValues, fontSizeFactor?: number) {
    EditorUtil.FontKit.setFontClass(fc, fontSizeFactor)
  }
  // transform
  updateTransform(
    sx: number,
    shy: number,
    shx: number,
    sy: number,
    tx: number,
    ty: number
  ) {
    const t = this.transform
    t.sx = sx
    t.shx = shx
    t.shy = shy
    t.sy = sy
    t.tx = tx
    t.ty = ty

    this.calcFinalTransform()
    if (false === this.isCxtTransformReset) {
      const ft = this.finalTransform
      this.context.setTransform(ft.sx, ft.shy, ft.shx, ft.sy, ft.tx, ft.ty)
    }
  }

  private calcFinalTransform(invert?) {
    const ft = this.finalTransform
    const t = this.transform
    ft.sx = t.sx
    ft.shx = t.shx
    ft.shy = t.shy
    ft.sy = t.sy
    ft.tx = t.tx
    ft.ty = t.ty
    MatrixUtils.multiplyMatrixes(ft, this.coordinateTransform)

    const it = this.invertFinalTransform
    it.sx = ft.sx
    it.shx = ft.shx
    it.shy = ft.shy
    it.sy = ft.sy
    it.tx = ft.tx
    it.ty = ft.ty

    if (false !== invert) {
      MatrixUtils.multiplyWithInvertMatrix(it, t)
    }
  }
  getCxtTransformReset() {
    return this.isCxtTransformReset
  }

  setCxtTransformReset(isResetTransform: boolean) {
    if (true === isResetTransform) {
      this.isCxtTransformReset = true
      this.context.setTransform(1, 0, 0, 1, 0, 0)
    } else {
      this.isCxtTransformReset = false
      this.context.setTransform(
        this.finalTransform.sx,
        this.finalTransform.shy,
        this.finalTransform.shx,
        this.finalTransform.sy,
        this.finalTransform.tx,
        this.finalTransform.ty
      )
    }
  }

  applyTransformMatrix(m: IMatrix2D, invert?: boolean) {
    const _t = this.transform
    _t.sx = m.sx
    _t.shx = m.shx
    _t.shy = m.shy
    _t.sy = m.sy
    _t.tx = m.tx
    _t.ty = m.ty
    this.calcFinalTransform(invert)

    toggleCxtTransformReset(this, false, true)
  }

  _XFromPoint(x: number, y: number) {
    return this.finalTransform.XFromPoint(x, y)
  }

  _YFromPoint(x: number, y: number) {
    return this.finalTransform.YFromPoint(x, y)
  }

  _invertTransform() {
    return MatrixUtils.invertMatrix(this.finalTransform)
  }
  // path commands
  _begin() {
    this.context.beginPath()
    if (this.pointsCollection != null) this.pointsCollection.length = 0
  }

  _end() {
    this.context.beginPath()
    if (this.pointsCollection != null) this.pointsCollection.length = 0
  }

  _close() {
    this.context.closePath()
  }

  _moveTo(x: number, y: number) {
    if (false === this.isCxtTransformReset) {
      this.context.moveTo(x, y)

      if (this.pointsCollection != null) {
        this.pointsCollection[this.pointsCollection.length] = { x: x, y: y }
      }
    } else {
      const _x = this.finalTransform.XFromPoint(x, y) >> 0
      const _y = this.finalTransform.YFromPoint(x, y) >> 0
      this.context.moveTo(_x + 0.5, _y + 0.5)
    }
  }

  _lineTo(x: number, y: number) {
    if (false === this.isCxtTransformReset) {
      this.context.lineTo(x, y)

      if (this.pointsCollection != null) {
        this.pointsCollection[this.pointsCollection.length] = { x: x, y: y }
      }
    } else {
      const _x = this.finalTransform.XFromPoint(x, y) >> 0
      const _y = this.finalTransform.YFromPoint(x, y) >> 0
      this.context.lineTo(_x + 0.5, _y + 0.5)
    }
  }

  _bezierCurveTo(
    x1: number,
    y1: number,
    x2: number,
    y2: number,
    x3: number,
    y3: number
  ) {
    if (false === this.isCxtTransformReset) {
      this.context.bezierCurveTo(x1, y1, x2, y2, x3, y3)

      if (this.pointsCollection != null) {
        this.pointsCollection[this.pointsCollection.length] = { x: x1, y: y1 }
        this.pointsCollection[this.pointsCollection.length] = { x: x2, y: y2 }
        this.pointsCollection[this.pointsCollection.length] = { x: x3, y: y3 }
      }
    } else {
      const _x1 = this.finalTransform.XFromPoint(x1, y1) >> 0
      const _y1 = this.finalTransform.YFromPoint(x1, y1) >> 0

      const _x2 = this.finalTransform.XFromPoint(x2, y2) >> 0
      const _y2 = this.finalTransform.YFromPoint(x2, y2) >> 0

      const _x3 = this.finalTransform.XFromPoint(x3, y3) >> 0
      const _y3 = this.finalTransform.YFromPoint(x3, y3) >> 0
      this.context.bezierCurveTo(
        _x1 + 0.5,
        _y1 + 0.5,
        _x2 + 0.5,
        _y2 + 0.5,
        _x3 + 0.5,
        _y3 + 0.5
      )
    }
  }

  _quadraticCurveTo(x1: number, y1: number, x2: number, y2: number) {
    if (false === this.isCxtTransformReset) {
      this.context.quadraticCurveTo(x1, y1, x2, y2)

      if (this.pointsCollection != null) {
        this.pointsCollection[this.pointsCollection.length] = { x: x1, y: y1 }
        this.pointsCollection[this.pointsCollection.length] = { x: x2, y: y2 }
      }
    } else {
      const _x1 = this.finalTransform.XFromPoint(x1, y1) >> 0
      const _y1 = this.finalTransform.YFromPoint(x1, y1) >> 0

      const _x2 = this.finalTransform.XFromPoint(x2, y2) >> 0
      const _y2 = this.finalTransform.YFromPoint(x2, y2) >> 0

      this.context.quadraticCurveTo(_x1 + 0.5, _y1 + 0.5, _x2 + 0.5, _y2 + 0.5)
    }
  }

  // canvas api
  drawStroke(p?: Path2D) {
    if (p != null) {
      this.context.stroke(p)
    } else this.context.stroke()
  }

  drawFill(p?: Path2D) {
    if (p != null) {
      this.context.fill(p)
    } else this.context.fill()
  }
  save() {
    this.context.save()
  }

  restore() {
    this.context.restore()
  }

  clip(p?: Path2D) {
    if (p) {
      this.context.clip(p)
    } else {
      this.context.clip()
    }
  }

  setLineJoin(lineJoin: CanvasLineJoin) {
    this.context.lineJoin = lineJoin
  }
  setGlobalAlpha(alpha: number) {
    this.context.globalAlpha = alpha
  }
  getGlobalAlpha() {
    return this.context.globalAlpha
  }

  setFillStyle(fillStyle) {
    this.context.fillStyle = fillStyle
  }

  setLineWidth(w: number) {
    let _w = w
    if (this.isCxtTransformReset) {
      _w = 1
    } else {
      if (MatrixUtils.isOnlyTranslate(this.transform)) {
        _w = 1
      } else {
        _w = 1 / this.coordinateTransform.sx
      }
    }
    this.context.lineWidth = _w
  }

  setCtxFont(font: string) {
    this.context.font = font
  }

  setCtxTextAlign(align: CanvasTextAlign) {
    this.context.textAlign = align
  }

  setCtxTextBaseline(baseline: CanvasTextBaseline) {
    this.context.textBaseline = baseline
  }

  fillCtxText(text: string, x: number, y: number, maxWidth?: number) {
    let _x, _y, isReset
    if (this.isCxtTransformReset) {
      _x = this._XFromPoint(x, y) >> 0
      _y = this._YFromPoint(x, y) >> 0
      this.context.fillText(text, x, y, maxWidth)
    } else {
      if (MatrixUtils.isOnlyTranslate(this.transform)) {
        isReset = true
        this.setCxtTransformReset(true)
        _x = this._XFromPoint(x, y) >> 0
        _y = this._YFromPoint(x, y) >> 0
      }
      this.context.fillText(text, _x, _y, maxWidth)
      if (isReset === true) {
        this.setCxtTransformReset(false)
      }
    }
  }

  measureText(text: string) {
    return this.context.measureText(text)
  }
  createLinearGradient(x0: number, y0: number, x1: number, y1: number) {
    return this.context.createLinearGradient(x0, y0, x1, y1)
  }

  createRadialGradient(
    x0: number,
    y0: number,
    r0: number,
    x1: number,
    y1: number,
    r1: number
  ) {
    return this.context.createRadialGradient(x0, y0, r0, x1, y1, r1)
  }

  translateCtx(x: number, y: number) {
    if (false === this.isCxtTransformReset) {
      this.context.translate(x, y)
    } else {
      const _x = this.finalTransform.XFromPoint(x, y) >> 0
      const _y = this.finalTransform.YFromPoint(x, y) >> 0
      this.context.translate(_x, _y)
    }
  }

  scaleCtx(x: number, y: number) {
    if (false === this.isCxtTransformReset) {
      this.context.scale(x, y)
    } else {
      const _x = this.finalTransform.XFromPoint(x, y) >> 0
      const _y = this.finalTransform.YFromPoint(x, y) >> 0
      this.context.scale(_x, _y)
    }
  }
  // images
  _drawImageElement(
    img: HTMLImageElement | ImageCacheData,
    x: number,
    y: number,
    w: number,
    h: number,
    alpha?: number,
    srcRect?: RelativeRect,
    fillRect?: RelativeRect
  ) {
    const isA = null != alpha && 255 !== alpha
    let orgGlobalAlpha = 0
    if (isA) {
      orgGlobalAlpha = this.context.globalAlpha
      this.context.globalAlpha = alpha! / 255
    }
    const isEmptyFillRect = !isSrcRect(fillRect)
    const isEmptySrcRect = !isSrcRect(srcRect)

    if (false === this.isCxtTransformReset) {
      if (isEmptyFillRect && isEmptySrcRect) {
        // here you need to check if you can draw accurately. those. maybe the picture is exactly what you need.
        if (!MatrixUtils.isOnlyTranslate(this.transform)) {
          this.context.drawImage(img, x, y, w, h)
        } else {
          const xx = this.finalTransform.XFromPoint(x, y)
          const yy = this.finalTransform.YFromPoint(x, y)
          const rr = this.finalTransform.XFromPoint(x + w, y + h)
          const bb = this.finalTransform.YFromPoint(x + w, y + h)
          const ww = rr - xx
          const hh = bb - yy

          if (Math.abs(img.width - ww) < 2 && Math.abs(img.height - hh) < 2) {
            // draw accurately
            this.context.setTransform(1, 0, 0, 1, 0, 0)
            this.context.drawImage(img, xx >> 0, yy >> 0)

            const _ft = this.finalTransform
            this.context.setTransform(
              _ft.sx,
              _ft.shy,
              _ft.shx,
              _ft.sy,
              _ft.tx,
              _ft.ty
            )
          } else {
            this.context.drawImage(img, x, y, w, h)
          }
        }
      } else if (isEmptySrcRect && !isEmptyFillRect) {
        const _w = img.width
        const _h = img.height
        if (_w > 0 && _h > 0) {
          const fillCoords = adjustCoordsByOffsetRect(
            fillRect!,
            x,
            y,
            w,
            h,
            0,
            0,
            _w,
            _h
          )

          if (fillCoords) {
            this.context.drawImage(
              img,
              fillCoords.tx,
              fillCoords.ty,
              fillCoords.tw,
              fillCoords.th,
              fillCoords.sx,
              fillCoords.sy,
              fillCoords.sw,
              fillCoords.sh
            )
          }
        } else {
          this.context.drawImage(img, x, y, w, h)
        }
      } else {
        const _w = img.width
        const _h = img.height
        if (_w > 0 && _h > 0) {
          let tx = x
          let ty = y
          let tw = w
          let th = h

          let sx = 0
          let sy = 0
          let sw = _w
          let sh = _h

          if (!isEmptyFillRect) {
            const fillCoords = adjustCoordsByOffsetRect(
              fillRect!,
              tx,
              ty,
              tw,
              th,
              sx,
              sy,
              sw,
              sh
            )
            if (fillCoords) {
              tx = fillCoords.sx
              ty = fillCoords.sy
              tw = fillCoords.sw
              th = fillCoords.sh
              sx = fillCoords.tx
              sy = fillCoords.ty
              sw = fillCoords.tw
              sh = fillCoords.th
            }
          }

          const coords = adjustCoordsByOffsetRect(
            srcRect!,
            sx,
            sy,
            sw,
            sh,
            tx,
            ty,
            tw,
            th
          )

          if (coords) {
            this.context.drawImage(
              img,
              coords.sx,
              coords.sy,
              coords.sw,
              coords.sh,
              coords.tx,
              coords.ty,
              coords.tw,
              coords.th
            )
          }
        } else {
          this.context.drawImage(img, x, y, w, h)
        }
      }
    } else {
      const _x1 = this.finalTransform.XFromPoint(x, y) >> 0
      const _y1 = this.finalTransform.YFromPoint(x, y) >> 0
      const _x2 = this.finalTransform.XFromPoint(x + w, y + h) >> 0
      const _y2 = this.finalTransform.YFromPoint(x + w, y + h) >> 0

      x = _x1
      y = _y1
      w = _x2 - _x1
      h = _y2 - _y1

      if (isEmptySrcRect) {
        if (!MatrixUtils.isOnlyTranslate(this.transform)) {
          this.context.drawImage(img, _x1, _y1, w, h)
        } else {
          if (Math.abs(img.width - w) < 2 && Math.abs(img.height - h) < 2) {
            this.context.drawImage(img, x, y)
          } else {
            this.context.drawImage(img, _x1, _y1, w, h)
          }
        }
      } else {
        const _w = img.width
        const _h = img.height
        if (_w > 0 && _h > 0) {
          const __w = w
          const __h = h
          const _divW =
            Math.max(0, -srcRect!.l!) + Math.max(0, srcRect!.r! - 100) + 100
          const _divH =
            Math.max(0, -srcRect!.t!) + Math.max(0, srcRect!.b! - 100) + 100

          let _sx = 0
          if (srcRect!.l! > 0 && srcRect!.l! < 100) {
            _sx = Math.min(((_w * srcRect!.l!) / 100) >> 0, _w - 1)
          } else if (srcRect!.l! < 0) {
            const _off = (-srcRect!.l! / _divW) * __w
            x += _off
            w -= _off
          }
          let _sy = 0
          if (srcRect!.t! > 0 && srcRect!.t! < 100) {
            _sy = Math.min(((_h * srcRect!.t!) / 100) >> 0, _h - 1)
          } else if (srcRect!.t! < 0) {
            const _off = (-srcRect!.t! / _divH) * __h
            y += _off
            h -= _off
          }
          let _sr = _w
          if (srcRect!.r! > 0 && srcRect!.r! < 100) {
            _sr = Math.max(
              Math.min(((_w * srcRect!.r!) / 100) >> 0, _w - 1),
              _sx
            )
          } else if (srcRect!.r! > 100) {
            const _off = ((srcRect!.r! - 100) / _divW) * __w
            w -= _off
          }
          let _sb = _h
          if (srcRect!.b! > 0 && srcRect!.b! < 100) {
            _sb = Math.max(
              Math.min(((_h * srcRect!.b!) / 100) >> 0, _h - 1),
              _sy
            )
          } else if (srcRect!.b! > 100) {
            const _off = ((srcRect!.b! - 100) / _divH) * __h
            h -= _off
          }

          if (_sr - _sx > 0 && _sb - _sy > 0 && w > 0 && h > 0) {
            this.context.drawImage(
              img,
              _sx,
              _sy,
              _sr - _sx,
              _sb - _sy,
              x,
              y,
              w,
              h
            )
          }
        } else {
          this.context.drawImage(img, x, y, w, h)
        }
      }
    }

    if (isA) {
      this.context.globalAlpha = orgGlobalAlpha
    }
  }

  drawImageFromSrc(
    imgSrc: string,
    x: number,
    y: number,
    w: number,
    h: number,
    alpha?: number,
    srcRect?: RelativeRect,
    fillRect?: RelativeRect
  ) {
    if (checkErrorImage(imgSrc)) return
    if (ImportErrorImageUrls.indexOf(imgSrc) >= 0) {
      this.setFillStyle(BGCOLOR_DRAW_IMAGE)
      this.fillRect(x, y, w, h)
      return
    }

    const img = hookManager.get(Hooks.Attrs.GetLoadableImageByImageSrc, {
      src: imgSrc,
    })

    const isImageLoading =
      !img ||
      img?.Status === StatusOfLoadableImage.Loading ||
      img?.Status === StatusOfLoadableImage.Init

    if (isImageLoading) {
      // 图片 Loading
      drawLoadingImage(this, x, y, w, h)
    } else if (img != null && img.Image != null) {
      this._drawImageElement(img.Image, x, y, w, h, alpha, srcRect, fillRect)
    } else {
      drawLoadFailedImage(this, x, y, w, h)
    }

    if (this.imageWidgetType != null) {
      drawImageTypeDecoration(this, imgSrc, x, y, w, h)
    }
  }

  drawTilingFromSrc(
    fill: BlipFill,
    x: number,
    y: number,
    w: number,
    h: number,
    transparent?: Nullable<number>,
    p?: Path2D
  ) {
    const imgSrc = fill.imageSrcId
    if (checkErrorImage(imgSrc)) return
    if (ImportErrorImageUrls.indexOf(imgSrc) >= 0) {
      this.setFillStyle(BGCOLOR_DRAW_IMAGE)
      this.fillRect(x, y, w, h)
      return
    }
    // handle tiling
    const img = hookManager.get(Hooks.Attrs.GetLoadableImageByImageSrc, {
      src: imgSrc,
    })

    const isImageLoading =
      !img ||
      img?.Status === StatusOfLoadableImage.Loading ||
      img?.Status === StatusOfLoadableImage.Init
    if (isImageLoading) {
      // 图片 Loading
      drawLoadingImage(this, x, y, w, h)
    } else if (img == null || img.Image == null) {
      drawLoadFailedImage(this, x, y, w, h)
    } else {
      const patt = this._createCanvasPatternFromImg(img, 'repeat', fill)

      this.save()

      this.translateCtx(x, y)

      const zoomValue = this.ZoomValue

      let factorX = zoomValue / 100
      let factorY = zoomValue / 100

      // 计算tile的patten时需要还原对高分辨率的处理
      factorX *= BrowserInfo.PixelRatio
      factorY *= BrowserInfo.PixelRatio
      this.scaleCtx(
        factorX * this.pattenFillTransformScaleX,
        factorY * this.pattenFillTransformScaleY
      )

      if (transparent != null) {
        const oldAlpha = this.context.globalAlpha
        this.context.globalAlpha = transparent / 255
        this.setFillStyle(patt!)
        this.drawFill(p)
        this.context.globalAlpha = oldAlpha
      } else {
        this.setFillStyle(patt!)
        this.drawFill(p)
      }

      this.restore()
    }
  }

  createPattern(image: CanvasImageSource, repetition: string | null) {
    return this.context.createPattern(image, repetition)
  }

  private _createCanvasPatternFromImg(
    loadableImg: LoadableImage,
    repetition: string | null,
    fill: BlipFill
  ) {
    const ctx = this.context
    const tile = fill.tile!
    const srcRect = isSrcRect(fill.srcRect)
      ? new RelativeRect(
          fill.srcRect!.l,
          fill.srcRect!.t,
          fill.srcRect!.r,
          fill.srcRect!.b
        )
      : new RelativeRect()
    const img = loadableImg.Image!
    const compressedScale = loadableImg.scale ?? 1
    if (srcRect.isInitial() && tile.isInitail() && loadableImg.isSVG !== true) {
      return ctx.createPattern(img, repetition)
    }

    let tx = tile.tx * Factor_mm_to_pix
    tx = tx >= 0 ? 0 : tx
    let ty = tile.ty * Factor_mm_to_pix
    ty = ty >= 0 ? 0 : ty
    srcRect.r *= tile.sx / 100
    srcRect.l *= tile.sx / 100
    srcRect.t *= tile.sy / 100
    srcRect.b *= tile.sy / 100

    const sx = (srcRect.r - srcRect.l) / 100
    const sy = (srcRect.b - srcRect.t) / 100
    const _w = img.width / compressedScale
    const _h = img.height / compressedScale

    const scaledW = _w * sx + tx
    const scaledH = _h * sy + ty

    const imgCanvas = document.createElement('canvas')
    // width and height should be at least 1 to avoid error
    imgCanvas.width = scaledW < 1 ? 1 : scaledW
    imgCanvas.height = scaledH < 1 ? 1 : scaledH

    // draw image patten to canvas
    const imgCtx = imgCanvas.getContext('2d')!

    imgCtx.drawImage(
      img,
      (-tx / sx) * compressedScale,
      (-ty / sy) * compressedScale,
      (scaledW / sx) * compressedScale,
      (scaledH / sy) * compressedScale,
      0,
      0,
      scaledW,
      scaledH
    )

    return ctx.createPattern(imgCanvas, repetition)
  }
  // text
  fillString(x: number, y: number, text: string, ignoreZoom = false) {
    if (this.isTextFillSet === false && this.isTextStrokeSet === false) {
      return
    }
    const _x = this.invertFinalTransform.XFromPoint(x, y)
    const _y = this.invertFinalTransform.YFromPoint(x, y)

    this.context.setTransform(1, 0, 0, 1, 0, 0)

    EditorUtil.FontKit.setTransform(this.transform)

    EditorUtil.FontKit.drawString(
      text,
      this.context,
      _x,
      _y,
      {
        isStrokeText: this.isTextStrokeSet,
        isFillText: this.isTextFillSet,
      },
      ignoreZoom !== true ? this._zoomValue : undefined
    )
    EditorUtil.FontKit.resetTransform()

    if (false === this.isCxtTransformReset) {
      this.context.setTransform(
        this.finalTransform.sx,
        this.finalTransform.shy,
        this.finalTransform.shx,
        this.finalTransform.sy,
        this.finalTransform.tx,
        this.finalTransform.ty
      )
    }
  }

  // smart methods for horizontal / vertical lines
  drawHorizontalLine(
    align: 0 | 1 | 2,
    y: number,
    x: number,
    r: number,
    widthOfPen: number
  ) {
    const checkedTransform = MatrixUtils.isOnlyTranslate(this.transform)
    if (!this.isCxtTransformReset || !checkedTransform) {
      if (checkedTransform) {
        toggleCxtTransformReset(this, true)
        this.drawHorizontalLine(align, y, x, r, widthOfPen)
        toggleCxtTransformReset(this, false)
        return
      }
      this.context.setTransform(1, 0, 0, 1, 0, 0)
      this.setPenWidth(widthOfPen * 1000)
      this._begin()
      this._moveTo(x, y)
      this._lineTo(r, y)
      this.drawStroke()
      return
    }

    let penWidth = ((this.dpiX * widthOfPen) / Factor_in_to_mm + 0.5) >> 0
    if (0 === penWidth) penWidth = 1

    const _x = (this.finalTransform.XFromPoint(x, y) >> 0) + 0.5 - 0.5
    const _r = (this.finalTransform.XFromPoint(r, y) >> 0) + 0.5 + 0.5

    const ctx = this.context

    ctx.setTransform(1, 0, 0, 1, 0, 0)

    ctx.lineWidth = penWidth

    switch (align) {
      case 0: {
        // top
        const _top = (this.finalTransform.YFromPoint(x, y) >> 0) + 0.5

        ctx.beginPath()
        ctx.moveTo(_x, _top + penWidth / 2 - 0.5)
        ctx.lineTo(_r, _top + penWidth / 2 - 0.5)
        ctx.stroke()

        break
      }
      case 1: {
        // center
        const _center = (this.finalTransform.YFromPoint(x, y) >> 0) + 0.5

        ctx.beginPath()
        if (0 === penWidth % 2) {
          ctx.moveTo(_x, _center - 0.5)
          ctx.lineTo(_r, _center - 0.5)
        } else {
          ctx.moveTo(_x, _center)
          ctx.lineTo(_r, _center)
        }
        ctx.stroke()

        break
      }
      case 2: {
        // bottom
        const _bottom = (this.finalTransform.YFromPoint(x, y) >> 0) + 0.5

        ctx.beginPath()
        ctx.moveTo(_x, _bottom - penWidth / 2 + 0.5)
        ctx.lineTo(_r, _bottom - penWidth / 2 + 0.5)
        ctx.stroke()

        break
      }
    }
  }

  drawVerticalLine(
    align: 0 | 1 | 2,
    x: number,
    y: number,
    b: number,
    widthOfPen: number
  ) {
    const checkedTransform = MatrixUtils.isOnlyTranslate(this.transform)
    if (!this.isCxtTransformReset || !checkedTransform) {
      if (checkedTransform) {
        toggleCxtTransformReset(this, true)
        this.drawVerticalLine(align, x, y, b, widthOfPen)
        toggleCxtTransformReset(this, false)
        return
      }

      this.setPenWidth(widthOfPen * 1000)
      this._begin()
      this._moveTo(x, y)
      this._lineTo(x, b)
      this.drawStroke()
      return
    }

    let penWidth = ((this.dpiX * widthOfPen) / Factor_in_to_mm + 0.5) >> 0
    if (0 === penWidth) penWidth = 1

    const _y = (this.finalTransform.YFromPoint(x, y) >> 0) + 0.5 - 0.5
    const _b = (this.finalTransform.YFromPoint(x, b) >> 0) + 0.5 + 0.5

    const ctx = this.context
    ctx.lineWidth = penWidth

    switch (align) {
      case 0: {
        // left
        const _left = (this.finalTransform.XFromPoint(x, y) >> 0) + 0.5

        ctx.beginPath()
        ctx.moveTo(_left + penWidth / 2 - 0.5, _y)
        ctx.lineTo(_left + penWidth / 2 - 0.5, _b)
        ctx.stroke()

        break
      }
      case 1: {
        // center
        const _center = (this.finalTransform.XFromPoint(x, y) >> 0) + 0.5

        ctx.beginPath()
        if (0 === penWidth % 2) {
          ctx.moveTo(_center - 0.5, _y)
          ctx.lineTo(_center - 0.5, _b)
        } else {
          ctx.moveTo(_center, _y)
          ctx.lineTo(_center, _b)
        }
        ctx.stroke()

        break
      }
      case 2: {
        // right
        const _right = (this.finalTransform.XFromPoint(x, y) >> 0) + 0.5

        ctx.beginPath()
        ctx.moveTo(_right - penWidth / 2 + 0.5, _y)
        ctx.lineTo(_right - penWidth / 2 + 0.5, _b)
        ctx.stroke()

        break
      }
    }
  }

  drawHorizontalLineByExt(
    align: 0 | 1 | 2,
    y: number,
    x: number,
    r: number,
    widthOfPen: number,
    leftMaxWidth: number,
    rightMaxWidth: number
  ) {
    const checkedTransform = MatrixUtils.isOnlyTranslate(this.transform)
    if (!this.isCxtTransformReset || !checkedTransform) {
      if (checkedTransform) {
        toggleCxtTransformReset(this, true)
        this.drawHorizontalLineByExt(
          align,
          y,
          x,
          r,
          widthOfPen,
          leftMaxWidth,
          rightMaxWidth
        )
        toggleCxtTransformReset(this, false)
        return
      }

      this.setPenWidth(widthOfPen * 1000)
      this._begin()
      this._moveTo(x, y)
      this._lineTo(r, y)
      this.drawStroke()
      return
    }

    let _x = (this.finalTransform.XFromPoint(x, y) >> 0) + 0.5
    let _r = (this.finalTransform.XFromPoint(r, y) >> 0) + 0.5

    if (leftMaxWidth !== 0) {
      const _center = _x
      const pw = Math.max(
        ((this.dpiX * Math.abs(leftMaxWidth) * 2) / Factor_in_to_mm + 0.5) >> 0,
        1
      )
      if (leftMaxWidth < 0) {
        if (pw % 2 === 0) {
          _x = _center - pw / 2
        } else {
          _x = _center - ((pw / 2) >> 0)
        }
      } else {
        if (pw % 2 === 0) {
          _x = _center + (pw / 2 - 1.0)
        } else {
          _x = _center + ((pw / 2) >> 0)
        }
      }
    }
    if (rightMaxWidth !== 0) {
      const _center = _r
      const pw = Math.max(
        ((this.dpiX * Math.abs(rightMaxWidth) * 2) / Factor_in_to_mm + 0.5) >>
          0,
        1
      )
      if (rightMaxWidth < 0) {
        if (pw % 2 === 0) {
          _r = _center - pw / 2
        } else {
          _r = _center - ((pw / 2) >> 0)
        }
      } else {
        if (pw % 2 === 0) {
          _r = _center + pw / 2 - 1.0
        } else {
          _r = _center + ((pw / 2) >> 0)
        }
      }
    }

    const ctx = this.context
    const pw = Math.max(
      ((this.dpiX * widthOfPen) / Factor_in_to_mm + 0.5) >> 0,
      1
    )
    ctx.lineWidth = pw

    _x -= 0.5
    _r += 0.5

    switch (align) {
      case 0: {
        // top
        const _top = (this.finalTransform.YFromPoint(x, y) >> 0) + 0.5

        ctx.beginPath()
        ctx.moveTo(_x, _top + pw / 2 - 0.5)
        ctx.lineTo(_r, _top + pw / 2 - 0.5)
        ctx.stroke()

        break
      }
      case 1: {
        // center
        const _center = (this.finalTransform.YFromPoint(x, y) >> 0) + 0.5

        ctx.beginPath()
        if (0 === pw % 2) {
          ctx.moveTo(_x, _center - 0.5)
          ctx.lineTo(_r, _center - 0.5)
        } else {
          ctx.moveTo(_x, _center)
          ctx.lineTo(_r, _center)
        }
        ctx.stroke()

        break
      }
      case 2: {
        // bottom
        const _bottom = (this.finalTransform.YFromPoint(x, y) >> 0) + 0.5

        ctx.beginPath()
        ctx.moveTo(_x, _bottom - pw / 2 + 0.5)
        ctx.lineTo(_r, _bottom - pw / 2 + 0.5)
        ctx.stroke()

        break
      }
    }
  }

  // draw utils
  drawAclinicRectWithDashBorder(
    x: number,
    y: number,
    r: number,
    b: number,
    dashDotW: number,
    dashDistW: number
  ) {
    if (this.isCxtTransformReset === true) {
      const _x = this._XFromPoint(x, y)
      const _y = this._YFromPoint(x, y)
      const _r = this._XFromPoint(r, b)
      const _b = this._YFromPoint(r, b)

      const __x = Math.min(_x, _r)
      const __y = Math.min(_y, _b)
      const __r = Math.max(_x, _r)
      const __b = Math.max(_y, _b)
      drawAclinicRectWithDashBorder(
        this.context,
        __x >> 0,
        __y >> 0,
        __r >> 0,
        __b >> 0,
        dashDotW,
        dashDistW
      )
    } else {
      const _x = Math.min(x, r) >> 0
      const _y = Math.min(y, b) >> 0
      const _r = Math.max(x, r) >> 0
      const _b = Math.max(y, b) >> 0
      drawAclinicRectWithDashBorder(
        this.context,
        _x,
        _y,
        _r,
        _b,
        dashDotW,
        dashDistW
      )
    }
  }

  drawRectWithDashBorder(
    x: number,
    y: number,
    r: number,
    b: number,
    dashDotW: number,
    dashDistW: number
  ) {
    let x1 = x
    let y1 = y

    let x2 = r
    let y2 = y

    let x3 = x
    let y3 = b

    let x4 = r
    let y4 = b

    if (this.isCxtTransformReset === true) {
      x1 = this._XFromPoint(x, y) >> 0
      y1 = this._YFromPoint(x, y) >> 0

      x2 = this._XFromPoint(r, y) >> 0
      y2 = this._YFromPoint(r, y) >> 0

      x3 = this._XFromPoint(x, b) >> 0
      y3 = this._YFromPoint(x, b) >> 0

      x4 = this._XFromPoint(r, b) >> 0
      y4 = this._YFromPoint(r, b) >> 0
    }
    drawRectWithDashBorder(
      this.context,
      x1,
      y1,
      x2,
      y2,
      x3,
      y3,
      x4,
      y4,
      dashDotW,
      dashDistW
    )
  }

  rect(x: number, y: number, w: number, h: number) {
    const ctx = this.context
    ctx.beginPath()

    if (this.isCxtTransformReset) {
      if (MatrixUtils.isOnlyTranslate(this.finalTransform)) {
        const _x = (this.finalTransform.XFromPoint(x, y) + 0.5) >> 0
        const _y = (this.finalTransform.YFromPoint(x, y) + 0.5) >> 0
        const _r = (this.finalTransform.XFromPoint(x + w, y) + 0.5) >> 0
        const _b = (this.finalTransform.YFromPoint(x, y + h) + 0.5) >> 0

        ctx.rect(_x, _y, _r - _x, _b - _y)
      } else {
        const x1 = this.finalTransform.XFromPoint(x, y)
        const y1 = this.finalTransform.YFromPoint(x, y)
        const x2 = this.finalTransform.XFromPoint(x + w, y)
        const y2 = this.finalTransform.YFromPoint(x + w, y)
        const x3 = this.finalTransform.XFromPoint(x + w, y + h)
        const y3 = this.finalTransform.YFromPoint(x + w, y + h)
        const x4 = this.finalTransform.XFromPoint(x, y + h)
        const y4 = this.finalTransform.YFromPoint(x, y + h)

        ctx.setTransform(1, 0, 0, 1, 0, 0)
        ctx.moveTo(x1, y1)
        ctx.lineTo(x2, y2)
        ctx.lineTo(x3, y3)
        ctx.lineTo(x4, y4)
        ctx.closePath()
      }
    } else {
      ctx.rect(x, y, w, h)
    }
  }

  fillRect(x: number, y: number, w: number, h: number) {
    const ctx = this.context

    if (this.isCxtTransformReset) {
      const _x = (this.finalTransform.XFromPoint(x, y) >> 0) + 0.5
      const _y = (this.finalTransform.YFromPoint(x, y) >> 0) + 0.5
      const _r = (this.finalTransform.XFromPoint(x + w, y) >> 0) + 0.5
      const _b = (this.finalTransform.YFromPoint(x, y + h) >> 0) + 0.5

      ctx.fillRect(_x - 0.5, _y - 0.5, _r - _x + 1, _b - _y + 1)
    } else {
      ctx.fillRect(x, y, w, h)
    }
  }

  addClipRect(x: number, y: number, w: number, h: number) {
    this.drawerState.addClipRect(x, y, w, h)
  }

  removeClip() {
    this.context.restore()
    this.context.save()
  }

  saveState() {
    this.drawerState.pushState()
  }

  restoreState() {
    this.drawerState.popState()
  }

  startClipPath() {}

  endClipPath() {
    this.context.clip()
  }

  rectWithPen(
    x: number,
    y: number,
    w: number,
    h: number,
    penWidth: number,
    p?: Path2D
  ) {
    if (!MatrixUtils.isOnlyTranslate(this.transform)) {
      const r = x + w
      const b = y + h
      const dx1 = this.finalTransform.XFromPoint(x, y)
      const dy1 = this.finalTransform.YFromPoint(x, y)

      const dx2 = this.finalTransform.XFromPoint(r, y)
      const dy2 = this.finalTransform.YFromPoint(r, y)

      const dx3 = this.finalTransform.XFromPoint(x, b)
      const dy3 = this.finalTransform.YFromPoint(x, b)

      const dx4 = this.finalTransform.XFromPoint(r, b)
      const dy4 = this.finalTransform.YFromPoint(r, b)

      const _eps = 0.001
      let isClever = false
      let _type = 1
      if (
        Math.abs(dx1 - dx3) < _eps &&
        Math.abs(dx2 - dx4) < _eps &&
        Math.abs(dy1 - dy2) < _eps &&
        Math.abs(dy3 - dy4) < _eps
      ) {
        isClever = true
        _type = 1
      }
      if (
        !isClever &&
        Math.abs(dx1 - dx2) < _eps &&
        Math.abs(dx3 - dx4) < _eps &&
        Math.abs(dy1 - dy3) < _eps &&
        Math.abs(dy2 - dy4) < _eps
      ) {
        _type = 2
        isClever = true
      }

      if (!isClever) {
        this.context.lineWidth = penWidth
        this.drawStroke(p)
        return
      }

      const _xI = _type === 1 ? Math.min(dx1, dx2) : Math.min(dx1, dx3)
      const _rI = _type === 1 ? Math.max(dx1, dx2) : Math.max(dx1, dx3)
      const _yI = _type === 1 ? Math.min(dy1, dy3) : Math.min(dy1, dy2)
      const _bI = _type === 1 ? Math.max(dy1, dy3) : Math.max(dy1, dy2)

      let isSmartForce = false
      if (!this.isCxtTransformReset) {
        toggleCxtTransformReset(this, true)
        isSmartForce = true

        if (this.dashParams) {
          for (let index = 0; index < this.dashParams.length; index++) {
            this.dashParams[index] =
              (this.coordinateTransform.sx * this.dashParams[index] + 0.5) >> 0
          }

          this.context.setLineDash(this.dashParams)
          this.dashParams = null
        }
      }

      let _penWidth = (penWidth * this.coordinateTransform.sx + 0.5) >> 0
      if (0 >= _penWidth) _penWidth = 1

      this._begin()

      if ((_penWidth & 0x01) === 0x01) {
        const _x = (_xI >> 0) + 0.5
        const _y = (_yI >> 0) + 0.5
        const _r = (_rI >> 0) + 0.5
        const _b = (_bI >> 0) + 0.5

        this.context.rect(_x, _y, _r - _x, _b - _y)
      } else {
        const _x = (_xI + 0.5) >> 0
        const _y = (_yI + 0.5) >> 0
        const _r = (_rI + 0.5) >> 0
        const _b = (_bI + 0.5) >> 0

        this.context.rect(_x, _y, _r - _x, _b - _y)
      }

      this.context.lineWidth = _penWidth
      this.drawStroke()

      if (isSmartForce) {
        toggleCxtTransformReset(this, false)
      }
      return
    }

    let isSmartForce = false
    if (!this.isCxtTransformReset) {
      toggleCxtTransformReset(this, true)
      isSmartForce = true

      if (this.dashParams) {
        for (let index = 0; index < this.dashParams.length; index++) {
          this.dashParams[index] =
            (this.coordinateTransform.sx * this.dashParams[index] + 0.5) >> 0
        }

        this.context.setLineDash(this.dashParams)
        this.dashParams = null
      }
    }

    let _penWidth = (penWidth * this.coordinateTransform.sx + 0.5) >> 0
    if (0 >= _penWidth) _penWidth = 1

    this._begin()

    if ((_penWidth & 0x01) === 0x01) {
      const _x = (this.finalTransform.XFromPoint(x, y) >> 0) + 0.5
      const _y = (this.finalTransform.YFromPoint(x, y) >> 0) + 0.5
      const _r = (this.finalTransform.XFromPoint(x + w, y + h) >> 0) + 0.5
      const _b = (this.finalTransform.YFromPoint(x + w, y + h) >> 0) + 0.5

      this.context.rect(_x, _y, _r - _x, _b - _y)
    } else {
      const _x = (this.finalTransform.XFromPoint(x, y) + 0.5) >> 0
      const _y = (this.finalTransform.YFromPoint(x, y) + 0.5) >> 0
      const _r = (this.finalTransform.XFromPoint(x + w, y + h) + 0.5) >> 0
      const _b = (this.finalTransform.YFromPoint(x + w, y + h) + 0.5) >> 0

      this.context.rect(_x, _y, _r - _x, _b - _y)
    }

    this.context.lineWidth = _penWidth
    this.drawStroke()

    if (isSmartForce) {
      toggleCxtTransformReset(this, false)
    }
  }

  drawFillWithPenStyle(p?: Path2D) {
    const tmp = this.brushStyle.color
    const p_c = this.penStyle.color
    this.setBrushColor(p_c.r, p_c.g, p_c.b, p_c.a)
    this.drawFill(p)
    this.setBrushColor(tmp.r, tmp.g, tmp.b, tmp.a)
  }

  setCoordTransform(tx: number, ty: number) {
    this.coordinateTransform.tx = tx
    this.coordinateTransform.ty = ty
  }
  getCoordTransformScale() {
    return {
      sx: this.coordinateTransform.sx,
      sy: this.coordinateTransform.sy,
    }
  }
  getTransform() {
    return this.transform
  }
}

interface ClipStateItem {
  rect: ClipRect
  isCxtTransformReset: boolean
  transform: Matrix2D
}

interface DrawerStateItem {
  transform: Matrix2D
  isCxtTransformReset: boolean
  clips: ClipStateItem[]
}

class DrawerState {
  canvasDrawer: CanvasDrawer
  private States: Array<DrawerStateItem>
  private Clips: ClipStateItem[]
  constructor(canvasDrawer: CanvasDrawer) {
    this.canvasDrawer = canvasDrawer
    this.States = []

    this.Clips = []
  }
  reset() {
    this.States.length = 0

    this.Clips.length = 0
  }

  pushState() {
    const state: DrawerStateItem = {
      transform: this.canvasDrawer.getTransform().clone(),
      isCxtTransformReset: this.canvasDrawer.isCxtTransformReset,
      clips: this.Clips,
    }
    this.States.push(state)
    this.Clips = []
  }

  popState() {
    const ind = this.States.length - 1
    if (-1 === ind) return

    const state = this.States[ind]
    if (this.Clips.length > 0) {
      this.canvasDrawer.removeClip()

      for (let i = 0; i <= ind; i++) {
        const s = this.States[i]

        const clips = s.clips
        const l = clips.length

        for (let j = 0; j < l; j++) {
          this.canvasDrawer.applyTransformMatrix(clips[j].transform)
          toggleCxtTransformReset(
            this.canvasDrawer,
            clips[j].isCxtTransformReset
          )

          const rect = clips[j].rect

          this.canvasDrawer.startClipPath()

          this.canvasDrawer._begin()
          this.canvasDrawer._moveTo(rect.x, rect.y)
          this.canvasDrawer._lineTo(rect.x + rect.w, rect.y)
          this.canvasDrawer._lineTo(rect.x + rect.w, rect.y + rect.h)
          this.canvasDrawer._lineTo(rect.x, rect.y + rect.h)
          this.canvasDrawer._lineTo(rect.x, rect.y)

          this.canvasDrawer.endClipPath()
        }
      }
    }

    this.Clips = state.clips
    this.States.splice(ind, 1)

    this.canvasDrawer.applyTransformMatrix(state.transform)
    toggleCxtTransformReset(this.canvasDrawer, state.isCxtTransformReset)
  }

  addClipRect(x: number, y: number, w: number, h: number) {
    const grClip: ClipStateItem = {
      transform: this.canvasDrawer.getTransform().clone(),
      isCxtTransformReset: this.canvasDrawer.isCxtTransformReset,
      rect: { x: x, y: y, w: w, h: h },
    }
    this.Clips.push(grClip)

    this.canvasDrawer.startClipPath()

    this.canvasDrawer._begin()
    this.canvasDrawer._moveTo(x, y)
    this.canvasDrawer._lineTo(x + w, y)
    this.canvasDrawer._lineTo(x + w, y + h)
    this.canvasDrawer._lineTo(x, y + h)
    this.canvasDrawer._lineTo(x, y)

    this.canvasDrawer.endClipPath()
  }
}

class PenStyle {
  color: ColorRGBA
  style: number
  lineCap: number
  lineJoin: number
  lineWidth: number
  constructor() {
    this.color = { r: 255, g: 255, b: 255, a: 255 }
    this.style = 0
    this.lineCap = 0
    this.lineJoin = 0

    this.lineWidth = 1
  }
  reset() {
    this.color = { r: 255, g: 255, b: 255, a: 255 }
    this.style = 0
    this.lineCap = 0
    this.lineJoin = 0

    this.lineWidth = 1
  }
  setColor(r: number, g: number, b: number, a: number) {
    this.color.r = r
    this.color.g = g
    this.color.b = b
    this.color.a = a
  }
  setWidth(w: number) {
    this.lineWidth = w
  }
}
class BrushStyle {
  color: ColorRGBA
  type: number
  constructor() {
    this.color = { r: 255, g: 255, b: 255, a: 255 }
    this.type = 0
  }
  reset() {
    this.color = { r: 255, g: 255, b: 255, a: 255 }
    this.type = 0
  }
  setColor(r: number, g: number, b: number, a: number) {
    this.color.r = r
    this.color.g = g
    this.color.b = b
    this.color.a = a
  }
}

export function styleStringFromRGBA(c: ColorRGBA) {
  return `rgba(${c.r},${c.g},${c.b},${c.a / 255})`
}

let canvasDrawerPool: CanvasDrawer[] = []

const _MAX_USABLE_NUM = 3
function freePool() {
  const resultPool: CanvasDrawer[] = []
  let amountOfUsable = 0
  for (const gd of canvasDrawerPool) {
    if (gd.isInUse !== true && amountOfUsable < _MAX_USABLE_NUM) {
      resultPool.push(gd)
      amountOfUsable += 1
    } else if (gd.isInUse) {
      resultPool.push(gd)
    }
  }
  canvasDrawerPool = resultPool
}

function getDrawerFromPool() {
  freePool()
  let gd: Nullable<CanvasDrawer>
  for (let i = 0; i < canvasDrawerPool.length; i++) {
    if (canvasDrawerPool[i].isInUse !== true) {
      gd = canvasDrawerPool[i]
      break
    }
  }

  if (gd == null) {
    gd = new CanvasDrawer()
    canvasDrawerPool.push(gd)
  }

  return gd
}

export function getCanvasDrawer(
  context: CanvasRenderingContext2D,
  widthInPx,
  heightInPx,
  widthInMM?,
  heightInMM?
) {
  const gd = getDrawerFromPool()
  gd.init(context, widthInPx, heightInPx, widthInMM, heightInMM)
  return gd
}

export function getCanvasDrawerWithScale(
  context: CanvasRenderingContext2D,
  sx: number,
  sy: number
) {
  const gd = getDrawerFromPool()

  gd.initWithScale(context, sx, sy)
  return gd
}
