import { Nullable } from '../../../liber/pervasive'
import { InstanceType } from '../instanceTypes'
import { SlideElement } from '../SlideElement/type'
import { ScaleOfPPTXSizes } from '../common/const/unit'
import { zoomFromScale } from '../utilities/zoom'
import { Matrix2D, MatrixUtils } from './Matrix'

export class Bounds {
  minX: number
  minY: number
  maxX: number
  maxY: number

  private static _tmpBounds = new Bounds()
  static containPoint(
    x: number,
    y: number,
    minX: number,
    minY: number,
    maxX?: number,
    maxY?: number
  ) {
    Bounds._tmpBounds.reset()
    Bounds._tmpBounds.update(minX, minY, maxX, maxY)
    return Bounds._tmpBounds.containPoint(x, y)
  }

  constructor() {
    this.minX = 0xffff
    this.minY = 0xffff
    this.maxX = -0xffff
    this.maxY = -0xffff
  }

  reset(minX?: number, minY?: number, maxX?: number, maxY?: number) {
    this.minX = minX ?? 0xffff
    this.minY = minY ?? 0xffff
    this.maxX = maxX ?? -0xffff
    this.maxY = maxY ?? -0xffff
  }

  update(minX: number, minY: number, maxX?: number, maxY?: number) {
    if (minX < this.minX) this.minX = minX
    if (minY < this.minY) this.minY = minY
    maxX = maxX ?? minX
    maxY = maxY ?? minY
    if (maxX > this.maxX) this.maxX = maxX
    if (maxY > this.maxY) this.maxY = maxY
  }

  containPoint(x: number, y: number): boolean {
    return this.minX <= x && this.maxX >= x && this.minY <= y && this.maxY >= y
  }
  isOverlap(bounds: WithBounds) {
    const isXOverlap = !(bounds.minX > this.maxX || bounds.maxX < this.minX)
    const isYOverlap = !(bounds.minY > this.maxY || bounds.maxY < this.minY)
    return isXOverlap && isYOverlap
  }
}

export class GraphicsBoundsChecker {
  readonly instanceType = InstanceType.GraphicsBoundsChecker

  isInPreview: boolean = false
  bounds: Bounds
  coordinateTransform: Matrix2D
  appliedTransform: Matrix2D
  finalTransform: Matrix2D
  lineWidth: Nullable<number>
  needCheckLineWidth: boolean

  dpiX!: number
  dpiY!: number
  private _zoomValue: number = 100
  get ZoomValue() {
    return this._zoomValue
  }

  constructor() {
    this.bounds = new Bounds()

    this.coordinateTransform = new Matrix2D()
    this.appliedTransform = new Matrix2D()
    this.finalTransform = new Matrix2D()

    this.lineWidth = null
    this.needCheckLineWidth = false
  }

  getCxtTransformReset() {
    return false
  }

  rectWithPen() {}

  init(
    widthInPx: number,
    heightInPx: number,
    widthInMM: number,
    heightInMM: number
  ) {
    this.dpiX = (25.4 * widthInPx) / widthInMM
    this.dpiY = (25.4 * heightInPx) / heightInMM

    const sx = this.dpiX / 25.4
    const sy = this.dpiY / 25.4
    this._zoomValue = zoomFromScale(Math.min(sx, sy))
    this.coordinateTransform.sx = sx
    this.coordinateTransform.sy = sy

    this.bounds.reset()
  }
  initWithScale(sx: number, sy: number) {
    this._zoomValue = zoomFromScale(Math.min(sx, sy))
    this.coordinateTransform.sx = sx
    this.dpiX = 25.4 * sx
    this.coordinateTransform.sy = sy
    this.dpiY = 25.4 * sy
    this.bounds.reset()
  }

  setGlobalAlpha(_enable, _alpha) {}

  // pen methods
  setPenColor(_r, _g, _b, _a) {}

  setPenWidth(_w) {}
  setPenDash(_params) {}

  // brush methods
  setBrushColor(_r, _g, _b, _a) {}

  transform(sx, shy, shx, sy, tx, ty) {
    this.appliedTransform.sx = sx
    this.appliedTransform.shx = shx
    this.appliedTransform.shy = shy
    this.appliedTransform.sy = sy
    this.appliedTransform.tx = tx
    this.appliedTransform.ty = ty

    this.calcFinalTransform()
  }

  calcFinalTransform() {
    this.finalTransform.sx = this.appliedTransform.sx
    this.finalTransform.shx = this.appliedTransform.shx
    this.finalTransform.shy = this.appliedTransform.shy
    this.finalTransform.sy = this.appliedTransform.sy
    this.finalTransform.tx = this.appliedTransform.tx
    this.finalTransform.ty = this.appliedTransform.ty
    MatrixUtils.multiplyMatrixes(this.finalTransform, this.coordinateTransform)
  }

  // path commands
  _begin() {}

  _end() {}
  _close() {}

  _moveTo(x, y) {
    const _x = this.finalTransform.XFromPoint(x, y)
    const _y = this.finalTransform.YFromPoint(x, y)

    this.bounds.update(_x, _y)
  }

  _lineTo(x, y) {
    const _x = this.finalTransform.XFromPoint(x, y)
    const _y = this.finalTransform.YFromPoint(x, y)

    this.bounds.update(_x, _y)
  }

  _bezierCurveTo(x1, y1, x2, y2, x3, y3) {
    const _x1 = this.finalTransform.XFromPoint(x1, y1)
    const _y1 = this.finalTransform.YFromPoint(x1, y1)

    const _x2 = this.finalTransform.XFromPoint(x2, y2)
    const _y2 = this.finalTransform.YFromPoint(x2, y2)

    const _x3 = this.finalTransform.XFromPoint(x3, y3)
    const _y3 = this.finalTransform.YFromPoint(x3, y3)

    this.bounds.update(_x1, _y1)
    this.bounds.update(_x2, _y2)
    this.bounds.update(_x3, _y3)
  }

  _quadraticCurveTo(x1, y1, x2, y2) {
    const _x1 = this.finalTransform.XFromPoint(x1, y1)
    const _y1 = this.finalTransform.YFromPoint(x1, y1)

    const _x2 = this.finalTransform.XFromPoint(x2, y2)
    const _y2 = this.finalTransform.YFromPoint(x2, y2)

    this.bounds.update(_x1, _y1)
    this.bounds.update(_x2, _y2)
  }

  drawStroke() {}
  drawFill() {}

  // canvas state
  save() {}

  restore() {}
  clip() {}

  reset() {
    this.appliedTransform.reset()
    this.calcFinalTransform()
  }

  applyTransformMatrix(m) {
    this.appliedTransform = m.clone()
    this.calcFinalTransform()
  }

  // images
  drawImage(_img, x, y, w, h) {
    const _x1 = this.finalTransform.XFromPoint(x, y)
    const _y1 = this.finalTransform.YFromPoint(x, y)

    const _x2 = this.finalTransform.XFromPoint(x + w, y)
    const _y2 = this.finalTransform.YFromPoint(x + w, y)

    const _x3 = this.finalTransform.XFromPoint(x + w, y + h)
    const _y3 = this.finalTransform.YFromPoint(x + w, y + h)

    const _x4 = this.finalTransform.XFromPoint(x, y + h)
    const _y4 = this.finalTransform.YFromPoint(x, y + h)

    this.bounds.update(_x1, _y1)
    this.bounds.update(_x2, _y2)
    this.bounds.update(_x3, _y3)
    this.bounds.update(_x4, _y4)
  }

  // text

  fillString(x, y) {
    const _x = this.finalTransform.XFromPoint(x, y)
    const _y = this.finalTransform.YFromPoint(x, y)
    this.bounds.update(_x, _y, _x + 1, _y + 1)
  }

  // smart methods for horizontal / vertical lines
  drawHorizontalLine(_align, y, x, r, widthOfPen?) {
    const _x1 = this.finalTransform.XFromPoint(x, y - widthOfPen)
    const _y1 = this.finalTransform.YFromPoint(x, y - widthOfPen)

    const _x2 = this.finalTransform.XFromPoint(x, y + widthOfPen)
    const _y2 = this.finalTransform.YFromPoint(x, y + widthOfPen)

    const _x3 = this.finalTransform.XFromPoint(r, y - widthOfPen)
    const _y3 = this.finalTransform.YFromPoint(r, y - widthOfPen)

    const _x4 = this.finalTransform.XFromPoint(r, y + widthOfPen)
    const _y4 = this.finalTransform.YFromPoint(r, y + widthOfPen)

    this.bounds.update(_x1, _y1)
    this.bounds.update(_x2, _y2)
    this.bounds.update(_x3, _y3)
    this.bounds.update(_x4, _y4)
  }

  drawVerticalLine(_align, x, y, b, widthOfPen) {
    const _x1 = this.finalTransform.XFromPoint(x - widthOfPen, y)
    const _y1 = this.finalTransform.YFromPoint(x - widthOfPen, y)

    const _x2 = this.finalTransform.XFromPoint(x + widthOfPen, y)
    const _y2 = this.finalTransform.YFromPoint(x + widthOfPen, y)

    const _x3 = this.finalTransform.XFromPoint(x - widthOfPen, b)
    const _y3 = this.finalTransform.YFromPoint(x - widthOfPen, b)

    const _x4 = this.finalTransform.XFromPoint(x + widthOfPen, b)
    const _y4 = this.finalTransform.YFromPoint(x + widthOfPen, b)

    this.bounds.update(_x1, _y1)
    this.bounds.update(_x2, _y2)
    this.bounds.update(_x3, _y3)
    this.bounds.update(_x4, _y4)
  }

  drawHorizontalLineByExt(
    align,
    y,
    x,
    r,
    _widthOfPen,
    leftMaxWidth,
    rightMaxWidth
  ) {
    this.drawHorizontalLine(align, y, x + leftMaxWidth, r + rightMaxWidth)
  }

  /** to right bottom */
  rect(x, y, w, h) {
    this._moveTo(x, y)
    this._lineTo(x + w, y)
    this._lineTo(x + w, y + h)
    this._lineTo(x, y + h)
  }

  /** to right top */
  rect2(x, y, w, h) {
    this._moveTo(x, y)
    this._lineTo(x + w, y)
    this._lineTo(x + w, y - h)
    this._lineTo(x, y - h)
  }

  fillRect(x, y, w, h) {
    this.rect(x, y, w, h)
  }

  addClipRect(_x, _y, _w, _h) {}

  saveState() {}
  restoreState() {}

  /** 去除 lineWidth 误差 */
  correctBoundsWithLines() {
    if (this.lineWidth != null) {
      const _correct = this.lineWidth / 2.0

      this.bounds.minX -= _correct
      this.bounds.minY -= _correct
      this.bounds.maxX += _correct
      this.bounds.maxY += _correct
    }
  }

  correctBoundsWithSx() {
    if (this.lineWidth != null) {
      const correction = (this.lineWidth * this.coordinateTransform.sx) / 2

      this.bounds.minX -= correction
      this.bounds.minY -= correction
      this.bounds.maxX += correction
      this.bounds.maxY += correction
    }
  }

  checkSpLineWidth(shape: SlideElement) {
    if (!shape) return
    const ln = shape.pen
    if (ln != null && ln.fillEffects != null && ln.fillEffects.fill != null) {
      this.lineWidth = ln.w == null ? 12700 : parseInt(ln.w as any)
      this.lineWidth /= ScaleOfPPTXSizes
    }
  }

  /** zoom */
  scale(scaleVal: number) {
    this.bounds.minX *= scaleVal
    this.bounds.minY *= scaleVal
    this.bounds.maxX *= scaleVal
    this.bounds.maxY *= scaleVal
  }
}

export interface WithBounds {
  minX: number
  minY: number
  maxX: number
  maxY: number
}

/**
 * Class represent bounds graphical object
 * @param {number} l
 * @param {number} t
 * @param {number} r
 * @param {number} b
 * @constructor
 */
export class GraphicBounds {
  l: number
  t: number
  r: number
  b: number
  x: number
  y: number
  w: number
  h: number
  constructor(l, t, r, b) {
    this.l = l
    this.t = t
    this.r = r
    this.b = b

    this.x = l
    this.y = t
    this.w = r - l
    this.h = b - t
  }

  fromBounds(bounds) {
    this.l = bounds.l
    this.t = bounds.t
    this.r = bounds.r
    this.b = bounds.b

    this.x = bounds.x
    this.y = bounds.y
    this.w = bounds.w
    this.h = bounds.h
  }

  clone() {
    return new GraphicBounds(this.l, this.t, this.r, this.b)
  }

  transform(transform: Matrix2D) {
    const xlt = transform.XFromPoint(this.l, this.t)
    const ylt = transform.YFromPoint(this.l, this.t)

    const xrt = transform.XFromPoint(this.r, this.t)
    const yrt = transform.YFromPoint(this.r, this.t)
    const xlb = transform.XFromPoint(this.l, this.b)
    const ylb = transform.YFromPoint(this.l, this.b)

    const xrb = transform.XFromPoint(this.r, this.b)
    const yrb = transform.YFromPoint(this.r, this.b)

    this.l = Math.min(xlb, xlt, xrb, xrt)
    this.t = Math.min(ylb, ylt, yrb, yrt)

    this.r = Math.max(xlb, xlt, xrb, xrt)
    this.b = Math.max(ylb, ylt, yrb, yrt)

    this.x = this.l
    this.y = this.t
    this.w = this.r - this.l
    this.h = this.b - this.t
  }

  checkByBounds(bounds) {
    if (bounds) {
      if (bounds.l < this.l) {
        this.l = bounds.l
      }
      if (bounds.t < this.t) {
        this.t = bounds.t
      }
      if (bounds.r > this.r) {
        this.r = bounds.r
      }
      if (bounds.b > this.b) {
        this.b = bounds.b
      }
    }
  }

  updateWidthAndHeight() {
    this.x = this.l
    this.y = this.t
    this.w = this.r - this.l
    this.h = this.b - this.t
  }

  reset(l, t, r, b) {
    this.l = l
    this.t = t
    this.r = r
    this.b = b

    this.x = l
    this.y = t
    this.w = r - l
    this.h = b - t
  }
}
