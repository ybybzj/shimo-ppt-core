import { Nullable } from '../../../liber/pervasive'
import { GradientStop } from '../../exports/type'
import { InstanceType } from '../instanceTypes'
import { EditLayerDrawer } from '../../ui/rendering/Drawer/EditLayerDrawer'
import { computedRGBColorFromStr } from '../color/ComputedRGBAColor'
import { GsList } from '../properties/props'
import { SpRenderingContext } from '../render/drawShape/type'
import { CanvasDrawer } from './CanvasDrawer'
import { IMatrix2D } from './Matrix'
import { ShapeDrawerContext } from './type'

export function toggleCxtTransformReset(
  g: ShapeDrawerContext,
  isResetTransform: boolean,
  force = false
) {
  if (
    g.instanceType !== InstanceType.CanvasDrawer ||
    (force !== true && g.isCxtTransformReset === isResetTransform)
  ) {
    return
  }

  g.setCxtTransformReset(isResetTransform)
}

interface GraphicTransformResetState {
  isChanged: boolean
  prevValue: boolean
}

export function setCxtTransformReset(
  g: SpRenderingContext,
  isResetTransform: boolean,
  force = false
): GraphicTransformResetState {
  if (g.instanceType !== InstanceType.CanvasDrawer) {
    return { isChanged: false, prevValue: false }
  }
  const prevValue = g.isCxtTransformReset
  let isChanged = prevValue !== isResetTransform
  if (force !== true && isChanged !== true) {
    return {
      isChanged: false,
      prevValue,
    }
  }

  if (force === true) {
    isChanged = true
  }

  toggleCxtTransformReset(g, isResetTransform, true)

  return {
    isChanged,
    prevValue,
  }
}

export function restoreCxtTransformReset(
  g: SpRenderingContext,
  state: GraphicTransformResetState
) {
  if (state.isChanged) {
    toggleCxtTransformReset(g, state.prevValue, true)
  }
}

export function getTransformFromDrawCxt(
  g: EditLayerDrawer | CanvasDrawer
): Nullable<IMatrix2D> {
  return g.instanceType === InstanceType.EditLayerDrawer
    ? g.canvasDrawer.getTransform()
    : g.getTransform()
}

export function getCanvasDrawerContext(
  g: EditLayerDrawer | CanvasDrawer
): CanvasDrawer {
  return g.instanceType === InstanceType.EditLayerDrawer ? g.canvasDrawer : g
}
export function formatGsLst(gsLst: GradientStop[]): GsList {
  return gsLst.map((stop) => {
    return { color: computedRGBColorFromStr(stop.color), pos: 1000 * stop.pos }
  })
}

export function formatLinearAngle(angle: number): number {
  let ang = angle ?? 0
  while (ang < 0) ang += 360
  return (ang % 360) * 60000
}
