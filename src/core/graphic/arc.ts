// Import

import { toAngleByXYRad } from '../common/utils'
import { CanvasPathLike } from './type'

interface CurveDrawerContext {
  _moveTo(x: number, y: number): void
  _bezierCurveTo(
    x0: number,
    y0: number,
    x1: number,
    y1: number,
    x2: number,
    y2: number
  ): void
}

interface Point {
  x: number
  y: number
}
export function arcToCurves(
  ctx: CurveDrawerContext,
  x: number,
  y: number,
  width: number,
  height: number,
  startAngle: number,
  sweepAngle: number
) {
  const sin1 = Math.sin(startAngle)
  const cos1 = Math.cos(startAngle)

  const _x = cos1 / width
  const _y = sin1 / height
  const l = 1 / Math.sqrt(_x * _x + _y * _y)

  const cx = x - l * cos1
  const cy = y - l * sin1

  arc(
    ctx,
    cx - width,
    cy - height,
    2 * width,
    2 * height,
    startAngle,
    sweepAngle
  )
}

function arc(
  ctx: CurveDrawerContext,
  x: number,
  y: number,
  width: number,
  height: number,
  startAngle: number,
  sweepAngle: number
) {
  if (0 >= width || 0 >= height) return

  startAngle = -startAngle
  sweepAngle = -sweepAngle

  let isClockDirection = false
  const endAngle = 2 * Math.PI - (sweepAngle + startAngle)
  const srtAngle = 2 * Math.PI - startAngle
  if (sweepAngle > 0) isClockDirection = true

  if (Math.abs(sweepAngle) >= 2 * Math.PI) {
    ellipse(ctx, x + width / 2, y + height / 2, width / 2, height / 2)
  } else {
    arcOfEllipse(
      ctx,
      x + width / 2,
      y + height / 2,
      width / 2,
      height / 2,
      srtAngle,
      endAngle,
      isClockDirection
    )
  }
}

function ellipse(
  ctx: CurveDrawerContext,
  x: number,
  y: number,
  xRad: number,
  yRad: number
) {
  ctx._moveTo(x - xRad, y)

  const kappa = 0.552
  ctx._bezierCurveTo(
    x - xRad,
    y + yRad * kappa,
    x - xRad * kappa,
    y + yRad,
    x,
    y + yRad
  )
  ctx._bezierCurveTo(
    x + xRad * kappa,
    y + yRad,
    x + xRad,
    y + yRad * kappa,
    x + xRad,
    y
  )
  ctx._bezierCurveTo(
    x + xRad,
    y - yRad * kappa,
    x + xRad * kappa,
    y - yRad,
    x,
    y - yRad
  )
  ctx._bezierCurveTo(
    x - xRad * kappa,
    y - yRad,
    x - xRad,
    y - yRad * kappa,
    x - xRad,
    y
  )
}

function arcOfEllipse(
  ctx: CurveDrawerContext,
  x: number,
  y: number,
  xRad: number,
  yRad: number,
  angle1: number,
  angle2: number,
  isClockDirection: boolean
) {
  while (angle1 < 0) angle1 += 2 * Math.PI

  while (angle1 > 2 * Math.PI) angle1 -= 2 * Math.PI

  while (angle2 < 0) angle2 += 2 * Math.PI

  while (angle2 >= 2 * Math.PI) angle2 -= 2 * Math.PI

  if (!isClockDirection) {
    if (angle1 <= angle2) {
      arcOfEllipseForQuards(ctx, x, y, xRad, yRad, angle1, angle2, false)
    } else {
      arcOfEllipseForQuards(ctx, x, y, xRad, yRad, angle1, 2 * Math.PI, false)
      arcOfEllipseForQuards(ctx, x, y, xRad, yRad, 0, angle2, false)
    }
  } else {
    if (angle1 >= angle2) {
      arcOfEllipseForQuards(ctx, x, y, xRad, yRad, angle1, angle2, true)
    } else {
      arcOfEllipseForQuards(ctx, x, y, xRad, yRad, angle1, 0, true)
      arcOfEllipseForQuards(ctx, x, y, xRad, yRad, 2 * Math.PI, angle2, true)
    }
  }
}

function arcOfEllipseForQuards(
  ctx: CurveDrawerContext,
  x: number,
  y: number,
  xRad: number,
  yRad: number,
  angle1: number,
  angle2: number,
  isClockDirection: boolean
) {
  let valueOfFirstPointQuard = (((2 * angle1) / Math.PI) >> 0) + 1
  let valueOfSecondPointQuard = (((2 * angle2) / Math.PI) >> 0) + 1
  valueOfSecondPointQuard = Math.min(4, Math.max(1, valueOfSecondPointQuard))
  valueOfFirstPointQuard = Math.min(4, Math.max(1, valueOfFirstPointQuard))

  let endPoint = { x: 0, y: 0 }

  let startAng = angle1
  let endAng = 0

  if (!isClockDirection) {
    for (let i = valueOfFirstPointQuard; i <= valueOfSecondPointQuard; i++) {
      if (i === valueOfSecondPointQuard) endAng = angle2
      else endAng = (i * Math.PI) / 2
      if (!(i === valueOfFirstPointQuard)) {
        startAng = ((i - 1) * Math.PI) / 2
      }

      endPoint = _arcOfEllipse(
        ctx,
        x,
        y,
        xRad,
        yRad,
        toAngleByXYRad(startAng, xRad, yRad),
        toAngleByXYRad(endAng, xRad, yRad),
        false
      )
    }
  } else {
    for (
      let valueOfIndex = valueOfFirstPointQuard;
      valueOfIndex >= valueOfSecondPointQuard;
      valueOfIndex--
    ) {
      if (valueOfIndex === valueOfFirstPointQuard) startAng = angle1
      else startAng = (valueOfIndex * Math.PI) / 2
      if (!(valueOfIndex === valueOfSecondPointQuard)) {
        endAng = ((valueOfIndex - 1) * Math.PI) / 2
      } else endAng = angle2

      endPoint = _arcOfEllipse(
        ctx,
        x,
        y,
        xRad,
        yRad,
        toAngleByXYRad(startAng, xRad, yRad),
        toAngleByXYRad(endAng, xRad, yRad),
        false
      )
    }
  }
  return endPoint
}

function _arcOfEllipse(
  ctx: CurveDrawerContext,
  x: number,
  y: number,
  xRad: number,
  yRad: number,
  ang1: number,
  ang2: number,
  isClockDirection: boolean
): Point {
  const alpha =
    (Math.sin(ang2 - ang1) *
      (Math.sqrt(
        4.0 +
          3.0 * Math.tan((ang2 - ang1) / 2.0) * Math.tan((ang2 - ang1) / 2.0)
      ) -
        1.0)) /
    3.0

  const sin1 = Math.sin(ang1)
  const cos1 = Math.cos(ang1)
  const sin2 = Math.sin(ang2)
  const cos2 = Math.cos(ang2)

  const x1 = x + xRad * cos1
  const y1 = y + yRad * sin1

  const x2 = x + xRad * cos2
  const y2 = y + yRad * sin2

  const cx1 = x1 - alpha * xRad * sin1
  const cy1 = y1 + alpha * yRad * cos1

  const cx2 = x2 + alpha * xRad * sin2
  const cy2 = y2 - alpha * yRad * cos2

  if (!isClockDirection) {
    ctx._bezierCurveTo(cx1, cy1, cx2, cy2, x2, y2)
    return { x: x2, y: y2 }
  } else {
    ctx._bezierCurveTo(cx2, cy2, cx1, cy1, x1, y1)
    return { x: x1, y: y1 }
  }
}

// ----------------------------------------------------------------------- //

export function drawArcOnCanvas(
  context: CanvasPathLike,
  startX: number,
  startY: number,
  widthR: number,
  heightR: number,
  startAng: number,
  sweepAng: number
) {
  const sin = Math.sin(startAng)
  const cos = Math.cos(startAng)

  const _x = cos / widthR
  const _y = sin / heightR
  const _l = 1 / Math.sqrt(_x * _x + _y * _y)

  const _cx = startX - _l * cos
  const _cy = startY - _l * sin

  arcOnCanvas(
    context,
    _cx - widthR,
    _cy - heightR,
    2 * widthR,
    2 * heightR,
    startAng,
    sweepAng
  )
}

function arcOnCanvas(
  context: CanvasPathLike,
  lcX: number,
  lcY: number,
  width: number,
  height: number,
  startAng: number,
  sweepAng: number
) {
  if (0 >= width || 0 >= height) return

  startAng = -startAng
  sweepAng = -sweepAng

  let isClockDirection = false
  const endAngle = 2 * Math.PI - (sweepAng + startAng)
  const stAngle = 2 * Math.PI - startAng
  if (sweepAng > 0) {
    isClockDirection = true
  }

  if (Math.abs(sweepAng) >= 2 * Math.PI) {
    darwEllipseOnCanvas(
      context,
      lcX + width / 2,
      lcY + height / 2,
      width / 2,
      height / 2
    )
  } else {
    drawArcOfEllipseOnCanvas(
      context,
      lcX + width / 2,
      lcY + height / 2,
      width / 2,
      height / 2,
      stAngle,
      endAngle,
      isClockDirection
    )
  }
}

function darwEllipseOnCanvas(
  ctx: CanvasPathLike,
  x: number,
  y: number,
  xRad: number,
  yRad: number
) {
  ctx.moveTo(x - xRad, y)
  const factor = 0.552
  ctx.bezierCurveTo(
    x - xRad,
    y + yRad * factor,
    x - xRad * factor,
    y + yRad,
    x,
    y + yRad
  )
  ctx.bezierCurveTo(
    x + xRad * factor,
    y + yRad,
    x + xRad,
    y + yRad * factor,
    x + xRad,
    y
  )
  ctx.bezierCurveTo(
    x + xRad,
    y - yRad * factor,
    x + xRad * factor,
    y - yRad,
    x,
    y - yRad
  )
  ctx.bezierCurveTo(
    x - xRad * factor,
    y - yRad,
    x - xRad,
    y - yRad * factor,
    x - xRad,
    y
  )
}

function drawArcOfEllipseOnCanvas(
  ctx: CanvasPathLike,
  x: number,
  y: number,
  xRad: number,
  yRad: number,
  angle1: number,
  angle2: number,
  isClockDirection: boolean
) {
  while (angle1 < 0) angle1 += 2 * Math.PI

  while (angle1 > 2 * Math.PI) angle1 -= 2 * Math.PI

  while (angle2 < 0) angle2 += 2 * Math.PI

  while (angle2 >= 2 * Math.PI) angle2 -= 2 * Math.PI

  if (!isClockDirection) {
    if (angle1 <= angle2) {
      DrawArcOfEllipseOnCanvasForQuards(
        ctx,
        x,
        y,
        xRad,
        yRad,
        angle1,
        angle2,
        false
      )
    } else {
      DrawArcOfEllipseOnCanvasForQuards(
        ctx,
        x,
        y,
        xRad,
        yRad,
        angle1,
        2 * Math.PI,
        false
      )
      DrawArcOfEllipseOnCanvasForQuards(ctx, x, y, xRad, yRad, 0, angle2, false)
    }
  } else {
    if (angle1 >= angle2) {
      DrawArcOfEllipseOnCanvasForQuards(
        ctx,
        x,
        y,
        xRad,
        yRad,
        angle1,
        angle2,
        true
      )
    } else {
      DrawArcOfEllipseOnCanvasForQuards(ctx, x, y, xRad, yRad, angle1, 0, true)
      DrawArcOfEllipseOnCanvasForQuards(
        ctx,
        x,
        y,
        xRad,
        yRad,
        2 * Math.PI,
        angle2,
        true
      )
    }
  }
}

function DrawArcOfEllipseOnCanvasForQuards(
  ctx: CanvasPathLike,
  x: number,
  y: number,
  xRad: number,
  yRad: number,
  angle1: number,
  angle2: number,
  isClockDirection: boolean
) {
  let valueOfFirstPointQuard = (((2 * angle1) / Math.PI) >> 0) + 1
  let valueOfSecondPointQuard = (((2 * angle2) / Math.PI) >> 0) + 1
  valueOfSecondPointQuard = Math.min(4, Math.max(1, valueOfSecondPointQuard))
  valueOfFirstPointQuard = Math.min(4, Math.max(1, valueOfFirstPointQuard))

  const startX = x + xRad * Math.cos(toAngleByXYRad(angle1, xRad, yRad))
  const startY = y + yRad * Math.sin(toAngleByXYRad(angle1, xRad, yRad))

  let endPoint = { x: 0, y: 0 }
  ctx.lineTo(startX, startY)

  let startAngle = angle1
  let endAngle = 0

  if (!isClockDirection) {
    for (
      let valueOfIndex = valueOfFirstPointQuard;
      valueOfIndex <= valueOfSecondPointQuard;
      valueOfIndex++
    ) {
      if (valueOfIndex === valueOfSecondPointQuard) endAngle = angle2
      else endAngle = (valueOfIndex * Math.PI) / 2
      if (!(valueOfIndex === valueOfFirstPointQuard)) {
        startAngle = ((valueOfIndex - 1) * Math.PI) / 2
      }

      endPoint = arcOfEllipseOnCtx(
        ctx,
        x,
        y,
        xRad,
        yRad,
        toAngleByXYRad(startAngle, xRad, yRad),
        toAngleByXYRad(endAngle, xRad, yRad),
        false
      )
    }
  } else {
    for (
      let valueOfIndex = valueOfFirstPointQuard;
      valueOfIndex >= valueOfSecondPointQuard;
      valueOfIndex--
    ) {
      if (valueOfIndex === valueOfFirstPointQuard) startAngle = angle1
      else startAngle = (valueOfIndex * Math.PI) / 2
      if (!(valueOfIndex === valueOfSecondPointQuard)) {
        endAngle = ((valueOfIndex - 1) * Math.PI) / 2
      } else endAngle = angle2

      endPoint = arcOfEllipseOnCtx(
        ctx,
        x,
        y,
        xRad,
        yRad,
        toAngleByXYRad(startAngle, xRad, yRad),
        toAngleByXYRad(endAngle, xRad, yRad),
        false
      )
    }
  }
  return endPoint
}

function arcOfEllipseOnCtx(
  ctx: CanvasPathLike,
  x: number,
  y: number,
  xRad: number,
  yRad: number,
  angle1: number,
  angle2: number,
  isClockDirection: boolean
): Point {
  const alpha =
    (Math.sin(angle2 - angle1) *
      (Math.sqrt(
        4.0 +
          3.0 *
            Math.tan((angle2 - angle1) / 2.0) *
            Math.tan((angle2 - angle1) / 2.0)
      ) -
        1.0)) /
    3.0

  const sin1 = Math.sin(angle1)
  const cos1 = Math.cos(angle1)
  const sin2 = Math.sin(angle2)
  const cos2 = Math.cos(angle2)

  const x1 = x + xRad * cos1
  const y1 = y + yRad * sin1

  const x2 = x + xRad * cos2
  const y2 = y + yRad * sin2

  const cx1 = x1 - alpha * xRad * sin1
  const cy1 = y1 + alpha * yRad * cos1

  const cx2 = x2 + alpha * xRad * sin2
  const cy2 = y2 - alpha * yRad * cos2

  if (!isClockDirection) {
    ctx.bezierCurveTo(cx1, cy1, cx2, cy2, x2, y2)
    return { x: x2, y: y2 }
  } else {
    ctx.bezierCurveTo(cx2, cy2, cx1, cy1, x1, y1)
    return { x: x1, y: y1 }
  }
}
