// Import

import { toAngleByXYRad } from '../common/utils'

export function hitLine(
  context: CanvasRenderingContext2D,
  px: number,
  py: number,
  x0: number,
  y0: number,
  x1: number,
  y1: number,
  isTouch: boolean
): boolean {
  /* let l = Math.min(x0, x1);
    let t = Math.min(y0, y1);
    let r = Math.max(x0, x1);
    let b = Math.max(y0, y1);
    if(px < l || px > r || py < t || py > b)
        return false;*/
  const tx = x1 - x0
  const ty = y1 - y0

  const isStraightLine = tx === 0 || ty === 0
  const dist = isTouch ? 3 : 1
  const d = dist / Math.sqrt(tx * tx + ty * ty)

  const dx = -ty * d
  const dy = tx * d

  const x01 = x0 + dx
  const y01 = y0 + dy
  const x11 = x1 + dx
  const y11 = y1 + dy
  const x02 = x0 - dx
  const y02 = y0 - dy
  const x12 = x1 - dx
  const y12 = y1 - dy

  const minX = Math.min(x0, x1, x01, x02, x11, x12)
  const maxX = Math.max(x0, x1, x01, x02, x11, x12)
  const minY = Math.min(y0, y1, y01, y02, y11, y12)
  const maxY = Math.max(y0, y1, y01, y02, y11, y12)

  if (px < minX || px > maxX || py < minY || py > maxY) return false

  if (isStraightLine) {
    return px >= minX && px <= maxX && py >= minY && py <= maxY
  }

  context.beginPath()
  context.moveTo(x0, y0)
  context.lineTo(x01, y01)
  context.lineTo(x11, y11)
  context.lineTo(x12, y12)
  context.lineTo(x02, y02)
  context.closePath()
  const isHit = context.isPointInPath(px, py)
  return isHit
}

export function hitInBezierCurve(
  context: CanvasRenderingContext2D,
  px: number,
  py: number,
  x0: number,
  y0: number,
  x1: number,
  y1: number,
  x2: number,
  y2: number,
  x3: number,
  y3: number
) {
  const l = Math.min(x0, x1, x2, x3)
  const t = Math.min(y0, y1, y2, y3)
  const r = Math.max(x0, x1, x2, x3)
  const b = Math.max(y0, y1, y2, y3)
  if (px < l || px > r || py < t || py > b) return false
  const tx = x3 - x0
  const ty = y3 - y0

  const d = 1.5 / Math.sqrt(tx * tx + ty * ty)

  const dx = -ty * d
  const dy = tx * d

  context.beginPath()
  context.moveTo(x0, y0)
  context.lineTo(x0 + dx, y0 + dy)
  context.bezierCurveTo(x1 + dx, y1 + dy, x2 + dx, y2 + dy, x3 + dx, y3 + dy)
  context.lineTo(x3 - dx, y3 - dy)
  context.bezierCurveTo(x2 - dx, y2 - dy, x1 - dx, y1 - dy, x0 - dx, y0 - dy)
  context.closePath()
  return context.isPointInPath(px, py)
}

export function hitInQuadraticCurve(
  context: CanvasRenderingContext2D,
  px: number,
  py: number,
  x0: number,
  y0: number,
  x1: number,
  y1: number,
  x2: number,
  y2: number
) {
  const l = Math.min(x0, x1, x2)
  const t = Math.min(y0, y1, y2)
  const r = Math.max(x0, x1, x2)
  const b = Math.max(y0, y1, y2)
  if (px < l || px > r || py < t || py > b) return false
  const tx = x2 - x0
  const ty = y2 - y0

  const d = 1.5 / Math.sqrt(tx * tx + ty * ty)

  const dx = -ty * d
  const dy = tx * d

  context.beginPath()
  context.moveTo(x0, y0)
  context.lineTo(x0 + dx, y0 + dy)
  context.quadraticCurveTo(x1 + dx, y1 + dy, x2 + dx, y2 + dy)
  context.lineTo(x2 - dx, y2 - dy)
  context.quadraticCurveTo(x1 - dx, y1 - dy, x0 - dx, y0 - dy)
  context.closePath()
  return context.isPointInPath(px, py)
}

interface HitEndPoint {
  x: number
  y: number
  hit: boolean
}
export function hitInArc(
  context: CanvasRenderingContext2D,
  px: number,
  py: number,
  start_x: number,
  start_y: number,
  width_r: number,
  height_r: number,
  start_ang: number,
  sweep_ang: number
) {
  const _sin = Math.sin(start_ang)
  const _cos = Math.cos(start_ang)

  const _x = _cos / width_r
  const _y = _sin / height_r
  const _l = 1 / Math.sqrt(_x * _x + _y * _y)

  const _cx = start_x - _l * _cos
  const _cy = start_y - _l * _sin

  return _hitInArc(
    px,
    py,
    context,
    _cx - width_r,
    _cy - height_r,
    2 * width_r,
    2 * height_r,
    start_ang,
    sweep_ang
  )
}

function _hitInArc(
  px: number,
  py: number,
  context: CanvasRenderingContext2D,
  _l_c_x: number,
  _l_c_y: number,
  width: number,
  height: number,
  start_ang: number,
  sweep_ang: number
) {
  if (0 >= width || 0 >= height) return false

  start_ang = -start_ang
  sweep_ang = -sweep_ang

  let isClockDirection = false
  const endAngle = 2 * Math.PI - (sweep_ang + start_ang)
  const stAngle = 2 * Math.PI - start_ang
  if (sweep_ang > 0) {
    isClockDirection = true
  }

  if (Math.abs(sweep_ang) >= 2 * Math.PI) {
    return hitInEllipse(
      px,
      py,
      context,
      _l_c_x + width / 2,
      _l_c_y + height / 2,
      width / 2,
      height / 2
    )
  } else {
    return hitInArcOfEllipse(
      px,
      py,
      context,
      _l_c_x + width / 2,
      _l_c_y + height / 2,
      width / 2,
      height / 2,
      stAngle,
      endAngle,
      isClockDirection
    )
  }
}

function hitInEllipse(
  px: number,
  py: number,
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  xRad: number,
  yRad: number
) {
  const kappa = 0.552
  return (
    hitInBezierCurve(
      ctx,
      px,
      py,
      x - xRad,
      y,
      x - xRad,
      y + yRad * kappa,
      x - xRad * kappa,
      y + yRad,
      x,
      y + yRad
    ) ||
    hitInBezierCurve(
      ctx,
      px,
      py,
      x,
      y + yRad,
      x + xRad * kappa,
      y + yRad,
      x + xRad,
      y + yRad * kappa,
      x + xRad,
      y
    ) ||
    hitInBezierCurve(
      ctx,
      px,
      py,
      x + xRad,
      y,
      x + xRad,
      y - yRad * kappa,
      x + xRad * kappa,
      y - yRad,
      x,
      y - yRad
    ) ||
    hitInBezierCurve(
      ctx,
      px,
      py,
      x,
      y - yRad,
      x - xRad * kappa,
      y - yRad,
      x - xRad,
      y - yRad * kappa,
      x - xRad,
      y
    )
  )
}

function hitInArcOfEllipse(
  px: number,
  py: number,
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  xRad: number,
  yRad: number,
  angle1: number,
  angle2: number,
  isClockDirection: boolean
) {
  while (angle1 < 0) angle1 += 2 * Math.PI

  while (angle1 > 2 * Math.PI) angle1 -= 2 * Math.PI

  while (angle2 < 0) angle2 += 2 * Math.PI

  while (angle2 >= 2 * Math.PI) angle2 -= 2 * Math.PI

  if (!isClockDirection) {
    if (angle1 <= angle2) {
      return _hitInArcOfEllipse(
        px,
        py,
        ctx,
        x,
        y,
        xRad,
        yRad,
        angle1,
        angle2,
        false
      )
    } else {
      return (
        _hitInArcOfEllipse(
          px,
          py,
          ctx,
          x,
          y,
          xRad,
          yRad,
          angle1,
          2 * Math.PI,
          false
        ) || _hitInArcOfEllipse(px, py, ctx, x, y, xRad, yRad, 0, angle2, false)
      )
    }
  } else {
    if (angle1 >= angle2) {
      return _hitInArcOfEllipse(
        px,
        py,
        ctx,
        x,
        y,
        xRad,
        yRad,
        angle1,
        angle2,
        true
      )
    } else {
      return (
        _hitInArcOfEllipse(px, py, ctx, x, y, xRad, yRad, angle1, 0, true) ||
        _hitInArcOfEllipse(
          px,
          py,
          ctx,
          x,
          y,
          xRad,
          yRad,
          2 * Math.PI,
          angle2,
          true
        )
      )
    }
  }
}

function _hitInArcOfEllipse(
  px: number,
  py: number,
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  xRad: number,
  yRad: number,
  angle1: number,
  angle2: number,
  isClockDirection: boolean
) {
  let valueOfFirstPointQuard = (((2 * angle1) / Math.PI) >> 0) + 1
  let valueOfSecondPointQuard = (((2 * angle2) / Math.PI) >> 0) + 1
  valueOfSecondPointQuard = Math.min(4, Math.max(1, valueOfSecondPointQuard))
  valueOfFirstPointQuard = Math.min(4, Math.max(1, valueOfFirstPointQuard))

  const startX = x + xRad * Math.cos(toAngleByXYRad(angle1, xRad, yRad))
  const startY = y + yRad * Math.sin(toAngleByXYRad(angle1, xRad, yRad))

  let endPoint: HitEndPoint = { x: startX, y: startY, hit: false }
  let startAngle = angle1
  let endAngle = 0

  if (!isClockDirection) {
    for (
      let valueOfIndex = valueOfFirstPointQuard;
      valueOfIndex <= valueOfSecondPointQuard;
      valueOfIndex++
    ) {
      if (valueOfIndex === valueOfSecondPointQuard) endAngle = angle2
      else endAngle = (valueOfIndex * Math.PI) / 2
      if (!(valueOfIndex === valueOfFirstPointQuard)) {
        startAngle = ((valueOfIndex - 1) * Math.PI) / 2
      }

      endPoint = hitInEllipseBezierCurve(
        px,
        py,
        endPoint,
        ctx,
        x,
        y,
        xRad,
        yRad,
        toAngleByXYRad(startAngle, xRad, yRad),
        toAngleByXYRad(endAngle, xRad, yRad),
        false
      )
      if (endPoint.hit) {
        break
      }
    }
  } else {
    for (
      let valueOfIndex = valueOfFirstPointQuard;
      valueOfIndex >= valueOfSecondPointQuard;
      valueOfIndex--
    ) {
      if (valueOfIndex === valueOfFirstPointQuard) startAngle = angle1
      else startAngle = (valueOfIndex * Math.PI) / 2
      if (!(valueOfIndex === valueOfSecondPointQuard)) {
        endAngle = ((valueOfIndex - 1) * Math.PI) / 2
      } else endAngle = angle2

      endPoint = hitInEllipseBezierCurve(
        px,
        py,
        endPoint,
        ctx,
        x,
        y,
        xRad,
        yRad,
        toAngleByXYRad(startAngle, xRad, yRad),
        toAngleByXYRad(endAngle, xRad, yRad),
        false
      )
      if (endPoint.hit) {
        break
      }
    }
  }

  return endPoint.hit
}

function hitInEllipseBezierCurve(
  px: number,
  py: number,
  EndPoint: HitEndPoint,
  ctx: CanvasRenderingContext2D,
  x: number,
  y: number,
  xRad: number,
  yRad: number,
  angle1: number,
  angle2: number,
  isClockDirection: boolean
): HitEndPoint {
  const alpha =
    (Math.sin(angle2 - angle1) *
      (Math.sqrt(
        4.0 +
          3.0 *
            Math.tan((angle2 - angle1) / 2.0) *
            Math.tan((angle2 - angle1) / 2.0)
      ) -
        1.0)) /
    3.0

  const sin1 = Math.sin(angle1)
  const cos1 = Math.cos(angle1)
  const sin2 = Math.sin(angle2)
  const cos2 = Math.cos(angle2)

  const x1 = x + xRad * cos1
  const y1 = y + yRad * sin1

  const x2 = x + xRad * cos2
  const y2 = y + yRad * sin2

  const cx1 = x1 - alpha * xRad * sin1
  const cy1 = y1 + alpha * yRad * cos1

  const cx2 = x2 + alpha * xRad * sin2
  const cy2 = y2 - alpha * yRad * cos2

  if (!isClockDirection) {
    return {
      x: x2,
      y: y2,
      hit: hitInBezierCurve(
        ctx,
        px,
        py,
        EndPoint.x,
        EndPoint.y,
        cx1,
        cy1,
        cx2,
        cy2,
        x2,
        y2
      ),
    }
  } else {
    return {
      x: x1,
      y: y1,
      hit: hitInBezierCurve(
        ctx,
        px,
        py,
        EndPoint.x,
        EndPoint.y,
        cx2,
        cy2,
        cx1,
        cy1,
        x1,
        y1
      ),
    }
  }
}

let hitTestCtx: CanvasRenderingContext2D | undefined
export function getHitTestContext(): CanvasRenderingContext2D {
  if (!hitTestCtx) {
    const canvas = document.createElement('canvas')
    canvas.width = 10
    canvas.height = 10
    hitTestCtx = canvas.getContext('2d')!
  }
  return hitTestCtx
}
