import { Nullable } from '../../../liber/pervasive'

export function deg2rad(deg: number) {
  return (deg * Math.PI) / 180.0
}

export function rad2deg(rad: number) {
  return (rad * 180.0) / Math.PI
}

export class Matrix2D {
  sx: number // Horizontal scaling
  shy: number // Vertical skewing
  shx: number // Horizontal skewing
  sy: number // Horizontal scaling
  tx: number // Horizontal translation
  ty: number // Vertical translation
  constructor() {
    this.sx = 1.0
    this.shx = 0.0
    this.shy = 0.0
    this.sy = 1.0
    this.tx = 0.0
    this.ty = 0.0
  }

  reset(matrix?: IMatrix2D) {
    if (matrix == null) {
      this.sx = 1.0
      this.shx = 0.0
      this.shy = 0.0
      this.sy = 1.0
      this.tx = 0.0
      this.ty = 0.0
    } else {
      this.sx = matrix.sx
      this.shx = matrix.shx
      this.shy = matrix.shy
      this.sy = matrix.sy
      this.tx = matrix.tx
      this.ty = matrix.ty
    }
  }

  rotate(degree: number) {
    const rad = deg2rad(degree)
    rotateMatrix(this, rad)
  }

  // determinant
  factorForInvert() {
    return this.sx * this.sy - this.shy * this.shx
  }

  // invert
  invert() {
    const det = this.factorForInvert()
    if (0.0001 > Math.abs(det)) return
    const d = 1 / det

    const t0 = this.sy * d
    this.sy = this.sx * d
    this.shy = -this.shy * d
    this.shx = -this.shx * d

    const t4 = -this.tx * t0 - this.ty * this.shx
    this.ty = -this.tx * this.shy - this.ty * this.sy

    this.sx = t0
    this.tx = t4
    return this
  }

  // transform point
  XFromPoint(x, y) {
    return XFromPoint(this, x, y)
  }

  YFromPoint(x, y) {
    return YFromPoint(this, x, y)
  }

  // calculate rotate angle
  getRotDegree() {
    const x1 = 0.0
    const y1 = 0.0
    const x2 = 1.0
    const y2 = 0.0
    const _x1 = this.XFromPoint(x1, y1)
    const _y1 = this.YFromPoint(x1, y1)
    const _x2 = this.XFromPoint(x2, y2)
    const _y2 = this.YFromPoint(x2, y2)

    const _y = _y2 - _y1
    const _x = _x2 - _x1

    if (Math.abs(_y) < 0.001) {
      if (_x > 0) return 0
      else return 180
    }
    if (Math.abs(_x) < 0.001) {
      if (_y > 0) return 90
      else return 270
    }

    let a = Math.atan2(_y, _x)
    a = rad2deg(a)
    if (a < 0) a += 360
    return a
  }

  clone() {
    return createMatrix(this)
  }

  getScaleVal() {
    const x1 = this.XFromPoint(0, 0)
    const y1 = this.YFromPoint(0, 0)
    const x2 = this.XFromPoint(1, 1)
    const y2 = this.YFromPoint(1, 1)
    return Math.sqrt(((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1)) / 2)
  }
}

export interface IMatrix2D {
  sx: number
  shy: number
  shx: number
  sy: number
  tx: number
  ty: number
}

export function createMatrix(matrix: IMatrix2D): Matrix2D {
  const m = new Matrix2D()
  m.reset(matrix)
  return m
}

// Matrix Transformers

function translateMatrix(m: IMatrix2D, tx: number, ty: number) {
  m.tx += tx
  m.ty += ty
}

function scaleMatrix(m: IMatrix2D, sx: number, sy: number) {
  m.sx *= sx
  m.shx *= sx
  m.shy *= sy
  m.sy *= sy
  m.tx *= sx
  m.ty *= sy
}

function rotateMatrix(m: IMatrix2D, rad: number) {
  const sx = Math.cos(rad)
  const shx = Math.sin(rad)
  const shy = -Math.sin(rad)
  const sy = Math.cos(rad)

  const t0 = m.sx * sx + m.shy * shx
  const t2 = m.shx * sx + m.sy * shx
  const t4 = m.tx * sx + m.ty * shx
  m.shy = m.sx * shy + m.shy * sy
  m.sy = m.shx * shy + m.sy * sy
  m.ty = m.tx * shy + m.ty * sy
  m.sx = t0
  m.shx = t2
  m.tx = t4
}

function multiplyMatrixes(m1: IMatrix2D, m2: IMatrix2D) {
  const t0 = m1.sx * m2.sx + m1.shy * m2.shx
  const t2 = m1.shx * m2.sx + m1.sy * m2.shx
  const t4 = m1.tx * m2.sx + m1.ty * m2.shx + m2.tx
  m1.shy = m1.sx * m2.shy + m1.shy * m2.sy
  m1.sy = m1.shx * m2.shy + m1.sy * m2.sy
  m1.ty = m1.tx * m2.shy + m1.ty * m2.sy + m2.ty
  m1.sx = t0
  m1.shx = t2
  m1.tx = t4
}

function invertMatrix(m: IMatrix2D) {
  const tmpMatrix = createMatrix(m)
  tmpMatrix.invert()
  return tmpMatrix
}

function invertMatrixFrom(t: Nullable<Matrix2D>, m: IMatrix2D) {
  if (t) {
    t.reset(m)
  } else {
    t = createMatrix(m)
  }

  t.invert()
  return t
}

function multiplyWithInvertMatrix(m1: IMatrix2D, m2: IMatrix2D) {
  const m = invertMatrix(m2)
  multiplyMatrixes(m1, m)
}

function preMultiplyMatrixes(m1: IMatrix2D, m2: IMatrix2D) {
  const m = createMatrix(m2)
  multiplyMatrixes(m, m1)
  m1.sx = m.sx
  m1.shx = m.shx
  m1.shy = m.shy
  m1.sy = m.sy
  m1.tx = m.tx
  m1.ty = m.ty
}

function isIdentity(m: IMatrix2D) {
  if (
    m.sx === 1.0 &&
    m.shx === 0.0 &&
    m.shy === 0.0 &&
    m.sy === 1.0 &&
    m.tx === 0.0 &&
    m.ty === 0.0
  ) {
    return true
  }
  return false
}

function isOnlyTranslate(m: IMatrix2D) {
  if (m.sx === 1.0 && m.shx === 0.0 && m.shy === 0.0 && m.sy === 1.0) {
    return true
  }
  return false
}

function XFromPoint(m: IMatrix2D, x: number, y: number): number {
  return x * m.sx + y * m.shx + m.tx
}

function YFromPoint(m: IMatrix2D, x: number, y: number): number {
  return x * m.shy + y * m.sy + m.ty
}

function getRotDegree(m: IMatrix2D) {
  return createMatrix(m).getRotDegree()
}

function flip(
  m: IMatrix2D,
  w: number,
  h: number,
  flipH: boolean,
  flipV: boolean
) {
  if (flipH !== true && flipV !== true) {
    return
  }

  const hc = w * 0.5
  const vc = h * 0.5
  translateMatrix(m, -hc, -vc)
  if (flipH) {
    scaleMatrix(m, -1, 1)
  }

  if (flipV) {
    scaleMatrix(m, 1, -1)
  }

  translateMatrix(m, hc, vc)
}

function applyToCanvas2DContext(m: IMatrix2D, ctx: CanvasRenderingContext2D) {
  ctx.setTransform(m.sx, m.shy, m.shx, m.sy, m.tx, m.ty)
}

export const MatrixUtils = {
  translateMatrix,
  scaleMatrix,
  rotateMatrix,
  multiplyMatrixes,
  invertMatrix,
  invertMatrixFrom,
  multiplyWithInvertMatrix,
  XFromPoint,
  YFromPoint,
  preMultiplyMatrixes,
  isIdentity,
  isOnlyTranslate,
  getRotDegree,
  flip,
  applyToCanvas2DContext,
} as const
