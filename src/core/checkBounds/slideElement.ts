import { InstanceType } from '../instanceTypes'
import { SlideElement } from '../SlideElement/type'
import { GraphicsBoundsChecker } from '../graphic/Bounds'
import { Shape } from '../SlideElement/Shape'
import { isPlaceholderWithEmptyContent } from '../utilities/shape/asserts'
import { Path, PathOperationType } from '../SlideElement/geometry/Path'
import { Geometry } from '../SlideElement/geometry/Geometry'
import { arcToCurves } from '../graphic/arc'
import { transformUpdater } from '../utilities/shape/type'
import { GraphicFrame } from '../SlideElement/GraphicFrame'
import { ImageShape } from '../SlideElement/Image'
import { checkBounds_TextBody } from './textContent'
import { checkTextTransformMatrix } from '../utilities/shape/getters'
import { Nullable } from '../../../liber/pervasive'
import {
  getInitState,
  getLnInfo,
  ShapeRenderingState,
} from '../graphic/ShapeRenderingState'
import { checkBoundsForShape } from '../utilities/shape/draw'

export function checkBounds_SlideElement(
  sp: SlideElement,
  checker: GraphicsBoundsChecker,
  transformUpdater?: transformUpdater
) {
  switch (sp.instanceType) {
    case InstanceType.Shape:
      checkBounds_Shape(sp, checker, transformUpdater)
      break
    case InstanceType.GroupShape: {
      const spTree = sp.spTree
      for (let i = 0, l = spTree.length; i < l; ++i) {
        checkBounds_SlideElement(spTree[i], checker, transformUpdater)
      }
      checker.reset()
      break
    }
    case InstanceType.GraphicFrame:
      checkBounds_GraphicFrame(sp, checker, transformUpdater)
      break
    case InstanceType.ImageShape:
      checkBounds_Image(sp, checker, transformUpdater)
      break
  }
}

function checkBounds_Shape(
  sp: Shape,
  checker: GraphicsBoundsChecker,
  transformUpdater?: transformUpdater
) {
  const isUseTransformUpdater = !!transformUpdater
  const transform = transformUpdater
    ? transformUpdater(sp, checker, sp.transform)
    : sp.transform
  const textTransform = sp.textTransform
  const geometry = sp.calcedGeometry || (sp.spPr && sp.spPr.geometry)

  checker.applyTransformMatrix(transform)
  if (
    !sp.spPr ||
    !geometry ||
    geometry.pathLst.length === 0 ||
    (geometry.pathLst.length === 1 &&
      geometry.pathLst[0].pathCommandsInfos.length === 0) ||
    geometry.preset === 'heart'
  ) {
    checker.rect(0, 0, sp.extX, sp.extY)
  } else {
    checkBounds_Geometry(geometry, checker)
  }
  if (sp.txBody) {
    let textMatrix
    if (
      (!sp.txBody.textDoc || sp.txBody.textDoc.isEmpty()) &&
      sp.txBody.phContent &&
      !sp.txBody.isCurrentPlaceholder() &&
      isPlaceholderWithEmptyContent(sp) &&
      sp.textTransformForPh
    ) {
      textMatrix = sp.textTransformForPh
    } else if (sp.txBody.textDoc) {
      textMatrix = textTransform

      if (isUseTransformUpdater) {
        textMatrix = textMatrix.clone()
        checkTextTransformMatrix(
          sp,
          textMatrix,
          sp.txBody.textDoc,
          sp.getBodyPr(),
          {
            transform,
            rot: 0,
          }
        )
      }
    }
    if (textMatrix) {
      checker.applyTransformMatrix(textMatrix)
    }
    checkBounds_TextBody(sp.txBody, checker)
  }
  checker.reset()
}

function checkBounds_GraphicFrame(
  sp: GraphicFrame,
  checker: GraphicsBoundsChecker,
  transformUpdater?: transformUpdater
) {
  const transform = transformUpdater
    ? transformUpdater(sp, checker, sp.transform)
    : sp.transform
  checker.applyTransformMatrix(transform)
  checker.rect(0, 0, sp.extX, sp.extY)
  checker.reset()
}

function checkBounds_Image(
  sp: ImageShape,
  checker: GraphicsBoundsChecker,
  transformUpdater?: transformUpdater
) {
  // 裁剪时不应再绘制原图但 bounds 仍需计算
  const transform = transformUpdater
    ? transformUpdater(sp, checker, sp.transform)
    : sp.transform
  checker.applyTransformMatrix(transform)
  const orgPen = sp.pen
  if (sp.pen || sp.brush) {
    _checkBoundsForImageShape(sp, checker, sp.calcedGeometry)
  }
  if (orgPen != null) {
    sp.pen = null
    _checkBoundsForImageShape(sp, checker, sp.calcedGeometry)
  }
  sp.pen = orgPen
  checker.reset()
}

export function checkBounds_Geometry(
  geom: Geometry,
  checker: GraphicsBoundsChecker
) {
  const pathLst = geom.pathLst
  for (let i = 0, n = pathLst.length; i < n; ++i) {
    checkBounds_Path(pathLst[i], checker)
  }
}

function checkBounds_Path(path: Path, checker: GraphicsBoundsChecker) {
  const pathCmd = path.pathCommands
  for (let j = 0, l = pathCmd.length; j < l; ++j) {
    const cmd = pathCmd[j]
    switch (cmd.id) {
      case PathOperationType.moveTo: {
        checker._moveTo(cmd.x, cmd.y)
        break
      }
      case PathOperationType.lineTo: {
        checker._lineTo(cmd.x, cmd.y)
        break
      }
      case PathOperationType.quadBezTo: {
        checker._quadraticCurveTo(cmd.x0, cmd.y0, cmd.x1, cmd.y1)
        break
      }
      case PathOperationType.cubicBezTo: {
        checker._bezierCurveTo(cmd.x0, cmd.y0, cmd.x1, cmd.y1, cmd.x2, cmd.y2)
        break
      }
      case PathOperationType.arcTo: {
        arcToCurves(
          checker,
          cmd.stX,
          cmd.stY,
          cmd.wR,
          cmd.hR,
          cmd.stAng,
          cmd.swAng
        )
        break
      }
      case PathOperationType.close: {
        checker._close()
        break
      }
    }
  }
}
function getStateForCheckBounds(shape: ImageShape): ShapeRenderingState {
  const state = getInitState()
  state.extX = shape.extX ?? 0
  state.extY = shape.extY ?? 0
  state.Ln = shape.pen

  const lnInfo = getLnInfo(state.Ln)
  if (lnInfo.noStroke === true) {
    state.isNoStrokeForce = true
  } else {
    const { StrokeRGBAColor: strokeRGBAColor, strokeWidth } = lnInfo
    state.StrokeRGBAColor = strokeRGBAColor
    state.strokeWidth = strokeWidth!
  }

  checkBoundsForShape(shape, state)
  return state
}

function updateBoundCheckerByState(
  checker: GraphicsBoundsChecker,
  state: ShapeRenderingState
) {
  checker.setPenWidth(1000 * state.strokeWidth)
  if (!state.isNoStrokeForce) {
    checker.lineWidth = state.strokeWidth
  }
}
function checkBounds(
  state: ShapeRenderingState,
  checker: GraphicsBoundsChecker,
  geom: Nullable<Geometry>
) {
  if (geom) {
    checkBounds_Geometry(geom, checker)
  } else {
    const { extX, extY } = state
    checker.rect(0, 0, extX, extY)
  }

  if (checker.needCheckLineWidth) {
    checker.correctBoundsWithSx()
  }
}
function _checkBoundsForImageShape(
  sp: ImageShape,
  checker: GraphicsBoundsChecker,
  geom: Nullable<Geometry>
) {
  const state = getStateForCheckBounds(sp)
  updateBoundCheckerByState(checker, state)

  checkBounds(state, checker, geom)
}
