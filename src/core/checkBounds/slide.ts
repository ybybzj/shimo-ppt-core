import { GraphicsBoundsChecker } from '../graphic/Bounds'
import { Presentation } from '../Slide/Presentation'
import { Slide } from '../Slide/Slide'
import { checkBounds_SlideElement } from './slideElement'

export function checkBounds_PresentationSlide(
  presentation: Presentation,
  slideIndex: number,
  checker: GraphicsBoundsChecker
) {
  const slide = presentation.slides[slideIndex]
  if (slide) {
    checkBounds_Slide(slide, checker)
  }
}

export function checkBounds_Slide(
  slide: Slide,
  checker: GraphicsBoundsChecker
) {
  checkBounds_SlideStaticContent(slide, checker)
  checkBounds_SlideShapes(slide, checker)
}

function checkBounds_SlideStaticContent(
  slide: Slide,
  checker: GraphicsBoundsChecker
) {
  const showMasterSp = slide.showMasterSp // remind false behave different with undefined
  const layoutShowMasterSp = slide.layout?.showMasterSp
  const shouldShowMasterSp =
    showMasterSp === true ||
    (!(showMasterSp === false) && layoutShowMasterSp !== false)

  let bounds
  checkBackgroundBound(checker, slide.width, slide.height)
  if (shouldShowMasterSp) {
    bounds = slide.layout?.master?.bounds
  }
  if (showMasterSp !== false) {
    bounds = slide.layout?.bounds
  }
  if (bounds) {
    checker.rect(bounds.l, bounds.t, bounds.w, bounds.h)
  }
}

function checkBounds_SlideShapes(slide: Slide, checker: GraphicsBoundsChecker) {
  const spTree = slide.cSld.spTree
  for (let i = 0, l = spTree.length; i < l; ++i) {
    checkBounds_SlideElement(spTree[i], checker)
  }
}

// helpers
function checkBackgroundBound(
  checker: GraphicsBoundsChecker,
  w: number,
  h: number
) {
  checker.rect(0, 0, w, h)

  if (checker.needCheckLineWidth) {
    checker.correctBoundsWithSx()
  }
}
