import { CommentStatus } from '../common/const/drawing'
import { GraphicsBoundsChecker } from '../graphic/Bounds'
import { getWidthOfComment, getHeightOfComment } from '../utilities/helpers'

export function checkBounds_comment(
  checker: GraphicsBoundsChecker,
  status: CommentStatus,
  x: number,
  y: number
) {
  const w = getWidthOfComment(status, checker.ZoomValue)
  const h = getHeightOfComment(status, checker.ZoomValue)
  checker.rect(x, y, w, h)
}
