import { TextDocument } from '../TextDocument/TextDocument'
import { GraphicsBoundsChecker } from '../graphic/Bounds'
import { TextBody } from '../SlideElement/TextBody'
import { isPlaceholderWithEmptyContent } from '../utilities/shape/asserts'

export function checkBounds_TextBody(
  txBody: TextBody,
  checker: GraphicsBoundsChecker
) {
  let x, y, w, h
  if (
    (!txBody.textDoc || txBody.textDoc.isEmpty()) &&
    !checker.isInPreview &&
    isPlaceholderWithEmptyContent(txBody.parent) &&
    !txBody.isCurrentPlaceholder()
  ) {
    if (txBody.phContent) {
      x = txBody.phContent.renderingState.x
      y = txBody.phContent.renderingState.y
      w = getWidthOfContent(txBody.phContent)
      h = txBody.phContent.getWholeHeight()
    }
  } else if (txBody.textDoc) {
    const isEmpty = txBody.textDoc.isEmpty()
    x = txBody.textDoc.renderingState.x
    y = txBody.textDoc.renderingState.y
    w = isEmpty ? 0.1 : getWidthOfContent(txBody.textDoc)
    h = txBody.textDoc.getWholeHeight()
  }

  if (x != null && y != null && w != null && h != null) {
    checker.rect(x, y, w, h)
  }
}

function getWidthOfContent(textDoc: TextDocument) {
  return textDoc.getWholeWidth()
}
