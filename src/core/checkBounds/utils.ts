import { isNumber } from '../../common/utils'
import { SlideElement } from '../SlideElement/type'
import { ScaleOfPPTXSizes } from '../common/const/unit'
import { Bounds, WithBounds } from '../graphic/Bounds'

function getSpBoundsForCheck(sp: SlideElement): WithBounds {
  const { bounds, pen } = sp
  const delta = 3 + (pen && isNumber(pen.w) ? pen.w / ScaleOfPPTXSizes : 0)
  return {
    minX: bounds.x - delta,
    minY: bounds.y - delta,
    maxX: bounds.x + bounds.w + delta,
    maxY: bounds.y + bounds.h + delta,
  }
}
export function isSpWithinBounds(sp: SlideElement, bounds: Bounds): boolean {
  const spBounds = getSpBoundsForCheck(sp)
  return bounds.isOverlap(spBounds)
}
