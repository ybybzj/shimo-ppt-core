export const enum EditActionFlag {
  edit_Unknown_Unknown = 0,

  edit_Paragraph_AddItem = 1,
  edit_Paragraph_RemoveItem = 2,
  edit_Paragraph_Align = 3,
  edit_Paragraph_Ind_First = 4,
  edit_Paragraph_Ind_Right = 5,
  edit_Paragraph_Ind_Left = 6,
  edit_Paragraph_Spacing_Line = 7,
  edit_Paragraph_Spacing_LineRule = 8,
  edit_Paragraph_Spacing_Before = 9,
  edit_Paragraph_Spacing_After = 10,
  edit_Paragraph_Spacing_AfterAutoSpacing = 11,
  edit_Paragraph_Spacing_BeforeAutoSpacing = 12,
  edit_Paragraph_Shd_Value = 13,
  edit_Paragraph_Shd_Color = 14,
  edit_Paragraph_Shd_FillEffects = 15,
  edit_Paragraph_Shd = 16,
  edit_Paragraph_Tabs = 17,
  edit_Paragraph_Pr = 18,
  edit_Paragraph_PresentationPr_Bullet = 19,
  edit_Paragraph_PresentationPr_Level = 20,
  edit_Paragraph_PrReviewInfo = 21,
  edit_Paragraph_OutlineLvl = 22,
  edit_Paragraph_DefaultTabSize = 23,

  edit_Table_TableCellMar = 24,
  edit_Table_TableAlign = 25,
  edit_Table_TableBorder_Left = 26,
  edit_Table_TableBorder_Top = 27,
  edit_Table_TableBorder_Right = 28,
  edit_Table_TableBorder_Bottom = 29,
  edit_Table_TableBorder_InsideH = 30,
  edit_Table_TableBorder_InsideV = 31,
  edit_Table_TableShd = 32,
  edit_Table_AddRow = 33,
  edit_Table_RemoveRow = 34,
  edit_Table_TableGrid = 35,
  edit_Table_TableLook = 36,
  edit_Table_TableStyleRowBandSize = 37,
  edit_Table_TableStyleColBandSize = 38,
  edit_Table_TableStyle = 39,
  edit_Table_Distance = 40,
  edit_Table_Pr = 41,
  edit_TableRow_Height = 42,
  edit_TableRow_AddCell = 43,
  edit_TableRow_RemoveCell = 44,
  edit_TableRow_Pr = 45,
  edit_TableCell_GridSpan = 46,
  edit_TableCell_Margins = 47,
  edit_TableCell_Shd = 48,
  edit_TableCell_VMerge = 49,
  edit_TableCell_Border_Left = 50,
  edit_TableCell_Border_Right = 51,
  edit_TableCell_Border_Top = 52,
  edit_TableCell_Border_Bottom = 53,
  edit_TableCell_VAlign = 54,
  edit_TableCell_Pr = 55,
  edit_TableCell_TextDirection = 56,
  edit_TextContent_Change = 59,
  edit_Comment_Change = 60,
  edit_Comment_TypeInfo = 61,
  edit_Comment_Position = 62,
  edit_Comments_Add = 63,
  edit_Comments_Remove = 64,

  edit_Shapes_SetLocks = 65,

  edit_Presentation_AddSlide = 66,
  edit_Presentation_RemoveSlide = 67,
  edit_Presentation_SlideSize = 68,
  edit_Presentation_AddSlideMaster = 69,
  edit_Presentation_RemoveSlideMaster = 70,
  edit_Presentation_UpdateSlideMaster = 71,
  edit_Presentation_ChangeTheme = 72,
  edit_Presentation_ChangeColorScheme = 73,
  edit_Presentation_SetShowPr = 74,
  edit_Presentation_SetDefaultTextStyle = 75,
  edit_Presentation_AddSection = 76,
  edit_Presentation_RemoveSection = 77,
  edit_Presentation_SetFirstSlideIndex = 78,

  edit_Xfrm_SetOffX = 79,
  edit_Xfrm_SetOffY = 80,
  edit_Xfrm_SetExtX = 81,
  edit_Xfrm_SetExtY = 82,
  edit_Xfrm_SetChOffX = 83,
  edit_Xfrm_SetChOffY = 84,
  edit_Xfrm_SetChExtX = 85,
  edit_Xfrm_SetChExtY = 86,
  edit_Xfrm_SetFlipH = 87,
  edit_Xfrm_SetFlipV = 88,
  edit_Xfrm_SetRot = 89,
  edit_Xfrm_SetParent = 90,
  edit_SpPr_SetBwMode = 91,
  edit_SpPr_SetXfrm = 92,
  edit_SpPr_SetGeometry = 93,
  edit_SpPr_SetFill = 94,
  edit_SpPr_SetLn = 95,
  edit_SpPr_SetParent = 96,

  edit_ShapeSetDeleted = 97,
  edit_ShapeSetNvSpPr = 98,
  edit_ShapeSetSpPr = 99,
  edit_ShapeSetStyle = 100,
  edit_ShapeSetTxBody = 101,
  edit_ShapeSetParent = 102,
  edit_ShapeSetGroup = 103,
  edit_ShapeSetBodyPr = 104,
  edit_GroupShapeSetNvGrpSpPr = 105,
  edit_GroupShapeSetSpPr = 106,
  edit_GroupShapeAddToSpTree = 107,
  edit_GroupShapeSetParent = 108,
  edit_GroupShapeSetGroup = 109,
  edit_GroupShapeRemoveFromSpTree = 110,
  edit_ImageShapeSetNvPicPr = 111,
  edit_ImageShapeSetSpPr = 112,
  edit_ImageShapeSetBlipFill = 113,
  edit_ImageShapeSetParent = 114,
  edit_ImageShapeSetGroup = 115,
  edit_ImageShapeSetStyle = 116,
  edit_ImageShapeSetData = 117,
  edit_ImageShapeSetApplicationId = 118,
  edit_ImageShapeSetPixSizes = 119,
  edit_ImageShapeSetObjectFile = 120,
  edit_GeometrySetParent = 121,
  edit_GeometryAddAdj = 122,
  edit_GeometrySetAdj = 123,
  edit_GeometryAddGuide = 124,
  edit_GeometryAddCnx = 125,
  edit_GeometryAddHandleXY = 126,
  edit_GeometryAddHandlePolar = 127,
  edit_GeometryAddPath = 128,
  edit_GeometryAddRect = 129,
  edit_GeometrySetPreset = 130,

  edit_TextBodySetBodyPr = 131,
  edit_TextBodySetLstStyle = 132,
  edit_TextBodySetContent = 133,
  edit_TextBodySetParent = 134,
  edit_Slide_SetComments = 135,
  edit_Slide_SetShow = 136,
  edit_SlideSetShowPhAnim = 137,
  edit_SlideSetShowMasterSp = 138,
  edit_Slide_SetLayout = 139,
  edit_SlideSetIndex = 140,
  edit_Slide_SetTiming = 141,
  edit_SlideSetSize = 142,
  edit_Slide_SetBg = 143,
  edit_SlideSetLocks = 144,
  edit_Slide_RemoveFromSpTree = 145,
  edit_Slide_AddToSpTree = 146,
  edit_SlideSetCSldName = 147,
  edit_SlideSetClrMapOverride = 148,
  edit_SlideSetNotes = 149,
  edit_SlideReplaceFromSpTree = 150,
  edit_Slide_SetAnimation = 151,
  edit_SlideLayoutSetMaster = 152,
  edit_SlideLayoutSetMatchingName = 153,
  edit_SlideLayoutSetType = 154,
  edit_SlideLayoutSetBg = 155,
  edit_SlideLayoutSetCSldName = 156,
  edit_SlideLayoutSetShowPhAnim = 158,
  edit_SlideLayoutSetShowMasterSp = 159,
  edit_SlideLayoutSetClrMapOverride = 160,
  edit_SlideLayoutAddToSpTree = 161,
  edit_SlideLayoutSetSize = 162,
  edit_SlideLayout_SetTransition = 163,
  edit_SlideMasterAddToSpTree = 164,
  edit_SlideMasterSetTheme = 165,
  edit_SlideMasterSetBg = 166,
  edit_SlideMasterSetTxStyles = 167,
  edit_SlideMasterSetCSldName = 168,
  edit_SlideMasterSetClrMapOverride = 169,
  edit_SlideMasterAddLayout = 170,
  edit_SlideMasterSetSize = 171,
  edit_SlideMaster_SetTransition = 172,
  edit_SlideComments_AddComment = 173,
  edit_SlideComments_RemoveComment = 174,

  edit_GraphicFrameSetSpPr = 175,
  edit_GraphicFrameSetGraphicObject = 176,
  edit_GraphicFrameSetSetNvSpPr = 177,
  edit_GraphicFrameSetSetParent = 178,
  edit_GraphicFrameSetSetGroup = 179,
  edit_NotesMasterSetHF = 180,
  edit_NotesMasterSetNotesStyle = 181,
  edit_NotesMasterSetNotesTheme = 182,
  edit_NotesMasterAddToSpTree = 183,
  edit_NotesMasterRemoveFromTree = 184,
  edit_NotesMasterSetBg = 185,
  edit_NotesMasterAddToNotesLst = 186,
  edit_NotesMasterSetName = 187,
  edit_NotesSetClrMap = 188,
  edit_NotesSetShowMasterPhAnim = 189,
  edit_NotesSetShowMasterSp = 190,
  edit_NotesAddToSpTree = 191,
  edit_NotesRemoveFromTree = 192,
  edit_NotesSetBg = 193,
  edit_NotesSetName = 194,
  edit_NotesSetSlide = 195,
  edit_NotesSetNotesMaster = 196,

  action_EditOnPresentation = 197,

  action_Slide_AddNewShape = 198,

  action_Document_ReplaceAll = 199,

  action_Document_ReplaceSingle = 200,

  action_Document_TableAddNewRow = 201,

  action_Document_SetTextLang = 202,

  action_Document_SetParagraphAlignByHotKey = 203,

  action_Document_PasteByHotKey = 204,

  action_Document_MoveTableBorder = 205,

  action_Slide_Remove = 206,

  action_SlideElements_EndAdjust = 207,

  action_SlideElements_EndControl = 208,

  action_SlideElements_CopyCtrl = 209,

  action_Presentation_ParaApply = 210,

  action_Presentation_ParaFormatPaste = 211,

  action_Presentation_AddNewParagraph = 212,

  action_Presentation_CreateGroup = 213,

  action_Presentation_UnGroup = 214,

  action_Presentation_ParagraphAdd = 215,

  action_Presentation_ParagraphClearFormatting = 216,

  action_Presentation_SetParagraphSpacing = 217,

  action_Presentation_SetParagraphTabs = 218,

  action_Presentation_SetParagraphIndent = 219,

  action_Presentation_SetParagraphNumbering = 220,

  action_Presentation_ParagraphIncDecFontSize = 221,

  action_Presentation_ParagraphIncDecIndent = 222,

  action_Presentation_SetImageProps = 223,

  action_Presentation_SetShapeProps = 224,

  action_Presentation_ChangeShapeType = 225,

  action_Presentation_SetVerticalAlign = 226,

  action_Presentation_HyperlinkAdd = 227,

  action_Presentation_HyperlinkModify = 228,

  action_Presentation_HyperlinkRemove = 229,

  action_Presentation_DisttributeHorizontal = 230,

  action_Presentation_DisttributeVertical = 231,

  action_Presentation_BringToFront = 232,

  action_Presentation_BringForward = 233,

  action_Presentation_SendToBack = 234,

  action_Presentation_BringBackward = 235,

  action_Presentation_ApplyTiming = 236,

  action_Presentation_MoveSlidesToEnd = 237,

  action_Presentation_MoveSlidesToNext = 238,

  action_Presentation_MoveSlidesToPrev = 239,

  action_Presentation_MoveSlidesToStart = 240,

  action_Presentation_AddImage = 241,

  action_Presentation_AddTable = 242,

  action_Presentation_ChangeBackground = 243,

  action_Presentation_AddNextSlide = 244,

  action_Presentation_ShiftSlides = 245,

  action_Presentation_DeleteSlides = 246,

  action_Presentation_ChangeLayout = 247,

  action_Presentation_ChangeSlideSize = 248,

  action_Presentation_ChangeColorScheme = 249,

  action_Presentation_AddComment = 250,

  action_Presentation_ChangeComment = 251,

  action_Presentation_SetTextPrAlign = 252,

  action_Presentation_SetPrIndent = 253,

  action_Presentation_SetPrIndentRight = 254,

  action_Presentation_SetPrFirstLineIndent = 255,

  action_Presentation_AddRowAbove = 256,

  action_Presentation_AddRowBelow = 257,

  action_Presentation_AddColLeft = 258,

  action_Presentation_AddColRight = 259,

  action_Presentation_RemoveRow = 260,

  action_Presentation_RemoveCol = 261,

  action_Presentation_RemoveTable = 262,

  action_Presentation_MergeCells = 263,

  action_Presentation_SplitCells = 264,

  action_Presentation_ApplyTable = 265,

  action_Presentation_RemoveComment = 266,

  action_Presentation_ChangeTheme = 267,

  action_Presentation_SetVert = 268,

  action_Document_MergeLetter = 270,

  action_Presentation_ApplyTimingToAll = 271,

  action_Document_CompositeInput = 272,

  action_Document_CompositeInputReplace = 273,

  action_Presentation_HideSlides = 274,

  action_Document_DistributeTableCells = 275,

  action_Presentation_DuplicateSlides = 276,

  action_Presentation_PreviewSlideTransition = 278,

  action_Presentation_ApplyAnimation = 279,

  action_Presentation_PreviewSlideAnimation = 280,

  action_Presentation_Crop = 281,

  action_SlideElements_EndMove = 282,

  action_SlideElements_EndRotate = 283,

  action_SlideElements_EndResize = 284,

  action_Presentation_ApplyAudio = 285,

  edit_Presentation_SetPresPr = 286,

  action_Paste_Assets = 287,

  edit_CxnSp_SetUniSpPr = 288,
}
