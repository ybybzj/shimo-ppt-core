import { Nullable, Optional } from '../../../liber/pervasive'
import { UploadUrlResult } from '../../editor/type'
import { AnimationSetting, FillType, SlideTransition } from '../../exports/type'
import {
  LineEndTypeValues,
  LineJoinTypeOO,
  PresetLineDashStyleValues,
  TextAnchorTypeOO,
  TextVerticalTypeOO,
} from '../../io/dataType/spAttrs'
import { ComputedRGBAColor } from '../color/ComputedRGBAColor'
import { AlphaModFix } from '../SlideElement/attrs/fill/Effects'
import { FillEffects } from '../SlideElement/attrs/fill/fill'
import { RelativeRect } from '../SlideElement/attrs/fill/RelativeRect'
import { Ln } from '../SlideElement/attrs/line'
import { UniMedia } from '../SlideElement/attrs/shapePrs'
import { FillBlipProp, FillGrad, FillHatch, FillSolidProp } from './props'
import { MarginPaddings } from './tableProp'

export type SlideOptions = {
  background?: ShapeFillOptions | null
  transition?: SlideTransition | null
  animation?: AnimationSetting[] | null
}

export type SlideElementOptions = {
  width?: Nullable<number>
  height?: Nullable<number>
  type?: Nullable<string>
  /** 设置属性时为 ShapeFillOptions, 读取属性时为 FillEffects*/
  fill?: Nullable<ShapeFillOptions | FillEffects>
  stroke?: Nullable<StrokeOptions | Ln>
  paddings?: Nullable<MarginPaddings>
  canFill?: boolean
  imageUrl?: Nullable<string>
  encryptImageUrl?: string
  attachment?: Optional<UploadUrlResult>
  isFromChart?: boolean
  isFromImage?: boolean
  locked?: boolean
  vert?: Nullable<TextVerticalTypeOO>
  verticalTextAlign?: Nullable<TextAnchorTypeOO>
  lockAspect?: boolean
  title?: string
  description?: string
  effects?: AlphaModFix[]
  columnCount?: Nullable<number>
  columnSpace?: Nullable<number>
  position?: Nullable<{ x?: Nullable<number>; y?: Nullable<number> }>
  srcRect?: RelativeRect
  rot?: Nullable<number>
  rotToIncr?: number
  flipH?: Nullable<boolean>
  flipV?: Nullable<boolean>
  InvertFlipH?: Nullable<boolean>
  InvertFlipV?: Nullable<boolean>
  isEmptyPH?: Nullable<boolean>
  subTypes?: Nullable<string[]>
  id?: Nullable<string>
  hyperlink?: Nullable<string>
  transparent?: Nullable<number>
  media?: Nullable<UniMedia>
}

export type ShapeFillOptions =
  | {
      type: FillType.none
      fill?: null
      transparent: number
    }
  | {
      type: FillType.noFill
      fill: null
      transparent?: null
    }
  | {
      type: FillType.solid
      fill: FillSolidProp
      transparent?: number
    }
  | {
      type: FillType.blip
      fill: FillBlipProp
      transparent?: number
    }
  | {
      type: FillType.gradient
      fill: FillGrad
      transparent?: null
    }
  | {
      type: FillType.pattern
      fill: FillHatch
      transparent?: null
    }
  | { type: FillType.grp; fill: null; transparent?: null }

export type HyperlinkOptions = {
  text?: Nullable<string>
  value?: Nullable<string>
  toolTip?: Nullable<string>
}

export type StrokeOptions = {
  type?: number
  width?: number
  color?: Nullable<ComputedRGBAColor>
  prstDash?: Nullable<PresetLineDashStyleValues>
  lineJoin?: LineJoinTypeOO | undefined
  lineCap?: number
  beginOfLineEndType?: Nullable<LineEndTypeValues>
  beginOfLineEndWidth?: Nullable<number>
  endOfLineEndType?: Nullable<LineEndTypeValues>
  endOflineEndWidth?: Nullable<number>
}
