import {
  FillBlipType,
  FillGradType,
  FillGradPathType,
} from '../common/const/attrs'
import { ComputedRGBAColor } from '../color/ComputedRGBAColor'
import { GradientDirections } from '../../exports/type'

/** @constructor */
export class FillBlipProp {
  type: number
  url: string
  textureId: any
  constructor() {
    this.type = FillBlipType.STRETCH
    this.url = ''
    this.textureId = null
  }

  getType() {
    return this.type
  }

  setType(v) {
    this.type = v
  }

  getUrl() {
    return this.url
  }

  setUrl(v) {
    this.url = v
  }

  getTextureId() {
    return this.textureId
  }

  setTextureId(v) {
    this.textureId = v
  }
}

/** @constructor */
export class FillHatch {
  patternType: any
  fgClr: any
  bgClr: any
  constructor() {
    this.patternType = undefined
    this.fgClr = undefined
    this.bgClr = undefined
  }

  setPatternType(v) {
    this.patternType = v
  }

  setColorFg(v) {
    this.fgClr = v
  }

  setColorBg(v) {
    this.bgClr = v
  }
}

export type GsList = Array<{
  color: ComputedRGBAColor
  pos: number
}>

/** @constructor */
export class FillGrad {
  gsLst: GsList
  gradType: (typeof FillGradType)[keyof typeof FillGradType]
  linearAngle?: number
  linearScale: boolean
  pathType?: (typeof FillGradPathType)[keyof typeof FillGradPathType]
  pathDirection?: GradientDirections

  constructor() {
    this.gsLst = []
    this.gradType = FillGradType.UNSET
    this.linearAngle = undefined
    this.linearScale = true
    this.pathType = undefined
  }

  getGsLst() {
    return this.gsLst
  }

  setGsLst(gsLst: GsList) {
    this.gsLst = gsLst
  }

  setGradType(v: (typeof FillGradType)[keyof typeof FillGradType]) {
    this.gradType = v
  }

  setLinearAngle(v: number) {
    this.linearAngle = v
  }

  setLinearScale(v: boolean) {
    this.linearScale = v
  }

  setPathType(v: (typeof FillGradPathType)[keyof typeof FillGradPathType]) {
    this.pathType = v
  }

  setPathDirection(dir: GradientDirections | undefined) {
    this.pathDirection = dir
  }
}

/** @constructor */
export class FillSolidProp {
  color: ComputedRGBAColor
  constructor() {
    this.color = new ComputedRGBAColor()
  }

  getColor() {
    return this.color
  }

  setColor(v) {
    this.color = v
  }
}

export class ErrorDataForTableOp {
  ['Value']: number
  constructor() {
    this['Value'] = 0
  }

  setValue(v) {
    this['Value'] = v
  }

  getValue() {
    return this['Value']
  }
}

export const ContextMenuTypes = {
  Main: 0,
  Thumbnails: 1,
} as const

export type ContextMenuData = {
  type: (typeof ContextMenuTypes)[keyof typeof ContextMenuTypes]
  absX: number
  absY: number
  isSlideSelected?: boolean
}
