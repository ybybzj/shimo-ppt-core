import { ShdType, ShdTypeValues, VertAlignJcValue } from '../common/const/attrs'
import { Nullable } from '../../../liber/pervasive'
import {
  createComputedRBGColorCustom,
  ComputedRGBAColor,
} from '../color/ComputedRGBAColor'
import { FillEffects } from '../SlideElement/attrs/fill/fill'
import { TableLookOptions } from '../Table/common'
import { CShd } from '../Table/attrs/CShd'
import {
  TableCellBorder,
  TableCellBorderOptions,
} from '../Table/attrs/CellBorder'
import { TableTextDirectionValues } from '../common/const/table'
export interface MarginPaddingsOptions {
  left?: Nullable<number>
  right?: Nullable<number>
  top?: Nullable<number>
  bottom?: Nullable<number>
}

export class MarginPaddings {
  left: Nullable<number>
  top: Nullable<number>
  bottom: Nullable<number>
  right: Nullable<number>
  constructor(obj?: MarginPaddingsOptions) {
    if (obj) {
      this.left = null == obj.left ? undefined : obj.left
      this.top = null == obj.top ? undefined : obj.top
      this.bottom = null == obj.bottom ? undefined : obj.bottom
      this.right = null == obj.right ? undefined : obj.right
    }
  }
}

// ---------------------------------------------------------------
// CBackground

export interface BackgroundInTableOptions {
  color?: Nullable<ComputedRGBAColor>
  value?: Nullable<ShdTypeValues>
  fillEffects?: FillEffects
}
// Color : { r : 0, g : 0, b : 0 }
export class BackgroundInTable {
  color: Nullable<ComputedRGBAColor>
  value: Nullable<ShdTypeValues>
  fillEffects?: FillEffects

  constructor(obj?: BackgroundInTableOptions) {
    if (obj != null) {
      this.color =
        null != obj.color
          ? createComputedRBGColorCustom(obj.color.r, obj.color.g, obj.color.b)
          : null

      if (obj.color?.mods != null && this.color != null) {
        this.color.mods = obj.color.mods.slice(0)
      }

      this.value = null != obj.value ? obj.value : ShdType.Clear
      this.fillEffects =
        obj.fillEffects != null ? obj.fillEffects.clone() : undefined
    } else {
      this.color = createComputedRBGColorCustom(0, 0, 0)
      this.value = ShdType.Nil
    }
  }

  static fromShd(shd: CShd): BackgroundInTable {
    const opts: BackgroundInTableOptions = {}
    opts.color =
      null != shd.color
        ? createComputedRBGColorCustom(shd.color.r, shd.color.g, shd.color.b)
        : undefined
    opts.value = shd.value
    opts.fillEffects = shd.fillEffects

    return new BackgroundInTable(opts)
  }
}

class TablePropLook {
  firstCol: boolean
  firstRow: boolean
  lastCol: boolean
  lastRow: boolean
  bandRow: boolean
  bandCol: boolean
  rtl: boolean
  constructor(obj?: TableLookOptions) {
    this.firstCol = false
    this.firstRow = false
    this.lastCol = false
    this.lastRow = false
    this.bandRow = false
    this.bandCol = false
    this.rtl = false

    if (obj) {
      this.firstCol = null == obj.firstCol ? false : obj.firstCol
      this.firstRow = null == obj.firstRow ? false : obj.firstRow
      this.lastCol = null == obj.lastCol ? false : obj.lastCol
      this.lastRow = null == obj.lastRow ? false : obj.lastRow
      this.bandRow = null == obj.bandRow ? false : obj.bandRow
      this.bandCol = null == obj.bandCol ? false : obj.bandCol
      this.rtl = null == obj.rtl ? false : obj.rtl
    }
  }
}

// ---------------------------------------------------------------

export interface BordersPropsOptions {
  left?: Nullable<TableCellBorderOptions>
  top?: Nullable<TableCellBorderOptions>
  right?: Nullable<TableCellBorderOptions>
  bottom?: Nullable<TableCellBorderOptions>
  insideH?: Nullable<TableCellBorderOptions>
  insideV?: Nullable<TableCellBorderOptions>
}

class BordersProps {
  left?: Nullable<TableCellBorder>
  top?: Nullable<TableCellBorder>
  right?: Nullable<TableCellBorder>
  bottom?: Nullable<TableCellBorder>
  insideH?: Nullable<TableCellBorder>
  insideV?: Nullable<TableCellBorder>
  constructor(opts?: BordersPropsOptions) {
    this.left = opts?.left ? TableCellBorder.new(opts.left) : undefined
    this.top = opts?.top ? TableCellBorder.new(opts.top) : undefined
    this.right = opts?.right ? TableCellBorder.new(opts.right) : undefined
    this.bottom = opts?.bottom ? TableCellBorder.new(opts.bottom) : undefined
    this.insideH = opts?.insideH ? TableCellBorder.new(opts.insideH) : undefined
    this.insideV = opts?.insideV ? TableCellBorder.new(opts.insideV) : undefined
  }
}
interface CellMarginsPropOptions {
  left?: Nullable<number>
  right?: Nullable<number>
  top?: Nullable<number>
  bottom?: Nullable<number>
  flag: Nullable<0 | 1 | 2>
}
class CellMarginsProp {
  left: Nullable<number>
  right: Nullable<number>
  top: Nullable<number>
  bottom: Nullable<number>
  flag: Nullable<0 | 1 | 2>
  constructor(obj?: CellMarginsPropOptions) {
    if (obj) {
      this.left = null != obj.left ? obj.left : null
      this.right = null != obj.right ? obj.right : null
      this.top = null != obj.top ? obj.top : null
      this.bottom = null != obj.bottom ? obj.bottom : null
      this.flag = null != obj.flag ? obj.flag : null
    } else {
      this.left = null
      this.right = null
      this.top = null
      this.bottom = null
      this.flag = null
    }
  }
}

interface PositionOfTable {
  x: Nullable<number>
  y: Nullable<number>
}
export interface TablePropOptions {
  isSelectCell?: boolean
  defaultMarginsOfTable?: Nullable<MarginPaddingsOptions>
  cellMargins?: Nullable<CellMarginsPropOptions>
  tablePaddings?: Nullable<MarginPaddingsOptions>
  tableBorders?: Nullable<BordersPropsOptions>
  cellBorders?: Nullable<BordersPropsOptions>
  tableBackground?: Nullable<BackgroundInTableOptions>
  cellsBackground?: Nullable<BackgroundInTableOptions>
  position?: Nullable<PositionOfTable>
  styleId?: Nullable<string>
  tableLook?: Nullable<TableLookOptions>
  cellsVertAlign?: Nullable<VertAlignJcValue>
  textDirectionInCell?: Nullable<TableTextDirectionValues>
  rowHeight?: Nullable<number>
  rtl?: boolean
}

export class TableProp {
  isSelectCell: boolean
  defaultMarginsOfTable: Nullable<MarginPaddings>
  cellMargins: Nullable<CellMarginsProp>
  tablePaddings: Nullable<MarginPaddings>
  tableBorders: Nullable<BordersProps>
  cellBorders: Nullable<BordersProps>
  tableBackground: Nullable<BackgroundInTable>
  cellsBackground: Nullable<BackgroundInTable>
  position: Nullable<PositionOfTable>
  styleId: Nullable<string>
  tableLook: Nullable<TablePropLook>
  cellsVertAlign: Nullable<VertAlignJcValue>
  textDirectionInCell: Nullable<TableTextDirectionValues>
  rowHeight: Nullable<number>
  rtl?: boolean
  constructor(tblProp?: Nullable<TablePropOptions>) {
    if (tblProp != null) {
      this.isSelectCell =
        null != tblProp.isSelectCell ? tblProp.isSelectCell : false

      this.defaultMarginsOfTable =
        null != tblProp.defaultMarginsOfTable
          ? new MarginPaddings(tblProp.defaultMarginsOfTable)
          : undefined

      this.cellMargins =
        null != tblProp.cellMargins
          ? new CellMarginsProp(tblProp.cellMargins)
          : undefined

      this.tablePaddings =
        null != tblProp.tablePaddings
          ? new MarginPaddings(tblProp.tablePaddings)
          : undefined

      this.tableBorders =
        null != tblProp.tableBorders
          ? new BordersProps(tblProp.tableBorders)
          : undefined

      this.cellBorders =
        null != tblProp.cellBorders
          ? new BordersProps(tblProp.cellBorders)
          : undefined

      this.tableBackground =
        null != tblProp.tableBackground
          ? new BackgroundInTable(tblProp.tableBackground)
          : undefined

      this.cellsBackground =
        null != tblProp.cellsBackground
          ? new BackgroundInTable(tblProp.cellsBackground)
          : undefined
      this.position = null != tblProp.position ? tblProp.position : undefined

      this.styleId = null != tblProp.styleId ? tblProp.styleId : undefined
      this.tableLook =
        null != tblProp.tableLook
          ? new TablePropLook(tblProp.tableLook)
          : undefined
      this.cellsVertAlign =
        null != tblProp.cellsVertAlign ? tblProp.cellsVertAlign : undefined
      this.textDirectionInCell = tblProp.textDirectionInCell

      this.rowHeight = tblProp.rowHeight
      this.rtl = tblProp.rtl
    } else {
      this.isSelectCell = false
    }
  }
}
