import { HyperlinkOptions } from './options'
export const MouseMoveDataTypes = {
  Common: 0,
  Hyperlink: 1,
  Rotate: 8,
} as const
export class MouseMoveData {
  type: (typeof MouseMoveDataTypes)[keyof typeof MouseMoveDataTypes]
  /** 相对于 mainContainer 的绝对 pix 坐标 */
  absX: number
  absY: number
  hyperlink?: HyperlinkOptions
  rotateInfo?:
    | {
        state: 'Hover'
      }
    | {
        state: 'Track'
        angle: string
      }
  constructor() {
    this.type = MouseMoveDataTypes.Common
    this.absX = 0
    this.absY = 0
  }
}
