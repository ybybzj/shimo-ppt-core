import { HoveredElementType } from '../common/const/attrs'

export type HoveredObjectInfo = {
  objectId: string
  /** Hover 对象起点, 相对于 slideRect 画布左上角, 单位为 mm */
  objectMMCoords: { x: number; y: number }
  /** Hover 对象起点, 相对于 sm-slide-viewport 主画布区左上角, 单位为 pixel */
  objectPixCoords: { x: number; y: number }
  /** 相对于浏览器, 若由外部触发的 Hover, 则不返回 */
  cursorCoords?: { x: number; y: number }
  type?: (typeof HoveredElementType)[keyof typeof HoveredElementType]
}
