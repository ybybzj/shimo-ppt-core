export interface RGBClr {
  r: number
  g: number
  b: number
}

export interface ColorRGBA {
  r: number
  g: number
  b: number
  a: number
}

export interface ColorHSL {
  h: number
  s: number
  l: number
}
