import { Nullable, valuesOfDict } from '../../../liber/pervasive'
import { ColorType } from '../common/const/attrs'
import {
  convertHexStringToRGB,
  convertRGBToHexString,
} from '../../common/utils/color'
import { InstanceType } from '../instanceTypes'
import {
  SysColorData,
  PrstColorData,
  RGBColorData,
  SchemeColorData,
  toSchemeColorType,
  fromSchemeColorType,
  ComplexColorData,
  ThemeColor,
  ThemeColorValue,
} from '../../io/dataType/spAttrs'
import { assignJSON, fromJSON } from '../../io/utils'
import { gDefaultColorMap, ClrMap } from './clrMap'
import { CHexColor } from './CHexColor'
import {
  PresetColorNameValueMap,
  PresetColorValues,
} from '../SlideElement/const'
import { Theme } from '../SlideElement/attrs/theme'
import { ColorMod, ColorModifiers, isTheSameMods } from './ColorModifiers'
import { ColorRGBA } from './type'
import { SlideLayout } from '../Slide/SlideLayout'
import { SlideMaster } from '../Slide/SlideMaster'
import { Slide } from '../Slide/Slide'
import { NotesMaster } from '../Slide/NotesMaster'
import { ComputedRGBAColor } from './ComputedRGBAColor'

export const defaultRGBAColor = (target?: ColorRGBA) => {
  if (target) {
    target.r = 0
    target.g = 0
    target.b = 0
    target.a = 255
    return target
  } else {
    return {
      r: 0,
      g: 0,
      b: 0,
      a: 255,
    }
  }
}

export function updateRGBA(
  target: ColorRGBA,
  by: Nullable<{
    r: number
    g: number
    b: number
    a?: number
  }>,
  updateA = true
): ColorRGBA {
  if (by != null) {
    target.r = by.r
    target.g = by.g
    target.b = by.b
    if (updateA === true && by.a != null) {
      target.a = by.a
    }
  }
  return target
}

interface SysColor {
  type: typeof ColorType.SYS
  id: string
  rgba: ColorRGBA
}
function newSysColor(id: string, c?: ColorRGBA): SysColor {
  const rgba = {
    r: c?.r ?? 0,
    g: c?.g ?? 0,
    b: c?.b ?? 0,
    a: c?.a ?? 255,
  }
  return {
    type: ColorType.SYS,
    id,
    rgba: rgba,
  }
}

export interface PrstColor {
  type: typeof ColorType.PRST
  id: Nullable<PresetColorValues>
}

function newPrstColor(id: Nullable<PresetColorValues>): PrstColor {
  return {
    type: ColorType.PRST,
    id,
  }
}

interface RGBAColor {
  type: typeof ColorType.SRGB
  rgba: ColorRGBA
}

function newRGBAColor(r: number, g: number, b: number, a?: number): RGBAColor {
  return {
    type: ColorType.SRGB,
    rgba: {
      r: r,
      g: g,
      b: b,
      a: a ?? 255,
    },
  }
}

export interface SchemeColor {
  type: typeof ColorType.SCHEME
  id: valuesOfDict<typeof ThemeColor>
}

function newSchemeColor(id: valuesOfDict<typeof ThemeColor>): SchemeColor {
  return {
    type: ColorType.SCHEME,
    id,
  }
}
export function updateRGBAByThemeColorId(
  themeColorId: ThemeColorValue,
  targetRGBA: ColorRGBA,
  theme: Nullable<Theme>,
  slide: Nullable<Slide>,
  layout: Nullable<SlideLayout>,
  master: Nullable<SlideMaster | NotesMaster>,
  rgba?: ColorRGBA,
  clrMap?: ClrMap
) {
  if (theme?.themeElements?.clrScheme != null) {
    if (themeColorId === ThemeColor.None) {
      updateRGBA(targetRGBA, rgba, false)
    } else {
      let mappings: Nullable<(number | null)[]>
      if (clrMap && clrMap.mappings) {
        mappings = clrMap.mappings
      } else if (slide != null && slide.clrMap != null) {
        mappings = slide.clrMap.mappings
      } else if (layout != null && layout.clrMap != null) {
        mappings = layout.clrMap.mappings
      } else if (master != null && master.clrMap != null) {
        mappings = master.clrMap.mappings
      } else {
        mappings = gDefaultColorMap.mappings
      }

      checkRGBAByThemeColorId(themeColorId, targetRGBA, theme, mappings)
    }
  }
}

function updateRGBABySchemeColor(
  c: SchemeColor,
  targetRGBA: ColorRGBA,
  theme: Nullable<Theme>,
  slide: Nullable<Slide>,
  layout: Nullable<SlideLayout>,
  master: Nullable<SlideMaster | NotesMaster>,
  rgba?: ColorRGBA,
  colorMap?: ClrMap
) {
  updateRGBAByThemeColorId(
    c.id,
    targetRGBA,
    theme,
    slide,
    layout,
    master,
    rgba,
    colorMap
  )
}

type ComplexColors = SysColor | PrstColor | RGBAColor | SchemeColor

function cloneCColor(c: ComplexColors) {
  switch (c.type) {
    case ColorType.SYS: {
      return newSysColor(c.id, c.rgba)
    }
    case ColorType.PRST: {
      return newPrstColor(c.id)
    }
    case ColorType.SCHEME: {
      return newSchemeColor(c.id)
    }
    case ColorType.SRGB: {
      const rgba = c.rgba
      return newRGBAColor(rgba.r, rgba.g, rgba.b, rgba.a)
    }
  }

  return c
}

function checkCColor(
  c: Nullable<ComplexColors>,
  theme: Nullable<Theme>,
  colorMap: Nullable<Array<number | null>>,
  targetRGBA: ColorRGBA
) {
  if (c == null) {
    return
  }
  switch (c.type) {
    case ColorType.SYS:
    case ColorType.SRGB: {
      const crgba = c.rgba
      targetRGBA.r = crgba.r
      targetRGBA.g = crgba.g
      targetRGBA.b = crgba.b
      return
    }
    case ColorType.PRST: {
      if (c.id != null) {
        const rgbNum = PresetColorNameValueMap[c.id]
        const r = (rgbNum >> 16) & 0xff
        const g = (rgbNum >> 8) & 0xff
        const b = rgbNum & 0xff
        targetRGBA.r = r
        targetRGBA.g = g
        targetRGBA.b = b
      }
      return
    }
    case ColorType.SCHEME: {
      checkRGBAByThemeColorId(c.id, targetRGBA, theme, colorMap)
      return
    }
  }
}

function checkRGBAByThemeColorId(
  themeColorId: ThemeColorValue,
  targetRGBA: ColorRGBA,
  theme: Nullable<Theme>,
  colorMap: Nullable<Array<number | null>>
) {
  if (theme == null) {
    return
  }
  const colors = theme.themeElements.clrScheme.colors

  const id = themeColorId

  if (colorMap && colorMap[id] != null && colors[colorMap[id]!] != null) {
    colors[colorMap[id]!]!.check(undefined, undefined, targetRGBA)
  } else if (colors[id] != null) {
    colors[id]!.check(undefined, undefined, targetRGBA)
  }
}

// toJSON
function sysColorToJSON(c: SysColor): SysColorData {
  return {
    ['type']: c.type,
    ['val']: c.id,
    ['lastClr']: convertRGBToHexString(c.rgba.r, c.rgba.g, c.rgba.b),
  }
}

function prstColorToJSON(c: PrstColor): PrstColorData {
  const data: PrstColorData = {
    ['type']: c.type,
  }
  if (c.id != null) {
    data['val'] = c.id
  }
  return data
}

function rgbColorToJSON(c: RGBAColor): RGBColorData {
  return {
    ['type']: c.type,
    ['val']: convertRGBToHexString(c.rgba.r, c.rgba.g, c.rgba.b),
  }
}

function schemeColorToJSON(c: SchemeColor): SchemeColorData {
  return {
    ['type']: c.type,
    ['val']: toSchemeColorType(c.id),
  }
}

// fromJSON
function sysColorFromJSON(data: SysColorData) {
  const id = data['val']
  let rgba
  if (data['lastClr']) {
    const rgb = convertHexStringToRGB(data['lastClr'])
    if (rgb) {
      rgba = {
        r: rgb.r,
        g: rgb.g,
        b: rgb.b,
        a: 255,
      }
    }
  }

  return id != null ? newSysColor(id, rgba) : undefined
}

function prstColorFromJSON(data: PrstColorData) {
  const id = data['val']
  return newPrstColor(id)
}

function rgbColorFromJSON(data: RGBColorData) {
  if (data['val']) {
    const rgb = convertHexStringToRGB(data['val'])
    if (rgb) {
      return newRGBAColor(rgb.r, rgb.g, rgb.b)
    }
  }
  return undefined
}

function schemeColorFromJSON(data: SchemeColorData) {
  // this.type = data['type']
  const id = fromSchemeColorType(data['val'])
  return newSchemeColor(id)
}

// isEqual
function isCColorsTheSame(
  c1: Nullable<ComplexColors>,
  c2: Nullable<ComplexColors>
): boolean {
  if (c1 == null && c2 == null) return true
  if (c1 == null || c2 == null) return false
  if (c1.type !== c2.type) return false

  const type = c1.type
  switch (type) {
    case ColorType.SYS: {
      return c1.id === (c2 as SysColor).id
    }
    case ColorType.PRST: {
      return c1.id === (c2 as PrstColor).id
    }
    case ColorType.SRGB: {
      const other = c2 as RGBAColor
      return (
        c1.rgba.r === other.rgba.r &&
        c1.rgba.g === other.rgba.g &&
        c1.rgba.b === other.rgba.b &&
        c1.rgba.a === other.rgba.a
      )
    }
    case ColorType.SCHEME: {
      return c1.id === (c2 as SchemeColor).id
    }
  }

  return false
}

export class CComplexColor {
  readonly instanceType = InstanceType.ComplexColor
  color?: ComplexColors
  mods?: ColorModifiers
  rgba: ColorRGBA
  static Sys(id: string, c?: ColorRGBA): CComplexColor {
    const ccolor = new CComplexColor()
    ccolor.color = newSysColor(id, c)
    return ccolor
  }
  static Prst(id: Nullable<PresetColorValues>): CComplexColor {
    const ccolor = new CComplexColor()
    ccolor.color = newPrstColor(id)
    return ccolor
  }
  static Scheme(id: valuesOfDict<typeof ThemeColor>): CComplexColor {
    const ccolor = new CComplexColor()
    ccolor.color = newSchemeColor(id)
    return ccolor
  }
  static RGBA(r: number, g: number, b: number, a?: number): CComplexColor {
    const ccolor = new CComplexColor()
    ccolor.color = newRGBAColor(r, g, b, a)
    return ccolor
  }

  static Computed(color: Nullable<ComputedRGBAColor>) {
    const ccolor = new CComplexColor()
    if (color) {
      ccolor.updateByComputedRGBAColor(color)
    }
    return ccolor
  }

  constructor() {
    this.rgba = defaultRGBAColor()
  }

  reset() {
    this.rgba.r = 0
    this.rgba.g = 0
    this.rgba.b = 0
    this.rgba.a = 255
    this.color = undefined
    this.mods = undefined
  }
  getColor() {
    return this.color
  }

  isEmpty() {
    return this.color == null
  }

  getColorMods() {
    return this.mods?.mods
  }

  updateColorForPh(ccolor: Nullable<CComplexColor>) {
    if (
      this.color &&
      this.color.type === ColorType.SCHEME &&
      this.color.id === ThemeColor.None
    ) {
      if (ccolor) {
        if (ccolor.color) {
          this.color = cloneCColor(ccolor.color)
        }
        if (ccolor.mods?.mods.length) {
          this.mods = ccolor.mods.clone()
        }
      }
    }
  }

  cloneMods(ccolor: CComplexColor) {
    if (this.mods) ccolor.mods = this.mods.clone()
    return ccolor
  }

  addColorMod(mod: ColorMod) {
    if (!this.mods) {
      this.mods = new ColorModifiers()
    }
    this.mods.addMod(mod)
  }

  updateAlphaMod(alpha: Nullable<number>) {
    if (!this.mods) {
      this.mods = new ColorModifiers()
    }
    this.mods.updateAlpha(alpha)
  }

  getAlphaFromMod() {
    return this.mods?.getAlpha()
  }
  check(
    theme?: Nullable<Theme>,
    colorMap?: Nullable<ClrMap>,
    targetRGBA?: ColorRGBA
  ) {
    const rgba = this.rgba
    if (this.color) {
      checkCColor(this.color, theme, colorMap?.mappings, rgba)
      if (this.mods) this.mods.apply(rgba)
    }
    if (targetRGBA) {
      updateRGBA(targetRGBA, rgba)
    }
  }

  validateMods() {
    if (this.mods) {
      this.mods.validateMods()
    }
  }

  toJSON(): ComplexColorData {
    const data: ComplexColorData = {}
    const color = this.color
    if (color) {
      switch (color.type) {
        case ColorType.SYS: {
          data['color'] = sysColorToJSON(color)
          break
        }
        case ColorType.PRST: {
          data['color'] = prstColorToJSON(color)
          break
        }
        case ColorType.SRGB: {
          data['color'] = rgbColorToJSON(color)
          break
        }
        case ColorType.SCHEME: {
          data['color'] = schemeColorToJSON(color)
          break
        }
      }
    }
    assignJSON(data, 'mods', this.mods)
    return data
  }

  fromJSON(data: ComplexColorData) {
    const colorData = data['color']
    if (colorData) {
      let color
      switch (colorData['type']) {
        case ColorType.SRGB: {
          color = rgbColorFromJSON(colorData)
          break
        }
        case ColorType.PRST: {
          color = prstColorFromJSON(colorData)
          break
        }
        case ColorType.SCHEME: {
          color = schemeColorFromJSON(colorData)
          break
        }
        case ColorType.SYS: {
          color = sysColorFromJSON(colorData)
          break
        }
      }
      if (color) {
        this.color = color
      }
    }

    if (data['mods']) {
      this.mods = new ColorModifiers()
      fromJSON(this.mods, data['mods'])
    } else {
      this.mods = undefined
    }
  }

  clone() {
    const duplicate = new CComplexColor()
    if (this.color != null) {
      duplicate.color = cloneCColor(this.color)
    }
    if (this.mods) duplicate.mods = this.mods.clone()

    duplicate.rgba.r = this.rgba.r
    duplicate.rgba.g = this.rgba.g
    duplicate.rgba.b = this.rgba.b
    duplicate.rgba.a = this.rgba.a
    return duplicate
  }

  sameAs(ccolor: CComplexColor) {
    if (ccolor == null) {
      return false
    }
    if (!isCColorsTheSame(this.color, ccolor.color)) {
      return false
    }
    if (!isTheSameMods(this.mods, ccolor.mods)) {
      return false
    }
    return true
  }

  updateBy(
    theme: Nullable<Theme>,
    slide: Nullable<Slide>,
    layout: Nullable<SlideLayout>,
    master: Nullable<SlideMaster | NotesMaster>,
    rgba?: ColorRGBA,
    colorMap?: ClrMap
  ) {
    if (this.color == null) return

    const colorType = this.color.type

    if (colorType === ColorType.SCHEME) {
      updateRGBABySchemeColor(
        this.color,
        this.rgba,
        theme,
        slide,
        layout,
        master,
        rgba,
        colorMap
      )
    } else {
      checkCColor(this.color, undefined, undefined, this.rgba)
    }

    if (this.mods) this.mods.apply(this.rgba)
  }

  intersect(ccolor: Nullable<CComplexColor>) {
    if (ccolor == null) {
      return undefined
    }
    const resultColor = new CComplexColor()
    if (this.color && isCColorsTheSame(this.color, ccolor.color)) {
      resultColor.color = cloneCColor(this.color)
      updateRGBA(resultColor.rgba, this.rgba, false)
    }

    if (this.mods && isTheSameMods(this.mods, ccolor.mods)) {
      resultColor.mods = this.mods.clone()
      resultColor.mods.apply(resultColor.rgba)
    }

    return resultColor
  }

  updateByComputedRGBAColor(computedRGBAColor: ComputedRGBAColor) {
    const ret = this

    const type = computedRGBAColor.type
    switch (type) {
      case ColorType.PRST: {
        if (ret.color == null || ret.color.type !== ColorType.PRST) {
          ret.color = newPrstColor(computedRGBAColor.value)
        }
        break
      }
      case ColorType.SCHEME: {
        // Here it is set ONLY from the menu. so:
        const index = parseInt(computedRGBAColor.value)
        if (isNaN(index)) break

        if (ret.color == null || ret.color.type !== ColorType.SCHEME) {
          ret.color = newSchemeColor(index as valuesOfDict<typeof ThemeColor>)
        }

        break
      }
      default: {
        if (ret.color == null || ret.color.type !== ColorType.SRGB) {
          ret.color = newRGBAColor(
            computedRGBAColor.r,
            computedRGBAColor.g,
            computedRGBAColor.b,
            computedRGBAColor.a
          )
        } else {
          const retRGBA = ret.color.rgba
          retRGBA.r = computedRGBAColor.r
          retRGBA.g = computedRGBAColor.g
          retRGBA.b = computedRGBAColor.b
          retRGBA.a = computedRGBAColor.a
        }
      }
    }
    if (ret.mods?.mods) {
      ret.mods.mods.length = 0
    }

    if (computedRGBAColor.mods && computedRGBAColor.mods.length) {
      computedRGBAColor.mods.forEach((mod) => {
        ret.addColorMod(mod)
      })
    }
    return ret
  }

  toComputedRGBAColor() {
    if (null == this.color) {
      return new ComputedRGBAColor()
    }

    const ret = new ComputedRGBAColor()
    ret.r = this.rgba.r
    ret.g = this.rgba.g
    ret.b = this.rgba.b
    ret.a = this.rgba.a

    const color = this.color
    switch (color.type) {
      case ColorType.SRGB:
      case ColorType.SYS: {
        break
      }
      case ColorType.PRST:
      case ColorType.SCHEME: {
        ret.type = color.type
        ret.value = color.id
        break
      }
      default:
        break
    }
    if (this.mods) {
      ret.mods = this.mods.clone().mods
    }
    return ret
  }

  getCSSColorString(transparent: Nullable<number>) {
    const css =
      'rgba(' +
      this.rgba.r +
      ',' +
      this.rgba.g +
      ',' +
      this.rgba.b +
      ',' +
      (transparent ?? this.rgba.a) / 255 +
      ')'
    return css
  }
}

export function complexColorToCHexColor(
  ccolor: CComplexColor,
  theme: Theme
): CHexColor {
  ccolor.check(theme, gDefaultColorMap)
  const rgba = ccolor.rgba
  const color = new CHexColor(rgba.r, rgba.g, rgba.b)

  return color
}

export function updateComplexColor(
  color: Nullable<ComputedRGBAColor>,
  ccolor: Nullable<CComplexColor>
) {
  if (null == color) return ccolor

  const ret = ccolor ?? new CComplexColor()
  ret.updateByComputedRGBAColor(color)

  return ret
}
