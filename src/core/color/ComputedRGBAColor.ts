import { Nullable } from '../../../liber/pervasive'
import { ColorType, ColorTypeValue } from '../common/const/attrs'
import { InstanceType } from '../instanceTypes'
import { ColorMod } from './ColorModifiers'
import { CComplexColor } from './complexColor'

export class ComputedRGBAColor {
  readonly instanceType = InstanceType.ComputedRGBColor
  type: ColorTypeValue
  r: number
  g: number
  b: number
  a: number
  auto: boolean
  value: any //Nullable<SchemeColor['id'] | PrstColor['id']>
  mods: ColorMod[]
  constructor() {
    this.type = ColorType.SRGB
    this.value = null
    this.r = 0
    this.g = 0
    this.b = 0
    this.a = 255

    this.auto = false
    this.mods = []
  }
}

export function createComputedRBGColorCustom(r, g, b, auto?) {
  const ret = new ComputedRGBAColor()
  ret.type = ColorType.SRGB
  ret.r = r
  ret.g = g
  ret.b = b
  ret.a = 255
  ret.auto = null == auto ? false : auto
  return ret
}

export function computedRGBColorFromComplexColor(
  ccolor: Nullable<CComplexColor>
) {
  if (null == ccolor) {
    return new ComputedRGBAColor()
  }
  return ccolor.toComputedRGBAColor()
}

export function computedRGBColorFromStr(clr: string) {
  clr = clr.replace(/#/, '')
  if (clr.length === 3) clr = clr.replace(/(.)/g, '$1$1')
  const color = parseInt(clr, 16)
  const c = new ComputedRGBAColor()
  c.type = ColorType.SRGB
  c.r = color >> 16
  c.g = (color & 0xff00) >> 8
  c.b = color & 0xff
  c.a = 0xff

  return c
}
