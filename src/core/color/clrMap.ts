import {
  ClrMapData,
  toSchemeColorType,
  ColorMappingIndex,
  fromSchemeColorType,
  ColorMappingOverride,
  ThemeColorCount,
} from '../../io/dataType/spAttrs'
import { readFromJSON, toJSON } from '../../io/utils'
import { isDict } from '../../common/utils'

export class ClrMap {
  mappings: Array<number | null>
  constructor() {
    this.mappings = []

    for (let i = 0; i < ThemeColorCount; i++) {
      this.mappings[i] = null
    }
  }

  clone() {
    const clrMap = new ClrMap()
    for (let i = 0, l = this.mappings.length; i < l; ++i) {
      clrMap.mappings[i] = this.mappings[i]
    }
    return clrMap
  }

  toJSON(): ClrMapData {
    const result: ClrMapData = {}
    const len = this.mappings.length
    for (let i = 0; i < len; i++) {
      const val = this.mappings[i]
      if (val != null) {
        const mappingIndex = toSchemeColorType(i)
        const clrSchemeIndex = toSchemeColorType(val)
        result[mappingIndex] = clrSchemeIndex
      }
    }
    return result
  }

  fromJSON(data: ClrMapData) {
    ColorMappingIndex.forEach((mappingIndex) => {
      const schemeIndex = data[mappingIndex]
      if (schemeIndex != null) {
        const index = fromSchemeColorType(mappingIndex)
        const val = fromSchemeColorType(schemeIndex)
        this.mappings[index] = val
      }
    })

    // 兼容初始化数据是数字的情况
    if (!this.mappings.find(Boolean)) {
      this.mappings = createDefaultColorMap().mappings
    }
  }
}

export function createDefaultColorMap() {
  const clrMap = new ClrMap()
  setDefaultColorMap(clrMap)
  return clrMap
}
export const gDefaultColorMap = createDefaultColorMap()

export function setDefaultColorMap(clrMap: ClrMap) {
  clrMap.mappings[0] = 0
  clrMap.mappings[1] = 1
  clrMap.mappings[2] = 2
  clrMap.mappings[3] = 3
  clrMap.mappings[4] = 4
  clrMap.mappings[5] = 5
  clrMap.mappings[10] = 10
  clrMap.mappings[11] = 11
  clrMap.mappings[6] = 12
  clrMap.mappings[7] = 13
  clrMap.mappings[15] = 8
  clrMap.mappings[16] = 9
}

export function assignClrMapOverride<K extends string>(
  data: { [k in K]?: ColorMappingOverride },
  key: K,
  clrMap: undefined | ClrMap
) {
  const clr_map = clrMap && toJSON(clrMap)
  if (clr_map) {
    data[key] = {
      type: 'override',
      data: clr_map,
    }
  }
}

export function fromClrMapOverride(
  clrMapData: ColorMappingOverride
): ClrMap | undefined {
  let clrMap
  if (clrMapData && clrMapData['type'] === 'useMaster') {
    clrMap = undefined
  } else if (clrMapData && clrMapData['type'] === 'override') {
    clrMap = readFromJSON(clrMapData.data as ClrMapData, ClrMap)
  } else if (isDict(clrMapData)) {
    clrMap = readFromJSON(clrMapData as unknown as ClrMapData, ClrMap)
  }

  return clrMap
}
