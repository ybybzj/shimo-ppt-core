import { ColorHSL, ColorRGBA } from './type'

const _1div6 = 1.0 / 6.0
const _1div3 = 1.0 / 3.0
const _2div3 = 2.0 / 3.0
export const hlsMaxVal = 255.0

export const toPct_GAMMA = 2.3
export const fromPct_GAMMA = 1.0 / toPct_GAMMA
export const MAX_PCT_VALUE = 100000

export function rgbToHsl(r: number, g: number, b: number, color: ColorHSL) {
  const minOfRGB = Math.min(r, g, b)
  const maxOfRGB = Math.max(r, g, b)
  const diffRGB = maxOfRGB - minOfRGB
  const dA = (maxOfRGB + minOfRGB) / 255.0
  let dD = diffRGB / 255.0
  let h = 0
  let s = 0
  let l = dA / 2.0

  if (diffRGB !== 0) {
    if (l < 0.5) s = dD / dA
    else s = dD / (2.0 - dA)

    dD = dD * 1530.0
    const dR = (maxOfRGB - r) / dD
    const dG = (maxOfRGB - g) / dD
    const dB = (maxOfRGB - b) / dD

    if (r === maxOfRGB) h = dB - dG
    else if (g === maxOfRGB) h = _1div3 + dR - dB
    else if (b === maxOfRGB) h = _2div3 + dG - dR

    if (h < 0.0) h += 1.0
    if (h > 1.0) h -= 1.0
  }

  h = ((h * hlsMaxVal) >> 0) & 0xff
  if (h < 0) h = 0
  if (h > 255) h = 255

  s = ((s * hlsMaxVal) >> 0) & 0xff
  if (s < 0) s = 0
  if (s > 255) s = 255

  l = ((l * hlsMaxVal) >> 0) & 0xff
  if (l < 0) l = 0
  if (l > 255) l = 255

  color.h = h
  color.s = s
  color.l = l
}

export function hslToRgb(hsl: ColorHSL, rgb: ColorRGBA) {
  if (hsl.s === 0) {
    rgb.r = hsl.l
    rgb.g = hsl.l
    rgb.b = hsl.l
  } else {
    const h = hsl.h / hlsMaxVal
    const s = hsl.s / hlsMaxVal
    const l = hsl.l / hlsMaxVal
    let v2 = 0
    if (l < 0.5) v2 = l * (1.0 + s)
    else v2 = l + s - s * l

    const v1 = 2.0 * l - v2

    let r = (255 * _ToRgbV(v1, v2, h + _1div3)) >> 0
    let g = (255 * _ToRgbV(v1, v2, h)) >> 0
    let b = (255 * _ToRgbV(v1, v2, h - _1div3)) >> 0

    if (r < 0) r = 0
    if (r > 255) r = 255

    if (g < 0) g = 0
    if (g > 255) g = 255

    if (b < 0) b = 0
    if (b > 255) b = 255

    rgb.r = r
    rgb.g = g
    rgb.b = b
  }
}

function _ToRgbV(v1: number, v2: number, vH: number) {
  if (vH < 0.0) vH += 1.0
  if (vH > 1.0) vH -= 1.0
  if (vH < _1div6) return v1 + (v2 - v1) * 6.0 * vH
  if (vH < 0.5) return v2
  if (vH < _2div3) return v1 + (v2 - v1) * (_2div3 - vH) * 6.0
  return v1
}

export function valToPct(value) {
  return (value * MAX_PCT_VALUE) / 255
}

export function pctToVal(value) {
  return (value * 255) / MAX_PCT_VALUE
}

function toValByGamma(valueOfComp, gamma) {
  return (
    (Math.pow(valueOfComp / MAX_PCT_VALUE, gamma) * MAX_PCT_VALUE + 0.5) >> 0
  )
}
export function toPctByGamma(v: number) {
  return toValByGamma(v, toPct_GAMMA)
}
export function fromPctByGamma(v: number) {
  return toValByGamma(v, fromPct_GAMMA)
}

export function toPctRGB(rgba: ColorRGBA) {
  rgba.r = toPctByGamma(valToPct(rgba.r))
  rgba.g = toPctByGamma(valToPct(rgba.g))
  rgba.b = toPctByGamma(valToPct(rgba.b))
}

export function fromPctRGB(rgba: ColorRGBA) {
  rgba.r = (pctToVal(fromPctByGamma(rgba.r)) + 0.5) >> 0
  rgba.g = (pctToVal(fromPctByGamma(rgba.g)) + 0.5) >> 0
  rgba.b = (pctToVal(fromPctByGamma(rgba.b)) + 0.5) >> 0
}

function _mixC(
  baseC: number,
  addedC: number,
  baseA: number,
  addedA: number,
  mixA: number
): number {
  return Math.round(
    (addedC * (addedA / 255)) / (mixA / 255) +
      (baseC * (baseA / 255) * (1 - addedA / 255)) / (mixA / 255)
  )
}

export function blendColors(...args: ColorRGBA[]): ColorRGBA {
  const base = { r: 0, g: 0, b: 0, a: 0 }
  let mixR = 0,
    mixG = 0,
    mixB = 0,
    mixA = 0
  let added: ColorRGBA | undefined
  while ((added = args.shift())) {
    if (added.a == null) {
      added.a = 255
    }
    // check if both alpha channels exist.
    if (base.a > 0 && added.a > 0) {
      // alpha
      mixA = 1 - (1 - added.a / 255) * (1 - base.a / 255)
      // red
      mixR = _mixC(base.r, added.r, base.a, added.a, mixA)
      // green
      mixG = _mixC(base.g, added.g, base.a, added.a, mixA)
      // blue
      mixB = _mixC(base.b, added.b, base.a, added.a, mixA)
    } else if (added.a > 0) {
      mixR = added.r
      mixG = added.g
      mixB = added.b
      mixA = added.a
    } else {
      mixR = base.r
      mixG = base.g
      mixB = base.b
      mixA = base.a
    }
    base.r = mixR
    base.g = mixG
    base.b = mixB
    base.a = mixA
  }

  return base
}
