import { each } from '../../../liber/l/each'
import { findIndex } from '../../../liber/l/findIndex'
import { Nullable } from '../../../liber/pervasive'
import { ColorModData } from '../../io/dataType/spAttrs'
import { ColorRGBA } from './type'
import {
  rgbToHsl,
  hslToRgb,
  toPctRGB,
  fromPctRGB,
  toPctByGamma,
  hlsMaxVal,
  MAX_PCT_VALUE,
  fromPctByGamma,
} from './utils'

export interface ColorMod {
  name: string
  val: number
}

export type ColorModifiersData = ColorModData[]

export class ColorModifiers {
  mods: ColorMod[]
  constructor() {
    this.mods = []
  }

  toJSON(): ColorModData[] {
    return this.mods.map((mod) => ({
      ['name']: mod.name,
      ['val']: mod.val,
    }))
  }

  fromJSON(data: ColorModData[]) {
    each((item) => {
      const name = item['name']
      const val = item['val']

      this.mods.push({
        val,
        name,
      })
    }, data)
  }

  addMod(mod: ColorMod) {
    this.mods.push(mod)
  }

  updateAlpha(alpha: Nullable<number>) {
    if (alpha != null) {
      const val = ((alpha * 100000) / 255) >> 0
      const idx = findIndex((mod) => mod.name === 'alpha', this.mods)
      if (idx > -1) {
        const alphaMod = this.mods[idx]
        alphaMod.val = val
      } else {
        this.addMod({
          name: 'alpha',
          val,
        })
      }
    }
  }
  getAlpha(): Nullable<number> {
    const idx = findIndex((mod) => mod.name === 'alpha', this.mods)
    if (idx > -1) {
      const alphaMod = this.mods[idx]
      const alpha = ((alphaMod.val * 255) / 100000) >> 0
      return alpha
    }
    return undefined
  }

  sameAs(mods: Nullable<ColorModifiers>) {
    if (mods == null) {
      return false
    }
    if (mods.mods == null || this.mods.length !== mods.mods.length) {
      return false
    }

    for (let i = 0; i < this.mods.length; ++i) {
      if (
        this.mods[i].name !== mods.mods[i].name ||
        this.mods[i].val !== mods.mods[i].val
      ) {
        return false
      }
    }
    return true
  }

  clone() {
    const duplicate = new ColorModifiers()
    for (let i = 0, l = this.mods.length; i < l; ++i) {
      const mod = this.mods[i]
      duplicate.mods[i] = { name: mod.name, val: mod.val }
    }
    return duplicate
  }

  validateMods() {
    if (isWordMods(this.mods)) {
      const { name, val: modVal } = this.mods[0]
      const val = ((modVal / 255) * 100000) >> 0

      this.mods.length = 0

      if (name === 'wordShade') {
        this.mods.push({
          name: 'lumMod',
          val,
        })
      } else {
        this.mods.push({
          name: 'lumMod',
          val: val,
        })

        this.mods.push({
          name: 'lumOff',
          val: 100000 - val,
        })
      }
    }
  }
  apply(rgba: ColorRGBA) {
    if (null == this.mods) return

    for (let i = 0, l = this.mods.length; i < l; i++) {
      const colorMod = this.mods[i]
      const val = colorMod.val / 100000.0

      switch (colorMod.name) {
        case 'alpha': {
          alpha(rgba, val)
          break
        }
        case 'blue': {
          blue(rgba, val)
          break
        }
        case 'blueMod': {
          blueMod(rgba, val)
          break
        }
        case 'blueOff': {
          blueOff(rgba, val)
          break
        }
        case 'green': {
          green(rgba, val)
          break
        }
        case 'greenMod': {
          greenMod(rgba, val)
          break
        }
        case 'greenOff': {
          greenOff(rgba, val)
          break
        }
        case 'red': {
          red(rgba, val)
          break
        }
        case 'redMod': {
          redMod(rgba, val)
          break
        }
        case 'redOff': {
          redOff(rgba, val)
          break
        }
        case 'hueMod': {
          hueMod(rgba, val)
          break
        }
        case 'hueOff': {
          hueOff(rgba, val)
          break
        }
        case 'inv': {
          inv(rgba)
          break
        }
        case 'lumMod': {
          lumMod(rgba, val)
          break
        }
        case 'lumOff': {
          lumOff(rgba, val)
          break
        }
        case 'satMod': {
          satMod(rgba, val)
          break
        }
        case 'satOff': {
          satOff(rgba, val)
          break
        }
        case 'wordShade': {
          wordShade(rgba, val, colorMod.val)
          break
        }
        case 'wordTint': {
          wordTint(rgba, val, colorMod.val)
          break
        }
        case 'shade': {
          shade(rgba, val)
          break
        }
        case 'tint': {
          tint(rgba, val)
          break
        }
        case 'gamma': {
          gamma(rgba)
          break
        }
        case 'invGamma': {
          invGamma(rgba)
          break
        }
      }
    }
  }
}
// helpers
function isWordMods(mods: ColorMod[]) {
  const modName = mods.length === 1 ? mods[0].name : null
  return modName === 'wordTint' || modName === 'wordShade'
}

//# region ---- modifiers -------- */
// type ColorModifer = (rgba: ColorRGBA, val: number, orgVal: number) => void

function _updateVal(val: number) {
  return Math.min(255, Math.max(0, 255 * val + 0.5)) >> 0
}

function _updateModVal(rgbaV: number, val: number) {
  return Math.max(0, (rgbaV * val + 0.5) >> 0)
}

function _updateOffVal(rgbaV: number, val: number) {
  return Math.max(0, rgbaV + val * 255 + 0.5) >> 0
}

function alpha(rgba: ColorRGBA, val: number) {
  rgba.a = _updateVal(val)
}
function blue(rgba: ColorRGBA, val: number) {
  rgba.b = _updateVal(val)
}
function blueMod(rgba: ColorRGBA, val: number) {
  rgba.b = _updateModVal(rgba.b, val)
}
function blueOff(rgba: ColorRGBA, val: number) {
  rgba.b = _updateOffVal(rgba.b, val)
}
function green(rgba: ColorRGBA, val: number) {
  rgba.g = _updateVal(val)
}
function greenMod(rgba: ColorRGBA, val: number) {
  rgba.g = _updateModVal(rgba.g, val)
}
function greenOff(rgba: ColorRGBA, val: number) {
  rgba.g = _updateOffVal(rgba.g, val)
}
function red(rgba: ColorRGBA, val: number) {
  rgba.r = _updateVal(val)
}
function redMod(rgba: ColorRGBA, val: number) {
  rgba.r = _updateModVal(rgba.r, val)
}
function redOff(rgba: ColorRGBA, val: number) {
  rgba.r = _updateOffVal(rgba.r, val)
}

function inv(rgba: ColorRGBA) {
  rgba.r ^= 0xff
  rgba.g ^= 0xff
  rgba.b ^= 0xff
}

function _hslVMod(hslV: number, val: number) {
  return hslV * val > hlsMaxVal
    ? hlsMaxVal
    : Math.max(0, (hslV * val + 0.5) >> 0)
}

function _hslVOff(hslV: number, val: number) {
  const res = (hslV + val * hlsMaxVal + 0.5) >> 0
  return Math.min(hlsMaxVal, Math.max(0, res))
}
function hueMod(rgab: ColorRGBA, val: number) {
  const hsl = { h: 0, s: 0, l: 0 }
  rgbToHsl(rgab.r, rgab.g, rgab.b, hsl)

  hsl.h = _hslVMod(hsl.h, val)

  hslToRgb(hsl, rgab)
}

function hueOff(rgba: ColorRGBA, val: number) {
  const hsl = { h: 0, s: 0, l: 0 }
  rgbToHsl(rgba.r, rgba.g, rgba.b, hsl)
  const res = (hsl.h + (val * 10.0) / 9.0 + 0.5) >> 0
  hsl.h = Math.min(hlsMaxVal, Math.max(0, res))

  hslToRgb(hsl, rgba)
}

function lumMod(rgba: ColorRGBA, val: number) {
  const hsl = { h: 0, s: 0, l: 0 }
  rgbToHsl(rgba.r, rgba.g, rgba.b, hsl)

  hsl.l = _hslVMod(hsl.l, val)
  hslToRgb(hsl, rgba)
}

function lumOff(rgba: ColorRGBA, val: number) {
  const hsl = { h: 0, s: 0, l: 0 }
  rgbToHsl(rgba.r, rgba.g, rgba.b, hsl)

  hsl.l = _hslVOff(hsl.l, val)

  hslToRgb(hsl, rgba)
}

function satMod(rgba: ColorRGBA, val: number) {
  const hsl = { h: 0, s: 0, l: 0 }
  rgbToHsl(rgba.r, rgba.g, rgba.b, hsl)

  hsl.s = _hslVMod(hsl.s, val)
  hslToRgb(hsl, rgba)
}

function satOff(rgba: ColorRGBA, val: number) {
  const hsl = { h: 0, s: 0, l: 0 }
  rgbToHsl(rgba.r, rgba.g, rgba.b, hsl)

  hsl.s = _hslVOff(hsl.s, val)

  hslToRgb(hsl, rgba)
}

function wordShade(rgba: ColorRGBA, _val: number, orgVal: number) {
  const val = orgVal / 255
  lumMod(rgba, val)
}

function wordTint(rgba: ColorRGBA, _val: number, orgVal: number) {
  const val = orgVal / 255
  const hsl = { h: 0, s: 0, l: 0 }
  rgbToHsl(rgba.r, rgba.g, rgba.b, hsl)

  const L_ = hsl.l * val + (255 - orgVal)
  if (L_ > hlsMaxVal) hsl.l = hlsMaxVal
  else hsl.l = Math.max(0, (L_ + 0.5) >> 0)
  hslToRgb(hsl, rgba)
}

function shade(rgba: ColorRGBA, val: number) {
  toPctRGB(rgba)
  if (val < 0) val = 0
  if (val > 1) val = 1
  rgba.r = rgba.r * val
  rgba.g = rgba.g * val
  rgba.b = rgba.b * val
  fromPctRGB(rgba)
}

function tint(rgba: ColorRGBA, val: number) {
  toPctRGB(rgba)
  if (val < 0) val = 0
  if (val > 1) val = 1
  rgba.r = MAX_PCT_VALUE - (MAX_PCT_VALUE - rgba.r) * val
  rgba.g = MAX_PCT_VALUE - (MAX_PCT_VALUE - rgba.g) * val
  rgba.b = MAX_PCT_VALUE - (MAX_PCT_VALUE - rgba.b) * val
  fromPctRGB(rgba)
}

function gamma(rgba: ColorRGBA) {
  toPctRGB(rgba)
  rgba.r = fromPctByGamma(rgba.r)
  rgba.g = fromPctByGamma(rgba.g)
  rgba.b = fromPctByGamma(rgba.b)
  fromPctRGB(rgba)
}

function invGamma(rgba: ColorRGBA) {
  toPctRGB(rgba)
  rgba.r = toPctByGamma(rgba.r)
  rgba.g = toPctByGamma(rgba.g)
  rgba.b = toPctByGamma(rgba.b)
  fromPctRGB(rgba)
}
//#regionend

export function isTheSameMods(
  m1: Nullable<ColorModifiers>,
  m2: Nullable<ColorModifiers>
): boolean {
  const isEmpty1 = m1 == null || m1.mods == null || m1.mods.length <= 0
  const isEmpty2 = m2 == null || m2.mods == null || m2.mods.length <= 0
  if (isEmpty1 && isEmpty2) return true
  if (isEmpty1 || isEmpty2) return false
  return m1.sameAs(m2)
}
