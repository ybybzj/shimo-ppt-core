import { assignVal } from '../../io/utils'
import { CHexColorData } from '../../io/dataType/style'
import { InstanceType } from '../instanceTypes'
export interface CHexColorOptions {
  r: number
  g: number
  b: number
  auto?: boolean
}
export class CHexColor {
  readonly instanceType = InstanceType.HexColor
  r: number
  g: number
  b: number
  auto: boolean
  constructor(r: number, g: number, b: number, auto?: boolean) {
    this.r = r
    this.g = g
    this.b = b

    this.auto = auto == null ? false : auto
  }

  clone() {
    return new CHexColor(this.r, this.g, this.b, this.auto)
  }

  toJSON(): CHexColorData {
    const data: CHexColorData = {
      ['auto']: this.auto,
    }
    assignVal(data, 'r', this.r)
    assignVal(data, 'g', this.g)
    assignVal(data, 'b', this.b)
    return data
  }

  fromJSON(data: CHexColorData) {
    this.r = data['r'] ?? 0
    this.g = data['g'] ?? 0
    this.b = data['b'] ?? 0
    this.auto = data['auto']
  }

  set(
    r: number | undefined,
    g: number | undefined,
    b: number | undefined,
    auto?: boolean
  ) {
    this.r = r ?? 0
    this.g = g ?? 0
    this.b = b ?? 0
    this.auto = auto == null ? false : auto
  }

  sameAs(color: CHexColorOptions) {
    if (
      color == null ||
      this.r !== color.r ||
      this.g !== color.g ||
      this.b !== color.b ||
      this.auto !== color.auto
    ) {
      return false
    }

    return true
  }

  isDark() {
    if (0.5 * this.r + this.g + 0.195 * this.b < 103) return false

    return true
  }
}
