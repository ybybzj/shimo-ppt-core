export const HightLight_None = -1

export const VertAlignFactor = { Size: 0.65, Super: 0.35, Sub: -0.141 } as const
export const smallcaps_Factor = 0.8

export const smallcaps_and_script_factor =
  VertAlignFactor.Size * smallcaps_Factor
export const gDPI = 96.0

export const Factor_mm_to_pix = gDPI / 25.4
export const Factor_pix_to_mm = 25.4 / gDPI

export const Factor_pt_to_mm = 25.4 / 72
export const Factor_pc_to_mm = Factor_pt_to_mm / 12
export const Factor_in_to_mm = 25.4
export const Factor_twips_to_mm = Factor_pt_to_mm / 20
export const Factor_mm_to_pt = 1 / Factor_pt_to_mm
export const Factor_mm_to_twips = 1 / Factor_twips_to_mm

export const ScaleOfPPTXSizes = 36000

export const THRESHOLD_DELTA = 1 / 100000
