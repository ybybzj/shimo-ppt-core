export const Max_ZoomValue = 400
const Min_ZoomValue = 20
const ZOOM_STEPS: number[] = (() => {
  const START = Min_ZoomValue
  const END = Max_ZoomValue
  const STEP = 10
  const res: number[] = [START]
  let next = res[res.length - 1] + STEP
  while (next < END) {
    res.push(next)
    next = next + STEP
  }

  res.push(END)
  return res
})()

export function getZoomOutValue(zoomValue: number, byWheel = false) {
  if (byWheel === true) {
    if (zoomValue > Min_ZoomValue) {
      zoomValue -= 5
    }
  } else {
    for (let i = ZOOM_STEPS.length - 1; i >= 0; i--) {
      if (zoomValue > ZOOM_STEPS[i]) {
        zoomValue = ZOOM_STEPS[i]
        break
      }
    }
  }

  return zoomValue
}

export function getZoomInValue(zoomValue: number, byWheel = false) {
  if (byWheel === true) {
    if (zoomValue < Max_ZoomValue) {
      zoomValue += 5
    }
  } else {
    for (let i = 0, l = ZOOM_STEPS.length; i < l; i++) {
      if (zoomValue < ZOOM_STEPS[i]) {
        zoomValue = ZOOM_STEPS[i]
        break
      }
    }
  }

  return zoomValue
}
