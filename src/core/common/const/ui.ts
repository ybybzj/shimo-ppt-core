import { ABS_DIRECTION } from './attrs'

export { ABS_DIRECTION }
export const CursorTypesByAbsDirection = {
  [ABS_DIRECTION.N]: 'n-resize',
  [ABS_DIRECTION.NE]: 'ne-resize',
  [ABS_DIRECTION.E]: 'e-resize',
  [ABS_DIRECTION.SE]: 'se-resize',
  [ABS_DIRECTION.S]: 's-resize',
  [ABS_DIRECTION.SW]: 'sw-resize',
  [ABS_DIRECTION.W]: 'w-resize',
  [ABS_DIRECTION.NW]: 'nw-resize',
} as const

// Todo, Move ABS_DIRECTION.XX define here

/** 相对于 target 的方向, 例如左上角触点，初始 0，垂直翻转后为 6(LT 变为 LB) */
export const REL_DIRECTION = {
  LT: 0,
  T: 1,
  RT: 2,
  R: 3,
  RB: 4,
  B: 5,
  LB: 6,
  L: 7,
} as const

export type RelativeDirections =
  (typeof REL_DIRECTION)[keyof typeof REL_DIRECTION]

export const LINE_DIRECTION = {
  START: 0,
  END: 4,
}

export type AbsoluteDirections =
  (typeof ABS_DIRECTION)[keyof typeof ABS_DIRECTION]

export const DIRECTION_UNHIT = -1 as const
export const DIRECTION_ROT = 8

export type DirectionValue =
  | typeof DIRECTION_UNHIT
  | RelativeDirections
  | typeof DIRECTION_ROT
export const EditControlType = {
  SHAPE: 1,
  TEXT: 2,
  CROP: 3,
  HoveringOutline: 4,
  ViewMod: 5,
} as const

export const GroupShapeRenderingMargin = 4
