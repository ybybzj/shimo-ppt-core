export const TextTabAlign = {
  Clear: 0x00,
  Left: 0x01,
  Right: 0x02,
  Center: 0x03,
} as const

export const ParaLineInfo = {
  IsEmpty: 0x0002, // 00000010
  HasParaEnd: 0x0004, // 00000100
  HasArabic: 0x0008, // 00001000
  HasTextOnLine: 0x0080, // 10000000
} as const
export type ParaLineInfoValues =
  (typeof ParaLineInfo)[keyof typeof ParaLineInfo]

export const TextContentDimensionLimit = {
  x: 0,
  y: 0,
  xLimit: 2000,
  yLimit: 2000,
} as const
