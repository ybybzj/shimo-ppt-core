import { valuesOfDict } from '../../../../liber/pervasive'

export const MaxColumnWidth = 255
export const MaxRowHeight = 409.5
export const MaxParaRunContentLength = 256

export const SelectedElementType = {
  Paragraph: 0,
  Table: 1,
  Image: 2,
  Header: 3,
  Hyperlink: 4,
  // SpellCheck: 5,
  Shape: 6,
  Slide: 7,
  Chart: 8,
  Math: 9,
  Presentation: 10,
} as const

export const HoveredElementType = {
  Shape: 0,
  ConnectionShape: 1,
  Image: 2,
  OleImage: 3,
  GroupeShape: 4,
  Table: 5,
  Comment: 6,
} as const

export const LineDrawingRule = {
  Left: 0,
  Center: 1,
  Right: 2,
  Top: 0,
  Bottom: 2,
} as const

export const AlignKind = {
  Right: 0,
  Left: 1,
  Center: 2,
  Justify: 3,
  Dist: 4,
} as const
export type AlignKindValue = (typeof AlignKind)[keyof typeof AlignKind]
export const LineHeightRule = {
  AtLeast: 0x00,
  Auto: 0x01,
  Exact: 0x02,
} as const

export const ShdType = { Clear: 0, Nil: 1 } as const
export type ShdTypeValues = valuesOfDict<typeof ShdType>

export const VertAlignType = {
  Baseline: 0,
  SuperScript: 1,
  SubScript: 2,
} as const

export const ColorType = {
  NONE: 0,
  SRGB: 1,
  PRST: 2,
  SCHEME: 3,
  SYS: 4,
} as const
export type ColorTypeValue = (typeof ColorType)[keyof typeof ColorType]

export const FillKIND = {
  NONE: 0,
  BLIP: 1,
  NOFILL: 2,
  SOLID: 3,
  GRAD: 4,
  PATT: 5,
  GRP: 6,
} as const

export const FillGradType = {
  UNSET: 0,
  LINEAR: 1,
  PATH: 2,
} as const

export const FillGradPathType = {
  Circle: 'circle',
  Rect: 'rect',
  Shape: 'shape',
} as const

export const FillBlipType = {
  STRETCH: 1,
  TILE: 2,
} as const
export const StrokeType = {
  NONE: 0,
  COLOR: 1,
} as const

export const LineJoinType = {
  Round: 1,
  Bevel: 2,
  Miter: 3,
} as const

export const DashStyle = {
  dash: 0,
  dashDot: 1,
  dot: 2,
  lgDash: 3,
  lgDashDot: 4,
  lgDashDotDot: 5,
  solid: 6,
  sysDash: 7,
  sysDashDot: 8,
  sysDashDotDot: 9,
  sysDot: 10,
} as const

export const ObjectsAlignType = {
  Selected: 0,
  Slide: 1,
  Page: 2,
  Margin: 3,
} as const

export type ObjectsAlignTypeValues = valuesOfDict<typeof ObjectsAlignType>

export const phSZType = {
  full: 0,
  half: 1,
  quarter: 2,
} as const

export const bodyPrAnchor = {
  b: 0,
  t: 4,
  ctr: 1,
  just: 3,
  dist: 2,
} as const

export const BWModeKind = {
  auto: 0,
  clr: 1,
  gray: 2,
  ltGray: 3,
  invGray: 4,
  grayWhite: 5,
  blackGray: 6,
  blackWhite: 7,
  black: 8,
  white: 9,
  hidden: 10,
} as const

export const VertAlignJc = {
  Top: 0,
  Center: 1,
  Bottom: 2,
} as const

export type VertAlignJcValue = valuesOfDict<typeof VertAlignJc>

export const PhButtonType = {
  Image: 0,
  ImageUrl: 1,
  Chart: 2,
  Table: 3,
  Video: 4,
  Audio: 5,
}

export const PhButtonState = {
  None: 0,
  Active: 1,
  Over: 2,
}

export enum MediaType {
  video = 7,
  audio = 8,
}

export const ABS_DIRECTION = {
  N: 0,
  NE: 1,
  E: 2,
  SE: 3,
  S: 4,
  SW: 5,
  W: 6,
  NW: 7,
} as const
// hatch pattern

export const PresetPatternValToNumMap = {
  ['cross']: 0,
  ['dashDnDiag']: 1,
  ['dashHorz']: 2,
  ['dashUpDiag']: 3,
  ['dashVert']: 4,
  ['diagBrick']: 5,
  ['diagCross']: 6,
  ['divot']: 7,
  ['dkDnDiag']: 8,
  ['dkHorz']: 9,
  ['dkUpDiag']: 10,
  ['dkVert']: 11,
  ['dnDiag']: 12,
  ['dotDmnd']: 13,
  ['dotGrid']: 14,
  ['horz']: 15,
  ['horzBrick']: 16,
  ['lgCheck']: 17,
  ['lgConfetti']: 18,
  ['lgGrid']: 19,
  ['ltDnDiag']: 20,
  ['ltHorz']: 21,
  ['ltUpDiag']: 22,
  ['ltVert']: 23,
  ['narHorz']: 24,
  ['narVert']: 25,
  ['openDmnd']: 26,
  ['pct10']: 27,
  ['pct20']: 28,
  ['pct25']: 29,
  ['pct30']: 30,
  ['pct40']: 31,
  ['pct5']: 32,
  ['pct50']: 33,
  ['pct60']: 34,
  ['pct70']: 35,
  ['pct75']: 36,
  ['pct80']: 37,
  ['pct90']: 38,
  ['plaid']: 39,
  ['shingle']: 40,
  ['smCheck']: 41,
  ['smConfetti']: 42,
  ['smGrid']: 43,
  ['solidDmnd']: 44,
  ['sphere']: 45,
  ['trellis']: 46,
  ['upDiag']: 47,
  ['vert']: 48,
  ['wave']: 49,
  ['wdDnDiag']: 50,
  ['wdUpDiag']: 51,
  ['weave']: 52,
  ['zigZag']: 53,
} as const

export const PresetPatternNumToValMap = [
  'cross',
  'dashDnDiag',
  'dashHorz',
  'dashUpDiag',
  'dashVert',
  'diagBrick',
  'diagCross',
  'divot',
  'dkDnDiag',
  'dkHorz',
  'dkUpDiag',
  'dkVert',
  'dnDiag',
  'dotDmnd',
  'dotGrid',
  'horz',
  'horzBrick',
  'lgCheck',
  'lgConfetti',
  'lgGrid',
  'ltDnDiag',
  'ltHorz',
  'ltUpDiag',
  'ltVert',
  'narHorz',
  'narVert',
  'openDmnd',
  'pct10',
  'pct20',
  'pct25',
  'pct30',
  'pct40',
  'pct5',
  'pct50',
  'pct60',
  'pct70',
  'pct75',
  'pct80',
  'pct90',
  'plaid',
  'shingle',
  'smCheck',
  'smConfetti',
  'smGrid',
  'solidDmnd',
  'sphere',
  'trellis',
  'upDiag',
  'vert',
  'wave',
  'wdDnDiag',
  'wdUpDiag',
  'weave',
  'zigZag',
] as const
