/* eslint-disable @typescript-eslint/naming-convention */
export const SearchType = {
  Common: 0x0000,
  Header: 0x0100,
  Footor: 0x0200,
} as const

export const ParaCalcStatus = {
  End: 0x00,
  NextElement: 0x01,
  NextColumn: 0x08,
  NextLine: 0x10,
} as const

export type ParaCalcStatusValues =
  (typeof ParaCalcStatus)[keyof typeof ParaCalcStatus]

export const MaxWidthLimitForTextWarpNone = 100000
