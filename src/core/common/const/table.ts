import { valuesOfDict } from '../../../../liber/pervasive'

export const TableSelectionType = {
  TABLE: 0,
  CELL: 1,
  CELLS: 2,
  TEXT: 3,
  COMMON: 4,
  BORDER: 5,
  ROWS: 6,
  COLUMNS: 7,
} as const

export type TableSelectionTypeValues = valuesOfDict<typeof TableSelectionType>

export const TableLengthUnitType = { AUTO: 0, MM: 1 } as const
export type TableLengthUnitTypeValues = valuesOfDict<typeof TableLengthUnitType>

export const TCBorderValue = { None: 0, Single: 1 } as const
export type TCBorderValues = valuesOfDict<typeof TCBorderValue>

export const VMergeType = { Restart: 1, Continue: 2 } as const
export type VMergeTypeValues = valuesOfDict<typeof VMergeType>

export const TableTextDirection = {
  LRTB: 0,
  TBRL: 1,
  BTLR: 2,
  LRTBV: 3,
  TBRLV: 4,
  TBLRV: 5,
} as const

export type TableTextDirectionValues = valuesOfDict<typeof TableTextDirection>

export const CellTextDirection = {
  LRTB: 0x00,
  TBRL: 0x01,
  BTLR: 0x02,
} as const
