export const NumberingFormat = {
  None: 0,
  Char: 1,
  arabicPeriod: 100, // 1., 2., 3., ...
  arabicParenR: 101, // 1), 2), 3), ...
  romanUcPeriod: 102, // I., II., III., ...
  romanLcPeriod: 103, // i., ii., iii., ...
  alphaLcParenR: 104, // a), b), c), ...
  alphaLcPeriod: 105, // a., b., c.,
  alphaUcParenR: 106, // A), B), C), ...
  alphaUcPeriod: 107, // A., B., C., ...
  arabicParenBoth: 108, // (1), (2), (3), ...
  ea1ChsPeriod: 109,
  chineseNumberParenBoth: 110,
} as const

export const NumberingFormatArrMapping = [
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcPeriod,
  NumberingFormat.alphaUcParenR,
  NumberingFormat.alphaUcParenR,
  NumberingFormat.alphaUcPeriod,
  NumberingFormat.arabicPeriod,
  NumberingFormat.arabicPeriod,
  NumberingFormat.arabicPeriod,
  NumberingFormat.arabicPeriod,
  NumberingFormat.arabicParenR,
  NumberingFormat.arabicParenR,
  NumberingFormat.arabicPeriod,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.romanLcPeriod,
  NumberingFormat.romanLcPeriod,
  NumberingFormat.romanLcPeriod,
  NumberingFormat.romanUcPeriod,
  NumberingFormat.romanUcPeriod,
  NumberingFormat.romanUcPeriod,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcParenR,
  NumberingFormat.alphaLcPeriod,
  NumberingFormat.arabicParenBoth,
  NumberingFormat.ea1ChsPeriod,
  NumberingFormat.chineseNumberParenBoth,
] as const
//  1 -> a
//  2 -> b
//   ...
// 26 -> z
// 27 -> aa
//   ...
// 52 -> zz
// 53 -> aaa
//   ...
export function numberToAlpha(n: number, isLowerCase: boolean) {
  n = n - 1
  const count = (n - (n % 26)) / 26
  const ost = n % 26

  let t = ''

  let letter: string
  if (true === isLowerCase) {
    letter = String.fromCodePoint(ost + 97)
  } else {
    letter = String.fromCodePoint(ost + 65)
  }

  for (let i = 0; i < count + 1; i++) {
    t += letter
  }

  return t
}

export function numberToString(n: number) {
  return '' + n
}

//    1 -> i
//    4 -> iv
//    5 -> v
//    9 -> ix
//   10 -> x
//   40 -> xl
//   50 -> l
//   90 -> xc
//  100 -> c
//  400 -> cd
//  500 -> d
//  900 -> cm
// 1000 -> m
export function numberToRoman(n: number, isLowerCase: boolean) {
  let rims: string[]

  if (true === isLowerCase) {
    rims = [
      'm',
      'cm',
      'd',
      'cd',
      'c',
      'xc',
      'l',
      'xl',
      'x',
      'ix',
      'v',
      'iv',
      'i',
      ' ',
    ]
  } else {
    rims = [
      'M',
      'CM',
      'D',
      'CD',
      'C',
      'XC',
      'L',
      'XL',
      'X',
      'IX',
      'V',
      'IV',
      'I',
      ' ',
    ]
  }

  const vals = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1, 0]

  let t = ''
  let i = 0
  while (n > 0) {
    while (vals[i] <= n) {
      t += rims[i]
      n -= vals[i]
    }

    i++

    if (i >= rims.length) {
      break
    }
  }

  return t
}

export function numberToChinese(n: number, lowerCase = true): string {
  const confs = {
    lower: {
      nums: ['〇', '一', '二', '三', '四', '五', '六', '七', '八', '九'],
      unit: ['', '十', '百', '千', '万'],
      level: ['', '万', '亿'],
    },
    upper: {
      nums: ['零', '壹', '贰', '叁', '肆', '伍', '陆', '柒', '捌', '玖'],
      unit: ['', '拾', '佰', '仟', '万'],
      level: ['', '万', '亿'],
    },
  }

  const conf = lowerCase ? confs.lower : confs.upper
  const zero = conf.nums[0]
  const numbers = String(Number(n).toFixed(2)).split('.')
  const integer = numbers[0].split('')
  const levels = integer.reverse().reduce((pre: any, item: any, idx) => {
    const level = pre[0] && pre[0].length < 4 ? pre[0] : []
    const value =
      item === '0' ? conf.nums[item] : conf.nums[item] + conf.unit[idx % 4]
    level.unshift(value)

    if (level.length === 1) {
      pre.unshift(level)
    } else {
      pre[0] = level
    }

    return pre
  }, [])

  return levels.reduce((pre: string, item: any, idx: number) => {
    let level = conf.level[levels.length - idx - 1]
    item = item.join('').replace(/(零|〇)\1+/g, '$1')
    if (n === 0) {
      item = zero
      level = ''
    } else if (item[item.length - 1] === zero) {
      item = item.slice(0, item.length - 1)
    }
    return pre + item + level
  }, '')
}

export const NumberingFormatToNumberMap = {
  ['arabicPeriod']: 6, // NumberingFormat.arabicPeriod
  ['arabicParenR']: 10, // NumberingFormat.arabicParenR
  ['romanUcPeriod']: 32, // NumberingFormat.romanUcPeriod
  ['romanLcPeriod']: 29, // NumberingFormat.romanLcPeriod
  ['alphaLcParenR']: 0, // NumberingFormat.alphaLcParenR
  ['alphaLcPeriod']: 2, // NumberingFormat.alphaLcPeriod
  ['alphaUcParenR']: 3, // NumberingFormat.alphaUcParenR
  ['alphaUcPeriod']: 5, // NumberingFormat.alphaUcPeriod
  ['arabicParenBoth']: 41, // NumberingFormat.arabicParenBoth
  ['ea1ChsPeriod']: 42, // NumberingFormat.ea1ChsPeriod
  ['ea1ChsPlain']: 43, // NumberingFormat.chineseNumberParenBoth
}

export function strToNumberingFormatValue(s: string): number | undefined {
  return NumberingFormatToNumberMap[s]
}

export function numberingFormatValueToStr(n: number): string | undefined {
  const schemeN = NumberingFormatArrMapping[n]
  switch (schemeN) {
    case NumberingFormat.arabicPeriod:
      return 'arabicPeriod'
    case NumberingFormat.arabicParenR:
      return 'arabicParenR'
    case NumberingFormat.romanUcPeriod:
      return 'romanUcPeriod'
    case NumberingFormat.romanLcPeriod:
      return 'romanLcPeriod'
    case NumberingFormat.alphaLcParenR:
      return 'alphaLcParenR'
    case NumberingFormat.alphaLcPeriod:
      return 'alphaLcPeriod'
    case NumberingFormat.alphaUcParenR:
      return 'alphaUcParenR'
    case NumberingFormat.alphaUcPeriod:
      return 'alphaUcPeriod'
    case NumberingFormat.arabicParenBoth:
      return 'arabicParenBoth'
    case NumberingFormat.ea1ChsPeriod:
      return 'ea1ChsPeriod'
    case NumberingFormat.chineseNumberParenBoth: // 暂不支持
      return 'ea1ChsPlain'
    default:
      return void 0
  }
}
