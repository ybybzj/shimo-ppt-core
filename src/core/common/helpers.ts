// import { Dict } from '../../../liber/pervasive'

import { hasOwnKey } from '../../../liber/hasOwnKey'
import { Dict, Nullable } from '../../../liber/pervasive'
import { Recyclable } from '../../common/allocator'

export function copyDict<T>(dict: T): T {
  if (!dict || !('object' == typeof dict || Array.isArray(dict))) {
    return dict
  }

  const c = (Array.isArray(dict) ? [] : {}) as T
  for (const p in dict) {
    if (hasOwnKey(dict, p)) {
      const v = dict[p]
      if (v && 'object' === typeof v) {
        c[p] = copyDict(v)
      } else {
        c[p] = v
      }
    }
  }
  return c as T
}

export function isObjectsTheSame<
  D extends Dict,
  T extends { sameAs(other: D): boolean } & D
>(a: Nullable<T>, b: Nullable<D>): boolean {
  if (a === b) return true
  if (a == null && b == null) return true
  if (a == null || b == null) return false
  return a.sameAs(b)
}

export function intersectObjects<
  D extends Dict,
  T extends { intersect(other: D): Nullable<T> } & D
>(a: Nullable<T>, b: Nullable<D>): Nullable<T> {
  if (a == null || b == null) return undefined
  return a.intersect(b)
}

export function isNumbersTheSame(
  valueOfNum1: Nullable<number>,
  valueOfNum2: Nullable<number>,
  exact = false
) {
  if (null == valueOfNum1 && null == valueOfNum2) return true

  if (null == valueOfNum1 || null == valueOfNum2) return false

  return exact
    ? valueOfNum1 === valueOfNum2
    : Math.abs(valueOfNum1 - valueOfNum2) < 0.001
}

export function nullableToNull<T>(isToNull: boolean, v: Nullable<T>) {
  return null == v ? (true === isToNull ? null : undefined) : v
}

export function addObjectInArray<T>(arrOfObjects: T[], object: T) {
  for (let i = 0; i < arrOfObjects.length; ++i) {
    if (arrOfObjects[i] === object) {
      return
    }
  }
  arrOfObjects.push(object)
}

export function fromOptions<
  D,
  FromOptArg,
  I extends { fromOptions: (data: D, arg?: FromOptArg) => void } & D
>(
  obj: Nullable<I>,
  options: Nullable<D>,
  Class: { new (...args: any[]): I },
  ctorArgs?: any[],
  fromOptArg?: FromOptArg
): I | undefined {
  if (options != null) {
    obj = obj ?? (ctorArgs !== undefined ? new Class(...ctorArgs) : new Class())
    obj.fromOptions(options, fromOptArg)
  }
  return obj ?? undefined
}

export function recycleObject<
  T extends Recyclable<T>,
  R extends { recycle: (item: T) => void }
>(obj: Nullable<T>, recycler: R) {
  if (obj != null) {
    recycler.recycle(obj)
  }
  return undefined
}
