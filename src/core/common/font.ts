let DefaultFontFamilyName = 'Arial'
export function generateDefaultFontFamily() {
  return {
    // name: 'SimSun',
    name: DefaultFontFamilyName,
    index: -1,
  }
}

export function setDefaultFontFamilyName(fontFamilyName: string) {
  if (fontFamilyName != null && fontFamilyName !== '') {
    DefaultFontFamilyName = fontFamilyName
  }
}

export function getDefaultFontFamilyName(): string {
  return DefaultFontFamilyName
}
