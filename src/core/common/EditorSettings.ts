/* eslint-disable no-redeclare */
export interface EditorSettings {
  isMobile: boolean
  isSupportComment: boolean
  isShowComment: boolean
  canAddComment: boolean
  canDragOut: boolean
  canZoomComment: boolean
  isNotesEnabled: boolean // actually mean is note show
  isNotesFeatureEnabled: boolean // means is notes feature enabled
  isSupportCxnShape: boolean
  isRenderingNotes: boolean
  isViewMode: boolean
  isSupportAnimation: boolean
  isSupportVideo: boolean
  isSupportAudio: boolean
  isSupportCrop: boolean
  isSupportEavert: boolean
  isRTL: boolean
  isShowThumbnail: boolean
  isEnableThumbnail: boolean
  isSupportArabicLigature: boolean
  isViewModeCopyable: boolean
  isUseHarfBuzz: boolean
  isMeasureGuidelineEnabled: boolean
  isAlignGuidelineEnabled: boolean
  isHandleTextForTouch: boolean
  isBatchRenderText: boolean
  isBatchDrawPath: boolean
  isClipByEditorView: boolean
  imageSrcOrigin: string
  useWorkerForImageLoading: boolean
  useCustomFnForImageLoading: boolean
  compressLargeImage: boolean
  imageBitmapCacheCount: number
  enableSpFormatPainting: boolean
  asyncLoadingSlides: boolean
  isSupportFetch: boolean
  isInDebug: boolean
  isLimitPasting: boolean
  limitCountForPasting: number
  customAssetProtocol: string
  preloadCountForSlideMaster: number
  isCopyAble: boolean
}

export type EditorSettingsKeys = keyof EditorSettings

// 同步修改 setEditorFeatureSetting()
export const EditorSettings: EditorSettings = {
  isMobile: false, // 注意和 browserInfo 内的 isMobile 不同, 这里代表 mobile 模式(例如关闭缩略图模块)，后者仅用来识别 dpr 等硬件参数
  isSupportComment: true, // 整个评论模块的功能开关, 支持一键关闭
  isShowComment: false, // 评论的展示开关
  canAddComment: false, // 评论的编辑开关
  canDragOut: true, // 是否允许拖出对象到浏览器外部
  canZoomComment: false,
  isNotesEnabled: true,
  isNotesFeatureEnabled: true, // means is notes feature enabled
  isSupportCxnShape: true,
  isRenderingNotes: false, // 是否正在绘制 Notes, Todo: 迁移为全局状态
  isViewMode: true,
  isSupportAnimation: true,
  isSupportVideo: true,
  isSupportAudio: true,
  isSupportCrop: false,
  isSupportEavert: true,
  isRTL: false,
  isShowThumbnail: true,
  isEnableThumbnail: true,
  isSupportArabicLigature: false,
  isViewModeCopyable: false,
  isUseHarfBuzz: true,
  isMeasureGuidelineEnabled: true,
  isAlignGuidelineEnabled: true,
  isHandleTextForTouch: true,
  isBatchRenderText: true,
  isBatchDrawPath: true,
  isClipByEditorView: true,
  imageSrcOrigin: window['location'].origin,
  useWorkerForImageLoading: true,
  useCustomFnForImageLoading: true,
  compressLargeImage: true,
  imageBitmapCacheCount: -1,
  enableSpFormatPainting: false,
  asyncLoadingSlides: false,
  isSupportFetch: true, // 存在离线客户端等场景
  isInDebug: false,
  isLimitPasting: true,
  limitCountForPasting: 200,
  customAssetProtocol: 'modoc-assets:/',
  preloadCountForSlideMaster: 5,
  isCopyAble: true,
}

export function setEditorSettings(key: EditorSettingsKeys, v: any) {
  switch (key) {
    // 注意和 browserInfo 内的 isMobile 不同, 这里代表 mobile 模式(例如关闭缩略图模块)，后者仅用来识别 dpr 等硬件参数
    case 'isMobile': {
      EditorSettings.isMobile = v
      break
    }
    // 整个评论模块的功能开关, 支持一键关闭
    case 'isSupportComment': {
      EditorSettings.isSupportComment = v
      break
    }
    // 评论的展示开关
    case 'isShowComment': {
      EditorSettings.isShowComment = v
      break
    }
    // 评论的编辑开关
    case 'canAddComment': {
      EditorSettings.canAddComment = v
      break
    }
    // 是否允许拖出对象到浏览器外部
    case 'canDragOut': {
      EditorSettings.canDragOut = v
      break
    }
    case 'canZoomComment': {
      EditorSettings.canZoomComment = v
      break
    }
    case 'isNotesEnabled': {
      EditorSettings.isNotesEnabled = v
      break
    }
    case 'isNotesFeatureEnabled': {
      EditorSettings.isNotesFeatureEnabled = v
      break
    }
    case 'isSupportCxnShape': {
      EditorSettings.isSupportCxnShape = v
      break
    }
    // 是否正在绘制 Notes, Todo: 迁移为全局状态
    case 'isRenderingNotes': {
      EditorSettings.isRenderingNotes = v
      break
    }
    case 'isViewMode': {
      EditorSettings.isViewMode = v
      break
    }
    case 'isSupportAnimation': {
      EditorSettings.isSupportAnimation = v
      break
    }
    case 'isSupportVideo': {
      EditorSettings.isSupportVideo = v
      break
    }
    case 'isSupportAudio': {
      EditorSettings.isSupportAudio = v
      break
    }
    case 'isSupportCrop': {
      EditorSettings.isSupportCrop = v
      break
    }
    case 'isSupportEavert': {
      EditorSettings.isSupportEavert = v
      break
    }
    case 'isRTL': {
      EditorSettings.isRTL = v
      break
    }
    case 'isShowThumbnail': {
      EditorSettings.isShowThumbnail = v
      break
    }
    case 'isEnableThumbnail': {
      EditorSettings.isEnableThumbnail = v
      break
    }

    case 'isSupportArabicLigature': {
      EditorSettings.isSupportArabicLigature = v
      break
    }
    case 'isViewModeCopyable': {
      EditorSettings.isViewModeCopyable = v
      break
    }
    case 'isUseHarfBuzz': {
      EditorSettings.isUseHarfBuzz = v
      break
    }
    case 'isMeasureGuidelineEnabled': {
      EditorSettings.isMeasureGuidelineEnabled = v
      break
    }
    case 'isAlignGuidelineEnabled': {
      EditorSettings.isAlignGuidelineEnabled = v
      break
    }
    case 'isHandleTextForTouch': {
      EditorSettings.isHandleTextForTouch = v
      break
    }
    case 'isBatchRenderText': {
      EditorSettings.isBatchRenderText = v
      break
    }
    case 'isBatchDrawPath': {
      EditorSettings.isBatchDrawPath = v
      break
    }
    case 'isClipByEditorView': {
      EditorSettings.isClipByEditorView = v
      break
    }
    case 'imageSrcOrigin': {
      EditorSettings.imageSrcOrigin = v
      break
    }
    case 'useWorkerForImageLoading': {
      EditorSettings.useWorkerForImageLoading = v
      break
    }
    case 'useCustomFnForImageLoading': {
      EditorSettings.useCustomFnForImageLoading = v
      break
    }
    case 'compressLargeImage': {
      EditorSettings.compressLargeImage = v
      break
    }
    case 'imageBitmapCacheCount': {
      EditorSettings.imageBitmapCacheCount = v
      break
    }
    case 'enableSpFormatPainting': {
      EditorSettings.enableSpFormatPainting = v
      break
    }
    case 'asyncLoadingSlides': {
      EditorSettings.asyncLoadingSlides = v
      break
    }
    // 存在离线客户端等场景
    case 'isSupportFetch': {
      EditorSettings.isSupportFetch = v
      break
    }
    case 'isInDebug': {
      EditorSettings.isInDebug = v
      break
    }
    case 'isLimitPasting': {
      EditorSettings.isLimitPasting = v
      break
    }
    case 'limitCountForPasting': {
      EditorSettings.limitCountForPasting = v
      break
    }
    case 'customAssetProtocol': {
      EditorSettings.customAssetProtocol = v
      break
    }
    case 'preloadCountForSlideMaster': {
      EditorSettings.preloadCountForSlideMaster = v
      break
    }
    case 'isCopyAble': {
      EditorSettings.isCopyAble = v
      break
    }
  }
}
