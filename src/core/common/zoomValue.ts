import { Nullable } from '../../../liber/pervasive'

let _ZoomValue = 100

let _OriginZoomValue: Nullable<number>
export const ZoomValue = {
  get value() {
    return _ZoomValue
  },
  set value(v: number) {
    _ZoomValue = v
  },
  /** 保留唤起虚拟键盘之前的 zoom, 键盘收起之后需要还原 */
  save() {
    _OriginZoomValue = _ZoomValue
  },
  checkRestore() {
    if (_OriginZoomValue != null) {
      _ZoomValue = _OriginZoomValue
      _OriginZoomValue = null
      return true
    }
  },
}
