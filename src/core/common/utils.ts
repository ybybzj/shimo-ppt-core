import { EditorUtil } from '../../globals/editor'
import { isNumber } from '../../common/utils'
import { Slide } from '../Slide/Slide'
import { InstanceType, isInstanceTypeOf } from '../instanceTypes'
import { Notes } from '../Slide/Notes'
import { Opaque } from '../../../liber/pervasive'

export type ChangeStackAndRegistryState = Opaque<
  'ChangeStackAndRegistryState',
  object
>

interface ChangeStackRestoreState {
  needTurnOnRegistry: boolean
}

/** 在此期间生成的对象不会注册到 entityRegistry 中 */
export function turnOffRecordChanges(): ChangeStackAndRegistryState {
  EditorUtil.ChangesStack.stopRecording()
  let needTurnOnRegistry = false
  if (!EditorUtil.entityRegistry.disallowAdd) {
    EditorUtil.entityRegistry.disallowAdd = true
    needTurnOnRegistry = true
  }
  return {
    needTurnOnRegistry,
  } as unknown as ChangeStackAndRegistryState
}

export function restoreRecordChangesState(s: ChangeStackAndRegistryState) {
  const { needTurnOnRegistry } = s as unknown as ChangeStackRestoreState
  EditorUtil.ChangesStack.enableRecording()
  if (needTurnOnRegistry) {
    EditorUtil.entityRegistry.disallowAdd = false
  }
}

export function rotToAngle(rot): number {
  return (rot * 180) / Math.PI
}

/** 返回 0 ～ 2π 的角度新值 */
export function normalizeRotValue(rot) {
  if (isNumber(rot)) {
    while (rot >= 2 * Math.PI) rot -= 2 * Math.PI
    while (rot < 0) rot += 2 * Math.PI
    return rot
  }
  return rot
}

/** 检测角度是否为 0～45° ｜ 135° ～ 225° ｜ 315 ～ 360° */
export function isRotateIn45D(rot) {
  const _rot = normalizeRotValue(rot)
  return (
    (_rot >= 0 && _rot < Math.PI * 0.25) ||
    (_rot >= 3 * Math.PI * 0.25 && _rot < 5 * Math.PI * 0.25) ||
    (_rot >= 7 * Math.PI * 0.25 && _rot < 2 * Math.PI)
  )
}

export function checkAlmostEqual(a: number, b: number, delta?) {
  if (a === b) {
    return true
  }
  if (isNumber(delta)) {
    return Math.abs(a - b) < delta
  }
  return Math.abs(a - b) < 1e-15
}

export function toAngleByXYRad(
  angle: number,
  xRad: number,
  yRad: number
): number {
  return Math.atan2(Math.sin(angle) / yRad, Math.cos(angle) / xRad)
}

const FontSizes = [
  1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 10.5, 11, 12, 14, 16, 18, 20, 24, 28, 32, 36,
  40, 44, 48, 54, 60, 66, 72, 80, 88, 96, 115, 138, 166, 199, 239, 287, 344,
  413, 496, 595, 714, 857, 1028, 1234, 1481, 1777, 2132, 2558, 3070, 3684, 4000,
] as const

export function getFontSizeForIncreaseDecreaseByInput(
  isIncrease: boolean,
  inputValue: number | string
) {
  const size =
    inputValue === 10.5
      ? inputValue
      : isNumber(inputValue)
      ? inputValue
      : parseFloat(inputValue)

  let newSize = size
  if (true === isIncrease) {
    if (size < FontSizes[0]) {
      newSize = FontSizes[0]
    } else if (size >= FontSizes[FontSizes.length - 1]) {
      newSize = FontSizes[FontSizes.length - 1]
    } else {
      const remainder = size % 1
      if (size < 8 && remainder !== 0) {
        if (remainder < 0.5) {
          newSize = Math.floor(size + 1)
        } else {
          newSize = Math.floor(size + 2)
        }
        if (newSize >= 8) {
          newSize = 8
        }
      } else {
        for (let i = 0; i < FontSizes.length; i++) {
          if (size < FontSizes[i]) {
            newSize = FontSizes[i]
            break
          }
        }
      }
    }
  } else {
    if (size <= FontSizes[0]) {
      newSize = FontSizes[0]
    } else if (size > FontSizes[FontSizes.length - 1]) {
      newSize = FontSizes[FontSizes.length - 1]
    } else {
      const remainder = size % 1
      if (size < 8 && remainder !== 0) {
        if (remainder < 0.5) {
          newSize = Math.floor(size - 1)
        } else {
          newSize = Math.floor(size)
        }
        if (newSize <= FontSizes[0]) {
          newSize = FontSizes[0]
        }
      } else {
        for (let i = FontSizes.length - 1; i >= 0; i--) {
          if (size > FontSizes[i]) {
            newSize = FontSizes[i]
            break
          }
        }
      }
    }
  }

  return newSize
}
export function isSlide(obj: any): obj is Slide {
  return isInstanceTypeOf(obj, InstanceType.Slide)
}

export function isSlideNotes(obj: any): obj is Notes {
  return isInstanceTypeOf(obj, InstanceType.Notes)
}
