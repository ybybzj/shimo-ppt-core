import { GraphicsBoundsChecker } from '../graphic/Bounds'
import { CanvasDrawer } from '../graphic/CanvasDrawer'

export type HtmlImageWithLoadStatus = HTMLImageElement & {
  loaded?: boolean
}

export interface Offset {
  x: number
  y: number
}

export interface Size {
  width: number
  height: number
}

export type TextContentDrawContext = GraphicsBoundsChecker | CanvasDrawer

