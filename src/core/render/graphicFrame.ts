import { CanvasDrawer } from '../graphic/CanvasDrawer'
import { toggleCxtTransformReset } from '../graphic/utils'
import { GraphicFrame } from '../SlideElement/GraphicFrame'
import { transformUpdater } from '../utilities/shape/type'
import { renderTable } from './table'

export function renderGraphicFrame(
  sp: GraphicFrame,
  graphics: CanvasDrawer,
  transformUpdater?: transformUpdater
) {
  const transform = transformUpdater
    ? transformUpdater(sp, graphics, sp.transform)
    : sp.transform

  if (sp.graphicObject) {
    graphics.saveState()
    graphics.applyTransformMatrix(transform)
    toggleCxtTransformReset(graphics, true)
    renderTable(sp.graphicObject, graphics, transform)
    graphics.restoreState()
  }
}
