import { Nullable } from '../../../../liber/pervasive'
import { InstanceType } from '../../instanceTypes'
import { SlideElement } from '../../SlideElement/type'
import { updatePenDash } from '../../graphic/drawUtils'
import { getCanvasDrawerContext } from '../../graphic/utils'
import { Geometry } from '../../SlideElement/geometry/Geometry'
import { updateBoundsByXYExt } from '../../utilities/shape/draw'
import { getParentSlide } from '../../utilities/shape/getters'
import { drawShapeFill } from './drawFill'
import { drawGeometry } from './drawGeometry'
import { drawShapeStroke } from './drawStroke'
import {
  getInitState,
  getLnInfo,
  getFillEffectsInfo,
  ShapeRenderingState,
} from '../../graphic/ShapeRenderingState'
import { ShapeRenderingInfo, SpRenderingContext } from './type'

export function drawShape(
  sp: SlideElement,
  drawContext: SpRenderingContext,
  geom: Nullable<Geometry>,
  beforeDraw?: (state: ShapeRenderingState, sp: SlideElement) => void
) {
  const state = getStateFromSlideElement(sp, drawContext)
  if (state.isNoFillForce && state.isNoStrokeForce) {
    return
  }

  if (beforeDraw) {
    beforeDraw(state, sp)
  }

  if (geom) {
    state.isRect = geom.preset === 'rect'
    drawGeometry(geom, state, drawContext)
  } else {
    drawShapeRect(state, drawContext)
  }
  clearPenDash(drawContext)
}

export function drawShapeByInfoWithDrawer(
  renderingInfo: ShapeRenderingInfo,
  drawer: {
    draw(state: ShapeRenderingState, drawContext: SpRenderingContext): void
  },
  drawContext: SpRenderingContext
) {
  const state = getStateForRenderingInfo(renderingInfo, drawContext)
  if (state.isNoFillForce && state.isNoStrokeForce) {
    return
  }
  drawer.draw(state, drawContext)
  clearPenDash(drawContext)
}

export function drawShapeByInfo(
  renderingInfo: ShapeRenderingInfo,
  drawContext: SpRenderingContext,
  geom?: Nullable<Geometry>
) {
  const state = getStateForRenderingInfo(renderingInfo, drawContext)
  if (state.isNoFillForce && state.isNoStrokeForce) {
    return
  }
  if (geom) {
    state.isRect = geom.preset === 'rect'
    drawGeometry(geom, state, drawContext)
  } else {
    drawShapeRect(state, drawContext)
  }
  clearPenDash(drawContext)
}

function clearPenDash(drawContext: SpRenderingContext) {
  const drawCanvas = getCanvasDrawerContext(drawContext)
  if (drawCanvas) {
    drawCanvas.context.setLineDash([])
  }
}

function drawShapeRect(
  state: ShapeRenderingState,
  drawContext: SpRenderingContext
) {
  const { l, t } = state
  drawContext._begin()
  drawContext._moveTo(l, t)
  drawContext._lineTo(l + state.extX, t)
  drawContext._lineTo(l + state.extX, t + state.extY)
  drawContext._lineTo(l, t + state.extY)
  drawContext._close()
  drawShapeFill(state, drawContext, 'norm')
  if (!state.isNoStrokeForce) {
    drawShapeStroke(state, drawContext, false)
  }
  drawContext._end()
}

function getStateFromSlideElement(
  sp: SlideElement,
  drawContext: SpRenderingContext
): ShapeRenderingState {
  const state = getStateByInfo(sp)

  const slide = getParentSlide(sp)
  const presentation = slide && slide.presentation

  state.useBgFill =
    presentation != null &&
    sp.instanceType === InstanceType.Shape &&
    sp.attrUseBgFill === true
  if (presentation != null) {
    state.slideWidth = presentation.width
    state.slideHeight = presentation.height
  }

  if (state.useBgFill === true) {
    state.fillEffects = slide?.backgroundFill
  }

  updateRenderingState(state)
  updateDrawContextByState(drawContext, state)
  return state
}

function getStateForRenderingInfo(
  shape: ShapeRenderingInfo,
  drawContext: SpRenderingContext
): ShapeRenderingState {
  const state = getStateByInfo(shape)
  updateRenderingState(state)
  updateDrawContextByState(drawContext, state)
  return state
}

function getStateByInfo(shape: ShapeRenderingInfo): ShapeRenderingState {
  const state = getInitState()
  state.isRect = true
  if (shape.l != null) {
    state.l = shape.l
  }
  if (shape.t != null) {
    state.t = shape.t
  }
  if (shape.extX != null) {
    state.extX = shape.extX
  }
  if (shape.extY != null) {
    state.extY = shape.extY
  }

  if (shape.brush != null) {
    state.fillEffects = shape.brush
  }
  if (shape.pen != null) {
    state.Ln = shape.pen
  }
  return state
}

function updateRenderingState(state: ShapeRenderingState) {
  let isCheckBounds = false

  const fillInfo = getFillEffectsInfo(state.fillEffects)
  if (fillInfo.noFill === true) {
    state.isNoFillForce = true
  } else {
    const {
      isTexture,
      fillRGBAColor: fillRGBAColor,
      isCheckBounds: _isCheckBounds,
    } = fillInfo

    if (isTexture === true) {
      state.isTexture = isTexture
    }

    if (fillRGBAColor != null) {
      state.fillRGBAColor = fillRGBAColor
    }

    if (_isCheckBounds === true) {
      isCheckBounds = true
    }
  }

  const lnInfo = getLnInfo(state.Ln)
  if (lnInfo.noStroke === true) {
    state.isNoStrokeForce = true
  } else {
    const { StrokeRGBAColor: strokeRGBAColor, strokeWidth } = lnInfo
    state.StrokeRGBAColor = strokeRGBAColor
    state.strokeWidth = strokeWidth!
  }
  if (isCheckBounds) {
    updateBoundsByXYExt(state, state.l, state.t, state.extX, state.extY)
  }
  return state
}

function updateDrawContextByState(
  drawContext: SpRenderingContext,
  state: ShapeRenderingState
) {
  drawContext.setPenWidth(1000 * state.strokeWidth)
  const drawer = getCanvasDrawerContext(drawContext)
  updatePenDash(state, drawer)

  if (state.isNoStrokeForce !== true) {
    const ln = state.Ln!
    if (
      (ln.headEnd != null && ln.headEnd.type != null) ||
      (ln.tailEnd != null && ln.tailEnd.type != null)
    ) {
      drawer.pointsCollection = []
    }

    if (
      drawContext.instanceType === InstanceType.CanvasDrawer &&
      drawContext.context != null &&
      ln.Join?.type != null
    ) {
      state.orgLineJoin = drawContext.context.lineJoin
    }
  }
}
