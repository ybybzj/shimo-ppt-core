import { InstanceType } from '../../instanceTypes'
import { drawPointsQueue, drawStrokeByState } from '../../graphic/drawUtils'
import { getCanvasDrawerContext } from '../../graphic/utils'
import { LineJoinType } from '../../SlideElement/const'
import { ShapeRenderingState } from '../../graphic/ShapeRenderingState'
import { SpRenderingContext } from './type'

export function drawShapeStroke(
  state: ShapeRenderingState,
  drawContext: SpRenderingContext,
  isRenderArrows = true,
  p?: Path2D
) {
  if (state.isNoStrokeForce) {
    return
  }

  if (drawContext.instanceType === InstanceType.EditLayerDrawer) {
    drawContext.editLayer.resetAll = true
  }

  const canvasDrawer = getCanvasDrawerContext(drawContext)
  if (state.orgLineJoin != null) {
    switch (state.Ln!.Join!.type) {
      case LineJoinType.Round: {
        canvasDrawer.setLineJoin('round')
        break
      }
      case LineJoinType.Bevel: {
        canvasDrawer.setLineJoin('bevel')
        break
      }
      case LineJoinType.Empty: {
        canvasDrawer.setLineJoin('miter')
        break
      }
      case LineJoinType.Miter: {
        canvasDrawer.setLineJoin('miter')
        break
      }
    }
  }
  drawStrokeByState(state, canvasDrawer, p)
  if (null != state.orgLineJoin) {
    canvasDrawer.setLineJoin(state.orgLineJoin)
  }

  if (isRenderArrows) {
    drawPointsQueue(state, canvasDrawer)
  }
}
