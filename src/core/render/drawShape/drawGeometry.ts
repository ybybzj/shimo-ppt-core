import { arcToCurves } from '../../graphic/arc'
import { Geometry } from '../../SlideElement/geometry/Geometry'
import { Path, PathOperationType } from '../../SlideElement/geometry/Path'
import { drawShapeFill } from './drawFill'
import { drawShapeStroke } from './drawStroke'
import { ShapeRenderingState } from '../../graphic/ShapeRenderingState'
import { SpRenderingContext } from './type'
import { InstanceType } from '../../instanceTypes'
import { EditorSettings } from '../../common/EditorSettings'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { Nullable } from '../../../../liber/pervasive'
import { FillKIND } from '../../common/const/attrs'

export function drawGeometry(
  geom: Geometry,
  state: ShapeRenderingState,
  drawContext: SpRenderingContext
) {
  for (let i = 0, l = geom.pathLst.length; i < l; ++i) {
    drawPath(geom.pathLst[i], state, drawContext)
  }
}

function drawPath(
  path: Path,
  state: ShapeRenderingState,
  drawContext: SpRenderingContext
) {
  if (
    isFillTilingOrPatten(state.fillEffects) ||
    hasPointsQueue(drawContext) ||
    EditorSettings.isBatchDrawPath === false
  ) {
    let isDrawLast = false
    const pathCmd = path.pathCommands
    drawContext._begin()
    for (let j = 0, l = pathCmd.length; j < l; ++j) {
      const cmd = pathCmd[j]
      switch (cmd.id) {
        case PathOperationType.moveTo: {
          isDrawLast = true
          drawContext._moveTo(cmd.x, cmd.y)
          break
        }
        case PathOperationType.lineTo: {
          isDrawLast = true
          drawContext._lineTo(cmd.x, cmd.y)
          break
        }
        case PathOperationType.quadBezTo: {
          isDrawLast = true
          drawContext._quadraticCurveTo(cmd.x0, cmd.y0, cmd.x1, cmd.y1)
          break
        }
        case PathOperationType.cubicBezTo: {
          isDrawLast = true
          drawContext._bezierCurveTo(
            cmd.x0,
            cmd.y0,
            cmd.x1,
            cmd.y1,
            cmd.x2,
            cmd.y2
          )
          break
        }
        case PathOperationType.arcTo: {
          isDrawLast = true
          arcToCurves(
            drawContext,
            cmd.stX,
            cmd.stY,
            cmd.wR,
            cmd.hR,
            cmd.stAng,
            cmd.swAng
          )
          break
        }
        case PathOperationType.close: {
          drawContext._close()
          break
        }
      }
    }

    if (isDrawLast) {
      if (path.fillMode != null) {
        drawShapeFill(state, drawContext, path.fillMode)
      }

      if (path.stroke && !state.isNoStrokeForce) {
        drawShapeStroke(state, drawContext)
      }
    }
  } else {
    const path2D = path.getPath2D()
    if (path2D != null) {
      if (path.fillMode != null) {
        drawShapeFill(state, drawContext, path.fillMode, path2D)
      }

      if (path.stroke && !state.isNoStrokeForce) {
        drawShapeStroke(state, drawContext, false, path2D)
      }
    }
  }
  drawContext._end()
}

function hasPointsQueue(drawContext: SpRenderingContext) {
  if (drawContext.instanceType === InstanceType.CanvasDrawer) {
    return drawContext.pointsCollection != null
  } else {
    return drawContext.canvasDrawer.pointsCollection != null
  }
}

function isFillTilingOrPatten(filleffect: Nullable<FillEffects>) {
  return (
    (filleffect &&
      filleffect.fill?.instanceType === InstanceType.BlipFill &&
      filleffect.fill.tile != null) ||
    filleffect?.fill?.type === FillKIND.PATT
  )
}
