import { Nullable } from '../../../../liber/pervasive'
import { FillKIND, PresetPatternNumToValMap } from '../../common/const/attrs'
import { checkImageSrc } from '../../../common/utils'
import { InstanceType } from '../../instanceTypes'
import { PathFillMode } from '../../../io/dataType/geometry'
import { checkAlmostEqual } from '../../common/utils'
import { getGradInfoByFillInfo } from '../../graphic/drawUtils'
import { CanvasDrawer } from '../../graphic/CanvasDrawer'
import { getPatternBrush } from '../../graphic/PatternBrush'
import { Matrix2D } from '../../graphic/Matrix'
import {
  getCanvasDrawerContext,
  setCxtTransformReset,
  restoreCxtTransformReset,
} from '../../graphic/utils'
import { drawImageFromSrc } from './drawImage'
import { ShapeRenderingState } from '../../graphic/ShapeRenderingState'
import { SpRenderingContext } from './type'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { RGBClr } from '../../color/type'
import { BlipFill } from '../../SlideElement/attrs/fill/blipFill'
import { BrowserInfo } from '../../../common/browserInfo'

interface FillInfo {
  fillEffects: Nullable<FillEffects>
  isTexture: boolean
  fillRGBAColor?: { r: number; g: number; b: number; a: number }
  isCheckBounds?: boolean
  isRect: boolean
  fillMode?: Nullable<PathFillMode>
  transparent?: number
}

export function drawShapeFill(
  state: ShapeRenderingState,
  drawContext: SpRenderingContext,
  fillMode: Nullable<PathFillMode>,
  p?: Path2D
) {
  if (fillMode === 'none' || state.isNoFillForce) return

  if (drawContext.instanceType === InstanceType.EditLayerDrawer) {
    drawContext.editLayer.resetAll = true
  }

  const fillInfo = getFillInfo(state, fillMode)

  if (state.useBgFill === true) {
    drawContext.save()
    drawContext.clip(p)

    drawContext.applyTransformMatrix(state.useBgFillMatrix ?? new Matrix2D())

    const x = 0
    const y = 0
    const w = state.slideWidth
    const h = state.slideHeight

    drawFillFromInfo(drawContext, fillInfo, false, x, y, w, h, (drawer) => {
      const g = getCanvasDrawerContext(drawer)
      g.fillRect(x, y, w, h)
    })

    drawContext.restore()
    drawContext.restoreState()
    return
  }

  const x = state.minX
  const y = state.minY
  const w = state.maxX - state.minX
  const h = state.maxY - state.minY

  drawFillFromInfo(
    drawContext,
    fillInfo,
    fillInfo.isRect !== true,
    x,
    y,
    w,
    h,
    (g) => g.drawFill(p),
    p
  )
}

function getFillInfo(
  state: ShapeRenderingState,
  fillMode: Nullable<PathFillMode>
): FillInfo {
  const fillEffects = state.fillEffects

  // get transparent
  let transparent
  if (fillEffects?.fill?.type === FillKIND.BLIP) {
    transparent = fillEffects?.fill?.getTransparent()
  } else if (fillEffects?.transparent != null) {
    transparent = fillEffects.transparent
  }

  const fillInfo: FillInfo = {
    fillEffects,
    isTexture: state.isTexture,
    isRect: state.useBgFill ? true : state.isRect,
    fillRGBAColor:
      state.fillRGBAColor != null ? state.fillRGBAColor : undefined,
    fillMode: fillMode != null ? fillMode : undefined,
    transparent,
  }

  return fillInfo
}

function drawFillFromInfo(
  g: SpRenderingContext,
  fillInfo: FillInfo,
  isClip: boolean,
  x: number,
  y: number,
  w: number,
  h: number,
  fillColor: (g: SpRenderingContext) => void,
  p?: Path2D
) {
  const { fillEffects, transparent } = fillInfo
  const fill = fillEffects?.fill
  switch (fill?.type) {
    case FillKIND.BLIP: {
      drawFillTexture(g, fillEffects, transparent, x, y, w, h, isClip, p)
      break
    }
    case FillKIND.PATT: {
      drawFillPatten(g, fillEffects, transparent, x, y, p)
      break
    }
    case FillKIND.GRAD: {
      drawFillGradient(g, fillEffects, transparent, x, y, w, h, p)
      break
    }
    default: {
      drawFillColor(g, fillInfo, fillColor)
    }
  }
}

// #region  fill texture
function drawFillImage(
  g: SpRenderingContext,
  blipfill: BlipFill,
  x: number,
  y: number,
  w: number,
  h: number,
  transparent?: Nullable<number>
) {
  const needApplyTransparent =
    transparent != null && g.instanceType === InstanceType.CanvasDrawer
  let oldAlpha
  if (needApplyTransparent) {
    const _g = g as CanvasDrawer
    oldAlpha = _g.getGlobalAlpha()
    _g.setGlobalAlpha(transparent / 255)
  }

  drawImageFromSrc(
    g,
    checkImageSrc(blipfill.imageSrcId),
    x,
    y,
    w,
    h,
    undefined,
    blipfill.srcRect,
    blipfill.fillRect
  )
  if (needApplyTransparent) {
    ;(g as CanvasDrawer).setGlobalAlpha(oldAlpha)
  }
}

function drawFillTiling(
  g: CanvasDrawer,
  fill: BlipFill,
  x: number,
  y: number,
  w: number,
  h: number,
  transparent?: Nullable<number>,
  p?: Path2D
) {
  g.drawTilingFromSrc(fill, x, y, w, h, transparent, p)
}

function drawFillTexture(
  g: SpRenderingContext,
  fillEffects: Nullable<FillEffects>,
  transparent: Nullable<number>,
  x: number,
  y: number,
  w: number,
  h: number,
  isClip = false,
  p?: Path2D
) {
  if (fillEffects?.fill?.type !== FillKIND.BLIP) return
  const fill = fillEffects.fill
  switch (g.instanceType) {
    case InstanceType.EditLayerDrawer: {
      drawFillImage(g, fill, x, y, w, h)
      break
    }
    case InstanceType.CanvasDrawer: {
      const resetState = setCxtTransformReset(g, false, true)

      // handle no tiling
      if (fill?.tile == null) {
        if (isClip) {
          g.save()
          g.clip(p)
        }

        drawFillImage(g, fill, x, y, w, h, transparent)
        if (isClip) {
          g.restore()
        }
      } else {
        drawFillTiling(g, fill, x, y, w, h, transparent, p)
      }
      restoreCxtTransformReset(g, resetState)
      break
    }
  }
}
// #endregion

// #region fill patten
function drawFillPatten(
  g: SpRenderingContext,
  fillEffects: Nullable<FillEffects>,
  transparent: Nullable<number>,
  x: number,
  y: number,
  p?: Path2D
) {
  const fill = fillEffects?.fill
  if (fill?.type !== FillKIND.PATT) {
    return
  }
  const needApplyTransparent =
    g.instanceType !== InstanceType.EditLayerDrawer && transparent != null

  const resetState = setCxtTransformReset(g, false, true)
  const canvasDrawerCtx = getCanvasDrawerContext(g)

  let pattenName = PresetPatternNumToValMap[fill.patternType]
  if (null == pattenName) pattenName = 'cross'

  const fc = fill.fgClr!.rgba
  const bc = fill.bgClr!.rgba

  const fa = null == transparent ? fc.a : 255
  const ba = null == transparent ? bc.a : 255

  const pattern = getPatternBrush(
    pattenName,
    fc.r,
    fc.g,
    fc.b,
    fa,
    bc.r,
    bc.g,
    bc.b,
    ba
  )
  const patt = canvasDrawerCtx.createPattern(pattern.canvas, 'repeat')!

  canvasDrawerCtx.save()

  const zoomValue = canvasDrawerCtx.ZoomValue

  let factorX = zoomValue / 100
  let factorY = zoomValue / 100
  // 计算tile的patten时需要还原对高分辨率的处理
  factorX *= BrowserInfo.PixelRatio
  factorY *= BrowserInfo.PixelRatio

  // if (g.instanceType === InstanceType.CanvasDrawer && g.isRenderThumbnail) {
  //   factorX = 1
  //   factorY = 1
  // }

  // TODO: !!!
  canvasDrawerCtx.translateCtx(x, y)

  if (g.instanceType === InstanceType.CanvasDrawer) {
    canvasDrawerCtx.scaleCtx(
      factorX * g.pattenFillTransformScaleX,
      factorY * g.pattenFillTransformScaleY
    )
  } else {
    canvasDrawerCtx.scaleCtx(
      factorX * g.canvasDrawer.pattenFillTransformScaleX,
      factorY * g.canvasDrawer.pattenFillTransformScaleY
    )
  }

  if (needApplyTransparent === true) {
    const oldAlpha = canvasDrawerCtx.getGlobalAlpha()

    if (null != transparent) {
      canvasDrawerCtx.setGlobalAlpha(transparent / 255)
    }

    canvasDrawerCtx.setFillStyle(patt)
    canvasDrawerCtx.drawFill(p)
    canvasDrawerCtx.setGlobalAlpha(oldAlpha)
  } else {
    canvasDrawerCtx.setFillStyle(patt)
    canvasDrawerCtx.drawFill(p)
  }

  canvasDrawerCtx.restore()
  restoreCxtTransformReset(g, resetState)
}

// #endregion

// #region fill gradient
function drawFillGradient(
  drawContext: SpRenderingContext,
  fillEffects: Nullable<FillEffects>,
  transparent: Nullable<number>,
  x: number,
  y: number,
  w: number,
  h: number,
  p?: Path2D
) {
  const gradient = getGradInfoByFillInfo(
    drawContext,
    fillEffects,
    transparent,
    x,
    y,
    w,
    h
  )
  if (!gradient) return
  const resetState = setCxtTransformReset(drawContext, false, true)
  const g = getCanvasDrawerContext(drawContext)
  g.setFillStyle(gradient)

  if (null != transparent) {
    const oldAlpha = g.getGlobalAlpha()
    g.setGlobalAlpha(transparent / 255)
    g.drawFill(p)
    g.setGlobalAlpha(oldAlpha)
  } else {
    g.drawFill(p)
  }
  restoreCxtTransformReset(drawContext, resetState)
}
// #endregion

// #region fill color
function drawFillColor(
  g: SpRenderingContext,
  fillInfo: FillInfo,
  fillColor: (g: SpRenderingContext) => void
) {
  const { fillRGBAColor, fillEffects, transparent, fillMode } = fillInfo
  let rgba = fillRGBAColor
  if (rgba) {
    if (fillMode === 'darken') {
      const rgb = darken({ r: rgba.r, g: rgba.g, b: rgba.b })
      rgba = { r: rgb.r, g: rgb.g, b: rgb.b, a: rgba.a }
    } else if (fillMode === 'darkenLess') {
      const rgb = darkenLess({ r: rgba.r, g: rgba.g, b: rgba.b })
      rgba = { r: rgb.r, g: rgb.g, b: rgb.b, a: rgba.a }
    } else if (fillMode === 'lighten') {
      const rgb = lighten({ r: rgba.r, g: rgba.g, b: rgba.b })
      rgba = { r: rgb.r, g: rgb.g, b: rgb.b, a: rgba.a }
    } else if (fillMode === 'lightenLess') {
      const rgb = lightenLess({ r: rgba.r, g: rgba.g, b: rgba.b })
      rgba = { r: rgb.r, g: rgb.g, b: rgb.b, a: rgba.a }
    }
    if (fillEffects != null && transparent != null) {
      rgba.a = transparent
    }
    g.setBrushColor(rgba.r, rgba.g, rgba.b, rgba.a)
    fillColor(g)
  }
}
// #endregion

//helpers

function rgbColorByBrightness(c: RGBClr, brightness: number): RGBClr {
  if (checkAlmostEqual(brightness, 0.0)) {
    return c
  }

  if (brightness >= 0.0) {
    return {
      r: ceilColor(c.r * (1.0 - brightness) + brightness * 255.0),
      g: ceilColor(c.g * (1.0 - brightness) + brightness * 255.0),
      b: ceilColor(c.b * (1.0 - brightness) + brightness * 255.0),
    }
  } else {
    return {
      r: ceilColor(c.r * (1.0 + brightness)),
      g: ceilColor(c.g * (1.0 + brightness)),
      b: ceilColor(c.b * (1.0 + brightness)),
    }
  }
}

function darken(c: RGBClr) {
  return rgbColorByBrightness(c, -0.4)
}

function darkenLess(c: RGBClr) {
  return rgbColorByBrightness(c, -0.2)
}

function lighten(c: RGBClr) {
  return rgbColorByBrightness(c, 0.4)
}

function lightenLess(c: RGBClr) {
  return rgbColorByBrightness(c, 0.2)
}

function ceilColor(c: number): number {
  return Math.min(255, Math.max(0, (c + 0.5) >> 0))
}
