import { InstanceType } from '../../instanceTypes'
import { EditLayerDrawer } from '../../../ui/rendering/Drawer/EditLayerDrawer'
import { CanvasDrawer } from '../../graphic/CanvasDrawer'
import { RelativeRect } from '../../SlideElement/attrs/fill/RelativeRect'
import { SpRenderingContext } from './type'

export function drawImageFromSrcOnGraphicsDrawer(
  g: CanvasDrawer,
  imgSrc: string,
  x: number,
  y: number,
  w: number,
  h: number,
  alpha?: number,
  srcRect?: RelativeRect,
  fillRect?: RelativeRect
) {
  g.drawImageFromSrc(imgSrc, x, y, w, h, alpha, srcRect, fillRect)
}

export function drawImageFromSrcOnEditLayer(
  editLayerDrawer: EditLayerDrawer,
  imageSrc: string,
  x: number,
  y: number,
  w: number,
  h: number,
  srcRect?: RelativeRect,
  fillRect?: RelativeRect
) {
  const g = editLayerDrawer.canvasDrawer
  drawImageFromSrcOnGraphicsDrawer(
    g,
    imageSrc,
    x,
    y,
    w,
    h,
    undefined,
    srcRect,
    fillRect
  )

  const _x1 = g._XFromPoint(x, y)
  const _y1 = g._YFromPoint(x, y)

  const _x2 = g._XFromPoint(x + w, y)
  const _y2 = g._YFromPoint(x + w, y)

  const _x3 = g._XFromPoint(x + w, y + h)
  const _y3 = g._YFromPoint(x + w, y + h)

  const _x4 = g._XFromPoint(x, y + h)
  const _y4 = g._YFromPoint(x, y + h)

  const editLayer = editLayerDrawer.editLayer
  editLayer.update(_x1, _y1)
  editLayer.update(_x2, _y2)
  editLayer.update(_x3, _y3)
  editLayer.update(_x4, _y4)
}

export function drawImageFromSrc(
  g: SpRenderingContext,
  imgSrc: string,
  x: number,
  y: number,
  w: number,
  h: number,
  alpha?: number,
  srcRect?: RelativeRect,
  fillRect?: RelativeRect
) {
  if (g.instanceType === InstanceType.CanvasDrawer) {
    drawImageFromSrcOnGraphicsDrawer(
      g,
      imgSrc,
      x,
      y,
      w,
      h,
      alpha,
      srcRect,
      fillRect
    )
  } else {
    drawImageFromSrcOnEditLayer(g, imgSrc, x, y, w, h, srcRect, fillRect)
  }
}
