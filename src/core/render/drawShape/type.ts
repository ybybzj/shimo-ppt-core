import { Nullable } from '../../../../liber/pervasive'
import { EditLayerDrawer } from '../../../ui/rendering/Drawer/EditLayerDrawer'
import { CanvasDrawer } from '../../graphic/CanvasDrawer'
import { IMatrix2D } from '../../graphic/Matrix'
import { FillEffects } from '../../SlideElement/attrs/fill/fill'
import { Ln } from '../../SlideElement/attrs/line'

export type SpRenderingContext = CanvasDrawer | EditLayerDrawer

export interface ShapeRenderingInfo {
  brush?: Nullable<FillEffects>
  pen?: Nullable<Ln>
  matrix?: Nullable<IMatrix2D>
  l?: number
  t?: number
  extX?: number
  extY?: number
}

export interface WithExts {
  extX: number
  extY: number
}
