import { BrowserInfo } from '../../common/browserInfo'
import { EditLayerDrawer } from '../../ui/rendering/Drawer/EditLayerDrawer'
import {
  CommentStatus,
  CommentStatusType,
  GCommentIconsRetinaSource,
  GCommentIconsRetinaSourceRTL,
  GCommentIconsSource,
  GCommentIconsSourceRTL,
} from '../common/const/drawing'
import { Factor_mm_to_pix } from '../common/const/unit'
import { EditorSettings } from '../common/EditorSettings'
import { CanvasDrawer } from '../graphic/CanvasDrawer'
import { getHeightOfComment, getWidthOfComment } from '../utilities/helpers'

export function renderPresentationComment(
  g: CanvasDrawer,
  status: CommentStatus,
  x: number,
  y: number
) {
  const isRTL = EditorSettings.isRTL
  const iconResource =
    BrowserInfo.isRetina && BrowserInfo.PixelRatio > 1
      ? isRTL
        ? GCommentIconsRetinaSourceRTL
        : GCommentIconsRetinaSource
      : isRTL
      ? GCommentIconsSourceRTL
      : GCommentIconsSource

  const commentIcon = iconResource[status].src

  if (EditorSettings.canZoomComment) {
    const mmW = getWidthOfComment(status)
    const mmH = getHeightOfComment(status)
    g._drawImageElement(commentIcon, x, y, mmW, mmH)
  } else {
    const _x = g.finalTransform.XFromPoint(x, y) >> 0
    const _y = g.finalTransform.YFromPoint(x, y) >> 0
    const w = iconResource[status].width
    const h = iconResource[status].height
    g.context.drawImage(commentIcon, _x, _y, w, h)
  }
}

export function renderCommentsOnEditLayer(
  editLayerDrawer: EditLayerDrawer,
  status: CommentStatus,
  x: number,
  y: number
) {
  if (!GCommentIconsSource || !editLayerDrawer.ctx) return

  const editLayer = editLayerDrawer.editLayer
  editLayerDrawer.curSlideRenderingInfo =
    editLayer.editorUI.getSlideRenderingInfo()

  const drawRect = editLayerDrawer.curSlideRenderingInfo.renderingRect

  const l = drawRect.left
  const t = drawRect.top
  const w = drawRect.right - drawRect.left
  const h = drawRect.bottom - drawRect.top

  const factorX = w / editLayerDrawer.curSlideRenderingInfo.widthInMM
  const factorY = h / editLayerDrawer.curSlideRenderingInfo.heightInMM

  const __x = (l + factorX * x) >> 0
  const __y = (t + factorY * y) >> 0

  const ctx = editLayer.context
  const _oldAlpha = ctx.globalAlpha
  ctx.globalAlpha = status === CommentStatusType.Drag ? 0.5 : 1

  editLayer.resetTransform()

  const isRTL = EditorSettings.isRTL
  const iconResource = isRTL ? GCommentIconsSourceRTL : GCommentIconsSource
  const commentIcon = iconResource[status].src

  if (EditorSettings.canZoomComment) {
    const zoomValue = editLayerDrawer.canvasDrawer.ZoomValue
    const mmW = getWidthOfComment(status, zoomValue) / BrowserInfo.PixelRatio
    const mmH = getHeightOfComment(status, zoomValue) / BrowserInfo.PixelRatio
    const commentW = mmW * Factor_mm_to_pix
    const commentH = mmH * Factor_mm_to_pix
    editLayer.updateByRect(__x, __y, commentW, commentH)
    editLayerDrawer.ctx.drawImage(commentIcon, __x, __y, commentW, commentH)
  } else {
    editLayer.updateByRect(__x, __y, commentIcon.width, commentIcon.height)
    editLayerDrawer.ctx.drawImage(
      commentIcon,
      __x,
      __y,
      commentIcon.width,
      commentIcon.height
    )
  }

  ctx.globalAlpha = _oldAlpha
}
