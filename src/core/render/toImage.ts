import { checkImageSrc, isNumber, toInt } from '../../common/utils'
import { InstanceType } from '../instanceTypes'
import { SlideElement } from '../SlideElement/type'
import { Factor_mm_to_pix, ScaleOfPPTXSizes } from '../common/const/unit'
import { checkAlmostEqual } from '../common/utils'
import { GraphicsBoundsChecker, WithBounds } from '../graphic/Bounds'
import {
  getCanvasDrawer,
  getCanvasDrawerWithScale,
} from '../graphic/CanvasDrawer'
import { invertTransformConverter } from '../utilities/shape/draw'
import { checkBounds_SlideElement } from '../checkBounds/slideElement'
import { drawSlideElement } from './drawSlideElement'
import { isBgTransparentForSp } from '../utilities/shape/getters'

export interface ShapeImageData {
  image: HTMLCanvasElement
  bounds: { offsetX: number; offsetY: number; width: number; height: number }
}

export function getSlideElementPreview(
  sp: SlideElement,
  scaleFactor: number
): ShapeImageData {
  const bounds = getBoundsForShape(sp, true)

  const im = document.createElement('canvas')
  im.width = bounds.width * scaleFactor
  im.height = bounds.height * scaleFactor

  const g = getCanvasDrawerWithScale(
    im.getContext('2d')!,
    scaleFactor,
    scaleFactor
  )

  g.isInPreview = true
  g.setCoordTransform(
    bounds.offsetX * scaleFactor,
    bounds.offsetY * scaleFactor
  )

  g.updateTransform(1, 0, 0, 1, 0, 0)
  g.isNoRenderEmptyPlaceholder = true

  g.isInShowMode = true

  const isBgTransparent = g.isBgTransparent

  g.isBgTransparent = isBgTransparentForSp(sp)
  drawSlideElement(sp, g, true)

  g.isInPreview = false
  g.isBgTransparent = isBgTransparent
  g.resetState()

  return { image: im, bounds }
}

function getBoundsForShape(
  sp: SlideElement,
  isForPreview = false
): WithBounds & {
  offsetX: number
  offsetY: number
  width: number
  height: number
} {
  const bounds = calcBoundsForSlideElement(sp, isForPreview)

  const strokeWidth =
    (sp.pen?.w == null ? 12700 : toInt(sp.pen.w)) / ScaleOfPPTXSizes
  const borderOffset = strokeWidth / 2

  let width = bounds.maxX - bounds.minX
  let height = bounds.maxY - bounds.minY
  width = width < sp.extX ? sp.extX : width
  height = height < sp.extY ? sp.extY : height
  const widthInPx = width + 1
  const heightInPx = height + 1

  let offsetX = borderOffset

  if (0 > bounds.minX + borderOffset + 0.001) {
    offsetX += -bounds.minX - borderOffset
  }

  let offsetY = borderOffset

  if (0 > bounds.minY + borderOffset + 0.001) {
    offsetY += -bounds.minY - borderOffset
  }

  return {
    minX: bounds.minX,
    maxX: bounds.maxX,
    minY: bounds.minY,
    maxY: bounds.maxY,
    offsetX,
    offsetY,
    width: widthInPx + borderOffset,
    height: heightInPx + borderOffset,
  }
}

function calcBoundsForSlideElement(
  sp: SlideElement,
  isForPreview = false
): WithBounds {
  if (sp.instanceType !== InstanceType.GroupShape) {
    const checker = new GraphicsBoundsChecker()
    if (isForPreview === true) {
      checker.isInPreview = true
    }
    checker.initWithScale(1, 1)
    checker.transform(1, 0, 0, 1, 0, 0)
    checker.needCheckLineWidth = true
    checker.checkSpLineWidth(sp)
    checkBounds_SlideElement(sp, checker, invertTransformConverter)
    checker.correctBoundsWithSx()
    if (isForPreview === true) {
      checker.isInPreview = false
    }
    return {
      minX: checker.bounds.minX,
      minY: checker.bounds.minY,
      maxX: checker.bounds.maxX,
      maxY: checker.bounds.maxY,
    }
  } else {
    const sp_tree = sp.spTree
    const maxXs: number[] = [],
      maxYs: number[] = [],
      minXs: number[] = [],
      minYs: number[] = []
    for (let i = 0; i < sp_tree.length; ++i) {
      const sub_sp = sp_tree[i]
      const bounds = calcBoundsForSlideElement(sub_sp, isForPreview)
      maxXs.push(bounds.maxX)
      minXs.push(bounds.minX)
      maxYs.push(bounds.maxY)
      minYs.push(bounds.minY)
    }

    if (!sp.group) {
      maxXs.push(sp.extX)
      minXs.push(0)
      maxYs.push(sp.extY)
      minYs.push(0)
    }

    const minX = Math.min(...minXs)
    const minY = Math.min(...minYs)
    const maxX = Math.max(...maxXs)
    const maxY = Math.max(...maxYs)
    return {
      minX,
      minY,
      maxX,
      maxY: maxY,
    }
  }
}
export function getBase64ImgForSlideElement(sp: SlideElement) {
  if (typeof sp.base64Img === 'string') {
    return sp.base64Img
  }
  if (
    !isNumber(sp.x) ||
    !isNumber(sp.y) ||
    !isNumber(sp.extX) ||
    !isNumber(sp.extY) ||
    (checkAlmostEqual(sp.extX, 0) && checkAlmostEqual(sp.extY, 0))
  ) {
    return ''
  }
  const imgObj = convertSpToImage(sp)
  if (imgObj) {
    if (imgObj.imageCanvas) {
      try {
        sp.base64ImgWidth = imgObj.imageCanvas.width
        sp.base64ImgHeight = imgObj.imageCanvas.height
      } catch (e) {
        sp.base64ImgWidth = 50
        sp.base64ImgHeight = 50
      }
    }
    return imgObj.imageUrl
  } else {
    return ''
  }
}
// helper
function convertSpToImage(sp: SlideElement) {
  const checker = new GraphicsBoundsChecker()
  checker.isInPreview = true

  const factor = Factor_mm_to_pix
  const w_mm = 210
  const h_mm = 297
  const w_px = (w_mm * factor) >> 0
  const h_px = (h_mm * factor) >> 0

  checker.init(w_px, h_px, w_mm, h_mm)
  checker.transform(1, 0, 0, 1, 0, 0)

  checker.needCheckLineWidth = true
  checker.checkSpLineWidth(sp)
  checkBounds_SlideElement(sp, checker)
  checker.correctBoundsWithSx()

  checker.isInPreview = false
  const needPxWidth = checker.bounds.maxX - checker.bounds.minX + 1
  const needPxHeight = checker.bounds.maxY - checker.bounds.minY + 1

  if (needPxWidth <= 0 || needPxHeight <= 0) return null

  const canvas = document.createElement('canvas')
  canvas.width = needPxWidth
  canvas.height = needPxHeight

  const ctx = canvas.getContext('2d')!

  const drawCxt = getCanvasDrawer(ctx, w_px, h_px, w_mm, h_mm)

  drawCxt.isInPreview = true
  drawCxt.setCoordTransform(-checker.bounds.minX, -checker.bounds.minY)

  drawCxt.updateTransform(1, 0, 0, 1, 0, 0)

  drawCxt.isBgTransparent = isBgTransparentForSp(sp)
  drawSlideElement(sp, drawCxt)

  drawCxt.isBgTransparent = false
  drawCxt.isInPreview = false
  drawCxt.resetState()
  const ret = { imageCanvas: canvas, imageUrl: '' }
  try {
    ret.imageUrl = canvas.toDataURL('image/png')
  } catch (err) {
    if (
      sp.brush != null &&
      sp.brush.fill &&
      (sp.brush.fill as any).imageSrcId
    ) {
      ret.imageUrl = checkImageSrc((sp.brush.fill as any).imageSrcId)
    } else ret.imageUrl = ''
  }
  return ret
}

function convertSpsToImage(sps: SlideElement[]) {
  if (sps.length < 1) return ''
  const checker = new GraphicsBoundsChecker()
  checker.isInPreview = true
  const factor = Factor_mm_to_pix
  const w_mm = 210
  const h_mm = 297
  const w_px = (w_mm * factor) >> 0
  const h_px = (h_mm * factor) >> 0

  checker.init(w_px, h_px, w_mm, h_mm)
  checker.transform(1, 0, 0, 1, 0, 0)

  checker.needCheckLineWidth = true
  for (const sp of sps) {
    checker.checkSpLineWidth(sp)
    checkBounds_SlideElement(sp, checker)
    checker.correctBoundsWithSx()
  }
  checker.isInPreview = false
  const { minX, maxX, minY, maxY } = checker.bounds
  const needPxWidth = maxX - minX + 1
  const needPxHeight = maxY - minY + 1
  if (needPxWidth <= 0 || needPxHeight <= 0) return null

  const canvas = document.createElement('canvas')
  canvas.width = needPxWidth
  canvas.height = needPxHeight

  const ctx = canvas.getContext('2d')!
  const drawCxt = getCanvasDrawer(ctx, w_px, h_px, w_mm, h_mm)
  drawCxt.isInPreview = true
  drawCxt.setCoordTransform(-minX, -minY)
  drawCxt.updateTransform(1, 0, 0, 1, 0, 0)

  for (const sp of sps) {
    drawCxt.isBgTransparent = isBgTransparentForSp(sp)
    drawSlideElement(sp, drawCxt)

    drawCxt.isBgTransparent = false
  }

  drawCxt.isInPreview = false
  drawCxt.resetState()

  const ret = { imageCanvas: canvas, imageUrl: '' }
  try {
    ret.imageUrl = canvas.toDataURL('image/png')
  } catch (err) {
    ret.imageUrl = ''
  }
  return ret
}

export function getBase64ImgForSlideElements(sps: SlideElement[]) {
  const imgObj = convertSpsToImage(sps) ?? ''
  if (imgObj) {
    if (imgObj.imageCanvas) {
      try {
        const { imageCanvas, imageUrl } = imgObj
        const { width, height } = imageCanvas
        return { src: imageUrl, width, height }
      } catch (e) {}
    }
    return ''
  } else {
    return ''
  }
}
