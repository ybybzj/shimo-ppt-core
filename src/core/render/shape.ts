import { SlideElement } from '../SlideElement/type'
import { checkAlmostEqual, isRotateIn45D } from '../common/utils'
import { CanvasDrawer } from '../graphic/CanvasDrawer'
import { Matrix2D, MatrixUtils } from '../graphic/Matrix'
import { ClipRect } from '../graphic/type'
import { toggleCxtTransformReset } from '../graphic/utils'
import { TextOverflowType } from '../SlideElement/const'
import { Shape } from '../SlideElement/Shape'
import {
  isPlaceholderWithEmptyContent,
  isTextContentSelected,
} from '../utilities/shape/asserts'
import {
  getFlipTransformForSp,
  getInvertTransformForConvert,
} from '../utilities/shape/draw'
import { checkTextTransformMatrix } from '../utilities/shape/getters'
import { transformUpdater } from '../utilities/shape/type'
import { drawShape } from './drawShape/drawShape'
import { ShapeRenderingState } from '../graphic/ShapeRenderingState'
import { renderTextBodyContent } from './textBody'

export function renderShape(
  shape: Shape,
  graphics: CanvasDrawer,
  transformUpdater?: transformUpdater
) {
  if (graphics.context == null) {
    return
  }
  const isUseTransformUpdater = !!transformUpdater
  const transform = transformUpdater
    ? transformUpdater(shape, graphics, shape.transform)
    : shape.transform
  const textTransform = shape.textTransform
  const geometry = shape.calcedGeometry || (shape.spPr && shape.spPr.geometry)

  // draw background fill
  if (
    geometry ||
    shape.style ||
    shape.brush?.fill ||
    shape.pen?.fillEffects?.fill ||
    shape.attrUseBgFill === true
  ) {
    toggleCxtTransformReset(graphics, false)
    graphics.applyTransformMatrix(transform, false)
    graphics.saveState()
    drawShape(
      shape,
      graphics,
      geometry,
      shape.attrUseBgFill && isUseTransformUpdater
        ? beforeDrawBgFill
        : undefined
    )
    graphics.restoreState()
  }

  const isTextSelected = isTextContentSelected(shape)
  // draw border for empty placeholder
  if (
    isPlaceholderWithEmptyContent(shape) &&
    !shape.pen?.fillEffects?.fill &&
    graphics.isNoRenderEmptyPlaceholder !== true &&
    !graphics.isInPreview
  ) {
    renderEmptyPlaceholderBorder(shape, graphics, transform, isTextSelected)
    renderPHTxBodyForShape(shape, graphics, transform, isTextSelected)
  } else {
    // draw text content
    renderTxBodyForShape(
      shape,
      graphics,
      transform,
      textTransform,
      isUseTransformUpdater,
      isTextSelected
    )
  }

  toggleCxtTransformReset(graphics, true)
  graphics.reset()
}

function beforeDrawBgFill(state: ShapeRenderingState, sp: SlideElement) {
  const invertTransform = getInvertTransformForConvert(sp)
  const flipTr = getFlipTransformForSp(sp)
  MatrixUtils.multiplyMatrixes(invertTransform, flipTr)
  state.useBgFillMatrix = invertTransform
}

function renderEmptyPlaceholderBorder(
  shape: Shape,
  graphics: CanvasDrawer,
  transform: Matrix2D,
  isTextSelected: boolean
) {
  const extX = shape.extX
  const extY = (shape.extY + 0.5) >> 0
  if (!isTextSelected) {
    const angle = transform.getRotDegree()
    if (
      checkAlmostEqual(angle, 0.0, 0.0) ||
      checkAlmostEqual(angle, 90.0, 0.0) ||
      checkAlmostEqual(angle, 180.0, 0.0) ||
      checkAlmostEqual(angle, 270.0, 0.0)
    ) {
      graphics.applyTransformMatrix(transform, false)
      toggleCxtTransformReset(graphics, true)

      graphics.setLineWidth(1)
      graphics.setPenColor(127, 127, 127, 255)

      graphics._begin()

      graphics.drawAclinicRectWithDashBorder(0, 0, extX, extY, 2, 2)
      graphics._end()
    } else {
      graphics.applyTransformMatrix(transform, false)
      toggleCxtTransformReset(graphics, true)

      const r = extX
      const b = extY

      graphics.setLineWidth(1)
      graphics.setPenColor(127, 127, 127, 255)

      graphics._begin()
      graphics.drawRectWithDashBorder(0, 0, r, b, 3, 1)
      graphics._end()
    }
  } else {
    toggleCxtTransformReset(graphics, false)
    graphics.setPenWidth(70)
    graphics.applyTransformMatrix(transform, false)
    graphics.setPenColor(0, 0, 0, 255)
    graphics._begin()
    graphics._moveTo(0, 0)
    graphics._lineTo(extX, 0)
    graphics._lineTo(extX, extY)
    graphics._lineTo(0, extY)
    graphics._close()
    graphics.drawStroke()

    toggleCxtTransformReset(graphics, true)
  }
}

function renderTxBodyForShape(
  shape: Shape,
  graphics: CanvasDrawer,
  transform: Matrix2D,
  textTransform: Matrix2D,
  isUseTransformUpdater: boolean,
  isTextSelected: boolean
) {
  const txBody = shape.txBody
  const isNotRect = shape.getGeometry()?.preset !== 'rect'
  if (txBody?.textDoc?.isEmpty({ ignoreEnd: isNotRect }) === false) {
    let textMatrix = textTransform
    let clipRect

    if (isUseTransformUpdater) {
      textMatrix = textMatrix.clone()
      clipRect = checkTextTransformMatrix(
        shape,
        textMatrix,
        txBody.textDoc,
        shape.getBodyPr(),
        {
          transform,
          rot: 0,
        }
      )
    }

    graphics.saveState()
    toggleCxtTransformReset(graphics, false)
    if (!isTextSelected) {
      clipTextRect(shape, graphics, {
        transform,
        clipRect,
        textTransform: textMatrix,
      })
    }

    graphics.applyTransformMatrix(textMatrix, true)

    toggleCxtTransformReset(graphics, true)
    renderTextBodyContent(txBody, txBody.textDoc, graphics)
    graphics.restoreState()
  }
}

function renderPHTxBodyForShape(
  shape: Shape,
  graphics: CanvasDrawer,
  transform: Matrix2D,
  isTextSelected: boolean
) {
  const txBody = shape.txBody
  if (txBody) {
    if (
      txBody.phContent != null &&
      !txBody.isCurrentPlaceholder() &&
      shape.textTransformForPh
    ) {
      const textMatrix = shape.textTransformForPh
      graphics.saveState()
      toggleCxtTransformReset(graphics, false)
      if (!isTextSelected) {
        clipTextRect(shape, graphics, {
          transform,
          textTransform: textMatrix,
        })
      }

      graphics.applyTransformMatrix(textMatrix, true)

      toggleCxtTransformReset(graphics, true)
      renderTextBodyContent(txBody, txBody.phContent, graphics)
      graphics.restoreState()
    }
  }
}

function clipTextRect(
  shape: Shape,
  graphics: CanvasDrawer,
  opts?: {
    transform?: Matrix2D
    textTransform?: Matrix2D
    clipRect?: ClipRect
  }
) {
  opts = opts ?? {}

  const clipRect = opts.clipRect ?? shape.clipRect
  const transform = opts.transform ?? shape.transform
  const textTransform = opts.textTransform ?? shape.textTransform
  if (clipRect) {
    const { x, y, w, h } = clipRect
    const bodyPr = shape.getBodyPr()
    if (bodyPr.vertOverflow === TextOverflowType.Overflow) {
      return
    }
    if (!bodyPr || !bodyPr.upright) {
      graphics.applyTransformMatrix(transform)
      graphics.addClipRect(x, y, w, h)

      toggleCxtTransformReset(graphics, false)
      graphics.applyTransformMatrix(textTransform, true)
    } else {
      const transform = new Matrix2D()
      const cX = transform.XFromPoint(shape.extX / 2, shape.extY / 2)
      const cY = transform.YFromPoint(shape.extX / 2, shape.extY / 2)

      if (isRotateIn45D(shape.rot)) {
        transform.tx = cX - shape.extX / 2
        transform.ty = cY - shape.extY / 2
      } else {
        MatrixUtils.translateMatrix(transform, -shape.extX / 2, -shape.extY / 2)
        MatrixUtils.rotateMatrix(transform, Math.PI / 2)
        MatrixUtils.translateMatrix(transform, cX, cY)
      }
      graphics.applyTransformMatrix(transform, true)
      graphics.addClipRect(x, y, w, h)

      toggleCxtTransformReset(graphics, false)
      graphics.applyTransformMatrix(textTransform, true)
    }
  }
}
