import { CanvasDrawer } from '../graphic/CanvasDrawer'

export function renderSearchResult(
  g: CanvasDrawer,
  x: number,
  y: number,
  w: number,
  h: number
) {
  g.setBrushColor(255, 215, 109, 122)
  g.rect(x, y, w, h)
  g.drawFill()
}
