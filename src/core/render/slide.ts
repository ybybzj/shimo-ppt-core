import { Nullable } from '../../../liber/pervasive'
import { CanvasDrawer } from '../graphic/CanvasDrawer'
import { toggleCxtTransformReset } from '../graphic/utils'
import { Presentation } from '../Slide/Presentation'
import { Slide } from '../Slide/Slide'
import { ShapeRenderingInfo } from './drawShape/type'
import { drawShapeByInfo } from './drawShape/drawShape'
import { drawSlideElement } from './drawSlideElement'
import { FillEffects } from '../SlideElement/attrs/fill/fill'
import { startPerfTiming } from '../../common/utils'
import { hookManager, Hooks } from '../hooks'
import { Bounds } from '../graphic/Bounds'

export function renderPresentationSlide(
  presentation: Presentation,
  slideIndex: number,
  canvasDrawer: CanvasDrawer,
  viewBounds?: Bounds
) {
  const slide = presentation.slides[slideIndex]
  if (slide) {
    const getRenderSlideTiming = startPerfTiming()
    renderSlide(slide, canvasDrawer, viewBounds)
    const renderSlideTiming = getRenderSlideTiming()
    if (renderSlideTiming > 100) {
      hookManager.invoke(Hooks.Document.TrackPerf, {
        type: 'RENDER_SLIDE',
        timing: getRenderSlideTiming(),
      })
    }
  }
}

export function renderSlide(
  slide: Slide,
  g: CanvasDrawer,
  viewBounds?: Bounds
) {
  renderSlideStaticContent(slide, g, viewBounds)
  renderShapesOfSlide(slide, g, viewBounds)
}

export function renderSlideBackground(
  graphics: CanvasDrawer,
  fillEffects: Nullable<FillEffects>,
  w: number,
  h: number
) {
  // first of all draw a white rect!
  // well, some kind of background must be
  toggleCxtTransformReset(graphics, false)
  const l = 0
  const t = 0
  const r = 0 + w
  const b = 0 + h

  graphics._begin()
  graphics._moveTo(l, t)
  graphics._lineTo(r, t)
  graphics._lineTo(r, b)
  graphics._lineTo(l, b)
  graphics._close()

  graphics.setBrushColor(255, 255, 255, 255)
  graphics.drawFill()
  graphics._end()

  if (fillEffects == null || fillEffects.fill == null) return

  toggleCxtTransformReset(graphics, false)
  const renderingInfo: ShapeRenderingInfo = {
    brush: fillEffects,
    extX: w,
    extY: h,
  }

  drawShapeByInfo(renderingInfo, graphics)
}

export function renderSlideStaticContent(
  slide: Slide,
  canvasDrawer: CanvasDrawer,
  viewBounds?: Bounds
) {
  const showMasterSp = slide.showMasterSp
  const layout = slide.layout
  const layoutShowMasterSp = layout?.showMasterSp
  const shouldShowMasterSp =
    showMasterSp === true ||
    (showMasterSp !== false && layoutShowMasterSp !== false)

  renderSlideBackground(
    canvasDrawer,
    slide.backgroundFill,
    slide.width,
    slide.height
  )

  if (shouldShowMasterSp) {
    layout?.master?.draw(canvasDrawer, slide, viewBounds)
  }

  if (showMasterSp !== false) {
    layout?.draw(canvasDrawer, slide, viewBounds)
  }
}

function renderShapesOfSlide(slide: Slide, canvasDrawer: CanvasDrawer, viewBounds?: Bounds) {
  const spTree = slide.cSld.spTree
  for (let i = 0, l = spTree.length; i < l; i++) {
    drawSlideElement(spTree[i], canvasDrawer, undefined, viewBounds)
  }
}
