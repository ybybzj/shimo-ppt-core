import { drawTextDocument } from '../TextDocument/typeset/draw'
import { CanvasDrawer } from '../graphic/CanvasDrawer'
import { TextBody } from '../SlideElement/TextBody'
import { isEavertTextContent } from '../utilities/shape/getters'
import { TextDocument } from '../TextDocument/TextDocument'

export function renderTextBodyContent(
  txBody: TextBody,
  content: TextDocument,
  g: CanvasDrawer
) {
  const isEaVert = isEavertTextContent(txBody)
  if (isEaVert) {
    g.setEaVertTextFlag(true)
  }

  drawTextDocument(content, g)

  if (isEaVert) {
    g.setEaVertTextFlag(false)
  }
}
