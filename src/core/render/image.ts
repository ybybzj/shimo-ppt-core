import { InstanceType } from '../instanceTypes'
import { toggleCxtTransformReset } from '../graphic/utils'
import { hookManager, Hooks } from '../hooks'
import { ImageShape } from '../SlideElement/Image'
import { setRenderingContentPenBrush } from '../utilities/shape/image'
import { transformUpdater } from '../utilities/shape/type'
import { drawShape } from './drawShape/drawShape'
import { SpRenderingContext } from './drawShape/type'

export function renderImageShape(
  imageSp: ImageShape,
  graphics: SpRenderingContext,
  transformUpdater?: transformUpdater
) {
  // 裁剪时不应再绘制原图但 bounds 仍需计算
  const sp = imageSp
  if (
    !hookManager.get(Hooks.EditorUI.GetSlideElementNeedDraw, {
      sp,
      graphics,
    })
  ) {
    return
  }

  const transform = transformUpdater
    ? transformUpdater(imageSp, graphics, imageSp.transform)
    : imageSp.transform
  toggleCxtTransformReset(graphics, false)
  graphics.applyTransformMatrix(transform, false)

  if (imageSp.pen || imageSp.brush) {
    drawShape(imageSp, graphics, imageSp.calcedGeometry)
  }

  const orgBrush = imageSp.brush
  const orgPen = imageSp.pen

  setRenderingContentPenBrush(imageSp)

  if (graphics.instanceType === InstanceType.CanvasDrawer) {
    graphics.imageWidgetType = hookManager.get(Hooks.Attrs.GetImageWidgetType, {
      sp: imageSp,
    })
  }
  drawShape(imageSp, graphics, imageSp.calcedGeometry)
  if (graphics.instanceType === InstanceType.CanvasDrawer) {
    graphics.imageWidgetType = null
  }

  imageSp.brush = orgBrush
  imageSp.pen = orgPen

  graphics.reset()
  toggleCxtTransformReset(graphics, true)
}
