import { Table } from '../Table/Table'
import { LineDrawingRule, ShdType } from '../common/const/attrs'
import { Nullable } from '../../../liber/pervasive'
import { Matrix2D, MatrixUtils } from '../graphic/Matrix'
import { CanvasDrawer } from '../graphic/CanvasDrawer'
import { toggleCxtTransformReset } from '../graphic/utils'
import { getReversedArray } from '../Table/utils/utils'
import { ShapeRenderingInfo } from './drawShape/type'
import { drawShapeByInfo } from './drawShape/drawShape'
import { drawTextDocument } from '../TextDocument/typeset/draw'
import { TableCell } from '../Table/TableCell'
import { ManagedSliceUtil } from '../../common/managedArray'
import { defaultRGBAColor } from '../color/complexColor'
import {
  TCBorderValue,
  TableTextDirection,
  VMergeType,
} from '../common/const/table'

export function renderTable(
  table: Table,
  drawer: CanvasDrawer,
  transform?: Matrix2D
) {
  const lastRow = table.lastRow
  const { h, diffYOfTop } = table.rowsInfo[lastRow]
  if (0 === lastRow && Math.abs(h - diffYOfTop) < 0.001) {
    return
  }

  drawer.saveState()
  _startRender(drawer)

  _renderTableBackgroundAndOuterBorder(table, drawer)
  _renderCellsBackground(table, drawer, transform)
  _renderCellsContent(table, drawer)
  drawer.save()
  drawer.setPenDash(null) // 暂时统一处理，之后可按属性来分别设置
  _renderCellsBorders(table, drawer)
  drawer.restore()
  _endRender(drawer)

  drawer.restoreState()
}

function _startRender(drawer: CanvasDrawer) {
  drawer.saveState()
  if (
    !drawer.isCxtTransformReset &&
    MatrixUtils.isOnlyTranslate(drawer.transform)
  ) {
    drawer.saveState()
    drawer.setCxtTransformReset(true)
    return true
  }
  return false
}

function _endRender(drawer: CanvasDrawer) {
  drawer.restoreState()
}

function _renderTableBackgroundAndOuterBorder(
  table: Table,
  drawer: CanvasDrawer
) {
  if (table.children.length <= 0) return

  const shd = table.getShd()!
  const { top, bottom } = table.dimension.bounds
  const { x } = table.dimension
  const row = table.children.at(0)!
  const cellsCount = row.cellsCount
  const lastIndex = cellsCount - 1
  const left = x + row.getCellInfo(table.isRTL ? lastIndex : 0).cellStartX
  const right = x + row.getCellInfo(table.isRTL ? 0 : lastIndex).cellEndX
  drawer.saveState()
  toggleCxtTransformReset(drawer, false)
  shd.fillEffects &&
    shd.fillEffects.check(table.getTheme(), table.getColorMap())
  const transform = table.parent.transform.clone()
  MatrixUtils.translateMatrix(transform, Math.min(left, right), top)
  drawer.applyTransformMatrix(transform, false)

  const spToDraw: ShapeRenderingInfo = {
    brush: shd.fillEffects,
    pen: null,
    extX: Math.abs(right - left),
    extY: Math.abs(bottom - top),
    matrix: transform,
  }

  drawShapeByInfo(spToDraw, drawer)

  drawer.restoreState()
}

function _renderCellsBackground(
  table: Table,
  drawer: CanvasDrawer,
  transform?: Matrix2D
) {
  const theme = table.getTheme()
  const colorMap = table.getColorMap()

  drawer.saveState()
  toggleCxtTransformReset(drawer, false)

  const dimension = table.dimension
  ManagedSliceUtil.forEach(table.children, (row, ci) => {
    const cellsCount = row.cellsCount
    const y = table.rowsInfo[ci].y

    for (let cellIndex = cellsCount - 1; cellIndex >= 0; cellIndex--) {
      const curCell = row.getCellByIndex(cellIndex)!
      let { startGridCol, cellStartX, cellEndX } = row.getCellInfo(cellIndex)
      const gridSpan = curCell.getGridSpan()
      const vMerge = curCell.getVMerge()

      if (VMergeType.Continue === vMerge) {
        continue
      }

      cellStartX = dimension.x + cellStartX
      cellEndX = dimension.x + cellEndX

      const verticalMergeCount = table.getVertMergeCount(
        ci,
        startGridCol,
        gridSpan
      )
      if (verticalMergeCount <= 0) continue

      const actulaHeight =
        table.rowsInfo[ci + verticalMergeCount - 1].y +
        table.rowsInfo[ci + verticalMergeCount - 1].h -
        y

      // FIXME：尝试修复无边框时，绘制单元格背景之间留有空隙
      const addExtX = cellIndex === cellsCount - 1 ? 0 : 0.2
      const addHeight = ci === table.lastRow ? 0 : 0.2
      // Fill the cell
      const cellShd = curCell.getShd()
      if (ShdType.Nil !== cellShd.value) {
        if (cellShd.fillEffects && cellShd.fillEffects.fill) {
          cellShd.fillEffects.check(theme, colorMap)
          const _transform = (transform ?? table.parent.transform).clone()
          MatrixUtils.translateMatrix(
            _transform,
            Math.min(cellStartX, cellEndX),
            Math.min(y, y + actulaHeight)
          )
          drawer.applyTransformMatrix(_transform, false)

          const spToDraw: ShapeRenderingInfo = {
            brush: cellShd.fillEffects,
            pen: null,
            extX: Math.abs(cellEndX - cellStartX) + addExtX,
            extY: Math.abs(actulaHeight) + addHeight,
            matrix: _transform,
          }

          drawShapeByInfo(spToDraw, drawer)
        }
      }
    }
  })

  drawer.restoreState()
}
function _renderCellsContent(table: Table, drawer: CanvasDrawer) {
  ManagedSliceUtil.forEach(table.children, (row, ci) => {
    const cellsCount = row.cellsCount
    for (let cellIndex = 0; cellIndex < cellsCount; cellIndex++) {
      const curCell = row.getCellByIndex(cellIndex)!
      const gridSpan = curCell.getGridSpan()
      const vMerge = curCell.getVMerge()
      const { startGridCol } = row.getCellInfo(cellIndex)

      if (VMergeType.Continue === vMerge) {
        continue
      }

      const verticalMergeCount = table.getVertMergeCount(
        ci,
        startGridCol,
        gridSpan
      )
      if (verticalMergeCount <= 0) continue

      const needDrawContent = curCell.textDoc.isEmpty() !== true
      if (needDrawContent) {
        _drawCellContent(curCell, drawer)
      }
    }
  })
}

function _drawCellContent(cell: TableCell, g: CanvasDrawer) {
  const textDirection = cell.getTextDirection()
  const { cellStartX, cellStartY, cellEndX, cellEndY } = cell.calculateInfo
  let isNeedRestore = false
  if (
    TableTextDirection.BTLR === textDirection ||
    TableTextDirection.TBRL === textDirection
  ) {
    isNeedRestore = true
    g.saveState()
    g.addClipRect(
      cellStartX!,
      cellStartY!,
      cellEndX! - cellStartX!,
      cellEndY! - cellStartY!
    )
    const transform = cell.getTextTransformOfParent()!
    g.applyTransformMatrix(transform)
  }

  drawTextDocument(cell.textDoc, g)
  if (isNeedRestore) {
    g.restoreState()
  }
}

function _renderCellsBorders(table: Table, drawer: CanvasDrawer) {
  const isRTL = table.isRTL
  const reducedTableGrid = table.getTableGridReduced()
  const dimension = table.dimension
  const theme = table.getTheme()
  const colorMap = table.getColorMap()
  const rgba = defaultRGBAColor()
  const lastRow = table.lastRow
  ManagedSliceUtil.forEach(table.children, (row, ri) => {
    const cellsCount = row.cellsCount
    const { y } = table.rowsInfo[ri]

    let finalTopBorderSize: number = 0

    for (let i = cellsCount - 1; i >= 0; i--) {
      const cellIndex = isRTL ? cellsCount - i - 1 : i
      const curCell = row.getCellByIndex(cellIndex)!
      let { startGridCol, curGridCol, cellStartX, cellEndX } =
        row.getCellInfo(cellIndex)
      const gridSpan = curCell.getGridSpan()
      const vMergeType = curCell.getVMerge()

      if (
        VMergeType.Continue === vMergeType &&
        table.getVertMergeCount(ri, startGridCol, gridSpan) > 1
      ) {
        finalTopBorderSize = 0
        continue
      }

      cellStartX = dimension.x + cellStartX
      cellEndX = dimension.x + cellEndX
      if (isRTL) {
        const t = cellEndX
        cellEndX = cellStartX
        cellStartX = t
      }
      const vMergeCount = table.getVertMergeCount(ri, startGridCol, gridSpan)
      if (vMergeCount <= 0) {
        finalTopBorderSize = 0
        continue
      }

      const actualHeight =
        table.rowsInfo[ri + vMergeCount - 1].y +
        table.rowsInfo[ri + vMergeCount - 1].h -
        y

      const borderInfo = curCell.getBorderInfo()
      const topBorderInfo = getReversedArray(borderInfo.top, isRTL)
      const bottomBorderInfo = getReversedArray(borderInfo.bottom, isRTL)
      const leftBorderInfo = borderInfo.left
      const rightBorderInfo = borderInfo.right

      const tmpCurRowIndex = curCell.parent.index
      const rowBorderStartIndex = tmpCurRowIndex < 0 ? 0 - tmpCurRowIndex : 0
      let rowBorderEndIndex = 0
      if (leftBorderInfo) {
        rowBorderEndIndex =
          leftBorderInfo.length - 1 + tmpCurRowIndex > lastRow
            ? lastRow - tmpCurRowIndex + 1
            : leftBorderInfo.length - 1
        for (let i = rowBorderStartIndex; i <= rowBorderEndIndex; i++) {
          const curerntBorderInfo = leftBorderInfo[i]
          const y0 = table.rowsInfo[tmpCurRowIndex + i].y
          const y1 =
            table.rowsInfo[tmpCurRowIndex + i].y +
            table.rowsInfo[tmpCurRowIndex + i].h

          if (TCBorderValue.Single === curerntBorderInfo.value) {
            curerntBorderInfo.getColorByTheme(theme, colorMap, rgba)
            drawer.setPenColor(rgba.r, rgba.g, rgba.b, rgba.a)

            drawer.drawVerticalLine(
              LineDrawingRule.Center,
              cellStartX,
              y0,
              y1,
              curerntBorderInfo.size!
            )
          }
        }
      }

      if (rightBorderInfo) {
        for (let i = rowBorderStartIndex; i <= rowBorderEndIndex; i++) {
          const currentBorderInfo = rightBorderInfo[i]
          const y0 = table.rowsInfo[tmpCurRowIndex + i].y
          const y1 =
            table.rowsInfo[tmpCurRowIndex + i].y +
            table.rowsInfo[tmpCurRowIndex + i].h

          const tmpCellIndex = table.getCellIndexByGridColIndex(
            tmpCurRowIndex + i,
            startGridCol
          )
          const tmpCellsCount = table.children.at(tmpCurRowIndex + i)
            ?.cellsCount

          if (tmpCellsCount != null && tmpCellsCount - 1 === tmpCellIndex) {
            if (TCBorderValue.Single === currentBorderInfo.value) {
              currentBorderInfo.getColorByTheme(theme, colorMap, rgba)
              drawer.setPenColor(rgba.r, rgba.g, rgba.b, rgba.a)

              drawer.drawVerticalLine(
                LineDrawingRule.Center,
                cellEndX,
                y0,
                y1,
                currentBorderInfo.size!
              )
            }
          }
        }
      }

      if (topBorderInfo && VMergeType.Continue !== vMergeType) {
        const prevLastTopBorderSize = finalTopBorderSize
        for (
          let borderInfoIndex = 0;
          borderInfoIndex < topBorderInfo.length;
          borderInfoIndex++
        ) {
          const currentBorderInfo = topBorderInfo[borderInfoIndex]
          const x0 =
            dimension.x + reducedTableGrid[borderInfoIndex + curGridCol - 1]
          const x1 =
            dimension.x + reducedTableGrid[borderInfoIndex + curGridCol]

          let leftMaxWidth = 0
          let rightMaxWidth = 0
          // last grid in cell
          if (topBorderInfo.length - 1 === borderInfoIndex) {
            let maxRight = 0
            if (ri > 0) {
              const rowAbove = table.children.at(ri - 1)!
              const l = rowAbove.cellsCount
              for (let i = 0; i < l; i++) {
                let cellAbove = rowAbove.getCellByIndex(i)!
                const startGridOfCellAbove =
                  rowAbove.getCellInfo(i).startGridCol
                const gridSpanOfCellAbove = cellAbove.getGridSpan()

                let isLeft: Nullable<boolean>
                if (startGridOfCellAbove === i + startGridCol + 1) {
                  isLeft = true
                } else if (
                  startGridOfCellAbove + gridSpanOfCellAbove ===
                  i + startGridCol + 1
                ) {
                  isLeft = false
                } else if (startGridOfCellAbove > startGridCol) break

                if (null != isLeft) {
                  const vMergeOfPrev = cellAbove.getVMerge()
                  if (VMergeType.Continue === vMergeOfPrev) {
                    cellAbove = table._getTopLeftMergedCell(
                      ri - 1,
                      startGridOfCellAbove,
                      gridSpanOfCellAbove
                    )!
                  }

                  if (null === cellAbove) break

                  const n = ri - 1 - cellAbove.parent.index
                  if (n < 0) break

                  if (true === isLeft) {
                    const prevLeftCellBorderInfo =
                      cellAbove.getBorderInfo().left
                    if (
                      null != prevLeftCellBorderInfo &&
                      prevLeftCellBorderInfo.length > n &&
                      TCBorderValue.Single === prevLeftCellBorderInfo[n].value
                    ) {
                      maxRight = prevLeftCellBorderInfo[n].size! / 2
                    }
                  } else {
                    const prevRightCellBorderInfo =
                      cellAbove.getBorderInfo().right
                    if (
                      null != prevRightCellBorderInfo &&
                      prevRightCellBorderInfo.length > n &&
                      TCBorderValue.Single === prevRightCellBorderInfo[n].value
                    ) {
                      maxRight = prevRightCellBorderInfo[n].size! / 2
                    }
                  }

                  break
                }
              }
            }

            if (
              rightBorderInfo &&
              rightBorderInfo.length > 0 &&
              TCBorderValue.Single === rightBorderInfo[0].value &&
              rightBorderInfo[0].size! / 2 > maxRight
            ) {
              maxRight = rightBorderInfo[0].size! / 2
            }

            if (
              TCBorderValue.Single === currentBorderInfo.value &&
              currentBorderInfo.size! > prevLastTopBorderSize
            ) {
              rightMaxWidth = maxRight
            } else {
              rightMaxWidth = -maxRight
            }

            const lastCell = row.getCellByIndex(cellsCount - 1)
            if (0 !== ri && lastCell) {
              const isLastTopBorder =
                row.getCellInfo(cellsCount - 1).startGridCol +
                  lastCell.getGridSpan() ===
                borderInfoIndex + startGridCol + 1
              if (isLastTopBorder) {
                rightMaxWidth = -Math.abs(rightMaxWidth)
              }
            }

            const nextCellIndex = cellIndex + 1
            if (nextCellIndex < cellsCount) {
              const nextCell = row.getCellByIndex(nextCellIndex)
              if (nextCell) {
                const nextCellBordersInfo = nextCell.getBorderInfo()
                if (
                  nextCellBordersInfo.top!.length &&
                  nextCellBordersInfo.top![0]?.value !== TCBorderValue.None
                ) {
                  rightMaxWidth = 0
                }
              }
            }
          }

          if (0 === borderInfoIndex) {
            let maxLeft = 0
            if (ri > 0) {
              const rowAbove = table.children.at(ri - 1)!
              const l = rowAbove.cellsCount
              for (let i = 0; i < l; i++) {
                let cellAbove = rowAbove.getCellByIndex(i)!
                const startGridOfCellAbove =
                  rowAbove.getCellInfo(i).startGridCol
                const gridSpanOfCellAbove = cellAbove.getGridSpan()

                let isLeft: Nullable<boolean>
                if (startGridOfCellAbove === startGridCol) {
                  isLeft = true
                } else if (
                  startGridOfCellAbove + gridSpanOfCellAbove ===
                  startGridCol
                ) {
                  isLeft = false
                } else if (startGridOfCellAbove > startGridCol) break

                if (null != isLeft) {
                  const vMergeOfPrev = cellAbove.getVMerge()
                  if (VMergeType.Continue === vMergeOfPrev) {
                    cellAbove = table._getTopLeftMergedCell(
                      ri - 1,
                      startGridOfCellAbove,
                      gridSpanOfCellAbove
                    )!
                  }

                  if (null === cellAbove) break

                  const n = ri - 1 - cellAbove.parent.index
                  if (n < 0) break

                  if (true === isLeft) {
                    const prevLeftBorderInfo = cellAbove.getBorderInfo().left
                    if (
                      null != prevLeftBorderInfo &&
                      prevLeftBorderInfo.length > n &&
                      TCBorderValue.Single === prevLeftBorderInfo[n].value
                    ) {
                      maxLeft = prevLeftBorderInfo[n].size! / 2
                    }
                  } else {
                    const prevRightCellBorderInfo =
                      cellAbove.getBorderInfo().right
                    if (
                      null != prevRightCellBorderInfo &&
                      prevRightCellBorderInfo.length > n &&
                      TCBorderValue.Single === prevRightCellBorderInfo[n].value
                    ) {
                      maxLeft = prevRightCellBorderInfo[n].size! / 2
                    }
                  }

                  break
                }
              }
            }

            if (
              leftBorderInfo &&
              leftBorderInfo.length > 0 &&
              TCBorderValue.Single === leftBorderInfo[0].value &&
              leftBorderInfo[0].size! / 2 > maxLeft
            ) {
              maxLeft = leftBorderInfo[0].size! / 2
            }

            if (0 !== ri && cellIndex === 0) {
              leftMaxWidth = maxLeft
            } else {
              leftMaxWidth = -maxLeft
            }

            const preCellIndex = cellIndex - 1
            if (preCellIndex >= 0) {
              const preCell = row.getCellByIndex(preCellIndex)
              if (preCell) {
                const preCellBordersInfo = preCell.getBorderInfo()
                if (
                  preCellBordersInfo.top!.length &&
                  preCellBordersInfo.top![0]?.value !== TCBorderValue.None
                ) {
                  leftMaxWidth = 0
                }
              }
            }

            finalTopBorderSize = 0

            if (TCBorderValue.Single === currentBorderInfo.value) {
              finalTopBorderSize = currentBorderInfo.size
            }
          }

          if (TCBorderValue.Single === currentBorderInfo.value) {
            currentBorderInfo.getColorByTheme(theme, colorMap, rgba)
            drawer.setPenColor(rgba.r, rgba.g, rgba.b, rgba.a)

            drawer.drawHorizontalLineByExt(
              LineDrawingRule.Center,
              y,
              x0,
              x1,
              currentBorderInfo.size!,
              leftMaxWidth,
              rightMaxWidth
            )
          }
        }
      }

      if (bottomBorderInfo) {
        const bottomBorder = bottomBorderInfo[0]
        if (bottomBorder && TCBorderValue.Single === bottomBorder.value) {
          bottomBorder.getColorByTheme(theme, colorMap, rgba)
          drawer.setPenColor(rgba.r, rgba.g, rgba.b, rgba.a)

          let leftMaxWidth = 0
          if (
            leftBorderInfo &&
            leftBorderInfo.length > 0 &&
            TCBorderValue.Single ===
              leftBorderInfo[leftBorderInfo.length - 1].value
          ) {
            leftMaxWidth = -leftBorderInfo[leftBorderInfo.length - 1].size! / 2
          }

          let rightMaxWidth = 0
          if (
            rightBorderInfo &&
            rightBorderInfo.length > 0 &&
            TCBorderValue.Single ===
              rightBorderInfo[rightBorderInfo.length - 1].value
          ) {
            rightMaxWidth =
              +rightBorderInfo[rightBorderInfo.length - 1].size! / 2
          }

          if (ri !== lastRow && cellIndex === 0) {
            leftMaxWidth = Math.abs(leftMaxWidth)
          } else if (cellIndex === cellsCount - 1) {
            rightMaxWidth = -Math.abs(rightMaxWidth)
          }

          const x0 =
            ri !== lastRow
              ? dimension.x + reducedTableGrid[curGridCol - 1]
              : cellStartX
          const x1 =
            ri !== lastRow
              ? dimension.x + reducedTableGrid[curGridCol]
              : cellEndX

          drawer.drawHorizontalLineByExt(
            LineDrawingRule.Center,
            y + actualHeight,
            x0,
            x1,
            bottomBorder.size!,
            leftMaxWidth,
            rightMaxWidth
          )
        }
      }
    }
  })
}
