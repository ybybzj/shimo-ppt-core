import { InstanceType } from '../instanceTypes'
import { isSpWithinBounds } from '../checkBounds/utils'
import { Bounds } from '../graphic/Bounds'
import { CanvasDrawer } from '../graphic/CanvasDrawer'
import { toggleCxtTransformReset } from '../graphic/utils'
import { GroupShape } from '../SlideElement/GroupShape'
import { transformUpdater } from '../utilities/shape/type'
import { renderGraphicFrame } from './graphicFrame'
import { renderImageShape } from './image'
import { renderShape } from './shape'

export function renderGroup(
  group: GroupShape,
  g: CanvasDrawer,
  transformUpdater?: transformUpdater,
  viewBounds?: Bounds
) {
  for (let i = 0; i < group.spTree.length; ++i) {
    const sp = group.spTree[i]
    if (viewBounds && !isSpWithinBounds(sp, viewBounds)) {
      continue
    }
    switch (sp.instanceType) {
      case InstanceType.Shape:
        renderShape(sp, g, transformUpdater)
        break
      case InstanceType.GroupShape:
        renderGroup(sp, g, transformUpdater, viewBounds)
        break
      case InstanceType.GraphicFrame:
        renderGraphicFrame(sp, g, transformUpdater)
        break
      case InstanceType.ImageShape:
        renderImageShape(sp, g, transformUpdater)
        break
    }
  }

  g.reset()
  toggleCxtTransformReset(g, true)
}
