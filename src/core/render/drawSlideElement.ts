import { InstanceType } from '../instanceTypes'
import { SlideElement } from '../SlideElement/type'
import { isSpWithinBounds } from '../checkBounds/utils'
import { Bounds } from '../graphic/Bounds'
import { CanvasDrawer } from '../graphic/CanvasDrawer'
import { invertTransformConverter } from '../utilities/shape/draw'
import { renderGraphicFrame } from './graphicFrame'
import { renderGroup } from './group'
import { renderImageShape } from './image'
import { renderShape } from './shape'

export function drawSlideElement(
  sp: SlideElement,
  graphics: CanvasDrawer,
  withoutTransform = false,
  viewBounds?: Bounds
) {
  if (viewBounds && !isSpWithinBounds(sp, viewBounds)) return

  const transformUpdater =
    withoutTransform === true ? invertTransformConverter : undefined
  switch (sp.instanceType) {
    case InstanceType.Shape:
      renderShape(sp, graphics, transformUpdater)
      break
    case InstanceType.GroupShape:
      renderGroup(sp, graphics, transformUpdater, viewBounds)
      break
    case InstanceType.GraphicFrame:
      renderGraphicFrame(sp, graphics, transformUpdater)
      break
    case InstanceType.ImageShape:
      if (graphics.isInShowMode && sp.hideWhenShow) break
      renderImageShape(sp, graphics, transformUpdater)
      break
  }
}
