import { EditActionFlag } from '../EditActionFlag'
import { gSpecialPasteUtil } from '../../copyPaste/gSpecialPasteUtil'
import { EditChange, EntityForChange } from './type'
import { Presentation } from '../Slide/Presentation'
import { Nullable } from '../../../liber/pervasive'
import { PPTSelectState } from '../Slide/SelectionState'
import { hookManager, Hooks } from '../hooks'
export interface IChangeItem<
  C extends EntityForChange = EntityForChange,
  CC extends EditChange = EditChange,
> {
  entity: C
  changeRecord: CC
  [k: string]: any
}

export interface IEditorAction {
  changes: IChangeItem[]
  actionType: EditActionFlag
  time?: number
  ignoreUndoRedo?: boolean
  docSelectionInfo: PPTSelectState
}

export type RenderingStateCalculator = (
  presentation: Presentation,
  changedSlideNums?: number[]
) => boolean

export class EditChangesStack {
  private static ignoreUndoRedo: boolean = false
  actions: IEditorAction[] // edit changes to be sent for collaboration

  private index: number // the last changeRecord index
  private presentation!: Presentation
  private stopRecordingCouter: number
  private hasContentChanged: boolean

  private renderingStateCalculator: RenderingStateCalculator
  constructor(
    renderingStateCalculator: RenderingStateCalculator,
    presentation?: Presentation
  ) {
    this.renderingStateCalculator = renderingStateCalculator
    this.hasContentChanged = false
    this.index = -1
    this.actions = []
    if (presentation) {
      this.presentation = presentation
    }

    this.stopRecordingCouter = 0
  }

  setIgnoreUndoRedo(v: boolean) {
    EditChangesStack.ignoreUndoRedo = v
  }

  setPresentation(presentation: Presentation) {
    if (!presentation) return

    this.presentation = presentation
  }

  calcRenderingState(changedSlideNums?: number[]) {
    if (this.presentation) {
      const result = this.renderingStateCalculator(
        this.presentation,
        changedSlideNums
      )
      this.hasContentChanged = result
      return result
    }
    return false
  }

  isEmpty() {
    if (this.actions.length <= 0) return true

    return false
  }

  haveChanges() {
    return this.actions.length > 0
  }

  clear() {
    this.index = -1
    this.actions.length = 0
    this.stopRecordingCouter = 0
    this.hasContentChanged = false
  }

  createNewAction(actionFlag: EditActionFlag, ignoreUndoRedo?: boolean) {
    if (0 !== this.stopRecordingCouter) return

    // normalize index
    if (this.index < 0) {
      this.index = -1
    }

    this.mergeCompositionActions()

    const changes = []
    const time = new Date().getTime()

    const action: IEditorAction = {
      changes: changes,
      time: time,
      actionType: actionFlag,
      docSelectionInfo: this.presentation.selectionState.getSelectionState(),
    }

    if (ignoreUndoRedo === true || EditChangesStack.ignoreUndoRedo === true) {
      action.ignoreUndoRedo = true
    }

    this.actions[++this.index] = action

    this.actions.length = this.index + 1

    if (!gSpecialPasteUtil.isPasteStart) {
      gSpecialPasteUtil.hideUI()
    }
  }

  removeLastAction() {
    this.index--
    this.actions.length = this.index + 1
  }

  peekLastChange(): Nullable<IChangeItem> {
    const lastAction = this.actions[this.index]
    const len = lastAction?.changes.length ?? 0
    return lastAction?.changes[len - 1]
  }

  peekLastActionChangeType(): Nullable<{
    action: EditActionFlag
    change: EditActionFlag
  }> {
    const action = this.actions[this.index]
    if (action != null) {
      const l = action.changes.length
      if (l > 0) {
        const change = action.changes[l - 1]

        return {
          action: action.actionType,
          change: change.changeRecord.editType,
        }
      }
    }

    return undefined
  }

  addToAction(changeData: EditChange) {
    const targetObject = changeData.targetEntity
    const data = changeData

    if (0 < this.stopRecordingCouter) {
      hookManager.invoke(Hooks.Document.CheckChangeStackLocked, {
        stopRecordingCouter: this.stopRecordingCouter,
        editType: changeData.editType,
      })
      return
    }
    hookManager.invoke(Hooks.Document.ResetChangeStackLocked)
    if (this.index < 0) {
      this.createNewAction(changeData.editType)
    }

    const action: IChangeItem = {
      entity: targetObject,
      changeRecord: data,
    }

    this.actions[this.index].changes.push(action)
  }

  private mergeCompositionActions() {
    if (this.actions.length < 2) {
      return false
    }

    const action1 = this.actions[this.actions.length - 2]
    const action2 = this.actions[this.actions.length - 1]

    if (
      action1.changes.length > 63 &&
      EditActionFlag.action_Document_MergeLetter === action1.actionType
    ) {
      return false
    }

    let newFlag: EditActionFlag
    if (
      (EditActionFlag.action_Document_CompositeInput === action1.actionType ||
        EditActionFlag.action_Document_CompositeInputReplace ===
          action1.actionType) &&
      EditActionFlag.action_Document_CompositeInputReplace ===
        action2.actionType
    ) {
      newFlag = EditActionFlag.action_Document_CompositeInput
    } else {
      return false
    }

    const newAction: IEditorAction = {
      changes: action1.changes.concat(action2.changes),
      time: action1.time,
      actionType: newFlag,
      docSelectionInfo: this.presentation.selectionState.getSelectionState(),
    }

    this.actions.splice(this.actions.length - 2, 2, newAction)
    if (this.index >= this.actions.length) {
      const d = -this.index + (this.actions.length - 1)
      this.index += d
    }

    return true
  }

  stopRecording() {
    this.stopRecordingCouter++
  }

  enableRecording() {
    this.stopRecordingCouter--
    if (this.stopRecordingCouter <= 0) {
      hookManager.invoke(Hooks.Document.ResetChangeStackLocked)
      this.stopRecordingCouter = 0
    }
  }

  getStopRecordingCouter() {
    return this.stopRecordingCouter
  }

  isOn() {
    return 0 === this.stopRecordingCouter
  }
}
