import { EditActionFlag } from '../EditActionFlag'
import { EntityForChange } from './type'

//
//Base classes for changes
//
//The difference between Property and Value classes is that Property can be undefined, and Value is always a value
//of the given type.
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
/**
 * Base class for all co-editing changes.
 * @constructor
 */
export class EntityEditChange {
  public targetEntity: EntityForChange
  public editType: EditActionFlag
  public readonly Category = 'EditChange'
  constructor(targetEntity: EntityForChange, type: EditActionFlag) {
    this.targetEntity = targetEntity
    this.editType = type
  }
}
