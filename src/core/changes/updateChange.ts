import { EditActionFlag } from '../EditActionFlag'
import { EntityForChange } from './type'

export type SpUpdateUnit = GraphicFrameUpdateUnit | 'SpXfrmAttrs'

type GraphicFrameUpdateUnit =
  | 'GraphicFrameAttrs'
  | 'TableAttrs'
  | 'TableGrid'
  | 'TableRowAttrs'
  | 'TableCellAttrs'
  | 'TableCellContent'

export class UpdateChange {
  public targetEntity: EntityForChange
  public editType: EditActionFlag
  actionType: EditActionFlag
  closestParentId: string
  updateType?: SpUpdateUnit
  readonly Category = 'UpdateChange'

  /** 这里 target 应该为判断更新粒度去重判断的 parent, 如 shape 固定更新 spPr 则 target 为 shape */
  constructor(
    targetEntity: EntityForChange,
    type: EditActionFlag,
    closestParentId: string,
    actionType: EditActionFlag,
    updateType?: SpUpdateUnit
  ) {
    this.targetEntity = targetEntity
    this.editType = type
    this.closestParentId = closestParentId
    this.actionType = actionType
    this.updateType = updateType
  }
}
