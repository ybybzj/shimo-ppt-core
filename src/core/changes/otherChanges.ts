import { EditActionFlag } from '../EditActionFlag'
import { Presentation } from '../Slide/Presentation'
import { Geometry } from '../SlideElement/geometry/Geometry'

export class ThemeChanges {
  public targetEntity: Presentation
  public editType: EditActionFlag
  public readonly Category = 'ThemeChange'

  indexes: any
  constructor(targetEntity: Presentation, type: EditActionFlag, indexes) {
    this.targetEntity = targetEntity
    this.editType = type
    this.indexes = indexes
  }
}

export class GeometryEditChange {
  public targetEntity: Geometry
  public editType: EditActionFlag
  public readonly Category = 'GeometryChange'
  extraData: any[]
  constructor(targetEntity: Geometry, type: EditActionFlag, extraData: any[]) {
    this.targetEntity = targetEntity
    this.editType = type
    this.extraData = extraData
  }
}
