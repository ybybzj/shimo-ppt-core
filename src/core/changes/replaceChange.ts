import { EditActionFlag } from '../EditActionFlag'
import { EntityForChange } from './type'

export class ReplaceChange {
  public targetEntity: EntityForChange
  public editType: EditActionFlag
  readonly Category = 'ReplaceChange'
  actionType: EditActionFlag

  constructor(
    targetEntity: EntityForChange,
    type: EditActionFlag,
    actionType: EditActionFlag
  ) {
    this.targetEntity = targetEntity
    this.editType = type
    this.actionType = actionType
  }
}
