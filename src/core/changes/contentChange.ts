import { EditActionFlag } from '../EditActionFlag'
import { EntityForChange, EntityObject } from './type'

export class EntityContentChange {
  public targetEntity: EntityForChange
  public editType: EditActionFlag
  public readonly Category = 'SpContentChange'
  public index: number
  public items: EntityObject[]
  public isAdd: boolean

  constructor(
    targetEntity: EntityForChange,
    type: EditActionFlag,
    index: number,
    items: EntityObject[],
    isAdd: boolean
  ) {
    this.targetEntity = targetEntity
    this.editType = type
    this.index = index
    this.items = items
    this.isAdd = isAdd
  }
}
