import { EntityEditChange } from './editChange'
import { InstanceTypeIDS } from '../instanceTypes'
import { EntityContentChange } from './contentChange'
import { EntityPropertyChange } from './propertyChange'
import { ThemeChanges, GeometryEditChange } from './otherChanges'
import { ReplaceChange } from './replaceChange'
import { OrderChange } from './orderChange'
import { UpdateChange } from './updateChange'
import { TextDocument } from '../TextDocument/TextDocument'
import { Presentation } from '../Slide/Presentation'
import { Slide } from '../Slide/Slide'
import { Geometry } from '../SlideElement/geometry/Geometry'
import { TextBody } from '../SlideElement/TextBody'
import { TableCell } from '../Table/TableCell'
import { TableRow } from '../Table/TableRow'
import { GraphicFrame } from '../SlideElement/GraphicFrame'
import { GroupShape } from '../SlideElement/GroupShape'
import { ImageShape } from '../SlideElement/Image'
import { Shape } from '../SlideElement/Shape'
import { Table } from '../Table/Table'
import { Comment } from '../Slide/Comments/Comment'
import { SlideAnimation } from '../Slide/Animation'
import { SlideMaster } from '../Slide/SlideMaster'
import { SlideLayout } from '../Slide/SlideLayout'
import { SlideComments } from '../Slide/Comments/SlideComments'
import { Notes } from '../Slide/Notes'
import { NotesMaster } from '../Slide/NotesMaster'
import { Xfrm, SpPr } from '../SlideElement/attrs/shapePrs'
import { SlideElement } from '../SlideElement/type'
import { TextContentItem } from '../calculation/utils'
export interface EntityObject {
  instanceType: InstanceTypeIDS
  id: string
  [k: string]: any
}

export type EntityForCalculation =
  | Slide
  | SlideElement
  | SlideMaster
  | SlideLayout

export type EntityForChange =
  | Presentation
  | Slide
  | Geometry
  | Xfrm
  | SpPr
  | TextBody
  | TextDocument
  | TextContentItem
  | TableCell
  | TableRow
  | Table
  | GraphicFrame
  | Shape
  | ImageShape
  | GroupShape
  | Comment
  | SlideAnimation
  | SlideMaster
  | SlideLayout
  | SlideComments
  | Notes
  | NotesMaster

export type EditChange =
  | EntityEditChange
  | ThemeChanges
  | GeometryEditChange
  | EntityContentChange
  | EntityPropertyChange
  | ReplaceChange
  | OrderChange
  | UpdateChange

export type ChangeCategory = EditChange['Category']
