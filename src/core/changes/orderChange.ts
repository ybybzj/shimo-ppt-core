import { EditActionFlag } from '../EditActionFlag'
import { EntityForChange } from './type'

export class OrderChange {
  public targetEntity: EntityForChange
  public editType: EditActionFlag
  readonly Category = 'OrderChange'

  constructor(targetEntity: EntityForChange, type: EditActionFlag) {
    this.targetEntity = targetEntity
    this.editType = type
  }
}
