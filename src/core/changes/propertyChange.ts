import { EditActionFlag } from '../EditActionFlag'
import { EntityForChange } from './type'

export class EntityPropertyChange<T = any> {
  public targetEntity: EntityForChange
  public editType: EditActionFlag
  public readonly Category = 'PropertyChange'
  public old: T
  public new: T
  constructor(
    targetEntity: EntityForChange,
    type: EditActionFlag,
    oldPr: T,
    newPr: T
  ) {
    this.targetEntity = targetEntity
    this.editType = type
    this.old = oldPr
    this.new = newPr
  }
}
