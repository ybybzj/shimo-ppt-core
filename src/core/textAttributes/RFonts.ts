import { FontHint, FontHintValues } from '../../fonts/const'
import { Nullable } from '../../../liber/pervasive'
import { assignVal } from '../../io/utils'
import { CRFontsData } from '../../io/dataType/style'
import { valFromNullable, valOfReset } from './utils'
import { nullableToNull } from '../common/helpers'
import { OptionsApplyResult } from './type'

export interface FontsProps {
  name: string
  index: number
}
export interface RFontsOptions {
  ascii?: FontsProps | null
  eastAsia?: FontsProps | null
  hAnsi?: FontsProps | null
  cs?: FontsProps | null
  hint?: FontHintValues | null
}
export class RFonts {
  ascii?: FontsProps
  eastAsia?: FontsProps
  hAnsi?: FontsProps
  cs?: FontsProps
  hint?: FontHintValues
  static new(opts?: RFontsOptions) {
    const ret = new RFonts()
    if (opts != null) {
      ret.fromOptions(opts)
    }
    return ret
  }
  static nulledOptions(opts: RFontsOptions, isToNull = false) {
    const ret: RFontsOptions = {
      ascii: nullableToNull(isToNull, opts.ascii),
      eastAsia: nullableToNull(isToNull, opts.eastAsia),
      hAnsi: nullableToNull(isToNull, opts.hAnsi),
      cs: nullableToNull(isToNull, opts.cs),
      hint: nullableToNull(isToNull, opts.hint),
    }
    return ret
  }

  constructor() {
    this.ascii = undefined
    this.eastAsia = undefined
    this.hAnsi = undefined
    this.cs = undefined
    this.hint = undefined
  }
  reset(applyResult?: OptionsApplyResult) {
    this.ascii = valOfReset(this.ascii, applyResult)
    this.eastAsia = valOfReset(this.eastAsia, applyResult)
    this.hAnsi = valOfReset(this.hAnsi, applyResult)
    this.cs = valOfReset(this.cs, applyResult)
    this.hint = valOfReset(this.hint, applyResult)
  }

  fromOptions(
    opts: Nullable<RFontsOptions>,
    applyResult?: { isApplied: boolean }
  ) {
    if (opts == null) return
    this.ascii = valFromNullable(this.ascii, opts.ascii, applyResult)
    this.eastAsia = valFromNullable(this.eastAsia, opts.eastAsia, applyResult)
    this.hAnsi = valFromNullable(this.hAnsi, opts.hAnsi, applyResult)
    this.cs = valFromNullable(this.cs, opts.cs, applyResult)
    this.hint = valFromNullable(this.hint, opts.hint, applyResult)
  }

  setAll(fontName: string, index: number) {
    this.ascii = {
      name: fontName,
      index: index,
    }

    this.eastAsia = {
      name: fontName,
      index: index,
    }

    this.hAnsi = {
      name: fontName,
      index: index,
    }

    this.cs = {
      name: fontName,
      index: index,
    }

    this.hint = FontHint.Default
  }

  clone() {
    return RFonts.new(this)
  }

  setDefault() {
    this.ascii = {
      name: 'Arial',
      index: -1,
    }

    this.eastAsia = {
      name: 'Arial',
      index: -1,
    }

    this.hAnsi = {
      name: 'Arial',
      index: -1,
    }

    this.cs = {
      name: 'Arial',
      index: -1,
    }

    this.hint = FontHint.Default
  }

  subDiff(rFonts: Nullable<RFontsOptions>) {
    if (rFonts == null) return
    // Ascii
    if (
      null != this.ascii &&
      (null == rFonts.ascii || this.ascii.name !== rFonts.ascii.name)
    ) {
      this.ascii = undefined
    }

    // EastAsia
    if (
      null != this.eastAsia &&
      (null == rFonts.eastAsia || this.eastAsia.name !== rFonts.eastAsia.name)
    ) {
      this.eastAsia = undefined
    }

    // HAnsi
    if (
      null != this.hAnsi &&
      (null == rFonts.hAnsi || this.hAnsi.name !== rFonts.hAnsi.name)
    ) {
      this.hAnsi = undefined
    }

    // CS
    if (
      null != this.cs &&
      (null == rFonts.cs || this.cs.name !== rFonts.cs.name)
    ) {
      this.cs = undefined
    }

    // Hint
    if (
      null != this.hint &&
      (null == rFonts.hint || this.hint !== rFonts.hint)
    ) {
      this.hint = undefined
    }
  }

  toJSON(): CRFontsData {
    const data: CRFontsData = {}
    if (null != this.ascii) {
      assignVal(data, 'ascii', this.ascii.name)
    }
    if (null != this.eastAsia) {
      assignVal(data, 'eastAsia', this.eastAsia.name)
    }
    if (null != this.hAnsi) {
      assignVal(data, 'hAnsi', this.hAnsi.name)
    }
    if (null != this.cs) {
      assignVal(data, 'cs', this.cs.name)
    }
    if (null != this.hint) {
      assignVal(data, 'hint', this.hint)
    }
    return data
  }

  fromJSON(data: CRFontsData) {
    if (null != data['ascii']) {
      this.ascii = { name: data['ascii'], index: -1 }
    }
    if (null != data['eastAsia']) {
      this.eastAsia = { name: data['eastAsia'], index: -1 }
    }
    if (null != data['hAnsi']) {
      this.hAnsi = { name: data['hAnsi'], index: -1 }
    }
    if (null != data['cs']) {
      this.cs = { name: data['cs'], index: -1 }
    }
    if (null != data['hint']) {
      this.hint = data['hint']
    }
  }

  sameAs(rFonts: RFonts) {
    if (
      (null == this.ascii && null != rFonts.ascii) ||
      (null != this.ascii &&
        (null == rFonts.ascii || this.ascii.name !== rFonts.ascii.name))
    ) {
      return false
    }

    if (
      (null == this.eastAsia && null != rFonts.eastAsia) ||
      (null != this.eastAsia &&
        (null == rFonts.eastAsia ||
          this.eastAsia.name !== rFonts.eastAsia.name))
    ) {
      return false
    }

    if (
      (null == this.hAnsi && null != rFonts.hAnsi) ||
      (null != this.hAnsi &&
        (null == rFonts.hAnsi || this.hAnsi.name !== rFonts.hAnsi.name))
    ) {
      return false
    }

    if (
      (null == this.cs && null != rFonts.cs) ||
      (null != this.cs &&
        (null == rFonts.cs || this.cs.name !== rFonts.cs.name))
    ) {
      return false
    }

    if (
      (null == this.hint && null != rFonts.hint) ||
      (null != this.hint && (null == rFonts.hint || this.hint !== rFonts.hint))
    ) {
      return false
    }

    return true
  }

  isEmpty() {
    if (
      null != this.ascii ||
      null != this.eastAsia ||
      null != this.hAnsi ||
      null != this.cs ||
      null != this.hint
    ) {
      return false
    }

    return true
  }

  merge(rFonts: RFontsOptions) {
    if (rFonts == null) return
    if (null != rFonts.ascii) this.ascii = rFonts.ascii

    if (null != rFonts.eastAsia) this.eastAsia = rFonts.eastAsia
    else if (null != rFonts.ascii) this.eastAsia = rFonts.ascii

    if (null != rFonts.hAnsi) this.hAnsi = rFonts.hAnsi
    else if (null != rFonts.ascii) this.hAnsi = rFonts.ascii

    if (null != rFonts.cs) this.cs = rFonts.cs
    else if (null != rFonts.ascii) this.cs = rFonts.ascii

    if (null != rFonts.hint) this.hint = rFonts.hint
  }
}
