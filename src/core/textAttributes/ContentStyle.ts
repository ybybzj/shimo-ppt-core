import { TextPr } from './TextPr'
import { ParaPr, ParaPrOptions } from './ParaPr'

import { gDefaultParaPr, gDefaultTextPr } from './globals'
import { Nullable } from '../../../liber/pervasive'
import { PoolAllocator, Recyclable } from '../../common/allocator'

export interface TextContentStylePrs {
  textPr: TextPr
  paraPr: ParaPr
}

export function recylceTextContentStylePrs(prs: Nullable<TextContentStylePrs>) {
  const textPr = prs?.textPr
  const paraPr = prs?.paraPr
  if (textPr != null) TextPr.recycle(textPr)
  if (paraPr != null) ParaPr.recycle(paraPr)
}

export class ContentStyle extends Recyclable<ContentStyle> {
  inheritedBy?: ContentStyle
  textPr: TextPr
  paraPr: ParaPr
  private static allocator: PoolAllocator<ContentStyle, []> =
    PoolAllocator.getAllocator<ContentStyle, []>(ContentStyle)
  static new() {
    return ContentStyle.allocator.allocate()
  }
  static recycle(contentStyle: ContentStyle) {
    ContentStyle.allocator.recycle(contentStyle)
  }

  constructor() {
    super()
    this.textPr = TextPr.new()
    this.paraPr = ParaPr.new()
  }
  reset() {
    this.inheritedBy = undefined
    this.textPr.reset()
    this.paraPr.reset()
  }
  fromParaPrOptions(opts: Nullable<ParaPrOptions>) {
    if (opts == null) return
    this.paraPr.fromOptions(opts)
    if (opts.defaultRunPr != null) {
      this.textPr.fromOptions(opts.defaultRunPr)
    }
  }
}

export const defaultContentStylePrs = () => ({
  textPr: gDefaultTextPr.clone(),
  paraPr: gDefaultParaPr.clone(),
})

export const contentStyleCollector: {
  add: (style: Nullable<ContentStyle>) => void
  collect: () => TextContentStylePrs
} = (() => {
  let head: ContentStyle | undefined
  let curStyle: ContentStyle | undefined

  function inQueue(style: Nullable<ContentStyle>) {
    if (style == null) return
    if (head == null) head = style
    if (curStyle != null) {
      curStyle.inheritedBy = style
    }
    curStyle = style
  }

  function collect() {
    const pr: TextContentStylePrs = {
      textPr: TextPr.new(gDefaultTextPr),
      paraPr: ParaPr.new(gDefaultParaPr),
    }
    curStyle = head
    while (curStyle) {
      pr.textPr.merge(curStyle.textPr)
      pr.paraPr.merge(curStyle.paraPr)
      ContentStyle.recycle(curStyle)
      curStyle = curStyle.inheritedBy
    }

    head = undefined
    curStyle = undefined
    return pr
  }

  return {
    add: inQueue,
    collect,
  }
})()
