import { ParaTabStop, ParaTabStopOptions } from './ParaTab'
import { TextTabAlign } from '../common/const/paragraph'
import { CParaTabsData } from '../../io/dataType/style'
import { fromJSON, toJSON } from '../../io/utils'
import { Nullable } from '../../../liber/pervasive'
import { findIndex } from '../../../liber/l/findIndex'
export interface ParaTabListOptions {
  tabStops?: ParaTabStopOptions[]
}
export class ParaTablist {
  tabStops: ParaTabStop[]
  constructor() {
    this.tabStops = []
  }

  reset() {
    this.tabStops.length = 0
  }

  private add(newTab: ParaTabStopOptions, isMerge = false) {
    if (newTab.index == null) return
    const index = this.findTabStopIndex(newTab.index)
    if (index > -1) {
      if (isMerge === true && newTab.value === TextTabAlign.Clear) {
        this.tabStops.splice(index, 1)
      } else {
        this.tabStops.splice(
          index,
          0,
          new ParaTabStop(newTab.value, newTab.index)
        )
      }
    } else if (isMerge === false) {
      this.tabStops.push(new ParaTabStop(newTab.value, newTab.index))
    }
  }

  merge(otherTabsStyle: ParaTabListOptions) {
    const otherTabs = otherTabsStyle.tabStops
    if (otherTabs == null) return

    for (let i = 0; i < otherTabs.length; i++) {
      this.add(otherTabs[i], true)
    }
  }

  sameAs(otherTabsStyle: Nullable<ParaTabListOptions>) {
    if (otherTabsStyle == null) return false
    if (this.tabStops == null && otherTabsStyle.tabStops == null) return true
    if (this.tabStops == null || otherTabsStyle.tabStops == null) return false
    if (this.tabStops.length !== otherTabsStyle.tabStops.length) return false

    for (let i = 0, l = this.tabStops.length; i < l; i++) {
      if (true !== this.tabStops[i].sameAs(otherTabsStyle.tabStops[i])) {
        return false
      }
    }

    return true
  }

  clone() {
    const tabsStyle = new ParaTablist()
    const l = this.tabStops.length

    for (let i = 0; i < l; i++) {
      tabsStyle.tabStops.push(this.tabStops[i].clone())
    }

    return tabsStyle
  }

  fromTabsOptions(tabs: Nullable<ParaTabStopOptions[]>) {
    if (Array.isArray(tabs) && tabs.length > 0) {
      for (let i = 0, l = tabs.length; i < l; ++i) {
        this.add(tabs[i])
      }
    }
  }

  fromOptions(opts: Nullable<ParaTabListOptions>) {
    if (opts == null) return
    if (opts.tabStops != null) this.fromTabsOptions(opts.tabStops)
  }
  getCount() {
    return this.tabStops.length
  }

  get(index: number) {
    return this.tabStops[index]
  }

  getValue(elmIndex: number) {
    const index = this.findTabStopIndex(elmIndex)
    return index > -1 ? this.tabStops[index].value : -1
  }

  private findTabStopIndex(index: number) {
    return findIndex(
      (tab) => tab.index != null && Math.abs(tab.index - index) < 0.001,
      this.tabStops
    )
  }

  toJSON(): CParaTabsData {
    return this.tabStops.map((tab) => toJSON(tab))
  }

  fromJSON(data: CParaTabsData) {
    const l = data.length
    this.tabStops.length = 0
    for (let i = 0; i < l; i++) {
      const tabData = data[i]
      if (tabData != null && tabData['position'] != null) {
        const tab = new ParaTabStop()
        fromJSON(tab, tabData)
        this.tabStops.push(tab)
      }
    }
  }
}
