import { ParaInd, ParaIndOptions } from './ParaInd'
import { ParaSpacing, ParaSpacingOptions } from './ParaSpacing'
import { TextPr, TextPrOptions } from './TextPr'
import { Factor_pt_to_mm, ScaleOfPPTXSizes } from '../common/const/unit'

import { ParaTablist, ParaTabListOptions } from './ParaTabs'
import {
  fromOptions,
  isNumbersTheSame,
  isObjectsTheSame,
} from '../common/helpers'
import {
  assignVal,
  assignJSON,
  readFromJSON,
  isEmptyPropValue,
  scaleValue,
  toJSON,
} from '../../io/utils'
import {
  CParaPrData,
  toJC,
  fromJC,
  JCAlignTypeValues,
} from '../../io/dataType/style'
import { AlignKind, LineHeightRule } from '../common/const/attrs'
import {
  Bullet,
  BulletOptions,
  BulletTypeOptions,
} from '../SlideElement/attrs/bullet'
import { BulletTypeValue } from '../SlideElement/const'
import { Nullable } from '../../../liber/pervasive'
import { PoolAllocator, Recyclable } from '../../common/allocator'
export interface ParaPrOptions {
  ind?: ParaIndOptions
  jcAlign?: JCAlignTypeValues
  spacing?: ParaSpacingOptions
  tabs?: ParaTabListOptions
  defaultRunPr?: TextPrOptions
  bullet?: BulletOptions
  lvl?: number
  defaultTab?: number
  rtl?: boolean
}
export class ParaPr extends Recyclable<ParaPr> {
  ind: ParaInd
  jcAlign?: JCAlignTypeValues
  spacing: ParaSpacing
  tabs?: ParaTablist
  defaultRunPr?: TextPr
  bullet?: Bullet
  lvl?: number
  defaultTab?: number
  rtl?: boolean
  private static allocator: PoolAllocator<ParaPr, []> =
    PoolAllocator.getAllocator<ParaPr, []>(ParaPr)

  static new(opts?: ParaPrOptions) {
    const ret = ParaPr.allocator.allocate()
    if (opts != null) ret.fromOptions(opts)
    return ret
  }
  static recycle(pr: ParaPr) {
    ParaPr.allocator.recycle(pr)
  }
  constructor() {
    super()
    this.ind = new ParaInd()
    this.jcAlign = undefined
    this.spacing = new ParaSpacing()
    this.tabs = undefined

    this.defaultRunPr = undefined
    this.bullet = undefined
    this.lvl = undefined
    this.defaultTab = undefined
    this.rtl = undefined
  }
  reset() {
    this.ind.reset()
    this.jcAlign = undefined
    this.spacing.reset()
    this.tabs = undefined

    this.defaultRunPr?.reset()
    this.bullet?.reset()
    this.lvl = undefined
    this.defaultTab = undefined
    this.rtl = undefined
  }

  clone() {
    return ParaPr.new(this)
  }

  merge(paraPr: ParaPrOptions) {
    if (paraPr == null) return
    this.fromOptions(paraPr)
  }

  setDefault() {
    const indDefaults: ParaIndOptions = {
      left: 0,
      right: 0,
      firstLine: 0,
    }
    this.ind = fromOptions(this.ind, indDefaults, ParaInd)!

    this.jcAlign = AlignKind.Left

    this.spacing = fromOptions(
      this.spacing,
      {
        line: 1.15,
        lineRule: LineHeightRule.Auto,
        before: 0,
        beforeAuto: false,
        after: 10 * Factor_pt_to_mm,
        afterAuto: false,
      } as ParaSpacingOptions,
      ParaSpacing
    )!

    this.tabs?.reset()

    this.defaultRunPr?.reset()
    this.bullet?.reset()
    this.defaultTab = undefined
    this.rtl = false
  }

  fromOptions(opts: Nullable<ParaPrOptions>) {
    if (opts == null) return
    this.ind.fromOptions(opts.ind)

    if (opts.jcAlign != null) this.jcAlign = opts.jcAlign

    this.spacing.fromOptions(opts.spacing)

    this.tabs = fromOptions(this.tabs, opts.tabs, ParaTablist)

    this.defaultRunPr = fromOptions<TextPrOptions, undefined, TextPr>(
      this.defaultRunPr,
      opts.defaultRunPr,
      TextPr
    )

    this.bullet = fromOptions(this.bullet, opts.bullet, Bullet)

    if (null != opts.defaultTab) {
      this.defaultTab = opts.defaultTab
    }

    if (opts.lvl != null) this.lvl = opts.lvl

    if (null != opts.rtl) {
      this.rtl = opts.rtl
    }
  }

  intersect(paraPr: ParaPrOptions) {
    const retOpts: ParaPrOptions = {}
    if (null != paraPr.ind && null != this.ind) {
      const retInd: ParaIndOptions = {}
      if (isNumbersTheSame(paraPr.ind.left, this.ind.left)) {
        retInd.left = paraPr.ind.left
      }

      if (isNumbersTheSame(paraPr.ind.right, this.ind.right)) {
        retInd.right = paraPr.ind.right
      }

      if (isNumbersTheSame(paraPr.ind.firstLine, this.ind.firstLine)) {
        retInd.firstLine = paraPr.ind.firstLine
      }
      retOpts.ind = retInd
    }

    if (paraPr.jcAlign === this.jcAlign) retOpts.jcAlign = paraPr.jcAlign

    if (null != this.spacing && null != paraPr.spacing) {
      retOpts.spacing = this.spacing.intersect(paraPr.spacing)
    }

    if (null != this.bullet && null != paraPr.bullet) {
      retOpts.bullet = intersectBullets(paraPr.bullet, this.bullet)
    }

    if (null != this.defaultRunPr && null != paraPr.defaultRunPr) {
      retOpts.defaultRunPr = this.defaultRunPr
        .clone()
        .subDiff(paraPr.defaultRunPr)
    }

    if (null != this.lvl && null != paraPr.lvl && paraPr.lvl === this.lvl) {
      retOpts.lvl = this.lvl
    }

    if (
      null != this.defaultTab &&
      null != paraPr.defaultTab &&
      paraPr.defaultTab === this.defaultTab
    ) {
      retOpts.defaultTab = this.defaultTab
    }

    if (null != this.rtl && null != paraPr.rtl && paraPr.rtl === this.rtl) {
      retOpts.rtl = this.rtl
    }

    if (
      null != this.tabs &&
      null != paraPr.tabs &&
      this.tabs.sameAs(paraPr.tabs)
    ) {
      retOpts.tabs = paraPr.tabs
    }

    return Object.keys(retOpts).length > 0 ? ParaPr.new(retOpts) : undefined
  }

  toJSON(): CParaPrData {
    const data: CParaPrData = {}
    if (null != this.jcAlign) {
      assignVal(data, 'algn', toJC(this.jcAlign))
    }

    assignVal(data, 'defTabSz', scaleValue(this.defaultTab, ScaleOfPPTXSizes))

    if (this.ind != null) {
      assignVal(data, 'marL', scaleValue(this.ind.left, ScaleOfPPTXSizes))
      assignVal(data, 'marR', scaleValue(this.ind.right, ScaleOfPPTXSizes))
      assignVal(
        data,
        'indent',
        scaleValue(this.ind.firstLine, ScaleOfPPTXSizes)
      )
    }
    assignVal(data, 'lvl', this.lvl)

    if (null != this.tabs) {
      assignJSON(data, 'tabs', this.tabs)
    }
    if (null != this.spacing) {
      const json = toJSON(this.spacing)
      assignVal(data, 'lineSpacing', json.lineSpacing)
      assignVal(data, 'spaceBefore', json.spaceBefore)
      assignVal(data, 'spaceAfter', json.spaceAfter)
    }

    assignJSON(data, 'defaultRunPr', this.defaultRunPr)
    assignJSON(data, 'bullet', this.bullet)
    assignVal(data, 'rtl', this.rtl)
    // TODO
    // eaLnBrk?: boolean            // OO: 未处理
    // fontAlgn?: FontAlignType     // OO: 未处理
    // latinLnBrk?: boolean         // OO: 未处理
    // hangingPunct?: boolean       // OO: 未处理

    return data
  }

  fromJSON(data: CParaPrData) {
    // adapter 20200714
    if (data['indent'] != null) {
      this.ind.firstLine = data['indent'] / ScaleOfPPTXSizes
    }
    if (data['marL'] != null) {
      this.ind.left = data['marL'] / ScaleOfPPTXSizes
    }
    if (data['marR'] != null) {
      this.ind.right = data['marR'] / ScaleOfPPTXSizes
    }

    if (data['lvl'] != null) {
      this.lvl = data['lvl']
    }
    if (data['defTabSz'] != null) {
      this.defaultTab = data['defTabSz'] / ScaleOfPPTXSizes
    } else if (data['defaultTab'] != null) {
      this.defaultTab = data['defaultTab'] / ScaleOfPPTXSizes
    }
    if (data['algn'] != null) {
      this.jcAlign = fromJC(data['algn'])
    }
    if (data['defaultRunPr'] != null) {
      this.defaultRunPr = readFromJSON(data['defaultRunPr'], TextPr)
    }
    if (data['bullet'] != null) {
      this.bullet = readFromJSON(data['bullet'], Bullet)
    }
    if (data['tabs'] != null) {
      this.tabs = readFromJSON(data['tabs'], ParaTablist)
    }
    const spacing = {
      ['lineSpacing']: data['lineSpacing'],
      ['spaceBefore']: data['spaceBefore'],
      ['spaceAfter']: data['spaceAfter'],
    }
    if (!isEmptyPropValue(spacing)) {
      this.spacing = readFromJSON(spacing, ParaSpacing) as ParaSpacing
    }

    if (data['rtl'] != null) {
      this.rtl = data['rtl']
    }

    // TODO
    // eaLnBrk?: boolean            // OO: 未处理
    // fontAlgn?: FontAlignType     // OO: 未处理
    // latinLnBrk?: boolean         // OO: 未处理
    // hangingPunct?: boolean       // OO: 未处理
  }

  sameAs(paraPr: ParaPr) {
    if (
      true !== isObjectsTheSame(this.ind, paraPr.ind) ||
      this.jcAlign !== paraPr.jcAlign ||
      true !== isObjectsTheSame(this.spacing, paraPr.spacing) ||
      true !== isObjectsTheSame(this.tabs, paraPr.tabs)
    ) {
      return false
    }

    return true
  }

  isEmpty() {
    if (
      true !== this.ind.isEmpty() ||
      null != this.jcAlign ||
      true !== this.spacing.isEmpty() ||
      null != this.tabs
    ) {
      return false
    }

    return true
  }
}

// helper
function intersectBullets(bullet1: BulletOptions, bullet2: BulletOptions) {
  if (
    bullet1.bulletType &&
    bullet2.bulletType &&
    bullet1.bulletType.type === bullet2.bulletType.type &&
    bullet1.bulletType.type !== BulletTypeValue.NONE
  ) {
    const retBulletType: BulletTypeOptions = {}
    switch (bullet1.bulletType.type) {
      case BulletTypeValue.CHAR: {
        retBulletType.type = BulletTypeValue.CHAR
        if (bullet1.bulletType.char === bullet2.bulletType.char) {
          retBulletType.char = bullet1.bulletType.char
        }
        break
      }
      case BulletTypeValue.BLIP: {
        retBulletType.type = BulletTypeValue.CHAR
        break
      }
      case BulletTypeValue.AUTONUM: {
        if (bullet1.bulletType.textAutoNum === bullet2.bulletType.textAutoNum) {
          retBulletType.textAutoNum = bullet1.bulletType.textAutoNum
        }
        if (bullet1.bulletType.startAt === bullet2.bulletType.startAt) {
          retBulletType.startAt = bullet1.bulletType.startAt
        }
        if (bullet1.bulletType.type === bullet2.bulletType.type) {
          retBulletType.type = bullet1.bulletType.type
        }
        break
      }
    }
    return Bullet.new({
      bulletType: retBulletType,
    })
  } else {
    return undefined
  }
}
