import { TextPr } from './TextPr'
import { ParaPr } from './ParaPr'
import { TablePr } from '../Table/attrs/TablePr'
import { TableCellPr } from '../Table/attrs/TableCellPr'
import { TableRowPr } from '../Table/attrs/TableRowPr'
import { TablePartStyle } from '../Table/attrs/TablePartStyle'

export const gDefaultTextPr = TextPr.new()
export const gDefaultParaPr = ParaPr.new()
export const gDefaultTablePr = TablePr.new()
export const gDefaultTableCellPr = TableCellPr.new()
export const gDefaultTableRowPr = new TableRowPr()
export const gDefaultTableStylePr = TablePartStyle.new()
gDefaultTextPr.setDefault()
gDefaultParaPr.setDefault()
gDefaultTablePr.setDefault()
gDefaultTableCellPr.setDefault()
gDefaultTableRowPr.setDefault()
gDefaultTableStylePr.setDefault()
