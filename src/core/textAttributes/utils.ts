import { CLnData } from '../../io/dataType/spAttrs'
import { assignJSON, readFromJSON } from '../../io/utils'
import { ScaleOfPPTXSizes } from '../common/const/unit'
import { KeysWithValueType, Dict, Nullable } from '../../../liber/pervasive'
import { FillKIND } from '../common/const/attrs'
import { TableCellBorder } from '../Table/attrs/CellBorder'
import { FillEffects } from '../SlideElement/attrs/fill/fill'
import { Ln, LnOptions } from '../SlideElement/attrs/line'
import { isObjectsTheSame } from '../common/helpers'
import { OptionsApplyResult } from './type'
import { TCBorderValue } from '../common/const/table'

export function assignLnByBorderPr<
  T extends Dict,
  K extends KeysWithValueType<T, undefined | CLnData>,
>(data: T, key: K, borderPr: TableCellBorder) {
  const opts: LnOptions = {}
  if (borderPr.size != null) {
    opts.w = (borderPr.size * ScaleOfPPTXSizes) >> 0
  }

  if (borderPr.value !== TCBorderValue.None && borderPr.fillEffects != null) {
    opts.fillEffects = borderPr.fillEffects
  } else {
    opts.fillEffects = FillEffects.NoFill()
  }

  const ln = Ln.new(opts)
  assignJSON(data, key, ln)
}

export function applyLnToBorderPr(
  data: Nullable<CLnData>,
  borderPr: TableCellBorder
) {
  if (data) {
    const ln = readFromJSON(data, Ln)
    if (ln?.fillEffects != null) {
      borderPr.fillEffects = ln.fillEffects
    }
    borderPr.size = ln?.w == null ? 12700 : ln.w >> 0
    borderPr.size /= ScaleOfPPTXSizes
    const isNoFill = ln?.fillEffects?.fill?.type === FillKIND.NOFILL
    borderPr.value = isNoFill ? TCBorderValue.None : TCBorderValue.Single
  }
}

export function valFromNullable<T>(
  orgV: T | undefined,
  v: Nullable<T>,
  applyResult?: OptionsApplyResult
): undefined | T {
  if (v !== undefined) {
    v = v === null ? undefined : v
    const isApplied = v !== orgV
    if (isApplied) {
      if (applyResult) {
        applyResult.isApplied = true
      }

      return v
    }
  }
  return orgV
}

export function objFromNullable<T extends { sameAs(opts: T): boolean }>(
  orgV: T | undefined,
  v: Nullable<T>,
  applyResult?: OptionsApplyResult
): undefined | T {
  if (v !== undefined) {
    v = v === null ? undefined : v
    const isApplied = !isObjectsTheSame(orgV, v)
    if (isApplied) {
      if (applyResult) {
        applyResult.isApplied = true
      }

      return v
    }
  }
  return orgV
}

export function valOfReset<T>(
  v: T | undefined,
  applyResult?: OptionsApplyResult
) {
  if (v !== undefined && applyResult) {
    applyResult.isApplied = true
  }

  return undefined
}

export function valForEmptyProp<T>(
  orgV: Nullable<T>,
  v: Nullable<T>,
  applyResult?: OptionsApplyResult
) {
  if (orgV == null && v != null) {
    if (applyResult) {
      applyResult.isApplied = true
    }
    return v
  }
  return orgV ?? undefined
}

export function resetObject<
  T extends { reset(applyResult?: OptionsApplyResult): void },
>(v: T | undefined, applyResult?: OptionsApplyResult) {
  if (v !== undefined) {
    v.reset(applyResult)
  }

  return v
}
