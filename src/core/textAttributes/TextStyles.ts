import { isDict } from '../../common/utils'
import { TextListStyle } from './TextListStyle'
import { assignJSON, readFromJSON } from '../../io/utils'
import { CTextStylesData } from '../../io/dataType/style'
import { PlaceholderType } from '../SlideElement/const'

export class TextStyles {
  titleStyle?: TextListStyle
  bodyStyle?: TextListStyle
  otherStyle?: TextListStyle
  constructor() {
    this.titleStyle = undefined
    this.bodyStyle = undefined
    this.otherStyle = undefined
  }

  getStyleByPlaceholderType(phType) {
    switch (phType) {
      case PlaceholderType.ctrTitle:
      case PlaceholderType.title: {
        return this.titleStyle
      }
      case PlaceholderType.body:
      case PlaceholderType.subTitle:
      case PlaceholderType.obj:
      case null:
      case undefined: {
        return this.bodyStyle
      }
      default: {
        break
      }
    }
    return this.otherStyle
  }

  clone() {
    const ret = new TextStyles()
    if (isDict(this.titleStyle)) {
      ret.titleStyle = this.titleStyle!.clone()
    }

    if (isDict(this.bodyStyle)) {
      ret.bodyStyle = this.bodyStyle!.clone()
    }

    if (isDict(this.otherStyle)) {
      ret.otherStyle = this.otherStyle!.clone()
    }
    return ret
  }

  toJSON(): CTextStylesData {
    const data: CTextStylesData = {}
    assignJSON(data, 'bodyStyle', this.bodyStyle)
    assignJSON(data, 'titleStyle', this.titleStyle)
    assignJSON(data, 'otherStyle', this.otherStyle)
    return data
  }

  fromJSON(data: CTextStylesData) {
    this.bodyStyle = readFromJSON(data['bodyStyle'], TextListStyle)
    this.titleStyle = readFromJSON(data['titleStyle'], TextListStyle)
    this.otherStyle = readFromJSON(data['otherStyle'], TextListStyle)
  }

  collectAllFontNames(allFonts) {
    if (this.titleStyle) {
      this.titleStyle.collectAllFontNames(allFonts)
    }
    if (this.bodyStyle) {
      this.bodyStyle.collectAllFontNames(allFonts)
    }
    if (this.otherStyle) {
      this.otherStyle.collectAllFontNames(allFonts)
    }
  }
}
