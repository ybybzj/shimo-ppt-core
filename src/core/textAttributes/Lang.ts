// import { assignVal } from '../../io/utils'
// import { CLangData } from '../../io/dataType/style'
import { EnUSLangID } from '../../lib/locale'
import { Nullable } from '../../../liber/pervasive'
import { valFromNullable, valOfReset } from './utils'
import { nullableToNull } from '../common/helpers'
import { OptionsApplyResult } from './type'
import { FontClass, FontClassValues } from '../../fonts/const'
export interface LangOptions {
  bidi?: string | null
  eastAsia?: string | null
  val?: string | null
  fontClass?: FontClassValues | null
}
export class Lang {
  bidi?: string
  eastAsia?: string
  val?: string
  fontClass?: FontClassValues
  static new(opts?: LangOptions): Lang {
    const ret = new Lang()
    if (opts != null) ret.fromOptions(opts)
    return ret
  }
  static nulledOptions(opts: LangOptions, isToNull = false) {
    const ret: LangOptions = {
      bidi: nullableToNull(isToNull, opts.bidi),
      eastAsia: nullableToNull(isToNull, opts.eastAsia),
      val: nullableToNull(isToNull, opts.val),
      fontClass: nullableToNull(isToNull, opts.fontClass),
    }
    return ret
  }
  constructor() {
    this.bidi = undefined
    this.eastAsia = undefined
    this.val = undefined
  }
  reset(applyResult?: OptionsApplyResult) {
    this.bidi = valOfReset(this.bidi, applyResult)
    this.eastAsia = valOfReset(this.eastAsia, applyResult)
    this.val = valOfReset(this.val, applyResult)
    this.fontClass = valOfReset(this.fontClass, applyResult)
  }

  clone() {
    const lang = new Lang()
    lang.bidi = this.bidi
    lang.eastAsia = this.eastAsia
    lang.val = this.val
    lang.fontClass = this.fontClass
    return lang
  }

  merge(lang: LangOptions) {
    if (null != lang.bidi) this.bidi = lang.bidi

    if (null != lang.eastAsia) this.eastAsia = lang.eastAsia

    if (null != lang.val) this.val = lang.val

    if (null != lang.fontClass) this.fontClass = lang.fontClass
  }

  setDefault() {
    this.bidi = EnUSLangID
    this.eastAsia = EnUSLangID
    this.val = EnUSLangID
    this.fontClass = FontClass.ASCII
  }

  fromOptions(
    opts: Nullable<LangOptions>,
    applyResult?: { isApplied: boolean }
  ) {
    if (opts == null) return
    this.bidi = valFromNullable(this.bidi, opts.bidi, applyResult)
    this.eastAsia = valFromNullable(this.eastAsia, opts.eastAsia, applyResult)
    this.val = valFromNullable(this.val, opts.val, applyResult)
    this.fontClass = valFromNullable(
      this.fontClass,
      opts.fontClass,
      applyResult
    )
  }

  subDiff(lang: Nullable<LangOptions>) {
    if (lang == null) return
    // Bidi
    if (null != this.bidi && this.bidi !== lang.bidi) {
      this.bidi = undefined
    }

    // EastAsia
    if (null != this.eastAsia && this.eastAsia !== lang.eastAsia) {
      this.eastAsia = undefined
    }

    // Val
    if (null != this.val && this.val !== lang.val) this.val = undefined

    // fontClass
    if (null != this.fontClass && this.fontClass !== lang.fontClass) {
      this.fontClass = undefined
    }
  }

  sameAs(lang: LangOptions) {
    if (
      this.bidi !== lang.bidi ||
      this.eastAsia !== lang.eastAsia ||
      this.val !== lang.val ||
      this.fontClass !== lang.fontClass
    ) {
      return false
    }

    return true
  }

  isEmpty() {
    if (null != this.bidi || null != this.eastAsia || null != this.val) {
      return false
    }

    return true
  }
}
