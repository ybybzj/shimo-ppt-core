import { isNumbersTheSame } from '../common/helpers'
import { assignVal } from '../../io/utils'
import { CParaIndData } from '../../io/dataType/style'
import { isFiniteNumber } from '../../common/utils'
import { Nullable } from '../../../liber/pervasive'
export interface ParaIndOptions {
  left?: number
  right?: number
  firstLine?: number
}
export class ParaInd {
  left?: number
  right?: number
  firstLine?: number
  static new(opts?: ParaIndOptions) {
    const ret = new ParaInd()
    if (opts != null) ret.fromOptions(opts)
    return ret
  }
  constructor() {
    this.left = undefined
    this.right = undefined
    this.firstLine = undefined
  }

  reset() {
    this.left = undefined
    this.right = undefined
    this.firstLine = undefined
  }

  scale(k: number) {
    if (this.left != null) {
      this.left *= k
    }
    if (this.right != null) {
      this.right *= k
    }
    if (this.firstLine != null) {
      this.firstLine *= k
    }
  }

  getCalcFirstLine() {
    if (isFiniteNumber(this.firstLine) && isFiniteNumber(this.left)) {
      if (this.firstLine! < 0 && Math.abs(this.firstLine!) > this.left!) {
        return -this.left!
      }
    }
    return this.firstLine
  }

  clone() {
    const ind = new ParaInd()
    ind.left = this.left
    ind.right = this.right
    ind.firstLine = this.firstLine
    return ind
  }

  merge(opts: Nullable<ParaIndOptions>) {
    this.fromOptions(opts)
  }

  fromOptions(opts: Nullable<ParaIndOptions>) {
    if (opts == null) return
    if (null != opts.left) this.left = opts.left

    if (null != opts.right) this.right = opts.right

    if (null != opts.firstLine) this.firstLine = opts.firstLine
  }

  sameAs(ind: Nullable<ParaIndOptions>) {
    if (
      !!ind &&
      isNumbersTheSame(this.left, ind.left) &&
      isNumbersTheSame(this.right, ind.right) &&
      isNumbersTheSame(this.firstLine, ind.firstLine)
    ) {
      return true
    }

    return false
  }

  toJSON(): CParaIndData {
    const data: CParaIndData = {}
    assignVal(data, 'left', this.left)
    assignVal(data, 'right', this.right)
    assignVal(data, 'firstLine', this.firstLine)
    return data
  }

  fromJSON(data: CParaIndData) {
    if (null != data['left']) {
      this.left = data['left']
    }
    if (null != data['right']) {
      this.right = data['right']
    }
    if (null != data['firstLine']) {
      this.firstLine = data['firstLine']
    }
  }

  isEmpty() {
    if (null != this.left || null != this.right || null != this.firstLine) {
      return false
    }

    return true
  }
}
