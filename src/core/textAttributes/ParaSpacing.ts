import { CParaSpacingData } from '../../io/dataType/style'
import { LineHeightRule } from '../common/const/attrs'
import { Nullable } from '../../../liber/pervasive'
import { isNumbersTheSame } from '../common/helpers'
export interface ParaSpacingOptions {
  line?: number
  lineRule?: number
  before?: number
  beforePercent?: number
  beforeAuto?: boolean
  after?: number
  afterPercent?: number
  afterAuto?: boolean
}

export class ParaSpacing {
  line?: number
  lineRule?: number
  before?: number
  beforePercent?: number
  beforeAuto?: boolean
  after?: number
  afterPercent?: number
  afterAuto?: boolean
  static new(opts?: ParaSpacingOptions) {
    const ret = new ParaSpacing()
    if (opts != null) ret.fromOptions(opts)
    return ret
  }
  static computeSpcVal(
    textHeight: number,
    ptVal: Nullable<number>,
    pctVal: Nullable<number>
  ): number {
    if (ptVal != null && ptVal !== 0) return ptVal
    return pctVal == null || pctVal === 0 ? 0 : textHeight * pctVal
  }

  constructor() {
    this.line = undefined
    this.lineRule = undefined
    this.before = undefined
    this.beforePercent = undefined
    this.beforeAuto = undefined
    this.after = undefined
    this.afterPercent = undefined
    this.afterAuto = undefined
  }

  reset() {
    this.line = undefined
    this.lineRule = undefined
    this.before = undefined
    this.beforePercent = undefined
    this.beforeAuto = undefined
    this.after = undefined
    this.afterPercent = undefined
    this.afterAuto = undefined
  }

  intersect(opts: Nullable<ParaSpacingOptions>) {
    if (opts == null) return undefined
    const retOpts: ParaSpacingOptions = {}
    if (this.line != null && isNumbersTheSame(this.line, opts.line)) {
      retOpts.line = opts.line
    }

    if (
      this.lineRule != null &&
      isNumbersTheSame(this.lineRule, opts.lineRule)
    ) {
      retOpts.lineRule = opts.lineRule
    }

    if (this.before != null && isNumbersTheSame(this.before, opts.before)) {
      retOpts.before = opts.before
    }
    if (this.after != null && isNumbersTheSame(this.after, opts.after)) {
      retOpts.after = opts.after
    }
    if (
      this.beforePercent != null &&
      isNumbersTheSame(this.beforePercent, opts.beforePercent)
    ) {
      retOpts.beforePercent = opts.beforePercent
    }
    if (
      this.afterPercent != null &&
      isNumbersTheSame(this.afterPercent, opts.afterPercent)
    ) {
      retOpts.afterPercent = opts.afterPercent
    }

    if (null != this.beforeAuto && this.beforeAuto === opts.beforeAuto) {
      retOpts.beforeAuto = opts.beforeAuto
    }

    if (null != this.afterAuto && this.afterAuto === opts.afterAuto) {
      retOpts.afterAuto = opts.afterAuto
    }

    return Object.keys(retOpts).length > 0
      ? ParaSpacing.new(retOpts)
      : undefined
  }
  clone() {
    return ParaSpacing.new(this)
  }

  merge(opts: Nullable<ParaSpacingOptions>) {
    this.fromOptions(opts)
  }

  fromOptions(opts: Nullable<ParaSpacingOptions>) {
    if (opts == null) return
    if (null != opts.line) this.line = opts.line

    if (null != opts.lineRule) this.lineRule = opts.lineRule

    if (null != opts.before) this.before = opts.before

    if (null != opts.beforeAuto) {
      this.beforeAuto = opts.beforeAuto
    }

    if (null != opts.after) this.after = opts.after

    if (null != opts.afterAuto) {
      this.afterAuto = opts.afterAuto
    }

    if (null != opts.beforePercent) {
      this.beforePercent = opts.beforePercent
    }

    if (null != opts.afterPercent) this.afterPercent = opts.afterPercent
  }

  sameAs(spacing: Nullable<ParaSpacing>) {
    if (spacing == null) return false
    return (
      isNumbersTheSame(this.line, spacing.line, true) &&
      isNumbersTheSame(this.lineRule, spacing.lineRule, true) &&
      isNumbersTheSame(this.before, spacing.before) &&
      isNumbersTheSame(this.beforePercent, spacing.beforePercent) &&
      ((this.beforeAuto == null && spacing.beforeAuto == null) ||
        this.beforeAuto !== spacing.beforeAuto) &&
      isNumbersTheSame(this.after, spacing.after) &&
      isNumbersTheSame(this.afterPercent, spacing.afterPercent) &&
      ((this.afterAuto == null && spacing.afterAuto == null) ||
        this.afterAuto !== spacing.afterAuto)
    )
  }

  toJSON(): CParaSpacingData {
    const data: CParaSpacingData = {}
    if (this.line != null) {
      data['lineSpacing'] = {
        ['type']: this.lineRule === LineHeightRule.Auto ? 'percent' : 'point',
        ['value']:
          this.lineRule === LineHeightRule.Auto
            ? toPercentVal(this.line)
            : toPointsVal(this.line),
      }
    }
    if (this.after != null) {
      data['spaceAfter'] = {
        ['type']: this.afterPercent! > 0 ? 'percent' : 'point',
        ['value']:
          this.afterPercent! > 0
            ? toPercentVal(this.afterPercent!)
            : toPointsVal(this.after),
      }
    }

    if (this.before != null) {
      data['spaceBefore'] = {
        ['type']: this.beforePercent! > 0 ? 'percent' : 'point',
        ['value']:
          this.beforePercent! > 0
            ? toPercentVal(this.beforePercent!)
            : toPointsVal(this.before),
      }
    }
    return data
  }

  fromJSON(data: CParaSpacingData) {
    if (data['lineSpacing'] != null) {
      this.lineRule =
        data['lineSpacing']['type'] === 'percent'
          ? LineHeightRule.Auto
          : LineHeightRule.Exact
      const val = data['lineSpacing']['value']
      this.line =
        data['lineSpacing']['type'] === 'percent'
          ? fromPercentVal(val)
          : fromPointsVal(val)
    }

    if (data['spaceAfter'] != null) {
      const spaceType = data['spaceAfter']['type']
      const val = data['spaceAfter']['value']
      if (spaceType === 'percent') {
        this.afterPercent = fromPercentVal(val)
        this.after = 0
      } else {
        this.after = fromPointsVal(val)
      }
    }

    if (data['spaceBefore'] != null) {
      const spaceType = data['spaceBefore']['type']
      const val = data['spaceBefore']['value']
      if (spaceType === 'percent') {
        this.beforePercent = fromPercentVal(val)
        this.before = 0
      } else {
        this.before = fromPointsVal(val)
      }
    }
  }

  isEmpty() {
    if (
      null != this.line ||
      null != this.lineRule ||
      null != this.before ||
      null != this.beforeAuto ||
      null != this.after ||
      null != this.afterAuto ||
      null != this.beforePercent ||
      null != this.afterPercent
    ) {
      return false
    }

    return true
  }
}

// helper
function toPointsVal(n: number): number {
  let val = (n / 0.00352777778) >> 0
  if (val < 0) {
    val = 0
  }
  if (val > 158400) {
    val = 158400
  }

  return val
}

function fromPointsVal(n: number): number {
  return n * 0.00352777778
}

function toPercentVal(n: number): number {
  return (n * 100000) >> 0
}

function fromPercentVal(n: number): number {
  return n / 100000
}
