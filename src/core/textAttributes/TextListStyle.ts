import { isDict } from '../../common/utils'
import { ParaPr } from './ParaPr'
import { Nullable } from '../../../liber/pervasive'
import { NULL_VALUE, readFromJSON, toJSON } from '../../io/utils'
import { TextListStyleData } from '../../io/dataType/style'
export class TextListStyle {
  levels: Array<Nullable<ParaPr>>
  constructor() {
    this.levels = new Array(10)
    for (let i = 0; i < 10; i++) this.levels[i] = null
  }

  clone() {
    const duplicate = new TextListStyle()
    for (let i = 0; i < 10; ++i) {
      if (this.levels[i] != null) {
        duplicate.levels[i] = this.levels[i]!.clone()
      }
    }
    return duplicate
  }

  toJSON(): TextListStyleData {
    const data: TextListStyleData = []
    for (let i = 0; i < 10; ++i) {
      if (isDict(this.levels[i])) {
        data.push(toJSON(this.levels[i]!))
      } else {
        data.push(NULL_VALUE)
      }
    }
    return data
  }
  fromJSON(data: TextListStyleData) {
    data.forEach((lvlData, i) => {
      if (lvlData === NULL_VALUE) {
        this.levels[i] = null
      } else {
        this.levels[i] = readFromJSON(lvlData, ParaPr)
      }
    })
  }

  merge(textListStyle?: TextListStyle) {
    if (!textListStyle) {
      return
    }
    for (let i = 0; i < this.levels.length; ++i) {
      const level = textListStyle.levels[i]
      if (level) {
        if (this.levels[i]) {
          this.levels[i]?.merge(level)
        } else {
          this.levels[i] = level.clone()
        }
      }
    }
  }

  collectAllFontNames(allFonts) {
    this.levels.forEach((level) => {
      if (level) {
        if (level.defaultRunPr) {
          level.defaultRunPr.collectAllFontNames(allFonts)
        }
        if (level.bullet) {
          level.bullet.collectAllFontNames(allFonts)
        }
      }
    })
  }
}
