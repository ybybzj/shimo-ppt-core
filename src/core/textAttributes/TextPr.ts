import { FontsProps, RFonts, RFontsOptions } from './RFonts'
import { Lang, LangOptions } from './Lang'
import { HightLight_None, VertAlignFactor } from '../common/const/unit'
import { FontStyleInd } from '../SlideElement/const'
import { VertAlignType } from '../common/const/attrs'
import {
  fromOptions,
  intersectObjects,
  isObjectsTheSame,
  nullableToNull,
} from '../common/helpers'
import { Dict, Nullable } from '../../../liber/pervasive'
import { assignVal, assignJSON, readFromJSON } from '../../io/utils'
import {
  CTextPrData,
  toCapType,
  toStrikeType,
  toTextPrSpacing,
  toUnderlineType,
  toBaseline,
  TextPrVertAlign,
  toTextPrFontSize,
  TextPrFont,
  fromCapType,
  fromStrikeType,
  fromBaseline,
  fromTextPrSpacing,
  fromUnderlineType,
  fromTextPrFontSize,
} from '../../io/dataType/style'
import { InstanceType } from '../instanceTypes'
import { CComplexColor } from '../color/complexColor'
import { FillEffects } from '../SlideElement/attrs/fill/fill'
import { FontRef } from '../SlideElement/attrs/refs'
import { HyperlinkProps } from '../SlideElement/attrs/HyperlinkProps'
import { hookManager, Hooks } from '../hooks'
import { Ln, LnOptions } from '../SlideElement/attrs/line'
import {
  objFromNullable,
  valForEmptyProp,
  valFromNullable,
  valOfReset,
} from './utils'
import { OptionsApplyResult } from './type'
import { PoolAllocator, Recyclable } from '../../common/allocator'

export interface TextPrOptions {
  bold?: boolean | null
  italic?: boolean | null
  strikeout?: boolean | null
  underline?: boolean | null
  fontFamily?: FontsProps | null
  fontSize?: number | null
  vertAlign?: TextPrVertAlign | null
  highLight?: (typeof HightLight_None | CComplexColor) | null
  rStyle?: string | null
  spacing?: number | null
  doubleStrike?: boolean | null
  caps?: boolean | null
  smallCaps?: boolean | null
  rFonts?: RFontsOptions | null
  cs?: boolean | null
  rtl?: boolean | null
  lang?: LangOptions | null
  fillEffects?: FillEffects | null
  fontRef?: FontRef | null
  textStroke?: LnOptions | null

  // hlink?: HyperlinkPropsOptions | null
}

export class TextPr extends Recyclable<TextPr> {
  readonly instanceType = InstanceType.TextPr
  bold?: boolean
  italic?: boolean
  strikeout?: boolean
  underline?: boolean
  fontFamily?: FontsProps
  fontSize?: number
  vertAlign?: TextPrVertAlign
  highLight?: typeof HightLight_None | CComplexColor
  rStyle?: string
  spacing?: number
  doubleStrike?: boolean
  caps?: boolean
  smallCaps?: boolean
  rFonts: RFonts

  cs?: boolean
  rtl?: boolean
  lang: Lang
  fillEffects?: FillEffects
  fontRef?: FontRef
  textStroke?: Ln

  // extra props that won't involve clone and option setting
  hlink?: HyperlinkProps

  private static allocator: PoolAllocator<
    TextPr,
    [applyResult?: OptionsApplyResult]
  > = PoolAllocator.getAllocator<TextPr, [applyResult?: OptionsApplyResult]>(
    TextPr
  )
  static new(opts?: TextPrOptions) {
    const ret = TextPr.allocator.allocate()
    opts && ret.fromOptions(opts)
    return ret
  }
  static recycle(textPr: TextPr) {
    TextPr.allocator.recycle(textPr)
  }
  static nulledOptions(opts: TextPrOptions, isToNull = false) {
    const ret: TextPrOptions = {
      bold: nullableToNull(isToNull, opts.bold),
      italic: nullableToNull(isToNull, opts.italic),
      strikeout: nullableToNull(isToNull, opts.strikeout),
      underline: nullableToNull(isToNull, opts.underline),
      fontFamily: nullableToNull(isToNull, opts.fontFamily),
      fontSize: nullableToNull(isToNull, opts.fontSize),
      vertAlign: nullableToNull(isToNull, opts.vertAlign),
      highLight: nullableToNull(isToNull, opts.highLight),
      rStyle: nullableToNull(isToNull, opts.rStyle),
      spacing: nullableToNull(isToNull, opts.spacing),
      doubleStrike: nullableToNull(isToNull, opts.doubleStrike),
      caps: nullableToNull(isToNull, opts.caps),
      smallCaps: nullableToNull(isToNull, opts.smallCaps),
      rFonts: opts.rFonts
        ? RFonts.nulledOptions(opts.rFonts, isToNull)
        : isToNull
        ? null
        : undefined,
      cs: nullableToNull(isToNull, opts.cs),
      rtl: nullableToNull(isToNull, opts.rtl),
      lang: opts.lang
        ? Lang.nulledOptions(opts.lang, isToNull)
        : isToNull
        ? null
        : undefined,
      fillEffects: nullableToNull(isToNull, opts.fillEffects),
      fontRef: nullableToNull(isToNull, opts.fontRef),
      textStroke: nullableToNull(isToNull, opts.textStroke),
    }

    return ret
  }
  constructor() {
    super()
    this.bold = undefined
    this.italic = undefined
    this.strikeout = undefined
    this.underline = undefined
    this.fontFamily = undefined
    this.fontSize = undefined
    this.vertAlign = undefined
    this.highLight = undefined // NoneHighlight/Color
    this.rStyle = undefined
    this.spacing = undefined
    this.doubleStrike = undefined
    this.caps = undefined
    this.smallCaps = undefined

    this.rFonts = new RFonts()

    this.cs = undefined
    this.lang = new Lang()
    this.fillEffects = undefined
    this.fontRef = undefined

    this.textStroke = undefined
  }
  reset(applyResult?: OptionsApplyResult) {
    this.bold = valOfReset(this.bold, applyResult)
    this.italic = valOfReset(this.italic, applyResult)
    this.strikeout = valOfReset(this.strikeout, applyResult)
    this.underline = valOfReset(this.underline, applyResult)
    this.fontFamily = valOfReset(this.fontFamily, applyResult)
    this.fontSize = valOfReset(this.fontSize, applyResult)
    this.vertAlign = valOfReset(this.vertAlign, applyResult)
    this.highLight = valOfReset(this.highLight, applyResult) // NoneHighlight/Color
    this.rStyle = valOfReset(this.rStyle, applyResult)
    this.spacing = valOfReset(this.spacing, applyResult)
    this.doubleStrike = valOfReset(this.doubleStrike, applyResult)
    this.caps = valOfReset(this.caps, applyResult)
    this.smallCaps = valOfReset(this.smallCaps, applyResult)

    this.rFonts.reset(applyResult)

    this.cs = valOfReset(this.cs, applyResult)
    this.lang?.reset(applyResult)
    this.fillEffects = valOfReset(this.fillEffects, applyResult)
    this.fontRef = valOfReset(this.fontRef, applyResult)

    this.textStroke = valOfReset(this.textStroke, applyResult)
    this.hlink = undefined
  }

  fillEmptyProps(
    opts: Nullable<TextPrOptions>,
    applyResult?: OptionsApplyResult
  ) {
    if (opts == null) return
    this.bold = valForEmptyProp(this.bold, opts.bold, applyResult)
    this.italic = valForEmptyProp(this.italic, opts.italic, applyResult)
    this.strikeout = valForEmptyProp(
      this.strikeout,
      opts.strikeout,
      applyResult
    )
    this.underline = valForEmptyProp(
      this.underline,
      opts.underline,
      applyResult
    )
    this.fontFamily = valForEmptyProp(
      this.fontFamily,
      opts.fontFamily,
      applyResult
    )
    this.fontSize = valForEmptyProp(this.fontSize, opts.fontSize, applyResult)
    this.vertAlign = valForEmptyProp(
      this.vertAlign,
      opts.vertAlign,
      applyResult
    )
    this.highLight = valForEmptyProp(
      this.highLight,
      opts.highLight,
      applyResult
    ) // NoneHighlight/Color
    this.rStyle = valForEmptyProp(this.rStyle, opts.rStyle, applyResult)
    this.spacing = valForEmptyProp(this.spacing, opts.spacing, applyResult)
    this.doubleStrike = valForEmptyProp(
      this.doubleStrike,
      opts.doubleStrike,
      applyResult
    )
    this.caps = valForEmptyProp(this.caps, opts.caps, applyResult)
    this.smallCaps = valForEmptyProp(
      this.smallCaps,
      opts.smallCaps,
      applyResult
    )
    if (this.rFonts.isEmpty()) {
      this.rFonts.fromOptions(opts.rFonts, applyResult)
    }

    this.cs = valForEmptyProp(this.cs, opts.cs, applyResult)
    if (this.lang == null || this.lang.isEmpty()) {
      this.lang = fromOptions(
        this.lang,
        opts.lang,
        Lang,
        undefined,
        applyResult
      )!
    }
    this.fillEffects = valForEmptyProp(
      this.fillEffects,
      opts.fillEffects,
      applyResult
    )
    this.fontRef = valForEmptyProp(this.fontRef, opts.fontRef, applyResult)

    if (this.textStroke == null) {
      this.textStroke = fromOptions(this.textStroke, opts.textStroke, Ln)
      if (this.textStroke) {
        if (applyResult) applyResult.isApplied = true
      }
    }
  }

  clearTextFormat(applyResult?: OptionsApplyResult) {
    this.bold = valOfReset(this.bold, applyResult)
    this.italic = valOfReset(this.italic, applyResult)
    this.strikeout = valOfReset(this.strikeout, applyResult)
    this.underline = valOfReset(this.underline, applyResult)
    this.fontSize = valOfReset(this.fontSize, applyResult)
    this.vertAlign = valOfReset(this.vertAlign, applyResult)
    this.fillEffects = valOfReset(this.fillEffects, applyResult)
    this.spacing = valOfReset(this.spacing, applyResult)
    this.caps = valOfReset(this.caps, applyResult)
    this.smallCaps = valOfReset(this.smallCaps, applyResult)

    this.rFonts?.reset(applyResult)
    this.textStroke = valOfReset(this.textStroke, applyResult)
  }

  clone() {
    return TextPr.new(this)
  }

  merge(textPr: TextPrOptions) {
    this.fromOptions(textPr)
  }

  setDefault() {
    this.bold = false
    this.italic = false
    this.underline = false
    this.strikeout = false
    this.fontFamily = {
      name: 'Arial',
      index: -1,
    }
    this.fontSize = 11
    this.vertAlign = VertAlignType.Baseline
    this.highLight = HightLight_None
    this.rStyle = undefined
    this.spacing = 0
    this.doubleStrike = false
    this.smallCaps = false
    this.caps = false
    this.rFonts.setDefault()

    this.cs = false
    this.lang!.setDefault()
    this.fillEffects = undefined
    this.fontRef = undefined

    this.textStroke = undefined
  }

  fromOptions(opts: Nullable<TextPrOptions>, applyResult?: OptionsApplyResult) {
    if (opts == null || isOptionsEmpty(opts)) return

    this.bold = valFromNullable(this.bold, opts.bold, applyResult)
    if (applyResult?.isApplied) applyResult.needReMeasure = true

    this.italic = valFromNullable(this.italic, opts.italic, applyResult)
    if (applyResult?.isApplied) applyResult.needReMeasure = true

    this.strikeout = valFromNullable(
      this.strikeout,
      opts.strikeout,
      applyResult
    )

    this.underline = valFromNullable(
      this.underline,
      opts.underline,
      applyResult
    )

    if (opts.fontFamily !== undefined) {
      this.fontFamily =
        opts.fontFamily !== null
          ? {
              name: opts.fontFamily.name,
              index: opts.fontFamily.index,
            }
          : undefined
      if (applyResult) {
        applyResult.isApplied = true
        applyResult.needReMeasure = true
      }
    }

    this.fontSize = valFromNullable(this.fontSize, opts.fontSize, applyResult)
    if (applyResult?.isApplied) applyResult.needReMeasure = true

    this.vertAlign = valFromNullable(
      this.vertAlign,
      opts.vertAlign,
      applyResult
    )
    if (applyResult?.isApplied) applyResult.needReMeasure = true

    if (opts.highLight !== undefined) {
      let isApplied = false
      if (opts.highLight === null) {
        isApplied = this.highLight !== undefined
        this.highLight = undefined
      } else if (opts.highLight === HightLight_None) {
        isApplied = this.highLight !== opts.highLight
        this.highLight = opts.highLight
      } else {
        isApplied =
          this.highLight == null || this.highLight === HightLight_None
            ? true
            : !isObjectsTheSame(this.highLight, opts.highLight)
        this.highLight = opts.highLight
      }
      if (applyResult && isApplied === true) {
        applyResult.isApplied = true
      }
    }
    this.highLight = valFromNullable(
      this.highLight,
      opts.highLight,
      applyResult
    )

    this.rStyle = valFromNullable(this.rStyle, opts.rStyle, applyResult)
    this.spacing = valFromNullable(this.spacing, opts.spacing, applyResult)
    if (applyResult?.isApplied) applyResult.needReMeasure = true

    this.doubleStrike = valFromNullable(
      this.doubleStrike,
      opts.doubleStrike,
      applyResult
    )

    this.caps = valFromNullable(this.caps, opts.caps, applyResult)
    if (applyResult?.isApplied) applyResult.needReMeasure = true

    this.smallCaps = valFromNullable(
      this.smallCaps,
      opts.smallCaps,
      applyResult
    )
    if (applyResult?.isApplied) applyResult.needReMeasure = true

    if (undefined !== opts.rFonts) {
      if (opts.rFonts === null) {
        this.rFonts.reset(applyResult)
      } else {
        this.rFonts.fromOptions(opts.rFonts, applyResult)
      }

      if (applyResult?.isApplied) applyResult.needReMeasure = true
    }

    this.cs = valFromNullable(this.cs, opts.cs, applyResult)

    if (undefined !== opts.lang) {
      if (opts.lang === null) {
        this.lang?.reset(applyResult)
      } else {
        this.lang = fromOptions(
          this.lang,
          opts.lang,
          Lang,
          undefined,
          applyResult
        )!
      }

      if (applyResult?.isApplied) applyResult.needReMeasure = true
    }

    this.fillEffects = objFromNullable(
      this.fillEffects,
      opts.fillEffects,
      applyResult
    )
    this.fontRef = objFromNullable(this.fontRef, opts.fontRef, applyResult)

    if (opts.textStroke !== undefined) {
      if (opts.textStroke === null) {
        this.textStroke = undefined
      } else {
        this.textStroke = fromOptions(
          this.textStroke,
          opts.textStroke,
          Ln,
          undefined,
          applyResult
        )
      }
    }
  }
  refreshTheme() {
    if (this.fontRef && !this.fillEffects) {
      let prefix: string
      if (this.fontRef.idx === FontStyleInd.minor) {
        prefix = '+mn-'
      } else {
        prefix = '+mj-'
      }
      this.rFonts.fromOptions({
        ascii: {
          name: prefix + 'lt',
          index: -1,
        },
        eastAsia: {
          name: prefix + 'ea',
          index: -1,
        },
        hAnsi: {
          name: prefix + 'lt',
          index: -1,
        },
        cs: {
          name: prefix + 'lt',
          index: -1,
        },
      })
      if (this.fontRef.color && !this.fillEffects) {
        this.fillEffects = FillEffects.SolidCColor(this.fontRef.color)
      }
    }
  }

  subDiff(textPr: Nullable<TextPrOptions>) {
    if (textPr == null) return this
    // Bold
    if (null != this.bold && this.bold !== textPr.bold) {
      this.bold = undefined
    }

    // Italic
    if (null != this.italic && this.italic !== textPr.italic) {
      this.italic = undefined
    }

    // Strikeout
    if (null != this.strikeout && this.strikeout !== textPr.strikeout) {
      this.strikeout = undefined
    }

    // Underline
    if (null != this.underline && this.underline !== textPr.underline) {
      this.underline = undefined
    }

    // FontFamily
    if (
      null != this.fontFamily &&
      (null == textPr.fontFamily ||
        this.fontFamily.name !== textPr.fontFamily.name)
    ) {
      this.fontFamily = undefined
    }

    // FontSize
    if (
      null != this.fontSize &&
      (null == textPr.fontSize ||
        Math.abs(this.fontSize - textPr.fontSize) >= 0.001)
    ) {
      this.fontSize = undefined
    }

    // VertAlign
    if (null != this.vertAlign && this.vertAlign !== textPr.vertAlign) {
      this.vertAlign = undefined
    }

    // HighLight
    if (
      null != this.highLight &&
      (null == textPr.highLight ||
        (HightLight_None === this.highLight &&
          HightLight_None !== textPr.highLight) ||
        (HightLight_None !== this.highLight &&
          HightLight_None === textPr.highLight) ||
        (HightLight_None !== this.highLight &&
          HightLight_None !== textPr.highLight &&
          true !== this.highLight.sameAs(textPr.highLight)))
    ) {
      this.highLight = undefined
    }

    // RStyle
    if (
      null != this.rStyle &&
      (null == textPr.rStyle || this.rStyle !== textPr.rStyle)
    ) {
      this.rStyle = undefined
    }

    // Spacing
    if (
      null != this.spacing &&
      (null == textPr.spacing ||
        Math.abs(this.spacing - textPr.spacing) >= 0.001)
    ) {
      this.spacing = undefined
    }

    // DStrikeout
    if (
      null != this.doubleStrike &&
      (null == textPr.doubleStrike || this.doubleStrike !== textPr.doubleStrike)
    ) {
      this.doubleStrike = undefined
    }

    // Caps
    if (
      null != this.caps &&
      (null == textPr.caps || this.caps !== textPr.caps)
    ) {
      this.caps = undefined
    }

    // SmallCaps
    if (
      null != this.smallCaps &&
      (null == textPr.smallCaps || this.smallCaps !== textPr.smallCaps)
    ) {
      this.smallCaps = undefined
    }

    // RFonts
    this.rFonts.subDiff(textPr.rFonts)

    // CS
    if (null != this.cs && this.cs !== textPr.cs) this.cs = undefined

    // Lang
    if (textPr.lang) {
      this.lang!.subDiff(textPr.lang)
    }

    if (
      null != this.fillEffects &&
      !this.fillEffects.sameAs(textPr.fillEffects)
    ) {
      this.fillEffects =
        intersectObjects(this.fillEffects, textPr.fillEffects) ?? undefined
      if (null == this.fillEffects) {
        this.fillEffects = undefined
      }
    }

    if (null != this.textStroke && !this.textStroke.sameAs(textPr.textStroke)) {
      this.textStroke = this.textStroke.intersect(textPr.textStroke)
      if (null == this.textStroke) {
        this.textStroke = undefined
      }
    }

    return this
  }
  isCompatible(textPr: TextPr) {
    if (textPr == null) {
      return false
    }
    const compareProps = [
      /* Bold */ [this.bold, textPr.bold],
      /* Italic */ [this.italic, textPr.italic],
      /* FontFamily */ [this.fontFamily?.name, textPr.fontFamily?.name],
      /* FontSize */ [this.fontSize, textPr.fontSize],
      /* VertAlign */ [this.vertAlign, textPr.vertAlign],
      /* Caps*/ [this.caps, textPr.caps],
      /* SmallCaps */ [this.smallCaps, textPr.smallCaps],
      /* CS */ [this.cs, textPr.cs],
    ]
    for (let i = 0, l = compareProps.length; i < l; i++) {
      const [v1, v2] = compareProps[i]
      if (!isEqualValues(v1, v2)) {
        return false
      }
    }

    /* TODO RFonts */

    // Lang
    if (
      (this.lang == null && textPr.lang != null) ||
      (this.lang != null && textPr.lang == null) ||
      (this.lang != null &&
        textPr.lang != null &&
        !this.lang.sameAs(textPr.lang))
    ) {
      return false
    }

    return true
  }
  toJSON(): CTextPrData {
    const data: CTextPrData = {}

    assignVal(data, 'bold', this.bold)
    assignVal(data, 'italic', this.italic)

    assignVal(data, 'cap', toCapType(this.caps, this.smallCaps))

    if (this.lang != null && this.lang.val != null) {
      assignVal(data, 'lang', this.lang.val)
    }

    assignVal(data, 'strike', toStrikeType(this.strikeout))

    if (this.spacing != null) {
      assignVal(data, 'spc', toTextPrSpacing(this.spacing))
    }

    if (this.underline != null) {
      assignVal(data, 'underline', toUnderlineType(this.underline))
    }

    if (this.fontSize != null) {
      assignVal(data, 'fontSize', toTextPrFontSize(this.fontSize))
    }

    if (this.vertAlign != null) {
      assignVal(data, 'baseline', toBaseline(this.vertAlign))
    }

    assignJSON(data, 'fill', this.fillEffects)
    assignJSON(data, 'ln', this.textStroke)

    if (this.highLight && this.highLight !== HightLight_None) {
      assignJSON(data, 'highlight', this.highLight)
    }

    if (this.rFonts != null) {
      if (this.rFonts.ascii) {
        assignVal(data, 'latin', {
          ['typeface']: this.rFonts.ascii.name,
        } as TextPrFont)
      }
      if (this.rFonts.eastAsia) {
        assignVal(data, 'ea', {
          ['typeface']: this.rFonts.eastAsia.name,
        } as TextPrFont)
      }
      if (this.rFonts.cs) {
        assignVal(data, 'cs', {
          ['typeface']: this.rFonts.cs.name,
        } as TextPrFont)
      }
    }

    assignJSON(data, 'hlinkClick', this.hlink)
    // assignJSON(data, 'hlinkMouseOver', this.hlink)

    if (this.fontFamily != null) {
      assignVal(data, 'fontFamily', {
        ['Name']: this.fontFamily.name,
        ['Index']: this.fontFamily.index,
      })
    }
    return data
  }

  fromJSON(data: CTextPrData) {
    if (null != data['fontFamily']) {
      this.fontFamily = {
        name: data['fontFamily']['Name'],
        index: data['fontFamily']['Index'],
      }
    }
    if (data['bold'] != null) {
      this.bold = data['bold']
    }

    if (data['italic'] != null) {
      this.italic = data['italic']
    }

    if (data['cap'] != null) {
      const cap = fromCapType(data['cap'])
      if (cap) {
        this.caps = cap.cap
        this.smallCaps = cap.smallCap
      }
    }

    if (data['lang'] != null || data['altLang'] != null) {
      const lang = data['lang'] || data['altLang']
      this.lang!.val = lang
    }

    if (data['strike'] != null) {
      const strike = fromStrikeType(data['strike'])
      if (strike) {
        this.strikeout = strike.strikeout
      }
    }

    if (null != data['baseline']) {
      this.vertAlign = fromBaseline(data['baseline'])
    }

    if (null != data['spc']) {
      this.spacing = fromTextPrSpacing(data['spc'])
    }

    if (null != data['underline']) {
      this.underline = fromUnderlineType(data['underline'])
    }

    if (null != data['fontSize']) {
      this.fontSize = fromTextPrFontSize(data['fontSize'])
    }

    if (null != data['fill']) {
      this.fillEffects = readFromJSON(data['fill'], FillEffects)
    }

    if (null != data['ln']) {
      this.textStroke = readFromJSON(data['ln'], Ln)
    }

    if (null != data['highlight']) {
      const ccolor = readFromJSON(data['highlight'], CComplexColor)
      this.highLight = ccolor ? ccolor : HightLight_None
    }

    if (null != data['latin']) {
      this.rFonts.ascii = { name: data['latin']['typeface'] || '', index: -1 }
      this.rFonts.hAnsi = { name: this.rFonts.ascii!.name, index: -1 }
    }

    if (null != data['ea']) {
      this.rFonts.eastAsia = { name: data['ea']['typeface'] || '', index: -1 }
    }

    if (null != data['cs']) {
      this.rFonts.cs = { name: data['cs']['typeface'] || '', index: -1 }
    }
    if (null != data['hlinkClick'] || null != data['hlinkMouseOver']) {
      const hlink = data['hlinkClick'] || data['hlinkMouseOver']
      this.hlink = readFromJSON(hlink, HyperlinkProps)
    }

    this.collectFonts()
  }

  getFontFactor() {
    let fontFactor = 1

    switch (this.vertAlign) {
      case VertAlignType.Baseline: {
        fontFactor = 1
        break
      }
      case VertAlignType.SubScript:
      case VertAlignType.SuperScript: {
        fontFactor = VertAlignFactor.Size
        break
      }
    }

    return fontFactor
  }

  collectFonts() {
    const rFonts = this.rFonts ?? {}
    const fonts = Object.values(rFonts).reduce((fonts: string[], font) => {
      if (typeof font === 'object' && font.name) {
        fonts.push(font.name)
      }
      return fonts
    }, [])
    fonts.forEach((font) => {
      hookManager.invoke(Hooks.Attrs.CollectFontFromDataChange, {
        font,
      })
    })
  }

  collectAllFontNames(allFonts: Dict<boolean>) {
    if (null != this.rFonts.ascii) allFonts[this.rFonts.ascii.name] = true

    if (null != this.rFonts.hAnsi) allFonts[this.rFonts.hAnsi.name] = true

    if (null != this.rFonts.eastAsia) {
      allFonts[this.rFonts.eastAsia.name] = true
    }

    if (null != this.rFonts.cs) allFonts[this.rFonts.cs.name] = true
  }

  sameAs(textPr: TextPrOptions) {
    if (this.bold !== textPr.bold) return false

    if (this.italic !== textPr.italic) return false

    if (this.strikeout !== textPr.strikeout) return false

    if (this.underline !== textPr.underline) return false

    if (
      (null == this.fontFamily && null != textPr.fontFamily) ||
      (null != this.fontFamily &&
        (null == textPr.fontFamily ||
          this.fontFamily.name !== textPr.fontFamily.name))
    ) {
      return false
    }

    if (
      false ||
      (null == this.fontSize && null != textPr.fontSize) ||
      (null != this.fontSize &&
        (null == textPr.fontSize ||
          Math.abs(this.fontSize - textPr.fontSize) >= 0.001))
    ) {
      return false
    }

    if (this.vertAlign !== textPr.vertAlign) return false

    if (
      (null == this.highLight && null != textPr.highLight) ||
      (null != this.highLight &&
        (null == textPr.highLight ||
          (HightLight_None === this.highLight &&
            HightLight_None !== textPr.highLight) ||
          (HightLight_None !== this.highLight &&
            HightLight_None === textPr.highLight) ||
          (HightLight_None !== this.highLight &&
            HightLight_None !== textPr.highLight &&
            true !== this.highLight.sameAs(textPr.highLight))))
    ) {
      return false
    }

    if (this.rStyle !== textPr.rStyle) return false

    if (
      (null == this.spacing && null != textPr.spacing) ||
      (null != this.spacing &&
        (null == textPr.spacing ||
          Math.abs(this.spacing - textPr.spacing) >= 0.001))
    ) {
      return false
    }

    if (this.doubleStrike !== textPr.doubleStrike) return false

    if (this.caps !== textPr.caps) return false

    if (this.smallCaps !== textPr.smallCaps) return false

    if (true !== isObjectsTheSame(this.rFonts, textPr.rFonts)) return false

    if (this.cs !== textPr.cs) return false

    if (true !== isObjectsTheSame(this.lang, textPr.lang)) return false

    if (
      (null == this.fillEffects && null != textPr.fillEffects) ||
      (null != this.fillEffects &&
        (null == textPr.fillEffects ||
          true !== this.fillEffects.sameAs(textPr.fillEffects)))
    ) {
      return false
    }

    if (
      (null == this.textStroke && null != textPr.textStroke) ||
      (null != this.textStroke &&
        (null == textPr.textStroke ||
          true !== this.textStroke.sameAs(textPr.textStroke)))
    ) {
      return false
    }

    return true
  }

  isEmpty() {
    if (
      null != this.bold ||
      null != this.italic ||
      null != this.strikeout ||
      null != this.underline ||
      null != this.fontFamily ||
      null != this.fontSize ||
      null != this.vertAlign ||
      null != this.highLight ||
      null != this.rStyle ||
      null != this.spacing ||
      null != this.doubleStrike ||
      null != this.caps ||
      null != this.smallCaps ||
      true !== this.rFonts.isEmpty() ||
      null != this.cs ||
      true !== this.lang!.isEmpty() ||
      null != this.fillEffects ||
      null != this.fontRef ||
      null != this.textStroke
    ) {
      return false
    }

    return true
  }

  // setters
  setUnderline(v: Nullable<boolean>): boolean {
    v = v == null ? undefined : v
    const isApplied = v !== this.underline
    if (isApplied) {
      this.underline = v
    }
    return isApplied
  }

  setFontSize(fontSize: Nullable<number>) {
    fontSize = fontSize ?? undefined
    const isApplied = fontSize !== this.fontSize
    if (isApplied) {
      this.fontSize = fontSize
    }
    return isApplied
  }

  setFillEffects(fill: Nullable<FillEffects>) {
    fill = fill ?? undefined
    const isApplied = !isObjectsTheSame(fill, this.fillEffects)
    if (isApplied) {
      this.fillEffects = fill
    }
    return isApplied
  }

  setSpacing(v: Nullable<number>) {
    v = v ?? undefined
    const isApplied = v !== this.spacing
    if (isApplied) {
      this.spacing = v
    }
    return isApplied
  }
}

// helper
function isEqualValues(
  v1: string | number | boolean | null | undefined,
  v2: string | number | boolean | null | undefined
) {
  if (v1 == null && v2 == null) {
    return true
  }
  if (typeof v1 === 'number' && typeof v2 === 'number') {
    return Math.abs(v1 - v2) < 0.001
  }
  return v1 === v2
}

function isOptionsEmpty(opts: TextPrOptions): boolean {
  if (
    undefined !== opts.bold ||
    undefined !== opts.italic ||
    undefined !== opts.strikeout ||
    undefined !== opts.underline ||
    undefined !== opts.fontFamily ||
    undefined !== opts.fontSize ||
    undefined !== opts.vertAlign ||
    undefined !== opts.highLight ||
    undefined !== opts.rStyle ||
    undefined !== opts.spacing ||
    undefined !== opts.doubleStrike ||
    undefined !== opts.caps ||
    undefined !== opts.smallCaps ||
    undefined !== opts.rFonts ||
    undefined !== opts.cs ||
    undefined !== opts.lang ||
    undefined !== opts.fillEffects ||
    undefined !== opts.fontRef ||
    undefined !== opts.textStroke
  ) {
    return false
  }

  return true
}
