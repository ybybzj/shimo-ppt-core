export interface OptionsApplyResult {
  needReMeasure?: boolean
  isApplied: boolean
}
