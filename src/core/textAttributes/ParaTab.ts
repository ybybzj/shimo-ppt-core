import { assignVal, scaleValue } from '../../io/utils'
import {
  CParaTabData,
  TextTabAlignTypeValues,
  toTextTabAlignType,
  fromTextTabAlignType,
} from '../../io/dataType/style'
import { ScaleOfPPTXSizes } from '../common/const/unit'
import { Nullable } from '../../../liber/pervasive'
export const DefaultTabStop = 12.5
export interface ParaTabStopOptions {
  value?: TextTabAlignTypeValues
  index?: number
}
export class ParaTabStop {
  value?: TextTabAlignTypeValues
  index?: number

  constructor(v?: TextTabAlignTypeValues, index?: number) {
    this.value = v
    this.index = index
  }

  fromOptions(opts: Nullable<ParaTabStopOptions>) {
    if (opts == null) return
    if (opts.value != null) this.value = opts.value
    if (opts.index != null) this.index = opts.index
  }

  toJSON(): CParaTabData {
    const data: CParaTabData = {}
    if (this.value != null) {
      assignVal(data, 'algn', toTextTabAlignType(this.value))
    }

    assignVal(data, 'position', scaleValue(this.index, ScaleOfPPTXSizes))
    return data
  }

  fromJSON(data: CParaTabData) {
    if (data['algn']) {
      this.value = fromTextTabAlignType(data['algn'])
    }
    if (data['position']) {
      this.index = data['position'] / ScaleOfPPTXSizes
    }
  }

  clone() {
    return new ParaTabStop(this.value, this.index)
  }

  sameAs(tab: Nullable<ParaTabStopOptions>) {
    return tab != null && this.value === tab.value && this.index === tab.index
  }
}
