# Shimo PPT Core SDK

## 开始

```shell
# in your workspace
git clone  git@github.com:ShimoFour/shimo-ppt-core.git
cd shimo-ppt-core
yarn install
yarn start
```

## Git 分支/npm 版本说明

在 push 不同分支代码后，CI 会自动发布 npm 包，版本号由 bump_version 脚本自动生成.

| core 分支            | core 版本                                                 |
| -------------------- | --------------------------------------------------------- |
| release              | `{major}.{minor}.{patch}` 如 0.0.2                        |
| master               | `{major}.{minor}.{patch}-beta.x` 如 0.0.3-beta.3          |
| feature/fix 功能分支 | `{major}.{minor}.{patch}-{feature}.x` 如 0.0.3-add-icon.3 |

## 目录结构

### src

源代码目录， 入口为`index.ts`

src中的主要目录有

#### editor

存放顶层接口代码，它依赖其他部分的代码，而其它代码不会依赖它

#### core

核心ppt业务逻辑代码，包括Presentation/Slide/SlideLayout/SlideMaster等容器，及shape/image/table/graphicframe/geometry等容器内元素的数据结构和操作绘制逻辑
大体按照三部分组织

- 基础数据层 - 按照ooxml标准的定义的各实体类型的数据结构，如果通过数据操作接口更改了基础数据，会通过`EditChangesStack`记录下来，之后通知绘制信息层需要重计算。
- 绘制信息运算层 - 绘制幻灯片时，所需要的各类信息数据，由基础数据层和当前的操作状态推演而来。当数据改变时， 需要重新计算绘制信息。
- 绘制逻辑层 - 通过绘制信息数据，实现适当的坐标转换，调用适当的底层绘制接口的逻辑层，绘制媒介会由参数传入（即canvasContext）

##### core/changes

编辑器数据层改动记录（changes）的数据定义， 包括元素位置顺序改动，增删元素，文本内容变化，属性改动等类型

##### core/changes/EditChangesStack

用于记录管理编辑器内各种操作所产生的数据层变化的信息(changes)，主要起到以下两个作用：记录需要重新计算的绘制信息，以及生产编辑器协作数据信息

#### common

定义了项目中需要用到的常量，基础工具方法和基本数据类型，作为整个项目的基础依赖

#### ui

主要交互逻辑的核心代码，包括界面布局，实际的图形绘制渲染，鼠标键盘操作处理逻辑等。
其中editorUI作为入口，在项目初始化时会实例化为项目的总contorller。

#### io

主要数据序列化和读取加载功能的核心代码，主要用于初始化加载和协同编辑场景

#### collaboration

主要处理协同编辑时数据交互的逻辑，还包括撤销重做

#### fonts

字体相关的代码， 不依赖除 common 以外的其它部分，也可是为基础依赖

#### globals

整个项目中会有若干个全局共用的服务性实例，如 imageLoader，fontLoader等，会在适当时候初始化并注入好依赖

这里定义了这些对象的存储和初始化逻辑

#### EntityRegistry

用于对于编辑内部存在的所有对象进行管理的 utility，通过给对象赋予 ID 并保存对应关系，来达到可以松耦合的获取某个对象

#### EditActionFlag

这里对编辑器内各种操作都做了类型标记，操作按照action分组，action由一个个的edit操作组成，这里定义了所有涉及到的action和edit类型常量

#### InstanceType

定义了编辑器涉及到的各类实体及属性类型的定义常量，方便对各类对象的类型识别

#### copyPaste

处理复制粘贴的逻辑

## 架构图

```
|---------------|-----------------------------------------------------------------|
|               |        editor api                                               |
|               |------------------------------------------------|----------------|
| collaboration |             ui                                 |                |
|               |------------------------------------------------|    Common      |
|               | dom    |      eventHandler      |mainController|                |
| --------------|------------------------------------------------|                |
|               | features(anim, edit, demo, crop, exports...)   |  fonts utils   |
|               |------------------------------------------------|  draw utils    |
|               |                   ooxml Core                   |  str utils     |
|       io      |------------------------------------------------|  matrix utils  |
|(serialization)| Model  |Recalcuation/RenderInfo |   Render     |                |
| --------------|--------|------------------------|--------------|----------------|
```
