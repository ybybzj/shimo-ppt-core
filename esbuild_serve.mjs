#!/usr/bin/env node
import * as esbuild from 'esbuild'
import { PluginInlineWorker } from './builder_plugins/esbuild-inline-worker-plugin.js'
const ctx = await esbuild.context({
  entryPoints: ['exp/index.ts', 'exp/exp.ts'],
  tsconfig: 'tsconfig.json',
  bundle: true,
  outdir: 'exp/dist',
  sourcemap: 'inline',
  target: 'es6',
  format: 'esm',
  plugins: [
    PluginInlineWorker({
      prefix: 'inline-worker',
      minify: false,
    }),
  ],
})

const { host, port } = await ctx.serve({
  servedir: 'exp',
})
console.log(`ESBuild Serving on http://${host}:${port}`)
