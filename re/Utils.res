open RescriptCore

@scope("Date") @val
external now: unit => int = "now"

module Compare = {
  type compareResult = Equal | Less | Greater

  type comparator<'a> = ('a, 'a) => int

  let fromInt = n =>
    switch n {
    | n if n === 0 => Equal
    | n if n < 0 => Less
    | _n => Greater
    }

  let compare = (l, r) => Pervasives.compare(l, r)->fromInt
}

module Dict = {
  type t<'a> = RescriptCore.Dict.t<'a>
  @get_index external unsafeGet: (t<'a>, string) => 'a = ""
}

module Arr = {
  let unshift = (arr, a) => {
    arr->Array.unshift(a)
    arr
  }

  let push = (arr, a) => {
    arr->Array.push(a)
    arr
  }

  let copyPush = (arr, a) => {
    let newArr = Array.copy(arr)
    newArr->push(a)
  }

  type direction = Forward | Backward

  let findMapIndexU = (arr: array<'a>, ~index, ~direction, ~map: ('a, int) => option<'b>): option<
    'b,
  > => {
    let len = Array.length(arr)
    let normalizeIndex = idx => idx > len - 1 ? len - 1 : idx < 0 ? 0 : idx
    if len <= 0 {
      None
    } else {
      let idx = normalizeIndex(index >= 0 ? index : len + index)
      let i: ref<int> = ref(idx)
      let stepIdx = i => {
        switch direction {
        | Forward => i + 1
        | Backward => i - 1
        }
      }
      let isValidIdx = i => {
        i >= 0 && i < len
      }

      let found: ref<option<'b>> = ref(None)
      while found.contents->Option.isNone && isValidIdx(i.contents) {
        let item = Array.getUnsafe(arr, i.contents)
        let result = map(item, i.contents)
        if Option.isSome(result) {
          found := result
        } else {
          i := stepIdx(i.contents)
        }
      }

      found.contents
    }
  }

  let findMapU = (arr: array<'a>, ~index, ~direction, ~map: 'a => option<'b>): option<'b> => {
    findMapIndexU(arr, ~index, ~direction, ~map=(item, _) => map(item))
  }

  let traverseUtil = (arr: array<'a>, ~index, ~direction, ~util: 'a => bool): unit => {
    let _ = findMapIndexU(arr, ~index, ~direction, ~map=(a, _) => util(a) ? Some(true) : None)
  }

  let consumeAt = (arr: array<'a>, ~at: 'a => bool, ~consumer: ('a, array<'a>) => unit) => {
    let collects: ref<array<'a>> = ref([])

    let consumeCollects = collects => {
      let arr = collects.contents
      if arr->Array.length > 0 {
        let first = arr->Array.getUnsafe(0)
        let rest = arr->Array.sliceToEnd(~start=1)
        consumer(first, rest)
      }
    }

    arr->Array.forEach(a => {
      switch at(a) {
      | true =>
        consumeCollects(collects)
        collects := [a]
      | false =>
        let _ = collects.contents->push(a)
      }
    })

    consumeCollects(collects)
  }

  let mapAt = (arr: array<'a>, ~at: 'a => bool, ~mapper: ('a, array<'a>) => 'b): array<'b> => {
    let result: array<'b> = []
    consumeAt(arr, ~at, ~consumer=(head, rest) => {
      result->Array.push(mapper(head, rest))
    })
    result
  }

  let reduceU = (a, x, f) => {
    let r = ref(x)
    for i in 0 to Array.length(a) - 1 {
      r.contents = f(r.contents, Belt.Array.getUnsafe(a, i))
    }
    r.contents
  }

  let reduceSelf = (arr: array<'a>, ~reducer: ('a, 'a) => 'a): option<'a> => {
    switch arr->Array.length {
    | 0 => None
    | _ =>
      let first = arr->Array.getUnsafe(0)
      let rest = arr->Array.sliceToEnd(~start=1)
      rest->reduceU(first, reducer)->Some
    }
  }

  let reduce = (arr: array<'a>, ~index, ~direction, ~init: 'b, ~reducer: ('b, 'a) => 'b): 'b => {
    let result: ref<'b> = ref(init)
    traverseUtil(arr, ~index, ~direction, ~util=item => {
      result := reducer(result.contents, item)
      false
    })
    result.contents
  }

  @set external truncateToLengthUnsafe: (array<'a>, int) => unit = "length"
  let appendArr = (target, ~start=?, ~end=?, arr) => {
    let t_len = target->Array.length
    let a_len = arr->Array.length
    let end_index = switch end {
    | Some(n) if n >= 0 && n < a_len => n
    | _ => a_len
    }
    let start_index = switch start {
    | Some(n) if n >= 0 && n <= end_index => n
    | _ => 0
    }

    let len = end_index - start_index
    if len > 0 {
      truncateToLengthUnsafe(target, t_len + len)
      for i in start_index to end_index - 1 {
        target[t_len + i - start_index] = arr->Array.getUnsafe(i)
      }
    }
  }
}

include Utils_intf

module type ItemM = {
  type t
  let hash: t => int
  let eq: (t, t) => bool
}

module MakeHashMap: (Item: ItemM) => (HashMap with type key = Item.t) = (Item: ItemM) => {
  module Key = Belt.Id.MakeHashableU(Item)

  type key = Key.t
  type t<'a> = Belt.HashMap.t<key, 'a, Key.identity>
  let make = (): t<'a> => Belt.HashMap.make(~hintSize=2, ~id=module(Key))
  let update = (t: t<'a>, key, item) => Belt.HashMap.set(t, key, item)
  let get = (t: t<'a>, key) => Belt.HashMap.get(t, key)
  let has = (t: t<'a>, key) => Belt.HashMap.has(t, key)
  let clear = t => Belt.HashMap.clear(t)
  let each = (t, iterater) => {
    t->Belt.HashMap.forEach((_, v) => iterater(v))
  }
}

module MakeHashSet: (Item: Hashable) => (HashSet with type item = Item.t) = (Item: Hashable) => {
  module ItemHash = Belt.Id.MakeHashableU(Item)
  type item = ItemHash.t
  type t = Belt.HashSet.t<item, ItemHash.identity>
  let make = (): t => Belt.HashSet.make(~hintSize=2, ~id=module(ItemHash))
  let size = t => t->Belt.HashSet.size
  let add = (t: t, item) => Belt.HashSet.add(t, item)
  let has = (t: t, item) => Belt.HashSet.has(t, item)

  let merge = (set1: t, set2: t) => {
    let set = Belt.HashSet.make(~hintSize=size(set1) + size(set2), ~id=module(ItemHash))
    set1->Belt.HashSet.forEach(item => add(set, item))
    set2->Belt.HashSet.forEach(item => add(set, item))
    set
  }
}

module MutableIntMap = Belt.MutableMap.Int

module Range = Belt.Range

@inline
let opt_mapWithDefault = (o, default, @inline f) =>
  switch o {
  | Some(x) => f(x)
  | None => default
  }
module Queue = {
  type t<'a> = array<'a>
  let make = () => []
  let enqueue = (t, a) => Array.push(t, a)

  let is_empty = t => t->Array.length <= 0
  let dequeue = t => Array.shift(t)
  @set external truncateToLengthUnsafe: (array<'a>, int) => unit = "length"
  let clear = t => {
    truncateToLengthUnsafe(t, 0)
  }
  let peek = t => t[0]
}

module Iter = {
  include RescriptCore.Iterator
  type iter_getter<'a> = unit => Iterator.t<'a>
  let of_string: string => iter_getter<string> = s => String.getSymbolUnsafe(s, Symbol.iterator)

  let of_array: array<'a> => iter_getter<'a> = a => Array.getSymbolUnsafe(a, Symbol.iterator)
}

module Seq = {
  type generator<'a> = unit => option<'a>
  type rec t<'a> = {
    mutable get: unit => node<'a>,
    peekQueue: Queue.t<'a>,
  }
  and node<'a> = Nil | Cons('a, t<'a>)

  let empty = () => {
    get: () => Nil,
    peekQueue: Queue.make(),
  }
  // updaters
  let cons = (t, a) => {
    t.peekQueue->Queue.enqueue(a)
  }

  let resetFromGen = (t, gen) => {
    t.peekQueue->Queue.clear
    t.get = () =>
      switch gen() {
      | None => Nil
      | Some(a) => Cons(a, t)
      }

    t
  }
  // constructors

  let fromGen = gen => {
    let rec t = {
      peekQueue: Queue.make(),
      get: () =>
        switch gen() {
        | None => Nil
        | Some(a) => Cons(a, t)
        },
    }
    t
  }

  let fromIterator = iter => {
    let rec t = {
      peekQueue: Queue.make(),
      get: () =>
        switch iter->Iter.next {
        | {value, done} if done == false =>
          switch value {
          | Some(v) => Cons(v, t)
          | None => Nil
          }
        | _ => Nil
        },
    }
    t
  }

  let fromString = s => Iter.of_string(s)()->fromIterator

  let fromArray = arr => Iter.of_array(arr)()->fromIterator

  let fromList = l => {
    let target = ref(l)
    let genItem = () =>
      switch target.contents {
      | list{h, ...l} =>
        target := l
        Some(h)
      | _ => None
      }
    fromGen(genItem)
  }

  // peekers
  let peek = ({get, peekQueue} as t) => {
    switch Queue.peek(peekQueue) {
    | Some(x) => Some((x, t))
    | None =>
      switch get() {
      | Cons(x, _) =>
        Queue.enqueue(peekQueue, x)
        Some(x, t)
      | Nil => None
      }
    }
  }

  let is_empty = ({peekQueue} as t) =>
    switch peekQueue->Queue.is_empty {
    | false => false
    | true => peek(t)->opt_mapWithDefault(true, _ => false)
    }

  // consumers
  let uncons = ({get, peekQueue} as t) =>
    switch peekQueue->Queue.dequeue {
    | Some(x) => Some((x, t))
    | None =>
      switch get() {
      | Nil => None
      | Cons(x, _) => Some((x, t))
      }
    }

  let to_list = t => {
    let rec _to_list = (t, l) => {
      switch t->uncons {
      | None => l
      | Some((a, t)) => list{a, ..._to_list(t, l)}
      }
    }
    t->_to_list(list{})
  }

  let take = (t, ~count) => {
    if count < 0 {
      (0, list{})
    } else {
      let rec _to_list = (t, l, n) => {
        if n === count {
          (n, l->List.reverse)
        } else {
          switch t->uncons {
          | Some((x, t)) => _to_list(t, list{x, ...l}, n + 1)
          | None => (n, l->List.reverse)
          }
        }
      }

      _to_list(t, list{}, 0)
    }
  }

  let peek_take = (t, ~count) =>
    switch take(t, ~count) {
    | (n, l) if n === count => Some(l)
    | (_, l) =>
      l->List.forEach(a => cons(t, a))
      None
    }

  let match_take = (t, match_t, ~eq) => {
    let rec _match = (t, match_t, l) =>
      if match_t->is_empty {
        (true, l)
      } else {
        switch (t->uncons, match_t->uncons) {
        | (Some((a, t)), Some((b, match_t))) if eq(a, b) => _match(t, match_t, list{a, ...l})
        | (Some((a, _)), _) => (false, list{a, ...l})
        | _ => (false, l)
        }
      }
    switch _match(t, match_t, list{}) {
    | (false, l) =>
      l->List.forEach(a => cons(t, a))
      None
    | (true, l) => Some(l->List.reverse)
    }
  }
}

module Equal = {
  type eq<'a> = ('a, 'a) => bool
  module type Equalable = {
    type t
    let eq: eq<t>
  }

  type equalable<'key> = module(Equalable with type t = 'key)

  let equalableU = (type item, ~eq): module(Equalable with type t = item) =>
    module(
      {
        type t = item

        let eq = eq
      }
    )
  module StrEqualable: Equalable with type t = string = {
    type t = string
    let eq = (s1, s2) => s1 === s2
  }
}
