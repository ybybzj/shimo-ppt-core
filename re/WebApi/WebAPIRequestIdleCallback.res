type timeRemainingFn = unit => int

type requestIdleCallbackArgs = {
  didTimeout: bool,
  timeRemaining: timeRemainingFn,
}

type requestIdleCallbackArgsForJs = {"didTimeout": bool, "timeRemaining": timeRemainingFn}

type callback = requestIdleCallbackArgs => unit

type callbackForJs = requestIdleCallbackArgsForJs => unit

@module("../shim/RequestIdleCallback.shim")
external requestIdleCallback_: callbackForJs => int = "requestIdleCallback"

@module("../shim/RequestIdleCallback.shim")
external cancelIdleCallback: int => unit = "cancelIdleCallback"

let requestIdleCallbackU = callback =>
  requestIdleCallback_(args =>
    callback({
      didTimeout: args["didTimeout"],
      timeRemaining: args["timeRemaining"],
    })
  )
let cancelIdleCallbackU = id => cancelIdleCallback(id)
