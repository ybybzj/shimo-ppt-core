/* TypeScript file generated from WebAPIDom.res by genType. */

/* eslint-disable */
/* tslint:disable */

import type {Dom_Canvas_t as $$Canvas_t} from '../shim/WebAPI.shim';

import type {Dom_Image_t as $$Image_t} from '../shim/WebAPI.shim';

import type {Dom_imageData as $$imageData} from '../shim/WebAPI.shim';

export type Image_t = $$Image_t;

export type imageData = $$imageData;

export type Canvas_t = $$Canvas_t;
