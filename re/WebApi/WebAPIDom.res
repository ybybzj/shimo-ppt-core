type document

module Image = {
  @gentype.import("../shim/WebAPI.shim")
  @gentype.as("Dom_Image_t")
  type t
  @get external width: t => float = "width"
  @get external height: t => float = "height"
}

@gentype.import("../shim/WebAPI.shim")
@gentype.as("Dom_imageData")
type imageData

module Canvas = {
  @gentype.import("../shim/WebAPI.shim")
  @gentype.as("Dom_Canvas_t")
  type t
  @set external setWidth: (t, float) => unit = "width"
  @set external setHeight: (t, float) => unit = "height"
  @get external width: t => float = "width"
  @get external height: t => float = "height"
}
