type callback = int => unit
@module("../shim/RequestAnimationFrame.shim")
external requestAnimationFrameU: callback => int = "requestAnimationFrame"

@module("../shim/RequestAnimationFrame.shim")
external cancelAnimationFrameU: int => unit = "cancelAnimationFrame"
