open RescriptCore
type t /* Main type, representing the 2d canvas rendering context object */

@send external getContext2d: (WebAPIDom.Canvas.t, @as("2d") _) => t = "getContext"

type gradient
type pattern
type measureText

/* Sub-modules (and their interfaces) for string enum arguments: */
type compositeType = [
  | #"source-over"
  | #"source-in"
  | #"source-out"
  | #"source-atop"
  | #"destination-over"
  | #"destination-in"
  | #"destination-out"
  | #"destination-atop"
  | #lighter
  | #copy
  | #xor
]

type lineCapType = [#butt | #round | #square]

type lineJoinType = [#round | #bevel | #miter]

type style = [
  | #String(string)
  | #Gradient(gradient)
  | #Pattern(pattern)
]

/**
 2d Canvas API, following https://simon.html5.org/dump/html5-canvas-cheat-sheet.html
*/
@send
external save: t => unit = "save"
@send external restore: t => unit = "restore"

/* Transformation */
@send external scale: (t, ~x: float, ~y: float) => unit = "scale"
@send external rotate: (t, float) => unit = "rotate"
@send external translate: (t, ~x: float, ~y: float) => unit = "translate"
@send
external transform: (
  t,
  ~m11: float,
  ~m12: float,
  ~m21: float,
  ~m22: float,
  ~dx: float,
  ~dy: float,
) => unit = "transform"

@send
external setTransform: (
  t,
  ~m11: float,
  ~m12: float,
  ~m21: float,
  ~m22: float,
  ~dx: float,
  ~dy: float,
) => unit = "setTransform"

@send external setLineDash: (t, ~segments: array<int>) => unit = "setLineDash"

/* Compositing */
@set external globalAlpha: (t, float) => unit = "globalAlpha"
@set external globalCompositeOperation: (t, compositeType) => unit = "globalCompositeOperation"

/* Line Styles */
@set external lineWidth: (t, float) => unit = "lineWidth"
@set external lineCap: (t, lineCapType) => unit = "lineCap"
@set external lineJoin: (t, lineJoinType) => unit = "lineJoin"
@set external miterLimit: (t, float) => unit = "miterLimit"

/* Colors, Styles, and Shadows */
@set
external setFillStyle: (
  t,
  @unwrap [#String(string) | #Gradient(gradient) | #Pattern(pattern)],
) => unit = "fillStyle"
@set
external setStrokeStyle: (
  t,
  @unwrap [#String(string) | #Gradient(gradient) | #Pattern(pattern)],
) => unit = "strokeStyle"

let reifyStyle = (x: 'a): option<style> => {
  module Internal = {
    type constructor
    @val external canvasGradient: constructor = "CanvasGradient" /* internal */
    @val external canvasPattern: constructor = "CanvasPattern" /* internal */
    let instanceOf: ('a, constructor) => bool = %raw(`function(x,y) {return +(x instanceof y)}`)
    /* internal */
  }
  let v = Obj.magic(x)

  if Type.typeof(x) == #string {
    Some(#String(v))
  } else if Internal.instanceOf(x, Internal.canvasGradient) {
    Some(#Gradient(v))
  } else if Internal.instanceOf(x, Internal.canvasPattern) {
    Some(#Pattern(v))
  } else {
    None
  }
}

@get external getFillStyle: t => 'a = "fillStyle"
@get external getStrokeStyle: t => 'a = "strokeStyle"

let fillStyle = (ctx: t) => ctx->getFillStyle->reifyStyle

let strokeStyle = (ctx: t) => ctx->getStrokeStyle->reifyStyle

@set external shadowOffsetX: (t, float) => unit = "shadowOffsetX"
@set external shadowOffsetY: (t, float) => unit = "shadowOffsetY"
@set external shadowBlur: (t, float) => unit = "shadowBlur"
@set external shadowColor: (t, string) => unit = "shadowColor"

/* Gradients */
@send
external createLinearGradient: (t, ~x0: float, ~y0: float, ~x1: float, ~y1: float) => gradient =
  "createLinearGradient"

@send
external createRadialGradient: (
  t,
  ~x0: float,
  ~y0: float,
  ~x1: float,
  ~y1: float,
  ~r0: float,
  ~r1: float,
) => gradient = "createRadialGradient"

@send external addColorStop: (t, float, string) => unit = "addColorStop"

@send
external createPattern: (
  t,
  Dom.element,
  @string
  [
    | #repeat
    | @as("repeat-x") #repeatX
    | @as("repeat-y") #repeatY
    | @as("no-repeat") #noRepeat
  ],
) => pattern = "createPattern"

/* Paths */
@send external beginPath: t => unit = "beginPath"
@send external closePath: t => unit = "closePath"
@send external fill: t => unit = "fill"
@send external stroke: t => unit = "stroke"
@send external clip: t => unit = "clip"
@send external moveTo: (t, ~x: float, ~y: float) => unit = "moveTo"
@send external lineTo: (t, ~x: float, ~y: float) => unit = "lineTo"

@send
external quadraticCurveTo: (t, ~cp1x: float, ~cp1y: float, ~x: float, ~y: float) => unit =
  "quadraticCurveTo"

@send
external bezierCurveTo: (
  t,
  ~cp1x: float,
  ~cp1y: float,
  ~cp2x: float,
  ~cp2y: float,
  ~x: float,
  ~y: float,
) => unit = "bezierCurveTo"

@send
external arcTo: (t, ~x1: float, ~y1: float, ~x2: float, ~y2: float, ~r: float) => unit = "arcTo"

@send
external arc: (
  t,
  ~x: float,
  ~y: float,
  ~r: float,
  ~startAngle: float,
  ~endAngle: float,
  ~anticw: bool,
) => unit = "arc"

@send external rect: (t, ~x: float, ~y: float, ~w: float, ~h: float) => unit = "rect"

@send external isPointInPath: (t, ~x: float, ~y: float) => bool = "isPointInPath"

/* Text */
@set external font: (t, string) => unit = "font"
@set external textAlign: (t, string) => unit = "textAlign"
@set external textBaseline: (t, string) => unit = "textBaseline"

@send external fillText: (t, string, ~x: float, ~y: float, ~maxWidth: float=?) => unit = "fillText"

@send
external strokeText: (t, string, ~x: float, ~y: float, ~maxWidth: float=?) => unit = "strokeText"

@send external measureText: (t, string) => measureText = "measureText"

@get external width: measureText => float = "width"

/* Rectangles */
@send external fillRect: (t, ~x: float, ~y: float, ~w: float, ~h: float) => unit = "fillRect"

@send external strokeRect: (t, ~x: float, ~y: float, ~w: float, ~h: float) => unit = "strokeRect"

@send external clearRect: (t, ~x: float, ~y: float, ~w: float, ~h: float) => unit = "clearRect"

@send
external createImageData: (t, ~width: float, ~height: float) => WebAPIDom.imageData =
  "createImageData"

@send
external createImageDataFromImage: (t, WebAPIDom.Image.t) => WebAPIDom.imageData = "createImageData"

@send
external getImageData: (t, ~sx: float, ~sy: float, ~sw: float, ~sh: float) => WebAPIDom.imageData =
  "getImageData"

@send
external putImageData: (
  t,
  ~imageData: WebAPIDom.imageData,
  ~dx: float,
  ~dy: float,
  ~dirtyX: float=?,
  ~dirtyY: float=?,
  ~dirtyWidth: float=?,
  ~dirtyHeight: float=?,
  unit,
) => unit = "putImageData"

@send
external drawImageToDest: (
  t,
  WebAPIDom.Image.t,
  ~dx: float,
  ~dy: float,
  ~dw: float=?,
  ~dh: float=?,
  unit,
) => unit = "drawImage"

@send
external drawImage: (
  t,
  WebAPIDom.Image.t,
  ~sx: float,
  ~sy: float,
  ~sw: float,
  ~sh: float,
  ~dx: float,
  ~dy: float,
  ~dw: float,
  ~dh: float,
) => unit = "drawImage"
