module type Hashable = {
  type t
  let hash: t => int
  let eq: (t, t) => bool
}
module type HashMap = {
  type t<'a>
  type key
  let make: unit => t<'a>
  let update: (t<'a>, key, 'a) => unit
  let get: (t<'a>, key) => option<'a>
  let has: (t<'a>, key) => bool
  let clear: t<'a> => unit
  let each: (t<'a>, 'a => unit) => unit
}

module type HashSet = {
  type t
  type item
  let make: unit => t
  let size: t => int
  let add: (t, item) => unit
  let has: (t, item) => bool
  let merge: (t, t) => t
}

module type Intf = {
  module type Hashable = Hashable
  module type HashMap = HashMap
  module type HashSet = HashSet

  module MakeHashMap: (Item: Hashable) => (HashMap with type key = Item.t)
  module MakeHashSet: (Item: Hashable) => (HashSet with type item = Item.t)
}
