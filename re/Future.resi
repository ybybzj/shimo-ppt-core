// type for a future
type t<'a>

let isPending: t<'a> => bool
let isCancelled: t<'a> => bool
let isResolved: t<'a> => bool

// `Future.fromValue(x)` create a future resolved to `x`
let fromValue: 'a => t<'a>

/**
 * `Future.make(initializer)` creates a future and resolves it
 * with the value passed to the `resolve` function, which is passed
 * as first argument to the initializer.
 *
 * The initializer should return an optional cancellation effect
 * (e.g. a function that cancels a request or a timer).
 *
 * example:
 *
 * ```reason
 * Future.make(resolve => {
 *   let timeoutId = Js.Global.setTimeout(resolve, 100)
 *   Some(() => Js.Global.clearTimeout(timeoutId))
 * })
 * ```
 */
let make: (('a => unit) => option<unit => unit>) => t<'a>

/**
 * `Future.makePure(initializer)` creates a future and resolves it
 * with the value passed to the `resolve` function, which is passed
 * as first argument to the initializer.
 *
 * As opposed to `make`, `makePure` doesn't accept an cancellation effect.
 * Only use `makePure` for side-effect free functions.
 *
 * example:
 *
 * ```reason
 * Future.makePure(resolve => {
 *   resolve(1)
 * })
 * ```
 */
let makePure: (('a => unit) => unit) => t<'a>

/**
 * Executes the callback when the future is resolved
 *
 * example:
 *
 * future->Future.wait(Js.log)
 */
let wait: (t<'a>, 'a => unit) => unit

/**
 * Cancels:
 * - the future you call it on
 * - the futures it depends on (if the future was created using `map` or `flatMap`)
 *
 * Note that it doesn't cancel futures returned by the flatMap callback
 *
 * example:
 *
 * let request = getUser()
 * let friends = request->Future.flatMap(getFriends)
 * let transformed = friends->Future.map(deserialize)
 *
 * request->Future.cancel // cancels `request`, `friends` and `transformed`
 * friends->Future.cancel // cancels `friends` and `transformed`
 * transformed->Future.cancel // cancels `transformed`
 */
let cancel: t<'a> => unit

/**
 * Returns a future with the value of the source future mapped
 */
let map: (t<'a>, ~propagateCancel: bool=?, 'a => 'b) => t<'b>

/**
 * Returns a future with the value of the source future mapped where the mapper returns a future itself
 */
let flatMap: (t<'a>, ~propagateCancel: bool=?, 'a => t<'b>) => t<'b>
