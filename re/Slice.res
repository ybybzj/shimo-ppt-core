open RescriptCore
include Slice_intf

module MakeSlice: SliceMaker = (Index: Indexable) => {
  type source = Index.t
  type t = (source, int, int)
  type item = Index.item
  @inline
  let adjustEdgeV = (n, l) => {
    switch n {
    | n if n < 0 => 0
    | n if n > l => l
    | n => n
    }
  }
  let make = (s, ~start, ~end) => {
    let s_len = Index.length(s)
    switch (start->adjustEdgeV(s_len), end->adjustEdgeV(s_len)) {
    | (start, end) if end < start => (s, end, start)
    | (start, end) => (s, start, end)
    }
  }
  let makeToEnd = (s, ~start) => {
    let s_len = Index.length(s)
    switch (start->adjustEdgeV(s_len), s_len) {
    | (start, end) if end < start => (s, end, start)
    | (start, end) => (s, start, end)
    }
  }
  let length = ((_, start, end)) => {
    end - start
  }
  let at = ((s, start, end), i) =>
    if i >= end - start {
      None
    } else {
      Index.get(s, start + i)
    }

  @inline
  let makeGen = ((s, start, end)) => {
    let i = ref(0)
    () => {
      let r = switch i.contents {
      | i if i < end - start => Index.get(s, start + i)
      | _ => None
      }
      i := i.contents + 1
      r
    }
  }

  let _seq = Utils.Seq.empty()

  let to_seq = t => t->makeGen->(Utils.Seq.resetFromGen(_seq, _))
  let slice = ((s, t_start, t_end), ~start: int, ~end: int) => {
    let t_len = t_end - t_start
    let (new_start, new_end) = switch (start->adjustEdgeV(t_len), end->adjustEdgeV(t_len)) {
    | (start, end) if end < start => (end, start)
    | (start, end) => (start, end)
    }
    (s, t_start + new_start, t_start + new_end)
  }

  let sliceToEnd = ((s, t_start, t_end), ~start: int) => {
    let t_len = t_end - t_start
    let (new_start, new_end) = switch (start->adjustEdgeV(t_len), t_len) {
    | (start, end) if end < start => (end, start)
    | (start, end) => (start, end)
    }
    (s, t_start + new_start, t_start + new_end)
  }
  let to_source = ((s, start, end)) => Index.sub(s, ~start, ~end)

  let take = ((s, start, _), ~count) => Index.sub(s, ~start, ~end=start + count)
}

let makeSlice: makeSlice<'item, 'source> = (type item source, ~length, ~get, ~sub) => {
  let indexable = module(
    {
      type t = source
      type item = item
      let length = length
      let get = get
      let sub = sub
    }: Indexable with type item = item and type t = source
  )
  module(MakeSlice(unpack(indexable)): Slice with type source = source and type item = item)
}

module StringSlice = unpack(
  makeSlice(
    ~length=s => String.length(s),
    ~get=(s, i) => String.get(s, i),
    ~sub=(s, ~start, ~end) => String.substring(s, ~start, ~end),
  )
)
