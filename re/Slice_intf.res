module type Indexable = {
  type t
  type item
  let length: t => int
  let get: (t, int) => option<item>
  let sub: (t, ~start: int, ~end: int) => t
}

module type Slice = {
  type t
  type source
  type item
  let make: (source, ~start: int, ~end: int) => t
  let makeToEnd: (source, ~start: int) => t
  let at: (t, int) => option<item>
  let length: t => int
  let to_seq: t => Utils.Seq.t<item>
  let slice: (t, ~start: int, ~end: int) => t
  let sliceToEnd: (t, ~start: int) => t
  // sub copy the slice part of source
  let to_source: t => source
  let take: (t, ~count: int) => source
}

module type SliceMaker = (Index: Indexable) =>
(Slice with type item = Index.item and type source = Index.t)

type makeSlice<'item, 'source> = (
  ~length: 'source => int,
  ~get: ('source, int) => option<'item>,
  ~sub: ('source, ~start: int, ~end: int) => 'source,
) => module(Slice with type source = 'source and type item = 'item)

module type Intf = {
  module type Slice = Slice
  module StringSlice: Slice with type source = string and type item = string
  let makeSlice: makeSlice<'item, 'source>
}
