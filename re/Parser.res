open RescriptCore
open Utils
%%private(
  @inline
  let opt_flatMap = (opt, f) =>
    switch opt {
    | Some(x) => f(x)
    | None => None
    }

  @inline
  let opt_map = (o, @inline mapper) =>
    switch o {
    | Some(x) => mapper(x)->Some
    | None => None
    }
)

type rec t<_, 'item> =
  | Fail(string): t<'a, 'item>
  | Return('a): t<'a, 'item>
  | Empty: t<unit, 'item>
  | Item('item): t<'item, 'item>
  | OneOf(list<'item>): t<'item, 'item>
  | Sequence(list<'item>): t<list<'item>, 'item>
  | Take(int): t<list<'item>, 'item>
  | Map('a => 'b, t<'a, 'item>): t<'b, 'item>
  | Product(t<'a, 'item>, t<'b, 'item>): t<('a, 'b), 'item>
  | Either(t<'a, 'item>, t<'a, 'item>): t<'a, 'item>
  | Fix(t<'a, 'item> => t<'a, 'item>): t<'a, 'item>
  | Raw(eval<'a, 'item>): t<'a, 'item>
and eval<'a, 'item> = Utils.Seq.t<'item> => option<('a, Utils.Seq.t<'item>)>

module C = {
  let fail = msg => Fail(msg)
  let return = a => Return(a)
  let empty = Empty
  let item = c => Item(c)
  let one_of = l => OneOf(l)
  let sequence = s => Sequence(s)
  let take = n => Take(n)
  let map = (t, f) => Map(f, t)
  let product = (a, b) => Product(a, b)

  let either = (a, b) => Either(a, b)
  let fix = f => Fix(f)
}
module U = {
  open C
  let choose = l =>
    switch l {
    | list{} => fail("")
    | list{first, ...rest} => rest->List.reduce(first, either)->either(fail(""))
    }

  let many = p =>
    (
      more => {
        product(p, more)->map(((p, more)) => list{p, ...more})->either(return(list{}))
      }
    )->fix

  let many_one = p => product(p, p->many)->map(((p, more)) => list{p, ...more})

  let left = (ta, tr) => product(ta, tr)->map(((a, _r)) => a)
  let right = (tl, ta) => product(tl, ta)->map(((_l, a)) => a)
  let between = (ta, tb, tc) => tc->right(ta, _)->left(tb)
  let chainU = (t, op) =>
    (
      acc => {
        let ep = product(t, op)->product(acc)->map((((l, op), acc)) => op(l, acc))
        either(t, ep)
      }
    )->fix

  let product3 = (a, b, c) => product(product(a, b), c)->map((((a, b), c)) => (a, b, c))

  let product4 = (a, b, c, d) =>
    product(product3(a, b, c), d)->map((((a, b, c), d)) => (a, b, c, d))
}
%%private(let eq = (a, b) => a === b)
%%private(let eqU = (a, b) => a === b)
let rec eval:
  type a item. t<a, item> => Seq.t<item> => option<(a, Seq.t<item>)> =
  t =>
    switch t {
    | Fail(_) => _seq => None
    | Empty =>
      seq =>
        switch seq->Seq.peek {
        | None => ((), seq)->Some
        | Some(_) => None
        }
    | Return(x) => seq => (x, seq)->Some
    | Item(item) =>
      seq =>
        switch seq->Seq.uncons {
        | Some((x, seq)) if x === item => (item, seq)->Some
        | Some((x, _)) =>
          Seq.cons(seq, x)
          None
        | None => None
        }
    | OneOf(l) =>
      seq =>
        switch seq->Seq.uncons {
        | Some((x, seq)) if List.has(l, x, eq) => (x, seq)->Some
        | Some((x, _)) =>
          Seq.cons(seq, x)
          None
        | None => None
        }
    | Sequence(l) =>
      seq =>
        switch Seq.match_take(seq, l->Seq.fromList, ~eq=eqU) {
        | Some(l) => Some((l, seq))
        | None => None
        }
    | Take(n) =>
      seq =>
        switch Seq.peek_take(seq, ~count=n) {
        | Some(l) => Some((l, seq))
        | None => None
        }
    | Map(f, p) =>
      let eq = eval(p)
      seq => eq(seq)->opt_map(((a, seq)) => (f(a), seq))

    | Product(p1, p2) =>
      let ep1 = eval(p1)
      seq =>
        ep1(seq)->opt_flatMap(((x1, seq)) =>
          seq->eval(p2)(_)->opt_map(((x2, seq)) => ((x1, x2), seq))
        )
    | Either(a, b) =>
      let ea = eval(a)
      seq =>
        switch ea(seq) {
        | Some(s) => Some(s)
        | None => seq->(eval(b)(_))
        }
    | Fix(f) =>
      let rec p = lazy (Raw(r)->f(_)->eval)
      and r = seq => Lazy.force(p)(seq)
      r
    | Raw(r) => r
    }
