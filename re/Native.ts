export function removeKeyOfDict<D extends Record<string, any>>(
  o: D,
  k: keyof D
): D {
  delete o[k]
  return o
}

export function strHash32(str: string, seed: number): number {
  let hash = seed | 0
  for (let i = 0, l = str.length; i < l; i++) {
    hash = (hash << 5) - hash + str.charCodeAt(i)
    hash |= 0 // Convert to 32bit integer
  }

  return hash
}
