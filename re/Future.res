module MutableQueue = Belt.MutableQueue
type callback<'a> = 'a => unit
type pendingPayload<'a> = {
  mutable resolveCallbacks: option<MutableQueue.t<callback<'a>>>,
  mutable cancelCallbacks: option<MutableQueue.t<callback<unit>>>,
  mutable cancel: option<unit => unit>,
}

type status<'a> = [#Pending(pendingPayload<'a>) | #Cancelled | #Resolved('a)]

type t<'a> = {mutable status: status<'a>}

let isPending = future => {
  switch future.status {
  | #Pending(_) => true
  | #Cancelled | #Resolved(_) => false
  }
}

let isCancelled = future => {
  switch future.status {
  | #Cancelled => true
  | #Pending(_) | #Resolved(_) => false
  }
}

let isResolved = future => {
  switch future.status {
  | #Resolved(_) => true
  | #Pending(_) | #Cancelled => false
  }
}

let fromValue = value => {
  status: #Resolved(value),
}

let rec run = (callbacks, value) => {
  switch callbacks->MutableQueue.pop {
  | Some(callback) =>
    callback(value)
    run(callbacks, value)
  | None => ()
  }
}

let make = init => {
  let pendingPayload = {
    resolveCallbacks: None,
    cancelCallbacks: None,
    cancel: None,
  }
  let future = {
    status: #Pending(pendingPayload),
  }
  let resolver = value => {
    switch future.status {
    | #Pending(pendingPayload) =>
      future.status = #Resolved(value)
      switch pendingPayload.resolveCallbacks {
      | Some(resolveCallbacks) => run(resolveCallbacks, value)
      | _ => ()
      }
    | #Resolved(_) | #Cancelled => ()
    }
  }
  pendingPayload.cancel = init(resolver)
  future
}

let makePure = init => {
  make(resolve => {
    init(resolve)
    None
  })
}

let makeQueue = func => {
  let q = MutableQueue.make()
  q->MutableQueue.add(func)
  q
}

let wait = (future, func) => {
  switch future.status {
  | #Cancelled => ()
  | #Pending(pendingPayload) =>
    switch pendingPayload.resolveCallbacks {
    | Some(resolveCallbacks) => resolveCallbacks->MutableQueue.add(func)
    | None => pendingPayload.resolveCallbacks = Some(makeQueue(func))
    }
  | #Resolved(value) => func(value)
  }
}

let cancel = future => {
  switch future.status {
  | #Pending(pendingPayload) =>
    future.status = #Cancelled
    switch pendingPayload.cancel {
    | Some(cancel) => cancel()
    | None => ()
    }
    switch pendingPayload.cancelCallbacks {
    | Some(cancelCallbacks) => run(cancelCallbacks, ())
    | None => ()
    }
  | #Cancelled | #Resolved(_) => ()
  }
}

let onCancel = (future, func) => {
  switch future.status {
  | #Cancelled => func()
  | #Pending(pendingPayload) =>
    switch pendingPayload.cancelCallbacks {
    | Some(cancelCallbacks) => cancelCallbacks->MutableQueue.add(func)
    | None => pendingPayload.cancelCallbacks = Some(makeQueue(func))
    }
  | #Resolved(_) => ()
  }
}

let map = (source, ~propagateCancel=false, f) => {
  let future = make(resolve => {
    source->wait(value => {
      resolve(f(value))
    })
    if propagateCancel {
      Some(
        () => {
          source->cancel
        },
      )
    } else {
      None
    }
  })

  source->onCancel(_ => {
    future->cancel
  })
  future
}

let flatMap = (source, ~propagateCancel=false, f) => {
  let pendingPayload = {
    resolveCallbacks: None,
    cancelCallbacks: None,
    cancel: None,
  }
  let future = {
    status: #Pending(pendingPayload),
  }
  source->wait(value => {
    let source' = f(value)
    source'->wait(value => {
      future.status = #Resolved(value)
      switch pendingPayload.resolveCallbacks {
      | Some(resolveCallbacks) => run(resolveCallbacks, value)
      | _ => ()
      }
    })
    source'->onCancel(_ => future->cancel)
  })
  if propagateCancel {
    pendingPayload.cancel = Some(
      () => {
        source->cancel
      },
    )
  }
  source->onCancel(_ => future->cancel)
  future
}
