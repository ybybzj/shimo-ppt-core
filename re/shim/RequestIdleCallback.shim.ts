export interface RequestIdleCallbackArgs {
  didTimeout: boolean
  timeRemaining: () => number
}

type requestIdleCallback = (
  handler: (args: RequestIdleCallbackArgs) => void
) => number
type cancelIdleCallback = (id: number) => void

const orgRequestIdleCallback =
  typeof window !== 'undefined' &&
  typeof (window as any).requestIdleCallback === 'function'
    ? (window as any).requestIdleCallback
    : null

// eslint-disable-next-line no-redeclare
export const requestIdleCallback: requestIdleCallback = orgRequestIdleCallback
  ? (handler: (args: RequestIdleCallbackArgs) => void) => {
      return orgRequestIdleCallback((args: RequestIdleCallbackArgs) => {
        handler({
          didTimeout: args.didTimeout,
          timeRemaining: args.timeRemaining.bind(args),
        })
      })
    }
  : (handler: (args: RequestIdleCallbackArgs) => void): number => {
      const startTime = Date.now()

      return setTimeout(() => {
        handler({
          didTimeout: false,
          timeRemaining: () => {
            return Math.max(0, 50.0 - (Date.now() - startTime))
          },
        })
      }, 1) as any
    }

const orgCancelIdleCallback =
  typeof window !== 'undefined' &&
  typeof (window as any).cancelIdleCallback === 'function'
    ? (window as any).cancelIdleCallback
    : null
// eslint-disable-next-line no-redeclare
export const cancelIdleCallback: cancelIdleCallback = orgCancelIdleCallback
  ? (id: number): void => {
      return orgCancelIdleCallback(id)
    }
  : (id: number): void => {
      clearTimeout(id)
    }
