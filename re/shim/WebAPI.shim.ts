export type Dom_Canvas_t = HTMLCanvasElement
export type Dom_Image_t = HTMLImageElement
export type Dom_imageData = ImageData
