// tslint:disable-next-line:class-name
export interface Dict_t<T>{
  [k: string]: T
} 

export type Array_t<T> = T[]