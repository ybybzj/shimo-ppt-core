type requestAnimationFrame = (handler: (args: number) => void) => number
type cancelAnimationFrame = (id: number) => void

let _requestAnimationFrame =
  typeof window !== 'undefined' &&
  typeof (window as any).requestAnimationFrame === 'function'
    ? (window as any).requestAnimationFrame
    : null
let _cancelAnimationFrame =
  typeof window !== 'undefined' &&
  typeof (window as any).cancelAnimationFrame === 'function'
    ? (window as any).cancelAnimationFrame
    : null

let lastTime = 0
if (!_requestAnimationFrame) {
  _requestAnimationFrame = (callback: (args: number) => void) => {
    const currTime = Date.now()
    const timeToCall = Math.max(0, 16 - (currTime - lastTime))
    const id = window.setTimeout(
      () => callback(currTime + timeToCall),
      timeToCall
    )

    lastTime = currTime + timeToCall
    return id
  }
}

if (!_cancelAnimationFrame) {
  _cancelAnimationFrame = (id: number) => clearTimeout(id)
}

export const requestAnimationFrame: requestAnimationFrame = _requestAnimationFrame
export const cancelAnimationFrame: cancelAnimationFrame = _cancelAnimationFrame
