open RescriptCore

type schar = string
module StrSlice = Slice.StringSlice
type slice = StrSlice.t
type item = StrSlice.item

type rec t<_> =
  | Fail(string): t<'a>
  | Return('a): t<'a>
  | Empty: t<unit>
  | Item(schar): t<schar>
  | OneOf(string): t<schar>
  | Sequence(string): t<string>
  | Take(int): t<string>
  | TakeUtil(string): t<(string, schar)>
  | TakeWhen(string): t<string>
  | Map('a => 'b, t<'a>): t<'b>
  | Product(t<'a>, t<'b>): t<('a, 'b)>
  | Either(t<'a>, t<'a>): t<'a>
  | Fix(t<'a> => t<'a>): t<'a>
  | Raw(eval<'a>): t<'a>
and eval<'a> = slice => option<('a, slice)>

module C = {
  let fail = msg => Fail(msg)
  let return = a => Return(a)
  let empty = Empty
  let item = c => Item(c)
  let one_of = l => OneOf(l)
  let sequence = s => Sequence(s)
  let take = n => Take(n)
  let takeUtil = s => TakeUtil(s)
  let takeWhen = s => TakeWhen(s)
  let map = (t, f) => Map(f, t)
  let product = (a, b) => Product(a, b)

  let either = (a, b) => Either(a, b)
  let fix = f => Fix(f)
}

module U = {
  open C
  let choose = l =>
    switch l {
    | list{} => fail("")
    | list{first, ...rest} => rest->List.reduce(first, either)->either(fail(""))
    }

  let many = p =>
    (
      more => {
        product(p, more)->map(((p, more)) => list{p, ...more})->either(return(list{}))
      }
    )->fix

  let many_one = p => product(p, p->many)->map(((p, more)) => list{p, ...more})

  let left = (ta, tr) => product(ta, tr)->map(((a, _r)) => a)
  let right = (tl, ta) => product(tl, ta)->map(((_l, a)) => a)
  let between = (ta, tb, tc) => tc->right(ta, _)->left(tb)
  let chainU = (t, op) =>
    (
      acc => {
        let ep = product(t, op)->product(acc)->map((((l, op), acc)) => op(l, acc))
        either(t, ep)
      }
    )->fix

  let product3 = (a, b, c) => product(product(a, b), c)->map((((a, b), c)) => (a, b, c))

  let product4 = (a, b, c, d) =>
    product(product3(a, b, c), d)->map((((a, b, c), d)) => (a, b, c, d))
}

@inline
let opt_map = (o, @inline f) =>
  switch o {
  | Some(x) => f(x)->Some
  | None => None
  }
let rec eval:
  type a. (t<a>, slice) => option<(a, slice)> =
  (t, s) =>
    switch t {
    | Fail(_) => None
    | Empty =>
      switch s->(StrSlice.length(_)) {
      | n if n <= 0 => ((), s)->Some
      | _ => None
      }
    | Return(x) => (x, s)->Some
    | Item(item) =>
      switch s->(StrSlice.at(_, 0)) {
      | Some(x) if x === item => (item, s->(StrSlice.sliceToEnd(_, ~start=1)))->Some
      | _ => None
      }
    | OneOf(l) =>
      switch s->(StrSlice.at(_, 0)) {
      | Some(x) if l->String.includes(x) => (x, s->(StrSlice.sliceToEnd(_, ~start=1)))->Some
      | _ => None
      }
    | Sequence(l) =>
      let s_len = String.length(l)
      switch s->(StrSlice.take(_, ~count=s_len)) {
      | ss if ss == l => Some((l, s->(StrSlice.sliceToEnd(_, ~start=s_len))))
      | _ => None
      }
    | Take(n) =>
      switch StrSlice.take(s, ~count=n) {
      | ss if String.length(ss) == n => Some((ss, s->(StrSlice.sliceToEnd(_, ~start=n))))
      | _ => None
      }
    | TakeUtil(l) => {
        let rec r = (s, arr, n) => {
          switch StrSlice.at(s, n) {
          | Some(x) if String.includes(l, x) =>
            n === 0
              ? None
              : ((Array.joinWith(arr, ""), x), s->(StrSlice.sliceToEnd(_, ~start=n + 1)))->Some
          | Some(x) =>
            Array.push(arr, x)
            r(s, arr, n + 1)
          | None => None
          }
        }

        r(s, [], 0)
      }
    | TakeWhen(l) => {
        let s_len = StrSlice.length(s)
        let rec r = (s, arr, n) => {
          if n >= s_len {
            (arr, s_len)->Some
          } else {
            switch StrSlice.at(s, n) {
            | Some(x) if !String.includes(l, x) => n === 0 ? None : (arr, n)->Some
            | Some(x) =>
              Array.push(arr, x)
              r(s, arr, n + 1)
            | None => None
            }
          }
        }

        r(s, [], 0)->opt_map(((arr, n)) => (
          Array.joinWith(arr, ""),
          s->(StrSlice.sliceToEnd(_, ~start=n)),
        ))
      }
    | Map(f, p) =>
      switch eval(p, s) {
      | Some(x, ss) => Some(f(x), ss)
      | None => None
      }
    | Product(p1, p2) =>
      switch eval(p1, s) {
      | Some(x1, s1) =>
        switch eval(p2, s1) {
        | Some(x2, s2) => Some((x1, x2), s2)
        | None => None
        }
      | None => None
      }
    | Either(a, b) =>
      switch eval(a, s) {
      | Some(s) => Some(s)
      | None => eval(b, s)
      }
    | Fix(f) =>
      let rec p = s => eval(f(Raw(r)), s)
      and r = s => p(s)
      r(s)
    // let rec p = lazy (Raw(r)->f(. _)->eval)
    // and r = s => Lazy.force(p)(s)
    // r(s)
    | Raw(r) => r(s)
    }
