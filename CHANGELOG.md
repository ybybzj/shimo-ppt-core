## [1.74.3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.74.0...v1.74.3) (2024-09-04)


### Bug Fixes

* applying transparent correctly when applying shape formatting for images ([000199f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/000199ffe2991201bf678d2aadd479ff31b967f3))
* correctly apply line style when applying sp format ([19ef9fe](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/19ef9fe34341c484376923af039174284b69b757))
* correctly apply rtl para props when pasting format ([5a579f7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5a579f7d170942f15c4120b0b41d77e7b812bcab))
* correctly apply textPr & paraPr when applying text format ([9ae26b3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9ae26b3e2700b749827f880269457aa9e0ec09ca))
* correctly applying formatting for shapes in group ([669427e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/669427e415809a750126419ed90399717a5a5c4f))
* don't copy tail props of ln for shapes during sp format applying ([d9c1e48](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d9c1e48bc1a9bc50714b58ded78e4dde3e9d6483))
* fix abnorms of rendering connection line shape vertically or horizontally ([b1b6c22](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b1b6c225b05550b66479d1f60bf910dd22bd16d8))
* fix abtaining line width of image shape when applying sp format ([3335be2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3335be2c2173b864bd35b7d42a28dad4e69c531e))
* fix bodyPr calculation ([5e4cfa3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5e4cfa3b9b13641b23ca644b2b31654ed9ba9b66))
* fix cell border style after merging cells ([7f88a56](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7f88a56c27649f524c8df426aacc5da7b38a3414))
* fix format painting when table is selected ([8126e84](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8126e84f9506d2f32af18bfffd7545641f2201c1))
* fix load table cells data in io ([381d1a0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/381d1a0cf3cad170dd5956483ebb3f2fa8e65547))
* fix paint format behaviour for text content ([3b506e7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3b506e769091476e41c3d122ca39ba10ac7b7c4f))
* fix painting paraPr for text content ([dc6c6db](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dc6c6dba6c6145b26201a4761f979038b779ccf5))
* only copy the textPr of the first run in the first paragraph for text format painting ([51acc8f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/51acc8f5b86ed2982884407ce31b66361693a749))
* prevent clearing para pr of the target text content improperly when painting text format ([7188946](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7188946a4dceef8e3c4fc2dffd6508d49a2a213b))
* refine copy format logic for text content ([c82b04e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c82b04ea211af71aa611d618795d5e278d74dd5e))
* unset props correctly when painting sp format ([e901627](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e90162773a627996501c2572f035858c5e62d17b))


### Features

* improve rendering speed when there are many large images ([85f672a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/85f672abbdde0cea56058afdadd9f9ef52f9edca))


### Reverts

* Revert "Fix: change format brush state as the condition of using shortcut" ([5bff4c6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5bff4c67c341df1e22e33a221e5239e224dede9a))



# [1.74.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.73.0...v1.74.0) (2024-08-08)


### Bug Fixes

* add "setRefContentForSpInsertion" api for update reference content before insert shapes ([c1783e6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c1783e666290ee15217d3c2422507f047a73c095))
* calculate shape correctly before insertion ([8a06fb6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8a06fb60fc28de8843ffbace770abd8eb446d48d))
* correct the logic of update text size when change slide element size ([ab486c1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ab486c1f5acf30b8c9f14dcb51c1a662af881259))
* correctly render consective chinese punctuations ([2c12656](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2c12656c1505133200e3093955d244643e3fb0d7))
* ensure recalc after font is loaded ([6f9de2e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6f9de2eed54bad38103b24ef2ad3c63cca1863d2))
* fix apply changes error when restore from history after layout modification ([789b015](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/789b015effe0d99a094c5a762d9770a4573029bf))
* fix calc segment width ([f992afd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f992afd926aebc59db9ffb80ce7005fc4efe1df3))
* fix font detection ([1627201](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/16272012ae0bcfaf0695c3579c88bc43d82a8e53))
* fix font scaling during change slide size ([7e2a94e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7e2a94e4669d47c6e61662fb08ba83653c5d10bf))
* fix manggle problem for insert shape ([e4ef13e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e4ef13eb131e16581f7590cef40e348c5f727d31))
* fix mangle error & runtime error when ppt is empty ([fb6a4b6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fb6a4b6adbbaeba2ee1acc82279dc7bf8b311b51))
* fix mangle problem for copypasting shapes with animation ([fe3689b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fe3689b93fee9d68822ae28a5f5a7061c26f5c44))
* fix measure for para text ([0a0a2a3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0a0a2a309e866364745a2a667bf3df2a3bcbed04))
* fix rendering abnormalities for connection shapes when cnx sites is on the same level ([3765d5e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3765d5e06c86a484c034d4986f97dee87ce8e846))
* fix trigger calc render after insert slides when presentation is empty ([045a201](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/045a20149ade156dc4a94eabe243cb464a05e2d1))
* fix typeset for kerning latin letters ([fa943d8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fa943d8ec5d14529b7a6aaa2f6d3ec95e862f663))
* fix typeset for segment break on punctuation ([ef214e0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ef214e012162d18a0e594a742c468e0abf71b800))
* put placeholder beneath unmatched shapes when applying layout ([fe6b638](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fe6b638ea0ef8cc554756a516706e45b8c106861))
* support shape formatter painter for group shape ([88cd3b1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/88cd3b1516660ddbbd4c1e21cee575f39b978958))


### Features

* add useCustomFnForImageLoading editor setting ([66c471b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/66c471bc8a0f7e80972a4d557a439c1b21e33931))
* refine serialization utils for shapes containing images ([7795ade](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7795adea5fa46f728d56f369c4442a5ce25a2758))
* support keeping shapes' original formatting when being inserted ([f32e798](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f32e798c5dbc5210112998dd3d886f7dae4d6fde))
* support load image using custom fetching method ([e927ca6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e927ca67f946b77dbe8e137c7dd66a7d5983a92c))
* update formater painter cursor ([7c164c1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7c164c1b898b35ca8a58c462987f69898d3a9ed5))



# [1.73.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.72.0...v1.73.0) (2024-06-21)


### Bug Fixes

*  add allow-scripts to sandbox ([aa428ce](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aa428ced415b12fb56dbcf610f52e140ea49f2a9))
*  fix Add slideComment operations data ([a96411e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a96411e0f57c5ed564e783cae26b4402d47c933e))
* add charset to copy content ([72ec443](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/72ec4430ed8158890f710875758e13b1b1e5a2d7))
* fix comment selection behavior ([7c5f40c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7c5f40c5055910e7c2fb091c11a092e7aa04aaae))
* fix connection lost after copying by ctrl-d ([59721e1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/59721e119404154598db31059284c7fb29fe39d7))
* fix connection lost after copying slide ([46ec75b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/46ec75b8a078cb28bbad48ee1c710b386bfb0b99))
* fix connection lost after deleting unrelated sps ([1242bf5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1242bf589aa0613d8f02c0acc87903e5f6e4cfaa))
* fix drag to other tab when no text in datatransfer ([e6cf975](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e6cf97532ee0cd30987d2a835ae29b88415ef325))
* fix drag to other tab when no text in datatransfer ([1c9c69d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1c9c69deced58ec0c5ad20813153c17351648496))
* fix keep connection state after copy/paste & collaboration ([33c5257](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/33c52571af2f4a5a3efe0868414bf8cd4c2b4e15))
* fix keep connection state after moving & collaboration ([9d9d249](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9d9d2493652ffc7db54e2d2724d6526dd16855fd))
* fix paste image to ppt with theme ([6517b8e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6517b8efa27c6cd5814afc27dc767cba7609d972))
* fix posistion lost after press option/alt key & refine drag text behaviour ([0bbb9fb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0bbb9fbc135371b5981c32c095db00100599b377))
* fix potential error of failing to execute "createPattern" ([71958a7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/71958a70a1bef020e2d093af15304e405e71ccaa))
* fix potential read property of undefined error ([e353a6e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e353a6e51b8a794d04743af0317ba163a30c56db))
* fix style applying for table cell left/right borders ([c5b40d5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c5b40d50e2f67f6179882cdc558d5295b882406e))
* fix the connection relationship of sub shapes in a copied group shape by dragging while holding ctrl ([2e56d77](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2e56d77fb5e3689e72bf6b13fd5f48fc0db8794b))
* fix the result of "getSelectedSpText" when table is selected ([72bff95](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/72bff95a33010763129051a5e943daffa02ef49a))
* fix the style applying of cell top/bottom borders ([d5a849d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d5a849d4f3ec49256b2ec68346d962c2786810a6))
* fix track error ([d0a4de9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d0a4de9e797193774d336275458e2279c53ab7d2))
* fix type error ([c5c5020](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c5c5020325d6eb6d97d58166a6c8e24646b5525d))
* generate correct change data when deleting slide immediately after editing it ([c9aa32e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c9aa32e218ce8331a762d0c7af609f8f48f77f07))
* refine caculation of length for delta generation of removing sp ([d190bd3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d190bd3941acf1c55c3b63d022e9c4869ef7f55d))
* refine caculation of length for delta generation of removing sp ([a514eae](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a514eaebabb1979e3be3825882cfed30cac5b452))
* update length when remove shape ([60b9b27](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/60b9b2717e2c64bd3946370d09d20b38d4343041))
* update remove comment logic ([2492a6e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2492a6e8dd9474acaf908c511f9715523a852f15))


### Features

* 增加isEmpty的接口 ([9c28411](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9c28411346dc978102467293accd693f5ab1f7dd))



# [1.72.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.71.0...v1.72.0) (2024-03-07)


### Bug Fixes

* autofit shape size correctly when text content is changed ([55ffc5c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/55ffc5cc9acfdd5ee92f0dd89f507fe142e8b87f))
* fix add text rect with proper shape size ([73bbfcd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/73bbfcdcbb6089459815fb10efd7d72f9cb08d75))
* fix apply cell background with empty fill when the whole table is selected ([435e9d1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/435e9d1808f739c9ebb26af0f199d92645b1d114))
* fix apply props to table when the whole table is selected ([65a3e83](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/65a3e832f9cff1f95620c8f2c92b0208a4ee76a9))
* fix applying props to table properly according to the selection ([383ce8e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/383ce8e5192f51ac36ea2c8cb8a58cbf8b973ef6))
* fix code mangle problem for comment ([8434e96](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8434e96d8f3767ff05c4988da78f61242e5b6be5))
* fix condition check in getGradInfoByFillInfo ([efaf6e1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/efaf6e11df4f8c58b9f6b92cad833f975b6cfe56))
* fix export types ([7d771e9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7d771e9bb8fd46e4e04ed69202a8ab53a9f922dc))
* fix set cellBorder props logic ([e55e72c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e55e72c378e87fd7b93b199b67ef4854e4bac90c))
* fix the offsets of pasted shapes that copied from a group ([27bec00](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/27bec004a5744984616d8cfa90b1c3cd68b4c871))
* fix zoom range to 20% ~ 400% ([6d03473](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6d03473e8cb2c34a2ff31f1a0508240f92d86a38))
* img & audio & video cant drag in client ([fb36044](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fb36044e4c6700726b3fe32c77c4bae177e82414))
* refine apply table props in touch mode & reset table selection ([5c13d4b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5c13d4b7c2095632277d2c2b4ca116cfc132efeb))
* refine timing data checking ([f080db1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f080db120d13060cbb1b81ff2c37c06b2d342078))
* relax move threshold value for touch action ([3d8fc55](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3d8fc55bf4d27adec8f7a3ae1a8d1208e8c171d6))



# [1.71.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.70.16...v1.71.0) (2024-01-29)


### Bug Fixes

* dedup table styles by guid ([15e38b3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/15e38b3f07c89b7fada2f7e5082900f844bd3525))
* donot autofit shape with empty textbody & refine patten fill rendering ([35f3950](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/35f39508b865321145790c34aeeb48aee6006c88))
* fix calc rect using geometry ([567dce6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/567dce631d95f3dc5b1e9d514c104670ca371857))
* fix distribute alignment for paragraph ([f648562](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f648562f08da9fd5b127d664764c08b34d58a9b0))
* fix draw shape stroke when shape is rotated ([285d47c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/285d47ce2816b508fb6b73917ed83ce8ccab28e7))
* 修复hittext逻辑 ([c10d8c5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c10d8c53994386cb7d2f366df55cacec58328e66))



## [1.70.16](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.70.13...v1.70.16) (2024-01-15)


### Bug Fixes

* add size info search params to chart object image url ([8d250db](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8d250db53e60ab4538738eb5ec05fb15963826d1))
* always take action when setViewMode with a different value ([321666e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/321666e5f094018ae2e955c2cb010bdc19914775))
* check asset urls length before invoke pasteCallbackFunc ([5501ad7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5501ad7113fe54b248892030cc8920390818751b))
* code mangle error for comment ([661341f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/661341fee6273aad0603290fd3429a6219838ae0))
* correctly copypaste audio/video/attachment on offline pcclient ([98757e6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/98757e6ec7b896475c1329c5b03da01b9ec4a59f))
* dedup audio nodes when applying settings ([de17d7a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/de17d7ade96fabce24be3b2707bb9c07204e9bc7))
* donot preselect empty placeholder when press functional keys(ctrl,alt,meta...) ([24d7695](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/24d7695a987a92325d33d7079672212b4d4c97fc))
* filtering valid custom assets url during pasting ([5ac729a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5ac729aa4b1f00adc39788a432c994bedca726fd))
* fix autoplay audio in show mode ([52dc275](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/52dc275d641dc6cee7673c5cab216b184a9857fd))
* fix check play audio when goto perv slide & stop audio properly according to the settings in show mode ([5731eab](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5731eabe521bb6ab4e62236246fbe2c55ff34a97))
* fix circula dependencies ([4593bad](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4593badb608e481280b7515b5c9a34aac10ac177))
* fix comment related problem ([dcf6d5a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dcf6d5a0617390ecef585f9fb025e30da444a1bf))
* fix contextmenu toggle conflict problem in touch mode ([75bec29](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/75bec29aadf56b03d425d08911b20dfa4d188c76))
* fix dist align for paragraph when textWarp is none ([549297b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/549297b7c7e6bb1ba8a84b0cec16a121c05bfc45))
* fix isChartImage for offline chart object assertion ([a2faa9d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a2faa9d45fdd8a051d98801806491bf0990f7a26))
* fix key handling after entering fullscreen show mode ([075757e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/075757ec5ea88a85bd297bb6622b8fbc9deb22c0))
* fix read audio setting from json ([6a3e9b5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6a3e9b52ae9d5031b0bf770fb7f4f8ef85e5c3c7))
* fix scroll bar is hard to hit in tablet ([1bd4009](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1bd400984b7f51a38871ef96ca7c8e53ab52334c))
* fix search replace focusing abnorm ([13fcf6a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/13fcf6a3b6967095080e1d4e4fbe267c18b53b28))
* fix toggle thumbnail list by dragging ([5b82d24](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5b82d24598cafc32b338e386f4c7ca2994a4db7c))
* generate valid changeset for reszing chart object image ([6c49ace](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6c49ace1cf423572d5be768fa7870711a6219acf))
* handle limiting copy paste logic correctly ([1e00ba9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1e00ba9ed6ae2543c7dd54d026d5b1f5155566c8))
* only collect base64 images during clone shapes ([8d829fb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8d829fb236481815b7ecd0c69ac3470544e27716))
* only collect custom assets & render empty thumbnail list correctly ([e01a4b3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e01a4b33ece9fe389724727e0781b9026588e394))
* refine scrollbar dragging operation in the thumbnail view ([17d9e39](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/17d9e3939e01ea5a725e5791aeea6691720c9a6f))
* refine setViewMode for OH ([9132124](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/91321241daa2435edec6ffe0714a30e994311851))
* refine setViewMode when async load slides ([7b09875](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7b098756068c2a1981c3ba6edd356e86ee9a44c8))
* refine toggle show thumbnails & notes ([7035fb4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7035fb4db582c3bd87d2f3dac50b1e702eb24e8a))
* retain the transparency when copying table cell's background for formatting ([dcae9dc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dcae9dcbe3aafcebb887a76422e4bf255fb83773))
* set cNvPr.id initial value to 1 ([2d5cfd6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2d5cfd630f59ddaab308543c020fa5e987a9f7cf))
* show contextmenu when "press" audio icon in touch mode ([8591aa1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8591aa1d21b79874af4960de727c6c10bd889cd1))
* support add attachment with filename info ([c704fb4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c704fb41fa5af953751828555217efdafbf7605d))
* support base64 upload before paste ([b6c5850](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b6c5850462917a39ed47b8293230315174817222))


### Features

* add ChartObject element & io support ([36653d8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/36653d87d5a0ca5d9cff2dd9e577bfdd81e0ecc2))
* add handleDoubleTapForPPTShow api ([cff5fee](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cff5feec561dfbeaba45e866302bbaee1c6574b7))
* add isInPPTShowMode api ([58010bc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/58010bc4b4c4b6ab1f3ba46d8fa37d5b82cf6f30))
* add limit paste settings ([e42f2a3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e42f2a380b26c9668b9ed2326ffe430b08e534a6))
* add prepare chart object image loading api & logic ([46fa060](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/46fa06094c7ddf6049319d1a14becf4f354a121e))
* chart object is cloned as image shape during copying ([46e0196](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/46e0196265bb2f44b5c2bbc8d726a166493b7fa6))



## [1.70.13](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.70.12...v1.70.13) (2023-11-28)


### Bug Fixes

* correctly update thumnbnails after inserting slide ([86b7d11](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/86b7d117ee002f762f2765441b8eeb757868f6c2))
* enable paste shapes as images in special paste ([89fb68e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/89fb68eb0ff0765b988d9a1c0d551ca3e971077c))
* enable undo/redo after exiting show by clicking hyperlink ([889daee](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/889daeee20cd8a2f3e83bbd35fdcfc84e6b7fee9))
* fix local comment io data definition ([a9070b8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a9070b83833c70fe0997a4e5ce6a0c99ae750740))
* fix search and replace order ([fcc8b0e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fcc8b0e6ffabb76f2733ffc9d08d67456e49261b))
* fix set notes content for slide ([fa4d783](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fa4d783e8e9e06f8b95098338b2f9698a835a198))
* fix toggle note visibility ([485a98f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/485a98f0182ec1edc92deb61192bea87764b4b3d))
* refine drag out and link html parsing ([fa2b924](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fa2b924b16b0c17cfefd3c643fefb8a8e99ccd08))
* refine DragAndDrop feature ([94a6d29](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/94a6d296856b162755c3f0006bb7a4bc9631e38e))


### Features

* support local comment editing ([8bf95ad](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8bf95ad20c493d9b55e4c46cbe4175dd763ace9c))



## [1.70.12](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.70.11...v1.70.12) (2023-11-07)


### Bug Fixes

* correct cloneCColorForFormatting logic ([729dc61](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/729dc618b4c9525604d72fcaf4d38fa7743fe472))
* fix a mangle error & typo ([57540c0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/57540c0ef7d5d48440d11b6d5979167dcc34d471))
* ignore empty text content in non-rect shapes ([1ebabec](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1ebabecee8e7b02bdd687d8517b272cd58f377f7))


### Features

* add setIgnoreUndoRedo utility ([64f40a8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/64f40a885a62eb4a0a1e71fbb900db9f2d257769))



## [1.70.11](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.70.9...v1.70.11) (2023-10-23)


### Bug Fixes

* fix empty para render with bullet style([#5874](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/5874)) ([3fbbda3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3fbbda387c283640fb1e7c244c2df966e9f7f5dd))
* fix min render width for adding shape([#5839](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/5839)) ([ad2091a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ad2091a72fdd5b032864e6ed61b9c3887e90a10b))
* refine audio player pos info ([8b51f91](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8b51f918ddc036ae57779f425b4907ede29e8cad))
* refine set EditorSettings & refine shortcut handling ([f09469a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f09469a7ee00462887e8b0f93908e4899756e347))
* retain vertAlign value for table cell after other props are changed ([4fdac40](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4fdac409ffd2699b2c366a584909faebde3965ad))
* support only set transparency for shape ([2d394c9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2d394c986e25d0cb78d47355a4d0771693d5dc27))
* support rtl for audio player ([3a8f7ea](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3a8f7eabf3b7978e05f2ab767c5de25a42f30956))



## [1.70.9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.70.8...v1.70.9) (2023-10-09)


### Bug Fixes

* fix invert error after copypaste with slide master data ([0337c14](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0337c1417fd96d60c219df3816994860875ce84a))
* image shape donot inherit parent group's fill ([8e790dc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8e790dc7cfc5c219e087288e8fa37ab492c68320))



## [1.70.8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.69.2...v1.70.8) (2023-09-27)


### Bug Fixes

*  避免 load error img src ([29c376e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/29c376e2ea8f7640464ccbc8082d5e7dcafb6092))
* correctly render highlights for search result in notes ([787ad7d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/787ad7d6a8041cab5e13b848a36c4d5d5e565ff5))
* fix typo ([0caa696](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0caa696a4297bf8e640380814a867a849414472b))
* getElementInfoById 接口 imgInfo 扩展 Audio ([376a611](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/376a6113269ee66461a4e6788aefb1ba61c90fa9))
* isSupportFetch 由外部设置 ([4a268bc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4a268bc35dba2ceb8e39efb2a11a6f9ea2dfbc38))
* refine blink cursor color ([41e6d2b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/41e6d2b0d36ec6145fca68c4556f4a11ed546354))
* refine line break calc logic ([141e02e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/141e02e2e502edc21b43ef2194604494516ed6b9))
* render empty thumbnail first during check update paint ([9ab566d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9ab566d5ab29496de7dae0bab4bed585d1d16ab8))
* 临时修复初始化 cxnSp undefined 异常的问题 ([4259ade](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4259ade4424e891e8123aed5020ebc57c0ef0a7c))
* 修复 autofit 后 geometry 未更新的问题 ([9aa8124](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9aa81246dd6500a3ac981e92b231f8f16ea12d19))
* 修复全选复制链接或全选复制带有链接的段落导致的选中状态异常 ([49c16b7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/49c16b77efe7d301a0abeba3899844206ba2985a))
* 修复删除 txBody apply 异常的问题 ([a213585](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a213585d773ce02cc6c4dd0b65c33a677595dd49))
* 修复增减字号基准为小数时的步进问题 ([fdf508e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fdf508e11c52856a12effaaf789153e0b2c4ed84))
* 修复复制超链接后选中光标异常的问题 ([1d60913](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1d60913b9c821340237024bf14562f1ad7387fcd))
* 修复新增(自由)曲线和任意曲线过程中未绘制的问题 ([e2a1d9d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e2a1d9daf37fd7669faf027a9bb86510a41c7537))
* 修复查找缓存被异常清空导致从头开始查找的问题 ([6a6a376](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6a6a3764c7a43e34139d00d641b2a5c873183ceb))
* 修复演示相关快捷键未响应问题 ([e45b457](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e45b457fe5dfe1cac96ca5aedd21d486182421a3))
* 修复焦点管理中pc/mobile模式识别不统一的问题 ([46af868](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/46af86873936f9c80f0ce8624ec331c50f964a0c))
* 修复特定情况下复制音频失败的问题 ([2425e2f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2425e2febfd83455115f49377820af74f00d2441))
* 修复部分新增的 setting 接口中未同步修改的问题 ([49d4e78](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/49d4e781dd7f04a830a69e9acad6a99cd0cf49d7))
* 修复音频动画隐藏图标默认数据读取错误的问题 ([54a618e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/54a618e1301f09d342aca26d04fee08166ae6995))
* 删除音频时删除对应的动画节点 ([b6bb628](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b6bb62891b2fc1c05f503d2e4bdfcefc998c1936))
* 只读模式支持单击播放音频 ([3268cb1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3268cb1399caa9860e29d38faae07dc71b1cb101))
* 只读模式新增 Evt_onReadOnlyModeFocusEmpty 事件 ([2d328d5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2d328d5a4d57cac8e23b1edb6280ef59ca789615))
* 复制和拖拽兼容只含有图片的内容格式 ([7da8b47](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7da8b476cb3ac3838af936acd2afe56dc26f256f))
* 完善焦点管理 ([8c044cf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8c044cf295ddb71042763e9dfe8507e4e4771ed3))
* 扩展 toggleAudio 事件参数 ([3047646](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3047646aada3d3449590aacc4b82a962ac4d3f61))
* 调整 ins 计算逻辑 ([4e0f5a8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4e0f5a80287ccb88224e6b606e06b0469b8428e7))
* 调整演示模式下音频播放逻辑 ([4413151](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/44131512d2a40f81b77ecef15b43389e2c5ddca8))


### Features

* support blink cursor ([#755](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/755)) ([aed5e8b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aed5e8bf8b3671c76f206a7a6c713811c66bf465))
* 外接键盘焦点适配 ([1f1b0a2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1f1b0a23e70cae4e9e9272b7df4220b107a64cb0))



## [1.69.2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.68.8...v1.69.2) (2023-07-24)


### Bug Fixes

* correctly set font class info ([cfc5ca9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cfc5ca9483c2d39faf54635c791bff1d7a5ebf4f))
* disable offscreenCanvas support for safari ([f6a677c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f6a677cf43841c6579fefe3cbc399e1c73ec215e))
* fix table row height calculation ([18d6c76](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/18d6c76f6e486576a9382bee29022ba0b1d776ac))
* get rid of redundant font loading ([9d95ff6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9d95ff633f004dd15968c8710a245b42726ff91e))
* recalc text typesets properly when ppt is refreshed & correctly inform geometry changed after adjustment operation ([5e65f0b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5e65f0ba6f7b70ec995c548d18d2cfe750ae143a))
* refine para line calculation ([6f4a4e5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6f4a4e501b0b203c70b935eb194bcc71ae2c445b))
* refine text rect caculation for shape ([7f87e23](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7f87e2393a2c4b1b4a25eff273aa5029a5387c6d))
* 临时修复 loader 解析 ole 异常的问题 ([25ce28d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/25ce28d7974b98aa001604d85af96b4921d65b23))
* 优化字体加载策略 ([e118a5d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e118a5d2d444143126ca03494637862ae0f1eaf4))
* 修复 apply tableRow 时重复计算的问题 ([5c78b20](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5c78b2039159038a8ed69da8a8d9669e9d8bfc55))
* 修复 copy 文本后选中文本光标异常的问题 ([6f7d5ed](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6f7d5ed2e78a821b97d87d5bec738417f70b74db))
* 修复 group 计算大小过程中 child spPr 不存在的异常 ([8b81d5a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8b81d5aac4fab71abe72798c23fb5d684f412e06))
* 修复选中 img 取 info 时图片资源可能不存在的问题 ([b4d6248](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b4d6248c7e4c4aa0cb4c560437fe14d1dac1bcca))
* 兼容导入没有 opType 的数据 ([b99f08c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b99f08c58b0e01543a70f4a62870c96c8a25e232))
* 只读模式下空 placeholder 不允许交互 ([1404b60](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1404b60ac610dee85620222a12e21792a2e9f56d))
* 完善 table 高度计算逻辑 ([3e75b0e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3e75b0e251122b119af1c7c93d39b135948b6a99))


### Features

* implement compose function for operations with rescript ([#745](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/745)) ([0d337e8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0d337e8536c799b5c7b3b624cef7176c43abf434))
* support copy paste shapes with animation info ([#747](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/747)) ([bc0f2b3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bc0f2b3cb24a90be0d9208e056f52339de5f0b01))
* support dragout ([#749](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/749)) ([caec65c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/caec65c9d6f79042433c20a046dfdda3de30dd9b))



## [1.68.8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.67.0...v1.68.8) (2023-04-26)


### Bug Fixes

*  setPropForTable 稳定性优化一处 ([b1fb311](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b1fb311569473005c95439637f99570cee0ef44e))
* (切页/对象)动画预览采用单独的分层 ([16da2ff](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/16da2ff56ea6cf88ee95784ac40228fc48066b57))
* apply 100% transparent correctly for shape ([4b6b131](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4b6b131a7b659c801b5a75daea9d47e6cee0c1e5))
* code mangling ([35f8206](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/35f8206c55d919de5598b6de3e4852ec32194347))
* correct behaviour for pasting base64 image ([7cca273](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7cca2737c5fe03eb8a39602d2d95fcdb02eb13bb))
* correctly copy/paste textContent with paraNewLine ([f9e6556](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f9e655607ed8edd48d2840fd5a69d7b4c390eaa2))
* draw stroke properly after dragging a shape with dashed border ([501aa50](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/501aa504d161601670cc9e63efc8bbf71eb12261))
* enter text editing mode correctly by double clicking new shape ([93ebe29](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/93ebe29ba88cc5c3bbad373d88740c22ddd97696))
* fix apply textPr correctly ([210ab5d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/210ab5d401bcb6127f14e5d62f3171eaa3232100))
* fix applying textPr to textContent when nothing is selected ([dfe5da8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dfe5da8e2a1be358d4fe04d15e30a9e9cf15d7eb))
* fix applying textPr to textContent when nothing is selected ([#707](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/707)) ([28fdfc7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/28fdfc777c113a816f7c3bc5302c2859e7009a8e))
* fix applying textPr to textContent when nothing is selected ([#707](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/707)) ([#708](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/708)) ([7b7d7a6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7b7d7a6fe83b622166449e9b64f30fe33ab7ac21))
* fix border's dash style will affect underline/strikeout rendering ([22d44ba](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/22d44bae6e1a34a02f5d12cce5a0498f4f9420c2))
* fix calculation for text content with multiple columns ([243153f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/243153f9dfcc13e32d86a8359c9fed6a98571d96))
* fix code mangling problem ([1b379d9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1b379d9ba17f57dd28799de0f42a160345a8eeec))
* fix cursor abnorms when text is dist aligned ([14a7c10](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/14a7c1006a4eaf62afc278ac13205c90c4e47706))
* fix datatime formatter code mangling ([f6d0717](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f6d07170e80ae51128bff905f2da682883892bc4))
* fix guideline drawing when dragging table borders ([1860054](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1860054d12cd4f05b9aa9caa0599ab2a161bfa7c))
* fix insert slide for thumnbnail ([d8915dc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d8915dcf78bfd3b5b2f1d1ca6a2e3ea1f53348eb))
* fix parsing html for table content ([a2505d5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a2505d5d945d7402c133abbb4b22d5440c268bee))
* fix recycle limitation logic for allocator ([978125a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/978125a91dd2536baf1b7cccfca83797d2509939))
* fix reloadBitmap logic when ImageBitmap is not supported ([53f6b5a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/53f6b5a519692e4eee974c27e2153946cdda09b5))
* fix set "justify" alignment for paragraph ([3f742fb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3f742fb4784a22aedab16a2d25e5c0a6af3f4da8))
* fix textDoc typeset with multiple colums ([c1200ad](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c1200ad79273e2f8f1dd13b17300863da3db9f87))
* fix trigger render logic & numbering palceholder render logic ([9c7612c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9c7612c365f115d79874750ab7ef4e42ac52f49d))
* limit memory  allocation cache pool & avoid typeset calculation taking too much time ([4aa24be](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4aa24be2da0fd06d7627a1c9b44a2542c0dfbac2))
* limit memory  allocation cache pool & avoid typeset calculation taking too much time ([f874cbd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f874cbd16f12805dac6c26fe594c678f556212ba))
* pc模式调整 pointerUp 时检查是否查看大图 ([0a490a1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0a490a13c1cc1ce637bc1d380bc0d9cadcba7a70))
* properly copy/paste table content ([e243ca9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e243ca9579c86f236f220bfc360ac901c5b26453))
* properly render one letter para when text alignment is 'dist' ([3a97baf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3a97baf0569dcdbe1b8d843b9f201a513518a126))
* refine calc budget for paragraph typeset ([01e861a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/01e861a34ab8e110d40f0655753679feeb954dd5))
* refine columns typeset ([4526d45](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4526d458e5e63a7ddda46888c25d598c8f3fe9d3))
* refine hyperlink handling ([030f7d5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/030f7d5680a03dc5271f9d8a49ac18e8bbc092b7))
* refine scroll switch slide sensitivity ([2732f5f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2732f5fa2a23f80a582ef5c2271a9bf1bac8d323))
* refine text selection ops in cell on touch mode ([e833cf6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e833cf685c067c4e3e0793630f07972d5e19211d))
* render jp chars correctly in vert textRect ([661a469](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/661a469de3d9701c767ad77653de6bcabe0aeff8))
* ShimoUnorderedList 字符映射 ([9b2b67e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9b2b67e1c8e00f1c63c6355898b5c32e5ae16913))
* should not batch rendering paraTexts with different form ([a8f57b1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a8f57b1fabc16f3d5f60887fec6aa5cd4a78da71))
* splitterBox 事件委托 ([b2bb723](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b2bb723d5421d12666a60d00c6021ddab8774279))
* support percentage spacing value ([758f990](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/758f99098462e59f9ac400b33c05f0fac103f61b))
* tap thumbnail 未调用 gotoslide ([3364d02](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3364d02785727ce65236f62090fc6dee5a02af36))
* typo ([6523e54](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6523e540c1c3bafaced42564e1c05c0c3e645e05))
* will not scroll text cursor into the editor view while scrolling editor canvas ([9aba68d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9aba68db84f851a13652466ad595dbb3ba48bff0))
* 临时修复外部触发的复制行为被错误捕获的问题 ([5268883](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/52688833c49b27a4089310836f153212c6c5700a))
* 仅 lock 状态下才能 unlock & 始终保留 pointerType ([69358b5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/69358b51b147f91c84735823c06d42b1556a2989))
* 仅 pad 下调大 hit 判定范围 ([71845b1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/71845b112253c5b26747e6ce39029eb8dc19000a))
* 修复 ctrl+d 复制组合异常的问题 ([2b3ae86](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2b3ae86da1626b692ff0d096c47ead8cd4c0c795))
* 修复 getSelectedImageLikes 接口处理选中组合的问题 ([83b719a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/83b719a0ead1f5eb66e14e00aa7ff303dc1459c3))
* 修复 ios 下使用空的 canvas 作为资源 drawimage 异常的问题 ([26b1099](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/26b109994aefaa7d656db9f37ac6b96412c21896))
* 修复 notes 无法被选中编辑的问题 ([0c39c0c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0c39c0c72e99193db02caa86849015a81b4bc0a4))
* 修复 ole apply 异常的问题 ([40cb95f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/40cb95f7c41d19e303806e488029c40cd97f39c6))
* 修复 ole 对象边框未绘制的问题 ([8206b73](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8206b736464460f713d7ae7c8f48f90af5994598))
* 修复 pc 模式下单击单元格无法进入编辑的问题 ([e458dbc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e458dbc0098041be9e620b88275a8357a4c4df6d))
* 修复 times new roman 字体直接绘制和缓存绘制时表现不一致的问题&正确计算粗斜体 ([9f15563](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9f15563f2ab3916d1e2096c5d71efd56c1c59e46))
* 修复pc端演示模式焦点异常导致键盘不响应的问题 ([b7782de](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b7782de942e3cadf343444a5e0d4c356c8c3313f))
* 修复图片资源相关混淆问题 ([7e77d5d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7e77d5dad7ad0b593a90ea87cd3fee4e631a2601))
* 修复存在不规则图片时其它图片绘制异常的问题 ([7762e5a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7762e5ae927745cf1946575fbb04fa0c8956f823))
* 修复托管离屏渲染的元素尺寸计算错误导致resize等异常的问题 ([ebd51c8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ebd51c8a22ce050a2365752e2cf1f8da720eeadf))
* 修复插入表格后不能立即输入的问题 ([8623440](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8623440febf6b18f4c02d2d135006e8a83679b1d))
* 修复撤销后查找结果未刷新的问题 ([cc88f1e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cc88f1ea23bcf6e74276750dc8ccc88466353152))
* 修复文本内容超过形状大小时 bounds 未正确计算的问题 ([01aacdb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/01aacdb20e202ec13ad1a4f5c85e53cb4198ac73))
* 修复文本替换位置的问题 ([cfd3c0b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cfd3c0b64abfb4fe15c8832fd55a46ff96e9ff4b))
* 修复新增形状混淆问题 ([d65a715](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d65a715906dd3711683cb9fe7e112216ddba8729))
* 修复查找替换缩略图未更新的问题 ([9bb9d73](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9bb9d73ef0976715158cd824638978e7841f9810))
* 修复混淆问题 ([aa38726](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aa38726e4a8c2c5bfff902ece04e594f4bd58a1f))
* 修复演示模式禁用结束页时演示到最后一页后触发resize黑屏的问题 ([e458309](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e458309a886dfc749a8ed56032a0e6386b852fff))
* 修复粘贴 base64 格式图片会产生两个 insert 的问题 ([4388011](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/43880114da342c35c1d894eafbe835999432678a))
* 修复缩略图滚动灵敏度异常的问题 ([8ec7b7e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8ec7b7e0685e19874c26dba5a4d8cbe1d914d9da))
* 修复缩略图组件未初始化时对 slide 操作异常的问题 ([8970e6f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8970e6f1a67971ea9ed9c485842c27210846d713))
* 修复触屏模式下缩略图选中后部分情况下未更新 curSlideIndex 的问题 ([d811856](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d81185654382ac4fb3a0e556496b1746a2128a8f))
* 修复触屏模式下进入演示模式唤起虚拟键盘的问题 ([c11044a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c11044ac2bfba5de2e9288ecbc7c54f88dc87f68))
* 修复触屏模式下选中 hyperlink 菜单异常的问题 ([81afaf9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/81afaf91c91067bf9cce4f0af19177272ba4c290))
* 修复触屏模式下选中单元格复制结果为整个表格的问题 ([69a33c0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/69a33c02715815a1be2db018599f2d7b2df3c41e))
* 修复调节控制点未 calc 的问题 ([66f9196](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/66f9196a3755d9ca2f47ce6fa6a4953d63bdadcc))
* 修复输入文本后 bounds 未更新的问题 ([2f2c896](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2f2c8965f4f3dd14c3e041425e53450a17ff7330))
* 修复非触屏模式下编辑态时点击工具栏后失去焦点的问题 ([733cb4a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/733cb4a66ff7feb08621b72fa19feb9bf83c7c1e))
* 兼容未知的语言环境 ([1c92ff1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1c92ff15ac18d900bed1c8f2c62f9d89bbfd713b))
* 动画稳定性优化 ([6fd8735](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6fd87353c7ea0cea7a9f4c3a134cd74426805ddc))
* 多选态长按处于选中态的组合则进入子对象多选态, 否则展开菜单 ([5c08b0a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5c08b0a6afceaf5a1620080200a93973f4ed8878))
* 完善字体检测机制 ([4af53db](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4af53db0f579853cebed4351469451b831d6f677))
* 完善焦点管理 ([dbf53f0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dbf53f0d62dcfdb2c2c7c3f3084fe1a8e7b5c558))
* 打印预览混淆问题 ([d7f347f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d7f347f6010f4e2946191a46cf635099c8da40e8))
* 暂时关闭 filed 解析 ([5ceb63a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5ceb63ae1f5b1e443a6af8e811b9781be3d69667))
* 暂时关闭 input autoZoom ([41dbd01](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/41dbd010fd6a0c0ba5dbf47c9f01e700cc90d6ce))
* 暂时关闭截图快捷粘贴, 避免引起异常 change ([b8907d9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b8907d9555aef2b378c94598cde345134d5f8025))
* 演示模式下调整 pointerUp 时检查查看大图 ([5c1cabb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5c1cabb6a19a86c5bcf6e04ea111c00b5e68f530))
* 演示模式支持双击查看附件 ([8157e1b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8157e1bfd49305495d6190373b0aecc007bdc1ba))
* 稳定性优化 2 处 & 新增初始化获取 DOM 失败日志 ([3994b02](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3994b02c61bb28d2d10f37c7b41ae4ba36a4ae49))
* 触屏切页时关闭菜单 ([e27d3d4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e27d3d4cbe8a9717690c85b94c00d70727fbfd27))
* 触屏模式下, 多选态时, 有且仅有组合对象被选中才进入下级对象多选态, 否则取消选中组合 ([48cafe5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/48cafe5a6485b34520ae3aea6d0e5fdeb5489e38))
* 触屏模式下初次命中表格响应选中表格而非单元格 ([0802a29](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0802a29b0e928e7c8230ee8cbf169f3c139459ca))
* 触屏模式多选态长按触发菜单 ([22d4e1f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/22d4e1f41c45225d4c610e48055a0cb5d3c03c5b))
* 触屏长按空白处响应选中和菜单 ([21fdf0c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/21fdf0c59677ae1659306bd61794159468a94b70))
* 调整 pad 判断尺寸阈值 ([154fb8f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/154fb8f41ccd7646afd44406a779d03290d9ec94))


### Features

* add io support for ole object ([cb870a2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cb870a21a518b378c65df70f121a51f836c58919))
* notes touch ([#724](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/724)) ([c8bae80](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c8bae80d9c95420463a818cec8fd96c6fcec9e50))
* pad 模式下添加缩略图展开收起按钮 ([69b771b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/69b771b66ede17e6a5f0442a7fe66848d7242674))
* refine table operation in touch mode ([8c384ee](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8c384ee47f6033269be26c0ef4a30856dd22b405))
* support dist align for paragraph ([9f5ed8e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9f5ed8ea6f54979e0f911c407fa1a0c366dd612d))
* 支持图片加密上传 ([#725](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/725)) ([3039589](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/30395896c3431ef7945996125d3f943a29d345de))
* 支持复制粘贴文件 ([9974026](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9974026f85f7c2176a37f12ea08e41f8784ba6ae))
* 支持操作 ole 对象 ([#737](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/737)) ([50c4b34](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/50c4b347015cc1dbe07dbcf1a6dec0c08992677d))
* 新增自有 icomoon 字体作为无序列表渲染备用字体 ([fafa883](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fafa88396b684959d19795db8ed7e0fe7b6821ff))
* 添加  selectElementByRefId api ([e4c0698](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e4c0698ed2500dd2ff3c08c298a89dbd4f31aae7))
* 触控点样式改版 ([#714](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/714)) ([aa3b84e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aa3b84e41f7c5dcd23c2344cf66be9385fad6ff2))



# [1.67.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.66.6...v1.67.0) (2022-11-16)


### Bug Fixes

* _handleHtmlPaste callback empty this ([3440bd1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3440bd19ec9895049374ddeb4d2ac7e5b9d1f9fd))
* addText 支持设置属性 ([6dcacaf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6dcacaf1d763137caf389db9d21552f4cfef4cf1))
* addText 支持设置属性 ([4d4a39c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4d4a39c34b7a1eed81f6334d492eb1d467bdd1ef))
* autofit 之后重新计算 SnapXY ([9663de9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9663de96855fe2f5db56f328cb0c3cbd95a94ec1))
* BlipFill fromOptions ([2f6f360](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2f6f36064c8f079d27c947e686d10c4479fa076a))
* clear all  EditLayer canvas areas per render ([f2d5b17](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f2d5b178a81d7757587fcabfeeda0ebd2beb3a7e))
* correct paraTab measurement & refine reMeasure logic for arabic content ([c05ea70](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c05ea704efc204ed2689e0331318dd61593dbafb))
* correct spell ([42a270c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/42a270cf85029b8080070655a8ab916823fe46bd))
* correctly copyPaste textShape ([b924dc4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b924dc41f27cb0f81606706c48674f4ed7dc0a67))
* correctly draw text with complex fill style ([b267867](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b267867e0b118255b1de770b2f97a40ed71ead70))
* date-fns ([957054a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/957054a2ed249edf17f0b401ace868e8b12894c4))
* failed to set blip fill ([123b429](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/123b429fd5c36185e259c7f4e1cfc12a8b8f13a6))
* fix safari oddness when stroke with different styles ([75a77c8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/75a77c8f682755ac49d2ec25f55b479a98c5b179))
* fix text rendering with complex fill ([3862305](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/386230552a5740e4c9ba6c31621b0d1086b74945))
* fix wrongly set (Para/Text)Pr ([7d7798d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7d7798d12a927c16a1aac6e18627f4569039fb18))
* get child item by "at" method ([8fffdf2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8fffdf20f2feb9ca9926291a529ec4b29af9bed0))
* group render & calculate ([13cf05b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/13cf05b1ba7325110ba2fb3c3e1ece0e11fc8a9b))
* ignore "hlink" prop when handle textPr ([abff901](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/abff9016e5e1ec85feed9a9935b04d86bf7b0d13))
* master 封面图可能获取不到 ([b708a18](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b708a1897e34d21bf78e8cd9055e83475a19cb44))
* prevent infinite loop during typeset computing ([2b01237](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2b0123761b873df334e934c16d2f9bb39f87360b))
* remove css float value check ([ff875c3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ff875c311b9749ad3fce5ff3be9bacd706c9e3a4))
* remove date-fns dependency ([0ccb80e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0ccb80eb1567938a65c99b605e69f23d398596fe))
* safari user-select 兼容 ([634ab69](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/634ab699135c90c7fda4b40068a1287ebe391fe5))
* set cell background transparent correctly ([7d0c792](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7d0c79238640dee147f3a643bbd9f159fdeb570b))
* set reMeasure flag correctly ([e9206d1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e9206d1795cc7c0652a9878bf5b09b0e5b8539c2))
* type ([1465228](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1465228331491b04608123a5bfcecec9154c2e1a))
* typo ([96bf595](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/96bf595b5443f1b2fa2ffb09fdd71eb0b05d56e5))
* typo ([d631b96](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d631b96a6f8d84601b035827335428fc414747c4))
* typo ([864a921](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/864a92187aeaa33ecbc796242496d1074a738613))
* update ccolor's rgba correctly when setting highlight ([acdd990](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/acdd99037826b88f2a1c1c474fc672eace25508b))
* update color correctly for solid fill ([f00d116](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f00d11601a9e5a4b5131f2bce56194103731fab1))
* update fillEffects's transparent correctly ([000f4a5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/000f4a55150eb6124296958210e5220e93ba8761))
* update rgba for fillEffect after applying textPr ([8badc86](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8badc86b162807f9d5fe7347cfbd92067b67ab6e))
* 临时修复未知场景下新增 notes 未关联 slide 的问题 ([3ca0c15](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3ca0c154ec96746d1af75120e8fac6e4512b6ad2))
* 临时修复未知场景下新增 notes 未关联 slide 的问题 ([b215ad6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b215ad622f72cd991b6a34d3f298aef50fe77fb9))
* 修复 keyEvent 重复响应的问题 ([94abbd2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/94abbd2a704e7d9f68ff7ed9c08aa68f9e26622d))
* 修复 moveSlide action 没有被正确处理的问题 ([ec9d865](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ec9d8656ed4d6b2425f4740384213dc83b4a212d))
* 修复 textPr apply highlight 异常的问题 ([03e0f53](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/03e0f5383fd21c04b04cbcc88938cb955ad9caed))
* 修复主题色未正确计算的问题 ([29ea5bc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/29ea5bcb7d9eb40ec4379a631ce428ff574bc5b7))
* 修复分布对齐使用 bounds 而非原始坐标计算间距导致误差的问题 ([e157ba2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e157ba22bcb05c8ba349f881860a9f09568bd3d5))
* 修复初次查找之后无法继续查找下一个的异常 ([6f6d77a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6f6d77a23c54688131ac46746c5adf27c6fc9b58))
* 修复图片位置异常偏移的问题 ([6b8b587](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6b8b58799bbfc1cd14d0297b5930dc16560969c3))
* 修复粘贴已有 notes 的 slide 时 note.slide 未正确关联的问题 ([4adcc93](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4adcc93c2406b54407cf8c2cb52173ed4ab60bae))
* 控制点绘制对齐 ([d6cfa70](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d6cfa709a421a9b67f73015388df069c2f9b1a8a))
* 稳定性优化 undefined error ([b56d951](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b56d95145ee32a77e32bc898e832bc6163a2a604))
* 触屏设备 keyEvent 焦点适配 ([74029d1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/74029d1e90f6f7ca0f6f71df96c56eea28ae8006))
* 距离辅助线过短时不显示箭头；优化辅助线对比范围； ([0b0bc82](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0b0bc824519aec8736908e3ea4c8abe318126479))
* 过滤重复的辅助线；宽高一致时忽略对齐中线 ([24deff7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/24deff7d108c28ce966ae37af71fc769b3d1bb2a))
* 避免粘贴 slide 时 note.setSlide 产生不必要的 action ([cd815a8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cd815a85cce6834ddadb39689729be7dfb1e0655))


### Features

* expand group shape rendering bounds ([76dbe85](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/76dbe853b0e00fa78ca3b11edafd0f4ed7305eb3))
* 表格内边框拖动辅助线；单元格拖动位置检测； ([8c3f6e8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8c3f6e8b96499af83f081f229e61e2b0cb8e2b40))



## [1.66.6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.64.0...v1.66.6) (2022-10-13)


### Bug Fixes

* afterLoad 图片不需要再次 refreshPPT ([ecb1351](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ecb1351d7e9136c537291c745cd8a0d1232bc244))
* avoid invalid state of GlobalPointerEventInfo ([4c052c6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4c052c6417b375cafb0f761bee022aba7da8f167))
* calc render state before go to another slide ([e614ed3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e614ed3ce575768144ee3a7e9b5128af20ba1238))
* calc rendering state in demostration transition ([353c289](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/353c289817d2838446da043128eec7c530e4a706))
* correct load image after  copypaste image shape ([#682](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/682)) ([c84a413](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c84a413660fdd9d92f118ccb88ae2770cb7bab07))
* Error type 混淆 ([4f58cb9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4f58cb98f9dfe0bf3865b381443e4c33ff5047d2))
* fix copy table to notes ([#684](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/684)) ([8060b60](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8060b6029ca548b6a24c594b13876c5663e039f6))
* fix focus problem in iframe ([858d2e3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/858d2e34a2acb28c8c18b246b50f823fc5c8fee8))
* fix input cjk char in Linux ([#676](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/676)) ([a00016c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a00016c1dbdef2104fd03658226f3d0ea1f3fbac))
* fix restore selectionState for paragraph ([dead324](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dead32457b9d5a9a222f22bbcab8657e14d64061))
* fix toJSON for bodyPr.numCol ([f1650c4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f1650c4b4b0f3c215d55fff5aa59421039a0aab0))
* fix too many log ([02fe541](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/02fe5414c1fa63a0a852132e9a311eb6cff92fce))
* limit perf log ([65149fb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/65149fb2c62ff2feba7cfcc3ea1ab374c2797d3f))
* longAction 稳定性日志优化 ([aba4c8f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aba4c8f5f08891c21e732bc796a98e6ec603afb7))
* refine log ([107a0a3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/107a0a33e26ead09d49ecfa2fb3fbf0d4e9456a5))
* typo ([7174437](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/717443742259d86209b540462ffeefd35d74f244))
* typo ([71cc16d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/71cc16de2282343beecdc96468387504151e8276))
* update extX/extY for shapes in group during autofit ([c7402d4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c7402d41985d7249509998415949173069cfde69))
* 修复 align 带连接线时异常的问题 ([77c455d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/77c455dd4dad345d7f1205a12dc30e5185ba4df6))
* 修复 handleEvent 被混淆的问题 ([667a30f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/667a30f1dcffb8faca1bc29e5e8294b67cc89599))
* 修复 solidfill isEqual & table calc ([4325751](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/432575124002f76150a8a494ad79173ecb1893f1))
* 修复 solidFill isEqual 兼容问题 ([b08928c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b08928c5a5f8374071878d682d3c928161218580))
* 修复 table 协作 ui 存在异常的问题 ([841b861](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/841b86147cdb21a41a4274a0c756c1790c964fec))
* 修复 table 的 calc 逻辑 ([8155dbb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8155dbbc91c5c47a2434ea454fe57d346436980c))
* 修复 table 的 calc 逻辑 ([#687](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/687)) ([f9c9118](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f9c9118f119003b8fa4c1555ca94faafe7bfc9a5))
* 修复 updateChange 目标被协作者删除后 undo, redo 的错误 ([1ae8980](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1ae8980b73a78df918f3d237cbb9c387d0bef9eb))
* 修复0张幻灯片时存在的异常 ([eb88056](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/eb880560b706e5dfc58ae387eb45820a4d69f936))
* 修复下载长图隐藏幻灯片异常的问题 ([0760a23](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0760a235691de977791d7d45c6f5f69f46addb61))
* 修复删除全部幻灯片后缩略图未更新的问题 ([56b9ceb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/56b9ceb42a740b391d304a8a9269e154e358e1fc))
* 修复协作编辑中异常加载字体导致 refreshPPT 的问题 ([6237940](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/623794071d8528bcf1b26cd86d75a06e9193a325))
* 修复只读模式仍响应图片占位符交互的问题 ([31a7473](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/31a7473ed45e03029b05179f7bac85dc0ccec008))
* 修复备注区域 ctrl + 上下方向键异常的问题 ([d52637d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d52637d831ccccf9ae8546d93b1ff6722e258efe))
* 修复字号 10 继续递增失败的问题 ([76b9b56](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/76b9b56d26c52ce3244fcd7e1dbf9e732959d798))
* 修复导出长图大小超限提示错误的问题 ([0a4169c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0a4169c8544833b34327fe7982b0360d9186e0f1))
* 修复异步粘贴图片过程中发送 change 的问题 ([#672](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/672)) ([7f2545d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7f2545da42e47ed6af80d148ef894df1182ceb61))
* 修复打印预览 placeholder 线条粗细不一的问题 ([4840897](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/484089732d31a356fb1b522e64cd2d8bbb3b1f67))
* 修复打印预览 rtl 布局时占位符位置错误的问题 ([4705a23](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4705a2398169d1199b81e3b245191bf343e18e6c))
* 修复打印预览选中幻灯片未过滤隐藏的问题 ([6df272d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6df272d1be4246d3cf467e1010764fadff1627b3))
* 修复演示模式 calc 时机 ([6bdca64](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6bdca64ef36406c62d05006e6404bc29330e3db4))
* 修复演示模式(客户端)未通过 gotoPage calc 时不能正确渲染的问题 ([643a10c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/643a10ca88fe95705e0ecf3f1ca68626c5bcd2d7))
* 修复绘制带有 field 的 layout 时错误产生 change 的情况 ([ce6ca61](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ce6ca61820a0e03afd3d6b13d7571e0ec51a8bb1))
* 修复表格协作部分文字编辑场景仍然全量替换的问题 ([a7c5f3c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a7c5f3c2223f3f4418f46a247e13a8e0ffaddc92))
* 修复连接线关联 shape cxnLst 为空的异常数据兼容问题 ([013f6d3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/013f6d37a939b341b96db1c1326209e33e25a45a))
* 只读模式点击 placeholder 不应出触发上传操作 ([6eef06a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6eef06afb7452f3b6cdba6167fde74fea71d63cf))
* 完善 mobile 判断 & 修复 1 < dpr < 2 时渲染模糊的问题 ([bf0ed14](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bf0ed145e24c7a12bd6214e2347b6888ad6570dd))
* 打印范围异常 ([ca09888](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ca09888561355f8e07722d7ec5a23bb2dc72db06))
* 打印预览适配竖屏幻灯片 ([55e3cfd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/55e3cfd0171eb49803fa3e289e0ad45ed86dffee))
* 支持 alt 跳词 ([24d55dc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/24d55dc7b964379ce2b4cd87c79570c11fab9183))
* 文字跳转快捷键调整 ([dd13930](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dd139305a9d64a4fd18cbab45036c7ad221789a2))
* 稳定性优化 - 修复部分边界情况导致 undefined 程序错误 ([aebfb39](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aebfb3905cb75f5367e350f892db4f84a1757927))
* 稳定性优化 - 修复部分边界情况导致 undefined 程序错误 ([b524c99](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b524c995054f8f96c86eda4b8e060fae504706a6))
* 稳定性优化 - 修复部分边界情况导致 undefined 程序错误 ([1d3fe5c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1d3fe5c43841253cff5019e136a2f43ebeafb106))
* 边界情况下 upload 接口失败时 remove 预插入的 image ([#674](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/674)) ([5827846](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/58278465ac09d13534d41ff4143e663d5a58628e))


### Features

* print & export ([#661](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/661)) ([129b91a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/129b91ac5ceabe8148ade2922b03ecbd952dd8cf))



# [1.64.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.63.3...v1.64.0) (2022-07-28)


### Bug Fixes

* align text to the bottom with line gap ([#680](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/680)) ([c1f1520](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c1f1520732ed82c82249c30c9df5b4a50a454117))
* fix abnorm rendering text in notes view ([42e8cf6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/42e8cf6851ad2f8c07fd8d6d92a6760b7ad6d2ea))
* fix copypaste shape with alpha fill ([8fb0e16](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8fb0e167e9114fcca9f6b78c7f8fa7b09886a965))
* fix error in dev console ([1820638](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/182063839b1d453eb84acbf8a3685d2d92152e57))
* fix line join style for rendering text stroke ([88cfadd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/88cfaddb0c0c2150a4ce727ee90b50c00505a96f))
* fix render dash line for empty placeholder ([11aa9fd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/11aa9fdab0663cd4f129a9c7e28c086a17886095))
* fix set text align correctly in group ([25564c3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/25564c3cf27c9d9fec3d0cf5b9762d1869824209))
* refine paste handle logic (internal>html>image>text) ([#679](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/679)) ([c42f591](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c42f591a1f67c128c4773ae3e846f920d67e9934))
* setBodyPr 后 update calc state ([049b453](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/049b453b4a99d4ed6ceada59bd4e39b80d94b325))
* 修复异步粘贴图片过程中发送 change 的问题 ([#673](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/673)) ([935fc94](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/935fc948512ee0329159ea8970a830ef8a159710))
* 修复表格超过十行后全选丢失部分单元格选中状态的问题 ([7cdf540](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7cdf540218b4c010f2673d059c917c94410f4299))
* 修复设置文本 vertAlign 后未 calc 的问题 ([974ff96](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/974ff965fe731d875f7750066acfda391e6c6cae))
* 空字体不 apply ([e23243c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e23243caba1dae896cd44921c7a4e084d2c660ac))
* 粘贴内容存在图片时优先加载图片，修复部分情况下未图片未粘贴成功的问题 ([0b64b73](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0b64b73da78d80a55dab6a9e6de64816179b387e))
* 边界情况下 upload 接口失败时 remove 预插入的 image ([5a7681c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5a7681cb7ad584424036cb7e1b42564346900a38))



## [1.63.3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.63.0...v1.63.3) (2022-07-12)


### Bug Fixes

* group 位移仍然全量替换 ([9521948](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/95219488f52a8b411a878010d5144cd753e66374))
* 修复 delta attributes 为空时 apply 失败的问题 ([6df5457](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6df545722ffb8350b01c8dab88d6d6552726b833))
* 修复主题缩略图在 retina 下字体的绘制效果 ([88c4e7e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/88c4e7e059efabb747f7db55afadd20a5373064e))
* 切换主题图片加载 ([8195df2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8195df24a4d5120134928045eee2f662a9c60141))
* 插入 spTree 时同步注册到 entityRegistry ([7c2d7fc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7c2d7fcc045e5f44be27fbf20dc8293678f32101))
* 撤销对 cxn 的校准 ([8c81523](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8c8152319b354723f65ec487b1c8220c6f878f98))
* 移除非缩略图区域页面快捷键操作 ([971adda](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/971adda0a93a1a377ec90c428354f1c6acc37eb1))
* 组合内插入形状修正 cNvPr id, 修复组合内连接线复制后丢失连接的问题 ([c94032b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c94032bf2e08f87b52fcb9fbcd7bb53e5e209a87))


### Features

* move next focus element into center view ([d77c8bc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d77c8bc38ea85a6fa1a91782548b291bfa94c6d4))


### Performance Improvements

* 幻灯片列表滚动之后，目标 slide 进行优先加载 ([20b2fb5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/20b2fb5bf8137b70ed030ef8703d886d91b38f8b))



# [1.63.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.62.0...v1.63.0) (2022-06-30)


### Bug Fixes

* add family to fontString ([056fa99](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/056fa9904acc926a27024d6dcdad23d65acb5944))
* check bold italic subfamily ([fa2247d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fa2247de122c2cbdb3609c3b02830a9c72324bfb))
* check fill after set color ([775f6f6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/775f6f6c2e7a008de2123a0b2251527a57778fdd))
* check font usable when cache metrics ([801be31](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/801be31880cbd8905a29f023a6d98577878a568e))
* compatible with bold italic font ([8f5c07e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8f5c07edf62e477b5fe918bbdaf8f7c453c842e6))
* copy table ([06a31c8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/06a31c8f7631d84beb70f459aa60152c529a1451))
* fix special copypaste html content ([94eb6d0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/94eb6d0034eb15d971a480ff599f70aee32fac8a))
* font detect && bullet draw ([1596a11](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1596a117bd08a7d72edf043a27fea696f2773f07))
* hyperlink getCalcedTextPr ([d989aad](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d989aad418b1747204a9cd425dd7ae51a8f0f4f0))
* inputManager resize 时检查 dom 是否挂载完毕 ([1288a5c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1288a5cbbcb7e8d23161b551ad32bd786e04c38a))
* quote font as detecting ([687f11c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/687f11c39c6d7712066e8a4ab23e86703152abde))
* refresh on every five fonts downloaded ([9358641](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/93586411b1cdefd7244b6d294e6386c65282b25d))
* run 中 fonts 计算 ([f8c84d7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f8c84d715a32dbda3d88849b86df0f68ca4be2aa))
* safari 不用缓存 drawIamge ([63f7975](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/63f7975c6f8352c8ffc509aaccd75fd782d2c05a))
* thumbnail 计算 endIndex ([6f25226](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6f25226b54c122da7e5db4c4b65158980cdb437e))
* use both detect ([b2ab717](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b2ab717d404a8fae0d902551c67f8304ee0e1784))
* 修复 Powerpoint 导入的组合内的连接线渲染异常的问题(导入后手动 calc) ([9f40f97](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9f40f9702d77c5ae91f76d6948f0f8766cb34c96))
* 修复演示模式下组合内超链接无法跳转的问题 ([2562fe9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2562fe99a96b53a5cbf8e51538af29965ec9fa93))
* 修复组合形状时连接线丢失连接的问题 ([2498365](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/24983658547dd812fc0e9c72f40239ddc9773ccb))
* 客户端字体检测 ([94ef0da](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/94ef0daa9b4c0cde29292e583c752234d43e63ce))
* 更新 thumbnail update 标记时检查 index 合法性 ([ae81e98](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ae81e9870a6863235a24e1ffe4af7fbbe23c57e6))



# [1.62.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.61.0...v1.62.0) (2022-06-16)


### Bug Fixes

*  debounce refreshPresentation memoized ([f7d3d7f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f7d3d7f2e4c5ef5d3d8fd3375dce31ea25cbd4b9))
* copySelectedElements ([1d810be](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1d810bee2d97da59cb1c098080e29a7b5be5171d))
* fix abnormal behavior when copy paste group ([9c3df3f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9c3df3faf77fa3f78bf75821986b2e747757f79c))
* MeasureCode ([02586d7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/02586d7d4388dab81cf8455f1b38f1266f6cda96))
* recalcGroup 时 normalize & checkOffset, 修复 group 内形状 changeSize 错位的问题 ([a7718c7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a7718c79ddfb33218c7f28814fd9e919c055ab87))
* reset cell margins as  changing table size ([37a1028](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/37a1028e2a776fa59be595bd16181509619ce59e))
* table change size ([97ac755](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/97ac755e86f06ef98463160853ebcd51f7dfc24c))
* turnOffRecordChange as parse theme data ([6841cff](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6841cff2902e098a4d1668e54eb2c390e271d0bd))
* update slide master and layout properly when set size for presentation ([23dd32a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/23dd32a0f09546fb857983536d37e9e342d957ef))
* 修复复制组合内文本框初始 autofit 时回写 ext 时, 未加成 group 系数导致偏移的问题 ([fd15e38](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fd15e38c35ae574db2c3a0765752eb879b6e30bc))
* 修复缩放时 defaultRunPr 未缩放的问题 ([de57375](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/de57375566cb171a5f056a9e4cbacac6e4754f00))
* 修复部分情况下 compositeInput 未更新缩略图的问题 ([f94b1a1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f94b1a1f6d6c148c82b7100be763dbf8ebf5146a))
* 复制 slide 支持 sourceFormatting & 复制 slide, sp 均默认 sourceFormatting ([36e585f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/36e585f5afb421d81bc0d705ef93d2086cb35abf))


### Features

* ole support ([799066b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/799066bb1144b9a186afd0cf091ae6f23b3801f7))



# [1.61.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.60.0...v1.61.0) (2022-06-09)


### Bug Fixes

* del 键向后删除 ([e901fd6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e901fd6f0609255ca696c3de589705300e22058f))
* fix error when copypaste text ([fd9c5de](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fd9c5de606423f9af83135bc5ba22990a248e8a3))
* try catch execCommand ([cb61936](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cb61936b41a1c7dd3b5649550c158324d3800f56))
* try catch execCommand ([0b14793](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0b14793fd6c66fcb9f8a9abadae2fc153489a763))
* updateImages ([4158c05](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4158c057c82a943c6e3da1fe47fc77456db25571))
* 修复单个单元格设置透明度 ([7e51f60](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7e51f60b47017ab3daff60b85061ec4c1bad6a27))
* 修复幻灯片背景设置为图片平铺时接口查询为无填充的错误 ([7d94e6b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7d94e6b0d07c5de05f6097ed7ffebde93d076ae8))
* 修复表格设置透明度异常的问题 ([0cb1eff](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0cb1effcfefc2e60eb54b0529ff6ec19fb66526a))
* 根据文本内容获取字体名 ([2a7c1fd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2a7c1fdce29e6e22bb91e397c66814653b577a19))



# [1.60.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.59.0...v1.60.0) (2022-05-26)


### Bug Fixes

* add  tableBackground typeDef ([f8a3c41](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f8a3c41a44eef6463a7f12797224e2376df3d200))
* fix retina comment render ([9db6327](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9db63276f736dc0a37e9887cc8d72ee396ae85f4))
* mobile点击重载失败图片 ([ec96c93](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ec96c930b9b24cd49ab95ccbfde6578fb190ea17))
* 修复单元格无法设置背景的问题 ([f6ef57d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f6ef57dae41cc28478009d2fe3f18a117da3ba18))
* 禁止评论缩放 & 跳转评论后保持缩放中心 ([104f552](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/104f5526949c0257e0624c0c95b45ece22d824c4))


### Features

* 内容区支持 slide 修改快捷键（新建、复制、删除） ([54a381d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/54a381d140614c3accf1248257a03d82568d139c))
* 点击组合内形状拖动优化 ([6b802b8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6b802b87859f373cbfe969d9fe3e7155169631d5))



# [1.59.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.58.0...v1.59.0) (2022-05-20)


### Bug Fixes

* [PPTPRO-1490] 修复连接线 Hover 到取消组合后的形状上未出现触点的问题 ([bdde398](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bdde398f5052bc1bf50fc2db63235469498aec42))
* [PPTPRO-1505] 修复演示模式未成功进入全屏时焦点异常导致按键未响应的问题 ([5162ce1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5162ce195ae7f0e0082ec39b90eb82532cc71fc8))
* [PPTPRO-1513] 修复同一个触点连接2个及以上的连接线时连接异常的问题(OO遗留) ([de971fd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/de971fd8c7f89393bcaa9eb8b634c0338f4f153f))
* [PPTPRO-1514] 取消组合时 recalc 新增的 shapes 修复其再次组合后位置异常的问题 ([e74040b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e74040bc523d99ac47eb0638ba29c3b3cd9f8ccb))
* [PPTPRO-1515] 修复仅选中文本后才可点击超链接的错误 ([82c1b96](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/82c1b963ecb060c35a5602d509ffe0a87abd3eb4))
* comment mobile api 混淆 ([4483c71](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4483c71e2ee0c43107e8b697ba1813d4e0cab5c0))
* ctrl 拖拽复制时不处理关联 connector ([020c217](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/020c21759bc9b7a23bddb2d2b11d4c1f76ea047c))
* drawLineOfRunElement ([e9e0438](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e9e0438da459dbef7860fb0518853beab079170a))
* field 绘制异常 ([f4f283f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f4f283fd1b69917262875ca8833b4c0a05854c9d))
* fix empty slideMaster ([5b83065](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5b83065f7bd7b78f7f82947adc05ea61fcb0cc42))
* slideDrawer drawCacheImage 关闭抗锯齿 ([9f94862](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9f9486218c7a4c7bea24b80e25c25b0f9200f6c0))
* 修复 del 键无法删除的问题 ([c5ae568](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c5ae5684e7663ac9cce6f04666b11999495c183d))
* 修复取消组合时对子Shape设置删除标记导致后续 recalc 异常的问题 ([9e9be5f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9e9be5f3243bd74baa465447553a42b1e45eae78))
* 修复对多层级连接线操作异常的问题 ([1acddc7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1acddc71ee2f1dbad2257d32b8a7001fad988761))
* 修复将 connector copy 为 shape 的错误 ([c8ef66c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c8ef66c439cd517e3b4e54502567b0618d5908c6))
* 去除 addcommentByPct 不必要参数 ([ca500f6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ca500f6d02fd9d4e2705b70e3ade94ecdf66c9d1))
* 图片和视频点击检测失效 ([f5735f1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f5735f116cb585447c0c439d5a66b8ac4c542761))
* 粘贴是否优先上传图片判断 ([9f8aecb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9f8aecb663a65b4bba079ebaa2f2ea86a3401faf))
* 设置有序列表时缩进 ([b1c4960](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b1c49605b9719710feda7346da464296f88701e2))



# [1.58.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.57.0...v1.58.0) (2022-05-12)


### Bug Fixes

* [PPTPRO-1485] 修复 RTL 表格内协作 ui 位置未正确计算的问题 ([048d4b4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/048d4b4e83d2780bbffb0c05c14a7ab8a4ffedc0))
* [PPTPRO-1486] 绘制 tableCell 协作 ui 不时再从 EntityRegistry 寻找 tableCell ([f06de6b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f06de6bca827835f90ed83acfe369d3c35ebd931))
* [PPTPRO-1486] 绘制 tableCell 协作 ui 时不再从 EntityRegistry 寻找 tableCell ([7362b06](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7362b066349e4ee58a4577434b8f67bddaca3fce))
* addImages 图片没有 recalculate ([40fa753](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/40fa7531104678293dff3ecc94afb614d03dc931))
* align 不再需要 moveState ([17c9737](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/17c9737e88b26d326c763fc0ea344baebce5f35f))
* apply 忽略形状空 retain 数据 ([#645](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/645)) ([453531f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/453531fad850d14b938b4c9d4b6d916e99331023))
* computedParaPr 中字体计算 ([fc3defa](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fc3defac5424ddb9b6126f0e5edbd9c2c8cb5066))
* cursor render ([4fb6b6c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4fb6b6c4748bae0a8a731432c96d517de92f80aa))
* cursor render ([bcdf7cf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bcdf7cf2064cb959ed2c7e622e4738634b6d248d))
* dependency ([d512836](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d512836f1fe7fdcebc5b69bd749a1788295f8aee))
* enable bidi calculation for arabic text when rtl is not set ([ba275bc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ba275bc87bb32e44a634135c69cdaf1b1f97325d))
* fix align by moveControl ([c5048a5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c5048a536e92fe5bcf9a75292b2b7863450a78b4))
* fix error when copypaste text ([848b3fe](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/848b3fe3909020ee6934e006e5e8beccbd609cff))
* fix keyY input event error prevented ([5e7f0b5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5e7f0b59164f6fc27736dad40275d6f7fd8ab05e))
* getSlideComments  混淆 ([b2fa0e4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b2fa0e4de6c9da39509eb1b8c615f5fe7e58f621))
* layout 无 master 绘制错误 ([92edafa](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/92edafad038367b2f501f0a2d583cedf1015b6e7))
* move forward insteadof move right after insert content ([d9b2c93](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d9b2c937e9444a6284050e35bcee2a09a335eae0))
* remove log ([c3d6249](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c3d624987d620f9ccdefd43ef40e91fb531f1a85))
* renaming missed ([44df459](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/44df4596279107c3c5765b7bbbbccd655398723c))
* RTL 下开启 bidi ([e17bfae](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e17bfaec14e74c970a6bbafaf1e614cf0a9d398c))
* RTL下右对齐计算 ([99af1bf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/99af1bf025cd771294d473cbe9bbca01fe4f384f))
* using old font detect on safari ([6b42091](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6b420916c8981f35ee7c8247cc737f13384d87e1))
* 修复 addToRecalc 判定 ([4fb7069](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4fb7069ce0d104206b0095e40f8a35fa7db4e0a4))
* 修复移动端 getSlideComments index 为 0 时的异常 ([d8a665c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d8a665cf10ef14901ebf33e34bd144354fd4146c))
* 修复连接线拖动渲染异常 ([671a8dc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/671a8dc15e9c7df02922d68f03e91b74d62b9cd5))
* 列表符号兼容显示 ([851610b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/851610b3056e5e3975a46ef514329c90c293b234))
* 发生 invert error 时中断 ([3ef7eb4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3ef7eb4c7916d63ef944a5fd7cb71abe4a49f18d))
* 对发生 Inver error 的 delta 及时弹窗提示刷新并中断发送 ([b171867](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b1718678cdc3b33e65665fed42bbb1b7d5ab3028))
* 文本宽度大于形状宽度时对齐 ([bbee1f4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bbee1f47aa796a3191e8f4c4e910d3a1e884583e))
* 缩放时备注光标位置 ([81d8a96](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/81d8a96bed8544630515a00c795348cacd4d7660))
* 输入时光标位置信息更新 ([e0c79f5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e0c79f5b4d0eebbe8e920859e80c05fdaa760cbc))
* 阿语对加粗斜体样式进行区分 ([e46807a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e46807a6d495d231fdeab3695ba7bfba074aedd9))



# [1.57.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.56.0...v1.57.0) (2022-04-21)


### Bug Fixes

* dom id 改为固定随机字符串 ([23f1454](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/23f1454e231c8be8d283c691afa9512fb2f9a60f))
* fix error when  editing connection shapes ([ed60c1c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ed60c1caa1fa643cd64430534e2b9239051d845d))
* fix typo ([b9806d5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b9806d512c1859a9e01f27496e959072507c2086))
* hide when playing animation; horizontal draw ([f863e82](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f863e82f4ec82aa019099df4418d7554a6e513a1))
* refer table style error ([5db64c4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5db64c4cef0aff4a868477e0687c45958d51ab8c))
* refreshPPT & reset selectionState after upload image failed ([c7d33f2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c7d33f2f4078ff9a1d18c2187c368ad30c724490))
* remove toJSON log ([2c230d4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2c230d4df0ee594e48985e19669b4b4d2d29e362))
* rename dom 并添加随机字符串 ([b77243f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b77243f03ada24b120655ea5b7d4d3a307bd77e8))
* revert color gumut ([5cb8ba3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5cb8ba3021ff4e423dfb870972cf4d1b1554e7c6))
* revert cursor draw ([cec657c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cec657caaac0b11f680b936538b1f17616034cb1))
* text cursor retina support ([c9f6590](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c9f65900e4b2d9aae68178e92700aee3888e04b9))
* thumbnail 获取当前大小 ([ede060b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ede060b1df3c079cc8387f4ddc46aa7deefd3cca))
* use lineCount instead of lines.length ([32c2f40](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/32c2f4029f30e136864feae65bd89a381f71c2a5))
* yarn ([53401fc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/53401fcb985ef767bc2256941640cc05b8b9fc8a))
* 修复带连接线的对象旋转时未正常展示 tooltip 的问题 ([991f358](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/991f3584f8d2bee6cd51eaa1565012ee93bc01c6))
* 修复某些情况下编辑画布文本内容闪烁的问题 ([ffb51d3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ffb51d32e3c8db772f7169baca8f6d05d15a4535))
* 修复输入法文本框不跟随光标的问题 ([db91124](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/db91124ec2f6645bcfb1b2471d23ff261c2bc3ad))
* 点击选中幻灯片应清除 selection ([02791fd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/02791fd33a9f291f6db528c64bd4fd390de8f545))
* 粘贴图片文件优化 ([4a3ff35](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4a3ff35ac1a054ccbd564d47ed3a2f2c1b5cc732))
* 线性渐变角度限制在 [0, 360) ([7bb033c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7bb033c8042443e4d958057e23401bf8decf9d82))
* 表格和粘贴文本RTL ([e2235ce](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e2235ce7c8b0f1bd220838ae7440da8cefd76207))
* 输入时页面闪烁 ([f525677](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f525677a435db01b234423664b5b2a961da71466))
* 输入法提示框位置  retina 屏幕支持 ([d22b771](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d22b771c8fecd292b3b23b11568d9ed420058569))
* 选择性粘贴成图进行上传 ([40eb3f0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/40eb3f0307e58d34c38b0dbb9c65d448d356c681))


### Features

* get slide previews image asynchronously ([dd3b3ab](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dd3b3ab0fffd8eb4928703b65e450cf846bd1c91))
* 渐变色填充 ([a64b27f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a64b27f2443300c8fa19c60bf200a1d85eb960fc))
* 移动端评论 ([2454cc8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2454cc82e3cfddf2123fa8ab153612a32efb1fa9))
* 针对广色域设备开启 p3 色域 ([ff83b59](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ff83b59e5afa64e9c0f80516d243f03edd396fc4))


### Performance Improvements

* 粘贴图片文件利用已有图片缓存进行渲染 ([3c4d56c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3c4d56c13a75e26c7cbfda2474cc5855d8e0b7cb))



# [1.56.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.55.4...v1.56.0) (2022-04-07)


### Bug Fixes

* [PPTPRO-1426] zoom 保持中心位置 & 滑动跳页至首位页时不再跳页 ([b720516](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b72051639a839e517eda9ab81d118fcd7339dbf9))
* [PPTPRO-1443] 添加形状时不对线段设置最小大小 ([d762d00](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d762d005cc17e20e815148d44ea56fa7bf11b037))
* ⌘ + M 添加 Slide ([9ddecfd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9ddecfd13fe8f5e1ca444e17bfc1a4404adb56e5))
* bounds 未发生变化时避免重新申请 canvas 的可能性 ([8a889c5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8a889c58849d6419cea4a51b496874dc04f5b69a))
* checkAutofit 不判断是否 wrap ([2b8417c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2b8417cd41e9f8e91f66d7a14961af1ed4be9502))
* draw dash border properly ([a877b0b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a877b0bf318e548be69dbe5058278e2d9f191ed0))
* fix comment icon render on retina screen ([69d9d2c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/69d9d2c48683c7d54024b4c02cae358d410e095d))
* fix render line shape properly ([c35655a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c35655aa6a15f767cec6d029520eb8f5a35654f0))
* overlay 绘制评论应排除 dpr 影响 ([07d85e0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/07d85e01136cb82853d94c060ebae9bfc3351623))
* 优化 zoom 平滑性 & 跳转至形状/评论默认居中 ([cc178b3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cc178b338d632617f8f5f244ec4c671521e2aae5))
* 兼容 apply slide 找不到 layout 情况 ([61c397f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/61c397f7248e21f22b2fe0a5c7fabf24a120d73c))
* 只读模式下播放视频 ([6398682](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6398682a19211ed4681766d53fc992a2d16a7734))
* 只读模式选择形状 ([a9a2f00](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a9a2f007b297bc38b96e718a2c2ce5feac4980c3))
* 右键点击选中图片触发裁剪 ([0b9ed35](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0b9ed3541e83c2fe150218b12c4905bef35ff6b4))
* 右键粘贴sourceFileId处理 ([4876194](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4876194aa481d11db82df65e7dcda17503035cda))
* 启用 slideDrawer 缓存 ([0ef9386](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0ef9386e5377c478f5ceef94a0e2082799c5628f))
* 无权限复制失败事件 ([d9d0d85](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d9d0d85e9e591f11ab5f2cb42784dbdfc5d857d4))
* 视频播放按钮绘制 ([05e221c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/05e221cb869af6498a22f968270b7f68741f1674))
* 线条类形状样式不继承主题中的默认形状 ([f973138](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f973138b154e1ca3df95fc8c519473a007256a85))
* 组合形状 autofit 计算 ([d594812](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d5948125f119ceb30354db8d228dda153b3d768b))
* 绘制在 overlay 上的评论同步缩放 ([6bec5dc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6bec5dcab6ad1a9d90f387deca1fc77c22db3b11))
* 通过接口缩放同步保持位置不变 ([1ab3de5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1ab3de5c23ed045b4ed83e2c573b5be650473fc2))



## [1.55.4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.55.3...v1.55.4) (2022-03-28)


### Bug Fixes

* fix drawFragment with correct textPr ([b6d7663](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b6d76638964bfdaf9f2cde68e0589b2afb32a82b))
* fix fullback rendering logic for text fragment with different colors ([5b92bbc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5b92bbce7adc294d026bf845345f1d9cb3b21c3e))
* fix lint error ([aaaf6b6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aaaf6b6b5996337dba7f5e33ab75b4a52ec25501))
* fix mangle error ([7241936](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7241936655705a36c84b3b76feec32d39d03c4f8))
* fix obfuscation error ([fcc37bf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fcc37bf38e6f8505a3edaa2e8e62c06fa9e913f0))
* fix tsc error ([6898fc3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6898fc3f3241def0e067baf3182f2de3222c1319))
* use the correct measurement result of the harfbuzz shape infomation ([bbbfae6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bbbfae6b877b507bbe8362621eb00db4d95f28ee))
* 移动端列表符号显示 ([0803e58](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0803e58f51b766be13877b58359b6ec543740b6f))


### Features

* add harfbuzz support(100%) ([157982f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/157982f751821b25ec09dc554ab6531a59ad02a5))
* add harfbuzz support(60%) ([247ee8c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/247ee8c85c14d831601410a749caafeeaf2daea3))
* 评论资源替换&支持缩放 ([73702b6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/73702b6505873f6b743a5ff0d3f911b9cdcae3f6))



## [1.55.3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.55.1...v1.55.3) (2022-03-25)


### Bug Fixes

* 幻灯片序号绘制缩放 ([36d048f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/36d048f973495e3f3876b1e563d9afd3e03b5263))
* 粘贴表格补全缺失单元格 ([ccb3c98](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ccb3c982295c85fe42353184bc7e62b775a20006))



## [1.55.1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.55.0...v1.55.1) (2022-03-25)



# [1.55.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.54.0...v1.55.0) (2022-03-24)


### Bug Fixes

* blob 空判断 ([9c49368](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9c493687b413c7f9d07ef3eced566111006cb5ba))
* export enum ContextmenuSpecialPasteStatusType ([5bb8315](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5bb831550a6f888879d316331243056df1e9334d))
* fontsize 事件空值 ([690db9b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/690db9b3d2b9f35d61b5968c96e8057afda5101f))
* getSlideMasters 翻译所有主题名 ([c1f6caf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c1f6cafb79fa2d15ec8c4ce4463f90991d622461))
* mangle ([0f41452](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0f41452cf09478209a3efad400c364cd102bba82))
* refine arabic ligature mapping ([db29f1a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/db29f1af75c088f75eb1240f7478f62b9d694340))
* resolve querySpecialPasteOptions 返回值 ([42f2364](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/42f2364ff1885693f9047e19b3108a6c4fdbd700))
* 仅粘贴文本 ([97d19ee](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/97d19ee47e624c7b2f8d7a79697ad4865e304795))
* 修复 reaclc noWrap  文本时未正确计算 maxWidth 导致 align 等效果失效问题 ([cb8e491](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cb8e491dd5dc097975329cefc26791bb66f7e2fd))
* 修复同名的 getInvertTransform 导致的动画/连接线 transform 计算问题 ([39cb04b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/39cb04b399ca233b955d76d2ef8386c48c9dc420))
* 修复备注收起时的焦点及selection绘制问题 & 更正备注与缩略图交界处 cursor ([9249eab](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9249eab261a6541184a7cf03d58baef50dd8e5e3))
* 右键粘贴html或text只进行一个 ([e752840](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e7528408ef4ec483a7ef4576fc7e1765d2c01cf9))
* 字号增减逻辑更新 ([70dae4f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/70dae4f911aedf45c257f25204a1a67bdb822169))
* 小于8号字，字号增减逻辑更新 ([28fb689](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/28fb689abd5369e863bb2f986af35ad3b132558e))
* 快捷键仅粘贴文本不显示选择性粘贴 ([f6b7c10](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f6b7c104f1ba3469b0c23de973ed1097b51a019a))
* 改变字体大小超过范围 ([2cfe67b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2cfe67b00ddf89062e05985e76a957ce9b86eb37))
* 更新当前复制粘贴版本 ([1ae7612](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1ae7612e1f2291ea0afe5fdde2ad8f17f2932681))


### Features

* ⌘ + shift + v 仅粘贴文本 ([c21ea1e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c21ea1e3ee426b5630438c84db958849f4df8e33))



# [1.54.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.51.0...v1.54.0) (2022-03-17)


### Bug Fixes

*  dev 环境挂载 editor, EditorCoreApi ([21a2aaf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/21a2aafc39f4ae5035d2b902a075252ce5bdff63))
* [PPTPRO-1404] 优化滑动跳页策略，修复存在滚动条时无法滑动跳页的问题 ([83c3c92](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/83c3c9275aa370629e7ced02bd92276ebbbbb46e))
* [PPTPRO-1405][PPTPRO-1406] 修复预览模式拖拽缩略图备注不易拖拽及光标问题 ([0b70997](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0b70997a53bf75cb674de07f58355b37b1bed595))
* enable both fontDetector ([f55e4ad](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f55e4ad1af2343000b7c5bd8a0f96944533bd131))
* fix cursor rendering at the end of rtl text line ([b0e9da5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b0e9da5330d6959c2d011b62e64d785c4ac73d5a))
* fix dependency error ([7e68ae1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7e68ae15f913493b73677eced6a8ad01560bb3a4))
* FontFaceSet Support Check ([fd1fcd1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fd1fcd111890cd56386a383632fa2db9d269cf99))
* getMatchingLayout名称匹配 ([7a66923](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7a669234472bfbb5261f7d6bdb8181972eb1f991))
* getMatchingLayout通过lowerCase对比名称 ([766360f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/766360f2ef37822a49e20c57d1b98ae3c3f490cc))
* mouseleave 时触发一次 mousemove 事件 ([89321c1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/89321c19dcbbc9fe21931a432f146318f5c4341e))
* notes 初始化发送 Evt_onNoteShow 事件 ([4fd5151](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4fd5151d9e87ebb42c846e4fd8770b44427e4a04))
* refine cursor behavior in rtl paragraph ([64e1479](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/64e1479a8513959ed3442c139a61a92a8ab0e006))
* refine cursor rendering at paraEnd position & fix lint error ([14a7b95](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/14a7b95980e19a9a5fd99856411ce8c616db387c))
* refine cursor rendering at the end of rtl text line ([78e388b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/78e388bda14d56c06572de1065ae1f9cf795e5f7))
* refine cursor rendering between arabic and other chars ([fb4580a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fb4580ae9fb6ed46bda5a5dc4f4d784072331bb8))
* rtl 下占位符内容强制为rtl右对齐 ([2184375](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/218437525026f59afa61c72cd67dd8e9459fdc28))
* 主题预览图Retina适配 ([0cb4d72](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0cb4d7239921d037030168884c38e59cdb1c35ed))
* 仅 debug 模式下挂载 editor 对象 ([e9e9193](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e9e9193ad7151448303b561ec924d6442f5f0d44))
* 修复 Hover 光标效果被 splitter 覆盖为 default 的问题 ([f0da67c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f0da67c234689f56404b39eb32dbbd13a5ee773b))
* 修复 img checkbounds 异常 ([acbdc2b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/acbdc2bfeb77f81a92aebd2419ad826b9b5ab018))
* 修复备注收起时 Selection 绘制异常 ([784db89](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/784db895e0d6f5146ad756af7d4de6b41a77bd5e))
* 删除最后一个字符错误 ([edc1ca5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/edc1ca55e66a7faa087f76406ce5f7346a66a01d))
* 判断形状是否进行autofit ([37ca9e3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/37ca9e367f6b95cbd140cbc2a8d415bccf23ab33))
* 备注收起后支持拖拽再拉起 ([6ee8709](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6ee870904f4fd28997e7a867678d67ccef489eef))
* 多选单元粘贴前删除内容 ([4842b93](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4842b93fed4a6ae7a44b28627439b9244848a6e9))
* 未真正切页重置 scroller ([1f4c263](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1f4c26311e9130d7c77c3fa351e38955c660a250))
* 粘贴文本无selection时ClearContent ([c62186c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c62186c4a225c11266f560e91d02a7670d76ee5b))
* 缩放最小值调整到 10% ([c4b022d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c4b022d4f0afae5c405dae20e3a66fde1b438ce1))
* 防止画布内容过大时 canvas 无法正常渲染 ([a4f6e51](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a4f6e51c108d55f8deb12b9b77063fac590f810b))


### Features

* paste table to table ([a4110fc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a4110fcdcf0fab010af89d1444ab94f7cf1fd3ef))
* 制度模式下支持切换 copyable ([e41897d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e41897df39a873c646e5dc8a13b7d1dfdb076735))
* 缩略图/备注展开收起 prd 对齐 ([ea57870](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ea578702275acd73c47eb165cd3c6eec34008667))



# [1.51.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.50.0...v1.51.0) (2022-03-10)


### Bug Fixes

* [稳定性]发送 delta 前对其校验避错误数据的生成 ([168fe2e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/168fe2e0137266f8f59c258a2a1754e17d0a524e))
* fix fontSlot calculation for text[PPTPRO-1394] ([2546787](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/25467874d1bcee9c87d81c311f58d7776d030eff))
* fix typo ([95004d9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/95004d9d4f54960628873bdb3cfe0332c6dc0c8c))
* 修复 notes  y 坐标计算错误的问题 ([87da07e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/87da07e8ee317cadb5f0a21d2e345dce134afec0))
* 增加利用 postScriptName 检测字体 ([3d13da3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3d13da35e4dcb74f5945d07b9479155f09caad0b))
* 字体检测与绘制 ([7c289f3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7c289f35f3e1fa28f134abe622c08002ffaf9612))
* 粘贴内部数据前缀判断 ([a89b7f0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a89b7f0056e7e754699c9031940673264fe2911e))
* 返回值与类型不一致 ([ea9c14f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ea9c14f7cddb3fe90d1445ccfa7687dfa57f6526))


### Features

* 复制粘贴通过版本号判断是否进行内部粘贴 ([eac32dd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/eac32dda36096ada527157cad708e490da771823))



# [1.50.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.49.0...v1.50.0) (2022-03-03)


### Bug Fixes

* [PPTPRO-1357] 修复组合内形状 reclac 计算 offset 异常的问题 ([7e89eef](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7e89eefcde60434cd80d86db5edcd272e332312c))
* [稳定性] 初始化数据时兼容由 retain 组成的无效 slide 数据 ([021d0fd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/021d0fdc7e27223e05196a8fea4066196444ad93))
* autofit 计算 ([a7da801](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a7da80123ccb03059fbb1d2d3f5e725c5da4d1c1))
* refine font metrics & measurement calculation ([db4df10](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/db4df108255db154ed649f69e9ed7509ac59a206))
* refine font metrics calculation ([8c1292d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8c1292da3c63f61e6816bb697c1f13a67163ba81))
* refine ligature default support ([6b97830](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6b978308cb17d6da1322244426ed467ca42f903c))
* refine ligature settings ([eb4b5e5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/eb4b5e506836c9eecac7f70312e57ca26d67ee51))
* refine measurement calculation for paraRun ([224477b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/224477bf01b4473b593d1c95f85846998b191aee))
* rtl 下列表缩进计算 ([923e04c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/923e04cc600d2523af2ab08bdbb9dbf83d7af6e1))
* RTL下空格后无内容换行逻辑 ([de4be05](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/de4be055c37fdda2e211bca03335ce77956c902a))
* 修复 RTL 布局下 notes Mouse 坐标未修正的问题 ([62d4832](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/62d4832e1ff16e2d5f49fba7a9575bb6ceef997a))
* 修复循环依赖 ([e27af81](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e27af81b906bf17f56e6ba5dda6fd9995c4e3ed8))
* 切换文字方向从ComputedParaPr上判断JC ([ccba85b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ccba85be42feaff72e9e7211f48744b88079a78d))
* 备注光标 Retina 屏幕下错位 ([91a1458](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/91a1458aab052bf251e5d81fa9e16a12d7c14528))
* 复制粘贴备注产生新的master引用 ([63e2bfc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/63e2bfc91b52a956c9282457f2ec94a9d5ef81ef))
* 超链接位置数据更新为global ([5a0d0c3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5a0d0c3e9b70a91278808adf1525001d9163459d))
* 避免空值 ([bea6a7b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bea6a7b26561b30e8350fe598da0e36e550bb8e4))



# [1.49.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.48.0...v1.49.0) (2022-02-24)


### Bug Fixes

* [PPTPRO-1279] tip Hover 效果多语言自适应 ([06bbe46](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/06bbe46d522939b5bb485b2570414bf746ffdd86))
* [PPTPRO-1290]fix reshape logic with harakat letter involved ([e697566](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e697566d7d7ca54d9aa96552109f8a04c5254057))
* [PPTPRO-1298] 修复设置文本属性新产生 paraRun 后文本选区变大的问题 ([1133601](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1133601d0430086b6501a0f78de69e7c6aacafee))
* [PPTPRO-1303] 选择性粘贴位置计算 ([8e8a11a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8e8a11a4ec3824506550fd66362b9a93873b711f))
* [PPTPRO-1335] 修复元素修改层级会删除关联动画的问题 ([67f1fe4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/67f1fe41b720a85deba53b12a4ee96c8cd6a15a3))
* [PPTPRO-1360] 修复 rtl 绘制光标和选区时计算 xVisible 修正 ind 异常的问题 ([341e4ff](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/341e4ffc7f30158848bc0d9496db0b9153f21c7d))
* [PPTPRO-1360] 修复选区时 endPos decrease 的判定 ([ed78e9b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ed78e9bc09a6b02c90efeb028dee33ab16e96fbe))
* [PTPRO-1303] 选择性粘贴 slide 位置适配 ([5fbac35](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5fbac3518bb154ab961a0c2dee523f65f0edff3b))
* Add_Cell参数变动 ([752f334](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/752f33484a402b7fce442544396bab03679e27e5))
* cache bidi reorder info into ParaLine ([109b61e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/109b61e7699bda1ceadd36701e9bd5fffda8c0aa))
* default support frequently used arabic ligature ([205b23b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/205b23bc1b8ef125aa09c7d23da474953777c690))
* enable the full ligature support ([489cc2b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/489cc2b4fb1579650deb48c886047ff1efe54a29))
* export api ([58186e0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/58186e0f83474cfa2aa5a3f5e35c411f10de9ccc))
* firstLine计算 ([c4cc49f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c4cc49f52c75a69d3df0a4dfabf28d1aa14c81c2))
* fix  drawing for textContent decoration(highlights/strikes...) ([588cae2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/588cae257f8290ad8f21e7509960c012c5f9ef6e))
* fix advance after behaviour for demostration mode ([6eff34d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6eff34de93968c47c49eec5e778f8998c0c62647))
* fix cursor rendering for bidi text content ([cde0ee7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cde0ee7e1c00b0c98d8caa5da9e7e12a3208fc2d))
* fix ligature for arabic ([263f999](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/263f999aad8f83075014dd69b694acabf4efb8c1))
* fix ligature support for arabic text content ([bf72cef](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bf72cef1f36b3a00279205437d3b038d54a6204d))
* fix line measurement ([ce0d03a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ce0d03a098be09a88cd9b8bbc12fed4967a30dc2))
* fix line measurement & drawing ([c046977](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c0469774ed77388ad42b9b91bdbf558915d514fc))
* fix logic in method 'iterateBetween' ([2516b96](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2516b96c92fc88e98e55cbca9a0fc91591633dbd))
* fix range recalc for typesetting of para with arabic text ([c2d9309](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c2d93096d5b653ced35e746168dd1883bfba732f))
* fix recalc line range caused by reshaping ([f16938b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f16938b123f40b15459e67b0ae493853c4665fcc))
* fix rendering arabic text with hyperlink ([c13913f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c13913ff105664abe568c9ff01637ccb400e2b27))
* fix selection rendering logic for rtl text content ([8c4f81a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8c4f81a36f1fc2d12f4629b31fa96ce0bdcbe842))
* fix the logic of line range recaculation caused by reshaping ([7a94f56](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7a94f563593537e683175049cc485e99bfe6c413))
* fix the logic of updating arabic info for line ([441233f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/441233fb6df42479c02832b89e9e35802a423d2d))
* getEmbeddingLevels 参数 ([080e741](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/080e74131814173fc45da0807701dc4b1b91ba3d))
* getEmbeddingLevels 方向参数 ([4cc9cc5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4cc9cc56be3639963443ae29eb766ca1c66c64a5))
* getItemRangeFromLineAndRange ([4490c95](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4490c95313e3e8a7aea3b9aaea8c9fdd24e142ef))
* getPosByLineAndRange ([0087cfb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0087cfb67bb8c2e63c671418f712938eb5e27b09))
* isRTL 判断 ([8fe36ac](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8fe36ac107e61541f7a463cf6873aad3e84654bd))
* lint error ([1fa5462](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1fa5462faf19e6720cceed37cf51b73c879ca198))
* ln 层级计算 ([6796ac9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6796ac91c7f0440a4600f3a91eb915946d76fce9))
* refine arabic ligature ([16ec23a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/16ec23af68f8e0fa3548f9f567b7fdc694c4d8f4))
* refine cursor update edge case for bidi text content ([fe43468](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fe434682909ac1050afdf3df6b6a7f3a122dfa7d))
* refine recalculateCurPos logic for arabic text ([0a3c8e7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0a3c8e77c91c806c9a585d3f1180fb0170c75334))
* retrieve bidi info correctly ([fef8325](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fef8325f4db7435c1fec6dfd12af5d0b813083c9))
* rtl 下绘制光标位置计算 ([f355ab5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f355ab52b50c958ab0e5826cb5fb9902c504bdf8))
* rtl 缩略图拖拽 && 选择性粘贴 right 值计算 ([78b3646](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/78b3646d69f813ffadec3d7c77b4c18aa6baa6e4))
* RTL下 justify 对齐应该靠右 ([19a4885](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/19a4885aca8ff9a9db57cc60a4c9f0d577a28d81))
* RTL下演示模式左右方向键互换 ([8ac47a3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8ac47a3985b31bdbfcb4739ab09d7be9df6c9804))
* RTL演示模式切页 ([0ce31bd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0ce31bd42d77c61d9ca6e0ba8c02d2a0970b4be2))
* RTL环境下段落的RTL和JC默认属性 ([5b4f7a8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5b4f7a862cd16c428a10d666f0fc0812caf4815e))
* table rtl ([e987dce](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e987dce6d92b7c19e2cbd89cff5bfdd49d3d732d))
* 修复 hyperlink 绘制异常 ([3959230](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3959230f8467fc5be125795a1717c5e51b818ea1))
* 修复 RTL 下插入文本后 curPos 修正异常的问题 ([3bd1463](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3bd146398c3ee2b3f94d07c33222418f4beb2ae7))
* 修复 selectionPos === length 时选区绘制异常 ([b9e2861](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b9e286145ef24aa19106ca7a7ea0305a2f0240f0))
* 修复动画中单个文本长度超出形状宽度时, bounds 小于实际宽度的情况 ([062ba57](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/062ba571d19954e12b0a6125c9b9fb0c1dfde43d))
* 修复粘贴后全选drawSelection错误 & 删除文本后 cursor 更新 ([71f09b5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/71f09b564651ddf4c5df8f3181abd62eac917618))
* 修复输入文本/Click curPos 超出 length 的情况下光标的展示 ([c4e3133](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c4e31334ea4ccf4a516c2d2f6be48b10b06e1888))
* 全选之后 syncEditorUIState ([d916020](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d91602064260df603ad1d9cf35ec9378f1ac97e7))
* 切换RTL，反转对齐方向 ([970cff8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/970cff8b73445fb2fc17aa43846e9b90dbffb871))
* 复制粘贴slide备注丢失 ([54e8036](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/54e803681a23a65b0379316253c314ef2836edab))
* 文本结尾 space 算入 range 宽度 ([d74b687](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d74b6876a7dd4395837a65fe32ee8059670717ce))
* 文本选取逻辑终点去除 paraEnd 长度 ([e584244](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e5842443a82f85558150d0973b2ed8190df4d88b))
* 根据首个输入字符是否为Arabic来设置文本方向 ([3d07965](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3d07965ab32498d26ef8bd9607432e3ec5b508ae))
* 根据首个输入字符是否为Arabic来设置文本方向 ([71a8729](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/71a8729eae80477427229e6807a785b0069b890b))
* 确保字体 detect 不受 lineHeight 影响 ([105372c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/105372c0f6172f07393b8cb1d46f0dc437f187dd))
* 设置 lineHeight 写法 ([85dda9e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/85dda9e143e81af9ecdf81c18f2f7d92c9000b6d))
* 设置容器方向为ltr ([8d700ea](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8d700ea8397b11185eaadaf3ce652892cd64f885))
* 超链接无法绘制 ([b39252c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b39252c2223475d5a910614a0c9b0f963b4f4b28))
* 逆向选择时文本选区绘制 ([85a6fba](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/85a6fbacc3ff732c626a2026ed287c71e7cbf988))
* 非阿语不缓存 reorder 信息 ([40f025e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/40f025ee2af0b90b707cfe5342fe2af3e1253408))


### Features

* (arabic)support reshaping when recalculate range ([f73f8ea](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f73f8ea6d06f1f9a9e3500c3131b7067c7069ec8))
* add arabic letter reshaper util ([d3bec7c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d3bec7c045face478b662ab74455e93ca58761ba))
* add bidi render support ([7c78d7c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7c78d7cd05311441cde7f1d675ea55fa10dd949e))
* add bidi utilities ([2d32272](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2d32272a962cbbdc84a038d5722eed95b5d6da3c))
* add editor settings for supporting arabic ligature ([9c4d006](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9c4d006e9635f63c29b1b369a3050a042ba2ef2f))
* core ui rtl 支持 ([f17d3c4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f17d3c47101c3ede96d9e5bbea2601b4c3e6e9ce))
* implement search logic position by XY hit info for bidi text content ([68d8a47](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/68d8a47136efd27d32b2acca4c7391e5fca6ac13))
* rtl选中单元格 ([601df5d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/601df5d7e5081739b7d81f76ebeab0f15d17ce46))
* support highlights & underline/strike rendering in bidi environment ([72ed9c9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/72ed9c93724aab873691f506368b38ca3cdaa357))
* text cursor support bidi text content ([57ce1a4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/57ce1a4ea6d14f3c49585e034150916b6fa214ec))
* 板式名进行翻译；更新幻灯片初始数据 ([b8d39dd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b8d39dd26c4dd6688785fc4e59c47c4c615bfd00))
* 混排文本选区绘制 ([24055da](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/24055dac2047c340f474f8f98c1a38f8a47ee715))


### Reverts

* Revert "core@rtl" ([cf0c9ed](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cf0c9ed59a18c756bbf8e64f5e7758c7279c885d))



# [1.48.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.47.0...v1.48.0) (2022-01-20)


### Bug Fixes

* [PPTPRO-1243] 使用 tab 移动单元格，同步信息 ([1e5d7a3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1e5d7a3403afddcf48578d41a815c4cbcc7b64f6))
* [PPTPRO-1243][PPTPRO-1245] 修复合并单元格之后的位置计算及Tab切换至空文本单元格的 selection 更新问题 ([137ea61](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/137ea61c5d9a4d4f617a57752ae783f0bf2f4561))
* [PPTPRO-1244][PPTPRO-1246] 修复 applyChange 恢复光标位置时对失效引用校验导致的恢复失败 ([c35be01](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c35be01e325d8b0055dd0b4639240c8e42bdbf77))
* [PPTPRO-1253] 删除 Slides 不全量修改 order & applySlides remove 完善 ([0b39962](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0b39962b419610b9e3e0a882e547636d9961911a))
* [PPTPRO-1254] 修复 applyChange 后 table selection 被重置为默认状态的问题 ([3a378c8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3a378c857e5ca03da7b12c39b5c26d786f1b224c))
* [PPTPRO-1259] 只读模式下，不绘制协作者信息 ([f546bad](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f546bad88edc8eee1bdefd8eb0459a23c40f9770))
* fix  abnormal rendering when moving shape with vertical text ([2aee74e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2aee74ed7f6fe181c906dd44e28b2a56467c4d0e))
* fix column heigth adjustment logic ([43f0319](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/43f03193cc83dc3fe494e9869f664472142a54db))
* focusInput时机 ([df780d2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/df780d2dfac0a7a3474d87e5d3598880630904f2))
* lint error ([f24a187](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f24a1874178dc5f020b16bd3e26c01d4d62cdae5))
* opsFromUpdateTableCellInfo 时避免不必要的 toJSON ([ba1a07d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ba1a07d3cfc74bdd08533d750b6988abcea48fed))
* param error ([14f4fe4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/14f4fe4ca249c843b95bb0a183241d162fc11a44))
* tip 组件位置过右时向左修正 ([df9fd54](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/df9fd546b2376f6cafa1890988f6d0e194d4acb1))
* 修复 tableCell Attrs 的 ops 多出的层级 ([6b805b3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6b805b3395ddff3006307ecd642f617365d666a5))
* 修复竖排文本 resize 触发 autoFit 后 offX/Y 同步的问题 ([ad3a9b3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ad3a9b31d6c3188b639d4332325b14872333878f))
* 兼容导入defaultTab数据 ([8e84df8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8e84df8578152b6338a5b349213c2c15a3311cc8))
* 兼容导入defaultTab数据 ([684f424](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/684f424939758135c7215e47b34bf43041a56868))
* 旋转tips翻译缺失 ([929f438](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/929f438164d05097763eb485350e9fa278d23a16))
* 链接框关闭后，无法聚焦输入 ([aeab369](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aeab36916f8da6789ae42f5a13d77e4c20c650b9))


### Features

* 协作 ui 单元格 Hover 效果细化 ([45b36ef](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/45b36ef05a75c0a0f0cf78bb8cdf9597078d0208))
* 协作 ui 绘制细化到 tableCell (静态边框, Hover 效果待设计) ([5e7f34d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5e7f34dfda374c8e0410b6983dee781b496c9b79))
* 支持 SlideElement 针对业务细粒度协作更新 ([9ffc392](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9ffc39280a4d4fd4575d8adfa27b36f51ed73d6f))
* 演示状态允许接收 change 进入 pending 状态，演示结束后 apply ([ccd2960](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ccd29604562a85b59929ff9b0d5ecbbe2cf2f748))



# [1.47.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.46.0...v1.47.0) (2022-01-07)


### Bug Fixes

*  切页时在更新scrolls之前checkSlide ([35f9b58](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/35f9b58481400761e51692cc385cf3c9c007d8e7))
* correctly drawing text placeholder that being focused ([4c2f556](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4c2f556fb6cb356f321e5a83a21e6da11ea878ce))
* hueMod 支持 ([2c0acd2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2c0acd2c115d77f2458ca755baff208c00911a3c))
* recalc transform after autoFit ([bfbfecf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bfbfecff7189bd11c9114de26feb900a9026dd62))
* recalculateBrush参数 ([43a81ab](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/43a81abcdad8f909997061d6bc43867d47725c3c))
* recalculateBrush参数 ([023cc00](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/023cc001d224a7fc5bd9e867c72add269dc05de9))
* 从iframe外输入框切回幻灯片无法输入；移动端进行输入无法唤起键盘 ([dbb469b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dbb469bf99cd5b3788cad13d88a4aba2f02825ed))
* 删除主题确保不会被 slideMaster 和 notesMaster 引用 ([ddfe262](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ddfe2620127dc6c2e76449377b357dbe4129de88))
* 字体加载并发限制 ([c141254](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c1412542c0811b7b0c4d41905876e366d4623f39))
* 微信浏览器无法快捷键复制 ([13edf79](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/13edf79bc8c0b146e51230a57bf93f4f4d3383b4))
* 表格边框优先级 ([d330a11](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d330a1120dc581b1c846360de5fd0288a9f0e6d1))


### Features

* 补充 io 日志，新增 actionTrails ([b010eab](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b010eab23b2318541d3f790dfd830ad447810333))


### Performance Improvements

* table ([54e57fc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/54e57fcbfcea951af6c2d2010258d67004077170))
* 文档初始化，字体放到图片之后加载 ([ba1fcdc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ba1fcdc45362510982baebfcebf882865ef4d205))



# [1.46.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.45.0...v1.46.0) (2021-12-23)


### Bug Fixes

* Aniamtion 原始数据中 tnLst 不存在时无需执行数据修复 ([d8df871](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d8df8718c89639e62bc8fa6bacbf2b27cbbc067c))
* apply slideMaster 避免重复添加 layout ([9804806](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/98048067b92e79c431cb66177b8ffb3624d4d128))
* enter 添加幻灯片，thumbnail 丢失选中状态 ([ed5f4fd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ed5f4fd40972ab295a2ce75e3e91fa6886ec8008))
* error ([63e1242](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/63e1242e3f084e223630f40ff07d994a051c0aea))
* fix error in checkTextWrap ([584f99b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/584f99b1edd639a4bc34b1bdc24a1bfe3c253906))
* fix measure text error ([b78364e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b78364e0afd5eacc7d48d9c3623c3c66357a6435))
* fix measurement during input composition ([aa524ef](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aa524efef594eb2f4d14a4b195f864a19aa988f2))
* note splitter hover ([58ea1a2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/58ea1a231342d28f309f2db40d6695ff7aac6a24))
* 传入空数据时才不修复animation相关changeset ([33143ca](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/33143ca34e7406cb97c7cd02b00fe3cd81a45c9b))
* 修复查找替换中涉及 Notes 切换的错误 ([60be2a3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/60be2a3b71a836dedcb37d2245677db4df996ae6))
* 准确拖动单元格边框计算 ([1c3dcd8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1c3dcd8c93a50a14adcebfc87ac44e5eae5540b1))
* 切页时清除 Hover 效果 ([dc1c204](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dc1c204e518c968eeaa4051647022ae794f96cf7))
* 只读下点击加载失败图片优先 reload ([58b770f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/58b770f9480e82ae468d383887986cf643ea37ac))
* 多余Set_CurPage调用，导致搜索跳页未渲染 ([9d3501e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9d3501e48f7df909c47bf30fd11d5d29ec3ad602))
* 对存在不支持动画的 SlideAnimation 随任意变更时发送 delta 修复 ([feb9598](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/feb9598e9bc719c02079afb86b9042e5d797847f))
* 形状 hovering 边框 ([cad01df](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cad01dfbcf32f1e54b215a5af7a725ca08b1188c))
* 插入placeholder形状，文本框样式处理 ([a339ba1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a339ba1dd013ef4e3f52ba771b60c9074877b0b7))
* 文本删除某些情况进行段落合并 ([49d23dc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/49d23dc171bce3d44277275872b2313a619eb93c))
* 更新 cursor 在同一个元素上 ([4c73058](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4c730582dab449709b7b06fd221d9f84f6d91808))
* 禁用右键触发超链接和右键菜单 ([2ac3082](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2ac3082a5ad93fa0acddd6dae3d3efdfda4b7746))
* 统一 cursor 设置 ([fce87b8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fce87b8970f07e7d80ac816d599e3f81cdb861b1))
* 表格选区计算 ([a763ed5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a763ed543f5eb9177c0cde44d7f86a60c803c294))
* 选中组合对象 ([83163ec](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/83163ecb37b063f3f37e93f717a8e2f1152fa903))
* 错误视频链接 selection 数据处理 ([a9be046](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a9be046cc01adc82847a8e88dfbc44a7f9996445))


### Features

* 添加 invert delta 错误日志 ([b1462be](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b1462be6916238adc7042902998f3caf38766ca1))



# [1.45.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.44.0...v1.45.0) (2021-12-09)


### Bug Fixes

* [PPTPRO-1168] 替换裁剪过的非本地图片时使其填充 ([5b8d52c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5b8d52ccbeb6f8887a19d6fd20935d6c77db6ee5))
* addTableStyle 相同 id 的 style 被覆盖 ([3fcb424](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3fcb424880f8dfcbe97f27d5a44ad786b93e95dc))
* cmd进行多选，点击表格应选中表格 ([29e991c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/29e991c5d667d639dd5be56ebc6c0d0fcc3fe142))
* detectLinkTypeFunc 改为 async ([05a0249](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/05a0249cae97388677ffae3ef11e30cedfd33c79))
* fix columns support for paragraph ([cfc5b67](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cfc5b67ca00de67332c5670eef4466d830db09eb))
* move updateSubTypeByLink to hook ([78200d7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/78200d78d3663490fb856397934b6f155d6e4d4d))
* zoomChange 事件在 onScroll 之后 ([6911f01](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6911f014b6a9580738aa3e2b82c6fd702c089167))
* 修复 notes 动态切换开关 splitter 重置位置 ([a794f6a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a794f6a66e407f8931c0db46bb1c5a3825bd821b))
* 切换键位后误触右键菜单 ([53b196d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/53b196de9eb05f5d59e4001209370b3bb9f8de5a))
* 复制元素 refid ([ef51da9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ef51da9f85379a747d39907ea5b2c28411231755))
* 复制单独形状，重置 ph ([d182d33](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d182d3395a35b60aefe0c623a17263f648b2855b))
* 复制粘贴 slide，选择性粘贴按钮位置 ([4ea4fad](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4ea4fad7ab907925b3c62106086857322aa88c75))
* 复制粘贴 slide，默认保留源格式粘贴 ([b63d178](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b63d178851163d352aa0396bea34d71998df0ef9))
* 复制粘贴 slide，默认保留源格式粘贴 ([4212f40](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4212f40537badfa43aaaf5273be49d8ce28c6c1d))
* 快捷键复制形状失效 ([526e23f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/526e23feec61b2ab107feb12065116e1f449512b))
* 水平边框在左右垂直边界绘制 ([e3456d8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e3456d803b61610889ef9f7158de30b42676f3ba))
* 滚动切页设置阀值，避免误触发 ([e5e2403](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e5e24031267b2013e131af34c330b20079d57676))
* 空run被合并，setSelectionState 进行 correctSelection ([08b454f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/08b454ff43e477f2019b715061e49e57e6c8fe53))
* 表格拖动行计算 ([729766b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/729766b980dc7098ac148feec1ba4d4e451ced68))
* 表格绘制背景 ([5e6b898](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5e6b89884f0748605dccea60ef6e42293286eb17))
* 表格辅助线；Overlay 更新优化 ([8595968](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8595968a0cd23823eae28987f3ae200aaa82e9bf))
* 表格边框对比 ([a20aaa9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a20aaa91a9d9e83632310b05a8238d4598bdb3cc))
* 选中表格有边框选框错位 ([02c0b29](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/02c0b29e8dfd710bacac8c88e33178a85ffde321))


### Features

* canva ([#565](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/565)) ([d7424ad](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d7424add6e05ab267426d7e437f0bed7e72832d1))
* 新增 tip 组件, 用于展示 rot 信息 ([123dcf8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/123dcf83fc1fa7717cf94c2cb755ab0d8b3a4ab5))



# [1.44.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.43.0...v1.44.0) (2021-11-25)


### Bug Fixes

* api 参数引用错误 ([4e2fc86](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4e2fc862912bdfd63474d4953a3b6894623678d5))
* apply 过滤空 table ([6c9f39f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6c9f39f1e5511f39d79d521d8967350e34a5d3b3))
* fix lint error ([db0f9d9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/db0f9d9f82edc4cf9971257895031baaee3615c6))
* getSelectedCharts ([72136b3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/72136b38a716278286d4e0dc252dca7674077f4d))
* gif 判断；复制 slide 图片大小 ([4556fa2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4556fa2b1c4662f6cb4e797452feb4b4a9945215))
* reduce memory consumption by removing closure usage ([ef5e72c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ef5e72cb54936ced13a4dd33402fc8ccd4e044c8))
* removeTable 参数错误 ([540c180](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/540c1808a67b703a975a7c1552e9f30c505e8180))
* stop append/remove font detect span element frequently ([5aa973c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5aa973ceec53ef4b120c842172c2603e6dfa7417))
* type ([f6fcf69](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f6fcf6902f5f5c8cbc1f797058333623eb63958c))
* updateImage ([3783caa](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3783caad15f0214aa392b87f8ebd4cff51258e31))
* 保留原格式同一主题判断 ([1ab76a0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1ab76a0b912be54fec30400293013898719ca631))
* 修复 isMapHasKey 导致的 apply 失败 ([2008084](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2008084f46d467657c666ba5144379b96b9cb5f6))
* 修复 paste 后 undo 没有恢复原状态的问题(需要在 CreateAction 之后再 reset 状态) ([91ae459](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/91ae459c6f73ebca41dd509237e48b6f4361444e))
* 修复评论 Hover 到新 shape 后未更新状态的问题以及评论相关 firePaint 启用缓存优化 ([e40460c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e40460c9c7a9c66fda4b3373ce79a86c7642496d))
* 判断 isSlideElementInDocument ([3dad22b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3dad22b75731883f855b35cbe01628b863bfaadb))
* 粘贴元素顺序 ([c070fb6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c070fb615210928435d57b3c72f7f8c8aee87b67))
* 粘贴按钮跟随滚动缩放；外部复制图片不显示粘贴按钮； ([6e31dc7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6e31dc78ef25172f88c533dada4c24c8870544e9))
* 粘贴文字不保留字体大小，不生成图片 ([125f5e3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/125f5e3bd219b9e807ddc8b8086f80db7a2362ec))
* 选择性粘贴不增加位置偏移；粘贴后按钮显示； ([6d5ec71](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6d5ec7117947424722318b5a138bf98e688021ed))


### Performance Improvements

* improve rendering performance during zoom in/zoom out ([2fd1261](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2fd1261de7e7363c4a4621b2a7b70beb1f801347))



# [1.43.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.42.0...v1.43.0) (2021-11-18)


### Bug Fixes

* [PPTPRO-1108] 完善根据形状坐标跳转滚动条 ([fdb1844](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fdb1844f50c589b13dcd8e4697f21f361166292d))
* [PPTPRO-1109] 修复绘制动画时计算 bounds 小于实际宽高的场景(arc 弧形) ([e944429](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e9444294d1173a4d745b56995c9115d048020bb9))
* bypass chrome's rendering performance abnorm ([a8016ac](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a8016acd780a86ff5801af94d969b4ec89e5fa0a))
* draw placeholder text properly ([d434627](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d434627adc1931cb3f33c7b3095c649a765b47e7))
* Evt_onCountPages 事件 ([9e6af39](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9e6af39eca78d9aff4b85593a08fe39d71a48989))
* fix code mangling ([e601892](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e601892fa32a702e08cda38dc758659c87a34101))
* improve drawing updates logic for thumbnail list ([d7cb81e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d7cb81ebda8dbc7c0dbe0adbe2a9dd8dc94234e1))
* notes 删除时去除双向引用 ([afe40fe](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/afe40fe2af2d250f71201e2ba087824825172af1))
* properly instantiate GraphicDrawer with pool ([34d4953](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/34d495385a94676b565f9612fdd97ce4ded0fb27))
* specialPaste 导出 ([d8309e5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d8309e5253eb0e1ab6c8adc94304214b84fb9ff8))
* spelling ([aa2a636](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aa2a636b4441861bceadc3eabd509b1e495eb8d7))
* syncInterfaceState 第一次不 throttle ([a01bb31](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a01bb3112046d981c251a538dbb7f7c33608db0c))
* 修复 group 内 command 复制 shape 后撤销, 依然可以选中 ([5aa8531](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5aa8531aeaa17ab3ed89ea1186a33e17adb553eb))
* 修复 group 内 shape 复制后撤销依然可以被选中的问题 ([fe7beab](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fe7beab82769bc22886f1f6ef6af8566309e1129))
* 修复 selectObjectById 未产生 scroll 时未更新 overlay ([2854d45](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2854d4526ef45d7908af241288355d6ea25761b4))
* 修复已有选中时方向键跳页问题 ([03cd43e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/03cd43e83791c66bef52832d311a8aa596c00611))
* 删除多个 slideMaster 导致更新 theme 数据不正确 ([797d483](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/797d48367422792177a645734436f0d4ebb5a924))
* 利用FirstLine计算时，应使实际可计算值 ([8cf1426](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8cf14261ea4693713f4a1d8d8f2296909b8940b9))
* 协作 ui Hover 效果修正 border 误差 ([3a959bf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3a959bfa07f84a19c1bb8121736cf7e63ec78654))
* 复制文本框内文本，文本框选中丢失 ([5fef6ff](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5fef6fff8bbfcaac2013a56466614d8e8c4a8da7))
* 文本框选中状态不正确 ([1f20b9f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1f20b9f4973621ba0b37287b584c8b186e83405f))
* 选中不再绘制 Hover 协作者列表 ([b1f4d3b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b1f4d3b3ee0bc01c8ff64a6aa1382dcad8deaf30))


### Features

* collaboration ui ([92f01fe](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/92f01fe9fc2580fa4f097d7d213c0547abeaba12))
* 协作 ui Hover展示协作者列表 ([137e55a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/137e55ad14466c73232e443c36ee597b65268cba))
* 协作 ui 对齐 prd ([329f438](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/329f4381d272892173591e2a1a667d8648ddc14f))
* 绘制悬浮协作者姓名框按 flipH 方向展开 ([b4c79e9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b4c79e9e9899429dae7d4fcae9295552789e2335))
* 调整协作 ui 样式 ([2bc4c10](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2bc4c104561aa8b69c665a848a20f21adb9797b7))



# [1.42.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.41.0...v1.42.0) (2021-11-04)


### Bug Fixes

* autofit 计算 ([7e57eaa](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7e57eaaa0246edad05d2c563e5c4d1f392a09be7))
* isUseEndPage设为false，自动播放进入endPage ([e1a450b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e1a450b00e83e7bce36a9a7b5ee890d590a7df0c))
* onInput without throttle ([d8cce04](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d8cce044450ee36a106cdd28791d3bdcd461b09b))
* syncEditorUIState 改为 throttle 调用 ([fd0141e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fd0141e03c7eb8432c747ffc9f49be613f9271fe))
* throttle syncInterfaceState by trailing ([f20766e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f20766e3bf1508afc82dc76c04762eac85f8e85c))
* 幻灯片列表缓存数量 ([f97d597](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f97d597ccc5856a2014dde8829046eb0f4b9ae40))
* 文本渲染性能 ([6f97758](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6f977587a0d8669188616de484a8bdae0951d048))
* 文本过多导致输入卡顿 ([991e10a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/991e10aea48f2a3dbc4b2551290ed9046be3f2c2))
* 未选中形状，插入符号 ([d1d39b2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d1d39b20109e50e5bb9edfd89891e5899816ec77))


### Features

* 对齐辅助线优化 ([#551](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/551)) ([f2606c9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f2606c9c9df89ae648322fde2a905d120edfeda3))


### Performance Improvements

* 渲染性能优化 ([ce8e238](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ce8e2383a0ead923f9fc4ae4f3f7c1552cde2ab7))



# [1.41.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.40.0...v1.41.0) (2021-10-28)


### Bug Fixes

* [PPTPRO-1097] 评论多人协作同步 ([f8fbaae](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f8fbaaed886ad1fc18753d340fb0addf2a6ec9f3))
* fix operate imgPlaceHolder packet change  unable to find parent  after addImageByPlaceholder ([cc8a576](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cc8a57651df501236dfabe47a4accfb954a0e0a9))
* fix the decline in rendering performance for thumbnail list ([13a28c7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/13a28c72aaa8d9cee15cdf38125891a3bb507342))
* gif 图片判断 ([a0ea601](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a0ea6019732653738006772a2d2c72217c2566a4))
* remove unused recalc logic & decrease cache size for thumbnails ([1d5c2f6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1d5c2f69a1e114280dad0fa43c8f7119cc4a7165))
* 切换主题数据 ([b4aae77](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b4aae77118829e6e6ddffe158e2b28fbbbab61ca))
* 图片占位符参与查看大图 ([1562236](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/15622362c93272109c53644441e7f874a0079696))
* 查看大图获取图片返回refId ([848fe01](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/848fe01cdac0fe4565e552c9fa3bcc285288c473))


### Features

* 支持缩略图&备注打开/收起 ([fe96092](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fe96092e1b46e3892b032511dc09bc1faa2ae923))


### Performance Improvements

* syncEditorUIState 和 FocusObject 进行 debounce ([689a115](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/689a115f8dce956ae0923c57a591d1a43ea16025))
* 协同时debounce幻灯片列表绘制 ([119976a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/119976a60b3867292ef65da2e9c4bd7bec6ca36c))
* 用户持续操作时不检查更新幻灯片列表 ([#547](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/547)) ([7d56630](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7d56630d34ece6e2f32b11f2b69df0125c6de49b))



# [1.40.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.39.0...v1.40.0) (2021-10-21)


### Bug Fixes

* • 符号字体改为 Arial ([9cadafb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9cadafb4665ae258e0be9c847f814d92a64fa559))
* canfocused判断层级改为10 ([ca77d8b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ca77d8bb1faf7707a12b5921d1cd16938c092ebe))
* check thumnail dpr variation ([540b19d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/540b19d123d5b4d48931fe5caf751a78826f8da9))
* fix error after undo deleting the last slide contains table ([83290d6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/83290d648f16f7ef2c0b5403aa270f7218711499))
* fix init marinContainer borderPos ([892fdf0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/892fdf047a7ec28479862d1ac6880f82b7d5a1c5))
* fix startAddShape error drawControl when focusOnNotes ([944ff77](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/944ff775855ca181923135df54009ed9b47e054c))
* gif图片判断 ([26b7014](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/26b7014cc326e2f37ea16804dab43007dd2c3046))
* mangle ([3b983b7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3b983b76e71ceb2ad1a79d598a3f479f84a9d716))
* sync stop state when demostration is playing backwards ([79e2120](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/79e2120eb7689a1a0815bbe97684b35b40df8c48))
* 制度模式下限制左键查看大图 ([9343e6a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9343e6ad86a014eed02c6dd2d730f4b3634f5244))
* 占位符图片不参与查看大图 ([da42f70](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/da42f7037485aa1da873c79b7e60f21e05741184))
* 只读模式下单击查看大图 ([89ef2bb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/89ef2bb605e168d3b105cd268513b5f7b4e5cb68))
* 右键菜单坐标改为基于全局 ([0477ba0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0477ba0771473e4cb43573ad5bf7f2caae822f31))
* 更新 gif 标识符图片 ([fe86f03](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fe86f03bd6b7408e54d3535e98d5c4ce0a3211da))
* 滚动切换 slide ([3e3fb71](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3e3fb71123d5bb6498f4f96d9ef527ad695efd7a))
* 演示模式下，点击查看大图坐标 ([5050052](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5050052adaed82fefb05316bbd5a4938e9d75e28))
* 结束演示，关闭自动播放 ([0eaab21](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0eaab213152555fa1164d350e4e7fcb80e3a16a1))


### Features

* 添加 gif 标识符 ([1174220](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/11742201cb1dce1a36702a08aee362cc1aba495c))



# [1.39.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.38.0...v1.39.0) (2021-09-28)


### Bug Fixes

* [PPTPRO-1053] fix shape inside group resize ([5136c5d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5136c5d75ee5f08a24f195c4fadd4dfca9da56b3))
* DocContent 最大宽度计算 ([8d77265](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8d77265d3c9e66a70315a55b8a37aa06bebf1323))
* error ([9f7ff68](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9f7ff6805ccff429f1ff50c4e6a27dd246fa15ee))
* fix add cxn track effect ([4647212](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4647212cea209ac3a8981623fd67bf3d866a1812))
* fix anim pos ([e6aeef9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e6aeef989daa51d55e92a39fd4131e9744c6db91))
* fix animation blink effect ([aa71d0d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aa71d0d6dd02470140458ea9c40ebd865a01913b))
* fix animation multi categories behavior ([6fb3806](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6fb3806eefe04e19aa2d2ced4832a9f3b16fbcab))
* fix bounds caculation for group shape ([21e66db](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/21e66db038043d6bffe320c5ec56e20dc9c4aeef))
* fix click effect in shape animation doesn't work ([69f7b77](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/69f7b7749395b3380589e5be1824eda943c3714f))
* fix cxn track transform ([4efe8f9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4efe8f9c03e3f994763d00db029940d293032ed6))
* fix typo ([9496535](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/949653552d644ae52b09fb78169b3bb0de488238))
* flatten animConfig ([b913997](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b913997af56ba59f0fc77bb2c1b23de5e3647c36))
* handleShapeImage处理顺序，文本链接、播放按钮优先级高于形状链接 ([dfe5d98](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dfe5d9872b6629e34107ac4b3f91b833b31f40f5))
* hover 形状位置检测 ([124d439](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/124d439ccab80e955e924c3e3c834c1f5c2efd88))
* hovering hittest ([7f05db1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7f05db1ffb9e786b974e46df62a0345a6779205d))
* refine synchronization for demostration ([2a6c290](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2a6c29091bee8c684050f1c488f2539d47a0e1fa))
* remove debug code ([4156af4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4156af44a4b9a75e431a337fbba30286dec12ec7))
* 切页或动画中不能点击播放视频；演示中后退，暂停自动切页 ([ec411b5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ec411b53c0618e0091230435c28dabe72714f7b2))
* 删除形状插入 placeholder 应取 hierarchy 第一个 ([c4b128e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c4b128ef0093ee0f1cd79fcf22c427f8f4d51d52))
* 插入占位符图片从 placeholder 继承 spPr ([9dccb22](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9dccb222203c46ed7bd6be92d1c5cab267bbce45))
* 文本框hovering hittest ([a1a9337](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a1a933784aa060118d778c0dfe3dd9fc2d266d0f))
* 无幻灯片时缩放&点击 ([08b8441](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/08b84412d31e030ceed1548dfe5618ba1aad1e3d))
* 格式刷不应用 paraPr ([c0c43c7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c0c43c7c067b0b8c98b2340643fc29ebec413619))
* 过滤 -apple-system 字体 ([b14e552](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b14e552862dfdf7d487b27a496bfc625f986630f))


### Features

* 演示模式下双击图片查看大图; HittestInfo添加链接信息 ([d93490f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d93490f3acf3ba6e1d7da13eadc8efbfdd47940b))



# [1.38.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.37.0...v1.38.0) (2021-09-16)


### Bug Fixes

* [PPTPRO-1018] fix crop palceholder checkXfrm ([28930c1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/28930c159b539299f342f9f490cda24cf8043482))
* [PPTPRO-1031] correct cxn controller dot drawing position ([d72c26e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d72c26e9c16f4411218aca825036138bae0e7b5d))
* [PPTPRO-1032] fix add bentConnector init rot & connector with rot ([400e239](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/400e239d558bb0fda1d88268a3817b6034638d67))
* disable crop in viewMode ([693edec](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/693edec8fe52405f1f17d844dacdd0e4c9a6f617))
* export error ([232a3b4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/232a3b4a00f9569a0ab96ca0340395c8b121f620))
* fix drawing flipped shapes without animation settings when animation is playing ([98aad79](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/98aad795154dca36e7e65834dbee3096a858fc47))
* hover 边框线检测是否还在内容中 ([e035b3a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e035b3aa5ae69f9a0efb704e91c7ddc3f0c0e26c))
* order 快捷键 ([1b1bd8d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1b1bd8dc9a06f333ee8b6289bc3acf6b96d09a69))
* refine synchronization between Client/Service in demostration mode ([e9dc56e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e9dc56ecb9a389262e31c0255c8c7abd60314629))
* 对象hover框绘制 ([733aa8d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/733aa8d79c36e5f14f09270c3782380d71ad14b3))
* 嵌套组合协作时选中状态丢失 ([8b76d9c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8b76d9cc4a277a1d960dd1f8d7a00d8f6a67d6f2))
* 旋转翻转后控制点绘制 ([2d894c8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2d894c8d146032dc22030b14e6370c8dfd1e98db))
* 无 Wingdings 的其他字符用 • 代替 ([caadb75](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/caadb75da273518972151d7f463a14b012105c7c))
* 编辑态控制点计算 ([4c0ce69](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4c0ce69bbcc8a0bee0c98007549393fb918596c3))


### Features

* support toggle auto play for demostration ([262a3df](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/262a3dfce6ad0252943ad1a1261bdb0017105a95))
* support useBgFill for shape rendering ([3c43a92](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3c43a92c189907cb5bd70f545c81909ac69366ff))
* 对象元素选框交互细节优化 ([349774d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/349774d0c0d43915f7ab033c08ea4a56a7a61644))



# [1.37.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.36.0...v1.37.0) (2021-09-09)


### Bug Fixes

* [PPTPRO-1001] fix replace img when crop ([c383e1b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c383e1b07b90bd11611996d8f0a14db70514afb5))
* [PPTPRO-1002] fix anim scale effect ([80541bd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/80541bd3a0185b47aca93c3903f19bc448a49634))
* [PPTPRO-1005] dbClick cropImg check ([711e3b7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/711e3b791c2b8f09e55985d43426f5f259c1ccd9))
* [PPTPRO-1011] apply change restore groupSelection ([b051e91](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b051e9127493f07334d8233e3086049bcee498a3))
* [PPTPRO-1012] fix img in group resetCrop & click thumbnail exit crop ([488514c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/488514cd7ebefef14756083c31525ae6b8917fef))
* [PPTPRO-997] [PPTPRO-1004] cropApi async thumbnails & undo emitCrop ([474753e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/474753e179b4699b5498ab8bad3ea0ec1a0a698f))
* apply transition to slideMasters and layouts as well when "ApplyToAll" is invoked ([872e12c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/872e12c54aef4cf5b53db91ed15939bd9c3fd237))
* cropByOption sync thumbnail ([13a36e2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/13a36e2e66cc8ff5f66912ad00307a63aba37e3a))
* duplicate variable ([70f7f05](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/70f7f051a6c4196a1778b8381251355d8230a22b))
* exit crop when undo util insert img ([28256f3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/28256f36a01c00b663264e9275da771ebcddb605))
* fix crop async thumbnails & exit crop ([65cf2f5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/65cf2f5162d5501ebb30f77c5dab12002615fdd7))
* fix drawing group with flipping during  animation ([e86cb0f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e86cb0fc478e148ff0dde55cb60b129cb718f96f))
* fix flipped group animation drawing(PPTPRO-994) ([a8e7ad8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a8e7ad8716d929d101d235de2a0950820f991996))
* fix img in group dbClick crop & Arrow key behavior when crop ([26083ac](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/26083acc2727a4466a884e8af5c5ab44e552384a))
* fix start coords for flyIn aniamtion ([40bae6f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/40bae6f6e6f78b8ddc9a61d612c58ef8ab203cc9))
* fix undo crop until no srcRect ([f0ae35f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f0ae35fe5284f0da24087ef481c2f69d31fc3ab0))
* fix withEffect animation behaviour ([887f6c1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/887f6c141215493b2b6769695aaefec8449b997a))
* hang 缩进计算 ([2cc91af](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2cc91aff6fca8dc2cf8ea6d931d3018d1d140f68))
* key 被混淆 ([b794ca4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b794ca483851493cc4e718eb949a81a9f513b903))
* run 的 Metrics 取值错误 ([1afebce](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1afebce62b0c3c1d1c5b5c45683e45707bd94163))
* SlideBoundsChecker 未绘制 txBody，导致幻灯片外超出文本框顶部的文本被截断 ([3413690](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/341369096dca0dca32b50f78bfe98bd2bba0e1dc))
* xfrm 初始值 ([82ad1fe](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/82ad1fe392168d146c3801f7d0c70d6b6d1c3138))
* 备注 cursor 变化 ([ca8a5f1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ca8a5f17bf32270d28581bca292b787a654cce0d))
* 无 Wingdings 字体列表显示 ([787c75e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/787c75e88ff321a9325600fe1b86b6e8971fe8dc))
* 竖排下无 vertTextMetrics 情况绘制优化 ([1aafae9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1aafae9bddaf32450d944ca051736978a66cf949))
* 竖排下非CJK字符绘制 ([81efd2a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/81efd2a3e06a7b552746b46fc263e7de223fee5c))
* 选中形状 api 直接插入文本 ([77df635](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/77df635bba6cd1fb5ab53164415b5ac7523efa0d))
* 限制  TEXT_AUTOFIT_SHAPE 形状的拖动 ([d966b1f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d966b1f17ce117114efb8e01f5ef23ceafea948d))


### Features

* support toggle useTiming ([6f01cf9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6f01cf9b0cd7ed1721dec489cf6e40387acd4e68))


### Reverts

* Revert "chore: add main field for dev" ([b06b09a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b06b09a54bd6583123435cf330e2ad358d319730))



# [1.36.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.35.0...v1.36.0) (2021-09-01)


### Bug Fixes

* correctly draw flipped shape without animation ([b87d98f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b87d98f1ddbcfb6ec8f986119b15bbb3b8d33757))
* disable chart/video image crop ([26ed2d3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/26ed2d37b3e92d330b195a090a2d17bea8da44eb))
* disable multiSelect Crop & revert updateExtents ([8f1c419](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8f1c419a7ed67497b0e576c27048c7ded8692fbc))
* fix apply notes changes ([eb3bf3e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/eb3bf3e553009196f736e0111d222647b8909dc3))
* fix click behavior for the shapes with animation[PPTPRO-981] ([e3ff91f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e3ff91f1fe1fed9e984dfef8b0bea50520566fe3))
* fix cropByRatio & handleUpdateExtents  able immediaete Recalc ([c0bb69c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c0bb69c9bcaba5cba855879f64b0194453d609f9))
* fix enter crop updateOverlay & undo crop restore correctly ([18f673e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/18f673e16a0429a06e56f0dfc4b1f4593a331984))
* fix image draw dependency ([8c94d07](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8c94d07c64cfd13b30420c64d12b421e23b3c316))
* fix setEditorFeatureSetting code mangling ([1d736ac](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1d736ac40da4bbc4e7c9cbaef901a9915539d041))
* fix typo ([3a2c969](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3a2c969b52458dd85e0cd2a86dbf9135634002a7))
* refine crop recalc when applyChaneg ([69cbcdb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/69cbcdb292aa3fbd906c41ca191525c71843bfea))
* refine handleUpdateExtents ([93fa17e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/93fa17e0cb9083b209045caac3c3f57adbe10625))
* refine no intersection cropEffect ([ef7572f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ef7572f8b5351c2f74ca1fb7025927eb3f812a0d))
* refine zoom behavior ([092c4af](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/092c4af13d0519d9426d61795558ade439414bd1))
* remove useless code ([bbe505a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bbe505ac4ecc7b1802200f7cc99fdfceedff25af))
* remove useless code ([dabeea8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dabeea833ed51cd3aabf7b99eeb7b16db3bc0ab9))
* turn off change collection during loading data ([706ab10](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/706ab10e47f5d6c06467cf1ea2d4180c244ae5e8))
* 从 txDef 中应用默认样式 ([74d16b6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/74d16b6c2508753a1d5738fcb687d8d04f81f7d2))
* 文本框无法点击输入 ([d9472aa](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d9472aa36af6efc0133b7e72d2fcabb059924906))
* 竖排下列表绘制、中文标点、水平垂直对齐计算 ([842ce0c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/842ce0cd548371d44b2a5eb8b4a635d9c36457bb))
* 竖排文本框高度 autofit ([c4da3ab](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c4da3ab047fbfe82f8e9b31220aed11a22e8ceca))
* 粘贴错误；列表绘制位置； ([739f7bb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/739f7bb76566efa9768d6db6c36c3ff919b2c1db))
* 苹果设备绘制时过滤 -apple-system 字体 ([19db691](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/19db69171967af3af9d8f54fd4ba2adbd72ccb54))


### Features

* eaVert ([5caae56](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5caae5645072a803ac849eef6fedd978baaad2e5))
* support loop show & pause/play demostration ([b1b3e30](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b1b3e306e0595c1e7ee45588cfb24b999de9f160))



# [1.35.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.34.2...v1.35.0) (2021-08-26)


### Bug Fixes

* [PPTPRO-960] animation fade export ([a3d831b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a3d831b2835c7ed75a4b7c3118d229c2b2827bcb))
* [PPTPRO-961] fix animation  sp  inside group with flip ([1722eb8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1722eb864893fd041ca29a2038578c74fe09747b))
* [PPTPRO-963] fix changeSize group recalc ([f36b7b2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f36b7b222aab999ef4c495bfea6db27c6e5e1332))
* fix gotoSlide's behaviour when animation is playing ([a38459c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a38459cd56bd4b740a8b6430d1890ad24becec32))
* getHoverObjectInfo 传值错误 ([2af5966](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2af596622d67474146eecae1ec85c2fa2d1b34ee))
* type error ([05dffc2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/05dffc2cae5edf337131bf08a5391ea54a464060))
* 占位符插入图片，更新cNvPr#id ([5921936](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/59219366b921659de34ccae370ad04c46edfca83))
* 幻灯片列表图片可能加载不出来 ([0562a55](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0562a55bf28595096031b4ed52d0092e966f6699))
* 幻灯片画布双指缩放 ([0d406b8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0d406b83fc089ecdef0132e6d9e93054f78a3b50))


### Features

* support set loop show ([002f68f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/002f68f81a94b56f85917782443f0b629025fe45))
* 快捷键改变字体大小 ([f5c95b8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f5c95b811b3667fa6907a2fb9c6e7a53e7c5c45b))



## [1.34.2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.34.0...v1.34.2) (2021-08-19)


### Bug Fixes

* 占位符插入图片，更新cNvPr#id ([#515](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/515)) ([c36e3be](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c36e3be17b0ddd77d15601acaad4774905c26562))



# [1.34.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.33.0...v1.34.0) (2021-08-18)


### Bug Fixes

* [PPTPRO-909] fix multiSelect table selectInfo ([d140cd9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d140cd9dffc850464c476f478b9c322b139ec8da))
* [PPTPRO-922] flyIn effect ([2b57d7c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2b57d7c88d2bbb4ebdc6451f79e41874562fb1fc))
* [PPTPRO-935] clear animQueue when fromJSON ([fc522ad](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fc522ad20561ec8850fde3458e5eec8efa192c48))
* adjust animation options order ([e776b18](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e776b1839d263726a2a65a598a1ebf40bc88d845))
* copy slide's animation correctly ([9a8ad12](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9a8ad1228a7ba60aca0e4e3826517df255b3e72d))
* correctly end transition when exit demostration mode ([e058f39](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e058f39ef9afc282bfb077739b9ef7bb8d98eea5))
* drawing properly when sp has a rotation ([9e097e9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9e097e9959dfa7bec8f374be54feec8d743a2c43))
* endAnimation repeat emitEvent ([6429109](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/642910952e86d30ef363b08718cbacfee992c561))
* fix anim visibility & demostration prev autoStart & code mangling ([9d7203c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9d7203cff3ac8430ad3126fdd8a1ee984d253415))
* fix animation abnorms when repeatCount > 1[PPTPRO-941,PPTPRO-903] ([67d24a2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/67d24a2a4776dbd4ba247f12acadacb8cb103126))
* fix animation afterNodes export ([41cc1a1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/41cc1a161a3732937abc494f27c9815a0bd1bbfa))
* fix animation drawing for the groupShapes that contain connectors[PPTPRO-949] ([df42e52](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/df42e526d006f0b5fb6140d49a8c3331aecc6862))
* fix animation scale effect ([ff9f8c8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ff9f8c85e50964c4196d61d3d127265899836579))
* fix Animationdefault trigger export ([a4f380c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a4f380c29aebf95f6aea8ed5694e38e8597ec708))
* fix animationPreview bgColor ([fdb6a3d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fdb6a3d29290464b1bfc92ba53757c1bd29521f3))
* fix apply palceHolder Anim && add assert field ([56374a6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/56374a646cfdf98c5fb476858e2ae00ebb6aca24))
* fix applySlideAnimation ([89496ce](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/89496cef6c271368838c59d672dbfc67cadf9bc4))
* fix checkSpPrXfrm setRot ([e3eb543](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e3eb543486e72ec3fa19e5db0f9e3ca235bf58cc))
* fix clone animation of the slide ([c086acd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c086acd65c0571af5f380a2c9d56b0b88f31bcf9))
* fix demostration | preiew bgColor ([6d4e1ae](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6d4e1ae5709fedbe4be2742f0ab42feed8b17baa))
* fix demostrationAnimation stopAtbegining prev setPage ([7fe1597](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7fe1597de186df70813daa53a716a5963d40e9bb))
* fix draw animation frame correctly when sp has flips ([07a0900](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/07a0900dc24a1cdf11731f63a6d44d4da01d6967))
* fix error when demostration a copy slide with animation ([c00a6b4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c00a6b4731cc7f428b93418e94156b475198d535))
* fix groupShape return selectId ([0d3b3a6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0d3b3a6cd2bca5f0d0da09ec1b2651b3ac371daf))
* fix percentage calculation when repeatCount is set greater than 1 ([2485bcc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2485bcc9a2444149247111c99713e1966dcda8b4))
* fix repeatCount toJSON ([81a849d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/81a849d502422a2b9ae168f498372392459f3b7a))
* fix SlideAnimation toJSON splitNode ([06e7242](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/06e7242075097d4770bc4ba347ed6b05b37d1d26))
* fix SlideAnimationOptions code mangling ([62e340b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/62e340baf48f191951924655cb61b663dc394d59))
* fix split timeNodes && apply repeatCount ([6a546f6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6a546f637e09ddfe2526cbb9c9b8521749e98eb0))
* fix syncDemoStration action ([abf332f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/abf332ff1187a916dd3dd296099179fdc6e446dc))
* fix TimeNode Id reset when toJSON && split timeNode ([a1f97e6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a1f97e637c3b158a0ddaf3e60d6ea84884fe2e07))
* fix transition end with animation  error paint twice ([5371205](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5371205489c8a8d03d9013b89035b476f90b5e0e))
* fix typo ([455ba6a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/455ba6a9a39bf08dd107ee2da711cd899c8f4df9))
* fix typo ([04d9d33](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/04d9d334aae8af94fb2fba9be1064dd9d498eb15))
* fix typo ([968bfea](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/968bfea658451197e0a46067731775fef1071dde))
* fix typo ([77b0739](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/77b07397251eb02835ac5db8c589dc11f92209c2))
* prevent cursor moving abnormally during collaboration ([a1304ce](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a1304ce204f2643efbf3ad46dd17b78812f6304e))
* refine animation shapeTypes ([aac45e7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aac45e7f7ecfd4b7ca44bc736fb5b46b8d3c2de5))
* refine canAddAnimation ([2434524](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/24345241dfa706c7079b539eaaf7cc743e3203e5))
* refine getShapeInfoById ([e21c99f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e21c99f3852893f907881807b4a9ff1020ee68a5))
* remove debug code ([12cc7c7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/12cc7c74f5a4ab457a1d2b82cc9522b3e66ff534))
* spelling ([609da20](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/609da204b18d3399dda46ba87baf0900863c78d8))


### Features

* enable animation feature ([4dbae9a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4dbae9a18ea2ea1f96dc495b951f14f139bf09c9))



# [1.33.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.32.0...v1.33.0) (2021-08-12)


### Bug Fixes

* SetPaintFormat 接口事件 ([5ca4fb2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5ca4fb2b128e14d1681f12c9c26739a71c04199f))
* 多选表格属性 ([25f58f6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/25f58f68fff9f1243c4c9fea610dcdbcd59de255))
* 新建线条 overlay 颜色 ([4f069b3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4f069b3acc2056b53037138c3f696905c45f32ba))
* 新建线条样式 ([e70666b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e70666b217d0b9b4aca6c1cedf8042e5d91bc4f6))
* 点击幻灯片内空白处，取消格式刷 ([525247e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/525247ea98e7e17b464fa22cb277d0f768de532d))


### Features

* 新建文本框应用主题中 txDef 文本默认样式 ([4e6d323](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4e6d323965178c966d0b773e6e3601600b7b1b68))



# [1.32.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.31.1...v1.32.0) (2021-08-05)


### Bug Fixes

* getMatchingShape 匹配规则 ([aeb4882](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aeb48820c24e880e1fa9c822c49a04f7666f5b13))
* improve the sensitivity of scrolling the thumbnail list ([7a9c8d0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7a9c8d01c66f62fce1c62ed59f6a2287cf8ca1d6))
* NotesMaster 只有一个 ([507d3d7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/507d3d78148741ebbbec6b19a90d0a316d2f8643))
* refreshPPT 性能 ([340afdd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/340afddc068ddb0e16b7361b2b2b561679f7735d))
* 列表字体；noDownload 不生成 font-face ([67ea959](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/67ea9593ee3232a39b00f97df9531b72bafffd41))
* 复制粘贴 changeSize 在设置 Layout 之后 ([642df22](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/642df220f8f020c3926bc66e93fa88f3697ded92))
* 多个 ParaNewLine 读取；选中多个多个单元格 delete 删除； ([221c336](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/221c3364a9f8e98459ded1556f157d6d56d24ca6))
* 字体 alias 支持 ([0128383](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0128383089284827cef934aa2165143d5851a18b))


### Features

* noDownload 字体不进行下载 ([c473cbc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c473cbc54bf4201500ef15a50cb40a10c24f189a))



## [1.31.1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.31.0...v1.31.1) (2021-07-28)


### Bug Fixes

* 插入无法获取封面的视频，加载展位图 ([e6bf6f9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e6bf6f9aa0296d59805f7c3fc3bed5bac2c5baa3))



# [1.31.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.30.1...v1.31.0) (2021-07-28)


### Bug Fixes

* maintain the thumbnail selection during collaboration ([7770d1b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7770d1b3327cdb90e7876201be892c4ade1923a1))
* onMouseDown 获取焦点 ([66c51d5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/66c51d508e37da9f20132dc4df1708c42b69b3fb))
* refine rendering logic after applyChange ([1257673](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1257673ccbd806a51ac13196e83513e0edeefcad))
* save & restore ui state during applyingChanges ([7444612](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7444612dcb08a7935139ee4c0eeda149dbafaafd))
* 插入图表后无法 focus ([9ea394c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9ea394c32c51e4dbbcb3d7788da45a73ef3057ed))
* 新建幻灯片无法输入 ([78c86e8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/78c86e8fcddf4f9c1a2a27e993e0080d872dc107))
* 视频错误处理 ([bd6d59a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bd6d59ad551132089ea6650905d010c047b257ff))
* 禁用视频超链接 ([f54afe6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f54afe623b0948a8315db40784840fb516c380d8))



## [1.30.1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.30.0...v1.30.1) (2021-07-22)


### Bug Fixes

* command 拖拽形状错误 ([f3e2c66](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f3e2c668ab8ab4380700503f33504225a68d010f))



# [1.30.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.29.0...v1.30.0) (2021-07-22)


### Bug Fixes

* checkErrorImage 判断 ([0bd238a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0bd238a56c957b73cd090aaf0ea2df9b1936d0a1))
* fix changeSize recal Hyperlink  Fontsize ([1b1a387](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1b1a387b06d66317b7f84dbc85d22969732475e5))
* getSlidePreviews 前更新 slidesCount ([487a836](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/487a836f6c47f9fef157ea584735396e2d1eb93e))
* prevent cursor moving abnormally during collaboration ([1b26584](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1b26584c45fc70d3824da0df804caf75c0691f19))
* refine drawing snap lines when moving shapes & strip out debug statements for release environment ([5d241ba](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5d241baf26fd9192331570f30c429431c44e14d4))
* 视频接口添加大小参数；组合形状播放视频 ([e301eae](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e301eaeeae6dbeef8bd0ad6ae7e68817ed239795))


### Performance Improvements

* 幻灯片列表滚动性能 ([e3a1dc5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e3a1dc518e553d3d4fad045dceb7f322afa4977f))



# [1.29.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.28.0...v1.29.0) (2021-07-14)


### Bug Fixes

* [PPTPRO-791],[PPTPRO-795]. enable edit Layout/Master sps && fix changeSize Op && fix apply layout/Master ([06fb870](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/06fb87039496a2f47267896fb8df62e27653607b))
* [PPTPRO-803] fix connector recalc after fresh ([c7e8c3a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c7e8c3a28cdc1e8182dd42abbb335654a5c46392))
* [PPTPRO-822] fix paste table recal rowInfo ([7071288](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7071288bf9c1084270665029bf26299ad40d14ff))
* add changePresentationSize api (for collaboration / maximize/fit friendly) ([672f5dd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/672f5dd19a535c82e58f6542c90df8a750e2cf84))
* circular dependency ([7e633ff](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7e633ffe7550205a96680e49bd2bf7b94b979099))
* fix apply changeSize textBody Recal ([937a65c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/937a65c029df9683beb91cb0f17954f0b7ec72c9))
* fix changeSize default behavior(theme,copy..) ([c19c208](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c19c208acec206a97d06d7b722fb256afade4c53))
* fix changeSize maximization / fit options ([aa5b890](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aa5b890606b0e9a5bf5330c3871ee39f637c0e78))
* fix dependencies relative path ([24bb58f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/24bb58f9eb37fff78109fcffed5e46f677c53e83))
* fix slide placeHolder changeSize TextFit ([35a6f30](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/35a6f3035db5259f543d58812edc826759a72866))
* PPTPRO-808][PPTPRO-809] fix change oneSide Size && recal content ([403400c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/403400cb276bc55b155ed8f5f1fdf7ccfb89b1c4))
* recal sp geometry after changeSize ([a527f8e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a527f8ea202539f278dd60165d77ba4991bc0b19))
* support random string format refid ([e27a2bf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e27a2bf15daf513c900c14b5127e19a3602a60ec))
* 切换版式，Placeholder Shape取 hierarchy 中非空第一个 ([b208637](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b208637a092a01da3b31af53a4c53828b4e4e461))
* 粘贴幻灯片 changeSize ([7314b32](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7314b32391f6e6b168d01fe9542d82d65ee29e9e))



# [1.28.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.27.1...v1.28.0) (2021-07-08)


### Bug Fixes

* adjust shape size properly when changing theme ([0667ad8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0667ad89f1647840347d533b1d022cdc56008c59))
* export SelectedObjectType ([620ebf8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/620ebf887ffecebe07eae3d253660b454ddee277))
* fix  type ([4ac830f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4ac830f8e17fcb48e828269c4353cd989d35bebb))
* fix changsize computation logic ([ae6f294](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ae6f294e4e6792524f085f2a45ae7494441acc15))
* fix lint error ([6bbc1e3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6bbc1e376c5df183337fc9d3da224fc44deb49ca))
* fix SelectedObjectType to enum ([7b47153](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7b471536b844e55f781078a5425c4e6398f0ebba))
* fix typo ([d65782c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d65782cc09b3c2ea6756168a2e0e9dbbf0f04183))
* isNotTextNoShape ([de5b31e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/de5b31e0ec7592a7f55d1c89f5d521c4599b5720))
* refine changeSize api ([4025091](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4025091f3fd58b03833f8a6313886bacac7415ef))
* refine changeSize logic ([3837fe2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3837fe2d7d04883abfc56fbfe2a1dfa1f2b66d1a))
* remove useless code ([64f7ed2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/64f7ed2ec1629bb058930304b43006cf98f322e6))
* TEXT_AUTOFIT_SHAPE 自适应计算文本框宽度 ([46a72f9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/46a72f977ac6c2325cfb4db2b239db83d58d58b4))



## [1.27.1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.27.0...v1.27.1) (2021-07-01)


### Bug Fixes

* isNotTextNoShape ([2484c9f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2484c9f102d1770ba2bf9e0fef86e138b5b46045))



# [1.27.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.26.0...v1.27.0) (2021-07-01)


### Bug Fixes

* adjust font size when slide size is changed ([5235eaa](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5235eaad35c26fcef0fc7d5154e25b0174f740a3))
* disable shape animation ([f1e7bb2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f1e7bb2504d9e85a6e2778f9802ec292f7025e19))
* enable sp resize with pres size ([589be98](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/589be980d9d5eafe6c840660f8b6d2d9c2935489))
* fix comment textExt type to NOCARE ([4346c7b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4346c7b3dafd166b8f641d74a279524b319abd66))
* fix onScroll clear overlay ([faf2920](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/faf2920e104c117da2f36b6997e661519b51ff6f))
* io apply 结束 checkTextFit；移除 apply layoutRefs 时排序 ([40112c2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/40112c2b52d84cc5467f7e7f6d8ef90a16ade6e3))
* typo ([53600e7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/53600e7b8a2f9a43625703fc2a36a72df8cc6241))
* 切换主题后检测选中元素，移除切换主题后不存在的元素 ([777abe7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/777abe7ee0b2dff9a5f65c3faea3ab25687593f9))
* 插入文本新建文本框大小 ([3684fbc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3684fbc28f4502a03dbc4ca52160422a66566c36))
* 演示模式计算鼠标位置考虑画布距离视窗位置 ([1448809](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/14488095e0e7a0d19c57b95d317ff8695b46ef94))
* 禁止复制粘贴上传 svg 图片 ([afa95c8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/afa95c82b279fd1aaa71a4b278e4393c491c3b84))
* 调整方向键移动距离 ([557fa4b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/557fa4b447a59a6b3cf15be4f223e449bc5f78fd))


### Features

* support changeSize by scaling options ([d898461](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d8984619f7a9c2ac6328c07faec72ef10ece5601))
* video ([#490](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/490)) ([29678b4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/29678b4e2ef62e32ce44ce56b18481fae135e836))



# [1.26.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.25.0...v1.26.0) (2021-06-24)


### Bug Fixes

* [PPTPRO-563] viewmode support search textSelection ([029fb20](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/029fb202637c90504486d2faee6ef3fc6788c00d))
* [PPTPRO-627] force recal note when demo end ([da73ace](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/da73ace401c251fd81dc6d18c1139d3ef6c7e4aa))
* animation in/export & add some api/events ([2adec92](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2adec926420190d899ba8126379f0a0c57f86f0c))
* animation scale ([9d5295b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9d5295b629445570002b2769dce9e2170dcc21d0))
* apply 之后 syncInterfaceState ([2e62283](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2e622837ab7607fb07579ea5031dc485d77d387f))
* apply 时检测图片是否为图表 ([d66740e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d66740ef18c270b59fdd98cc7c439a79a9bc994c))
* applyLayoutToSlide ([6eea21e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6eea21eff998bf88cc05c85c64337a5b71839018))
* applyLayoutToSlide 设置 Xfrm ([35ad9da](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/35ad9da3e2b6290c44fe40f3dba61c01c33191ca))
* changeSlideSize disable change master/layout/elements ([00cb064](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/00cb0649c78994c87579275e27a8d483f999a1a6))
* check empty data in "fromJSON" utility ([eb6f6fb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/eb6f6fbbf66d393faa14da5052d9d05bb564a7f3))
* clone 替换 createDuplicate ([a59a065](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a59a0655e7f1fef3e42c6a03e95352cd84c75928))
* do not update overlay during animation ([f6f6d63](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f6f6d638528fcbbac57d866f65add61c58be9706))
* fix animation set cursor ([2480d86](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2480d8643b91e2d8e992b0e9e199d5f7948063d4))
* fix clip during preview shape animation ([5343689](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/53436898c6b551e65e264231b652d41bf8b5a138))
* fix diappear timing &  Add anim changeSet ([41c3a86](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/41c3a86ad3a2fa5c868950819ede42bc3ec599a6))
* fix empty timing ([ab22e7d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ab22e7de1d068c12c89baffe47ba031ddabaf479))
* fix Exit Animation sent unnessary visibility attr ([332b879](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/332b8796a80dc6a81fcc3c3418c7eaf63cac4ab7))
* fix master/layouy changeSlideSizee & sync state transparent ([e12502f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e12502f4805d35679167379add705dd4d7b039b7))
* fix recal table bounds when no xfrm ([36030a8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/36030a8d876bda1d1e63c7810c5a43e03730dcb0))
* fix timeNode to ui code mangling ([c2be9d8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c2be9d8b8da83e3c73fac7761f580c26dc7be153))
* prevent applying changes during preview animation ([a499b37](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a499b374960920e13b684a8e7a1a4f60b4b741ae))
* refine animation scheduler in demostration mode ([a076f03](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a076f03009b06d572dfe36d621971e9bd999d96b))
* refine demostration syncState ([0bcb07a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0bcb07a7dd81199c3268687b75bfcfd4ed84d1a9))
* refine updateInfo computation logic ([4cc539d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4cc539dc655bc0a976c77dcd04209696eac79cdd))
* refine updateInfo to TransformInfo logic ([14dbaee](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/14dbaeefd3a592fbc7f2fa521a4ca2c44f8154c9))
* sync demostration on mouse click ([05095f4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/05095f4abb352cf5a05ab03e1e321f5eeb985747))
* 从版式复制形状；getSelectedCharts 混淆 ([d4c774e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d4c774e8d00d08f70d9a4c68a40e4fd2c01db0c6))
* 在 applyLayoutToSlide 最前面清除 emptyPlaceholder ([c38ba99](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c38ba99eedb414f1d8628d17f91abde0d3c584ef))
* 复制粘贴Slide应用Layout；上传图片加载失败； ([af29f2d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/af29f2dc7229da0544f2293addf9837cf3b1981c))
* 演示模式切换到最后一样错误 ([25851a9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/25851a9101d9c1b1cbdeec5e53fa659941b61faf))
* 演讲者视图，观众视图中图片加载完成后需要clearCache ([d00f355](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d00f355108169c672c1815c26df5133b6c5c498e))
* 禁止图表超链接点击 ([5fb1a48](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5fb1a48cf4056dd927db1f3878804d153f8e3cd1))
* 调整getSelectedCharts相对坐标 ([f2f00e7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f2f00e7022975e33f09ea18af65730e75f7dc5e6))


### Features

* add "visibility" property for update info ([cf3cece](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cf3cecee6ac6424cdf0ed4b8b720009aa05da3e1))
* add animation manager ([501907e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/501907e0f2e1939c989f7c1a3d97d0af1a228c07))
* add Animation Player ([9f68b8f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9f68b8f0b401956b3c6a2260078dbfc614a4edeb))
* add draw sp animation for preview mode & demostration mode ([157bfef](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/157bfef4ac4b92a3c4cadf9d57b855df54dc3e40))
* add playPrev & playStartFrame/playEndFrame to ShapeAnimationPlayer ([3c88aa2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3c88aa28f6bb9ae89e5a698716197324169ce615))
* add ShapeAnimationNode utils ([79b699d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/79b699d0a0af5a71568bc06afbf76e681a87a77d))
* connect transition with animation ([6f4697a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6f4697ae89a6c28249c5275b83c778d8ec2d2adb))
* refine ShapeAnimationPlayer & add some experiment code ([01969a3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/01969a3831b8b5c2dbb9409f7a65865ab8e20820))
* support animation for group shape ([f48e9bd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f48e9bd07152c6477662ded213d73b0c73d65350))
* support autoPlay for animationPlayer & some refinement ([687bbcb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/687bbcbe391f700fafb78e7dd64bc6d580c5ae7d))



# [1.25.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.24.1...v1.25.0) (2021-06-10)


### Bug Fixes

* [PPTPRO-591] fix find TableText when jumpPage ([4b06854](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4b06854fdfd0fefdece28dc0334272457bae2683))
* [PPTPRO-601] fix adding cxn / after add flip ([f2483a5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f2483a51da1beccba96e1055f903f8fe6c70bfe2))
* [PPTPRO-602] enable parallel bentConnector adjust ([e60bb2b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e60bb2b7e03ee00cb3dac6fa925b9c2376bf5423))
* fix table bound cal ([bea0e5a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bea0e5ab3063d340ac1b9f32d69957bb2768ef26))
* Layout 匹配逻辑；粘贴幻灯片应用 Layout 通过 applyLayoutToSlide ([9e84823](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9e84823f570f9a64b04250d57949eb7f376cc46e))
* master预览图尺寸；删除第一张幻灯片未更新占位符UI ([569aa0e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/569aa0e5585a36963f1e6faec106f158f5932820))
* refreshPPT重新生成预览图；master 预览图生成匹配逻辑； ([a55881e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a55881e159b3773fb43ac1548fa9dd672bc1b5da))
* thumbnail 的 Keydown 事件默认不 preventDefault ([2ae06db](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2ae06db7422a513ee6e9f65b8fe021115730da71))
* 兼容 Slide 无 Layout ([7e3c311](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7e3c311ef76182081e82c0e4f0beb1c1c3d26776))
* 切换版式，加入的占位符形状不进行匹配 ([878180c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/878180c5f4bb4559ec1742adeb54dc2d9c742af7))
* 切换版式匹配规则 ([9bed393](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9bed3937723d4f26f6a90334f76badc7292dd15f))
* 匹配Layout逻辑加入空白匹配；recalculateTable 错误 ([5543caa](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5543caa1fd6b341f16f8dafbfa96ed22e4972e93))
* 图片加载状态判断；切换主题 change 过滤；clrMap的modoc数据结构更新； ([3a69aa9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3a69aa969b5186781396a2d3240a6e38310d8b13))
* 图片占位符不显示文字；默认主题名翻译；应用主题后生成主题预览图； ([39c266a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/39c266a9e6d5d29fac3e7034344966b5ce370bc5))
* 插入占位符图片关联到 Placeholder ([5e65e1e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5e65e1e34ab563926580b3f4c104e5ffc7a5ba78))


### Features

* 图片占位符插入图片自适应占位符大小 ([b080533](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b0805335dfa4619a97f5c2b7425f539e66d2ecda))



## [1.24.1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.24.0...v1.24.1) (2021-05-19)


### Bug Fixes

* correctUniColor cheme颜色id赋值 ([f532128](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f532128274082ad855946144f84b9244787f8b0e))
* loadStorage下载字体匹配 ([8ad9a9e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8ad9a9e3afb1fabec13128632e7144f2e102d7a5))



# [1.24.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.23.0...v1.24.0) (2021-05-13)


### Bug Fixes

* [PPTPRO-562] fix  shape align with cxn ([c24d70c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c24d70c75300026e94559d9f1bc507dd0a39090b))
* compatible hoverInfo happened lost Entity ([b119344](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b11934400300dcafcf707025970064ee4c8edcc6))
* draw border properly when shape is out of slide bounds ([03b4449](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/03b4449110d723bf9bf055efae483118591ece1e))
* draw pic preview when moving a pic ([baeb9a6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/baeb9a6cee9f0a72a540a1a1737697b79588557a))
* fix Hover into textArea when adding Connector ([bc58c00](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bc58c00a575af3433059484cfbff4c1b815dd40d))
* fix hover shape handler when demostration ([ea1a276](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ea1a276a5c36dfb00e5289c9017520a6523d1a65))
* fix hover/comment  operate overlay  when demo ([5e33ea9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5e33ea961c71484d9c212e8f92d553e40c7f65fe))
* fix typo ([1962414](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1962414cd61e5db7873a6a34732b2674d00e683f))
* make animation function correctly with high refresh rate monitor & update to rescript ([08b4760](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/08b4760f0b757dd5924168363e3555d8b54241ea))
* param error ([7ad1eb1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7ad1eb1f86f03b9aceef528c4f154694517a0c12))
* remove useless code ([5e60c82](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5e60c82deacd051fae840fcbf465a985129dec8e))
* stop drawing comments during playing transition ([87751da](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/87751da3d4fba88701a16b737b76c20d5e4df097))
* viewmode draw tmpComment ([75daf6c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/75daf6c122c34a34260a2c726c881aa8ec565fea))
* 从外部复制粘贴图片/形状居中显示 ([b13c0ef](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b13c0ef26bb06a22267e5a1c6e02141577405da0))
* 复制 HTML 到备注错误 ([3be9684](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3be9684ce2ba77352fee4be0d7e09a6db3635f27))
* 复制粘贴字体获取 ([653a1b6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/653a1b63a5eaa5b070a301890f6473fb8b33a1ae))
* 复制粘贴幻灯片，重新计算zoomValue;  updateImageSrcFunc方法改为异步 ([0094664](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0094664a63ac5516eb7f14e58c997940cff8021d))
* 客户端复制粘贴图片 ([5a85304](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5a8530424951c1cb81ad03e642cb822247220fe2))
* 正确设置粘贴对象spTreeParent ([a9abd96](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a9abd961fa6f666fd004f2b1badb7bc4db655335))
* 点击 thumbnail 获取焦点 ([192dc38](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/192dc38c2fada153e72be846fa78dfde122d4526))
* 粘贴尝试匹配翻译后Layout名；正确解析HTML链接前后换行； ([0f6f969](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0f6f969c7ea56679441246340171376239ed0ca4))
* 预览切页动画，zoom取值不正确文字绘制错位 ([2bfc152](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2bfc152fde806ed38b5b44cf3e7a7027993653ea))
* 鼠标 Hover 到超链接错位 ([f9c4098](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f9c4098096686e08ec76cdf66535d608a7943574))


### Features

* change theme ([61fa653](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/61fa65304ac5d920e8062b92a61557be871720fb))
* 切换主题数据同步 ([a13c962](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a13c962bf05435ac876291831551c49140ce8899))
* 单击产生的文本框，默认不会 wrap text ([4beb044](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4beb0447b42f6aee0052387dea7f6169dbb517ea))



# [1.23.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.22.0...v1.23.0) (2021-04-21)


### Bug Fixes

* refien EmptyOpData dataType ([776b590](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/776b590aff42e76b4d24c3b515a09b330542b808))
* type err ([a95a52a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a95a52a0979e74d3c9f761cc2ac0ea47c16ca838))
* type err ([123875b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/123875b74cf1b5598068e1944ffea6b728aa1d90))
* 去除hyperlinkClickCallbackFunc；粘贴回调逻辑调整，添加sourcefileid参数； ([279394d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/279394d958a9910cf0739284a01f0067780e1e9c))
* 幻灯片列表 ⌘ + H 快捷键 ([4d572ab](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4d572ab557cae3eab3f6b75db5ed9ede45fb6478))
* 微信浏览器文字绘制 ([257999c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/257999c132e3f60a063d0c053ca9b04a9e1b6039))


### Features

* add SlideTiming dataType definition & refine esbuild:serv script ([74689c1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/74689c148ff64735f760e85a510106bd3beeed5c))
* 支持粘贴和超链接点击回调 ([4ee2bb7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4ee2bb741d1df78be2cbf22da176687e06c1fe54))



# [1.22.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.21.0...v1.22.0) (2021-04-15)


### Bug Fixes

* [PPTPRO-525] checkslide when zoom ([0688add](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0688add25cc0e762b1722de02a09f946aee29933))
* add scrollToPositon for comment ([e115cde](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e115cdea68358df4171a6e2c43e38b3c08074b9b))
* add scrollToPositon for comment ([1a60fb4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1a60fb4b506a91e2f520aa1ae9833330ca3d2732))
* allow click hyperlink in remote demostration mode ([a76e6d4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a76e6d4bf38f2a6e675a03c1215decc65aa5e962))
* fix abnorms in comments ([8ef785a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8ef785a7847526406976013a72c1eba8b182a2c2))
* fix defect in collecting changes in groups ([f925b7c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f925b7c6d3241d968f0b0b715a5817459ebe7327))
* fix renaming DrawContext to graphicDrawer ([3445b7e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3445b7eef81867f2be7dfe7958e6cc7cda061951))
* replace comment thumbnails icon ([a74ed6a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a74ed6a53b5656a971c474f769fc17de58a15949))
* Slide#draw 可能报错 ([f229c81](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f229c81a643eedc0494fc726e86a332f301ea922))
* svg patten & font drawing ([093c1d6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/093c1d67a7e77c55074a3aaf988a9f4f903f3a13))
* 图片加载失败没有refreshPPT ([775ff13](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/775ff13a548857985110eca03c5639cac1ac6878))
* 幻灯片列表排序Tack缩放适配 ([3d99d49](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3d99d49c50bc3e3cf5adba6bdd44b1b55a74ca4c))
* 幻灯片演示后退时页数事件 ([56152f6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/56152f6a31f73dd1dec969641e3eb6cc3fd42bff))
* 幻灯片缩放跟随浏览器，保持清晰 ([59f5262](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/59f5262f0ca7cc7fc854a1345814157c5888a53b))
* 旋转图标 ([3f1067e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3f1067e0beda06b1d681bcb9d5a3af841238a1e8))
* 正确绘制幻灯片阴影 ([99d5f5b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/99d5f5b5422357dccf12df7b2f27b2a945e26f04))
* 浏览器缩放 retinaPixelRatio 变化 ([ebae81e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ebae81e18b8885cd0b9c0783b778272140a71a71))


### Features

* support create change action that ignore UndoRedo ([efbea2d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/efbea2d95bdc0a04fbabd8f0e3fdc12d95fd9eef))
* 幻灯片列表Overlay适配高清缩放 ([cff5d68](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cff5d681b49ec3db814a4d2797cb19bad08b6919))



# [1.21.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.20.3...v1.21.0) (2021-04-08)


### Bug Fixes

* [PPTPRO-438] fix no oldOp when exist insert2 ([924db25](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/924db25608c3d84ee939ce79955db05b881bd7c3))
* [PPTPRO-465]showComment keep center & so on.. ([#459](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/459)) ([d86dfa5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d86dfa5f12ae2e3b287b6542d095e2c58d3cdca0))
* comment default hide ([3d6d49a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3d6d49aec6c8c0e2f6df31abd7b2c59cf1d5de1e))
* export comment axis should be interger ([31ee003](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/31ee003be5f126dd543489a4f2c4f6dd9a637014))
* fix comment deSelect ([df549fd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/df549fd937ae15a1e883d4bb563c2a89543ca56e))
* fix tepComment hover ([01b507e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/01b507e48f46902a92714eb768aca57f1529a8d0))
* fix tsc error ([f9d71dc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f9d71dc53638ee28229a56d844cd3fb184364c35))
* hover comment ([b67bfd6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b67bfd69288eebd9d07bf9fb9f588090d567f3bf))
* separate comment feature / show swtich ([d359562](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d359562c0a4830515b9c437f5a5f85b1d945c344))
* 后退时页数变化 ([cbbe095](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cbbe0954c04fda2480f9ea9953dec60bc0bfb6ba))
* 复制粘贴字体没有下载 ([a7fcc5e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a7fcc5ec7676df0c06c76c8ec389a533c9c5449f))
* 更改版式Xfrm数据更新 ([7f1f964](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7f1f9644d53718326a1890fff32426edc09ed049))
* 配置快捷键是否可用 ([03c6585](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/03c6585c607e8f90aa5b1243264b7852baac3552))



## [1.20.3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.20.1...v1.20.3) (2021-04-01)


### Bug Fixes

* allow shape 'flowChartOnlineStorage' to include textbody ([eba0b76](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/eba0b76515322e20e9d34fdefa1c7d8073ffc758))
* clear highlight during clearing formatting ([2702a2b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2702a2b711469722eafe6c318bdf6d73d3f65730))
* code confuse ([bd2180a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bd2180aa6c12a51ffadc588b58037aba2f627168))
* comment  code review && event adjust ([37c8379](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/37c8379d6a0bba12443a819032dfad7f463cf12f))
* comments sort ([abe598a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/abe598ad726198d0813559f4c6de54ef06a63a8c))
* copy 事件 target 检测是否是幻灯片的元素 ([ed0e69a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ed0e69a32b85dc32ec9f2b1bcf593d6c257b748c))
* dix apply error due to delete placeholder image ([208e974](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/208e974b433596712d9dc1bf692e867c851c480f))
* error ([f0760f0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f0760f01e704eda4f9e491dee4d04467f0202c35))
* fetch polyfill ([c3873d8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c3873d84243a7a4a3f9e121952fd75467eb62da6))
* fix getSlideComments when no slide ([cac617d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cac617d520da1fcae3f684d1f84501b6bdc25994))
* fix merge error ([629db1d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/629db1d5c61448dd99ae02d73c0274534f8fbbaa))
* fix some comment problems ([c02100a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c02100a687ccc204dff117728710b1a872987171))
* fix tsc error ([70a5869](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/70a5869f7d890966addc282280404d8fbab6ecec))
* Footer 日期显示为 NaN ([0b2edf0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0b2edf0cd4b3589d79b9cdf7cd3ca4304f9b3188))
* getParentObjects返回值 ([3766883](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3766883d30099d593b8ff6793276aab57772a085))
* main entry ([322ee60](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/322ee60861eb8f1b97ab3b3660fc3878080cf1c2))
* merge error ([b1f86cb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b1f86cbf5ec23ad3e762670e788475a30ebcf006))
* refine the logic of getFontInfo ([c12e932](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c12e932f24739e96b94fb59c409d68d3d0a0b3ff))
* remove isLongAction ([a65c3a4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a65c3a477e34ecb6863f9235dcaf3a6eaeaceefe))
* Safari 浏览器无法复制分享链接 ([ebd615e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ebd615e8c570d6bcef9bee07e091b42b7ba3520f))
* SelectComment when no slide ([6eee697](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6eee697405af164309b1ea8c9de44a11e62416db))
* setLocale duplicate ([b75399d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b75399d9820739d222ac1360f38295f1c17d5220))
* shortcut, first add comment &... ([4ab75b4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4ab75b40d373636c764c0ffeca8065b25ba77c2e))
* SrcRect toJSON ([ec166a7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ec166a74149b35b2148074bd7b4b8ecfd3437075))
* stop apply changes during long actions ([a86d5eb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a86d5eb04e3a97a2d88512d9084002ef3190c343))
* tsc error ([80d41c5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/80d41c53944867e996c7b0c5cc9478625fd53c6b))
* 删除行报错 ([b68ef2a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b68ef2ad97e8abbaee2e2e4bdaec75a95bffce3f))
* 去除 ctrl-s 保存监听 ([e4cbbd7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e4cbbd712dc7c036621aa281f48be3a00f4bb9b0))
* 在超链接两端输入的内容不加入到超链接中[PPTPRO-415] ([c62a64f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c62a64fb073912c854c50d9f44d7020a79baeb3c))
* 复制粘贴多个Slide匹配Notes顺序 ([c11b5d8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c11b5d828e30ec0d8a9cbf942f59808d0ddd0502))
* 复制粘贴幻灯片形状缩放 ([1d5a12e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1d5a12ea0a260d473201fab1831574a8bb985a75))
* 复制粘贴文本 ([2bd9736](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2bd973640b437de051599d8a7fb0336a01801d32))
* 移除 Command + Shift + E 和 Command + M 监听 ([2a9ce15](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2a9ce15ff3abfb3fe47a7886e7225119d5761030))
* 表格单元格支持缩进 ([bee66f0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bee66f09088011f6161195adcc47cd8b628d4ddf))


### Features

* load svg image ([7a965fc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7a965fcb12e93b5a6b5de84ca24f4deafaae369b))


### Reverts

* Revert "refact: rename paragraph" ([64210d6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/64210d64b10c6792a82876526355026c3c739a45))



## [1.20.1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.20.0...v1.20.1) (2021-03-19)


### Bug Fixes

* getParentObjects返回值 ([7e2210c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7e2210cabf9af35fd38b6091ca0a06993bfc7c5c))



# [1.20.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.19.3...v1.20.0) (2021-03-18)


### Bug Fixes

* allow undoredo's record to be disabled ([f86a912](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f86a9120ce1a7aece755b3109366d8aa78fc2d07))
* Tab 键缩进优化 ([232b9ec](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/232b9eceb66167a868a235f36071e347de887a9e))
* TextStyles合成层级错误，导致字体颜色显示不正确 ([685b923](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/685b92333e2a836c599ac8aae50a7a7a39e9b32e))
* 图表错误图片处理 ([3332da7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3332da7ae2bbe83078d5a91e02505e7bf74af4cc))
* 幻灯片协作UI绘制层级 ([19a1bee](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/19a1bee78ae29f8582baf0f3b36736f8f03d4dbd))
* 支持浏览器缩放 ([39dd757](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/39dd7579b49161c9911f49ef81506c418dad8fb3))
* 支持浏览器缩放 ([a1ad0e6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a1ad0e6a0aa106ea5945b31183df02919f8666f1))
* 组合图形填充 ([1a510da](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1a510dacd9cd786747b8c9f31ff914129ff63458))



## [1.19.3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.19.2...v1.19.3) (2021-03-11)


### Bug Fixes

* thumbnail#onMouseUp 触发 GoToPage 来更新Document_UpdateInterfaceState ([d92bad4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d92bad415ab116f5db23964c8c20eeafa323b806))



## [1.19.2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.19.0...v1.19.2) (2021-03-11)



# [1.19.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.18.0...v1.19.0) (2021-03-11)


### Bug Fixes

* &被过滤 ([7224533](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7224533cd77743dae41754ce3d17016859f2f704))
* Evt_onDemonstrationAction事件在真实切换时发送 ([2754a63](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2754a6382c9b84f835392ae57c837dd38e6d5727))
* fontsize scale ([9d07fc8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9d07fc8f5497191b8278fc6a30fb25dcbe0a97fc))
* transform px to mm ([c17eade](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c17eade033561f6dbdeefab10fe2278822dfb2b6))
* trigger ci ([afe2775](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/afe2775088d725cefc9599f7a89911bd5eee78a7))
* 右键点击幻灯片优化 ([be87c4e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/be87c4e58f73dd704e0ec8cf1c5c3bf0886fedff))
* 图片加载失败文本在旋转状态下绘制异常 ([1095975](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/10959755272de97f3a6180ed543951a16cdb3bb8))
* 复制粘贴匹配Layout ([0cd14f5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0cd14f56a4490d8c9afdc063240f38703756f1a0))
* 多选Slide进行右键，保持多选 ([725bb63](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/725bb63c4f11a97df73f4a796bd1ed330c274fc4))
* 未正确设置DefaultTableStyleId，导致两边表格样式不一致 ([feb3002](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/feb30027d5ac92805a8b0898bf483bcbafa2b0d9))
* 演示状态下，错误图片填充为为灰色 ([f843e88](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f843e8815f98963aea9cf6d1c1f31a6688c37052))
* 演讲者模式同步 ([7dbc0b2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7dbc0b20492b0847b507793b92a0a0a5b1c1274d))


### Features

* 图片占位符，支持失败图片重试； ([a30587d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a30587d6a2c317b10ec1bd53c4635d7149eac009))
* 多选排序 ([3e388fd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3e388fdff908d17a28d2c7b19219160f0330c68a))



# [1.18.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.17.0...v1.18.0) (2021-03-04)


### Bug Fixes

* canUnGroup返回值判断 ([8c5a120](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8c5a120ab0f5ce8429c3654ce43882cbee26f964))
* cleanup para_Numbering & para_PageNum & para_PageCount ([eb5f5e9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/eb5f5e94ce35fe0251de48841279e0661add6f15))
* demonstrationGoToSlide是否使用transition参数 ([060e242](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/060e242a43db5f339eac11691dde4bb74b631b5c))
* Evt_onCanUnGroup 值强制转换为 boolean ([9d9ca42](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9d9ca4291dfa1f4c1a60dcb2dada9aedf1cf645b))
* Evt_onDemonstrationAction事件goto页码 ([266cf76](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/266cf76d296c4311555724088458b5366f3693d2))
* fix rot flip transform ([0ea2086](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0ea208682368e0dc2a8660e2d5ba023d608432f7))
* make sure redraw notesShape while collab on notes ([4278081](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4278081be2f71fc03f690316c4f8570af786f38b))
* Numbering 符号显示 ([#390](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/390)) ([8c5dff7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8c5dff71da815ac43a29db8b512c9d0e98e32196))
* strip illegal characters ([06b3ff2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/06b3ff21c088d5a7dde0b306943ee24a3c209daf))
* 切页动画标识显示更新 ([#388](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/388)) ([714c6f4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/714c6f4a6e5f1eb8cdbeaf5300c6556bfc8a5aea))
* 演示接口上一页下一页支持停止transition ([fbedc71](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fbedc71697d793ab4b8a49f996063b0f50fc74b5))
* 演示接口上一页下一页支持停止transition ([cceac7a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cceac7a0731668e91b90f2d74d7f117a67a3df53))
* 隐藏幻灯片标识 & 切页动画标识绘制 ([81d9ec7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/81d9ec7cf7b4d8e0777265686b2e759405aa19e7))


### Features

* 隐藏幻灯片标识 & 切页动画标识 ([#387](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/387)) ([934c7a5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/934c7a561d45f99dd9e6273bf124cac2e58058e5))



# [1.17.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.16.0...v1.17.0) (2021-02-24)


### Bug Fixes

* apply时过滤非法XML字符 ([#384](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/384)) ([ac9bd00](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ac9bd000f07b4308525f20bec673d5708706e8f9))
* Duplicate形状未渲染；复制粘贴形状列表样式丢失； ([#381](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/381)) ([a943c97](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a943c974fb53aeafd331b9912f5cf9b68a70673c))
* fix cursor cannot move between text in shape ([0a0a5ee](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0a0a5ee46298b8f1f52f17cdceadb2f0450ce3e4))
* fix cxn flip ([476b19c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/476b19c5c997d5ff852152fb09ed16b812ac61b0))
* fix isPlaceholder method for CImageShape ([1a68835](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1a6883507ae64a305c18ec9a40eee096a3be0770))
* fix sometimes won't apply changes during collaboration ([2c78389](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2c78389df1ba77e123a1cb40f38346b9d2e46cfc))
* getMatchingLayout逻辑 ([#370](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/370)) ([b5cc5f6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b5cc5f65d3883a290fee5eeabbbfb978eab86b8f))
* getNvProps空错误 ([b92b0c5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b92b0c5ca225e81d1b73db86ab32c7daae967649))
* opType被吞，没有apply ([34f00cf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/34f00cfc9ff220ee445aad89bc0f376dfa1153ba))
* ParaSpace宽度计算不正确导致换行 ([#378](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/378)) ([65d2632](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/65d2632f674ab31c235d83cd5b4877c3a7509745))
* private_UpdateSelectedCellsArray根据单元格取值XY ([024d02e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/024d02e335bbfbbaed3b2392a1906dff68ce887f))
* range out of order in character class ([be47d7a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/be47d7af9e253c8eab50da22964488d7339d68e5))
* refine compactChanges logic ([2291717](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2291717d107b23fc1f4ada5b0183024b1925ab53))
* setEndPageText时更新UI ([20e4b85](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/20e4b85bee213158637bf0d44f2b78c4bc17a095))
* Tab键进行缩进内容被删除 ([b918934](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b918934eb752a477137f54c7263523faea333b9e))
* temporary fix cxn resize after disConnect ([c6efdc8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c6efdc8d4cc8e9d8043b7c7642b47cf9a39d169e))
* transition默认值 ([5c53b2b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5c53b2b60c47f1aed8e937c928ff66e6745ea514))
* trigger ci ([26c7a39](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/26c7a3926455078bcdbf32ab4d09bb979e8daf1b))
* trigger ci ([6db0887](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6db0887994cbe47e5741910ab2d133e5cda565f4))
* 光标选中单元格内时, 边框属性 ([89d157a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/89d157a27b9e0e04bf901c8d69de159957b38037))
* 协作者操作导致幻灯片页面滚动 ([8a224f9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8a224f9416772d1f1510085ffa628c506fdd0053))
* 在可滚动和不可滚动幻灯片间切换，幻灯片内容未正确更新; 幻灯片阴影 ([7c61be6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7c61be6e4b5891a8159c9788364c1651f360cc03))
* 复制粘贴 Slide 顺序错乱 ([6bfcbf2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6bfcbf243cf0c106d10cf19441c9aef9ccba66eb))
* 复制粘贴导致无法滚动 ([9211858](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9211858a27bf15fd5d00791731f028becfa2c462))
* 复制粘贴幻灯片，删除形状数据错误 ([741ebe2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/741ebe2841750c9ec3a95b2979b1e84a36af3dac))
* 复制粘贴连接线丢失 ([996e09f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/996e09fcd813d3b89704b43c1ddafc4e2500d0a4))
* 复制表格到备注错误 ([#382](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/382)) ([a7aef81](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a7aef8132178fbb3dca5157dbaddaf10f4817dce))
* 形状导出背景色不显示 ([cf0a5ee](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cf0a5ee7a106c9101d23c629e87edc4008e3fd62))
* 拖动表格边框错乱；拆分单元格 grid 最小值; ([#372](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/372)) ([b48bedf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b48bedf52712f4f2e19c466e61eff205c009172f))
* 检查是否需要 ResetSelection ([#373](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/373)) ([629cba9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/629cba926ee9a0e4e2ce675117c81552b59e1ebd))
* 组合图形创建副本未渲染 ([f87f2db](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f87f2db8cc9287c7584b64aac6bdb515e31794ee))
* 选择组合内透明图片报错 ([dac46c7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dac46c7b97412eda1f60850876d5a23b50989986))
* 隐藏对象被显示 ([#380](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/380)) ([8e1649a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8e1649a166baa2a3926beb7133f8ac74551cee6e))
* 高亮支持透明度显示 ([#366](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/366)) ([a214ae2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a214ae27c794450577b7166bc5a8541d69cfa1d1))


### Features

* 删除列逻辑，判断合并单元格相交部分，避免产生空白 ([#383](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/383)) ([ef83b8b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ef83b8bd5460cf2d1b47508f36bce7a6818a1867))
* 推出出演示模式设置；最后一页显示设置； ([#379](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/379)) ([44f9dca](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/44f9dca34f396a686f0bcd2a5ba7605e94a29230))
* 支持本地复制粘贴图片文件 ([50c85d2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/50c85d2e92d4d1c63f29e5f90d918b614a637240))
* 获取和替换notes内容 ([#369](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/369)) ([4d43f0e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4d43f0e85396d865f40a9846bc5855c4678c30cd))



# [1.16.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.15.0...v1.16.0) (2021-01-21)


### Bug Fixes

* CanSplitTableCells选中多个单元格判断；删除innerTable判断； ([#342](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/342)) ([d8395e9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d8395e9373ee2fd380eccc6a731781dadf1fb861))
* endRunProperties读取；移除applyCDocumentContent之后Correct_Content； ([39b7ab0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/39b7ab0e37793617dabf7b438dbe6da722ecdf80))
* ErrorTypes，CErrorData 混淆 ([#355](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/355)) ([c1941f2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c1941f2d1879a23456d2e11aa3580d1c3bedb57d))
* export error ([#351](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/351)) ([0852cbc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0852cbc23488ec87462b40edb009230273a85d7a))
* export ErrorTypes ([#354](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/354)) ([b3243dd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b3243dd06c103058402c823616e69406b2d68cf4))
* export ErrorTypes; remove IsSlideHidden ([#353](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/353)) ([cb111ce](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cb111cee511e3928085ba882cd3e708bc3d5b1d7))
* fix drawing for layout previews ([940f9c3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/940f9c37f8c399208f8b92433b79e6be3230ab50))
* fix scrolls drawing abnorm ([2b0f39d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2b0f39d7bbd8f3dde8bf1b434eb90b87e4bad0af))
* fix thumbnail scroll ([c5ef777](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c5ef777fb8900e361c95769441fdae81cdcf06f4))
* layout里面的表格找不到Presentation；表格提前被recalculate，未正常渲染； ([#362](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/362)) ([c2d5943](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c2d5943db79d3c0e768df875bd7728c644b84390))
* textPr.Set_FromObject ([69e4666](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/69e4666265f0334e230e7df829c862f9b5ad01da))
* trigger ci ([5554e4a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5554e4a018e1387161caa6c38f6e66636e5dfde2))
* trigger ci ([6a69113](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6a69113c794250aba82b09a8ae4972cc107a2167))
* trigger ci ([#343](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/343)) ([f10052f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f10052f1a4f086beeeb48d8c1834b59ec30c88e3))
* trigger ci ([#352](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/352)) ([59ae194](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/59ae194813860198b015353b491cdf9ab1a92d96))
* trigger ci ([#359](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/359)) ([117f0fe](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/117f0fe8c1bcc98c9a1629cdf7e33c7acfbed229))
* trigger ci ([#361](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/361)) ([4f26c12](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4f26c127aa3eb22679cdceca5f41b14ef1701610))
* 合并单元格高亮未绘制 ([09b68cd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/09b68cd3f991e9f6769eec1daf796a9d82588de7))
* 复制外部表格进来报错 ([#333](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/333)) ([aad36bd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aad36bdfd9243cd49fb536657fa1fedfc1b1c39b))
* 复制外部表格进来报错 ([#333](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/333)) ([#334](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/334)) ([2ae6eaa](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2ae6eaabe20bb044aa4056ec32159e467f95f88c))
* 复制粘贴表格错误；多出空行； ([978c5f2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/978c5f28679bf12d399295ea07fa3f13c20c528a))
* 幻灯片水平滚动 ([#356](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/356)) ([dcf8048](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/dcf8048c32efe2dd353a2706eaaed0a803b586c0))
* 支持多单元格拆分 ([#338](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/338)) ([38b7f7d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/38b7f7da396a8b5a72ef6cfefe96f0b9afe0ce11))
* 设置表格边框报错 ([a13aa44](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a13aa4463beb1dab7c330261bf8eece62c9a26b9))
* 高亮段落结尾换行，新的段落带上高亮 ([#336](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/336)) ([9c9d4d6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9c9d4d6e279fc17d1c82070a0d18690bcd2911d7))


### Features

* 表格支持内置样式 ([#358](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/358)) ([f8862d3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f8862d3f966f7c3b449abc664a430c496b32db12))


### Performance Improvements

* thumbnail滚动时不触发update ([#357](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/357)) ([02c6a10](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/02c6a1003e2c70b1ad1d7267a328aec32a6c97b5))



# [1.15.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.12.0...v1.15.0) (2021-01-07)


### Bug Fixes

*  circular dependency ([#332](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/332)) ([798da70](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/798da70cc06115fa5d5a8f112a22d5a9e34d94bf))
* border unifill check error ([88d86e7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/88d86e7d20436e545e04302a0838b355c8f4bacb))
* checkDrawingUniNvPr设置cNvPr#id ([29de448](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/29de448922c6889e46ff2e655b24fba0486a0d98))
* checkDrawingUniNvPr设置cNvPr#id ([98a0563](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/98a0563d763df7b52e41d32b87770b590e5726f3))
* Clear TextHeight Event ([8f77339](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8f773394af8f7465fc8bbc8ae652654994b431b0))
* disable undo/redo during mouse dragging ([14410d5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/14410d50b0015c86a8d347a01ac3768aa0273b5e))
* import error ([934de47](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/934de476aaab14b9597c8ff75c15ed8435049402))
* layout getParent ([63714ff](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/63714ffbea0df50d871d881082960a99d9ce0b2e))
* notesMasters ([65db7fa](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/65db7fa1955f9ac3262e4095675bd6b4d6ddf3dd))
* refine delete placeholder shape logic ([e376f31](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e376f316287094f9ca21da50aa211d6d9f4943ce))
* TextHighLight Event ([1d29ded](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1d29dedd5b490b386f1efcda6da1754dfe757fde))
* UnLockMouse变化时触发informUndoRedo ([#324](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/324)) ([567ce6e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/567ce6e9223b7ea42f9038121f5bdc006d20ef67))
* 复制文本，选中图形或文本框对象进行粘贴时，应替换掉对象中的文本[PPTPRO-133] ([#319](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/319)) ([b1911b5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b1911b53be0fc6afb27259050a102f76eb2eef6f))
* 复制粘贴多个slide，layout错乱 ([#325](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/325)) ([249a777](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/249a7779fcbfb96f660388d6b34a27c72eb3a71f))
* 尝试修复无边框时，绘制单元格背景之间留有空隙 ([#328](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/328)) ([509d9f8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/509d9f85d8e64d0d4c7448cc8a949527195f882e))
* 撤销过快，undo和redo队列的数据丢失 ([95aaf11](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/95aaf1156fe44e1e3e78e98d892640e3453f434e))
* 演示模式没有正确resize ([#326](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/326)) ([f8cb1ac](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f8cb1ace9c307d60d138d8f31f0756ad0111f9f6))
* 粘贴文本到新建形状，如果没有TextBody，需要新建 ([df01b65](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/df01b65fe3903b2ea35d1bfb5683e97eacca12c0))
* 表格没有styleId，给一个NoStyle样式 ([#323](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/323)) ([4e8894c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4e8894c53124c026e6c9f115d1467079f7d39efd))
* 设置alphaLcParenR列表数据错误 ([0b83a58](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0b83a583e00dfb0694616871b1a28ea210264c02))
* 连接图片不生效；剪切形状复制到幻灯片cNvPr#id更新； ([#321](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/321)) ([fc1e009](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fc1e0098a38cfa86a49c363cadf87f01557b10fc))
* 选中多个表格进行设置无效 ([#330](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/330)) ([f3af302](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f3af302e3c07a0f3cdb704bd21b5d075e9de0ebe))


### Features

* ⌘ + Shift + Z  = Redo ([3118b97](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3118b975c7a84825c1856a9a61e4e6cadf3568e5))
* 幻灯片区域支持方向键切换幻灯片 ([e698a71](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e698a7188369e718df74e5bf8600a705a17e1f21))



# [1.12.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.11.0...v1.12.0) (2020-12-24)


### Bug Fixes

* fix remove placeholder ([2b52c3e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2b52c3eea7dafae58c2719b8ba82fe22fcfe8c22))
* prevent sending adnormal changeset of notes removal ([ab85076](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ab850769fd10ee5a6a9d6f4798805f515c9318c1))
* remove cellspacing ([195c861](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/195c861c9ae631dd31ffde09ccf3b1676dc1f057))
* setLocale support js-JP ([887bfea](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/887bfea3d80d04a9c2f0773c5af08a4e142d93e2))
* 删除notes时不删除noteMaster数据 ([ffc2b3e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ffc2b3eb08af3510ced469ebe23bef4d62f8f04d))
* 去除CTextBorder默认值，支持表格边框各个值单独设置 ([59fde55](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/59fde55cd8dfcad62998ac444d32df1fdf3af3de))
* 复制粘贴保留fontsize ([#313](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/313)) ([16fe6e6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/16fe6e6e149ce039ee08e5fe59209c613c6a9de2))
* 导入边框显示 ([#316](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/316)) ([92714bc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/92714bc9edb48460e5213e74c1a99d9d6485a6fb))
* 导出Locales ([c3ca230](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c3ca230ed7d3da6b2166056d4902a3954efea17f))
* 按Tab键在非列表段落中增加Tab而不是缩进 ([#311](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/311)) ([80e24b7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/80e24b7c7be53ef6519bf342ea164389fe597cc1))
* 支持Emoji显示 ([#299](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/299)) ([bca4ac7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bca4ac7b28deb072500c05b044fc19b8ead4a085))
* 空fillRect fromJSON返回空 ([1d12139](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1d12139a5309754532a5a70a381d993772ad8135))



# [1.11.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.10.0...v1.11.0) (2020-12-17)


### Bug Fixes

* add instanceType for CUniColor ([e4f362f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e4f362fa9d0ba11e6399e4624fbd1972ffed5988))
* do not try to restore docState for applying changes ([68e7f5c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/68e7f5c45398798762fb2bd2b7e6a7f04db1bf2d))
* enable gotoPage without scroll thumbnail list view ([5a8b099](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5a8b0993f370c69f84d7dac864ac1b354ff2b8a0))
* fix correctly remove empty notes when remove slide ([558d701](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/558d701a7cef7bb7a5dd6272f8ae7c80d5ad859d))
* fix gotoPage in applyChange ([a090f23](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a090f2398267bdeecc5d327fbfaf686332015e5d))
* fix RGBToCSS method ([50443e2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/50443e2f17f82af157dc35e130d7a42fd763ca3c))
* fix slideComment nullable ([30b9830](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/30b98303edc2d8d3ef3d2b8c38d438289826e51d))
* image transparent ([#300](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/300)) ([b886b44](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b886b44cfab4891550688047ed3f493e7dfcdf12))
* reserve current browse state during collabration ([b59f657](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b59f657f179cb3764e08c949aee8ba3cf52385a6))
* spTreeParent ([#306](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/306)) ([3aa3e8e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3aa3e8ea0614fe104588bdb0cf4ab0f4a1ddd0f9))
* trigger ci ([d51507f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d51507f4eec244166f1e4d08b81cce744acd893d))
* 分布距离不正确 ([#303](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/303)) ([18bf6e9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/18bf6e9ad8e757a494b838003036f5941e0c1a79))
* 复制文本到表格和文本框错误 ([#305](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/305)) ([c8f0030](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c8f003021fbc540eaa61f63442aa8a736619ff4f))
* 复制粘贴到PowerPoint卡 ([#308](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/308)) ([455f7f3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/455f7f35efaf8364ee9938d9ce9f841113d7ebcc))
* 跨文件复制表格找不到style，设置默认style； ([#310](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/310)) ([4d5e114](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4d5e114fc2e82723115a469dfcd58d29b02f70cd))



# [1.10.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.9.2...v1.10.0) (2020-12-10)


### Bug Fixes

* add remove/add notes changeset during remove/add slides ([998db5c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/998db5c94da013a3958589f68df4cee80d815f90))
* add remove/add notes changeset during remove/add slides[SKIP CI] ([65aa4e7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/65aa4e770ce1d4ace333a25794f84110c44987bc))
* apply effects ([#297](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/297)) ([79e0c02](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/79e0c028d2b0d4953f6c56851d664d023bf80568))
* CNvPr复制时需设置新id ([#289](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/289)) ([8d96b5a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8d96b5a42e16760737a31a82f5cf7deb64c18053))
* export getSelectedDrawingObjectsCount ([#296](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/296)) ([6cf36fd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6cf36fdfd3227eab39f206ea302532ce1b8c8347))
* fix zoomValue for drawing tiling background ([313271f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/313271f1b528643b9b68f7f66987427ef57b7a66))
* 新建幻灯片错误 ([#290](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/290)) ([acce7ec](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/acce7ecb41ec61dbd08efa1e3cf8c37d2146156b))
* 表格报错 ([#292](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/292)) ([b5ba455](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b5ba455c69820fadc7b2da33b0b959696b0fae48))



## [1.9.2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.9.1...v1.9.2) (2020-12-02)


### Bug Fixes

* async load fonts will trigger recalculate ([388f658](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/388f6583e4b1dbbc5005720e1aace56bfc9a2f3d))
* compatLnSpc默认值 ([#283](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/283)) ([3906d5c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3906d5c377be294b0fec75cf0a641205a9ae9b31))
* redraw notes after assets loaded ([adb28a7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/adb28a780929aea9a71663160b99c0ff3b6f5d7e))
* 加载图片更新src ([#286](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/286)) ([2ef4239](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2ef423943ccf50a8fbd5f829858cda7858ec8fb3))
* 图片src通过updateImageSrcFunc处理 ([#285](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/285)) ([c021b0a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c021b0a61d482ce28a07c02594376e0c2deb507a))
* 打开notes功能 ([#284](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/284)) ([029c427](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/029c4271defe93316c2ac6b69a31aa41ac3d02ff))
* 点击幻灯片列表command + enter不新增幻灯片；字体大小超过100不能应用 ([#288](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/288)) ([25d92c2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/25d92c28a554cc08b98a64cd6202ebdec587baec))


### Features

* add setDefaultFontFamily api ([eddca99](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/eddca99169a19290b7d7b07e910718b21e6e4e3a))



## [1.9.1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.9.0...v1.9.1) (2020-11-26)



# [1.9.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.8.6...v1.9.0) (2020-11-26)


### Bug Fixes

* applyDrawingProps某些属性只apply到选中形状上；表格报错； ([#278](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/278)) ([971b551](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/971b55135bf310566374c2aed2dafdeb23e6b977))
* circular dependencies ([#272](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/272)) ([a3386a6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a3386a6ead234c72f84babaa2512662590ccb7ed))
* copypaste images cross files ([e5db4b5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e5db4b5009f254109699b69e4c2b4dce265a355b))
* fix bound_checker for images ([5d4817d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5d4817d72eaa4c00d3493d76a67260ea966df622))
* fix code mangle error ([b3c0523](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b3c0523c28f73a2a38656a9b2a108abf9af4ad49))
* fix freeze when hovering over shapes ([a10d3c8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a10d3c8f39bfae20847011b1eb8dd9bbdf17f34e))
* fix freeze when hovering over shapes ([#276](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/276)) ([c183263](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c18326311f0ad25472fdbfb92ab4d0799c4c38cb))
* fix toJSON for CBlipFill ([37c4135](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/37c4135658791ae4c8880afb61c9c12e1c16298d))
* handleTitlesAfterChangeTheme错误 ([#280](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/280)) ([e567621](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e567621e43f86f40d612b8a497ea089e55b7d4b4))
* Retina屏幕下drawConnectors错位 ([#273](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/273)) ([0d90f6f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0d90f6fb25731615465de81ff391bec11a42a399))
* tsc error ([3327040](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/33270404d02d03be988d6846f507d9f1bcaf5bbc))
* tsc error ([2f9394a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2f9394a115dc4477d2fc317aae60aa62766a8bc3))
* 中划线和演示快捷键 ([#268](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/268)) ([c6a1fbf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c6a1fbf0871323fae4bc4e36c152ed4335f188b0))
* 图片crossorigin ([#279](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/279)) ([9dcc81d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9dcc81de0cb970c733d7295478873d9e2bf948d5))
* 复制粘贴slide ([#281](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/281)) ([c17d71c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c17d71c212dc1f04914747f05e4b13a659cc1760))
* 复制粘贴文字 ([#267](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/267)) ([2669f66](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2669f6615cfc6f194c99c9ae0e54d86dba41cf17))
* 连接关系替换为cNvPr#id ([#271](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/271)) ([5cea9ce](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5cea9cec32f2413b1f08c4efe3dbb75d8286e11f))
* 选择多行进行缩进 ([#270](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/270)) ([7d2372a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7d2372a5e6fa344c16b453ba9abc2d4b2b0a31e5))
* 隐藏幻灯片评论 ([#274](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/274)) ([9982fa4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9982fa43f2e8da61da214e2079d7049870ee8d23))



## [1.8.6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.8.2...v1.8.6) (2020-11-19)


### Bug Fixes

* adjust SlideTransitionOptions ([2e6c36c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2e6c36c05d72570ba6d581454add0757de85d6d4))
* fix generate changeset for update table ([225ce3e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/225ce3e4fa50bb61006bcdb2c11a13fc69236975))
* fix toJSON for CBlipFill ([fe2e56d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fe2e56d4a6048419a62f5867e71ec6abefdaa6b5))
* fix toJSON for GeomGuide ([a51a06b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a51a06bcc1c0215bcf453d80d8c6981ae8cbc641))
* fix typo ([78934bb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/78934bb13888928f81625f74aeb45b5bf2a02d38))
* 上传幻灯片背景图片失败 ([#266](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/266)) ([4b2e754](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4b2e7544408a01cd20352bee4836c6fce33717c8))
* 复制粘贴形状记录每张幻灯片的次数 ([#263](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/263)) ([c369dc8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c369dc870e463a4a66d1088301ef5c9471d32585))
* 复制粘贴形状记录每张幻灯片的次数 ([#263](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/263)) ([#264](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/264)) ([359eeac](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/359eeac7d7722b563b5871e9f60e0ca840c2e5a9))



## [1.8.2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.8.0...v1.8.2) (2020-11-11)


### Bug Fixes

* fix circular dependency ([877ca2a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/877ca2acfd963448a58a885edfbf46dd90a267eb))
* fix generate changeset for update table ([bd8b73f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bd8b73f6b7e89f76481fb00693059cdeab25dff3))
* properly undo/redo unset slide transition ([6f4d694](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6f4d694e8f47f8941525e0f764baa904956967a7))



# [1.8.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.6.0...v1.8.0) (2020-11-09)


### Bug Fixes

* apply timing 之前 setDefault ([#256](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/256)) ([7252f7f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7252f7fee4a6afd093a2532c00f164cc02ed6a58))
* copyShapeOffsetState circular dependency ([#248](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/248)) ([6b97fb4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6b97fb4bdf142aa87ef8984153c8075a7d782b0f))
* disable CMobileTouchManager&initEventsMobile ([aebe6a4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aebe6a483e66918f92bff78ffea3da6481d3cee9))
* disable notes UI ([ea3a422](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ea3a422cd2d6d51e5205f13106886ea08fc64af6))
* export interface SlideTransitionSettings ([f1d5a2b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f1d5a2becc9d65b3f36929b13bda605cc2da92ca))
* export SlidePreviewImageInfo type ([e91bb09](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e91bb097e19589868a1328dc444b5322cf8c56f8))
* fix build error ([2ab9941](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2ab9941958d2d18a1c5044dbdcaa92b23695a288))
* fix changeset for adding slide ([5085a2f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5085a2f1e8cdf14528dbaf3906496024d1574db6))
* fix collab on notes ([de8d0fe](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/de8d0fe373a1dbd95eca0cccd41af728b24d2c8f))
* fix default "dir" value in slide transition ([5ff43d6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5ff43d603aeb686d35e135a5c30a966d1ce80697))
* fix default behavior for split transition ([073c6a2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/073c6a2b8f3cf897f3fc7484f7a25dd22b0beb87))
* fix draw background img for thumbnail ([3bf5f01](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3bf5f01d7faa298477daaffe77f2cad748aaeaa1))
* fix error on copy slide ([058d3a2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/058d3a22d6e52ba04618a5348d94f81a1e5adb53))
* fix error when using vite ([1ca299f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1ca299fd4702b220827cbf7f0a0e0caf9520bc72))
* fix mangle error ([760136f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/760136fb4caa4b6679eda365eecf5efe95abc651))
* fix mangle error ([2294432](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/22944320764861818a505f59c7fa50f898ed9d6d))
* fix mangle error ([301bc85](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/301bc859223f41616b4ce64b31272911a0b636f3))
* improve cache strategy for getSlidePreviewImages ([c44a812](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c44a81299d3742119a0b77cf04a7e8f7b2540a75))
* mobile环境不InitBrowserInputContext ([b109436](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b1094369104bd1d74c5cf61b40f19eb6eeabda26))
* new Slide时生成 refId ([#252](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/252)) ([c848723](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c84872357eb501e2d92062bb9da483a3b9c8454a))
* Retina屏幕下超链接不能点击 ([#242](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/242)) ([15bc51c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/15bc51c44cbd804b0820a2cf58e419bc30f59815))
* update default params for slide transition ([93b0bcf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/93b0bcf459af0e4608857bf5d1c8925474a88f9a))
* 幻灯片列表选中幻灯片效果 ([#249](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/249)) ([8355401](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/83554015c5a05bc6d6a541002b11f20988dd72af))
* 幻灯片列表选中幻灯片效果 ([#251](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/251)) ([b106cac](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b106cac4bd46d5262bae8d520e9d5342805ba7d6))
* 幻灯片列表阴影逻辑 ([#253](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/253)) ([a7cc27a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a7cc27a37e492dce646bbe02a73747f697530c78))
* 插入横向或竖向的直线后，按住shift按钮，拖动直线一端，直线会超出屏幕范围 [PPTPRO-326] ([#247](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/247)) ([751497d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/751497d8e6acf326b7083d976f078a99be737b9d))
* 滚动切换页之后获取的GetSelectedArray获取的幻灯片不正确 ([#254](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/254)) ([ca6acc6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ca6acc6086866a3546f264da1a46fd036de4e7e9))


### Features

* add applyCurrentTransitionToAll api ([047d4b4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/047d4b4738de88ef7c37c10e9c7505e97ca13811))
* add getSlidePreviews api ([1bd4948](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1bd49488e9de217add2597eb44caeaad5f7c4f9c))
* add SlideTransition events ([77957f2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/77957f2b4b99ded3877e685eed58b4403951eb37))
* add stopPlayingSlideTrasition api ([18ffdfc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/18ffdfc46dd16b990d87d700bd46003f14fb56d7))



# [1.6.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v1.1.0...v1.6.0) (2020-10-29)


### Bug Fixes

* [IO]refine apply notesMasters ([0b7664c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0b7664cf8fa0cb4afb37ac80fb7d1eb56191f60b))
* adjust size textInput shape according to the font size ([b399853](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b399853ed5338e4883f86d9158a349a951b29d7a))
* applyChange时新的class没有加到gTableId中 ([28ae5ea](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/28ae5ea24b7e1245e33dc595ba4943c75aeceabb))
* blipfill复制数据丢失 ([#204](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/204)) ([e427dad](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e427dadc13aebb7973b1676ee851bcf15aff46b5))
* blipfill复制粘贴数据 ([#206](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/206)) ([f4a9217](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f4a921797ed11395367eebdff0696b8c7dc9722d))
* correct placeholder drawing of layout thumbnail ([3afdc98](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3afdc98cdbfa249747cc46705406678bf9fe1dc1))
* disable notes ([872ffbf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/872ffbf8059427e79fd4a481945ae5c5a0a870d2))
* enable cache on drawSlide ([6d3e4f1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6d3e4f168bf67c8fcdbf3bfd14a57a9f766ba5f1))
* fix default value for Path instance ([99a2015](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/99a2015ac92d2d53e37bbee71c489c1d85a28dd5))
* fix draw image with transparent ([1031b08](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1031b084437c1223eb1739d44f9ae6548de624c7))
* fix error on pressing "enter" inside shape ([aae9c0a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aae9c0a375959dc53fe27eeeb907da72fd40cfb2))
* fix generate current info  from presentation ([37a0059](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/37a00593409abbbf7f01af998ed9c378581c0519))
* fix handle avLst in geometry fromJSON ([45e44f3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/45e44f3c9ec0156b43c5b0cdd73d5c1f20950aec))
* fix mangle error ([fb40cfa](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fb40cfa3268a136a3587a632788e34d3ff5ae534))
* fix reset logic for reloadDocument api ([cfbdd23](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cfbdd2343e8f9caed90d2c9bdd7413e8cd0f5cb7))
* fix toJSON for BlipFill's fillRect ([737059e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/737059e8e3d646ab5369d74c5821fdcfee344b8a))
* fix update drawing in demostration mode ([cdbacef](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cdbacefd7f748e5108a9196925e3c0c265d174db))
* handle CParaTabs.fromJSON correctly ([c2d263e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c2d263ecd7b87a29299011f23f0297be1769583c))
* handle theme update properly during editing notes ([eaaec49](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/eaaec493bc66f5713149d236583dd59cc07b3462))
* HF的fromJSON默认值为true ([0fbd3d4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0fbd3d4b96ad8e05d69ad80e53e231d28f3bf3ac))
* hf默认值 ([1335fe2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1335fe2d8dff4322d70d6b24c7f17c725995aef5))
* HtmlElementName ([#182](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/182)) ([3fd804f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3fd804fe7a9c5fce99de583dd8833967819b5116))
* hyperlink的action不支持时不清空id ([6ed94a2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6ed94a207d89c38d3dd01bae01e6bea992683426))
* import ([a3bdd81](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a3bdd819630eca3ebcf707b9e72e54694d42fed2))
* m_oThumbnailsContainer.Resize参数 ([#175](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/175)) ([3d7f729](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3d7f72960d9a7a015f0c79225aae21e7d8a701f8))
* Notes绘制时字体缩放 ([#211](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/211)) ([e448451](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e448451f3249db900cbbedf21a7bea22c463fc1d))
* notes输入框不能resize ([#213](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/213)) ([99f4e1f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/99f4e1fde791e5d6840f4f6e809fa12ac9ac3597))
* preset geom 兼容老格式 ([e182d4a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e182d4acb4061b704016321ed6d4de9b39435df6))
* redraw custGeom correctly after adjusting ([bd87997](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bd8799787e6d778aaf4645e7350faccf79e7755e))
* refine load font ([353b444](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/353b44447adb7293ff64243c6d89fcd06ab44e7d))
* reloadDocument ([578d8bf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/578d8bfc2973e4e481e2e606d8c769ffb4d85e95))
* remove bWord & Statistics ([a8e2396](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a8e2396ab3d00340f197892b68ab3990ceccf307))
* remove redundant code ([7596b78](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7596b7868efa8c0d8eb6ef848eab1838a270b7ed))
* remove redundant code ([38f36d1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/38f36d1cf1ae07056a47d8bf15b4f7940736e11f))
* State方法引用错误 ([#183](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/183)) ([b6702fc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b6702fcd5f2dabb8072284d6da9f678543012ac8))
* transition中事件方法未绑定this ([8434bbf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8434bbf8cdb8f0158f77abccfe36672f6e7528ba))
* 主题颜色更改 ([94dfb1f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/94dfb1f78983eacc3dc7ac615f023d9358399c8b))
* 分栏导入错误 ([2fb3dbf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2fb3dbfacd8747e0b865ce2fdd36899b757928d5))
* 删除形状错误 ([#186](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/186)) ([1f6f4e5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1f6f4e59363dcaf55127663863400f5aaddd7424))
* 只读模式下不绘制Numbering符号 ([#194](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/194)) ([8d8c9f6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8d8c9f66b51a676c6467f0438fdc7a2c0c9e5e5b))
* 图片地址不存在或错误时不绘制 ([5bc810d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5bc810d59a01135fd119ade4d4010f0a7a021b19))
* 图片透明度数据 ([#197](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/197)) ([b00b89a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b00b89a31a950fe6ca9bb2169cccbcd795a14654))
* 幻灯片列表滚动切换页 ([#184](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/184)) ([d832b6f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d832b6fd65e002e0c93cc9946670a49f00978813))
* 幻灯片列表滚动条高度计算 ([#176](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/176)) ([1793a69](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1793a694918b29cced629ef8d6ab311d5d558987))
* 幻灯片列表绘制 ([#174](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/174)) ([fb1cf77](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fb1cf77a347910a873d785d0b0b318024510ad4e))
* 拖动slide产生数据异常 ([#178](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/178)) ([24aef7d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/24aef7dca9eba683150d370a03ab9ae3342559de))
* 正在演示时canAcceptChange返回false ([bfd32ae](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bfd32aebab46e2b426f3f6b9c7ff181102cdca44))
* 禁用Thumbnails的resize；调整Thumbnails宽度； ([#210](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/210)) ([83cb79a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/83cb79a3595061d9835444b735ce70095d9e02a5))
* 移除初始数据hf ([e70c259](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e70c25907f265bdd204834826703ba7ac41c4709))
* 艺术字支持显示 ([#193](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/193)) ([d84fa88](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d84fa88c2e965d1b16a33146111cea1da7379cf1))
* 过滤空的notesMaster ([#214](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/214)) ([5e546d4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5e546d49780f1f0029ad618c2865a1166093516d))
* 选择多个形状进行对齐 ([8bdb3ff](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8bdb3ff3fda60d7091218f1b0bb964b7d135f1c8))
* 阻止切换页时引起下一页滚动 ([#181](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/181)) ([f405e4a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f405e4a82cd77136643e31d5fbc0c2c7b0b3543a))


### Features

* add getPPTData api ([63848c9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/63848c961c092d4433be6d9c6dabb592404be5dd))
* add playSlideTransition api ([97c57a0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/97c57a0c769523005f848141e32ad3ae50d992bd))
* add resetUndoRedo Api ([459e209](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/459e2093035bf8b7e226e87eb0c615889c132ef4))
* add support  for custGeom ([3727183](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/372718339027cffd307d4124d5e75243e3fcd618))
* enable notes ([a795c25](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a795c25614839b6364bf2227dc6c56e29c23eec2))
* enable single click to close polyline shape ([ed064f7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ed064f77840826e0a6bbe69671e2d8f611083e6c))
* UI去OO，替换主题色，形状颜色 ([#192](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/192)) ([e8b01ab](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e8b01abddad24f8a4305f72f8ad0cb0fc35cd057))
* UI去OO化 ([#190](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/190)) ([83ec97f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/83ec97f4a739ac0ab316b40380ee11b398a67c53))


### Performance Improvements

* 幻灯片列表调整缓存大小 ([#173](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/173)) ([2086477](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/20864774a0db0ac96b0c5cee55870759f2544bf8))



# [1.1.0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v0.0.12...v1.1.0) (2020-08-27)


### Bug Fixes

* cellPr.fill读取 ([#169](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/169)) ([cfa4241](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cfa42411176448d87e019e0d69426abb530214ad))
* **ci:** build ([aa80d6f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aa80d6f3f20ad83f8690553d8f06cf5a89d24f5b))
* correct font size for draw text ([a360c5b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a360c5b0165fd6e0ee59428a17e8235aa315ccfb))
* ctrl/cmd + click 直接打开链接 ([#165](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/165)) ([4918b90](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4918b90224bfce9d8ab625330d5056d0c042d71d))
* ea1JpnChsDbPeriod转换 ([#106](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/106)) ([7a48c83](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7a48c83501571db1700ac4b63306ad5559c6002c))
* enable async image loading on first load ([cd6acd7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cd6acd7c027e080233e720233cd48cb1f12e4b0b))
* enable initFullscreenchangeEvent ([7f64dfd](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7f64dfdc76f33e9ca8702bb579fae84e5e780c6f))
* esc退出全屏编辑器不能点击输入 ([#94](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/94)) ([910a76f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/910a76f9836212e37eee6184a0b5059bab7a1491))
* Evt_onMouseMove事件Hyperlink参添加Text ([#123](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/123)) ([d7ef3a5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d7ef3a5c8bac38af10d78881969d217e09c02a72))
* fontRef JSON ([7ad9194](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7ad9194d3aedd31430d61716d8fb43c35d5cff1f))
* fontRef to/fromJSON ([1d56d55](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1d56d550aea399a8ce56494c8838ab448297bc27))
* getSlideMasters判断master是否选中 ([ee87853](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ee878535d6cc1e3ce50b6a59ca88ba6100f3925a))
* handle click hyperlink correctly ([6847bf8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6847bf8776dff6294408fba5f592c6ba528eeb4f))
* layout thumnail render&detect default theme ([2607f3d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2607f3d7af60b4d7affbb871303629942a1b1e29))
* layout背景图片不显示 ([b06e50f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b06e50feafff80760452974c7a23745ebea1e766))
* make sure redraw sp element after setting spPr ([33b9717](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/33b97173e728ba9a21ed1e209e00c6e3d6a04f8d))
* mousemove 内链接显示实际链接 ([fe745db](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fe745db0e28c4ed3a6106f2a327d0ad86008d225))
* paraRun#Get_ParaContentPosByXY, paragraph#Correct_ContentPos ([f94c2c0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f94c2c05dbaa4091d513fa466d102e814d0ebf6a))
* redraw shape after xfrm is updated ([af54971](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/af54971217996eb2f380f6712c1b889714ed9f76))
* refine clrMapOverride input/output ([5a1ba1f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5a1ba1f89bcc03de1824a450f7d60fd0f31eecc4))
* refine fromJSON for  fontScheme ([bea0729](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bea07298607e69f97621293ce8a4874d7b4af3b9))
* refine table attrs.sytle ([bcd7e0b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bcd7e0b27c9ce897989f9cc73abd7733fb1abf0b))
* refine toJSON for CSrcRectData ([e429514](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e429514da563b2c096f0dba7185e05afe58b86f4))
* remove hlink info of paraRun after ParaHyperlink is removed ([e19c93b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e19c93bd1812a306a392ac29a93bc0eb848fda25))
* removeHyperlink ([0368cb3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0368cb346699e70750306bb6097cb02e31a65a7e))
* replace id & class name ([#161](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/161)) ([7b13a18](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7b13a18cfa3dfe20eea7d677ec65f7bcf45a5d31))
* tile默认值 ([885e434](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/885e4349bc79a2fa83d310291a5457cd6cade103))
* toJSON of clrMap ([0eb56e9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0eb56e9846cd4faf278af5490aae2adfa1f54e02))
* viewport背景色 ([#171](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/171)) ([103723c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/103723c921123169f0579b3ccae85ace889538fa))
* 不支持的autoNumType默认为arabicPeriod ([#155](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/155)) ([7f3b952](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7f3b95294aac6f8dc68e8bffb259c6ebc4ae1276))
* 兼容BlipFill的tile为空 ([cd01c59](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cd01c59e42586a9c6896ed15ad3b1035883eacea))
* 兼容clrMap初始化数据是数字的情况 ([279aea4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/279aea472b2d6fd162bcf8680ce43698b435ae3b))
* 内链接跳转不正确 ([3f02751](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3f0275140edc3889aae216a7b1b550161fb781ee))
* 列表头输入时不显示 ([#97](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/97)) ([b6cc067](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b6cc0673173df3340ce217c5a51e90d88295eaaf))
* 只读模式下某些情况overlay没清除；隐藏target光标； ([#164](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/164)) ([eb0a197](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/eb0a197166d3b6b5a0c74c5c267f27d4061006ed))
* 只读模式禁用onMouseDown，onMouswMove事件 ([#99](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/99)) ([171681c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/171681cb2ea2a6c8ba59cb097050afe4444b5810))
* 只读模式链接能够点击 ([aeb0686](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/aeb0686f3e62f95876e9f0a679f0fda1f268bb06))
* 图片显示异常； ([#124](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/124)) ([88bf90f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/88bf90f481a8da9712dbeebca665188ec4b3f280))
* 在链接中enter，换行不打开链接；添加toolTip ([be894d0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/be894d0498f77151c402a69556285635d52adc17))
* 垂直文本显示有问题，转成水平显示 ([b957263](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b9572639ff65f01499cb852bb5dc2085898a07fc))
* 字体重叠 ([a7f0fa1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a7f0fa114ca5bef57cbc1df539170d4e9010c9a2))
* 属性混淆；禁用Notes ([#162](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/162)) ([7a8a25a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7a8a25a495742a6ca7e29596438a30f089c64682))
* 幻灯片列表序列数字retina支持 ([#168](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/168)) ([111302a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/111302acd2a158e3208ee4dbc1c270e0b86f83d2))
* 幻灯片列表显示模糊 ([#167](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/167)) ([58fc440](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/58fc44055b02ee9e0bdef6fcb5a1eafbff04a4c8))
* 幻灯片板式缩略图绘制 ([#170](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/170)) ([709dcb7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/709dcb7040501a173e5b688c31e6ed2b9032a93a))
* 形状旋转绘制文本高亮重置ctx ([5338640](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5338640ed5c6fd2e190a6df2c5f385dbffe89739))
* 文本高亮 ([4012a18](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4012a1899d89f4531dbcc8622620a2b69dc58062))
* 无序列表的类型刷新变了 ([3627260](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/36272607fa519014b7603332aed875d35901130c))
* 暂时关闭图形的connect功能 ([#101](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/101)) ([1a44199](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1a44199742f3a2fbe96d499d5c84eb590d74daa0))
* 替换列表字符 ([#96](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/96)) ([8061d91](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8061d917f84dce5df6615d953c2b0ba8119b92d3))
* 样式加上限定；禁用Notes ([#163](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/163)) ([fd24069](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fd24069169b44835abfb0a6f84f75f68ead804d8))
* 格式刷支持段落属性；CBlipFillData的stretch属性toJSON默认true ([#103](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/103)) ([153e645](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/153e6451f40111edda956cd4af937510a2cb5ff6))
* 滚动条位置不变，拖动滚动条滚动不生效 ([97a9d5b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/97a9d5b83aef4f75295b578bf4a0ad4553c29803))
* 演示不能用键盘控制 ([53b64de](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/53b64de4c758479d93489eb3aeae4bb75fd36727))
* 演示模式下内链接跳转 ([a30e73b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a30e73b9f895443c97904163a78438cdc0523cc6))
* 演示模式下表格中的链接不能点击；从头演示页数不正确； ([#147](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/147)) ([69bca38](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/69bca3841dff5395258a6e59d7da23999835dbed))
* 演示模式不能点击超链接 ([9b80d07](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9b80d0701adc5e59ed099238c3f9d5fa1ce9abc4))
* 聚焦编辑器外后无法输入 ([#100](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/100)) ([86e27a7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/86e27a747a6a4c29da45d90b51e9d41e5054ba8a))
* 表格getDocContent支持xy ([#150](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/150)) ([f61d47e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f61d47edafb219784ddb3fc6cfeb5a4d31b36dcb))
* 表格单元格样式同步异常 & copy文本报错 ([54f199e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/54f199e4b88c10c08b1bfb68f1936506c77f83fb))
* 设置图片边框颜色增加边框宽度 ([8783663](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8783663dc074c0cb411ea62a305afa96d0eb3234))
* 过滤Slides中为null数据 ([#105](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/105)) ([bb48cf9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/bb48cf991ebeaf08f71e43b335dda995a20f66f2))
* 选中多个链接CanAddHyperlink返回false ([41d847c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/41d847ce5d8469904f0af2f806adad0076288df8))


### Performance Improvements

* init优化 ([#172](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/172)) ([1d602d3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1d602d300ec3480af204b92558217bdcc1a5abf6))


### Reverts

* 段落内容为空也绘制 Numbering ([#93](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/93)) ([b515866](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b515866c9748f204a6c7d6e65be5512d76e0bbec))



## [0.0.12](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v0.0.11...v0.0.12) (2020-07-28)


### Bug Fixes

* [import/export] fix import table ([ac4eea4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ac4eea4a0dd0e0781895365a5320961f6729a5f8))
* [import/export]support empty graphicFrame & expty image url ([6c7d2bf](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/6c7d2bf40a93e4765c748e32b23526098c8a8320))
* abnormal behavior of copypasting slide ([1c783c1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1c783c19169cb247b54ddbf01ed22df2edad0860))
* CParaPrData fromJSON error & generate modoc for copy multiple slides correctly ([802d2f5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/802d2f5f21ce64f5ec6a04101cc48494f9e3c0d9))
* CParaPrData fromJSON error & generate modoc for copy multiple slides correctly ([5aa090c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5aa090cb62a5ff02ddf72f0be46456e52714605f))
* delay handle reorder cchange operations ([3053515](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3053515f7f39849a0b4226354f9982985b64d702))
* mangle error & enable mangle debug mode ([a452779](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a452779e8f32d27b6ffe3c7d1d77789b723e32eb))
* setShapeStrokeDash ([#79](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/79)) ([66f4186](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/66f418691357fbdb49e078f28a846f67a3a55ca5))
* srcRect转换 ([#87](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/87)) ([185c96c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/185c96c3cf49b0b7993097d23f7c9d887bc4b8c8))
* uploadImagesFunc传入被混淆 ([#77](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/77)) ([e59790a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e59790ad158bf5c8ae9babce06813b3a377d9c7c))
* 复制图片如果是url需要上传 ([#84](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/84)) ([92b255a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/92b255a39c84310d941f85cf5ae022186b5934a7))
* 幻灯片列表选中多选到单选IsSelected未更新 ([#83](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/83)) ([007e8e3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/007e8e38676d31673c28b6d64982b9fe229ba380))
* 文本高亮适配 ([#75](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/75)) ([825d88f](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/825d88fedeba7124a59f20f218e75430da59cbf9))
* 旋转图形，文本高亮未跟着旋转 ([#81](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/81)) ([1236134](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/12361344c6490d3fb3e03e99a1f0c13eb962496e))
* 段落内容为空也绘制 Numbering；更新Selection颜色为石墨色； ([#91](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/91)) ([d9d6e02](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/d9d6e023dbed74424030f727f73a523f8d69eaf1))
* 表格协作报错【PPTPRO-130】；渐变色不支持转成填充色； ([#89](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/89)) ([a87a0c2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a87a0c2242f89508217b60734ea69704fb65d2de))
* 设置边框撤销重做；格式刷curosr样式； ([#86](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/86)) ([8e6a5cc](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/8e6a5ccdccd04b39cf8c4aa7e30433c2b25a588a))
* 选中overlay去掉边框；高亮颜色事件参数； ([#90](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/90)) ([2b3933c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2b3933cb01ab955d1c2eeb48502630e344cd871e))



## [0.0.11](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v0.0.10...v0.0.11) (2020-07-16)


### Bug Fixes

*  thumbnail数字渲染异常；文字设置上下标大小异常 ([#67](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/67)) ([e97f0e0](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e97f0e0394896dfcfe179934dcf9a0a4b83ba512))
* clrMap使用oo默认值 ([#72](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/72)) ([be0bcac](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/be0bcac5ed02eef32c4ab5324b6fe2162f830d1c))
* fix code mangling ([9fe762e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9fe762eb4e6b00cd46fac507afd0f57d9d11dc04))
* fix incorrect code mangling ([cdb7b26](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cdb7b260730ff0f308ab81c4f08b94dcf1118621))
* ignore unsupported operation in doc content ([3327a14](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3327a142c515705b32a3585d46e455f54d713509))
* ppr and rpr adpter and so on ([15eca9e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/15eca9e388ecf60a666a7d06ab99de481c2524cb))
* 上传的图片应fit幻灯片的高度[PPTPRO-71] ([#65](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/65)) ([f6c9ebb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f6c9ebba907f15f920cbd2064ce02e4aab0e9a17))
* 快捷键undo、redo ([#66](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/66)) ([a7ef55c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a7ef55c701d7f82a6e1f34160a9f1494839a468e))
* 适配CUniColorData ([#74](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/74)) ([7c9c2b4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/7c9c2b4cb9134ba10730cd2d3bf430b4e62ab60c))



## [0.0.10](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v0.0.9...v0.0.10) (2020-07-02)


### Bug Fixes

* [IO]apply reordering shape correctly ([3795e68](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3795e68d9bf9e348beadf992de9a4ae40243da0a))
* add translation for default layout name ([68042e2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/68042e25859daf9b2ccfb78a372a03033fb676c4))
* build ([#61](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/61)) ([fa90c15](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/fa90c1505b1bb935175413b1946463980b2630c2))
* fix change slide's bg filling mode ([b5ad2f5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b5ad2f5cd6a19d2be5bcbd13dc899bda0853ba62))
* fix error when copy slide using mouse right click ([607df7b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/607df7b08ddb161c33272b504c3904532d15624a))
* handle copy/paste shapes/slides correctly ([52231d1](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/52231d11c6a52e8dc1cac8d0ebda3e674555f697))
* 修复旋转形状时下划线绘制因单位矩阵未重置而渲染异常 ([2b61b6b](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/2b61b6b99c52276471407621b44c89dcd165adb1))
* 函数名 ([ec9e2d6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ec9e2d6d4325ab23a198616c38e3fceb2d6e3b09))
* 只选中幻灯片时Can_CopyCut返回值 ([#63](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/63)) ([4c91d8c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4c91d8cff8db8f70fba025bece375deced34d85b))
* 打包类型路径 ([#60](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/60)) ([ac19521](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/ac19521942b2b7448c1868a4c695a326475c3956))



## [0.0.9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v0.0.8...v0.0.9) (2020-06-24)


### Bug Fixes

* fix error when check placeholder of graphicFrame ([df44123](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/df441231621821a5dff1244720a1ef0bff63e6d3))



## [0.0.8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v0.0.7...v0.0.8) (2020-06-22)



## [0.0.7](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v0.0.6...v0.0.7) (2020-06-22)


### Bug Fixes

* fix error when apply delta to remove sp item ([464a39d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/464a39d9163a1db524a9abbf46dd8b7337658c92))
* 修复 slide 预览图和数字渲染异常 ([c28d7d9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c28d7d93d3492b57e08727984e31fa2717bf0090))



## [0.0.6](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v0.0.5...v0.0.6) (2020-06-19)


### Bug Fixes

* adjust the timing of sending Evt_onDocumentContentReady event ([b126275](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b1262752e666758e126102a60ade840efc9d286b))
* 更新段落属性 ([#48](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/48)) ([04e6bd9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/04e6bd9bfafe7f0b414a7a081300e6c0e31ab314))



## [0.0.5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v0.0.4...v0.0.5) (2020-06-18)


### Bug Fixes

* **ci:** 先构建再发布；版本号变化时，重置分支版本号 ([#44](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/44)) ([f62d902](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f62d90230c32cf1e51d9580795a1c58765465b1f))
* typo ([#46](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/46)) ([c6c70f2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/c6c70f2516cacc31e5aa246961271579a05e54c3))



## [0.0.4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/v0.0.3...v0.0.4) (2020-06-15)


### Bug Fixes

* **ci:** skip publish from pushing git tag ([79bdb2a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/79bdb2a6cbbb6b75cfc3b2958324472144eb287c))



## [0.0.3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/compare/e04bf82ecb30186972f188fe4ed59fc12173bdde...v0.0.3) (2020-06-12)


### Bug Fixes

* [#2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/2) 不能协同保存 ([e04bf82](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/e04bf82ecb30186972f188fe4ed59fc12173bdde))
* [#3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/3) 新建slide，控制台报错 ([b5ebc9c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b5ebc9c5f1e5d47544f026a5d330f1f7bf317f8a))
* add forgot proto methods to GroupShape ([cfb595e](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/cfb595edb6154f19189f6e2489fee6886ad9b6b7))
* any --- CShape-nvsppr toJSON/fromJSON ([221a4b3](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/221a4b36a8fd18b9c1839597be5f2ebdbf3a4649))
* avoid use global dependency before init ([00283c5](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/00283c57f271a5681d1afc61b55586072451f8f7))
* CShape-nvsppr and children toJSON/fromJSON and ts ([1d69d6a](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/1d69d6a62e05e5d6be0d795dc6b3150dacc45ac3))
* CShape-sppr and children toJSON/fromJSON and ts ([383ebcb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/383ebcb79f820fb69eefe470fea3f98e5fba6ffe))
* fix call args spread problem ([83af08c](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/83af08c542fc0c55313e8dd83798962ff251155a))
* fix reduction on object instead of array ([4877921](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/4877921b33c29018b8f97e59ce953a445393a3cf))
* fix text matrix transform for rotation ([0ebeb08](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/0ebeb08108b6e625b710910924c29436cabfeb02))
* interface name error --- CShape-nvsppr toJSON/fromJSON ([3bf08eb](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/3bf08ebc33d9de8140f54f1f2ecd2cc68989142a))
* max stacks overflow error ([953ffc9](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/953ffc912d988e8f7367e21bb77b05bd97e71d9b))
* modify readme.md [npm run build]and url error ([014d9d8](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/014d9d806ebfe7d6a96e80237d60a418faf140d1))
* presenation showPr -> presentationPr ([8626049](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/862604946499fceb09ed19e0b0a5ac31e601888f))
* typo & eqeqeq problem ([a69ccd2](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a69ccd240091dffddad1e0ac7861cd56fa1bb22f))
* version ([9219f56](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/9219f56d4cf3ad3bf554529b2c0d72dcd88a23ba))
* 修复字体相关 bug ([#36](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/issues/36)) ([b297ad4](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b297ad4f51722de310d7ae5137b0111cc47d1c35))
* 修复替换字体导致换行报错的问题 ([f659a71](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/f659a719a6a6e0213780bd65956c05861f65f1bf))
* 替换字体 metrics 导致的排版误差 ([466cfef](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/466cfeff068cd65427aa69da9e6c9de767cc270f))


### Features

* 处理 ccchange 并构建 tree，找到最小可 toJSON 的父元素 ([b250b37](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/b250b371c7ed071073833c76621970708d384487))
* 添加 cchanges collector 和协作模块基础结构 ([a5118fa](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/a5118fad7647d8cde1d2eab97bb313365ea6b143))


### Reverts

* Revert "fix: editor.toJSON() error about shape-sppr-geometry-avLst" ([5da5486](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/5da548670570dd6e7f57d08df2b6b3c06605b02e))
* Revert "fix: editor.toJSON() error about shape-sppr-geometry-avLst reduce" ([60ab33d](https://fegit.shimo.im/fe-pptx/shimo-ppt-core/commits/60ab33d80089375d76ebdcfae5da602eee7d950f))



