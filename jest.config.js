module.exports = {
  testEnvironment: "node",
  transform: {
    "^.+\\.tsx?$": "ts-jest",
    "^.+\\.js$": "babel-jest"
  },
  moduleNameMapper: {
    // '^re/(.*)$': '<rootDir>/src/re/$1',
    "^bs-platform(.*)$": "<rootDir>/re/bs-platform$1"
    // '^liber(.*)$': '<rootDir>/liber$1',
    // '^@/(.*)$': '<rootDir>/src/$1',
    // '^src/(.*)$': '<rootDir>/src/$1',
  },
  transformIgnorePatterns: ["<rootDir>/node_modules/"]
};
