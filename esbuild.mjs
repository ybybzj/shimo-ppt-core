#!/usr/bin/env node
import * as esbuild from 'esbuild'
import { PluginInlineWorker } from './builder_plugins/esbuild-inline-worker-plugin.js'
import { argv } from 'process'
// console.log(argv)
const cliArg = argv && argv.length > 2 ? argv[2] : ''

const ctx = await esbuild.context({
  entryPoints: ['./src/index.ts'],
  tsconfig: 'tsconfig.json',
  bundle: true,
  outfile: 'dist/sdk.min.js',
  sourcemap: 'inline',
  target: 'es6',
  format: 'esm',
  plugins: [
    PluginInlineWorker({
      prefix: 'inline-worker',
      minify: false,
    }),
  ],
})

if (cliArg === '--watch') {
  await ctx.watch()
} else {
  await ctx.rebuild()
  await ctx.dispose()
}
