const path = require('path')
const rollup = require('rollup')

const defaultConfig = {
  prefix: 'inline-worker',
  extensions: ['.js', '.ts'],
  plugins: [],
}

module.exports = function inlineWorkerLoaderPlugin(userConfig = null) {
  const config = Object.assign({}, defaultConfig, userConfig)

  const namespace = config.prefix
  const prefix = `${namespace}:`

  const state = {
    idMap: new Set(),
  }

  return {
    name: 'rollup-plugin-inline-worker-loader',

    resolveId(importee, importer) {
      return resolveId(state, config, prefix, importee, importer)
    },

    load(id) {
      return load(state, config, id)
    },
  }
}
function resolveId(state, config, prefix, importee, importer) {
  if (importee.startsWith(`${prefix}`)) {
    const name = importee.slice(prefix.length)
    let target = null
    if (importer) {
      const folder = path.dirname(importer)
      const paths = require.resolve.paths(importer)
      paths.push(folder)
      target = resolveModule(name, paths, config.extensions)
    } else if (path.isAbsolute(name)) {
      target = name
    }
    if (target != null) {
      state.idMap.add(target)
      console.log('worker script:', target)
    }

    return target
  }

  return null
}

function resolveModule(name, paths, extensions) {
  const testNames = [
    name,
    ...extensions.map((extension) =>
      extension.startsWith('.') ? `${name}${extension}` : `${name}.${extension}`
    ),
  ]

  for (let i = 0, n = testNames.length; i < n; ++i) {
    try {
      return require.resolve(testNames[i], { paths })
    } catch (e) {
      // empty
    }
  }

  return null
}

function load(state, config, id) {
  return new Promise((resolve, reject) => {
    if (state.idMap.has(id)) {
      const inputOptions = {
        input: id,
      }
      if (config.plugins) {
        inputOptions.plugins = config.plugins
      }

      rollup
        .rollup(inputOptions)
        .then((bundle) => {
          const bundleOptions = {
            format: 'iife',
            name: 'worker_code',
          }
          return bundle.generate(bundleOptions).then((result) => {
            resolve(handleBundleGenerated(result))
          })
        })
        .catch(reject)
    } else {
      resolve(null)
    }
  })
}

function findChunk(rollupOutput) {
  for (const chunk of rollupOutput) {
    if (!chunk.isAsset) {
      // here to support older rollup versions will be removed soon
      return chunk
    }
  }
  return null
}

function handleBundleGenerated(result) {
  const chunk = findChunk(result.output)
  if (chunk !== null) {
    const source = `/* rollup-plugin-web-worker-loader */\n${chunk.code}\n`

    return {
      code: buildWorkerCode(source),
    }
  }
  return null
}

function buildWorkerCode(source) {
  const base64 = Buffer.from(source).toString('base64')
  return `export default "data:application/javascript;base64,${base64}";`
}
