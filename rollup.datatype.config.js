const dts = require('rollup-plugin-dts')

module.exports = {
  input: './tmp/tsc-output-types/src/io/dataType/pptDoc.d.ts',
  output: {
    file: './dist/sdk-datatype.d.ts',
    format: 'es',
  },
  plugins: [dts()],
}
