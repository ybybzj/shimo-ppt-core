/* eslint-disable no-redeclare */
export interface Dict<T = any> {
  [key: string]: T
}

export interface ArrLike<T> {
  [index: number]: T
  length: number
}

export type Indexed<T> = Dict<T> | ArrLike<T>

export type Indexable = Indexed<any>

// nullable
export type Nullable<T> = T | null | undefined

export type Ref<T> = [T]
export function ref<T>(v: T): Ref<T> {
  return [v]
}
export function assignRef<T>(ref: Ref<T>, v: T): void {
  ref[0] = v
}

export function deref<T>(ref: Ref<T>): T {
  return ref[0]
}

//option
type someTag = '_$Option$_$Some$_'
export interface Some<T> {
  _tag: someTag
  kind: 'Some'
  value: T
}

export interface None {
  kind: 'None'
}

export type Option<T> = Some<T> | None

export const None: None = {
  kind: 'None',
}

export function isNone(o: any): o is None {
  return o === None
}

export function isSome<T>(o: any): o is Some<T> {
  return o && o._tag === '_$Option$_$Some$_'
}

export function Some<T>(v: T): Some<T> {
  return {
    _tag: '_$Option$_$Some$_',
    kind: 'Some',
    value: v,
  }
}

export function nullable_to_opt<T>(v: Nullable<T>): Option<T> {
  if (v == null) {
    return None
  } else {
    return Some<T>(v)
  }
}

export function getWithDefault<T>(v: Option<T>, defaultValue: T): T {
  switch (v.kind) {
    case 'Some':
      return v.value
    case 'None':
      return defaultValue
  }
}

export function getOption<T>(v: Option<T>, ref: Ref<T>): boolean {
  switch (v.kind) {
    case 'Some':
      assignRef(ref, v.value)
      return true
    default:
      return false
  }
}

// opaque type
export type Opaque<D, B> = B & { _TYPE_: D }
export type OpaqueCreator<D, B> = (v: B) => Opaque<D, B>
export function createOpaque<D, B>(v: B): Opaque<D, B> {
  return v as Opaque<D, B>
}

export type Result<T, E extends Error = Error> = T | E

export const Result = {
  Ok: (v: any) => v,
  Error: (e: Error) => e,
}

export function getResult<T>(v: Result<T>, ref: Ref<T>): boolean {
  if (v instanceof Error) {
    return false
  } else {
    assignRef(ref, v)
    return true
  }
}
export type NonUndefined<T> = Exclude<T, undefined>

//object

export type Pick<O extends object, K extends keyof O> = { [k in K]: O[k] }
export type Omit<O extends object, K extends keyof O> = Pick<
  O,
  Exclude<keyof O, K>
>

export type Optional<T extends Dict> = {
  [k in keyof T]?: T[k]
}
export type NullableOf<T extends Dict> = {
  [k in keyof T]?: Nullable<T[k]>
}

export type KeysWithValueType<T extends Dict, VT> = {
  [k in keyof T]: VT extends T[k] ? k : never
}[keyof T]

export type valuesOfDict<O extends Dict<any>> = O[keyof O]

export type ConstructorParams<T> = T extends {
  new (...args: infer U): any
}
  ? U
  : never
