import R, { Stream } from '../rstream'

function fromPromise<T>(promise: PromiseLike<T>): Stream<T> {
  const s: Stream<T> = R()

  function onVal(val: T): void {
    s(val)
    s.end(true)
  }
  promise.then(onVal)['catch'](onVal)
  return s
}
export default fromPromise
