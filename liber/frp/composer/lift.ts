import R, { Stream } from '../rstream'

function lift<T>(f: (...values: any[]) => T, streams: Stream<any>[]): Stream<T> {
  return R.combine<T>((...args): void => {
    const self = args[args.length - 2] as Stream<any>
    const ss = args.slice(0, args.length - 2) as Stream<any>[]
    const vals: any[] = []
    for (let i = 0, l = ss.length; i < l; i++) {
      const v = ss[i]()
      if (v instanceof Error) {
        self(v)
        return
      } else {
        vals.push(v)
      }
    }

    self(f(...vals))
  }, streams)
}
export default lift
