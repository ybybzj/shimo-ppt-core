import R, { Stream } from '../rstream'

export default function groupImmediate<T>(streams: Stream<any>[]): Stream<T> {
  const resultStream = R.immediate(
    R.combine<T>((...args) => {
      const main = args[0]
      const self = args[args.length - 2] as Stream<any>
      const changed = args[args.length - 1] as Stream<any>[]
      const findIndex = changed.findIndex((s) => s === main)
      const isMainStreamChanged = findIndex !== -1
      const isMainStreamHasVal = main.hasVal

      if (isMainStreamChanged && isMainStreamHasVal) {
        changed.unshift(changed.splice(findIndex, 1)[0])
        self(changed.map((s) => s()))
      }
    }, streams)
  )

  return resultStream
}
