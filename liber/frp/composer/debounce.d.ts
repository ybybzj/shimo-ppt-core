import { Stream } from '../rstream'

export default Debounce

declare function Debounce<T>(wait: number, s: Stream<T>): Stream<T>
declare function Debounce<T>(wait: number): (s: Stream<T>) => Stream<T>
