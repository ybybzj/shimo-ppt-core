import R, { Stream } from '../rstream'
import map from '../../l/map'

export default function mergeAll<R>(streams: Stream<any>[]): Stream<R> {
  const s = R.immediate(
    R.combine<R>((...args) => {
      const self = args[args.length - 2]
      const changed = args[args.length - 1]
      if (changed[0]) {
        self(changed[0]())
      } else {
        args.slice(0, args.length - 2).some((s1) => {
          if (s1.hasVal) {
            self(s1.val)
            return true
          }
          return false
        })
      }
    }, streams)
  )
  R.endsOn(
    R.combine(
      () => true,
      map((sm) => sm.end, streams)
    ),
    s
  )
  return s
}
