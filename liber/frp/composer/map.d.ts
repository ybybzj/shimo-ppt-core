import { Stream } from '../rstream'

export default Map

declare function Map<T, R>(mapFn: (o: T) => R, s: Stream<T>): Stream<R>

declare function Map<T, R>(mapFn: (o: T) => R, s: Stream<T>[]): Stream<R>[]

declare function Map<T, R>(mapFn: (o: T) => R): (s: Stream<T>) => Stream<R>
