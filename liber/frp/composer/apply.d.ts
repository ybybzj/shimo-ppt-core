import { Stream } from '../rstream'

export default Apply

declare function Apply<T, R>(f$: Stream<(a: T) => R>, s: Stream<T>): Stream<R>
