import R from '../rstream'
function apply(f$, s) {
  return R.combine(
    function(s, f$) {
      return f$()(s())
    },
    [s, f$]
  )
}
export default apply
