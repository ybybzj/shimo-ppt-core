import { Stream } from '../rstream'
export default Scan

declare function Scan<T, R>(f: (a: R, s: T) => R, init: R, s: Stream<T>): Stream<R>

declare function Scan<T, R>(f: (a: R, s: T) => R, init: R): (s: Stream<T>) => Stream<R>
