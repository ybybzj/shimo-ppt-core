import { Stream } from '../rstream'
export default Filter

declare function Filter<T>(fn: (o: T) => boolean, s: Stream<T>): Stream<T>

declare function Filter<T>(fn: (o: T) => boolean): (s: Stream<T>) => Stream<T>
