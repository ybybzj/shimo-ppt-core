import R from '../rstream'
import curry from '../../fn/curry'
import map from './map'

function flatmap(f, s) {
  return R.combine(
    function(s, self) {
      map(self, f(s()))
    },
    [s]
  )
}
export default curry(2, flatmap)
