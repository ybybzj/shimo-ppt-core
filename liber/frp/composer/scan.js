import R from '../rstream'
import curry from '../../fn/curry'

function scan(f, acc, s) {
  var ns = R.combine(
    function(s) {
      return (acc = f(acc, s()))
    },
    [s]
  )
  if (!ns.hasVal) ns(acc)
  return ns
}
export default curry(3, scan)
