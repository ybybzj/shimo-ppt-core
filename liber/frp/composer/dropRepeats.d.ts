import { Stream } from '../rstream'
export default DropRepeats

declare function DropRepeats<T>(pred: (pre: T, cur: T) => boolean, s: Stream<T>): Stream<T>

declare function DropRepeats<T>(s: Stream<T>): Stream<T>
