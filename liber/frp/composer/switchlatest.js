import R from '../rstream'
import merge from './merge'

function switchlatest(s) {
  var inner
  return R.combine(
    function(s, self) {
      inner = s()
      if (!R.isStream(inner)) {
        throw new Error(`[rstream/switchlatest]inner is not a stream! given${inner}`)
      }
      R.endsOn(
        merge([s, inner.end]),
        R.combine(
          function(inner) {
            self(inner())
          },
          [inner]
        )
      )
    },
    [s]
  )
}
export default switchlatest
