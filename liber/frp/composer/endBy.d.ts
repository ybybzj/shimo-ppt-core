import { Stream } from '../rstream'
export default EndBy

declare function EndBy<T>(fn: (o: T) => boolean, s: Stream<T>): Stream<T>

declare function EndBy<T>(fn: (o: T) => boolean): (s: Stream<T>) => Stream<T>
