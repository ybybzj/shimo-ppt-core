import curry from '../../fn/curry'
import RStream from '../rstream'

function tap(f, s) {
  return RStream.combine(
    (s, self) => {
      f(s.val, s)
      self(s.val)
    },
    [s]
  )
}
export default curry(2, tap)
