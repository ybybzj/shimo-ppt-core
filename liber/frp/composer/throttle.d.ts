import { Stream } from '../rstream'
export default Throttle

declare function Throttle<T>(wait: number, s: Stream<T>): Stream<T>

declare function Throttle<T>(wait: number): (s: Stream<T>) => Stream<T>
