import R, { Stream } from '../rstream'
import Dict from '../../adt/dict'

const _EMPTY_SLOT_ = {
  _EMPTY_SLOT_: true,
}
const _DISPOSE_SLOT_ = (s) => ({
  _DISPOSE_SLOT_: true,
  timestamp: s.lastTimestamp,
})
const groupLatest = (() => {
  const streamCache = new Dict<Stream<any>, any>()
  return function groupLatest<T>(streams: Stream<any>[]): Stream<T> {
    let streamValMap = _resetMap(streams, streamCache)
    const resultStream = R.immediate(
      R.combine<T>((...args) => {
        const self = args[args.length - 2] as Stream<any>
        const changed = args[args.length - 1] as Stream<any>[]

        changed.forEach((s) => {
          const curVal = s()
          _setVal(streamValMap, s, curVal)
        })

        const [isHappenedAll, vals] = _extractGroupedVals(streams, streamValMap)
        if (isHappenedAll) {
          self(vals)
          streamValMap = _setDispose(streams, streamValMap)
        }
      }, streams)
    )
    return resultStream
  }
})()

export default groupLatest

//helper

function _resetMap(streams: Stream<any>[], map: Dict<Stream<any>, any>): Dict<Stream<any>, any> {
  streams.forEach((s) => {
    map.set(s, _EMPTY_SLOT_)
  })
  return map
}

function _setDispose(streams: Stream<any>[], map: Dict<Stream<any>, any>): Dict<Stream<any>, any> {
  streams.forEach((s) => {
    map.set(s, _DISPOSE_SLOT_(s))
  })
  return map
}

function _setVal(map: Dict<Stream<any>, any>, stream: Stream<any>, v: any): void {
  const val = map.get(stream)

  if (isDispose(val) && stream.lastTimestamp && stream.lastTimestamp === val.timestamp) {
    map.set(stream, _DISPOSE_SLOT_(stream))
  } else {
    map.set(stream, v)
  }
}

function _extractGroupedVals(
  streams: Stream<any>[],
  map: Dict<Stream<any>, any>
): [boolean, any[] | undefined] {
  const result = new Array(streams.length)
  for (let i = 0, l = streams.length; i < l; i++) {
    const s = streams[i]
    const v = map.get(s)
    if (v === _EMPTY_SLOT_ || isDispose(v)) {
      return [false, undefined]
    }

    result[i] = v
  }

  return [true, result]
}

function isDispose(v: any): boolean {
  return v && v._DISPOSE_SLOT_
}
