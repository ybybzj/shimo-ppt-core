import { Stream } from '../rstream'
export default Flatmap

declare function Flatmap<T, R>(f: (o: T) => R, s: Stream<Stream<T>>): Stream<R>

declare function Flatmap<T, R>(f: (o: T) => R): (s: Stream<Stream<T>>) => Stream<R>
