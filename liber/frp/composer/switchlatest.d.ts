import { Stream } from '../rstream'
export default SwitchLatest

declare function SwitchLatest<T>(ss: Stream<Stream<T>>): Stream<T>
