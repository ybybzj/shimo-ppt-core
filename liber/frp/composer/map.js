import R from '../rstream'
import curry from '../../fn/curry'
import { type } from '../../type'
function map(f, s) {
  if (type.isArray(s)) {
    return s.map(function(_s) {
      return map(f, _s)
    })
  }
  return R.combine(
    function(s, self) {
      self(f(s.val))
    },
    [s]
  )
}
export default curry(2, map)
