import { Stream } from '../rstream'

export default Tap

declare function Tap<T>(f: (o: T, s: Stream<T>) => void, s: Stream<T>): Stream<T>
declare function Tap<T>(f: (o: T, s: Stream<T>) => void): (s: Stream<T>) => Stream<T>
