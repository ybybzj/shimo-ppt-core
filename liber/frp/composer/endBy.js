import R from '../rstream'
import curry from '../../fn/curry'
var filter = function(fn, s) {
  return R.combine(
    function(s, self) {
      if (fn(s())) {
        self(s.val)
        self.end(true)
      }
    },
    [s]
  )
}
export default curry(2, filter)
