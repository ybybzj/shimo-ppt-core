/* eslint-disable no-redeclare */
import { Result } from 'liber/pervasive'

/**
 *
 *                  _____ _           _    __            _
 *                 |  ___| |_   _  __| |  / _| ___  _ __| | __
 *                 | |_  | | | | |/ _` | | |_ / _ \| '__| |/ /
 *                 |  _| | | |_| | (_| | |  _| (_) | |  |   <
 *                 |_|   |_|\__, |\__,_| |_|  \___/|_|  |_|\_\
 *                          |___/
 *
 *  credits to: https://github.com/paldepind/flyd
 */

// Utility
function isFun(o: any): o is Function {
  return !!(o && o.constructor && o.call && o.apply)
}

function trueFn(): true {
  return true
}

function RStream<T>(value?: T): Stream<T> {
  return value === undefined ? CreateStream() : CreateStream(value as T)
}

// Globals
const toUpdate: InternalStream<any>[] = []
let inStream: InternalStream<any> | undefined
const order: InternalStream<any>[] = []
let orderNextIdx = -1
let flushing = false

const streamMixins = {
  on: on,
  log: log,
}

/*------------ API ---------*/
/**
 * Creates a new stream
 *
 * __Signature__: `a -> Stream a`
 *
 * @name stream
 * @param {*} initialValue - (Optional) the initial value of the stream
 * @return {stream} the stream
 *
 * @example
 * var n = stream(1); // Stream with initial value `1`
 * var s = stream(); // Stream with no initial value
 */
export interface Stream<T> {
  (): Result<T>
  (v: T): Stream<T>
  end: Stream<any>
  val?: Result<T>
  id?: string
  lastTimestamp?: number
  on<T>(handler: (v: Result<T>) => void): Stream<T>
  log<T>(flag?: string): Stream<T>
}

interface InternalStream<T> {
  (): undefined | Result<T>
  (v: T): Stream<T>
  id?: string
  end?: InternalStream<any>
  fnArgs: any[]
  listeners: InternalStream<any>[]
  queued: boolean
  val?: Result<T>
  hasVal: boolean
  lastTimestamp?: number
  vals: T[]
  depsChanged?: InternalStream<any>[]
  deps: InternalStream<any>[]
  depsMet?: boolean
  _$type$_: 'r$'
  fn?: CombineFnMany<any>
  shouldUpdate: boolean
  on<T>(handler: (v: Result<T>) => void): Stream<T>
  log<T>(flag?: string): Stream<T>
}

type CombineFn0<R> = (self?: Stream<R>) => R | void
type CombineFn1<T, R> = (
  value: Stream<T>,
  self: Stream<R>,
  changed: Stream<T>[]
) => R | void
type CombineFn2<T, T1, R> = (
  value: Stream<T>,
  t1: Stream<T1>,
  self: Stream<R>,
  changed: Stream<T1 | T>[]
) => R | void
type CombineFn3<T, T1, T2, R> = (
  value: Stream<T>,
  t1: Stream<T1>,
  t2: Stream<T2>,
  self: Stream<R>,
  changed?: Stream<T | T1 | T2>[]
) => R | void
type CombineFn4<T, T1, T2, T3, R> = (
  value: Stream<T>,
  t1: Stream<T1>,
  t2: Stream<T2>,
  t3: Stream<T3>,
  self: Stream<R>,
  changed: Stream<T | T1 | T2 | T3>
) => R | void
type CombineFn5<T, T1, T2, T3, T4, R> = (
  value: Stream<T>,
  t1: Stream<T1>,
  t2: Stream<T2>,
  t3: Stream<T3>,
  t4: Stream<T4>,
  self: Stream<R>,
  changed: Stream<T | T1 | T2 | T3 | T4>
) => R | void
type CombineFn6<T, T1, T2, T3, T4, T5, R> = (
  value: Stream<T>,
  t1: Stream<T1>,
  t2: Stream<T2>,
  t3: Stream<T3>,
  t4: Stream<T4>,
  t5: Stream<T5>,
  self: Stream<R>,
  changed: Stream<T | T1 | T2 | T3 | T4 | T5>
) => R | void
type CombineFnMany<R> = (...args: any[]) => R | void

export function CreateStream<T>(initialValue?: T): Stream<T> {
  const endStream = createDependentStream([], trueFn as CombineFn0<true>)
  const s = createStream<T>()
  s.end = endStream
  s.fnArgs = []
  endStream.listeners.push(s)
  if (arguments.length > 0) {
    s(initialValue!)
  }
  return s as unknown as Stream<T>
}

/**
 * Create a new dependent stream
 *
 * __Signature__: `(...Stream * -> Stream b -> b) -> [Stream *] -> Stream b`
 *
 * @name combine
 * @param {Function} fn - the function used to combine the streams
 * @param {Array<stream>} dependencies - the streams that this one depends on
 * @return {stream} the dependent stream
 *
 * @example
 * var n1 = stream(0);
 * var n2 = stream(0);
 * var max = combine(function(n1, n2, self, changed) {
 *   return n1() > n2() ? n1() : n2();
 * }, [n1, n2]);
 */
export function combine<T, R>(fn: CombineFn1<T, R>, ss: [Stream<T>]): Stream<R>
export function combine<T, T1, R>(
  fn: CombineFn2<T, T1, R>,
  ss: [Stream<T>, Stream<T1>]
): Stream<R>
export function combine<T, T1, T2, R>(
  fn: CombineFn3<T, T1, T2, R>,
  ss: [Stream<T>, Stream<T1>, Stream<T2>]
): Stream<R>
export function combine<T, T1, T2, T3, R>(
  fn: CombineFn4<T, T1, T2, T3, R>,
  ss: [Stream<T>, Stream<T1>, Stream<T2>, Stream<T3>]
): Stream<R>
export function combine<T, T1, T2, T3, T4, R>(
  fn: CombineFn5<T, T1, T2, T3, T4, R>,
  ss: [Stream<T>, Stream<T1>, Stream<T2>, Stream<T3>, Stream<T4>]
): Stream<R>
export function combine<T, T1, T2, T3, T4, T5, R>(
  fn: CombineFn6<T, T1, T2, T3, T4, T5, R>,
  ss: [Stream<T>, Stream<T1>, Stream<T2>, Stream<T3>, Stream<T4>, Stream<T5>]
): Stream<R>
export function combine<R>(
  fn: CombineFnMany<R>,
  streams: Stream<any>[]
): Stream<R>
export function combine<R>(
  fn: CombineFnMany<R>,
  streams: Stream<any>[]
): Stream<R> {
  const l = streams.length

  const endStream = createDependentStream([], trueFn as CombineFn0<true>)
  const deps: InternalStream<any>[] = []
  const depEndStreams: InternalStream<any>[] = []
  for (let i = 0; i < l; i++) {
    const ds = streams[i] as InternalStream<any>
    if (ds !== undefined) {
      deps.push(ds)
      if (ds.end !== undefined) {
        depEndStreams.push(ds.end)
      }
    }
  }
  const s = createDependentStream<R>(deps, fn)
  s.depsChanged = []
  s.fnArgs = (s.deps as any[]).concat([s, s.depsChanged])
  s.end = endStream
  endStream.listeners.push(s)
  addListeners(depEndStreams, endStream)
  endStream.deps = depEndStreams
  updateStream(s)
  return s as Stream<R>
}

/**
 * Returns `true` if the supplied argument is a Flyd stream and `false` otherwise.
 *
 * __Signature__: `* -> Boolean`
 *
 * @name isStream
 * @param {*} value - the value to test
 * @return {Boolean} `true` if is a Flyd streamn, `false` otherwise
 *
 * @example
 * var s = stream(1);
 * var n = 1;
 * isStream(s); //=> true
 * isStream(n); //=> false
 */

export function isStream(s: any): s is Stream<any> {
  return isFun(s) && s._$type$_ === 'r$'
}

/**
 * Invokes the body (the function to calculate the value) of a dependent stream
 *
 * By default the body of a dependent stream is only called when all the streams
 * upon which it depends has a value. `immediate` can circumvent this behaviour.
 * It immediately invokes the body of a dependent stream.
 *
 * __Signature__: `Stream a -> Stream a`
 *
 * @name immediate
 * @param {stream} stream - the dependent stream
 * @return {stream} the same stream
 *
 * @example
 * var s = stream();
 * var hasItems = immediate(combine(function(s) {
 *   return s() !== undefined && s().length > 0;
 * }, [s]);
 * console.log(hasItems()); // logs `false`. Had `immediate` not been
 *                          // used `hasItems()` would've returned `undefined`
 * s([1]);
 * console.log(hasItems()); // logs `true`.
 * s([]);
 * console.log(hasItems()); // logs `false`.
 */
export function immediate<T>(s: Stream<T>): Stream<T> {
  const _s = s as InternalStream<T>
  if (_s.depsMet === false) {
    _s.depsMet = true
    updateStream(_s)
  }

  return s
}

/**
 * Changes which `endsStream` should trigger the ending of `s`.
 *
 * __Signature__: `Stream a -> Stream b -> Stream b`
 *
 * @name endsOn
 * @param {stream} endStream - the stream to trigger the ending
 * @param {stream} stream - the stream to be ended by the endStream
 * @param {stream} the stream modified to be ended by endStream
 *
 * @example
 * var n = stream(1);
 * var killer = stream();
 * // `double` ends when `n` ends or when `killer` emits any value
 * var double = endsOn(merge(n.end, killer), combine(function(n) {
 *   return 2 * n();
 * }, [n]);
 */

export function endsOn<T, U>(endS: Stream<U>, s: Stream<T>): Stream<T> {
  const s_end = s.end as InternalStream<any>
  const _endS = endS as InternalStream<U>
  detachDeps(s_end)
  _endS.listeners.push(s_end)
  s_end.deps.push(_endS)
  return s
}

/**
 * Get a human readable view of a stream
 * @name stream.toString
 * @return {String} the stream string representation
 */
function _streamToString<T>(s: InternalStream<T>): string {
  return `stream[${s.id}]('${s.val}')`
}
// function _streamInfo<T>(s: InternalStream<T>): any {
//   return Object.keys(s).reduce((m, k) => {
//     m[k] = s[k]
//     return m
//   }, {})
// }
/**
 * Listen to stream events
 *
 * Use `on` for doing side effects in reaction to stream changes.
 * Use the returned stream only if you need to manually end it.
 *
 * __Signature__: `(a -> result) -> Stream a -> Stream undefined`
 *
 * @name on
 * @param {Function} cb - the callback
 * @param {stream} stream - the stream
 * @return {stream} an empty stream (can be ended)
 */
function on<T>(s: Stream<T>, f: (v: Result<T> | undefined) => void): Stream<T> {
  combine(
    (s) => {
      f(s.val)
    },
    [s]
  )
  return s
}

/**
 * Print log info according to stream events, including end event
 * __Signature__: `(s, Stream a) -> undefined`
 *
 * @name log
 * @param {String} msg - the prepend message
 * @param {stream} stream - the stream
 * @return {undefined}
 */
function _printVal(msg: string, val: any): void {
  if (Object(val) === val) {
    console.log(msg + '-->\n')
    console.log(val)
    console.log('<--' + msg)
  } else {
    console.log(msg + ' - ' + val)
  }
}

function log<T>(s: Stream<T>, msg?: string): Stream<T> {
  combine(() => {
    msg = msg || ''
    _printVal(`[${s.id}]${msg}`, s.val)
  }, [s])
  combine(() => {
    msg = msg ? msg + ' - ' : ''
    console.log(`[${s.id}]${msg}` + 'stream<End>')
  }, [s.end])
  return s
}

//must be called before any R creation

/*---------------------- Private ---------------------*/

/**
 * @private
 * Create a stream with no dependencies and no value
 * @return {Function} a reactive stream
 */
function createStream<T>(): InternalStream<T> {
  const mixinKeys = Object.keys(streamMixins)
  function _s(v?: T): any {
    if (arguments.length === 0) {
      return s.val
    }
    updateStreamValue(s as any, v)
    return s
  }

  const s = _s as InternalStream<T>
  s._$type$_ = 'r$'
  s.hasVal = false
  s.val = undefined
  s.vals = []
  s.listeners = []
  s.queued = false
  s.end = undefined
  s.toString = () => _streamToString(s)

  if (mixinKeys.length) {
    mixinKeys.forEach((name) => {
      s[name] = (...args) => streamMixins[name](s, ...args)
    })
  }
  return s
}

/**
 * @private
 * Create a dependent stream
 * @param {Array<stream>} dependencies - an array of the streams
 * @param {Function} fn - the function used to calculate the new stream value
 * from the dependencies
 * @return {stream} the created stream
 */
function createDependentStream<T>(
  deps: InternalStream<any>[],
  fn: CombineFnMany<T>
): InternalStream<T> {
  const s = createStream<T>()
  s.fn = fn
  s.deps = deps
  s.depsMet = false
  s.depsChanged = deps.length > 0 ? [] : undefined
  s.shouldUpdate = false
  addListeners(deps, s)
  return s
}

/**
 * @private
 * Check if all the dependencies have values
 * @param {stream} stream - the stream to check depencencies from
 * @return {Boolean} `true` if all dependencies have vales, `false` otherwise
 */

function initialDepsNotMet<T>(stream: InternalStream<T>): boolean {
  stream.depsMet = stream.deps.every((s) => {
    return s.hasVal
  })

  return !stream.depsMet
}

/**
 * @private
 * Update a dependent stream using its dependencies in an atomic way
 * @param {stream} stream - the stream to update
 */
function updateStream<T>(s: InternalStream<T>): void {
  if (
    s.fn === undefined ||
    (s.depsMet !== true && initialDepsNotMet(s)) ||
    (s.end !== undefined && s.end.val === true)
  ) {
    return
  }

  if (inStream !== undefined) {
    toUpdate.push(s)
    return
  }

  inStream = s

  if (s.depsChanged) s.fnArgs[s.fnArgs.length - 1] = s.depsChanged

  let returnVal
  try {
    returnVal = s.fn.apply(s.fn, s.fnArgs)
  } catch (err) {
    returnVal =
      err instanceof Error ? err : new Error('[rstream unknown error]:' + err)
  }

  if (returnVal !== undefined) {
    s(returnVal)
  }

  inStream = undefined
  if (s.depsChanged !== undefined) {
    s.depsChanged.length = 0
  }

  s.shouldUpdate = false

  if (flushing === false) {
    flushUpdate()
  }
}

/**
 * @private
 * Update the dependencies of a stream
 * @param {stream} stream
 */
function updateDeps<T>(s: InternalStream<T>): void {
  let i, l, o, lsr
  const listeners = s.listeners
  for (i = 0, l = listeners.length; i < l; i++) {
    lsr = listeners[i]
    if (lsr.end === s) {
      endStream(lsr)
    } else {
      if (lsr.depsChanged !== undefined && s.hasVal) {
        lsr.depsChanged.push(s)
      }
      lsr.shouldUpdate = true
      depsInQueue(lsr)
    }
  }

  for (; orderNextIdx >= 0; --orderNextIdx) {
    o = order[orderNextIdx]
    if (o.shouldUpdate === true) {
      updateStream(o)
    }
    o.queued = false
  }
}

/**
 * @private
 * Add stream dependencies to the global `order` queue.
 * @param {stream} stream
 * @see updateDeps
 */
function depsInQueue<T>(s: InternalStream<T>): void {
  let i, l
  const listeners = s.listeners

  if (s.queued === false) {
    s.queued = true
    for (i = 0, l = listeners.length; i < l; i++) {
      depsInQueue(listeners[i])
    }
    order[++orderNextIdx] = s
  }
}

/**
 * @private
 */
function flushUpdate(): void {
  flushing = true
  while (toUpdate.length > 0) {
    const s = toUpdate.shift()
    if (s!.vals.length > 0) {
      s!.val = s!.vals.shift()
    }
    updateDeps(s!)
  }
  flushing = false
}

/**
 * @private
 * Push down a value into a stream
 * @param {stream} stream
 * @param {*} value
 */
function updateStreamValue<T>(s: InternalStream<T>, n: T): void {
  s.val = n
  s.hasVal = true
  s.lastTimestamp = new Date().getTime()
  if (inStream === undefined) {
    flushing = true
    updateDeps(s)
    if (toUpdate.length > 0) {
      flushUpdate()
    } else {
      flushing = false
    }
  } else if (inStream === s) {
    markListeners(s, s.listeners)
  } else {
    s.vals.push(n)
    toUpdate.push(s)
  }
}

/**
 * @private
 */

function markListeners<T>(
  s: InternalStream<T>,
  listeners: InternalStream<any>[]
): void {
  const l = listeners.length
  for (let i = 0; i < l; i++) {
    const lsr = listeners[i]
    if (lsr.end !== s) {
      if (lsr.depsChanged !== undefined) {
        lsr.depsChanged.push(s)
      }

      lsr.shouldUpdate = true
    } else {
      endStream(lsr)
    }
  }
}

/**
 * @private
 * Add dependencies to a stream
 * @param {Array<stream>} dependencies
 * @param {stream} stream
 */

function addListeners<T>(
  deps: InternalStream<any>[],
  s: InternalStream<T>
): void {
  deps.forEach((dep) => {
    dep.listeners.push(s)
  })
}

/**
 * @private
 * Removes an stream from a dependency array
 * @param {stream} stream
 * @param {Array<stream>} dependencies
 */
function removeListener<T>(
  s: InternalStream<T>,
  listeners: InternalStream<any>[]
): void {
  const idx = listeners.indexOf(s)
  if (idx > -1) {
    listeners[idx] = listeners[listeners.length - 1]
    listeners.length -= 1
  }
}

/**
 * @private
 * Detach a stream from its dependencies
 * @param {stream} stream
 */
function detachDeps<T>(s: InternalStream<T>): void {
  s.deps.forEach((dep) => {
    removeListener(s, dep.listeners)
  })
  s.deps.length = 0
}

/**
 * @private
 * Ends a stream
 */
function endStream<T>(s: InternalStream<T>): void {
  if (s.deps !== undefined) {
    detachDeps(s)
  }

  if (s.end !== undefined) {
    detachDeps(s.end)
  }
}

RStream.CreateStream = CreateStream
RStream.combine = combine
RStream.isStream = isStream
RStream.immediate = immediate
RStream.endsOn = endsOn
export default RStream
