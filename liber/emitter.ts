import { FuncMany } from './fn/types'
import { Dict } from './pervasive'

type callback<Args extends any[] = any[]> = (...args: Args) => void

export interface Emitter<Args extends any[] = any[]> {
  on(cb: callback<Args>): void
  off(cb?: callback<Args>): void
  emit(...args: Args): void
}

export function makeEmitter<Args extends any[] = any[]>(): Emitter<Args> {
  const cbs: Array<callback<Args>> = []

  return {
    on(cb: callback<Args>): void {
      if (cbs.indexOf(cb) <= -1) {
        cbs.push(cb)
      }
    },
    off(cb?: callback<Args>): void {
      if (cb) {
        const idx = cbs.indexOf(cb)
        if (idx > -1) {
          cbs.splice(idx, 1)
        }
      } else {
        cbs.length = 0
      }
    },
    emit(...args: Args): void {
      cbs.forEach((cb) => {
        cb(...args)
      })
    },
  }
}

export interface EventEmitter {
  on(evt: string, cb: FuncMany<any>): void
  off(evt: string, cb?: FuncMany<any>): void
  emit(evt: string, ...args: any[]): void
}

export function makeEventEmitter(): EventEmitter {
  const emitters: Dict<Emitter> = {}

  return {
    on(evt: string, cb: FuncMany<any>) {
      let emitter = emitters[evt]
      if (!emitter) {
        emitter = emitters[evt] = makeEmitter()
      }
      emitter.on(cb)
    },
    off(evt: string, cb?: FuncMany<any>) {
      const emitter = emitters[evt]
      if (!emitter) {
        return
      }
      if (cb) {
        emitter.off(cb)
      } else {
        emitter.off()
      }
    },
    emit(evt: string, ...args: any[]) {
      const emitter = emitters[evt]
      if (!emitter) {
        return
      }
      emitter.emit(...args)
    },
  }
}
