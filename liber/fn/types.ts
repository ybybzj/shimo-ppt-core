export type Func0<R> = () => R
export type Func1<T1, R> = (a1: T1) => R
export type Func2<T1, T2, R> = (a1: T1, a2: T2) => R
export type Func3<T1, T2, T3, R> = (a1: T1, a2: T2, a3: T3, ...args: any[]) => R
export type FuncMany<R> = (...args: any[]) => R
