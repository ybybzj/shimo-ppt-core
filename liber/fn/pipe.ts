import { Func0, Func1, Func2, Func3 } from './types'
/**
 * Composes single-argument functions from left to right. The leftmost
 * function can take multiple arguments as it provides the signature for the
 * resulting composite function.
 *
 * @param funcs The functions to compose.
 * @returns R function obtained by composing the argument functions from left
 *   to right. For example, `pipe(f, g, h)` is identical to doing
 *   `(...args) => h(g(f(...args)))`.
 */

export function pipeFns<A, R>(f: Func1<A, R>): Func1<A, R>

export function pipeFns<A, T1, R>(
  f1: Func1<T1, A>,
  f2: (b: A) => R
): Func1<T1, R>

export function pipeFns<A, B, T1, R>(
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (b: B) => R
): Func1<T1, R>

export function pipeFns<A, B, C, T1, R>(
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (b: C) => R
): Func1<T1, R>

export function pipeFns<A, B, C, D, T1, R>(
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (a: C) => D,
  f5: (b: D) => R
): Func1<T1, R>

export function pipeFns<A, B, C, D, E, T1, R>(
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (a: C) => D,
  f5: (b: D) => E,
  f6: (b: E) => R
): Func1<T1, R>

export function pipeFns<A, B, C, D, E, F, T1, R>(
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (a: C) => D,
  f5: (b: D) => E,
  f6: (b: E) => F,
  f7: (b: F) => R
): Func1<T1, R>

export function pipeFns<A, B, C, D, E, F, G, T1, R>(
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (a: C) => D,
  f5: (b: D) => E,
  f6: (b: E) => F,
  f7: (b: F) => G,
  f8: (b: G) => R
): Func1<T1, R>

export function pipeFns<A, B, C, D, E, F, G, H, T1, R>(
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (a: C) => D,
  f5: (b: D) => E,
  f6: (b: E) => F,
  f7: (b: F) => G,
  f8: (b: G) => H,
  f9: (b: H) => R
): Func1<T1, R>

export function pipeFns<A, B, C, D, E, F, G, H, I, T1, R>(
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (a: C) => D,
  f5: (b: D) => E,
  f6: (b: E) => F,
  f7: (b: F) => G,
  f8: (b: G) => H,
  f9: (b: H) => I,
  f10: (b: I) => R
): Func1<T1, R>

export function pipeFns<R>(...fns: Function[]): (...args: any[]) => R {
  return fns.reduce((f, g) => (a) => g(f(a))) as (...args: any[]) => R
}

export function pipe<A, R>(input: A, f: Func1<A, R>): R

export function pipe<A, T1, R>(input: T1, f1: Func1<T1, A>, f2: (b: A) => R): R

export function pipe<A, B, T1, R>(
  input: T1,
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (b: B) => R
): R

export function pipe<A, B, C, T1, R>(
  input: T1,
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (b: C) => R
): R

export function pipe<A, B, C, D, T1, R>(
  input: T1,
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (a: C) => D,
  f5: (b: D) => R
): R

export function pipe<A, B, C, D, E, T1, R>(
  input: T1,
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (a: C) => D,
  f5: (b: D) => E,
  f6: (b: E) => R
): R

export function pipe<A, B, C, D, E, F, T1, R>(
  input: T1,
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (a: C) => D,
  f5: (b: D) => E,
  f6: (b: E) => F,
  f7: (b: F) => R
): R

export function pipe<A, B, C, D, E, F, G, T1, R>(
  input: T1,
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (a: C) => D,
  f5: (b: D) => E,
  f6: (b: E) => F,
  f7: (b: F) => G,
  f8: (b: G) => R
): R

export function pipe<A, B, C, D, E, F, G, H, T1, R>(
  input: T1,
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (a: C) => D,
  f5: (b: D) => E,
  f6: (b: E) => F,
  f7: (b: F) => G,
  f8: (b: G) => H,
  f9: (b: H) => R
): R

export function pipe<A, B, C, D, E, F, G, H, I, T1, R>(
  input: T1,
  f1: Func1<T1, A>,
  f2: (a: A) => B,
  f3: (a: B) => C,
  f4: (a: C) => D,
  f5: (b: D) => E,
  f6: (b: E) => F,
  f7: (b: F) => G,
  f8: (b: G) => H,
  f9: (b: H) => I,
  f10: (b: I) => R
): R
export function pipe<I, R>(input: I, ...fns: Func1<any, any>[]): R {
  let result = input
  for (const fn of fns) {
    result = fn(result)
  }
  return (result as unknown) as R
}
