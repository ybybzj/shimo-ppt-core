import { Func0, Func1, Func2, Func3 } from './types'

/**
 * Composes single-argument functions from left to right. The leftmost
 * function can take multiple arguments as it provides the signature for the
 * resulting composite function.
 *
 * @param funcs The functions to compose.
 * @returns R function obtained by composing the argument functions from left
 *   to right. For example, `pipe(f, g, h)` is identical to doing
 *   `(...args) => h(g(f(...args)))`.
 */

export default function compose(): <R>(a: R) => R

export default function compose<F extends Function>(f: F): F

/* two functions */
export default function compose<A, R>(f1: (b: A) => R, f2: Func0<A>): Func0<R>

export default function compose<A, T1, R>(f1: (b: A) => R, f2: Func1<T1, A>): Func1<T1, R>

export default function compose<A, T1, T2, R>(
  f1: (b: A) => R,
  f2: Func2<T1, T2, A>
): Func2<T1, T2, R>

export default function compose<A, T1, T2, T3, R>(
  f1: (b: A) => R,
  f2: Func3<T1, T2, T3, A>
): Func3<T1, T2, T3, R>

/* three functions */
export default function compose<A, B, R>(f1: (b: B) => R, f2: (a: A) => B, f3: Func0<A>): Func0<R>

export default function compose<A, B, T1, R>(
  f1: (a: B) => R,
  f2: (b: A) => B,
  f3: Func1<T1, A>
): Func1<T1, R>

export default function compose<A, B, T1, T2, R>(
  f1: (b: B) => R,
  f2: (a: A) => B,
  f3: Func2<T1, T2, A>
): Func2<T1, T2, R>

export default function compose<A, B, T1, T2, T3, R>(
  f1: (b: B) => R,
  f2: (a: A) => B,
  f3: Func3<T1, T2, T3, A>
): Func3<T1, T2, T3, R>

/* four functions */
export default function compose<A, B, C, R>(
  f1: (b: C) => R,
  f2: (a: B) => C,
  f3: (a: A) => B,
  f4: Func0<A>
): Func0<R>

export default function compose<A, B, C, T1, R>(
  f1: (b: C) => R,
  f2: (a: B) => C,
  f3: (a: A) => B,
  f4: Func1<T1, A>
): Func1<T1, R>

export default function compose<A, B, C, T1, T2, R>(
  f1: (b: C) => R,
  f2: (a: B) => C,
  f3: (a: A) => B,
  f4: Func2<T1, T2, A>
): Func2<T1, T2, R>

export default function compose<A, B, C, T1, T2, T3, R>(
  f1: (b: C) => R,
  f2: (a: B) => C,
  f3: (a: A) => B,
  f4: Func3<T1, T2, T3, A>
): Func3<T1, T2, T3, R>

/* five functions */
export default function compose<A, B, C, D, R>(
  f1: (b: D) => R,
  f2: (a: C) => D,
  f3: (a: B) => C,
  f4: (a: A) => B,
  f5: Func0<A>
): Func0<R>

export default function compose<A, B, C, D, T1, R>(
  f1: (b: D) => R,
  f2: (a: C) => D,
  f3: (a: B) => C,
  f4: (a: A) => B,
  f5: Func1<T1, A>
): Func1<T1, R>

export default function compose<A, B, C, D, T1, T2, R>(
  f1: (b: D) => R,
  f2: (a: C) => D,
  f3: (a: B) => C,
  f4: (a: A) => B,
  f5: Func2<T1, T2, A>
): Func2<T1, T2, R>

export default function compose<A, B, C, D, T1, T2, T3, R>(
  f1: (b: D) => R,
  f2: (a: C) => D,
  f3: (a: B) => C,
  f4: (a: A) => B,
  f5: Func3<T1, T2, T3, A>
): Func3<T1, T2, T3, R>

export default function compose<R>(...fns: Function[]): (...args: any[]) => R
export default function compose<R>(...fns: Function[]): (...args: any[]) => R {
  return fns.reduce((f, g) => (a) => f(g(a))) as (...args: any[]) => R
}
