import clone from '../l/clone'
export default function tap(fn) {
  return function(input) {
    fn(clone(input))
    return input
  }
}
