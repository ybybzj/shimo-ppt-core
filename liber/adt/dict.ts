export default class KVMap<K, V> {
  _index: number
  _keys: K[]
  _values: V[]
  constructor() {
    this._index = -1
    this._keys = []
    this._values = []
  }
  has(key: K): boolean {
    const list = this._keys
    let i
    if (isNaNOrZero(key)) {
      //NaN or 0
      for (i = list.length; i--; ) {
        if (is(list[i], key)) {
          break
        }
      }
    } else {
      i = list.indexOf(key)
    }
    //update index
    this._index = i
    return i > -1
  }
  clear(): void {
    this._keys.length = 0
    this._values.length = 0
    this._index = -1
  }
  set(key: K, value: V): this {
    if (this.has(key)) {
      this._values[this._index] = value
    } else {
      this._values[this._keys.push(key) - 1] = value
    }
    return this
  }
  get(key: K, defaultValue?: V): V | undefined {
    if (this.has(key)) {
      return this._values[this._index]
    } else {
      if (defaultValue !== undefined) {
        this.set(key, defaultValue)
      }
      return defaultValue
    }
  }
  remove(key: K): boolean {
    let i
    if (this.has(key)) {
      i = this._index
      this._keys.splice(i, 1)
      this._values.splice(i, 1)
    }
    return i > -1
  }
  each(fn: (v: V, k: K) => void): void {
    if (typeof fn !== 'function') {
      return
    }
    let i = 0
    const l = this._keys.length
    for (; i < l; i++) {
      fn(this._values[i], this._keys[i])
    }
  }
  length(): number {
    return this._keys.length
  }
  values(): V[] {
    return this._values
  }
}

//detect NaN/0 equality
function isNaNOrZero(a: any): boolean {
  return a !== a || a === 0
}
function is(a: any, b: any): boolean {
  return isNaN(a) ? isNaN(b) : a === b
}
