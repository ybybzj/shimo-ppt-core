function hasOwn(o: object, k: string): boolean {
  return Object.prototype.hasOwnProperty.call(o, k)
}

export function extend(...args: object[]): object {
  const l = args.length
  let i = 0
  let target
  while (i < l) {
    target = args[i]
    if (target === Object(target)) {
      break
    }
    i++
  }

  if (i === l) {
    return {}
  }

  i++
  while (i < l) {
    const o = args[i++]
    if (o !== Object(o)) {
      continue
    }
    for (const k in o) {
      if (hasOwn(o, k)) {
        target[k] = o[k]
      }
    }
  }
  return target
}
