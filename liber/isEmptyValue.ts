import { type as t } from './type'

export function isEmptyValue(o: any): boolean {
  switch (t(o)) {
    case 'undefined':
    case 'null':
    case 'NaN':
      return true
    case 'array':
    case 'string':
      return o.length === 0
    case 'object':
      return Object.keys(o).length === 0
    default:
      return false
  }
}
