import { Dict } from './pervasive'

const typeReg = /^\[object (\w+)\]$/
type jsType =
  | 'null'
  | 'undefined'
  | 'NaN'
  | 'string'
  | 'number'
  | 'function'
  | 'array'
  | 'object'
  | 'regexp'
  | 'arguments'
  | 'boolean'
  | 'unknown'
type typeAssert<T> = (o: any) => o is T
interface TypeTester {
  (o: any): jsType
  isString: typeAssert<string>
  isFunction: typeAssert<Function>
  isArray: typeAssert<any[]>
  isUndefined: typeAssert<undefined>
  isNull: typeAssert<null>
  isObject: typeAssert<Dict>
  isRegExp: typeAssert<RegExp>
  isArguments: typeAssert<any[]>
  isBoolean: typeAssert<boolean>
  isNaN: (o: any) => boolean
  isNumber: typeAssert<number>
  isAny: typeAssert<true>
  [key: string]: typeAssert<any> | ((o: any) => boolean)
}

export const type = function type(o: any): jsType {
  if (o === null) {
    return 'null'
  }
  if (o === undefined) {
    return 'undefined'
  }
  if (o !== o) {
    return 'NaN'
  }
  /* jshint eqnull: true */
  const tm = Object.prototype.toString.call(o).match(typeReg)
  return tm == null ? 'unknown' : tm[1].toLowerCase()
} as TypeTester

type.isString = (o): o is string => {
  return type(o) === 'string'
}

type.isFunction = (o): o is Function => {
  return type(o) === 'function'
}

type.isArray = (o): o is any[] => {
  return Array.isArray(o)
}

type.isUndefined = (o): o is undefined => {
  return o === undefined
}

type.isNull = (o): o is null => {
  return o === null
}

type.isObject = (o): o is Dict => {
  return type(o) === 'object'
}

type.isRegExp = (o): o is RegExp => {
  return type(o) === 'regexp'
}

type.isArguments = (o): o is any[] => {
  return type(o) === 'arguments'
}

type.isBoolean = (o): o is boolean => {
  return type(o) === 'boolean'
}

type.isNaN = (o): boolean => {
  return type(o) === 'NaN'
}

type.isNumber = (o): o is number => {
  return type(o) === 'number' && !isNaN(o)
}

type.isAny = (_o): _o is true => {
  return true
}
