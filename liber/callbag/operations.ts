import { noop } from '../fn/const'
import { SinkTalkback, Source, SourceTalkback } from './type'

export function filter<T>(
  cond: (a: T) => boolean
): (source: Source<T>) => Source<T> {
  return (source) => (sink) => {
    source({
      ...sink,
      DATA(data) {
        if (cond(data)) {
          sink.DATA(data)
        }
      },
    })
  }
}

export function map<A, B>(
  mapper: (a: A) => B
): (source: Source<A>) => Source<B> {
  return (source) => (sink) => {
    source({
      ...sink,
      DATA(data) {
        sink.DATA(mapper(data))
      },
    })
  }
}

export const concat = (sources: Source<any>[]) => (sink: SinkTalkback<any>) => {
  const n = sources.length
  if (n === 0) {
    sink.START({
      REQUEST: noop,
      END: noop,
    })
    sink.END()
    return
  }
  let i = 0
  let sourceTalkback: SourceTalkback<any>
  let isPulled = false
  const talkback = {
    REQUEST() {
      isPulled = true
      sourceTalkback.REQUEST()
    },
    END() {
      sourceTalkback.END()
    },
  }
  ;(function next() {
    if (i === n) {
      sink.END()
      return
    }
    sources[i]({
      START(d) {
        sourceTalkback = d
        if (i === 0) {
          sink.START(talkback)
        } else if (isPulled === true) sourceTalkback.REQUEST()
      },
      DATA(data) {
        sink.DATA(data)
      },
      END(err?) {
        if (err) {
          sink.END(err)
        } else {
          i += 1
          next()
        }
      },
    })
  })()
}
