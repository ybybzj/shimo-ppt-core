import { makeEmitter } from '../emitter'
import { fromEmitter } from './from'
import { Source } from './type'

export function makePushSource<T>(): {
  source: Source<T>
  emitter: (a: T) => void
} {
  const _emitter = makeEmitter<[T]>()
  const source = fromEmitter(_emitter)
  return {
    source,
    emitter: (a: T) => _emitter.emit(a),
  }
}
