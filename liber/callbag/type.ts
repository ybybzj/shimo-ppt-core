export interface SourceTalkback<Err = any> {
  REQUEST(): void
  END(err?: Err): void
}

export interface SinkTalkback<T, Err = any> {
  START(talkback: SourceTalkback<Err>): void
  DATA(data: T): void
  END: (err?: Err) => void
}

export type Source<T, Err = any> = (sink: SinkTalkback<T, Err>) => void

export type SinkConnector<A, B> = (source: Source<A>) => Source<B> | void

export type SourceFactory<T, Err = any> = <Args extends any[] = any[]>(
  ...args: Args
) => Source<T, Err>

export type Operator<A, B> = <Args extends any[] = any[]>(
  ...args: Args
) => SinkConnector<A, B>

export type SourceValue<S extends Source<any>> = S extends Source<infer U>
  ? U
  : never
