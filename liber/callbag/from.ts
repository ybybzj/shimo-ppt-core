import { Emitter } from '../emitter'
import { noop } from '../fn/const'
import { Source } from './type'

export function fromArray<T>(arr: T[]): Source<T> {
  return (sink) => {
    const tb = {
      REQUEST: noop,
      END: noop,
    }
    sink.START(tb)
    for (const a of arr) {
      sink.DATA(a)
    }
    sink.END()
  }
}

export function fromEmitter<T>(emitter: Emitter<[T]>): Source<T> {
  return (sink) => {
    const emitterCallback = (a: T) => {
      sink.DATA(a)
    }
    emitter.on(emitterCallback)

    const srcTalkback = {
      REQUEST: noop,
      END() {
        emitter.off(emitterCallback)
      },
    }

    sink.START(srcTalkback)
  }
}
