import { noop } from '../fn/const'
import { SinkTalkback, Source, SourceTalkback } from './type'

export function onSource<T>(
  listener: (a: T) => void
): (source: Source<T>) => void {
  return (source) => {
    let srcTB: SourceTalkback<any>

    const sink: SinkTalkback<T> = {
      START(talkback) {
        srcTB = talkback
        srcTB.REQUEST()
      },
      DATA(data) {
        listener(data)
        srcTB.REQUEST()
      },
      END: noop,
    }

    source(sink)
  }
}

export type watcherDispose = () => void
export function watch<T>(
  watcher: (a: T) => void
): (source: Source<T>) => watcherDispose {
  return (source) => {
    let srcTB: SourceTalkback<any>

    const sink: SinkTalkback<T> = {
      START(talkback) {
        srcTB = talkback
        srcTB.REQUEST()
      },
      DATA(data) {
        watcher(data)
        srcTB.REQUEST()
      },
      END: noop,
    }

    source(sink)

    return () => {
      srcTB.END()
    }
  }
}
