export function hasOwnKey(o: any, k: string): boolean {
  return Object.prototype.hasOwnProperty.call(o, k)
}
