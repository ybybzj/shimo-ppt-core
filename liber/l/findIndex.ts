export function findIndex<T>(
  pred: (v: T, i?: number, arr?: T[]) => boolean,
  arr: T[]
): number {
  const l = arr.length
  let result = -1
  for (let i = 0; i < l; i++) {
    if (pred(arr[i], i, arr)) {
      result = i
      break
    }
  }
  return result
}
