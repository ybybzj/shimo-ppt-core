import { Dict } from '../pervasive'

export function each<T>(fn: (v: T, key: string, o: Dict<T>) => void, o: Dict<T>)
export function each<T>(fn: (v: T, key: number, o: T[]) => void, o: T[])
export function each(fn, o) {
  let keys, i, l, k
  if (Array.isArray(o)) {
    for (i = 0, l = o.length; i < l; i++) {
      fn(o[i], i, o)
    }
  } else {
    keys = Object.keys(o)
    for (i = 0, l = keys.length; i < l; i++) {
      k = keys[i]
      fn(o[k], k, o)
    }
  }
}
