import { findIndex } from './findIndex'
export function find<T>(
  pred: (t: T, i?: number, arr?: T[]) => boolean,
  arr: T[]
) {
  const idx = findIndex(pred, arr)
  return idx !== -1 ? arr[idx] : undefined
}
