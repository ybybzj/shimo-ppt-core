import { Indexed } from './../pervasive'
export default Map

declare function Map<T, V>(fn: (v: V, index: number, o: Indexed<V>) => T, o: Indexed<V>): T[]

declare function Map<T, V>(fn: (v: V, index: number, o: Indexed<V>) => T): (o: Indexed<V>) => T[]
