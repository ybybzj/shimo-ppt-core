import { type } from '../type'
import valByKeypath from './valByKeypath'
import curry from '../fn/curry'
function values(keys, o) {
  if (type(keys) === 'array') {
    return keys.map(valByKeypath(curry.$, o))
  } else {
    return valByKeypath(keys, o)
  }
}
export default curry(2, values)
