export default MapRecursive

declare function MapRecursive(fn: (v: any) => any, o: object): object
declare function MapRecursive(fn: (v: any) => any): (o: object) => object
