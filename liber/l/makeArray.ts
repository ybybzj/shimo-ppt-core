export function makeArray<T>(length: number, f: (index: number) => T): T[] {
  const v = new Array(length)
  for (let i = 0, i_finish = (length - 1) | 0; i <= i_finish; ++i) {
    v[i] = f(i)
  }
  return v
}

export function makeArrayOf<T>(len: number, item: T): T[] {
  const v = new Array(len)
  for (let i = 0; i < len; ++i) {
    v[i] = item
  }
  return v
}
