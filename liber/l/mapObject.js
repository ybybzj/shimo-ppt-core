import curry from '../fn/curry'
import each from './each'

function mapObject(fn, o) {
  var result = {}
  each(function(v, k, o) {
    result[k] = fn(v, k, o)
  }, o)
  return result
}

export default curry(2, mapObject)
