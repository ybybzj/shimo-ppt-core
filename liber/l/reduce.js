import each from './each'
import curry from '../fn/curry'

function reduce(reducer, accu, o) {
  let result = accu
  each(function(v, k) {
    result = reducer(result, v, k)
  }, o)

  return result
}

export default curry(3, reduce)
