import map from './map'
import mapObject from './mapObject'
import { type } from '../type'
/**
 * deep clone an vanilla object(that can be JSON.stringified)
 * @param {Any} obj
 */
export default function deepClone(obj) {
  const objType = type(obj)

  if (objType !== 'object' && objType !== 'array') {
    return obj
  }

  if (type(obj.toJSON) === 'function') {
    obj = obj.toJSON()
  }

  const mapFn = objType === 'array' ? map : mapObject

  return mapFn(deepClone, obj)
}
