import { Omit } from '../pervasive'

export default function omit<O extends object, K extends keyof O>(keys: K[], o: O): Omit<O, K>
export default function omit<O extends object, K extends keyof O>(keys: K[]): (o: O) => Omit<O, K>
export default function omit<O extends object, K extends keyof O>(
  keys: K[],
  o?: O
): Omit<O, K> | ((o: O) => Omit<O, K>) {
  if (o === undefined) {
    return (o: O) => omit(keys, o)
  }
  const result = {}
  Object.keys(o).forEach((k) => {
    if (keys.indexOf(k as any) === -1) {
      result[k] = o[k]
    }
  })
  return result as Omit<O, K>
}
