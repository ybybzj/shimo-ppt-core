export default flatten

declare function flatten<T>(a: (T[] | T)[] | T, isRecursive: boolean): T[]
