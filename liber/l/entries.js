export default function entries(o) {
  return Object.keys(o).reduce((m, k) => {
    m.push([k, o[k]])
    return m
  }, [])
}
