export default MapObject

declare function MapObject<T extends object, U>(
  fn: (v: T[keyof T], k: keyof T, o: object) => U,
  o: T
): { [k in keyof T]: U }
declare function MapObject<T extends object, U>(
  fn: (v: T[keyof T], k: keyof T, o: object) => U
): (o: T) => { [k in keyof T]: U }
