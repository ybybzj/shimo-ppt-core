import { type } from '../type'
import assert from '../assert'
import mapObject from './mapObject'
import curry from '../fn/curry'

function reduceBy(keyFn, reducer, arr) {
  assert(type.isArray(arr), '[reduceBy]invalid argument type!')
  const grouped = arr.reduce(function(m, item) {
    const key = keyFn(item)
    m[key] = m[key] || []
    m[key].push(item)
    return m
  }, {})
  return mapObject(function(val) {
    return val.reduce(reducer)
  }, grouped)
}

export default curry(3, reduceBy)
