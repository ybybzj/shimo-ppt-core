import map from './map'
import mapObject from './mapObject'
import { type } from '../type'
import assert from '../assert'
import curry from '../fn/curry'

/**
 * deep clone an vanilla object(that can be JSON.stringified)
 * @param {Any} obj
 */
function mapRecursive(fn, obj) {
  assert(type.isFunction(fn), '[mapRecursive] first param need to be function')
  var objType = type(obj),
    mapFn
  if (objType !== 'object' && objType !== 'array') {
    return fn(obj)
  }

  mapFn = objType === 'array' ? map : mapObject

  return mapFn(function(v) {
    return mapRecursive(fn, v)
  }, obj)
}

export default curry(2, mapRecursive)
