import curry from '../fn/curry'
import each from './each'

function map(fn, o) {
  var result = []
  each(function(v, i, o) {
    result.push(fn(v, i, o))
  }, o)
  return result
}
export default curry(2, map)
