export default async function asyncForEach(ary: any[], callback: Function): Promise<void> {
  for (let i = 0, l = ary.length; i < l; i++) {
    await callback(ary[i], i, ary)
  }
}
