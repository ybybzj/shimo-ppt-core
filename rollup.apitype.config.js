const dts = require('rollup-plugin-dts')

module.exports = {
  input: './tmp/tsc-output-types/src/index.d.ts',
  output: {
    file: './dist/sdk.min.d.ts',
    format: 'es',
  },
  plugins: [dts()],
}
