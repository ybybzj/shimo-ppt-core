const devCfg =  require('./rollup.config.es5.js').config
const esBuildCfg = require('./rollup.config.esbuild.js').config

module.exports =  (commandLineArgs) => {
  const env = commandLineArgs.configRelease === true ? 'release' : 'dev'
  switch (commandLineArgs.configSrc) {
    case 'es5':
      return devCfg({env, sourcemap: commandLineArgs.configSourcemap === true})
    case 'esbuild':
      return esBuildCfg({env})
  }

  return {}
  // if(commandLineArgs.configDev === true) {
  //   return devCfg(env)
  // }else if()
}
