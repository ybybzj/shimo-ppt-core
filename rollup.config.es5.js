const commonjs = require('@rollup/plugin-commonjs')
const { nodeResolve } = require('@rollup/plugin-node-resolve')
const { uglify } = require('rollup-plugin-uglify')
const strip = require('@rollup/plugin-strip')
const { reserved } = require('./mangle_reserved.js')
const webWorkerLoader = require('./builder_plugins/rollup-inline-worker-plugin.js')

const sharedPlugins = (env) => {
  const upglifyConfig = {
    compress: true,
    toplevel: true,
    output: {
      beautify: env !== 'release',
    },
    mangle: {
      properties: {
        debug: env !== 'release' ? '' : false,
        keep_quoted: 'strict',
        reserved: reserved,
      },
    },
  }

  return [
    nodeResolve(),
    commonjs(),

    env !== 'release'
      ? undefined
      : strip({
          labels: ['dev'],
        }),
    uglify(upglifyConfig),
  ]
}

exports.config = ({ env, sourcemap }) => {
  return {
    input: './tmp/tsc-output/src/index.js',
    plugins: [
      webWorkerLoader({
        plugins: sharedPlugins(env),
      }),
      ...sharedPlugins(env),
    ],
    output: {
      file: env !== 'release' ? 'dist/sdk.min.js' : 'dist/sdk.js',
      format: 'cjs',
      // name: 'PptCore',
      freeze: false,
      sourcemap: sourcemap ? 'inline' : false,
    },
  }
}
