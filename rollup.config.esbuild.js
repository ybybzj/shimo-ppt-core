const esbuild = require('rollup-plugin-esbuild').default
const { nodeResolve } = require('@rollup/plugin-node-resolve')
const { reserved } = require('./mangle_reserved.js')
const commonjs = require('@rollup/plugin-commonjs')
const { terser } = require('rollup-plugin-terser')
const strip = require('@rollup/plugin-strip')
// const webWorkerLoader = require('rollup-plugin-web-worker-loader')
const webWorkerLoader = require('./builder_plugins/rollup-inline-worker-plugin.js')

const sharedPlugins = (useSourceMap, env) => {
  const terserOptions = {
    ecma: 5,
    toplevel: true,
    compress: {
      passes: 2,
      computed_props: false,
    },
    mangle: {
      properties: {
        debug: env !== 'release' ? '' : false,
        keep_quoted: true,
        reserved: reserved,
      },
    },
    format: {
      beautify: env !== 'release',
      // beautify: true,
    },
  }
  return [
    nodeResolve(),
    esbuild({
      // tsconfig: './tsconfig.json',
      target: 'es6',
      loaders: {
        '.json': 'json',
      },
      sourceMap: useSourceMap,
    }),
    // commonjs(),
    env !== 'release'
      ? undefined
      : strip({
          labels: ['dev'],
        }),
    terser(terserOptions),
  ]
}
exports.config = ({ env }) => {
  const useSourceMap = false // env !== 'release'

  return {
    input: './src/index.ts',
    plugins: [
      webWorkerLoader({ plugins: sharedPlugins(false, env) }),
      ...sharedPlugins(useSourceMap, env),
    ],
    output: {
      file: env !== 'release' ? 'dist/sdk.min.js' : 'dist/sdk.js',
      format: 'cjs',
      // name: 'PptCore',
      freeze: false,
      sourcemap: useSourceMap,
    },
  }
}
